﻿#region Update
/*
    18/05/2017 [TKG] tambah info downpayment dari dokumen ap downpayment yg lain
    12/07/2017 [TKG] bug fixing apabila sales order memiliki dokumen uang muka lbh dari 1 ketika data ditampilkan akan keluar warning.
    13/04/2018 [TKG] filter by site
    07/08/2018 [HAR] Bisa ambil data dari SO contract untuk VIR
    14/08/2018 [WED] BUG typo nama parameter GenerateCustomerCOAFormat
    27/08/2018 [TKG] tambah Customer's PO#
    19/11/2019 [WED/IMS] amount SO Contract inventory belum ketarik
    06/01/2019 [VIN/SIER] Bug Combobox customer saat parameter IsARDownpaymentSONotMandatory bernilai N masih readonly
    13/04/2019 [TKG/IMS] menambah proses journal
    14/07/2020 [TKG/IMS] DP before tax bisa diinput manual.
    17/01/2021 [TKG/PHT] ubah GenerateVoucherRequestDocNo
    04/03/2021 [IBL/IMS] Penyesuaian printout
    05/03/2021 [IBL/IMS] Menambahkan local docno
    05/03/2021 [IBL/IMS] Penyesuaian printout
    26/01/2022 [WED/GSS] BUG saat generate Voucher Request#, belum menambahkan melihat ke parameter IsVoucherDocSeqNoEnabled
    28/01/2022 [MYA/ALL] Penyesuaian jurnal AR Downpayment dengan tambahan additional COA amount
    18/02/2022 [TKG/GSS] merubah GetParameter() dan proses save
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;
using System.Data;
using System.IO;
using System.Reflection;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

#endregion

namespace RunSystem
{
    public partial class FrmARDownpayment2 : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string
            mMenuCode = string.Empty, mAccessInd = string.Empty,
            mDocNo = string.Empty; //if this application is called from other application;
        internal FrmARDownpayment2Find FrmFind;
        private bool
            mIsARDownpayment2SOContractNotMandatory = false,
            mIsARDPUseCOA = false,
            mIsAutoJournalActived = false,
            mIsARDownpayment2JournalEnabled = false,
            mIsCustomerARDPUseLocalDocNo = false,
            mIsARDPProcessTo1Journal = false;
        private string
            mVoucherCodeFormatType = "1",
            mMainCurCode = string.Empty;
        internal string mGenerateCustomerCOAFormat = string.Empty;

        #endregion

        #region Constructor

        public FrmARDownpayment2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Customer's AR Downpayment";

                

                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();

                TcARDownpayment.SelectedTabPage = TpTax;
                Sl.SetLueTaxCode(ref LueTaxCode);
                Sl.SetLueTaxCode(ref LueTaxCode2);
                Sl.SetLueTaxCode(ref LueTaxCode3);

                TcARDownpayment.SelectedTabPage = TpCOA;
                TpCOA.PageVisible = mIsARDPUseCOA;

                TcARDownpayment.SelectedTabPage = TpGeneral;
                Sl.SetLueCurCode(ref LueCurCode);
                Sl.SetLueUserCode(ref LuePIC);
                Sl.SetLueOption(ref LuePaymentType, "VoucherPaymentType");
                Sl.SetLueBankCode(ref LueBankCode);

                if (!mIsCustomerARDPUseLocalDocNo)
                    label24.Visible = MeeLocalDocNo.Visible = false;

                SetFormControl(mState.View);

                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grid Approval Info
            Grd2.Cols.Count = 4;
            Grd2.ReadOnly = true;
            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[] { "Checked By", "Status", "Date", "Remark" },
                    new int[] { 150, 100, 100, 400 }
                );
            Sm.GrdFormatDate(Grd2, new int[] { 2 });
            #endregion

            #region Grid coa

            Grd3.Cols.Count = 9;
            Grd3.FrozenArea.ColCount = 3;

            Sm.GrdHdrWithColWidth(
                    Grd3,
                    new string[] 
                    {
                        //0
                        "",

                        //1-5
                        "Account#",
                        "Description",
                        "Debit",
                        "Credit",
                        "Remark",

                        //6-8
                        "", //Checkbox debit ind
                        "", //Checkbox credit ind
                        "" //CheckBox Db Cr Indicator
                    },
                     new int[] 
                    {
                        //0
                        20,

                        //1-5
                        150, 300, 100, 100, 400,

                         //6-8
                        20, 20, 0
                    }
                );
            Sm.GrdColButton(Grd3, new int[] { 0 });
            Sm.GrdColCheck(Grd3, new int[] { 6, 7, 8 });
            Sm.GrdColReadOnly(Grd3, new int[] { 1, 2 });
            Sm.GrdFormatDec(Grd3, new int[] { 3, 4 }, 0);
            Sm.GrdColInvisible(Grd3, new int[] { 1, 8 }, false);
            Sm.GrdColReadOnly(true, true, Grd3, new int[] { 1, 2 });
            if (!mIsARDPProcessTo1Journal)
                Sm.GrdColInvisible(Grd3, new int[] { 6, 7, 8 }, false);

            Grd3.Cols[6].Move(3);
            Grd3.Cols[7].Move(5);

            #endregion
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueCtCode, LueCurCode, TxtAmt, LuePIC, 
                        MeeRemark, LuePaymentType, LueBankCode, TxtGiroNo, DteDueDt, 
                        MeeCancelReason, TxtDownpaymentPercentage, LueTaxCode, TxtTaxInvoiceNo, DteTaxInvoiceDt, 
                        LueTaxCode2, TxtTaxInvoiceNo2, DteTaxInvoiceDt2, LueTaxCode3, TxtTaxInvoiceNo3, 
                        DteTaxInvoiceDt3, TxtAmtBefTax, MeeLocalDocNo
                    }, true);
                    BtnSODocNo.Enabled = false;
                    Sm.SetControlReadOnly(ChkCancelInd, true);
                    Sm.GrdColReadOnly(true, true, Grd3, new int[] { 0, 3, 4, 5, 6, 7, 8 });
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueCurCode, LuePIC, LuePaymentType, MeeRemark, 
                        TxtDownpaymentPercentage, LueTaxCode, TxtTaxInvoiceNo, DteTaxInvoiceDt, LueTaxCode2, 
                        TxtTaxInvoiceNo2, DteTaxInvoiceDt2, LueTaxCode3, TxtTaxInvoiceNo3, DteTaxInvoiceDt3, 
                        TxtAmtBefTax, MeeLocalDocNo
                    }, false);
                    if (mIsARDownpayment2SOContractNotMandatory)
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueCtCode }, false);
                    BtnSODocNo.Enabled = true;
                    Sm.GrdColReadOnly(false, true, Grd3, new int[] { 0, 3, 4, 5, 6, 7 });
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeCancelReason, ChkCancelInd }, false);
                    ChkCancelInd.Focus();
                    Sm.SetControlReadOnly(ChkCancelInd, false);
                    Grd3.ReadOnly = true;
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, TxtStatus, TxtSODocNo, TxtNoOfDownpayment, 
                LueCtCode, TxtPtName, TxtSOCurCode, LueCurCode, LuePIC,  
                LuePaymentType, LueBankCode, TxtGiroNo, DteDueDt, MeeCancelReason,
                TxtVoucherRequestDocNo, TxtVoucherDocNo, MeeRemark, LueTaxCode, TxtTaxInvoiceNo, 
                DteTaxInvoiceDt, LueTaxCode2, TxtTaxInvoiceNo2, DteTaxInvoiceDt2, LueTaxCode3, 
                TxtTaxInvoiceNo3, DteTaxInvoiceDt3, MeeLocalDocNo
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
            { 
                TxtSOAmt, TxtOtherAmt, TxtAmt, TxtAmtBefTax, TxtDownpaymentPercentage, 
                TxtTaxAmt, TxtTaxAmt2, TxtTaxAmt3, TxtAmtAftTax 
            }, 0);
            ChkCancelInd.Checked = false;
            Grd2.Rows.Clear();
            Grd2.Rows.Count = 1;
            Grd3.Rows.Clear();
            Grd3.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd3, 0, new int[] { 3, 4 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmARDownpayment2Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                if (mIsARDownpayment2SOContractNotMandatory)
                    SetLueCtCode(ref LueCtCode, string.Empty);
                else
                    BtnSODocNo_Click(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            if (!ChkCancelInd.Checked) ParPrint();
        }

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var SQL = new StringBuilder();

                SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelInd, A.SODocNo, ");
                SQL.AppendLine("A.CurCode, A.Amt, A.PIC, A.VoucherRequestDocNo, A.CancelReason, A.Remark, ");
                SQL.AppendLine("B.CurCode As SOCurCode, ");
                SQL.AppendLine("(B.Amt+B.AmtBOM) As SOAmt, ");
                SQL.AppendLine("A.CtCode, ");
                SQL.AppendLine("C.VoucherDocNo,  ");
                SQL.AppendLine("Case IfNull(A.Status, '') ");
                SQL.AppendLine("    When 'O' Then 'Outstanding' ");
                SQL.AppendLine("    When 'A' Then 'Approved' ");
                SQL.AppendLine("    When 'C' Then 'Cancel' End ");
                SQL.AppendLine("As StatusDesc, ");
                SQL.AppendLine("A.PaymentType,A.BankCode, A.GiroNo, A.DueDt,  ");
                SQL.AppendLine("IfNull((Select Count(1) From TblARDownpayment Where SODocNo=A.SODocNo And CancelInd='N'), 0) As NoOfDownpayment, ");
                SQL.AppendLine("A.VoucherRequestDocNo2, ");
                SQL.AppendLine("D.VoucherDocNo As VoucherDocNo2, ");
                SQL.AppendLine("IfNull(( ");
                SQL.AppendLine("    Select Sum(Amt) ");
                SQL.AppendLine("    From TblARDownpayment ");
                SQL.AppendLine("    Where SODocNo=A.SODocNo ");
                SQL.AppendLine("    And DocNo<>@DocNo ");
                SQL.AppendLine("    And CancelInd='N' ");
                SQL.AppendLine("    And Status In ('O', 'A') ");
                SQL.AppendLine("), 0.00) As OtherAmt, ");
                SQL.AppendLine("E.PtName, A.DownpaymentPercentage, A.AmtBefTax, ");
                SQL.AppendLine("A.TaxInvoiceNo, A.TaxInvoiceDt, A.TaxCode, A.TaxAmt, ");
                SQL.AppendLine("A.TaxInvoiceNo2, A.TaxInvoiceDt2, A.TaxCode2, A.TaxAmt2, ");
                SQL.AppendLine("A.TaxInvoiceNo3, A.TaxInvoiceDt3, A.TaxCode3, A.TaxAmt3, ");
                if (mIsCustomerARDPUseLocalDocNo)
                    SQL.AppendLine("A.LocalDocNo ");
                else
                    SQL.AppendLine("Null As LocalDocNo ");
                SQL.AppendLine("From TblARDownpayment A ");
                SQL.AppendLine("Left Join TblSOContractHdr B On A.SODocNo=B.DocNo ");
                SQL.AppendLine("Left Join TblVoucherRequestHdr C On A.VoucherRequestDocNo=C.DocNo ");
                SQL.AppendLine("Left Join TblVoucherRequestHdr D On A.VoucherRequestDocNo2=D.DocNo ");
                SQL.AppendLine("Left Join TblPaymentTerm E On B.PtCode=E.PtCode ");
                SQL.AppendLine("Where A.DocNo=@DocNo;");

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
                Sm.ShowDataInCtrl(
                        ref cm, SQL.ToString(),
                        new string[] 
                        {
                            //0
                            "DocNo", 

                            //1-5
                            "DocDt", "StatusDesc", "CancelInd", "SODocNo", "NoOfDownpayment",
                            
                            //6-10
                            "CtCode", "SOCurcode", "SOAmt", "CurCode", "Amt",  
                            
                            //11-15
                            "PIC", "VoucherRequestDocNo", "VoucherDocNo",  "paymentType", "bankCode", 
                            
                            //16-20
                            "GiroNo", "DueDt", "CancelReason", "VoucherRequestDocNo2", "VoucherDocNo2", 
                            
                            //21-25
                            "Remark", "OtherAmt", "PtName", "DownpaymentPercentage", "AmtBefTax", 
                            
                            //26-30
                            "TaxInvoiceNo", "TaxInvoiceDt", "TaxCode", "TaxAmt", "TaxInvoiceNo2", 
                            
                            //31-35
                            "TaxInvoiceDt2", "TaxCode2", "TaxAmt2", "TaxInvoiceNo3", "TaxInvoiceDt3", 
                            
                            //36-38
                            "TaxCode3", "TaxAmt3", "LocalDocNo"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                            Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                            TxtStatus.EditValue = Sm.DrStr(dr, c[2]);
                            MeeCancelReason.EditValue = Sm.DrStr(dr, c[18]);
                            ChkCancelInd.Checked = Sm.DrStr(dr, c[3]) == "Y";
                            TxtSODocNo.EditValue = Sm.DrStr(dr, c[4]);
                            TxtNoOfDownpayment.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[5]), 2);
                            SetLueCtCode(ref LueCtCode, Sm.DrStr(dr, c[6]));
                            TxtSOCurCode.EditValue = Sm.DrStr(dr, c[7]);
                            TxtSOAmt.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[8]), 0);
                            Sm.SetLue(LueCurCode, Sm.DrStr(dr, c[9]));
                            TxtAmt.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[10]), 0);
                            TxtAmtAftTax.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[10]), 0);
                            Sm.SetLue(LuePIC, Sm.DrStr(dr, c[11]));
                            TxtVoucherRequestDocNo.EditValue = Sm.DrStr(dr, c[12]);
                            TxtVoucherDocNo.EditValue = Sm.DrStr(dr, c[13]);
                            Sm.SetLue(LuePaymentType, Sm.DrStr(dr, c[14]));
                            Sm.SetLue(LueBankCode, Sm.DrStr(dr, c[15]));
                            TxtGiroNo.EditValue = Sm.DrStr(dr, c[16]);
                            Sm.SetDte(DteDueDt, Sm.DrStr(dr, c[17]));
                            TxtVoucherRequestDocNo2.EditValue = (mIsARDPProcessTo1Journal ? Sm.DrStr(dr, c[12]) : Sm.DrStr(dr, c[19]));
                            TxtVoucherDocNo2.EditValue = (mIsARDPProcessTo1Journal ? Sm.DrStr(dr, c[13]) : Sm.DrStr(dr, c[20]));
                            MeeRemark.EditValue = Sm.DrStr(dr, c[21]);
                            TxtOtherAmt.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[22]), 0);
                            TxtPtName.EditValue = Sm.DrStr(dr, c[23]);
                            TxtDownpaymentPercentage.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[24]), 0);
                            TxtAmtBefTax.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[25]), 0);
                            TxtTaxInvoiceNo.EditValue = Sm.DrStr(dr, c[26]);
                            Sm.SetDte(DteTaxInvoiceDt, Sm.DrStr(dr, c[27]));
                            Sm.SetLue(LueTaxCode, Sm.DrStr(dr, c[28]));
                            TxtTaxAmt.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[29]), 0);
                            TxtTaxInvoiceNo2.EditValue = Sm.DrStr(dr, c[30]);
                            Sm.SetDte(DteTaxInvoiceDt2, Sm.DrStr(dr, c[31]));
                            Sm.SetLue(LueTaxCode2, Sm.DrStr(dr, c[32]));
                            TxtTaxAmt2.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[33]), 0);
                            TxtTaxInvoiceNo3.EditValue = Sm.DrStr(dr, c[34]);
                            Sm.SetDte(DteTaxInvoiceDt3, Sm.DrStr(dr, c[35]));
                            Sm.SetLue(LueTaxCode3, Sm.DrStr(dr, c[36]));
                            TxtTaxAmt3.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[37]), 0);
                            MeeLocalDocNo.EditValue = Sm.DrStr(dr, c[38]);
                        }, true
                    );
                ShowDocApproval(DocNo);
                ShowARDownPaymentDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowARDownPaymentDtl(string DocNo)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();
            SQL.AppendLine("Select A.AcNo, B.AcDesc, A.DAmt, A.CAmt, A.Remark, A.ActInd ");
            SQL.AppendLine("From TblARDownPaymentDtl A, TblCOA B ");
            SQL.AppendLine("Where A.DocNo=@DocNo And A.AcNo=B.AcNo Order By A.DNo;");

            Sm.ShowDataInGrid(
                ref Grd3, ref cm, SQL.ToString(),
                new string[] { "AcNo", "AcDesc", "DAmt", "CAmt", "Remark", "ActInd" },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 8, 5);
                    if (Sm.DrStr(dr, c[5]) == "Y")
                    {
                        if (Sm.GetGrdDec(Grd3, Row, 3) > 0)
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 6, 5);
                        else
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 7, 5);
                    }
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd3, Grd3.Rows.Count - 1, new int[] { 3, 4 });
            Sm.FocusGrd(Grd3, 0, 1);
            ComputeCOAAmt();
        }


        private void ShowDocApproval(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.ApprovalDNo, B.UserName, A.LastUpDt, A.Remark, ");
            SQL.AppendLine("Case Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' When 'O' Then 'Outstanding' End As StatusDesc ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Left Join TblUser B On A.UserCode = B.UserCode ");
            SQL.AppendLine("Where A.DocType='ARDownpayment' ");
            SQL.AppendLine("And IfNull(Status, 'O')<>'O' ");
            SQL.AppendLine("And A.DocNo=@DocNo ");
            SQL.AppendLine("Order By A.ApprovalDNo;");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                    ref Grd2, ref cm,
                    SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "UserName",
                        
                        //1-3
                        "StatusDesc","LastUpDt", "Remark"

                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    }, false, false, false, false
            );
            Sm.FocusGrd(Grd2, 0, 0);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No ||
                IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "ARDownpayment", "TblARDownpayment");
            string VoucherRequestDocNo = string.Empty;

            if (mVoucherCodeFormatType == "2")
                VoucherRequestDocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "VoucherRequest", "TblVoucherRequestHdr", "1");
            else
                VoucherRequestDocNo = GenerateVoucherRequestDocNo();

            var cml = new List<MySqlCommand>();
            cml.Add(SaveARDownPayment(DocNo, VoucherRequestDocNo));

            if (Grd3.Rows.Count > 1)
            {
                cml.Add(SaveARDownPaymentDtl2(DocNo));
                //for (int Row = 0; Row < Grd3.Rows.Count; Row++)
                //    if (Sm.GetGrdStr(Grd3, Row, 1).Length > 0) cml.Add(SaveARDownPaymentDtl2(DocNo, Row));
            }

            if (mIsAutoJournalActived && mIsARDownpayment2JournalEnabled) cml.Add(SaveJournal(DocNo));

            Sm.ExecCommands(cml);

            if (!mIsARDPProcessTo1Journal) InsertDataVR(DocNo);

            SetFormControl(mState.View);
            ShowData(DocNo);
        }

        private void InsertDataVR(string ARDocNo)
        {
            var cml = new List<MySqlCommand>();
            if (mIsARDPUseCOA && Grd3.Rows.Count > 1)
            {
                var VoucherRequestDocNo2 = string.Empty;

                if (mVoucherCodeFormatType == "2")
                    VoucherRequestDocNo2 = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "VoucherRequest", "TblVoucherRequestHdr", "1");
                else
                    VoucherRequestDocNo2 = GenerateVoucherRequestDocNo();

                cml.Add(SaveVoucherRequestHdr(VoucherRequestDocNo2, ARDocNo));
                cml.Add(SaveVoucherRequestDtl(VoucherRequestDocNo2));
            }
            Sm.ExecCommands(cml);
        }

        private string GenerateVoucherRequestDocNo()
        {
            var SQL = new StringBuilder();
            bool IsDocSeqNoEnabled = Sm.GetParameterBoo("IsDocSeqNoEnabled");
            bool IsVoucherDocSeqNoEnabled = Sm.GetParameterBoo("IsVoucherDocSeqNoEnabled");
            string DocSeqNo = "4";

            if (IsDocSeqNoEnabled || IsVoucherDocSeqNoEnabled) DocSeqNo = Sm.GetParameter("DocSeqNo");

            SQL.Append("Select Concat( ");
            SQL.Append("@Yr,'/', @Mth, '/', ");
            SQL.Append("( ");
            SQL.Append("    Select IfNull((");
            SQL.Append("    Select Right(Concat(Repeat('0', " + DocSeqNo + "), Convert(DocNo+1, Char)), " + DocSeqNo + ") As Numb From ( ");
            SQL.Append("Select Convert(SUBSTRING(DocNo,7," + DocSeqNo + "), Decimal) As DocNo ");
            //SQL.Append("        Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) As Numb ");
            //SQL.Append("        From ( ");
            //SQL.Append("            Select Convert(Substring(DocNo, 7, 4), Decimal) As DocNo ");
            SQL.Append("            From TblVoucherRequestHdr ");
            SQL.Append("            Where Left(DocNo, 5)= Concat(@Yr,'/', @Mth) ");
            SQL.Append("            Order By Substring(DocNo, 7, " + DocSeqNo + ") Desc Limit 1 ");
            SQL.Append("        ) As Temp ");
            SQL.Append("    ), Right(Concat(Repeat('0', " + DocSeqNo + "), '1'), " + DocSeqNo + ") ");
            //SQL.Append("    ), '0001' ");
            SQL.Append(") As Number ");
            SQL.Append("), '/', ");
            SQL.Append("(Select DocAbbr From TblDocAbbreviation Where DocType='VoucherRequest') ");
            SQL.Append(") As DocNo ");
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetDte(DteDocDt).Substring(2, 2));
            Sm.CmParam<String>(ref cm, "@Mth", Sm.GetDte(DteDocDt).Substring(4, 2));
            return Sm.GetValue(cm);
        }

        //private string GenerateVoucherRequestDocNo()
        //{
        //    var SQL = new StringBuilder();
        //    SQL.Append("Select Concat( ");
        //    SQL.Append("@Yr,'/', @Mth, '/', ");
        //    SQL.Append("( ");
        //    SQL.Append("    Select IfNull((");
        //    SQL.Append("        Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) As Numb ");
        //    SQL.Append("        From ( ");
        //    SQL.Append("            Select Convert(Substring(DocNo, 7, 4), Decimal) As DocNo ");
        //    SQL.Append("            From TblVoucherRequestHdr ");
        //    SQL.Append("            Where Left(DocNo, 5)= Concat(@Yr,'/', @Mth) ");
        //    SQL.Append("            Order By Substring(DocNo, 7, 5) Desc Limit 1 ");
        //    SQL.Append("        ) As Temp ");
        //    SQL.Append("    ), '0001') As Number ");
        //    SQL.Append("), '/', ");
        //    SQL.Append("(Select DocAbbr From TblDocAbbreviation Where DocType='VoucherRequest') ");
        //    SQL.Append(") As DocNo ");
        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };
        //    Sm.CmParam<String>(ref cm, "@Yr", Sm.GetDte(DteDocDt).Substring(2, 2));
        //    Sm.CmParam<String>(ref cm, "@Mth", Sm.GetDte(DteDocDt).Substring(4, 2));
        //    return Sm.GetValue(cm);
        //}

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueCurCode, "CurCode") ||
                Sm.IsTxtEmpty(TxtAmt, "Amount", true) ||
                Sm.IsLueEmpty(LuePaymentType, "Payment Type") ||
                IsPaymentTypeNotValid() ||
                Sm.IsLueEmpty(LuePIC, "Person in charge") ||
                Sm.IsMeeEmpty(MeeRemark, "Remark") ||
                IsGrdValueNotValid() ||
                IsAmtNotValid();
        }

        private bool IsAmtNotValid()
        {
            decimal SOAmt = decimal.Parse(TxtSOAmt.Text);
            decimal OtherAmt = decimal.Parse(TxtOtherAmt.Text);
            decimal Amt = decimal.Parse(TxtAmt.Text);

            if ((OtherAmt + Amt) > SOAmt)
            {
                if (Sm.StdMsgYN("Question",
                    "SO : " + Sm.FormatNum(SOAmt, 0) + Environment.NewLine +
                    "Other Downpayment : " + Sm.FormatNum(OtherAmt, 0) + Environment.NewLine +
                    "Downpayment : " + Sm.FormatNum(Amt, 0) + Environment.NewLine + Environment.NewLine +
                    "AR downpayment amount is bigger than SO contract's amount." + Environment.NewLine +
                    "Do you want to continue to save this data ?"
                    ) == DialogResult.No)
                    return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
            {
                if (Grd3.Rows.Count > 1)
                {
                    for (int RowX = 0; RowX < Grd3.Rows.Count - 1; RowX++)
                    {
                        if (Sm.IsGrdValueEmpty(Grd3, RowX, 1, false, "COA's account is empty.")) return true;
                        if (Sm.GetGrdDec(Grd3, RowX, 3) == 0m && Sm.GetGrdDec(Grd3, RowX, 4) == 0m)
                        {
                            Sm.StdMsg(mMsgType.Warning,
                                "Account# : " + Sm.GetGrdStr(Grd3, RowX, 1) + Environment.NewLine +
                                "Description : " + Sm.GetGrdStr(Grd3, RowX, 2) + Environment.NewLine + Environment.NewLine +
                                "Both debit and credit amount can't be 0.");
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        private bool IsPaymentTypeNotValid()
        {
            if (Sm.CompareStr(Sm.GetLue(LuePaymentType), "B"))
            {
                if (Sm.IsLueEmpty(LueBankCode, "Bank")) return true;
            }

            if (Sm.CompareStr(Sm.GetLue(LuePaymentType), "G") || Sm.CompareStr(Sm.GetLue(LuePaymentType), "K"))
            {
                if (Sm.IsLueEmpty(LueBankCode, "Bank")) return true;
                if (Sm.IsTxtEmpty(TxtGiroNo, "Giro Bilyet/Cheque Number ", false)) return true;
                if (Sm.IsDteEmpty(DteDueDt, "Due Date ")) return true;
            }

            return false;
        }

        private MySqlCommand SaveARDownPayment(string DocNo, string VoucherRequestDocNo)
        {
            var SQL = new StringBuilder();

            #region save AR Downpayment
            SQL.AppendLine("Insert Into TblARDownpayment ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, Status, SODocNo, CtCode, PaymentType, BankCode, GiroNo, DueDt, VoucherRequestDocNo, CurCode, Amt, PIC, ");
            SQL.AppendLine("DownpaymentPercentage, AmtBefTax, ");
            if(mIsCustomerARDPUseLocalDocNo)
                SQL.AppendLine("LocalDocNo, ");
            SQL.AppendLine("TaxInvoiceNo, TaxInvoiceDt, TaxCode, TaxAmt, ");
            SQL.AppendLine("TaxInvoiceNo2, TaxInvoiceDt2, TaxCode2, TaxAmt2, ");
            SQL.AppendLine("TaxInvoiceNo3, TaxInvoiceDt3, TaxCode3, TaxAmt3, ");
            SQL.AppendLine("Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'N', 'O', @SODocNo, @CtCode, @PaymentType, @BankCode, @GiroNo, @DueDt, @VoucherRequestDocNo, @CurCode, @Amt, @PIC, ");
            SQL.AppendLine("@DownpaymentPercentage, @AmtBefTax, ");
            if (mIsCustomerARDPUseLocalDocNo)
                SQL.AppendLine("@LocalDocNo, ");
            SQL.AppendLine("@TaxInvoiceNo, @TaxInvoiceDt, @TaxCode, @TaxAmt, ");
            SQL.AppendLine("@TaxInvoiceNo2, @TaxInvoiceDt2, @TaxCode2, @TaxAmt2, ");
            SQL.AppendLine("@TaxInvoiceNo3, @TaxInvoiceDt3, @TaxCode3, @TaxAmt3, ");
            SQL.AppendLine("@Remark, @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblDocApprovalSetting T ");
            SQL.AppendLine("Where T.DocType='ARDownpayment' ");
            SQL.AppendLine("And (T.StartAmt=0 ");
            SQL.AppendLine("Or T.StartAmt<=IfNull(( ");
            SQL.AppendLine("    Select A.Amt*IfNull(B.Amt, 1) ");
            SQL.AppendLine("    From TblARDownpayment A ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select D1.CurCode1, D1.Amt ");
            SQL.AppendLine("        From TblCurrencyRate D1 ");
            SQL.AppendLine("        Inner Join ( ");
            SQL.AppendLine("            Select CurCode1, Max(RateDt) RateDt ");
            SQL.AppendLine("            From TblCurrencyRate ");
            SQL.AppendLine("            Where CurCode2=(Select ParValue From TblParameter Where ParCode='MainCurCode') ");
            SQL.AppendLine("            Group By CurCode1 ");
            SQL.AppendLine("        ) D2 On D1.CurCode1=D2.CurCode1 And D1.RateDt=D2.RateDt ");
            SQL.AppendLine("    ) B On A.CurCode=B.CurCode1 ");
            SQL.AppendLine("    Where A.DocNo=@DocNo ");
            SQL.AppendLine("), 0)); ");

            SQL.AppendLine("Update TblARDownpayment Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo And Not Exists( ");
            SQL.AppendLine("    Select 1 From TblDocApproval ");
            SQL.AppendLine("    Where DocType='ARDownpayment' ");
            SQL.AppendLine("    And DocNo=@DocNo ");
            SQL.AppendLine("    ); ");

            #endregion

            #region save VR

            SQL.AppendLine("Insert Into TblVoucherRequestHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, Status, DeptCode, DocType, VoucherDocNo, AcType, BankAcCode, PaymentType, GiroNo, BankCode, DueDt, PIC, DocEnclosure, CurCode, Amt, PaymentUser, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@VoucherRequestDocNo, @DocDt, 'N', 'O', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where ParCode='ARDownPaymentDeptCode'), ");
            SQL.AppendLine("'05', Null, 'D', Null, @PaymentType, @GiroNo, @BankCode, @DueDt, @PIC, 0, @CurCode, " + (mIsARDPProcessTo1Journal ? "@Amt2" : "@Amt") + ", Null, ");
            if (mIsARDownpayment2SOContractNotMandatory)
                SQL.AppendLine("Concat(@CtName, '. ', @Remark), ");
            else
                SQL.AppendLine("Concat(@SODocNo, ' (', @CtName, '). ', @Remark), ");
            SQL.AppendLine("@CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblVoucherRequestDtl(DocNo, DNo, Description, Amt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@VoucherRequestDocNo, '001', ");
            if (mIsARDownpayment2SOContractNotMandatory)
                SQL.AppendLine("Concat(@CtName, '. ', @Remark), ");
            else
                SQL.AppendLine("Concat(@SODocNo, ' (', @CtName, '). ', @Remark), ");
            SQL.AppendLine("@Amt, Null, @CreateBy, CurrentDateTime()); ");

            if (mIsARDPProcessTo1Journal && Decimal.Parse(TxtCOAAmt.Text) != 0)
            {
                SQL.AppendLine("Insert Into TblVoucherRequestDtl(DocNo, DNo, Description, Amt, Remark, CreateBy, CreateDt) ");
                SQL.AppendLine("Values (@VoucherRequestDocNo, '002', 'Customer Account Receivable Downpayment', @Amt3, Null, @CreateBy, CurrentDateTime()); ");
            }

            SQL.AppendLine("Update TblVoucherRequestHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@VoucherRequestDocNo And Not Exists( ");
            SQL.AppendLine("    Select DocNo From TblDocApproval ");
            SQL.AppendLine("    Where DocType='VoucherRequest' ");
            SQL.AppendLine("    And DocNo=@VoucherRequestDocNo ");
            SQL.AppendLine("    ); ");

            #endregion

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@LocalDocNo", MeeLocalDocNo.Text);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@SODocNo", TxtSODocNo.Text);
            Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
            Sm.CmParam<String>(ref cm, "@CtName", LueCtCode.GetColumnValue("Col2").ToString());
            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo", VoucherRequestDocNo);
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtAmt.Text));
            Sm.CmParam<Decimal>(ref cm, "@Amt2", Decimal.Parse(TxtAmt.Text) + Decimal.Parse(TxtCOAAmt.Text));
            Sm.CmParam<Decimal>(ref cm, "@Amt3", Decimal.Parse(TxtCOAAmt.Text));
            Sm.CmParam<String>(ref cm, "@PIC", Sm.GetLue(LuePIC));
            Sm.CmParam<String>(ref cm, "@PaymentType", Sm.GetLue(LuePaymentType));
            Sm.CmParam<String>(ref cm, "@BankCode", Sm.GetLue(LueBankCode));
            Sm.CmParam<String>(ref cm, "@GiroNo", TxtGiroNo.Text);
            Sm.CmParamDt(ref cm, "@DueDt", Sm.GetDte(DteDueDt));
            Sm.CmParam<Decimal>(ref cm, "@DownpaymentPercentage", decimal.Parse(TxtDownpaymentPercentage.Text));
            Sm.CmParam<Decimal>(ref cm, "@AmtBefTax", decimal.Parse(TxtAmtBefTax.Text));
            Sm.CmParam<String>(ref cm, "@TaxCode", Sm.GetLue(LueTaxCode));
            Sm.CmParam<String>(ref cm, "@TaxInvoiceNo", TxtTaxInvoiceNo.Text);
            Sm.CmParamDt(ref cm, "@TaxInvoiceDt", Sm.GetDte(DteTaxInvoiceDt));
            Sm.CmParam<Decimal>(ref cm, "@TaxAmt", decimal.Parse(TxtTaxAmt.Text));
            Sm.CmParam<String>(ref cm, "@TaxCode2", Sm.GetLue(LueTaxCode2));
            Sm.CmParam<String>(ref cm, "@TaxInvoiceNo2", TxtTaxInvoiceNo2.Text);
            Sm.CmParamDt(ref cm, "@TaxInvoiceDt2", Sm.GetDte(DteTaxInvoiceDt2));
            Sm.CmParam<Decimal>(ref cm, "@TaxAmt2", decimal.Parse(TxtTaxAmt2.Text));
            Sm.CmParam<String>(ref cm, "@TaxCode3", Sm.GetLue(LueTaxCode3));
            Sm.CmParam<String>(ref cm, "@TaxInvoiceNo3", TxtTaxInvoiceNo3.Text);
            Sm.CmParamDt(ref cm, "@TaxInvoiceDt3", Sm.GetDte(DteTaxInvoiceDt3));
            Sm.CmParam<Decimal>(ref cm, "@TaxAmt3", decimal.Parse(TxtTaxAmt3.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveARDownPaymentDtl2(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* AR Downpayment - Dtl */ ");
            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            for (int r = 0; r < Grd3.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd3, r, 1).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblARDownpaymentDtl(DocNo, DNo, AcNo, DAmt, CAmt, ActInd, Remark, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");

                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");

                    SQL.AppendLine(
                        " (@DocNo, @DNo_" + r.ToString() +
                        ", @AcNo_" + r.ToString() +
                        ", @DAmt_" + r.ToString() +
                        ", @CAmt_" + r.ToString() +
                        ", @ActInd_" + r.ToString() +
                        ", @Remark_" + r.ToString() +
                        ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@AcNo_" + r.ToString(), Sm.GetGrdStr(Grd3, r, 1));
                    Sm.CmParam<Decimal>(ref cm, "@DAmt_" + r.ToString(), Sm.GetGrdDec(Grd3, r, 3));
                    Sm.CmParam<Decimal>(ref cm, "@CAmt_" + r.ToString(), Sm.GetGrdDec(Grd3, r, 4));
                    Sm.CmParam<String>(ref cm, "@ActInd_" + r.ToString(), Sm.GetGrdBool(Grd3, r, 8) ? "Y" : "N");
                    Sm.CmParam<String>(ref cm, "@Remark_" + r.ToString(), Sm.GetGrdStr(Grd3, r, 5));
                }
            }

            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        //private MySqlCommand SaveARDownPaymentDtl2(string DocNo, int Row)
        //{
        //    var cm = new MySqlCommand()
        //    {
        //        CommandText =
        //            "Insert Into TblARDownpaymentDtl(DocNo, DNo, AcNo, DAmt, CAmt, ActInd, Remark, CreateBy, CreateDt) " +
        //            "Values(@DocNo, @DNo, @AcNo, @DAmt, @CAmt, @ActInd, @Remark, @CreateBy, CurrentDateTime()); "
        //    };
        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
        //    Sm.CmParam<String>(ref cm, "@AcNo", Sm.GetGrdStr(Grd3, Row, 1));
        //    Sm.CmParam<Decimal>(ref cm, "@DAmt", Sm.GetGrdDec(Grd3, Row, 3));
        //    Sm.CmParam<Decimal>(ref cm, "@CAmt", Sm.GetGrdDec(Grd3, Row, 4));
        //    Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd3, Row, 5));
        //    Sm.CmParam<String>(ref cm, "@ActInd", Sm.GetGrdBool(Grd3, Row, 8) ? "Y" : "N");
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    return cm;
        //}

        //save vr additional COA
        private MySqlCommand SaveVoucherRequestHdr(string VoucherRequestDocNo, string ARDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblVoucherRequestHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, Status, MInd, DeptCode, DocType, VoucherDocNo, ");
            SQL.AppendLine("AcType, PaymentType, BankAcCode, BankCode, GiroNo, DueDt, ");
            SQL.AppendLine("PIC, CurCode, Amt, PaymentUser, ");
            SQL.AppendLine("Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@DocNo, @DocDt, 'N', 'O', 'N', (Select ParValue From TblParameter Where ParCode='ARDownPaymentDeptCode'), '20', Null, ");
            SQL.AppendLine("'D', @PaymentType, null, @BankCode, @GiroNo, @DueDt, ");
            SQL.AppendLine("@PIC, @CurCode, @Amt, null, ");
            SQL.AppendLine("@Remark, @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblDocApprovalSetting T ");
            SQL.AppendLine("Where T.DocType='VoucherRequest' ");
            SQL.AppendLine("And T.DeptCode In ( ");
            SQL.AppendLine("    Select ParValue From TblParameter ");
            SQL.AppendLine("    Where ParCode='IncomingPaymentDeptCode' ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("And (T.StartAmt=0 ");
            SQL.AppendLine("Or T.StartAmt<=IfNull(( ");
            SQL.AppendLine("    Select A.Amt*IfNull(B.Amt, 1) ");
            SQL.AppendLine("    From TblVoucherRequestHdr A ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select B1.CurCode1, B1.Amt ");
            SQL.AppendLine("        From TblCurrencyRate B1 ");
            SQL.AppendLine("        Inner Join ( ");
            SQL.AppendLine("            Select CurCode1, Max(RateDt) RateDt ");
            SQL.AppendLine("            From TblCurrencyRate ");
            SQL.AppendLine("            Where CurCode2=(Select ParValue From TblParameter Where ParCode='MainCurCode') ");
            SQL.AppendLine("            Group By CurCode1 ");
            SQL.AppendLine("        ) B2 On B1.CurCode1=B2.CurCode1 And B1.RateDt=B2.RateDt ");
            SQL.AppendLine("    ) B On A.CurCode=B.CurCode1 ");
            SQL.AppendLine("    Where A.DocNo=@DocNo ");
            SQL.AppendLine("), 0)); ");

            SQL.AppendLine("Update TblVoucherRequestHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select DocNo From TblDocApproval ");
            SQL.AppendLine("    Where DocType='VoucherRequest' And DocNo=@DocNo ");
            SQL.AppendLine("); ");

            SQL.AppendLine("Update TblARDownPayment Set VoucherRequestDocNo2=@DocNo ");
            SQL.AppendLine("Where DocNo=@ARDocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", VoucherRequestDocNo);
            Sm.CmParam<String>(ref cm, "@ARDocNo", ARDocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetParameter("IncomingPaymentDeptCode"));
            Sm.CmParam<String>(ref cm, "@PaymentType", Sm.GetLue(LuePaymentType));
            Sm.CmParam<String>(ref cm, "@BankCode", Sm.GetLue(LueBankCode));
            Sm.CmParam<String>(ref cm, "@GiroNo", TxtGiroNo.Text);
            Sm.CmParamDt(ref cm, "@DueDt", Sm.GetDte(DteDueDt));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<String>(ref cm, "@PIC", Sm.GetLue(LuePIC));
            Sm.CmParam<Decimal>(ref cm, "@Amt", decimal.Parse(TxtCOAAmt.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveVoucherRequestDtl(string VoucherRequestDocNo)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblVoucherRequestDtl(DocNo, DNo, Description, Amt, Remark, CreateBy, CreateDt) " +
                    "Values (@DocNo, @DNo, @Description, @Amt, Null, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", VoucherRequestDocNo);
            Sm.CmParam<String>(ref cm, "@DNo", "001");
            Sm.CmParam<String>(ref cm, "@Description", MeeRemark.Text);
            Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtCOAAmt.Text));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal(string DocNo)
        {
            var SQL = new StringBuilder();
            var CurCode = Sm.GetLue(LueCurCode);

            SQL.AppendLine("Set @Row:=0; ");
            SQL.AppendLine("Set @Dt:=CurrentDateTime(); ");
            SQL.AppendLine("Set @JournalDocNo:=");
            SQL.AppendLine(Sm.GetNewJournalDocNo(Sm.GetDte(DteDocDt), 1));
            SQL.AppendLine(";");

            SQL.AppendLine("Update TblARDownpayment Set JournalDocNo=@JournalDocNo Where DocNo=@DocNo;");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@JournalDocNo, @DocDt, ");
            SQL.AppendLine("Concat('Customer AR Downpayment : ', @DocNo), ");
            SQL.AppendLine("@MenuCode, (Select MenuDesc From TblMenu Where MenuCode=@MenuCode), ");
            SQL.AppendLine("@Remark, @UserCode, @Dt); ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, Remark, CreateBy, CreateDt) ");

            SQL.AppendLine("Select @JournalDocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("T.AcNo, T.DAmt, T.CAMt, Null As Remark, @UserCode As CreateBy, @Dt As CreateDt ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From (");

            SQL.AppendLine("        Select Concat(B.ParValue, A.CtCode) As AcNo, ");
            SQL.AppendLine("        A.Amt As DAmt, ");
            SQL.AppendLine("        0.00 As CAmt ");
            SQL.AppendLine("        From TblARDownpayment A ");
            SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='CustomerAcNoAR' And B.ParValue Is Not Null ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");
            SQL.AppendLine("        And A.CurCode=@MainCurCode ");

            SQL.AppendLine("    Union All ");
            SQL.AppendLine("        Select Concat(B.ParValue, A.CtCode) As AcNo, ");
            SQL.AppendLine("        0.00 As DAmt, ");
            SQL.AppendLine("        A.AmtBefTax As CAmt ");
            SQL.AppendLine("        From TblARDownpayment A ");
            SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='CustomerAcNoDownPayment' And B.ParValue Is Not Null ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");
            SQL.AppendLine("        And A.CurCode=@MainCurCode ");

            SQL.AppendLine("    Union All ");
            SQL.AppendLine("        Select B.AcNo2 As AcNo, ");
            SQL.AppendLine("        0.00 As DAmt, ");
            SQL.AppendLine("        A.TaxAmt As CAmt ");
            SQL.AppendLine("        From TblARDownpayment A ");
            SQL.AppendLine("        Inner Join TblTax B On A.TaxCode=B.TaxCode And B.AcNo2 Is Not Null ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");
            SQL.AppendLine("        And A.TaxCode Is Not Null ");
            SQL.AppendLine("        And A.CurCode=@MainCurCode ");

            SQL.AppendLine("    Union All ");
            SQL.AppendLine("        Select B.AcNo2 As AcNo, ");
            SQL.AppendLine("        0.00 As DAmt, ");
            SQL.AppendLine("        A.TaxAmt2 As CAmt ");
            SQL.AppendLine("        From TblARDownpayment A ");
            SQL.AppendLine("        Inner Join TblTax B On A.TaxCode2=B.TaxCode And B.AcNo2 Is Not Null ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");
            SQL.AppendLine("        And A.TaxCode2 Is Not Null ");
            SQL.AppendLine("        And A.CurCode=@MainCurCode ");

            SQL.AppendLine("    Union All ");
            SQL.AppendLine("        Select B.AcNo2 As AcNo, ");
            SQL.AppendLine("        0.00 As DAmt, ");
            SQL.AppendLine("        A.TaxAmt3 As CAmt ");
            SQL.AppendLine("        From TblARDownpayment A ");
            SQL.AppendLine("        Inner Join TblTax B On A.TaxCode3=B.TaxCode And B.AcNo2 Is Not Null ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");
            SQL.AppendLine("        And A.TaxCode3 Is Not Null ");
            SQL.AppendLine("        And A.CurCode=@MainCurCode ");

            SQL.AppendLine("    Union All ");
            SQL.AppendLine("        Select B.AcNo, B.DAmt, B.CAmt ");
            SQL.AppendLine("        From TblARDownpayment A ");
            SQL.AppendLine("        Inner Join TblARDownpaymentDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");
            SQL.AppendLine("        And A.CurCode=@MainCurCode ");

            SQL.AppendLine("    Union All ");
            SQL.AppendLine("        Select Concat(B.ParValue, A.CtCode) As AcNo, ");
            SQL.AppendLine("        IfNull(( ");
            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
            SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
            SQL.AppendLine("        ), 0.00) * ");
            SQL.AppendLine("        A.Amt As DAmt, ");
            SQL.AppendLine("        0.00 As CAmt ");
            SQL.AppendLine("        From TblARDownpayment A ");
            SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='CustomerAcNoAR' And B.ParValue Is Not Null ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");
            SQL.AppendLine("        And A.CurCode<>@MainCurCode ");

            SQL.AppendLine("    Union All ");
            SQL.AppendLine("        Select Concat(B.ParValue, A.CtCode) As AcNo, ");
            SQL.AppendLine("        0.00 As DAmt, ");
            SQL.AppendLine("        IfNull(( ");
            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
            SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
            SQL.AppendLine("        ), 0.00) * ");
            SQL.AppendLine("        A.AmtBefTax As CAmt ");
            SQL.AppendLine("        From TblARDownpayment A ");
            SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='CustomerAcNoDownPayment' And B.ParValue Is Not Null ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");
            SQL.AppendLine("        And A.CurCode<>@MainCurCode ");


            SQL.AppendLine("    Union All ");
            SQL.AppendLine("        Select B.AcNo2 As AcNo, ");
            SQL.AppendLine("        0.00 As DAmt, ");
            SQL.AppendLine("        IfNull(( ");
            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
            SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
            SQL.AppendLine("        ), 0.00) * ");
            SQL.AppendLine("        A.TaxAmt As CAmt ");
            SQL.AppendLine("        From TblARDownpayment A ");
            SQL.AppendLine("        Inner Join TblTax B On A.TaxCode=B.TaxCode And B.AcNo2 Is Not Null ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");
            SQL.AppendLine("        And A.TaxCode Is Not Null ");
            SQL.AppendLine("        And A.CurCode<>@MainCurCode ");

            SQL.AppendLine("    Union All ");
            SQL.AppendLine("        Select B.AcNo2 As AcNo, ");
            SQL.AppendLine("        0.00 As DAmt, ");
            SQL.AppendLine("        IfNull(( ");
            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
            SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
            SQL.AppendLine("        ), 0.00) * ");
            SQL.AppendLine("        A.TaxAmt2 As CAmt ");
            SQL.AppendLine("        From TblARDownpayment A ");
            SQL.AppendLine("        Inner Join TblTax B On A.TaxCode2=B.TaxCode And B.AcNo2 Is Not Null ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");
            SQL.AppendLine("        And A.TaxCode2 Is Not Null ");
            SQL.AppendLine("        And A.CurCode<>@MainCurCode ");

            SQL.AppendLine("    Union All ");
            SQL.AppendLine("        Select B.AcNo2 As AcNo, ");
            SQL.AppendLine("        0.00 As DAmt, ");
            SQL.AppendLine("        IfNull(( ");
            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
            SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
            SQL.AppendLine("        ), 0.00) * ");
            SQL.AppendLine("        A.TaxAmt3 As CAmt ");
            SQL.AppendLine("        From TblARDownpayment A ");
            SQL.AppendLine("        Inner Join TblTax B On A.TaxCode3=B.TaxCode And B.AcNo2 Is Not Null ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");
            SQL.AppendLine("        And A.TaxCode3 Is Not Null ");
            SQL.AppendLine("        And A.CurCode<>@MainCurCode ");

            SQL.AppendLine("    Union All ");
            SQL.AppendLine("        Select B.AcNo, ");
            SQL.AppendLine("        IfNull(( ");
            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
            SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
            SQL.AppendLine("        ), 0.00) * ");
            SQL.AppendLine("        B.DAmt, ");
            SQL.AppendLine("        IfNull(( ");
            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
            SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
            SQL.AppendLine("        ), 0.00) * ");
            SQL.AppendLine("        B.CAmt ");
            SQL.AppendLine("        From TblARDownpayment A ");
            SQL.AppendLine("        Inner Join TblARDownpaymentDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");
            SQL.AppendLine("        And A.CurCode<>@MainCurCode ");

            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select ParValue As AcNo, ");
            SQL.AppendLine("        0.00 As DAmt, ");
            SQL.AppendLine("        0.00 As CAmt ");
            SQL.AppendLine("        From TblARDownpayment A ");
            SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='AcNoForForeignCurrencyExchangeGains' And B.ParValue Is Not Null ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");
            SQL.AppendLine("        And A.CurCode<>@MainCurCode ");

            SQL.AppendLine("    ) Tbl ");
            SQL.AppendLine("    Where AcNo Is Not Null ");
            SQL.AppendLine("    Group By AcNo  ");
            SQL.AppendLine(") T;  ");


            SQL.AppendLine("Update TblJournalDtl A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select DAmt, CAmt From (");
            SQL.AppendLine("        Select Sum(DAmt) As DAmt, Sum(CAmt) As CAmt ");
            SQL.AppendLine("        From TblJournalDtl Where DocNo=@JournalDocNo ");
            SQL.AppendLine("    ) Tbl ");
            SQL.AppendLine(") B On 1=1 ");
            SQL.AppendLine("Set ");
            SQL.AppendLine("    A.DAmt=Case When B.DAmt<B.CAmt Then Abs(B.CAmt-B.DAmt) Else 0 End, ");
            SQL.AppendLine("    A.CAmt=Case When B.DAmt>B.CAmt Then Abs(B.DAmt-B.CAmt) Else 0 End ");
            SQL.AppendLine("Where A.DocNo=@JournalDocNo ");
            SQL.AppendLine("And A.AcNo In ( ");
            SQL.AppendLine("    Select ParValue From TblParameter ");
            SQL.AppendLine("    Where ParCode='AcNoForForeignCurrencyExchangeGains' ");
            SQL.AppendLine("    And ParValue Is Not Null ");
            SQL.AppendLine("    ); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@MainCurCode", mMainCurCode);
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (IsEditedDataNotValid() || Sm.StdMsgYN("Save", string.Empty) == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditARDownpayment());

            if (mIsAutoJournalActived && mIsARDownpayment2JournalEnabled) cml.Add(SaveJournal2());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                Sm.IsClosingJournalInvalid(mIsAutoJournalActived && mIsARDownpayment2JournalEnabled, true, Sm.GetDte(DteDocDt)) ||
                IsDataCancelledAlready() ||
                IsDataProcessedAlready() ||
                Sm.IsMeeEmpty(MeeCancelReason, "Reason for cancellation") ||
                IsCancelIndNotTrue();
        }

        private bool IsDataCancelledAlready()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select DocNo From TblARDownpayment " +
                    "Where (CancelInd='Y' Or Status='C') And DocNo=@DocNo "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data is already cancelled.");
                return true;
            }
            return false;
        }

        private bool IsCancelIndNotTrue()
        {
            if (ChkCancelInd.Checked == false)
            {
                Sm.StdMsg(mMsgType.Warning, "You must cancel this document.");
                return true;
            }
            return false;
        }

        private bool IsDataProcessedAlready()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo ");
            SQL.AppendLine("From TblVoucherRequestHdr A, TblVoucherHdr B ");
            SQL.AppendLine("Where A.VoucherDocNo=B.DocNo And B.CancelInd='N' ");
            SQL.AppendLine("And A.DocNo=@DocNo ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtVoucherRequestDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "Data already processed into voucher.");
                return true;
            }

            return false;
        }

        private MySqlCommand EditARDownpayment()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblARDownpayment Set ");
            SQL.AppendLine("    CancelInd='Y', CancelReason=@CancelReason, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N' And Status<>'C'; ");

            SQL.AppendLine("Update TblVoucherRequestHdr Set ");
            SQL.AppendLine("    CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where (DocNo=@VoucherRequestDocNo Or DocNo=@VoucherRequestDocNo2)  And CancelInd='N' And Status<>'C'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo", TxtVoucherRequestDocNo.Text);
            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo2", TxtVoucherRequestDocNo2.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal2()
        {
            var SQL = new StringBuilder();
            var CurrentDt = Sm.ServerCurrentDate();
            var DocDt = Sm.GetDte(DteDocDt);
            var IsClosingJournalUseCurrentDt = Sm.IsClosingJournalUseCurrentDt(DocDt);

            SQL.AppendLine("Set @JournalDocNo:=");
            SQL.AppendLine(Sm.GetNewJournalDocNo(Sm.GetDte(DteDocDt), 1));
            SQL.AppendLine(";");

            SQL.AppendLine("Update TblARDownpayment Set JournalDocNo2=@JournalDocNo ");
            SQL.AppendLine("Where DocNo=@DocNo And JournalDocNo Is Not Null; ");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, ");
            if (IsClosingJournalUseCurrentDt)
                SQL.AppendLine("Replace(CurDate(), '-', ''), ");
            else
                SQL.AppendLine("DocDt, ");
            SQL.AppendLine("Concat('Cancelling ', JnDesc) As JnDesc, ");
            SQL.AppendLine("MenuCode, MenuDesc, Remark, CreateBy, CreateDt ");
            SQL.AppendLine("From TblJournalHdr ");
            SQL.AppendLine("Where DocNo In (Select JournalDocNo From TblARDownpayment Where DocNo=@DocNo And JournalDocNo Is Not Null); ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, DNo, AcNo, CAMt As DAmt, DAmt As CAmt, EntCode, CreateBy, CreateDt ");
            SQL.AppendLine("From TblJournalDtl ");
            SQL.AppendLine("Where DocNo In (Select JournalDocNo From TblARDownpayment Where DocNo=@DocNo And JournalDocNo Is Not Null); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            return cm;
        }

        #endregion

        #endregion

        #region Additional Method

        internal string GetSelectedAcNo()
        {
            var SQL = string.Empty;
            if (Grd3.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd3, Row, 1).Length != 0)
                        SQL += "##" + Sm.GetGrdStr(Grd3, Row, 1) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        private void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'VoucherCodeFormatType', 'GenerateCustomerCOAFormat', 'IsARDPUseCOA', 'IsARDPProcessTo1Journal', 'MainCurCode', ");
            SQL.AppendLine("'IsARDownpayment2SOContractNotMandatory', 'IsAutoJournalActived', 'IsARDownpayment2JournalEnabled', 'IsCustomerARDPUseLocalDocNo' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsCustomerARDPUseLocalDocNo": mIsCustomerARDPUseLocalDocNo = ParValue == "Y"; break;
                            case "IsARDownpayment2JournalEnabled": mIsARDownpayment2JournalEnabled = ParValue == "Y"; break;
                            case "IsAutoJournalActived": mIsAutoJournalActived = ParValue == "Y"; break;
                            case "IsARDownpayment2SOContractNotMandatory": mIsARDownpayment2SOContractNotMandatory = ParValue == "Y"; break;
                            case "IsARDPUseCOA": mIsARDPUseCOA = ParValue == "Y"; break;
                            case "IsARDPProcessTo1Journal": mIsARDPProcessTo1Journal = ParValue == "Y"; break;

                            //string
                            case "MainCurCode": mMainCurCode = ParValue; break;
                            case "VoucherCodeFormatType": mVoucherCodeFormatType = ParValue; break;
                            case "GenerateCustomerCOAFormat": mGenerateCustomerCOAFormat = ParValue; break;
                            
                        }
                    }
                }
                dr.Close();
            }
        }

        #region Convert To Words

        private static string[] _ones =
            {
                "Zero",
                "One",
                "Two",
                "Three",
                "Four",
                "Five",
                "Six",
                "Seven",
                "Eight",
                "Nine"
            };

        private static string[] _teens =
            {
                "Ten",
                "Eleven",
                "Twelve",
                "Thirteen",
                "Fourteen",
                "Fifteen",
                "Sixteen",
                "Seventeen",
                "Eighteen",
                "Nineteen"
            };

        private static string[] _tens =
            {
                "",
                "Ten",
                "Twenty",
                "Thirty",
                "Forty",
                "Fifty",
                "Sixty",
                "Seventy",
                "Eighty",
                "Ninety"
            };

        // US Nnumbering:
        private static string[] _thousands =
            {
                "",
                "Thousand",
                "Million",
                "Billion",
                "Trillion",
                "Quadrillion"
            };


        private static string Convert(decimal value)
        {
            string digits, temp;
            bool showThousands = false;
            bool allZeros = true;

            StringBuilder builder = new StringBuilder();
            digits = ((long)value).ToString();
            for (int i = digits.Length - 1; i >= 0; i--)
            {
                int ndigit = (int)(digits[i] - '0');
                int column = (digits.Length - (i + 1));

                switch (column % 3)
                {
                    case 0:        // Ones position
                        showThousands = true;
                        if (i == 0)
                        {
                            temp = String.Format("{0} ", _ones[ndigit]);
                        }
                        else if (digits[i - 1] == '1')
                        {
                            temp = String.Format("{0} ", _teens[ndigit]);
                            i--;
                        }
                        else if (ndigit != 0)
                        {
                            temp = String.Format("{0} ", _ones[ndigit]);
                        }
                        else
                        {
                            temp = String.Empty;
                            if (digits[i - 1] != '0' || (i > 1 && digits[i - 2] != '0'))
                                showThousands = true;
                            else
                                showThousands = false;
                        }

                        if (showThousands)
                        {
                            if (column > 0)
                            {
                                temp = String.Format("{0}{1}{2}",
                                    temp,
                                    _thousands[column / 3],
                                    allZeros ? " " : " ");
                            }
                            allZeros = false;
                        }
                        builder.Insert(0, temp);
                        break;

                    case 1:        // Tens column
                        if (ndigit > 0)
                        {
                            temp = String.Format("{0}{1}",
                                _tens[ndigit],
                                (digits[i + 1] != '0') ? " " : " ");
                            builder.Insert(0, temp);
                        }
                        break;

                    case 2:        // Hundreds column
                        if (ndigit > 0)
                        {
                            temp = String.Format("{0} Hundred ", _ones[ndigit]);
                            builder.Insert(0, temp);
                        }
                        break;
                }
            }

            string cents = value.ToString();
            decimal cettt = Decimal.Parse(cents.Substring(cents.Length - 2, 2));
            string cent = Sm.Terbilang2(cettt);
            builder.AppendFormat("Dollars And " + cent + " Cents # ", (value - (long)value) * 100);

            return String.Format("{0}{1} ",
                Char.ToUpper(builder[0]),
                builder.ToString(1, builder.Length - 1));
        }
        #endregion

        private void ParPrint()
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;
            var ld = new List<ARDep>();
            var ldIMS = new List<ARDPInvoiceIMS>();
            var lsign = new List<ARDPSign>();
            string DocTitle = Sm.GetValue("Select ParValue From TblParameter Where ParCode = 'DocTitle'");
            string[] TableName = { "ARDep", "ARDPInvoiceIMS", "ARDPSign" };
            List<IList> myLists = new List<IList>();

            #region Header

            var cm = new MySqlCommand();

            var SQL = new StringBuilder();
            SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyAddressCity', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyPhone', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle5') As 'CompanyFax', ");
            SQL.AppendLine("@CompanyFooterImage As 'CompanyFooterImage', ");
            if (DocTitle == "IMS")
                SQL.AppendLine("DATE_FORMAT(A.DocDt,'%d %M %Y') As DocDt, A.LocalDocNo, ");
            else
                SQL.AppendLine("DATE_FORMAT(A.DocDt,'%M %d, %Y') As DocDt, Null As LocalDocNo, ");
            SQL.AppendLine("A.DocNo, B.CurCode, IfNull(A.Amt, 0.00) As Amt, ");
            SQL.AppendLine("(IfNull(A.AmtBefTax, 0.00) + IfNull(A.TaxAmt, 0.00) + IfNull(A.TaxAmt2, 0.00) + IfNull(A.TaxAmt3, 0.00)) As TotalAmount, ");
            SQL.AppendLine("F.TaxName, A.TaxAmt, G.TaxName TaxName2, A.TaxAmt2, H.TaxName TaxName3, A.TaxAmt3, ");

            if (mGenerateCustomerCOAFormat == "1")
                SQL.AppendLine("IfNull(B.Amt, 0.00) As SoAmt, ");
            else
                SQL.AppendLine("(IfNull(B.Amt, 0.00) + IfNull(B.AmtBOM, 0.00)) As SoAmt, ");
            SQL.AppendLine("C.CtName, A.SODocNo, E.DocNo As DocNoVC, DATE_FORMAT(E.DocDt,'%M %d, %Y') As DocDtVC, C.Address, A.Remark, I.BankName, I.BankAcNo ");
            SQL.AppendLine("From TblARDownpayment A   ");
            if (mGenerateCustomerCOAFormat == "1")
                SQL.AppendLine("Left Join TblSOHdr B On A.SODocNo=B.DocNo ");
            else
                SQL.AppendLine("Left Join TblSOContractHdr B On A.SODocNo=B.DocNo ");

            if (DocTitle == "IMS")
                SQL.AppendLine("Left Join TblCustomer C On A.CtCode=C.CtCode ");
            else
                SQL.AppendLine("Inner Join TblCustomer C On B.CtCode=C.CtCode ");
            SQL.AppendLine("Left Join TblVoucherRequestHdr D On A.VoucherRequestDocNo=D.DocNo And D.CancelInd= 'N'");
            SQL.AppendLine("Left Join tblvoucherhdr E On D.DocNo=E.VoucherRequestDocNo And E.CancelInd= 'N'  ");
            SQL.AppendLine("Left Join TblTax F On A.TaxCode = F.TaxCode ");
            SQL.AppendLine("Left Join TblTax G On A.TaxCode2 = G.TaxCode ");
            SQL.AppendLine("Left Join TblTax H On A.TaxCode3 = H.TaxCode ");
            SQL.AppendLine("Left Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select T2.BankCode, T2.BankName, T3.BankAcNo ");
            SQL.AppendLine("    From TblARDownpayment T1 ");
	        SQL.AppendLine("    Left Join TblBank T2 On T1.BankCode = T2.BankCode ");
            SQL.AppendLine("    Left Join TblBankAccount T3 On T2.BankCode = T3.BankCode ");
            SQL.AppendLine("    Where T1.DocNo = @DocNo ");
            SQL.AppendLine("    And T3.CurCode = 'IDR' ");
            SQL.AppendLine("    Limit 1 ");
            SQL.AppendLine(") I On A.BankCode = I.BankCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                Sm.CmParam<String>(ref cm, "@CompanyFooterImage", Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + @"\FooterImage.png");
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                    {
                     //0
                     "CompanyLogo",

                     //1-5
                     "CompanyName",
                     "CompanyAddress",
                     "CompanyAddressCity",
                     "CompanyPhone",
                     "CompanyFax",
                     //6-10
                     "DocNo",
                     "DocDt",
                     "CurCode",
                     "Amt",
                     "SoAmt",
                     //11-15
                     "CtName",
                     "SODocNo",
                     "DocNoVC",
                     "DocDtVC",
                     "Address",
                     //16-20
                     "Remark",
                     "TotalAmount",
                     "TaxName",
                     "TaxAmt",
                     "TaxName2",
                     //21-25
                     "TaxAmt2",
                     "TaxName3",
                     "TaxAmt3",
                     "BankName",
                     "BankAcNo",
                     //26
                     "LocalDocNo",
                     "CompanyFooterImage"
                    });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ld.Add(new ARDep()
                        {
                            CompanyLogo = Sm.DrStr(dr, c[0]),

                            CompanyName = Sm.DrStr(dr, c[1]),
                            CompanyAddress = Sm.DrStr(dr, c[2]),
                            CompanyAddressCity = Sm.DrStr(dr, c[3]),
                            CompanyPhone = Sm.DrStr(dr, c[4]),
                            CompanyFax = Sm.DrStr(dr, c[5]),

                            DocNo = Sm.DrStr(dr, c[6]),
                            DocDt = Sm.DrStr(dr, c[7]),
                            CurCode = Sm.DrStr(dr, c[8]),
                            Amt = Sm.DrDec(dr, c[9]),
                            Terbilang = Sm.Terbilang(Sm.DrDec(dr, c[10])),
                            Terbilang2 = Convert(Sm.DrDec(dr, c[10])),

                            SoAmt = Sm.DrDec(dr, c[10]),
                            CtName = Sm.DrStr(dr, c[11]),
                            SODocNo = Sm.DrStr(dr, c[12]),
                            DocNoVC = Sm.DrStr(dr, c[13]),
                            DocDtVC = Sm.DrStr(dr, c[14]),

                            Address = Sm.DrStr(dr, c[15]),
                            Remark = Sm.DrStr(dr, c[16]),
                            TotalAmount = Sm.DrDec(dr, c[17]),
                            TotalAmtTerbilang = Sm.Terbilang(Sm.DrDec(dr, c[17])),
                            TaxName = Sm.DrStr(dr, c[18]),

                            TaxAmt = Sm.DrDec(dr, c[19]),
                            TaxName2 = Sm.DrStr(dr, c[20]),
                            TaxAmt2 = Sm.DrDec(dr, c[21]),
                            TaxName3 = Sm.DrStr(dr, c[22]),
                            TaxAmt3 = Sm.DrDec(dr, c[23]),

                            BankName = Sm.DrStr(dr, c[24]),
                            BankAcNo = Sm.DrStr(dr, c[25]),
                            LocalDocNo = Sm.DrStr(dr, c[26]),
                            CompanyFooterImage = Sm.DrStr(dr, c[27]),

                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
                        });
                    }
                }
                dr.Close();
            }
            myLists.Add(ld);

            #endregion

            #region IMS

            #region Invoice

            var cm2 = new MySqlCommand();
            var SQL2 = new StringBuilder();
            SQL2.AppendLine("Select Remark, IfNull(AmtBefTax, 0.00) As AmtBefTax ");
            SQL2.AppendLine("From TblARDownpayment ");
            SQL2.AppendLine("Where DocNo = @DocNo ");
            using (var cn2 = new MySqlConnection(Gv.ConnectionString))
            {
                cn2.Open();
                cm2.Connection = cn2;
                cm2.CommandText = SQL2.ToString();
                Sm.CmParam<String>(ref cm2, "@DocNo", TxtDocNo.Text);
                var dr2 = cm2.ExecuteReader();
                var c2 = Sm.GetOrdinal(dr2, new string[] 
                    {
                     //0
                     "Remark",

                     //1
                     "AmtBefTax",
                    });
                if (dr2.HasRows)
                {
                    while (dr2.Read())
                    {
                        ldIMS.Add(new ARDPInvoiceIMS()
                        {
                            Remark = Sm.DrStr(dr2, c2[0]),
                            AmtBeforeTax = Sm.DrDec(dr2, c2[1]),
                        });
                    }
                }
                dr2.Close();
            }
            myLists.Add(ldIMS);
            #endregion

            #region Signature
            var cmDtl2 = new MySqlCommand();

            var SQLDtl2 = new StringBuilder();
            using (var cnDtl2 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl2.Open();
                cmDtl2.Connection = cnDtl2;

                SQLDtl2.AppendLine("Select Concat(Upper(left(T4.UserName,1)),Substring(Lower(T4.UserName), 2, Length(T4.UserName))) As UserName, ");
                SQLDtl2.AppendLine("Concat(IfNull(T7.ParValue, ''), T2.UserCode, '.JPG') As Signature, T6.PosName ");
                SQLDtl2.AppendLine("From ");
                SQLDtl2.AppendLine("( ");
                SQLDtl2.AppendLine("    Select A.DocNo, Max(C.Level) MaxLvl ");
                SQLDtl2.AppendLine("    From TblARDownpayment A ");
                SQLDtl2.AppendLine("    Inner Join TblDocapproval B On A.DocNo = B.DocNo ");
                SQLDtl2.AppendLine("    Inner Join TblDocApprovalSetting C On B.DocType = C.DocType And B.ApprovalDNo = C.DNo ");
                SQLDtl2.AppendLine("    Group By A.DocNo ");
                SQLDtl2.AppendLine(") T1 ");
                SQLDtl2.AppendLine("Inner Join TblDocapproval T2 On T1.DocNo = T2.DocNo ");
                SQLDtl2.AppendLine("Inner Join TblDocApprovalSetting T3 On T2.DocType = T3.DocType  ");
                SQLDtl2.AppendLine("    And T2.ApprovalDNo = T3.DNo And T3.`Level` = T1.MaxLvl ");
                SQLDtl2.AppendLine("Inner Join TblUser T4 On T2.UserCode = T4.UserCode ");
                SQLDtl2.AppendLine("Left Join TblEmployee T5 On T4.UserCode=T5.UserCode  ");
                SQLDtl2.AppendLine("Left Join TblPosition T6 On T5.PosCode=T6.PosCode  ");
                SQLDtl2.AppendLine("Left Join TblParameter T7 On T7.ParCode = 'ImgFileSignature' ");
                SQLDtl2.AppendLine("Where T1.DocNo = @DocNo ");
                SQLDtl2.AppendLine("Group  By T7.ParValue, T2.UserCode, T4.UserName, T6.PosName; ");

                cmDtl2.CommandText = SQLDtl2.ToString();
                Sm.CmParam<String>(ref cmDtl2, "@DocNo", TxtDocNo.Text);
                var drDtl2 = cmDtl2.ExecuteReader();
                var cDtl2 = Sm.GetOrdinal(drDtl2, new string[] 
                            {

                             //0
                             "Signature" ,

                             //1-5
                             "UserName" ,
                             "PosName"
                             
            
                            });
                if (drDtl2.HasRows)
                {
                    while (drDtl2.Read())
                    {
                        lsign.Add(new ARDPSign()
                        {
                            Signature = Sm.DrStr(drDtl2, cDtl2[0]),
                            UserName = Sm.DrStr(drDtl2, cDtl2[1]),
                            POSName = Sm.DrStr(drDtl2, cDtl2[2])

                        });
                    }
                }
                drDtl2.Close();
            }
            myLists.Add(lsign);
            #endregion

            #endregion

            if (DocTitle == "IMS")
            {
                Sm.PrintReport("ARDownpayment2IMS_1", myLists, TableName, false);
                Sm.PrintReport("ARDownpayment2IMS_2", myLists, TableName, false);
                Sm.PrintReport("ARDownpayment2IMS_3", myLists, TableName, false);
            }
            else
                Sm.PrintReport("ARDownpayment", myLists, TableName, false);
        }

        internal void SetLueCtCode(ref DXE.LookUpEdit Lue, string CtCode)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select CtCode As Col1, CtName As Col2 ");
                SQL.AppendLine("From TblCustomer ");
                if (CtCode.Length == 0)
                    SQL.AppendLine("Where ActInd='Y' ");
                else
                    SQL.AppendLine("Where CtCode=@CtCode ");
                SQL.AppendLine("Order By CtName; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };

                if (CtCode.Length != 0)
                    Sm.CmParam<String>(ref cm, "@CtCode", CtCode);

                Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
                if (CtCode.Length != 0) Sm.SetLue(LueCtCode, CtCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        internal void ComputeCOAAmt()
        {
            decimal COAAmt = 0m;
            try
            {
                var SQL = new StringBuilder();

                if (mIsARDPProcessTo1Journal)
                {
                    for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                    {
                        if (Sm.GetGrdStr(Grd3, Row, 1).Length > 0 && Sm.GetGrdBool(Grd3, Row, 8))
                        {

                            if (Sm.GetGrdBool(Grd3, Row, 6) && Sm.GetGrdDec(Grd3, Row, 3) != 0)
                            {
                                //kalau dicentang di debit, (-)
                                COAAmt -= Sm.GetGrdDec(Grd3, Row, 3);
                            }
                            if (Sm.GetGrdBool(Grd3, Row, 7) && Sm.GetGrdDec(Grd3, Row, 4) != 0)
                            {
                                //kalau dicentang di credit, (+)
                                COAAmt += Sm.GetGrdDec(Grd3, Row, 4);
                            }
                        }
                    }
                }
                else
                {

                    SQL.AppendLine("Select Concat(C.ParValue, A.CtCode) As AcNo ");
                    SQL.AppendLine("From TblCustomer A ");
                    SQL.AppendLine("Inner Join TblCustomerCategory B On A.CtCtCode=B.CtCtCode ");
                    SQL.AppendLine("Left Join TblParameter C On C.ParCode='CustomerAcNoAR' ");
                    SQL.AppendLine("Where A.CtCtCode is Not Null ");
                    SQL.AppendLine("And A.CtCode=@CtCode Limit 1; ");

                    var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                    Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
                    var AcNo = Sm.GetValue(cm);

                    cm.CommandText = "Select AcType From TblCOA Where AcNo=@AcNo;";
                    Sm.CmParam<String>(ref cm, "@AcNo", AcNo);
                    string AcType = Sm.GetValue(cm);

                    for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                    {
                        if (AcNo.Length > 0 && Sm.CompareStr(AcNo, Sm.GetGrdStr(Grd3, Row, 1)))
                        {
                            if (Sm.GetGrdDec(Grd3, Row, 3) != 0)
                            {
                                if (AcType == "D")
                                    COAAmt += Sm.GetGrdDec(Grd3, Row, 3);
                                else
                                    COAAmt -= Sm.GetGrdDec(Grd3, Row, 3);
                            }
                            if (Sm.GetGrdDec(Grd3, Row, 4) != 0)
                            {
                                if (AcType == "C")
                                    COAAmt += Sm.GetGrdDec(Grd3, Row, 4);
                                else
                                    COAAmt -= Sm.GetGrdDec(Grd3, Row, 4);
                            }
                        }
                    }
                }
                TxtCOAAmt.EditValue = Sm.FormatNum(COAAmt, 0);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        internal void ComputeOtherAmt()
        {
            var OtherAmt = Sm.GetValueDec(
                "Select Amt From TblARDownpayment " +
                "Where SODocNo=@Param And CancelInd='N' And Status In ('O', 'A');",
                TxtSODocNo.Text);
            TxtOtherAmt.EditValue = Sm.FormatNum(OtherAmt, 0);
        }

        internal void ComputeAmtAftTax()
        {
            decimal
                SOContractAmt = 0m,
                DownpaymentPercentage = 0m,
                AmtBefTax = 0m,
                TaxAmt = 0m,
                TaxAmt2 = 0m,
                TaxAmt3 = 0m
                ;
            string
                TaxCode = Sm.GetLue(LueTaxCode),
                TaxCode2 = Sm.GetLue(LueTaxCode2),
                TaxCode3 = Sm.GetLue(LueTaxCode3)
                ;

            SOContractAmt = GetAmtBefTax();

            if (TxtDownpaymentPercentage.Text.Length > 0) DownpaymentPercentage = decimal.Parse(TxtDownpaymentPercentage.Text);
            if (DownpaymentPercentage != 0m)
                AmtBefTax = DownpaymentPercentage * 0.01m * SOContractAmt;
            else
            {
                if (TxtAmtBefTax.Text.Length > 0) AmtBefTax = decimal.Parse(TxtAmtBefTax.Text);
            }
            TxtAmtBefTax.EditValue = Sm.FormatNum(AmtBefTax, 0);

            if (TaxCode.Length != 0)
            {
                var TaxRate = Sm.GetValueDec("Select TaxRate From TblTax Where TaxCode=@Param;", TaxCode);
                if (TaxRate != 0) TaxAmt = TaxRate * 0.01m * AmtBefTax;
            }
            if (TaxCode2.Length != 0)
            {
                var TaxRate2 = Sm.GetValueDec("Select TaxRate From TblTax Where TaxCode=@Param;", TaxCode2);
                if (TaxRate2 != 0) TaxAmt2 = TaxRate2 * 0.01m * AmtBefTax;
            }
            if (TaxCode3.Length != 0)
            {
                var TaxRate3 = Sm.GetValueDec("Select TaxRate From TblTax Where TaxCode=@Param;", TaxCode3);
                if (TaxRate3 != 0) TaxAmt3 = TaxRate3 * 0.01m * AmtBefTax;
            }

            TxtTaxAmt.EditValue = Sm.FormatNum(TaxAmt, 0);
            TxtTaxAmt2.EditValue = Sm.FormatNum(TaxAmt2, 0);
            TxtTaxAmt3.EditValue = Sm.FormatNum(TaxAmt3, 0);
            TxtAmtAftTax.EditValue = Sm.FormatNum(AmtBefTax + TaxAmt + TaxAmt2 + TaxAmt3, 0);
            TxtAmt.EditValue = TxtAmtAftTax.EditValue;
        }

        private decimal GetAmtBefTax()
        {
            var SQL = new StringBuilder();
            var Amt = 0m;

            SQL.AppendLine("Select (A.Amt+A.AmtBOM)-IfNull(B.OtherAmt, 0.00) As Amt ");
            SQL.AppendLine("From TblSOContractHdr A ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T1.DocNo, ");
            SQL.AppendLine("    Sum(T2.Amt) As OtherAmt ");
            SQL.AppendLine("    From TblSOContractHdr T1 ");
            SQL.AppendLine("    Inner Join TblARDownpayment T2 On T1.DocNo=T2.SODocNo ");
            SQL.AppendLine("        And T2.CancelInd='N' ");
            SQL.AppendLine("        And T2.Status In ('O', 'A') ");
            SQL.AppendLine("        And T1.DocNo=T2.SODocNo ");
            SQL.AppendLine("    Where T1.Amt+T1.AmtBOM>0.00 ");
            SQL.AppendLine("    And T1.CancelInd='N' ");
            SQL.AppendLine("    And T1.DocNo=@Param ");
            SQL.AppendLine("    Group By T1.DocNo ");
            SQL.AppendLine(") B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where A.Amt+A.AmtBOM>0.00 ");
            SQL.AppendLine("And A.CancelInd='N' ");
            SQL.AppendLine("And A.DocNo=@Param;");

            Amt = Sm.GetValueDec(SQL.ToString(), TxtSODocNo.Text);
            return Amt;
        }

        #endregion

        #endregion

        #region Event

        #region Button Event

        private void BtnSODocNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmARDownpayment2Dlg(this));
        }

        private void BtnSODocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtSODocNo, "SO#", false))
            {
                try
                {
                    var f = new FrmSOContract2(mMenuCode);
                    f.Tag = "***";
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = TxtSODocNo.Text;
                    f.ShowDialog();
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
            }
        }

        private void BtnVoucherRequestDocNo_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtVoucherRequestDocNo, "Voucher request#", false))
            {
                try
                {
                    var f = new FrmVoucherRequest(mMenuCode);
                    f.Tag = "***";
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = TxtVoucherRequestDocNo.Text;
                    f.ShowDialog();
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
            }
        }

        private void BtnVoucherDocNo_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtVoucherDocNo, "Voucher#", false))
            {
                try
                {
                    var f = new FrmVoucher(mMenuCode);
                    f.Tag = "***";
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = TxtVoucherDocNo.Text;
                    f.ShowDialog();
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
            }
        }


        #endregion

        #region Misc Control Event

        private void LuePIC_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LuePIC, new Sm.RefreshLue1(Sl.SetLueUserCode));
        }

        private void LueCurCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueCurCode, new Sm.RefreshLue1(Sl.SetLueCurCode));
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue2(SetLueCtCode), string.Empty);
        }

        private void TxtAmt_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtAmt, 0);
        }

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        private void LuePaymentType_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LuePaymentType, new Sm.RefreshLue1(Sl.SetLueVoucherPaymentType));

                Sm.SetControlEditValueNull(new List<DXE.BaseEdit> { LueBankCode, TxtGiroNo, DteDueDt });

                if (Sm.CompareStr(Sm.GetLue(LuePaymentType), "B"))
                {
                    Sm.SetControlReadOnly(LueBankCode, false);
                    Sm.SetControlReadOnly(TxtGiroNo, true);
                    Sm.SetControlReadOnly(DteDueDt, true);
                    return;
                }

                if (Sm.CompareStr(Sm.GetLue(LuePaymentType), "G") || Sm.CompareStr(Sm.GetLue(LuePaymentType), "K"))
                {
                    Sm.SetControlReadOnly(LueBankCode, false);
                    Sm.SetControlReadOnly(TxtGiroNo, false);
                    Sm.SetControlReadOnly(DteDueDt, false);
                    return;
                }

                Sm.SetControlReadOnly(LueBankCode, true);
                Sm.SetControlReadOnly(TxtGiroNo, true);
                Sm.SetControlReadOnly(DteDueDt, true);
            }
        }

        private void LueBankCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueBankCode, new Sm.RefreshLue1(Sl.SetLueBankCode));
        }

        private void TxtDownpaymentPercentage_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.FormatNumTxt(TxtDownpaymentPercentage, 2);
                ComputeAmtAftTax();
            }
        }

        private void TxtAmtBefTax_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.FormatNumTxt(TxtAmtBefTax, 2);
                ComputeAmtAftTax();
            }
        }

        private void LueTaxCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueTaxCode, new Sm.RefreshLue1(Sl.SetLueTaxCode));
                ComputeAmtAftTax();
            }
        }

        private void LueTaxCode2_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueTaxCode2, new Sm.RefreshLue1(Sl.SetLueTaxCode));
                ComputeAmtAftTax();
            }
        }

        private void LueTaxCode3_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueTaxCode3, new Sm.RefreshLue1(Sl.SetLueTaxCode));
                ComputeAmtAftTax();
            }
        }

        private void TxtTaxInvoiceNo_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtTaxInvoiceNo);
        }

        private void TxtTaxInvoiceNo2_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtTaxInvoiceNo2);
        }

        private void TxtTaxInvoiceNo3_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtTaxInvoiceNo3);
        }
     

        #endregion

        #region Grid Event

        private void Grd3_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0 && TxtDocNo.Text.Length == 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmARDownPayment2Dlg2(this));
            }
        }

        private void Grd3_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && TxtDocNo.Text.Length == 0) Sm.FormShowDialog(new FrmARDownPayment2Dlg2(this));
        }

        private void Grd3_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd3, e, BtnSave);
            ComputeCOAAmt();
            Sm.GrdEnter(Grd3, e);
            Sm.GrdTabInLastCell(Grd3, e, BtnFind, BtnSave);
        }

        private void Grd3_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd3, e.RowIndex, 1).Length != 0)
                Grd3.Cells[e.RowIndex, 4].Value = 0;

            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd3, e.RowIndex, 1).Length != 0)
                Grd3.Cells[e.RowIndex, 3].Value = 0;

            if (e.ColIndex == 6 && Sm.GetGrdBool(Grd3, e.RowIndex, 6))
                Grd3.Cells[e.RowIndex, 7].Value = false;

            if (e.ColIndex == 7 && Sm.GetGrdBool(Grd3, e.RowIndex, 7))
                Grd3.Cells[e.RowIndex, 6].Value = false;

            if (e.ColIndex == 6 || e.ColIndex == 7)
                Grd3.Cells[e.RowIndex, 8].Value = Sm.GetGrdBool(Grd3, e.RowIndex, e.ColIndex);

            if (Sm.IsGrdColSelected(new int[] { 3, 4, 6, 7 }, e.ColIndex))
                ComputeCOAAmt();
        }

        #endregion

        #endregion

        #region Class

        private class ARDep
        {
            public string CompanyLogo { set; get; }
            public string CompanyName { get; set; }
            public string CompanyAddressCity { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyPhone { get; set; }

            public string CompanyFax { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string CurCode { get; set; }
            public decimal Amt { get; set; }

            public string Terbilang { get; set; }
            public string Terbilang2 { get; set; }
            public decimal SoAmt { get; set; }
            public string CtName { get; set; }
            public string SODocNo { get; set; }

            public string DocNoVC { get; set; }
            public string DocDtVC { get; set; }
            public string Address { get; set; }
            public string PrintBy { get; set; }
            public string Remark { get; set; }

            public string TotalAmtTerbilang { get; set; }
            public decimal TotalAmount { get; set; }
            public string TaxName { get; set; }
            public decimal TaxAmt { get; set; }
             
            public string TaxName2 { get; set; }
            public decimal TaxAmt2 { get; set; }
            public string TaxName3 { get; set; }
            public decimal TaxAmt3 { get; set; }
            public string BankName { get; set; }

            public string BankAcNo { get; set; }
            public string LocalDocNo { get; set; }
            public string CompanyFooterImage { get; set; }
        }

        private class ARDPInvoiceIMS
        {
            public string Remark { get; set; }
            public decimal AmtBeforeTax { get; set; }
        }

        private class ARDPSign
        {
            public string Signature { get; set; }
            public string UserName { get; set; }
            public string POSName { get; set; }
        }

        #endregion
    }
}
