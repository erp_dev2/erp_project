﻿#region Update
/*
    09/10/2017 [TKG] Panjang nama site 100 karakter
    18/09/2019 [TKG/IMS] tambah daftar division
    14/02/2020 [WED/YK] tambah COA POS Perantara. mandatory berdasarkan parameter IsJournalCreateAdditionalPOS
    11/05/2020 [IBL/KSM] tambah field input Short Code
    15/09/2020 [WED/YK] tambah ada grid untuk ngisi POS COA Site cabang, berdasarkan parameter IsSiteUsePOSDetail
    13/10/2020 [IBL/PHT] tambah field input city, berdasarkan parameter IsSiteUseCity
    02/09/2021 [ICA/AMKA] membuat bisa dibuka di aplikasi lain
    07/03/2022 [TKG/PHT] tambah site group, ubah GetParameter()
    28/04/2022 [TRI] bug di tab POS COA Site cabang, luebranch dan button acno tidak tampil ketika insert
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using TenTec.Windows.iGridLib;

#endregion

namespace RunSystem
{
    public partial class FrmSite : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty,
            mSiteCode = string.Empty; //if this application is called from other application;
        internal FrmSiteFind FrmFind;
        private bool 
            mIsSiteDivisionEnabled = false, 
            mIsJournalCreateAdditionalPOS = false, 
            mIsSiteUsePOSDetail = false,
            mIsFilterBySiteHR = false;
        internal bool mIsSiteGroupEnabled = false, mIsSiteUseCity = false;
        iGCell fCell;
        bool fAccept;
        
        #endregion

        #region Constructor

        public FrmSite(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Site";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                BtnPrint.Visible = false;
                GetParameter();

                Tp2.PageVisible = mIsSiteDivisionEnabled;
                
                Tc1.SelectedTabPage = Tp4;
                if (mIsSiteUseCity) Sl.SetLueCityCode(ref LueCityCode);
                if (mIsSiteGroupEnabled) Sl.SetLueOption(ref LueSiteGroup, "SiteGroup");
                if (mIsJournalCreateAdditionalPOS) LblAcNo.ForeColor = Color.Red;

                if (mIsSiteUsePOSDetail)
                {
                    Tc1.SelectedTabPage = Tp3;
                    SetLueBranchCode(ref LueBranchCode);
                }
                else
                {
                    Tp3.PageVisible = false;
                    
                }
                LueBranchCode.Visible = false;
                Tc1.SelectedTabPage = Tp1;
                Sl.SetLueProfitCenterCode(ref LueProfitCenterCode);

                SetGrd();
                SetFormControl(mState.View);

                //if this application is called from other application
                if (mSiteCode.Length != 0)
                {
                    ShowData(mSiteCode);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        # region Standard Method

        private void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'IsSiteDivisionEnabled', 'IsJournalCreateAdditionalPOS', 'IsSiteUsePOSDetail', 'IsFilterBySiteHR', 'IsSiteUseCity', ");
            SQL.AppendLine("'IsSiteGroupEnabled' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsSiteGroupEnabled": mIsSiteGroupEnabled = ParValue == "Y"; break;
                            case "IsSiteUseCity": mIsSiteUseCity = ParValue == "Y"; break;
                            case "IsFilterBySiteHR": mIsFilterBySiteHR = ParValue == "Y"; break;
                            case "IsSiteUsePOSDetail": mIsSiteUsePOSDetail = ParValue == "Y"; break;
                            case "IsJournalCreateAdditionalPOS": mIsJournalCreateAdditionalPOS = ParValue == "Y"; break;
                            case "IsSiteDivisionEnabled": mIsSiteDivisionEnabled = ParValue == "Y"; break;

                        }
                    }
                }
                dr.Close();
            }
        }

        private void SetGrd()
        {
            #region Grid 1
            Grd1.Cols.Count = 3;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[]{ "", "Code", "Division" },
                new int[]{ 20, 0, 300 }
            );
            Sm.GrdColButton(Grd1, new int[] { 0 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 2 });
            #endregion

            #region Grid 2
            Grd2.Cols.Count = 6;
            Sm.GrdHdrWithColWidth(
                Grd2, new string[] { "SiteCode", "Site/Branch", "", "COA#", "Account", "ExistingPOSCOA#" },
                new int[] { 0, 250, 20, 150, 300, 0 }
            );
            Sm.GrdColButton(Grd2, new int[] { 2 });
            Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 3, 4 });
            Sm.GrdColInvisible(Grd2, new int[] { 0, 5 });
            #endregion
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtSiteCode, TxtSiteName, MeeAddress, LueProfitCenterCode, MeeRemark, 
                        TxtShortCode, LueCityCode, LueSiteGroup 
                    }, true);
                    ChkActInd.Properties.ReadOnly = true;
                    ChkHOInd.Properties.ReadOnly = true;
                    BtnAcNo.Enabled = false;
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0 });
                    Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 1, 2, 3, 4 });
                    TxtSiteCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtSiteCode, TxtSiteName, MeeAddress, LueProfitCenterCode, MeeRemark, 
                        TxtShortCode
                    }, false);
                    ChkActInd.Properties.ReadOnly = false;
                    ChkHOInd.Properties.ReadOnly = false;
                    if (mIsSiteUseCity) Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ LueCityCode }, false);
                    if (mIsSiteGroupEnabled) Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueSiteGroup }, false);
                    if (mIsJournalCreateAdditionalPOS) BtnAcNo.Enabled = true;
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0 });
                    Sm.GrdColReadOnly(false, true, Grd2, new int[] { 1, 2 });
                    TxtSiteCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                       TxtSiteName, MeeAddress, LueProfitCenterCode, MeeRemark, TxtShortCode
                    }, false);
                    ChkActInd.Properties.ReadOnly = false;
                    ChkHOInd.Properties.ReadOnly = false;
                    if (mIsSiteUseCity) Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueCityCode }, false);
                    if (mIsSiteGroupEnabled) Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueSiteGroup }, false);
                    if (mIsJournalCreateAdditionalPOS) BtnAcNo.Enabled = true;
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0 });
                    Sm.GrdColReadOnly(false, true, Grd2, new int[] { 1, 2 });
                    TxtSiteName.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            { 
                TxtAcNo, TxtAcDesc, TxtSiteCode, TxtSiteName, MeeAddress, 
                LueProfitCenterCode, MeeRemark, TxtShortCode, LueCityCode, LueSiteGroup 
            });
            ChkActInd.Checked = false;
            ChkHOInd.Checked = false;
            Sm.ClearGrd(Grd1, true);
            Sm.ClearGrd(Grd2, true);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
           if (FrmFind == null) FrmFind = new FrmSiteFind(this);
           Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
            ChkActInd.Checked = true;
            ChkHOInd.Checked = true;
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtSiteCode, string.Empty, false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                SaveData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        internal void ShowData(string SiteCode)
        {
            try
            {
                ClearData();
                ShowSite(SiteCode);
                ShowSiteDivision(SiteCode);
                if (mIsSiteUsePOSDetail) ShowSiteBranchPOS(SiteCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowSite(string SiteCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.SiteCode, A.SiteName, A.ActInd, A.HOInd, A.Address, A.ProfitCenterCode, A.Remark, ");
            SQL.AppendLine("A.AcNo, B.AcDesc, A.ShortCode, ");
            if (mIsSiteUseCity)
                SQL.AppendLine("A.CityCode, ");
            else
                SQL.AppendLine("Null As CityCode, ");
            if (mIsSiteGroupEnabled)
                SQL.AppendLine("A.SiteGroup ");
            else
                SQL.AppendLine("Null As SiteGroup ");
            SQL.AppendLine("From TblSite A ");
            SQL.AppendLine("Left Join TblCOA B ON A.AcNo = B.AcNo ");           
            SQL.AppendLine("Where A.SiteCode = @SiteCode; ");
            
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@SiteCode",SiteCode);
            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[] 
                {
                    "SiteCode", 
                    "SiteName", "ActInd", "HOInd", "Address", "ProfitCenterCode", 
                    "Remark", "AcNo", "AcDesc", "ShortCode", "CityCode",
                    "SiteGroup"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtSiteCode.EditValue = Sm.DrStr(dr, c[0]);
                    TxtSiteName.EditValue = Sm.DrStr(dr, c[1]);
                    ChkActInd.Checked = Sm.DrStr(dr, c[2]) == "Y";
                    ChkHOInd.Checked = Sm.DrStr(dr, c[3]) == "Y";
                    MeeAddress.EditValue = Sm.DrStr(dr, c[4]);
                    Sm.SetLue(LueProfitCenterCode, Sm.DrStr(dr, c[5]));
                    MeeRemark.EditValue = Sm.DrStr(dr, c[6]);
                    TxtAcNo.EditValue = Sm.DrStr(dr, c[7]);
                    TxtAcDesc.EditValue = Sm.DrStr(dr, c[8]);
                    TxtShortCode.EditValue = Sm.DrStr(dr, c[9]);
                    Sm.SetLue(LueCityCode, Sm.DrStr(dr, c[10]));
                    Sm.SetLue(LueSiteGroup, Sm.DrStr(dr, c[11]));
                }, true
            );
        }

        private void ShowSiteDivision(string SiteCode)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@SiteCode", SiteCode);
            Sm.ShowDataInGrid(
                    ref Grd1, ref cm, 
                    "Select A.DivisionCode, B.DivisionName From TblSiteDivision A, TblDivision B " +
                    "Where A.DivisionCode=B.DivisionCode And A.SiteCode=@SiteCode;",
                    new string[] { "DivisionCode", "DivisionName" },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ShowSiteBranchPOS(string SiteCode)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.BranchCode, B.SiteName BranchName, A.AcNo, C.AcDesc ");
            SQL.AppendLine("From TblSiteBranchPOS A ");
            SQL.AppendLine("Inner Join TblSite B On A.BranchCode = B.SiteCode ");
            SQL.AppendLine("Inner Join TblCOA C On A.AcNo = C.AcNo ");
            SQL.AppendLine("Where A.SiteCode = @SiteCode; ");

            Sm.CmParam<String>(ref cm, "@SiteCode", SiteCode);
            Sm.ShowDataInGrid(
                ref Grd2, ref cm, SQL.ToString(),
                new string[] { "BranchCode", "BranchName", "AcNo", "AcDesc" },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd2, 0, 1);
        }

        #endregion

        #region Save Data

        private void SaveData()
        {
            if (IsDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;
            string DivisionCode = string.Empty;
            var cml = new List<MySqlCommand>();

            cml.Add(SaveSite());

            if (Grd1.Rows.Count > 1)
            {
                for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
                {
                    DivisionCode = Sm.GetGrdStr(Grd1, i, 1);
                    if (DivisionCode.Length > 0)
                    {
                        cml.Add(SaveSiteDtl(i, DivisionCode));
                    }
                }
            }

            if(mIsSiteUsePOSDetail)
            {
                cml.Add(DeleteSiteDtl2());
                if (Grd2.Rows.Count > 0)
                {
                    for (int i = 0; i < Grd2.Rows.Count; ++i)
                    {
                        if (Sm.GetGrdStr(Grd2, i, 0).Length > 0)
                        {
                            cml.Add(SaveSiteDtl2(i));
                        }
                    }
                }
            }

            Sm.ExecCommands(cml);

            ShowData(TxtSiteCode.Text);
        }

        private MySqlCommand SaveSite()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Set @Dt:=CurrentDateTime(); ");

            SQL.AppendLine("Insert Into TblSite(SiteCode, ShortCode, SiteName, ActInd, HOInd, Address, ProfitCenterCode, AcNo, ");
            if (mIsSiteUseCity) SQL.AppendLine("CityCode, ");
            if (mIsSiteGroupEnabled) SQL.AppendLine("SiteGroup, ");
            SQL.AppendLine("Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@SiteCode, @ShortCode, @SiteName, @ActInd, @HOInd, @Address, @ProfitCenterCode, @AcNo, ");
            if (mIsSiteUseCity) SQL.AppendLine("@CityCode, ");
            if (mIsSiteGroupEnabled) SQL.AppendLine("@SiteGroup, ");
            SQL.AppendLine("@Remark, @UserCode, @Dt) ");
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("    Update ");
            SQL.AppendLine("        ActInd=@ActInd, HOInd=@HOInd, SiteName=@SiteName, ShortCode=@ShortCode, Address=@Address, ");
            SQL.AppendLine("        ProfitCenterCode=@ProfitCenterCode, AcNo=@AcNo, ");
            if (mIsSiteUseCity) SQL.AppendLine("        CityCode=@CityCode, ");
            if (mIsSiteGroupEnabled) SQL.AppendLine("        SiteGroup=@SiteGroup, ");
            SQL.AppendLine("        Remark=@Remark, LastUpBy=@UserCode, LastUpDt=@Dt; ");

            SQL.AppendLine("Delete From TblSiteDivision Where SiteCode=@SiteCode; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@SiteCode", TxtSiteCode.Text);
            Sm.CmParam<String>(ref cm, "@ShortCode", TxtShortCode.Text);
            Sm.CmParam<String>(ref cm, "@SiteName", TxtSiteName.Text);
            Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@HOInd", ChkHOInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@CityCode", Sm.GetLue(LueCityCode));
            Sm.CmParam<String>(ref cm, "@SiteGroup", Sm.GetLue(LueSiteGroup));
            Sm.CmParam<String>(ref cm, "@Address", MeeAddress.Text);
            Sm.CmParam<String>(ref cm, "@ProfitCenterCode", Sm.GetLue(LueProfitCenterCode));
            Sm.CmParam<String>(ref cm, "@AcNo", TxtAcNo.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveSiteDtl(int Row, string DivisionCode)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Insert Into TblSiteDivision(SiteCode, DivisionCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@SiteCode, @DivisionCode0" + Row.ToString() + ", @UserCode, CurrentDateTime()); ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@SiteCode", TxtSiteCode.Text);
            Sm.CmParam<String>(ref cm, "@DivisionCode0" + Row.ToString(), DivisionCode);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand DeleteSiteDtl2()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Delete From TblSiteBranchPOS ");
            SQL.AppendLine("Where SiteCode = @SiteCode ");
            SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@SiteCode", TxtSiteCode.Text);

            return cm;
        }

        private MySqlCommand SaveSiteDtl2(int Row)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Insert Into TblSiteBranchPOS(SiteCode, BranchCode, AcNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@SiteCode, @BranchCode, @AcNo, @CreateBy, CurrentDateTime()); ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@SiteCode", TxtSiteCode.Text);
            Sm.CmParam<String>(ref cm, "@BranchCode", Sm.GetGrdStr(Grd2, Row, 0));
            Sm.CmParam<String>(ref cm, "@AcNo", Sm.GetGrdStr(Grd2, Row, 3));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);


            return cm;
        }

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtSiteCode, "Site code", false) ||
                Sm.IsTxtEmpty(TxtSiteName, "Site name", false) ||
                Sm.IsLueEmpty(LueProfitCenterCode, "Profit center") ||
                (mIsJournalCreateAdditionalPOS && Sm.IsTxtEmpty(TxtAcNo, "POS COA#", false)) ||
                IsEntCodeExisted() ||
                IsDataInActiveAlready() ||
                IsPOSDetailInvalid();
        }

        private bool IsPOSDetailInvalid()
        {
            if (!mIsSiteUsePOSDetail) return false;

            return IsDetailEmpty() || IsBranchDuplicate() || IsAcNoAlreadyUsed() || IsAcNoInvalid();
        }

        private bool IsBranchDuplicate()
        {
            for (int i = 0; i < Grd2.Rows.Count; ++i)
            {
                for (int j = (i + 1); j < Grd2.Rows.Count; ++j)
                {
                    if (Sm.GetGrdStr(Grd2, i, 0) == Sm.GetGrdStr(Grd2, j, 0))
                    {
                        Tc1.SelectedTabPage = Tp3;
                        Sm.StdMsg(mMsgType.Warning, "You have duplicate site/branch " + Sm.GetGrdStr(Grd2, j, 1) + ".");
                        return true;
                    }
                }
            }

            return false;
        }

        private bool IsDetailEmpty()
        {
            if (Grd2.Rows.Count > 1)
            {
                for (int i = 0; i < Grd2.Rows.Count - 1; ++i)
                {
                    if (Sm.IsGrdValueEmpty(Grd2, i, 1, false, "Branch is empty.")) { Tc1.SelectedTabPage = Tp3; return true; }
                    if (Sm.IsGrdValueEmpty(Grd2, i, 4, false, "POS COA is empty.")) { Tc1.SelectedTabPage = Tp3; return true; }
                }
            }
            return false;
        }

        private bool IsAcNoAlreadyUsed()
        {
            UpdateExistingPOSCOA();

            for (int i = 0; i < Grd2.Rows.Count - 1; ++i)
            {
                if (Sm.GetGrdStr(Grd2, i, 3) == Sm.GetGrdStr(Grd2, i, 5))
                {
                    Tc1.SelectedTabPage = Tp3;
                    Sm.StdMsg(mMsgType.Warning, "Your selected POS COA for this branch (" + Sm.GetGrdStr(Grd2, i, 1) + ") already used.");
                    return true;
                }
            }

            return false;
        }

        private bool IsAcNoInvalid()
        {
            for (int i = 0; i < Grd2.Rows.Count; ++i)
            {
                if (Sm.GetGrdStr(Grd2, i, 3) == TxtSiteCode.Text)
                {
                    Tc1.SelectedTabPage = Tp3;
                    Sm.StdMsg(mMsgType.Warning, "You should set different site.");
                    return true;
                }
            }

            return false;
        }

        private bool IsEntCodeExisted()
        {
            if (!TxtSiteCode.Properties.ReadOnly && 
                Sm.IsDataExist("Select 1 From TblSite Where SiteCode=@Param Limit 1;", TxtSiteCode.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "Site code ( " + TxtSiteCode.Text + " ) already existed.");
                return true;
            }
            return false;
        }

        private bool IsDataInActiveAlready()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select 1 From TblSite " +
                    "Where ActInd='N' And SiteCode=@SiteCode;"
            };
            Sm.CmParam<String>(ref cm, "@SiteCode", TxtSiteCode.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This site already not actived.");
                return true;
            }
            return false;
        }

        #endregion

        #region Grid Methods

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                e.DoDefault = false;
                Sm.FormShowDialog(new FrmSiteDlg(this));
            }
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0) Sm.FormShowDialog(new FrmSiteDlg(this));
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        private void Grd2_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (BtnSave.Enabled && mIsSiteUsePOSDetail)
            {
                if (e.ColIndex == 2)
                {
                    Sm.FormShowDialog(new FrmSiteDlg2(this, 2, e.RowIndex));
                }
            }
        }

        private void Grd2_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && mIsSiteUsePOSDetail)
            {
                if (Sm.IsGrdColSelected(new int[] { 1, 2 }, e.ColIndex))
                {
                    if (e.ColIndex == 1)
                    {
                        SetLueBranchCode(ref LueBranchCode);
                        LueRequestEdit(Grd2, LueBranchCode, ref fCell, ref fAccept, e);
                    }
                    if (e.ColIndex == 2)
                    {
                        Sm.FormShowDialog(new FrmSiteDlg2(this, 2, e.RowIndex));
                    }
                    Sm.GrdRequestEdit(Grd2, e.RowIndex);
                }
            }
        }

        private void Grd2_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled && mIsSiteUsePOSDetail)
            {
                Sm.GrdRemoveRow(Grd2, e, BtnSave);
                Sm.GrdKeyDown(Grd2, e, BtnFind, BtnSave);
            }
        }

        #endregion

        #region Additional Methods

        internal void GetAcNoForPOSDetail(string AcNo, string AcDesc, int Row)
        {
            Grd2.Cells[Row, 3].Value = AcNo;
            Grd2.Cells[Row, 4].Value = AcDesc;
        }

        private void LueRequestEdit(
           iGrid Grd,
           DevExpress.XtraEditors.LookUpEdit Lue,
           ref iGCell fCell,
           ref bool fAccept,
           TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;
            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        private void SetLueBranchCode(ref DXE.LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select SiteCode Col1, SiteName Col2 ");
            SQL.AppendLine("From TblSite ");
            SQL.AppendLine("Where ActInd = 'Y' ");
            if (TxtSiteCode.Text.Length > 0)
                SQL.AppendLine("And SiteCode != @SiteCode ");
            SQL.AppendLine("; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@SiteCode", TxtSiteCode.Text);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void UpdateExistingPOSCOA()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string BranchCode = string.Empty;
            var lB = new List<Branch>();

            for (int i = 0; i < Grd2.Rows.Count; ++i)
            {
                if (BranchCode.Length > 0) BranchCode += ",";
                BranchCode += Sm.GetGrdStr(Grd2, i, 0);
            }

            if (BranchCode.Length > 0)
            {
                SQL.AppendLine("Select SiteCode, AcNo ");
                SQL.AppendLine("From TblSite ");
                SQL.AppendLine("Where Find_In_Set(SiteCode, @BranchCode) ");
                SQL.AppendLine("; ");

                Sm.CmParam<String>(ref cm, "@BranchCode", BranchCode);

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = SQL.ToString();
                    cm.CommandTimeout = 600;
                    using (var dr = cm.ExecuteReader())
                    {
                        var c = Sm.GetOrdinal(dr, new string[] { "SiteCode", "AcNo" });
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                lB.Add(new Branch() 
                                {
                                    BranchCode = Sm.DrStr(dr, c[0]),
                                    POSCOA = Sm.DrStr(dr, c[1])
                                });
                            }
                        }
                        dr.Close();
                    }
                }

                if (lB.Count > 0)
                {
                    for (int i = 0; i < Grd2.Rows.Count; ++i)
                    {
                        foreach(var x in lB.Where(w => w.BranchCode == Sm.GetGrdStr(Grd2, i, 0)))
                        {
                            Grd2.Cells[i, 5].Value = x.POSCOA;
                        }
                    }
                }
            }

            lB.Clear();
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtSiteCode_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtSiteName);
        }

        private void LueProfitCenterCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueProfitCenterCode, new Sm.RefreshLue1(Sl.SetLueProfitCenterCode));
        }

        private void TxtSiteName_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtSiteName);
        }

        private void LueSiteGroup_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueSiteGroup, new Sm.RefreshLue2(Sl.SetLueOption), "SiteGroup");
        }

        private void LueBranchCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled && mIsSiteUsePOSDetail)
            {
                Sm.RefreshLookUpEdit(LueBranchCode, new Sm.RefreshLue1(SetLueBranchCode));
            }
        }

        private void LueBranchCode_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled && mIsSiteUsePOSDetail)
            {
                Sm.LueKeyDown(Grd2, ref fAccept, e);
            }
        }

        private void LueBranchCode_Leave(object sender, EventArgs e)
        {
            if (BtnSave.Enabled && mIsSiteUsePOSDetail)
            {
                if (LueBranchCode.Visible && fAccept && fCell.ColIndex == 1)
                {
                    if (Sm.GetLue(LueBranchCode).Length == 0)
                        Grd2.Cells[fCell.RowIndex, 0].Value =
                        Grd2.Cells[fCell.RowIndex, 1].Value = null;
                    else
                    {
                        Grd2.Cells[fCell.RowIndex, 0].Value = Sm.GetLue(LueBranchCode);
                        Grd2.Cells[fCell.RowIndex, 1].Value = LueBranchCode.GetColumnValue("Col2");
                    }
                    LueBranchCode.Visible = false;
                }
            }
        }

        private void LueCityCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled && mIsSiteUseCity)
            {
                Sm.RefreshLookUpEdit(LueCityCode, new Sm.RefreshLue1(Sl.SetLueCityCode));
            }
        }

        #endregion

        #region Button Click

        private void BtnAcNo_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.FormShowDialog(new FrmSiteDlg2(this, 1, 0));
            }
        }

        #endregion

        #endregion

        #region Class

        private class Branch
        {
            public string BranchCode { get; set; }
            public string POSCOA { get; set; }
        }

        #endregion
    }
}
