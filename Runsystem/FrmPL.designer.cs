﻿namespace RunSystem
{
    partial class FrmPL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmPL));
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.panel4 = new System.Windows.Forms.Panel();
            this.Grd1 = new TenTec.Windows.iGridLib.iGrid();
            this.BtnSO = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.LueItCode1 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd11 = new TenTec.Windows.iGridLib.iGrid();
            this.panel8 = new System.Windows.Forms.Panel();
            this.MeeSM1 = new DevExpress.XtraEditors.MemoExEdit();
            this.label119 = new System.Windows.Forms.Label();
            this.LueSize1 = new DevExpress.XtraEditors.LookUpEdit();
            this.label57 = new System.Windows.Forms.Label();
            this.TxtCnt1 = new DevExpress.XtraEditors.TextEdit();
            this.label28 = new System.Windows.Forms.Label();
            this.TxtSeal1 = new DevExpress.XtraEditors.TextEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.TxtTotal1 = new DevExpress.XtraEditors.TextEdit();
            this.label11 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.LueItCode2 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd12 = new TenTec.Windows.iGridLib.iGrid();
            this.panel9 = new System.Windows.Forms.Panel();
            this.MeeSM2 = new DevExpress.XtraEditors.MemoExEdit();
            this.label120 = new System.Windows.Forms.Label();
            this.LueSize2 = new DevExpress.XtraEditors.LookUpEdit();
            this.label58 = new System.Windows.Forms.Label();
            this.TxtCnt2 = new DevExpress.XtraEditors.TextEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.TxtSeal2 = new DevExpress.XtraEditors.TextEdit();
            this.label13 = new System.Windows.Forms.Label();
            this.TxtTotal2 = new DevExpress.XtraEditors.TextEdit();
            this.label41 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.LueItCode3 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd13 = new TenTec.Windows.iGridLib.iGrid();
            this.panel10 = new System.Windows.Forms.Panel();
            this.MeeSM3 = new DevExpress.XtraEditors.MemoExEdit();
            this.label121 = new System.Windows.Forms.Label();
            this.LueSize3 = new DevExpress.XtraEditors.LookUpEdit();
            this.label59 = new System.Windows.Forms.Label();
            this.TxtCnt3 = new DevExpress.XtraEditors.TextEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.TxtSeal3 = new DevExpress.XtraEditors.TextEdit();
            this.label15 = new System.Windows.Forms.Label();
            this.TxtTotal3 = new DevExpress.XtraEditors.TextEdit();
            this.label43 = new System.Windows.Forms.Label();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.LueItCode4 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd14 = new TenTec.Windows.iGridLib.iGrid();
            this.panel11 = new System.Windows.Forms.Panel();
            this.MeeSM4 = new DevExpress.XtraEditors.MemoExEdit();
            this.label122 = new System.Windows.Forms.Label();
            this.LueSize4 = new DevExpress.XtraEditors.LookUpEdit();
            this.label62 = new System.Windows.Forms.Label();
            this.TxtCnt4 = new DevExpress.XtraEditors.TextEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.TxtSeal4 = new DevExpress.XtraEditors.TextEdit();
            this.label19 = new System.Windows.Forms.Label();
            this.TxtTotal4 = new DevExpress.XtraEditors.TextEdit();
            this.label44 = new System.Windows.Forms.Label();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.LueItCode5 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd15 = new TenTec.Windows.iGridLib.iGrid();
            this.panel12 = new System.Windows.Forms.Panel();
            this.MeeSM5 = new DevExpress.XtraEditors.MemoExEdit();
            this.label123 = new System.Windows.Forms.Label();
            this.LueSize5 = new DevExpress.XtraEditors.LookUpEdit();
            this.label63 = new System.Windows.Forms.Label();
            this.TxtCnt5 = new DevExpress.XtraEditors.TextEdit();
            this.label14 = new System.Windows.Forms.Label();
            this.TxtSeal5 = new DevExpress.XtraEditors.TextEdit();
            this.label21 = new System.Windows.Forms.Label();
            this.TxtTotal5 = new DevExpress.XtraEditors.TextEdit();
            this.label45 = new System.Windows.Forms.Label();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.LueItCode6 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd16 = new TenTec.Windows.iGridLib.iGrid();
            this.panel13 = new System.Windows.Forms.Panel();
            this.MeeSM6 = new DevExpress.XtraEditors.MemoExEdit();
            this.label124 = new System.Windows.Forms.Label();
            this.LueSize6 = new DevExpress.XtraEditors.LookUpEdit();
            this.label64 = new System.Windows.Forms.Label();
            this.TxtCnt6 = new DevExpress.XtraEditors.TextEdit();
            this.label16 = new System.Windows.Forms.Label();
            this.TxtSeal6 = new DevExpress.XtraEditors.TextEdit();
            this.label23 = new System.Windows.Forms.Label();
            this.TxtTotal6 = new DevExpress.XtraEditors.TextEdit();
            this.label46 = new System.Windows.Forms.Label();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.LueItCode7 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd17 = new TenTec.Windows.iGridLib.iGrid();
            this.panel14 = new System.Windows.Forms.Panel();
            this.MeeSM7 = new DevExpress.XtraEditors.MemoExEdit();
            this.label125 = new System.Windows.Forms.Label();
            this.LueSize7 = new DevExpress.XtraEditors.LookUpEdit();
            this.label65 = new System.Windows.Forms.Label();
            this.TxtCnt7 = new DevExpress.XtraEditors.TextEdit();
            this.label20 = new System.Windows.Forms.Label();
            this.TxtSeal7 = new DevExpress.XtraEditors.TextEdit();
            this.label25 = new System.Windows.Forms.Label();
            this.TxtTotal7 = new DevExpress.XtraEditors.TextEdit();
            this.label47 = new System.Windows.Forms.Label();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this.LueItCode8 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd18 = new TenTec.Windows.iGridLib.iGrid();
            this.panel15 = new System.Windows.Forms.Panel();
            this.MeeSM8 = new DevExpress.XtraEditors.MemoExEdit();
            this.label126 = new System.Windows.Forms.Label();
            this.LueSize8 = new DevExpress.XtraEditors.LookUpEdit();
            this.label66 = new System.Windows.Forms.Label();
            this.TxtCnt8 = new DevExpress.XtraEditors.TextEdit();
            this.label22 = new System.Windows.Forms.Label();
            this.TxtSeal8 = new DevExpress.XtraEditors.TextEdit();
            this.label27 = new System.Windows.Forms.Label();
            this.TxtTotal8 = new DevExpress.XtraEditors.TextEdit();
            this.label48 = new System.Windows.Forms.Label();
            this.tabPage9 = new System.Windows.Forms.TabPage();
            this.LueItCode9 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd19 = new TenTec.Windows.iGridLib.iGrid();
            this.panel16 = new System.Windows.Forms.Panel();
            this.MeeSM9 = new DevExpress.XtraEditors.MemoExEdit();
            this.label127 = new System.Windows.Forms.Label();
            this.LueSize9 = new DevExpress.XtraEditors.LookUpEdit();
            this.label67 = new System.Windows.Forms.Label();
            this.TxtCnt9 = new DevExpress.XtraEditors.TextEdit();
            this.label24 = new System.Windows.Forms.Label();
            this.TxtSeal9 = new DevExpress.XtraEditors.TextEdit();
            this.label29 = new System.Windows.Forms.Label();
            this.TxtTotal9 = new DevExpress.XtraEditors.TextEdit();
            this.label49 = new System.Windows.Forms.Label();
            this.tabPage10 = new System.Windows.Forms.TabPage();
            this.LueItCode10 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd20 = new TenTec.Windows.iGridLib.iGrid();
            this.panel17 = new System.Windows.Forms.Panel();
            this.MeeSM10 = new DevExpress.XtraEditors.MemoExEdit();
            this.label128 = new System.Windows.Forms.Label();
            this.LueSize10 = new DevExpress.XtraEditors.LookUpEdit();
            this.label68 = new System.Windows.Forms.Label();
            this.TxtCnt10 = new DevExpress.XtraEditors.TextEdit();
            this.label26 = new System.Windows.Forms.Label();
            this.TxtSeal10 = new DevExpress.XtraEditors.TextEdit();
            this.label31 = new System.Windows.Forms.Label();
            this.TxtTotal10 = new DevExpress.XtraEditors.TextEdit();
            this.label50 = new System.Windows.Forms.Label();
            this.tabPage11 = new System.Windows.Forms.TabPage();
            this.LueItCode11 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd21 = new TenTec.Windows.iGridLib.iGrid();
            this.panel5 = new System.Windows.Forms.Panel();
            this.MeeSM11 = new DevExpress.XtraEditors.MemoExEdit();
            this.label129 = new System.Windows.Forms.Label();
            this.LueSize11 = new DevExpress.XtraEditors.LookUpEdit();
            this.label69 = new System.Windows.Forms.Label();
            this.TxtCnt11 = new DevExpress.XtraEditors.TextEdit();
            this.label30 = new System.Windows.Forms.Label();
            this.TxtSeal11 = new DevExpress.XtraEditors.TextEdit();
            this.label32 = new System.Windows.Forms.Label();
            this.TxtTotal11 = new DevExpress.XtraEditors.TextEdit();
            this.label51 = new System.Windows.Forms.Label();
            this.tabPage12 = new System.Windows.Forms.TabPage();
            this.LueItCode12 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd22 = new TenTec.Windows.iGridLib.iGrid();
            this.panel18 = new System.Windows.Forms.Panel();
            this.MeeSM12 = new DevExpress.XtraEditors.MemoExEdit();
            this.label130 = new System.Windows.Forms.Label();
            this.LueSize12 = new DevExpress.XtraEditors.LookUpEdit();
            this.label70 = new System.Windows.Forms.Label();
            this.TxtCnt12 = new DevExpress.XtraEditors.TextEdit();
            this.label33 = new System.Windows.Forms.Label();
            this.TxtSeal12 = new DevExpress.XtraEditors.TextEdit();
            this.label34 = new System.Windows.Forms.Label();
            this.TxtTotal12 = new DevExpress.XtraEditors.TextEdit();
            this.label52 = new System.Windows.Forms.Label();
            this.tabPage13 = new System.Windows.Forms.TabPage();
            this.LueItCode13 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd23 = new TenTec.Windows.iGridLib.iGrid();
            this.panel19 = new System.Windows.Forms.Panel();
            this.MeeSM13 = new DevExpress.XtraEditors.MemoExEdit();
            this.label131 = new System.Windows.Forms.Label();
            this.LueSize13 = new DevExpress.XtraEditors.LookUpEdit();
            this.label71 = new System.Windows.Forms.Label();
            this.TxtCnt13 = new DevExpress.XtraEditors.TextEdit();
            this.label35 = new System.Windows.Forms.Label();
            this.TxtSeal13 = new DevExpress.XtraEditors.TextEdit();
            this.label36 = new System.Windows.Forms.Label();
            this.TxtTotal13 = new DevExpress.XtraEditors.TextEdit();
            this.label53 = new System.Windows.Forms.Label();
            this.tabPage14 = new System.Windows.Forms.TabPage();
            this.LueItCode14 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd24 = new TenTec.Windows.iGridLib.iGrid();
            this.panel20 = new System.Windows.Forms.Panel();
            this.MeeSM14 = new DevExpress.XtraEditors.MemoExEdit();
            this.label132 = new System.Windows.Forms.Label();
            this.LueSize14 = new DevExpress.XtraEditors.LookUpEdit();
            this.label72 = new System.Windows.Forms.Label();
            this.TxtCnt14 = new DevExpress.XtraEditors.TextEdit();
            this.label37 = new System.Windows.Forms.Label();
            this.TxtSeal14 = new DevExpress.XtraEditors.TextEdit();
            this.label38 = new System.Windows.Forms.Label();
            this.TxtTotal14 = new DevExpress.XtraEditors.TextEdit();
            this.label54 = new System.Windows.Forms.Label();
            this.tabPage15 = new System.Windows.Forms.TabPage();
            this.LueItCode15 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd25 = new TenTec.Windows.iGridLib.iGrid();
            this.panel21 = new System.Windows.Forms.Panel();
            this.MeeSM15 = new DevExpress.XtraEditors.MemoExEdit();
            this.label133 = new System.Windows.Forms.Label();
            this.LueSize15 = new DevExpress.XtraEditors.LookUpEdit();
            this.label73 = new System.Windows.Forms.Label();
            this.TxtCnt15 = new DevExpress.XtraEditors.TextEdit();
            this.label39 = new System.Windows.Forms.Label();
            this.TxtSeal15 = new DevExpress.XtraEditors.TextEdit();
            this.label40 = new System.Windows.Forms.Label();
            this.TxtTotal15 = new DevExpress.XtraEditors.TextEdit();
            this.label55 = new System.Windows.Forms.Label();
            this.tabPage16 = new System.Windows.Forms.TabPage();
            this.LueItCode16 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd26 = new TenTec.Windows.iGridLib.iGrid();
            this.panel3 = new System.Windows.Forms.Panel();
            this.MeeSM16 = new DevExpress.XtraEditors.MemoExEdit();
            this.label134 = new System.Windows.Forms.Label();
            this.LueSize16 = new DevExpress.XtraEditors.LookUpEdit();
            this.label77 = new System.Windows.Forms.Label();
            this.TxtCnt16 = new DevExpress.XtraEditors.TextEdit();
            this.label78 = new System.Windows.Forms.Label();
            this.TxtSeal16 = new DevExpress.XtraEditors.TextEdit();
            this.label79 = new System.Windows.Forms.Label();
            this.TxtTotal16 = new DevExpress.XtraEditors.TextEdit();
            this.label80 = new System.Windows.Forms.Label();
            this.tabPage17 = new System.Windows.Forms.TabPage();
            this.LueItCode17 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd27 = new TenTec.Windows.iGridLib.iGrid();
            this.panel22 = new System.Windows.Forms.Panel();
            this.MeeSM17 = new DevExpress.XtraEditors.MemoExEdit();
            this.label135 = new System.Windows.Forms.Label();
            this.LueSize17 = new DevExpress.XtraEditors.LookUpEdit();
            this.label81 = new System.Windows.Forms.Label();
            this.TxtCnt17 = new DevExpress.XtraEditors.TextEdit();
            this.label82 = new System.Windows.Forms.Label();
            this.TxtSeal17 = new DevExpress.XtraEditors.TextEdit();
            this.label83 = new System.Windows.Forms.Label();
            this.TxtTotal17 = new DevExpress.XtraEditors.TextEdit();
            this.label84 = new System.Windows.Forms.Label();
            this.tabPage18 = new System.Windows.Forms.TabPage();
            this.LueItCode18 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd28 = new TenTec.Windows.iGridLib.iGrid();
            this.panel23 = new System.Windows.Forms.Panel();
            this.MeeSM18 = new DevExpress.XtraEditors.MemoExEdit();
            this.label136 = new System.Windows.Forms.Label();
            this.LueSize18 = new DevExpress.XtraEditors.LookUpEdit();
            this.label85 = new System.Windows.Forms.Label();
            this.TxtCnt18 = new DevExpress.XtraEditors.TextEdit();
            this.label86 = new System.Windows.Forms.Label();
            this.TxtSeal18 = new DevExpress.XtraEditors.TextEdit();
            this.label87 = new System.Windows.Forms.Label();
            this.TxtTotal18 = new DevExpress.XtraEditors.TextEdit();
            this.label88 = new System.Windows.Forms.Label();
            this.tabPage19 = new System.Windows.Forms.TabPage();
            this.LueItCode19 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd29 = new TenTec.Windows.iGridLib.iGrid();
            this.panel24 = new System.Windows.Forms.Panel();
            this.MeeSM19 = new DevExpress.XtraEditors.MemoExEdit();
            this.label137 = new System.Windows.Forms.Label();
            this.LueSize19 = new DevExpress.XtraEditors.LookUpEdit();
            this.label89 = new System.Windows.Forms.Label();
            this.TxtCnt19 = new DevExpress.XtraEditors.TextEdit();
            this.label90 = new System.Windows.Forms.Label();
            this.TxtSeal19 = new DevExpress.XtraEditors.TextEdit();
            this.label91 = new System.Windows.Forms.Label();
            this.TxtTotal19 = new DevExpress.XtraEditors.TextEdit();
            this.label92 = new System.Windows.Forms.Label();
            this.tabPage20 = new System.Windows.Forms.TabPage();
            this.LueItCode20 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd30 = new TenTec.Windows.iGridLib.iGrid();
            this.panel25 = new System.Windows.Forms.Panel();
            this.MeeSM20 = new DevExpress.XtraEditors.MemoExEdit();
            this.label138 = new System.Windows.Forms.Label();
            this.LueSize20 = new DevExpress.XtraEditors.LookUpEdit();
            this.label93 = new System.Windows.Forms.Label();
            this.TxtCnt20 = new DevExpress.XtraEditors.TextEdit();
            this.label94 = new System.Windows.Forms.Label();
            this.TxtSeal20 = new DevExpress.XtraEditors.TextEdit();
            this.label95 = new System.Windows.Forms.Label();
            this.TxtTotal20 = new DevExpress.XtraEditors.TextEdit();
            this.label96 = new System.Windows.Forms.Label();
            this.tabPage21 = new System.Windows.Forms.TabPage();
            this.LueItCode21 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd31 = new TenTec.Windows.iGridLib.iGrid();
            this.panel26 = new System.Windows.Forms.Panel();
            this.MeeSM21 = new DevExpress.XtraEditors.MemoExEdit();
            this.label139 = new System.Windows.Forms.Label();
            this.LueSize21 = new DevExpress.XtraEditors.LookUpEdit();
            this.label97 = new System.Windows.Forms.Label();
            this.TxtCnt21 = new DevExpress.XtraEditors.TextEdit();
            this.label98 = new System.Windows.Forms.Label();
            this.TxtSeal21 = new DevExpress.XtraEditors.TextEdit();
            this.label99 = new System.Windows.Forms.Label();
            this.TxtTotal21 = new DevExpress.XtraEditors.TextEdit();
            this.label100 = new System.Windows.Forms.Label();
            this.tabPage22 = new System.Windows.Forms.TabPage();
            this.LueItCode22 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd32 = new TenTec.Windows.iGridLib.iGrid();
            this.panel27 = new System.Windows.Forms.Panel();
            this.MeeSM22 = new DevExpress.XtraEditors.MemoExEdit();
            this.label140 = new System.Windows.Forms.Label();
            this.LueSize22 = new DevExpress.XtraEditors.LookUpEdit();
            this.label101 = new System.Windows.Forms.Label();
            this.TxtCnt22 = new DevExpress.XtraEditors.TextEdit();
            this.label102 = new System.Windows.Forms.Label();
            this.TxtSeal22 = new DevExpress.XtraEditors.TextEdit();
            this.label103 = new System.Windows.Forms.Label();
            this.TxtTotal22 = new DevExpress.XtraEditors.TextEdit();
            this.label104 = new System.Windows.Forms.Label();
            this.tabPage23 = new System.Windows.Forms.TabPage();
            this.LueItCode23 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd33 = new TenTec.Windows.iGridLib.iGrid();
            this.panel28 = new System.Windows.Forms.Panel();
            this.MeeSM23 = new DevExpress.XtraEditors.MemoExEdit();
            this.label141 = new System.Windows.Forms.Label();
            this.LueSize23 = new DevExpress.XtraEditors.LookUpEdit();
            this.label105 = new System.Windows.Forms.Label();
            this.TxtCnt23 = new DevExpress.XtraEditors.TextEdit();
            this.label106 = new System.Windows.Forms.Label();
            this.TxtSeal23 = new DevExpress.XtraEditors.TextEdit();
            this.label107 = new System.Windows.Forms.Label();
            this.TxtTotal23 = new DevExpress.XtraEditors.TextEdit();
            this.label108 = new System.Windows.Forms.Label();
            this.tabPage24 = new System.Windows.Forms.TabPage();
            this.LueItCode24 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd34 = new TenTec.Windows.iGridLib.iGrid();
            this.panel29 = new System.Windows.Forms.Panel();
            this.MeeSM24 = new DevExpress.XtraEditors.MemoExEdit();
            this.label142 = new System.Windows.Forms.Label();
            this.LueSize24 = new DevExpress.XtraEditors.LookUpEdit();
            this.label109 = new System.Windows.Forms.Label();
            this.TxtCnt24 = new DevExpress.XtraEditors.TextEdit();
            this.label110 = new System.Windows.Forms.Label();
            this.TxtSeal24 = new DevExpress.XtraEditors.TextEdit();
            this.label111 = new System.Windows.Forms.Label();
            this.TxtTotal24 = new DevExpress.XtraEditors.TextEdit();
            this.label112 = new System.Windows.Forms.Label();
            this.tabPage25 = new System.Windows.Forms.TabPage();
            this.LueItCode25 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd35 = new TenTec.Windows.iGridLib.iGrid();
            this.panel30 = new System.Windows.Forms.Panel();
            this.MeeSM25 = new DevExpress.XtraEditors.MemoExEdit();
            this.label143 = new System.Windows.Forms.Label();
            this.LueSize25 = new DevExpress.XtraEditors.LookUpEdit();
            this.label113 = new System.Windows.Forms.Label();
            this.TxtCnt25 = new DevExpress.XtraEditors.TextEdit();
            this.label114 = new System.Windows.Forms.Label();
            this.TxtSeal25 = new DevExpress.XtraEditors.TextEdit();
            this.label115 = new System.Windows.Forms.Label();
            this.TxtTotal25 = new DevExpress.XtraEditors.TextEdit();
            this.label116 = new System.Windows.Forms.Label();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label18 = new System.Windows.Forms.Label();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.label61 = new System.Windows.Forms.Label();
            this.LueNotify = new DevExpress.XtraEditors.LookUpEdit();
            this.label60 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.LueCtCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.BtnSI = new DevExpress.XtraEditors.SimpleButton();
            this.BtnSI2 = new DevExpress.XtraEditors.SimpleButton();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.LCDocDt = new DevExpress.XtraEditors.DateEdit();
            this.MeeAccount = new DevExpress.XtraEditors.MemoExEdit();
            this.label42 = new System.Windows.Forms.Label();
            this.TxtSIDocNo = new DevExpress.XtraEditors.TextEdit();
            this.TxtInvoice = new DevExpress.XtraEditors.TextEdit();
            this.TxtSalesContract = new DevExpress.XtraEditors.TextEdit();
            this.TxtLcNo = new DevExpress.XtraEditors.TextEdit();
            this.panel7 = new System.Windows.Forms.Panel();
            this.BtnDownload = new DevExpress.XtraEditors.SimpleButton();
            this.TxtLocalDocNo = new DevExpress.XtraEditors.TextEdit();
            this.LblLocalDoc = new System.Windows.Forms.Label();
            this.BtnSource = new DevExpress.XtraEditors.SimpleButton();
            this.label75 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label117 = new System.Windows.Forms.Label();
            this.TxtTab = new DevExpress.XtraEditors.TextEdit();
            this.ChkShippingMark = new DevExpress.XtraEditors.CheckEdit();
            this.MeeRemark2 = new DevExpress.XtraEditors.MemoExEdit();
            this.label74 = new System.Windows.Forms.Label();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.label56 = new System.Windows.Forms.Label();
            this.MeeIssued = new DevExpress.XtraEditors.MemoExEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.MeeOrder4 = new DevExpress.XtraEditors.MemoExEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.MeeOrder3 = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeIssued2 = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeIssued3 = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeIssued4 = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeOrder = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeOrder2 = new DevExpress.XtraEditors.MemoExEdit();
            this.TxtSource = new DevExpress.XtraEditors.TextEdit();
            this.SFD1 = new System.Windows.Forms.SaveFileDialog();
            this.OD1 = new System.Windows.Forms.OpenFileDialog();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd11)).BeginInit();
            this.panel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeSM1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSize1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal1.Properties)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd12)).BeginInit();
            this.panel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeSM2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSize2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal2.Properties)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd13)).BeginInit();
            this.panel10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeSM3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSize3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal3.Properties)).BeginInit();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd14)).BeginInit();
            this.panel11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeSM4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSize4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal4.Properties)).BeginInit();
            this.tabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd15)).BeginInit();
            this.panel12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeSM5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSize5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal5.Properties)).BeginInit();
            this.tabPage6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd16)).BeginInit();
            this.panel13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeSM6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSize6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal6.Properties)).BeginInit();
            this.tabPage7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd17)).BeginInit();
            this.panel14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeSM7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSize7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal7.Properties)).BeginInit();
            this.tabPage8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd18)).BeginInit();
            this.panel15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeSM8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSize8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal8.Properties)).BeginInit();
            this.tabPage9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd19)).BeginInit();
            this.panel16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeSM9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSize9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal9.Properties)).BeginInit();
            this.tabPage10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd20)).BeginInit();
            this.panel17.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeSM10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSize10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal10.Properties)).BeginInit();
            this.tabPage11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd21)).BeginInit();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeSM11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSize11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal11.Properties)).BeginInit();
            this.tabPage12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd22)).BeginInit();
            this.panel18.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeSM12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSize12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal12.Properties)).BeginInit();
            this.tabPage13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode13.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd23)).BeginInit();
            this.panel19.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeSM13.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSize13.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt13.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal13.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal13.Properties)).BeginInit();
            this.tabPage14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode14.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd24)).BeginInit();
            this.panel20.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeSM14.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSize14.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt14.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal14.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal14.Properties)).BeginInit();
            this.tabPage15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode15.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd25)).BeginInit();
            this.panel21.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeSM15.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSize15.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt15.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal15.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal15.Properties)).BeginInit();
            this.tabPage16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode16.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd26)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeSM16.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSize16.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt16.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal16.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal16.Properties)).BeginInit();
            this.tabPage17.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode17.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd27)).BeginInit();
            this.panel22.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeSM17.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSize17.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt17.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal17.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal17.Properties)).BeginInit();
            this.tabPage18.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode18.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd28)).BeginInit();
            this.panel23.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeSM18.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSize18.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt18.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal18.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal18.Properties)).BeginInit();
            this.tabPage19.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode19.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd29)).BeginInit();
            this.panel24.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeSM19.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSize19.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt19.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal19.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal19.Properties)).BeginInit();
            this.tabPage20.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode20.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd30)).BeginInit();
            this.panel25.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeSM20.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSize20.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt20.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal20.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal20.Properties)).BeginInit();
            this.tabPage21.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode21.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd31)).BeginInit();
            this.panel26.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeSM21.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSize21.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt21.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal21.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal21.Properties)).BeginInit();
            this.tabPage22.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode22.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd32)).BeginInit();
            this.panel27.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeSM22.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSize22.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt22.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal22.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal22.Properties)).BeginInit();
            this.tabPage23.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode23.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd33)).BeginInit();
            this.panel28.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeSM23.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSize23.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt23.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal23.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal23.Properties)).BeginInit();
            this.tabPage24.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode24.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd34)).BeginInit();
            this.panel29.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeSM24.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSize24.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt24.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal24.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal24.Properties)).BeginInit();
            this.tabPage25.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode25.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd35)).BeginInit();
            this.panel30.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeSM25.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSize25.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt25.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal25.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal25.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueNotify.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCtCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LCDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LCDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeAccount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSIDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInvoice.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSalesContract.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLcNo.Properties)).BeginInit();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLocalDocNo.Properties)).BeginInit();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTab.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkShippingMark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeIssued.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeOrder4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeOrder3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeIssued2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeIssued3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeIssued4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeOrder.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeOrder2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSource.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(823, 0);
            this.panel1.Size = new System.Drawing.Size(70, 523);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.splitContainer1);
            this.panel2.Controls.Add(this.panel7);
            this.panel2.Size = new System.Drawing.Size(823, 523);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 275);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.panel4);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.tabControl1);
            this.splitContainer1.Size = new System.Drawing.Size(823, 248);
            this.splitContainer1.SplitterDistance = 117;
            this.splitContainer1.TabIndex = 43;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel4.Controls.Add(this.Grd1);
            this.panel4.Controls.Add(this.BtnSO);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(823, 117);
            this.panel4.TabIndex = 88;
            // 
            // Grd1
            // 
            this.Grd1.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd1.DefaultRow.Height = 20;
            this.Grd1.DefaultRow.Sortable = false;
            this.Grd1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 21;
            this.Grd1.Location = new System.Drawing.Point(0, 22);
            this.Grd1.Name = "Grd1";
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd1.SingleClickEdit = true;
            this.Grd1.Size = new System.Drawing.Size(823, 95);
            this.Grd1.TabIndex = 52;
            this.Grd1.TreeCol = null;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd1_EllipsisButtonClick);
            this.Grd1.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd1_RequestEdit);
            // 
            // BtnSO
            // 
            this.BtnSO.BackColor = System.Drawing.Color.LightBlue;
            this.BtnSO.Dock = System.Windows.Forms.DockStyle.Top;
            this.BtnSO.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnSO.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSO.Location = new System.Drawing.Point(0, 0);
            this.BtnSO.Name = "BtnSO";
            this.BtnSO.Size = new System.Drawing.Size(823, 22);
            this.BtnSO.TabIndex = 36;
            this.BtnSO.Text = "Sales Order";
            this.BtnSO.UseVisualStyleBackColor = false;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Controls.Add(this.tabPage7);
            this.tabControl1.Controls.Add(this.tabPage8);
            this.tabControl1.Controls.Add(this.tabPage9);
            this.tabControl1.Controls.Add(this.tabPage10);
            this.tabControl1.Controls.Add(this.tabPage11);
            this.tabControl1.Controls.Add(this.tabPage12);
            this.tabControl1.Controls.Add(this.tabPage13);
            this.tabControl1.Controls.Add(this.tabPage14);
            this.tabControl1.Controls.Add(this.tabPage15);
            this.tabControl1.Controls.Add(this.tabPage16);
            this.tabControl1.Controls.Add(this.tabPage17);
            this.tabControl1.Controls.Add(this.tabPage18);
            this.tabControl1.Controls.Add(this.tabPage19);
            this.tabControl1.Controls.Add(this.tabPage20);
            this.tabControl1.Controls.Add(this.tabPage21);
            this.tabControl1.Controls.Add(this.tabPage22);
            this.tabControl1.Controls.Add(this.tabPage23);
            this.tabControl1.Controls.Add(this.tabPage24);
            this.tabControl1.Controls.Add(this.tabPage25);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(823, 127);
            this.tabControl1.TabIndex = 47;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.LueItCode1);
            this.tabPage1.Controls.Add(this.Grd11);
            this.tabPage1.Controls.Add(this.panel8);
            this.tabPage1.Location = new System.Drawing.Point(4, 23);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(815, 100);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "No.1";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // LueItCode1
            // 
            this.LueItCode1.EnterMoveNextControl = true;
            this.LueItCode1.Location = new System.Drawing.Point(83, 53);
            this.LueItCode1.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode1.Name = "LueItCode1";
            this.LueItCode1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode1.Properties.Appearance.Options.UseFont = true;
            this.LueItCode1.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode1.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode1.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode1.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode1.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode1.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode1.Properties.DropDownRows = 30;
            this.LueItCode1.Properties.NullText = "[Empty]";
            this.LueItCode1.Properties.PopupWidth = 660;
            this.LueItCode1.Size = new System.Drawing.Size(152, 20);
            this.LueItCode1.TabIndex = 53;
            this.LueItCode1.ToolTip = "F4 : Show/hide list";
            this.LueItCode1.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueItCode1.EditValueChanged += new System.EventHandler(this.LueItCode1_EditValueChanged_1);
            this.LueItCode1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueItCode1_KeyDown);
            this.LueItCode1.Leave += new System.EventHandler(this.LueItCode1_Leave);
            // 
            // Grd11
            // 
            this.Grd11.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd11.DefaultRow.Height = 20;
            this.Grd11.DefaultRow.Sortable = false;
            this.Grd11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd11.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd11.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd11.Header.Height = 21;
            this.Grd11.Location = new System.Drawing.Point(3, 33);
            this.Grd11.Name = "Grd11";
            this.Grd11.RowHeader.Visible = true;
            this.Grd11.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd11.SingleClickEdit = true;
            this.Grd11.Size = new System.Drawing.Size(809, 64);
            this.Grd11.TabIndex = 51;
            this.Grd11.TreeCol = null;
            this.Grd11.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd11.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd11_ColHdrDoubleClick);
            this.Grd11.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd11_EllipsisButtonClick);
            this.Grd11.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd11_RequestEdit);
            this.Grd11.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd11_AfterCommitEdit);
            this.Grd11.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd11_KeyDown);
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel8.Controls.Add(this.MeeSM1);
            this.panel8.Controls.Add(this.label119);
            this.panel8.Controls.Add(this.LueSize1);
            this.panel8.Controls.Add(this.label57);
            this.panel8.Controls.Add(this.TxtCnt1);
            this.panel8.Controls.Add(this.label28);
            this.panel8.Controls.Add(this.TxtSeal1);
            this.panel8.Controls.Add(this.label8);
            this.panel8.Controls.Add(this.TxtTotal1);
            this.panel8.Controls.Add(this.label11);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel8.Location = new System.Drawing.Point(3, 3);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(809, 30);
            this.panel8.TabIndex = 44;
            // 
            // MeeSM1
            // 
            this.MeeSM1.EnterMoveNextControl = true;
            this.MeeSM1.Location = new System.Drawing.Point(683, 5);
            this.MeeSM1.Margin = new System.Windows.Forms.Padding(5);
            this.MeeSM1.Name = "MeeSM1";
            this.MeeSM1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM1.Properties.Appearance.Options.UseFont = true;
            this.MeeSM1.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM1.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeSM1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeSM1.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM1.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeSM1.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM1.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeSM1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeSM1.Properties.MaxLength = 1000;
            this.MeeSM1.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeSM1.Properties.ShowIcon = false;
            this.MeeSM1.Size = new System.Drawing.Size(120, 20);
            this.MeeSM1.TabIndex = 63;
            this.MeeSM1.ToolTip = "F4 : Show/hide text";
            this.MeeSM1.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeSM1.ToolTipTitle = "Run System";
            // 
            // label119
            // 
            this.label119.AutoSize = true;
            this.label119.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label119.Location = new System.Drawing.Point(658, 8);
            this.label119.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label119.Name = "label119";
            this.label119.Size = new System.Drawing.Size(23, 14);
            this.label119.TabIndex = 61;
            this.label119.Text = "SM";
            this.label119.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueSize1
            // 
            this.LueSize1.EnterMoveNextControl = true;
            this.LueSize1.Location = new System.Drawing.Point(31, 5);
            this.LueSize1.Margin = new System.Windows.Forms.Padding(5);
            this.LueSize1.Name = "LueSize1";
            this.LueSize1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize1.Properties.Appearance.Options.UseFont = true;
            this.LueSize1.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize1.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSize1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSize1.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize1.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSize1.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize1.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSize1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSize1.Properties.DropDownRows = 20;
            this.LueSize1.Properties.NullText = "[Empty]";
            this.LueSize1.Properties.PopupWidth = 300;
            this.LueSize1.Size = new System.Drawing.Size(67, 20);
            this.LueSize1.TabIndex = 54;
            this.LueSize1.ToolTip = "F4 : Show/hide list";
            this.LueSize1.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSize1.EditValueChanged += new System.EventHandler(this.LueSize1_EditValueChanged);
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label57.ForeColor = System.Drawing.Color.Red;
            this.label57.Location = new System.Drawing.Point(3, 7);
            this.label57.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(28, 14);
            this.label57.TabIndex = 53;
            this.label57.Text = "Size";
            this.label57.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCnt1
            // 
            this.TxtCnt1.EditValue = "";
            this.TxtCnt1.EnterMoveNextControl = true;
            this.TxtCnt1.Location = new System.Drawing.Point(180, 4);
            this.TxtCnt1.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCnt1.Name = "TxtCnt1";
            this.TxtCnt1.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCnt1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCnt1.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCnt1.Properties.Appearance.Options.UseFont = true;
            this.TxtCnt1.Size = new System.Drawing.Size(144, 20);
            this.TxtCnt1.TabIndex = 56;
            this.TxtCnt1.Validated += new System.EventHandler(this.TxtCnt1_Validated);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Red;
            this.label28.Location = new System.Drawing.Point(100, 7);
            this.label28.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(78, 14);
            this.label28.TabIndex = 55;
            this.label28.Text = "Container No";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSeal1
            // 
            this.TxtSeal1.EditValue = "";
            this.TxtSeal1.EnterMoveNextControl = true;
            this.TxtSeal1.Location = new System.Drawing.Point(377, 5);
            this.TxtSeal1.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSeal1.Name = "TxtSeal1";
            this.TxtSeal1.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSeal1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSeal1.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSeal1.Properties.Appearance.Options.UseFont = true;
            this.TxtSeal1.Size = new System.Drawing.Size(145, 20);
            this.TxtSeal1.TabIndex = 58;
            this.TxtSeal1.Validated += new System.EventHandler(this.TxtSeal1_Validated);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(526, 8);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(35, 14);
            this.label8.TabIndex = 59;
            this.label8.Text = "Total";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal1
            // 
            this.TxtTotal1.EditValue = "";
            this.TxtTotal1.EnterMoveNextControl = true;
            this.TxtTotal1.Location = new System.Drawing.Point(564, 5);
            this.TxtTotal1.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal1.Name = "TxtTotal1";
            this.TxtTotal1.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal1.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal1.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal1.Size = new System.Drawing.Size(90, 20);
            this.TxtTotal1.TabIndex = 60;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Red;
            this.label11.Location = new System.Drawing.Point(327, 8);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(48, 14);
            this.label11.TabIndex = 57;
            this.label11.Text = "Seal No";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.LueItCode2);
            this.tabPage2.Controls.Add(this.Grd12);
            this.tabPage2.Controls.Add(this.panel9);
            this.tabPage2.Location = new System.Drawing.Point(4, 23);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(815, 100);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "No.2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // LueItCode2
            // 
            this.LueItCode2.EnterMoveNextControl = true;
            this.LueItCode2.Location = new System.Drawing.Point(72, 58);
            this.LueItCode2.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode2.Name = "LueItCode2";
            this.LueItCode2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode2.Properties.Appearance.Options.UseFont = true;
            this.LueItCode2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode2.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode2.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode2.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode2.Properties.DropDownRows = 20;
            this.LueItCode2.Properties.NullText = "[Empty]";
            this.LueItCode2.Properties.PopupWidth = 500;
            this.LueItCode2.Size = new System.Drawing.Size(152, 20);
            this.LueItCode2.TabIndex = 54;
            this.LueItCode2.ToolTip = "F4 : Show/hide list";
            this.LueItCode2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueItCode2.EditValueChanged += new System.EventHandler(this.LueItCode2_EditValueChanged);
            this.LueItCode2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueItCode2_KeyDown);
            this.LueItCode2.Leave += new System.EventHandler(this.LueItCode2_Leave);
            // 
            // Grd12
            // 
            this.Grd12.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd12.DefaultRow.Height = 20;
            this.Grd12.DefaultRow.Sortable = false;
            this.Grd12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd12.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd12.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd12.Header.Height = 21;
            this.Grd12.Location = new System.Drawing.Point(3, 35);
            this.Grd12.Name = "Grd12";
            this.Grd12.RowHeader.Visible = true;
            this.Grd12.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd12.SingleClickEdit = true;
            this.Grd12.Size = new System.Drawing.Size(809, 62);
            this.Grd12.TabIndex = 74;
            this.Grd12.TreeCol = null;
            this.Grd12.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd12.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd12_ColHdrDoubleClick);
            this.Grd12.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd12_EllipsisButtonClick);
            this.Grd12.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd12_RequestEdit);
            this.Grd12.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd12_AfterCommitEdit);
            this.Grd12.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd12_KeyDown);
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel9.Controls.Add(this.MeeSM2);
            this.panel9.Controls.Add(this.label120);
            this.panel9.Controls.Add(this.LueSize2);
            this.panel9.Controls.Add(this.label58);
            this.panel9.Controls.Add(this.TxtCnt2);
            this.panel9.Controls.Add(this.label9);
            this.panel9.Controls.Add(this.TxtSeal2);
            this.panel9.Controls.Add(this.label13);
            this.panel9.Controls.Add(this.TxtTotal2);
            this.panel9.Controls.Add(this.label41);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel9.Location = new System.Drawing.Point(3, 3);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(809, 32);
            this.panel9.TabIndex = 44;
            // 
            // MeeSM2
            // 
            this.MeeSM2.EnterMoveNextControl = true;
            this.MeeSM2.Location = new System.Drawing.Point(680, 6);
            this.MeeSM2.Margin = new System.Windows.Forms.Padding(5);
            this.MeeSM2.Name = "MeeSM2";
            this.MeeSM2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM2.Properties.Appearance.Options.UseFont = true;
            this.MeeSM2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeSM2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeSM2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM2.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeSM2.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM2.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeSM2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeSM2.Properties.MaxLength = 1000;
            this.MeeSM2.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeSM2.Properties.ShowIcon = false;
            this.MeeSM2.Size = new System.Drawing.Size(120, 20);
            this.MeeSM2.TabIndex = 73;
            this.MeeSM2.ToolTip = "F4 : Show/hide text";
            this.MeeSM2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeSM2.ToolTipTitle = "Run System";
            // 
            // label120
            // 
            this.label120.AutoSize = true;
            this.label120.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label120.Location = new System.Drawing.Point(655, 9);
            this.label120.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label120.Name = "label120";
            this.label120.Size = new System.Drawing.Size(23, 14);
            this.label120.TabIndex = 72;
            this.label120.Text = "SM";
            this.label120.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueSize2
            // 
            this.LueSize2.EnterMoveNextControl = true;
            this.LueSize2.Location = new System.Drawing.Point(31, 6);
            this.LueSize2.Margin = new System.Windows.Forms.Padding(5);
            this.LueSize2.Name = "LueSize2";
            this.LueSize2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize2.Properties.Appearance.Options.UseFont = true;
            this.LueSize2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSize2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSize2.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize2.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSize2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize2.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSize2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSize2.Properties.DropDownRows = 20;
            this.LueSize2.Properties.NullText = "[Empty]";
            this.LueSize2.Properties.PopupWidth = 300;
            this.LueSize2.Size = new System.Drawing.Size(67, 20);
            this.LueSize2.TabIndex = 65;
            this.LueSize2.ToolTip = "F4 : Show/hide list";
            this.LueSize2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSize2.EditValueChanged += new System.EventHandler(this.LueSize2_EditValueChanged);
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label58.ForeColor = System.Drawing.Color.Red;
            this.label58.Location = new System.Drawing.Point(3, 8);
            this.label58.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(28, 14);
            this.label58.TabIndex = 64;
            this.label58.Text = "Size";
            this.label58.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCnt2
            // 
            this.TxtCnt2.EditValue = "";
            this.TxtCnt2.EnterMoveNextControl = true;
            this.TxtCnt2.Location = new System.Drawing.Point(178, 6);
            this.TxtCnt2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCnt2.Name = "TxtCnt2";
            this.TxtCnt2.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCnt2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCnt2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCnt2.Properties.Appearance.Options.UseFont = true;
            this.TxtCnt2.Size = new System.Drawing.Size(146, 20);
            this.TxtCnt2.TabIndex = 67;
            this.TxtCnt2.Validated += new System.EventHandler(this.TxtCnt2_Validated);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Location = new System.Drawing.Point(98, 9);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(78, 14);
            this.label9.TabIndex = 66;
            this.label9.Text = "Container No";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSeal2
            // 
            this.TxtSeal2.EditValue = "";
            this.TxtSeal2.EnterMoveNextControl = true;
            this.TxtSeal2.Location = new System.Drawing.Point(379, 5);
            this.TxtSeal2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSeal2.Name = "TxtSeal2";
            this.TxtSeal2.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSeal2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSeal2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSeal2.Properties.Appearance.Options.UseFont = true;
            this.TxtSeal2.Size = new System.Drawing.Size(147, 20);
            this.TxtSeal2.TabIndex = 69;
            this.TxtSeal2.Validated += new System.EventHandler(this.TxtSeal2_Validated);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(530, 8);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(35, 14);
            this.label13.TabIndex = 70;
            this.label13.Text = "Total";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal2
            // 
            this.TxtTotal2.EditValue = "";
            this.TxtTotal2.EnterMoveNextControl = true;
            this.TxtTotal2.Location = new System.Drawing.Point(567, 5);
            this.TxtTotal2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal2.Name = "TxtTotal2";
            this.TxtTotal2.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal2.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal2.Size = new System.Drawing.Size(84, 20);
            this.TxtTotal2.TabIndex = 71;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.ForeColor = System.Drawing.Color.Red;
            this.label41.Location = new System.Drawing.Point(327, 8);
            this.label41.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(48, 14);
            this.label41.TabIndex = 68;
            this.label41.Text = "Seal No";
            this.label41.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.LueItCode3);
            this.tabPage3.Controls.Add(this.Grd13);
            this.tabPage3.Controls.Add(this.panel10);
            this.tabPage3.Location = new System.Drawing.Point(4, 23);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(815, 100);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "No.3";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // LueItCode3
            // 
            this.LueItCode3.EnterMoveNextControl = true;
            this.LueItCode3.Location = new System.Drawing.Point(69, 57);
            this.LueItCode3.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode3.Name = "LueItCode3";
            this.LueItCode3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode3.Properties.Appearance.Options.UseFont = true;
            this.LueItCode3.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode3.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode3.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode3.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode3.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode3.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode3.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode3.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode3.Properties.DropDownRows = 20;
            this.LueItCode3.Properties.NullText = "[Empty]";
            this.LueItCode3.Properties.PopupWidth = 500;
            this.LueItCode3.Size = new System.Drawing.Size(152, 20);
            this.LueItCode3.TabIndex = 54;
            this.LueItCode3.ToolTip = "F4 : Show/hide list";
            this.LueItCode3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueItCode3.EditValueChanged += new System.EventHandler(this.LueItCode3_EditValueChanged);
            this.LueItCode3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueItCode3_KeyDown);
            this.LueItCode3.Leave += new System.EventHandler(this.LueItCode3_Leave);
            // 
            // Grd13
            // 
            this.Grd13.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd13.DefaultRow.Height = 20;
            this.Grd13.DefaultRow.Sortable = false;
            this.Grd13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd13.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd13.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd13.Header.Height = 21;
            this.Grd13.Location = new System.Drawing.Point(0, 34);
            this.Grd13.Name = "Grd13";
            this.Grd13.RowHeader.Visible = true;
            this.Grd13.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd13.SingleClickEdit = true;
            this.Grd13.Size = new System.Drawing.Size(815, 66);
            this.Grd13.TabIndex = 85;
            this.Grd13.TreeCol = null;
            this.Grd13.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd13.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd13_ColHdrDoubleClick);
            this.Grd13.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd13_EllipsisButtonClick);
            this.Grd13.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd13_RequestEdit);
            this.Grd13.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd13_AfterCommitEdit);
            this.Grd13.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd13_KeyDown);
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel10.Controls.Add(this.MeeSM3);
            this.panel10.Controls.Add(this.label121);
            this.panel10.Controls.Add(this.LueSize3);
            this.panel10.Controls.Add(this.label59);
            this.panel10.Controls.Add(this.TxtCnt3);
            this.panel10.Controls.Add(this.label10);
            this.panel10.Controls.Add(this.TxtSeal3);
            this.panel10.Controls.Add(this.label15);
            this.panel10.Controls.Add(this.TxtTotal3);
            this.panel10.Controls.Add(this.label43);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel10.Location = new System.Drawing.Point(0, 0);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(815, 34);
            this.panel10.TabIndex = 44;
            // 
            // MeeSM3
            // 
            this.MeeSM3.EnterMoveNextControl = true;
            this.MeeSM3.Location = new System.Drawing.Point(687, 7);
            this.MeeSM3.Margin = new System.Windows.Forms.Padding(5);
            this.MeeSM3.Name = "MeeSM3";
            this.MeeSM3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM3.Properties.Appearance.Options.UseFont = true;
            this.MeeSM3.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM3.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeSM3.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM3.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeSM3.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM3.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeSM3.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM3.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeSM3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeSM3.Properties.MaxLength = 1000;
            this.MeeSM3.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeSM3.Properties.ShowIcon = false;
            this.MeeSM3.Size = new System.Drawing.Size(120, 20);
            this.MeeSM3.TabIndex = 84;
            this.MeeSM3.ToolTip = "F4 : Show/hide text";
            this.MeeSM3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeSM3.ToolTipTitle = "Run System";
            // 
            // label121
            // 
            this.label121.AutoSize = true;
            this.label121.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label121.Location = new System.Drawing.Point(662, 10);
            this.label121.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label121.Name = "label121";
            this.label121.Size = new System.Drawing.Size(23, 14);
            this.label121.TabIndex = 83;
            this.label121.Text = "SM";
            this.label121.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueSize3
            // 
            this.LueSize3.EnterMoveNextControl = true;
            this.LueSize3.Location = new System.Drawing.Point(31, 9);
            this.LueSize3.Margin = new System.Windows.Forms.Padding(5);
            this.LueSize3.Name = "LueSize3";
            this.LueSize3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize3.Properties.Appearance.Options.UseFont = true;
            this.LueSize3.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize3.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSize3.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize3.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSize3.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize3.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSize3.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize3.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSize3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSize3.Properties.DropDownRows = 20;
            this.LueSize3.Properties.NullText = "[Empty]";
            this.LueSize3.Properties.PopupWidth = 300;
            this.LueSize3.Size = new System.Drawing.Size(67, 20);
            this.LueSize3.TabIndex = 76;
            this.LueSize3.ToolTip = "F4 : Show/hide list";
            this.LueSize3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSize3.EditValueChanged += new System.EventHandler(this.LueSize3_EditValueChanged);
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label59.ForeColor = System.Drawing.Color.Red;
            this.label59.Location = new System.Drawing.Point(3, 11);
            this.label59.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(28, 14);
            this.label59.TabIndex = 75;
            this.label59.Text = "Size";
            this.label59.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCnt3
            // 
            this.TxtCnt3.EditValue = "";
            this.TxtCnt3.EnterMoveNextControl = true;
            this.TxtCnt3.Location = new System.Drawing.Point(180, 8);
            this.TxtCnt3.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCnt3.Name = "TxtCnt3";
            this.TxtCnt3.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCnt3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCnt3.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCnt3.Properties.Appearance.Options.UseFont = true;
            this.TxtCnt3.Size = new System.Drawing.Size(148, 20);
            this.TxtCnt3.TabIndex = 78;
            this.TxtCnt3.Validated += new System.EventHandler(this.TxtCnt3_Validated);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Red;
            this.label10.Location = new System.Drawing.Point(100, 11);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(78, 14);
            this.label10.TabIndex = 77;
            this.label10.Text = "Container No";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSeal3
            // 
            this.TxtSeal3.EditValue = "";
            this.TxtSeal3.EnterMoveNextControl = true;
            this.TxtSeal3.Location = new System.Drawing.Point(384, 7);
            this.TxtSeal3.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSeal3.Name = "TxtSeal3";
            this.TxtSeal3.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSeal3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSeal3.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSeal3.Properties.Appearance.Options.UseFont = true;
            this.TxtSeal3.Size = new System.Drawing.Size(145, 20);
            this.TxtSeal3.TabIndex = 80;
            this.TxtSeal3.Validated += new System.EventHandler(this.TxtSeal3_Validated);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(533, 11);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(35, 14);
            this.label15.TabIndex = 81;
            this.label15.Text = "Total";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal3
            // 
            this.TxtTotal3.EditValue = "";
            this.TxtTotal3.EnterMoveNextControl = true;
            this.TxtTotal3.Location = new System.Drawing.Point(570, 8);
            this.TxtTotal3.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal3.Name = "TxtTotal3";
            this.TxtTotal3.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal3.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal3.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal3.Size = new System.Drawing.Size(89, 20);
            this.TxtTotal3.TabIndex = 82;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.ForeColor = System.Drawing.Color.Red;
            this.label43.Location = new System.Drawing.Point(332, 10);
            this.label43.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(48, 14);
            this.label43.TabIndex = 79;
            this.label43.Text = "Seal No";
            this.label43.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.LueItCode4);
            this.tabPage4.Controls.Add(this.Grd14);
            this.tabPage4.Controls.Add(this.panel11);
            this.tabPage4.Location = new System.Drawing.Point(4, 23);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(815, 100);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "No.4";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // LueItCode4
            // 
            this.LueItCode4.EnterMoveNextControl = true;
            this.LueItCode4.Location = new System.Drawing.Point(71, 57);
            this.LueItCode4.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode4.Name = "LueItCode4";
            this.LueItCode4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode4.Properties.Appearance.Options.UseFont = true;
            this.LueItCode4.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode4.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode4.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode4.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode4.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode4.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode4.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode4.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode4.Properties.DropDownRows = 20;
            this.LueItCode4.Properties.NullText = "[Empty]";
            this.LueItCode4.Properties.PopupWidth = 500;
            this.LueItCode4.Size = new System.Drawing.Size(152, 20);
            this.LueItCode4.TabIndex = 54;
            this.LueItCode4.ToolTip = "F4 : Show/hide list";
            this.LueItCode4.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueItCode4.EditValueChanged += new System.EventHandler(this.LueItCode4_EditValueChanged);
            this.LueItCode4.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueItCode4_KeyDown);
            this.LueItCode4.Leave += new System.EventHandler(this.LueItCode4_Leave);
            // 
            // Grd14
            // 
            this.Grd14.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd14.DefaultRow.Height = 20;
            this.Grd14.DefaultRow.Sortable = false;
            this.Grd14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd14.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd14.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd14.Header.Height = 21;
            this.Grd14.Location = new System.Drawing.Point(0, 32);
            this.Grd14.Name = "Grd14";
            this.Grd14.RowHeader.Visible = true;
            this.Grd14.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd14.SingleClickEdit = true;
            this.Grd14.Size = new System.Drawing.Size(815, 68);
            this.Grd14.TabIndex = 96;
            this.Grd14.TreeCol = null;
            this.Grd14.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd14.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd14_ColHdrDoubleClick);
            this.Grd14.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd14_EllipsisButtonClick);
            this.Grd14.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd14_RequestEdit);
            this.Grd14.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd14_AfterCommitEdit);
            this.Grd14.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd14_KeyDown);
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel11.Controls.Add(this.MeeSM4);
            this.panel11.Controls.Add(this.label122);
            this.panel11.Controls.Add(this.LueSize4);
            this.panel11.Controls.Add(this.label62);
            this.panel11.Controls.Add(this.TxtCnt4);
            this.panel11.Controls.Add(this.label12);
            this.panel11.Controls.Add(this.TxtSeal4);
            this.panel11.Controls.Add(this.label19);
            this.panel11.Controls.Add(this.TxtTotal4);
            this.panel11.Controls.Add(this.label44);
            this.panel11.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel11.Location = new System.Drawing.Point(0, 0);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(815, 32);
            this.panel11.TabIndex = 44;
            // 
            // MeeSM4
            // 
            this.MeeSM4.EnterMoveNextControl = true;
            this.MeeSM4.Location = new System.Drawing.Point(691, 5);
            this.MeeSM4.Margin = new System.Windows.Forms.Padding(5);
            this.MeeSM4.Name = "MeeSM4";
            this.MeeSM4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM4.Properties.Appearance.Options.UseFont = true;
            this.MeeSM4.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM4.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeSM4.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM4.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeSM4.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM4.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeSM4.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM4.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeSM4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeSM4.Properties.MaxLength = 1000;
            this.MeeSM4.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeSM4.Properties.ShowIcon = false;
            this.MeeSM4.Size = new System.Drawing.Size(120, 20);
            this.MeeSM4.TabIndex = 95;
            this.MeeSM4.ToolTip = "F4 : Show/hide text";
            this.MeeSM4.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeSM4.ToolTipTitle = "Run System";
            // 
            // label122
            // 
            this.label122.AutoSize = true;
            this.label122.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label122.Location = new System.Drawing.Point(666, 8);
            this.label122.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label122.Name = "label122";
            this.label122.Size = new System.Drawing.Size(23, 14);
            this.label122.TabIndex = 94;
            this.label122.Text = "SM";
            this.label122.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueSize4
            // 
            this.LueSize4.EnterMoveNextControl = true;
            this.LueSize4.Location = new System.Drawing.Point(31, 7);
            this.LueSize4.Margin = new System.Windows.Forms.Padding(5);
            this.LueSize4.Name = "LueSize4";
            this.LueSize4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize4.Properties.Appearance.Options.UseFont = true;
            this.LueSize4.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize4.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSize4.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize4.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSize4.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize4.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSize4.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize4.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSize4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSize4.Properties.DropDownRows = 20;
            this.LueSize4.Properties.NullText = "[Empty]";
            this.LueSize4.Properties.PopupWidth = 300;
            this.LueSize4.Size = new System.Drawing.Size(67, 20);
            this.LueSize4.TabIndex = 87;
            this.LueSize4.ToolTip = "F4 : Show/hide list";
            this.LueSize4.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSize4.EditValueChanged += new System.EventHandler(this.LueSize4_EditValueChanged);
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label62.ForeColor = System.Drawing.Color.Red;
            this.label62.Location = new System.Drawing.Point(3, 9);
            this.label62.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(28, 14);
            this.label62.TabIndex = 86;
            this.label62.Text = "Size";
            this.label62.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCnt4
            // 
            this.TxtCnt4.EditValue = "";
            this.TxtCnt4.EnterMoveNextControl = true;
            this.TxtCnt4.Location = new System.Drawing.Point(180, 6);
            this.TxtCnt4.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCnt4.Name = "TxtCnt4";
            this.TxtCnt4.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCnt4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCnt4.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCnt4.Properties.Appearance.Options.UseFont = true;
            this.TxtCnt4.Size = new System.Drawing.Size(168, 20);
            this.TxtCnt4.TabIndex = 89;
            this.TxtCnt4.Validated += new System.EventHandler(this.TxtCnt4_Validated);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Red;
            this.label12.Location = new System.Drawing.Point(100, 9);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(78, 14);
            this.label12.TabIndex = 88;
            this.label12.Text = "Container No";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSeal4
            // 
            this.TxtSeal4.EditValue = "";
            this.TxtSeal4.EnterMoveNextControl = true;
            this.TxtSeal4.Location = new System.Drawing.Point(403, 7);
            this.TxtSeal4.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSeal4.Name = "TxtSeal4";
            this.TxtSeal4.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSeal4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSeal4.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSeal4.Properties.Appearance.Options.UseFont = true;
            this.TxtSeal4.Size = new System.Drawing.Size(136, 20);
            this.TxtSeal4.TabIndex = 91;
            this.TxtSeal4.Validated += new System.EventHandler(this.TxtSeal4_Validated);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(537, 9);
            this.label19.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(35, 14);
            this.label19.TabIndex = 92;
            this.label19.Text = "Total";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal4
            // 
            this.TxtTotal4.EditValue = "";
            this.TxtTotal4.EnterMoveNextControl = true;
            this.TxtTotal4.Location = new System.Drawing.Point(574, 6);
            this.TxtTotal4.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal4.Name = "TxtTotal4";
            this.TxtTotal4.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal4.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal4.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal4.Size = new System.Drawing.Size(90, 20);
            this.TxtTotal4.TabIndex = 93;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.ForeColor = System.Drawing.Color.Red;
            this.label44.Location = new System.Drawing.Point(351, 10);
            this.label44.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(48, 14);
            this.label44.TabIndex = 90;
            this.label44.Text = "Seal No";
            this.label44.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.LueItCode5);
            this.tabPage5.Controls.Add(this.Grd15);
            this.tabPage5.Controls.Add(this.panel12);
            this.tabPage5.Location = new System.Drawing.Point(4, 23);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(815, 100);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "No.5";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // LueItCode5
            // 
            this.LueItCode5.EnterMoveNextControl = true;
            this.LueItCode5.Location = new System.Drawing.Point(73, 55);
            this.LueItCode5.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode5.Name = "LueItCode5";
            this.LueItCode5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode5.Properties.Appearance.Options.UseFont = true;
            this.LueItCode5.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode5.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode5.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode5.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode5.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode5.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode5.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode5.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode5.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode5.Properties.DropDownRows = 20;
            this.LueItCode5.Properties.NullText = "[Empty]";
            this.LueItCode5.Properties.PopupWidth = 500;
            this.LueItCode5.Size = new System.Drawing.Size(152, 20);
            this.LueItCode5.TabIndex = 54;
            this.LueItCode5.ToolTip = "F4 : Show/hide list";
            this.LueItCode5.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueItCode5.EditValueChanged += new System.EventHandler(this.LueItCode5_EditValueChanged);
            this.LueItCode5.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueItCode5_KeyDown);
            this.LueItCode5.Leave += new System.EventHandler(this.LueItCode5_Leave);
            // 
            // Grd15
            // 
            this.Grd15.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd15.DefaultRow.Height = 20;
            this.Grd15.DefaultRow.Sortable = false;
            this.Grd15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd15.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd15.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd15.Header.Height = 21;
            this.Grd15.Location = new System.Drawing.Point(0, 32);
            this.Grd15.Name = "Grd15";
            this.Grd15.RowHeader.Visible = true;
            this.Grd15.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd15.SingleClickEdit = true;
            this.Grd15.Size = new System.Drawing.Size(815, 68);
            this.Grd15.TabIndex = 107;
            this.Grd15.TreeCol = null;
            this.Grd15.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd15.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd15_ColHdrDoubleClick);
            this.Grd15.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd15_EllipsisButtonClick);
            this.Grd15.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd15_RequestEdit);
            this.Grd15.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd15_AfterCommitEdit);
            this.Grd15.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd15_KeyDown);
            // 
            // panel12
            // 
            this.panel12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel12.Controls.Add(this.MeeSM5);
            this.panel12.Controls.Add(this.label123);
            this.panel12.Controls.Add(this.LueSize5);
            this.panel12.Controls.Add(this.label63);
            this.panel12.Controls.Add(this.TxtCnt5);
            this.panel12.Controls.Add(this.label14);
            this.panel12.Controls.Add(this.TxtSeal5);
            this.panel12.Controls.Add(this.label21);
            this.panel12.Controls.Add(this.TxtTotal5);
            this.panel12.Controls.Add(this.label45);
            this.panel12.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel12.Location = new System.Drawing.Point(0, 0);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(815, 32);
            this.panel12.TabIndex = 44;
            // 
            // MeeSM5
            // 
            this.MeeSM5.EnterMoveNextControl = true;
            this.MeeSM5.Location = new System.Drawing.Point(692, 6);
            this.MeeSM5.Margin = new System.Windows.Forms.Padding(5);
            this.MeeSM5.Name = "MeeSM5";
            this.MeeSM5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM5.Properties.Appearance.Options.UseFont = true;
            this.MeeSM5.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM5.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeSM5.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM5.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeSM5.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM5.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeSM5.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM5.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeSM5.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeSM5.Properties.MaxLength = 1000;
            this.MeeSM5.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeSM5.Properties.ShowIcon = false;
            this.MeeSM5.Size = new System.Drawing.Size(120, 20);
            this.MeeSM5.TabIndex = 106;
            this.MeeSM5.ToolTip = "F4 : Show/hide text";
            this.MeeSM5.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeSM5.ToolTipTitle = "Run System";
            // 
            // label123
            // 
            this.label123.AutoSize = true;
            this.label123.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label123.Location = new System.Drawing.Point(667, 9);
            this.label123.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label123.Name = "label123";
            this.label123.Size = new System.Drawing.Size(23, 14);
            this.label123.TabIndex = 105;
            this.label123.Text = "SM";
            this.label123.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueSize5
            // 
            this.LueSize5.EnterMoveNextControl = true;
            this.LueSize5.Location = new System.Drawing.Point(36, 6);
            this.LueSize5.Margin = new System.Windows.Forms.Padding(5);
            this.LueSize5.Name = "LueSize5";
            this.LueSize5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize5.Properties.Appearance.Options.UseFont = true;
            this.LueSize5.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize5.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSize5.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize5.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSize5.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize5.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSize5.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize5.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSize5.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSize5.Properties.DropDownRows = 20;
            this.LueSize5.Properties.NullText = "[Empty]";
            this.LueSize5.Properties.PopupWidth = 300;
            this.LueSize5.Size = new System.Drawing.Size(67, 20);
            this.LueSize5.TabIndex = 98;
            this.LueSize5.ToolTip = "F4 : Show/hide list";
            this.LueSize5.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSize5.EditValueChanged += new System.EventHandler(this.LueSize5_EditValueChanged);
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label63.ForeColor = System.Drawing.Color.Red;
            this.label63.Location = new System.Drawing.Point(8, 8);
            this.label63.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(28, 14);
            this.label63.TabIndex = 97;
            this.label63.Text = "Size";
            this.label63.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCnt5
            // 
            this.TxtCnt5.EditValue = "";
            this.TxtCnt5.EnterMoveNextControl = true;
            this.TxtCnt5.Location = new System.Drawing.Point(187, 6);
            this.TxtCnt5.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCnt5.Name = "TxtCnt5";
            this.TxtCnt5.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCnt5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCnt5.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCnt5.Properties.Appearance.Options.UseFont = true;
            this.TxtCnt5.Size = new System.Drawing.Size(166, 20);
            this.TxtCnt5.TabIndex = 100;
            this.TxtCnt5.Validated += new System.EventHandler(this.TxtCnt5_Validated);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Red;
            this.label14.Location = new System.Drawing.Point(107, 9);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(78, 14);
            this.label14.TabIndex = 99;
            this.label14.Text = "Container No";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSeal5
            // 
            this.TxtSeal5.EditValue = "";
            this.TxtSeal5.EnterMoveNextControl = true;
            this.TxtSeal5.Location = new System.Drawing.Point(407, 6);
            this.TxtSeal5.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSeal5.Name = "TxtSeal5";
            this.TxtSeal5.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSeal5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSeal5.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSeal5.Properties.Appearance.Options.UseFont = true;
            this.TxtSeal5.Size = new System.Drawing.Size(134, 20);
            this.TxtSeal5.TabIndex = 102;
            this.TxtSeal5.Validated += new System.EventHandler(this.TxtSeal5_Validated);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Black;
            this.label21.Location = new System.Drawing.Point(540, 8);
            this.label21.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(35, 14);
            this.label21.TabIndex = 103;
            this.label21.Text = "Total";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal5
            // 
            this.TxtTotal5.EditValue = "";
            this.TxtTotal5.EnterMoveNextControl = true;
            this.TxtTotal5.Location = new System.Drawing.Point(577, 5);
            this.TxtTotal5.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal5.Name = "TxtTotal5";
            this.TxtTotal5.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal5.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal5.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal5.Size = new System.Drawing.Size(90, 20);
            this.TxtTotal5.TabIndex = 104;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.ForeColor = System.Drawing.Color.Red;
            this.label45.Location = new System.Drawing.Point(356, 9);
            this.label45.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(48, 14);
            this.label45.TabIndex = 101;
            this.label45.Text = "Seal No";
            this.label45.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.LueItCode6);
            this.tabPage6.Controls.Add(this.Grd16);
            this.tabPage6.Controls.Add(this.panel13);
            this.tabPage6.Location = new System.Drawing.Point(4, 23);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Size = new System.Drawing.Size(815, 100);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "No.6";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // LueItCode6
            // 
            this.LueItCode6.EnterMoveNextControl = true;
            this.LueItCode6.Location = new System.Drawing.Point(75, 56);
            this.LueItCode6.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode6.Name = "LueItCode6";
            this.LueItCode6.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode6.Properties.Appearance.Options.UseFont = true;
            this.LueItCode6.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode6.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode6.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode6.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode6.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode6.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode6.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode6.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode6.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode6.Properties.DropDownRows = 20;
            this.LueItCode6.Properties.NullText = "[Empty]";
            this.LueItCode6.Properties.PopupWidth = 500;
            this.LueItCode6.Size = new System.Drawing.Size(152, 20);
            this.LueItCode6.TabIndex = 54;
            this.LueItCode6.ToolTip = "F4 : Show/hide list";
            this.LueItCode6.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueItCode6.EditValueChanged += new System.EventHandler(this.LueItCode6_EditValueChanged);
            this.LueItCode6.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueItCode6_KeyDown);
            this.LueItCode6.Leave += new System.EventHandler(this.LueItCode6_Leave);
            // 
            // Grd16
            // 
            this.Grd16.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd16.DefaultRow.Height = 20;
            this.Grd16.DefaultRow.Sortable = false;
            this.Grd16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd16.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd16.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd16.Header.Height = 21;
            this.Grd16.Location = new System.Drawing.Point(0, 33);
            this.Grd16.Name = "Grd16";
            this.Grd16.RowHeader.Visible = true;
            this.Grd16.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd16.SingleClickEdit = true;
            this.Grd16.Size = new System.Drawing.Size(815, 67);
            this.Grd16.TabIndex = 51;
            this.Grd16.TreeCol = null;
            this.Grd16.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd16.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd16_ColHdrDoubleClick);
            this.Grd16.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd16_EllipsisButtonClick);
            this.Grd16.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd16_RequestEdit);
            this.Grd16.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd16_AfterCommitEdit);
            this.Grd16.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd16_KeyDown);
            // 
            // panel13
            // 
            this.panel13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel13.Controls.Add(this.MeeSM6);
            this.panel13.Controls.Add(this.label124);
            this.panel13.Controls.Add(this.LueSize6);
            this.panel13.Controls.Add(this.label64);
            this.panel13.Controls.Add(this.TxtCnt6);
            this.panel13.Controls.Add(this.label16);
            this.panel13.Controls.Add(this.TxtSeal6);
            this.panel13.Controls.Add(this.label23);
            this.panel13.Controls.Add(this.TxtTotal6);
            this.panel13.Controls.Add(this.label46);
            this.panel13.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel13.Location = new System.Drawing.Point(0, 0);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(815, 33);
            this.panel13.TabIndex = 44;
            // 
            // MeeSM6
            // 
            this.MeeSM6.EnterMoveNextControl = true;
            this.MeeSM6.Location = new System.Drawing.Point(692, 6);
            this.MeeSM6.Margin = new System.Windows.Forms.Padding(5);
            this.MeeSM6.Name = "MeeSM6";
            this.MeeSM6.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM6.Properties.Appearance.Options.UseFont = true;
            this.MeeSM6.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM6.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeSM6.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM6.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeSM6.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM6.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeSM6.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM6.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeSM6.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeSM6.Properties.MaxLength = 1000;
            this.MeeSM6.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeSM6.Properties.ShowIcon = false;
            this.MeeSM6.Size = new System.Drawing.Size(120, 20);
            this.MeeSM6.TabIndex = 118;
            this.MeeSM6.ToolTip = "F4 : Show/hide text";
            this.MeeSM6.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeSM6.ToolTipTitle = "Run System";
            // 
            // label124
            // 
            this.label124.AutoSize = true;
            this.label124.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label124.Location = new System.Drawing.Point(667, 9);
            this.label124.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label124.Name = "label124";
            this.label124.Size = new System.Drawing.Size(23, 14);
            this.label124.TabIndex = 116;
            this.label124.Text = "SM";
            this.label124.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueSize6
            // 
            this.LueSize6.EnterMoveNextControl = true;
            this.LueSize6.Location = new System.Drawing.Point(33, 5);
            this.LueSize6.Margin = new System.Windows.Forms.Padding(5);
            this.LueSize6.Name = "LueSize6";
            this.LueSize6.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize6.Properties.Appearance.Options.UseFont = true;
            this.LueSize6.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize6.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSize6.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize6.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSize6.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize6.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSize6.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize6.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSize6.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSize6.Properties.DropDownRows = 20;
            this.LueSize6.Properties.NullText = "[Empty]";
            this.LueSize6.Properties.PopupWidth = 300;
            this.LueSize6.Size = new System.Drawing.Size(67, 20);
            this.LueSize6.TabIndex = 109;
            this.LueSize6.ToolTip = "F4 : Show/hide list";
            this.LueSize6.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSize6.EditValueChanged += new System.EventHandler(this.LueSize6_EditValueChanged);
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label64.ForeColor = System.Drawing.Color.Red;
            this.label64.Location = new System.Drawing.Point(5, 7);
            this.label64.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(28, 14);
            this.label64.TabIndex = 108;
            this.label64.Text = "Size";
            this.label64.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCnt6
            // 
            this.TxtCnt6.EditValue = "";
            this.TxtCnt6.EnterMoveNextControl = true;
            this.TxtCnt6.Location = new System.Drawing.Point(181, 6);
            this.TxtCnt6.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCnt6.Name = "TxtCnt6";
            this.TxtCnt6.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCnt6.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCnt6.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCnt6.Properties.Appearance.Options.UseFont = true;
            this.TxtCnt6.Size = new System.Drawing.Size(155, 20);
            this.TxtCnt6.TabIndex = 111;
            this.TxtCnt6.Validated += new System.EventHandler(this.TxtCnt6_Validated);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Red;
            this.label16.Location = new System.Drawing.Point(101, 8);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(78, 14);
            this.label16.TabIndex = 110;
            this.label16.Text = "Container No";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSeal6
            // 
            this.TxtSeal6.EditValue = "";
            this.TxtSeal6.EnterMoveNextControl = true;
            this.TxtSeal6.Location = new System.Drawing.Point(390, 5);
            this.TxtSeal6.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSeal6.Name = "TxtSeal6";
            this.TxtSeal6.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSeal6.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSeal6.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSeal6.Properties.Appearance.Options.UseFont = true;
            this.TxtSeal6.Size = new System.Drawing.Size(146, 20);
            this.TxtSeal6.TabIndex = 113;
            this.TxtSeal6.Validated += new System.EventHandler(this.TxtSeal6_Validated);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Black;
            this.label23.Location = new System.Drawing.Point(539, 9);
            this.label23.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(35, 14);
            this.label23.TabIndex = 114;
            this.label23.Text = "Total";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal6
            // 
            this.TxtTotal6.EditValue = "";
            this.TxtTotal6.EnterMoveNextControl = true;
            this.TxtTotal6.Location = new System.Drawing.Point(576, 6);
            this.TxtTotal6.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal6.Name = "TxtTotal6";
            this.TxtTotal6.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal6.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal6.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal6.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal6.Size = new System.Drawing.Size(90, 20);
            this.TxtTotal6.TabIndex = 115;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.ForeColor = System.Drawing.Color.Red;
            this.label46.Location = new System.Drawing.Point(338, 8);
            this.label46.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(48, 14);
            this.label46.TabIndex = 112;
            this.label46.Text = "Seal No";
            this.label46.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage7
            // 
            this.tabPage7.Controls.Add(this.LueItCode7);
            this.tabPage7.Controls.Add(this.Grd17);
            this.tabPage7.Controls.Add(this.panel14);
            this.tabPage7.Location = new System.Drawing.Point(4, 23);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Size = new System.Drawing.Size(815, 100);
            this.tabPage7.TabIndex = 6;
            this.tabPage7.Text = "No.7";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // LueItCode7
            // 
            this.LueItCode7.EnterMoveNextControl = true;
            this.LueItCode7.Location = new System.Drawing.Point(67, 55);
            this.LueItCode7.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode7.Name = "LueItCode7";
            this.LueItCode7.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode7.Properties.Appearance.Options.UseFont = true;
            this.LueItCode7.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode7.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode7.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode7.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode7.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode7.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode7.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode7.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode7.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode7.Properties.DropDownRows = 20;
            this.LueItCode7.Properties.NullText = "[Empty]";
            this.LueItCode7.Properties.PopupWidth = 500;
            this.LueItCode7.Size = new System.Drawing.Size(152, 20);
            this.LueItCode7.TabIndex = 54;
            this.LueItCode7.ToolTip = "F4 : Show/hide list";
            this.LueItCode7.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueItCode7.EditValueChanged += new System.EventHandler(this.LueItCode7_EditValueChanged);
            this.LueItCode7.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueItCode7_KeyDown);
            this.LueItCode7.Leave += new System.EventHandler(this.LueItCode7_Leave);
            // 
            // Grd17
            // 
            this.Grd17.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd17.DefaultRow.Height = 20;
            this.Grd17.DefaultRow.Sortable = false;
            this.Grd17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd17.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd17.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd17.Header.Height = 21;
            this.Grd17.Location = new System.Drawing.Point(0, 32);
            this.Grd17.Name = "Grd17";
            this.Grd17.RowHeader.Visible = true;
            this.Grd17.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd17.SingleClickEdit = true;
            this.Grd17.Size = new System.Drawing.Size(815, 68);
            this.Grd17.TabIndex = 51;
            this.Grd17.TreeCol = null;
            this.Grd17.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd17.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd17_ColHdrDoubleClick);
            this.Grd17.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd17_EllipsisButtonClick);
            this.Grd17.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd17_RequestEdit);
            this.Grd17.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd17_AfterCommitEdit);
            this.Grd17.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd17_KeyDown);
            // 
            // panel14
            // 
            this.panel14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel14.Controls.Add(this.MeeSM7);
            this.panel14.Controls.Add(this.label125);
            this.panel14.Controls.Add(this.LueSize7);
            this.panel14.Controls.Add(this.label65);
            this.panel14.Controls.Add(this.TxtCnt7);
            this.panel14.Controls.Add(this.label20);
            this.panel14.Controls.Add(this.TxtSeal7);
            this.panel14.Controls.Add(this.label25);
            this.panel14.Controls.Add(this.TxtTotal7);
            this.panel14.Controls.Add(this.label47);
            this.panel14.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel14.Location = new System.Drawing.Point(0, 0);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(815, 32);
            this.panel14.TabIndex = 44;
            // 
            // MeeSM7
            // 
            this.MeeSM7.EnterMoveNextControl = true;
            this.MeeSM7.Location = new System.Drawing.Point(692, 5);
            this.MeeSM7.Margin = new System.Windows.Forms.Padding(5);
            this.MeeSM7.Name = "MeeSM7";
            this.MeeSM7.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM7.Properties.Appearance.Options.UseFont = true;
            this.MeeSM7.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM7.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeSM7.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM7.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeSM7.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM7.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeSM7.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM7.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeSM7.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeSM7.Properties.MaxLength = 1000;
            this.MeeSM7.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeSM7.Properties.ShowIcon = false;
            this.MeeSM7.Size = new System.Drawing.Size(120, 20);
            this.MeeSM7.TabIndex = 129;
            this.MeeSM7.ToolTip = "F4 : Show/hide text";
            this.MeeSM7.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeSM7.ToolTipTitle = "Run System";
            // 
            // label125
            // 
            this.label125.AutoSize = true;
            this.label125.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label125.Location = new System.Drawing.Point(667, 8);
            this.label125.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label125.Name = "label125";
            this.label125.Size = new System.Drawing.Size(23, 14);
            this.label125.TabIndex = 127;
            this.label125.Text = "SM";
            this.label125.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueSize7
            // 
            this.LueSize7.EnterMoveNextControl = true;
            this.LueSize7.Location = new System.Drawing.Point(33, 6);
            this.LueSize7.Margin = new System.Windows.Forms.Padding(5);
            this.LueSize7.Name = "LueSize7";
            this.LueSize7.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize7.Properties.Appearance.Options.UseFont = true;
            this.LueSize7.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize7.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSize7.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize7.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSize7.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize7.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSize7.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize7.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSize7.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSize7.Properties.DropDownRows = 20;
            this.LueSize7.Properties.NullText = "[Empty]";
            this.LueSize7.Properties.PopupWidth = 300;
            this.LueSize7.Size = new System.Drawing.Size(67, 20);
            this.LueSize7.TabIndex = 120;
            this.LueSize7.ToolTip = "F4 : Show/hide list";
            this.LueSize7.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSize7.EditValueChanged += new System.EventHandler(this.LueSize7_EditValueChanged);
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label65.ForeColor = System.Drawing.Color.Red;
            this.label65.Location = new System.Drawing.Point(5, 8);
            this.label65.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(28, 14);
            this.label65.TabIndex = 119;
            this.label65.Text = "Size";
            this.label65.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCnt7
            // 
            this.TxtCnt7.EditValue = "";
            this.TxtCnt7.EnterMoveNextControl = true;
            this.TxtCnt7.Location = new System.Drawing.Point(182, 6);
            this.TxtCnt7.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCnt7.Name = "TxtCnt7";
            this.TxtCnt7.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCnt7.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCnt7.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCnt7.Properties.Appearance.Options.UseFont = true;
            this.TxtCnt7.Size = new System.Drawing.Size(167, 20);
            this.TxtCnt7.TabIndex = 122;
            this.TxtCnt7.Validated += new System.EventHandler(this.TxtCnt7_Validated);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Red;
            this.label20.Location = new System.Drawing.Point(102, 9);
            this.label20.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(78, 14);
            this.label20.TabIndex = 121;
            this.label20.Text = "Container No";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSeal7
            // 
            this.TxtSeal7.EditValue = "";
            this.TxtSeal7.EnterMoveNextControl = true;
            this.TxtSeal7.Location = new System.Drawing.Point(401, 6);
            this.TxtSeal7.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSeal7.Name = "TxtSeal7";
            this.TxtSeal7.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSeal7.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSeal7.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSeal7.Properties.Appearance.Options.UseFont = true;
            this.TxtSeal7.Size = new System.Drawing.Size(148, 20);
            this.TxtSeal7.TabIndex = 124;
            this.TxtSeal7.Validated += new System.EventHandler(this.TxtSeal7_Validated);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Black;
            this.label25.Location = new System.Drawing.Point(548, 8);
            this.label25.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(35, 14);
            this.label25.TabIndex = 125;
            this.label25.Text = "Total";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal7
            // 
            this.TxtTotal7.EditValue = "";
            this.TxtTotal7.EnterMoveNextControl = true;
            this.TxtTotal7.Location = new System.Drawing.Point(585, 5);
            this.TxtTotal7.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal7.Name = "TxtTotal7";
            this.TxtTotal7.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal7.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal7.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal7.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal7.Size = new System.Drawing.Size(82, 20);
            this.TxtTotal7.TabIndex = 126;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.ForeColor = System.Drawing.Color.Red;
            this.label47.Location = new System.Drawing.Point(349, 9);
            this.label47.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(48, 14);
            this.label47.TabIndex = 123;
            this.label47.Text = "Seal No";
            this.label47.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage8
            // 
            this.tabPage8.Controls.Add(this.LueItCode8);
            this.tabPage8.Controls.Add(this.Grd18);
            this.tabPage8.Controls.Add(this.panel15);
            this.tabPage8.Location = new System.Drawing.Point(4, 23);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Size = new System.Drawing.Size(815, 100);
            this.tabPage8.TabIndex = 7;
            this.tabPage8.Text = "No.8";
            this.tabPage8.UseVisualStyleBackColor = true;
            // 
            // LueItCode8
            // 
            this.LueItCode8.EnterMoveNextControl = true;
            this.LueItCode8.Location = new System.Drawing.Point(71, 57);
            this.LueItCode8.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode8.Name = "LueItCode8";
            this.LueItCode8.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode8.Properties.Appearance.Options.UseFont = true;
            this.LueItCode8.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode8.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode8.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode8.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode8.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode8.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode8.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode8.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode8.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode8.Properties.DropDownRows = 20;
            this.LueItCode8.Properties.NullText = "[Empty]";
            this.LueItCode8.Properties.PopupWidth = 500;
            this.LueItCode8.Size = new System.Drawing.Size(152, 20);
            this.LueItCode8.TabIndex = 54;
            this.LueItCode8.ToolTip = "F4 : Show/hide list";
            this.LueItCode8.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueItCode8.EditValueChanged += new System.EventHandler(this.LueItCode8_EditValueChanged);
            this.LueItCode8.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueItCode8_KeyDown);
            this.LueItCode8.Leave += new System.EventHandler(this.LueItCode8_Leave);
            // 
            // Grd18
            // 
            this.Grd18.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd18.DefaultRow.Height = 20;
            this.Grd18.DefaultRow.Sortable = false;
            this.Grd18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd18.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd18.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd18.Header.Height = 21;
            this.Grd18.Location = new System.Drawing.Point(0, 32);
            this.Grd18.Name = "Grd18";
            this.Grd18.RowHeader.Visible = true;
            this.Grd18.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd18.SingleClickEdit = true;
            this.Grd18.Size = new System.Drawing.Size(815, 68);
            this.Grd18.TabIndex = 140;
            this.Grd18.TreeCol = null;
            this.Grd18.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd18.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd18_ColHdrDoubleClick);
            this.Grd18.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd18_EllipsisButtonClick);
            this.Grd18.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd18_RequestEdit);
            this.Grd18.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd18_AfterCommitEdit);
            this.Grd18.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd18_KeyDown);
            // 
            // panel15
            // 
            this.panel15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel15.Controls.Add(this.MeeSM8);
            this.panel15.Controls.Add(this.label126);
            this.panel15.Controls.Add(this.LueSize8);
            this.panel15.Controls.Add(this.label66);
            this.panel15.Controls.Add(this.TxtCnt8);
            this.panel15.Controls.Add(this.label22);
            this.panel15.Controls.Add(this.TxtSeal8);
            this.panel15.Controls.Add(this.label27);
            this.panel15.Controls.Add(this.TxtTotal8);
            this.panel15.Controls.Add(this.label48);
            this.panel15.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel15.Location = new System.Drawing.Point(0, 0);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(815, 32);
            this.panel15.TabIndex = 44;
            // 
            // MeeSM8
            // 
            this.MeeSM8.EnterMoveNextControl = true;
            this.MeeSM8.Location = new System.Drawing.Point(691, 7);
            this.MeeSM8.Margin = new System.Windows.Forms.Padding(5);
            this.MeeSM8.Name = "MeeSM8";
            this.MeeSM8.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM8.Properties.Appearance.Options.UseFont = true;
            this.MeeSM8.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM8.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeSM8.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM8.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeSM8.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM8.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeSM8.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM8.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeSM8.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeSM8.Properties.MaxLength = 1000;
            this.MeeSM8.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeSM8.Properties.ShowIcon = false;
            this.MeeSM8.Size = new System.Drawing.Size(120, 20);
            this.MeeSM8.TabIndex = 139;
            this.MeeSM8.ToolTip = "F4 : Show/hide text";
            this.MeeSM8.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeSM8.ToolTipTitle = "Run System";
            // 
            // label126
            // 
            this.label126.AutoSize = true;
            this.label126.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label126.Location = new System.Drawing.Point(666, 10);
            this.label126.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label126.Name = "label126";
            this.label126.Size = new System.Drawing.Size(23, 14);
            this.label126.TabIndex = 138;
            this.label126.Text = "SM";
            this.label126.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueSize8
            // 
            this.LueSize8.EnterMoveNextControl = true;
            this.LueSize8.Location = new System.Drawing.Point(34, 7);
            this.LueSize8.Margin = new System.Windows.Forms.Padding(5);
            this.LueSize8.Name = "LueSize8";
            this.LueSize8.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize8.Properties.Appearance.Options.UseFont = true;
            this.LueSize8.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize8.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSize8.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize8.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSize8.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize8.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSize8.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize8.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSize8.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSize8.Properties.DropDownRows = 20;
            this.LueSize8.Properties.NullText = "[Empty]";
            this.LueSize8.Properties.PopupWidth = 300;
            this.LueSize8.Size = new System.Drawing.Size(67, 20);
            this.LueSize8.TabIndex = 131;
            this.LueSize8.ToolTip = "F4 : Show/hide list";
            this.LueSize8.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSize8.EditValueChanged += new System.EventHandler(this.LueSize8_EditValueChanged);
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label66.ForeColor = System.Drawing.Color.Red;
            this.label66.Location = new System.Drawing.Point(6, 9);
            this.label66.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(28, 14);
            this.label66.TabIndex = 130;
            this.label66.Text = "Size";
            this.label66.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCnt8
            // 
            this.TxtCnt8.EditValue = "";
            this.TxtCnt8.EnterMoveNextControl = true;
            this.TxtCnt8.Location = new System.Drawing.Point(185, 5);
            this.TxtCnt8.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCnt8.Name = "TxtCnt8";
            this.TxtCnt8.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCnt8.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCnt8.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCnt8.Properties.Appearance.Options.UseFont = true;
            this.TxtCnt8.Size = new System.Drawing.Size(149, 20);
            this.TxtCnt8.TabIndex = 133;
            this.TxtCnt8.Validated += new System.EventHandler(this.TxtCnt8_Validated);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Red;
            this.label22.Location = new System.Drawing.Point(105, 8);
            this.label22.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(78, 14);
            this.label22.TabIndex = 132;
            this.label22.Text = "Container No";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSeal8
            // 
            this.TxtSeal8.EditValue = "";
            this.TxtSeal8.EnterMoveNextControl = true;
            this.TxtSeal8.Location = new System.Drawing.Point(387, 6);
            this.TxtSeal8.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSeal8.Name = "TxtSeal8";
            this.TxtSeal8.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSeal8.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSeal8.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSeal8.Properties.Appearance.Options.UseFont = true;
            this.TxtSeal8.Size = new System.Drawing.Size(149, 20);
            this.TxtSeal8.TabIndex = 135;
            this.TxtSeal8.Validated += new System.EventHandler(this.TxtSeal8_Validated);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Black;
            this.label27.Location = new System.Drawing.Point(537, 9);
            this.label27.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(35, 14);
            this.label27.TabIndex = 136;
            this.label27.Text = "Total";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal8
            // 
            this.TxtTotal8.EditValue = "";
            this.TxtTotal8.EnterMoveNextControl = true;
            this.TxtTotal8.Location = new System.Drawing.Point(574, 6);
            this.TxtTotal8.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal8.Name = "TxtTotal8";
            this.TxtTotal8.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal8.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal8.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal8.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal8.Size = new System.Drawing.Size(90, 20);
            this.TxtTotal8.TabIndex = 137;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.ForeColor = System.Drawing.Color.Red;
            this.label48.Location = new System.Drawing.Point(335, 9);
            this.label48.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(48, 14);
            this.label48.TabIndex = 134;
            this.label48.Text = "Seal No";
            this.label48.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage9
            // 
            this.tabPage9.Controls.Add(this.LueItCode9);
            this.tabPage9.Controls.Add(this.Grd19);
            this.tabPage9.Controls.Add(this.panel16);
            this.tabPage9.Location = new System.Drawing.Point(4, 23);
            this.tabPage9.Name = "tabPage9";
            this.tabPage9.Size = new System.Drawing.Size(815, 100);
            this.tabPage9.TabIndex = 8;
            this.tabPage9.Text = "No.9";
            this.tabPage9.UseVisualStyleBackColor = true;
            // 
            // LueItCode9
            // 
            this.LueItCode9.EnterMoveNextControl = true;
            this.LueItCode9.Location = new System.Drawing.Point(67, 59);
            this.LueItCode9.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode9.Name = "LueItCode9";
            this.LueItCode9.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode9.Properties.Appearance.Options.UseFont = true;
            this.LueItCode9.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode9.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode9.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode9.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode9.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode9.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode9.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode9.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode9.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode9.Properties.DropDownRows = 20;
            this.LueItCode9.Properties.NullText = "[Empty]";
            this.LueItCode9.Properties.PopupWidth = 500;
            this.LueItCode9.Size = new System.Drawing.Size(152, 20);
            this.LueItCode9.TabIndex = 54;
            this.LueItCode9.ToolTip = "F4 : Show/hide list";
            this.LueItCode9.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueItCode9.EditValueChanged += new System.EventHandler(this.LueItCode9_EditValueChanged);
            this.LueItCode9.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueItCode9_KeyDown);
            this.LueItCode9.Leave += new System.EventHandler(this.LueItCode9_Leave);
            // 
            // Grd19
            // 
            this.Grd19.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd19.DefaultRow.Height = 20;
            this.Grd19.DefaultRow.Sortable = false;
            this.Grd19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd19.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd19.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd19.Header.Height = 21;
            this.Grd19.Location = new System.Drawing.Point(0, 32);
            this.Grd19.Name = "Grd19";
            this.Grd19.RowHeader.Visible = true;
            this.Grd19.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd19.SingleClickEdit = true;
            this.Grd19.Size = new System.Drawing.Size(815, 68);
            this.Grd19.TabIndex = 151;
            this.Grd19.TreeCol = null;
            this.Grd19.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd19.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd19_ColHdrDoubleClick);
            this.Grd19.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd19_EllipsisButtonClick);
            this.Grd19.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd19_RequestEdit);
            this.Grd19.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd19_AfterCommitEdit);
            this.Grd19.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd19_KeyDown);
            // 
            // panel16
            // 
            this.panel16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel16.Controls.Add(this.MeeSM9);
            this.panel16.Controls.Add(this.label127);
            this.panel16.Controls.Add(this.LueSize9);
            this.panel16.Controls.Add(this.label67);
            this.panel16.Controls.Add(this.TxtCnt9);
            this.panel16.Controls.Add(this.label24);
            this.panel16.Controls.Add(this.TxtSeal9);
            this.panel16.Controls.Add(this.label29);
            this.panel16.Controls.Add(this.TxtTotal9);
            this.panel16.Controls.Add(this.label49);
            this.panel16.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel16.Location = new System.Drawing.Point(0, 0);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(815, 32);
            this.panel16.TabIndex = 44;
            // 
            // MeeSM9
            // 
            this.MeeSM9.EnterMoveNextControl = true;
            this.MeeSM9.Location = new System.Drawing.Point(691, 6);
            this.MeeSM9.Margin = new System.Windows.Forms.Padding(5);
            this.MeeSM9.Name = "MeeSM9";
            this.MeeSM9.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM9.Properties.Appearance.Options.UseFont = true;
            this.MeeSM9.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM9.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeSM9.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM9.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeSM9.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM9.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeSM9.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM9.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeSM9.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeSM9.Properties.MaxLength = 1000;
            this.MeeSM9.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeSM9.Properties.ShowIcon = false;
            this.MeeSM9.Size = new System.Drawing.Size(120, 20);
            this.MeeSM9.TabIndex = 150;
            this.MeeSM9.ToolTip = "F4 : Show/hide text";
            this.MeeSM9.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeSM9.ToolTipTitle = "Run System";
            // 
            // label127
            // 
            this.label127.AutoSize = true;
            this.label127.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label127.Location = new System.Drawing.Point(666, 9);
            this.label127.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label127.Name = "label127";
            this.label127.Size = new System.Drawing.Size(23, 14);
            this.label127.TabIndex = 149;
            this.label127.Text = "SM";
            this.label127.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueSize9
            // 
            this.LueSize9.EnterMoveNextControl = true;
            this.LueSize9.Location = new System.Drawing.Point(32, 5);
            this.LueSize9.Margin = new System.Windows.Forms.Padding(5);
            this.LueSize9.Name = "LueSize9";
            this.LueSize9.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize9.Properties.Appearance.Options.UseFont = true;
            this.LueSize9.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize9.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSize9.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize9.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSize9.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize9.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSize9.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize9.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSize9.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSize9.Properties.DropDownRows = 20;
            this.LueSize9.Properties.NullText = "[Empty]";
            this.LueSize9.Properties.PopupWidth = 300;
            this.LueSize9.Size = new System.Drawing.Size(67, 20);
            this.LueSize9.TabIndex = 142;
            this.LueSize9.ToolTip = "F4 : Show/hide list";
            this.LueSize9.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSize9.EditValueChanged += new System.EventHandler(this.LueSize9_EditValueChanged);
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label67.ForeColor = System.Drawing.Color.Red;
            this.label67.Location = new System.Drawing.Point(4, 7);
            this.label67.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(28, 14);
            this.label67.TabIndex = 141;
            this.label67.Text = "Size";
            this.label67.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCnt9
            // 
            this.TxtCnt9.EditValue = "";
            this.TxtCnt9.EnterMoveNextControl = true;
            this.TxtCnt9.Location = new System.Drawing.Point(179, 6);
            this.TxtCnt9.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCnt9.Name = "TxtCnt9";
            this.TxtCnt9.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCnt9.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCnt9.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCnt9.Properties.Appearance.Options.UseFont = true;
            this.TxtCnt9.Size = new System.Drawing.Size(151, 20);
            this.TxtCnt9.TabIndex = 144;
            this.TxtCnt9.Validated += new System.EventHandler(this.TxtCnt9_Validated);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Red;
            this.label24.Location = new System.Drawing.Point(99, 9);
            this.label24.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(78, 14);
            this.label24.TabIndex = 143;
            this.label24.Text = "Container No";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSeal9
            // 
            this.TxtSeal9.EditValue = "";
            this.TxtSeal9.EnterMoveNextControl = true;
            this.TxtSeal9.Location = new System.Drawing.Point(385, 6);
            this.TxtSeal9.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSeal9.Name = "TxtSeal9";
            this.TxtSeal9.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSeal9.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSeal9.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSeal9.Properties.Appearance.Options.UseFont = true;
            this.TxtSeal9.Size = new System.Drawing.Size(155, 20);
            this.TxtSeal9.TabIndex = 146;
            this.TxtSeal9.Validated += new System.EventHandler(this.TxtSeal9_Validated);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Black;
            this.label29.Location = new System.Drawing.Point(539, 9);
            this.label29.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(35, 14);
            this.label29.TabIndex = 147;
            this.label29.Text = "Total";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal9
            // 
            this.TxtTotal9.EditValue = "";
            this.TxtTotal9.EnterMoveNextControl = true;
            this.TxtTotal9.Location = new System.Drawing.Point(576, 6);
            this.TxtTotal9.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal9.Name = "TxtTotal9";
            this.TxtTotal9.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal9.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal9.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal9.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal9.Size = new System.Drawing.Size(90, 20);
            this.TxtTotal9.TabIndex = 148;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.ForeColor = System.Drawing.Color.Red;
            this.label49.Location = new System.Drawing.Point(333, 9);
            this.label49.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(48, 14);
            this.label49.TabIndex = 145;
            this.label49.Text = "Seal No";
            this.label49.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage10
            // 
            this.tabPage10.Controls.Add(this.LueItCode10);
            this.tabPage10.Controls.Add(this.Grd20);
            this.tabPage10.Controls.Add(this.panel17);
            this.tabPage10.Location = new System.Drawing.Point(4, 23);
            this.tabPage10.Name = "tabPage10";
            this.tabPage10.Size = new System.Drawing.Size(815, 100);
            this.tabPage10.TabIndex = 9;
            this.tabPage10.Text = "No.10";
            this.tabPage10.UseVisualStyleBackColor = true;
            // 
            // LueItCode10
            // 
            this.LueItCode10.EnterMoveNextControl = true;
            this.LueItCode10.Location = new System.Drawing.Point(67, 57);
            this.LueItCode10.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode10.Name = "LueItCode10";
            this.LueItCode10.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode10.Properties.Appearance.Options.UseFont = true;
            this.LueItCode10.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode10.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode10.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode10.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode10.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode10.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode10.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode10.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode10.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode10.Properties.DropDownRows = 20;
            this.LueItCode10.Properties.NullText = "[Empty]";
            this.LueItCode10.Properties.PopupWidth = 500;
            this.LueItCode10.Size = new System.Drawing.Size(152, 20);
            this.LueItCode10.TabIndex = 54;
            this.LueItCode10.ToolTip = "F4 : Show/hide list";
            this.LueItCode10.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueItCode10.EditValueChanged += new System.EventHandler(this.LueItCode10_EditValueChanged);
            this.LueItCode10.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueItCode10_KeyDown);
            this.LueItCode10.Leave += new System.EventHandler(this.LueItCode10_Leave);
            // 
            // Grd20
            // 
            this.Grd20.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd20.DefaultRow.Height = 20;
            this.Grd20.DefaultRow.Sortable = false;
            this.Grd20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd20.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd20.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd20.Header.Height = 21;
            this.Grd20.Location = new System.Drawing.Point(0, 33);
            this.Grd20.Name = "Grd20";
            this.Grd20.RowHeader.Visible = true;
            this.Grd20.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd20.SingleClickEdit = true;
            this.Grd20.Size = new System.Drawing.Size(815, 67);
            this.Grd20.TabIndex = 162;
            this.Grd20.TreeCol = null;
            this.Grd20.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd20.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd20_ColHdrDoubleClick);
            this.Grd20.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd20_EllipsisButtonClick);
            this.Grd20.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd20_RequestEdit);
            this.Grd20.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd20_AfterCommitEdit);
            this.Grd20.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd20_KeyDown);
            // 
            // panel17
            // 
            this.panel17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel17.Controls.Add(this.MeeSM10);
            this.panel17.Controls.Add(this.label128);
            this.panel17.Controls.Add(this.LueSize10);
            this.panel17.Controls.Add(this.label68);
            this.panel17.Controls.Add(this.TxtCnt10);
            this.panel17.Controls.Add(this.label26);
            this.panel17.Controls.Add(this.TxtSeal10);
            this.panel17.Controls.Add(this.label31);
            this.panel17.Controls.Add(this.TxtTotal10);
            this.panel17.Controls.Add(this.label50);
            this.panel17.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel17.Location = new System.Drawing.Point(0, 0);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(815, 33);
            this.panel17.TabIndex = 44;
            // 
            // MeeSM10
            // 
            this.MeeSM10.EnterMoveNextControl = true;
            this.MeeSM10.Location = new System.Drawing.Point(692, 5);
            this.MeeSM10.Margin = new System.Windows.Forms.Padding(5);
            this.MeeSM10.Name = "MeeSM10";
            this.MeeSM10.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM10.Properties.Appearance.Options.UseFont = true;
            this.MeeSM10.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM10.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeSM10.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM10.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeSM10.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM10.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeSM10.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM10.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeSM10.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeSM10.Properties.MaxLength = 1000;
            this.MeeSM10.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeSM10.Properties.ShowIcon = false;
            this.MeeSM10.Size = new System.Drawing.Size(120, 20);
            this.MeeSM10.TabIndex = 161;
            this.MeeSM10.ToolTip = "F4 : Show/hide text";
            this.MeeSM10.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeSM10.ToolTipTitle = "Run System";
            // 
            // label128
            // 
            this.label128.AutoSize = true;
            this.label128.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label128.Location = new System.Drawing.Point(667, 8);
            this.label128.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label128.Name = "label128";
            this.label128.Size = new System.Drawing.Size(23, 14);
            this.label128.TabIndex = 160;
            this.label128.Text = "SM";
            this.label128.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueSize10
            // 
            this.LueSize10.EnterMoveNextControl = true;
            this.LueSize10.Location = new System.Drawing.Point(32, 7);
            this.LueSize10.Margin = new System.Windows.Forms.Padding(5);
            this.LueSize10.Name = "LueSize10";
            this.LueSize10.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize10.Properties.Appearance.Options.UseFont = true;
            this.LueSize10.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize10.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSize10.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize10.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSize10.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize10.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSize10.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize10.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSize10.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSize10.Properties.DropDownRows = 20;
            this.LueSize10.Properties.NullText = "[Empty]";
            this.LueSize10.Properties.PopupWidth = 300;
            this.LueSize10.Size = new System.Drawing.Size(67, 20);
            this.LueSize10.TabIndex = 153;
            this.LueSize10.ToolTip = "F4 : Show/hide list";
            this.LueSize10.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSize10.EditValueChanged += new System.EventHandler(this.LueSize10_EditValueChanged);
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label68.ForeColor = System.Drawing.Color.Red;
            this.label68.Location = new System.Drawing.Point(4, 9);
            this.label68.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(28, 14);
            this.label68.TabIndex = 152;
            this.label68.Text = "Size";
            this.label68.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCnt10
            // 
            this.TxtCnt10.EditValue = "";
            this.TxtCnt10.EnterMoveNextControl = true;
            this.TxtCnt10.Location = new System.Drawing.Point(182, 6);
            this.TxtCnt10.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCnt10.Name = "TxtCnt10";
            this.TxtCnt10.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCnt10.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCnt10.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCnt10.Properties.Appearance.Options.UseFont = true;
            this.TxtCnt10.Size = new System.Drawing.Size(169, 20);
            this.TxtCnt10.TabIndex = 155;
            this.TxtCnt10.Validated += new System.EventHandler(this.TxtCnt10_Validated);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Red;
            this.label26.Location = new System.Drawing.Point(102, 9);
            this.label26.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(78, 14);
            this.label26.TabIndex = 154;
            this.label26.Text = "Container No";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSeal10
            // 
            this.TxtSeal10.EditValue = "";
            this.TxtSeal10.EnterMoveNextControl = true;
            this.TxtSeal10.Location = new System.Drawing.Point(403, 6);
            this.TxtSeal10.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSeal10.Name = "TxtSeal10";
            this.TxtSeal10.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSeal10.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSeal10.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSeal10.Properties.Appearance.Options.UseFont = true;
            this.TxtSeal10.Size = new System.Drawing.Size(137, 20);
            this.TxtSeal10.TabIndex = 157;
            this.TxtSeal10.Validated += new System.EventHandler(this.TxtSeal10_Validated);
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.Black;
            this.label31.Location = new System.Drawing.Point(537, 8);
            this.label31.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(35, 14);
            this.label31.TabIndex = 158;
            this.label31.Text = "Total";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal10
            // 
            this.TxtTotal10.EditValue = "";
            this.TxtTotal10.EnterMoveNextControl = true;
            this.TxtTotal10.Location = new System.Drawing.Point(574, 5);
            this.TxtTotal10.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal10.Name = "TxtTotal10";
            this.TxtTotal10.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal10.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal10.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal10.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal10.Size = new System.Drawing.Size(90, 20);
            this.TxtTotal10.TabIndex = 159;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.ForeColor = System.Drawing.Color.Red;
            this.label50.Location = new System.Drawing.Point(351, 9);
            this.label50.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(48, 14);
            this.label50.TabIndex = 156;
            this.label50.Text = "Seal No";
            this.label50.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage11
            // 
            this.tabPage11.Controls.Add(this.LueItCode11);
            this.tabPage11.Controls.Add(this.Grd21);
            this.tabPage11.Controls.Add(this.panel5);
            this.tabPage11.Location = new System.Drawing.Point(4, 23);
            this.tabPage11.Name = "tabPage11";
            this.tabPage11.Size = new System.Drawing.Size(815, 100);
            this.tabPage11.TabIndex = 10;
            this.tabPage11.Text = "No.11";
            this.tabPage11.UseVisualStyleBackColor = true;
            // 
            // LueItCode11
            // 
            this.LueItCode11.EnterMoveNextControl = true;
            this.LueItCode11.Location = new System.Drawing.Point(68, 55);
            this.LueItCode11.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode11.Name = "LueItCode11";
            this.LueItCode11.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode11.Properties.Appearance.Options.UseFont = true;
            this.LueItCode11.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode11.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode11.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode11.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode11.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode11.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode11.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode11.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode11.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode11.Properties.DropDownRows = 20;
            this.LueItCode11.Properties.NullText = "[Empty]";
            this.LueItCode11.Properties.PopupWidth = 500;
            this.LueItCode11.Size = new System.Drawing.Size(152, 20);
            this.LueItCode11.TabIndex = 54;
            this.LueItCode11.ToolTip = "F4 : Show/hide list";
            this.LueItCode11.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueItCode11.EditValueChanged += new System.EventHandler(this.LueItCode11_EditValueChanged);
            this.LueItCode11.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueItCode11_KeyDown);
            this.LueItCode11.Leave += new System.EventHandler(this.LueItCode11_Leave);
            // 
            // Grd21
            // 
            this.Grd21.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd21.DefaultRow.Height = 20;
            this.Grd21.DefaultRow.Sortable = false;
            this.Grd21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd21.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd21.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd21.Header.Height = 21;
            this.Grd21.Location = new System.Drawing.Point(0, 33);
            this.Grd21.Name = "Grd21";
            this.Grd21.RowHeader.Visible = true;
            this.Grd21.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd21.SingleClickEdit = true;
            this.Grd21.Size = new System.Drawing.Size(815, 67);
            this.Grd21.TabIndex = 173;
            this.Grd21.TreeCol = null;
            this.Grd21.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd21.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd21_ColHdrDoubleClick);
            this.Grd21.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd21_EllipsisButtonClick);
            this.Grd21.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd21_RequestEdit);
            this.Grd21.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd21_AfterCommitEdit);
            this.Grd21.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd21_KeyDown);
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel5.Controls.Add(this.MeeSM11);
            this.panel5.Controls.Add(this.label129);
            this.panel5.Controls.Add(this.LueSize11);
            this.panel5.Controls.Add(this.label69);
            this.panel5.Controls.Add(this.TxtCnt11);
            this.panel5.Controls.Add(this.label30);
            this.panel5.Controls.Add(this.TxtSeal11);
            this.panel5.Controls.Add(this.label32);
            this.panel5.Controls.Add(this.TxtTotal11);
            this.panel5.Controls.Add(this.label51);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(815, 33);
            this.panel5.TabIndex = 44;
            // 
            // MeeSM11
            // 
            this.MeeSM11.EnterMoveNextControl = true;
            this.MeeSM11.Location = new System.Drawing.Point(690, 6);
            this.MeeSM11.Margin = new System.Windows.Forms.Padding(5);
            this.MeeSM11.Name = "MeeSM11";
            this.MeeSM11.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM11.Properties.Appearance.Options.UseFont = true;
            this.MeeSM11.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM11.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeSM11.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM11.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeSM11.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM11.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeSM11.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM11.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeSM11.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeSM11.Properties.MaxLength = 1000;
            this.MeeSM11.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeSM11.Properties.ShowIcon = false;
            this.MeeSM11.Size = new System.Drawing.Size(120, 20);
            this.MeeSM11.TabIndex = 172;
            this.MeeSM11.ToolTip = "F4 : Show/hide text";
            this.MeeSM11.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeSM11.ToolTipTitle = "Run System";
            // 
            // label129
            // 
            this.label129.AutoSize = true;
            this.label129.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label129.Location = new System.Drawing.Point(665, 9);
            this.label129.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label129.Name = "label129";
            this.label129.Size = new System.Drawing.Size(23, 14);
            this.label129.TabIndex = 171;
            this.label129.Text = "SM";
            this.label129.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueSize11
            // 
            this.LueSize11.EnterMoveNextControl = true;
            this.LueSize11.Location = new System.Drawing.Point(34, 6);
            this.LueSize11.Margin = new System.Windows.Forms.Padding(5);
            this.LueSize11.Name = "LueSize11";
            this.LueSize11.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize11.Properties.Appearance.Options.UseFont = true;
            this.LueSize11.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize11.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSize11.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize11.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSize11.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize11.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSize11.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize11.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSize11.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSize11.Properties.DropDownRows = 20;
            this.LueSize11.Properties.NullText = "[Empty]";
            this.LueSize11.Properties.PopupWidth = 300;
            this.LueSize11.Size = new System.Drawing.Size(67, 20);
            this.LueSize11.TabIndex = 164;
            this.LueSize11.ToolTip = "F4 : Show/hide list";
            this.LueSize11.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSize11.EditValueChanged += new System.EventHandler(this.LueSize11_EditValueChanged);
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label69.ForeColor = System.Drawing.Color.Red;
            this.label69.Location = new System.Drawing.Point(6, 8);
            this.label69.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(28, 14);
            this.label69.TabIndex = 163;
            this.label69.Text = "Size";
            this.label69.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCnt11
            // 
            this.TxtCnt11.EditValue = "";
            this.TxtCnt11.EnterMoveNextControl = true;
            this.TxtCnt11.Location = new System.Drawing.Point(183, 5);
            this.TxtCnt11.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCnt11.Name = "TxtCnt11";
            this.TxtCnt11.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCnt11.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCnt11.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCnt11.Properties.Appearance.Options.UseFont = true;
            this.TxtCnt11.Size = new System.Drawing.Size(157, 20);
            this.TxtCnt11.TabIndex = 166;
            this.TxtCnt11.Validated += new System.EventHandler(this.TxtCnt11_Validated);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.Red;
            this.label30.Location = new System.Drawing.Point(103, 8);
            this.label30.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(78, 14);
            this.label30.TabIndex = 165;
            this.label30.Text = "Container No";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSeal11
            // 
            this.TxtSeal11.EditValue = "";
            this.TxtSeal11.EnterMoveNextControl = true;
            this.TxtSeal11.Location = new System.Drawing.Point(394, 5);
            this.TxtSeal11.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSeal11.Name = "TxtSeal11";
            this.TxtSeal11.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSeal11.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSeal11.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSeal11.Properties.Appearance.Options.UseFont = true;
            this.TxtSeal11.Size = new System.Drawing.Size(141, 20);
            this.TxtSeal11.TabIndex = 168;
            this.TxtSeal11.Validated += new System.EventHandler(this.TxtSeal11_Validated);
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.Black;
            this.label32.Location = new System.Drawing.Point(537, 8);
            this.label32.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(35, 14);
            this.label32.TabIndex = 169;
            this.label32.Text = "Total";
            this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal11
            // 
            this.TxtTotal11.EditValue = "";
            this.TxtTotal11.EnterMoveNextControl = true;
            this.TxtTotal11.Location = new System.Drawing.Point(574, 5);
            this.TxtTotal11.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal11.Name = "TxtTotal11";
            this.TxtTotal11.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal11.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal11.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal11.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal11.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotal11.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotal11.Size = new System.Drawing.Size(90, 20);
            this.TxtTotal11.TabIndex = 170;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.ForeColor = System.Drawing.Color.Red;
            this.label51.Location = new System.Drawing.Point(342, 8);
            this.label51.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(48, 14);
            this.label51.TabIndex = 167;
            this.label51.Text = "Seal No";
            this.label51.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage12
            // 
            this.tabPage12.Controls.Add(this.LueItCode12);
            this.tabPage12.Controls.Add(this.Grd22);
            this.tabPage12.Controls.Add(this.panel18);
            this.tabPage12.Location = new System.Drawing.Point(4, 23);
            this.tabPage12.Name = "tabPage12";
            this.tabPage12.Size = new System.Drawing.Size(815, 100);
            this.tabPage12.TabIndex = 11;
            this.tabPage12.Text = "No.12";
            this.tabPage12.UseVisualStyleBackColor = true;
            // 
            // LueItCode12
            // 
            this.LueItCode12.EnterMoveNextControl = true;
            this.LueItCode12.Location = new System.Drawing.Point(79, 59);
            this.LueItCode12.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode12.Name = "LueItCode12";
            this.LueItCode12.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode12.Properties.Appearance.Options.UseFont = true;
            this.LueItCode12.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode12.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode12.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode12.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode12.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode12.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode12.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode12.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode12.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode12.Properties.DropDownRows = 20;
            this.LueItCode12.Properties.NullText = "[Empty]";
            this.LueItCode12.Properties.PopupWidth = 500;
            this.LueItCode12.Size = new System.Drawing.Size(152, 20);
            this.LueItCode12.TabIndex = 54;
            this.LueItCode12.ToolTip = "F4 : Show/hide list";
            this.LueItCode12.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueItCode12.EditValueChanged += new System.EventHandler(this.LueItCode12_EditValueChanged);
            this.LueItCode12.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueItCode12_KeyDown);
            this.LueItCode12.Leave += new System.EventHandler(this.LueItCode12_Leave);
            // 
            // Grd22
            // 
            this.Grd22.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd22.DefaultRow.Height = 20;
            this.Grd22.DefaultRow.Sortable = false;
            this.Grd22.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd22.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd22.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd22.Header.Height = 21;
            this.Grd22.Location = new System.Drawing.Point(0, 38);
            this.Grd22.Name = "Grd22";
            this.Grd22.RowHeader.Visible = true;
            this.Grd22.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd22.SingleClickEdit = true;
            this.Grd22.Size = new System.Drawing.Size(815, 62);
            this.Grd22.TabIndex = 184;
            this.Grd22.TreeCol = null;
            this.Grd22.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd22.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd22_ColHdrDoubleClick);
            this.Grd22.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd22_EllipsisButtonClick);
            this.Grd22.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd22_RequestEdit);
            this.Grd22.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd22_AfterCommitEdit);
            this.Grd22.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd22_KeyDown);
            // 
            // panel18
            // 
            this.panel18.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel18.Controls.Add(this.MeeSM12);
            this.panel18.Controls.Add(this.label130);
            this.panel18.Controls.Add(this.LueSize12);
            this.panel18.Controls.Add(this.label70);
            this.panel18.Controls.Add(this.TxtCnt12);
            this.panel18.Controls.Add(this.label33);
            this.panel18.Controls.Add(this.TxtSeal12);
            this.panel18.Controls.Add(this.label34);
            this.panel18.Controls.Add(this.TxtTotal12);
            this.panel18.Controls.Add(this.label52);
            this.panel18.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel18.Location = new System.Drawing.Point(0, 0);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(815, 38);
            this.panel18.TabIndex = 44;
            // 
            // MeeSM12
            // 
            this.MeeSM12.EnterMoveNextControl = true;
            this.MeeSM12.Location = new System.Drawing.Point(694, 8);
            this.MeeSM12.Margin = new System.Windows.Forms.Padding(5);
            this.MeeSM12.Name = "MeeSM12";
            this.MeeSM12.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM12.Properties.Appearance.Options.UseFont = true;
            this.MeeSM12.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM12.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeSM12.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM12.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeSM12.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM12.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeSM12.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM12.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeSM12.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeSM12.Properties.MaxLength = 1000;
            this.MeeSM12.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeSM12.Properties.ShowIcon = false;
            this.MeeSM12.Size = new System.Drawing.Size(120, 20);
            this.MeeSM12.TabIndex = 183;
            this.MeeSM12.ToolTip = "F4 : Show/hide text";
            this.MeeSM12.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeSM12.ToolTipTitle = "Run System";
            // 
            // label130
            // 
            this.label130.AutoSize = true;
            this.label130.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label130.Location = new System.Drawing.Point(669, 11);
            this.label130.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label130.Name = "label130";
            this.label130.Size = new System.Drawing.Size(23, 14);
            this.label130.TabIndex = 182;
            this.label130.Text = "SM";
            this.label130.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueSize12
            // 
            this.LueSize12.EnterMoveNextControl = true;
            this.LueSize12.Location = new System.Drawing.Point(33, 9);
            this.LueSize12.Margin = new System.Windows.Forms.Padding(5);
            this.LueSize12.Name = "LueSize12";
            this.LueSize12.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize12.Properties.Appearance.Options.UseFont = true;
            this.LueSize12.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize12.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSize12.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize12.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSize12.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize12.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSize12.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize12.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSize12.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSize12.Properties.DropDownRows = 20;
            this.LueSize12.Properties.NullText = "[Empty]";
            this.LueSize12.Properties.PopupWidth = 300;
            this.LueSize12.Size = new System.Drawing.Size(67, 20);
            this.LueSize12.TabIndex = 175;
            this.LueSize12.ToolTip = "F4 : Show/hide list";
            this.LueSize12.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSize12.EditValueChanged += new System.EventHandler(this.LueSize12_EditValueChanged);
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label70.ForeColor = System.Drawing.Color.Red;
            this.label70.Location = new System.Drawing.Point(5, 11);
            this.label70.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(28, 14);
            this.label70.TabIndex = 174;
            this.label70.Text = "Size";
            this.label70.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCnt12
            // 
            this.TxtCnt12.EditValue = "";
            this.TxtCnt12.EnterMoveNextControl = true;
            this.TxtCnt12.Location = new System.Drawing.Point(183, 8);
            this.TxtCnt12.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCnt12.Name = "TxtCnt12";
            this.TxtCnt12.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCnt12.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCnt12.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCnt12.Properties.Appearance.Options.UseFont = true;
            this.TxtCnt12.Size = new System.Drawing.Size(151, 20);
            this.TxtCnt12.TabIndex = 177;
            this.TxtCnt12.Validated += new System.EventHandler(this.TxtCnt12_Validated);
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.Red;
            this.label33.Location = new System.Drawing.Point(103, 11);
            this.label33.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(78, 14);
            this.label33.TabIndex = 176;
            this.label33.Text = "Container No";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSeal12
            // 
            this.TxtSeal12.EditValue = "";
            this.TxtSeal12.EnterMoveNextControl = true;
            this.TxtSeal12.Location = new System.Drawing.Point(387, 8);
            this.TxtSeal12.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSeal12.Name = "TxtSeal12";
            this.TxtSeal12.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSeal12.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSeal12.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSeal12.Properties.Appearance.Options.UseFont = true;
            this.TxtSeal12.Size = new System.Drawing.Size(149, 20);
            this.TxtSeal12.TabIndex = 179;
            this.TxtSeal12.Validated += new System.EventHandler(this.TxtSeal12_Validated);
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.Black;
            this.label34.Location = new System.Drawing.Point(542, 11);
            this.label34.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(35, 14);
            this.label34.TabIndex = 180;
            this.label34.Text = "Total";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal12
            // 
            this.TxtTotal12.EditValue = "";
            this.TxtTotal12.EnterMoveNextControl = true;
            this.TxtTotal12.Location = new System.Drawing.Point(579, 8);
            this.TxtTotal12.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal12.Name = "TxtTotal12";
            this.TxtTotal12.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal12.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal12.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal12.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal12.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotal12.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotal12.Size = new System.Drawing.Size(90, 20);
            this.TxtTotal12.TabIndex = 181;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.ForeColor = System.Drawing.Color.Red;
            this.label52.Location = new System.Drawing.Point(335, 11);
            this.label52.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(48, 14);
            this.label52.TabIndex = 178;
            this.label52.Text = "Seal No";
            this.label52.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage13
            // 
            this.tabPage13.Controls.Add(this.LueItCode13);
            this.tabPage13.Controls.Add(this.Grd23);
            this.tabPage13.Controls.Add(this.panel19);
            this.tabPage13.Location = new System.Drawing.Point(4, 23);
            this.tabPage13.Name = "tabPage13";
            this.tabPage13.Size = new System.Drawing.Size(815, 100);
            this.tabPage13.TabIndex = 12;
            this.tabPage13.Text = "No.13";
            this.tabPage13.UseVisualStyleBackColor = true;
            // 
            // LueItCode13
            // 
            this.LueItCode13.EnterMoveNextControl = true;
            this.LueItCode13.Location = new System.Drawing.Point(70, 56);
            this.LueItCode13.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode13.Name = "LueItCode13";
            this.LueItCode13.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode13.Properties.Appearance.Options.UseFont = true;
            this.LueItCode13.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode13.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode13.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode13.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode13.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode13.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode13.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode13.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode13.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode13.Properties.DropDownRows = 20;
            this.LueItCode13.Properties.NullText = "[Empty]";
            this.LueItCode13.Properties.PopupWidth = 500;
            this.LueItCode13.Size = new System.Drawing.Size(152, 20);
            this.LueItCode13.TabIndex = 54;
            this.LueItCode13.ToolTip = "F4 : Show/hide list";
            this.LueItCode13.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueItCode13.EditValueChanged += new System.EventHandler(this.LueItCode13_EditValueChanged);
            this.LueItCode13.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueItCode13_KeyDown);
            this.LueItCode13.Leave += new System.EventHandler(this.LueItCode13_Leave);
            // 
            // Grd23
            // 
            this.Grd23.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd23.DefaultRow.Height = 20;
            this.Grd23.DefaultRow.Sortable = false;
            this.Grd23.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd23.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd23.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd23.Header.Height = 21;
            this.Grd23.Location = new System.Drawing.Point(0, 33);
            this.Grd23.Name = "Grd23";
            this.Grd23.RowHeader.Visible = true;
            this.Grd23.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd23.SingleClickEdit = true;
            this.Grd23.Size = new System.Drawing.Size(815, 67);
            this.Grd23.TabIndex = 51;
            this.Grd23.TreeCol = null;
            this.Grd23.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd23.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd23_ColHdrDoubleClick);
            this.Grd23.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd23_EllipsisButtonClick);
            this.Grd23.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd23_RequestEdit);
            this.Grd23.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd23_AfterCommitEdit);
            this.Grd23.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd23_KeyDown);
            // 
            // panel19
            // 
            this.panel19.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel19.Controls.Add(this.MeeSM13);
            this.panel19.Controls.Add(this.label131);
            this.panel19.Controls.Add(this.LueSize13);
            this.panel19.Controls.Add(this.label71);
            this.panel19.Controls.Add(this.TxtCnt13);
            this.panel19.Controls.Add(this.label35);
            this.panel19.Controls.Add(this.TxtSeal13);
            this.panel19.Controls.Add(this.label36);
            this.panel19.Controls.Add(this.TxtTotal13);
            this.panel19.Controls.Add(this.label53);
            this.panel19.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel19.Location = new System.Drawing.Point(0, 0);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(815, 33);
            this.panel19.TabIndex = 44;
            // 
            // MeeSM13
            // 
            this.MeeSM13.EnterMoveNextControl = true;
            this.MeeSM13.Location = new System.Drawing.Point(693, 6);
            this.MeeSM13.Margin = new System.Windows.Forms.Padding(5);
            this.MeeSM13.Name = "MeeSM13";
            this.MeeSM13.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM13.Properties.Appearance.Options.UseFont = true;
            this.MeeSM13.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM13.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeSM13.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM13.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeSM13.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM13.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeSM13.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM13.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeSM13.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeSM13.Properties.MaxLength = 1000;
            this.MeeSM13.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeSM13.Properties.ShowIcon = false;
            this.MeeSM13.Size = new System.Drawing.Size(120, 20);
            this.MeeSM13.TabIndex = 119;
            this.MeeSM13.ToolTip = "F4 : Show/hide text";
            this.MeeSM13.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeSM13.ToolTipTitle = "Run System";
            // 
            // label131
            // 
            this.label131.AutoSize = true;
            this.label131.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label131.Location = new System.Drawing.Point(668, 9);
            this.label131.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label131.Name = "label131";
            this.label131.Size = new System.Drawing.Size(23, 14);
            this.label131.TabIndex = 118;
            this.label131.Text = "SM";
            this.label131.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueSize13
            // 
            this.LueSize13.EnterMoveNextControl = true;
            this.LueSize13.Location = new System.Drawing.Point(33, 6);
            this.LueSize13.Margin = new System.Windows.Forms.Padding(5);
            this.LueSize13.Name = "LueSize13";
            this.LueSize13.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize13.Properties.Appearance.Options.UseFont = true;
            this.LueSize13.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize13.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSize13.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize13.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSize13.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize13.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSize13.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize13.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSize13.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSize13.Properties.DropDownRows = 20;
            this.LueSize13.Properties.NullText = "[Empty]";
            this.LueSize13.Properties.PopupWidth = 300;
            this.LueSize13.Size = new System.Drawing.Size(67, 20);
            this.LueSize13.TabIndex = 117;
            this.LueSize13.ToolTip = "F4 : Show/hide list";
            this.LueSize13.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSize13.EditValueChanged += new System.EventHandler(this.LueSize13_EditValueChanged);
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label71.ForeColor = System.Drawing.Color.Red;
            this.label71.Location = new System.Drawing.Point(5, 8);
            this.label71.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(28, 14);
            this.label71.TabIndex = 116;
            this.label71.Text = "Size";
            this.label71.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCnt13
            // 
            this.TxtCnt13.EditValue = "";
            this.TxtCnt13.EnterMoveNextControl = true;
            this.TxtCnt13.Location = new System.Drawing.Point(183, 5);
            this.TxtCnt13.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCnt13.Name = "TxtCnt13";
            this.TxtCnt13.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCnt13.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCnt13.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCnt13.Properties.Appearance.Options.UseFont = true;
            this.TxtCnt13.Size = new System.Drawing.Size(153, 20);
            this.TxtCnt13.TabIndex = 57;
            this.TxtCnt13.Validated += new System.EventHandler(this.TxtCnt13_Validated);
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.Color.Red;
            this.label35.Location = new System.Drawing.Point(103, 8);
            this.label35.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(78, 14);
            this.label35.TabIndex = 56;
            this.label35.Text = "Container No";
            this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSeal13
            // 
            this.TxtSeal13.EditValue = "";
            this.TxtSeal13.EnterMoveNextControl = true;
            this.TxtSeal13.Location = new System.Drawing.Point(390, 5);
            this.TxtSeal13.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSeal13.Name = "TxtSeal13";
            this.TxtSeal13.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSeal13.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSeal13.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSeal13.Properties.Appearance.Options.UseFont = true;
            this.TxtSeal13.Size = new System.Drawing.Size(150, 20);
            this.TxtSeal13.TabIndex = 55;
            this.TxtSeal13.Validated += new System.EventHandler(this.TxtSeal13_Validated);
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.ForeColor = System.Drawing.Color.Black;
            this.label36.Location = new System.Drawing.Point(539, 8);
            this.label36.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(35, 14);
            this.label36.TabIndex = 53;
            this.label36.Text = "Total";
            this.label36.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal13
            // 
            this.TxtTotal13.EditValue = "";
            this.TxtTotal13.EnterMoveNextControl = true;
            this.TxtTotal13.Location = new System.Drawing.Point(576, 5);
            this.TxtTotal13.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal13.Name = "TxtTotal13";
            this.TxtTotal13.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal13.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal13.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal13.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal13.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotal13.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotal13.Size = new System.Drawing.Size(90, 20);
            this.TxtTotal13.TabIndex = 54;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.ForeColor = System.Drawing.Color.Red;
            this.label53.Location = new System.Drawing.Point(338, 8);
            this.label53.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(48, 14);
            this.label53.TabIndex = 52;
            this.label53.Text = "Seal No";
            this.label53.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage14
            // 
            this.tabPage14.Controls.Add(this.LueItCode14);
            this.tabPage14.Controls.Add(this.Grd24);
            this.tabPage14.Controls.Add(this.panel20);
            this.tabPage14.Location = new System.Drawing.Point(4, 23);
            this.tabPage14.Name = "tabPage14";
            this.tabPage14.Size = new System.Drawing.Size(815, 100);
            this.tabPage14.TabIndex = 13;
            this.tabPage14.Text = "No.14";
            this.tabPage14.UseVisualStyleBackColor = true;
            // 
            // LueItCode14
            // 
            this.LueItCode14.EnterMoveNextControl = true;
            this.LueItCode14.Location = new System.Drawing.Point(69, 55);
            this.LueItCode14.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode14.Name = "LueItCode14";
            this.LueItCode14.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode14.Properties.Appearance.Options.UseFont = true;
            this.LueItCode14.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode14.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode14.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode14.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode14.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode14.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode14.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode14.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode14.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode14.Properties.DropDownRows = 20;
            this.LueItCode14.Properties.NullText = "[Empty]";
            this.LueItCode14.Properties.PopupWidth = 500;
            this.LueItCode14.Size = new System.Drawing.Size(152, 20);
            this.LueItCode14.TabIndex = 54;
            this.LueItCode14.ToolTip = "F4 : Show/hide list";
            this.LueItCode14.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueItCode14.EditValueChanged += new System.EventHandler(this.LueItCode14_EditValueChanged);
            this.LueItCode14.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueItCode14_KeyDown);
            this.LueItCode14.Leave += new System.EventHandler(this.LueItCode14_Leave);
            // 
            // Grd24
            // 
            this.Grd24.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd24.DefaultRow.Height = 20;
            this.Grd24.DefaultRow.Sortable = false;
            this.Grd24.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd24.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd24.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd24.Header.Height = 21;
            this.Grd24.Location = new System.Drawing.Point(0, 33);
            this.Grd24.Name = "Grd24";
            this.Grd24.RowHeader.Visible = true;
            this.Grd24.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd24.SingleClickEdit = true;
            this.Grd24.Size = new System.Drawing.Size(815, 67);
            this.Grd24.TabIndex = 51;
            this.Grd24.TreeCol = null;
            this.Grd24.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd24.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd24_ColHdrDoubleClick);
            this.Grd24.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd24_EllipsisButtonClick);
            this.Grd24.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd24_RequestEdit);
            this.Grd24.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd24_AfterCommitEdit);
            this.Grd24.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd24_KeyDown);
            // 
            // panel20
            // 
            this.panel20.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel20.Controls.Add(this.MeeSM14);
            this.panel20.Controls.Add(this.label132);
            this.panel20.Controls.Add(this.LueSize14);
            this.panel20.Controls.Add(this.label72);
            this.panel20.Controls.Add(this.TxtCnt14);
            this.panel20.Controls.Add(this.label37);
            this.panel20.Controls.Add(this.TxtSeal14);
            this.panel20.Controls.Add(this.label38);
            this.panel20.Controls.Add(this.TxtTotal14);
            this.panel20.Controls.Add(this.label54);
            this.panel20.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel20.Location = new System.Drawing.Point(0, 0);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(815, 33);
            this.panel20.TabIndex = 44;
            // 
            // MeeSM14
            // 
            this.MeeSM14.EnterMoveNextControl = true;
            this.MeeSM14.Location = new System.Drawing.Point(690, 6);
            this.MeeSM14.Margin = new System.Windows.Forms.Padding(5);
            this.MeeSM14.Name = "MeeSM14";
            this.MeeSM14.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM14.Properties.Appearance.Options.UseFont = true;
            this.MeeSM14.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM14.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeSM14.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM14.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeSM14.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM14.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeSM14.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM14.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeSM14.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeSM14.Properties.MaxLength = 1000;
            this.MeeSM14.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeSM14.Properties.ShowIcon = false;
            this.MeeSM14.Size = new System.Drawing.Size(120, 20);
            this.MeeSM14.TabIndex = 121;
            this.MeeSM14.ToolTip = "F4 : Show/hide text";
            this.MeeSM14.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeSM14.ToolTipTitle = "Run System";
            // 
            // label132
            // 
            this.label132.AutoSize = true;
            this.label132.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label132.Location = new System.Drawing.Point(665, 9);
            this.label132.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label132.Name = "label132";
            this.label132.Size = new System.Drawing.Size(23, 14);
            this.label132.TabIndex = 120;
            this.label132.Text = "SM";
            this.label132.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueSize14
            // 
            this.LueSize14.EnterMoveNextControl = true;
            this.LueSize14.Location = new System.Drawing.Point(34, 7);
            this.LueSize14.Margin = new System.Windows.Forms.Padding(5);
            this.LueSize14.Name = "LueSize14";
            this.LueSize14.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize14.Properties.Appearance.Options.UseFont = true;
            this.LueSize14.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize14.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSize14.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize14.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSize14.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize14.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSize14.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize14.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSize14.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSize14.Properties.DropDownRows = 20;
            this.LueSize14.Properties.NullText = "[Empty]";
            this.LueSize14.Properties.PopupWidth = 300;
            this.LueSize14.Size = new System.Drawing.Size(67, 20);
            this.LueSize14.TabIndex = 119;
            this.LueSize14.ToolTip = "F4 : Show/hide list";
            this.LueSize14.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSize14.EditValueChanged += new System.EventHandler(this.LueSize14_EditValueChanged);
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label72.ForeColor = System.Drawing.Color.Red;
            this.label72.Location = new System.Drawing.Point(6, 9);
            this.label72.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(28, 14);
            this.label72.TabIndex = 118;
            this.label72.Text = "Size";
            this.label72.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCnt14
            // 
            this.TxtCnt14.EditValue = "";
            this.TxtCnt14.EnterMoveNextControl = true;
            this.TxtCnt14.Location = new System.Drawing.Point(181, 7);
            this.TxtCnt14.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCnt14.Name = "TxtCnt14";
            this.TxtCnt14.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCnt14.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCnt14.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCnt14.Properties.Appearance.Options.UseFont = true;
            this.TxtCnt14.Size = new System.Drawing.Size(153, 20);
            this.TxtCnt14.TabIndex = 57;
            this.TxtCnt14.Validated += new System.EventHandler(this.TxtCnt14_Validated);
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.ForeColor = System.Drawing.Color.Red;
            this.label37.Location = new System.Drawing.Point(101, 10);
            this.label37.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(78, 14);
            this.label37.TabIndex = 56;
            this.label37.Text = "Container No";
            this.label37.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSeal14
            // 
            this.TxtSeal14.EditValue = "";
            this.TxtSeal14.EnterMoveNextControl = true;
            this.TxtSeal14.Location = new System.Drawing.Point(386, 7);
            this.TxtSeal14.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSeal14.Name = "TxtSeal14";
            this.TxtSeal14.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSeal14.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSeal14.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSeal14.Properties.Appearance.Options.UseFont = true;
            this.TxtSeal14.Size = new System.Drawing.Size(153, 20);
            this.TxtSeal14.TabIndex = 55;
            this.TxtSeal14.Validated += new System.EventHandler(this.TxtSeal14_Validated);
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.ForeColor = System.Drawing.Color.Black;
            this.label38.Location = new System.Drawing.Point(539, 9);
            this.label38.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(35, 14);
            this.label38.TabIndex = 53;
            this.label38.Text = "Total";
            this.label38.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal14
            // 
            this.TxtTotal14.EditValue = "";
            this.TxtTotal14.EnterMoveNextControl = true;
            this.TxtTotal14.Location = new System.Drawing.Point(576, 6);
            this.TxtTotal14.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal14.Name = "TxtTotal14";
            this.TxtTotal14.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal14.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal14.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal14.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal14.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotal14.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotal14.Size = new System.Drawing.Size(90, 20);
            this.TxtTotal14.TabIndex = 54;
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.ForeColor = System.Drawing.Color.Red;
            this.label54.Location = new System.Drawing.Point(334, 10);
            this.label54.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(48, 14);
            this.label54.TabIndex = 52;
            this.label54.Text = "Seal No";
            this.label54.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage15
            // 
            this.tabPage15.Controls.Add(this.LueItCode15);
            this.tabPage15.Controls.Add(this.Grd25);
            this.tabPage15.Controls.Add(this.panel21);
            this.tabPage15.Location = new System.Drawing.Point(4, 23);
            this.tabPage15.Name = "tabPage15";
            this.tabPage15.Size = new System.Drawing.Size(815, 100);
            this.tabPage15.TabIndex = 14;
            this.tabPage15.Text = "No.15";
            this.tabPage15.UseVisualStyleBackColor = true;
            // 
            // LueItCode15
            // 
            this.LueItCode15.EnterMoveNextControl = true;
            this.LueItCode15.Location = new System.Drawing.Point(73, 56);
            this.LueItCode15.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode15.Name = "LueItCode15";
            this.LueItCode15.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode15.Properties.Appearance.Options.UseFont = true;
            this.LueItCode15.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode15.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode15.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode15.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode15.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode15.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode15.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode15.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode15.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode15.Properties.DropDownRows = 20;
            this.LueItCode15.Properties.NullText = "[Empty]";
            this.LueItCode15.Properties.PopupWidth = 500;
            this.LueItCode15.Size = new System.Drawing.Size(152, 20);
            this.LueItCode15.TabIndex = 54;
            this.LueItCode15.ToolTip = "F4 : Show/hide list";
            this.LueItCode15.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueItCode15.EditValueChanged += new System.EventHandler(this.LueItCode15_EditValueChanged);
            this.LueItCode15.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueItCode15_KeyDown);
            this.LueItCode15.Leave += new System.EventHandler(this.LueItCode15_Leave);
            // 
            // Grd25
            // 
            this.Grd25.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd25.DefaultRow.Height = 20;
            this.Grd25.DefaultRow.Sortable = false;
            this.Grd25.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd25.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd25.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd25.Header.Height = 21;
            this.Grd25.Location = new System.Drawing.Point(0, 33);
            this.Grd25.Name = "Grd25";
            this.Grd25.RowHeader.Visible = true;
            this.Grd25.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd25.SingleClickEdit = true;
            this.Grd25.Size = new System.Drawing.Size(815, 67);
            this.Grd25.TabIndex = 51;
            this.Grd25.TreeCol = null;
            this.Grd25.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd25.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd25_ColHdrDoubleClick);
            this.Grd25.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd25_EllipsisButtonClick);
            this.Grd25.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd25_RequestEdit);
            this.Grd25.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd25_AfterCommitEdit);
            this.Grd25.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd25_KeyDown);
            // 
            // panel21
            // 
            this.panel21.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel21.Controls.Add(this.MeeSM15);
            this.panel21.Controls.Add(this.label133);
            this.panel21.Controls.Add(this.LueSize15);
            this.panel21.Controls.Add(this.label73);
            this.panel21.Controls.Add(this.TxtCnt15);
            this.panel21.Controls.Add(this.label39);
            this.panel21.Controls.Add(this.TxtSeal15);
            this.panel21.Controls.Add(this.label40);
            this.panel21.Controls.Add(this.TxtTotal15);
            this.panel21.Controls.Add(this.label55);
            this.panel21.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel21.Location = new System.Drawing.Point(0, 0);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(815, 33);
            this.panel21.TabIndex = 44;
            // 
            // MeeSM15
            // 
            this.MeeSM15.EnterMoveNextControl = true;
            this.MeeSM15.Location = new System.Drawing.Point(684, 5);
            this.MeeSM15.Margin = new System.Windows.Forms.Padding(5);
            this.MeeSM15.Name = "MeeSM15";
            this.MeeSM15.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM15.Properties.Appearance.Options.UseFont = true;
            this.MeeSM15.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM15.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeSM15.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM15.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeSM15.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM15.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeSM15.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM15.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeSM15.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeSM15.Properties.MaxLength = 1000;
            this.MeeSM15.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeSM15.Properties.ShowIcon = false;
            this.MeeSM15.Size = new System.Drawing.Size(120, 20);
            this.MeeSM15.TabIndex = 123;
            this.MeeSM15.ToolTip = "F4 : Show/hide text";
            this.MeeSM15.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeSM15.ToolTipTitle = "Run System";
            // 
            // label133
            // 
            this.label133.AutoSize = true;
            this.label133.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label133.Location = new System.Drawing.Point(659, 8);
            this.label133.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label133.Name = "label133";
            this.label133.Size = new System.Drawing.Size(23, 14);
            this.label133.TabIndex = 122;
            this.label133.Text = "SM";
            this.label133.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueSize15
            // 
            this.LueSize15.EnterMoveNextControl = true;
            this.LueSize15.Location = new System.Drawing.Point(35, 6);
            this.LueSize15.Margin = new System.Windows.Forms.Padding(5);
            this.LueSize15.Name = "LueSize15";
            this.LueSize15.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize15.Properties.Appearance.Options.UseFont = true;
            this.LueSize15.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize15.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSize15.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize15.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSize15.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize15.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSize15.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize15.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSize15.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSize15.Properties.DropDownRows = 20;
            this.LueSize15.Properties.NullText = "[Empty]";
            this.LueSize15.Properties.PopupWidth = 300;
            this.LueSize15.Size = new System.Drawing.Size(67, 20);
            this.LueSize15.TabIndex = 121;
            this.LueSize15.ToolTip = "F4 : Show/hide list";
            this.LueSize15.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSize15.EditValueChanged += new System.EventHandler(this.LueSize15_EditValueChanged);
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label73.ForeColor = System.Drawing.Color.Red;
            this.label73.Location = new System.Drawing.Point(7, 8);
            this.label73.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(28, 14);
            this.label73.TabIndex = 120;
            this.label73.Text = "Size";
            this.label73.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCnt15
            // 
            this.TxtCnt15.EditValue = "";
            this.TxtCnt15.EnterMoveNextControl = true;
            this.TxtCnt15.Location = new System.Drawing.Point(186, 6);
            this.TxtCnt15.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCnt15.Name = "TxtCnt15";
            this.TxtCnt15.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCnt15.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCnt15.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCnt15.Properties.Appearance.Options.UseFont = true;
            this.TxtCnt15.Size = new System.Drawing.Size(149, 20);
            this.TxtCnt15.TabIndex = 57;
            this.TxtCnt15.Validated += new System.EventHandler(this.TxtCnt15_Validated);
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.ForeColor = System.Drawing.Color.Red;
            this.label39.Location = new System.Drawing.Point(106, 9);
            this.label39.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(78, 14);
            this.label39.TabIndex = 56;
            this.label39.Text = "Container No";
            this.label39.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSeal15
            // 
            this.TxtSeal15.EditValue = "";
            this.TxtSeal15.EnterMoveNextControl = true;
            this.TxtSeal15.Location = new System.Drawing.Point(389, 6);
            this.TxtSeal15.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSeal15.Name = "TxtSeal15";
            this.TxtSeal15.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSeal15.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSeal15.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSeal15.Properties.Appearance.Options.UseFont = true;
            this.TxtSeal15.Size = new System.Drawing.Size(139, 20);
            this.TxtSeal15.TabIndex = 55;
            this.TxtSeal15.Validated += new System.EventHandler(this.TxtSeal15_Validated);
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.ForeColor = System.Drawing.Color.Black;
            this.label40.Location = new System.Drawing.Point(529, 8);
            this.label40.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(35, 14);
            this.label40.TabIndex = 53;
            this.label40.Text = "Total";
            this.label40.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal15
            // 
            this.TxtTotal15.EditValue = "";
            this.TxtTotal15.EnterMoveNextControl = true;
            this.TxtTotal15.Location = new System.Drawing.Point(566, 5);
            this.TxtTotal15.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal15.Name = "TxtTotal15";
            this.TxtTotal15.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal15.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal15.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal15.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal15.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotal15.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotal15.Size = new System.Drawing.Size(90, 20);
            this.TxtTotal15.TabIndex = 54;
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.ForeColor = System.Drawing.Color.Red;
            this.label55.Location = new System.Drawing.Point(337, 9);
            this.label55.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(48, 14);
            this.label55.TabIndex = 52;
            this.label55.Text = "Seal No";
            this.label55.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage16
            // 
            this.tabPage16.Controls.Add(this.LueItCode16);
            this.tabPage16.Controls.Add(this.Grd26);
            this.tabPage16.Controls.Add(this.panel3);
            this.tabPage16.Location = new System.Drawing.Point(4, 23);
            this.tabPage16.Name = "tabPage16";
            this.tabPage16.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage16.Size = new System.Drawing.Size(815, 100);
            this.tabPage16.TabIndex = 15;
            this.tabPage16.Text = "No.16";
            this.tabPage16.UseVisualStyleBackColor = true;
            // 
            // LueItCode16
            // 
            this.LueItCode16.EnterMoveNextControl = true;
            this.LueItCode16.Location = new System.Drawing.Point(88, 59);
            this.LueItCode16.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode16.Name = "LueItCode16";
            this.LueItCode16.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode16.Properties.Appearance.Options.UseFont = true;
            this.LueItCode16.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode16.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode16.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode16.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode16.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode16.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode16.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode16.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode16.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode16.Properties.DropDownRows = 20;
            this.LueItCode16.Properties.NullText = "[Empty]";
            this.LueItCode16.Properties.PopupWidth = 500;
            this.LueItCode16.Size = new System.Drawing.Size(152, 20);
            this.LueItCode16.TabIndex = 55;
            this.LueItCode16.ToolTip = "F4 : Show/hide list";
            this.LueItCode16.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueItCode16.EditValueChanged += new System.EventHandler(this.LueItCode16_EditValueChanged);
            this.LueItCode16.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueItCode16_KeyDown);
            this.LueItCode16.Leave += new System.EventHandler(this.LueItCode16_Leave);
            // 
            // Grd26
            // 
            this.Grd26.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd26.DefaultRow.Height = 20;
            this.Grd26.DefaultRow.Sortable = false;
            this.Grd26.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd26.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd26.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd26.Header.Height = 21;
            this.Grd26.Location = new System.Drawing.Point(3, 36);
            this.Grd26.Name = "Grd26";
            this.Grd26.RowHeader.Visible = true;
            this.Grd26.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd26.SingleClickEdit = true;
            this.Grd26.Size = new System.Drawing.Size(809, 61);
            this.Grd26.TabIndex = 52;
            this.Grd26.TreeCol = null;
            this.Grd26.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd26.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd26_ColHdrDoubleClick);
            this.Grd26.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd26_EllipsisButtonClick);
            this.Grd26.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd26_RequestEdit);
            this.Grd26.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd26_AfterCommitEdit);
            this.Grd26.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd26_KeyDown);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel3.Controls.Add(this.MeeSM16);
            this.panel3.Controls.Add(this.label134);
            this.panel3.Controls.Add(this.LueSize16);
            this.panel3.Controls.Add(this.label77);
            this.panel3.Controls.Add(this.TxtCnt16);
            this.panel3.Controls.Add(this.label78);
            this.panel3.Controls.Add(this.TxtSeal16);
            this.panel3.Controls.Add(this.label79);
            this.panel3.Controls.Add(this.TxtTotal16);
            this.panel3.Controls.Add(this.label80);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(3, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(809, 33);
            this.panel3.TabIndex = 45;
            // 
            // MeeSM16
            // 
            this.MeeSM16.EnterMoveNextControl = true;
            this.MeeSM16.Location = new System.Drawing.Point(686, 5);
            this.MeeSM16.Margin = new System.Windows.Forms.Padding(5);
            this.MeeSM16.Name = "MeeSM16";
            this.MeeSM16.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM16.Properties.Appearance.Options.UseFont = true;
            this.MeeSM16.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM16.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeSM16.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM16.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeSM16.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM16.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeSM16.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM16.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeSM16.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeSM16.Properties.MaxLength = 1000;
            this.MeeSM16.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeSM16.Properties.ShowIcon = false;
            this.MeeSM16.Size = new System.Drawing.Size(120, 20);
            this.MeeSM16.TabIndex = 123;
            this.MeeSM16.ToolTip = "F4 : Show/hide text";
            this.MeeSM16.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeSM16.ToolTipTitle = "Run System";
            // 
            // label134
            // 
            this.label134.AutoSize = true;
            this.label134.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label134.Location = new System.Drawing.Point(661, 8);
            this.label134.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label134.Name = "label134";
            this.label134.Size = new System.Drawing.Size(23, 14);
            this.label134.TabIndex = 122;
            this.label134.Text = "SM";
            this.label134.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueSize16
            // 
            this.LueSize16.EnterMoveNextControl = true;
            this.LueSize16.Location = new System.Drawing.Point(35, 6);
            this.LueSize16.Margin = new System.Windows.Forms.Padding(5);
            this.LueSize16.Name = "LueSize16";
            this.LueSize16.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize16.Properties.Appearance.Options.UseFont = true;
            this.LueSize16.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize16.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSize16.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize16.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSize16.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize16.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSize16.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize16.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSize16.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSize16.Properties.DropDownRows = 20;
            this.LueSize16.Properties.NullText = "[Empty]";
            this.LueSize16.Properties.PopupWidth = 300;
            this.LueSize16.Size = new System.Drawing.Size(67, 20);
            this.LueSize16.TabIndex = 121;
            this.LueSize16.ToolTip = "F4 : Show/hide list";
            this.LueSize16.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSize16.EditValueChanged += new System.EventHandler(this.LueSize16_EditValueChanged);
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label77.ForeColor = System.Drawing.Color.Red;
            this.label77.Location = new System.Drawing.Point(7, 8);
            this.label77.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(28, 14);
            this.label77.TabIndex = 120;
            this.label77.Text = "Size";
            this.label77.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCnt16
            // 
            this.TxtCnt16.EditValue = "";
            this.TxtCnt16.EnterMoveNextControl = true;
            this.TxtCnt16.Location = new System.Drawing.Point(186, 6);
            this.TxtCnt16.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCnt16.Name = "TxtCnt16";
            this.TxtCnt16.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCnt16.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCnt16.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCnt16.Properties.Appearance.Options.UseFont = true;
            this.TxtCnt16.Size = new System.Drawing.Size(148, 20);
            this.TxtCnt16.TabIndex = 57;
            this.TxtCnt16.Validated += new System.EventHandler(this.TxtCnt16_Validated);
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label78.ForeColor = System.Drawing.Color.Red;
            this.label78.Location = new System.Drawing.Point(106, 9);
            this.label78.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(78, 14);
            this.label78.TabIndex = 56;
            this.label78.Text = "Container No";
            this.label78.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSeal16
            // 
            this.TxtSeal16.EditValue = "";
            this.TxtSeal16.EnterMoveNextControl = true;
            this.TxtSeal16.Location = new System.Drawing.Point(387, 6);
            this.TxtSeal16.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSeal16.Name = "TxtSeal16";
            this.TxtSeal16.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSeal16.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSeal16.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSeal16.Properties.Appearance.Options.UseFont = true;
            this.TxtSeal16.Size = new System.Drawing.Size(144, 20);
            this.TxtSeal16.TabIndex = 55;
            this.TxtSeal16.Validated += new System.EventHandler(this.TxtSeal16_Validated);
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label79.ForeColor = System.Drawing.Color.Black;
            this.label79.Location = new System.Drawing.Point(534, 8);
            this.label79.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(35, 14);
            this.label79.TabIndex = 53;
            this.label79.Text = "Total";
            this.label79.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal16
            // 
            this.TxtTotal16.EditValue = "";
            this.TxtTotal16.EnterMoveNextControl = true;
            this.TxtTotal16.Location = new System.Drawing.Point(571, 5);
            this.TxtTotal16.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal16.Name = "TxtTotal16";
            this.TxtTotal16.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal16.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal16.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal16.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal16.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotal16.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotal16.Size = new System.Drawing.Size(90, 20);
            this.TxtTotal16.TabIndex = 54;
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label80.ForeColor = System.Drawing.Color.Red;
            this.label80.Location = new System.Drawing.Point(335, 9);
            this.label80.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(48, 14);
            this.label80.TabIndex = 52;
            this.label80.Text = "Seal No";
            this.label80.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage17
            // 
            this.tabPage17.Controls.Add(this.LueItCode17);
            this.tabPage17.Controls.Add(this.Grd27);
            this.tabPage17.Controls.Add(this.panel22);
            this.tabPage17.Location = new System.Drawing.Point(4, 23);
            this.tabPage17.Name = "tabPage17";
            this.tabPage17.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage17.Size = new System.Drawing.Size(815, 100);
            this.tabPage17.TabIndex = 16;
            this.tabPage17.Text = "No.17";
            this.tabPage17.UseVisualStyleBackColor = true;
            // 
            // LueItCode17
            // 
            this.LueItCode17.EnterMoveNextControl = true;
            this.LueItCode17.Location = new System.Drawing.Point(91, 61);
            this.LueItCode17.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode17.Name = "LueItCode17";
            this.LueItCode17.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode17.Properties.Appearance.Options.UseFont = true;
            this.LueItCode17.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode17.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode17.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode17.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode17.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode17.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode17.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode17.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode17.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode17.Properties.DropDownRows = 20;
            this.LueItCode17.Properties.NullText = "[Empty]";
            this.LueItCode17.Properties.PopupWidth = 500;
            this.LueItCode17.Size = new System.Drawing.Size(152, 20);
            this.LueItCode17.TabIndex = 55;
            this.LueItCode17.ToolTip = "F4 : Show/hide list";
            this.LueItCode17.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueItCode17.EditValueChanged += new System.EventHandler(this.LueItCode17_EditValueChanged);
            this.LueItCode17.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueItCode17_KeyDown);
            this.LueItCode17.Leave += new System.EventHandler(this.LueItCode17_Leave);
            // 
            // Grd27
            // 
            this.Grd27.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd27.DefaultRow.Height = 20;
            this.Grd27.DefaultRow.Sortable = false;
            this.Grd27.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd27.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd27.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd27.Header.Height = 21;
            this.Grd27.Location = new System.Drawing.Point(3, 36);
            this.Grd27.Name = "Grd27";
            this.Grd27.RowHeader.Visible = true;
            this.Grd27.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd27.SingleClickEdit = true;
            this.Grd27.Size = new System.Drawing.Size(809, 61);
            this.Grd27.TabIndex = 53;
            this.Grd27.TreeCol = null;
            this.Grd27.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd27.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd27_ColHdrDoubleClick);
            this.Grd27.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd27_EllipsisButtonClick);
            this.Grd27.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd27_RequestEdit);
            this.Grd27.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd27_AfterCommitEdit);
            this.Grd27.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd27_KeyDown);
            // 
            // panel22
            // 
            this.panel22.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel22.Controls.Add(this.MeeSM17);
            this.panel22.Controls.Add(this.label135);
            this.panel22.Controls.Add(this.LueSize17);
            this.panel22.Controls.Add(this.label81);
            this.panel22.Controls.Add(this.TxtCnt17);
            this.panel22.Controls.Add(this.label82);
            this.panel22.Controls.Add(this.TxtSeal17);
            this.panel22.Controls.Add(this.label83);
            this.panel22.Controls.Add(this.TxtTotal17);
            this.panel22.Controls.Add(this.label84);
            this.panel22.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel22.Location = new System.Drawing.Point(3, 3);
            this.panel22.Name = "panel22";
            this.panel22.Size = new System.Drawing.Size(809, 33);
            this.panel22.TabIndex = 46;
            // 
            // MeeSM17
            // 
            this.MeeSM17.EnterMoveNextControl = true;
            this.MeeSM17.Location = new System.Drawing.Point(685, 6);
            this.MeeSM17.Margin = new System.Windows.Forms.Padding(5);
            this.MeeSM17.Name = "MeeSM17";
            this.MeeSM17.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM17.Properties.Appearance.Options.UseFont = true;
            this.MeeSM17.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM17.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeSM17.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM17.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeSM17.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM17.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeSM17.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM17.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeSM17.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeSM17.Properties.MaxLength = 1000;
            this.MeeSM17.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeSM17.Properties.ShowIcon = false;
            this.MeeSM17.Size = new System.Drawing.Size(120, 20);
            this.MeeSM17.TabIndex = 123;
            this.MeeSM17.ToolTip = "F4 : Show/hide text";
            this.MeeSM17.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeSM17.ToolTipTitle = "Run System";
            // 
            // label135
            // 
            this.label135.AutoSize = true;
            this.label135.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label135.Location = new System.Drawing.Point(660, 9);
            this.label135.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label135.Name = "label135";
            this.label135.Size = new System.Drawing.Size(23, 14);
            this.label135.TabIndex = 122;
            this.label135.Text = "SM";
            this.label135.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueSize17
            // 
            this.LueSize17.EnterMoveNextControl = true;
            this.LueSize17.Location = new System.Drawing.Point(35, 6);
            this.LueSize17.Margin = new System.Windows.Forms.Padding(5);
            this.LueSize17.Name = "LueSize17";
            this.LueSize17.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize17.Properties.Appearance.Options.UseFont = true;
            this.LueSize17.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize17.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSize17.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize17.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSize17.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize17.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSize17.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize17.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSize17.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSize17.Properties.DropDownRows = 20;
            this.LueSize17.Properties.NullText = "[Empty]";
            this.LueSize17.Properties.PopupWidth = 300;
            this.LueSize17.Size = new System.Drawing.Size(67, 20);
            this.LueSize17.TabIndex = 121;
            this.LueSize17.ToolTip = "F4 : Show/hide list";
            this.LueSize17.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSize17.EditValueChanged += new System.EventHandler(this.LueSize17_EditValueChanged);
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label81.ForeColor = System.Drawing.Color.Red;
            this.label81.Location = new System.Drawing.Point(7, 8);
            this.label81.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(28, 14);
            this.label81.TabIndex = 120;
            this.label81.Text = "Size";
            this.label81.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCnt17
            // 
            this.TxtCnt17.EditValue = "";
            this.TxtCnt17.EnterMoveNextControl = true;
            this.TxtCnt17.Location = new System.Drawing.Point(186, 6);
            this.TxtCnt17.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCnt17.Name = "TxtCnt17";
            this.TxtCnt17.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCnt17.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCnt17.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCnt17.Properties.Appearance.Options.UseFont = true;
            this.TxtCnt17.Size = new System.Drawing.Size(160, 20);
            this.TxtCnt17.TabIndex = 57;
            this.TxtCnt17.Validated += new System.EventHandler(this.TxtCnt17_Validated);
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label82.ForeColor = System.Drawing.Color.Red;
            this.label82.Location = new System.Drawing.Point(106, 9);
            this.label82.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(78, 14);
            this.label82.TabIndex = 56;
            this.label82.Text = "Container No";
            this.label82.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSeal17
            // 
            this.TxtSeal17.EditValue = "";
            this.TxtSeal17.EnterMoveNextControl = true;
            this.TxtSeal17.Location = new System.Drawing.Point(399, 6);
            this.TxtSeal17.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSeal17.Name = "TxtSeal17";
            this.TxtSeal17.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSeal17.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSeal17.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSeal17.Properties.Appearance.Options.UseFont = true;
            this.TxtSeal17.Size = new System.Drawing.Size(135, 20);
            this.TxtSeal17.TabIndex = 55;
            this.TxtSeal17.Validated += new System.EventHandler(this.TxtSeal17_Validated);
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label83.ForeColor = System.Drawing.Color.Black;
            this.label83.Location = new System.Drawing.Point(534, 9);
            this.label83.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(35, 14);
            this.label83.TabIndex = 53;
            this.label83.Text = "Total";
            this.label83.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal17
            // 
            this.TxtTotal17.EditValue = "";
            this.TxtTotal17.EnterMoveNextControl = true;
            this.TxtTotal17.Location = new System.Drawing.Point(571, 6);
            this.TxtTotal17.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal17.Name = "TxtTotal17";
            this.TxtTotal17.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal17.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal17.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal17.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal17.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotal17.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotal17.Size = new System.Drawing.Size(90, 20);
            this.TxtTotal17.TabIndex = 54;
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label84.ForeColor = System.Drawing.Color.Red;
            this.label84.Location = new System.Drawing.Point(347, 9);
            this.label84.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(48, 14);
            this.label84.TabIndex = 52;
            this.label84.Text = "Seal No";
            this.label84.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage18
            // 
            this.tabPage18.Controls.Add(this.LueItCode18);
            this.tabPage18.Controls.Add(this.Grd28);
            this.tabPage18.Controls.Add(this.panel23);
            this.tabPage18.Location = new System.Drawing.Point(4, 23);
            this.tabPage18.Name = "tabPage18";
            this.tabPage18.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage18.Size = new System.Drawing.Size(815, 100);
            this.tabPage18.TabIndex = 17;
            this.tabPage18.Text = "No.18";
            this.tabPage18.UseVisualStyleBackColor = true;
            // 
            // LueItCode18
            // 
            this.LueItCode18.EnterMoveNextControl = true;
            this.LueItCode18.Location = new System.Drawing.Point(195, 57);
            this.LueItCode18.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode18.Name = "LueItCode18";
            this.LueItCode18.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode18.Properties.Appearance.Options.UseFont = true;
            this.LueItCode18.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode18.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode18.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode18.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode18.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode18.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode18.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode18.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode18.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode18.Properties.DropDownRows = 20;
            this.LueItCode18.Properties.NullText = "[Empty]";
            this.LueItCode18.Properties.PopupWidth = 500;
            this.LueItCode18.Size = new System.Drawing.Size(152, 20);
            this.LueItCode18.TabIndex = 55;
            this.LueItCode18.ToolTip = "F4 : Show/hide list";
            this.LueItCode18.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueItCode18.EditValueChanged += new System.EventHandler(this.LueItCode18_EditValueChanged);
            this.LueItCode18.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueItCode18_KeyDown);
            this.LueItCode18.Leave += new System.EventHandler(this.LueItCode18_Leave);
            // 
            // Grd28
            // 
            this.Grd28.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd28.DefaultRow.Height = 20;
            this.Grd28.DefaultRow.Sortable = false;
            this.Grd28.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd28.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd28.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd28.Header.Height = 21;
            this.Grd28.Location = new System.Drawing.Point(3, 36);
            this.Grd28.Name = "Grd28";
            this.Grd28.RowHeader.Visible = true;
            this.Grd28.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd28.SingleClickEdit = true;
            this.Grd28.Size = new System.Drawing.Size(809, 61);
            this.Grd28.TabIndex = 53;
            this.Grd28.TreeCol = null;
            this.Grd28.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd28.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd28_ColHdrDoubleClick);
            this.Grd28.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd28_EllipsisButtonClick);
            this.Grd28.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd28_RequestEdit);
            this.Grd28.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd28_AfterCommitEdit);
            this.Grd28.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd28_KeyDown);
            // 
            // panel23
            // 
            this.panel23.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel23.Controls.Add(this.MeeSM18);
            this.panel23.Controls.Add(this.label136);
            this.panel23.Controls.Add(this.LueSize18);
            this.panel23.Controls.Add(this.label85);
            this.panel23.Controls.Add(this.TxtCnt18);
            this.panel23.Controls.Add(this.label86);
            this.panel23.Controls.Add(this.TxtSeal18);
            this.panel23.Controls.Add(this.label87);
            this.panel23.Controls.Add(this.TxtTotal18);
            this.panel23.Controls.Add(this.label88);
            this.panel23.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel23.Location = new System.Drawing.Point(3, 3);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(809, 33);
            this.panel23.TabIndex = 46;
            // 
            // MeeSM18
            // 
            this.MeeSM18.EnterMoveNextControl = true;
            this.MeeSM18.Location = new System.Drawing.Point(686, 4);
            this.MeeSM18.Margin = new System.Windows.Forms.Padding(5);
            this.MeeSM18.Name = "MeeSM18";
            this.MeeSM18.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM18.Properties.Appearance.Options.UseFont = true;
            this.MeeSM18.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM18.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeSM18.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM18.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeSM18.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM18.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeSM18.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM18.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeSM18.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeSM18.Properties.MaxLength = 1000;
            this.MeeSM18.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeSM18.Properties.ShowIcon = false;
            this.MeeSM18.Size = new System.Drawing.Size(120, 20);
            this.MeeSM18.TabIndex = 123;
            this.MeeSM18.ToolTip = "F4 : Show/hide text";
            this.MeeSM18.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeSM18.ToolTipTitle = "Run System";
            // 
            // label136
            // 
            this.label136.AutoSize = true;
            this.label136.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label136.Location = new System.Drawing.Point(661, 7);
            this.label136.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label136.Name = "label136";
            this.label136.Size = new System.Drawing.Size(23, 14);
            this.label136.TabIndex = 122;
            this.label136.Text = "SM";
            this.label136.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueSize18
            // 
            this.LueSize18.EnterMoveNextControl = true;
            this.LueSize18.Location = new System.Drawing.Point(35, 6);
            this.LueSize18.Margin = new System.Windows.Forms.Padding(5);
            this.LueSize18.Name = "LueSize18";
            this.LueSize18.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize18.Properties.Appearance.Options.UseFont = true;
            this.LueSize18.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize18.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSize18.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize18.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSize18.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize18.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSize18.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize18.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSize18.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSize18.Properties.DropDownRows = 20;
            this.LueSize18.Properties.NullText = "[Empty]";
            this.LueSize18.Properties.PopupWidth = 300;
            this.LueSize18.Size = new System.Drawing.Size(67, 20);
            this.LueSize18.TabIndex = 121;
            this.LueSize18.ToolTip = "F4 : Show/hide list";
            this.LueSize18.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSize18.EditValueChanged += new System.EventHandler(this.LueSize18_EditValueChanged);
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label85.ForeColor = System.Drawing.Color.Red;
            this.label85.Location = new System.Drawing.Point(7, 8);
            this.label85.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(28, 14);
            this.label85.TabIndex = 120;
            this.label85.Text = "Size";
            this.label85.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCnt18
            // 
            this.TxtCnt18.EditValue = "";
            this.TxtCnt18.EnterMoveNextControl = true;
            this.TxtCnt18.Location = new System.Drawing.Point(186, 6);
            this.TxtCnt18.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCnt18.Name = "TxtCnt18";
            this.TxtCnt18.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCnt18.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCnt18.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCnt18.Properties.Appearance.Options.UseFont = true;
            this.TxtCnt18.Size = new System.Drawing.Size(154, 20);
            this.TxtCnt18.TabIndex = 57;
            this.TxtCnt18.Validated += new System.EventHandler(this.TxtCnt18_Validated);
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label86.ForeColor = System.Drawing.Color.Red;
            this.label86.Location = new System.Drawing.Point(106, 9);
            this.label86.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(78, 14);
            this.label86.TabIndex = 56;
            this.label86.Text = "Container No";
            this.label86.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSeal18
            // 
            this.TxtSeal18.EditValue = "";
            this.TxtSeal18.EnterMoveNextControl = true;
            this.TxtSeal18.Location = new System.Drawing.Point(392, 5);
            this.TxtSeal18.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSeal18.Name = "TxtSeal18";
            this.TxtSeal18.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSeal18.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSeal18.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSeal18.Properties.Appearance.Options.UseFont = true;
            this.TxtSeal18.Size = new System.Drawing.Size(138, 20);
            this.TxtSeal18.TabIndex = 55;
            this.TxtSeal18.Validated += new System.EventHandler(this.TxtSeal18_Validated);
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label87.ForeColor = System.Drawing.Color.Black;
            this.label87.Location = new System.Drawing.Point(532, 8);
            this.label87.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(35, 14);
            this.label87.TabIndex = 53;
            this.label87.Text = "Total";
            this.label87.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal18
            // 
            this.TxtTotal18.EditValue = "";
            this.TxtTotal18.EnterMoveNextControl = true;
            this.TxtTotal18.Location = new System.Drawing.Point(569, 5);
            this.TxtTotal18.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal18.Name = "TxtTotal18";
            this.TxtTotal18.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal18.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal18.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal18.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal18.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotal18.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotal18.Size = new System.Drawing.Size(90, 20);
            this.TxtTotal18.TabIndex = 54;
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label88.ForeColor = System.Drawing.Color.Red;
            this.label88.Location = new System.Drawing.Point(340, 8);
            this.label88.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(48, 14);
            this.label88.TabIndex = 52;
            this.label88.Text = "Seal No";
            this.label88.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage19
            // 
            this.tabPage19.Controls.Add(this.LueItCode19);
            this.tabPage19.Controls.Add(this.Grd29);
            this.tabPage19.Controls.Add(this.panel24);
            this.tabPage19.Location = new System.Drawing.Point(4, 23);
            this.tabPage19.Name = "tabPage19";
            this.tabPage19.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage19.Size = new System.Drawing.Size(815, 100);
            this.tabPage19.TabIndex = 18;
            this.tabPage19.Text = "No.19";
            this.tabPage19.UseVisualStyleBackColor = true;
            // 
            // LueItCode19
            // 
            this.LueItCode19.EnterMoveNextControl = true;
            this.LueItCode19.Location = new System.Drawing.Point(195, 64);
            this.LueItCode19.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode19.Name = "LueItCode19";
            this.LueItCode19.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode19.Properties.Appearance.Options.UseFont = true;
            this.LueItCode19.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode19.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode19.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode19.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode19.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode19.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode19.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode19.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode19.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode19.Properties.DropDownRows = 20;
            this.LueItCode19.Properties.NullText = "[Empty]";
            this.LueItCode19.Properties.PopupWidth = 500;
            this.LueItCode19.Size = new System.Drawing.Size(152, 20);
            this.LueItCode19.TabIndex = 55;
            this.LueItCode19.ToolTip = "F4 : Show/hide list";
            this.LueItCode19.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueItCode19.EditValueChanged += new System.EventHandler(this.LueItCode19_EditValueChanged);
            this.LueItCode19.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueItCode19_KeyDown);
            this.LueItCode19.Leave += new System.EventHandler(this.LueItCode19_Leave);
            // 
            // Grd29
            // 
            this.Grd29.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd29.DefaultRow.Height = 20;
            this.Grd29.DefaultRow.Sortable = false;
            this.Grd29.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd29.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd29.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd29.Header.Height = 21;
            this.Grd29.Location = new System.Drawing.Point(3, 36);
            this.Grd29.Name = "Grd29";
            this.Grd29.RowHeader.Visible = true;
            this.Grd29.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd29.SingleClickEdit = true;
            this.Grd29.Size = new System.Drawing.Size(809, 61);
            this.Grd29.TabIndex = 53;
            this.Grd29.TreeCol = null;
            this.Grd29.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd29.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd29_ColHdrDoubleClick);
            this.Grd29.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd29_EllipsisButtonClick);
            this.Grd29.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd29_RequestEdit);
            this.Grd29.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd29_AfterCommitEdit);
            this.Grd29.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd29_KeyDown);
            // 
            // panel24
            // 
            this.panel24.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel24.Controls.Add(this.MeeSM19);
            this.panel24.Controls.Add(this.label137);
            this.panel24.Controls.Add(this.LueSize19);
            this.panel24.Controls.Add(this.label89);
            this.panel24.Controls.Add(this.TxtCnt19);
            this.panel24.Controls.Add(this.label90);
            this.panel24.Controls.Add(this.TxtSeal19);
            this.panel24.Controls.Add(this.label91);
            this.panel24.Controls.Add(this.TxtTotal19);
            this.panel24.Controls.Add(this.label92);
            this.panel24.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel24.Location = new System.Drawing.Point(3, 3);
            this.panel24.Name = "panel24";
            this.panel24.Size = new System.Drawing.Size(809, 33);
            this.panel24.TabIndex = 46;
            // 
            // MeeSM19
            // 
            this.MeeSM19.EnterMoveNextControl = true;
            this.MeeSM19.Location = new System.Drawing.Point(687, 4);
            this.MeeSM19.Margin = new System.Windows.Forms.Padding(5);
            this.MeeSM19.Name = "MeeSM19";
            this.MeeSM19.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM19.Properties.Appearance.Options.UseFont = true;
            this.MeeSM19.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM19.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeSM19.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM19.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeSM19.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM19.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeSM19.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM19.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeSM19.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeSM19.Properties.MaxLength = 1000;
            this.MeeSM19.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeSM19.Properties.ShowIcon = false;
            this.MeeSM19.Size = new System.Drawing.Size(120, 20);
            this.MeeSM19.TabIndex = 123;
            this.MeeSM19.ToolTip = "F4 : Show/hide text";
            this.MeeSM19.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeSM19.ToolTipTitle = "Run System";
            // 
            // label137
            // 
            this.label137.AutoSize = true;
            this.label137.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label137.Location = new System.Drawing.Point(662, 7);
            this.label137.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label137.Name = "label137";
            this.label137.Size = new System.Drawing.Size(23, 14);
            this.label137.TabIndex = 122;
            this.label137.Text = "SM";
            this.label137.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueSize19
            // 
            this.LueSize19.EnterMoveNextControl = true;
            this.LueSize19.Location = new System.Drawing.Point(35, 6);
            this.LueSize19.Margin = new System.Windows.Forms.Padding(5);
            this.LueSize19.Name = "LueSize19";
            this.LueSize19.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize19.Properties.Appearance.Options.UseFont = true;
            this.LueSize19.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize19.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSize19.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize19.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSize19.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize19.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSize19.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize19.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSize19.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSize19.Properties.DropDownRows = 20;
            this.LueSize19.Properties.NullText = "[Empty]";
            this.LueSize19.Properties.PopupWidth = 300;
            this.LueSize19.Size = new System.Drawing.Size(67, 20);
            this.LueSize19.TabIndex = 121;
            this.LueSize19.ToolTip = "F4 : Show/hide list";
            this.LueSize19.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSize19.EditValueChanged += new System.EventHandler(this.LueSize19_EditValueChanged);
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label89.ForeColor = System.Drawing.Color.Red;
            this.label89.Location = new System.Drawing.Point(7, 8);
            this.label89.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(28, 14);
            this.label89.TabIndex = 120;
            this.label89.Text = "Size";
            this.label89.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCnt19
            // 
            this.TxtCnt19.EditValue = "";
            this.TxtCnt19.EnterMoveNextControl = true;
            this.TxtCnt19.Location = new System.Drawing.Point(186, 6);
            this.TxtCnt19.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCnt19.Name = "TxtCnt19";
            this.TxtCnt19.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCnt19.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCnt19.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCnt19.Properties.Appearance.Options.UseFont = true;
            this.TxtCnt19.Size = new System.Drawing.Size(150, 20);
            this.TxtCnt19.TabIndex = 57;
            this.TxtCnt19.Validated += new System.EventHandler(this.TxtCnt19_Validated);
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label90.ForeColor = System.Drawing.Color.Red;
            this.label90.Location = new System.Drawing.Point(106, 9);
            this.label90.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(78, 14);
            this.label90.TabIndex = 56;
            this.label90.Text = "Container No";
            this.label90.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSeal19
            // 
            this.TxtSeal19.EditValue = "";
            this.TxtSeal19.EnterMoveNextControl = true;
            this.TxtSeal19.Location = new System.Drawing.Point(389, 5);
            this.TxtSeal19.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSeal19.Name = "TxtSeal19";
            this.TxtSeal19.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSeal19.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSeal19.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSeal19.Properties.Appearance.Options.UseFont = true;
            this.TxtSeal19.Size = new System.Drawing.Size(145, 20);
            this.TxtSeal19.TabIndex = 55;
            this.TxtSeal19.EditValueChanged += new System.EventHandler(this.TxtSeal19_EditValueChanged);
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label91.ForeColor = System.Drawing.Color.Black;
            this.label91.Location = new System.Drawing.Point(537, 7);
            this.label91.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(35, 14);
            this.label91.TabIndex = 53;
            this.label91.Text = "Total";
            this.label91.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal19
            // 
            this.TxtTotal19.EditValue = "";
            this.TxtTotal19.EnterMoveNextControl = true;
            this.TxtTotal19.Location = new System.Drawing.Point(574, 4);
            this.TxtTotal19.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal19.Name = "TxtTotal19";
            this.TxtTotal19.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal19.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal19.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal19.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal19.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotal19.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotal19.Size = new System.Drawing.Size(89, 20);
            this.TxtTotal19.TabIndex = 54;
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label92.ForeColor = System.Drawing.Color.Red;
            this.label92.Location = new System.Drawing.Point(337, 8);
            this.label92.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(48, 14);
            this.label92.TabIndex = 52;
            this.label92.Text = "Seal No";
            this.label92.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage20
            // 
            this.tabPage20.Controls.Add(this.LueItCode20);
            this.tabPage20.Controls.Add(this.Grd30);
            this.tabPage20.Controls.Add(this.panel25);
            this.tabPage20.Location = new System.Drawing.Point(4, 23);
            this.tabPage20.Name = "tabPage20";
            this.tabPage20.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage20.Size = new System.Drawing.Size(815, 100);
            this.tabPage20.TabIndex = 19;
            this.tabPage20.Text = "No.20";
            this.tabPage20.UseVisualStyleBackColor = true;
            // 
            // LueItCode20
            // 
            this.LueItCode20.EnterMoveNextControl = true;
            this.LueItCode20.Location = new System.Drawing.Point(211, 62);
            this.LueItCode20.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode20.Name = "LueItCode20";
            this.LueItCode20.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode20.Properties.Appearance.Options.UseFont = true;
            this.LueItCode20.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode20.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode20.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode20.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode20.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode20.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode20.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode20.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode20.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode20.Properties.DropDownRows = 20;
            this.LueItCode20.Properties.NullText = "[Empty]";
            this.LueItCode20.Properties.PopupWidth = 500;
            this.LueItCode20.Size = new System.Drawing.Size(152, 20);
            this.LueItCode20.TabIndex = 55;
            this.LueItCode20.ToolTip = "F4 : Show/hide list";
            this.LueItCode20.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueItCode20.EditValueChanged += new System.EventHandler(this.LueItCode20_EditValueChanged);
            this.LueItCode20.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueItCode20_KeyDown);
            this.LueItCode20.Leave += new System.EventHandler(this.LueItCode20_Leave);
            // 
            // Grd30
            // 
            this.Grd30.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd30.DefaultRow.Height = 20;
            this.Grd30.DefaultRow.Sortable = false;
            this.Grd30.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd30.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd30.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd30.Header.Height = 21;
            this.Grd30.Location = new System.Drawing.Point(3, 36);
            this.Grd30.Name = "Grd30";
            this.Grd30.RowHeader.Visible = true;
            this.Grd30.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd30.SingleClickEdit = true;
            this.Grd30.Size = new System.Drawing.Size(809, 61);
            this.Grd30.TabIndex = 53;
            this.Grd30.TreeCol = null;
            this.Grd30.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd30.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd30_ColHdrDoubleClick);
            this.Grd30.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd30_EllipsisButtonClick);
            this.Grd30.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd30_RequestEdit);
            this.Grd30.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd30_AfterCommitEdit);
            this.Grd30.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd30_KeyDown);
            // 
            // panel25
            // 
            this.panel25.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel25.Controls.Add(this.MeeSM20);
            this.panel25.Controls.Add(this.label138);
            this.panel25.Controls.Add(this.LueSize20);
            this.panel25.Controls.Add(this.label93);
            this.panel25.Controls.Add(this.TxtCnt20);
            this.panel25.Controls.Add(this.label94);
            this.panel25.Controls.Add(this.TxtSeal20);
            this.panel25.Controls.Add(this.label95);
            this.panel25.Controls.Add(this.TxtTotal20);
            this.panel25.Controls.Add(this.label96);
            this.panel25.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel25.Location = new System.Drawing.Point(3, 3);
            this.panel25.Name = "panel25";
            this.panel25.Size = new System.Drawing.Size(809, 33);
            this.panel25.TabIndex = 46;
            // 
            // MeeSM20
            // 
            this.MeeSM20.EnterMoveNextControl = true;
            this.MeeSM20.Location = new System.Drawing.Point(692, 5);
            this.MeeSM20.Margin = new System.Windows.Forms.Padding(5);
            this.MeeSM20.Name = "MeeSM20";
            this.MeeSM20.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM20.Properties.Appearance.Options.UseFont = true;
            this.MeeSM20.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM20.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeSM20.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM20.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeSM20.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM20.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeSM20.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM20.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeSM20.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeSM20.Properties.MaxLength = 1000;
            this.MeeSM20.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeSM20.Properties.ShowIcon = false;
            this.MeeSM20.Size = new System.Drawing.Size(120, 20);
            this.MeeSM20.TabIndex = 123;
            this.MeeSM20.ToolTip = "F4 : Show/hide text";
            this.MeeSM20.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeSM20.ToolTipTitle = "Run System";
            // 
            // label138
            // 
            this.label138.AutoSize = true;
            this.label138.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label138.Location = new System.Drawing.Point(667, 8);
            this.label138.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label138.Name = "label138";
            this.label138.Size = new System.Drawing.Size(23, 14);
            this.label138.TabIndex = 122;
            this.label138.Text = "SM";
            this.label138.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueSize20
            // 
            this.LueSize20.EnterMoveNextControl = true;
            this.LueSize20.Location = new System.Drawing.Point(35, 6);
            this.LueSize20.Margin = new System.Windows.Forms.Padding(5);
            this.LueSize20.Name = "LueSize20";
            this.LueSize20.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize20.Properties.Appearance.Options.UseFont = true;
            this.LueSize20.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize20.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSize20.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize20.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSize20.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize20.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSize20.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize20.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSize20.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSize20.Properties.DropDownRows = 20;
            this.LueSize20.Properties.NullText = "[Empty]";
            this.LueSize20.Properties.PopupWidth = 300;
            this.LueSize20.Size = new System.Drawing.Size(67, 20);
            this.LueSize20.TabIndex = 121;
            this.LueSize20.ToolTip = "F4 : Show/hide list";
            this.LueSize20.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSize20.EditValueChanged += new System.EventHandler(this.LueSize20_EditValueChanged);
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label93.ForeColor = System.Drawing.Color.Red;
            this.label93.Location = new System.Drawing.Point(7, 8);
            this.label93.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(28, 14);
            this.label93.TabIndex = 120;
            this.label93.Text = "Size";
            this.label93.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCnt20
            // 
            this.TxtCnt20.EditValue = "";
            this.TxtCnt20.EnterMoveNextControl = true;
            this.TxtCnt20.Location = new System.Drawing.Point(186, 6);
            this.TxtCnt20.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCnt20.Name = "TxtCnt20";
            this.TxtCnt20.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCnt20.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCnt20.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCnt20.Properties.Appearance.Options.UseFont = true;
            this.TxtCnt20.Size = new System.Drawing.Size(157, 20);
            this.TxtCnt20.TabIndex = 57;
            this.TxtCnt20.Validated += new System.EventHandler(this.TxtCnt20_Validated);
            // 
            // label94
            // 
            this.label94.AutoSize = true;
            this.label94.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label94.ForeColor = System.Drawing.Color.Red;
            this.label94.Location = new System.Drawing.Point(106, 9);
            this.label94.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(78, 14);
            this.label94.TabIndex = 56;
            this.label94.Text = "Container No";
            this.label94.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSeal20
            // 
            this.TxtSeal20.EditValue = "";
            this.TxtSeal20.EnterMoveNextControl = true;
            this.TxtSeal20.Location = new System.Drawing.Point(396, 5);
            this.TxtSeal20.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSeal20.Name = "TxtSeal20";
            this.TxtSeal20.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSeal20.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSeal20.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSeal20.Properties.Appearance.Options.UseFont = true;
            this.TxtSeal20.Size = new System.Drawing.Size(143, 20);
            this.TxtSeal20.TabIndex = 55;
            this.TxtSeal20.Validated += new System.EventHandler(this.TxtSeal20_Validated);
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label95.ForeColor = System.Drawing.Color.Black;
            this.label95.Location = new System.Drawing.Point(539, 7);
            this.label95.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(35, 14);
            this.label95.TabIndex = 53;
            this.label95.Text = "Total";
            this.label95.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal20
            // 
            this.TxtTotal20.EditValue = "";
            this.TxtTotal20.EnterMoveNextControl = true;
            this.TxtTotal20.Location = new System.Drawing.Point(576, 5);
            this.TxtTotal20.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal20.Name = "TxtTotal20";
            this.TxtTotal20.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal20.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal20.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal20.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal20.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotal20.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotal20.Size = new System.Drawing.Size(90, 20);
            this.TxtTotal20.TabIndex = 54;
            // 
            // label96
            // 
            this.label96.AutoSize = true;
            this.label96.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label96.ForeColor = System.Drawing.Color.Red;
            this.label96.Location = new System.Drawing.Point(344, 8);
            this.label96.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(48, 14);
            this.label96.TabIndex = 52;
            this.label96.Text = "Seal No";
            this.label96.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage21
            // 
            this.tabPage21.Controls.Add(this.LueItCode21);
            this.tabPage21.Controls.Add(this.Grd31);
            this.tabPage21.Controls.Add(this.panel26);
            this.tabPage21.Location = new System.Drawing.Point(4, 23);
            this.tabPage21.Name = "tabPage21";
            this.tabPage21.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage21.Size = new System.Drawing.Size(815, 100);
            this.tabPage21.TabIndex = 20;
            this.tabPage21.Text = "No.21";
            this.tabPage21.UseVisualStyleBackColor = true;
            // 
            // LueItCode21
            // 
            this.LueItCode21.EnterMoveNextControl = true;
            this.LueItCode21.Location = new System.Drawing.Point(217, 61);
            this.LueItCode21.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode21.Name = "LueItCode21";
            this.LueItCode21.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode21.Properties.Appearance.Options.UseFont = true;
            this.LueItCode21.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode21.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode21.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode21.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode21.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode21.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode21.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode21.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode21.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode21.Properties.DropDownRows = 20;
            this.LueItCode21.Properties.NullText = "[Empty]";
            this.LueItCode21.Properties.PopupWidth = 500;
            this.LueItCode21.Size = new System.Drawing.Size(152, 20);
            this.LueItCode21.TabIndex = 55;
            this.LueItCode21.ToolTip = "F4 : Show/hide list";
            this.LueItCode21.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueItCode21.EditValueChanged += new System.EventHandler(this.LueItCode21_EditValueChanged);
            this.LueItCode21.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueItCode21_KeyDown);
            this.LueItCode21.Leave += new System.EventHandler(this.LueItCode21_Leave);
            // 
            // Grd31
            // 
            this.Grd31.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd31.DefaultRow.Height = 20;
            this.Grd31.DefaultRow.Sortable = false;
            this.Grd31.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd31.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd31.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd31.Header.Height = 21;
            this.Grd31.Location = new System.Drawing.Point(3, 36);
            this.Grd31.Name = "Grd31";
            this.Grd31.RowHeader.Visible = true;
            this.Grd31.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd31.SingleClickEdit = true;
            this.Grd31.Size = new System.Drawing.Size(809, 61);
            this.Grd31.TabIndex = 53;
            this.Grd31.TreeCol = null;
            this.Grd31.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd31.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd31_ColHdrDoubleClick);
            this.Grd31.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd31_EllipsisButtonClick);
            this.Grd31.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd31_RequestEdit);
            this.Grd31.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd31_AfterCommitEdit);
            this.Grd31.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd31_KeyDown);
            // 
            // panel26
            // 
            this.panel26.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel26.Controls.Add(this.MeeSM21);
            this.panel26.Controls.Add(this.label139);
            this.panel26.Controls.Add(this.LueSize21);
            this.panel26.Controls.Add(this.label97);
            this.panel26.Controls.Add(this.TxtCnt21);
            this.panel26.Controls.Add(this.label98);
            this.panel26.Controls.Add(this.TxtSeal21);
            this.panel26.Controls.Add(this.label99);
            this.panel26.Controls.Add(this.TxtTotal21);
            this.panel26.Controls.Add(this.label100);
            this.panel26.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel26.Location = new System.Drawing.Point(3, 3);
            this.panel26.Name = "panel26";
            this.panel26.Size = new System.Drawing.Size(809, 33);
            this.panel26.TabIndex = 46;
            // 
            // MeeSM21
            // 
            this.MeeSM21.EnterMoveNextControl = true;
            this.MeeSM21.Location = new System.Drawing.Point(683, 6);
            this.MeeSM21.Margin = new System.Windows.Forms.Padding(5);
            this.MeeSM21.Name = "MeeSM21";
            this.MeeSM21.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM21.Properties.Appearance.Options.UseFont = true;
            this.MeeSM21.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM21.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeSM21.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM21.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeSM21.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM21.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeSM21.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM21.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeSM21.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeSM21.Properties.MaxLength = 1000;
            this.MeeSM21.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeSM21.Properties.ShowIcon = false;
            this.MeeSM21.Size = new System.Drawing.Size(120, 20);
            this.MeeSM21.TabIndex = 123;
            this.MeeSM21.ToolTip = "F4 : Show/hide text";
            this.MeeSM21.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeSM21.ToolTipTitle = "Run System";
            // 
            // label139
            // 
            this.label139.AutoSize = true;
            this.label139.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label139.Location = new System.Drawing.Point(658, 9);
            this.label139.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label139.Name = "label139";
            this.label139.Size = new System.Drawing.Size(23, 14);
            this.label139.TabIndex = 122;
            this.label139.Text = "SM";
            this.label139.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueSize21
            // 
            this.LueSize21.EnterMoveNextControl = true;
            this.LueSize21.Location = new System.Drawing.Point(35, 6);
            this.LueSize21.Margin = new System.Windows.Forms.Padding(5);
            this.LueSize21.Name = "LueSize21";
            this.LueSize21.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize21.Properties.Appearance.Options.UseFont = true;
            this.LueSize21.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize21.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSize21.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize21.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSize21.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize21.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSize21.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize21.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSize21.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSize21.Properties.DropDownRows = 20;
            this.LueSize21.Properties.NullText = "[Empty]";
            this.LueSize21.Properties.PopupWidth = 300;
            this.LueSize21.Size = new System.Drawing.Size(67, 20);
            this.LueSize21.TabIndex = 121;
            this.LueSize21.ToolTip = "F4 : Show/hide list";
            this.LueSize21.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSize21.EditValueChanged += new System.EventHandler(this.LueSize21_EditValueChanged);
            // 
            // label97
            // 
            this.label97.AutoSize = true;
            this.label97.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label97.ForeColor = System.Drawing.Color.Red;
            this.label97.Location = new System.Drawing.Point(7, 8);
            this.label97.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(28, 14);
            this.label97.TabIndex = 120;
            this.label97.Text = "Size";
            this.label97.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCnt21
            // 
            this.TxtCnt21.EditValue = "";
            this.TxtCnt21.EnterMoveNextControl = true;
            this.TxtCnt21.Location = new System.Drawing.Point(186, 6);
            this.TxtCnt21.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCnt21.Name = "TxtCnt21";
            this.TxtCnt21.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCnt21.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCnt21.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCnt21.Properties.Appearance.Options.UseFont = true;
            this.TxtCnt21.Size = new System.Drawing.Size(142, 20);
            this.TxtCnt21.TabIndex = 57;
            this.TxtCnt21.Validated += new System.EventHandler(this.TxtCnt21_Validated);
            // 
            // label98
            // 
            this.label98.AutoSize = true;
            this.label98.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label98.ForeColor = System.Drawing.Color.Red;
            this.label98.Location = new System.Drawing.Point(106, 9);
            this.label98.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(78, 14);
            this.label98.TabIndex = 56;
            this.label98.Text = "Container No";
            this.label98.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSeal21
            // 
            this.TxtSeal21.EditValue = "";
            this.TxtSeal21.EnterMoveNextControl = true;
            this.TxtSeal21.Location = new System.Drawing.Point(381, 6);
            this.TxtSeal21.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSeal21.Name = "TxtSeal21";
            this.TxtSeal21.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSeal21.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSeal21.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSeal21.Properties.Appearance.Options.UseFont = true;
            this.TxtSeal21.Size = new System.Drawing.Size(144, 20);
            this.TxtSeal21.TabIndex = 55;
            this.TxtSeal21.Validated += new System.EventHandler(this.TxtSeal21_Validated);
            // 
            // label99
            // 
            this.label99.AutoSize = true;
            this.label99.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label99.ForeColor = System.Drawing.Color.Black;
            this.label99.Location = new System.Drawing.Point(525, 9);
            this.label99.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(35, 14);
            this.label99.TabIndex = 53;
            this.label99.Text = "Total";
            this.label99.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal21
            // 
            this.TxtTotal21.EditValue = "";
            this.TxtTotal21.EnterMoveNextControl = true;
            this.TxtTotal21.Location = new System.Drawing.Point(562, 6);
            this.TxtTotal21.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal21.Name = "TxtTotal21";
            this.TxtTotal21.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal21.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal21.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal21.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal21.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotal21.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotal21.Size = new System.Drawing.Size(90, 20);
            this.TxtTotal21.TabIndex = 54;
            // 
            // label100
            // 
            this.label100.AutoSize = true;
            this.label100.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label100.ForeColor = System.Drawing.Color.Red;
            this.label100.Location = new System.Drawing.Point(329, 9);
            this.label100.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(48, 14);
            this.label100.TabIndex = 52;
            this.label100.Text = "Seal No";
            this.label100.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage22
            // 
            this.tabPage22.Controls.Add(this.LueItCode22);
            this.tabPage22.Controls.Add(this.Grd32);
            this.tabPage22.Controls.Add(this.panel27);
            this.tabPage22.Location = new System.Drawing.Point(4, 23);
            this.tabPage22.Name = "tabPage22";
            this.tabPage22.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage22.Size = new System.Drawing.Size(815, 100);
            this.tabPage22.TabIndex = 21;
            this.tabPage22.Text = "No.22";
            this.tabPage22.UseVisualStyleBackColor = true;
            // 
            // LueItCode22
            // 
            this.LueItCode22.EnterMoveNextControl = true;
            this.LueItCode22.Location = new System.Drawing.Point(197, 62);
            this.LueItCode22.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode22.Name = "LueItCode22";
            this.LueItCode22.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode22.Properties.Appearance.Options.UseFont = true;
            this.LueItCode22.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode22.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode22.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode22.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode22.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode22.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode22.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode22.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode22.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode22.Properties.DropDownRows = 20;
            this.LueItCode22.Properties.NullText = "[Empty]";
            this.LueItCode22.Properties.PopupWidth = 500;
            this.LueItCode22.Size = new System.Drawing.Size(152, 20);
            this.LueItCode22.TabIndex = 55;
            this.LueItCode22.ToolTip = "F4 : Show/hide list";
            this.LueItCode22.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueItCode22.EditValueChanged += new System.EventHandler(this.LueItCode22_EditValueChanged);
            this.LueItCode22.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueItCode22_KeyDown);
            this.LueItCode22.Leave += new System.EventHandler(this.LueItCode22_Leave);
            // 
            // Grd32
            // 
            this.Grd32.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd32.DefaultRow.Height = 20;
            this.Grd32.DefaultRow.Sortable = false;
            this.Grd32.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd32.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd32.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd32.Header.Height = 21;
            this.Grd32.Location = new System.Drawing.Point(3, 36);
            this.Grd32.Name = "Grd32";
            this.Grd32.RowHeader.Visible = true;
            this.Grd32.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd32.SingleClickEdit = true;
            this.Grd32.Size = new System.Drawing.Size(809, 61);
            this.Grd32.TabIndex = 53;
            this.Grd32.TreeCol = null;
            this.Grd32.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd32.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd32_ColHdrDoubleClick);
            this.Grd32.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd32_EllipsisButtonClick);
            this.Grd32.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd32_RequestEdit);
            this.Grd32.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd32_AfterCommitEdit);
            this.Grd32.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd32_KeyDown);
            // 
            // panel27
            // 
            this.panel27.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel27.Controls.Add(this.MeeSM22);
            this.panel27.Controls.Add(this.label140);
            this.panel27.Controls.Add(this.LueSize22);
            this.panel27.Controls.Add(this.label101);
            this.panel27.Controls.Add(this.TxtCnt22);
            this.panel27.Controls.Add(this.label102);
            this.panel27.Controls.Add(this.TxtSeal22);
            this.panel27.Controls.Add(this.label103);
            this.panel27.Controls.Add(this.TxtTotal22);
            this.panel27.Controls.Add(this.label104);
            this.panel27.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel27.Location = new System.Drawing.Point(3, 3);
            this.panel27.Name = "panel27";
            this.panel27.Size = new System.Drawing.Size(809, 33);
            this.panel27.TabIndex = 46;
            // 
            // MeeSM22
            // 
            this.MeeSM22.EnterMoveNextControl = true;
            this.MeeSM22.Location = new System.Drawing.Point(689, 6);
            this.MeeSM22.Margin = new System.Windows.Forms.Padding(5);
            this.MeeSM22.Name = "MeeSM22";
            this.MeeSM22.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM22.Properties.Appearance.Options.UseFont = true;
            this.MeeSM22.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM22.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeSM22.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM22.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeSM22.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM22.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeSM22.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM22.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeSM22.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeSM22.Properties.MaxLength = 1000;
            this.MeeSM22.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeSM22.Properties.ShowIcon = false;
            this.MeeSM22.Size = new System.Drawing.Size(120, 20);
            this.MeeSM22.TabIndex = 123;
            this.MeeSM22.ToolTip = "F4 : Show/hide text";
            this.MeeSM22.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeSM22.ToolTipTitle = "Run System";
            // 
            // label140
            // 
            this.label140.AutoSize = true;
            this.label140.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label140.Location = new System.Drawing.Point(664, 9);
            this.label140.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label140.Name = "label140";
            this.label140.Size = new System.Drawing.Size(23, 14);
            this.label140.TabIndex = 122;
            this.label140.Text = "SM";
            this.label140.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueSize22
            // 
            this.LueSize22.EnterMoveNextControl = true;
            this.LueSize22.Location = new System.Drawing.Point(35, 6);
            this.LueSize22.Margin = new System.Windows.Forms.Padding(5);
            this.LueSize22.Name = "LueSize22";
            this.LueSize22.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize22.Properties.Appearance.Options.UseFont = true;
            this.LueSize22.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize22.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSize22.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize22.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSize22.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize22.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSize22.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize22.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSize22.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSize22.Properties.DropDownRows = 20;
            this.LueSize22.Properties.NullText = "[Empty]";
            this.LueSize22.Properties.PopupWidth = 300;
            this.LueSize22.Size = new System.Drawing.Size(67, 20);
            this.LueSize22.TabIndex = 121;
            this.LueSize22.ToolTip = "F4 : Show/hide list";
            this.LueSize22.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSize22.EditValueChanged += new System.EventHandler(this.LueSize22_EditValueChanged);
            // 
            // label101
            // 
            this.label101.AutoSize = true;
            this.label101.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label101.ForeColor = System.Drawing.Color.Red;
            this.label101.Location = new System.Drawing.Point(7, 8);
            this.label101.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(28, 14);
            this.label101.TabIndex = 120;
            this.label101.Text = "Size";
            this.label101.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCnt22
            // 
            this.TxtCnt22.EditValue = "";
            this.TxtCnt22.EnterMoveNextControl = true;
            this.TxtCnt22.Location = new System.Drawing.Point(186, 6);
            this.TxtCnt22.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCnt22.Name = "TxtCnt22";
            this.TxtCnt22.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCnt22.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCnt22.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCnt22.Properties.Appearance.Options.UseFont = true;
            this.TxtCnt22.Size = new System.Drawing.Size(151, 20);
            this.TxtCnt22.TabIndex = 57;
            this.TxtCnt22.Validated += new System.EventHandler(this.TxtCnt22_Validated);
            // 
            // label102
            // 
            this.label102.AutoSize = true;
            this.label102.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label102.ForeColor = System.Drawing.Color.Red;
            this.label102.Location = new System.Drawing.Point(106, 9);
            this.label102.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(78, 14);
            this.label102.TabIndex = 56;
            this.label102.Text = "Container No";
            this.label102.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSeal22
            // 
            this.TxtSeal22.EditValue = "";
            this.TxtSeal22.EnterMoveNextControl = true;
            this.TxtSeal22.Location = new System.Drawing.Point(390, 5);
            this.TxtSeal22.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSeal22.Name = "TxtSeal22";
            this.TxtSeal22.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSeal22.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSeal22.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSeal22.Properties.Appearance.Options.UseFont = true;
            this.TxtSeal22.Size = new System.Drawing.Size(146, 20);
            this.TxtSeal22.TabIndex = 55;
            this.TxtSeal22.Validated += new System.EventHandler(this.TxtSeal22_Validated);
            // 
            // label103
            // 
            this.label103.AutoSize = true;
            this.label103.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label103.ForeColor = System.Drawing.Color.Black;
            this.label103.Location = new System.Drawing.Point(537, 9);
            this.label103.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(35, 14);
            this.label103.TabIndex = 53;
            this.label103.Text = "Total";
            this.label103.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal22
            // 
            this.TxtTotal22.EditValue = "";
            this.TxtTotal22.EnterMoveNextControl = true;
            this.TxtTotal22.Location = new System.Drawing.Point(574, 6);
            this.TxtTotal22.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal22.Name = "TxtTotal22";
            this.TxtTotal22.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal22.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal22.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal22.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal22.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotal22.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotal22.Size = new System.Drawing.Size(90, 20);
            this.TxtTotal22.TabIndex = 54;
            // 
            // label104
            // 
            this.label104.AutoSize = true;
            this.label104.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label104.ForeColor = System.Drawing.Color.Red;
            this.label104.Location = new System.Drawing.Point(338, 8);
            this.label104.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(48, 14);
            this.label104.TabIndex = 52;
            this.label104.Text = "Seal No";
            this.label104.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage23
            // 
            this.tabPage23.Controls.Add(this.LueItCode23);
            this.tabPage23.Controls.Add(this.Grd33);
            this.tabPage23.Controls.Add(this.panel28);
            this.tabPage23.Location = new System.Drawing.Point(4, 23);
            this.tabPage23.Name = "tabPage23";
            this.tabPage23.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage23.Size = new System.Drawing.Size(815, 100);
            this.tabPage23.TabIndex = 22;
            this.tabPage23.Text = "No.23";
            this.tabPage23.UseVisualStyleBackColor = true;
            // 
            // LueItCode23
            // 
            this.LueItCode23.EnterMoveNextControl = true;
            this.LueItCode23.Location = new System.Drawing.Point(208, 58);
            this.LueItCode23.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode23.Name = "LueItCode23";
            this.LueItCode23.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode23.Properties.Appearance.Options.UseFont = true;
            this.LueItCode23.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode23.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode23.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode23.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode23.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode23.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode23.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode23.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode23.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode23.Properties.DropDownRows = 20;
            this.LueItCode23.Properties.NullText = "[Empty]";
            this.LueItCode23.Properties.PopupWidth = 500;
            this.LueItCode23.Size = new System.Drawing.Size(152, 20);
            this.LueItCode23.TabIndex = 55;
            this.LueItCode23.ToolTip = "F4 : Show/hide list";
            this.LueItCode23.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueItCode23.EditValueChanged += new System.EventHandler(this.LueItCode23_EditValueChanged);
            this.LueItCode23.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueItCode23_KeyDown);
            this.LueItCode23.Leave += new System.EventHandler(this.LueItCode23_Leave);
            // 
            // Grd33
            // 
            this.Grd33.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd33.DefaultRow.Height = 20;
            this.Grd33.DefaultRow.Sortable = false;
            this.Grd33.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd33.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd33.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd33.Header.Height = 21;
            this.Grd33.Location = new System.Drawing.Point(3, 36);
            this.Grd33.Name = "Grd33";
            this.Grd33.RowHeader.Visible = true;
            this.Grd33.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd33.SingleClickEdit = true;
            this.Grd33.Size = new System.Drawing.Size(809, 61);
            this.Grd33.TabIndex = 53;
            this.Grd33.TreeCol = null;
            this.Grd33.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd33.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd33_ColHdrDoubleClick);
            this.Grd33.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd33_EllipsisButtonClick);
            this.Grd33.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd33_RequestEdit);
            this.Grd33.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd33_AfterCommitEdit);
            this.Grd33.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd33_KeyDown);
            // 
            // panel28
            // 
            this.panel28.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel28.Controls.Add(this.MeeSM23);
            this.panel28.Controls.Add(this.label141);
            this.panel28.Controls.Add(this.LueSize23);
            this.panel28.Controls.Add(this.label105);
            this.panel28.Controls.Add(this.TxtCnt23);
            this.panel28.Controls.Add(this.label106);
            this.panel28.Controls.Add(this.TxtSeal23);
            this.panel28.Controls.Add(this.label107);
            this.panel28.Controls.Add(this.TxtTotal23);
            this.panel28.Controls.Add(this.label108);
            this.panel28.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel28.Location = new System.Drawing.Point(3, 3);
            this.panel28.Name = "panel28";
            this.panel28.Size = new System.Drawing.Size(809, 33);
            this.panel28.TabIndex = 46;
            // 
            // MeeSM23
            // 
            this.MeeSM23.EnterMoveNextControl = true;
            this.MeeSM23.Location = new System.Drawing.Point(683, 6);
            this.MeeSM23.Margin = new System.Windows.Forms.Padding(5);
            this.MeeSM23.Name = "MeeSM23";
            this.MeeSM23.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM23.Properties.Appearance.Options.UseFont = true;
            this.MeeSM23.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM23.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeSM23.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM23.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeSM23.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM23.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeSM23.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM23.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeSM23.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeSM23.Properties.MaxLength = 1000;
            this.MeeSM23.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeSM23.Properties.ShowIcon = false;
            this.MeeSM23.Size = new System.Drawing.Size(120, 20);
            this.MeeSM23.TabIndex = 123;
            this.MeeSM23.ToolTip = "F4 : Show/hide text";
            this.MeeSM23.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeSM23.ToolTipTitle = "Run System";
            // 
            // label141
            // 
            this.label141.AutoSize = true;
            this.label141.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label141.Location = new System.Drawing.Point(658, 9);
            this.label141.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label141.Name = "label141";
            this.label141.Size = new System.Drawing.Size(23, 14);
            this.label141.TabIndex = 122;
            this.label141.Text = "SM";
            this.label141.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueSize23
            // 
            this.LueSize23.EnterMoveNextControl = true;
            this.LueSize23.Location = new System.Drawing.Point(35, 6);
            this.LueSize23.Margin = new System.Windows.Forms.Padding(5);
            this.LueSize23.Name = "LueSize23";
            this.LueSize23.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize23.Properties.Appearance.Options.UseFont = true;
            this.LueSize23.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize23.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSize23.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize23.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSize23.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize23.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSize23.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize23.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSize23.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSize23.Properties.DropDownRows = 20;
            this.LueSize23.Properties.NullText = "[Empty]";
            this.LueSize23.Properties.PopupWidth = 300;
            this.LueSize23.Size = new System.Drawing.Size(67, 20);
            this.LueSize23.TabIndex = 121;
            this.LueSize23.ToolTip = "F4 : Show/hide list";
            this.LueSize23.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSize23.EditValueChanged += new System.EventHandler(this.LueSize23_EditValueChanged);
            // 
            // label105
            // 
            this.label105.AutoSize = true;
            this.label105.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label105.ForeColor = System.Drawing.Color.Red;
            this.label105.Location = new System.Drawing.Point(7, 8);
            this.label105.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(28, 14);
            this.label105.TabIndex = 120;
            this.label105.Text = "Size";
            this.label105.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCnt23
            // 
            this.TxtCnt23.EditValue = "";
            this.TxtCnt23.EnterMoveNextControl = true;
            this.TxtCnt23.Location = new System.Drawing.Point(186, 6);
            this.TxtCnt23.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCnt23.Name = "TxtCnt23";
            this.TxtCnt23.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCnt23.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCnt23.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCnt23.Properties.Appearance.Options.UseFont = true;
            this.TxtCnt23.Size = new System.Drawing.Size(148, 20);
            this.TxtCnt23.TabIndex = 57;
            this.TxtCnt23.Validated += new System.EventHandler(this.TxtCnt23_Validated);
            // 
            // label106
            // 
            this.label106.AutoSize = true;
            this.label106.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label106.ForeColor = System.Drawing.Color.Red;
            this.label106.Location = new System.Drawing.Point(106, 9);
            this.label106.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(78, 14);
            this.label106.TabIndex = 56;
            this.label106.Text = "Container No";
            this.label106.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSeal23
            // 
            this.TxtSeal23.EditValue = "";
            this.TxtSeal23.EnterMoveNextControl = true;
            this.TxtSeal23.Location = new System.Drawing.Point(385, 5);
            this.TxtSeal23.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSeal23.Name = "TxtSeal23";
            this.TxtSeal23.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSeal23.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSeal23.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSeal23.Properties.Appearance.Options.UseFont = true;
            this.TxtSeal23.Size = new System.Drawing.Size(140, 20);
            this.TxtSeal23.TabIndex = 55;
            this.TxtSeal23.Validated += new System.EventHandler(this.TxtSeal23_Validated);
            // 
            // label107
            // 
            this.label107.AutoSize = true;
            this.label107.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label107.ForeColor = System.Drawing.Color.Black;
            this.label107.Location = new System.Drawing.Point(524, 9);
            this.label107.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(35, 14);
            this.label107.TabIndex = 53;
            this.label107.Text = "Total";
            this.label107.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal23
            // 
            this.TxtTotal23.EditValue = "";
            this.TxtTotal23.EnterMoveNextControl = true;
            this.TxtTotal23.Location = new System.Drawing.Point(561, 6);
            this.TxtTotal23.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal23.Name = "TxtTotal23";
            this.TxtTotal23.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal23.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal23.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal23.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal23.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotal23.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotal23.Size = new System.Drawing.Size(90, 20);
            this.TxtTotal23.TabIndex = 54;
            // 
            // label108
            // 
            this.label108.AutoSize = true;
            this.label108.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label108.ForeColor = System.Drawing.Color.Red;
            this.label108.Location = new System.Drawing.Point(333, 8);
            this.label108.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(48, 14);
            this.label108.TabIndex = 52;
            this.label108.Text = "Seal No";
            this.label108.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage24
            // 
            this.tabPage24.Controls.Add(this.LueItCode24);
            this.tabPage24.Controls.Add(this.Grd34);
            this.tabPage24.Controls.Add(this.panel29);
            this.tabPage24.Location = new System.Drawing.Point(4, 23);
            this.tabPage24.Name = "tabPage24";
            this.tabPage24.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage24.Size = new System.Drawing.Size(815, 100);
            this.tabPage24.TabIndex = 23;
            this.tabPage24.Text = "No.24";
            this.tabPage24.UseVisualStyleBackColor = true;
            // 
            // LueItCode24
            // 
            this.LueItCode24.EnterMoveNextControl = true;
            this.LueItCode24.Location = new System.Drawing.Point(175, 57);
            this.LueItCode24.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode24.Name = "LueItCode24";
            this.LueItCode24.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode24.Properties.Appearance.Options.UseFont = true;
            this.LueItCode24.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode24.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode24.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode24.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode24.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode24.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode24.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode24.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode24.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode24.Properties.DropDownRows = 20;
            this.LueItCode24.Properties.NullText = "[Empty]";
            this.LueItCode24.Properties.PopupWidth = 500;
            this.LueItCode24.Size = new System.Drawing.Size(152, 20);
            this.LueItCode24.TabIndex = 55;
            this.LueItCode24.ToolTip = "F4 : Show/hide list";
            this.LueItCode24.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueItCode24.EditValueChanged += new System.EventHandler(this.LueItCode24_EditValueChanged);
            this.LueItCode24.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueItCode24_KeyDown);
            this.LueItCode24.Leave += new System.EventHandler(this.LueItCode24_Leave);
            // 
            // Grd34
            // 
            this.Grd34.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd34.DefaultRow.Height = 20;
            this.Grd34.DefaultRow.Sortable = false;
            this.Grd34.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd34.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd34.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd34.Header.Height = 21;
            this.Grd34.Location = new System.Drawing.Point(3, 36);
            this.Grd34.Name = "Grd34";
            this.Grd34.RowHeader.Visible = true;
            this.Grd34.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd34.SingleClickEdit = true;
            this.Grd34.Size = new System.Drawing.Size(809, 61);
            this.Grd34.TabIndex = 53;
            this.Grd34.TreeCol = null;
            this.Grd34.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd34.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd34_ColHdrDoubleClick);
            this.Grd34.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd34_EllipsisButtonClick);
            this.Grd34.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd34_RequestEdit);
            this.Grd34.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd34_AfterCommitEdit);
            this.Grd34.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd34_KeyDown);
            // 
            // panel29
            // 
            this.panel29.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel29.Controls.Add(this.MeeSM24);
            this.panel29.Controls.Add(this.label142);
            this.panel29.Controls.Add(this.LueSize24);
            this.panel29.Controls.Add(this.label109);
            this.panel29.Controls.Add(this.TxtCnt24);
            this.panel29.Controls.Add(this.label110);
            this.panel29.Controls.Add(this.TxtSeal24);
            this.panel29.Controls.Add(this.label111);
            this.panel29.Controls.Add(this.TxtTotal24);
            this.panel29.Controls.Add(this.label112);
            this.panel29.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel29.Location = new System.Drawing.Point(3, 3);
            this.panel29.Name = "panel29";
            this.panel29.Size = new System.Drawing.Size(809, 33);
            this.panel29.TabIndex = 46;
            // 
            // MeeSM24
            // 
            this.MeeSM24.EnterMoveNextControl = true;
            this.MeeSM24.Location = new System.Drawing.Point(686, 5);
            this.MeeSM24.Margin = new System.Windows.Forms.Padding(5);
            this.MeeSM24.Name = "MeeSM24";
            this.MeeSM24.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM24.Properties.Appearance.Options.UseFont = true;
            this.MeeSM24.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM24.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeSM24.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM24.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeSM24.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM24.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeSM24.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM24.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeSM24.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeSM24.Properties.MaxLength = 1000;
            this.MeeSM24.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeSM24.Properties.ShowIcon = false;
            this.MeeSM24.Size = new System.Drawing.Size(120, 20);
            this.MeeSM24.TabIndex = 123;
            this.MeeSM24.ToolTip = "F4 : Show/hide text";
            this.MeeSM24.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeSM24.ToolTipTitle = "Run System";
            // 
            // label142
            // 
            this.label142.AutoSize = true;
            this.label142.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label142.Location = new System.Drawing.Point(661, 8);
            this.label142.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label142.Name = "label142";
            this.label142.Size = new System.Drawing.Size(23, 14);
            this.label142.TabIndex = 122;
            this.label142.Text = "SM";
            this.label142.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueSize24
            // 
            this.LueSize24.EnterMoveNextControl = true;
            this.LueSize24.Location = new System.Drawing.Point(35, 6);
            this.LueSize24.Margin = new System.Windows.Forms.Padding(5);
            this.LueSize24.Name = "LueSize24";
            this.LueSize24.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize24.Properties.Appearance.Options.UseFont = true;
            this.LueSize24.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize24.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSize24.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize24.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSize24.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize24.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSize24.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize24.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSize24.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSize24.Properties.DropDownRows = 20;
            this.LueSize24.Properties.NullText = "[Empty]";
            this.LueSize24.Properties.PopupWidth = 300;
            this.LueSize24.Size = new System.Drawing.Size(67, 20);
            this.LueSize24.TabIndex = 121;
            this.LueSize24.ToolTip = "F4 : Show/hide list";
            this.LueSize24.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSize24.EditValueChanged += new System.EventHandler(this.LueSize24_EditValueChanged);
            // 
            // label109
            // 
            this.label109.AutoSize = true;
            this.label109.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label109.ForeColor = System.Drawing.Color.Red;
            this.label109.Location = new System.Drawing.Point(7, 8);
            this.label109.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(28, 14);
            this.label109.TabIndex = 120;
            this.label109.Text = "Size";
            this.label109.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCnt24
            // 
            this.TxtCnt24.EditValue = "";
            this.TxtCnt24.EnterMoveNextControl = true;
            this.TxtCnt24.Location = new System.Drawing.Point(186, 6);
            this.TxtCnt24.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCnt24.Name = "TxtCnt24";
            this.TxtCnt24.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCnt24.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCnt24.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCnt24.Properties.Appearance.Options.UseFont = true;
            this.TxtCnt24.Size = new System.Drawing.Size(159, 20);
            this.TxtCnt24.TabIndex = 57;
            this.TxtCnt24.Validated += new System.EventHandler(this.TxtCnt24_Validated);
            // 
            // label110
            // 
            this.label110.AutoSize = true;
            this.label110.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label110.ForeColor = System.Drawing.Color.Red;
            this.label110.Location = new System.Drawing.Point(106, 9);
            this.label110.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label110.Name = "label110";
            this.label110.Size = new System.Drawing.Size(78, 14);
            this.label110.TabIndex = 56;
            this.label110.Text = "Container No";
            this.label110.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSeal24
            // 
            this.TxtSeal24.EditValue = "";
            this.TxtSeal24.EnterMoveNextControl = true;
            this.TxtSeal24.Location = new System.Drawing.Point(401, 5);
            this.TxtSeal24.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSeal24.Name = "TxtSeal24";
            this.TxtSeal24.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSeal24.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSeal24.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSeal24.Properties.Appearance.Options.UseFont = true;
            this.TxtSeal24.Size = new System.Drawing.Size(136, 20);
            this.TxtSeal24.TabIndex = 55;
            this.TxtSeal24.Validated += new System.EventHandler(this.TxtSeal24_Validated);
            // 
            // label111
            // 
            this.label111.AutoSize = true;
            this.label111.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label111.ForeColor = System.Drawing.Color.Black;
            this.label111.Location = new System.Drawing.Point(534, 7);
            this.label111.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(35, 14);
            this.label111.TabIndex = 53;
            this.label111.Text = "Total";
            this.label111.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal24
            // 
            this.TxtTotal24.EditValue = "";
            this.TxtTotal24.EnterMoveNextControl = true;
            this.TxtTotal24.Location = new System.Drawing.Point(571, 4);
            this.TxtTotal24.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal24.Name = "TxtTotal24";
            this.TxtTotal24.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal24.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal24.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal24.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal24.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotal24.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotal24.Size = new System.Drawing.Size(90, 20);
            this.TxtTotal24.TabIndex = 54;
            // 
            // label112
            // 
            this.label112.AutoSize = true;
            this.label112.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label112.ForeColor = System.Drawing.Color.Red;
            this.label112.Location = new System.Drawing.Point(349, 8);
            this.label112.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label112.Name = "label112";
            this.label112.Size = new System.Drawing.Size(48, 14);
            this.label112.TabIndex = 52;
            this.label112.Text = "Seal No";
            this.label112.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage25
            // 
            this.tabPage25.Controls.Add(this.LueItCode25);
            this.tabPage25.Controls.Add(this.Grd35);
            this.tabPage25.Controls.Add(this.panel30);
            this.tabPage25.Location = new System.Drawing.Point(4, 23);
            this.tabPage25.Name = "tabPage25";
            this.tabPage25.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage25.Size = new System.Drawing.Size(815, 100);
            this.tabPage25.TabIndex = 24;
            this.tabPage25.Text = "No.25";
            this.tabPage25.UseVisualStyleBackColor = true;
            // 
            // LueItCode25
            // 
            this.LueItCode25.EnterMoveNextControl = true;
            this.LueItCode25.Location = new System.Drawing.Point(172, 59);
            this.LueItCode25.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode25.Name = "LueItCode25";
            this.LueItCode25.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode25.Properties.Appearance.Options.UseFont = true;
            this.LueItCode25.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode25.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode25.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode25.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode25.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode25.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode25.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode25.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode25.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode25.Properties.DropDownRows = 20;
            this.LueItCode25.Properties.NullText = "[Empty]";
            this.LueItCode25.Properties.PopupWidth = 500;
            this.LueItCode25.Size = new System.Drawing.Size(152, 20);
            this.LueItCode25.TabIndex = 55;
            this.LueItCode25.ToolTip = "F4 : Show/hide list";
            this.LueItCode25.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueItCode25.EditValueChanged += new System.EventHandler(this.LueItCode25_EditValueChanged);
            this.LueItCode25.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueItCode25_KeyDown);
            this.LueItCode25.Leave += new System.EventHandler(this.LueItCode25_Leave);
            // 
            // Grd35
            // 
            this.Grd35.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd35.DefaultRow.Height = 20;
            this.Grd35.DefaultRow.Sortable = false;
            this.Grd35.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd35.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd35.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd35.Header.Height = 21;
            this.Grd35.Location = new System.Drawing.Point(3, 36);
            this.Grd35.Name = "Grd35";
            this.Grd35.RowHeader.Visible = true;
            this.Grd35.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd35.SingleClickEdit = true;
            this.Grd35.Size = new System.Drawing.Size(809, 61);
            this.Grd35.TabIndex = 53;
            this.Grd35.TreeCol = null;
            this.Grd35.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd35.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd35_ColHdrDoubleClick);
            this.Grd35.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd35_EllipsisButtonClick);
            this.Grd35.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd35_RequestEdit);
            this.Grd35.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd35_AfterCommitEdit);
            this.Grd35.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd35_KeyDown);
            // 
            // panel30
            // 
            this.panel30.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel30.Controls.Add(this.MeeSM25);
            this.panel30.Controls.Add(this.label143);
            this.panel30.Controls.Add(this.LueSize25);
            this.panel30.Controls.Add(this.label113);
            this.panel30.Controls.Add(this.TxtCnt25);
            this.panel30.Controls.Add(this.label114);
            this.panel30.Controls.Add(this.TxtSeal25);
            this.panel30.Controls.Add(this.label115);
            this.panel30.Controls.Add(this.TxtTotal25);
            this.panel30.Controls.Add(this.label116);
            this.panel30.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel30.Location = new System.Drawing.Point(3, 3);
            this.panel30.Name = "panel30";
            this.panel30.Size = new System.Drawing.Size(809, 33);
            this.panel30.TabIndex = 46;
            // 
            // MeeSM25
            // 
            this.MeeSM25.EnterMoveNextControl = true;
            this.MeeSM25.Location = new System.Drawing.Point(685, 6);
            this.MeeSM25.Margin = new System.Windows.Forms.Padding(5);
            this.MeeSM25.Name = "MeeSM25";
            this.MeeSM25.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM25.Properties.Appearance.Options.UseFont = true;
            this.MeeSM25.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM25.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeSM25.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM25.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeSM25.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM25.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeSM25.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSM25.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeSM25.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeSM25.Properties.MaxLength = 1000;
            this.MeeSM25.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeSM25.Properties.ShowIcon = false;
            this.MeeSM25.Size = new System.Drawing.Size(120, 20);
            this.MeeSM25.TabIndex = 123;
            this.MeeSM25.ToolTip = "F4 : Show/hide text";
            this.MeeSM25.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeSM25.ToolTipTitle = "Run System";
            // 
            // label143
            // 
            this.label143.AutoSize = true;
            this.label143.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label143.Location = new System.Drawing.Point(660, 9);
            this.label143.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label143.Name = "label143";
            this.label143.Size = new System.Drawing.Size(23, 14);
            this.label143.TabIndex = 122;
            this.label143.Text = "SM";
            this.label143.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueSize25
            // 
            this.LueSize25.EnterMoveNextControl = true;
            this.LueSize25.Location = new System.Drawing.Point(35, 6);
            this.LueSize25.Margin = new System.Windows.Forms.Padding(5);
            this.LueSize25.Name = "LueSize25";
            this.LueSize25.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize25.Properties.Appearance.Options.UseFont = true;
            this.LueSize25.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize25.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSize25.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize25.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSize25.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize25.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSize25.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSize25.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSize25.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSize25.Properties.DropDownRows = 20;
            this.LueSize25.Properties.NullText = "[Empty]";
            this.LueSize25.Properties.PopupWidth = 300;
            this.LueSize25.Size = new System.Drawing.Size(67, 20);
            this.LueSize25.TabIndex = 121;
            this.LueSize25.ToolTip = "F4 : Show/hide list";
            this.LueSize25.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSize25.EditValueChanged += new System.EventHandler(this.LueSize25_EditValueChanged);
            // 
            // label113
            // 
            this.label113.AutoSize = true;
            this.label113.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label113.ForeColor = System.Drawing.Color.Red;
            this.label113.Location = new System.Drawing.Point(7, 8);
            this.label113.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label113.Name = "label113";
            this.label113.Size = new System.Drawing.Size(28, 14);
            this.label113.TabIndex = 120;
            this.label113.Text = "Size";
            this.label113.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCnt25
            // 
            this.TxtCnt25.EditValue = "";
            this.TxtCnt25.EnterMoveNextControl = true;
            this.TxtCnt25.Location = new System.Drawing.Point(186, 6);
            this.TxtCnt25.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCnt25.Name = "TxtCnt25";
            this.TxtCnt25.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCnt25.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCnt25.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCnt25.Properties.Appearance.Options.UseFont = true;
            this.TxtCnt25.Size = new System.Drawing.Size(148, 20);
            this.TxtCnt25.TabIndex = 57;
            this.TxtCnt25.Validated += new System.EventHandler(this.TxtCnt25_Validated);
            // 
            // label114
            // 
            this.label114.AutoSize = true;
            this.label114.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label114.ForeColor = System.Drawing.Color.Red;
            this.label114.Location = new System.Drawing.Point(106, 9);
            this.label114.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label114.Name = "label114";
            this.label114.Size = new System.Drawing.Size(78, 14);
            this.label114.TabIndex = 56;
            this.label114.Text = "Container No";
            this.label114.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSeal25
            // 
            this.TxtSeal25.EditValue = "";
            this.TxtSeal25.EnterMoveNextControl = true;
            this.TxtSeal25.Location = new System.Drawing.Point(386, 7);
            this.TxtSeal25.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSeal25.Name = "TxtSeal25";
            this.TxtSeal25.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSeal25.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSeal25.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSeal25.Properties.Appearance.Options.UseFont = true;
            this.TxtSeal25.Size = new System.Drawing.Size(154, 20);
            this.TxtSeal25.TabIndex = 55;
            this.TxtSeal25.Validated += new System.EventHandler(this.TxtSeal25_Validated);
            // 
            // label115
            // 
            this.label115.AutoSize = true;
            this.label115.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label115.ForeColor = System.Drawing.Color.Black;
            this.label115.Location = new System.Drawing.Point(540, 9);
            this.label115.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label115.Name = "label115";
            this.label115.Size = new System.Drawing.Size(35, 14);
            this.label115.TabIndex = 53;
            this.label115.Text = "Total";
            this.label115.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal25
            // 
            this.TxtTotal25.EditValue = "";
            this.TxtTotal25.EnterMoveNextControl = true;
            this.TxtTotal25.Location = new System.Drawing.Point(577, 6);
            this.TxtTotal25.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal25.Name = "TxtTotal25";
            this.TxtTotal25.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal25.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal25.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal25.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal25.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotal25.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotal25.Size = new System.Drawing.Size(84, 20);
            this.TxtTotal25.TabIndex = 54;
            // 
            // label116
            // 
            this.label116.AutoSize = true;
            this.label116.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label116.ForeColor = System.Drawing.Color.Red;
            this.label116.Location = new System.Drawing.Point(334, 10);
            this.label116.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label116.Name = "label116";
            this.label116.Size = new System.Drawing.Size(48, 14);
            this.label116.TabIndex = 52;
            this.label116.Text = "Seal No";
            this.label116.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(123, 7);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 16;
            this.TxtDocNo.Size = new System.Drawing.Size(196, 20);
            this.TxtDocNo.TabIndex = 9;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(45, 10);
            this.label18.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(73, 14);
            this.label18.TabIndex = 8;
            this.label18.Text = "Document#";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(123, 29);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(126, 20);
            this.DteDocDt.TabIndex = 11;
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label61.ForeColor = System.Drawing.Color.Red;
            this.label61.Location = new System.Drawing.Point(47, 142);
            this.label61.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(71, 14);
            this.label61.TabIndex = 23;
            this.label61.Text = "Notify Party";
            this.label61.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueNotify
            // 
            this.LueNotify.EnterMoveNextControl = true;
            this.LueNotify.Location = new System.Drawing.Point(123, 139);
            this.LueNotify.Margin = new System.Windows.Forms.Padding(5);
            this.LueNotify.Name = "LueNotify";
            this.LueNotify.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueNotify.Properties.Appearance.Options.UseFont = true;
            this.LueNotify.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueNotify.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueNotify.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueNotify.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueNotify.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueNotify.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueNotify.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueNotify.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueNotify.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueNotify.Properties.DropDownRows = 30;
            this.LueNotify.Properties.NullText = "[Empty]";
            this.LueNotify.Properties.PopupWidth = 300;
            this.LueNotify.Size = new System.Drawing.Size(234, 20);
            this.LueNotify.TabIndex = 24;
            this.LueNotify.ToolTip = "F4 : Show/hide list";
            this.LueNotify.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueNotify.EditValueChanged += new System.EventHandler(this.LueNotify_EditValueChanged);
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label60.ForeColor = System.Drawing.Color.Red;
            this.label60.Location = new System.Drawing.Point(59, 54);
            this.label60.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(59, 14);
            this.label60.TabIndex = 12;
            this.label60.Text = "Customer";
            this.label60.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(46, 230);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(72, 14);
            this.label6.TabIndex = 31;
            this.label6.Text = "L/C Number";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueCtCode
            // 
            this.LueCtCode.EnterMoveNextControl = true;
            this.LueCtCode.Location = new System.Drawing.Point(123, 51);
            this.LueCtCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueCtCode.Name = "LueCtCode";
            this.LueCtCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.Appearance.Options.UseFont = true;
            this.LueCtCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCtCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCtCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCtCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCtCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCtCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCtCode.Properties.DropDownRows = 30;
            this.LueCtCode.Properties.NullText = "[Empty]";
            this.LueCtCode.Properties.PopupWidth = 300;
            this.LueCtCode.Size = new System.Drawing.Size(234, 20);
            this.LueCtCode.TabIndex = 13;
            this.LueCtCode.ToolTip = "F4 : Show/hide list";
            this.LueCtCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCtCode.EditValueChanged += new System.EventHandler(this.LueCtCode_EditValueChanged_1);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(1, 98);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(122, 14);
            this.label2.TabIndex = 17;
            this.label2.Text = "Shipment Instruction";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnSI
            // 
            this.BtnSI.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnSI.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSI.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSI.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSI.Appearance.Options.UseBackColor = true;
            this.BtnSI.Appearance.Options.UseFont = true;
            this.BtnSI.Appearance.Options.UseForeColor = true;
            this.BtnSI.Appearance.Options.UseTextOptions = true;
            this.BtnSI.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnSI.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnSI.Image = ((System.Drawing.Image)(resources.GetObject("BtnSI.Image")));
            this.BtnSI.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnSI.Location = new System.Drawing.Point(361, 94);
            this.BtnSI.Name = "BtnSI";
            this.BtnSI.Size = new System.Drawing.Size(24, 21);
            this.BtnSI.TabIndex = 19;
            this.BtnSI.ToolTip = "List of Shipment Instruction";
            this.BtnSI.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnSI.ToolTipTitle = "Run System";
            this.BtnSI.Click += new System.EventHandler(this.BtnSI_Click_1);
            // 
            // BtnSI2
            // 
            this.BtnSI2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnSI2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSI2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSI2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSI2.Appearance.Options.UseBackColor = true;
            this.BtnSI2.Appearance.Options.UseFont = true;
            this.BtnSI2.Appearance.Options.UseForeColor = true;
            this.BtnSI2.Appearance.Options.UseTextOptions = true;
            this.BtnSI2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnSI2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnSI2.Image = ((System.Drawing.Image)(resources.GetObject("BtnSI2.Image")));
            this.BtnSI2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnSI2.Location = new System.Drawing.Point(388, 94);
            this.BtnSI2.Name = "BtnSI2";
            this.BtnSI2.Size = new System.Drawing.Size(24, 21);
            this.BtnSI2.TabIndex = 20;
            this.BtnSI2.ToolTip = "Show Shipment Instruction";
            this.BtnSI2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnSI2.ToolTipTitle = "Run System";
            this.BtnSI2.Click += new System.EventHandler(this.BtnSI2_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(25, 186);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(93, 14);
            this.label4.TabIndex = 27;
            this.label4.Text = "Invoice Number";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(14, 208);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(104, 14);
            this.label1.TabIndex = 29;
            this.label1.Text = "Sales Contract No";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(63, 252);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(55, 14);
            this.label7.TabIndex = 33;
            this.label7.Text = "L/C Date";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Red;
            this.label17.Location = new System.Drawing.Point(85, 32);
            this.label17.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(33, 14);
            this.label17.TabIndex = 10;
            this.label17.Text = "Date";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LCDocDt
            // 
            this.LCDocDt.EditValue = null;
            this.LCDocDt.EnterMoveNextControl = true;
            this.LCDocDt.Location = new System.Drawing.Point(123, 249);
            this.LCDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.LCDocDt.Name = "LCDocDt";
            this.LCDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LCDocDt.Properties.Appearance.Options.UseFont = true;
            this.LCDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LCDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LCDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LCDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.LCDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.LCDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.LCDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.LCDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.LCDocDt.Properties.MaxLength = 8;
            this.LCDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.LCDocDt.Size = new System.Drawing.Size(120, 20);
            this.LCDocDt.TabIndex = 34;
            // 
            // MeeAccount
            // 
            this.MeeAccount.EnterMoveNextControl = true;
            this.MeeAccount.Location = new System.Drawing.Point(123, 161);
            this.MeeAccount.Margin = new System.Windows.Forms.Padding(5);
            this.MeeAccount.Name = "MeeAccount";
            this.MeeAccount.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAccount.Properties.Appearance.Options.UseFont = true;
            this.MeeAccount.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAccount.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeAccount.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAccount.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeAccount.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAccount.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeAccount.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F);
            this.MeeAccount.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeAccount.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeAccount.Properties.MaxLength = 400;
            this.MeeAccount.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeAccount.Properties.ShowIcon = false;
            this.MeeAccount.Size = new System.Drawing.Size(234, 20);
            this.MeeAccount.TabIndex = 26;
            this.MeeAccount.ToolTip = "F4 : Show/hide text";
            this.MeeAccount.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeAccount.ToolTipTitle = "Run System";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.ForeColor = System.Drawing.Color.Red;
            this.label42.Location = new System.Drawing.Point(11, 164);
            this.label42.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(107, 14);
            this.label42.TabIndex = 25;
            this.label42.Text = "Account of Messrs";
            this.label42.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSIDocNo
            // 
            this.TxtSIDocNo.EnterMoveNextControl = true;
            this.TxtSIDocNo.Location = new System.Drawing.Point(123, 95);
            this.TxtSIDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSIDocNo.Name = "TxtSIDocNo";
            this.TxtSIDocNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSIDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSIDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSIDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtSIDocNo.Properties.MaxLength = 250;
            this.TxtSIDocNo.Size = new System.Drawing.Size(234, 20);
            this.TxtSIDocNo.TabIndex = 18;
            // 
            // TxtInvoice
            // 
            this.TxtInvoice.EnterMoveNextControl = true;
            this.TxtInvoice.Location = new System.Drawing.Point(123, 183);
            this.TxtInvoice.Margin = new System.Windows.Forms.Padding(5);
            this.TxtInvoice.Name = "TxtInvoice";
            this.TxtInvoice.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtInvoice.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtInvoice.Properties.Appearance.Options.UseBackColor = true;
            this.TxtInvoice.Properties.Appearance.Options.UseFont = true;
            this.TxtInvoice.Properties.MaxLength = 250;
            this.TxtInvoice.Size = new System.Drawing.Size(234, 20);
            this.TxtInvoice.TabIndex = 28;
            // 
            // TxtSalesContract
            // 
            this.TxtSalesContract.EnterMoveNextControl = true;
            this.TxtSalesContract.Location = new System.Drawing.Point(123, 205);
            this.TxtSalesContract.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSalesContract.Name = "TxtSalesContract";
            this.TxtSalesContract.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSalesContract.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSalesContract.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSalesContract.Properties.Appearance.Options.UseFont = true;
            this.TxtSalesContract.Properties.MaxLength = 250;
            this.TxtSalesContract.Size = new System.Drawing.Size(234, 20);
            this.TxtSalesContract.TabIndex = 30;
            // 
            // TxtLcNo
            // 
            this.TxtLcNo.EnterMoveNextControl = true;
            this.TxtLcNo.Location = new System.Drawing.Point(123, 227);
            this.TxtLcNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLcNo.Name = "TxtLcNo";
            this.TxtLcNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLcNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLcNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLcNo.Properties.Appearance.Options.UseFont = true;
            this.TxtLcNo.Properties.MaxLength = 250;
            this.TxtLcNo.Size = new System.Drawing.Size(234, 20);
            this.TxtLcNo.TabIndex = 32;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel7.Controls.Add(this.BtnDownload);
            this.panel7.Controls.Add(this.TxtLocalDocNo);
            this.panel7.Controls.Add(this.LblLocalDoc);
            this.panel7.Controls.Add(this.BtnSource);
            this.panel7.Controls.Add(this.label75);
            this.panel7.Controls.Add(this.panel6);
            this.panel7.Controls.Add(this.TxtLcNo);
            this.panel7.Controls.Add(this.label18);
            this.panel7.Controls.Add(this.TxtSalesContract);
            this.panel7.Controls.Add(this.TxtDocNo);
            this.panel7.Controls.Add(this.TxtInvoice);
            this.panel7.Controls.Add(this.TxtSIDocNo);
            this.panel7.Controls.Add(this.label42);
            this.panel7.Controls.Add(this.MeeAccount);
            this.panel7.Controls.Add(this.LCDocDt);
            this.panel7.Controls.Add(this.label17);
            this.panel7.Controls.Add(this.label7);
            this.panel7.Controls.Add(this.label1);
            this.panel7.Controls.Add(this.label4);
            this.panel7.Controls.Add(this.BtnSI2);
            this.panel7.Controls.Add(this.BtnSI);
            this.panel7.Controls.Add(this.label2);
            this.panel7.Controls.Add(this.LueCtCode);
            this.panel7.Controls.Add(this.label6);
            this.panel7.Controls.Add(this.label60);
            this.panel7.Controls.Add(this.LueNotify);
            this.panel7.Controls.Add(this.label61);
            this.panel7.Controls.Add(this.DteDocDt);
            this.panel7.Controls.Add(this.TxtSource);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel7.Location = new System.Drawing.Point(0, 0);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(823, 275);
            this.panel7.TabIndex = 7;
            // 
            // BtnDownload
            // 
            this.BtnDownload.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDownload.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDownload.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDownload.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDownload.Appearance.Options.UseBackColor = true;
            this.BtnDownload.Appearance.Options.UseFont = true;
            this.BtnDownload.Appearance.Options.UseForeColor = true;
            this.BtnDownload.Appearance.Options.UseTextOptions = true;
            this.BtnDownload.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDownload.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDownload.Image = ((System.Drawing.Image)(resources.GetObject("BtnDownload.Image")));
            this.BtnDownload.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDownload.Location = new System.Drawing.Point(359, 117);
            this.BtnDownload.Name = "BtnDownload";
            this.BtnDownload.Size = new System.Drawing.Size(24, 21);
            this.BtnDownload.TabIndex = 106;
            this.BtnDownload.ToolTip = "Download File";
            this.BtnDownload.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDownload.ToolTipTitle = "Run System";
            this.BtnDownload.Click += new System.EventHandler(this.BtnDownload_Click);
            // 
            // TxtLocalDocNo
            // 
            this.TxtLocalDocNo.EnterMoveNextControl = true;
            this.TxtLocalDocNo.Location = new System.Drawing.Point(123, 117);
            this.TxtLocalDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLocalDocNo.Name = "TxtLocalDocNo";
            this.TxtLocalDocNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLocalDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLocalDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLocalDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtLocalDocNo.Properties.MaxLength = 30;
            this.TxtLocalDocNo.Size = new System.Drawing.Size(234, 20);
            this.TxtLocalDocNo.TabIndex = 22;
            // 
            // LblLocalDoc
            // 
            this.LblLocalDoc.AutoSize = true;
            this.LblLocalDoc.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblLocalDoc.Location = new System.Drawing.Point(24, 120);
            this.LblLocalDoc.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblLocalDoc.Name = "LblLocalDoc";
            this.LblLocalDoc.Size = new System.Drawing.Size(95, 14);
            this.LblLocalDoc.TabIndex = 21;
            this.LblLocalDoc.Text = "Local Document";
            this.LblLocalDoc.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnSource
            // 
            this.BtnSource.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnSource.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSource.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSource.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSource.Appearance.Options.UseBackColor = true;
            this.BtnSource.Appearance.Options.UseFont = true;
            this.BtnSource.Appearance.Options.UseForeColor = true;
            this.BtnSource.Appearance.Options.UseTextOptions = true;
            this.BtnSource.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnSource.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnSource.Image = ((System.Drawing.Image)(resources.GetObject("BtnSource.Image")));
            this.BtnSource.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnSource.Location = new System.Drawing.Point(361, 72);
            this.BtnSource.Name = "BtnSource";
            this.BtnSource.Size = new System.Drawing.Size(24, 21);
            this.BtnSource.TabIndex = 16;
            this.BtnSource.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnSource.ToolTipTitle = "Run System";
            this.BtnSource.Click += new System.EventHandler(this.BtnSource_Click);
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label75.ForeColor = System.Drawing.Color.Black;
            this.label75.Location = new System.Drawing.Point(74, 75);
            this.label75.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(45, 14);
            this.label75.TabIndex = 14;
            this.label75.Text = "Source";
            this.label75.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel6.Controls.Add(this.label117);
            this.panel6.Controls.Add(this.TxtTab);
            this.panel6.Controls.Add(this.ChkShippingMark);
            this.panel6.Controls.Add(this.MeeRemark2);
            this.panel6.Controls.Add(this.label74);
            this.panel6.Controls.Add(this.MeeRemark);
            this.panel6.Controls.Add(this.label56);
            this.panel6.Controls.Add(this.MeeIssued);
            this.panel6.Controls.Add(this.label3);
            this.panel6.Controls.Add(this.MeeOrder4);
            this.panel6.Controls.Add(this.label5);
            this.panel6.Controls.Add(this.MeeOrder3);
            this.panel6.Controls.Add(this.MeeIssued2);
            this.panel6.Controls.Add(this.MeeIssued3);
            this.panel6.Controls.Add(this.MeeIssued4);
            this.panel6.Controls.Add(this.MeeOrder);
            this.panel6.Controls.Add(this.MeeOrder2);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel6.Location = new System.Drawing.Point(460, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(363, 275);
            this.panel6.TabIndex = 105;
            // 
            // label117
            // 
            this.label117.AutoSize = true;
            this.label117.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label117.ForeColor = System.Drawing.Color.Black;
            this.label117.Location = new System.Drawing.Point(1, 249);
            this.label117.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label117.Name = "label117";
            this.label117.Size = new System.Drawing.Size(107, 14);
            this.label117.TabIndex = 50;
            this.label117.Text = "Print Container No";
            this.label117.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTab
            // 
            this.TxtTab.EnterMoveNextControl = true;
            this.TxtTab.Location = new System.Drawing.Point(112, 246);
            this.TxtTab.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTab.Name = "TxtTab";
            this.TxtTab.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTab.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTab.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTab.Properties.Appearance.Options.UseFont = true;
            this.TxtTab.Properties.MaxLength = 16;
            this.TxtTab.Size = new System.Drawing.Size(48, 20);
            this.TxtTab.TabIndex = 51;
            // 
            // ChkShippingMark
            // 
            this.ChkShippingMark.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkShippingMark.Location = new System.Drawing.Point(112, 224);
            this.ChkShippingMark.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkShippingMark.Name = "ChkShippingMark";
            this.ChkShippingMark.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkShippingMark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkShippingMark.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkShippingMark.Properties.Appearance.Options.UseBackColor = true;
            this.ChkShippingMark.Properties.Appearance.Options.UseFont = true;
            this.ChkShippingMark.Properties.Appearance.Options.UseForeColor = true;
            this.ChkShippingMark.Properties.Caption = "Print With Shipping Mark";
            this.ChkShippingMark.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkShippingMark.ShowToolTips = false;
            this.ChkShippingMark.Size = new System.Drawing.Size(197, 22);
            this.ChkShippingMark.TabIndex = 49;
            // 
            // MeeRemark2
            // 
            this.MeeRemark2.EnterMoveNextControl = true;
            this.MeeRemark2.Location = new System.Drawing.Point(112, 204);
            this.MeeRemark2.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark2.Name = "MeeRemark2";
            this.MeeRemark2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark2.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark2.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark2.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark2.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark2.Properties.MaxLength = 1000;
            this.MeeRemark2.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeRemark2.Properties.ShowIcon = false;
            this.MeeRemark2.Size = new System.Drawing.Size(241, 20);
            this.MeeRemark2.TabIndex = 48;
            this.MeeRemark2.ToolTip = "F4 : Show/hide text";
            this.MeeRemark2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark2.ToolTipTitle = "Run System";
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label74.Location = new System.Drawing.Point(74, 207);
            this.label74.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(34, 14);
            this.label74.TabIndex = 47;
            this.label74.Text = "Note";
            this.label74.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(112, 182);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 1000;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(241, 20);
            this.MeeRemark.TabIndex = 46;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label56.Location = new System.Drawing.Point(41, 185);
            this.label56.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(67, 14);
            this.label56.TabIndex = 45;
            this.label56.Text = "Description";
            this.label56.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeIssued
            // 
            this.MeeIssued.EnterMoveNextControl = true;
            this.MeeIssued.Location = new System.Drawing.Point(112, 6);
            this.MeeIssued.Margin = new System.Windows.Forms.Padding(5);
            this.MeeIssued.Name = "MeeIssued";
            this.MeeIssued.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIssued.Properties.Appearance.Options.UseFont = true;
            this.MeeIssued.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIssued.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeIssued.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIssued.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeIssued.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIssued.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeIssued.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIssued.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeIssued.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeIssued.Properties.MaxLength = 400;
            this.MeeIssued.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeIssued.Properties.ShowIcon = false;
            this.MeeIssued.Size = new System.Drawing.Size(241, 20);
            this.MeeIssued.TabIndex = 36;
            this.MeeIssued.ToolTip = "F4 : Show/hide text";
            this.MeeIssued.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeIssued.ToolTipTitle = "Run System";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(8, 97);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(100, 14);
            this.label3.TabIndex = 40;
            this.label3.Text = "To The Order Of";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeOrder4
            // 
            this.MeeOrder4.EnterMoveNextControl = true;
            this.MeeOrder4.Location = new System.Drawing.Point(112, 160);
            this.MeeOrder4.Margin = new System.Windows.Forms.Padding(5);
            this.MeeOrder4.Name = "MeeOrder4";
            this.MeeOrder4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeOrder4.Properties.Appearance.Options.UseFont = true;
            this.MeeOrder4.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeOrder4.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeOrder4.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeOrder4.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeOrder4.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeOrder4.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeOrder4.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeOrder4.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeOrder4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeOrder4.Properties.MaxLength = 400;
            this.MeeOrder4.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeOrder4.Properties.ShowIcon = false;
            this.MeeOrder4.Size = new System.Drawing.Size(241, 20);
            this.MeeOrder4.TabIndex = 44;
            this.MeeOrder4.ToolTip = "F4 : Show/hide text";
            this.MeeOrder4.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeOrder4.ToolTipTitle = "Run System";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(49, 9);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 14);
            this.label5.TabIndex = 35;
            this.label5.Text = "Issued By";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeOrder3
            // 
            this.MeeOrder3.EnterMoveNextControl = true;
            this.MeeOrder3.Location = new System.Drawing.Point(112, 138);
            this.MeeOrder3.Margin = new System.Windows.Forms.Padding(5);
            this.MeeOrder3.Name = "MeeOrder3";
            this.MeeOrder3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeOrder3.Properties.Appearance.Options.UseFont = true;
            this.MeeOrder3.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeOrder3.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeOrder3.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeOrder3.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeOrder3.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeOrder3.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeOrder3.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeOrder3.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeOrder3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeOrder3.Properties.MaxLength = 400;
            this.MeeOrder3.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeOrder3.Properties.ShowIcon = false;
            this.MeeOrder3.Size = new System.Drawing.Size(241, 20);
            this.MeeOrder3.TabIndex = 43;
            this.MeeOrder3.ToolTip = "F4 : Show/hide text";
            this.MeeOrder3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeOrder3.ToolTipTitle = "Run System";
            // 
            // MeeIssued2
            // 
            this.MeeIssued2.EnterMoveNextControl = true;
            this.MeeIssued2.Location = new System.Drawing.Point(112, 28);
            this.MeeIssued2.Margin = new System.Windows.Forms.Padding(5);
            this.MeeIssued2.Name = "MeeIssued2";
            this.MeeIssued2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIssued2.Properties.Appearance.Options.UseFont = true;
            this.MeeIssued2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIssued2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeIssued2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIssued2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeIssued2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIssued2.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeIssued2.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIssued2.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeIssued2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeIssued2.Properties.MaxLength = 400;
            this.MeeIssued2.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeIssued2.Properties.ShowIcon = false;
            this.MeeIssued2.Size = new System.Drawing.Size(241, 20);
            this.MeeIssued2.TabIndex = 37;
            this.MeeIssued2.ToolTip = "F4 : Show/hide text";
            this.MeeIssued2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeIssued2.ToolTipTitle = "Run System";
            // 
            // MeeIssued3
            // 
            this.MeeIssued3.EnterMoveNextControl = true;
            this.MeeIssued3.Location = new System.Drawing.Point(112, 50);
            this.MeeIssued3.Margin = new System.Windows.Forms.Padding(5);
            this.MeeIssued3.Name = "MeeIssued3";
            this.MeeIssued3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIssued3.Properties.Appearance.Options.UseFont = true;
            this.MeeIssued3.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIssued3.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeIssued3.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIssued3.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeIssued3.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIssued3.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeIssued3.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIssued3.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeIssued3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeIssued3.Properties.MaxLength = 400;
            this.MeeIssued3.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeIssued3.Properties.ShowIcon = false;
            this.MeeIssued3.Size = new System.Drawing.Size(241, 20);
            this.MeeIssued3.TabIndex = 38;
            this.MeeIssued3.ToolTip = "F4 : Show/hide text";
            this.MeeIssued3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeIssued3.ToolTipTitle = "Run System";
            // 
            // MeeIssued4
            // 
            this.MeeIssued4.EnterMoveNextControl = true;
            this.MeeIssued4.Location = new System.Drawing.Point(112, 72);
            this.MeeIssued4.Margin = new System.Windows.Forms.Padding(5);
            this.MeeIssued4.Name = "MeeIssued4";
            this.MeeIssued4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIssued4.Properties.Appearance.Options.UseFont = true;
            this.MeeIssued4.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIssued4.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeIssued4.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIssued4.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeIssued4.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIssued4.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeIssued4.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIssued4.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeIssued4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeIssued4.Properties.MaxLength = 400;
            this.MeeIssued4.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeIssued4.Properties.ShowIcon = false;
            this.MeeIssued4.Size = new System.Drawing.Size(241, 20);
            this.MeeIssued4.TabIndex = 39;
            this.MeeIssued4.ToolTip = "F4 : Show/hide text";
            this.MeeIssued4.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeIssued4.ToolTipTitle = "Run System";
            // 
            // MeeOrder
            // 
            this.MeeOrder.EnterMoveNextControl = true;
            this.MeeOrder.Location = new System.Drawing.Point(112, 94);
            this.MeeOrder.Margin = new System.Windows.Forms.Padding(5);
            this.MeeOrder.Name = "MeeOrder";
            this.MeeOrder.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeOrder.Properties.Appearance.Options.UseFont = true;
            this.MeeOrder.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeOrder.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeOrder.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeOrder.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeOrder.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeOrder.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeOrder.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeOrder.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeOrder.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeOrder.Properties.MaxLength = 400;
            this.MeeOrder.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeOrder.Properties.ShowIcon = false;
            this.MeeOrder.Size = new System.Drawing.Size(241, 20);
            this.MeeOrder.TabIndex = 41;
            this.MeeOrder.ToolTip = "F4 : Show/hide text";
            this.MeeOrder.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeOrder.ToolTipTitle = "Run System";
            // 
            // MeeOrder2
            // 
            this.MeeOrder2.EnterMoveNextControl = true;
            this.MeeOrder2.Location = new System.Drawing.Point(112, 116);
            this.MeeOrder2.Margin = new System.Windows.Forms.Padding(5);
            this.MeeOrder2.Name = "MeeOrder2";
            this.MeeOrder2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeOrder2.Properties.Appearance.Options.UseFont = true;
            this.MeeOrder2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeOrder2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeOrder2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeOrder2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeOrder2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeOrder2.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeOrder2.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeOrder2.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeOrder2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeOrder2.Properties.MaxLength = 400;
            this.MeeOrder2.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeOrder2.Properties.ShowIcon = false;
            this.MeeOrder2.Size = new System.Drawing.Size(241, 20);
            this.MeeOrder2.TabIndex = 42;
            this.MeeOrder2.ToolTip = "F4 : Show/hide text";
            this.MeeOrder2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeOrder2.ToolTipTitle = "Run System";
            // 
            // TxtSource
            // 
            this.TxtSource.EnterMoveNextControl = true;
            this.TxtSource.Location = new System.Drawing.Point(123, 73);
            this.TxtSource.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSource.Name = "TxtSource";
            this.TxtSource.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSource.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSource.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSource.Properties.Appearance.Options.UseFont = true;
            this.TxtSource.Size = new System.Drawing.Size(234, 20);
            this.TxtSource.TabIndex = 15;
            // 
            // OD1
            // 
            this.OD1.FileName = "NamaFile";
            // 
            // FrmPL
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(893, 523);
            this.Name = "FrmPL";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd11)).EndInit();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeSM1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSize1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal1.Properties)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd12)).EndInit();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeSM2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSize2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal2.Properties)).EndInit();
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd13)).EndInit();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeSM3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSize3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal3.Properties)).EndInit();
            this.tabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd14)).EndInit();
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeSM4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSize4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal4.Properties)).EndInit();
            this.tabPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd15)).EndInit();
            this.panel12.ResumeLayout(false);
            this.panel12.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeSM5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSize5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal5.Properties)).EndInit();
            this.tabPage6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd16)).EndInit();
            this.panel13.ResumeLayout(false);
            this.panel13.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeSM6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSize6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal6.Properties)).EndInit();
            this.tabPage7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd17)).EndInit();
            this.panel14.ResumeLayout(false);
            this.panel14.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeSM7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSize7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal7.Properties)).EndInit();
            this.tabPage8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd18)).EndInit();
            this.panel15.ResumeLayout(false);
            this.panel15.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeSM8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSize8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal8.Properties)).EndInit();
            this.tabPage9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd19)).EndInit();
            this.panel16.ResumeLayout(false);
            this.panel16.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeSM9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSize9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal9.Properties)).EndInit();
            this.tabPage10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd20)).EndInit();
            this.panel17.ResumeLayout(false);
            this.panel17.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeSM10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSize10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal10.Properties)).EndInit();
            this.tabPage11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd21)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeSM11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSize11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal11.Properties)).EndInit();
            this.tabPage12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd22)).EndInit();
            this.panel18.ResumeLayout(false);
            this.panel18.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeSM12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSize12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal12.Properties)).EndInit();
            this.tabPage13.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode13.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd23)).EndInit();
            this.panel19.ResumeLayout(false);
            this.panel19.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeSM13.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSize13.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt13.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal13.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal13.Properties)).EndInit();
            this.tabPage14.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode14.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd24)).EndInit();
            this.panel20.ResumeLayout(false);
            this.panel20.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeSM14.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSize14.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt14.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal14.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal14.Properties)).EndInit();
            this.tabPage15.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode15.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd25)).EndInit();
            this.panel21.ResumeLayout(false);
            this.panel21.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeSM15.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSize15.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt15.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal15.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal15.Properties)).EndInit();
            this.tabPage16.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode16.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd26)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeSM16.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSize16.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt16.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal16.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal16.Properties)).EndInit();
            this.tabPage17.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode17.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd27)).EndInit();
            this.panel22.ResumeLayout(false);
            this.panel22.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeSM17.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSize17.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt17.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal17.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal17.Properties)).EndInit();
            this.tabPage18.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode18.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd28)).EndInit();
            this.panel23.ResumeLayout(false);
            this.panel23.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeSM18.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSize18.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt18.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal18.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal18.Properties)).EndInit();
            this.tabPage19.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode19.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd29)).EndInit();
            this.panel24.ResumeLayout(false);
            this.panel24.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeSM19.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSize19.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt19.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal19.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal19.Properties)).EndInit();
            this.tabPage20.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode20.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd30)).EndInit();
            this.panel25.ResumeLayout(false);
            this.panel25.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeSM20.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSize20.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt20.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal20.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal20.Properties)).EndInit();
            this.tabPage21.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode21.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd31)).EndInit();
            this.panel26.ResumeLayout(false);
            this.panel26.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeSM21.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSize21.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt21.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal21.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal21.Properties)).EndInit();
            this.tabPage22.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode22.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd32)).EndInit();
            this.panel27.ResumeLayout(false);
            this.panel27.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeSM22.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSize22.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt22.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal22.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal22.Properties)).EndInit();
            this.tabPage23.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode23.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd33)).EndInit();
            this.panel28.ResumeLayout(false);
            this.panel28.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeSM23.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSize23.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt23.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal23.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal23.Properties)).EndInit();
            this.tabPage24.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode24.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd34)).EndInit();
            this.panel29.ResumeLayout(false);
            this.panel29.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeSM24.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSize24.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt24.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal24.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal24.Properties)).EndInit();
            this.tabPage25.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode25.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd35)).EndInit();
            this.panel30.ResumeLayout(false);
            this.panel30.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeSM25.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSize25.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCnt25.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSeal25.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal25.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueNotify.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCtCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LCDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LCDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeAccount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSIDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInvoice.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSalesContract.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLcNo.Properties)).EndInit();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLocalDocNo.Properties)).EndInit();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTab.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkShippingMark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeIssued.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeOrder4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeOrder3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeIssued2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeIssued3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeIssued4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeOrder.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeOrder2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSource.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        protected System.Windows.Forms.Panel panel7;
        protected System.Windows.Forms.Panel panel6;
        private DevExpress.XtraEditors.MemoExEdit MeeIssued;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.MemoExEdit MeeOrder4;
        private System.Windows.Forms.Label label5;
        private DevExpress.XtraEditors.MemoExEdit MeeOrder3;
        private DevExpress.XtraEditors.MemoExEdit MeeIssued2;
        private DevExpress.XtraEditors.MemoExEdit MeeIssued3;
        private DevExpress.XtraEditors.MemoExEdit MeeIssued4;
        private DevExpress.XtraEditors.MemoExEdit MeeOrder;
        private DevExpress.XtraEditors.MemoExEdit MeeOrder2;
        private DevExpress.XtraEditors.TextEdit TxtLcNo;
        private System.Windows.Forms.Label label18;
        private DevExpress.XtraEditors.TextEdit TxtSalesContract;
        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        private DevExpress.XtraEditors.TextEdit TxtInvoice;
        internal DevExpress.XtraEditors.TextEdit TxtSIDocNo;
        private System.Windows.Forms.Label label42;
        internal DevExpress.XtraEditors.DateEdit LCDocDt;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        public DevExpress.XtraEditors.SimpleButton BtnSI2;
        public DevExpress.XtraEditors.SimpleButton BtnSI;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.LookUpEdit LueCtCode;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label60;
        internal DevExpress.XtraEditors.LookUpEdit LueNotify;
        private System.Windows.Forms.Label label61;
        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        protected System.Windows.Forms.Panel panel4;
        protected internal TenTec.Windows.iGridLib.iGrid Grd1;
        private System.Windows.Forms.Button BtnSO;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private DevExpress.XtraEditors.LookUpEdit LueItCode1;
        protected internal TenTec.Windows.iGridLib.iGrid Grd11;
        protected System.Windows.Forms.Panel panel8;
        internal DevExpress.XtraEditors.TextEdit TxtCnt1;
        private System.Windows.Forms.Label label28;
        internal DevExpress.XtraEditors.TextEdit TxtSeal1;
        private System.Windows.Forms.Label label8;
        internal DevExpress.XtraEditors.TextEdit TxtTotal1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TabPage tabPage2;
        protected internal TenTec.Windows.iGridLib.iGrid Grd12;
        protected System.Windows.Forms.Panel panel9;
        internal DevExpress.XtraEditors.TextEdit TxtCnt2;
        private System.Windows.Forms.Label label9;
        internal DevExpress.XtraEditors.TextEdit TxtSeal2;
        private System.Windows.Forms.Label label13;
        internal DevExpress.XtraEditors.TextEdit TxtTotal2;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.TabPage tabPage3;
        protected internal TenTec.Windows.iGridLib.iGrid Grd13;
        protected System.Windows.Forms.Panel panel10;
        internal DevExpress.XtraEditors.TextEdit TxtCnt3;
        private System.Windows.Forms.Label label10;
        internal DevExpress.XtraEditors.TextEdit TxtSeal3;
        private System.Windows.Forms.Label label15;
        internal DevExpress.XtraEditors.TextEdit TxtTotal3;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.TabPage tabPage4;
        protected internal TenTec.Windows.iGridLib.iGrid Grd14;
        protected System.Windows.Forms.Panel panel11;
        internal DevExpress.XtraEditors.TextEdit TxtCnt4;
        private System.Windows.Forms.Label label12;
        internal DevExpress.XtraEditors.TextEdit TxtSeal4;
        private System.Windows.Forms.Label label19;
        internal DevExpress.XtraEditors.TextEdit TxtTotal4;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.TabPage tabPage5;
        protected internal TenTec.Windows.iGridLib.iGrid Grd15;
        protected System.Windows.Forms.Panel panel12;
        internal DevExpress.XtraEditors.TextEdit TxtCnt5;
        private System.Windows.Forms.Label label14;
        internal DevExpress.XtraEditors.TextEdit TxtSeal5;
        private System.Windows.Forms.Label label21;
        internal DevExpress.XtraEditors.TextEdit TxtTotal5;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.TabPage tabPage6;
        protected internal TenTec.Windows.iGridLib.iGrid Grd16;
        protected System.Windows.Forms.Panel panel13;
        internal DevExpress.XtraEditors.TextEdit TxtCnt6;
        private System.Windows.Forms.Label label16;
        internal DevExpress.XtraEditors.TextEdit TxtSeal6;
        private System.Windows.Forms.Label label23;
        internal DevExpress.XtraEditors.TextEdit TxtTotal6;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.TabPage tabPage7;
        protected internal TenTec.Windows.iGridLib.iGrid Grd17;
        protected System.Windows.Forms.Panel panel14;
        internal DevExpress.XtraEditors.TextEdit TxtCnt7;
        private System.Windows.Forms.Label label20;
        internal DevExpress.XtraEditors.TextEdit TxtSeal7;
        private System.Windows.Forms.Label label25;
        internal DevExpress.XtraEditors.TextEdit TxtTotal7;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.TabPage tabPage8;
        protected internal TenTec.Windows.iGridLib.iGrid Grd18;
        protected System.Windows.Forms.Panel panel15;
        internal DevExpress.XtraEditors.TextEdit TxtCnt8;
        private System.Windows.Forms.Label label22;
        internal DevExpress.XtraEditors.TextEdit TxtSeal8;
        private System.Windows.Forms.Label label27;
        internal DevExpress.XtraEditors.TextEdit TxtTotal8;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.TabPage tabPage9;
        protected internal TenTec.Windows.iGridLib.iGrid Grd19;
        protected System.Windows.Forms.Panel panel16;
        internal DevExpress.XtraEditors.TextEdit TxtCnt9;
        private System.Windows.Forms.Label label24;
        internal DevExpress.XtraEditors.TextEdit TxtSeal9;
        private System.Windows.Forms.Label label29;
        internal DevExpress.XtraEditors.TextEdit TxtTotal9;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.TabPage tabPage10;
        protected internal TenTec.Windows.iGridLib.iGrid Grd20;
        protected System.Windows.Forms.Panel panel17;
        internal DevExpress.XtraEditors.TextEdit TxtCnt10;
        private System.Windows.Forms.Label label26;
        internal DevExpress.XtraEditors.TextEdit TxtSeal10;
        private System.Windows.Forms.Label label31;
        internal DevExpress.XtraEditors.TextEdit TxtTotal10;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.TabPage tabPage11;
        protected internal TenTec.Windows.iGridLib.iGrid Grd21;
        protected System.Windows.Forms.Panel panel5;
        internal DevExpress.XtraEditors.TextEdit TxtCnt11;
        private System.Windows.Forms.Label label30;
        internal DevExpress.XtraEditors.TextEdit TxtSeal11;
        private System.Windows.Forms.Label label32;
        internal DevExpress.XtraEditors.TextEdit TxtTotal11;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.TabPage tabPage12;
        protected internal TenTec.Windows.iGridLib.iGrid Grd22;
        protected System.Windows.Forms.Panel panel18;
        internal DevExpress.XtraEditors.TextEdit TxtCnt12;
        private System.Windows.Forms.Label label33;
        internal DevExpress.XtraEditors.TextEdit TxtSeal12;
        private System.Windows.Forms.Label label34;
        internal DevExpress.XtraEditors.TextEdit TxtTotal12;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.TabPage tabPage13;
        protected internal TenTec.Windows.iGridLib.iGrid Grd23;
        protected System.Windows.Forms.Panel panel19;
        internal DevExpress.XtraEditors.TextEdit TxtCnt13;
        private System.Windows.Forms.Label label35;
        internal DevExpress.XtraEditors.TextEdit TxtSeal13;
        private System.Windows.Forms.Label label36;
        internal DevExpress.XtraEditors.TextEdit TxtTotal13;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.TabPage tabPage14;
        protected internal TenTec.Windows.iGridLib.iGrid Grd24;
        protected System.Windows.Forms.Panel panel20;
        internal DevExpress.XtraEditors.TextEdit TxtCnt14;
        private System.Windows.Forms.Label label37;
        internal DevExpress.XtraEditors.TextEdit TxtSeal14;
        private System.Windows.Forms.Label label38;
        internal DevExpress.XtraEditors.TextEdit TxtTotal14;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.TabPage tabPage15;
        protected internal TenTec.Windows.iGridLib.iGrid Grd25;
        protected System.Windows.Forms.Panel panel21;
        internal DevExpress.XtraEditors.TextEdit TxtCnt15;
        private System.Windows.Forms.Label label39;
        internal DevExpress.XtraEditors.TextEdit TxtSeal15;
        private System.Windows.Forms.Label label40;
        internal DevExpress.XtraEditors.TextEdit TxtTotal15;
        private System.Windows.Forms.Label label55;
        private DevExpress.XtraEditors.LookUpEdit LueItCode2;
        private DevExpress.XtraEditors.LookUpEdit LueItCode3;
        private DevExpress.XtraEditors.LookUpEdit LueItCode4;
        private DevExpress.XtraEditors.LookUpEdit LueItCode5;
        private DevExpress.XtraEditors.LookUpEdit LueItCode6;
        private DevExpress.XtraEditors.LookUpEdit LueItCode7;
        private DevExpress.XtraEditors.LookUpEdit LueItCode8;
        private DevExpress.XtraEditors.LookUpEdit LueItCode9;
        private DevExpress.XtraEditors.LookUpEdit LueItCode10;
        private DevExpress.XtraEditors.LookUpEdit LueItCode11;
        private DevExpress.XtraEditors.LookUpEdit LueItCode12;
        private DevExpress.XtraEditors.LookUpEdit LueItCode13;
        private DevExpress.XtraEditors.LookUpEdit LueItCode14;
        private DevExpress.XtraEditors.LookUpEdit LueItCode15;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private System.Windows.Forms.Label label56;
        internal DevExpress.XtraEditors.LookUpEdit LueSize1;
        private System.Windows.Forms.Label label57;
        internal DevExpress.XtraEditors.LookUpEdit LueSize2;
        private System.Windows.Forms.Label label58;
        internal DevExpress.XtraEditors.LookUpEdit LueSize3;
        private System.Windows.Forms.Label label59;
        internal DevExpress.XtraEditors.LookUpEdit LueSize4;
        private System.Windows.Forms.Label label62;
        internal DevExpress.XtraEditors.LookUpEdit LueSize5;
        private System.Windows.Forms.Label label63;
        internal DevExpress.XtraEditors.LookUpEdit LueSize6;
        private System.Windows.Forms.Label label64;
        internal DevExpress.XtraEditors.LookUpEdit LueSize7;
        private System.Windows.Forms.Label label65;
        internal DevExpress.XtraEditors.LookUpEdit LueSize8;
        private System.Windows.Forms.Label label66;
        internal DevExpress.XtraEditors.LookUpEdit LueSize9;
        private System.Windows.Forms.Label label67;
        internal DevExpress.XtraEditors.LookUpEdit LueSize10;
        private System.Windows.Forms.Label label68;
        internal DevExpress.XtraEditors.LookUpEdit LueSize11;
        private System.Windows.Forms.Label label69;
        internal DevExpress.XtraEditors.LookUpEdit LueSize12;
        private System.Windows.Forms.Label label70;
        internal DevExpress.XtraEditors.LookUpEdit LueSize13;
        private System.Windows.Forms.Label label71;
        internal DevExpress.XtraEditors.LookUpEdit LueSize14;
        private System.Windows.Forms.Label label72;
        internal DevExpress.XtraEditors.LookUpEdit LueSize15;
        private System.Windows.Forms.Label label73;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark2;
        private System.Windows.Forms.Label label74;
        public DevExpress.XtraEditors.SimpleButton BtnSource;
        internal DevExpress.XtraEditors.TextEdit TxtSource;
        private System.Windows.Forms.Label label75;
        private DevExpress.XtraEditors.CheckEdit ChkShippingMark;
        private DevExpress.XtraEditors.TextEdit TxtLocalDocNo;
        private System.Windows.Forms.Label LblLocalDoc;
        private System.Windows.Forms.TabPage tabPage16;
        protected internal TenTec.Windows.iGridLib.iGrid Grd26;
        protected System.Windows.Forms.Panel panel3;
        internal DevExpress.XtraEditors.LookUpEdit LueSize16;
        private System.Windows.Forms.Label label77;
        internal DevExpress.XtraEditors.TextEdit TxtCnt16;
        private System.Windows.Forms.Label label78;
        internal DevExpress.XtraEditors.TextEdit TxtSeal16;
        private System.Windows.Forms.Label label79;
        internal DevExpress.XtraEditors.TextEdit TxtTotal16;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.TabPage tabPage17;
        protected System.Windows.Forms.Panel panel22;
        internal DevExpress.XtraEditors.LookUpEdit LueSize17;
        private System.Windows.Forms.Label label81;
        internal DevExpress.XtraEditors.TextEdit TxtCnt17;
        private System.Windows.Forms.Label label82;
        internal DevExpress.XtraEditors.TextEdit TxtSeal17;
        private System.Windows.Forms.Label label83;
        internal DevExpress.XtraEditors.TextEdit TxtTotal17;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.TabPage tabPage18;
        protected System.Windows.Forms.Panel panel23;
        internal DevExpress.XtraEditors.LookUpEdit LueSize18;
        private System.Windows.Forms.Label label85;
        internal DevExpress.XtraEditors.TextEdit TxtCnt18;
        private System.Windows.Forms.Label label86;
        internal DevExpress.XtraEditors.TextEdit TxtSeal18;
        private System.Windows.Forms.Label label87;
        internal DevExpress.XtraEditors.TextEdit TxtTotal18;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.TabPage tabPage19;
        protected System.Windows.Forms.Panel panel24;
        internal DevExpress.XtraEditors.LookUpEdit LueSize19;
        private System.Windows.Forms.Label label89;
        internal DevExpress.XtraEditors.TextEdit TxtCnt19;
        private System.Windows.Forms.Label label90;
        internal DevExpress.XtraEditors.TextEdit TxtSeal19;
        private System.Windows.Forms.Label label91;
        internal DevExpress.XtraEditors.TextEdit TxtTotal19;
        private System.Windows.Forms.Label label92;
        private System.Windows.Forms.TabPage tabPage20;
        protected System.Windows.Forms.Panel panel25;
        internal DevExpress.XtraEditors.LookUpEdit LueSize20;
        private System.Windows.Forms.Label label93;
        internal DevExpress.XtraEditors.TextEdit TxtCnt20;
        private System.Windows.Forms.Label label94;
        internal DevExpress.XtraEditors.TextEdit TxtSeal20;
        private System.Windows.Forms.Label label95;
        internal DevExpress.XtraEditors.TextEdit TxtTotal20;
        private System.Windows.Forms.Label label96;
        private System.Windows.Forms.TabPage tabPage21;
        protected System.Windows.Forms.Panel panel26;
        internal DevExpress.XtraEditors.LookUpEdit LueSize21;
        private System.Windows.Forms.Label label97;
        internal DevExpress.XtraEditors.TextEdit TxtCnt21;
        private System.Windows.Forms.Label label98;
        internal DevExpress.XtraEditors.TextEdit TxtSeal21;
        private System.Windows.Forms.Label label99;
        internal DevExpress.XtraEditors.TextEdit TxtTotal21;
        private System.Windows.Forms.Label label100;
        private System.Windows.Forms.TabPage tabPage22;
        protected System.Windows.Forms.Panel panel27;
        internal DevExpress.XtraEditors.LookUpEdit LueSize22;
        private System.Windows.Forms.Label label101;
        internal DevExpress.XtraEditors.TextEdit TxtCnt22;
        private System.Windows.Forms.Label label102;
        internal DevExpress.XtraEditors.TextEdit TxtSeal22;
        private System.Windows.Forms.Label label103;
        internal DevExpress.XtraEditors.TextEdit TxtTotal22;
        private System.Windows.Forms.Label label104;
        private System.Windows.Forms.TabPage tabPage23;
        protected System.Windows.Forms.Panel panel28;
        internal DevExpress.XtraEditors.LookUpEdit LueSize23;
        private System.Windows.Forms.Label label105;
        internal DevExpress.XtraEditors.TextEdit TxtCnt23;
        private System.Windows.Forms.Label label106;
        internal DevExpress.XtraEditors.TextEdit TxtSeal23;
        private System.Windows.Forms.Label label107;
        internal DevExpress.XtraEditors.TextEdit TxtTotal23;
        private System.Windows.Forms.Label label108;
        private System.Windows.Forms.TabPage tabPage24;
        protected System.Windows.Forms.Panel panel29;
        internal DevExpress.XtraEditors.LookUpEdit LueSize24;
        private System.Windows.Forms.Label label109;
        internal DevExpress.XtraEditors.TextEdit TxtCnt24;
        private System.Windows.Forms.Label label110;
        internal DevExpress.XtraEditors.TextEdit TxtSeal24;
        private System.Windows.Forms.Label label111;
        internal DevExpress.XtraEditors.TextEdit TxtTotal24;
        private System.Windows.Forms.Label label112;
        private System.Windows.Forms.TabPage tabPage25;
        protected System.Windows.Forms.Panel panel30;
        internal DevExpress.XtraEditors.LookUpEdit LueSize25;
        private System.Windows.Forms.Label label113;
        internal DevExpress.XtraEditors.TextEdit TxtCnt25;
        private System.Windows.Forms.Label label114;
        internal DevExpress.XtraEditors.TextEdit TxtSeal25;
        private System.Windows.Forms.Label label115;
        internal DevExpress.XtraEditors.TextEdit TxtTotal25;
        private System.Windows.Forms.Label label116;
        protected internal TenTec.Windows.iGridLib.iGrid Grd27;
        protected internal TenTec.Windows.iGridLib.iGrid Grd28;
        protected internal TenTec.Windows.iGridLib.iGrid Grd29;
        protected internal TenTec.Windows.iGridLib.iGrid Grd30;
        protected internal TenTec.Windows.iGridLib.iGrid Grd31;
        protected internal TenTec.Windows.iGridLib.iGrid Grd32;
        protected internal TenTec.Windows.iGridLib.iGrid Grd33;
        protected internal TenTec.Windows.iGridLib.iGrid Grd34;
        protected internal TenTec.Windows.iGridLib.iGrid Grd35;
        private DevExpress.XtraEditors.LookUpEdit LueItCode16;
        private DevExpress.XtraEditors.LookUpEdit LueItCode17;
        private DevExpress.XtraEditors.LookUpEdit LueItCode18;
        private DevExpress.XtraEditors.LookUpEdit LueItCode19;
        private DevExpress.XtraEditors.LookUpEdit LueItCode20;
        private DevExpress.XtraEditors.LookUpEdit LueItCode21;
        private DevExpress.XtraEditors.LookUpEdit LueItCode22;
        private DevExpress.XtraEditors.LookUpEdit LueItCode23;
        private DevExpress.XtraEditors.LookUpEdit LueItCode24;
        private DevExpress.XtraEditors.LookUpEdit LueItCode25;
        private System.Windows.Forms.Label label117;
        internal DevExpress.XtraEditors.TextEdit TxtTab;
        private DevExpress.XtraEditors.MemoExEdit MeeSM1;
        private System.Windows.Forms.Label label119;
        private DevExpress.XtraEditors.MemoExEdit MeeSM2;
        private System.Windows.Forms.Label label120;
        private DevExpress.XtraEditors.MemoExEdit MeeSM3;
        private System.Windows.Forms.Label label121;
        private DevExpress.XtraEditors.MemoExEdit MeeSM4;
        private System.Windows.Forms.Label label122;
        private DevExpress.XtraEditors.MemoExEdit MeeSM5;
        private System.Windows.Forms.Label label123;
        private DevExpress.XtraEditors.MemoExEdit MeeSM6;
        private System.Windows.Forms.Label label124;
        private DevExpress.XtraEditors.MemoExEdit MeeSM7;
        private System.Windows.Forms.Label label125;
        private DevExpress.XtraEditors.MemoExEdit MeeSM8;
        private System.Windows.Forms.Label label126;
        private DevExpress.XtraEditors.MemoExEdit MeeSM9;
        private System.Windows.Forms.Label label127;
        private DevExpress.XtraEditors.MemoExEdit MeeSM10;
        private System.Windows.Forms.Label label128;
        private DevExpress.XtraEditors.MemoExEdit MeeSM11;
        private System.Windows.Forms.Label label129;
        private DevExpress.XtraEditors.MemoExEdit MeeSM12;
        private System.Windows.Forms.Label label130;
        private DevExpress.XtraEditors.MemoExEdit MeeSM13;
        private System.Windows.Forms.Label label131;
        private DevExpress.XtraEditors.MemoExEdit MeeSM14;
        private System.Windows.Forms.Label label132;
        private DevExpress.XtraEditors.MemoExEdit MeeSM15;
        private System.Windows.Forms.Label label133;
        private DevExpress.XtraEditors.MemoExEdit MeeSM16;
        private System.Windows.Forms.Label label134;
        private DevExpress.XtraEditors.MemoExEdit MeeSM17;
        private System.Windows.Forms.Label label135;
        private DevExpress.XtraEditors.MemoExEdit MeeSM18;
        private System.Windows.Forms.Label label136;
        private DevExpress.XtraEditors.MemoExEdit MeeSM19;
        private System.Windows.Forms.Label label137;
        private DevExpress.XtraEditors.MemoExEdit MeeSM20;
        private System.Windows.Forms.Label label138;
        private DevExpress.XtraEditors.MemoExEdit MeeSM21;
        private System.Windows.Forms.Label label139;
        private DevExpress.XtraEditors.MemoExEdit MeeSM22;
        private System.Windows.Forms.Label label140;
        private DevExpress.XtraEditors.MemoExEdit MeeSM23;
        private System.Windows.Forms.Label label141;
        private DevExpress.XtraEditors.MemoExEdit MeeSM24;
        private System.Windows.Forms.Label label142;
        private DevExpress.XtraEditors.MemoExEdit MeeSM25;
        private System.Windows.Forms.Label label143;
        public DevExpress.XtraEditors.MemoExEdit MeeAccount;
        public DevExpress.XtraEditors.SimpleButton BtnDownload;
        private System.Windows.Forms.SaveFileDialog SFD1;
        private System.Windows.Forms.OpenFileDialog OD1;
    }
}