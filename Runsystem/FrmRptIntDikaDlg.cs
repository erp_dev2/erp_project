﻿#region Update
/*
 * 13/12/2022 [HAR/PHT] New Apps
 * 24/01/2023 [HAR/PHT] merubah urutan journal
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using System.Drawing;
using System.Text;

using TenTec.Windows.iGridLib;
using MySql.Data.MySqlClient;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
#endregion

namespace RunSystem
{
    public partial class FrmRptIntDikaDlg : RunSystem.FrmBase9
    {
        #region Field

        private FrmRptIntDika mFrmParent;
        private string mVCDocNo = string.Empty;
        private string mSQL = string.Empty;
        private string mMenuCode = string.Empty;

        #endregion

        #region Constructor

        public FrmRptIntDikaDlg(FrmRptIntDika FrmParent, string VCDocNo)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mVCDocNo = VCDocNo;
        }

        #endregion

        #region Methods

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            SetFormControl(mState.View);
            SetGrd(); ShowData();
        }

        private void SetFormControl(RunSystem.mState state)
        {

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                       
                    }, true);
                    break;
            }
        }

        private void ClearData()
        {
            
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 5;
            Grd1.FrozenArea.ColCount = 1;
            Sm.GrdHdr(
                    Grd1, new string[]
                    {
                        //0
                        "No",

                        //1-3
                        "Journal",
                        "",
                        "Cost Center",
                        "Remark"
                    }
                );
            Sm.SetGrdProperty(Grd1, true);
            Sm.GrdColButton(Grd1, new int[] { 2 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 3, 4 });
        }

        private void HideInfoInGrd()
        {
            Sm.SetGrdAutoSize(Grd1);
        }

        #endregion

        #region Show data
        public void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("SELECT B.Dno, B.JournalDocNo, D.CCName, C.Remark ");
                SQL.AppendLine("FROM tblDikaHdr A ");
                SQL.AppendLine("INNER JOIN tblDikadtl B ON A.DocDt = B.DocDt AND A.ReceiptNo = B.ReceiptNo ");
                SQL.AppendLine("INNER JOIN tbljournalhdr C ON B.JournalDocNo = C.DocNo ");
                SQL.AppendLine("INNER JOIN tblcostcenter D ON C.CCCode = D.CCCode ");

                mSQL = SQL.ToString();

                string Filter = "WHERE A.VCDocNo = '" + mVCDocNo + "'  ";

                var cm = new MySqlCommand();

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + "ORDER BY  B.DNo ",
                        new string[]
                        {
                            //0
                             "Dno",
                            //1-5
                            "JournalDocNo", "CCName", "Remark"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 0, 0);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 1);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                        }, false, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #endregion

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmJournal(mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }
    }
}
