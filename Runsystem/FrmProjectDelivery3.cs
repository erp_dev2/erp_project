﻿#region Update
/*
    21/08/2018 [WED] ambil dari project implementation yang status nya approved
    02/11/2018 [DITA] project implementation yg bisa di deliver hanya yg sudah final
    06/11/2018 [MEY] penambahan filter nama project
    08/11/2018 [MEY] ubah filter nama project ke combobox
    09/11/2018 [MEY] list LueProject ambil dari LOP yang sudah di Project Implementation kan
    17/12/2018 [WED] Prosentase PRJI dibagi 100
    18/12/2018 [TKG] Update SettleDt
    23/01/2019 [HAR] digit bobot jadi 8
    07/08/2019 [WED] update settledAmt di ProjectImplementation
    08/08/2019 [WED] alur nya ditambah dari SO Contract Revision
    13/08/2019 [WED] validasi, tidak bisa save jika SO Contract Revision yang digunakan oleh Project Implementation menggunakan SO Contract Revision yang lama
    28/08/2019 [DITA] BUG Filter customer
    29/08/2019 [DITA] BUG Nama project tidak muncul saat filter
 * 
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmProjectDelivery3 : RunSystem.FrmBase5
    {
        #region Field

        internal string mMenuCode = string.Empty, mSQL = string.Empty;
        private bool mIsFilterBySite = false;
        
        #endregion

        #region Constructor

        public FrmProjectDelivery3(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                GetParameter();
                Sl.SetLueCtCode(ref LueCtCode);
                SetLueProjectName(ref LueProjectName);
                SetGrd();
                SetSQL();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mIsFilterBySite = Sm.GetParameterBoo("IsFilterBySite");
        }

        private void SetLueProjectName(ref DXE.LookUpEdit Lue)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select Distinct A.DocNo as 'Col1', A.ProjectName as 'Col2' from TblLopHdr A  ");
                SQL.AppendLine("Inner Join TblBOQHdr B ON A.DocNo=B.LOPDocNo  ");
                SQL.AppendLine("Inner Join TblSOContractHdr C ON B.DocNo=C.BOQDocNo  ");
                SQL.AppendLine("Inner Join  ");
                SQL.AppendLine("( ");
	            SQL.AppendLine("    Select Distinct T3.SOCDocNo ");
	            SQL.AppendLine("    From TblProjectImplementationHdr T1 ");
	            SQL.AppendLine("    Inner Join TblProjectImplementationDtl T2 On T1.DocNo = T2.DocNo And T1.CancelInd = 'N'  ");
	            SQL.AppendLine("    And T2.SettledInd = 'N' And T1.ProcessInd = 'F' And T1.Status = 'A' ");
                SQL.AppendLine("    Inner Join TblSOContractRevisionHdr T3 On T1.SOCOntractDocNo = T3.DocNo ");
                SQL.AppendLine(") D ON C.DocNo=D.SOCDocNo ");
                if (mIsFilterBySite)
                {
                    SQL.AppendLine("Inner Join TblLOPHdr E On B.LOPDocNo=E.DocNo ");
                    SQL.AppendLine("    And (E.SiteCode Is Null Or ( ");
                    SQL.AppendLine("    E.SiteCode Is Not Null ");
                    SQL.AppendLine("    And Exists( ");
                    SQL.AppendLine("        Select 1 From TblGroupSite ");
                    SQL.AppendLine("        Where SiteCode=IfNull(E.SiteCode, '') ");
                    SQL.AppendLine("        And GrpCode In ( ");
                    SQL.AppendLine("            Select GrpCode From TblUser ");
                    SQL.AppendLine("            Where UserCode=@UserCode ");
                    SQL.AppendLine("            ) ");
                    SQL.AppendLine("        ) ");
                    SQL.AppendLine("    )) ");
                }
                SQL.AppendLine("Order by A.ProjectName ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Type", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }
        #endregion

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DNo, A.StageCode, C.StageName, A.TaskCode, D.TaskName,");
            SQL.AppendLine("A.BobotPercentage, B.SOContractDocNo As SOCRDocNo, Left(B.SOContractDocNo, 17) SOContractDocNo ");
            SQL.AppendLine(" From tblprojectimplementationdtl A ");
            SQL.AppendLine("Inner Join tblprojectimplementationhdr B On A.DocNo = B.DocNo ");
	        SQL.AppendLine("    And B.CancelInd = 'N' And B.Status = 'A' And A.SettledInd = 'N' And A.InvoicedInd = 'N' and B.ProcessInd='F' ");
            SQL.AppendLine("Inner Join tblprojectstage C On A.StageCode = C.StageCode ");
            SQL.AppendLine("Inner Join tblprojecttask D On A.TaskCode = D.TaskCode ");
			SQL.AppendLine("Inner Join tblsocontractrevisionhdr E On B.SOContractDocNo = E.DocNo ");
            SQL.AppendLine("Inner Join TblSOContractHdr E1 On E.SOCDocNo = E1.DocNo ");
		    SQL.AppendLine("Inner Join tblboqhdr F On F.DocNo=E1.BOQDocNo ");
            SQL.AppendLine("Inner Join tbllophdr G On G.DocNo=F.LOPDocNo ");
            if (mIsFilterBySite)
            {
                SQL.AppendLine("And (G.SiteCode Is Null Or ( ");
                SQL.AppendLine("    G.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(G.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("            ) ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine(")) ");
            }
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 11;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "Settled",
                    
                    //1-5
                    "Document#", 
                    "",
                    "DNo",
                    "Stage",
                    "Task",

                    //6-10
                    "Bobot (%)",
                    "Remark",
                    "SO Contract",
                    "SO Revision" + Environment.NewLine + "(Existing)",
                    "SO Revision" + Environment.NewLine + "(Latest)"
                },
                new int[] 
                {
                    //0
                    60,

                    //1-5
                    150, 20, 0, 120, 120, 
                    
                    //6-10
                    100,200, 100, 180, 180
                }
            );
            Sm.GrdColCheck(Grd1, new int[] { 0 });
            Sm.GrdColButton(Grd1, new int[] { 2 });
            Sm.GrdFormatDec(Grd1, new int[] { 6 }, 8);
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 8 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 3, 4, 5, 6, 8, 9, 10 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " Where 0 = 0 ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueProjectName), "F.LOPDocNo", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCode), "E1.CtCode", true);
             
                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By A.DocNo, A.DNo; ",
                    new string[]
                    {
                        //0
                        "DocNo",

                        //1-5
                        "DNo", "StageName", "TaskName", "BobotPercentage", "SOContractDocNo",

                        //6
                        "SOCRDocNo"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = false;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Settled Data

        override protected void SaveData()
        {
            try
            {
                UpdateLatestSOContractRevision();
                if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var cml = new List<MySqlCommand>();
                var l = new List<PrImpl1>();

                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    if (Sm.GetGrdBool(Grd1, r, 0))
                    {
                        cml.Add(SettledTask(Sm.GetGrdStr(Grd1, r, 1), Sm.GetGrdStr(Grd1, r, 3), Sm.GetGrdStr(Grd1, r, 7)));

                        l.Add(new PrImpl1()
                        {
                            DocNo = Sm.GetGrdStr(Grd1, r, 1),
                            Bobot = Sm.GetGrdDec(Grd1, r, 6)
                        });
                    }
                }

                var result = from value in l
                             group value.Bobot by new
                             {
                                 value.DocNo
                             } into grouped
                             select new PrImpl2()
                             {
                                 DocNo = grouped.Key.DocNo,
                                 BobotSum = grouped.Sum()
                             };

                foreach (PrImpl2 x in result)
                {
                    cml.Add(UpdateAchievement(x.DocNo, x.BobotSum));
                }

                Sm.ExecCommands(cml);
                ShowData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private bool IsDataNotValid()
        {
            return
                IsDataNotExisted() ||
                IsProjectImplementationAlreadyCancelled() ||
                IsTaskAlreadySettled() ||
                IsSOContractRevisionOutdated();
        }

        private bool IsSOContractRevisionOutdated()
        {
            for (int i = 0; i < Grd1.Rows.Count; i++)
            {
                if(Sm.GetGrdBool(Grd1, i, 0))
                {
                    if (Sm.GetGrdStr(Grd1, i, 9) != Sm.GetGrdStr(Grd1, i, 10))
                    {
                        Sm.StdMsg(mMsgType.Warning, "This document needs revision, due to outdated SO Contract Revision.");
                        Sm.FocusGrd(Grd1, i, 1);
                        return true;
                    }
                }
            }

            return false;
        }

        private bool IsDataNotExisted()
        {
            for (int r = 0; r < Grd1.Rows.Count; r++)
                if (Sm.GetGrdBool(Grd1, r, 0)) return false;

            Sm.StdMsg(mMsgType.Warning, "You need to settle at least 1 Task.");
            return true;
        }

        private bool IsTaskAlreadySettled()
        {
            for (int i = 0; i < Grd1.Rows.Count; i++)
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select DocNo ");
                SQL.AppendLine("From TblProjectImplementationDtl ");
                SQL.AppendLine("Where DocNo = @Param1 ");
                SQL.AppendLine("And DNo = @Param2 ");
                SQL.AppendLine("And SettledInd = @Param3 ");
                SQL.AppendLine("Limit 1; ");

                if (Sm.IsDataExist(SQL.ToString(), Sm.GetGrdStr(Grd1, i, 1), Sm.GetGrdStr(Grd1, i, 3), "Y"))
                {
                    Sm.StdMsg(mMsgType.Warning, "This data is already settled");
                    Sm.FocusGrd(Grd1, i, 1);
                    return true;
                }
            }

            return false;
        }

        private bool IsProjectImplementationAlreadyCancelled()
        {
            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdBool(Grd1, r, 0) && IsProjectImplementationAlreadyCancelled(Sm.GetGrdStr(Grd1, r, 1)))
                {
                    Sm.StdMsg(mMsgType.Warning,
                        "Project Implementation : " + Sm.GetGrdStr(Grd1, r, 1) + Environment.NewLine +
                        "This document already cancelled.");
                    return true;
                }
            }
            return false;
        }

        private bool IsProjectImplementationAlreadyCancelled(string DocNo)
        {
            return Sm.IsDataExist("Select DocNo From TblProjectImplementationHdr Where DocNo=@Param And (CancelInd='Y' Or Status = 'C'); ", DocNo);
        }

        private MySqlCommand SettledTask(string DocNo, string DNo, string Remark)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblProjectImplementationDtl Set ");
            SQL.AppendLine("    SettledInd='Y', ");
            SQL.AppendLine("    SettledAmt = EstimatedAmt, ");
            SQL.AppendLine("    SettleDt=Replace(CurDate(), '-', ''), ");
            SQL.AppendLine("    Remark=@Remark, ");
            SQL.AppendLine("    LastUpBy=@UserCode, ");
            SQL.AppendLine("    LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And DNo = @DNo And SettledInd='N' ");
            SQL.AppendLine("And Exists (Select 1 From TblProjectImplementationHdr T Where T.DocNo = @DocNo And T.CancelInd = 'N' And T.Status = 'A' Limit 1) ");
            SQL.AppendLine("; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", DNo);
            Sm.CmParam<String>(ref cm, "@Remark", Remark);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand UpdateAchievement(string DocNo, decimal Bobot)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblProjectImplementationHdr ");
            SQL.AppendLine("    Set Achievement = Achievement + @Bobot, LastUpBy = @UserCode, LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("Where DocNo = @DocNo ");
            SQL.AppendLine("And CancelInd = 'N' And Status = 'A'; ");

            SQL.AppendLine("Update TblProjectImplementationHdr ");
            SQL.AppendLine("    Set Achievement = 100 ");
            SQL.AppendLine("Where DocNo = @DocNo ");
            SQL.AppendLine("And Achievement > 99.9; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<Decimal>(ref cm, "@Bobot", Bobot);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;            
        }

        #endregion

        #region Grid Methods

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmProjectImplementation(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmProjectImplementation(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                //Grd1.Cells[e.RowIndex, 0].Value = !Sm.GetGrdBool(Grd1, e.RowIndex, 0);
            }
        }

        private void Grd1_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 0);
                for (int r = 0; r < Grd1.Rows.Count; r++)
                    if (Sm.GetGrdStr(Grd1, r, 1).Length > 0)
                        Grd1.Cells[r, 0].Value = !IsSelected;
            }
        }

        #endregion

        #region Additional Methods

        private void UpdateLatestSOContractRevision()
        {
            string mSOCDocNo = string.Empty;

            for (int i = 0; i < Grd1.Rows.Count; i++)
            {
                if (Sm.GetGrdStr(Grd1, i, 1).Length > 0)
                {
                    if (mSOCDocNo.Length > 0) mSOCDocNo += ",";
                    mSOCDocNo += Sm.GetGrdStr(Grd1, i, 8);
                }
            }

            if (mSOCDocNo.Length > 0)
            {
                var SQL = new StringBuilder();
                var cm = new MySqlCommand();
                var lx = new List<SOCR>();

                SQL.AppendLine("Select SOCDocNo, Max(DocNo) SOCRDocNo ");
                SQL.AppendLine("From TblSOContractRevisionHdr ");
                SQL.AppendLine("Where Find_In_Set(SOCDocNo, @SOCDocNo) ");
                SQL.AppendLine("Group By SOCDocNo; ");

                Sm.CmParam<String>(ref cm, "@SOCDocNo", mSOCDocNo);

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandTimeout = 600;
                    cm.CommandText = SQL.ToString();
                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] { "SOCDocNo", "SOCRDocNo" });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            lx.Add(new SOCR() 
                            {
                                SOCDocNo = Sm.DrStr(dr, c[0]),
                                SOCRDocNo = Sm.DrStr(dr, c[1]),
                            });
                        }
                    }
                    dr.Close();
                }

                if (lx.Count > 0)
                {
                    for (int i = 0; i < lx.Count; i++)
                    {
                        for (int j = 0; j < Grd1.Rows.Count; j++)
                        {
                            if (lx[i].SOCDocNo == Sm.GetGrdStr(Grd1, j, 8))
                            {
                                Grd1.Cells[j, 10].Value = lx[i].SOCRDocNo;
                                //break;
                            }
                        }
                    }
                }
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");
        }
        private void LueProjectName_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueProjectName, new Sm.RefreshLue1(SetLueProjectName));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkProjectName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Project");
        }

        #endregion

        #endregion

        #region Class

        class PrImpl1
        {
            public string DocNo { get; set; }
            public decimal Bobot { get; set; }

        }

        class PrImpl2
        {
            public string DocNo { get; set; }
            public decimal BobotSum { get; set; }
        }

        class SOCR
        {
            public string SOCDocNo { get; set; }
            public string SOCRDocNo { get; set; }
        }

        #endregion
   
    }
}
