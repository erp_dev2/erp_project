﻿#region Update 
/*  
    17/02/2020 [VIN/ALL] new apps 
    27/03/2020 [DITA/ALL] tambah validasi saat mengisi factur number
    19/05/2020 [WED/SIER] tambah fitur generate factur number berdasarkan start dan end
  */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmFacturNumber : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "";
        internal FrmFacturNumberFind FrmFind;
        private string mFirstDelimiterFacturNo = string.Empty, mSecondDelimiterFacturNo = string.Empty;

        #endregion

        #region Constructor

        public FrmFacturNumber(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method 

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Factur Number";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                BtnPrint.Visible = false;
                GetParameter();
                base.FrmLoad(sender, e);
                SetGrd();

                SetFormControl(mState.View);
                base.FrmLoad(sender, e);

                
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 3;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "Factur Number",

                        //1-2
                        "Used",
                        "Sales Invoice#"
                       
                    },
                    new int[] 
                    {
                        //0
                        150,

                        //1-2
                        100, 100
                    }

                );
            
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            //Sm.SetGrdProperty(Grd1, false);
            Sm.GrdColReadOnly(Grd1, new int[] { 1,2 });
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt,  MeeRemark, TxtStart1, TxtStart2, TxtStart3, TxtEnd1, TxtEnd2, TxtEnd3
                    }, true);
                    Grd1.ReadOnly = true;
                    BtnImportWBS.Enabled = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, MeeRemark, TxtStart1, TxtStart2, TxtStart3, TxtEnd1, TxtEnd2, TxtEnd3
                    }, false);
                    Grd1.ReadOnly = false;
                    BtnImportWBS.Enabled = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, MeeRemark, TxtStart1, TxtStart2, TxtStart3, TxtEnd1, TxtEnd2, TxtEnd3
                    }, false);
                    Grd1.ReadOnly = false;
                    BtnImportWBS.Enabled = true;
                    DteDocDt.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, MeeRemark, TxtStart1, TxtStart2, TxtStart3, TxtEnd1, TxtEnd2, TxtEnd3
            });
            Sm.ClearGrd(Grd1, true);
            Sm.FocusGrd(Grd1, 0, 0);

        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmFacturNumberFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }
        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                DteDocDt.DateTime = Sm.ConvertDate(Sm.ServerCurrentDateTime());
                
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }
        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.View);
        }
        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    SaveData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }
        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }
        
        #endregion

        #region Grid Method

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }
        
        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 0 }, e.ColIndex) && BtnSave.Enabled)
            {
                Sm.GrdRequestEdit(Grd1, e.RowIndex);
            }
        }

        #endregion

        #region Save Data

        private void SaveData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;
            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "FacturNumber", "TblFacturNumberHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveFNHdr(DocNo));
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 0).Length > 0)
                {
                    cml.Add(SaveFNDtl(DocNo, Row));
                    cml.Add(UpdateFNDtl(DocNo, Row));
                }
            }

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private void FocusGrid(int Row)
        {
            //Sm.FocusGrd(Grd1, Row, 0);
            Cursor.Current = Cursors.WaitCursor;
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Document Date") ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                FacturNumberNotValid()||
                IsGrdExceedMaxRecords();
        }

        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {

                if (Sm.IsGrdValueEmpty(Grd1, Row, 0, false, "Factur Number is empty.")) return true;
            }
            return false;
        }

        private bool FacturNumberNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 0).Length != 15)
                {
                    Sm.StdMsg(mMsgType.Info, "Make sure Factur Number have a correct format (ex : xxx-xx.xxxxxxxx )");
                    return true;
                }
            }
            return false;
        }

        private MySqlCommand SaveFNHdr(string DocNo)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblFacturNumberHdr(DocNo, DocDt, Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DocDt, @Remark, @CreateBy, CurrentDateTime()) " 
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveFNDtl(string  DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblFacturNumberDtl(DocNo, FacturNo, UsedInd, SLIDocNo,  CreateBy, CreateDt) " +
                    "Values(@DocNo, @FacturNo, 'N', @SLIDocNo, @CreateBy, CurrentDateTime()) " 
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FacturNo", Sm.GetGrdStr(Grd1, Row, 0));
            Sm.CmParam<String>(ref cm, "@UsedInd", Sm.GetGrdStr(Grd1, Row, 1));  
            Sm.CmParam<String>(ref cm, "@SLIDocNo", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand UpdateFNDtl(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                "   UPDATE TblFacturNumberDtl SET UsedInd='Y', " +
                "   LastUpBy=@UserCode, LastUpDt=CurrentDateTime() WHERE UsedInd='N' AND DocNo!=@DocNo "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UsedInd", Sm.GetGrdStr(Grd1, Row, 1)); 
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count <= 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 Factur Number");
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 10001)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (10,000).");
                return true;
            }
            return false;
        }

        private bool IsFacturNumberRangeValid()
        {
            if (Decimal.Parse(string.Concat(TxtStart1.Text, TxtStart2.Text, TxtStart3.Text)) > Decimal.Parse(string.Concat(TxtEnd1.Text, TxtEnd2.Text, TxtEnd3.Text)))
            {
                Sm.StdMsg(mMsgType.Warning, "End number should be bigger than Start number");
                return true;
            }

            if (TxtStart1.Text != TxtEnd1.Text)
            {
                Sm.StdMsg(mMsgType.Warning, "First section of Start and End should be the same.");
                return true;
            }

            if (TxtStart2.Text != TxtEnd2.Text)
            {
                Sm.StdMsg(mMsgType.Warning, "Second section of Start and End should be the same.");
                return true;
            }

            return false;
        }

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowFNHdr(DocNo);
                ShowFNDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowFNHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select DocNo, DocDt, Remark From TblFacturNumberHdr Where DocNo=@DocNo",
                    new string[] { 
                        //0
                        "DocNo",
                        //1-2
                        "DocDt", "Remark"
                         },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        MeeRemark.EditValue = Sm.DrStr(dr, c[2]);
                    }, true
                );
        }

        private void ShowFNDtl(string DocNo)
        {

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select  FacturNo, UsedInd, SLIDocNo  ");
            SQL.AppendLine("From TblFacturNumberDtl  ");
            SQL.AppendLine("Where DocNo=@DocNo Order By DocNo");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "FacturNo",

                    //1-2
                     "UsedInd", "SLIDocNo"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Grd1.Cells[Row, 0].Value = Row + 1;
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("B", Grd1, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 2);
                    
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 0);
        }


        #endregion

        #region Additional methods

        private void GetParameter()
        {
            mFirstDelimiterFacturNo = Sm.GetParameter("FirstDelimiterFacturNo");
            mSecondDelimiterFacturNo = Sm.GetParameter("SecondDelimiterFacturNo");

            if (mFirstDelimiterFacturNo.Length == 0) mFirstDelimiterFacturNo = "-";
            if (mSecondDelimiterFacturNo.Length == 0) mSecondDelimiterFacturNo = ".";
        }

        #endregion

        #endregion

        #region Events

        #region Button Click

        private void BtnImportWBS_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled && 
                !Sm.IsTxtEmpty(TxtStart1, "Start", false) &&
                !Sm.IsTxtEmpty(TxtStart2, "Start", false) &&
                !Sm.IsTxtEmpty(TxtStart3, "Start", false) && 
                !Sm.IsTxtEmpty(TxtEnd1, "End", false) &&
                !Sm.IsTxtEmpty(TxtEnd2, "End", false) &&
                !Sm.IsTxtEmpty(TxtEnd3, "End", false)
                )
            {
                if (IsFacturNumberRangeValid()) return;

                Sm.ClearGrd(Grd1, true);

                Grd1.BeginUpdate();
                int Row = 0;
                for (decimal i = Decimal.Parse(TxtStart3.Text); i <= Decimal.Parse(TxtEnd3.Text); ++i)
                {
                    Grd1.Rows.Add();

                    string mNumber = Sm.Right(string.Concat("00000000", i.ToString()), 8);

                    Grd1.Cells[Row, 0].Value = string.Concat(TxtStart1.Text, mFirstDelimiterFacturNo, TxtStart2.Text, mSecondDelimiterFacturNo, mNumber);

                    Row += 1;
                }
                Grd1.EndUpdate();
            }
        }

        #endregion

        #endregion

    }
}
