﻿#region Update
/*
    19/05/2020 [DITA/IMS] new apps
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmTransferRequestProject : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string
           mMenuCode = string.Empty, mAccessInd = string.Empty,
           mDocNo = string.Empty, //if this application is called from other application;
           mPGCode = string.Empty; 
        internal FrmTransferRequestProjectFind FrmFind;
        internal int mNumberOfInventoryUomCode = 1;
        internal bool mIsSystemUseCostCenter = false;
        internal bool 
            mIsTransferRequestWhsProjectEnabled = false,
            mIsItGrpCodeShow = false,
            mIsBOMShowSpecifications= false;
        #endregion

        #region Constructor

        public FrmTransferRequestProject(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Project's Transfer Request";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                Tp2.PageVisible = mIsTransferRequestWhsProjectEnabled;
                SetGrd();
                Tc1.SelectedTabPage = Tp2;
                SetLueItCode(ref LueItCode, string.Empty);
                Tc1.SelectedTabPage = Tp1;
                SetFormControl(mState.View);
                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 23;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",
                        
                        //1-5
                        "",
                        "",
                        "Item's Code",
                        "Item's Name",
                        "Stock",

                        //6-10
                        "Requested",
                        "UoM",
                        "Stock",
                        "Requested",
                        "UoM",

                        //11-15
                        "Stock",
                        "Requested",
                        "UoM",
                        "Remark",
                        "Cost Category",

                        //16-20
                        "Group",
                        "Local Code",
                        "Specification",
                        "Warehouse Code",
                        "Warehouse from",

                         //21-22
                        "Transfer Request Warehouse DocNo",
                        "Transfer Request Warehouse DNo",
                    },
                     new int[] 
                    {
                        //0
                        0,

                        //1-5
                        20, 20, 100, 200, 100, 

                        //6-10
                        100, 80, 100, 100, 80, 

                        //11-15
                        100, 100, 80, 200, 150, 
 
                        //16-20
                        150, 180, 200, 0, 200,

                        //21-22
                        0, 0
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 5, 6, 8, 9, 11, 12 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 1, 2 });
            Sm.GrdColInvisible(Grd1, new int[] { 0, 3, 9, 10, 11, 12, 13, 17, 19, 21, 22 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 3, 4, 5, 7, 8, 10, 11, 13, 15, 16, 17, 18, 19, 20 });

            if (mIsItGrpCodeShow)
            {
                Grd1.Cols[16].Visible = true;
                Grd1.Cols[16].Move(5);
            }
            ShowInventoryUomCode();
            if (!mIsBOMShowSpecifications) Sm.GrdColInvisible(Grd1, new int[] { 18 });
            Grd1.Cols[17].Move(4);
            Grd1.Cols[18].Move(7);
            Grd1.Cols[20].Move(6);

        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 3, 17 }, !ChkHideInfoInGrd.Checked);
            ShowInventoryUomCode();
            if (!(BtnSave.Enabled && TxtDocNo.Text.Length == 0))
            {
                if (mNumberOfInventoryUomCode == 2)
                    Sm.GrdColInvisible(Grd1, new int[] { 10 }, true);
                if (mNumberOfInventoryUomCode == 3)
                    Sm.GrdColInvisible(Grd1, new int[] { 13, 15 }, true);
            }
        }

        private void ShowInventoryUomCode()
        {
            if (mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 8, 9, 10 }, true);

            if (mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 8, 9, 10, 11, 12, 13 }, true);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, TxtLocalDocNo, LueWhsCode2, MeeRemark, LueItCode
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 6, 9, 12, 14 });
                    Sm.GrdColInvisible(Grd1, new int[] { 5, 8, 11 }, false);
                    BtnPGCode.Enabled = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, TxtLocalDocNo, LueWhsCode2, MeeRemark
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] {  6, 9, 12, 14 });
                    Sm.GrdColInvisible(Grd1, new int[] { 5 }, true);
                    if (mNumberOfInventoryUomCode == 2)
                        Sm.GrdColInvisible(Grd1, new int[] { 8 }, true);
                    if (mNumberOfInventoryUomCode == 3)
                        Sm.GrdColInvisible(Grd1, new int[] { 8, 11 }, true);
                    BtnPGCode.Enabled = true;
                    DteDocDt.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            mPGCode = string.Empty;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, TxtLocalDocNo, LueWhsCode2, 
                MeeRemark, TxtProjectCode, TxtProjectName, TxtSOCDocNo, LueItCode
            });
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 5, 6, 8, 9, 11, 12 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmTransferRequestProjectFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sl.SetLueWhsCode(ref LueWhsCode2, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    if (e.ColIndex == 1)
                    {
                        e.DoDefault = false;
                        if (e.KeyChar == Char.Parse(" "))
                            Sm.FormShowDialog(new FrmTransferRequestProjectDlg(this));
                    }
                }
            }

            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 3).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 3);
                f.ShowDialog();
            }
          
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdEnter(Grd1, e);
            Sm.GrdTabInLastCell(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1 && TxtDocNo.Text.Length == 0)
                Sm.FormShowDialog(new FrmTransferRequestProjectDlg(this));

            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 3).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 3);
                f.ShowDialog();
            }

        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 6, 9, 12 }, e);

            if (e.ColIndex == 6)
            {
                Sm.ComputeQtyBasedOnConvertionFormula("12", Grd1, e.RowIndex, 3, 6, 9, 12, 7, 10, 13);
                Sm.ComputeQtyBasedOnConvertionFormula("13", Grd1, e.RowIndex, 3, 6, 12, 9, 7, 13, 10);
            }

            if (e.ColIndex == 9)
            {
                Sm.ComputeQtyBasedOnConvertionFormula("21", Grd1, e.RowIndex, 3, 9, 6, 12, 10, 7, 13);
                Sm.ComputeQtyBasedOnConvertionFormula("23", Grd1, e.RowIndex, 3, 9, 12, 6, 10, 13, 7);
            }

            if (e.ColIndex == 12)
            {
                Sm.ComputeQtyBasedOnConvertionFormula("31", Grd1, e.RowIndex, 3, 12, 6, 9, 13, 7, 10);
                Sm.ComputeQtyBasedOnConvertionFormula("32", Grd1, e.RowIndex, 3, 12, 9, 6, 13, 10, 7);
            }

            if (e.ColIndex == 6 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 7), Sm.GetGrdStr(Grd1, e.RowIndex, 10)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 9, Grd1, e.RowIndex, 6);

            if (e.ColIndex == 6 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 7), Sm.GetGrdStr(Grd1, e.RowIndex, 13)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 12, Grd1, e.RowIndex, 6);

            if (e.ColIndex == 9 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 10), Sm.GetGrdStr(Grd1, e.RowIndex, 13)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 12, Grd1, e.RowIndex, 9);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "TransferRequestProject", "TblTransferRequestProjectHdr");

            ProcessDataTransferRequestWhs();

            var cml = new List<MySqlCommand>();

            cml.Add(SaveTransferRequestProjectHdr(DocNo));

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 3).Length > 0)
                    cml.Add(SaveTransferRequestProjectDtl(DocNo, Row));

            Sm.ExecCommands(cml);

      

            BtnInsertClick(sender, e);
        }


        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueWhsCode2, "Warehouse to") ||
                IsGrdEmpty() ||
                IsGrdExceedMaxRecords() ||
                IsGrdValueNotValid();
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 item.");
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Item data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            string Msg = string.Empty;
            
            ReComputeStock();

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, Row, 3, false, "Item is empty.")) return true;
              
                Msg =
                    "Item's Code : " + Sm.GetGrdStr(Grd1, Row, 3) + Environment.NewLine +
                    "Item's Name : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine + Environment.NewLine;

                if (Sm.GetGrdDec(Grd1, Row, 6) <= 0m)
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Quantity should be greater than 0.");
                    return true;
                }

                if (Sm.GetGrdDec(Grd1, Row, 6) > Sm.GetGrdDec(Grd1, Row, 5))
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "DO quantity should not be bigger than available stock.");
                    return true;
                }

                if (Grd1.Cols[9].Visible)
                {
                    if (Sm.GetGrdDec(Grd1, Row, 9) <= 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Quantity (2) should be greater than 0.");
                        return true;
                    }

                    if (Sm.GetGrdDec(Grd1, Row, 9) > Sm.GetGrdDec(Grd1, Row, 8))
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "DO quantity (2) should not be bigger than available stock (2).");
                        return true;
                    }
                }

                if (Grd1.Cols[11].Visible)
                {
                    if (Sm.GetGrdDec(Grd1, Row, 12) <= 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Quantity (3) should be greater than 0.");
                        return true;
                    }

                    if (Sm.GetGrdDec(Grd1, Row, 12) > Sm.GetGrdDec(Grd1, Row, 12))
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "DO quantity (3) should not be bigger than available stock (3).");
                        return true;
                    }
                }
            }
            return false;
        }

        private void ReComputeStock()
        {
            string Filter = string.Empty, ItCode = string.Empty;
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
    
            int No = 1;
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 3).Length != 0)
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += "(Y.WhsCode = @WhsCode" + No + " And X.ItCode=@ItCode" + No + ") ";
                    Sm.CmParam<String>(ref cm, "@ItCode" + No, Sm.GetGrdStr(Grd1, Row, 3));
                    Sm.CmParam<String>(ref cm, "@WhsCode" + No, Sm.GetGrdStr(Grd1, Row, 19));
                    No += 1;
                }
            }
       
            SQL.AppendLine("Select A.ItCode, ");
            SQL.AppendLine("IfNull(A.StockQty, 0)-IfNull(B.TransferredQty, 0)-IfNull(C.DOQty, 0) As Qty, ");
            SQL.AppendLine("IfNull(A.StockQty2, 0)-IfNull(B.TransferredQty2, 0)-IfNull(C.DOQty2, 0) As Qty2, ");
            SQL.AppendLine("IfNull(A.StockQty3, 0)-IfNull(B.TransferredQty3, 0)-IfNull(C.DOQty3, 0) As Qty3 ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select ItCode, ");
            SQL.AppendLine("    Sum(Qty) As StockQty, Sum(Qty2) As StockQty2, Sum(Qty3) As StockQty3 ");
            SQL.AppendLine("    From TblStockSummary ");
            SQL.AppendLine("    Where Qty<>0 ");
            SQL.AppendLine("    And (" + Filter.Replace("X.", "").Replace("Y.", "") + ") ");
            SQL.AppendLine("    Group By ItCode ");
            SQL.AppendLine(") A ");
            SQL.AppendLine("Left join ( ");
            SQL.AppendLine("    Select T2.ItCode, ");
            SQL.AppendLine("    Sum(T2.Qty) As TransferredQty, ");
            SQL.AppendLine("    Sum(T2.Qty2) As TransferredQty2, ");
            SQL.AppendLine("    Sum(T2.Qty3) As TransferredQty3 ");
            SQL.AppendLine("    From TblTransferRequestWhsHdr T1 ");
            SQL.AppendLine("    Inner Join TblTransferRequestWhsDtl T2 On T1.DocNo=T2.DocNo And T2.ProcessInd ='O' ");
            SQL.AppendLine("    And (" + Filter.Replace("X.", "T2.").Replace("Y.", "T1.") + ") ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    And IfNull(T1.Status, 'O') In ('O', 'A') ");
            SQL.AppendLine("    Group By T2.ItCode ");
            SQL.AppendLine(") B On A.ItCode=B.ItCode ");
            SQL.AppendLine("Left join ( ");
            SQL.AppendLine("    Select T4.ItCode, ");
            SQL.AppendLine("    Sum(T4.Qty) As DOQty, ");
            SQL.AppendLine("    Sum(T4.Qty2) As DOQty2, ");
            SQL.AppendLine("    Sum(T4.Qty3) As DOQty3 ");
            SQL.AppendLine("    From TblTransferRequestWhsHdr T1 ");
            SQL.AppendLine("    Inner Join TblTransferRequestWhsDtl T2 On T1.DocNo=T2.DocNo And T2.ProcessInd='F' ");
            SQL.AppendLine("    And (" + Filter.Replace("X.", "T2.").Replace("Y.", "T1.") + ") ");
            SQL.AppendLine("    Inner Join TblDOWhsHdr T3 ");
            SQL.AppendLine("        On T3.TransferRequestWhsDocNo Is Not Null ");
            SQL.AppendLine("        And T2.DocNo=IfNull(T3.TransferRequestWhsDocNo, '') ");
            SQL.AppendLine("        And T3.CancelInd ='N' ");
            SQL.AppendLine("        And T3.Status In ('A', 'O') ");
            SQL.AppendLine("    Inner Join TblDOWhsDtl T4 ");
            SQL.AppendLine("        On T3.DocNo=T4.DocNo ");
            SQL.AppendLine("        And T2.ItCode=T4.ItCode ");
            SQL.AppendLine("        And T4.ProcessInd='O' ");
            SQL.AppendLine("        And T4.CancelInd='N' ");
            SQL.AppendLine("        And (" + Filter.Replace("X.", "T4.").Replace("Y.", "T3.") + ") ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    And IfNull(T1.Status, 'O')='A' ");
            SQL.AppendLine("    Group By T4.ItCode ");
            SQL.AppendLine(") C On A.ItCode=C.ItCode; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                    { "ItCode", "Qty", "Qty2", "Qty3" });
                if (dr.HasRows)
                {
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        ItCode = Sm.DrStr(dr, 0);
                        for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        {
                            if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 3), ItCode))
                            {
                                Sm.SetGrdValue("N", Grd1, dr, c, Row, 5, 1);
                                Sm.SetGrdValue("N", Grd1, dr, c, Row, 8, 2);
                                Sm.SetGrdValue("N", Grd1, dr, c, Row, 11, 3);
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        void GenerateSQLConditionForInventory(
            ref MySqlCommand cm, ref string Filter, int No,
            ref iGrid Grd, int Row, int Col)
        {
            if (Filter.Length > 0) Filter += " Or ";
            Filter += "(X.ItCode=@ItCode" + No + ") ";
            Sm.CmParam<String>(ref cm, "@ItCode" + No, Sm.GetGrdStr(Grd, Row, Col));
        }

        private MySqlCommand SaveTransferRequestProjectHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblTransferRequestProjectHdr(DocNo, DocDt, LocalDocNo, WhsCode, PGCode, SOContractDocNo, ItCode,  Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @LocalDocNo, @WhsCode, @PGCode, @SOContractDocNo, @ItCode,  @Remark, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@LocalDocNo", TxtLocalDocNo.Text);
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode2));
            Sm.CmParam<String>(ref cm, "@PGCode", mPGCode);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@SOContractDocNo", TxtSOCDocNo.Text);
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetLue(LueItCode));
            return cm;
        }

        private MySqlCommand SaveTransferRequestProjectDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblTransferRequestProjectDtl(DocNo, DNo, WhsCode, TransferRequestWhsDocNo, TransferRequestWhsDNo,  ItCode, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @WhsCode, @TransferRequestWhsDocNo, @TransferRequestWhsDNo, @ItCode, @Qty, @Qty2, @Qty3, @Remark, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 3));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 6));
            Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd1, Row, 9));
            Sm.CmParam<Decimal>(ref cm, "@Qty3", Sm.GetGrdDec(Grd1, Row, 12));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 14));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetGrdStr(Grd1, Row, 19));
            Sm.CmParam<String>(ref cm, "@TransferRequestWhsDocNo", Sm.GetGrdStr(Grd1, Row, 21));
            Sm.CmParam<String>(ref cm, "@TransferRequestWhsDNo", Sm.GetGrdStr(Grd1, Row, 22));

            return cm;
        }

        private MySqlCommand SaveTransferRequestWhsHdr(TransferReqProjectHdr x)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblTransferRequestWhsHdr(DocNo, DocDt, LocalDocNo, WhsCode, WhsCode2, PGCode, SOContractDocNo, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @LocalDocNo, @WhsCode, @WhsCode2, @PGCode, @SOContractDocNo, @Remark, @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select DocType, @DocNo, '001', DNo, @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblDocApprovalSetting ");
            SQL.AppendLine("Where DocType='TransferRequestWhs' And WhsCode = '"+x.WhsCode2+"'; ");

            SQL.AppendLine("Update TblTransferRequestWhsHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select 1 From TblDocApproval ");
            SQL.AppendLine("    Where DocType='TransferRequestWhs' And DocNo=@DocNo ");
            SQL.AppendLine("); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", x.DocNo);
            Sm.CmParamDt(ref cm, "@DocDt",x.DocDt);
            Sm.CmParam<String>(ref cm, "@LocalDocNo", x.LocalDocNo);
            Sm.CmParam<String>(ref cm, "@WhsCode", x.WhsCode);
            Sm.CmParam<String>(ref cm, "@WhsCode2", x.WhsCode2);
            Sm.CmParam<String>(ref cm, "@PGCode", x.PGCode);
            Sm.CmParam<String>(ref cm, "@Remark", x.Remark);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@SOContractDocNo", x.SOContractDocNo);
            return cm;
        }

        private MySqlCommand SaveTransferRequestWhsDtl(TransferReqProjectDtl y)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblTransferRequestWhsDtl(DocNo, DNo, ItCode, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @ItCode, @Qty, @Qty2, @Qty3, @Remark, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", y.DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", y.DNo);
            Sm.CmParam<String>(ref cm, "@ItCode", y.ItCode);
            Sm.CmParam<Decimal>(ref cm, "@Qty", y.Qty);
            Sm.CmParam<Decimal>(ref cm, "@Qty2", y.Qty2);
            Sm.CmParam<Decimal>(ref cm, "@Qty3", y.Qty3);
            Sm.CmParam<String>(ref cm, "@Remark", y.Remark2);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }


        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowTransferRequestProjectHdr(DocNo);
                ShowTransferRequestProjectDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowTransferRequestProjectHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, ");
            SQL.AppendLine("A.LocalDocNo, A.WhsCode, A.Remark, B.PGCode, B.ProjectCode, B.ProjectName, A.SOContractDocNo, A.ItCode ");
            SQL.AppendLine("From TblTransferRequestProjectHdr A ");
            SQL.AppendLine("Left Join TblProjectGroup B On A.PGCode=B.PGCode ");
            SQL.AppendLine("Where DocNo=@DocNo;");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo",
 
                        //1-5
                        "DocDt", "LocalDocNo", "WhsCode",  "Remark", "PGCode",
                        
                        //6-9
                        "ProjectCode", "ProjectName", "SOContractDocNo", "ItCode"
                       
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        TxtLocalDocNo.EditValue = Sm.DrStr(dr, c[2]);
                        Sl.SetLueWhsCode(ref LueWhsCode2, Sm.DrStr(dr, c[3]));
                        MeeRemark.EditValue = Sm.DrStr(dr, c[4]);
                        mPGCode = Sm.DrStr(dr, c[5]);
                        TxtProjectCode.EditValue = Sm.DrStr(dr, c[6]);
                        TxtProjectName.EditValue = Sm.DrStr(dr, c[7]);
                        TxtSOCDocNo.EditValue = Sm.DrStr(dr, c[8]);
                        SetLueItCode(ref LueItCode, Sm.DrStr(dr, c[9]));
                        Sm.SetLue(LueItCode, Sm.DrStr(dr, c[9]));
                    }, true
                );
        }

        private void ShowTransferRequestProjectDtl(string DocNo)
        {
            try
            {
                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
                var SQL = new StringBuilder();

                SQL.AppendLine("Select A.DNo, ");
                SQL.AppendLine("A.ItCode, B.ItName, ");
                SQL.AppendLine("A.Qty, B.InventoryUOMCode, ");
                SQL.AppendLine("A.Qty2, B.InventoryUOMCode2, ");
                SQL.AppendLine("A.Qty3, B.InventoryUOMCode3, ");
                SQL.AppendLine("A.Remark, E.CCtName, B.ItGrpCode, B.ItCodeInternal, B.Specification, A.WhsCode, F.WhsName, A.TransferRequestWhsDocNo, A.TransferRequestWhsDNo ");
                SQL.AppendLine("From TblTransferRequestProjectDtl A ");
                SQL.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
                SQL.AppendLine("Inner Join TblItemCategory C On B.ItCtCode=C.ItCtCode ");
                SQL.AppendLine("Left Join TblItemCostCategory D On A.ItCode=D.ItCode And D.CCCode=@CCCode ");
                SQL.AppendLine("Left Join TblCostCategory E On D.CCCode=E.CCCode And D.CCtCode=E.CCtCode ");
                SQL.AppendLine("Inner Join TblWarehouse F On F.WhsCode=A.WhsCode ");
                SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo;");

                Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                    { 
                        //0
                        "DNo", 

                        //1-5
                        "ItCode", "ItName", "Qty",  "InventoryUomCode", "Qty2", 
                        
                        //6-10
                        "InventoryUomCode2", "Qty3", "InventoryUomCode3", "Remark",  "CCtName",   
                        
                        //11-15
                        "ItGrpCode", "ItCodeInternal", "Specification", "WhsCode","WhsName",

                        //16-17
                        "TransferRequestWhsDocNo", "TransferRequestWhsDNo",
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 6);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 10);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 12);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 13);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 14);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 15);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 16);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 17);
                       
                    }, false, false, true, false
                );
                Sm.FocusGrd(Grd1, 0, 1);
                Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 5, 6, 8, 9, 11, 12 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Additional Method

        private void ProcessDataTransferRequestWhs()
        {
            var l = new List<TransferReqProject>();
            var l2 = new List<TransferReqProjectHdr>();
            var l3 = new List<TransferReqProjectDtl>();
            Process1(ref l);
            l.OrderBy(o => o.WhsCode);
            SetDocNo(ref l);
            Process2(ref l, ref l2, ref l3);
            Process3(ref l3);
            Process4(ref l, ref l2, ref l3);
            Process5(ref l2, ref l3);

            l.Clear(); l2.Clear(); l3.Clear();
        }

        public string GenerateDocNo(int Index, string DocDt, string DocType, string Tbl)
        {
            string
                Yr = DocDt.Substring(2, 2),
                Mth = DocDt.Substring(4, 2),
                DocTitle = Sm.GetParameter("DocTitle"),
                DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='" + DocType + "'");

            var SQL = new StringBuilder();

            SQL.Append("Select Concat( ");
            SQL.Append("IfNull(( ");
            SQL.Append("   Select Right(Concat('0000', Convert(DocNo+ " + Index + ", Char)), 4) From ( ");
            SQL.Append("       Select Convert(Left(DocNo, 4), Decimal) As DocNo From " + Tbl);
            SQL.Append("       Where Right(DocNo, 5)=Concat('" + Mth + "','/', '" + Yr + "') ");
            SQL.Append("       Order By Left(DocNo, 4) Desc Limit 1 ");
            SQL.Append("       ) As Temp ");
            SQL.Append("   ), Right(Concat('000', " + Index + "), 4)) ");
            SQL.Append(", '/', '" + ((DocTitle.Length == 0) ? "XXX" : DocTitle));
            SQL.Append("', '/', '" + DocAbbr + "', '/', '" + Mth + "','/', '" + Yr + "'");
            SQL.Append(") As DocNo");

            return Sm.GetValue(SQL.ToString());

        }

        private void SetDocNo(ref List<TransferReqProject> l)
        {
            string mDNo = string.Empty, mWhsCode =string.Empty;
            int mIndex = 0;

            for (int i = 0; i < l.Count; i++)
            {
                if (mWhsCode.Length == 0)
                {
                    mIndex = 1;
                }
                else if (mWhsCode != l[i].WhsCode)
                {
                    mIndex++;
                }

                l[i].Index = mIndex;
                l[i].DocNo = GenerateDocNo(l[i].Index, l[i].DocDt, "TransferRequestWhs", "TblTransferRequestWhsHdr");
                mWhsCode = l[i].WhsCode;
            }
        }

        private void Process1(ref List<TransferReqProject> l)
        {
            string mWhsCode = string.Empty;

            for (int Row = 0; Row < Grd1.Rows.Count-1; Row++)
            {
                l.Add(new TransferReqProject()
                {
                    DocDt = Sm.GetDte(DteDocDt),
                    LocalDocNo = TxtLocalDocNo.Text,
                    WhsCode = Sm.GetGrdStr(Grd1, Row, 19),
                    WhsCode2 = Sm.GetLue(LueWhsCode2),
                    PGCode = mPGCode,
                    SOContractDocNo = TxtSOCDocNo.Text,
                    Remark = MeeRemark.Text,
                    ItCode = Sm.GetGrdStr(Grd1, Row, 3),
                    Qty = Sm.GetGrdDec(Grd1, Row, 6),
                    Qty2 = Sm.GetGrdDec(Grd1, Row, 9),
                    Qty3 = Sm.GetGrdDec(Grd1, Row, 12),
                    Remark2 = Sm.GetGrdStr(Grd1, Row, 14),


                });
            }
        }

        private void Process2(ref List<TransferReqProject> l, ref List<TransferReqProjectHdr> l2, ref List<TransferReqProjectDtl> l3)
        {
            string mDocNoHdr = string.Empty;
            foreach (var m in l
                .Select(x => new { x.DocNo, x.DocDt, x.LocalDocNo ,x.WhsCode, x.WhsCode2, x.SOContractDocNo, x.PGCode, x.Remark })
                .OrderBy(o => o.DocNo)
                )
                if (mDocNoHdr.Length <= 0)
                {
                    l2.Add(new TransferReqProjectHdr()
                    {
                        DocNo = m.DocNo,
                        DocDt = m.DocDt,
                        LocalDocNo = m.LocalDocNo,
                        WhsCode = m.WhsCode,
                        WhsCode2 = m.WhsCode2,
                        SOContractDocNo = m.SOContractDocNo,
                        PGCode = m.PGCode,
                        Remark = m.Remark,
                    });

                    mDocNoHdr = m.DocNo;
                }
                else
                {
                    if (mDocNoHdr != m.DocNo)
                    {
                        l2.Add(new TransferReqProjectHdr()
                        {
                            DocNo = m.DocNo,
                            DocDt = m.DocDt,
                            LocalDocNo = m.LocalDocNo,
                            WhsCode = m.WhsCode,
                            WhsCode2 = m.WhsCode2,
                            SOContractDocNo = m.SOContractDocNo,
                            PGCode = m.PGCode,
                            Remark = m.Remark,
                        });

                        mDocNoHdr = m.DocNo;
                    }
                }

            foreach (var n in l.Select(x => new { x.DocNo, x.DNo, x.ItCode, x.Qty, x.Qty2, x.Qty3, x.Remark2 })
               .OrderBy(o => o.DocNo))
                if (n.ItCode.Trim().Length > 0 || n.Remark2.Trim().Length > 0)
                {
                    l3.Add(new TransferReqProjectDtl()
                    {
                        DocNo = n.DocNo,
                        ItCode = n.ItCode,
                        DNo = n.DNo,
                        Qty = n.Qty,
                        Qty2 = n.Qty2,
                        Qty3 = n.Qty3,
                        Remark2 = n.Remark2,
                    });
                }
        }

        private void Process3(ref List<TransferReqProjectDtl> l3)
        {
            string mDocNo = string.Empty, mDNo = string.Empty;
            for (int i = 0; i < l3.Count; i++)
            {
                if (mDNo.Length <= 0)
                {
                    mDNo = "001";
                    mDocNo = l3[i].DocNo;
                    l3[i].DNo = mDNo;
                }
                else
                {
                    if (mDocNo == l3[i].DocNo)
                    {
                        mDNo = Sm.Right(string.Concat("000", (Int32.Parse(mDNo) + 1)), 3);
                    }
                    else
                    {
                        mDNo = "001";
                        mDocNo = l3[i].DocNo;

                    }

                    l3[i].DNo = mDNo;
                }
            }
        }

        private void Process4(ref List<TransferReqProject> l,ref List<TransferReqProjectHdr> l2, ref List<TransferReqProjectDtl> l3)
        {
            try
            {
                var cml = new List<MySqlCommand>();
                l2.ForEach(i =>
                {
                    cml.Add(SaveTransferRequestWhsHdr(i));
                });
                l3.ForEach(i =>
                {
                    cml.Add(SaveTransferRequestWhsDtl(i));
                });

                Sm.ExecCommands(cml);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private void Process5(ref List<TransferReqProjectHdr> l2, ref List<TransferReqProjectDtl> l3)
        {
            for (int i = 0; i < Grd1.Rows.Count; i++)
            {
                foreach (var x in l2.Where(p => p.WhsCode == Sm.GetGrdStr(Grd1, i, 19)))
                {
                    foreach (var y in l3.Where(p => p.ItCode == Sm.GetGrdStr(Grd1, i, 3)))
                    {
                        if (x.DocNo == y.DocNo)
                        {
                            Grd1.Cells[i, 21].Value = y.DocNo;
                            Grd1.Cells[i, 22].Value = y.DNo;
                        }

                    }
                }
            }
        }
       

        private void GetParameter()
        {
            string NumberOfInventoryUomCode = Sm.GetParameter("NumberOfInventoryUomCode");
            if (NumberOfInventoryUomCode.Length == 0)
                mNumberOfInventoryUomCode = 1;
            else
                mNumberOfInventoryUomCode = int.Parse(NumberOfInventoryUomCode);
            mIsSystemUseCostCenter = Sm.GetParameterBoo("IsSystemUseCostCenter");
            mIsItGrpCodeShow = Sm.GetParameterBoo("IsItGrpCodeShow");
            mIsBOMShowSpecifications = Sm.GetParameterBoo("IsBOMShowSpecifications");
            mIsTransferRequestWhsProjectEnabled = Sm.GetParameterBoo("IsTransferRequestWhsProjectEnabled");
        }

        //private void ComputeQtyBasedOnConvertionFormula(
        //    string ConvertType, iGrid Grd, int Row, int ColItCode,
        //    int ColQty1, int ColQty2, int ColUom1, int ColUom2)
        //{
        //    try
        //    {
        //        if (!Sm.CompareGrdStr(Grd, Row, ColUom1, Grd, Row, ColUom2))
        //        {
        //            decimal Convert = GetInventoryUomCodeConvert(ConvertType, Sm.GetGrdStr(Grd, Row, ColItCode));
        //            if (Convert != 0)
        //            {
        //                Grd.Cells[Row, ColQty2].Value = Convert * Sm.GetGrdDec(Grd, Row, ColQty1);
        //            }
        //        }
        //    }
        //    catch (Exception Exc)
        //    {
        //        Sm.ShowErrorMsg(Exc);
        //    }
        //}

        //private decimal GetInventoryUomCodeConvert(string ConvertType, string ItCode)
        //{
        //    var cm = new MySqlCommand
        //    {
        //        CommandText =
        //            "Select InventoryUomCodeConvert" + ConvertType + " From TblItem Where ItCode=@ItCode;"
        //    };
        //    Sm.CmParam<String>(ref cm, "@ItCode", ItCode);
        //    return Sm.GetValueDec(cm);
        //}

        internal string GetSelectedItem()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 3).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL +=
                            "'" +
                            Sm.GetGrdStr(Grd1, Row, 3) +
                            "'";
                    }
                }
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }

        public static void SetLueCCCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select T.CCCode As Col1, T.CCName As Col2 " +
                "From TblCostCenter T " +
                "Where Not Exists(Select Parent From TblCostCenter Where CCCode=T.CCCode And Parent Is Not Null) " +
                "Order By T.CCName;",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void SetLueItCode(ref DXE.LookUpEdit Lue, string ItCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.ItCode As Col1, B.ItName As Col2 ");
            SQL.AppendLine("From TblSOContractDtl A ");
            SQL.AppendLine("Inner Join TblItem B On A.ItCode = B.ItCode ");
            if(ItCode.Length > 0)
                SQL.AppendLine("Where A.ItCode=@ItCode ");
            else
                SQL.AppendLine("Where A.DocNo=@SOCDocNo ");
            SQL.AppendLine("Order By B.ItName; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@SOCDocNo", TxtSOCDocNo.Text);
            Sm.CmParam<String>(ref cm, "@ItCode", ItCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #endregion

        #region Event

        private void TxtLocalDocNo_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtLocalDocNo);
        }

        private void LueWhsCode2_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueWhsCode2, new Sm.RefreshLue2(Sl.SetLueWhsCode), string.Empty);
        }
        private void LueItCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCode, new Sm.RefreshLue2(SetLueItCode), string.Empty);
        }

        private void BtnPGCode_Click(object sender, EventArgs e)
        {
            var f = new FrmProjectGroupStdDlg();
            f.TopLevel = true;
            f.ShowDialog();
            if (f.mPGCode.Length > 0)
            {
                mPGCode = f.mPGCode;
                TxtProjectCode.EditValue = f.mProjectCode;
                TxtProjectName.EditValue = f.mProjectName;
                TxtSOCDocNo.EditValue = f.mSOCDocNo;
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueItCode }, false);
                SetLueItCode(ref LueItCode, string.Empty);
            }
            else
            {
                mPGCode = string.Empty;
                TxtProjectCode.EditValue = null;
                TxtProjectName.EditValue = null;
                TxtSOCDocNo.EditValue = null;
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueItCode }, true);
                LueItCode.EditValue = null;
            }
            f.Close();
        }

        #endregion
    }

    #region Report Class

    class TransferReqProject
    {
        public int Index { get; set; }
        public string DocNo { get; set; }
        public string DocDt { get; set; }
        public string LocalDocNo { get; set; }
        public string WhsCode { get; set; }
        public string WhsCode2 { get; set; }
        public string SOContractDocNo { get; set; }
        public string PGCode { get; set; }
        public string Remark { get; set; }
        public string DNo { get; set; }
        public string ItCode { get; set; }
        public decimal Qty { get; set; }
        public decimal Qty2 { get; set; }
        public decimal Qty3 { get; set; }
        public string Remark2 { get; set; }

    }

    class TransferReqProjectHdr
    {
        public string DocNo { get; set; }
        public string DocDt { get; set; }
        public string LocalDocNo { get; set; }
        public string WhsCode { get; set; }
        public string WhsCode2 { get; set; }
        public string SOContractDocNo { get; set; }
        public string PGCode { get; set; }
        public string Remark { get; set; }
    }

    class TransferReqProjectDtl
    {
        public string DocNo { get; set; }
        public string DNo { get; set; }
        public string ItCode { get; set; }
        public decimal Qty { get; set; }
        public decimal Qty2 { get; set; }
        public decimal Qty3 { get; set; }
        public string Remark2 { get; set; }

    }

    #endregion
}
