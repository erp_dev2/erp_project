﻿#region Update
/*
    20/10/2017 [TKG] ubah tampilan
    22/07/2021 [WED/PHT] tambah informasi DeptCode dan SiteCode berdasarkan parameter IsEmpClaimRequestUseBudget
    01/07/2022 [MYA/PHT] Claim Request yang membentuk VR, kolom PIC dan Dept nya ambil dari employee dan debt claim request (desktop)
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmEmpClaimDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmEmpClaim mFrmParent;
        private string mDocDt;
        string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmEmpClaimDlg(FrmEmpClaim FrmParent, string DocDt)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mDocDt = DocDt;
        }

        #endregion

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            SetGrd();
            SetSQL();
            Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mFrmParent.mIsFilterByDeptHR ? "Y" : "N");
        }

        #endregion

        #region Standard Method

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.EmpCode, A.EmpName, A.EmpCodeOld, B.DeptName, C.PosName, A.JoinDt, A.ResignDt, D.OptDesc as Gender, A.DeptCode, A.SiteCode ");
            SQL.AppendLine("From TblEmployee A ");
            SQL.AppendLine("Left Join TblDepartment B On A.DeptCode=B.DeptCode ");
            SQL.AppendLine("Left Join TblPosition C On A.PosCode=C.PosCode ");
            SQL.AppendLine("Left Join TblOption D On A.Gender=D.OptCode And D.OptCat='Gender' ");
            SQL.AppendLine("Where ((A.ResignDt Is Not Null And A.ResignDt>=@DocDt ) Or A.ResignDt Is Null) ");
            if (mFrmParent.mIsFilterByDeptHR)
            {
                SQL.AppendLine("And A.DeptCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupDepartment ");
                SQL.AppendLine("    Where DeptCode=IfNull(A.DeptCode, '') ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 11;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[]
                    {
                         //0
                        "No.",

                        //1-5
                        "Employee's"+Environment.NewLine+"Code",
                        "Employee's Name",
                        "Old Code",
                        "Join"+Environment.NewLine+"Date",
                        "Resign"+Environment.NewLine+"Date",

                        //6-10
                        "Department",
                        "Position",
                        "Gender",
                        "DeptCode",
                        "SiteCode"
                    },
                    new int[]
                    {
                        //0
                        50,

                        //1-5
                        100, 200, 100, 80, 80, 
                        
                        //6-10
                        200, 200, 100, 0, 0
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 4, 5 });
            Sm.GrdColInvisible(Grd1, new int[] { 4, 5, 7, 8, 9, 10 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4, 5, 7, 8 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                String Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@DocDt", mDocDt);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "A.EmpCode", "A.EmpName", "A.EmpCodeOld" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "A.DeptCode", true);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, mSQL + Filter + " Order By A.EmpName ",
                    new string[]
                    { 
                        //0
                        "EmpCode", 

                        //1-5
                        "EmpName", "EmpCodeOld", "JoinDt", "ResignDt", "DeptName", 
                        
                        //6-10
                        "PosName", "Gender", "DeptCode", "SiteCode"

                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 0))
            {
                mFrmParent.TxtEmpCode.Text = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
                mFrmParent.TxtEmpName.Text = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 2);
                mFrmParent.TxtDeptCode.Text = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 6);
                mFrmParent.TxtPosCode.Text = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 7);
                if (mFrmParent.mIsEmpClaimRequestUseBudget)
                {
                    mFrmParent.mDeptCode = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 9);
                    mFrmParent.mSiteCode = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 10);
                    mFrmParent.SetLueBCCode(ref mFrmParent.LueBCCode, string.Empty, Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 9));
                }
                if (mFrmParent.mIsVRClaimUsePIC)
                {
                    mFrmParent.mDeptCode = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 9);
                }
                mFrmParent.ComputeBalance();
                this.Close();
            }
        }

        private void Grd1_CellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            ChooseData();
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        #endregion

    }
}
