﻿#region Update
/*
    29/12/2021 [TYO/PRODUCT] New apps
    22/01/2022 [TKG/PRODUCT] BUG DI IsVRMAlreadyChosen()
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmAsset2Dlg4 : RunSystem.FrmBase4
    {
        #region Field

        private FrmAsset2 mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmAsset2Dlg4(FrmAsset2 FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                this.Text = "List of Voucher Request (Manual)";
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 7;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "",
                    "Voucher Request#",
                    "",
                    "Date",
                    "Currency",

                    //6
                    "Amount"
                },
                new int[] 
                {
                    //0
                    50,
                    
                    //1-5
                    20, 180, 20, 80, 80,

                    //6
                    120
                }
            );
            Sm.GrdColInvisible(Grd1, new int[] { 3 }, false);
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColButton(Grd1, new int[] { 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 6 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 4 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 3 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo, DocDt, CurCode, Amt ");
            SQL.AppendLine("From TblVoucherRequestHdr ");
            SQL.AppendLine("Where CancelInd = 'N' ");
            SQL.AppendLine("And Status = 'A' ");
            SQL.AppendLine("And DocType = '01' ");
            SQL.AppendLine("And AssetCode Is Null ");
            SQL.AppendLine("And Locate(Concat('##', DocNo, '##'), @SelectedVRM)<1 ");

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            if (
                   Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                   Sm.IsDteEmpty(DteDocDt2, "End date") ||
                   Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                   ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " And 0 = 0 ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@SelectedVRM", mFrmParent.GetSelectedVRM());

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "DocNo", false);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "DocDt");

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By DocNo;",
                    new string[] 
                    { 
                        //0
                        "DocNo",

                        //1-3
                        "DocDt", "CurCode", "Amt"
                     },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Grd.Cells[Row, 1].Value = false;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 4, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 2);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 3);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && Sm.GetGrdStr(Grd1, Row, 2).Length > 0 && !IsVRMAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd2.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 1, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 3, Grd1, Row2, 4);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 4, Grd1, Row2, 5);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 5, Grd1, Row2, 6);
                        
                        mFrmParent.Grd2.Rows.Add();
                        Sm.SetGrdNumValueZero(mFrmParent.Grd2, mFrmParent.Grd2.Rows.Count - 1, new int[] { 5 });
                    }
                }
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 voucher request manual.");
        }

        private bool IsVRMAlreadyChosen(int Row)
        {
            string DocNo = Sm.GetGrdStr(Grd1, Row, 2), DocNoTemp = string.Empty;

            if (mFrmParent.Grd2.Rows.Count> 0)
            {
                for (int r = 0; r < mFrmParent.Grd2.Rows.Count; r++)
                {
                    DocNoTemp = Sm.GetGrdStr(mFrmParent.Grd2, r, 1);
                    if (DocNoTemp.Length>0 && Sm.CompareStr(DocNoTemp, DocNo))
                        return true;
                }
            }
            return false;
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmVoucherRequest(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.Text = Sm.GetValue("Select MenuDesc From TblMenu Where Param = 'FrmVoucherRequest' Limit 1;");
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmVoucherRequest(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.Text = Sm.GetValue("Select MenuDesc From TblMenu Where Param = 'FrmVoucherRequest' Limit 1;");
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        private void Grd1_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    Grd1.Cells[Row, 1].Value = !IsSelected;
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Voucher Request#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt1).Length == 0) DteDocDt1.EditValue = DteDocDt2.EditValue;
        }

        #endregion

        #endregion

    }
}
