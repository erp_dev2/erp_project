﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmLeaveGrp : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mLGCode = string.Empty;
        internal FrmLeaveGrpFind FrmFind;

        #endregion

        #region Constructor

        public FrmLeaveGrp(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Leave Group";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                BtnPrint.Visible = false;
                SetFormControl(mState.View);
                if (mLGCode.Length != 0)
                {
                    ShowData(mLGCode);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
          if (FrmFind != null) FrmFind.Close();
          
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtLGCode, TxtLGName, TxtShortName, MeeRemark
                    }, true);
                    ChkActInd.Properties.ReadOnly = true;
                    TxtLGCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtLGCode, TxtLGName, TxtShortName, MeeRemark
                    }, false);
                    TxtLGCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtLGName, TxtShortName, MeeRemark
                    }, false);
                    ChkActInd.Properties.ReadOnly = false;
                    TxtLGName.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> { TxtLGCode, TxtLGName, TxtShortName, MeeRemark });
            ChkActInd.Checked = false;
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmLeaveGrpFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
            ChkActInd.Checked = true;
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtLGCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtLGCode, "", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblLeaveGrp Where LGCode=@LGCode" };
                Sm.CmParam<String>(ref cm, "@LGCode", TxtLGCode.Text);
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                bool IsInsert = !TxtLGCode.Properties.ReadOnly;

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into TblLeaveGrp(LGCode, LGName, ActInd, ShortName, Remark, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@LGCode, @LGName, @ActInd, @ShortName, @Remark, @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("   Update LGName=@LGName, ActInd=@ActInd, ShortName=@ShortName, Remark=@Remark, ");
                SQL.AppendLine("   LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };

                Sm.CmParam<String>(ref cm, "@LGCode", TxtLGCode.Text);
                Sm.CmParam<String>(ref cm, "@LGName", TxtLGName.Text);
                Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked?"Y":"N");
                Sm.CmParam<String>(ref cm, "@ShortName", TxtShortName.Text);
                Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                
                Sm.ExecCommand(cm);

                if (IsInsert)
                    BtnInsertClick(sender, e);
                else
                    ShowData(TxtLGCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string LGCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@LGCode", LGCode);
                Sm.ShowDataInCtrl(
                        ref cm,
                        "Select LGCode, LGName, ActInd, ShortName, Remark " +
                        "From TblLeaveGrp Where LGCode=@LGCode;",
                        new string[] 
                        {
                            "LGCode", 
                            "LGName", "ActInd", "ShortName", "Remark"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtLGCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtLGName.EditValue = Sm.DrStr(dr, c[1]);
                            ChkActInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]),"Y");
                            TxtShortName.EditValue = Sm.DrStr(dr, c[3]);
                            MeeRemark.EditValue = Sm.DrStr(dr, c[4]);
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtLGCode, "Leave group code", false) ||
                Sm.IsTxtEmpty(TxtLGName, "Leave group name", false) ||
                Sm.IsTxtEmpty(TxtShortName, "Short name", false) ||
                IsLGCodeExisted() ||
                IsLGNameExisted(); 
        }

        private bool IsLGCodeExisted()
        {
            if (!TxtLGCode.Properties.ReadOnly)
            {
                var cm = new MySqlCommand()
                {
                    CommandText = "Select LGCode From TblLeaveGrp Where LGCode=@LGCode Limit 1;"
                };
                Sm.CmParam<String>(ref cm, "@LGCode", TxtLGCode.Text);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "Leave group code ( " + TxtLGCode.Text + " ) already existed.");
                    return true;
                }
            }
            return false;
        }

        private bool IsLGNameExisted()
        {
            
            var cm = new MySqlCommand()
            {
                CommandText = 
                    "Select LGCode From TblLeaveGrp " +
                    "Where LGName=@LGName " +
                    (TxtLGCode.Properties.ReadOnly?"And LGCode<>@LGCode ":string.Empty) +
                    "Limit 1;"
            };
            Sm.CmParam<String>(ref cm, "@LGName", TxtLGName.Text);
            if (TxtLGCode.Properties.ReadOnly)
                Sm.CmParam<String>(ref cm, "@LGCode", TxtLGCode.Text);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "Leave group name ( " + TxtLGName.Text + " ) already existed.");
                return true;
            }
            return false;
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtLGCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtLGCode);
        }

        private void TxtLGName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtLGName);
        }

        private void TxtShortName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtShortName);
        }

        #endregion

        #endregion
    }
}
