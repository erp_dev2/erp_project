﻿#region Update
/* 
    11/02/2019 [TKG] New Application
    27/01/2020 [RDH/PHT] menambah filter dan grid Level dan menambah parameter IsDocApprovalGroupUseLevel
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmDocApprovalGroup : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mAGCode = string.Empty;
        internal FrmDocApprovalGroupFind FrmFind;

        internal bool mIsDocApprovalGroupUseLevel = false;
        
        #endregion

        #region Constructor

        public FrmDocApprovalGroup(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Document Approval's Group";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mAGCode.Length != 0)
                {
                    ShowData(mAGCode);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }


        public void GetParameter()
        {
            mIsDocApprovalGroupUseLevel = Sm.GetParameterBoo("IsDocApprovalGroupUseLevel");
        }
        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 7;
            Grd1.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "",

                        //1-5
                        "Employee's"+Environment.NewLine+"Code",
                        "Employee's Name",
                        "Old Code",
                        "Position",
                        "Department",

                        //6
                        "Site"
                    },
                     new int[] 
                    {
                        //0
                        20,
 
                        //1-5
                        100, 200, 80, 200, 200,

                        //6
                        200
                    }
                );
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 2, 3, 4, 5, 6 });
            Sm.GrdColButton(Grd1, new int[] { 0 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3 }, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtDAGCode, TxtDAGName, MeeRemark }, true);
                    ChkActInd.Properties.ReadOnly = true;
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0 });
                    TxtDAGCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtDAGCode, TxtDAGName, MeeRemark }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0 });
                    TxtDAGCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtDAGName, MeeRemark }, false);
                    ChkActInd.Properties.ReadOnly = false;
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0 });
                    TxtDAGName.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDAGCode, TxtDAGName, MeeRemark
            });
            ChkActInd.Checked = false;
            Sm.ClearGrd(Grd1, true);
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmDocApprovalGroupFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                ChkActInd.Checked = true;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDAGCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (!TxtDAGCode.Properties.ReadOnly)
                    InsertData(sender, e);
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(SaveDocApprovalGroupHdr());

            if (Grd1.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0) cml.Add(SaveDocApprovalGroupDtl(Row));
            }

            Sm.ExecCommands(cml);

            BtnInsertClick(sender, e);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDAGCode, "Group code", false) ||
                Sm.IsTxtEmpty(TxtDAGName, "Group name", false) ||
                IsDAGCodeExisted() ||
                IsDAGNameExisted() ||
                IsGrdValueNotValid();
        }

        private bool IsDAGCodeExisted()
        {
            if (!TxtDAGCode.Properties.ReadOnly)
            {
                var cm = new MySqlCommand()
                {
                    CommandText = "Select DAGCode From TblDocApprovalGroupHdr Where DAGCode=@DAGCode Limit 1;"
                };
                Sm.CmParam<String>(ref cm, "@DAGCode", TxtDAGCode.Text);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "Group code ( " + TxtDAGCode.Text + " ) already existed.");
                    return true;
                }
            }
            return false;
        }

        private bool IsDAGNameExisted()
        {

            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select DAGCode From TblDocApprovalGroupHdr " +
                    "Where DAGName=@DAGName " +
                    (TxtDAGCode.Properties.ReadOnly ? "And DAGCode<>@DAGCode " : string.Empty) +
                    "Limit 1;"
            };
            Sm.CmParam<String>(ref cm, "@DAGName", TxtDAGName.Text);
            if (TxtDAGCode.Properties.ReadOnly)
                Sm.CmParam<String>(ref cm, "@DAGCode", TxtDAGCode.Text);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "Group name ( " + TxtDAGName.Text + " ) already existed.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            if (Grd1.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.IsGrdValueEmpty(Grd1, Row, 1, false, "Employee is empty.")) return true;       
            }
            return false;
        }

        private MySqlCommand SaveDocApprovalGroupHdr()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblDocApprovalGroupHdr(DAGCode, DAGName, ActInd, Remark, CreateBy, CreateDt) " +
                    "Values(@DAGCode, @DAGName, 'Y', @Remark, @CreateBy, CurrentDateTime()); "
            };
            Sm.CmParam<String>(ref cm, "@DAGCode", TxtDAGCode.Text);
            Sm.CmParam<String>(ref cm, "@DAGName", TxtDAGName.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveDocApprovalGroupDtl(int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblDocApprovalGroupDtl(DAGCode, EmpCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DAGCode, @EmpCode, @UserCode, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DAGCode", TxtDAGCode.Text);
            Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || 
                IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditDocApprovalGroupHdr());

            if (Grd1.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0) cml.Add(SaveDocApprovalGroupDtl(Row));
            }

            Sm.ExecCommands(cml);

            ShowData(TxtDAGCode.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return 
                Sm.IsTxtEmpty(TxtDAGName, "Group name", false);
        }

        private MySqlCommand EditDocApprovalGroupHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblDocApprovalGroupHdr Set ");
            SQL.AppendLine("    ActInd=@ActInd, DAGName=@DAGName, Remark=@Remark, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DAGCode=@DAGCode; ");

            SQL.AppendLine("Delete From TblDocApprovalGroupDtl Where DAGCode=@DAGCode; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DAGCode", TxtDAGCode.Text);
            Sm.CmParam<String>(ref cm, "@DAGName", TxtDAGName.Text);
            Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked?"Y":"N");
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string AGCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                ShowDocApprovalGroupHdr(AGCode);
                ShowDocApprovalGroupDtl(AGCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowDocApprovalGroupHdr(string DAGCode)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DAGCode", DAGCode);
            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select DAGCode, DAGName, ActInd, Remark " +
                    "From TblDocApprovalGroupHdr Where DAGCode=@DAGCode;",
                    new string[] 
                    { "DAGCode", "DAGName", "ActInd", "Remark" },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDAGCode.EditValue = Sm.DrStr(dr, c[0]);
                        TxtDAGName.EditValue = Sm.DrStr(dr, c[1]);
                        ChkActInd.Checked = Sm.DrStr(dr, c[2])=="Y";
                        MeeRemark.EditValue = Sm.DrStr(dr, c[3]);
                    }, true
                );
        }

        private void ShowDocApprovalGroupDtl(string DAGCode)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DAGCode", DAGCode);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.EmpCode, B.EmpName, B.EmpCodeOld, C.PosName, D.DeptName, E.SiteName ");
            SQL.AppendLine("From TblDocApprovalGroupDtl A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode=B.EmpCode ");
            SQL.AppendLine("Left Join TblPosition C On B.PosCode=C.PosCode ");
            SQL.AppendLine("Left Join TblDepartment D On B.DeptCode=D.DeptCode ");
            SQL.AppendLine("Left Join TblSite E On B.SiteCode=E.SiteCode ");
            SQL.AppendLine("Where A.DAGCode=@DAGCode Order By E.SiteName, D.DeptName, B.EmpName; ");
            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "EmpCode",

                    //1-5
                    "EmpName", "EmpCodeOld", "PosName", "DeptName", "SiteName"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && e.ColIndex == 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmDocApprovalGroupDlg(this));
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && BtnSave.Enabled) Sm.FormShowDialog(new FrmDocApprovalGroupDlg(this));
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDAGCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtDAGCode);
        }

        private void TxtDAGName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtDAGCode);
        }

        #endregion

        #endregion
    }
}
