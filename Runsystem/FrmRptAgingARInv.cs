﻿#region Update
/*
    14/07/2017 [TKG] menghitung balance juga berdasarkan settlement
    04/11/2017 [TKG] Data yg Balance=0 tidak ditampilkan lagi. 
    08/11/2017 [HAR] tambah informasi tax
    09/11/2017 [HAR] bug fixing saat tax amount 3 kosong
    06/02/2018 [TKG] KIM bisa melihat data dokumen invoice
    03/06/2020 [IBL/MMM] Tambah filter document date
    04/09/2020 [TKG/IMS] bug saat menampilkan entity, warehouse, do to customer
    04/03/2021 [DITA/IMS] tambah loop untuk melihat transaksi voucher berdasarkan param : IsRptShowVoucherLoop
    25/03/2021 [ICA/SIER] bug saat klik lup ke sales invoice
    12/04/2021 [TKG/SRN] tambah customer category
 *  26/07/2021 [ICA/IMS] menampilkan document fulfilled berdasarkan param IsRptAgingInvoiceARShowFulfilledDocument
 *  14/10/2021 [YOG/AMKA] Menambah kolom departement di Aging Invoice AR disebelah kanan Customer's Category berdasarkan parameter RptAgingARInvDlgFormat
 *  18/10/2021 [SET/AMKA] Merubah tampilan List of Invoice di Aging Invoice AR berdasar param RptAgingARInvDlgFormat
 *  09/11/2021 [VIN/AMKA] Amka lup -> SLI based on DO 
 *  11/11/2021 [RIS/AMKA] Membuat field customer dan customer category terfilter berdasarkan group user & param IsFilterByCtCt
 *  11/11/2021 [RIS/AMKA] Memfilter dokumen berdasarkan department group user & param IsFilterByDept
 *  25/11/2021 [SET/PHT] Menambahkan multi profit center di reporting aging Invoice AR, profit center berdasarkan cost center warehouse nya menggunakan parameter IsFicoUseMultiProfitCenterFilter, IsFilterByProfitCenter
 *  13/01/2022 [TRI/AMKA] Mengganti Tulisan Current AP menjadi Current AR di grid
 *  23/02/2022 [VIN/ALL] Bug: tambah union all ke doct2hdr
 *  23/03/2022 [VIN/ALL] Bug: kurang group by 
    07/06/2022 [SET/PHT] Feedback - penyesuain source data dari multi profit center 
    02/11/2022 [HAR/GSS] tambah tarikan dari sales invoice project
    03/11/2022 [HAR/IOK] DOctdocno berdasarkan param mIsRptAgingARInvShowDOCt
    09/11/2022 [HAR/GSS] bug : salesproject yang sdh di invoice kan amsih muncul
    02/01/2023 [ICA/MNET] menambah tarikan dari net off payment
    02/02/2023 [RDA/MNET] typo filter profit center
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

#endregion

namespace RunSystem
{
    public partial class FrmRptAgingARInv : RunSystem.FrmBase6
    {
        #region Field

        private List<String> mlProfitCenter = null;
        private string  mSQL = string.Empty, mDocTitle = string.Empty,
            mRptAgingARInvDlgFormat = string.Empty,
            mQueryProfitCenter = string.Empty;
        private bool 
            mMInd = false, 
            mIsRptAgingARInvShowDOCt = false,
            mIsRptAgingInvoiceARShowFulfilledDocument = false,
            mIsFilterByCtCt = false,
            mIsFilterByDept = false,
            mIsFicoUseMultiProfitCenterFilter = false,
            mIsFilterByProfitCenter = false,
            mIsAllProfitCenterSelected = false;
        internal bool mIsUseMInd = false, mIsRptAgingARInvUseDocDtFilter = false, mIsRptShowVoucherLoop = false;
        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;

        #endregion

        #region Constructor

        public FrmRptAgingARInv(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                if (!mIsRptAgingARInvUseDocDtFilter)
                {
                    label2.Visible = DteDocumentDt1.Visible = label6.Visible = DteDocumentDt2.Visible = ChkDocumentDt.Visible = false;
                }
                if (!mIsFicoUseMultiProfitCenterFilter)
                {
                    LblMultiProfitCenterCode.Visible = false;
                    CcbProfitCenterCode.Visible = false;
                    ChkProfitCenterCode.Visible = false;
                }
                mlProfitCenter = new List<String>();
                SetGrd();
                Sl.SetLueCtCode(ref LueCtCode, string.Empty, mIsFilterByCtCt ? "Y" : "N");
                Sl.SetLueCtCtCode(ref LueCtCtCode, string.Empty, mIsFilterByCtCt ? "Y" : "N");
                SetLueStatus();
                Sm.SetLue(LueStatus, "OP");
                SetCcbProfitCenterCode(ref CcbProfitCenterCode);
                LblStatus.Visible = LueStatus.Visible = ChkStatus.Visible = mIsRptAgingInvoiceARShowFulfilledDocument;
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private string GetSQL(ref MySqlCommand cm)
        {
            var SQL = new StringBuilder();
            string Filter = " ", Filter2 = " ", Filter3 = " ", Filter4 = " ", Filter5 = " ";

            SetProfitCenter();

            Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCode), "A.CtCode", true);
            
            Filter2 = Filter;

            Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DueDt");
            Sm.FilterDt(ref Filter2, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDt");
            Sm.FilterDt(ref Filter3, ref cm, Sm.GetDte(DteDocumentDt1), Sm.GetDte(DteDocumentDt2), "A.DocDt");
            Sm.FilterStr(ref Filter4, ref cm, Sm.GetLue(LueCtCtCode), "T2.CtCtCode", true);
            if (Sm.GetLue(LueStatus) == "OP")
                Filter5 += " And T1.Status In ('O', 'P') ";
            else
                Sm.FilterStr(ref Filter5, ref cm, Sm.GetLue(LueStatus), "T1.Status", true);

            Sm.CmParam<String>(ref cm, "@MInd", mMInd ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            //if (ChkProfitCenterCode.Checked) Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());

            if (ChkProfitCenterCode.Checked)
            {
                if (!mIsAllProfitCenterSelected)
                {
                    var Filter_ = string.Empty;
                    mQueryProfitCenter = null;
                    int i = 0;
                    foreach (var x in mlProfitCenter.Distinct())
                    {
                        if (Filter_.Length > 0) Filter_ += " Or ";
                        Filter_ += " (K.ProfitCenterCode=@ProfitCenter_" + i.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@ProfitCenter_" + i.ToString(), x);
                        i++;
                    }
                    if (Filter_.Length == 0)
                    {
                        mQueryProfitCenter += "    And 1=0 ";
                    }
                    else
                    {
                        mQueryProfitCenter += "    And (" + Filter_ + ") ";
                    }
                }
                else
                {
                    if (ChkProfitCenterCode.Checked)
                    {
                        mQueryProfitCenter += "    And Find_In_Set(K.ProfitCenterCode, @ProfitCenterCode2) ";
                        if (ChkProfitCenterCode.Checked)
                            Sm.CmParam<String>(ref cm, "@ProfitCenterCode2", GetCcbProfitCenterCode());
                    }
                    else
                    {
                        mQueryProfitCenter += "    And K.ProfitCenterCode In ( ";
                        mQueryProfitCenter += "        Select Distinct ProfitCenterCode From TblGroupProfitCenter T ";
                        mQueryProfitCenter += "        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ";
                        mQueryProfitCenter += "    ) ";
                    }
                }
            }

            SQL.AppendLine("Select T1.Period, T1.DueDt, T1.AgingDays, T1.CtCode, T1.DocNo, T1.DocDt, T2.CtName, T4.CtCtName, T1.CurCode, T1.LocalDocNo, T1.TaxInvDocument, ");
            SQL.AppendLine("T1.TotalAmt, T1.TotalTax, T1.Downpayment, T1.Amt, T1.PaidAmt, T1.VoucherDocNo,T1.Balance, ");
            SQL.AppendLine("T1.DueDt, T1.AgingDays, ");
            SQL.AppendLine("Case T1.Status When 'O' Then 'Outstanding' When 'F' Then 'Fulfilled' When 'P' Then 'Partial' End As StatusDesc, ");
            SQL.AppendLine("If (AgingDays < 0, Balance, 0) As AgingCurrent, ");
            SQL.AppendLine("If (AgingDays > 0 And AgingDays < 31, Balance, 0) As Aging1To30, ");
            SQL.AppendLine("If (AgingDays > 30 And AgingDays < 61, Balance, 0) As Aging31To60, ");
            SQL.AppendLine("If (AgingDays > 60 And AgingDays < 91, Balance, 0) As Aging61To90, ");
            SQL.AppendLine("If (AgingDays > 90 And AgingDays < 121, Balance, 0) As Aging91To120, ");
            SQL.AppendLine("If (AgingDays > 120, Balance, 0) As AgingOver120, T1.Remark, T1.WhsName, T1.EntName, ");
            if (mIsRptAgingARInvShowDOCt)
                SQL.AppendLine("T3.DOCtDocNo, ");
            else
                SQL.AppendLine("Null As DOCtDocNo, ");
            SQL.AppendLine("T1.taxCode1, ifnull(T1.taxAmt1, 0) As TaxAmt1, T1.taxCode2, ifnull(T1.taxAmt2, 0)  As TaxAmt2, T1.taxCode3, ifnull(T1.taxAmt3, 0) As TaxAmt3, ");
            if (mIsRptAgingARInvShowDOCt)
                SQL.AppendLine("T3.DOCtDocNo, ");
            else
                SQL.AppendLine("Null As DOCtDocNo, ");
            SQL.AppendLine(" T5.DeptName As DeptName ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select Concat(Left(A.DocDt, 4), '-',Substring(A.DocDt, 5, 2)) As Period,  ");
            SQL.AppendLine("    A.DueDt, DateDiff(Left(currentdatetime(), 8), A.DueDt) As AgingDays,  ");
            SQL.AppendLine("    A.CtCode, A.DocNo, A.Docdt, A.LocalDocNo, A.TaxInvDocument, A.CurCode,  ");
            SQL.AppendLine("    A.TotalAmt, A.TotalTax, A.Downpayment, A.Amt, IfNull(B.PaidAmt, 0)+IfNull(C.ARSAmt, 0)+IfNull(M.NOPAmt, 0) As PaidAmt, Concat(IfNull(B.VoucherDocNo, ''), IfNull(M.VoucherNOPDocNo, '')) VoucherDocNo,  ");
            SQL.AppendLine("    A.Amt-IfNull(B.PaidAmt, 0)-IfNull(C.ARSAmt, 0)-IfNull(M.NOPAmt, 0) As Balance, A.Remark, ");
            SQL.AppendLine("    g.WhsName, G.EntName, G.DOCtDocNo, ");
            SQL.AppendLine("    Case ");
            SQL.AppendLine("        When A.Amt-IfNull(B.PaidAmt, 0)-IfNull(C.ARSAmt, 0)-IfNull(M.NOPAmt, 0) = A.Amt then 'O' ");
            SQL.AppendLine("        When A.Amt-IfNull(B.PaidAmt, 0)-IfNull(C.ARSAmt, 0)-IfNull(M.NOPAmt, 0) = 0 then 'F' ");
            SQL.AppendLine("        else 'P' ");
            SQL.AppendLine("    End Status, ");
		    SQL.AppendLine("    A.TaxCode1, ((D.Taxrate/100) * A.TotalAmt) taxAmt1, A.TaxCode2, ((E.Taxrate/100) * A.TotalAmt) taxAmt2, A.TaxCode3 , ((F.Taxrate/100) * A.TotalAmt) As taxAmt3 ");
            SQL.AppendLine("    From TblSalesInvoiceHdr A  ");
            SQL.AppendLine("    Left Join (  ");
            SQL.AppendLine("        Select B1.DocNo, Sum(B2.Amt) As PaidAmt, Group_Concat(IfNull(B5.DocNo, '')) VoucherDocNo  ");
            SQL.AppendLine("        From TblSalesInvoiceHdr B1  ");
            SQL.AppendLine("        Inner Join TblIncomingPaymentDtl B2 On B1.DocNo=B2.InvoiceDocNo And B2.InvoiceType='1'  ");
            SQL.AppendLine("        Inner Join TblIncomingPaymentHdr B3 On B2.DocNo=B3.DocNo  ");
            SQL.AppendLine("        Inner Join TblVoucherRequestHdr B4 On B3.VoucherRequestDocNo=B4.DocNo  ");
            SQL.AppendLine("        Inner Join TblVoucherHdr B5 On B4.VoucherDocNo=B5.DocNo And B5.CancelInd='N'  ");
            SQL.AppendLine("        Where B1.CancelInd='N' " + Filter.Replace("A.", "B1."));
            SQL.AppendLine("        Group By B1.DocNo  ");
            SQL.AppendLine("    ) B On A.DocNo=B.DocNo  ");
            SQL.AppendLine("    Left Join (  ");
            SQL.AppendLine("        Select T1.DocNo, Sum(T2.Amt) As ARSAmt  ");
            SQL.AppendLine("        From TblSalesInvoiceHdr T1  ");
            SQL.AppendLine("        Inner Join TblARSHdr T2 On T1.DocNo=T2.SalesInvoiceDocNo And T2.CancelInd='N'  ");
            SQL.AppendLine("        Where T1.CancelInd='N' " + Filter.Replace("A.", "T1."));
            SQL.AppendLine("        Group By T1.DocNo  ");
            SQL.AppendLine("    ) C On A.DocNo=C.DocNo  ");
            SQL.AppendLine("    Left Join Tbltax D On A.TaxCode1 = D.TaxCode ");
            SQL.AppendLine("    Left Join Tbltax E On A.TaxCode2 = E.TaxCode ");
            SQL.AppendLine("    Left Join Tbltax F On A.TaxCode3 = F.TaxCode ");
            SQL.AppendLine("    Left Join  ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("	        Select A.DocNo, ");
            SQL.AppendLine("            Group_Concat(Distinct D.WhsName Separator ', ') As WhsName, ");
            SQL.AppendLine("            Group_Concat(Distinct G.EntName Separator ', ') As EntName, ");
            SQL.AppendLine("            Group_Concat(Distinct C.DocNo Separator ', ') As DOCtDocNo ");
            SQL.AppendLine("	        From tblSalesInvoiceHdr A ");
            SQL.AppendLine("	        Inner Join TblsalesInvoiceDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("	        Inner Join TblDOCthdr C On B.DOCtDocNo = C.DocNo ");
            SQL.AppendLine("	        Inner Join TblWarehouse D On C.WhSCode = D.WhsCode ");
            SQL.AppendLine("	        Left Join TblCostcenter E On D.CCCode = E.CCCode ");
            SQL.AppendLine("	        left Join TblProfitcenter F On E.ProfitCenterCode = F.ProfitcenterCode ");
            SQL.AppendLine("	        left Join TblEntity G On F.EntCode = G.EntCode ");
            SQL.AppendLine("	        Group By A.DocNo ");
            SQL.AppendLine("    )G On A.DocNo = G.DocNo ");
            if (mIsFicoUseMultiProfitCenterFilter)
            {
                SQL.AppendLine("Inner Join TblsalesInvoiceDtl H On A.DocNo = H.DocNo ");
                SQL.AppendLine("Inner Join TblDOCthdr I On H.DOCtDocNo = I.DocNo  ");
                SQL.AppendLine("Inner Join TblWarehouse J ON I.WhSCode = J.WhsCode ");
                SQL.AppendLine("Left Join TblCostcenter K On J.CCCode = K.CCCode ");
                SQL.AppendLine("Left Join TblProfitcenter L On K.ProfitCenterCode = L.ProfitcenterCode ");
            }
            SQL.AppendLine("    Left Join (  ");
            SQL.AppendLine("        Select A.DocNo, Sum(B.Amt) As NOPAmt, Group_Concat(IfNull(E.DocNo, '')) VoucherNOPDocNo  ");
            SQL.AppendLine("        From TblSalesInvoiceHdr A  ");
            SQL.AppendLine("        Inner Join TblNetOffPaymentDtl B On A.DocNo=B.InvoiceDocNo And B.InvoiceType='1'  ");
            SQL.AppendLine("        Inner Join TblNetOffPaymentHdr C On B.DocNo=C.DocNo  ");
            SQL.AppendLine("        Inner Join TblVoucherRequestHdr D On C.VoucherRequestDocNo=D.DocNo  ");
            SQL.AppendLine("        Inner Join TblVoucherHdr E On D.VoucherDocNo=E.DocNo And E.CancelInd='N'  ");
            SQL.AppendLine("        Where A.CancelInd='N' " + Filter);
            SQL.AppendLine("        Group By A.DocNo  ");
            SQL.AppendLine("    ) M On A.DocNo=M.DocNo  ");
            if (!mIsRptAgingARInvUseDocDtFilter)
                SQL.AppendLine("    Where A.CancelInd='N'  ");
            else
                SQL.AppendLine("    Where A.CancelInd='N' " + Filter3);

            if (mIsUseMInd)
            {
                if (!mMInd)
                    SQL.AppendLine("        And A.MInd='N' ");
            }
            if (mIsFicoUseMultiProfitCenterFilter)
            {
                //SQL.AppendLine("AND FIND_IN_SET(K.ProfitCenterCode, @ProfitCenterCode) ");                
                SQL.AppendLine(mQueryProfitCenter);
            }
            SQL.AppendLine(Filter);

            //rewgin sales project
            SQL.AppendLine("    UNION ALL");
            SQL.AppendLine("    SELECT  ");
            SQL.AppendLine("    Concat(Left(A.DocDt, 4), '-', Substring(A.DocDt, 5, 2)) As Period, ");
            SQL.AppendLine("    A.DueDt, DateDiff(Left(currentdatetime(), 8), A.DueDt) As AgingDays, ");
            SQL.AppendLine("    A.CtCode, A.DocNo, A.Docdt, A.LocalDocNo, null As TaxInvDocument, A.CurCode, ");
            SQL.AppendLine("    A.TotalAmt, A.TotalTax, A.Downpayment, A.Amt, IfNull(B.PaidAmt, 0)-IfNull(G.NOPAmt, 0) As PaidAmt, Concat(IfNull(B.VoucherDocNo, ''), IfNull(G.VoucherNOPDocNo, '')) VoucherDocNo, ");
            SQL.AppendLine("    A.Amt - IfNull(B.PaidAmt, 0) - IfNull(G.NOPAmt, 0) As Balance, A.Remark, ");
            SQL.AppendLine("    null WhsName, null EntName, S1.PDDocno as DOCtDocNo, ");
            SQL.AppendLine("    Case  ");
            SQL.AppendLine("    When A.Amt - IfNull(B.PaidAmt, 0) - IfNull(G.NOPAmt, 0) = A.Amt then 'O'  ");
            SQL.AppendLine("    When A.Amt - IfNull(B.PaidAmt, 0) - IfNull(G.NOPAmt, 0) = 0 then 'F'  ");
            SQL.AppendLine("        else 'P'  ");
            SQL.AppendLine("    End Status, ");
            SQL.AppendLine("    A.TaxCode1, ((D.Taxrate / 100) * A.TotalAmt) taxAmt1, A.TaxCode2, ((E.Taxrate / 100) * A.TotalAmt) taxAmt2, A.TaxCode3 , ((F.Taxrate / 100) * A.TotalAmt) As taxAmt3  ");
            SQL.AppendLine("    From tblsalesinvoice5hdr A  ");
            SQL.AppendLine("    Left Join( ");
            SQL.AppendLine("        Select B1.DocNo, Sum(B2.Amt) As PaidAmt, Group_Concat(IfNull(B5.DocNo, '')) VoucherDocNo  ");
            SQL.AppendLine("        From tblsalesinvoice5hdr B1  ");
            SQL.AppendLine("        Inner Join TblIncomingPaymentDtl B2 On B1.DocNo = B2.InvoiceDocNo And B2.InvoiceType = '5'  ");
            SQL.AppendLine("        Inner Join TblIncomingPaymentHdr B3 On B2.DocNo = B3.DocNo  ");
            SQL.AppendLine("        Inner Join TblVoucherRequestHdr B4 On B3.VoucherRequestDocNo = B4.DocNo  ");
            SQL.AppendLine("        Inner Join TblVoucherHdr B5 On B4.VoucherDocNo = B5.DocNo And B5.CancelInd = 'N'  ");
            SQL.AppendLine("        Where B1.CancelInd = 'N'  ");
            SQL.AppendLine("        Group By B1.DocNo  ");
            SQL.AppendLine("    )B On A.DocNo = B.DocNo  ");
            SQL.AppendLine("    Left Join Tbltax D On A.TaxCode1 = D.TaxCode  ");
            SQL.AppendLine("    Left Join Tbltax E On A.TaxCode2 = E.TaxCode  ");
            SQL.AppendLine("    Left Join Tbltax F On A.TaxCode3 = F.TaxCode  ");
            SQL.AppendLine("    LEFT JOIN(  ");
            SQL.AppendLine("        SELECT F.DocNo,  ");
            SQL.AppendLine("        GROUP_CONCAT(P.DocNo) AS PDDocNo  ");
            SQL.AppendLine("        FROM tblsalesinvoice5hdr F  ");
            SQL.AppendLine("        Inner Join tblprojectimplementationhdr G ON F.ProjectImplementationDocNo = G.DocNo AND G.`Status` = 'A' AND G.CancelInd = 'N'  ");
            SQL.AppendLine("        LEFT JOIN tblprojectdeliveryhdr P ON G.DocNo = P.PRJIDocNo AND P.canCelInd = 'N'  ");
            SQL.AppendLine("        WHERE F.CancelInd = 'N'  ");
            SQL.AppendLine("        Group By F.DocNo  ");
            SQL.AppendLine("    )S1 ON A.DocNO = S1.DocNo  ");
            SQL.AppendLine("    Left Join (  ");
            SQL.AppendLine("        Select A.DocNo, Sum(B.Amt) As NOPAmt, Group_Concat(IfNull(E.DocNo, '')) VoucherNOPDocNo  ");
            SQL.AppendLine("        From TblSalesInvoiceHdr A  ");
            SQL.AppendLine("        Inner Join TblNetOffPaymentDtl B On A.DocNo=B.InvoiceDocNo And B.InvoiceType='5'  ");
            SQL.AppendLine("        Inner Join TblNetOffPaymentHdr C On B.DocNo=C.DocNo  ");
            SQL.AppendLine("        Inner Join TblVoucherRequestHdr D On C.VoucherRequestDocNo=D.DocNo  ");
            SQL.AppendLine("        Inner Join TblVoucherHdr E On D.VoucherDocNo=E.DocNo And E.CancelInd='N'  ");
            SQL.AppendLine("        Where A.CancelInd='N' ");
            SQL.AppendLine("        Group By A.DocNo  ");
            SQL.AppendLine("    )G On A.DocNo=G.DocNo  ");
            SQL.AppendLine("    Where A.CancelInd = 'N' ");

            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select Concat(Left(A.DocDt, 4), '-',Substring(A.DocDt, 5, 2)) As Period,  ");
			SQL.AppendLine("    A.DocDt As DueDt,  ");
			SQL.AppendLine("    DateDiff(Left(currentdatetime(), 8), A.DocDt) As AgingDays,  ");
			SQL.AppendLine("    A.CtCode, A.DocNo, A.DocDt,  A.LocalDocNo, A.TaxInvDocument, A.CurCode,  ");
			SQL.AppendLine("    -1*A.TotalAmt As TotalAmt,  ");
			SQL.AppendLine("    0 As TotalTax,  ");
			SQL.AppendLine("    0 As Downpayment,  ");
			SQL.AppendLine("    -1*A.TotalAmt As Amt,  ");
			SQL.AppendLine("    IfNull(B.PaidAmt, 0)+IfNull(D.NOPAmt, 0.00) As PaidAmt, Concat(IfNull(B.VoucherDocNo, ''), IfNull(D.VoucherNOPDocNo, '')) VoucherDocNo,   ");
			SQL.AppendLine("    (-1*A.TotalAmt)+IfNull(B.PaidAmt, 0)+IfNull(D.NOPAmt, 0.00) As Balance, A.Remark, ");
            SQL.AppendLine("    C.WhsName, C.EntName, C.DOCtDocNo, ");
            SQL.AppendLine("    Case ");
            SQL.AppendLine("        When (-1*A.TotalAmt)+IfNull(B.PaidAmt, 0)+IfNull(D.NOPAmt, 0.00) = (-1*A.TotalAmt) then 'O' ");
            SQL.AppendLine("        When (-1*A.TotalAmt)+IfNull(B.PaidAmt, 0)+IfNull(D.NOPAmt, 0.00) = 0 then 'F' ");
            SQL.AppendLine("        else 'P' ");
            SQL.AppendLine("    End Status, ");
			SQL.AppendLine("    null, 0 taxAmt1, null, 0 taxAmt2, null, 0 As taxAmt3 ");
			SQL.AppendLine("    From TblSalesReturnInvoiceHdr A  ");
			SQL.AppendLine("    Left Join (  ");
            SQL.AppendLine("        Select B1.DocNo, Sum(B2.Amt) As PaidAmt, Group_Concat(IfNull(B5.DocNo, '')) VoucherDocNo  ");
			SQL.AppendLine("        From TblSalesReturnInvoiceHdr B1  ");
			SQL.AppendLine("        Inner Join TblIncomingPaymentDtl B2 On B1.DocNo=B2.InvoiceDocNo And B2.InvoiceType='2'  ");
			SQL.AppendLine("        Inner Join TblIncomingPaymentHdr B3 On B2.DocNo=B3.DocNo  ");
			SQL.AppendLine("        Inner Join TblVoucherRequestHdr B4 On B3.VoucherRequestDocNo=B4.DocNo  ");
			SQL.AppendLine("        Inner Join TblVoucherHdr B5 On B4.VoucherDocNo=B5.DocNo And B5.CancelInd='N'  ");
            SQL.AppendLine("        Where B1.CancelInd='N' " + Filter2.Replace("A.", "B1."));
            SQL.AppendLine("        Group By B1.DocNo  ");
			SQL.AppendLine("    ) B On A.DocNo=B.DocNo  ");
            SQL.AppendLine("    Left Join ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("	    Select A.DocNo, ");
            SQL.AppendLine("        Group_Concat(Distinct D.WhsName Separator ', ') As WhsName, ");
            SQL.AppendLine("        Group_Concat(Distinct G.EntName Separator ', ') As EntName, ");
            SQL.AppendLine("        Group_Concat(Distinct C.DocNo Separator ', ') As DOCtDocNo ");
            SQL.AppendLine("	    From tblSalesReturnInvoiceDtl A  ");
            SQL.AppendLine("	    Inner Join tblRecvCtHdr B On A.RecvCtDocno = B.DocNo ");
            SQL.AppendLine("	    Inner Join TblDOCthdr C On B.DOCtDocNo = C.DocNo ");
            SQL.AppendLine("	    Inner Join TblWarehouse D On C.WhSCode = D.WhsCode ");
            SQL.AppendLine("	    Left Join TblCostcenter E On D.CCCode = E.CCCode ");
            SQL.AppendLine("	    Left Join TblProfitcenter F On E.ProfitCenterCode = F.ProfitcenterCode ");
            SQL.AppendLine("	    Left Join TblEntity G On F.EntCode = G.EntCode ");
            SQL.AppendLine("	    Group By A.DocNo ");
            SQL.AppendLine("    )C On A.DocNo = C.Docno ");
            SQL.AppendLine("    Left Join (  ");
            SQL.AppendLine("        Select A.DocNo, Sum(B.Amt) As NOPAmt, Group_Concat(IfNull(E.DocNo, '')) VoucherNOPDocNo  ");
            SQL.AppendLine("        From TblSalesInvoiceHdr A  ");
            SQL.AppendLine("        Inner Join TblNetOffPaymentDtl B On A.DocNo=B.InvoiceDocNo And B.InvoiceType='2'  ");
            SQL.AppendLine("        Inner Join TblNetOffPaymentHdr C On B.DocNo=C.DocNo  ");
            SQL.AppendLine("        Inner Join TblVoucherRequestHdr D On C.VoucherRequestDocNo=D.DocNo  ");
            SQL.AppendLine("        Inner Join TblVoucherHdr E On D.VoucherDocNo=E.DocNo And E.CancelInd='N'  ");
            SQL.AppendLine("        Where A.CancelInd='N' " + Filter);
            SQL.AppendLine("        Group By A.DocNo  ");
            SQL.AppendLine("    )D On A.DocNo=D.DocNo  ");
            if (mIsFicoUseMultiProfitCenterFilter)
            {
                SQL.AppendLine("    Inner Join TblsalesInvoiceDtl H On A.DocNo = H.DocNo ");
                SQL.AppendLine("    Inner Join TblDOCthdr I On H.DOCtDocNo = I.DocNo  ");
                SQL.AppendLine("    Inner Join TblWarehouse J ON I.WhSCode = J.WhsCode ");
                SQL.AppendLine("    Left Join TblCostcenter K On J.CCCode = K.CCCode ");
                SQL.AppendLine("    Left Join TblProfitcenter L On K.ProfitCenterCode = L.ProfitcenterCode ");
            }
            SQL.AppendLine("    Where A.CancelInd='N'  ");
            if (mIsFicoUseMultiProfitCenterFilter)
            {
                //SQL.AppendLine("AND FIND_IN_SET(K.ProfitCenterCode, @ProfitCenterCode) ");
                SQL.AppendLine(mQueryProfitCenter);
            }
            SQL.AppendLine(Filter2);

            SQL.AppendLine(") T1 ");
            SQL.AppendLine("Inner Join TblCustomer T2 On T1.CtCode=T2.CtCode ");
            SQL.AppendLine(Filter4);
            if (mIsRptAgingARInvShowDOCt)
            {
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select A.DocNo, ");
                SQL.AppendLine("    Group_Concat(Distinct B.DOCtDocNo Order By B.DOCtDocNo Separator ', ') As DOCtDocNo ");
                SQL.AppendLine("    From TblSalesInvoiceHdr A, TblSalesInvoiceDtl B ");
                SQL.AppendLine("    Where A.DocNo=B.DocNo ");
                SQL.AppendLine("    And B.DOCtDocNo Is Not Null ");
                SQL.AppendLine("    And A.CancelInd='N' ");
                SQL.AppendLine(Filter);
                SQL.AppendLine("    Group By A.DocNo ");
                SQL.AppendLine("    UNION ALL ");
                SQL.AppendLine("    SELECT A.DocNO, GROUP_CONCAT(distinct C.DocNo) ");
                SQL.AppendLine("  FROM tblsalesinvoice5hdr A ");
                SQL.AppendLine("    Left JOIN tblprojectimplementationhdr B ON A.ProjectImplementationDocNo = B.DocNo AND B.`Status` = 'A' ");
                SQL.AppendLine("    Left JOIN tblprojectdeliveryhdr C ON B.DocNo = C.PRJIDocNo AND C.CancelInd = 'N' ");
                SQL.AppendLine("    WHERE A.cancelInd = 'N' ");
                SQL.AppendLine("    Group By A.DocNo ");
                SQL.AppendLine(") T3 On T1.DocNo=T3.DocNo ");
            }
                SQL.AppendLine("Left Join TblCustomerCategory T4 On T2.CtCtCode=T4.CtCtCode ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("       Select A.DocNo, ");
                SQL.AppendLine("       Group_Concat(Distinct IfNull(E.DeptName, '') SEPARATOR ', ') AS DeptName,  E.DeptCode ");
                SQL.AppendLine("       FROM tblSalesInvoiceDtl A ");
                SQL.AppendLine("       Inner Join tblDoctHdr B ON B.DocNo = A.DOCtDocNo ");
                SQL.AppendLine("       Inner Join tblWarehouse C ON C.WhsCode = B.WhsCode ");
                SQL.AppendLine("       Inner Join tblCostCenter D ON D.CCCode = C.CCCode ");
                SQL.AppendLine("       Inner Join tblDepartment E ON E.DeptCode = D.DeptCode ");
                SQL.AppendLine("       Group By A.DocNo, E.DeptCode ");
                SQL.AppendLine("       Union All ");
                SQL.AppendLine("       Select A.DocNo, ");
                SQL.AppendLine("       Group_Concat(Distinct IfNull(E.DeptName, '') SEPARATOR ', ') AS DeptName,  E.DeptCode ");
                SQL.AppendLine("       FROM tblSalesInvoiceDtl A ");
                SQL.AppendLine("       Inner Join tblDoct2Hdr B ON B.DocNo = A.DOCtDocNo ");
                SQL.AppendLine("       Inner Join tblWarehouse C ON C.WhsCode = B.WhsCode ");
                SQL.AppendLine("       Inner Join tblCostCenter D ON D.CCCode = C.CCCode ");
                SQL.AppendLine("       Inner Join tblDepartment E ON E.DeptCode = D.DeptCode ");
                SQL.AppendLine("       Group By A.DocNo, E.DeptCode ");
                SQL.AppendLine("       Union all ");

                SQL.AppendLine("       SELECT F.DocNo, IfNull(M.DeptName, O.DeptName) AS DeptName, IFNULL(M.deptCode, N.ParValue) AS DeptCode ");
                SQL.AppendLine("       FROM tblsalesinvoice5hdr F ");
                SQL.AppendLine("           Inner Join tblprojectimplementationhdr G ON F.ProjectImplementationDocNo = G.DocNo AND G.`Status` = 'A' AND G.CancelInd = 'N' ");
                SQL.AppendLine("           INNER JOIN tblsocontractrevisionhdr H ON G.SOContractDocNo = H.DocNo AND H.`Status` = 'A' ");
                SQL.AppendLine("           INNER JOIN tblsocontracthdr I ON H.SOCDocNo = I.DocNo AND I.`Status` = 'A' AND I.CancelInd = 'N' ");
                SQL.AppendLine("           INNER JOIN tblboqhdr J ON I.BOQDocNo = J.DocNo AND J.ActInd = 'Y' AND J.`Status` = 'A' ");
                SQL.AppendLine("           INNER JOIN tbllophdr K ON J.LOPDocNo = K.DocNo AND K.CancelInd = 'N' AND K.`Status` = 'A' ");
                SQL.AppendLine("           Left JOIN tblcostcenter L ON K.CCCode = L.CCCode ");
                SQL.AppendLine("       Left JOIN tbldepartment M ON L.DeptCode = M.DeptCode ");
                SQL.AppendLine("           Left JOIN tblparameter N ON 0 = 0 AND N.parcode = 'IncomingPaymentDeptCode' ");
                SQL.AppendLine("           LEFT JOIN tbldepartment O ON N.Parvalue = O.DeptCode ");
                SQL.AppendLine("           WHERE F.CancelInd = 'N' ");
                SQL.AppendLine("           Group By F.DocNo ");

            SQL.AppendLine(") T5 ON T5.DocNo = T1.DocNo ");
            if (!mIsRptAgingInvoiceARShowFulfilledDocument)
                SQL.AppendLine("Where T1.Balance<>0.00 ");
            else
            {
                SQL.AppendLine("Where 0 = 0 ");
                SQL.AppendLine(Filter5);
            }
            if (mIsFilterByDept)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupDepartment ");
                SQL.AppendLine("    Where DeptCode=T5.DeptCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            if (mIsFilterByCtCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupCustomerCategory ");
                SQL.AppendLine("    Where CtCtCode=T4.CtCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Order By T1.Period, T1.CurCode, T2.CtName, T1.DocNo; ");

            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 39;
            Grd1.ReadOnly = false;
            Grd1.FrozenArea.ColCount = 7;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No", 
                        //1-5
                        "Period",
                        "Currency",
                        "Customer", 
                        "Invoice#",
                        "",
                        //6-10
                        "Local"+Environment.NewLine+"Document#",
                        "Document"+Environment.NewLine+"Date",
                        "Remark",
                        "Entity",
                        "DO",
                        //11-15
                        "Warehouse",
                        "Tax"+Environment.NewLine+"Invoice#",
                        "Amount"+Environment.NewLine+"Before Tax",
                        "Tax"+Environment.NewLine+"Amount",
                        "Downpayment",
                        //16-20
                        "Invoice"+Environment.NewLine+"Amount",
                        "Paid/"+Environment.NewLine+"Settled",
                        "Balance",
                        "Due"+Environment.NewLine+"Date",
                        "Aging"+Environment.NewLine+"(Days)",
                        //21-25
                        "Aging"+Environment.NewLine+"Current AR",
                        "Over Due"+Environment.NewLine+"1-30 Days",
                        "Over Due"+Environment.NewLine+"31-60 Days",
                        "Over Due"+Environment.NewLine+"61-90 Days",
                        "Over Due"+Environment.NewLine+"91-120 Days",
                        //26-30
                        "Over 120 Days",
                        "Tax 1",
                        "Tax Amount 1",
                        "Tax 2",
                        "Tax Amount 2",

                        //31-35
                        "Tax 3",
                        "Tax Amount 3",
                        "DO To Customer",
                        "Voucher#",
                        "",

                        //36-38
                        "Customer's Category",
                        "Status",
                        "Department Name"
                    },
                    new int[] 
                    {
                        //0
                        50,
                        //1-5
                        100, 60, 250, 145, 20,
                        //6-10
                        130, 130, 200, 180, 130, 
                        //11-15
                        130, 130, 130, 80, 130, 
                        //16-20
                        130, 130, 130, 130, 130,
                        //21-25
                        130, 130, 130, 130, 130,
                        //26-30
                        130, 100, 120, 100, 120,
                        //31-35
                        100, 120, 150, 0, 20,

                        //36-38
                        200, 100, 150
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 5, 35 });
            Sm.GrdFormatDec(Grd1, new int[] { 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26,  28,  30,  32 }, 2);
            Sm.GrdFormatDate(Grd1, new int[] { 7, 19 });
            Sm.GrdColInvisible(Grd1, new int[] { 8, 9, 10, 11, 27, 28, 29, 30, 31, 32, 34 }, false);
            if (!mIsRptAgingARInvShowDOCt)
                Sm.GrdColInvisible(Grd1, new int[] { 33 }, false);
            if (!mIsRptShowVoucherLoop) Sm.GrdColInvisible(Grd1, new int[] { 35 }, false);
            if (mRptAgingARInvDlgFormat == "1")
                Grd1.Cols[38].Visible = false;
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 36, 37 });
            Grd1.Cols[36].Move(4);
            Grd1.Cols[35].Move(18);
            Grd1.Cols[37].Move(21);
            Grd1.Cols[38].Move(5);
            Sm.GrdColInvisible(Grd1, new int[] { 37 }, mIsRptAgingInvoiceARShowFulfilledDocument);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 8, 9, 10, 11, 27, 28, 29, 30, 31, 32 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            if (mIsFicoUseMultiProfitCenterFilter)
                if (IsProfitCenterInvalid()) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    GetSQL(ref cm),
                    new string[]
                        { 
                                //0
                                "Period", 
                                //1-5
                                "CurCode", "CtName", "DocNo", "LocalDocNo", "DocDt", 
                                //6-10
                                "Remark", "EntName", "DOCtDocNo", "WhsName", "TaxInvDocument", 
                                //11-15
                                "TotalAmt", "TotalTax", "Downpayment", "Amt", "PaidAmt",   
                                //16-20
                                "Balance", "DueDt", "AgingDays", "AgingCurrent", "Aging1To30",                 
                                //21-25
                                "Aging31To60", "Aging61To90", "Aging91To120", "AgingOver120", "TaxCode1",
                                //26-30
                                "TaxAmt1", "TaxCode2", "TaxAmt2", "TaxCode3", "TaxAmt3",
                                //31-35
                                "DOCtDocNo", "VoucherDocNo", "CtCtName", "StatusDesc", "DeptName"
                        },

                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd1.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 7, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 10);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 11);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 12);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 13);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 14);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 15);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 16);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 19, 17);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 18);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 19);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 22, 20);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 23, 21);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 24, 22);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 25, 23);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 26, 24);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 27, 25);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 28, 26);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 29, 27);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 30, 28);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 31, 29);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 32, 30);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 33, 31);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 34, 32);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 36, 33);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 37, 34);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 38, 35);

                    }, true, false, false, false
                );
                Grd1.GroupObject.Add(1);
                Grd1.GroupObject.Add(2);
                Grd1.GroupObject.Add(3);
                Grd1.Group();
                AdjustSubtotals();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                e.DoDefault = false;
                switch (mDocTitle)
                {
                    case "KMI":
                    case "KIM":
                        {
                            var f1 = new FrmSalesInvoice3(mMenuCode);
                            f1.Tag = mMenuCode;
                            f1.WindowState = FormWindowState.Normal;
                            f1.StartPosition = FormStartPosition.CenterScreen;
                            f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                            f1.ShowDialog();
                            break;
                        }
                    case "AMKA":
                        {
                            var f1 = new FrmSalesInvoice3(mMenuCode);
                            f1.Tag = mMenuCode;
                            f1.WindowState = FormWindowState.Normal;
                            f1.StartPosition = FormStartPosition.CenterScreen;
                            f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                            f1.ShowDialog();
                            break;
                        }
                    default:
                        {
                            var f1 = new FrmSalesInvoice(mMenuCode);
                            f1.Tag = mMenuCode;
                            f1.WindowState = FormWindowState.Normal;
                            f1.StartPosition = FormStartPosition.CenterScreen;
                            f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                            f1.ShowDialog();
                            break;
                        }
                }
            }

            if (e.ColIndex == 35 && Sm.GetGrdStr(Grd1, e.RowIndex, 34).Length != 0)
            {
                var f = new FrmRptAgingARInvDlg(this);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.VoucherDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 34);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                switch (mDocTitle)
                {
                    case "KMI":
                    case "KIM":
                        {
                            var f1 = new FrmSalesInvoice3(mMenuCode);
                            f1.Tag = mMenuCode;
                            f1.WindowState = FormWindowState.Normal;
                            f1.StartPosition = FormStartPosition.CenterScreen;
                            f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                            f1.ShowDialog();
                            break;
                        }
                    case "AMKA":
                        {
                            var f1 = new FrmSalesInvoice3(mMenuCode);
                            f1.Tag = mMenuCode;
                            f1.WindowState = FormWindowState.Normal;
                            f1.StartPosition = FormStartPosition.CenterScreen;
                            f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                            f1.ShowDialog();
                            break;
                        }
                    case "SIER":
                        {
                            if (Sm.GetValue("Select DocType From TblSalesInvoiceDtl where DocNo=@Param", Sm.GetGrdStr(Grd1, e.RowIndex, 4)) == "1")
                            {
                                var f1 = new FrmSalesInvoice(mMenuCode);
                                f1.Tag = mMenuCode;
                                f1.WindowState = FormWindowState.Normal;
                                f1.StartPosition = FormStartPosition.CenterScreen;
                                f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                                f1.ShowDialog();
                            }
                            else if (Sm.GetValue("Select DocType From TblSalesInvoiceDtl where DocNo=@Param", Sm.GetGrdStr(Grd1, e.RowIndex, 4)) == "3")
                            {
                                var f1 = new FrmSalesInvoice3(mMenuCode);
                                f1.Tag = mMenuCode;
                                f1.WindowState = FormWindowState.Normal;
                                f1.StartPosition = FormStartPosition.CenterScreen;
                                f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                                f1.ShowDialog();
                            }
                            break;
                        }
                    default:
                        {
                            var f1 = new FrmSalesInvoice(mMenuCode);
                            f1.Tag = mMenuCode;
                            f1.WindowState = FormWindowState.Normal;
                            f1.StartPosition = FormStartPosition.CenterScreen;
                            f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                            f1.ShowDialog();
                            break;
                        }
                }
            }
                if (mRptAgingARInvDlgFormat == "1" && (e.ColIndex == 35 && Sm.GetGrdStr(Grd1, e.RowIndex, 34).Length != 0))
                {
                    var f = new FrmRptAgingARInvDlg(this);
                    f.Tag = "***";
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.VoucherDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 34);
                    f.ShowDialog();
                }
                if (mRptAgingARInvDlgFormat == "2" && (e.ColIndex == 35 && Sm.GetGrdStr(Grd1, e.RowIndex, 17).Length != 0))
                {
                    var f = new FrmRptAgingARInvDlg2(this);
                    f.Tag = "***";
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.SalesInvoiceDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                    f.ShowDialog();
                }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Method

        private void SetProfitCenter()
        {
            mIsAllProfitCenterSelected = false;

            if (!ChkProfitCenterCode.Checked) mIsAllProfitCenterSelected = true;

            if (mIsAllProfitCenterSelected) return;

            bool IsCompleted = false, IsFirst = true;

            mlProfitCenter.Clear();

            while (!IsCompleted)
                SetProfitCenter(ref IsFirst, ref IsCompleted);
        }

        private void SetProfitCenter(ref bool IsFirst, ref bool IsCompleted)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string Filter = string.Empty, ProfitCenterCode = string.Empty;
            bool IsExisted = false;
            int i = 0;

            SQL.AppendLine("Select Distinct ProfitCenterCode From TblProfitCenter ");
            if (IsFirst)
            {
                if (ChkProfitCenterCode.Checked)
                {
                    SQL.AppendLine("    Where Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ");
                    Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                    IsCompleted = false;
                }
                else
                    IsCompleted = true;
                IsFirst = false;
            }
            else
            {
                SQL.AppendLine("    Where Parent Is Not Null ");
                foreach (var x in mlProfitCenter.Distinct())
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += " (Parent=@ProfitCenterCode" + i.ToString() + ") ";

                    Sm.CmParam<String>(ref cm, "@ProfitCenterCode" + i.ToString(), x);
                    i++;
                }
                if (Filter.Length == 0)
                    SQL.AppendLine("    And 1=0 ");
                else
                {
                    SQL.AppendLine("    And (" + Filter + ") ");
                }
                IsCompleted = true;
            }
            SQL.AppendLine("Order By ProfitCenterCode;");

            cm.CommandText = SQL.ToString();

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ProfitCenterCode" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        if (IsFirst)
                            mlProfitCenter.Add(ProfitCenterCode);
                        else
                        {
                            ProfitCenterCode = Sm.DrStr(dr, c[0]);
                            IsExisted = false;
                            foreach (var x in mlProfitCenter.Where(w => Sm.CompareStr(w, ProfitCenterCode)))
                                IsExisted = true;
                            if (!IsExisted)
                            {
                                mlProfitCenter.Add(ProfitCenterCode);
                                IsCompleted = false;
                            }
                        }
                    }
                }
                else
                    IsCompleted = true;
                dr.Close();
            }
        }

        private void SetCcbProfitCenterCode(ref CheckedComboBoxEdit Ccb)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Col, Col2 From (");
            SQL.AppendLine("    Select T.ProfitCenterName As Col, T.ProfitCenterCode As Col2 ");
            SQL.AppendLine("    From TblProfitCenter T ");
            if (mIsFilterByProfitCenter)
            {
                SQL.AppendLine("WHERE Exists(  ");
                SQL.AppendLine("        Select 1 From TblGroupProfitCenter ");
                SQL.AppendLine("        Where ProfitCenterCode=T.ProfitCenterCode  ");
                SQL.AppendLine("        And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine(") Tbl Order By Col2; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetCcb(ref Ccb, cm);
        }

        private string GetCcbProfitCenterCode()
        {
            var Value = Sm.GetCcb(CcbProfitCenterCode);
            if (Value.Length == 0) return string.Empty;
            return GetProfitCenterCode(Value).Replace(", ", ",");
        }

        private string GetProfitCenterCode(string Value)
        {
            if (Value.Length == 0) return string.Empty;

            return
                Sm.GetValue(
                    "Select Group_Concat(T.Code Separator ', ') As Code " +
                    "From (Select ProfitCenterCode As Code From TblProfitCenter Where Find_In_Set(ProfitCenterName, @Param)) T; ",
                    Value.Replace(", ", ","));
        }

        private bool IsProfitCenterInvalid()
        {
            if (Sm.GetCcb(CcbProfitCenterCode).Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Profit center is empty.");
                CcbProfitCenterCode.Focus();
                return true;
            }

            return false;
        }

        private void GetParameter()
        {
            //string MenuCodeForDocWithMInd = Sm.GetParameter("MenuCodeForDocWithMInd");
            //if (MenuCodeForDocWithMInd.Length > 0)
            //    mMInd = MenuCodeForDocWithMInd.IndexOf("##" + mMenuCode + "##") != -1;
            //mIsUseMInd = Sm.GetParameterBoo("IsUseMInd");
            //mIsRptAgingARInvShowDOCt = Sm.GetParameterBoo("IsRptAgingARInvShowDOCt");
            //mIsRptAgingARInvUseDocDtFilter = Sm.GetParameterBoo("IsRptAgingARInvUseDocDtFilter");
            //mDocTitle = Sm.GetParameter("DocTitle");
            //mIsRptShowVoucherLoop = Sm.GetParameterBoo("IsRptShowVoucherLoop");
           

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'DocTitle', 'IsUseMInd', 'IsRptAgingARInvShowDOCt', 'IsRptAgingARInvUseDocDtFilter', 'IsRptShowVoucherLoop', ");
            SQL.AppendLine("'MenuCodeForDocWithMInd', 'IsRptAgingInvoiceARShowFulfilledDocument', 'RptAgingARInvDlgFormat', 'IsFilterByCtCt', 'IsFilterByDept', ");
            SQL.AppendLine("'IsFicoUseMultiProfitCenterFilter', 'IsFilterByProfitCenter' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsUseMInd": mIsUseMInd = ParValue == "Y"; break;
                            case "IsRptAgingARInvShowDOCt": mIsRptAgingARInvShowDOCt = ParValue == "Y"; break;
                            case "IsRptAgingARInvUseDocDtFilter": mIsRptAgingARInvUseDocDtFilter = ParValue == "Y"; break;
                            case "IsRptShowVoucherLoop": mIsRptShowVoucherLoop = ParValue == "Y"; break;
                            case "IsRptAgingInvoiceARShowFulfilledDocument": mIsRptAgingInvoiceARShowFulfilledDocument = ParValue == "Y"; break;
                            case "IsFilterByCtCt": mIsFilterByCtCt = ParValue == "Y"; break;
                            case "IsFilterByDept": mIsFilterByDept = ParValue == "Y"; break;
                            case "IsFicoUseMultiProfitCenterFilter": mIsFicoUseMultiProfitCenterFilter = ParValue == "Y"; break;
                            case "IsFilterByProfitCenter": mIsFilterByProfitCenter = ParValue == "Y"; break;

                            //string
                            case "DocTitle": mDocTitle = ParValue; break;
                            case "MenuCodeForDocWithMInd":
                                string MenuCodeForDocWithMInd = ParValue;
                                if (MenuCodeForDocWithMInd.Length > 0)
                                    mMInd = MenuCodeForDocWithMInd.IndexOf("##" + mMenuCode + "##") != -1;
                                break;
                            case "RptAgingARInvDlgFormat": mRptAgingARInvDlgFormat = ParValue;
                                if (mRptAgingARInvDlgFormat.Length == 0) mRptAgingARInvDlgFormat = "1";
                                break;
                        }
                    }
                }
                dr.Close();
            }
        }

        private void AdjustSubtotals()
        {
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ForeColor = Color.Black;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.HideSubtotals(Grd1);
            iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 12, 13, 14, 15, 16, 17, 18, 20, 21, 22, 23, 24, 25, 26, 28, 30, 32 });
        }

        private void SetLueStatus()
        {
            Sm.SetLue2(
                ref LueStatus,
                "Select 'O' As Col1, 'Outstanding' As Col2 " +
                "Union All Select 'P' As Col1, 'Partial' As Col2 " +
                "Union All Select 'F' As Col1, 'Fulfilled' As Col2 " +
                "Union All Select 'OP' As Col1, 'Outstanding+Partial' As Col2;",
                0, 35, false, true, "Code", "Status", "Col2", "Col1");
        }


        #endregion

        #endregion

        #region Event

        #region Misc Control Event
        private void CcbProfitCenterCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterCcbSetCheckEdit(this, sender);
        }

        private void ChkProfitCenterCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetCheckedComboBoxEdit(this, sender, "Profit Center");
        }

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue3(Sl.SetLueCtCode), string.Empty, mIsFilterByCtCt ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Due date");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocumentDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocumentDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void ChkDocumentDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Document's date");
        }

        private void LueCtCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCtCode, new Sm.RefreshLue3(Sl.SetLueCtCtCode), string.Empty, mIsFilterByCtCt ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCtCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer's category");
        }

        private void LueStatus_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.CompareStr(Sm.GetLue(LueStatus), "<Refresh>")) LueStatus.EditValue = null;
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkStatus_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Status");
        }

        #endregion

        #endregion

        

    }
}
