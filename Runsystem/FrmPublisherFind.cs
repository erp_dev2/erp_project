﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmPublisherFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmPublisher mFrmParent;

        #endregion

        #region Constructor

        public FrmPublisherFind(FrmPublisher FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            this.Text = mFrmParent.Text;
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            SetGrd();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 13;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Publisher"+Environment.NewLine+"Code", 
                        "Publisher"+Environment.NewLine+"Name",
                        "Active",
                        "Publisher"+Environment.NewLine+"Date",
                        "Register"+Environment.NewLine+"Number",
                        //6-10
                        "Publisher"+Environment.NewLine+"location",
                        "Created"+Environment.NewLine+"By",
                        "Created"+Environment.NewLine+"Date", 
                        "Created"+Environment.NewLine+"Time",
                        "Last"+Environment.NewLine+"Updated By", 
                        //11
                        "Last"+Environment.NewLine+"Updated Date",
                        "Last"+Environment.NewLine+"Updated Time"
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 4, 8, 11 });
            Sm.GrdFormatTime(Grd1, new int[] { 9, 12 });
            Sm.GrdColCheck(Grd1, new int[] { 3 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 7, 8, 9, 10, 11, 12 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 7, 8, 9, 10, 11, 12 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdAutoSize(Grd1);
        }


        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtPbsName.Text, new string[] { "A.PbsCode", "B.EmpName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        "Select A.PbsCode, B.EmpName, A.ActInd, A.PbsDt, A.PbsRegisterNo, A.PbsLocation, A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt " +
                        "From TblPublisher A " +
                        "Inner Join TblEmployee B On A.pbsEmpCode = B.EmpCode" +
                        Filter + " Order By B.EmpName",
                        new string[]
                        {
                            //0
                            "PbsCode", 
                                
                            //1-5
                            "EmpName", "ActInd", "PbsDt", "PbsRegisterNo", "PbsLocation",   

                            //6-9
                            "CreateBy","CreateDt","LastUpBy", "LastUpDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("B", Grd1, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("T", Grd1, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("T", Grd1, dr, c, Row, 12, 9);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #endregion

        #region Event
        private void ChkPbsName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Publisher");
        }

        private void TxtPbsName_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }
        #endregion
    }
}
