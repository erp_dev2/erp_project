﻿#region Update
/*
    07/07/2021 [IBL/PHT] new dialog apps
    13/10/2021 [DITA/PHT] Bug filter Budget category
    15/10/2021 [BRI/PHT] Process Available Budget Maintenance dan Process Available Budget saat memilih list of budget
    04/02/2022 [DITA/PHT] Source budget ambil dari Company Budget Plan berdasarkan parameter MultiVRAvailableBudgetMaintenanceSource
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmVoucherRequestMulti2Dlg2 : RunSystem.FrmBase4
    {
        #region Field

        private FrmVoucherRequestMulti2 mFrmParent;
        private string
            mBCCode = string.Empty,
            mYr = string.Empty,
            mMth = string.Empty,
            mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmVoucherRequestMulti2Dlg2(FrmVoucherRequestMulti2 FrmParent, string BCCode, string Yr, string Mth)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mBCCode = BCCode;
            mYr = Yr;
            mMth = Mth;
        }

        #endregion

        #region Method

        #region From Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                if (mFrmParent.mMultiVRAvailableBudgetMaintenanceSource == "2")
                {
                    label1.ForeColor = Color.Red;
                    ChkCCCode.Visible = false;
                }
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private protected string GetSQL(string Filter)
        {
            var SQL = new StringBuilder();
            if (mFrmParent.mMultiVRAvailableBudgetMaintenanceSource == "1")
            {
                SQL.AppendLine("Select D.CCCode, D.CCName, Sum(A1.Amt" + mMth + ") As Amount, F.SiteCode, ");
                SQL.AppendLine("F.SiteName, G.DeptCode, G.DeptName, C.BCCode, H.BCName ");
                SQL.AppendLine("From TblBudgetMaintenanceYearlyHdr A ");
                SQL.AppendLine("Inner Join TblBudgetMaintenanceYearlyDtl A1 On A.DocNo = A1.DocNo ");
                SQL.AppendLine("Inner Join TblBudgetRequestYearlyHdr B On A.BudgetRequestDocNo = B.DocNo ");
                SQL.AppendLine("    And A.Status = 'A' ");
                SQL.AppendLine("    And B.Status = 'A' ");
                SQL.AppendLine("    And B.CancelInd = 'N' ");
                SQL.AppendLine("Inner Join TblBudgetRequestYearlyDtl C On B.DocNo = C.DocNo And A1.BudgetRequestSeqNo = C.SeqNo ");
                SQL.AppendLine("Left Join TblCostCenter D On B.CCCode = D.CCCode ");
                SQL.AppendLine("Left Join TblProfitCenter E On D.ProfitCenterCode = E.ProfitCenterCode ");
                SQL.AppendLine("Left Join TblSite F On E.ProfitCenterCode = F.ProfitCenterCode ");
                SQL.AppendLine("Left Join TblDepartment G On D.DeptCode = G.DeptCode ");
                SQL.AppendLine("LEft Join TblBudgetCategory H On C.BCCode = H.BCCode ");
                SQL.AppendLine("Where B.Yr = @Yr ");
                if (mBCCode.Length > 0)
                    SQL.AppendLine("And C.BCCode = @BCCode ");
                SQL.AppendLine(Filter);
                SQL.AppendLine("Group By D.CCCode, C.BCCode; ");
            }
            else
            {
                SQL.AppendLine("Select T5.CCCode, T5.CCName, (T1.Amt - IfNull(T2.Amt, 0.00) - IfNull(T3.Amt, 0.00)) Amount, ");
                SQL.AppendLine("T7.SiteCode, T7.SiteName, T8.DeptCode, T8.DeptName, T4.BCCode, T4.BCName ");
                SQL.AppendLine("From (  ");
                SQL.AppendLine("Select C.BCCode, A.CCCode, Sum(B.Amt" + mMth + ") Amt ");
                SQL.AppendLine("    From TblCompanyBudgetPlanHdr A ");
                SQL.AppendLine("    Inner Join TblCompanyBudgetPlanDtl B On A.DocNo = B.DocNo ");
                SQL.AppendLine("        And A.CancelInd = 'N' ");
                SQL.AppendLine("        And A.CCCode Is Not Null ");
                SQL.AppendLine("        And A.CompletedInd = 'Y'  ");
                SQL.AppendLine("        And A.Yr = @Yr  ");
                SQL.AppendLine("    Inner Join TblBudgetCategory C On B.AcNo = C.AcNo  ");
                SQL.AppendLine("    Inner Join TblCostCenter D On C.CCCode = D.CCCode ");
                SQL.AppendLine("    Where 1=1 ");
                SQL.AppendLine(Filter.Replace("H.", "C."));
                SQL.AppendLine("    Group By C.BCCode, A.CCCode  ");
                SQL.AppendLine(") T1  ");
                SQL.AppendLine("Left Join  ");
                SQL.AppendLine("(  ");
                SQL.AppendLine("    Select A.CCCode, C.BCCode, Sum(B.Amt) Amt  ");
                SQL.AppendLine("    From TblCashAdvanceSettlementHdr A  ");
                SQL.AppendLine("    Inner Join TblCashAdvanceSettlementDtl B On A.DocNo = B.DocNo  ");
                SQL.AppendLine("        And A.CCCode Is Not Null  ");
                SQL.AppendLine("    And A.Status = 'A'  ");
                SQL.AppendLine("    And A.CancelInd = 'N'  ");
                SQL.AppendLine("    And Left(A.DocDt, 6) = Concat(@Yr, '" + mMth + "')  ");
                SQL.AppendLine("    And B.ItCode Is Not Null  ");
                SQL.AppendLine("    Inner Join TblBudgetCategory C On B.CCtCode = C.CCtCode And A.CCCode = C.CCCode  ");
                SQL.AppendLine("    Inner Join  ");
                SQL.AppendLine("    (  ");
                SQL.AppendLine("        Select Distinct X1.CCCode, X3.ItCode  ");
                SQL.AppendLine("        From TblCompanyBudgetPlanHdr X1  ");
                SQL.AppendLine("        Inner Join TblCompanyBudgetPlanDtl X2 On X1.DocNo = X2.DocNo  ");
                SQL.AppendLine("            And X1.CancelInd = 'N'  ");
                SQL.AppendLine("            And X1.CompletedInd = 'Y'  ");
                SQL.AppendLine("                And X1.CCCode Is Not Null  ");
                SQL.AppendLine("            And X1.Yr = @Yr  ");
                SQL.AppendLine("        Inner Join TblCompanyBudgetPlanDtl2 X3 On X1.DocNo = X3.DocNo And X2.AcNo = X3.AcNo  ");
                SQL.AppendLine("    ) D On A.CCCode = D.CCCode And B.ItCode = D.ItCode  ");
                SQL.AppendLine("    Inner Join TblCostCenter E On C.CCCode = E.CCCode ");
                SQL.AppendLine("    Where 1=1 ");
                SQL.AppendLine(Filter.Replace("H.", "C.").Replace("D.", "E."));
                SQL.AppendLine("    Group By A.CCCode, C.BCCode  ");
                SQL.AppendLine(") T2 On T1.CCCode = T2.CCCode AND T1.BCCode = T2.BCCode  ");
                SQL.AppendLine("Left Join  ");
                SQL.AppendLine("(  ");
                SQL.AppendLine("    Select A.CCCode, E.BCCode, Sum(  ");
                SQL.AppendLine("    Case D.AcType When 'D' Then (B.DAmt - B.CAmt) Else (B.CAmt - B.DAmt) End) As Amt  ");
                SQL.AppendLine("    From TblJournalHdr A  ");
                SQL.AppendLine("    Inner Join TblJournalDtl B On A.DocNo = B.DocNo  ");
                SQL.AppendLine("        And Left(A.DocDt, 6) = Concat(@Yr, '" + mMth + "')  ");
                SQL.AppendLine("        And A.MenuCode Not In (Select Menucode From TblMenu Where Param = 'CashAdvanceSettlement')  ");
                SQL.AppendLine("        And A.DocNo Not In (Select JournalDocno From TblVoucherHdr Where Doctype = '58')  ");
                SQL.AppendLine("        And A.CCCode Is Not Null  ");
                SQL.AppendLine("    Inner Join  ");
                SQL.AppendLine("    (  ");
                SQL.AppendLine("        Select Distinct X1.CCCode, X2.AcNo  ");
                SQL.AppendLine("        From TblCompanyBudgetPlanHdr X1  ");
                SQL.AppendLine("        Inner Join TblCompanyBudgetPlanDtl X2 On X1.DocNo = X2.DocNo  ");
                SQL.AppendLine("            And X1.CancelInd = 'N'  ");
                SQL.AppendLine("            And X1.CompletedInd = 'Y'  ");
                SQL.AppendLine("            And X1.CCCode Is Not Null  ");
                SQL.AppendLine("            And X1.Yr = @Yr  ");
                SQL.AppendLine("    ) C On A.CCCode = C.CCCode And B.AcNo = C.AcNo  ");
                SQL.AppendLine("    Inner Join TblCOA D On B.AcNo = D.AcNo And D.ActInd = 'Y'  ");
                SQL.AppendLine("    Inner Join TblBudgetCategory E On B.AcNo = E.AcNo  ");
                SQL.AppendLine("    Inner Join TblCostCenter F On E.CCCode = F.CCCode ");
                SQL.AppendLine("    Where 1=1 ");
                SQL.AppendLine(Filter.Replace("C.", "E.").Replace("H.", "E.").Replace("D.", "F."));
                SQL.AppendLine("    Group By A.CCCode, E.BCCode  ");
                SQL.AppendLine(") T3 On T1.CCCode = T3.CCCode And T1.BCCode = T3.BCCode  ");
                SQL.AppendLine("Inner Join TblBudgetCategory T4 On T1.BCCode = T4.BCCode ");
                SQL.AppendLine("Left Join TblCostCenter T5 On T1.CCCode = T5.CCCode ");
                SQL.AppendLine("Left Join TblProfitCenter T6 On T5.ProfitCenterCode = T6.ProfitCenterCode ");
                SQL.AppendLine("Left Join TblSite T7 On T6.ProfitCenterCode = T7.ProfitCenterCode ");
                SQL.AppendLine("Left Join TblDepartment T8 On T5.DeptCode = T8.DeptCode ");
                SQL.AppendLine("Where 1=1");
                SQL.AppendLine(Filter.Replace("C.", "T4.").Replace("H.", "T4.").Replace("D.", "T5."));
                SQL.AppendLine("Group By T4.BCCode, T5.CCCode ");
            }

            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 11;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "",
                        "Cost Center" + Environment.NewLine + "Code",
                        "Cost Center",
                        "Amount",
                        "Site Code",
                        
                        //6-10
                        "Site Name",                       
                        "Department Code",
                        "Department",
                        "Budget Category" + Environment.NewLine + "Code",
                        "Budget Category", 
                      },
                    new int[] 
                    {
                        //0
                        40,

                        //1-5
                        20, 100, 200, 200, 100,
                        
                        //6-10
                        150, 100, 200, 100, 200, 
                    }
                );
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 3, 4, 5, 6, 7, 8, 9, 10 }, true);
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdFormatDec(Grd1, new int[] { 4 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 2, 5, 7, 9 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 5, 7, 9 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            if ((mFrmParent.mMultiVRAvailableBudgetMaintenanceSource == "2" &&
                Sm.IsTxtEmpty(TxtCCCode, "Cost Center", false))) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@Yr", mYr);
                Sm.CmParam<String>(ref cm, "@BCCode", mBCCode);
                Sm.FilterStr(ref Filter, ref cm, TxtBCName.Text, new string[] { "C.BCCode", "H.BCName" });
                Sm.FilterStr(ref Filter, ref cm, TxtCCCode.Text, new string[] { "D.CCCode", "D.CCName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        GetSQL(Filter),
                        new string[] 
                        { 
                             //0
                            "CCCode",

                            //1-5
                            "CCName",
                            "Amount",
                            "SiteCode",
                            "SiteName",
                            "DeptCode",
                            
                            //6-8
                            "DeptName",
                            "BCCode",
                            "BCName"
                         },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = false;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsBudgetAlreadyChoosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        mFrmParent.Grd1.Cells[Row1, 0].Value = Row + 1;
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 5, Grd1, Row2, 4);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 6, Grd1, Row2, 5);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 7, Grd1, Row2, 6);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 8, Grd1, Row2, 7);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 9, Grd1, Row2, 8);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 10, Grd1, Row2, 9);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 11, Grd1, Row2, 10);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 14, Grd1, Row2, 3);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 16, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 17, Grd1, Row2, 4);
                        mFrmParent.Grd1.Cells[Row1, 12].Value = 0m;
                        mFrmParent.Grd1.Cells[Row1, 15].Value = 0m;

                        mFrmParent.Grd1.Rows.Add();
                        mFrmParent.ProcessAvailableBudget();
                        mFrmParent.ProcessAvailableBudgetMaintenance();
                    }
                }

                mFrmParent.ComputePlafondAmount();
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 account.");
        }

        private bool IsBudgetAlreadyChoosen(int Row)
        {
            for (int Index = 0; Index < mFrmParent.Grd1.Rows.Count - 1; Index++)
                if (Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd1, Index, 10), Sm.GetGrdStr(Grd1, Row, 2))) return true;

            return false;
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtBCName_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBCName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Budget category");
        }

        private void TxtCCCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkCCCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Cost Center");
        }

        #endregion

        #region Grid Control Event

        private void Grd1_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0)
                        Grd1.Cells[Row, 1].Value = !IsSelected;
            }
        }

        #endregion

        #endregion

    }
}
