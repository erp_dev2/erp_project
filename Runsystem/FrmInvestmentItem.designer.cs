﻿namespace RunSystem
{
    partial class FrmInvestmentItem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmInvestmentItem));
            this.TxtInvestmentName = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtInvestmentCode = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtForeignName = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.TpgGeneral = new System.Windows.Forms.TabPage();
            this.label23 = new System.Windows.Forms.Label();
            this.LueInventoryUomCode = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtPublisherCode = new DevExpress.XtraEditors.TextEdit();
            this.label51 = new System.Windows.Forms.Label();
            this.MeeSpecification = new DevExpress.XtraEditors.MemoExEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.LueInvestmentCtCode = new DevExpress.XtraEditors.LookUpEdit();
            this.ChkActInd = new DevExpress.XtraEditors.CheckEdit();
            this.lookUpEdit1 = new DevExpress.XtraEditors.LookUpEdit();
            this.checkEdit1 = new DevExpress.XtraEditors.CheckEdit();
            this.label27 = new System.Windows.Forms.Label();
            this.lookUpEdit2 = new DevExpress.XtraEditors.LookUpEdit();
            this.lookUpEdit3 = new DevExpress.XtraEditors.LookUpEdit();
            this.panel5 = new System.Windows.Forms.Panel();
            this.iGrid1 = new TenTec.Windows.iGridLib.iGrid();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.label28 = new System.Windows.Forms.Label();
            this.textEdit2 = new DevExpress.XtraEditors.TextEdit();
            this.label29 = new System.Windows.Forms.Label();
            this.textEdit3 = new DevExpress.XtraEditors.TextEdit();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.lookUpEdit4 = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtInvestmentCodeInternal = new DevExpress.XtraEditors.TextEdit();
            this.LblItCodeInternal = new System.Windows.Forms.Label();
            this.BtnItem = new DevExpress.XtraEditors.SimpleButton();
            this.TxtSource = new DevExpress.XtraEditors.TextEdit();
            this.label48 = new System.Windows.Forms.Label();
            this.SFD = new System.Windows.Forms.SaveFileDialog();
            this.OD = new System.Windows.Forms.OpenFileDialog();
            this.TxtInvestmentCodeOld = new DevExpress.XtraEditors.TextEdit();
            this.label14 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInvestmentName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInvestmentCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtForeignName.Properties)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.TpgGeneral.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueInventoryUomCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPublisherCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeSpecification.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueInvestmentCtCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit3.Properties)).BeginInit();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.iGrid1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInvestmentCodeInternal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSource.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInvestmentCodeOld.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(1184, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.panel1.Size = new System.Drawing.Size(100, 742);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnCancel.Size = new System.Drawing.Size(100, 31);
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnSave.Size = new System.Drawing.Size(100, 31);
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDelete.Size = new System.Drawing.Size(100, 31);
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnEdit.Size = new System.Drawing.Size(100, 31);
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnInsert.Size = new System.Drawing.Size(100, 31);
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnFind.Size = new System.Drawing.Size(100, 31);
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnPrint.Size = new System.Drawing.Size(100, 31);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.TxtInvestmentCodeOld);
            this.panel2.Controls.Add(this.label14);
            this.panel2.Controls.Add(this.BtnItem);
            this.panel2.Controls.Add(this.TxtInvestmentCodeInternal);
            this.panel2.Controls.Add(this.TxtSource);
            this.panel2.Controls.Add(this.LblItCodeInternal);
            this.panel2.Controls.Add(this.label48);
            this.panel2.Controls.Add(this.ChkActInd);
            this.panel2.Controls.Add(this.tabControl1);
            this.panel2.Controls.Add(this.TxtForeignName);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.TxtInvestmentName);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.TxtInvestmentCode);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.panel2.Size = new System.Drawing.Size(1184, 742);
            // 
            // TxtInvestmentName
            // 
            this.TxtInvestmentName.EnterMoveNextControl = true;
            this.TxtInvestmentName.Location = new System.Drawing.Point(207, 41);
            this.TxtInvestmentName.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.TxtInvestmentName.Name = "TxtInvestmentName";
            this.TxtInvestmentName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtInvestmentName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtInvestmentName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtInvestmentName.Properties.Appearance.Options.UseFont = true;
            this.TxtInvestmentName.Properties.MaxLength = 250;
            this.TxtInvestmentName.Size = new System.Drawing.Size(644, 28);
            this.TxtInvestmentName.TabIndex = 16;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(50, 46);
            this.label2.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(150, 22);
            this.label2.TabIndex = 15;
            this.label2.Text = "Investment Name";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtInvestmentCode
            // 
            this.TxtInvestmentCode.EnterMoveNextControl = true;
            this.TxtInvestmentCode.Location = new System.Drawing.Point(207, 8);
            this.TxtInvestmentCode.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.TxtInvestmentCode.Name = "TxtInvestmentCode";
            this.TxtInvestmentCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtInvestmentCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtInvestmentCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtInvestmentCode.Properties.Appearance.Options.UseFont = true;
            this.TxtInvestmentCode.Properties.MaxLength = 16;
            this.TxtInvestmentCode.Properties.ReadOnly = true;
            this.TxtInvestmentCode.Size = new System.Drawing.Size(239, 28);
            this.TxtInvestmentCode.TabIndex = 10;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(55, 13);
            this.label1.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(145, 22);
            this.label1.TabIndex = 9;
            this.label1.Text = "Investment Code";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtForeignName
            // 
            this.TxtForeignName.EnterMoveNextControl = true;
            this.TxtForeignName.Location = new System.Drawing.Point(207, 107);
            this.TxtForeignName.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.TxtForeignName.Name = "TxtForeignName";
            this.TxtForeignName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtForeignName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtForeignName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtForeignName.Properties.Appearance.Options.UseFont = true;
            this.TxtForeignName.Properties.MaxLength = 250;
            this.TxtForeignName.Size = new System.Drawing.Size(644, 28);
            this.TxtForeignName.TabIndex = 20;
            this.TxtForeignName.Validated += new System.EventHandler(this.TxtForeignName_Validated);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(81, 112);
            this.label3.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(119, 22);
            this.label3.TabIndex = 19;
            this.label3.Text = "Foreign Name";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabControl1
            // 
            this.tabControl1.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.tabControl1.Controls.Add(this.TpgGeneral);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tabControl1.Location = new System.Drawing.Point(0, 218);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabControl1.Multiline = true;
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.ShowToolTips = true;
            this.tabControl1.Size = new System.Drawing.Size(1184, 524);
            this.tabControl1.TabIndex = 30;
            // 
            // TpgGeneral
            // 
            this.TpgGeneral.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.TpgGeneral.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.TpgGeneral.Controls.Add(this.label23);
            this.TpgGeneral.Controls.Add(this.LueInventoryUomCode);
            this.TpgGeneral.Controls.Add(this.TxtPublisherCode);
            this.TpgGeneral.Controls.Add(this.label51);
            this.TpgGeneral.Controls.Add(this.MeeSpecification);
            this.TpgGeneral.Controls.Add(this.label9);
            this.TpgGeneral.Controls.Add(this.MeeRemark);
            this.TpgGeneral.Controls.Add(this.label8);
            this.TpgGeneral.Controls.Add(this.label4);
            this.TpgGeneral.Controls.Add(this.LueInvestmentCtCode);
            this.TpgGeneral.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TpgGeneral.Location = new System.Drawing.Point(4, 34);
            this.TpgGeneral.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.TpgGeneral.Name = "TpgGeneral";
            this.TpgGeneral.Size = new System.Drawing.Size(1176, 486);
            this.TpgGeneral.TabIndex = 0;
            this.TpgGeneral.Text = "General";
            this.TpgGeneral.UseVisualStyleBackColor = true;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Red;
            this.label23.Location = new System.Drawing.Point(101, 137);
            this.label23.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(46, 22);
            this.label23.TabIndex = 47;
            this.label23.Text = "UoM";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueInventoryUomCode
            // 
            this.LueInventoryUomCode.EnterMoveNextControl = true;
            this.LueInventoryUomCode.Location = new System.Drawing.Point(161, 132);
            this.LueInventoryUomCode.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.LueInventoryUomCode.Name = "LueInventoryUomCode";
            this.LueInventoryUomCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInventoryUomCode.Properties.Appearance.Options.UseFont = true;
            this.LueInventoryUomCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInventoryUomCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueInventoryUomCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInventoryUomCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueInventoryUomCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInventoryUomCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueInventoryUomCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInventoryUomCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueInventoryUomCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueInventoryUomCode.Properties.DropDownRows = 30;
            this.LueInventoryUomCode.Properties.NullText = "[Empty]";
            this.LueInventoryUomCode.Properties.PopupWidth = 200;
            this.LueInventoryUomCode.Size = new System.Drawing.Size(456, 28);
            this.LueInventoryUomCode.TabIndex = 48;
            this.LueInventoryUomCode.ToolTip = "F4 : Show/hide list";
            this.LueInventoryUomCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueInventoryUomCode.EditValueChanged += new System.EventHandler(this.LueInventoryUomCode_EditValueChanged_1);
            // 
            // TxtPublisherCode
            // 
            this.TxtPublisherCode.EnterMoveNextControl = true;
            this.TxtPublisherCode.Location = new System.Drawing.Point(161, 99);
            this.TxtPublisherCode.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.TxtPublisherCode.Name = "TxtPublisherCode";
            this.TxtPublisherCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPublisherCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPublisherCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPublisherCode.Properties.Appearance.Options.UseFont = true;
            this.TxtPublisherCode.Properties.MaxLength = 250;
            this.TxtPublisherCode.Properties.ReadOnly = true;
            this.TxtPublisherCode.Size = new System.Drawing.Size(456, 28);
            this.TxtPublisherCode.TabIndex = 42;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.ForeColor = System.Drawing.Color.Black;
            this.label51.Location = new System.Drawing.Point(20, 104);
            this.label51.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(127, 22);
            this.label51.TabIndex = 41;
            this.label51.Text = "Publisher Code";
            this.label51.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeSpecification
            // 
            this.MeeSpecification.EnterMoveNextControl = true;
            this.MeeSpecification.Location = new System.Drawing.Point(161, 66);
            this.MeeSpecification.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.MeeSpecification.Name = "MeeSpecification";
            this.MeeSpecification.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSpecification.Properties.Appearance.Options.UseFont = true;
            this.MeeSpecification.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSpecification.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeSpecification.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSpecification.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeSpecification.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSpecification.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeSpecification.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSpecification.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeSpecification.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeSpecification.Properties.MaxLength = 400;
            this.MeeSpecification.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeSpecification.Properties.ShowIcon = false;
            this.MeeSpecification.Size = new System.Drawing.Size(846, 28);
            this.MeeSpecification.TabIndex = 40;
            this.MeeSpecification.ToolTip = "F4 : Show/hide text";
            this.MeeSpecification.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeSpecification.ToolTipTitle = "Run System";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(39, 71);
            this.label9.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(108, 22);
            this.label9.TabIndex = 39;
            this.label9.Text = "Specification";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(161, 166);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 1000;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(846, 28);
            this.MeeRemark.TabIndex = 44;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(78, 171);
            this.label8.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(69, 22);
            this.label8.TabIndex = 43;
            this.label8.Text = "Remark";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(67, 38);
            this.label4.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(80, 22);
            this.label4.TabIndex = 31;
            this.label4.Text = "Category";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueInvestmentCtCode
            // 
            this.LueInvestmentCtCode.EnterMoveNextControl = true;
            this.LueInvestmentCtCode.Location = new System.Drawing.Point(161, 33);
            this.LueInvestmentCtCode.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.LueInvestmentCtCode.Name = "LueInvestmentCtCode";
            this.LueInvestmentCtCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInvestmentCtCode.Properties.Appearance.Options.UseFont = true;
            this.LueInvestmentCtCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInvestmentCtCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueInvestmentCtCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInvestmentCtCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueInvestmentCtCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInvestmentCtCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueInvestmentCtCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInvestmentCtCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueInvestmentCtCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueInvestmentCtCode.Properties.DropDownRows = 30;
            this.LueInvestmentCtCode.Properties.NullText = "[Empty]";
            this.LueInvestmentCtCode.Properties.PopupWidth = 350;
            this.LueInvestmentCtCode.Size = new System.Drawing.Size(456, 28);
            this.LueInvestmentCtCode.TabIndex = 32;
            this.LueInvestmentCtCode.ToolTip = "F4 : Show/hide list";
            this.LueInvestmentCtCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // ChkActInd
            // 
            this.ChkActInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkActInd.Location = new System.Drawing.Point(451, 6);
            this.ChkActInd.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
            this.ChkActInd.Name = "ChkActInd";
            this.ChkActInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkActInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkActInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkActInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkActInd.Properties.Appearance.Options.UseFont = true;
            this.ChkActInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkActInd.Properties.Caption = "Active";
            this.ChkActInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkActInd.Size = new System.Drawing.Size(109, 27);
            this.ChkActInd.TabIndex = 11;
            // 
            // lookUpEdit1
            // 
            this.lookUpEdit1.EnterMoveNextControl = true;
            this.lookUpEdit1.Location = new System.Drawing.Point(206, 80);
            this.lookUpEdit1.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit1.Name = "lookUpEdit1";
            this.lookUpEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit1.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit1.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit1.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit1.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit1.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit1.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit1.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit1.Properties.DropDownRows = 12;
            this.lookUpEdit1.Properties.NullText = "[Empty]";
            this.lookUpEdit1.Properties.PopupWidth = 500;
            this.lookUpEdit1.Size = new System.Drawing.Size(245, 20);
            this.lookUpEdit1.TabIndex = 34;
            this.lookUpEdit1.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit1.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // checkEdit1
            // 
            this.checkEdit1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.checkEdit1.Location = new System.Drawing.Point(20, 79);
            this.checkEdit1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.checkEdit1.Name = "checkEdit1";
            this.checkEdit1.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.checkEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkEdit1.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.checkEdit1.Properties.Appearance.Options.UseBackColor = true;
            this.checkEdit1.Properties.Appearance.Options.UseFont = true;
            this.checkEdit1.Properties.Appearance.Options.UseForeColor = true;
            this.checkEdit1.Properties.Caption = "Material Request is issued by";
            this.checkEdit1.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.checkEdit1.Properties.ReadOnly = true;
            this.checkEdit1.Size = new System.Drawing.Size(186, 22);
            this.checkEdit1.TabIndex = 33;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Red;
            this.label27.Location = new System.Drawing.Point(282, 8);
            this.label27.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(64, 14);
            this.label27.TabIndex = 34;
            this.label27.Text = "( Primary )";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit2
            // 
            this.lookUpEdit2.EnterMoveNextControl = true;
            this.lookUpEdit2.Location = new System.Drawing.Point(113, 51);
            this.lookUpEdit2.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit2.Name = "lookUpEdit2";
            this.lookUpEdit2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit2.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit2.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit2.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit2.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit2.Properties.DropDownRows = 12;
            this.lookUpEdit2.Properties.NullText = "[Empty]";
            this.lookUpEdit2.Properties.PopupWidth = 500;
            this.lookUpEdit2.Size = new System.Drawing.Size(166, 20);
            this.lookUpEdit2.TabIndex = 26;
            this.lookUpEdit2.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // lookUpEdit3
            // 
            this.lookUpEdit3.EnterMoveNextControl = true;
            this.lookUpEdit3.Location = new System.Drawing.Point(113, 28);
            this.lookUpEdit3.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit3.Name = "lookUpEdit3";
            this.lookUpEdit3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit3.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit3.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit3.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit3.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit3.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit3.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit3.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit3.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit3.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit3.Properties.DropDownRows = 12;
            this.lookUpEdit3.Properties.NullText = "[Empty]";
            this.lookUpEdit3.Properties.PopupWidth = 500;
            this.lookUpEdit3.Size = new System.Drawing.Size(166, 20);
            this.lookUpEdit3.TabIndex = 25;
            this.lookUpEdit3.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.SkyBlue;
            this.panel5.Controls.Add(this.iGrid1);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel5.Location = new System.Drawing.Point(0, 123);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(760, 197);
            this.panel5.TabIndex = 33;
            // 
            // iGrid1
            // 
            this.iGrid1.BackColorEvenRows = System.Drawing.Color.WhiteSmoke;
            this.iGrid1.BackColorOddRows = System.Drawing.Color.White;
            this.iGrid1.DefaultAutoGroupRow.Height = 21;
            this.iGrid1.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.iGrid1.DefaultRow.Height = 20;
            this.iGrid1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.iGrid1.FrozenArea.ColCount = 3;
            this.iGrid1.FrozenArea.SortFrozenRows = true;
            this.iGrid1.GroupBox.BackColor = System.Drawing.Color.CornflowerBlue;
            this.iGrid1.GroupBox.HintBackColor = System.Drawing.Color.CornflowerBlue;
            this.iGrid1.GroupBox.HintForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.iGrid1.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.iGrid1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.iGrid1.Header.Height = 22;
            this.iGrid1.Header.UseXPStyles = false;
            this.iGrid1.Location = new System.Drawing.Point(0, 0);
            this.iGrid1.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.iGrid1.Name = "iGrid1";
            this.iGrid1.ProcessTab = false;
            this.iGrid1.ReadOnly = true;
            this.iGrid1.RowTextStartColNear = 3;
            this.iGrid1.ScrollBarSettings.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.iGrid1.SearchAsType.Mode = TenTec.Windows.iGridLib.iGSearchAsTypeMode.Seek;
            this.iGrid1.SearchAsType.SearchCol = null;
            this.iGrid1.SingleClickEdit = true;
            this.iGrid1.Size = new System.Drawing.Size(760, 101);
            this.iGrid1.TabIndex = 35;
            this.iGrid1.TreeCol = null;
            this.iGrid1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // textEdit1
            // 
            this.textEdit1.EnterMoveNextControl = true;
            this.textEdit1.Location = new System.Drawing.Point(451, 51);
            this.textEdit1.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit1.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit1.Properties.Appearance.Options.UseFont = true;
            this.textEdit1.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEdit1.Size = new System.Drawing.Size(166, 20);
            this.textEdit1.TabIndex = 32;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Black;
            this.label28.Location = new System.Drawing.Point(362, 54);
            this.label28.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(85, 14);
            this.label28.TabIndex = 31;
            this.label28.Text = "Reorder Stock";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit2
            // 
            this.textEdit2.EnterMoveNextControl = true;
            this.textEdit2.Location = new System.Drawing.Point(451, 28);
            this.textEdit2.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit2.Name = "textEdit2";
            this.textEdit2.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit2.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit2.Properties.Appearance.Options.UseFont = true;
            this.textEdit2.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEdit2.Size = new System.Drawing.Size(166, 20);
            this.textEdit2.TabIndex = 30;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Black;
            this.label29.Location = new System.Drawing.Point(355, 31);
            this.label29.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(92, 14);
            this.label29.TabIndex = 29;
            this.label29.Text = "Maximum Stock";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit3
            // 
            this.textEdit3.EnterMoveNextControl = true;
            this.textEdit3.Location = new System.Drawing.Point(451, 5);
            this.textEdit3.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit3.Name = "textEdit3";
            this.textEdit3.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit3.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit3.Properties.Appearance.Options.UseFont = true;
            this.textEdit3.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit3.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEdit3.Size = new System.Drawing.Size(166, 20);
            this.textEdit3.TabIndex = 28;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.Black;
            this.label30.Location = new System.Drawing.Point(358, 8);
            this.label30.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(89, 14);
            this.label30.TabIndex = 27;
            this.label30.Text = "Minimum Stock";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.Red;
            this.label31.Location = new System.Drawing.Point(20, 8);
            this.label31.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(89, 14);
            this.label31.TabIndex = 23;
            this.label31.Text = "Inventory UoM";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit4
            // 
            this.lookUpEdit4.EnterMoveNextControl = true;
            this.lookUpEdit4.Location = new System.Drawing.Point(113, 5);
            this.lookUpEdit4.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit4.Name = "lookUpEdit4";
            this.lookUpEdit4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit4.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit4.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit4.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit4.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit4.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit4.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit4.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit4.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit4.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit4.Properties.DropDownRows = 12;
            this.lookUpEdit4.Properties.NullText = "[Empty]";
            this.lookUpEdit4.Properties.PopupWidth = 500;
            this.lookUpEdit4.Size = new System.Drawing.Size(166, 20);
            this.lookUpEdit4.TabIndex = 24;
            this.lookUpEdit4.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit4.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // TxtInvestmentCodeInternal
            // 
            this.TxtInvestmentCodeInternal.EnterMoveNextControl = true;
            this.TxtInvestmentCodeInternal.Location = new System.Drawing.Point(207, 74);
            this.TxtInvestmentCodeInternal.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.TxtInvestmentCodeInternal.Name = "TxtInvestmentCodeInternal";
            this.TxtInvestmentCodeInternal.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtInvestmentCodeInternal.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtInvestmentCodeInternal.Properties.Appearance.Options.UseBackColor = true;
            this.TxtInvestmentCodeInternal.Properties.Appearance.Options.UseFont = true;
            this.TxtInvestmentCodeInternal.Properties.MaxLength = 30;
            this.TxtInvestmentCodeInternal.Size = new System.Drawing.Size(644, 28);
            this.TxtInvestmentCodeInternal.TabIndex = 18;
            this.TxtInvestmentCodeInternal.Validated += new System.EventHandler(this.TxtItCodeInternal_Validated);
            // 
            // LblItCodeInternal
            // 
            this.LblItCodeInternal.AutoSize = true;
            this.LblItCodeInternal.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblItCodeInternal.ForeColor = System.Drawing.Color.Black;
            this.LblItCodeInternal.Location = new System.Drawing.Point(9, 79);
            this.LblItCodeInternal.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.LblItCodeInternal.Name = "LblItCodeInternal";
            this.LblItCodeInternal.Size = new System.Drawing.Size(191, 22);
            this.LblItCodeInternal.TabIndex = 17;
            this.LblItCodeInternal.Text = "Investment Local Code";
            this.LblItCodeInternal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnItem
            // 
            this.BtnItem.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnItem.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnItem.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnItem.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnItem.Appearance.Options.UseBackColor = true;
            this.BtnItem.Appearance.Options.UseFont = true;
            this.BtnItem.Appearance.Options.UseForeColor = true;
            this.BtnItem.Appearance.Options.UseTextOptions = true;
            this.BtnItem.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnItem.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnItem.Image = ((System.Drawing.Image)(resources.GetObject("BtnItem.Image")));
            this.BtnItem.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnItem.Location = new System.Drawing.Point(856, 6);
            this.BtnItem.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.BtnItem.Name = "BtnItem";
            this.BtnItem.Size = new System.Drawing.Size(34, 33);
            this.BtnItem.TabIndex = 14;
            this.BtnItem.ToolTip = "Find Item";
            this.BtnItem.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnItem.ToolTipTitle = "Run System";
            this.BtnItem.Click += new System.EventHandler(this.BtnItem_Click);
            // 
            // TxtSource
            // 
            this.TxtSource.EnterMoveNextControl = true;
            this.TxtSource.Location = new System.Drawing.Point(644, 8);
            this.TxtSource.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.TxtSource.Name = "TxtSource";
            this.TxtSource.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtSource.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSource.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSource.Properties.Appearance.Options.UseFont = true;
            this.TxtSource.Properties.MaxLength = 80;
            this.TxtSource.Properties.ReadOnly = true;
            this.TxtSource.Size = new System.Drawing.Size(207, 28);
            this.TxtSource.TabIndex = 13;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.ForeColor = System.Drawing.Color.Black;
            this.label48.Location = new System.Drawing.Point(577, 11);
            this.label48.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(63, 22);
            this.label48.TabIndex = 12;
            this.label48.Text = "Source";
            this.label48.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // OD
            // 
            this.OD.FileName = "openFileDialog1";
            // 
            // TxtInvestmentCodeOld
            // 
            this.TxtInvestmentCodeOld.EnterMoveNextControl = true;
            this.TxtInvestmentCodeOld.Location = new System.Drawing.Point(207, 140);
            this.TxtInvestmentCodeOld.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.TxtInvestmentCodeOld.Name = "TxtInvestmentCodeOld";
            this.TxtInvestmentCodeOld.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtInvestmentCodeOld.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtInvestmentCodeOld.Properties.Appearance.Options.UseBackColor = true;
            this.TxtInvestmentCodeOld.Properties.Appearance.Options.UseFont = true;
            this.TxtInvestmentCodeOld.Properties.MaxLength = 30;
            this.TxtInvestmentCodeOld.Size = new System.Drawing.Size(644, 28);
            this.TxtInvestmentCodeOld.TabIndex = 22;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(117, 145);
            this.label14.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(83, 22);
            this.label14.TabIndex = 21;
            this.label14.Text = "Old Code";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // FrmInvestmentItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 22F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1284, 742);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "FrmInvestmentItem";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInvestmentName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInvestmentCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtForeignName.Properties)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.TpgGeneral.ResumeLayout(false);
            this.TpgGeneral.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueInventoryUomCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPublisherCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeSpecification.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueInvestmentCtCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit3.Properties)).EndInit();
            this.panel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.iGrid1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInvestmentCodeInternal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSource.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInvestmentCodeOld.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtInvestmentCode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage TpgGeneral;
        private DevExpress.XtraEditors.CheckEdit ChkActInd;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.LookUpEdit LueInvestmentCtCode;
        private DevExpress.XtraEditors.MemoExEdit MeeSpecification;
        private System.Windows.Forms.Label label9;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private System.Windows.Forms.Label label8;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit1;
        private DevExpress.XtraEditors.CheckEdit checkEdit1;
        private System.Windows.Forms.Label label27;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit2;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit3;
        protected System.Windows.Forms.Panel panel5;
        protected TenTec.Windows.iGridLib.iGrid iGrid1;
        internal DevExpress.XtraEditors.TextEdit textEdit1;
        private System.Windows.Forms.Label label28;
        internal DevExpress.XtraEditors.TextEdit textEdit2;
        private System.Windows.Forms.Label label29;
        internal DevExpress.XtraEditors.TextEdit textEdit3;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit4;
        private System.Windows.Forms.Label LblItCodeInternal;
        public DevExpress.XtraEditors.SimpleButton BtnItem;
        internal DevExpress.XtraEditors.TextEdit TxtSource;
        private System.Windows.Forms.Label label48;
        internal DevExpress.XtraEditors.TextEdit TxtForeignName;
        internal DevExpress.XtraEditors.TextEdit TxtInvestmentName;
        internal DevExpress.XtraEditors.TextEdit TxtInvestmentCodeInternal;
        private DevExpress.XtraEditors.TextEdit TxtPublisherCode;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.SaveFileDialog SFD;
        private System.Windows.Forms.OpenFileDialog OD;
        internal DevExpress.XtraEditors.TextEdit TxtInvestmentCodeOld;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label23;
        private DevExpress.XtraEditors.LookUpEdit LueInventoryUomCode;
    }
}