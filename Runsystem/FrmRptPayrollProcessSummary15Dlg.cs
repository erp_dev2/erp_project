﻿#region Update
/*
    11/10/2019 [TKG] MMM
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using System.Drawing;
using System.Text;

using TenTec.Windows.iGridLib;
using MySql.Data.MySqlClient;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptPayrollProcessSummary15Dlg : RunSystem.FrmBase9
    {
        #region Field

        private FrmRptPayrollProcessSummary15 mFrmParent;
        private string mSQL = string.Empty, mPayrunCode = string.Empty, mEmpCode = string.Empty;

        #endregion

        #region Constructor

        public FrmRptPayrollProcessSummary15Dlg(
            FrmRptPayrollProcessSummary15 FrmParent, 
            String PayrunCode, 
            string EmpCode
            )
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mPayrunCode = PayrunCode;
            mEmpCode = EmpCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                base.FrmLoad(sender, e);
                SetGrd();
                ShowData();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 71;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.RowHeader.Visible = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "Date", 
                        "Grade"+Environment.NewLine+"Level", 
                        "Department", 
                        "Paid Date", 
                        "Period", 

                        //6-10
                        "Group", 
                        "Site", 
                        "Process", 
                        "Holiday"+Environment.NewLine+"Earning",
                        "Status", 

                         //11-15
                        "Working"+Environment.NewLine+"Day", 
                        "Schedule", 
                        "ExtraFooding",
                        "Holiday"+Environment.NewLine+"Index",
                        "Public"+Environment.NewLine+"Holiday", 

                        //16-20
                        "Actual"+Environment.NewLine+"Time (In)", 
                        "Actual"+Environment.NewLine+"Time (Out)", 
                        "Work"+Environment.NewLine+"In", 
                        "Work"+Environment.NewLine+"Out",
                        "Break"+Environment.NewLine+"In", 

                        //21-25
                        "Break"+Environment.NewLine+"Out",
                        "Routine"+Environment.NewLine+"OT In",
                        "Routine"+Environment.NewLine+"OT Out",
                        "Shift"+Environment.NewLine+"1/2",
                        "Late",

                        //26-30
                        "Working"+Environment.NewLine+"Date (In)",
                        "Working"+Environment.NewLine+"Date (Out)",
                        "Working"+Environment.NewLine+"Duration",
                        "Monthly"+Environment.NewLine+"Salary",
                        "Daily"+Environment.NewLine+"Salary",

                        //31-35
                        "Type",  
                        "Premi"+Environment.NewLine+"Hadir",
                        "Leave",
                        "Working"+Environment.NewLine+"Time (In)",
                        "Working"+Environment.NewLine+"Time (Out)",
                        
                        //36-40
                        "Duration",
                        "Paid Leave"+Environment.NewLine+"Day",
                        "Paid Leave"+Environment.NewLine+"Hour",
                        "Paid Leave"+Environment.NewLine+"Amount",
                        "Processed Paid"+Environment.NewLine+"Leave Amount",

                        //41-45
                        "Unpaid Leave"+Environment.NewLine+"Day",
                        "Unpaid Leave"+Environment.NewLine+"Hour",
                        "UnPaid Leave"+Environment.NewLine+"Amount",
                        "Processed Unpaid"+Environment.NewLine+"Leave Amount",
                        "Production"+Environment.NewLine+"Wages",

                        //46-50
                        "Actual"+Environment.NewLine+"Salary", 
                        "Production"+Environment.NewLine+"Incentive",
                        "Min.Wages"+Environment.NewLine+"Incentive",
                        "Production"+Environment.NewLine+"Deduction",
                        "Performance"+Environment.NewLine+"Incentive",

                        //51-55
                        "OT 1"+Environment.NewLine+"(Hour)",
                        "OT 1"+Environment.NewLine+"(Amount)",
                        "OT 2"+Environment.NewLine+"(Hour)",
                        "OT 2"+Environment.NewLine+"(Amount)",
                        "OT Holiday"+Environment.NewLine+"(Hour)",

                        //56-60
                        "OT Holiday"+Environment.NewLine+"(Amount)",
                        "OT To"+Environment.NewLine+"Leave",
                        "Holiday",
                        "OT"+Environment.NewLine+"Allowance",
                        "",
                        
                        //61-65
                        "Meal",
                        "Transport",
                        "Field"+Environment.NewLine+"Assignment",
                        "Leave"+Environment.NewLine+"Deduction", 
                        "Actual"+Environment.NewLine+"Date (In)",
                        
                        //66-70
                        "Actual"+Environment.NewLine+"Date (Out)",
                        "Type",
                        "Paid",
                        "Start"+Environment.NewLine+"Time",
                        "End"+Environment.NewLine+"Time"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        100, 130, 150, 150, 150, 
                        
                        //6-10
                        150, 150, 90, 150, 130, 
                        
                        //11-15
                        100, 150, 100, 100, 100, 

                        //16-20
                        100, 100, 100, 100, 100,

                        //21-25
                        100, 100, 100, 100, 100,

                        //26-30
                        100, 100, 100, 100, 100,

                        //31-35
                        100, 130, 130, 100, 100,

                        //36-40
                        100, 150, 130, 100, 140, 

                        //41-45
                        100, 100, 100, 120, 100,

                        //46-50
                        130, 100, 100, 100, 140, 

                        //51-55
                        100, 100, 100, 100, 100,

                        //56-60
                        100, 100, 100, 100, 20,

                        //61-65
                        100, 100, 100, 100, 100,

                        //66-68
                        100, 100, 60, 100, 100
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 60 });
            Sm.GrdFormatDec(Grd1, new int[] { 
                9, 11, 13, 14, 28, 29, 30, 
                36, 37, 38, 39, 40, 
                41, 42, 43, 44, 45, 46, 47, 48, 49, 50,
                51, 53, 55, 52, 54, 56, 59, 
                61, 62, 
                63, 64
            }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 
                1, 4, 
                26, 27, 
                65, 66
            });
            Sm.GrdFormatTime(Grd1, new int[]{ 
                16, 17, 18, 19, 20, 
                21, 22, 23, 
                34, 35, 
                69, 70
            });
            Sm.GrdColCheck(Grd1, new int[] { 
                8, 
                15, 
                24, 25, 
                32, 
                57, 58, 
                68 
            });
            Sm.GrdColInvisible(Grd1, new int[] { 
                2, 3, 4, 5, 6, 7, 
                11, 14, 15, 18, 19, 20, 
                21, 22, 23, 24, 25, 26, 27, 29, 30, 
                31, 34, 35, 36, 37, 38, 39, 
                41, 42, 43, 
                52, 54, 56, 57, 58, 
                61, 62, 63, 64, 65, 66, 67, 68, 69, 70 
            }, false);

            Sm.GrdColReadOnly(Grd1, new int[] { 
                0, 
                1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
                11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
                21, 22, 23, 24, 25, 26, 27, 28, 29, 30,
                31, 32, 33, 34, 35, 36, 37, 38, 39, 40,
                41, 42, 43, 44, 45, 46, 47, 48, 49, 50,
                51, 52, 53, 54, 55, 56, 57, 58, 59, 
                61, 62, 63, 64, 65, 66, 67, 68, 69, 70 
            }, false);

            Sm.SetGrdProperty(Grd1, false);
        }

        private string GetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.*, ");
            SQL.AppendLine("A.EmpSalary As BSalary1, A.EmpSalary2 As BSalary2, ");
            SQL.AppendLine("B.WSName, C.LeaveName, D.DeptName, ");
            SQL.AppendLine("E.OptDesc As EmploymentStatusDesc, ");
            SQL.AppendLine("F.OptDesc As SystemTypeDesc, ");
            SQL.AppendLine("G.GrdLvlName, ");
            SQL.AppendLine("H.OptDesc As PayrunPeriodDesc, ");
            SQL.AppendLine("I.PGName, J.SiteName, K.OptDesc As LeaveTypeDesc ");
            SQL.AppendLine("From TblPayrollProcess2 A ");
            SQL.AppendLine("Left Join TblWorkSchedule B on A.WSCode = B.WSCode ");
            SQL.AppendLine("Left Join TblLeave C on A.LeaveCode = C.LeaveCode ");
            SQL.AppendLine("Left Join TblDepartment D On A.DeptCode=D.DeptCode ");
            SQL.AppendLine("Left Join TblOption E On A.EmploymentStatus=E.OptCode And E.OptCat='EmploymentStatus' ");
            SQL.AppendLine("Left Join TblOption F On A.SystemType=F.OptCode And F.OptCat='EmpSystemType' ");
            SQL.AppendLine("Left Join TblGradeLevelHdr G On A.GrdLvlCode=G.GrdLvlCode ");
            SQL.AppendLine("Left Join TblOption H On A.PayrunPeriod=H.OptCode And H.OptCat='PayrunPeriod' ");
            SQL.AppendLine("Left Join TblPayrollGrpHdr I On A.PGCode=I.PGCode ");
            SQL.AppendLine("Left Join TblSite J On A.SiteCode=J.SiteCode ");
            SQL.AppendLine("Left Join TblOption K On A.LeaveType=K.OptCode And K.OptCat='LeaveType' ");
            SQL.AppendLine("Where A.EmpCode=@EmpCode ");
            SQL.AppendLine("And A.PayrunCode=@PayrunCode ");
            SQL.AppendLine("Order By A.Dt; ");

            return SQL.ToString();
        }

        private void ShowData()
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@EmpCode", mEmpCode);
            Sm.CmParam<String>(ref cm, "@PayrunCode", mPayrunCode);

            Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    GetSQL(),
                    new string[]
                    {
                        //0
                        "Dt",

                        //1-5
                        "GrdLvlName",
                        "DeptName",
                        "LatestPaidDt",
                        "PayrunPeriodDesc",
                        "PGName",

                        //6-10
                        "SiteName",
                        "ProcessInd",
                        "HolidayEarning",
                        "EmploymentStatusDesc",
                        "WorkingDay",

                        //11-15
                        "WSName",
                        "ExtraFooding",
                        "HolidayIndex",
                        "HolInd",
                        "ActualIn",

                        //16-20
                        "ActualOut",
                        "WSIn1",
                        "WSOut1",
                        "WSIn2",
                        "WSOut2",

                        //21-25
                        "WSIn3",
                        "WSOut3",
                        "OneDayInd",
                        "LateInd",
                        "WorkingIn",
                        
                        //26-30
                        "WorkingOut",
                        "WorkingDuration",
                        "BSalary1",
                        "BSalary2",
                        "SystemTypeDesc",

                        //31-35
                        "PresenceRewardInd",
                        "LeaveName",
                        "LeaveDuration",
                        "PLDay",
                        "PLHr",

                        //36-40
                        "PLAmt",
                        "ProcessPLAmt",
                        "UPLDay",
                        "UPLHr",
                        "UPLAmt",

                        //41-45
                        "ProcessUPLAmt",
                        "ProductionWages",
                        "Salary",
                        "IncProduction",
                        "IncMinWages",

                        //46-50
                        "DedProduction",
                        "IncPerformance",
                        "OT1Hr",
                        "OT1Amt",
                        "OT2Hr",

                        //51-55
                        "OT2Amt",
                        "OTHolidayHr",
                        "OTHolidayAmt",
                        "OTToLeaveInd",
                        "WSHolidayInd",

                        //56-60
                        "ADOT",
                        "Meal",
                        "Transport",
                        "FieldAssignment",
                        "DedProdLeave",
                        
                        //61-64
                        "LeaveTypeDesc",
                        "PaidLeaveInd",
                        "LeaveStartTm",
                        "LeaveEndTm"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 12);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 13);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 15, 14);
                        Sm.SetGrdValue("T", Grd, dr, c, Row, 16, 15);
                        Sm.SetGrdValue("T", Grd, dr, c, Row, 17, 16);
                        Sm.SetGrdValue("T2", Grd, dr, c, Row, 18, 17);
                        Sm.SetGrdValue("T2", Grd, dr, c, Row, 19, 18);
                        Sm.SetGrdValue("T2", Grd, dr, c, Row, 20, 19);
                        Sm.SetGrdValue("T2", Grd, dr, c, Row, 21, 20);
                        Sm.SetGrdValue("T2", Grd, dr, c, Row, 22, 21);
                        Sm.SetGrdValue("T2", Grd, dr, c, Row, 23, 22);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 24, 23);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 25, 24);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 26, 25);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 27, 26);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 28, 27);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 29, 28);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 30, 29);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 31, 30);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 32, 31);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 33, 32);
                        Sm.SetGrdValue("T", Grd, dr, c, Row, 34, 25);
                        Sm.SetGrdValue("T", Grd, dr, c, Row, 35, 26);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 36, 33);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 37, 34);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 38, 35);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 39, 36);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 40, 37);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 41, 38);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 42, 39);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 43, 40);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 44, 41);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 45, 42);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 46, 43);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 47, 44);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 48, 45);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 49, 46);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 50, 47);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 51, 48);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 52, 49);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 53, 50);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 54, 51);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 55, 52);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 56, 53);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 57, 54);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 58, 55);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 59, 56);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 61, 57);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 62, 58);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 63, 59);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 64, 60);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 65, 15);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 66, 16);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 67, 61);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 68, 62);
                        Sm.SetGrdValue("T2", Grd, dr, c, Row, 69, 63);
                        Sm.SetGrdValue("T2", Grd, dr, c, Row, 70, 64);

                    }, false, false, false, false
                );
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 
                 9, 11, 13, 14, 28, 29, 30, 
                36, 37, 38, 39, 40, 
                41, 42, 43, 44, 45, 46, 47, 48, 49, 50,
                51, 53, 55, 52, 54, 56, 59, 
                61, 62, 
                63, 64 
            });   
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            int r = e.RowIndex;
            if (e.ColIndex == 60 && Sm.GetGrdStr(Grd1, r, 1).Length != 0) 
            {
                e.DoDefault = false;
                if (Sm.GetGrdDec(Grd1, r, 59) == 0m)
                {
                    if (e.KeyChar == Char.Parse(" "))
                        Sm.StdMsg(mMsgType.NoData, string.Empty);
                }
                else
                {
                    if (e.KeyChar == Char.Parse(" "))
                        ShowPayrollProcessADOT(mPayrunCode, mEmpCode, Sm.GetGrdDate(Grd1, r, 1));
                }
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            int r = e.RowIndex;
            if (e.ColIndex == 60 && Sm.GetGrdStr(Grd1, r, 1).Length != 0)
            {
                if (Sm.GetGrdDec(Grd1, r, 59) == 0m)
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
                else
                    ShowPayrollProcessADOT(mPayrunCode, mEmpCode, Sm.GetGrdDate(Grd1, r, 1));
            }
        }

        #endregion

        #region Additional Method

        private void ShowPayrollProcessADOT(string PayrunCode, string EmpCode, string Dt)                
        {
            StringBuilder 
                SQL = new StringBuilder(),
                Msg = new StringBuilder();

            SQL.AppendLine("Select B.ADName, A.Amt, A.Duration ");
            SQL.AppendLine("From TblPayrollProcessADOT A ");
            SQL.AppendLine("Left Join TblAllowanceDeduction B On A.ADCode=B.ADCode ");
            SQL.AppendLine("Where A.PayrunCode=@PayrunCode ");
            SQL.AppendLine("And A.EmpCode=@EmpCode ");
            SQL.AppendLine("And A.Dt=@Dt ");
            SQL.AppendLine("Order By B.ADName;");

            try
            {
                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    var cm = new MySqlCommand()
                    {
                        Connection = cn,
                        CommandTimeout = 600,
                        CommandText = SQL.ToString()
                    };
                    Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);
                    Sm.CmParam<String>(ref cm, "@PayrunCode", PayrunCode);
                    Sm.CmParamDt(ref cm, "@Dt", Dt);

                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] { "ADName", "Amt", "Duration" });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Msg.Append("Allowance : ");
                            Msg.AppendLine(Sm.DrStr(dr, c[0]));
                            Msg.Append("Amount : ");
                            Msg.AppendLine(Sm.FormatNum(Sm.DrDec(dr, c[1]), 0));
                            Msg.Append("Duration : ");
                            Msg.AppendLine(Sm.FormatNum(Sm.DrDec(dr, c[2]), 2));
                        }
                    }
                    dr.Close();
                }
                if (Msg.Length>0) Sm.StdMsg(mMsgType.Info, Msg.ToString());
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        #endregion

        #endregion

        #region Event

        private void ChkHideInfoInGrd_CheckedChanged(object sender, EventArgs e)
        {
            Sm.GrdColInvisible(Grd1, new int[] { 
                2, 3, 4, 5, 6, 7, 
                11, 14, 15, 18, 19, 20, 
                21, 22, 23, 24, 25, 26, 27, 29, 30, 
                31, 34, 35, 36, 37, 38, 39, 
                41, 42, 43, 
                52, 54, 56, 57, 58, 
                61, 62, 63, 64, 65, 66, 67, 68, 69, 70  
                }, !ChkHideInfoInGrd.Checked);
        }
        
        private void BtnExcel_Click(object sender, EventArgs e)
        {
            Sm.ExportToExcel(Grd1);
        }

        #endregion
    }
}
