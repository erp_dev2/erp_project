﻿#region Update
/*
    18/09/2018 [WED] TblSurveyDtl diganti jadi TblLoanSummary
    04/10/2018 [WED] filter tanggal belom masuk
    25/02/2019 [MEY] penambahan informasi (kolom) partner code dan partner name
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmPLPFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmPLP mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmPLPFind(FrmPLP FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                SetSQL();
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelInd, ");
            SQL.AppendLine("Case A.Status When 'A' Then 'Approved' When 'O' Then 'Outstanding' When 'C' Then 'Cancelled' End As StatusDesc, ");
            SQL.AppendLine("MonthName(Str_To_Date(B1.Mth, '%m')) As Mth, B1.Amt, D.PnCode, D.PnName, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("From TblPLPHdr A ");
            SQL.AppendLine("Inner Join TblPLPDtl A1 On A.DocNo = A1.DocNo And (A.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("Inner Join TblSurvey B On A.SurveyDocNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblLoanSummary B1 On A.SurveyDocNo = B1.SurveyDocNo And A1.SurveyDNo = B1.SurveyDNo ");
            SQL.AppendLine("Inner Join TblRequestLP C On B.RQLPDocNo = C.DocNo ");
            SQL.AppendLine("Inner Join TblPartner D On C.PnCode = D.PnCode ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 15;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Document#", 
                    "Date",
                    "Cancel",
                    "Status",
                    "Month",
                    
                    //6-10
                    "Payment"+Environment.NewLine+"Amount",
                    "Partner Code",
                    "Partner Name",
                    "Created"+Environment.NewLine+"By",
                    "Created"+Environment.NewLine+"Date",
                   

                    //11-14              
                    "Created"+Environment.NewLine+"Time",
                    "Last"+Environment.NewLine+"Updated By", 
                    "Last"+Environment.NewLine+"Updated Date", 
                    "Last"+Environment.NewLine+"Updated Time"
                    
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    150, 80, 60, 100, 160, 
                    
                    //6-10
                    160, 100, 200, 100, 100, 
                    
                    //11-12
                    100, 100, 100, 100
                }
            );
            Sm.GrdColCheck(Grd1, new int[] { 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 6 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2, 10, 13 });
            Sm.GrdFormatTime(Grd1, new int[] { 11, 14 });
            Sm.GrdColInvisible(Grd1, new int[] { 7, 9, 10, 11, 12, 13, 14 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 7, 9, 10, 11, 12, 13, 14 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " Where 0 = 0 ";

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtPnCode.Text, new string[] { "C.PnCode", "D.PnName", "D.CompanyName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.CreateDt Desc; ",
                        new string[]
                        {
                            //0
                            "DocNo",

                            //1-5
                            "DocDt", "CancelInd", "StatusDesc", "Mth", "Amt",

                            //6-10
                             "PnCode","PnName", "CreateBy", "CreateDt", "LastUpBy",
                             
                             //11
                             "LastUpDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 10);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 13, 11);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 14, 11);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void TxtPnCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkPnCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Partner");
        }

        #endregion

        #endregion
    }
}
