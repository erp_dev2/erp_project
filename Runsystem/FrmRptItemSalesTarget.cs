﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptItemSalesTarget : RunSystem.FrmBase6
    {
        #region Field

        public string mMenuCode = "", mAccessInd = "", mSQL = "";

        #endregion

        #region Constructor

        public FrmRptItemSalesTarget(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Standard Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);

                string CurrentDateTime = Sm.ServerCurrentDateTime();

                Sl.SetLueYr(LueYr, "");
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));

                Sl.SetLueSPCode(ref LueSalesPerson);
                SetGrd();
                SetSQL();

                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.Yr, A.SpCode, B.SPName, C.Doctype, C.Yr, A.ItCode, D.ItName, D.ItCodeInternal, D.ForeignName, E.ItCtName, ");
            SQL.AppendLine("ifnull(C.Qty, 0) Qty, D.InventoryUomCode, ifnull(C.Qty2, 0) Qty2, D.InventoryUomCode2, ifnull(C.Qty3, 0) Qty3, D.InventoryUomCode3, A.AMt   ");
            SQL.AppendLine("From ( ");
	        SQL.AppendLine("    Select A.Yr, A.SpCode, B.ItCode, B.Amt ");
	        SQL.AppendLine("    From tblItemsalestargetHdr  A ");
	        SQL.AppendLine("    Inner Join TblItemSalestargetDtl B On  A.UniqueCode = B.UniqueCode And A.SpCode = B.SPCode ");
	        SQL.AppendLine("    Where CancelInd = 'N' ");
            SQL.AppendLine(")A  ");
            SQL.AppendLine("Inner Join TblSalesPerson B On A.SpCode = B.SpCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("SELECT X.Doctype, X.Yr, X.ItCode,  ");
            SQL.AppendLine("ifnull(X.Qty, 0) Qty, ifnull(X.Qty2, 0) Qty2,  ifnull(X.Qty3, 0) Qty3, X.SPCode ");
            SQL.AppendLine("From ( ");
            //SQL.AppendLine("    Select '01' As DocType, Left(A.DocDt, 4) As Yr,  B.ItCode, C.ItName, C.ItCodeInternal, C.ForeignName, D.ItCtName, ");
            //SQL.AppendLine("    SUM(B.Qty) Qty, C.InventoryUomCode, SUM(B.Qty2) Qty2,  C.InventoryUomCode2,  SUM(B.Qty3) Qty3, C.InventoryUomCode3, ");
            //SQL.AppendLine("    null As SPCode ");
            //SQL.AppendLine("    From TblDoCtHdr A ");
            //SQL.AppendLine("    Inner Join TblDoCtDtl B On A.DocNo = B.DocNo ");
            //SQL.AppendLine("    INner Join TblItem C On B.ItCode = C.ItCode ");
            //SQL.AppendLine("    INner JOin TblItemCategory  D On C.ItCtCode = D.ItCtCode ");
            //SQL.AppendLine("    Where B.CancelInd = 'N' ");
            //SQL.AppendLine("    Group BY Left(A.DocDt, 4),  B.ItCode, C.ItName, C.ItCodeInternal, C.ForeignName, D.ItCtName, ");
            //SQL.AppendLine("    C.InventoryUomCode,  C.InventoryUomCode2, C.InventoryUomCode3   ");
            //SQL.AppendLine("    Union ALL ");
	        SQL.AppendLine("    Select '02' As Doctype, Left(A.DocDt, 4) As Yr, B.ItCode, ");
	        SQL.AppendLine("    SUM(B.Qty) Qty, SUM(B.Qty2) Qty2, SUM(B.Qty3) Qty3, ");
	        SQL.AppendLine("    If(A.DrDocNo is null, J.SPCode, I.SPCode ) As SpCode ");
	        SQL.AppendLine("    from TblDOCt2Hdr A ");
	        SQL.AppendLine("    Inner Join TblDOCt2Dtl B On A.DocNo = B.DocNo ");
	        SQL.AppendLine("    Inner Join TblSalesInvoiceDtl C on B.DocNo = C.DOCtDocNo And B.Dno = C.DOCtDno ");
	        SQL.AppendLine("    Inner Join TblIncomingPaymentDtl D On C.DocNo = D.InvoiceDocNo ");
	        SQL.AppendLine("    Inner Join TblIncomingPaymentHdr E On D.DocNo = E.DocNo ");
	        SQL.AppendLine("    Inner Join TblVoucherRequestHdr F On E.VoucherrequestDocno = F.Docno And F.VoucherDocno Is null ");
	        SQL.AppendLine("    Left Join (  ");
            SQL.AppendLine("        Select Distinct A.DocNo, ifnull(C.SpCode, D.SpCode) As SpCode from TblDrHdr A  ");
		    SQL.AppendLine("        Inner Join TblDrDtl B On A.DocNo = b.DocNo ");
		    SQL.AppendLine("        Inner Join TblSOHdr  C On B.SODocNo = C.DocnO ");
		    SQL.AppendLine("        Inner Join TblCtQthdr D On C.CtQtDocNo = D.DocNO ");
	        SQL.AppendLine("    )I On A.DRDocNo = I.DocNo  ");
	        SQL.AppendLine("    Left Join ( ");
		    SQL.AppendLine("        Select Distinct A.DocNo, ifnull(C.SpCode, D.SpCode) As SpCode  ");
		    SQL.AppendLine("        from TblPLHdr A  ");
		    SQL.AppendLine("        Inner Join TblPLDtl B On A.DocNo = b.DocNo ");
		    SQL.AppendLine("        Inner Join TblSOHdr  C On B.SODocNo = C.DocnO ");
		    SQL.AppendLine("        Inner Join TblCtQthdr D On C.CtQtDocNo = D.DocNo ");
	        SQL.AppendLine("    )J On A.PLDocNo = J.DocNo ");
	        SQL.AppendLine("    Group BY Left(A.DocDt, 4),  B.ItCode ");
	        SQL.AppendLine("    )X ");
            SQL.AppendLine(")C On A.Yr = C.Yr And A.SpCode=C.SpCode And A.ItCode = C.ItCode ");
            SQL.AppendLine("Inner Join TblItem D On A.ItCode = D.ItCode ");
            SQL.AppendLine("INner JOin TblItemCategory E On D.ItCtCode = E.ItCtCode ");

           

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 18;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",
                        //1-5
                        "Year",
                        "Sales Person Code",
                        "Sales Person",
                        "DocType",
                        "Item Code",
                        //6-10
                        "Item Name",
                        "Item Code"+Environment.NewLine+"Internal",
                        "Foreign Name",
                        "Category",
                        "Target",
                        //11-15
                        "Realization",
                        "Uom",
                        "Quantity 2",
                        "Uom 2",
                        "Quantity 3",
                        //16
                        "Uom 3",
                        "Detail"
                    },
                    new int[] 
                    {
                        //0
                        30,
                        //1-5
                        80, 80, 180, 80, 100,    
                        //6-10
                        250, 150, 150, 100, 100, 
                        //11-15
                        100, 80, 100, 80, 100,  
                        //16
                        80, 40
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 17 });
            Sm.GrdFormatDec(Grd1, new int[] { 10, 11, 13, 15 }, 0);
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 4, 5, 7, 8, 13, 14, 15, 16 });
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 5, 7, 8,  }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            string Year = Sm.GetLue(LueYr);
          
            if (Year == string.Empty)
            {
                Sm.StdMsg(mMsgType.Warning, "Year still Empty.");
            }
            else
            {
                try
                {
                    Cursor.Current = Cursors.WaitCursor;
                    string Filter = string.Empty;

                    var cm = new MySqlCommand();

                    Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSalesPerson), "A.SPCode", true);
                    Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueYr), "A.Yr", true);
                    //Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));

                    Sm.ShowDataInGrid(
                            ref Grd1, ref cm,
                            mSQL + Filter + "  ",
                            new string[]
                        {
                            //0
                            "Yr",  
                            //1-5
                            "SpCode", "SPName", "Doctype", "ItCode", "ItName", 
                            //6-10
                            "ItCodeInternal", "ForeignName", "ItCtName", "AMt", "Qty", 
                            //11-15
                            "InventoryUomCode", "Qty2", "InventoryUomCode2", "Qty3", "InventoryUomCode3", 
                        },
                            (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                            {
                                Grd1.Cells[Row, 0].Value = Row + 1;
                                Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                                Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                                Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                                Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                                Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 4);
                                Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 5);

                                Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 6);
                                Sm.SetGrdValue("S", Grd1, dr, c, Row, 8, 7);
                                Sm.SetGrdValue("S", Grd1, dr, c, Row, 9, 8);
                                Sm.SetGrdValue("N", Grd1, dr, c, Row, 10, 9);
                                Sm.SetGrdValue("N", Grd1, dr, c, Row, 11, 10);

                                Sm.SetGrdValue("S", Grd1, dr, c, Row, 12, 11);
                                Sm.SetGrdValue("N", Grd1, dr, c, Row, 13, 12);
                                Sm.SetGrdValue("S", Grd1, dr, c, Row, 14, 13);
                                Sm.SetGrdValue("N", Grd1, dr, c, Row, 15, 14);
                                Sm.SetGrdValue("S", Grd1, dr, c, Row, 16, 15);

                            }, true, false, false, false
                        );
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
                finally
                {
                    Sm.FocusGrd(Grd1, 0, 1);
                    Cursor.Current = Cursors.Default;
                }
            }

        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {

        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 17 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmRptItemSalesTargetDlg(this, Sm.GetGrdStr(Grd1, e.RowIndex, 5), Sm.GetGrdStr(Grd1, e.RowIndex, 2), Sm.GetGrdStr(Grd1, e.RowIndex, 1));
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.TxtItCode.EditValue = Sm.GetGrdStr(Grd1, e.RowIndex, 5);
                f.TxtItName.EditValue = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                f.TxtSalesPerson.EditValue = Sm.GetGrdStr(Grd1, e.RowIndex, 3);
                f.ShowDialog();
            }
        }

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 17 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmRptItemSalesTargetDlg(this, Sm.GetGrdStr(Grd1, e.RowIndex, 5), Sm.GetGrdStr(Grd1, e.RowIndex, 2), Sm.GetGrdStr(Grd1, e.RowIndex, 1));
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.TxtItCode.EditValue = Sm.GetGrdStr(Grd1, e.RowIndex, 5);
                f.TxtItName.EditValue = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                f.TxtSalesPerson.EditValue = Sm.GetGrdStr(Grd1, e.RowIndex, 3);
                f.ShowDialog();
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event
        private void ChkSalesPerson_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Sales Person");
        }

        private void LueSalesPerson_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSalesPerson, new Sm.RefreshLue1(Sl.SetLueSPCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void LueYr_EditValueChanged(object sender, EventArgs e)
        {

        }
        #endregion

      

      
    }
}
