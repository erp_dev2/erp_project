﻿#region Update
/*
    25/11/2019 [VIN/IMS] new apps
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptEmpPerformanceAllowanceFixedTerm : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = "", mAccessInd = "", mSQL = "";

        #endregion

        #region Constructor

        public FrmRptEmpPerformanceAllowanceFixedTerm(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region method 
        
        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sl.SetLueYr(LueYr, string.Empty);
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                Sl.SetLueMth(LueMth);
                Sm.SetLue(LueMth, CurrentDateTime.Substring(4, 2));
                Sl.SetLueDeptCode(ref LueDeptCode);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT B.EmpCodeOld, B.EmpCode, B.EmpName, ");
            SQL.AppendLine("D.DeptName, C.PosName, E.grdlvlname, ");
            SQL.AppendLine("IfNull(G.ParValue, 0) AS 'EffectiveWorkingDay', ");
            SQL.AppendLine("if(A.WorkingDay <= IfNull(G.ParValue, 0), (IfNull(G.ParValue, 0) - A.WorkingDay) , 0) AS 'WorkPermit', ");
            SQL.AppendLine("A.WorkingDay , IFNULL(A.Salary, 0) AS Salary, A.FixAllowance, ");
            SQL.AppendLine("(A.Salary + A.FixAllowance) AS Total , IFNULL(F.IKP, 0.00) AS 'PerformanceAllowance', ");
            SQL.AppendLine("(IfNull(H.ParValue, 0) * A.Salary) AS 'ratioSalary', ");
            SQL.AppendLine("(IFNULL(F.IKP, 0.00) * (IfNull(H.ParValue, 0) * A.Salary)) AS 'PerformanceAllowanceConversion', ");
            SQL.AppendLine("((IFNULL(F.IKP, 0.00) * (IfNull(H.ParValue, 0) * A.Salary)) / IfNull(G.ParValue, 0)) * A.WorkingDay AS 'ReceivedPerformanceAllowance' ");

            SQL.AppendLine("FROM tblpayrollprocess1 A ");
            SQL.AppendLine("INNER join tblemployee B ON A.EmpCode=B.EmpCode ");
            SQL.AppendLine("AND (LEFT(A.PayrunCode, 6) = CONCAT(@Yr, @Mth)) ");
            SQL.AppendLine("Left JOIN tblgradelevelhdr E ON B.GrdLvlCode=E.GrdLvlCode ");
            SQL.AppendLine("Left JOIN tblempperformanceallowance F ON B.EmpCode=F.EmpCode and F.Yr=@Yr And F.Mth=@Mth ");
            SQL.AppendLine("INNER JOIN tblparameter G ON G.Parcode = 'NoWorkDayPerMth' ");
            SQL.AppendLine("INNER JOIN tblparameter H ON H.Parcode = 'SalaryPerformanceAllowanceMultiplier' ");
            SQL.AppendLine("Left JOIN tblposition C ON B.PosCode=C.PosCode ");
            SQL.AppendLine("Left JOIN tbldepartment D ON B.DeptCode=D.DeptCode ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 17;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Employe"+Environment.NewLine+"Code Old",
                    "Employee Code",
                    "Employee Name", 
                    "Departement",
                    "Position", 
                    
                    //6-10
                    "Grade",
                    "Effective"+Environment.NewLine+"Working Day",
                    "Work Permit",
                    "Working Day",
                    "Salary", 

                    //11-15
                    "Fix Allowace",
                    "Total",
                    "Performance Allowance",
                    "(1/12) X Salary",
                    "Performance"+Environment.NewLine+"Allowance Conversion",

                    //16
                    "Received"+Environment.NewLine+"Performance Allowance"

                },
                new int[] 
                {
                    //0
                    30, 

                    //1-5
                    150, 150, 150, 150, 150,  
                    
                    //6-10
                    150, 150, 150, 150, 150,

                    //11-15
                    150, 150, 150, 150, 150,

                    //16
                    150

                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2 });
            Sm.SetGrdProperty(Grd1, false);
        }
        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {

            if (Sm.IsLueEmpty(LueYr, "Year") || Sm.IsLueEmpty(LueMth, "Month")) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " where 0 = 0 ";
                var cm = new MySqlCommand();

                Sm.CmParam(ref cm, "@Yr", Sm.GetLue(LueYr));
                Sm.CmParam(ref cm, "@Mth", Sm.GetLue(LueMth));
                Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "B.EmpCode", "B.EmpName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "D.DeptCode", true);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By B.EmpName; ",
                    new string[]
                    {
                        //0
                        "EmpCodeOld",  
                        //1-5
                        "EmpCode", "EmpName", "DeptName", "PosName", "grdlvlname", 
                        //6-10
                        "EffectiveWorkingDay", "WorkPermit", "WorkingDay", "Salary", "FixAllowance",
                        //11-15
                        "Total", "PerformanceAllowance", "ratioSalary", "PerformanceAllowanceConversion", "ReceivedPerformanceAllowance"
                        
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 12);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 13);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 14);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 15);
                    }, true, false, false, false
                );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ForeColor = Color.Black;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.HideSubtotals(Grd1);
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 10, 11, 12, 13, 14, 15, 16 });
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion method 

        #region events 

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode,new Sm.RefreshLue1(Sl.SetLueDeptCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {

            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        #endregion events

       
    }
}
