﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmWSR : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mWSRCode = string.Empty;
        internal FrmWSRFind FrmFind;

        #endregion

        #region Constructor

        public FrmWSR(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Work Schedule Rule";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetGrd();
                SetFormControl(mState.View);

                Sm.SetLookUpEdit(LueFontSize, new string[] { "6", "7", "8", "9", "10", "11", "12" });
                Sm.SetLue(LueFontSize, "9");

                Sl.SetLueOption(ref LueWSRType, "WSRType");
                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mWSRCode.Length != 0)
                {
                    ShowData(mWSRCode);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grid 1

            Grd1.Cols.Count = 5;
            Grd1.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "1",

                        //1-4
                        "Day",
                        "Full Day",
                        "Half Day",
                        "Off Day"
                    },
                     new int[] 
                    {
                        //0
                        0,
 
                        //1-4
                        120, 100, 100, 100
                    }
                );
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1 });
            Sm.GrdColCheck(Grd1, new int[] { 2, 3, 4 });
            Sm.GrdColInvisible(Grd1, new int[] { 0, 3 }, false);

            #endregion

            #region Grid 2

            Grd2.Cols.Count = 6;
            Grd2.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[] 
                    {
                        //0
                        "Date",

                        //1-5
                        "Day",
                        "Holiday",
                        "Full Day",
                        "Half Day",
                        "Off Day"
                    },
                     new int[] 
                    {
                        //0
                        80,
 
                        //1-5
                        120, 150, 100, 100, 100
                    }
                );
            Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 1, 2 });
            Sm.GrdColCheck(Grd2, new int[] { 3, 4, 5 });
            Sm.GrdFormatDate(Grd2, new int[] { 0 });
            Sm.GrdColInvisible(Grd2, new int[] { 2, 4 }, false);

            #endregion
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtWSRCode, TxtWSRName, TmeStartTm, TmeEndTm, TmeStartTm2, TmeEndTm2, LueWSRType, DteStartDt, DteEndDt }, true);
                    ChkActInd.Properties.ReadOnly = true;
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 2, 3, 4 });
                    TxtWSRCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtWSRCode, TxtWSRName, TmeStartTm, TmeEndTm, TmeStartTm2, TmeEndTm2, LueWSRType, DteStartDt, DteEndDt }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 2, 3, 4 });
                    TxtWSRCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtWSRName }, false);
                    ChkActInd.Properties.ReadOnly = false;
                    TxtWSRName.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtWSRCode, TxtWSRName, TmeStartTm, TmeEndTm, TmeStartTm2, TmeEndTm2, LueWSRType, DteStartDt, DteEndDt
            });
            ChkActInd.Checked = false;

            ClearGrd1();
            ClearGrd2();
        }

        private void ClearGrd1()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdBoolValueFalse(ref Grd1, 0, new int[] { 2, 3, 4 });
            Sm.FocusGrd(Grd1, 0, 1);        
        }

        private void ClearGrd2()
        {
            Sm.ClearGrd(Grd2, true);
            Sm.SetGrdBoolValueFalse(ref Grd2, 0, new int[] { 3, 4, 5 });
            Sm.FocusGrd(Grd2, 0, 1);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmWSRFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                ChkActInd.Checked = true;
                Sm.SetLue(LueWSRType, "2");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtWSRCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (!TxtWSRCode.Properties.ReadOnly)
                    InsertData(sender, e);
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(SaveWSRHdr());

            if (Grd2.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd2.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd2, Row, 0).Length > 0) cml.Add(SaveWSRDtl(Row));
            }

            Sm.ExecCommands(cml);

            BtnInsertClick(sender, e);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtWSRCode, "Work schedule rule code", false) ||
                Sm.IsTxtEmpty(TxtWSRName, "Work schedule rule name", false) ||
                Sm.IsLueEmpty(LueWSRType, "Work schedule rule type") ||
                Sm.IsTmeEmpty(TmeStartTm, "Start time") ||
                Sm.IsTmeEmpty(TmeEndTm, "End time") ||
                IsWSRCodeExisted() ||
                IsWSRNameExisted() ||
                IsPeriodEmpty();
        }

        private bool IsWSRCodeExisted()
        {
            if (!TxtWSRCode.Properties.ReadOnly)
            {
                var cm = new MySqlCommand()
                {
                    CommandText = "Select WSRCode From TblWSRHdr Where WSRCode=@WSRCode Limit 1;"
                };
                Sm.CmParam<String>(ref cm, "@WSRCode", TxtWSRCode.Text);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "Work schedule rule code ( " + TxtWSRCode.Text + " ) already existed.");
                    return true;
                }
            }
            return false;
        }

        private bool IsWSRNameExisted()
        {

            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select WSRCode From TblWSRHdr " +
                    "Where WSRName=@WSRName " +
                    (TxtWSRCode.Properties.ReadOnly ? "And WSRCode<>@WSRCode " : string.Empty) +
                    "Limit 1;"
            };
            Sm.CmParam<String>(ref cm, "@WSRName", TxtWSRName.Text);
            if (TxtWSRCode.Properties.ReadOnly)
                Sm.CmParam<String>(ref cm, "@WSRCode", TxtWSRCode.Text);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "Work schedule rule name ( " + TxtWSRName.Text + " ) already existed.");
                return true;
            }
            return false;
        }

        private bool IsPeriodEmpty()
        {
            if (Sm.GetLue(LueWSRType)=="2")
            {
                return
                    Sm.IsDteEmpty(DteStartDt, "Start date") ||
                    Sm.IsDteEmpty(DteEndDt, "End date");
            }
            return false;
        }

        private MySqlCommand SaveWSRHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblWSRHdr(WSRCode, WSRName, ActInd, StartTm, EndTm, StartTm2, EndTm2, WSRType, StartDt, EndDt, ");
            SQL.AppendLine("Value1, Value2, Value3, Value4, Value5, Value6, Value7, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@WSRCode, @WSRName, 'Y', @StartTm, @EndTm, @StartTm2, @EndTm2, @WSRType, @StartDt, @EndDt, ");
            SQL.AppendLine("@Value1, @Value2, @Value3, @Value4, @Value5, @Value6, @Value7, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand(){CommandText = SQL.ToString()};
            Sm.CmParam<String>(ref cm, "@WSRCode", TxtWSRCode.Text);
            Sm.CmParam<String>(ref cm, "@WSRName", TxtWSRName.Text);
            Sm.CmParam<String>(ref cm, "@StartTm", Sm.GetTme(TmeStartTm));
            Sm.CmParam<String>(ref cm, "@EndTm", Sm.GetTme(TmeEndTm));
            Sm.CmParam<String>(ref cm, "@StartTm2", Sm.GetTme(TmeStartTm2));
            Sm.CmParam<String>(ref cm, "@EndTm2", Sm.GetTme(TmeEndTm2));
            Sm.CmParam<String>(ref cm, "@WSRType", Sm.GetLue(LueWSRType));
            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));

            if (Sm.GetLue(LueWSRType) == "1")
            {
                for (int Row = 1; Row <= 7; Row++)
                    Sm.CmParam<String>(ref cm, "@Value" + Row, GetValue(Row-1));
            }
            else
            {
                for (int Row = 1; Row <= 7; Row++)
                    Sm.CmParam<String>(ref cm, "@Value" + Row, "");
            }
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveWSRDtl(int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblWSRDtl(WSRCode, WSRDt, Value, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@WSRCode, @WSRDt, @Value, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@WSRCode", TxtWSRCode.Text);
            Sm.CmParamDt(ref cm, "@WSRDt", Sm.GetGrdDate(Grd2, Row, 0));

            string Value = "N";

            if (Sm.GetGrdBool(Grd2, Row, 3)) Value = "F";
            if (Sm.GetGrdBool(Grd2, Row, 4)) Value = "H";
            if (Sm.GetGrdBool(Grd2, Row, 5)) Value = "O";

            Sm.CmParam<String>(ref cm, "@Value", Value);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (IsEditedDataNotValid() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditWSRHdr());

            Sm.ExecCommands(cml);

            ShowData(TxtWSRCode.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return Sm.IsTxtEmpty(TxtWSRName, "Work schedule rule", false);
        }

        private MySqlCommand EditWSRHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblWSRHdr Set ");
            SQL.AppendLine("    ActInd=@ActInd, WSRName=@WSRName, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where WSRCode=@WSRCode; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@WSRCode", TxtWSRCode.Text);
            Sm.CmParam<String>(ref cm, "@WSRName", TxtWSRName.Text);
            Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string WSRCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                ShowWSRHdr(WSRCode);
                ShowWSRDtl(WSRCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowWSRHdr(string WSRCode)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@WSRCode", WSRCode);
            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select WSRCode, WSRName, ActInd, WSRType, StartDt, EndDt, StartTm, EndTm, StartTm2, EndTm2, " +
                    "Value1, Value2, Value3, Value4, Value5, Value6, Value7 " +
                    "From TblWSRHdr Where WSRCode=@WSRCode;",
                    new string[] 
                    { 
                        //0
                        "WSRCode", 
                        
                        //1-5
                        "WSRName", "ActInd", "WSRType", "StartDt", "EndDt", 
                        
                        //6-10
                        "StartTm", "EndTm", "StartTm2", "EndTm2", "Value1",  
                        
                        //11-15
                        "Value2", "Value3", "Value4", "Value5", "Value6", 
                        
                        //16
                        "Value7"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtWSRCode.EditValue = Sm.DrStr(dr, c[0]);
                        TxtWSRName.EditValue = Sm.DrStr(dr, c[1]);
                        ChkActInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y") ? true : false;
                        Sm.SetLue(LueWSRType, Sm.DrStr(dr, c[3]));;
                        Sm.SetTme(TmeStartTm, Sm.DrStr(dr, c[6]));
                        Sm.SetTme(TmeEndTm, Sm.DrStr(dr, c[7]));
                        Sm.SetTme(TmeStartTm2, Sm.DrStr(dr, c[8]));
                        Sm.SetTme(TmeEndTm2, Sm.DrStr(dr, c[9]));
                        if (Sm.GetLue(LueWSRType) == "1")
                        {
                            for (int i = 0; i <= 6; i++)
                                ShowWeekly(i, Sm.DrStr(dr, c[i + 10]));
                        }
                        else
                        { 
                            Sm.SetDte(DteStartDt, Sm.DrStr(dr, c[4]));
                            Sm.SetDte(DteEndDt, Sm.DrStr(dr, c[5]));
                            GenerateByPeriodInfo();
                        }
                    }, true
                );
        }

        private void ShowWeekly(int Row, string Value)
        {
            int Col = 4;
            switch(Value)
            {
                case "F": Col = 2; break;
                case "H": Col = 3; break;
            }
            Sm.SetGrdBoolValueFalse(ref Grd1, Row, new int[] { 2, 3, 4 });
            Grd1.Cells[Row, Col].Value = true;
        }

        private void ShowWSRDtl(string WSRCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select WSRDt, Value From TblWSRDtl ");
            SQL.AppendLine("Where WSRCode=@WSRCode Order By WSRDt; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = SQL.ToString()
                };
                Sm.CmParam<String>(ref cm, "@WSRCode", WSRCode);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]{ "WSRDt", "Value" });
                if (dr.HasRows)
                {
                    Grd2.ProcessTab = true;
                    Grd2.BeginUpdate();
                    while (dr.Read())
                    {
                        for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                        {
                            if (Sm.CompareStr(Sm.GetGrdDate(Grd2, Row, 0).Substring(0, 8), Sm.DrStr(dr, 0)))
                            {
                                Sm.SetGrdBoolValueFalse(ref Grd2, Row, new int[] { 3, 4, 5 });
                                if (Sm.DrStr(dr, 1) == "F") Grd2.Cells[Row, 3].Value = true;
                                if (Sm.DrStr(dr, 1) == "H") Grd2.Cells[Row, 4].Value = true;
                                if (Sm.DrStr(dr, 1) == "O") Grd2.Cells[Row, 5].Value = true;
                                break;
                            }
                        }
                    }
                    Grd2.EndUpdate();
                }
                dr.Close();
            }
        }

        #endregion

        #region Additional Method

        private void GenerateByPeriodInfo() 
        {
            try
            {
                ClearGrd2();
                if (!(Sm.GetDte(DteStartDt).Length > 0 && Sm.GetDte(DteEndDt).Length > 0)) return;

                string
                    StartDt = Sm.GetDte(DteStartDt).Substring(0, 8),
                    EndDt = Sm.GetDte(DteEndDt).Substring(0, 8);

                if (decimal.Parse(StartDt) > decimal.Parse(EndDt))
                {
                    Sm.StdMsg(mMsgType.Warning, "End date is earlier than start date.");
                    return;
                }

                DateTime Dt1 = new DateTime(
                    Int32.Parse(StartDt.Substring(0, 4)),
                    Int32.Parse(StartDt.Substring(4, 2)),
                    Int32.Parse(StartDt.Substring(6, 2)),
                    0, 0, 0
                    );

                DateTime Dt2 = new DateTime(
                    Int32.Parse(EndDt.Substring(0, 4)),
                    Int32.Parse(EndDt.Substring(4, 2)),
                    Int32.Parse(EndDt.Substring(6, 2)),
                    0, 0, 0
                    );

                var DurationDay = (Dt2 - Dt1).Days + 1;
                DateTime TempDt = Dt1;

                Grd2.Cells[0, 0].Value = TempDt;
                Grd2.Cells[0, 1].Value = TempDt.DayOfWeek.ToString();
                Grd2.Cells[0, (TempDt.DayOfWeek == 0 ? 5 : 3)].Value = true;
                Grd2.Rows.Add();
                Sm.SetGrdBoolValueFalse(ref Grd2, 1, new int[] { 3, 4, 5 });

                if (DurationDay > 1)
                {
                    for (int i = 1; i < DurationDay; i++)
                    {
                        TempDt = TempDt.AddDays(1);
                        Grd2.Cells[i, 0].Value = TempDt;
                        Grd2.Cells[i, 1].Value = TempDt.DayOfWeek.ToString();
                        Grd2.Cells[i, (TempDt.DayOfWeek==0?5:3)].Value = true;
                        Grd2.Rows.Add();
                        Sm.SetGrdBoolValueFalse(ref Grd2, i+1, new int[] { 3, 4, 5 });
                    }
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GenerateWeeklyInfo()
        {
            ClearGrd1();
            Sm.SetGrdBoolValueFalse(ref Grd1, 0, new int[] { 2, 3, 4 });
            for (int i = 0; i <= 6; i++)
                GenerateWeeklyInfo(i);
        }

        private void GenerateWeeklyInfo(int Row)
        {
            int Index = Row + 1;
            Grd1.Cells[Row, 0].Value = Index;
            Grd1.Cells[Row, 1].Value = GetDayName(Index);
            Grd1.Cells[Row, (Index == 7 ? 4 : 2)].Value = true;
            Grd1.Rows.Add();
            Sm.SetGrdBoolValueFalse(ref Grd1, Row + 1, new int[] { 2, 3, 4 });
        }

        private string GetDayName(int Value)
        {
            switch (Value)
            {
                case 1: return "Monday";
                case 2: return "Tuesday";
                case 3: return "Wednesday";
                case 4: return "Thursday";
                case 5: return "Friday";
                case 6: return "Saturday";
                case 7: return "Sunday";
            }
            return "None";
        }

        private void SetWSRType(string WSRType)
        {
            ClearGrd1();
            ClearGrd2();
            if (WSRType == "1")
            {
                GenerateWeeklyInfo();
                xtraTabPage1.PageVisible = true;
                xtraTabControl1.SelectedTabPage = xtraTabPage1;
                xtraTabPage2.PageVisible = false;
            }
            if (WSRType == "2")
            {
                xtraTabPage2.PageVisible = true;
                xtraTabControl1.SelectedTabPage = xtraTabPage2;
                xtraTabPage1.PageVisible = false;
            }
        }

        private string GetValue(int Row)
        {
            string Value = "N";

            if (Sm.GetGrdBool(Grd1, Row, 2)) Value = "F";
            if (Sm.GetGrdBool(Grd1, Row, 3)) Value = "H";
            if (Sm.GetGrdBool(Grd1, Row, 4)) Value = "O";

            return Value;
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueFontSize_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                var TheFont = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
                Grd1.Font = TheFont;
                Grd2.Font = TheFont;
            }
        }


        private void ChkHideInfoInGrd_CheckedChanged(object sender, EventArgs e)
        {
            Sm.GrdColInvisible(Grd1, new int[] { 3 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd2, new int[] { 4 }, !ChkHideInfoInGrd.Checked);
        }


        private void TxtWTCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtWSRCode);
        }

        private void TxtWTName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtWSRName);
        }

        private void TmeStartTm_EditValueChanged(object sender, EventArgs e)
        {
            TmeEndTm.EditValue = TmeStartTm.EditValue;
        }

        private void TmeEndTm_EditValueChanged(object sender, EventArgs e)
        {

        }

        private void TmeStartTm2_EditValueChanged(object sender, EventArgs e)
        {
            TmeEndTm2.EditValue = TmeStartTm2.EditValue;
        }

        private void DteStartDt_EditValueChanged(object sender, EventArgs e)
        {
            DteEndDt.EditValue = DteStartDt.EditValue;
            GenerateByPeriodInfo();
        }

        private void DteEndDt_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteStartDt).Length == 0 &&
                Sm.GetDte(DteEndDt).Length > 0)
                DteStartDt.EditValue = DteEndDt.EditValue;
            GenerateByPeriodInfo();
        }

        private void LueWSRType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWSRType, new Sm.RefreshLue2(Sl.SetLueOption), "WSRType");
            if (Sm.GetLue(LueWSRType).Length != 0)
                SetWSRType(Sm.GetLue(LueWSRType));
        }

        #endregion

        #region Grid Event

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (!(
                BtnSave.Enabled && 
                !TxtWSRCode.Properties.ReadOnly &&
                Sm.GetLue(LueWSRType)=="1" &&
                e.RowIndex!=Grd1.Rows.Count-1 &&
                Sm.IsGrdColSelected(new int[] { 2, 3, 4 }, e.ColIndex)
                ))
                e.DoDefault = false;
        }

        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (e.ColIndex == 2)
            {
                Grd1.Cells[e.RowIndex, 3].Value = false;
                if (Sm.GetGrdBool(Grd1, e.RowIndex, 2))
                    Grd1.Cells[e.RowIndex, 4].Value = false;
                else
                    Grd1.Cells[e.RowIndex, 4].Value = true;
            }

            if (e.ColIndex == 3)
            {
                Grd1.Cells[e.RowIndex, 4].Value = false;
                if (Sm.GetGrdBool(Grd1, e.RowIndex, 3))
                    Grd1.Cells[e.RowIndex, 2].Value = false;
                else
                    Grd1.Cells[e.RowIndex, 2].Value = true;
            }

            if (e.ColIndex == 4)
            {
                Grd1.Cells[e.RowIndex, 3].Value = false;
                if (Sm.GetGrdBool(Grd1, e.RowIndex, 4))
                    Grd1.Cells[e.RowIndex, 2].Value = false;
                else
                    Grd1.Cells[e.RowIndex, 2].Value = true;
            }
        }

        private void Grd2_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (!(
               BtnSave.Enabled &&
               !TxtWSRCode.Properties.ReadOnly &&
               Sm.GetLue(LueWSRType) == "2" &&
               e.RowIndex != Grd2.Rows.Count - 1 &&
               Sm.IsGrdColSelected(new int[] { 3, 4, 5 }, e.ColIndex)
               ))
                e.DoDefault = false;
        }

        private void Grd2_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (e.ColIndex == 3)
            {
                Grd2.Cells[e.RowIndex, 4].Value = false;
                if (Sm.GetGrdBool(Grd2, e.RowIndex, 3))
                    Grd2.Cells[e.RowIndex, 5].Value = false;
                else
                    Grd2.Cells[e.RowIndex, 5].Value = true;
            }

            if (e.ColIndex == 4)
            {
                Grd2.Cells[e.RowIndex, 5].Value = false;
                if (Sm.GetGrdBool(Grd2, e.RowIndex, 4))
                    Grd2.Cells[e.RowIndex, 3].Value = false;
                else
                    Grd2.Cells[e.RowIndex, 3].Value = true;
            }

            if (e.ColIndex == 5)
            {
                Grd2.Cells[e.RowIndex, 4].Value = false;
                if (Sm.GetGrdBool(Grd2, e.RowIndex, 5))
                    Grd2.Cells[e.RowIndex, 3].Value = false;
                else
                    Grd2.Cells[e.RowIndex, 3].Value = true;
            }
        }

        #endregion

        #endregion

    }
}
