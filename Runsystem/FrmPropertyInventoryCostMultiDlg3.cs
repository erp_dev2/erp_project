﻿#region Update
/*
    14/04/2023 [SET/BBT] Menu Baru 
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPropertyInventoryCostMultiDlg3 : RunSystem.FrmBase4
    {
        #region Field

        private FrmPropertyInventoryCostMulti mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmPropertyInventoryCostMultiDlg3(FrmPropertyInventoryCostMulti FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 12;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[]
                {
                    //0
                    "No",

                    //1-5
                    "",
                    "",
                    "Document#",
                    "Date",
                    "Deptartment Code",
                        
                    //6-10
                    "Department",
                    "PIC",
                    "Currency",
                    "Amount",
                    "Outstanding"+Environment.NewLine+"Quantity",
                        
                    //11-14
                    "Remark",

                },
                new int[]
                {
                    //0
                    50,

                    //1-5
                    20, 20, 150, 100, 70, 
                        
                    //6-10
                    150, 120, 100, 150, 100, 

                    //11
                    150
                }
            );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColButton(Grd1, new int[] { 2 });
            Sm.GrdFormatDate(Grd1, new int[] { 4 });
            Sm.GrdFormatDec(Grd1, new int[] { 9, 10 }, 0);
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 3, 4, 5, 6, 7, 8, 9, 10, 11 });
            Sm.GrdColInvisible(Grd1, new int[] { 5, 10 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT A.DocNo, B.DNo, A.DocDt, A.DeptCode, E.DeptName, A.PIC, C.InventoryUOMCode UoM, A.Remark, B.CurCode, Sum(B.Amt) Amt ");
            SQL.AppendLine("FROM tblcashadvancesettlementhdr A ");
            SQL.AppendLine("INNER JOIN tblcashadvancesettlementdtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("LEFT JOIN tblitem C ON B.ItCode = C.ItCode ");
            SQL.AppendLine("INNER JOIN tblcostcenter D ON A.CCCode = D.CCCode ");
            SQL.AppendLine("INNER JOIN tbldepartment E ON A.DeptCode = E.DeptCode ");
            SQL.AppendLine("WHERE A.CancelInd = 'N' AND A.`Status` = 'A' AND D.AssetProjectInd = 'Y' ");
            SQL.AppendLine("And A.DocNo Not In ( ");
            SQL.AppendLine("    Select CASDocNo From TblPropertyInventoryCostComponentDtl2 A1 ");
            SQL.AppendLine("    Inner Join TblPropertyInventoryCostComponentHdr A2 On A1.DocNo = A2.DocNo ");
            SQL.AppendLine("    Where A2.CancelInd = 'N' AND A2.status <> 'C') ");
            //SQL.AppendLine("    Group By A.DocNo ");
            //SQL.AppendLine("Inner Join TblCostCenter C On A.CCCode = C.CCCode ");
            //SQL.AppendLine("    And C.AssetProjectInd = 'Y' ");
            //SQL.AppendLine("Inner Join TblItem D On B.ItCode = D.ItCode ");
            //SQL.AppendLine("Inner Join TblStockPrice E On B.Source = E.Source And B.ItCode = E.ItCode ");
            //SQL.AppendLine("Left Join ");
            //SQL.AppendLine("( ");
            //SQL.AppendLine("    Select DODDocNo, DODDNo, Sum(IfNull(QtyUsed,0.00)) As QtyUsed ");
            //SQL.AppendLine("    From ( ");
            //SQL.AppendLine("        Select DODDocNo, DODDNo, Qty As QtyUsed ");
            //SQL.AppendLine("        From TblReconditionAssetHdr T1 ");
            //SQL.AppendLine("        Inner Join TblReconditionAssetDtl T2 On T1.DocNo = T2.DocNo ");
            //SQL.AppendLine("        And T1.CancelInd = 'N' ");
            //SQL.AppendLine("        And T1.Status = 'A' ");
            //SQL.AppendLine("        Union All ");
            //SQL.AppendLine("        Select T2.RecvVdDocNo As DODDocNo, T2.RecvVdDNo As DODDNo, T2.Qty As QtyUsed ");
            //SQL.AppendLine("        From TblAsset T1  ");
            //SQL.AppendLine("        Inner Join TblAssetDtl T2 On T1.AssetCode = T2.AssetCode And T1.ActiveInd = 'Y' ");
            //SQL.AppendLine("    )Tbl Group By Tbl.DODDocNo, Tbl.DODDNo ");
            //SQL.AppendLine(")F On A.DocNo = F.DODDocNo And B.DNo = F.DODDNo ");
            //SQL.AppendLine("Where IfNull(B.Qty, 0.00) - IfNull(F.QtyUsed, 0.00) > 0 ");

            //if (mFrmParent.mIsFilterByDept)
            //{
            //    SQL.AppendLine("And EXISTS ");
            //    SQL.AppendLine("(   ");
            //    SQL.AppendLine("	SELECT 1    ");
            //    SQL.AppendLine("	FROM TblGroupDepartment   ");
            //    SQL.AppendLine("	WHERE DeptCode =A.DeptCode   ");
            //    SQL.AppendLine("	AND GrpCode IN    ");
            //    SQL.AppendLine("	(   ");
            //    SQL.AppendLine("		SELECT GrpCode FROM tbluser   ");
            //    SQL.AppendLine("		WHERE UserCode = @UserCode   ");
            //    SQL.AppendLine("	)   ");
            //    SQL.AppendLine(")   ");
            //}

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);

            try
            {
                string Filter = " ";

                var cm = new MySqlCommand();
                //Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", true);
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, new string[] { "A.DocNo" });
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL.ToString() + Filter + " Group By A.DocNo Order By A.DocNo, B.DNo;",
                        new string[]
                        { 
                            //0
                            "DocNo",

                            //1-5
                            "DNo", "DocDt", "DeptCode", "DeptName", "PIC",

                            //6-10
                            "CurCode", "Amt", "Remark",
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                        
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 7);
                            //Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                            //Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 8);
                            //Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 10);
                            //Grd1.Cells[Row, 14].Value = Sm.DrDec(dr, c[8]) * Sm.DrDec(dr, c[10]);

                        }, true, false, false, false
                    );

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {

            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsDocumentAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd3.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 0, Grd1, Row2, 3);
                        Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 2, Grd1, Row2, 4);
                        Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 3, Grd1, Row2, 5);
                        Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 4, Grd1, Row2, 6);
                        Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 5, Grd1, Row2, 7);
                        Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 6, Grd1, Row2, 8);
                        Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 7, Grd1, Row2, 9);
                        Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 8, Grd1, Row2, 11);
                        mFrmParent.Grd3.Rows.Add();
                    }
                }

                Sm.ClearGrd(mFrmParent.Grd1, true);
                mFrmParent.ComputeCostComponentValue();
                Sm.SetControlEditValueNull(new List<DXE.BaseEdit>
                {
                    //mFrmParent.LueFairValueType, mFrmParent.TxtInventoryQty, mFrmParent.TxtUoM, mFrmParent.TxtAmtBefore, mFrmParent.TxtUPriceBefore,
                    //mFrmParent.TxtFairValueAmt, mFrmParent.TxtUPrice, mFrmParent.TxtUPriceAfter
                });
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 item.");
        }

        private bool IsDocumentAlreadyChosen(int Row)
        {
            for (int Index = 0; Index < mFrmParent.Grd3.Rows.Count - 1; Index++)
                if (Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd3, Index, 0), Sm.GetGrdStr(Grd1, Row, 3)) &&
                    Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd3, Index, 1), Sm.GetGrdStr(Grd1, Row, 4))
                    )
                    return true;
            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 3).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmCashAdvanceSettlement2(mFrmParent.mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 3);
                f.ShowDialog();
            }

        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 3).Length != 0)
            {
                var f = new FrmCashAdvanceSettlement2(mFrmParent.mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 3);
                f.ShowDialog();
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {

        }

        #endregion

        #region Misc Control Method
        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void Grd1_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 3).Length > 0)
                        Grd1.Cells[Row, 1].Value = !IsSelected;
            }
        }

        #endregion

        #endregion

    }
}
