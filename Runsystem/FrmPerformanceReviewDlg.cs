﻿#region Update
/*
    31/03/2018 [TKG] tambah filter site dan level 
    20/04/2018 [HAR] feedback result indicator dijadiin satu, achievement juga disatuin tpi berdasarkan type nya jika SUM ya di SUm klo AVg ya dirata rata
    23/08/2018 [WED] ngerapihin display kolom tanggal
    11/12/2018 [HAR] tambah filter kpi name
    19/12/2018 [DITA] tambah filter site
    17/05/2022 [TYO/Product] Menambah validasi KPI yang sudah ditarik ke Performance Review tidak bisa ditarik lagi
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPerformanceReviewDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmPerformanceReview mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmPerformanceReviewDlg(FrmPerformanceReview FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            this.Text = "List of KPI Document";
            base.FrmLoad(sender, e);
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            Sl.SetLuePosCode(ref LuePosCode);
            Sl.SetLueSiteCode(ref LueSite, string.Empty, mFrmParent.mIsFilterBySiteHR ? "Y" : "N");
            SetGrd();
            SetSQL();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 9;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No",

                    //1-5
                    "Document#", 
                    "",
                    "Date", 
                    "KPI Name",
                    "PosCode",

                    //6-8
                    "Responsibility By",
                    "Remark",
                    "PIC Name"
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    150, 20, 80, 200, 0,

                    //6-8
                    200, 200, 100
                }
            );
            Sm.GrdColButton(Grd1, new int[] { 2 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 3, 4, 5, 6, 7 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 5, 8 });
            Sm.GrdFormatDate(Grd1, new int[] { 3 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.KPIName, A.PosCode, B.PosName, A.Remark, C.EmpName, A.SiteCode ");
            SQL.AppendLine("From TblKPIHdr A ");
            SQL.AppendLine("Inner Join TblPosition B On A.PosCode = B.PosCode ");
            if (mFrmParent.mIsFilterBySiteHR || mFrmParent.mIsFilterByLevelHR)
            {
                SQL.AppendLine("Inner Join TblEmployee C On A.PICCode=C.EmpCode ");
                if (mFrmParent.mIsFilterBySiteHR)
                {
                    SQL.AppendLine("And C.SiteCode Is Not Null ");
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupSite ");
                    SQL.AppendLine("    Where SiteCode=IfNull(C.SiteCode, '') ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode=@UserCode ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine(") ");
                }
                if (mFrmParent.mIsFilterByLevelHR)
                {
                    SQL.AppendLine("And C.GrdLvlCode Is Not Null ");
                    SQL.AppendLine("And C.GrdLvlCode In ( ");
                    SQL.AppendLine("    Select X.GrdLvlCode From TblGradeLevelHdr X ");
                    SQL.AppendLine("    Where X.LevelCode Is Not Null ");
                    SQL.AppendLine("    And Exists( ");
                    SQL.AppendLine("        Select 1 From TblGroupDepartment ");
                    SQL.AppendLine("        Where LevelCode=X.LevelCode ");
                    SQL.AppendLine("        And GrpCode In ( ");
                    SQL.AppendLine("            Select GrpCode From TblUser ");
                    SQL.AppendLine("            Where UserCode=@UserCode ");
                    SQL.AppendLine("        ) ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine(") ");
                }
            }

            SQL.AppendLine("Where A.ActInd = 'Y' ");
            SQL.AppendLine("And Not EXISTS  ");
            SQL.AppendLine("( ");
            SQL.AppendLine("	Select 1  ");
            SQL.AppendLine("	From TblPerformanceReviewHdr  ");
            SQL.AppendLine("	Where KPIDocNo = A.DocNo  ");
            SQL.AppendLine("	And CancelInd = 'N' ");
            SQL.AppendLine(") ");

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtKPIName.Text, "A.KPIName", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LuePosCode), "A.PosCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSite), "A.SiteCode", true);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By A.DocNo;",
                    new string[] 
                    { 
                        //0
                        "DocNo",
                        
                        //1-5
                        "DocDt", "KPIName", "PosCode", "PosName", "Remark",

                        //6
                        "EmpName"
                        
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            mFrmParent.ShowKPIProcess(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
            mFrmParent.TxtKPIDocNo.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
            Sm.SetLue(mFrmParent.LuePosCode, Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 5));
            mFrmParent.TxtPICName.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 8);
            
            this.Close();
        }

        private bool IsDataAlreadyChosen(int Row)
        {
            for (int Index = 0; Index <= mFrmParent.Grd1.Rows.Count - 1; Index++)
                if (Sm.CompareStr(
                    Sm.GetGrdStr(mFrmParent.Grd1, Index, 3),
                    Sm.GetGrdStr(Grd1, Row, 3)
                    )) return true;
            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmKPI("XXX");
                f.Tag = "XXX";
                f.Text = "KPI";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmKPI("XXX");
                f.Tag = "XXX";
                f.Text = "KPI";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        private void Grd1_CellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            ChooseData();
        }


        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender); 
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void LuePosCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePosCode, new Sm.RefreshLue1(Sl.SetLuePosCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkPosCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Responsibility By");
        }

        private void TxtKPIName_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender); 
        }

        private void ChkKPIName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "KPI Name#");
        }

        private void LueSite_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSite, new Sm.RefreshLue3(Sl.SetLueSiteCode), string.Empty, mFrmParent.mIsFilterBySiteHR ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void chkSite_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }
        #endregion  
        
        #endregion

    }
}
