﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmLoadingQueueRevision : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        public string LoadAreaOld = string.Empty;
        internal FrmLoadingQueueRevisionFind FrmFind;

        #endregion

        #region Constructor

        public FrmLoadingQueueRevision(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Revisi Antrian";

            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            SetFormControl(mState.View);
            Sl.SetLueLoadingArea(ref LueAreaBongkar);
            Sl.SetLueTTCode(ref LueTTCode);

            base.FrmLoad(sender, e);
            //if this application is called from other application
            //if (mDocNo.Length != 0)
            //{
            //    ShowData(mDocNo);
            //    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
            //}
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNoLoadingQueue, TxtTTCodeOld, LueTTCode, TxtLicenceNoOld, TxtLicenceNo, TxtAreaBongkarOld,
                        TxtDriverName, TxtDriverNameOld, LueAreaBongkar, TxtItem, MeeRemark, TxtDocNo, DteDocDt
                    }, true);
                    BtnLoadingQueue.Enabled = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtLicenceNo, TxtDriverName, LueAreaBongkar, MeeRemark, DteDocDt, LueTTCode 
                    }, false);
                    BtnLoadingQueue.Enabled = true;
                    BtnLoadingQueue.Focus();
                    break;
                case mState.Edit:
                    //ChkCancelInd.Properties.ReadOnly = false;
                    //ChkCancelInd.Focus();
                    TxtDocNo.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
               TxtDocNoLoadingQueue, TxtTTCodeOld, LueTTCode, TxtLicenceNoOld, TxtLicenceNo, TxtAreaBongkarOld,
               TxtDriverName, TxtDriverNameOld, LueAreaBongkar, TxtItem, MeeRemark, TxtDocNo, DteDocDt
            });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmLoadingQueueRevisionFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);

            Sm.SetDteCurrentDate(DteDocDt);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNoLoadingQueue, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "LoadingQueueRevision", "tblloadingqueuerevision");

            var cml = new List<MySqlCommand>();  
            
            cml.Add(SaveLoadingQueueRevision(DocNo));
            cml.Add(UpdateLoadingQueue(TxtDocNoLoadingQueue.Text));
                
            Sm.ExecCommands(cml);
            
            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Tanggal Dokumen") ||
                Sm.IsLueEmpty(LueTTCode, "Transport Type")||
                Sm.IsTxtEmpty(TxtDocNoLoadingQueue, "Nomor Antrian", false)||
                Sm.IsTxtEmpty(TxtLicenceNo, "Nomor Polisi", false) ||
                Sm.IsTxtEmpty(TxtDriverName, "Nama Supir", false) ||
                Sm.IsLueEmpty(LueAreaBongkar, "Area Bongkar");

        }

        private MySqlCommand SaveLoadingQueueRevision(string DocNo)
        {
            var SQL = new StringBuilder();
            SQL.AppendLine("Insert Into tblloadingqueuerevision(DocNo, DocDt, LoadingQueueDocNo, LoadAreaOld, LicenceNoOld, DrvNameOld, TTCodeOld, LoadArea, LicenceNo, DrvName, TTCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@DocNo, @DocDt, @LoadingQueueDocNo, @LoadAreaOld, @LicenceNoOld, @DrvNameOld, @TTCodeOld, @LoadArea, @LicenceNo, @DrvName, @TTCode, @Remark, @CreateBy, CurrentDateTime()) ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@LoadingQueueDocNo", TxtDocNoLoadingQueue.Text);
            Sm.CmParam<String>(ref cm, "@LoadAreaOld", LoadAreaOld);
            Sm.CmParam<String>(ref cm, "@LicenceNoOld", TxtLicenceNoOld.Text);
            Sm.CmParam<String>(ref cm, "@DrvNameOld", TxtDriverNameOld.Text);
            Sm.CmParam<String>(ref cm, "@TTCodeOld", Sm.GetValue("Select TTCode From TblTransportType Where TTName = '"+TxtTTCodeOld.Text+"'"));
            Sm.CmParam<String>(ref cm, "@LoadArea", Sm.GetLue(LueAreaBongkar));
            Sm.CmParam<String>(ref cm, "@LicenceNo", TxtLicenceNo.Text);
            Sm.CmParam<String>(ref cm, "@DrvName", TxtDriverName.Text);
            Sm.CmParam<String>(ref cm, "@TTCode", Sm.GetLue(LueTTCode));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }


        private MySqlCommand UpdateLoadingQueue(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblLoadingQueue Set ");
            SQL.AppendLine("    LoadArea=@LoadArea, ");
            SQL.AppendLine("    LicenceNo=@LicenceNo, ");
            SQL.AppendLine("    DrvName=@DrvName, ");
            SQL.AppendLine("    TTCode = @TTCode, ");
            SQL.AppendLine("    LastUpBy=@UserCode, ");
            SQL.AppendLine("    LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo; ");

            SQL.AppendLine("Update TblLegalDocVerifyHdr Set ");
            SQL.AppendLine("    VehicleRegNo=@LicenceNo ");
            SQL.AppendLine("Where QueueNo=@DocNo ");
            SQL.AppendLine("And CancelInd='N'; ");

            SQL.AppendLine("Update TblLegalDocVerifyUpdateHdr Set ");
            SQL.AppendLine("    VehicleRegNo=@LicenceNo ");
            SQL.AppendLine("Where QueueNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@LoadArea", Sm.GetLue(LueAreaBongkar));
            Sm.CmParam<String>(ref cm, "@LicenceNo", TxtLicenceNo.Text);
            Sm.CmParam<String>(ref cm, "@DrvName", TxtDriverName.Text);
            Sm.CmParam<String>(ref cm, "@TTCode", Sm.GetLue(LueTTCode));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }


        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowLoadingQueueRevision(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowLoadingQueueRevision(string DocNo)
        {

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.LoadingQueueDocNo, A.LoadAreaOld, A.LicenceNoOld, ");
            SQL.AppendLine("A.DrvNameOld, C.TTName As TTCodeOld, A.LoadArea, A.LicenceNo, A.DrvName, A.TTCode, A.Remark, D.OptDesc, E.OptDesc As ItCat ");
            SQL.AppendLine("From TblLoadingQueueRevision A ");
            SQL.AppendLine("Inner Join TblLoadingQueue B On A.LoadingQueueDocNo = B.DocNo");
            SQL.AppendLine("Left Join TbltransportType C On A.TTCodeOld = C.TTCode");
            SQL.AppendLine("Inner Join TblOption D On A.LoadAreaOld = D.OptCode And D.optCat = 'LoadingArea'");
            SQL.AppendLine("Inner Join tblOption E On B.ItCat = E.OptCode And E.OptCat = 'LoadingItemCat' ");
            SQL.AppendLine("Left Join TblTransportType F On A.TTCode = F.TTCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");

            Sm.ShowDataInCtrl(
               ref cm, SQL.ToString(),
               new string[] { 
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "LoadingQueueDocNo", "OptDesc", "LicenceNoOld", "DrvNameOld", 
                        
                        //6-10
                        "TTCodeOld", "LoadArea", "LicenceNo", "DrvName", "TTCode", 
                        
                        //11-13
                        "Remark", "ItCat"

                    },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                    TxtDocNoLoadingQueue.EditValue = Sm.DrStr(dr, c[2]);
                    TxtAreaBongkarOld.EditValue = Sm.DrStr(dr, c[3]);
                    TxtLicenceNoOld.EditValue = Sm.DrStr(dr, c[4]);
                    TxtDriverNameOld.EditValue = Sm.DrStr(dr, c[5]);
                    TxtTTCodeOld.EditValue = Sm.DrStr(dr, c[6]);
                    LueAreaBongkar.EditValue = Sm.DrStr(dr, c[7]);
                    TxtLicenceNo.EditValue = Sm.DrStr(dr, c[8]);
                    TxtDriverName.EditValue = Sm.DrStr(dr, c[9]);
                    LueTTCode.EditValue = Sm.DrStr(dr, c[10]);
                    MeeRemark.EditValue = Sm.DrStr(dr, c[11]);
                    TxtItem.EditValue = Sm.DrStr(dr, c[12]);

                }, true
        );

        }

        #endregion
       
        #endregion

        #region Event 

        private void BtnLoadingQueue_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmLoadingQueueRevisionDlg(this));
        }

        private void LueTTCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueTTCode, new Sm.RefreshLue1(Sl.SetLueTTCode));
        }

        #endregion

       
    }
}
