﻿#region Update
/*
    08/10/2020 [WED/IMS] new apps
    15/12/2020 [WED/IMS] tambah kolom status
    11/03/2021 [TKG/IMS] menambah heat number
    21/04/2021 [TKG/IMS] menambah transfer request#
 *  12/07/2021 [ICA/IMS] menambah kolom Received
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmDOWhs4Find : RunSystem.FrmBase2
    {
        #region Field

        private FrmDOWhs4 mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmDOWhs4Find(FrmDOWhs4 FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -3);
                Sl.SetLueWhsCode(ref LueWhsCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, D.CancelInd, A.LocalDocNo, ");
            SQL.AppendLine("Case A.Status When 'A' Then 'Approved' When 'O' Then 'Outstanding' When 'C' Then 'Cancelled' Else '' End As StatusDesc, ");
            SQL.AppendLine("E.WhsName WhsFrom, B.WhsName WhsTo, F.ItCode, G.ItName, G.ItCodeInternal, G.Specification, ");
            SQL.AppendLine("F.Qty, G.InventoryUomCode, F.Qty2, G.InventoryUomCode2, F.Qty3, G.InventoryUomCode3,  ");
            SQL.AppendLine("H.PGCode, I.ProjectCode, I.ProjectName, ");
            SQL.AppendLine("H.SOContractDocNo, J.PONo, C.RecvQty, ");
            SQL.AppendLine("IfNull(C.Remark, A.Remark) Remark, C.TransferRequestProjectDocNo, ");
            if (mFrmParent.mIsDOWhs4HeatNumberEnabled)
                SQL.AppendLine("K.Value2 As HeatNumber, ");
            else
                SQL.AppendLine("Null As HeatNumber, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("From TblDOWhs4Hdr A ");
            SQL.AppendLine("Inner Join TblWarehouse B On A.WhsCode2 = B.WhsCode ");
            SQL.AppendLine("Inner Join TblDOWhs4Dtl C On A.DocNo = C.DocNo ");
            SQL.AppendLine("Inner Join TblTransferRequestWhsHdr D On C.TransferRequestWhsDocNo = D.DocNo ");
            SQL.AppendLine("Inner Join TblWarehouse E On D.WhsCode = E.WhsCode ");
            SQL.AppendLine("Inner Join TblDOWhsDtl F On C.DOWhsDocNo = F.DocNo And C.DOWhsDNo = F.DNo ");
            SQL.AppendLine("Inner Join TblItem G On F.ItCode = G.ItCode ");
            SQL.AppendLine("Inner Join TblTransferRequestProjectHdr H On A.TransferRequestProjectDocNo = H.DocNo ");
            SQL.AppendLine("Left Join TblProjectGroup I On H.PGCode = I.PGCode ");
            SQL.AppendLine("Left Join TblSOContractHdr J On H.SOContractDocNo = J.DocNo ");
            if (mFrmParent.mIsDOWhs4HeatNumberEnabled)
                SQL.AppendLine("Left Join TblSourceInfo K On F.Source=K.Source ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 33;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Document#", 
                    "Date",
                    "Cancel",
                    "Local#",
                    "Warehouse"+Environment.NewLine+"From",
                    
                    //6-10
                    "Warehouse"+Environment.NewLine+"To",
                    "Item's Code",
                    "Item's Name",
                    "Item's"+Environment.NewLine+"Local Code",
                    "Specification",
                    
                    //11-15
                    "Quantity",
                    "UoM",
                    "Quantity",
                    "UoM",
                    "Quantity",

                    //16-20
                    "UoM",
                    "Project's Group",
                    "Project's Code",
                    "Project's Name",
                    "SO Contract#",

                    //21-25
                    "Customer's PO#",
                    "Remark",
                    "Created By",
                    "Created Date", 
                    "Created Time", 

                    //26-30
                    "Last Updated By", 
                    "Last Updated Date", 
                    "Last Updated Time",
                    "Status",
                    "Heat Number",

                    //31-32
                    "Transfer Request#",
                    "Received"
                },
                 new int[] 
                {
                    //0
                    50,

                    //1-5
                    150, 80, 60, 140, 200,
                    
                    //6-10
                    200, 100, 200, 100, 250, 
                    
                    //11-15
                    100, 100, 100, 100, 100, 

                    //16-20
                    100, 130, 130, 200, 150, 

                    //21-25
                    130, 200, 100, 100, 100, 

                    //26-30
                    100, 100, 100, 120, 200,

                    //31-32
                    180, 100
                }
            );
            Sm.GrdColCheck(Grd1, new int[] { 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 11, 13, 15, 32 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2, 24, 27 });
            Sm.GrdFormatTime(Grd1, new int[] { 25, 28 });
            Sm.GrdColInvisible(Grd1, new int[] { 7, 9, 10, 13, 14, 15, 16, 17, 18, 19, 20, 21, 23, 24, 25, 26, 27, 28 }, false);
            Grd1.Cols[29].Move(4);
            Grd1.Cols[31].Move(7);
            if (mFrmParent.mIsDOWhs4HeatNumberEnabled)
                Grd1.Cols[30].Move(12);
            else
                Grd1.Cols[30].Visible = false;
            ShowInventoryUomCode();
            Grd1.Cols[32].Move(19);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 7, 9, 10, 17, 18, 19, 20, 21, 23, 24, 25, 26, 27, 28 }, !ChkHideInfoInGrd.Checked);
        }

        private void ShowInventoryUomCode()
        {
            if (mFrmParent.mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 13, 14 }, true);

            if (mFrmParent.mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 13, 14, 15, 16 }, true);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " Where (A.DocDt Between @DocDt1 And @DocDt2) ";

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, new string[] { "A.DocNo", "A.LocalDocNo" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueWhsCode), "A.WhsCode2", true);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "G.ItCode", "G.ItName" });
                Sm.FilterStr(ref Filter, ref cm, TxtTransferRequestProjectDocNo.Text, "C.TransferRequestProjectDocNo", false);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By A.CreateDt Desc; ",
                    new string[]
                    {
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "CancelInd", "LocalDocNo", "WhsFrom", "WhsTo", 
                        
                        //6-10
                        "ItCode", "ItName", "ItCodeInternal", "Specification", "Qty", 
                        
                        //11-15
                        "InventoryUomCode",  "Qty2", "InventoryUomCode2", "Qty3", "InventoryUomCode3", 
                        
                        //16-20
                        "PGCode", "ProjectCode", "ProjectName", "SOContractDocNo", "PONo",

                        //21-25
                        "Remark", "CreateBy", "CreateDt", "LastUpBy", "LastUpDt",

                        //26-29
                        "StatusDesc", "HeatNumber", "TransferRequestProjectDocNo", "RecvQty"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd1.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 12);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 14);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 15);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 16);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 17);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 18);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 19);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 20);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 21);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 22);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 24, 23);
                        Sm.SetGrdValue("T", Grd, dr, c, Row, 25, 23);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 24);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 27, 25);
                        Sm.SetGrdValue("T", Grd, dr, c, Row, 28, 25);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 29, 26);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 30, 27);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 31, 28);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 32, 29);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Event

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void ChkWhsCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Warehouse To");
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue1(Sl.SetLueWhsCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void TxtTransferRequestProjectDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkTransferRequestProjectDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Transfer Request#");
        }

        #endregion
    }
}
