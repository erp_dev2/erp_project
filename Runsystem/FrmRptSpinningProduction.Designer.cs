﻿namespace RunSystem
{
    partial class FrmRptSpinningProduction
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.BtnPrintOut = new DevExpress.XtraEditors.SimpleButton();
            this.LueFontSize = new DevExpress.XtraEditors.LookUpEdit();
            this.ChkHideInfoInGrd = new DevExpress.XtraEditors.CheckEdit();
            this.BtnExcel = new DevExpress.XtraEditors.SimpleButton();
            this.BtnPrint = new DevExpress.XtraEditors.SimpleButton();
            this.BtnRefresh = new DevExpress.XtraEditors.SimpleButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.DteDt = new DevExpress.XtraEditors.DateEdit();
            this.TcVoucher = new DevExpress.XtraTab.XtraTabControl();
            this.Tp1 = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.Grd1 = new TenTec.Windows.iGridLib.iGrid();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.Grd2 = new TenTec.Windows.iGridLib.iGrid();
            this.Grd3 = new TenTec.Windows.iGridLib.iGrid();
            this.Tp2 = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.Grd4 = new TenTec.Windows.iGridLib.iGrid();
            this.splitContainer4 = new System.Windows.Forms.SplitContainer();
            this.Grd5 = new TenTec.Windows.iGridLib.iGrid();
            this.Grd6 = new TenTec.Windows.iGridLib.iGrid();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueFontSize.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DteDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TcVoucher)).BeginInit();
            this.TcVoucher.SuspendLayout();
            this.Tp1.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).BeginInit();
            this.Tp2.SuspendLayout();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd4)).BeginInit();
            this.splitContainer4.Panel1.SuspendLayout();
            this.splitContainer4.Panel2.SuspendLayout();
            this.splitContainer4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd6)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.SteelBlue;
            this.panel1.Controls.Add(this.BtnPrintOut);
            this.panel1.Controls.Add(this.LueFontSize);
            this.panel1.Controls.Add(this.ChkHideInfoInGrd);
            this.panel1.Controls.Add(this.BtnExcel);
            this.panel1.Controls.Add(this.BtnPrint);
            this.panel1.Controls.Add(this.BtnRefresh);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.Location = new System.Drawing.Point(772, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(70, 473);
            this.panel1.TabIndex = 0;
            // 
            // BtnPrintOut
            // 
            this.BtnPrintOut.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrintOut.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrintOut.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrintOut.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrintOut.Appearance.Options.UseBackColor = true;
            this.BtnPrintOut.Appearance.Options.UseFont = true;
            this.BtnPrintOut.Appearance.Options.UseForeColor = true;
            this.BtnPrintOut.Appearance.Options.UseTextOptions = true;
            this.BtnPrintOut.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnPrintOut.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnPrintOut.Dock = System.Windows.Forms.DockStyle.Top;
            this.BtnPrintOut.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.BtnPrintOut.Location = new System.Drawing.Point(0, 93);
            this.BtnPrintOut.Name = "BtnPrintOut";
            this.BtnPrintOut.Size = new System.Drawing.Size(70, 31);
            this.BtnPrintOut.TabIndex = 7;
            this.BtnPrintOut.Text = "  &Print Out";
            this.BtnPrintOut.ToolTip = "Print Data - Preview (Alt-P)";
            this.BtnPrintOut.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnPrintOut.ToolTipTitle = "Run System";
            this.BtnPrintOut.Visible = false;
            this.BtnPrintOut.Click += new System.EventHandler(this.BtnPrintOut_Click);
            // 
            // LueFontSize
            // 
            this.LueFontSize.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.LueFontSize.EnterMoveNextControl = true;
            this.LueFontSize.Location = new System.Drawing.Point(0, 431);
            this.LueFontSize.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.LueFontSize.Name = "LueFontSize";
            this.LueFontSize.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFontSize.Properties.Appearance.Options.UseFont = true;
            this.LueFontSize.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFontSize.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueFontSize.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFontSize.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueFontSize.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueFontSize.Properties.DropDownRows = 8;
            this.LueFontSize.Properties.NullText = "[Empty]";
            this.LueFontSize.Properties.PopupWidth = 40;
            this.LueFontSize.Properties.ShowHeader = false;
            this.LueFontSize.Size = new System.Drawing.Size(70, 20);
            this.LueFontSize.TabIndex = 5;
            this.LueFontSize.ToolTip = "List\'s Font Size";
            this.LueFontSize.ToolTipTitle = "Run System";
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ChkHideInfoInGrd.EditValue = true;
            this.ChkHideInfoInGrd.Location = new System.Drawing.Point(0, 451);
            this.ChkHideInfoInGrd.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ChkHideInfoInGrd.Name = "ChkHideInfoInGrd";
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.ChkHideInfoInGrd.Properties.Caption = "Hide";
            this.ChkHideInfoInGrd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkHideInfoInGrd.Size = new System.Drawing.Size(70, 22);
            this.ChkHideInfoInGrd.TabIndex = 6;
            this.ChkHideInfoInGrd.ToolTip = "Hide some informations in the list";
            this.ChkHideInfoInGrd.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkHideInfoInGrd.ToolTipTitle = "Run System";
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnExcel.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnExcel.Dock = System.Windows.Forms.DockStyle.Top;
            this.BtnExcel.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.BtnExcel.Location = new System.Drawing.Point(0, 62);
            this.BtnExcel.Name = "BtnExcel";
            this.BtnExcel.Size = new System.Drawing.Size(70, 31);
            this.BtnExcel.TabIndex = 4;
            this.BtnExcel.Text = "  &Excel";
            this.BtnExcel.ToolTip = "Export To Excel (Alt-E)";
            this.BtnExcel.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnExcel.ToolTipTitle = "Run System";
            this.BtnExcel.Visible = false;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnPrint.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnPrint.Dock = System.Windows.Forms.DockStyle.Top;
            this.BtnPrint.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.BtnPrint.Location = new System.Drawing.Point(0, 31);
            this.BtnPrint.Name = "BtnPrint";
            this.BtnPrint.Size = new System.Drawing.Size(70, 31);
            this.BtnPrint.TabIndex = 3;
            this.BtnPrint.Text = "  &Print";
            this.BtnPrint.ToolTip = "Print Data - Preview (Alt-P)";
            this.BtnPrint.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnPrint.ToolTipTitle = "Run System";
            this.BtnPrint.Visible = false;
            // 
            // BtnRefresh
            // 
            this.BtnRefresh.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRefresh.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnRefresh.Appearance.Options.UseBackColor = true;
            this.BtnRefresh.Appearance.Options.UseFont = true;
            this.BtnRefresh.Appearance.Options.UseForeColor = true;
            this.BtnRefresh.Appearance.Options.UseTextOptions = true;
            this.BtnRefresh.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnRefresh.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnRefresh.Dock = System.Windows.Forms.DockStyle.Top;
            this.BtnRefresh.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.BtnRefresh.Location = new System.Drawing.Point(0, 0);
            this.BtnRefresh.Name = "BtnRefresh";
            this.BtnRefresh.Size = new System.Drawing.Size(70, 31);
            this.BtnRefresh.TabIndex = 2;
            this.BtnRefresh.Text = "  &Refresh";
            this.BtnRefresh.ToolTip = "Refresh Data (Alt-R)";
            this.BtnRefresh.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnRefresh.ToolTipTitle = "Run System";
            this.BtnRefresh.Click += new System.EventHandler(this.BtnRefresh_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.DteDt);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(772, 32);
            this.panel2.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(6, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 14);
            this.label1.TabIndex = 8;
            this.label1.Text = "Date";
            // 
            // DteDt
            // 
            this.DteDt.EditValue = null;
            this.DteDt.EnterMoveNextControl = true;
            this.DteDt.Location = new System.Drawing.Point(42, 4);
            this.DteDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDt.Name = "DteDt";
            this.DteDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDt.Properties.Appearance.Options.UseFont = true;
            this.DteDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDt.Size = new System.Drawing.Size(116, 20);
            this.DteDt.TabIndex = 9;
            // 
            // TcVoucher
            // 
            this.TcVoucher.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TcVoucher.HeaderAutoFill = DevExpress.Utils.DefaultBoolean.True;
            this.TcVoucher.Location = new System.Drawing.Point(0, 32);
            this.TcVoucher.Name = "TcVoucher";
            this.TcVoucher.SelectedTabPage = this.Tp1;
            this.TcVoucher.Size = new System.Drawing.Size(772, 441);
            this.TcVoucher.TabIndex = 10;
            this.TcVoucher.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.Tp1,
            this.Tp2});
            // 
            // Tp1
            // 
            this.Tp1.Appearance.Header.Options.UseTextOptions = true;
            this.Tp1.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp1.Controls.Add(this.splitContainer1);
            this.Tp1.Name = "Tp1";
            this.Tp1.Size = new System.Drawing.Size(766, 413);
            this.Tp1.Text = "Work Center Without Item";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.Grd1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Size = new System.Drawing.Size(766, 413);
            this.splitContainer1.SplitterDistance = 113;
            this.splitContainer1.TabIndex = 0;
            // 
            // Grd1
            // 
            this.Grd1.BackColorEvenRows = System.Drawing.Color.WhiteSmoke;
            this.Grd1.BackColorOddRows = System.Drawing.Color.White;
            this.Grd1.CurCellBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.Grd1.DefaultAutoGroupRow.Height = 21;
            this.Grd1.DefaultRow.Height = 20;
            this.Grd1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd1.FrozenArea.ColCount = 3;
            this.Grd1.FrozenArea.SortFrozenRows = true;
            this.Grd1.GridLines.Mode = TenTec.Windows.iGridLib.iGGridLinesMode.None;
            this.Grd1.GroupBox.BackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintBackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Grd1.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 19;
            this.Grd1.Header.UseXPStyles = false;
            this.Grd1.Location = new System.Drawing.Point(0, 0);
            this.Grd1.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.Grd1.Name = "Grd1";
            this.Grd1.ProcessTab = false;
            this.Grd1.ReadOnly = true;
            this.Grd1.RowMode = true;
            this.Grd1.RowModeHasCurCell = true;
            this.Grd1.RowTextStartColNear = 3;
            this.Grd1.ScrollBarSettings.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.SearchAsType.Mode = TenTec.Windows.iGridLib.iGSearchAsTypeMode.Seek;
            this.Grd1.SearchAsType.SearchCol = null;
            this.Grd1.SingleClickEdit = true;
            this.Grd1.Size = new System.Drawing.Size(766, 113);
            this.Grd1.TabIndex = 11;
            this.Grd1.TreeCol = null;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.VScrollBar.Visibility = TenTec.Windows.iGridLib.iGScrollBarVisibility.Always;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.Grd2);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.Grd3);
            this.splitContainer2.Size = new System.Drawing.Size(766, 296);
            this.splitContainer2.SplitterDistance = 89;
            this.splitContainer2.TabIndex = 0;
            // 
            // Grd2
            // 
            this.Grd2.BackColorEvenRows = System.Drawing.Color.WhiteSmoke;
            this.Grd2.BackColorOddRows = System.Drawing.Color.White;
            this.Grd2.CurCellBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.Grd2.DefaultAutoGroupRow.Height = 21;
            this.Grd2.DefaultRow.Height = 20;
            this.Grd2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd2.FrozenArea.ColCount = 3;
            this.Grd2.FrozenArea.SortFrozenRows = true;
            this.Grd2.GridLines.Mode = TenTec.Windows.iGridLib.iGGridLinesMode.None;
            this.Grd2.GroupBox.BackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd2.GroupBox.HintBackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd2.GroupBox.HintForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Grd2.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd2.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd2.Header.Height = 19;
            this.Grd2.Header.UseXPStyles = false;
            this.Grd2.Location = new System.Drawing.Point(0, 0);
            this.Grd2.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.Grd2.Name = "Grd2";
            this.Grd2.ProcessTab = false;
            this.Grd2.ReadOnly = true;
            this.Grd2.RowMode = true;
            this.Grd2.RowModeHasCurCell = true;
            this.Grd2.RowTextStartColNear = 3;
            this.Grd2.ScrollBarSettings.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd2.SearchAsType.Mode = TenTec.Windows.iGridLib.iGSearchAsTypeMode.Seek;
            this.Grd2.SearchAsType.SearchCol = null;
            this.Grd2.SingleClickEdit = true;
            this.Grd2.Size = new System.Drawing.Size(766, 89);
            this.Grd2.TabIndex = 12;
            this.Grd2.TreeCol = null;
            this.Grd2.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd2.VScrollBar.Visibility = TenTec.Windows.iGridLib.iGScrollBarVisibility.Always;
            // 
            // Grd3
            // 
            this.Grd3.BackColorEvenRows = System.Drawing.Color.WhiteSmoke;
            this.Grd3.BackColorOddRows = System.Drawing.Color.White;
            this.Grd3.CurCellBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.Grd3.DefaultAutoGroupRow.Height = 21;
            this.Grd3.DefaultRow.Height = 20;
            this.Grd3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd3.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd3.FrozenArea.ColCount = 3;
            this.Grd3.FrozenArea.SortFrozenRows = true;
            this.Grd3.GridLines.Mode = TenTec.Windows.iGridLib.iGGridLinesMode.None;
            this.Grd3.GroupBox.BackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd3.GroupBox.HintBackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd3.GroupBox.HintForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Grd3.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd3.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd3.Header.Height = 19;
            this.Grd3.Header.UseXPStyles = false;
            this.Grd3.Location = new System.Drawing.Point(0, 0);
            this.Grd3.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.Grd3.Name = "Grd3";
            this.Grd3.ProcessTab = false;
            this.Grd3.ReadOnly = true;
            this.Grd3.RowMode = true;
            this.Grd3.RowModeHasCurCell = true;
            this.Grd3.RowTextStartColNear = 3;
            this.Grd3.ScrollBarSettings.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd3.SearchAsType.Mode = TenTec.Windows.iGridLib.iGSearchAsTypeMode.Seek;
            this.Grd3.SearchAsType.SearchCol = null;
            this.Grd3.SingleClickEdit = true;
            this.Grd3.Size = new System.Drawing.Size(766, 203);
            this.Grd3.TabIndex = 13;
            this.Grd3.TreeCol = null;
            this.Grd3.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd3.VScrollBar.Visibility = TenTec.Windows.iGridLib.iGScrollBarVisibility.Always;
            // 
            // Tp2
            // 
            this.Tp2.Appearance.Header.Options.UseTextOptions = true;
            this.Tp2.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp2.Controls.Add(this.splitContainer3);
            this.Tp2.Name = "Tp2";
            this.Tp2.Size = new System.Drawing.Size(766, 413);
            this.Tp2.Text = "Work Center With Item";
            // 
            // splitContainer3
            // 
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer3.Location = new System.Drawing.Point(0, 0);
            this.splitContainer3.Name = "splitContainer3";
            this.splitContainer3.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.Controls.Add(this.Grd4);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.Controls.Add(this.splitContainer4);
            this.splitContainer3.Size = new System.Drawing.Size(766, 413);
            this.splitContainer3.SplitterDistance = 102;
            this.splitContainer3.TabIndex = 0;
            // 
            // Grd4
            // 
            this.Grd4.BackColorEvenRows = System.Drawing.Color.WhiteSmoke;
            this.Grd4.BackColorOddRows = System.Drawing.Color.White;
            this.Grd4.CurCellBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.Grd4.DefaultAutoGroupRow.Height = 21;
            this.Grd4.DefaultRow.Height = 20;
            this.Grd4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd4.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd4.FrozenArea.ColCount = 3;
            this.Grd4.FrozenArea.SortFrozenRows = true;
            this.Grd4.GridLines.Mode = TenTec.Windows.iGridLib.iGGridLinesMode.None;
            this.Grd4.GroupBox.BackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd4.GroupBox.HintBackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd4.GroupBox.HintForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Grd4.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd4.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd4.Header.Height = 19;
            this.Grd4.Header.UseXPStyles = false;
            this.Grd4.Location = new System.Drawing.Point(0, 0);
            this.Grd4.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.Grd4.Name = "Grd4";
            this.Grd4.ProcessTab = false;
            this.Grd4.ReadOnly = true;
            this.Grd4.RowMode = true;
            this.Grd4.RowModeHasCurCell = true;
            this.Grd4.RowTextStartColNear = 3;
            this.Grd4.ScrollBarSettings.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd4.SearchAsType.Mode = TenTec.Windows.iGridLib.iGSearchAsTypeMode.Seek;
            this.Grd4.SearchAsType.SearchCol = null;
            this.Grd4.SingleClickEdit = true;
            this.Grd4.Size = new System.Drawing.Size(766, 102);
            this.Grd4.TabIndex = 11;
            this.Grd4.TreeCol = null;
            this.Grd4.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd4.VScrollBar.Visibility = TenTec.Windows.iGridLib.iGScrollBarVisibility.Always;
            // 
            // splitContainer4
            // 
            this.splitContainer4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer4.Location = new System.Drawing.Point(0, 0);
            this.splitContainer4.Name = "splitContainer4";
            this.splitContainer4.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer4.Panel1
            // 
            this.splitContainer4.Panel1.Controls.Add(this.Grd5);
            // 
            // splitContainer4.Panel2
            // 
            this.splitContainer4.Panel2.Controls.Add(this.Grd6);
            this.splitContainer4.Size = new System.Drawing.Size(766, 307);
            this.splitContainer4.SplitterDistance = 166;
            this.splitContainer4.TabIndex = 0;
            // 
            // Grd5
            // 
            this.Grd5.BackColorEvenRows = System.Drawing.Color.WhiteSmoke;
            this.Grd5.BackColorOddRows = System.Drawing.Color.White;
            this.Grd5.CurCellBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.Grd5.DefaultAutoGroupRow.Height = 21;
            this.Grd5.DefaultRow.Height = 20;
            this.Grd5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd5.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd5.FrozenArea.ColCount = 3;
            this.Grd5.FrozenArea.SortFrozenRows = true;
            this.Grd5.GridLines.Mode = TenTec.Windows.iGridLib.iGGridLinesMode.None;
            this.Grd5.GroupBox.BackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd5.GroupBox.HintBackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd5.GroupBox.HintForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Grd5.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd5.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd5.Header.Height = 19;
            this.Grd5.Header.UseXPStyles = false;
            this.Grd5.Location = new System.Drawing.Point(0, 0);
            this.Grd5.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.Grd5.Name = "Grd5";
            this.Grd5.ProcessTab = false;
            this.Grd5.ReadOnly = true;
            this.Grd5.RowMode = true;
            this.Grd5.RowModeHasCurCell = true;
            this.Grd5.RowTextStartColNear = 3;
            this.Grd5.ScrollBarSettings.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd5.SearchAsType.Mode = TenTec.Windows.iGridLib.iGSearchAsTypeMode.Seek;
            this.Grd5.SearchAsType.SearchCol = null;
            this.Grd5.SingleClickEdit = true;
            this.Grd5.Size = new System.Drawing.Size(766, 166);
            this.Grd5.TabIndex = 12;
            this.Grd5.TreeCol = null;
            this.Grd5.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd5.VScrollBar.Visibility = TenTec.Windows.iGridLib.iGScrollBarVisibility.Always;
            // 
            // Grd6
            // 
            this.Grd6.BackColorEvenRows = System.Drawing.Color.WhiteSmoke;
            this.Grd6.BackColorOddRows = System.Drawing.Color.White;
            this.Grd6.CurCellBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.Grd6.DefaultAutoGroupRow.Height = 21;
            this.Grd6.DefaultRow.Height = 20;
            this.Grd6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd6.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd6.FrozenArea.ColCount = 3;
            this.Grd6.FrozenArea.SortFrozenRows = true;
            this.Grd6.GridLines.Mode = TenTec.Windows.iGridLib.iGGridLinesMode.None;
            this.Grd6.GroupBox.BackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd6.GroupBox.HintBackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd6.GroupBox.HintForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Grd6.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd6.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd6.Header.Height = 19;
            this.Grd6.Header.UseXPStyles = false;
            this.Grd6.Location = new System.Drawing.Point(0, 0);
            this.Grd6.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.Grd6.Name = "Grd6";
            this.Grd6.ProcessTab = false;
            this.Grd6.ReadOnly = true;
            this.Grd6.RowMode = true;
            this.Grd6.RowModeHasCurCell = true;
            this.Grd6.RowTextStartColNear = 3;
            this.Grd6.ScrollBarSettings.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd6.SearchAsType.Mode = TenTec.Windows.iGridLib.iGSearchAsTypeMode.Seek;
            this.Grd6.SearchAsType.SearchCol = null;
            this.Grd6.SingleClickEdit = true;
            this.Grd6.Size = new System.Drawing.Size(766, 137);
            this.Grd6.TabIndex = 13;
            this.Grd6.TreeCol = null;
            this.Grd6.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd6.VScrollBar.Visibility = TenTec.Windows.iGridLib.iGScrollBarVisibility.Always;
            // 
            // FrmRptSpinningProduction
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 473);
            this.Controls.Add(this.TcVoucher);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.KeyPreview = true;
            this.Name = "FrmRptSpinningProduction";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Load += new System.EventHandler(this.FrmRptSpinningProduction_Load);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueFontSize.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DteDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TcVoucher)).EndInit();
            this.TcVoucher.ResumeLayout(false);
            this.Tp1.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).EndInit();
            this.Tp2.ResumeLayout(false);
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel2.ResumeLayout(false);
            this.splitContainer3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd4)).EndInit();
            this.splitContainer4.Panel1.ResumeLayout(false);
            this.splitContainer4.Panel2.ResumeLayout(false);
            this.splitContainer4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd6)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.Panel panel1;
        internal DevExpress.XtraEditors.LookUpEdit LueFontSize;
        protected internal DevExpress.XtraEditors.CheckEdit ChkHideInfoInGrd;
        protected DevExpress.XtraEditors.SimpleButton BtnPrint;
        protected DevExpress.XtraEditors.SimpleButton BtnRefresh;
        protected System.Windows.Forms.Panel panel2;
        private DevExpress.XtraTab.XtraTabControl TcVoucher;
        private DevExpress.XtraTab.XtraTabPage Tp1;
        private DevExpress.XtraTab.XtraTabPage Tp2;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.SplitContainer splitContainer3;
        private System.Windows.Forms.SplitContainer splitContainer4;
        private System.Windows.Forms.Label label1;
        internal DevExpress.XtraEditors.DateEdit DteDt;
        protected internal TenTec.Windows.iGridLib.iGrid Grd1;
        protected internal TenTec.Windows.iGridLib.iGrid Grd2;
        protected internal TenTec.Windows.iGridLib.iGrid Grd3;
        protected internal TenTec.Windows.iGridLib.iGrid Grd4;
        protected internal TenTec.Windows.iGridLib.iGrid Grd5;
        protected internal TenTec.Windows.iGridLib.iGrid Grd6;
        protected DevExpress.XtraEditors.SimpleButton BtnExcel;
        protected DevExpress.XtraEditors.SimpleButton BtnPrintOut;
    }
}