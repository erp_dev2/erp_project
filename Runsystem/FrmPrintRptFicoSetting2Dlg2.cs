﻿#region Update
/*
    02/04/2020 [WED/YK] untuk pilih COA nya
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPrintRptFicoSetting2Dlg2 : RunSystem.FrmBase4
    {
        #region Field

        private FrmPrintRptFicoSetting2 mFrmParent;
        private int mRow = 0;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmPrintRptFicoSetting2Dlg2(FrmPrintRptFicoSetting2 FrmParent, int Row)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mRow = Row;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "List Of COA";
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                ChkParentOnly.Checked = true;
                SetSQL();
                ShowData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 6;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "",
                    "Account#",
                    "Account Description",
                    "Alias",
                    "Type"
                }, new int[]
                {
                    50,
                    20, 200, 300, 180, 100
                }
            );
            Sm.GrdColInvisible(Grd1, new int[] { 2 });
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 3, 4, 5 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select AcNo, Concat(AcNo, ' - ', AcDesc) AcDesc, Alias, ");
            SQL.AppendLine("Case AcType When 'C' Then 'Credit' When 'D' Then 'Debit' End As AcType ");
            SQL.AppendLine("From TblCOA ");
            SQL.AppendLine("Where ActInd = 'Y' ");

            mSQL = SQL.ToString();
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " And 0 = 0 ";

                if (ChkParentOnly.Checked)
                {
                    if (mFrmParent.mCOALevelFicoSettingJournalToCBP.Length > 0)
                    {
                        Filter += " And Level <= " + mFrmParent.mCOALevelFicoSettingJournalToCBP + Environment.NewLine;
                    }

                    Filter += " And AcNo In ";
                    Filter += " ( ";
                    Filter += "     Select Distinct Parent ";
                    Filter += "     From TblCOA ";
                    Filter += "     Where ActInd = 'Y' ";
                    Filter += " ) ";
                }

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtAcNo.Text, new string[] { "AcNo", "AcDesc", "Alias" });
                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By AcNo; ",
                    new string[] 
                    { 
                        "AcNo", 
                        "AcDesc", "Alias", "AcType"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd1.Cells[Row, 0].Value = Row + 1;
                        Grd1.Cells[Row, 1].Value = false;
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 0);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 1);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 3);
                    }, true, false, false, false
                );
                GetCheckedData();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                string mAcNo = string.Empty;

                for (int i = 0; i < Grd1.Rows.Count; ++i)
                {
                    if (Sm.GetGrdBool(Grd1, i, 1))
                    {
                        if (mAcNo.Length > 0) mAcNo += ",";
                        mAcNo += Sm.GetGrdStr(Grd1, i, 2);
                    }
                }

                mFrmParent.GetCOAList(mRow, mAcNo);
                this.Close();
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    Grd1.Cells[Row, 1].Value = !IsSelected;
            }
        }

        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (e.ColIndex == 1)
            {
                TickGrdCols(e.RowIndex, e.ColIndex);
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Methods

        private void GetCheckedData()
        {
            if (Sm.GetGrdStr(mFrmParent.Grd1, mRow, 7).Length > 0)
            {
                string mAcNo = mFrmParent.l4[mRow].AcNo; //Sm.GetGrdStr(mFrmParent.Grd1, mRow, 7);
                string[] mAcNos = mAcNo.Split(',');
                int mIndex = 0;

                for (int i = 0; i < Grd1.Rows.Count; ++i)
                {
                    for (int j = mIndex; j < mAcNos.Count(); ++j)
                    {
                        if (Sm.GetGrdStr(Grd1, i, 2) == mAcNos[j])
                        {
                            Grd1.Cells[i, 1].Value = true;
                            mIndex = j;
                        }
                    }
                }
            }
        }

        private void TickGrdCols(int RowIndex, int ColIndex)
        {
            try
            {
                if (ColIndex != 1) return;

                int Len = Sm.GetGrdStr(Grd1, RowIndex, 2).Length;
                bool Value = Sm.GetGrdBool(Grd1, RowIndex, ColIndex);
                string MenuCode = Sm.GetGrdStr(Grd1, RowIndex, 2);

                for (int Row = RowIndex + 1; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length <= Len)
                    {
                        break;
                    }
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length > Len)
                    {
                        Grd1.Cells[Row, ColIndex].Value = Value;
                    }

                }

                for (int Row = RowIndex - 1; Row >= 0; Row--)
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length < Len &&
                        ("X" + MenuCode).IndexOf("X" + Sm.GetGrdStr(Grd1, Row, 2)) >= 0 &&
                        (Value || (!Value && IsNotHaveThickedChildNode(Row, ColIndex, Sm.GetGrdStr(Grd1, Row, 2).Length))))
                        Grd1.Cells[Row, ColIndex].Value = Value;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private bool IsNotHaveThickedChildNode(int CurRow, int CurCol, int Len)
        {
            for (int Row = CurRow + 1; Row < Grd1.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 2).Length > Len)
                {
                    if (Sm.GetGrdBool(Grd1, Row, CurCol))
                        return false;
                }
                else
                    return true;
            }
            return true;
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtAcNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkAcNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Account");
        }

        private void ChkParentOnly_CheckedChanged(object sender, EventArgs e)
        {
            Sm.ClearGrd(Grd1, false);
        }

        #endregion

        #endregion

    }
}
