﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRecvDeptFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmRecvDept mFrmParent;
        private string mSQL = string.Empty;
        //private bool mIsInventoryShowTotalQty = false;

        #endregion

        #region Constructor

        public FrmRecvDeptFind(FrmRecvDept FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -1);
                Sl.SetLueWhsCode(ref LueWhsCode, string.Empty);
                Sl.SetLueDeptCode(ref LueDeptCode);
                //mIsInventoryShowTotalQty = (Sm.GetParameter("IsInventoryShowTotalQty") == "Y");
                ChkExcludedCancelledItem.Checked = true;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, B.CancelInd, C.WhsName, D.DeptName, ");
            SQL.AppendLine("B.DODeptDocNo, E.DocDt As DODeptDocDt, F.ItCode, G.ItCodeInternal, G.ItName, F.BatchNo, F.Source, F.Lot, F.Bin, ");
            SQL.AppendLine("B.Qty, B.Qty2, B.Qty3, G.InventoryUomCode, G.InventoryUomCode2, G.InventoryUomCode3,  ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("From TblRecvDeptHdr A ");
            SQL.AppendLine("Inner Join TblRecvDeptDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblWarehouse C On A.WhsCode=C.WhsCode ");
            SQL.AppendLine("    And Exists( ");
            SQL.AppendLine("        Select WhsCode From TblGroupWarehouse ");
            SQL.AppendLine("        Where WhsCode=C.WhsCode ");
            SQL.AppendLine("        And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("Inner Join TblDepartment D On A.DeptCode=D.DeptCode ");
            SQL.AppendLine("Inner Join TblDODeptHdr E On B.DODeptDocNo=E.DocNo ");
            SQL.AppendLine("Inner Join TblDODeptDtl F On B.DODeptDocNo=F.DocNo And B.DODeptDNo=F.DNo ");
            SQL.AppendLine("Inner Join TblItem G On F.ItCode=G.ItCode ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 28;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#", 
                        "Date",
                        "Cancel",
                        "Warehouse",
                        "Department",

                        //6-10
                        "DO#",
                        "",
                        "Item's Code",
                        "",
                        "Item's"+Environment.NewLine+"Local Code",

                        //11-15
                        "Item's Name",
                        "Batch Number",
                        "Source",
                        "Lot",
                        "Bin",

                        //16-20
                        "Quantity",
                        "UoM",
                        "Quantity 2",
                        "Uom 2",
                        "Quantity 3",

                        //21-25
                        "Uom 3",
                        "Created"+Environment.NewLine+"By",
                        "Created"+Environment.NewLine+"Date", 
                        "Created"+Environment.NewLine+"Time", 
                        "Last"+Environment.NewLine+"Updated By", 

                        //26-27
                        "Last"+Environment.NewLine+"Updated Date", 
                        "Last"+Environment.NewLine+"Updated Time"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        130, 80, 50, 150, 150, 
                        
                        //6-10
                        130, 20, 80, 20, 80, 
                        
                        //11-15
                        250, 200, 150, 60, 80, 

                        //16-20
                        80, 80, 80, 80, 80, 

                        //21-25
                        80, 80, 100, 100, 80,

                        //26-27
                        130, 130
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 7, 9 });
            Sm.GrdColCheck(Grd1, new int[] { 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 16, 18, 20 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2, 8, 23, 26 });
            Sm.GrdFormatTime(Grd1, new int[] { 24, 27 });
            Sm.GrdColInvisible(Grd1, new int[] { 7, 8, 9, 10, 13, 14, 15, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27 }, false);
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 8, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27 });
            ShowInventoryUomCode();
            Sm.SetGrdProperty(Grd1 , false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 7, 8, 9, 10, 13, 14, 15, 22, 23, 24, 25, 26, 27 }, !ChkHideInfoInGrd.Checked);
        }

        private void ShowInventoryUomCode()
        {
            if (mFrmParent.mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 18, 19 }, true);

            if (mFrmParent.mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 18, 19, 20, 21 }, true);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();

                if (ChkExcludedCancelledItem.Checked)
                    Filter += " And B.CancelInd='N' ";

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDt");
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueWhsCode), "A.WhsCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "A.DeptCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtDODeptDocNo.Text, "B.DODeptDocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "F.ItCode", "G.ItCodeInternal", "G.ItName" });
                Sm.FilterStr(ref Filter, ref cm, TxtBatchNo.Text, "F.BatchNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtLot.Text, "F.Lot", false);
                Sm.FilterStr(ref Filter, ref cm, TxtBin.Text, "F.Bin", false);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.CreateDt Desc;",
                        new string[]
                        {
                            //0
                            "DocNo", 
                            
                            //1-5
                            "DocDt", "CancelInd", "WhsName", "DeptName", "DODeptDocNo", 
                            
                            //6-10
                            "ItCode", "ItCodeInternal", "ItName", "BatchNo", "Source", 
                            
                            //11-15
                            "Lot", "Bin", "Qty", "InventoryUomCode", "Qty2", 
                            
                            //16-20
                            "InventoryUomCode2", "Qty3", "InventoryUomCode3", "CreateBy", "CreateDt", 
                            
                            //21-22
                            "LastUpBy", "LastUpDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 12);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 14);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 15);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 16);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 17);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 18);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 19);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 23, 20);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 24, 20);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 21);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 26, 22);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 27, 22);
                        }, true, false, false, false
                    );
                //if (mIsInventoryShowTotalQty)
                //{
                //    iGSubtotalManager.BackColor = Color.LightSalmon;
                //    iGSubtotalManager.ShowSubtotalsInCells = true;
                //    iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 16, 18, 20 });
                //    Grd1.Rows.ExpandAll();
                //}
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            e.DoDefault = false;
            if (e.ColIndex == 7 && Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length != 0)
            {
                var f = new FrmDODeptWO(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                f.ShowDialog();
            }
            if (e.ColIndex == 9 && Sm.GetGrdStr(Grd1, e.RowIndex, 8).Length != 0)
            {
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 8);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 7 && Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length != 0)
            {
                var f = new FrmDODeptWO(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                f.ShowDialog();
            }
            if (e.ColIndex == 9 && Sm.GetGrdStr(Grd1, e.RowIndex, 8).Length != 0)
            {
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 8);
                f.ShowDialog();
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            //if (mIsInventoryShowTotalQty) Sm.GrdExpand(Grd1);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }


        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Date");
        }

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue2(Sl.SetLueWhsCode), string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkWhsCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Warehouse");
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void TxtDODeptDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDODeptDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "DO#");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void TxtBatchNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBatchNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Batch number");
        }

        private void TxtLot_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkLot_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Lot");
        }

        private void TxtBin_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBin_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Bin");
        }

        #endregion

        #endregion

    }
}
