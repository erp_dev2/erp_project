﻿#region Update
/*
    06/12/2019 [WED/WMS] new apps
    31/12/2019 [WED/WMS] tambah validasi tidak boleh cancel jika picking nya sudah di packing
    08/01/2020 [WED/WMS] validasi habis stock, diperbaiki
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmIMMPicking : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string 
            mMenuCode = string.Empty, mAccessInd = string.Empty,
            mDocTitle = string.Empty,
            mDocAbbr = string.Empty,
            mDocTypeMin = "05",
            mDocTypeMinCancel = "06",
            mDocTypePlus = "11",
            mDocTypePlusCancel = "12",
            mBarcode = string.Empty,
            mDocNo = string.Empty, mDocSeqNo = string.Empty; //if this application is called from other application
        internal FrmIMMPickingFind FrmFind;
        private bool mIsNew = false;

        #endregion

        #region Constructor

        public FrmIMMPicking(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                Sl.SetLueWhsCode(ref LueWhsCode, string.Empty);
                Sl.SetLueBin(ref LueBin);
                base.FrmLoad(sender, e);
                //if this application is called from other application

                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo, mDocSeqNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 7;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1,
                new string[] 
                {
                    //0
                    "",
                    
                    //1-5
                    "Bin",
                    "Source",
                    "Product's Code",
                    "Product's Description",
                    "Seller",
                    
                    //6
                    "Quantity"
                },
                 new int[] 
                {
                    //0
                    20,

                    //1-5
                    130, 130, 100, 200, 150, 

                    //6
                    150
                }
            );
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 2, 3, 4, 5, 6 });
            Sm.GrdFormatDec(Grd1, new int[] { 6 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 0 });
            Sm.GrdColInvisible(Grd1, new int[] { 3 }, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 3 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { DteDocDt, LueWhsCode, LueBin, MeeCancelReason, TxtBarcode }, true);
                    ChkCancelInd.Properties.ReadOnly = true;
                    Grd1.ReadOnly = true;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { DteDocDt, LueWhsCode, LueBin, TxtBarcode }, false);
                    Grd1.ReadOnly = false;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeCancelReason }, false);
                    ChkCancelInd.Properties.ReadOnly = false;
                    MeeCancelReason.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> { TxtDocNo, DteDocDt, LueWhsCode, MeeCancelReason, TxtBarcode, LueBin });
            ChkCancelInd.Checked = false;
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 6 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmIMMPickingFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
            MeeCancelReason.Focus();
            
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0) InsertData(sender, e);
                else EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 0)
                {
                    e.DoDefault = false;
                    if (e.KeyChar == Char.Parse(" ") && !Sm.IsLueEmpty(LueWhsCode, "Warehouse"))
                        Sm.FormShowDialog(new FrmIMMPickingDlg(this, Sm.GetLue(LueWhsCode)));
                }
            }

            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmIMMOutboundOrder("***");
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.mDocSeqNo = Sm.GetGrdStr(Grd1, e.RowIndex, 3);
                f.ShowDialog();
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.GrdRemoveRow(Grd1, e, BtnSave);
                Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && BtnSave.Enabled && !Sm.IsLueEmpty(LueWhsCode, "Warehouse"))
                Sm.FormShowDialog(new FrmIMMPickingDlg(this, Sm.GetLue(LueWhsCode)));

            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmIMMOutboundOrder("***");
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.mDocSeqNo = Sm.GetGrdStr(Grd1, e.RowIndex, 3);
                f.ShowDialog();
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = string.Empty;
            decimal DocSeqNo = 0m;

            GenerateDocNo(ref DocNo, ref DocSeqNo);

            var cml = new List<MySqlCommand>();

            cml.Add(SaveIMMPickingHdr(DocNo, DocSeqNo));
            for (int r = 0; r < Grd1.Rows.Count; r++)
                if (Sm.GetGrdStr(Grd1, r, 1).Length > 0) cml.Add(SaveIMMPickingDtl(DocNo, DocSeqNo, r));

            cml.Add(SaveStock(DocNo, DocSeqNo));

            Sm.ExecCommands(cml);

            BtnInsertClick(sender, e);
        }

        private void GenerateDocNo(ref string DocNo, ref decimal DocSeqNo)
        {
            string DocDt = Sm.GetDte(DteDocDt);
            string Yr = Sm.Left(DocDt, 4);
            string Mth = DocDt.Substring(4, 2);

            DocNo = string.Concat(mDocTitle, "/", mDocAbbr, "/", Yr, "/", Mth, "/");

            var SQL = new StringBuilder();

            SQL.Append("Select IfNull(( ");
            SQL.Append("    Select DocSeqNo From TblIMMPickingHdr ");
            SQL.Append("    Where DocNo=@Param Order By DocSeqNo Desc Limit 1 ");
            SQL.Append("   ), 0.00) As DocSeqNo");

            DocSeqNo = Sm.GetValueDec(SQL.ToString(), DocNo);
            DocSeqNo += 1;
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueWhsCode, "Warehouse") ||
                Sm.IsLueEmpty(LueBin, "Bin") ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                IsItemWhsInvalid() ||
                IsStockInvalid()
                ;
        }

        private bool IsItemWhsInvalid()
        {
            var SQL = new StringBuilder();
            string mSource = string.Empty;

            for (int i = 0; i < Grd1.Rows.Count; ++i)
            {
                if (Sm.GetGrdStr(Grd1, i, 2).Length > 0)
                {
                    if (mSource.Length > 0) mSource += ",";
                    mSource += Sm.GetGrdStr(Grd1, i, 2);
                }
            }

            if (mSource.Length > 0)
            {
                SQL.AppendLine("Select Source ");
                SQL.AppendLine("From TblIMMStockSummary ");
                SQL.AppendLine("Where WhsCode != @Param1 ");
                SQL.AppendLine("And Find_In_Set(Source, @Param2) ");
                SQL.AppendLine("And Qty > 0 ");
                SQL.AppendLine("Limit 1; ");

                if (Sm.IsDataExist(SQL.ToString(), Sm.GetLue(LueWhsCode), mSource, string.Empty))
                {
                    Sm.StdMsg(mMsgType.Warning, "This source : " + Sm.GetValue(SQL.ToString(), Sm.GetLue(LueWhsCode), mSource, string.Empty) + " is not from warehouse " + LueWhsCode.Text);
                    return true;
                }
            }
            return false;
        }

        private bool IsStockInvalid()
        {
            var SQL = new StringBuilder();
            string mSource = string.Empty;

            for (int i = 0; i < Grd1.Rows.Count; ++i)
            {
                if(Sm.GetGrdStr(Grd1, i, 2).Length > 0)
                {
                    if (mSource.Length > 0) mSource += ",";
                    mSource += string.Concat(Sm.GetGrdStr(Grd1, i, 1), Sm.GetGrdStr(Grd1, i, 2));
                }
            }

            if (mSource.Length > 0)
            {
                SQL.AppendLine("Select Source ");
                SQL.AppendLine("From TblIMMStockSummary ");
                SQL.AppendLine("Where WhsCode = @Param1 ");
                SQL.AppendLine("And Find_In_Set(Concat(Bin, Source), @Param2) ");
                SQL.AppendLine("And Qty <= 0 ");
                SQL.AppendLine("Limit 1; ");

                if (Sm.IsDataExist(SQL.ToString(), Sm.GetLue(LueWhsCode), mSource, string.Empty))
                {
                    Sm.StdMsg(mMsgType.Warning, "This source : " + Sm.GetValue(SQL.ToString(), Sm.GetLue(LueWhsCode), mSource, string.Empty) + " has no stock.");
                    return true;
                }
            }

            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 product.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int r = 0; r < Grd1.Rows.Count - 1; r++)
                if (Sm.IsGrdValueEmpty(Grd1, r, 2, false, "Source is empty."))
                    return true;
            return false;
        }

        private MySqlCommand SaveIMMPickingHdr(string DocNo, decimal DocSeqNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblIMMPickingHdr ");
            SQL.AppendLine("(DocNo, DocSeqNo, DocDt, CancelInd, WhsCode, Bin, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocSeqNo, @DocDt, 'N', @WhsCode, @Bin, @UserCode, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<Decimal>(ref cm, "@DocSeqNo", DocSeqNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            Sm.CmParam<String>(ref cm, "@Bin", Sm.GetLue(LueBin));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveIMMPickingDtl(string DocNo, decimal DocSeqNo, int r)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblIMMPickingDtl(DocNo, DocSeqNo, Source, Bin, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocSeqNo, @Source, @Bin, @UserCode, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<Decimal>(ref cm, "@DocSeqNo", DocSeqNo);
            Sm.CmParam<String>(ref cm, "@Source", Sm.GetGrdStr(Grd1, r, 2));
            Sm.CmParam<String>(ref cm, "@Bin", Sm.GetGrdStr(Grd1, r, 1));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveStock(string DocNo, decimal DocSeqNo)
        {
            var SQL = new StringBuilder();

            // decrease detail's item
            SQL.AppendLine("Insert Into TblIMMStockMovement ");
            SQL.AppendLine("(DocType, DocNo, DocSeqNo, Source, DocDt, WhsCode, LocCode, Bin, ProdCode, Qty, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocTypeMin, @DocNo, @DocSeqNo, B.Source, A.DocDt, A.WhsCode, C.LocCode, B.Bin, C.ProdCode, -1.00, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblIMMPickingHdr A ");
            SQL.AppendLine("Inner Join TblIMMPickingDtl B On A.DocNo=B.DocNo And A.DocSeqNo=B.DocSeqNo ");
            SQL.AppendLine("Inner Join TblIMMStockSummary C On B.Source=C.Source And A.WhsCode=C.WhsCode And B.Bin = C.Bin And C.Qty>0.00 ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("And A.DocSeqNo=@DocSeqNo; ");

            SQL.AppendLine("Update TblIMMStockSummary A ");
            SQL.AppendLine("Inner Join TblIMMPickingHdr B ");
            SQL.AppendLine("    On B.DocNo=@DocNo ");
            SQL.AppendLine("    And B.DocSeqNo=@DocSeqNo ");
            SQL.AppendLine("    And A.WhsCode=B.WhsCode ");
            SQL.AppendLine("Inner Join TblIMMPickingDtl C ");
            SQL.AppendLine("    On B.DocNo=C.DocNo ");
            SQL.AppendLine("    And B.DocSeqNo=C.DocSeqNo ");
            SQL.AppendLine("    And A.Source=C.Source ");
            SQL.AppendLine("    And A.Bin = C.Bin ");
            SQL.AppendLine("Set A.Qty = 0.00 ");
            SQL.AppendLine("Where A.Qty > 0.00; ");

            // increase on bin's destination (header)
            SQL.AppendLine("Insert Into TblIMMStockMovement ");
            SQL.AppendLine("(DocType, DocNo, DocSeqNo, Source, DocDt, WhsCode, LocCode, Bin, ProdCode, Qty, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocTypePlus, @DocNo, @DocSeqNo, B.Source, A.DocDt, A.WhsCode, C.LocCode, A.Bin, C.ProdCode, 1.00, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblIMMPickingHdr A ");
            SQL.AppendLine("Inner Join TblIMMPickingDtl B On A.DocNo = B.DocNo And A.DocSeqNo = B.DocSeqNo ");
            SQL.AppendLine("Inner Join TblIMMStockSummary C On B.Source = C.Source And A.WhsCode = C.WhsCode And B.Bin = C.Bin ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("And A.DocSeqNo=@DocSeqNo; ");

            SQL.AppendLine("Insert Into TblIMMStockSummary ");
            SQL.AppendLine("(WhsCode, Bin, Source, ProdCode, SellerCode, LocCode, Qty, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.WhsCode, A.Bin, B.Source, C.ProdCode, C.SellerCode, C.LocCode, 1.00, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblIMMPickingHdr A ");
            SQL.AppendLine("Inner Join TblIMMPickingDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("    And A.DocSeqNo = B.DocSeqNo ");
            SQL.AppendLine("    And A.DocNo = @DocNo ");
            SQL.AppendLine("    And A.DocSeqNo = @DocSeqNo ");
            SQL.AppendLine("Inner Join TblIMMStockSummary C On A.WhsCode = C.WhsCode ");
            SQL.AppendLine("    And B.Bin = C.Bin ");
            SQL.AppendLine("    And B.Source = C.Source ");
            SQL.AppendLine("On Duplicate Key Update ");
            SQL.AppendLine("    Qty = 1.00, ");
            SQL.AppendLine("    LastUpBy = @UserCode, ");
            SQL.AppendLine("    LastUpDt = CurrentDateTime(); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<Decimal>(ref cm, "@DocSeqNo", DocSeqNo);
            Sm.CmParam<String>(ref cm, "@DocTypeMin", mDocTypeMin);
            Sm.CmParam<String>(ref cm, "@DocTypePlus", mDocTypePlus);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditIMMPickingHdr());

            cml.Add(SaveStock2());

            Sm.ExecCommands(cml);

            ShowData(mDocNo, mDocSeqNo);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDocumentNotCancelled() ||
                IsDataCancelledAlready() ||
                Sm.IsMeeEmpty(MeeCancelReason, "Reason for cancellation") ||
                IsDataAlreadyProcessed();
        }

        private bool IsDataAlreadyProcessed()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Concat(A.DocNo, A.DocSeqNo) ");
            SQL.AppendLine("From TblIMMPackingDtl A ");
            SQL.AppendLine("Inner Join TblIMMPackingHdr B On A.DocNo = B.DocNo ");
            SQL.AppendLine("    And A.DocSeqNo = B.DocSeqNo ");
            SQL.AppendLine("    And B.CancelInd = 'N' ");
            SQL.AppendLine("    And A.PickingDocNo = @Param1 ");
            SQL.AppendLine("    And A.PickingDocSeqNo = @Param2 ");
            SQL.AppendLine("Limit 1 ");
            SQL.AppendLine("; ");

            if(Sm.IsDataExist(SQL.ToString(), mDocNo, mDocSeqNo, string.Empty))
            {
                Sm.StdMsg(mMsgType.Warning, "This data has processed to Packing #" + Sm.GetValue(SQL.ToString(), mDocNo, mDocSeqNo, string.Empty));
                return true;
            }

            return false;
        }

        private bool IsDocumentNotCancelled()
        {
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this document.");
                return true;
            }
            return false;
        }

        private bool IsDataCancelledAlready()
        {
            if (Sm.IsDataExist("Select 1 From TblIMMPickingHdr Where CancelInd='Y' And DocNo=@Param1 And DocSeqNo=@Param2;",
                mDocNo, mDocSeqNo, string.Empty))
            {
                Sm.StdMsg(mMsgType.Warning, "Data already cancelled.");
                return true;
            }
            return false;
        }

        private MySqlCommand EditIMMPickingHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblIMMPickingHdr Set ");
            SQL.AppendLine("    CancelInd='Y', CancelReason=@CancelReason, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And DocSeqNo=@DocSeqNo And CancelInd='N'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", mDocNo);
            Sm.CmParam<String>(ref cm, "@DocSeqNo", mDocSeqNo);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveStock2()
        {
            var SQL = new StringBuilder();

            // decrease on bin's destination (header)
            SQL.AppendLine("Insert Into TblIMMStockMovement ");
            SQL.AppendLine("(DocType, DocNo, DocSeqNo, Source, DocDt, WhsCode, LocCode, Bin, ProdCode, Qty, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocTypePlusCancel, @DocNo, @DocSeqNo, B.Source, A.DocDt, A.WhsCode, C.LocCode, A.Bin, C.ProdCode, -1.00, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblIMMPickingHdr A ");
            SQL.AppendLine("Inner Join TblIMMPickingDtl B On A.DocNo=B.DocNo And A.DocSeqNo=B.DocSeqNo ");
            SQL.AppendLine("Inner Join TblIMMStockSummary C On B.Source=C.Source And A.WhsCode=C.WhsCode And A.Bin = C.Bin And C.Qty > 0.00 ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("And A.DocSeqNo=@DocSeqNo; ");

            SQL.AppendLine("Update TblIMMStockSummary A ");
            SQL.AppendLine("Inner Join TblIMMPickingHdr B ");
            SQL.AppendLine("    On B.DocNo=@DocNo ");
            SQL.AppendLine("    And B.DocSeqNo=@DocSeqNo ");
            SQL.AppendLine("    And A.WhsCode=B.WhsCode ");
            SQL.AppendLine("Inner Join TblIMMPickingDtl C ");
            SQL.AppendLine("    On B.DocNo=C.DocNo ");
            SQL.AppendLine("    And B.DocSeqNo=C.DocSeqNo ");
            SQL.AppendLine("    And A.Source=C.Source ");
            SQL.AppendLine("    And A.Bin = B.Bin ");
            SQL.AppendLine("Set A.Qty = 0.00 ");
            SQL.AppendLine("Where A.Qty > 0.00; ");

            // increase detail's item
            SQL.AppendLine("Insert Into TblIMMStockMovement ");
            SQL.AppendLine("(DocType, DocNo, DocSeqNo, Source, DocDt, WhsCode, LocCode, Bin, ProdCode, Qty, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocTypeMinCancel, @DocNo, @DocSeqNo, B.Source, A.DocDt, A.WhsCode, C.LocCode, B.Bin, C.ProdCode, 1.00, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblIMMPickingHdr A ");
            SQL.AppendLine("Inner Join TblIMMPickingDtl B On A.DocNo = B.DocNo And A.DocSeqNo = B.DocSeqNo ");
            SQL.AppendLine("Inner Join TblIMMStockSummary C On B.Source = C.Source And A.WhsCode = C.WhsCode And B.Bin = C.Bin ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("And A.DocSeqNo=@DocSeqNo; ");

            SQL.AppendLine("Insert Into TblIMMStockSummary ");
            SQL.AppendLine("(WhsCode, Bin, Source, ProdCode, SellerCode, LocCode, Qty, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.WhsCode, B.Bin, B.Source, C.ProdCode, C.SellerCode, C.LocCode, 1.00, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblIMMPickingHdr A ");
            SQL.AppendLine("Inner Join TblIMMPickingDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("    And A.DocSeqNo = B.DocSeqNo ");
            SQL.AppendLine("    And A.DocNo = @DocNo ");
            SQL.AppendLine("    And A.DocSeqNo = @DocSeqNo ");
            SQL.AppendLine("Inner Join TblIMMStockSummary C On A.WhsCode = C.WhsCode ");
            SQL.AppendLine("    And A.Bin = C.Bin ");
            SQL.AppendLine("    And B.Source = C.Source ");
            SQL.AppendLine("On Duplicate Key Update ");
            SQL.AppendLine("    Qty = 1.00, ");
            SQL.AppendLine("    LastUpBy = @UserCode, ");
            SQL.AppendLine("    LastUpDt = CurrentDateTime(); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", mDocNo);
            Sm.CmParam<Decimal>(ref cm, "@DocSeqNo", Decimal.Parse(mDocSeqNo));
            Sm.CmParam<String>(ref cm, "@DocTypePlusCancel", mDocTypePlusCancel);
            Sm.CmParam<String>(ref cm, "@DocTypeMinCancel", mDocTypeMinCancel);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo, string DocSeqNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                mDocNo = DocNo;
                mDocSeqNo = DocSeqNo;
                ShowIMMPickingHdr(DocNo, DocSeqNo);
                ShowIMMPickingDtl(DocNo, DocSeqNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowIMMPickingHdr(string DocNo, string DocSeqNo)
        {
            mDocNo = DocNo;
            mDocSeqNo = DocSeqNo;

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DocSeqNo", DocSeqNo);

            Sm.ShowDataInCtrl(
                ref cm,
                "Select DocDt, WhsCode, Bin, CancelReason, CancelInd From TblIMMPickingHdr Where DocNo=@DocNo And DocSeqNo=@DocSeqNo;",
                new string[] { "DocDt", "WhsCode", "Bin", "CancelReason", "CancelInd" },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = string.Concat(DocNo, DocSeqNo);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[0]));
                    Sm.SetLue(LueWhsCode, Sm.DrStr(dr, c[1]));
                    Sm.SetLue(LueBin, Sm.DrStr(dr, c[2]));
                    MeeCancelReason.Text = Sm.DrStr(dr, c[3]);
                    ChkCancelInd.Checked = Sm.DrStr(dr, c[4]) == "Y";
                }, true
            );
        }

        private void ShowIMMPickingDtl(string DocNo, string DocSeqNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select ");
            SQL.AppendLine("B.Bin, B.Source, C.ProdCode, D.ProdDesc, E.SellerName ");
            SQL.AppendLine("From TblIMMPickingHdr A ");
            SQL.AppendLine("Inner Join TblIMMPickingDtl B On A.DocNo=B.DocNo And A.DocSeqNo=B.DocSeqNo ");
            SQL.AppendLine("Inner Join TblIMMStockSummary C On A.WhsCode = C.WhsCode And B.Source=C.Source And A.Bin = C.Bin ");
            SQL.AppendLine("Inner Join TblIMMProduct D On C.ProdCode=D.ProdCode ");
            SQL.AppendLine("Inner Join TblIMMSeller E On C.SellerCode=E.SellerCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo And A.DocSeqNo=@DocSeqNo ");
            SQL.AppendLine("Order By A.DocNo, A.DocSeqNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DocSeqNo", DocSeqNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "Bin", 

                    //1-4
                    "Source", 
                    "ProdCode", 
                    "ProdDesc", 
                    "SellerName", 
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Grd.Cells[Row, 6].Value = 1m;
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 6 });
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mDocTitle = Sm.GetParameter("DocTitle");
            mDocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='IMMPicking';");
        }

        internal string GetSelectedSource()
        {
            var Source = string.Empty;

            for (int i = 0; i < Grd1.Rows.Count; ++i)
            {
                if (Sm.GetGrdStr(Grd1, i, 2).Length > 0)
                {
                    if (Source.Length > 0) Source += ",";
                    Source += Sm.GetGrdStr(Grd1, i, 2);
                }
            }

            return (Source.Length == 0) ? "XXX" : Source;
        }

        private void ShowBarcodeInfo()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.Bin, A.Source, C.ProdCode, C.ProdDesc, D.SellerName, A.Qty ");
            SQL.AppendLine("From TblIMMStockSummary A ");
            SQL.AppendLine("Inner Join TblWarehouse B On A.WhsCode=B.WhsCode ");
            SQL.AppendLine("Inner Join TblIMMProduct C On A.ProdCode=C.ProdCode ");
            SQL.AppendLine("Inner Join TblIMMSeller D On A.SellerCode=D.SellerCode ");
            SQL.AppendLine("Left Join TblIMMLocation E On A.LocCode=E.LocCode ");
            SQL.AppendLine("Where A.Source = @Source ");
            SQL.AppendLine("And A.Qty > 0.00 ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select 1 From TblIMMPickingDtl T1 ");
            SQL.AppendLine("    Inner Join TblIMMPickingHdr T2 On T1.DocNo = T2.DocNo ");
            SQL.AppendLine("        And T2.CancelInd='N' ");
            SQL.AppendLine("        And T1.Source = A.Source ");
            SQL.AppendLine("        And T1.PackingDocNo Is Null ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("And Exists( ");
            SQL.AppendLine("    Select 1 From TblGroupWarehouse ");
            SQL.AppendLine("    Where WhsCode = A.WhsCode ");
            SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode = @UserCode) ");
            SQL.AppendLine("    ) ");

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();
                string Filter = string.Empty, Source = string.Empty;

                if (Grd1.Rows.Count >= 1)
                {
                    for (int r = 0; r < Grd1.Rows.Count; r++)
                    {
                        Source = Sm.GetGrdStr(Grd1, r, 3);
                        if (Source.Length != 0)
                        {
                            if (Filter.Length > 0) Filter += " Or ";
                            Filter += " (A.Source=@Source0" + r.ToString() + " ) ";
                            Sm.CmParam<String>(ref cm, "@Source0" + r.ToString(), Source);
                        }
                    }
                }
                if (Filter.Length > 0)
                    Filter = " And Not (" + Filter + ") Limit 1;";
                else
                    Filter = " Limit 1;";
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@Source", TxtBarcode.Text);

                int Row = Grd1.Rows.Count - 1;
                Grd1.BeginUpdate();
                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandTimeout = 600;
                    cm.CommandText = SQL.ToString() + Filter;
                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] 
                    { 
                        //0
                        "Bin", 
                        //1-5
                        "Source", "ProdCode", "ProdDesc", "SellerName", "Qty", 
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Grd1.Rows.Add();
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 6, 5);
                            Row++;
                        }
                    }
                    else
                    {
                        Sm.StdMsg(mMsgType.Warning, "Invalid data.");
                        Console.Beep();
                    }
                    dr.Close();
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Grd1.EndUpdate();
                TxtBarcode.EditValue = null;
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue1(Sl.SetLueWhsCode));
                Sm.ClearGrd(Grd1, true);
            }
        }

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.MeeTrim(MeeCancelReason);
                ChkCancelInd.Checked = (MeeCancelReason.Text.Length > 0);
            }
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (ChkCancelInd.Checked)
                {
                    if (MeeCancelReason.Text.Length == 0)
                    {
                        Sm.StdMsg(mMsgType.Warning, "Reason for cancellation is empty.");
                        ChkCancelInd.Checked = false;
                    }
                }
                else
                    MeeCancelReason.EditValue = null;
            }
        }

        private void TxtBarcode_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Back) e.SuppressKeyPress = true;
        }

        private void TxtBarcode_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                mIsNew = true;
                mBarcode = string.Empty;
                ShowBarcodeInfo();
                TxtBarcode.Focus();
            }
            else
            {
                if (mIsNew)
                {
                    mBarcode = string.Empty;
                    TxtBarcode.Text = string.Empty;
                }
                mBarcode = mBarcode + e.KeyChar;
                mIsNew = false;
                TxtBarcode.Focus();
            }
        }

        #endregion

        #endregion
    }
}
