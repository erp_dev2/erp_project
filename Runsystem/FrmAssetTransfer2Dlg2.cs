﻿#region Update
/*
    17/11/2022 [IBL/BBT] New apps
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using System.Drawing;
using System.Text;

using TenTec.Windows.iGridLib;
using MySql.Data.MySqlClient;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmAssetTransfer2Dlg2 : RunSystem.FrmBase9
    {
        #region Field

        private FrmAssetTransfer2Dlg mFrmParent;
        private string mSQL = string.Empty, mDocNo = string.Empty, mMenuCode = string.Empty;

        #endregion

        #region Constructor

        public FrmAssetTransfer2Dlg2(FrmAssetTransfer2Dlg FrmParent, String DocNo )
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            //mMenuCode = MenuCode;
            mDocNo = DocNo;
        }

        #endregion

        #region Methods

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            SetFormControl(mState.View);
            SetGrd(); ShowData();
        }

        private void SetFormControl(RunSystem.mState state)
        {

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtRequestDocNo, TxtCCCodeFrom, TxtCCCodeTo, 
                    }, true);
                    TxtRequestDocNo.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtRequestDocNo, TxtCCCodeFrom, TxtCCCodeTo
            });
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 5;
            Grd1.FrozenArea.ColCount = 6;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",
                        
                        //1-5
                        "Asset Code",
                        "",
                        "Asset Name",
                        "Remark"
                    },
                     new int[] 
                    {
                        20, 
                        100, 20, 150, 100
                    }
                );
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 3, 4 });
            Sm.GrdColButton(Grd1, new int[] { 2 });
            Sm.GrdColInvisible(Grd1, new int[] { 0}, false);
        }

        private void HideInfoInGrd()
        {
            Sm.SetGrdAutoSize(Grd1);
        }

        #endregion

        #region Show data
        public void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Select B.DNo, B.AssetCode, C.AssetName, B.Remark ");
                SQL.AppendLine("From TblAssetTransferRequestHdr A ");
                SQL.AppendLine("Inner Join TblAssetTransferRequestDtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("Inner Join TblAsset C On B.AssetCode=C.AssetCode ");

                mSQL = SQL.ToString();

                string Filter = "Where A.DocNo= '" + mDocNo + "' ";

                var cm = new MySqlCommand();

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        SQL.ToString()+Filter,
                        new string[] 
                        { 
                            //0
                            "DNo", 

                            //1-5
                            "AssetCode", "AssetName", "Remark" 
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        }, false, false, true, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method
        private void Grd1_RequestEdit_1(object sender, iGRequestEditEventArgs e)
        {
            //if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            //{
            //    e.DoDefault = false;
            //    var f = new FrmAsset(mMenuCode);
            //    f.WindowState = FormWindowState.Normal;
            //    f.StartPosition = FormStartPosition.CenterScreen;
            //    f.mAssetCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
            //    f.ShowDialog();
            //}
        }

        private void Grd1_EllipsisButtonClick_1(object sender, iGEllipsisButtonClickEventArgs e)
        {
            //if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            //{
            //    var f = new FrmAsset(mMenuCode);
            //    f.WindowState = FormWindowState.Normal;
            //    f.StartPosition = FormStartPosition.CenterScreen;
            //    f.mAssetCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
            //    f.ShowDialog();
            //}
        }

        #endregion

       

        #endregion

        
    }
}
