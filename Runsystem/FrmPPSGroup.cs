﻿#region Update
/*
    19/02/2020 [HAR/IOK] Group PPS ketika update, payroll group jadi kosong
    17/01/2021 [TKG/PHT] ubah GenerateDocNo*
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

#endregion

namespace RunSystem
{
    public partial class FrmPPSGroup : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "";
        //internal FrmFind FrmFind;

        internal string
            mProposeCandidateDocNo = string.Empty,
            mProposeCandidateDNo = string.Empty,
            mEmpCodePPs = string.Empty;
        internal bool
            mIsNotFilterByAuthorization = false,
            mIsFilterByDeptHR = false,
            mIsFilterBySiteHR = false,
            mIsFilterByLevelHR = false
            ;
        private bool
            mIsPPSApprovalBasedOnDept = false,
            mIsAnnualLeaveUseStartDt = false,
            mIsPPSShowJobTransferByGrp = false;

        #endregion

        #region Constructor

        public FrmPPSGroup(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                Sl.SetLueDeptCode(ref LueDeptCode2);
                Sl.SetLueDeptCode(ref LueDeptCode);
                Sl.SetLuePosCode(ref LuePosCode);
                Sl.SetLueGrdLvlCode(ref LueGrdLvlCode);
                Sl.SetLuePayrollGrpCode(ref LuePGCode);
                Sl.SetLuePayrollGrpCode(ref LuePayrollGroup);
                if (mIsPPSShowJobTransferByGrp)
                    SetLueEmpJobTransfer(ref LueType);
                else
                    Sl.SetLueOption(ref LueType, "EmpJobTransfer");
                Sl.SetLueOption(ref LueEmploymentStatus, "EmploymentStatus");
                Sl.SetLueOption(ref LueSystemType, "EmpSystemType");
                Sl.SetLueOption(ref LuePayrunPeriod, "PayrunPeriod");

                SetFormControl(mState.View);

                base.FrmLoad(sender, e);
              
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 23;
            Grd1.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",
                        //1-5
                        "Employee"+Environment.NewLine+"Code",
                        "Employee"+Environment.NewLine+"Name",
                        "EntCode",
                        "Entity",
                        "SiteCode",
                        //6-10
                        "Site",
                        "DeptCode",
                        "Department",
                        "PosCode",
                        "Position",
                        //11-15
                        "GradeLevelCode",
                        "Grade"+Environment.NewLine+"Level",
                        "EmpStatusCode",
                        "Employement"+Environment.NewLine+"Status",
                        "EmpSystemCode",
                        //16-20
                        "System Type",
                        "PayrunPeriodCode",
                        "Payrun Period",
                        "PayrollTypeCode",
                        "Payroll Type",
                        //21-22
                        "PayrollGroupCode",
                        "Payroll Group"
                    },
                     new int[] 
                    {
                        20, 
                        100, 250, 80, 120, 80, 
                        80, 80, 120, 80, 120, 
                        80, 120, 80, 120, 80, 
                        120, 80, 120, 80, 120,
                        80, 120
                    }
                );
            Sm.GrdColInvisible(Grd1, new int[] { 0, 1, 3, 4, 5, 7, 9, 11, 13, 15, 17, 19, 20, 21 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9,
            10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22});
        }


        private void ChkHideInfoInGrd_CheckedChanged(object sender, EventArgs e)
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, !ChkHideInfoInGrd.Checked);
        }


        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                       DteStartDt, LueType, LuePayrollGroup, LueDeptCode2, MeeRemark, DteDocDt,
                       LueSiteCode, LueDeptCode, LuePosCode, LueGrdLvlCode, LueEmploymentStatus, 
                       LueSystemType, LuePayrunPeriod, LuePGCode
                    }, true);
                    Grd1.ReadOnly = true;
                    DteStartDt.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                       DteDocDt, DteStartDt, LueType, LuePayrollGroup, LueDeptCode2, MeeRemark,
                       LueSiteCode, LueDeptCode, LuePosCode, LueGrdLvlCode, LueEmploymentStatus, 
                       LueSystemType, LuePayrunPeriod, LuePGCode
                    }, false);
                    Grd1.ReadOnly = false;
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
               DteDocDt, DteStartDt, LueType, LuePayrollGroup, LueDeptCode2, MeeRemark,
               LueSiteCode, LueDeptCode, LuePosCode, LueGrdLvlCode, LueEmploymentStatus, 
               LueSystemType, LuePayrunPeriod, LuePGCode
            });
            ClearGrd();
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
           
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sm.SetDteCurrentDate(DteStartDt);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
               InsertData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            bool NeedApproval = IsDocApprovalSettingExisted();
            var cml = new List<MySqlCommand>();

            for (int Row = 0; Row < Grd1.Rows.Count-1; Row++)
            {
                string DocNo = GenerateDocNo((Row+1), Sm.GetDte(DteDocDt), "PPS", "TblPPS");
                cml.Add(SavePPS(Row, DocNo, NeedApproval, mProposeCandidateDocNo, mProposeCandidateDNo));
                if (mProposeCandidateDocNo.Length > 0 && mProposeCandidateDNo.Length > 0)
                    cml.Add(UpdateProposeCandidate(DocNo, mProposeCandidateDocNo, mProposeCandidateDNo));
            }

            if (NeedApproval == false)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Decimal.Parse(Sm.GetDte(DteStartDt).Substring(0, 8)) <= Decimal.Parse(Sm.GetValue("Select concat(replace(curdate(), '-', ''))")))
                    {
                        cml.Add(UpdateEmployee(Sm.GetGrdStr(Grd1, Row, 1), Row));
                    }
                }
            }
            Sm.ExecCommands(cml);
            Sm.StdMsg(mMsgType.Info, "PPS Process is completed");
            SetFormControl(mState.View);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsDteEmpty(DteStartDt, "Start Date") ||
                Sm.IsLueEmpty(LueType, "Job Transfer") ||
                Sm.IsLueEmpty(LuePayrollGroup, "Payroll Group") ||
                IsGrdEmpty()||
                StartDtValidated()||
                IsUpdateDataEmpty();
        }

        private bool StartDtValidated()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (IsStartDtNotValid(Sm.GetGrdStr(Grd1, Row, 1), Sm.GetGrdStr(Grd1, Row, 2)))
                return true;
            }
            return false;
        }

        private bool IsUpdateDataEmpty()
        {
            if (Sm.GetLue(LueDeptCode).Length ==0 &&
                Sm.GetLue(LueSiteCode).Length == 0 &&
                Sm.GetLue(LuePosCode).Length == 0 &&
                Sm.GetLue(LueGrdLvlCode).Length == 0 &&
                Sm.GetLue(LueEmploymentStatus).Length == 0 &&
                Sm.GetLue(LueSystemType).Length == 0 &&
                Sm.GetLue(LuePayrunPeriod).Length ==0 &&
                Sm.GetLue(LuePGCode).Length == 0
                )
            {
                Sm.StdMsg(mMsgType.Warning, "Data update is empty");
                return true;
            }
            return false;
        }

        private bool IsStartDtNotValid(string EmpCode, string EmpName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select StartDt From TblEmployeePPS ");
            SQL.AppendLine("Where StartDt>=@StartDt ");
            SQL.AppendLine("And EmpCode=@EmpCode ");
            SQL.AppendLine("Limit 1; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParam<String>(ref cm, "@EmpCode",EmpCode);

            var StartDt = Sm.GetValue(cm);
            if (StartDt.Length > 0)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Employee's Code : " + EmpCode + Environment.NewLine +
                    "Employee's Name : " + EmpName + Environment.NewLine + Environment.NewLine +
                    "Invalid start date. " + Environment.NewLine +
                    "Start date should not be earlier than /equal as " + Sm.DateValue(StartDt) + ".");
                return true;
            }
            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 employee.");
                return true;
            }
            return false;
        }

        private MySqlCommand SavePPS(int Row, string DocNo, bool NeedApproval, string ProposeCandidateDocNo, string ProposeCandidateDNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblPPS(DocNo, DocDt, CancelInd, Status, Jobtransfer, StartDt, EmpCode, ProposeCandidateDocNo, ProposeCandidateDNo, ");
            SQL.AppendLine("DeptCodeOld, PosCodeOld, GrdLvlCodeOld, EmploymentStatusOld, SystemTypeOld, PayrunPeriodOld, PGCodeOld, SiteCodeOld,  ");
            SQL.AppendLine("DeptCodeNew, PosCodeNew, GrdLvlCodeNew, EmploymentStatusNew, SystemTypeNew, PayrunPeriodNew, PGCodeNew, SiteCodeNew, UpdLeaveStartDtInd, Remark, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'N', 'O', @Jobtransfer, @StartDt, @EmpCode, @ProposeCandidateDocNo, @ProposeCandidateDNo, ");
            SQL.AppendLine("@DeptCodeOld, @PosCodeOld, @GrdLvlCodeOld, @EmploymentStatusOld, @SystemTypeOld, @PayrunPeriodOld, @PGCodeOld, @SiteCodeOld, ");
            SQL.AppendLine("@DeptCodeNew, @PosCodeNew, @GrdLvlCodeNew, @EmploymentStatusNew, @SystemTypeNew, @PayrunPeriodNew, @PGCodeNew, @SiteCodeNew, @UpdLeaveStartDtInd, @Remark, ");
            SQL.AppendLine("@CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select T.DocType, @DocNo, @Dno, T.DNo, @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblDocApprovalSetting T ");
            SQL.AppendLine("Where T.DocType='PPS' ");
            if (mIsPPSApprovalBasedOnDept)
                SQL.AppendLine("And T.DeptCode In (Select DeptCodeNew From TblPPS Where DocNo=@DocNo) ");
            SQL.AppendLine("; ");

            SQL.AppendLine("Update TblPPS Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select DocNo From TblDocApproval ");
            SQL.AppendLine("    Where DocType='PPS' ");
            SQL.AppendLine("    And DocNo=@DocNo ");
            SQL.AppendLine("    ); ");

            if (mIsAnnualLeaveUseStartDt && ChkUpdLeaveStartDtInd.Checked)
            {
                SQL.AppendLine("Update TblEmployee Set LeaveStartDt=@StartDt ");
                SQL.AppendLine("Where EmpCode=@EmpCode ");
                SQL.AppendLine("And Not Exists( ");
                SQL.AppendLine("    Select DocNo From TblDocApproval ");
                SQL.AppendLine("    Where DocType='PPS' ");
                SQL.AppendLine("    And DocNo=@DocNo ");
                SQL.AppendLine("    ); ");
            }


            if (NeedApproval == false) //kalau nggak butuh approval
            {
                SQL.AppendLine("Update TblEmployeePPS Set ");
                SQL.AppendLine("    EndDt=@EndDt ");
                SQL.AppendLine("Where EmpCode=@EmpCode ");
                SQL.AppendLine("And StartDt=( ");
                SQL.AppendLine("    Select StartDt From (");
                SQL.AppendLine("        Select Max(StartDt) StartDt ");
                SQL.AppendLine("        From TblEmployeePPS ");
                SQL.AppendLine("        Where EmpCode=@EmpCode ");
                SQL.AppendLine("        And StartDt<@StartDt ");
                SQL.AppendLine("    ) T );");

                SQL.AppendLine("Insert Into TblEmployeePPS(EmpCode, StartDt, EndDt, ");
                SQL.AppendLine("DeptCode, PosCode, GrdLvlCode, EmploymentStatus, SystemType, PayrunPeriod, PGCode, SiteCode, ");
                SQL.AppendLine("CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@EmpCode, @StartDt, Null, ");
                SQL.AppendLine("@DeptCodeNew, @PosCodeNew, @GrdLvlCodeNew, @EmploymentStatusNew, @SystemTypeNew, @PayrunPeriodNew, @PGCodeNew, @SiteCodeNew, ");
                SQL.AppendLine("@CreateBy, CurrentDateTime()); ");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@StartEmpCode", Sm.GetValue("Select Concat(StartDt,EmpCode) From TblEmployeepps Where EmpCode = '" + Sm.GetGrdStr(Grd1, Row, 1) + "' And " +
                "CreateDt in (Select max(CreateDt) From TblEmployeepps Where EmpCode = '" + Sm.GetGrdStr(Grd1, Row, 1) + "' )"));

            string CurrentDate = Sm.GetDte(DteStartDt);
            DateTime DateMin = Sm.ConvertDate(CurrentDate).AddDays(-1);
            Sm.CmParam<String>(ref cm, "@EndDt", DateMin.ToString("yyyyMMdd"));
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right(string.Concat("00", Row), 3));
            Sm.CmParam<String>(ref cm, "@ProposeCandidateDocNo", ProposeCandidateDocNo);
            Sm.CmParam<String>(ref cm, "@ProposeCandidateDNo", ProposeCandidateDNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParam<String>(ref cm, "@JobTransfer", Sm.GetLue(LueType));
            Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<String>(ref cm, "@DeptCodeOld", Sm.GetGrdStr(Grd1, Row, 7));
            Sm.CmParam<String>(ref cm, "@PosCodeOld", Sm.GetGrdStr(Grd1, Row, 9));
            Sm.CmParam<String>(ref cm, "@GrdLvlCodeOld", Sm.GetGrdStr(Grd1, Row, 11));
            Sm.CmParam<String>(ref cm, "@EmploymentStatusOld", Sm.GetGrdStr(Grd1, Row, 13));
            Sm.CmParam<String>(ref cm, "@SystemTypeOld", Sm.GetGrdStr(Grd1, Row, 15));
            Sm.CmParam<String>(ref cm, "@PayrunPeriodOld", Sm.GetGrdStr(Grd1, Row, 17));
            Sm.CmParam<String>(ref cm, "@PGCodeOld", Sm.GetGrdStr(Grd1, Row, 21));
            Sm.CmParam<String>(ref cm, "@SiteCodeOld", Sm.GetGrdStr(Grd1, Row, 5));
            Sm.CmParam<String>(ref cm, "@DeptCodeNew", Sm.GetLue(LueDeptCode).Length > 0 ? Sm.GetLue(LueDeptCode) : Sm.GetGrdStr(Grd1, Row, 7));
            Sm.CmParam<String>(ref cm, "@PosCodeNew", Sm.GetLue(LuePosCode).Length > 0 ? Sm.GetLue(LuePosCode) : Sm.GetGrdStr(Grd1, Row, 9));
            Sm.CmParam<String>(ref cm, "@GrdLvlCodeNew", Sm.GetLue(LueGrdLvlCode).Length > 0 ? Sm.GetLue(LueGrdLvlCode) : Sm.GetGrdStr(Grd1, Row, 11));
            Sm.CmParam<String>(ref cm, "@EmploymentStatusNew", Sm.GetLue(LueEmploymentStatus).Length > 0 ? Sm.GetLue(LueEmploymentStatus) : Sm.GetGrdStr(Grd1, Row, 13));
            Sm.CmParam<String>(ref cm, "@SystemTypeNew", Sm.GetLue(LueSystemType).Length > 0 ? Sm.GetLue(LueSystemType) : Sm.GetGrdStr(Grd1, Row, 15));
            Sm.CmParam<String>(ref cm, "@PayrunPeriodNew", Sm.GetLue(LuePayrunPeriod).Length > 0 ? Sm.GetLue(LuePayrunPeriod) : Sm.GetGrdStr(Grd1, Row, 17));
            Sm.CmParam<String>(ref cm, "@PGCodeNew", Sm.GetLue(LuePGCode).Length > 0 ? Sm.GetLue(LuePGCode) : Sm.GetGrdStr(Grd1, Row, 21));
            Sm.CmParam<String>(ref cm, "@SiteCodeNew", Sm.GetLue(LueSiteCode).Length > 0 ? Sm.GetLue(LueSiteCode) : Sm.GetGrdStr(Grd1, Row, 5));
            Sm.CmParam<String>(ref cm, "@UpdLeaveStartDtInd", ChkUpdLeaveStartDtInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand UpdateProposeCandidate(string PPSDocNo, string ProposeCandidateDocNo, string ProposeCandidateDNo)
        {
            var SQL = new StringBuilder();
            SQL.AppendLine("Update TblProposeCandidateDtl2 ");
            SQL.AppendLine("Set PPSDocNo = @PPSDocNo, LastUpBy = @CreateBy, LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("Where DocNo = @ProposeCandidateDocNo And DNo = @ProposeCandidateDNo ");
            SQL.AppendLine("And Exists ( ");
            SQL.AppendLine("  Select A.DocNo ");
            SQL.AppendLine("  From TblPPS A ");
            SQL.AppendLine("  Where A.DocNo = @PPSDocNo ");
            SQL.AppendLine("  And A.Status = 'A' ");
            SQL.AppendLine("); ");

            SQL.AppendLine("Update TblProposeCandidateHdr ");
            SQL.AppendLine("Set ProcessInd = 'F', LastUpBy = @CreateBy, LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("Where DocNo = @ProposeCandidateDocNo ");
            SQL.AppendLine("And Exists ( ");
            SQL.AppendLine("  Select A.DocNo ");
            SQL.AppendLine("  From TblPPS A ");
            SQL.AppendLine("  Where A.DocNo = @PPSDocNo ");
            SQL.AppendLine("  And A.Status = 'A' ");
            SQL.AppendLine(") ");
            SQL.AppendLine("And Not Exists ( ");
            SQL.AppendLine("  Select T.DocNo ");
            SQL.AppendLine("  From TblProposeCandidateDtl2 T ");
            SQL.AppendLine("  Where T.DocNo = @ProposeCandidateDocNo ");
            SQL.AppendLine("  And T.PPSDocNo Is Null ");
            SQL.AppendLine("); ");


            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@PPSDocNo", PPSDocNo);
            Sm.CmParam<String>(ref cm, "@ProposeCandidateDocNo", ProposeCandidateDocNo);
            Sm.CmParam<String>(ref cm, "@ProposeCandidateDNo", ProposeCandidateDNo);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand UpdateEmployee(string EmpCode, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblEmployee Set ");
            SQL.AppendLine("    DeptCode=@DeptCode, ");
            SQL.AppendLine("    PosCode=@PosCode, ");
            SQL.AppendLine("    GrdLvlCode=@GrdLvlCode, ");
            SQL.AppendLine("    EmploymentStatus=@EmploymentStatus, ");
            SQL.AppendLine("    SystemType=@SystemType, ");
            SQL.AppendLine("    PayrunPeriod=@PayrunPeriod, ");
            SQL.AppendLine("    PGCode=@PGCode, ");
            SQL.AppendLine("    SiteCode=@SiteCode, ");
            SQL.AppendLine("    EntCode=( ");
            SQL.AppendLine("        Select T2.EntCode ");
            SQL.AppendLine("        From TblSite T1 ");
            SQL.AppendLine("        Inner Join TblProfitCenter T2 On T1.ProfitCenterCode=T2.ProfitCenterCode ");
            SQL.AppendLine("        Where T1.SiteCode=@SiteCode ");
            SQL.AppendLine("    ), ");
            SQL.AppendLine("    LastUpBy=@UserCode, ");
            SQL.AppendLine("    LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where EmpCode=@EmpCode; ");

            SQL.AppendLine("Update TblEmployeePPS P Set P.ProcessInd = 'Y' ");
            SQL.AppendLine("Where P.EmpCode = @EmpCode And P.ProcessInd = 'N' ");
            SQL.AppendLine("And P.EndDt Is Null; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode).Length > 0 ? Sm.GetLue(LueDeptCode) : Sm.GetGrdStr(Grd1, Row, 7));
            Sm.CmParam<String>(ref cm, "@PosCode", Sm.GetLue(LuePosCode).Length > 0 ? Sm.GetLue(LuePosCode) : Sm.GetGrdStr(Grd1, Row, 9));
            Sm.CmParam<String>(ref cm, "@GrdLvlCode", Sm.GetLue(LueGrdLvlCode).Length > 0 ? Sm.GetLue(LueGrdLvlCode) : Sm.GetGrdStr(Grd1, Row, 11));
            Sm.CmParam<String>(ref cm, "@EmploymentStatus", Sm.GetLue(LueEmploymentStatus).Length > 0 ? Sm.GetLue(LueEmploymentStatus) : Sm.GetGrdStr(Grd1, Row, 13));
            Sm.CmParam<String>(ref cm, "@SystemType", Sm.GetLue(LueSystemType).Length > 0 ? Sm.GetLue(LueSystemType) : Sm.GetGrdStr(Grd1, Row, 15));
            Sm.CmParam<String>(ref cm, "@PayrunPeriod", Sm.GetLue(LuePayrunPeriod).Length > 0 ? Sm.GetLue(LuePayrunPeriod) : Sm.GetGrdStr(Grd1, Row, 17));
            Sm.CmParam<String>(ref cm, "@PGCode", Sm.GetLue(LuePGCode).Length > 0 ? Sm.GetLue(LuePGCode) : Sm.GetGrdStr(Grd1, Row, 21));
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode).Length > 0 ? Sm.GetLue(LueSiteCode) : Sm.GetGrdStr(Grd1, Row, 5));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region Show data
        private void ShowDataEmployee(string PGCode, string DeptCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.EmpCode, A.Empname, A.DeptCode, B.Deptname, A.PosCode, C.PosName,  ");
            SQL.AppendLine("A.Employmentstatus As EmploymentstatusCode, D.OptDesc As Employmentstatus, A.SiteCode,   ");
            SQL.AppendLine("E.Sitename, A.EntCode, F.EntName,  A.GrdLvlCode, G.grdLvlName, A.PGCode, H.PGName,   ");
            SQL.AppendLine("A.Systemtype As SystemTypeCode, I.OptDesc As SystemType, A.payrollType As PayrollTypeCode,   ");
            SQL.AppendLine("J.OptDesc As payrollType, A.payRunperiod As PayrunperiodCode, K.Optdesc As Payrunperiod   ");
            SQL.AppendLine("From tblemployee A  ");
            SQL.AppendLine("Inner Join TblDepartment B On A.deptCode  = b.DeptCode  ");
            SQL.AppendLine("left Join TblPosition C on A.posCode  = C.posCode  ");
            SQL.AppendLine("Left Join TblOption D On A.EmploymentStatus = D.OptCode And D.optCat = 'EmploymentStatus'  ");
            SQL.AppendLine("Left Join TblSite E On A.SiteCode = E.SiteCode  ");
            SQL.AppendLine("left join TblEntity F on A.EntCode = F.EntCode  ");
            SQL.AppendLine("Left Join Tblgradelevelhdr G On A.GrdLvlCode =  G.GrdLvlCode  ");
            SQL.AppendLine("left Join Tblpayrollgrphdr H On A.PGCode = H.PGCode  ");
            SQL.AppendLine("Left join tblOption I On A.SystemType = I.optCode And I.OptCat = 'EmpSystemType'  ");
            SQL.AppendLine("Left join tblOption J On A.payrollType = J.optCode And J.OptCat = 'EmployeePayrollType'  ");
            SQL.AppendLine("Left join tblOption K On A.payrunPeriod = K.OptCode And K.OptCat = 'PayrunPeriod'  ");
            SQL.AppendLine("Left Join TblEmployeePPS L On A.EmpCode=L.EmpCode And L.EndDt Is Null ");
            if (mIsFilterByDeptHR)
            {
                SQL.AppendLine("And L.DeptCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupDepartment ");
                SQL.AppendLine("    Where DeptCode=IfNull(L.DeptCode, '') ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            if (mIsFilterBySiteHR)
            {
                SQL.AppendLine("And L.SiteCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupSite ");
                SQL.AppendLine("    Where SiteCode=IfNull(L.SiteCode, '') ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("Where ((A.ResignDt Is Not Null And A.ResignDt>=(Select concat(replace(curdate(), '-', '')))) Or A.ResignDt Is Null) ");
            SQL.AppendLine("And A.PGCode=@PGCode ");
            if (!mIsNotFilterByAuthorization)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblEmployee ");
                SQL.AppendLine("    Where EmpCode=A.EmpCode ");
                SQL.AppendLine("    And GrdLvlCode In ( ");
                SQL.AppendLine("        Select T2.GrdLvlCode ");
                SQL.AppendLine("        From TblPPAHdr T1 ");
                SQL.AppendLine("        Inner Join TblPPADtl T2 On T1.DocNo=T2.DocNo ");
                SQL.AppendLine("        Where T1.ActInd='Y' And T1.UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            if (mIsFilterByDeptHR)
            {
                SQL.AppendLine("And A.DeptCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupDepartment ");
                SQL.AppendLine("    Where DeptCode=IfNull(A.DeptCode, '') ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            if (mIsFilterBySiteHR)
            {
                SQL.AppendLine("And A.SiteCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupSite ");
                SQL.AppendLine("    Where SiteCode=IfNull(A.SiteCode, '') ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            if (mIsFilterByLevelHR)
            {
                SQL.AppendLine("And A.GrdLvlCode Is Not Null ");
                SQL.AppendLine("And A.GrdLvlCode In ( ");
                SQL.AppendLine("    Select X.GrdLvlCode From TblGradeLevelHdr X ");
                SQL.AppendLine("    Where X.LevelCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupDepartment ");
                SQL.AppendLine("        Where LevelCode=X.LevelCode ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            if (DeptCode.Length > 0)
                SQL.AppendLine("And A.DeptCode=@DeptCode ");
            

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@PGCode", PGCode);
            Sm.CmParam<String>(ref cm, "@DeptCode", DeptCode);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "EmpCode", 
                    //1-5
                    "Empname", "EntCode", "EntName", "SiteCode", "Sitename", 
                    //6-10
                    "DeptCode", "Deptname", "PosCode", "PosName", "GrdLvlCode",    
                    //11-15
                    "grdLvlName", "EmploymentstatusCode", "Employmentstatus",  "SystemTypeCode", "SystemType", 
                    //16-20
                    "PayrunperiodCode",  "Payrunperiod", "PayrollTypeCode", "payrollType","PGCode", 
                    //21
                   "PGName", 
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Grd1.Cells[Row, 0].Value = Row;
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 9, 8);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 10, 9);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 11, 10);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 12, 11);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 13, 12);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 14, 13);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 15, 14);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 16, 15);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 17, 16);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 18, 17);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 19, 18);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 20, 19);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 21, 20);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 22, 21);
                }, true, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mIsNotFilterByAuthorization = Sm.GetParameter("IsPayrollDataFilterByAuthorization") == "N";
            mIsFilterByDeptHR = Sm.GetParameterBoo("IsFilterByDeptHR");
            mIsFilterBySiteHR = Sm.GetParameterBoo("IsFilterBySiteHR");
            mIsFilterByLevelHR = Sm.GetParameterBoo("IsFilterByLevelHR");
            mIsPPSApprovalBasedOnDept = Sm.GetParameterBoo("IsPPSApprovalBasedOnDept");
            mIsAnnualLeaveUseStartDt = Sm.GetParameterBoo("IsAnnualLeaveUseStartDt");
            mIsPPSShowJobTransferByGrp = Sm.GetParameterBoo("IsPPSShowJobTransferByGrp");
            mEmpCodePPs = Sm.GetParameter("EmpCodePPs");
        }

        private static string GenerateDocNo(int Row, string DocDt, string DocType, string Tbl)
        {
            var SQL = new StringBuilder();
            bool IsDocSeqNoEnabled = Sm.GetParameterBoo("IsDocSeqNoEnabled");
            string
                Yr = DocDt.Substring(2, 2),
                Mth = DocDt.Substring(4, 2),
                DocTitle = Sm.GetParameter("DocTitle"),
                DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='" + DocType + "'"),
                DocSeqNo = "4";
            if (IsDocSeqNoEnabled) DocSeqNo = Sm.GetParameter("DocSeqNo");
            
            SQL.Append("Select Concat( ");
            SQL.Append("IfNull(( ");
            SQL.Append("   Select Right(Concat(Repeat('0', " + DocSeqNo + "), Convert(DocNo+" + Row + ", Char)), " + DocSeqNo + ") From ( ");
            SQL.Append("       Select Convert(Left(DocNo, Locate('/', DocNo)-1), Decimal) As DocNo ");
            SQL.Append("       From " + Tbl);
            //SQL.Append("   Select Right(Concat('0000', Convert(DocNo+" + Row + ", Char)), 4) From ( ");
            //SQL.Append("       Select Convert(Left(DocNo, 4), Decimal) As DocNo From " + Tbl);
            SQL.Append("       Where Right(DocNo, 5)=Concat('" + Mth + "','/', '" + Yr + "') ");
            SQL.Append("       Order By Left(DocNo, "+DocSeqNo+") Desc Limit 1 ");
            SQL.Append("       ) As Temp ");
            SQL.Append("   ), Right(Concat(Repeat('0', " + DocSeqNo + "), '" + Row.ToString() + "'), " + DocSeqNo + ") ");
            //SQL.Append("   ), '" + Sm.Right(String.Concat("0000", Row.ToString()), 4) + "' "); 
            SQL.Append("), '/', '" + ((DocTitle.Length == 0) ? "XXX" : DocTitle));
            SQL.Append("', '/', '" + DocAbbr + "', '/', '" + Mth + "','/', '" + Yr + "'");
            SQL.Append(") As DocNo");

            return Sm.GetValue(SQL.ToString());
        }


        private bool IsDocApprovalSettingExisted()
        {
            return Sm.IsDataExist(
                "Select 1 From TblDocApprovalSetting Where DocType = 'PPS' " +
                (mIsPPSApprovalBasedOnDept ? " And DeptCode Is Not Null " : string.Empty) +
                "Limit 1; "
                );
        }

        private void SetLueEmpJobTransfer(ref LookUpEdit Lue)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select A.JobCode As Col1, B.OptDesc As Col2 from TblGroupJobTransfer A ");
                SQL.AppendLine("Inner Join TblOption B On B.OptCat='empjobtransfer' and A.JobCode=B.OptCode ");
                SQL.AppendLine("Where A.GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode)");
                SQL.AppendLine("Order By B.OptDesc; ");
                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.SetLue2(
                    ref Lue, ref cm,
                    0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }

        }

        private void SetLueGrdLvlCode(ref DXE.LookUpEdit Lue, string GrdLvlCode)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select T.GrdLvlCode As Col1, T.GrdLvlName As Col2 ");
                SQL.AppendLine("From TblGradeLevelHdr T ");
                if (GrdLvlCode.Length != 0)
                    SQL.AppendLine("Where T.GrdLvlCode='" + GrdLvlCode + "' ");
                else
                {
                    if (!mIsNotFilterByAuthorization)
                    {
                        SQL.AppendLine("Where GrdLvlCode In ( ");
                        SQL.AppendLine("    Select T2.GrdLvlCode ");
                        SQL.AppendLine("    From TblPPAHdr T1 ");
                        SQL.AppendLine("    Inner Join TblPPADtl T2 On T1.DocNo=T2.DocNo ");
                        SQL.AppendLine("    Where T1.ActInd='Y' And T1.UserCode=@UserCode ");
                        SQL.AppendLine(") ");
                    }
                }
                SQL.AppendLine("Order By GrdLvlName; ");
                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.SetLue2(
                    ref Lue, ref cm,
                    0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }


        #endregion

        #endregion

        #region Event

        private void BtnRefresh_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (!Sm.IsLueEmpty(LuePayrollGroup, "Payroll Group"))
                {
                    Sm.ClearGrd(Grd1, true);
                    ShowDataEmployee(Sm.GetLue(LuePayrollGroup), Sm.GetLue(LueDeptCode2));
                }
            }
        }


        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue3(Sl.SetLueSiteCode), string.Empty, mIsFilterBySiteHR ? "Y" : "N");
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, mIsFilterByDeptHR ? "Y" : "N");
        }

        private void LuePosCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePosCode, new Sm.RefreshLue1(Sl.SetLuePosCode));
        }

        private void LueGrdLvlCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueGrdLvlCode, new Sm.RefreshLue2(SetLueGrdLvlCode), string.Empty);
        }

        private void LueEmploymentStatus_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueEmploymentStatus, new Sm.RefreshLue2(Sl.SetLueOption), "EmploymentStatus");
        }

        private void LueSystemType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSystemType, new Sm.RefreshLue2(Sl.SetLueOption), "EmpSystemType");
        }

        private void LuePayrunPeriod_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePayrunPeriod, new Sm.RefreshLue2(Sl.SetLueOption), "PayrunPeriod");
        }

        private void LuePGCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePGCode, new Sm.RefreshLue1(Sl.SetLuePayrollGrpCode));
        }

        private void LueType_EditValueChanged(object sender, EventArgs e)
        {
            if (mIsPPSShowJobTransferByGrp)
                Sm.RefreshLookUpEdit(LueType, new Sm.RefreshLue1(SetLueEmpJobTransfer));
            else
                Sm.RefreshLookUpEdit(LueType, new Sm.RefreshLue2(Sl.SetLueOption), "EmpJobTransfer");
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
        }
        #endregion

       
    }
}
