﻿#region Update
/*
   12/12/2019 [HAR/IMS] 
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmVoucherRequestIncentiveDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmVoucherRequestIncentive mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmVoucherRequestIncentiveDlg(FrmVoucherRequestIncentive FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mFrmParent.mIsFilterByDeptHR ? "Y" : "N");
                Sl.SetLueSiteCode(ref LueSiteCode, string.Empty, mFrmParent.mIsFilterBySiteHR ? "Y" : "N");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.Yr, A.Mth, B.DeptName, SUM(C.TotalAmt) As Amt ");
            SQL.AppendLine("From TblEmpIncentiveHdr A  ");
            SQL.AppendLine("Inner Join TblDepartment B On A.DeptCode=B.DeptCode  ");
            SQL.AppendLine("Inner Join TblEmpIncentiveDtl C On A.DoCNo = C.DocNo ");
            SQL.AppendLine("Where A.CancelInd='N'  ");
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 8;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "",
                        "Document"+Environment.NewLine+"Number",
                        "Document"+Environment.NewLine+"Date",
                        "Year", 
                        "Month",  
                        //6
                        "Department",
                        "Amount"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        20, 150, 100, 80, 150, 

                        //6-10
                        180, 120
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdFormatDate(Grd1, new int[] { 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 7 }, 0);
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 3, 4, 5, 6, 7 }, true);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = string.Empty;

                var cm = new MySqlCommand();

               
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtPayrunName.Text, new string[] { "A.PayrunCode", "A.PayrunName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "A.DeptCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "A.SiteCode", true);

                Sm.ShowDataInGrid(
                       ref Grd1, ref cm,
                       mSQL + Filter + " Group By A.DocNo, A.DocDt, A.Yr, A.Mth, B.DeptName ;",
                       new string[] 
                        { 
                             //0
                             "DocNo",
                             
                             //1-5
                             "DocDt",
                             "Yr", 
                             "Mth",
                             "DeptName", 
                             "Amt",
                         },
                       (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                       {
                           Grd.Cells[Row, 0].Value = Row + 1;
                           Grd.Cells[Row, 1].Value = false;
                           Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                           Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 1);
                           Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                           Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                           Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                           Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 5);
                       }, true, false, false, false
                   );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsPayrunCodeAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd3.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 1, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 3, Grd1, Row2, 3);
                        Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 4, Grd1, Row2, 4);
                        Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 5, Grd1, Row2, 5);
                        Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 6, Grd1, Row2, 6);
                        Sm.CopyGrdValue(mFrmParent.Grd3, Row1, 7, Grd1, Row2, 7);

                        mFrmParent.Grd3.Rows.Add();
                        Sm.SetGrdNumValueZero(mFrmParent.Grd3, mFrmParent.Grd3.Rows.Count - 1, new int[] { 7 });
                    }
                }
            }

            if (!IsChoose) 
                Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 payrun.");
            else
                mFrmParent.ComputeIncentiveInfo();
        }

        private bool IsPayrunCodeAlreadyChosen(int Row)
        {
            var DocNo = Sm.GetGrdStr(Grd1, Row, 2);
            for (int Index = 0; Index < mFrmParent.Grd3.Rows.Count - 1; Index++)
                if (Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd3, Index, 1), DocNo)) return true;
            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmEmpIncentive(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                //f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmEmpIncentive(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                //f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int r = 0; r < Grd1.Rows.Count; r++)
                    if (Sm.GetGrdStr(Grd1, r, 2).Length > 0)
                        Grd1.Cells[r, 1].Value = !IsSelected;
            }
        }


        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtPayrunName_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkPayrunName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Payrun");
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, mFrmParent.mIsFilterByDeptHR ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue3(Sl.SetLueSiteCode), string.Empty, mFrmParent.mIsFilterBySiteHR ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }

        #endregion

        #endregion
    }
}
