namespace RunSystem
{
    partial class FrmPPS
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmPPS));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.TpgGeneral = new System.Windows.Forms.TabPage();
            this.TxtLevelCode = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtSectionCodeOld = new DevExpress.XtraEditors.TextEdit();
            this.LblSectionOld = new System.Windows.Forms.Label();
            this.ChkActingOfficialOld = new DevExpress.XtraEditors.CheckEdit();
            this.TxtPositionStatusCode = new DevExpress.XtraEditors.TextEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.ChkUpdLeaveStartDtInd = new DevExpress.XtraEditors.CheckEdit();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.DteResignDtOld = new DevExpress.XtraEditors.DateEdit();
            this.label22 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.TxtSiteCode = new DevExpress.XtraEditors.TextEdit();
            this.label18 = new System.Windows.Forms.Label();
            this.TxtPGCode = new DevExpress.XtraEditors.TextEdit();
            this.ChkCancelInd = new DevExpress.XtraEditors.CheckEdit();
            this.DteStartDt = new DevExpress.XtraEditors.DateEdit();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.TxtPayrunPeriod = new DevExpress.XtraEditors.TextEdit();
            this.label14 = new System.Windows.Forms.Label();
            this.TxtSystemType = new DevExpress.XtraEditors.TextEdit();
            this.label13 = new System.Windows.Forms.Label();
            this.TxtEmploymentStatus = new DevExpress.XtraEditors.TextEdit();
            this.TxtPosition = new DevExpress.XtraEditors.TextEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtDeptName = new DevExpress.XtraEditors.TextEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.TxtGrade = new DevExpress.XtraEditors.TextEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.LueType = new DevExpress.XtraEditors.LookUpEdit();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label11 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.BtnLogWorkPeriod = new DevExpress.XtraEditors.SimpleButton();
            this.TxtWorkPeriodMth = new DevExpress.XtraEditors.TextEdit();
            this.TxtWorkPeriodYr = new DevExpress.XtraEditors.TextEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.LueLevelCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label15 = new System.Windows.Forms.Label();
            this.LblSectionNew = new System.Windows.Forms.Label();
            this.LueSectionCode = new DevExpress.XtraEditors.LookUpEdit();
            this.ChkActingOfficialNew = new DevExpress.XtraEditors.CheckEdit();
            this.LuePositionStatusCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label19 = new System.Windows.Forms.Label();
            this.BtnEmployeeRequestDocNo2 = new DevExpress.XtraEditors.SimpleButton();
            this.DteResignDtNew = new DevExpress.XtraEditors.DateEdit();
            this.BtnEmployeeRequestDocNo = new DevExpress.XtraEditors.SimpleButton();
            this.label23 = new System.Windows.Forms.Label();
            this.TxtEmployeeRequestDocNo = new DevExpress.XtraEditors.TextEdit();
            this.LueSiteCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label21 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.LuePGCode = new DevExpress.XtraEditors.LookUpEdit();
            this.LuePosCode = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtEntityName = new DevExpress.XtraEditors.TextEdit();
            this.label49 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.LueDeptCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label51 = new System.Windows.Forms.Label();
            this.LuePayrunPeriod = new DevExpress.XtraEditors.LookUpEdit();
            this.LueGrdLvlCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label52 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.LueEmploymentStatus = new DevExpress.XtraEditors.LookUpEdit();
            this.label55 = new System.Windows.Forms.Label();
            this.LueSystemType = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtEmpCode = new DevExpress.XtraEditors.TextEdit();
            this.label56 = new System.Windows.Forms.Label();
            this.BtnEmpCode = new DevExpress.XtraEditors.SimpleButton();
            this.label57 = new System.Windows.Forms.Label();
            this.TxtEmpName = new DevExpress.XtraEditors.TextEdit();
            this.BtnEmpCode2 = new DevExpress.XtraEditors.SimpleButton();
            this.TpgFile = new System.Windows.Forms.TabPage();
            this.label1 = new System.Windows.Forms.Label();
            this.PbUpload3 = new System.Windows.Forms.ProgressBar();
            this.PbUpload2 = new System.Windows.Forms.ProgressBar();
            this.ChkFile3 = new DevExpress.XtraEditors.CheckEdit();
            this.TxtFile3 = new DevExpress.XtraEditors.TextEdit();
            this.BtnFile3 = new DevExpress.XtraEditors.SimpleButton();
            this.ChkFile2 = new DevExpress.XtraEditors.CheckEdit();
            this.TxtFile2 = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.BtnFile2 = new DevExpress.XtraEditors.SimpleButton();
            this.ChkFile = new DevExpress.XtraEditors.CheckEdit();
            this.PbUpload = new System.Windows.Forms.ProgressBar();
            this.TxtFile = new DevExpress.XtraEditors.TextEdit();
            this.label25 = new System.Windows.Forms.Label();
            this.BtnFile = new DevExpress.XtraEditors.SimpleButton();
            this.OD = new System.Windows.Forms.OpenFileDialog();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.TpgGeneral.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLevelCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSectionCodeOld.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActingOfficialOld.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPositionStatusCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkUpdLeaveStartDtInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteResignDtOld.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteResignDtOld.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSiteCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPGCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPayrunPeriod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSystemType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmploymentStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPosition.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDeptName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGrade.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtWorkPeriodMth.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtWorkPeriodYr.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueLevelCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSectionCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActingOfficialNew.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePositionStatusCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteResignDtNew.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteResignDtNew.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmployeeRequestDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSiteCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePGCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePosCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEntityName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDeptCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePayrunPeriod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueGrdLvlCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueEmploymentStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSystemType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpName.Properties)).BeginInit();
            this.TpgFile.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(915, 0);
            this.panel1.Size = new System.Drawing.Size(70, 423);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.tabControl1);
            this.panel2.Size = new System.Drawing.Size(915, 423);
            // 
            // tabControl1
            // 
            this.tabControl1.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.tabControl1.Controls.Add(this.TpgGeneral);
            this.tabControl1.Controls.Add(this.TpgFile);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Multiline = true;
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.ShowToolTips = true;
            this.tabControl1.Size = new System.Drawing.Size(915, 423);
            this.tabControl1.TabIndex = 22;
            // 
            // TpgGeneral
            // 
            this.TpgGeneral.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.TpgGeneral.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.TpgGeneral.Controls.Add(this.TxtLevelCode);
            this.TpgGeneral.Controls.Add(this.label3);
            this.TpgGeneral.Controls.Add(this.TxtSectionCodeOld);
            this.TpgGeneral.Controls.Add(this.LblSectionOld);
            this.TpgGeneral.Controls.Add(this.ChkActingOfficialOld);
            this.TpgGeneral.Controls.Add(this.TxtPositionStatusCode);
            this.TpgGeneral.Controls.Add(this.label4);
            this.TpgGeneral.Controls.Add(this.ChkUpdLeaveStartDtInd);
            this.TpgGeneral.Controls.Add(this.MeeRemark);
            this.TpgGeneral.Controls.Add(this.label5);
            this.TpgGeneral.Controls.Add(this.DteResignDtOld);
            this.TpgGeneral.Controls.Add(this.label22);
            this.TpgGeneral.Controls.Add(this.label20);
            this.TpgGeneral.Controls.Add(this.TxtSiteCode);
            this.TpgGeneral.Controls.Add(this.label18);
            this.TpgGeneral.Controls.Add(this.TxtPGCode);
            this.TpgGeneral.Controls.Add(this.ChkCancelInd);
            this.TpgGeneral.Controls.Add(this.DteStartDt);
            this.TpgGeneral.Controls.Add(this.label17);
            this.TpgGeneral.Controls.Add(this.label16);
            this.TpgGeneral.Controls.Add(this.TxtPayrunPeriod);
            this.TpgGeneral.Controls.Add(this.label14);
            this.TpgGeneral.Controls.Add(this.TxtSystemType);
            this.TpgGeneral.Controls.Add(this.label13);
            this.TpgGeneral.Controls.Add(this.TxtEmploymentStatus);
            this.TpgGeneral.Controls.Add(this.TxtPosition);
            this.TpgGeneral.Controls.Add(this.label6);
            this.TpgGeneral.Controls.Add(this.TxtDeptName);
            this.TpgGeneral.Controls.Add(this.label7);
            this.TpgGeneral.Controls.Add(this.label8);
            this.TpgGeneral.Controls.Add(this.TxtGrade);
            this.TpgGeneral.Controls.Add(this.label9);
            this.TpgGeneral.Controls.Add(this.LueType);
            this.TpgGeneral.Controls.Add(this.DteDocDt);
            this.TpgGeneral.Controls.Add(this.label10);
            this.TpgGeneral.Controls.Add(this.TxtDocNo);
            this.TpgGeneral.Controls.Add(this.label11);
            this.TpgGeneral.Controls.Add(this.panel5);
            this.TpgGeneral.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TpgGeneral.Location = new System.Drawing.Point(4, 26);
            this.TpgGeneral.Name = "TpgGeneral";
            this.TpgGeneral.Size = new System.Drawing.Size(907, 393);
            this.TpgGeneral.TabIndex = 0;
            this.TpgGeneral.Text = "General";
            this.TpgGeneral.UseVisualStyleBackColor = true;
            // 
            // TxtLevelCode
            // 
            this.TxtLevelCode.EnterMoveNextControl = true;
            this.TxtLevelCode.Location = new System.Drawing.Point(128, 320);
            this.TxtLevelCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLevelCode.Name = "TxtLevelCode";
            this.TxtLevelCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtLevelCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLevelCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLevelCode.Properties.Appearance.Options.UseFont = true;
            this.TxtLevelCode.Properties.MaxLength = 80;
            this.TxtLevelCode.Properties.ReadOnly = true;
            this.TxtLevelCode.Size = new System.Drawing.Size(342, 20);
            this.TxtLevelCode.TabIndex = 92;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(88, 323);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 14);
            this.label3.TabIndex = 91;
            this.label3.Text = "Level";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSectionCodeOld
            // 
            this.TxtSectionCodeOld.EnterMoveNextControl = true;
            this.TxtSectionCodeOld.Location = new System.Drawing.Point(128, 110);
            this.TxtSectionCodeOld.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSectionCodeOld.Name = "TxtSectionCodeOld";
            this.TxtSectionCodeOld.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtSectionCodeOld.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSectionCodeOld.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSectionCodeOld.Properties.Appearance.Options.UseFont = true;
            this.TxtSectionCodeOld.Properties.MaxLength = 80;
            this.TxtSectionCodeOld.Properties.ReadOnly = true;
            this.TxtSectionCodeOld.Size = new System.Drawing.Size(342, 20);
            this.TxtSectionCodeOld.TabIndex = 71;
            // 
            // LblSectionOld
            // 
            this.LblSectionOld.AutoSize = true;
            this.LblSectionOld.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblSectionOld.ForeColor = System.Drawing.Color.Black;
            this.LblSectionOld.Location = new System.Drawing.Point(75, 112);
            this.LblSectionOld.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblSectionOld.Name = "LblSectionOld";
            this.LblSectionOld.Size = new System.Drawing.Size(48, 14);
            this.LblSectionOld.TabIndex = 70;
            this.LblSectionOld.Text = "Section";
            this.LblSectionOld.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkActingOfficialOld
            // 
            this.ChkActingOfficialOld.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkActingOfficialOld.Location = new System.Drawing.Point(325, 172);
            this.ChkActingOfficialOld.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkActingOfficialOld.Name = "ChkActingOfficialOld";
            this.ChkActingOfficialOld.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkActingOfficialOld.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkActingOfficialOld.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkActingOfficialOld.Properties.Appearance.Options.UseBackColor = true;
            this.ChkActingOfficialOld.Properties.Appearance.Options.UseFont = true;
            this.ChkActingOfficialOld.Properties.Appearance.Options.UseForeColor = true;
            this.ChkActingOfficialOld.Properties.Caption = "Acting Official (PLT)";
            this.ChkActingOfficialOld.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkActingOfficialOld.ShowToolTips = false;
            this.ChkActingOfficialOld.Size = new System.Drawing.Size(140, 22);
            this.ChkActingOfficialOld.TabIndex = 78;
            // 
            // TxtPositionStatusCode
            // 
            this.TxtPositionStatusCode.EnterMoveNextControl = true;
            this.TxtPositionStatusCode.Location = new System.Drawing.Point(128, 299);
            this.TxtPositionStatusCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPositionStatusCode.Name = "TxtPositionStatusCode";
            this.TxtPositionStatusCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtPositionStatusCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPositionStatusCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPositionStatusCode.Properties.Appearance.Options.UseFont = true;
            this.TxtPositionStatusCode.Properties.MaxLength = 80;
            this.TxtPositionStatusCode.Properties.ReadOnly = true;
            this.TxtPositionStatusCode.Size = new System.Drawing.Size(342, 20);
            this.TxtPositionStatusCode.TabIndex = 90;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(35, 302);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(88, 14);
            this.label4.TabIndex = 89;
            this.label4.Text = "Position Status";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkUpdLeaveStartDtInd
            // 
            this.ChkUpdLeaveStartDtInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkUpdLeaveStartDtInd.Location = new System.Drawing.Point(243, 48);
            this.ChkUpdLeaveStartDtInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkUpdLeaveStartDtInd.Name = "ChkUpdLeaveStartDtInd";
            this.ChkUpdLeaveStartDtInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkUpdLeaveStartDtInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkUpdLeaveStartDtInd.Properties.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.ChkUpdLeaveStartDtInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkUpdLeaveStartDtInd.Properties.Appearance.Options.UseFont = true;
            this.ChkUpdLeaveStartDtInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkUpdLeaveStartDtInd.Properties.Caption = "Update Permanent Date";
            this.ChkUpdLeaveStartDtInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkUpdLeaveStartDtInd.ShowToolTips = false;
            this.ChkUpdLeaveStartDtInd.Size = new System.Drawing.Size(159, 22);
            this.ChkUpdLeaveStartDtInd.TabIndex = 67;
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(128, 362);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 400;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(350, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(342, 20);
            this.MeeRemark.TabIndex = 96;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(76, 365);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 14);
            this.label5.TabIndex = 95;
            this.label5.Text = "Remark";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteResignDtOld
            // 
            this.DteResignDtOld.EditValue = null;
            this.DteResignDtOld.EnterMoveNextControl = true;
            this.DteResignDtOld.Location = new System.Drawing.Point(128, 341);
            this.DteResignDtOld.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteResignDtOld.Name = "DteResignDtOld";
            this.DteResignDtOld.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.DteResignDtOld.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteResignDtOld.Properties.Appearance.Options.UseBackColor = true;
            this.DteResignDtOld.Properties.Appearance.Options.UseFont = true;
            this.DteResignDtOld.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteResignDtOld.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteResignDtOld.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteResignDtOld.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteResignDtOld.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteResignDtOld.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteResignDtOld.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteResignDtOld.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteResignDtOld.Properties.ReadOnly = true;
            this.DteResignDtOld.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteResignDtOld.Size = new System.Drawing.Size(111, 20);
            this.DteResignDtOld.TabIndex = 94;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(51, 344);
            this.label22.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(72, 14);
            this.label22.TabIndex = 93;
            this.label22.Text = "Resign Date";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(95, 134);
            this.label20.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(28, 14);
            this.label20.TabIndex = 72;
            this.label20.Text = "Site";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSiteCode
            // 
            this.TxtSiteCode.EnterMoveNextControl = true;
            this.TxtSiteCode.Location = new System.Drawing.Point(128, 131);
            this.TxtSiteCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSiteCode.Name = "TxtSiteCode";
            this.TxtSiteCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtSiteCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSiteCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSiteCode.Properties.Appearance.Options.UseFont = true;
            this.TxtSiteCode.Properties.MaxLength = 80;
            this.TxtSiteCode.Properties.ReadOnly = true;
            this.TxtSiteCode.Size = new System.Drawing.Size(342, 20);
            this.TxtSiteCode.TabIndex = 73;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(45, 281);
            this.label18.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(78, 14);
            this.label18.TabIndex = 87;
            this.label18.Text = "Payroll Group";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPGCode
            // 
            this.TxtPGCode.EnterMoveNextControl = true;
            this.TxtPGCode.Location = new System.Drawing.Point(128, 278);
            this.TxtPGCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPGCode.Name = "TxtPGCode";
            this.TxtPGCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtPGCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPGCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPGCode.Properties.Appearance.Options.UseFont = true;
            this.TxtPGCode.Properties.MaxLength = 80;
            this.TxtPGCode.Properties.ReadOnly = true;
            this.TxtPGCode.Size = new System.Drawing.Size(342, 20);
            this.TxtPGCode.TabIndex = 88;
            // 
            // ChkCancelInd
            // 
            this.ChkCancelInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkCancelInd.Location = new System.Drawing.Point(408, 6);
            this.ChkCancelInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkCancelInd.Name = "ChkCancelInd";
            this.ChkCancelInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkCancelInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkCancelInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkCancelInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseFont = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkCancelInd.Properties.Caption = "Cancel";
            this.ChkCancelInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkCancelInd.ShowToolTips = false;
            this.ChkCancelInd.Size = new System.Drawing.Size(67, 22);
            this.ChkCancelInd.TabIndex = 62;
            // 
            // DteStartDt
            // 
            this.DteStartDt.EditValue = null;
            this.DteStartDt.EnterMoveNextControl = true;
            this.DteStartDt.Location = new System.Drawing.Point(128, 49);
            this.DteStartDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteStartDt.Name = "DteStartDt";
            this.DteStartDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteStartDt.Properties.Appearance.Options.UseFont = true;
            this.DteStartDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteStartDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteStartDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteStartDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteStartDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteStartDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteStartDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteStartDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteStartDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteStartDt.Size = new System.Drawing.Size(111, 20);
            this.DteStartDt.TabIndex = 66;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Red;
            this.label17.Location = new System.Drawing.Point(59, 52);
            this.label17.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(64, 14);
            this.label17.TabIndex = 65;
            this.label17.Text = "Start Date";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(34, 260);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(89, 14);
            this.label16.TabIndex = 85;
            this.label16.Text = "Payrun Periode";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPayrunPeriod
            // 
            this.TxtPayrunPeriod.EnterMoveNextControl = true;
            this.TxtPayrunPeriod.Location = new System.Drawing.Point(128, 257);
            this.TxtPayrunPeriod.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPayrunPeriod.Name = "TxtPayrunPeriod";
            this.TxtPayrunPeriod.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtPayrunPeriod.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPayrunPeriod.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPayrunPeriod.Properties.Appearance.Options.UseFont = true;
            this.TxtPayrunPeriod.Properties.MaxLength = 80;
            this.TxtPayrunPeriod.Properties.ReadOnly = true;
            this.TxtPayrunPeriod.Size = new System.Drawing.Size(342, 20);
            this.TxtPayrunPeriod.TabIndex = 86;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(44, 239);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(79, 14);
            this.label14.TabIndex = 83;
            this.label14.Text = "System Type";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSystemType
            // 
            this.TxtSystemType.EnterMoveNextControl = true;
            this.TxtSystemType.Location = new System.Drawing.Point(128, 236);
            this.TxtSystemType.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSystemType.Name = "TxtSystemType";
            this.TxtSystemType.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtSystemType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSystemType.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSystemType.Properties.Appearance.Options.UseFont = true;
            this.TxtSystemType.Properties.MaxLength = 80;
            this.TxtSystemType.Properties.ReadOnly = true;
            this.TxtSystemType.Size = new System.Drawing.Size(342, 20);
            this.TxtSystemType.TabIndex = 84;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(2, 218);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(121, 14);
            this.label13.TabIndex = 81;
            this.label13.Text = "Employement Status";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtEmploymentStatus
            // 
            this.TxtEmploymentStatus.EnterMoveNextControl = true;
            this.TxtEmploymentStatus.Location = new System.Drawing.Point(128, 215);
            this.TxtEmploymentStatus.Margin = new System.Windows.Forms.Padding(5);
            this.TxtEmploymentStatus.Name = "TxtEmploymentStatus";
            this.TxtEmploymentStatus.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtEmploymentStatus.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEmploymentStatus.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEmploymentStatus.Properties.Appearance.Options.UseFont = true;
            this.TxtEmploymentStatus.Properties.MaxLength = 80;
            this.TxtEmploymentStatus.Properties.ReadOnly = true;
            this.TxtEmploymentStatus.Size = new System.Drawing.Size(342, 20);
            this.TxtEmploymentStatus.TabIndex = 82;
            // 
            // TxtPosition
            // 
            this.TxtPosition.EnterMoveNextControl = true;
            this.TxtPosition.Location = new System.Drawing.Point(128, 173);
            this.TxtPosition.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPosition.Name = "TxtPosition";
            this.TxtPosition.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtPosition.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPosition.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPosition.Properties.Appearance.Options.UseFont = true;
            this.TxtPosition.Properties.MaxLength = 80;
            this.TxtPosition.Properties.ReadOnly = true;
            this.TxtPosition.Size = new System.Drawing.Size(193, 20);
            this.TxtPosition.TabIndex = 77;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(50, 155);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(73, 14);
            this.label6.TabIndex = 74;
            this.label6.Text = "Department";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDeptName
            // 
            this.TxtDeptName.EnterMoveNextControl = true;
            this.TxtDeptName.Location = new System.Drawing.Point(128, 152);
            this.TxtDeptName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDeptName.Name = "TxtDeptName";
            this.TxtDeptName.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDeptName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDeptName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDeptName.Properties.Appearance.Options.UseFont = true;
            this.TxtDeptName.Properties.MaxLength = 80;
            this.TxtDeptName.Properties.ReadOnly = true;
            this.TxtDeptName.Size = new System.Drawing.Size(342, 20);
            this.TxtDeptName.TabIndex = 75;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(74, 176);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(49, 14);
            this.label7.TabIndex = 76;
            this.label7.Text = "Position";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(52, 197);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(71, 14);
            this.label8.TabIndex = 79;
            this.label8.Text = "Grade Level";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtGrade
            // 
            this.TxtGrade.EnterMoveNextControl = true;
            this.TxtGrade.Location = new System.Drawing.Point(128, 194);
            this.TxtGrade.Margin = new System.Windows.Forms.Padding(5);
            this.TxtGrade.Name = "TxtGrade";
            this.TxtGrade.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtGrade.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtGrade.Properties.Appearance.Options.UseBackColor = true;
            this.TxtGrade.Properties.Appearance.Options.UseFont = true;
            this.TxtGrade.Properties.MaxLength = 80;
            this.TxtGrade.Properties.ReadOnly = true;
            this.TxtGrade.Size = new System.Drawing.Size(342, 20);
            this.TxtGrade.TabIndex = 80;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Location = new System.Drawing.Point(48, 73);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(75, 14);
            this.label9.TabIndex = 68;
            this.label9.Text = "Job Transfer";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueType
            // 
            this.LueType.EnterMoveNextControl = true;
            this.LueType.Location = new System.Drawing.Point(128, 70);
            this.LueType.Margin = new System.Windows.Forms.Padding(5);
            this.LueType.Name = "LueType";
            this.LueType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueType.Properties.Appearance.Options.UseFont = true;
            this.LueType.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueType.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueType.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueType.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueType.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueType.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueType.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueType.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueType.Properties.DropDownRows = 30;
            this.LueType.Properties.NullText = "[Empty]";
            this.LueType.Properties.PopupWidth = 300;
            this.LueType.Size = new System.Drawing.Size(276, 20);
            this.LueType.TabIndex = 69;
            this.LueType.ToolTip = "F4 : Show/hide list";
            this.LueType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueType.EditValueChanged += new System.EventHandler(this.LueType_EditValueChanged);
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(128, 28);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(111, 20);
            this.DteDocDt.TabIndex = 64;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Red;
            this.label10.Location = new System.Drawing.Point(90, 31);
            this.label10.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(33, 14);
            this.label10.TabIndex = 63;
            this.label10.Text = "Date";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(128, 7);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 30;
            this.TxtDocNo.Properties.ReadOnly = true;
            this.TxtDocNo.Size = new System.Drawing.Size(276, 20);
            this.TxtDocNo.TabIndex = 61;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(50, 10);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(73, 14);
            this.label11.TabIndex = 60;
            this.label11.Text = "Document#";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel5.Controls.Add(this.BtnLogWorkPeriod);
            this.panel5.Controls.Add(this.TxtWorkPeriodMth);
            this.panel5.Controls.Add(this.TxtWorkPeriodYr);
            this.panel5.Controls.Add(this.label12);
            this.panel5.Controls.Add(this.LueLevelCode);
            this.panel5.Controls.Add(this.label15);
            this.panel5.Controls.Add(this.LblSectionNew);
            this.panel5.Controls.Add(this.LueSectionCode);
            this.panel5.Controls.Add(this.ChkActingOfficialNew);
            this.panel5.Controls.Add(this.LuePositionStatusCode);
            this.panel5.Controls.Add(this.label19);
            this.panel5.Controls.Add(this.BtnEmployeeRequestDocNo2);
            this.panel5.Controls.Add(this.DteResignDtNew);
            this.panel5.Controls.Add(this.BtnEmployeeRequestDocNo);
            this.panel5.Controls.Add(this.label23);
            this.panel5.Controls.Add(this.TxtEmployeeRequestDocNo);
            this.panel5.Controls.Add(this.LueSiteCode);
            this.panel5.Controls.Add(this.label21);
            this.panel5.Controls.Add(this.label24);
            this.panel5.Controls.Add(this.label47);
            this.panel5.Controls.Add(this.LuePGCode);
            this.panel5.Controls.Add(this.LuePosCode);
            this.panel5.Controls.Add(this.TxtEntityName);
            this.panel5.Controls.Add(this.label49);
            this.panel5.Controls.Add(this.label50);
            this.panel5.Controls.Add(this.LueDeptCode);
            this.panel5.Controls.Add(this.label51);
            this.panel5.Controls.Add(this.LuePayrunPeriod);
            this.panel5.Controls.Add(this.LueGrdLvlCode);
            this.panel5.Controls.Add(this.label52);
            this.panel5.Controls.Add(this.label53);
            this.panel5.Controls.Add(this.label54);
            this.panel5.Controls.Add(this.LueEmploymentStatus);
            this.panel5.Controls.Add(this.label55);
            this.panel5.Controls.Add(this.LueSystemType);
            this.panel5.Controls.Add(this.TxtEmpCode);
            this.panel5.Controls.Add(this.label56);
            this.panel5.Controls.Add(this.BtnEmpCode);
            this.panel5.Controls.Add(this.label57);
            this.panel5.Controls.Add(this.TxtEmpName);
            this.panel5.Controls.Add(this.BtnEmpCode2);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel5.Location = new System.Drawing.Point(476, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(427, 389);
            this.panel5.TabIndex = 59;
            // 
            // BtnLogWorkPeriod
            // 
            this.BtnLogWorkPeriod.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnLogWorkPeriod.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnLogWorkPeriod.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnLogWorkPeriod.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnLogWorkPeriod.Appearance.Options.UseBackColor = true;
            this.BtnLogWorkPeriod.Appearance.Options.UseFont = true;
            this.BtnLogWorkPeriod.Appearance.Options.UseForeColor = true;
            this.BtnLogWorkPeriod.Appearance.Options.UseTextOptions = true;
            this.BtnLogWorkPeriod.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnLogWorkPeriod.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnLogWorkPeriod.Image = ((System.Drawing.Image)(resources.GetObject("BtnLogWorkPeriod.Image")));
            this.BtnLogWorkPeriod.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnLogWorkPeriod.Location = new System.Drawing.Point(239, 361);
            this.BtnLogWorkPeriod.Name = "BtnLogWorkPeriod";
            this.BtnLogWorkPeriod.Size = new System.Drawing.Size(24, 21);
            this.BtnLogWorkPeriod.TabIndex = 87;
            this.BtnLogWorkPeriod.ToolTip = "Show Employee Information";
            this.BtnLogWorkPeriod.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnLogWorkPeriod.ToolTipTitle = "Run System";
            this.BtnLogWorkPeriod.Click += new System.EventHandler(this.BtnLogWorkPeriod_Click);
            // 
            // TxtWorkPeriodMth
            // 
            this.TxtWorkPeriodMth.EnterMoveNextControl = true;
            this.TxtWorkPeriodMth.Location = new System.Drawing.Point(185, 362);
            this.TxtWorkPeriodMth.Margin = new System.Windows.Forms.Padding(5);
            this.TxtWorkPeriodMth.Name = "TxtWorkPeriodMth";
            this.TxtWorkPeriodMth.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtWorkPeriodMth.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtWorkPeriodMth.Properties.Appearance.Options.UseBackColor = true;
            this.TxtWorkPeriodMth.Properties.Appearance.Options.UseFont = true;
            this.TxtWorkPeriodMth.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtWorkPeriodMth.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtWorkPeriodMth.Size = new System.Drawing.Size(48, 20);
            this.TxtWorkPeriodMth.TabIndex = 86;
            this.TxtWorkPeriodMth.ToolTip = "Month";
            this.TxtWorkPeriodMth.Validated += new System.EventHandler(this.TxtWorkPeriodMth_Validated);
            // 
            // TxtWorkPeriodYr
            // 
            this.TxtWorkPeriodYr.EnterMoveNextControl = true;
            this.TxtWorkPeriodYr.Location = new System.Drawing.Point(122, 362);
            this.TxtWorkPeriodYr.Margin = new System.Windows.Forms.Padding(5);
            this.TxtWorkPeriodYr.Name = "TxtWorkPeriodYr";
            this.TxtWorkPeriodYr.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtWorkPeriodYr.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtWorkPeriodYr.Properties.Appearance.Options.UseBackColor = true;
            this.TxtWorkPeriodYr.Properties.Appearance.Options.UseFont = true;
            this.TxtWorkPeriodYr.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtWorkPeriodYr.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtWorkPeriodYr.Size = new System.Drawing.Size(61, 20);
            this.TxtWorkPeriodYr.TabIndex = 85;
            this.TxtWorkPeriodYr.ToolTip = "Year";
            this.TxtWorkPeriodYr.Validated += new System.EventHandler(this.TxtWorkPeriodYr_Validated);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(46, 365);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(74, 14);
            this.label12.TabIndex = 84;
            this.label12.Text = "Work Period";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueLevelCode
            // 
            this.LueLevelCode.EnterMoveNextControl = true;
            this.LueLevelCode.Location = new System.Drawing.Point(122, 320);
            this.LueLevelCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueLevelCode.Name = "LueLevelCode";
            this.LueLevelCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevelCode.Properties.Appearance.Options.UseFont = true;
            this.LueLevelCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevelCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueLevelCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevelCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueLevelCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevelCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueLevelCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevelCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueLevelCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueLevelCode.Properties.DropDownRows = 30;
            this.LueLevelCode.Properties.MaxLength = 16;
            this.LueLevelCode.Properties.NullText = "[Empty]";
            this.LueLevelCode.Properties.PopupWidth = 300;
            this.LueLevelCode.Size = new System.Drawing.Size(297, 20);
            this.LueLevelCode.TabIndex = 81;
            this.LueLevelCode.ToolTip = "F4 : Show/hide list";
            this.LueLevelCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueLevelCode.EditValueChanged += new System.EventHandler(this.LueLevelCode_EditValueChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(85, 323);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(35, 14);
            this.label15.TabIndex = 80;
            this.label15.Text = "Level";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblSectionNew
            // 
            this.LblSectionNew.AutoSize = true;
            this.LblSectionNew.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblSectionNew.ForeColor = System.Drawing.Color.Black;
            this.LblSectionNew.Location = new System.Drawing.Point(72, 112);
            this.LblSectionNew.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblSectionNew.Name = "LblSectionNew";
            this.LblSectionNew.Size = new System.Drawing.Size(48, 14);
            this.LblSectionNew.TabIndex = 59;
            this.LblSectionNew.Text = "Section";
            this.LblSectionNew.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueSectionCode
            // 
            this.LueSectionCode.EnterMoveNextControl = true;
            this.LueSectionCode.Location = new System.Drawing.Point(122, 110);
            this.LueSectionCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueSectionCode.Name = "LueSectionCode";
            this.LueSectionCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSectionCode.Properties.Appearance.Options.UseFont = true;
            this.LueSectionCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSectionCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSectionCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSectionCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSectionCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSectionCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSectionCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSectionCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSectionCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSectionCode.Properties.DropDownRows = 30;
            this.LueSectionCode.Properties.MaxLength = 16;
            this.LueSectionCode.Properties.NullText = "[Empty]";
            this.LueSectionCode.Properties.PopupWidth = 300;
            this.LueSectionCode.Size = new System.Drawing.Size(297, 20);
            this.LueSectionCode.TabIndex = 60;
            this.LueSectionCode.ToolTip = "F4 : Show/hide list";
            this.LueSectionCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSectionCode.EditValueChanged += new System.EventHandler(this.LueSectionCode_EditValueChanged);
            // 
            // ChkActingOfficialNew
            // 
            this.ChkActingOfficialNew.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkActingOfficialNew.Location = new System.Drawing.Point(276, 172);
            this.ChkActingOfficialNew.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkActingOfficialNew.Name = "ChkActingOfficialNew";
            this.ChkActingOfficialNew.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkActingOfficialNew.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkActingOfficialNew.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkActingOfficialNew.Properties.Appearance.Options.UseBackColor = true;
            this.ChkActingOfficialNew.Properties.Appearance.Options.UseFont = true;
            this.ChkActingOfficialNew.Properties.Appearance.Options.UseForeColor = true;
            this.ChkActingOfficialNew.Properties.Caption = "Acting Official (PLT)";
            this.ChkActingOfficialNew.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkActingOfficialNew.ShowToolTips = false;
            this.ChkActingOfficialNew.Size = new System.Drawing.Size(140, 22);
            this.ChkActingOfficialNew.TabIndex = 67;
            // 
            // LuePositionStatusCode
            // 
            this.LuePositionStatusCode.EnterMoveNextControl = true;
            this.LuePositionStatusCode.Location = new System.Drawing.Point(122, 299);
            this.LuePositionStatusCode.Margin = new System.Windows.Forms.Padding(5);
            this.LuePositionStatusCode.Name = "LuePositionStatusCode";
            this.LuePositionStatusCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePositionStatusCode.Properties.Appearance.Options.UseFont = true;
            this.LuePositionStatusCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePositionStatusCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePositionStatusCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePositionStatusCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePositionStatusCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePositionStatusCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePositionStatusCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePositionStatusCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePositionStatusCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePositionStatusCode.Properties.DropDownRows = 30;
            this.LuePositionStatusCode.Properties.MaxLength = 16;
            this.LuePositionStatusCode.Properties.NullText = "[Empty]";
            this.LuePositionStatusCode.Properties.PopupWidth = 300;
            this.LuePositionStatusCode.Size = new System.Drawing.Size(297, 20);
            this.LuePositionStatusCode.TabIndex = 79;
            this.LuePositionStatusCode.ToolTip = "F4 : Show/hide list";
            this.LuePositionStatusCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LuePositionStatusCode.EditValueChanged += new System.EventHandler(this.LuePositionStatusCode_EditValueChanged);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(32, 302);
            this.label19.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(88, 14);
            this.label19.TabIndex = 78;
            this.label19.Text = "Position Status";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnEmployeeRequestDocNo2
            // 
            this.BtnEmployeeRequestDocNo2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnEmployeeRequestDocNo2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEmployeeRequestDocNo2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEmployeeRequestDocNo2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEmployeeRequestDocNo2.Appearance.Options.UseBackColor = true;
            this.BtnEmployeeRequestDocNo2.Appearance.Options.UseFont = true;
            this.BtnEmployeeRequestDocNo2.Appearance.Options.UseForeColor = true;
            this.BtnEmployeeRequestDocNo2.Appearance.Options.UseTextOptions = true;
            this.BtnEmployeeRequestDocNo2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnEmployeeRequestDocNo2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnEmployeeRequestDocNo2.Image = ((System.Drawing.Image)(resources.GetObject("BtnEmployeeRequestDocNo2.Image")));
            this.BtnEmployeeRequestDocNo2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnEmployeeRequestDocNo2.Location = new System.Drawing.Point(397, 27);
            this.BtnEmployeeRequestDocNo2.Name = "BtnEmployeeRequestDocNo2";
            this.BtnEmployeeRequestDocNo2.Size = new System.Drawing.Size(24, 21);
            this.BtnEmployeeRequestDocNo2.TabIndex = 27;
            this.BtnEmployeeRequestDocNo2.ToolTip = "Show Employee Request Document Information";
            this.BtnEmployeeRequestDocNo2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnEmployeeRequestDocNo2.ToolTipTitle = "Run System";
            this.BtnEmployeeRequestDocNo2.Click += new System.EventHandler(this.BtnEmployeeRequestDocNo2_Click);
            // 
            // DteResignDtNew
            // 
            this.DteResignDtNew.EditValue = null;
            this.DteResignDtNew.EnterMoveNextControl = true;
            this.DteResignDtNew.Location = new System.Drawing.Point(122, 341);
            this.DteResignDtNew.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteResignDtNew.Name = "DteResignDtNew";
            this.DteResignDtNew.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteResignDtNew.Properties.Appearance.Options.UseFont = true;
            this.DteResignDtNew.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteResignDtNew.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteResignDtNew.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteResignDtNew.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteResignDtNew.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteResignDtNew.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteResignDtNew.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteResignDtNew.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteResignDtNew.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteResignDtNew.Size = new System.Drawing.Size(111, 20);
            this.DteResignDtNew.TabIndex = 83;
            // 
            // BtnEmployeeRequestDocNo
            // 
            this.BtnEmployeeRequestDocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnEmployeeRequestDocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEmployeeRequestDocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEmployeeRequestDocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEmployeeRequestDocNo.Appearance.Options.UseBackColor = true;
            this.BtnEmployeeRequestDocNo.Appearance.Options.UseFont = true;
            this.BtnEmployeeRequestDocNo.Appearance.Options.UseForeColor = true;
            this.BtnEmployeeRequestDocNo.Appearance.Options.UseTextOptions = true;
            this.BtnEmployeeRequestDocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnEmployeeRequestDocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnEmployeeRequestDocNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnEmployeeRequestDocNo.Image")));
            this.BtnEmployeeRequestDocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnEmployeeRequestDocNo.Location = new System.Drawing.Point(369, 27);
            this.BtnEmployeeRequestDocNo.Name = "BtnEmployeeRequestDocNo";
            this.BtnEmployeeRequestDocNo.Size = new System.Drawing.Size(24, 21);
            this.BtnEmployeeRequestDocNo.TabIndex = 26;
            this.BtnEmployeeRequestDocNo.ToolTip = "Find Employee Request Document#";
            this.BtnEmployeeRequestDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnEmployeeRequestDocNo.ToolTipTitle = "Run System";
            this.BtnEmployeeRequestDocNo.Click += new System.EventHandler(this.BtnEmployeeRequestDocNo_Click);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Black;
            this.label23.Location = new System.Drawing.Point(48, 344);
            this.label23.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(72, 14);
            this.label23.TabIndex = 82;
            this.label23.Text = "Resign Date";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtEmployeeRequestDocNo
            // 
            this.TxtEmployeeRequestDocNo.EnterMoveNextControl = true;
            this.TxtEmployeeRequestDocNo.Location = new System.Drawing.Point(122, 28);
            this.TxtEmployeeRequestDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtEmployeeRequestDocNo.Name = "TxtEmployeeRequestDocNo";
            this.TxtEmployeeRequestDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtEmployeeRequestDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEmployeeRequestDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEmployeeRequestDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtEmployeeRequestDocNo.Properties.MaxLength = 30;
            this.TxtEmployeeRequestDocNo.Properties.ReadOnly = true;
            this.TxtEmployeeRequestDocNo.Size = new System.Drawing.Size(246, 20);
            this.TxtEmployeeRequestDocNo.TabIndex = 25;
            // 
            // LueSiteCode
            // 
            this.LueSiteCode.EnterMoveNextControl = true;
            this.LueSiteCode.Location = new System.Drawing.Point(122, 131);
            this.LueSiteCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueSiteCode.Name = "LueSiteCode";
            this.LueSiteCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.Appearance.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSiteCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSiteCode.Properties.DropDownRows = 30;
            this.LueSiteCode.Properties.NullText = "[Empty]";
            this.LueSiteCode.Properties.PopupWidth = 300;
            this.LueSiteCode.Size = new System.Drawing.Size(297, 20);
            this.LueSiteCode.TabIndex = 62;
            this.LueSiteCode.ToolTip = "F4 : Show/hide list";
            this.LueSiteCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSiteCode.EditValueChanged += new System.EventHandler(this.LueSiteCode_EditValueChanged);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Black;
            this.label21.Location = new System.Drawing.Point(2, 31);
            this.label21.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(118, 14);
            this.label21.TabIndex = 24;
            this.label21.Text = "Employee Request#";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Black;
            this.label24.Location = new System.Drawing.Point(92, 134);
            this.label24.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(28, 14);
            this.label24.TabIndex = 61;
            this.label24.Text = "Site";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.ForeColor = System.Drawing.Color.Black;
            this.label47.Location = new System.Drawing.Point(42, 281);
            this.label47.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(78, 14);
            this.label47.TabIndex = 76;
            this.label47.Text = "Payroll Group";
            this.label47.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LuePGCode
            // 
            this.LuePGCode.EnterMoveNextControl = true;
            this.LuePGCode.Location = new System.Drawing.Point(122, 278);
            this.LuePGCode.Margin = new System.Windows.Forms.Padding(5);
            this.LuePGCode.Name = "LuePGCode";
            this.LuePGCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePGCode.Properties.Appearance.Options.UseFont = true;
            this.LuePGCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePGCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePGCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePGCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePGCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePGCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePGCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePGCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePGCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePGCode.Properties.DropDownRows = 30;
            this.LuePGCode.Properties.NullText = "[Empty]";
            this.LuePGCode.Properties.PopupWidth = 300;
            this.LuePGCode.Size = new System.Drawing.Size(297, 20);
            this.LuePGCode.TabIndex = 77;
            this.LuePGCode.ToolTip = "F4 : Show/hide list";
            this.LuePGCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LuePGCode.EditValueChanged += new System.EventHandler(this.LuePGCode_EditValueChanged);
            // 
            // LuePosCode
            // 
            this.LuePosCode.EnterMoveNextControl = true;
            this.LuePosCode.Location = new System.Drawing.Point(122, 173);
            this.LuePosCode.Margin = new System.Windows.Forms.Padding(5);
            this.LuePosCode.Name = "LuePosCode";
            this.LuePosCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePosCode.Properties.Appearance.Options.UseFont = true;
            this.LuePosCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePosCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePosCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePosCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePosCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePosCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePosCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePosCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePosCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePosCode.Properties.DropDownRows = 30;
            this.LuePosCode.Properties.MaxLength = 50;
            this.LuePosCode.Properties.NullText = "[Empty]";
            this.LuePosCode.Properties.PopupWidth = 300;
            this.LuePosCode.Size = new System.Drawing.Size(153, 20);
            this.LuePosCode.TabIndex = 66;
            this.LuePosCode.ToolTip = "F4 : Show/hide list";
            this.LuePosCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LuePosCode.EditValueChanged += new System.EventHandler(this.LuePosCode_EditValueChanged);
            // 
            // TxtEntityName
            // 
            this.TxtEntityName.EnterMoveNextControl = true;
            this.TxtEntityName.Location = new System.Drawing.Point(122, 70);
            this.TxtEntityName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtEntityName.Name = "TxtEntityName";
            this.TxtEntityName.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtEntityName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEntityName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEntityName.Properties.Appearance.Options.UseFont = true;
            this.TxtEntityName.Properties.MaxLength = 80;
            this.TxtEntityName.Properties.ReadOnly = true;
            this.TxtEntityName.Size = new System.Drawing.Size(296, 20);
            this.TxtEntityName.TabIndex = 31;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.ForeColor = System.Drawing.Color.Black;
            this.label49.Location = new System.Drawing.Point(38, 260);
            this.label49.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(82, 14);
            this.label49.TabIndex = 74;
            this.label49.Text = "Payrun Period";
            this.label49.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.ForeColor = System.Drawing.Color.Black;
            this.label50.Location = new System.Drawing.Point(81, 73);
            this.label50.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(39, 14);
            this.label50.TabIndex = 30;
            this.label50.Text = "Entity";
            this.label50.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueDeptCode
            // 
            this.LueDeptCode.EnterMoveNextControl = true;
            this.LueDeptCode.Location = new System.Drawing.Point(122, 152);
            this.LueDeptCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueDeptCode.Name = "LueDeptCode";
            this.LueDeptCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.Appearance.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueDeptCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueDeptCode.Properties.DropDownRows = 30;
            this.LueDeptCode.Properties.NullText = "[Empty]";
            this.LueDeptCode.Properties.PopupWidth = 300;
            this.LueDeptCode.Size = new System.Drawing.Size(297, 20);
            this.LueDeptCode.TabIndex = 64;
            this.LueDeptCode.ToolTip = "F4 : Show/hide list";
            this.LueDeptCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueDeptCode.EditValueChanged += new System.EventHandler(this.LueDeptCode_EditValueChanged);
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.ForeColor = System.Drawing.Color.Red;
            this.label51.Location = new System.Drawing.Point(47, 155);
            this.label51.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(73, 14);
            this.label51.TabIndex = 63;
            this.label51.Text = "Department";
            this.label51.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LuePayrunPeriod
            // 
            this.LuePayrunPeriod.EnterMoveNextControl = true;
            this.LuePayrunPeriod.Location = new System.Drawing.Point(122, 257);
            this.LuePayrunPeriod.Margin = new System.Windows.Forms.Padding(5);
            this.LuePayrunPeriod.Name = "LuePayrunPeriod";
            this.LuePayrunPeriod.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePayrunPeriod.Properties.Appearance.Options.UseFont = true;
            this.LuePayrunPeriod.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePayrunPeriod.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePayrunPeriod.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePayrunPeriod.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePayrunPeriod.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePayrunPeriod.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePayrunPeriod.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePayrunPeriod.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePayrunPeriod.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePayrunPeriod.Properties.DropDownRows = 30;
            this.LuePayrunPeriod.Properties.NullText = "[Empty]";
            this.LuePayrunPeriod.Properties.PopupWidth = 300;
            this.LuePayrunPeriod.Size = new System.Drawing.Size(297, 20);
            this.LuePayrunPeriod.TabIndex = 75;
            this.LuePayrunPeriod.ToolTip = "F4 : Show/hide list";
            this.LuePayrunPeriod.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LuePayrunPeriod.EditValueChanged += new System.EventHandler(this.LuePayrunPeriod_EditValueChanged);
            // 
            // LueGrdLvlCode
            // 
            this.LueGrdLvlCode.EnterMoveNextControl = true;
            this.LueGrdLvlCode.Location = new System.Drawing.Point(122, 194);
            this.LueGrdLvlCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueGrdLvlCode.Name = "LueGrdLvlCode";
            this.LueGrdLvlCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGrdLvlCode.Properties.Appearance.Options.UseFont = true;
            this.LueGrdLvlCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGrdLvlCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueGrdLvlCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGrdLvlCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueGrdLvlCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGrdLvlCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueGrdLvlCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGrdLvlCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueGrdLvlCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueGrdLvlCode.Properties.DropDownRows = 30;
            this.LueGrdLvlCode.Properties.NullText = "[Empty]";
            this.LueGrdLvlCode.Properties.PopupWidth = 300;
            this.LueGrdLvlCode.Size = new System.Drawing.Size(297, 20);
            this.LueGrdLvlCode.TabIndex = 69;
            this.LueGrdLvlCode.ToolTip = "F4 : Show/hide list";
            this.LueGrdLvlCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueGrdLvlCode.EditValueChanged += new System.EventHandler(this.LueGrdLvlCode_EditValueChanged);
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.ForeColor = System.Drawing.Color.Black;
            this.label52.Location = new System.Drawing.Point(49, 197);
            this.label52.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(71, 14);
            this.label52.TabIndex = 68;
            this.label52.Text = "Grade Level";
            this.label52.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.ForeColor = System.Drawing.Color.Black;
            this.label53.Location = new System.Drawing.Point(71, 176);
            this.label53.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(49, 14);
            this.label53.TabIndex = 65;
            this.label53.Text = "Position";
            this.label53.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.ForeColor = System.Drawing.Color.Black;
            this.label54.Location = new System.Drawing.Point(41, 239);
            this.label54.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(79, 14);
            this.label54.TabIndex = 72;
            this.label54.Text = "System Type";
            this.label54.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueEmploymentStatus
            // 
            this.LueEmploymentStatus.EnterMoveNextControl = true;
            this.LueEmploymentStatus.Location = new System.Drawing.Point(122, 215);
            this.LueEmploymentStatus.Margin = new System.Windows.Forms.Padding(5);
            this.LueEmploymentStatus.Name = "LueEmploymentStatus";
            this.LueEmploymentStatus.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEmploymentStatus.Properties.Appearance.Options.UseFont = true;
            this.LueEmploymentStatus.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEmploymentStatus.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueEmploymentStatus.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEmploymentStatus.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueEmploymentStatus.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEmploymentStatus.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueEmploymentStatus.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEmploymentStatus.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueEmploymentStatus.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueEmploymentStatus.Properties.DropDownRows = 30;
            this.LueEmploymentStatus.Properties.NullText = "[Empty]";
            this.LueEmploymentStatus.Properties.PopupWidth = 300;
            this.LueEmploymentStatus.Size = new System.Drawing.Size(297, 20);
            this.LueEmploymentStatus.TabIndex = 71;
            this.LueEmploymentStatus.ToolTip = "F4 : Show/hide list";
            this.LueEmploymentStatus.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueEmploymentStatus.EditValueChanged += new System.EventHandler(this.LueEmploymentStatus_EditValueChanged);
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.ForeColor = System.Drawing.Color.Black;
            this.label55.Location = new System.Drawing.Point(6, 218);
            this.label55.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(114, 14);
            this.label55.TabIndex = 70;
            this.label55.Text = "Employment Status";
            this.label55.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueSystemType
            // 
            this.LueSystemType.EnterMoveNextControl = true;
            this.LueSystemType.Location = new System.Drawing.Point(122, 236);
            this.LueSystemType.Margin = new System.Windows.Forms.Padding(5);
            this.LueSystemType.Name = "LueSystemType";
            this.LueSystemType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSystemType.Properties.Appearance.Options.UseFont = true;
            this.LueSystemType.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSystemType.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSystemType.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSystemType.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSystemType.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSystemType.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSystemType.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSystemType.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSystemType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSystemType.Properties.DropDownRows = 30;
            this.LueSystemType.Properties.MaxLength = 30;
            this.LueSystemType.Properties.NullText = "[Empty]";
            this.LueSystemType.Properties.PopupWidth = 300;
            this.LueSystemType.Size = new System.Drawing.Size(297, 20);
            this.LueSystemType.TabIndex = 73;
            this.LueSystemType.ToolTip = "F4 : Show/hide list";
            this.LueSystemType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSystemType.EditValueChanged += new System.EventHandler(this.LueSystemType_EditValueChanged);
            // 
            // TxtEmpCode
            // 
            this.TxtEmpCode.EnterMoveNextControl = true;
            this.TxtEmpCode.Location = new System.Drawing.Point(122, 7);
            this.TxtEmpCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtEmpCode.Name = "TxtEmpCode";
            this.TxtEmpCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtEmpCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEmpCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEmpCode.Properties.Appearance.Options.UseFont = true;
            this.TxtEmpCode.Properties.MaxLength = 16;
            this.TxtEmpCode.Properties.ReadOnly = true;
            this.TxtEmpCode.Size = new System.Drawing.Size(246, 20);
            this.TxtEmpCode.TabIndex = 21;
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label56.ForeColor = System.Drawing.Color.Red;
            this.label56.Location = new System.Drawing.Point(28, 10);
            this.label56.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(92, 14);
            this.label56.TabIndex = 20;
            this.label56.Text = "Employee Code";
            this.label56.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnEmpCode
            // 
            this.BtnEmpCode.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnEmpCode.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEmpCode.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEmpCode.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEmpCode.Appearance.Options.UseBackColor = true;
            this.BtnEmpCode.Appearance.Options.UseFont = true;
            this.BtnEmpCode.Appearance.Options.UseForeColor = true;
            this.BtnEmpCode.Appearance.Options.UseTextOptions = true;
            this.BtnEmpCode.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnEmpCode.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnEmpCode.Image = ((System.Drawing.Image)(resources.GetObject("BtnEmpCode.Image")));
            this.BtnEmpCode.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnEmpCode.Location = new System.Drawing.Point(369, 6);
            this.BtnEmpCode.Name = "BtnEmpCode";
            this.BtnEmpCode.Size = new System.Drawing.Size(24, 21);
            this.BtnEmpCode.TabIndex = 22;
            this.BtnEmpCode.ToolTip = "Find Employee";
            this.BtnEmpCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnEmpCode.ToolTipTitle = "Run System";
            this.BtnEmpCode.Click += new System.EventHandler(this.BtnEmpCode_Click_1);
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label57.ForeColor = System.Drawing.Color.Black;
            this.label57.Location = new System.Drawing.Point(25, 52);
            this.label57.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(95, 14);
            this.label57.TabIndex = 28;
            this.label57.Text = "Employee Name";
            this.label57.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtEmpName
            // 
            this.TxtEmpName.EnterMoveNextControl = true;
            this.TxtEmpName.Location = new System.Drawing.Point(122, 49);
            this.TxtEmpName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtEmpName.Name = "TxtEmpName";
            this.TxtEmpName.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtEmpName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEmpName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEmpName.Properties.Appearance.Options.UseFont = true;
            this.TxtEmpName.Properties.MaxLength = 80;
            this.TxtEmpName.Properties.ReadOnly = true;
            this.TxtEmpName.Size = new System.Drawing.Size(296, 20);
            this.TxtEmpName.TabIndex = 29;
            // 
            // BtnEmpCode2
            // 
            this.BtnEmpCode2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnEmpCode2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEmpCode2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEmpCode2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEmpCode2.Appearance.Options.UseBackColor = true;
            this.BtnEmpCode2.Appearance.Options.UseFont = true;
            this.BtnEmpCode2.Appearance.Options.UseForeColor = true;
            this.BtnEmpCode2.Appearance.Options.UseTextOptions = true;
            this.BtnEmpCode2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnEmpCode2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnEmpCode2.Image = ((System.Drawing.Image)(resources.GetObject("BtnEmpCode2.Image")));
            this.BtnEmpCode2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnEmpCode2.Location = new System.Drawing.Point(397, 6);
            this.BtnEmpCode2.Name = "BtnEmpCode2";
            this.BtnEmpCode2.Size = new System.Drawing.Size(24, 21);
            this.BtnEmpCode2.TabIndex = 23;
            this.BtnEmpCode2.ToolTip = "Show Employee Information";
            this.BtnEmpCode2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnEmpCode2.ToolTipTitle = "Run System";
            this.BtnEmpCode2.Click += new System.EventHandler(this.BtnEmpCode2_Click_1);
            // 
            // TpgFile
            // 
            this.TpgFile.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.TpgFile.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.TpgFile.Controls.Add(this.label1);
            this.TpgFile.Controls.Add(this.PbUpload3);
            this.TpgFile.Controls.Add(this.PbUpload2);
            this.TpgFile.Controls.Add(this.ChkFile3);
            this.TpgFile.Controls.Add(this.TxtFile3);
            this.TpgFile.Controls.Add(this.BtnFile3);
            this.TpgFile.Controls.Add(this.ChkFile2);
            this.TpgFile.Controls.Add(this.TxtFile2);
            this.TpgFile.Controls.Add(this.label2);
            this.TpgFile.Controls.Add(this.BtnFile2);
            this.TpgFile.Controls.Add(this.ChkFile);
            this.TpgFile.Controls.Add(this.PbUpload);
            this.TpgFile.Controls.Add(this.TxtFile);
            this.TpgFile.Controls.Add(this.label25);
            this.TpgFile.Controls.Add(this.BtnFile);
            this.TpgFile.Location = new System.Drawing.Point(4, 26);
            this.TpgFile.Name = "TpgFile";
            this.TpgFile.Size = new System.Drawing.Size(764, 203);
            this.TpgFile.TabIndex = 12;
            this.TpgFile.Text = "File";
            this.TpgFile.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(6, 96);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 14);
            this.label1.TabIndex = 57;
            this.label1.Text = "File 3";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // PbUpload3
            // 
            this.PbUpload3.Location = new System.Drawing.Point(48, 114);
            this.PbUpload3.Name = "PbUpload3";
            this.PbUpload3.Size = new System.Drawing.Size(500, 17);
            this.PbUpload3.TabIndex = 61;
            // 
            // PbUpload2
            // 
            this.PbUpload2.Location = new System.Drawing.Point(48, 73);
            this.PbUpload2.Name = "PbUpload2";
            this.PbUpload2.Size = new System.Drawing.Size(500, 17);
            this.PbUpload2.TabIndex = 56;
            // 
            // ChkFile3
            // 
            this.ChkFile3.Location = new System.Drawing.Point(465, 92);
            this.ChkFile3.Name = "ChkFile3";
            this.ChkFile3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkFile3.Properties.Appearance.Options.UseFont = true;
            this.ChkFile3.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkFile3.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkFile3.Properties.Caption = " ";
            this.ChkFile3.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkFile3.Size = new System.Drawing.Size(20, 22);
            this.ChkFile3.TabIndex = 59;
            this.ChkFile3.ToolTip = "Remove filter";
            this.ChkFile3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkFile3.ToolTipTitle = "Run System";
            this.ChkFile3.CheckedChanged += new System.EventHandler(this.ChkFile3_CheckedChanged);
            // 
            // TxtFile3
            // 
            this.TxtFile3.EnterMoveNextControl = true;
            this.TxtFile3.Location = new System.Drawing.Point(48, 92);
            this.TxtFile3.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFile3.Name = "TxtFile3";
            this.TxtFile3.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFile3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFile3.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFile3.Properties.Appearance.Options.UseFont = true;
            this.TxtFile3.Properties.MaxLength = 16;
            this.TxtFile3.Size = new System.Drawing.Size(410, 20);
            this.TxtFile3.TabIndex = 58;
            // 
            // BtnFile3
            // 
            this.BtnFile3.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnFile3.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFile3.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFile3.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFile3.Appearance.Options.UseBackColor = true;
            this.BtnFile3.Appearance.Options.UseFont = true;
            this.BtnFile3.Appearance.Options.UseForeColor = true;
            this.BtnFile3.Appearance.Options.UseTextOptions = true;
            this.BtnFile3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnFile3.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnFile3.Image = ((System.Drawing.Image)(resources.GetObject("BtnFile3.Image")));
            this.BtnFile3.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnFile3.Location = new System.Drawing.Point(493, 92);
            this.BtnFile3.Name = "BtnFile3";
            this.BtnFile3.Size = new System.Drawing.Size(24, 21);
            this.BtnFile3.TabIndex = 60;
            this.BtnFile3.ToolTip = "BrowseFile";
            this.BtnFile3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnFile3.ToolTipTitle = "Run System";
            this.BtnFile3.Click += new System.EventHandler(this.BtnFile3_Click);
            // 
            // ChkFile2
            // 
            this.ChkFile2.Location = new System.Drawing.Point(465, 50);
            this.ChkFile2.Name = "ChkFile2";
            this.ChkFile2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkFile2.Properties.Appearance.Options.UseFont = true;
            this.ChkFile2.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkFile2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkFile2.Properties.Caption = " ";
            this.ChkFile2.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkFile2.Size = new System.Drawing.Size(20, 22);
            this.ChkFile2.TabIndex = 54;
            this.ChkFile2.ToolTip = "Remove filter";
            this.ChkFile2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkFile2.ToolTipTitle = "Run System";
            this.ChkFile2.CheckedChanged += new System.EventHandler(this.ChkFile2_CheckedChanged);
            // 
            // TxtFile2
            // 
            this.TxtFile2.EnterMoveNextControl = true;
            this.TxtFile2.Location = new System.Drawing.Point(48, 50);
            this.TxtFile2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFile2.Name = "TxtFile2";
            this.TxtFile2.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFile2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFile2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFile2.Properties.Appearance.Options.UseFont = true;
            this.TxtFile2.Properties.MaxLength = 16;
            this.TxtFile2.Size = new System.Drawing.Size(410, 20);
            this.TxtFile2.TabIndex = 53;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(6, 54);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 14);
            this.label2.TabIndex = 52;
            this.label2.Text = "File 2";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnFile2
            // 
            this.BtnFile2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnFile2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFile2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFile2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFile2.Appearance.Options.UseBackColor = true;
            this.BtnFile2.Appearance.Options.UseFont = true;
            this.BtnFile2.Appearance.Options.UseForeColor = true;
            this.BtnFile2.Appearance.Options.UseTextOptions = true;
            this.BtnFile2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnFile2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnFile2.Image = ((System.Drawing.Image)(resources.GetObject("BtnFile2.Image")));
            this.BtnFile2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnFile2.Location = new System.Drawing.Point(493, 50);
            this.BtnFile2.Name = "BtnFile2";
            this.BtnFile2.Size = new System.Drawing.Size(24, 21);
            this.BtnFile2.TabIndex = 55;
            this.BtnFile2.ToolTip = "BrowseFile";
            this.BtnFile2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnFile2.ToolTipTitle = "Run System";
            this.BtnFile2.Click += new System.EventHandler(this.BtnFile2_Click);
            // 
            // ChkFile
            // 
            this.ChkFile.Location = new System.Drawing.Point(465, 9);
            this.ChkFile.Name = "ChkFile";
            this.ChkFile.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkFile.Properties.Appearance.Options.UseFont = true;
            this.ChkFile.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkFile.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkFile.Properties.Caption = " ";
            this.ChkFile.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkFile.Size = new System.Drawing.Size(20, 22);
            this.ChkFile.TabIndex = 49;
            this.ChkFile.ToolTip = "Remove filter";
            this.ChkFile.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkFile.ToolTipTitle = "Run System";
            this.ChkFile.CheckedChanged += new System.EventHandler(this.ChkFile_CheckedChanged);
            // 
            // PbUpload
            // 
            this.PbUpload.Location = new System.Drawing.Point(48, 31);
            this.PbUpload.Name = "PbUpload";
            this.PbUpload.Size = new System.Drawing.Size(500, 17);
            this.PbUpload.TabIndex = 51;
            // 
            // TxtFile
            // 
            this.TxtFile.EnterMoveNextControl = true;
            this.TxtFile.Location = new System.Drawing.Point(48, 9);
            this.TxtFile.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFile.Name = "TxtFile";
            this.TxtFile.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFile.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFile.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFile.Properties.Appearance.Options.UseFont = true;
            this.TxtFile.Properties.MaxLength = 16;
            this.TxtFile.Size = new System.Drawing.Size(410, 20);
            this.TxtFile.TabIndex = 48;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Black;
            this.label25.Location = new System.Drawing.Point(17, 13);
            this.label25.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(24, 14);
            this.label25.TabIndex = 47;
            this.label25.Text = "File";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnFile
            // 
            this.BtnFile.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnFile.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFile.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFile.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFile.Appearance.Options.UseBackColor = true;
            this.BtnFile.Appearance.Options.UseFont = true;
            this.BtnFile.Appearance.Options.UseForeColor = true;
            this.BtnFile.Appearance.Options.UseTextOptions = true;
            this.BtnFile.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnFile.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnFile.Image = ((System.Drawing.Image)(resources.GetObject("BtnFile.Image")));
            this.BtnFile.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnFile.Location = new System.Drawing.Point(493, 9);
            this.BtnFile.Name = "BtnFile";
            this.BtnFile.Size = new System.Drawing.Size(24, 21);
            this.BtnFile.TabIndex = 50;
            this.BtnFile.ToolTip = "BrowseFile";
            this.BtnFile.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnFile.ToolTipTitle = "Run System";
            this.BtnFile.Click += new System.EventHandler(this.BtnFile_Click);
            // 
            // OD
            // 
            this.OD.FileName = "openFileDialog1";
            // 
            // FrmPPS
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(985, 423);
            this.Name = "FrmPPS";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.TpgGeneral.ResumeLayout(false);
            this.TpgGeneral.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLevelCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSectionCodeOld.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActingOfficialOld.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPositionStatusCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkUpdLeaveStartDtInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteResignDtOld.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteResignDtOld.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSiteCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPGCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPayrunPeriod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSystemType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmploymentStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPosition.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDeptName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGrade.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtWorkPeriodMth.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtWorkPeriodYr.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueLevelCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSectionCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActingOfficialNew.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePositionStatusCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteResignDtNew.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteResignDtNew.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmployeeRequestDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSiteCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePGCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePosCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEntityName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDeptCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePayrunPeriod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueGrdLvlCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueEmploymentStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSystemType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpName.Properties)).EndInit();
            this.TpgFile.ResumeLayout(false);
            this.TpgFile.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage TpgGeneral;
        internal DevExpress.XtraEditors.TextEdit TxtLevelCode;
        private System.Windows.Forms.Label label3;
        internal DevExpress.XtraEditors.TextEdit TxtSectionCodeOld;
        private System.Windows.Forms.Label LblSectionOld;
        internal DevExpress.XtraEditors.CheckEdit ChkActingOfficialOld;
        internal DevExpress.XtraEditors.TextEdit TxtPositionStatusCode;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.CheckEdit ChkUpdLeaveStartDtInd;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private System.Windows.Forms.Label label5;
        public DevExpress.XtraEditors.DateEdit DteResignDtOld;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label20;
        internal DevExpress.XtraEditors.TextEdit TxtSiteCode;
        private System.Windows.Forms.Label label18;
        internal DevExpress.XtraEditors.TextEdit TxtPGCode;
        private DevExpress.XtraEditors.CheckEdit ChkCancelInd;
        internal DevExpress.XtraEditors.DateEdit DteStartDt;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        internal DevExpress.XtraEditors.TextEdit TxtPayrunPeriod;
        private System.Windows.Forms.Label label14;
        internal DevExpress.XtraEditors.TextEdit TxtSystemType;
        private System.Windows.Forms.Label label13;
        internal DevExpress.XtraEditors.TextEdit TxtEmploymentStatus;
        internal DevExpress.XtraEditors.TextEdit TxtPosition;
        private System.Windows.Forms.Label label6;
        internal DevExpress.XtraEditors.TextEdit TxtDeptName;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        internal DevExpress.XtraEditors.TextEdit TxtGrade;
        private System.Windows.Forms.Label label9;
        public DevExpress.XtraEditors.LookUpEdit LueType;
        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        private System.Windows.Forms.Label label10;
        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Label label11;
        protected System.Windows.Forms.Panel panel5;
        internal DevExpress.XtraEditors.TextEdit TxtWorkPeriodMth;
        internal DevExpress.XtraEditors.TextEdit TxtWorkPeriodYr;
        private System.Windows.Forms.Label label12;
        internal DevExpress.XtraEditors.LookUpEdit LueLevelCode;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label LblSectionNew;
        internal DevExpress.XtraEditors.LookUpEdit LueSectionCode;
        internal DevExpress.XtraEditors.CheckEdit ChkActingOfficialNew;
        internal DevExpress.XtraEditors.LookUpEdit LuePositionStatusCode;
        private System.Windows.Forms.Label label19;
        public DevExpress.XtraEditors.SimpleButton BtnEmployeeRequestDocNo2;
        public DevExpress.XtraEditors.DateEdit DteResignDtNew;
        public DevExpress.XtraEditors.SimpleButton BtnEmployeeRequestDocNo;
        private System.Windows.Forms.Label label23;
        public DevExpress.XtraEditors.TextEdit TxtEmployeeRequestDocNo;
        internal DevExpress.XtraEditors.LookUpEdit LueSiteCode;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label47;
        internal DevExpress.XtraEditors.LookUpEdit LuePGCode;
        internal DevExpress.XtraEditors.LookUpEdit LuePosCode;
        public DevExpress.XtraEditors.TextEdit TxtEntityName;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label50;
        internal DevExpress.XtraEditors.LookUpEdit LueDeptCode;
        private System.Windows.Forms.Label label51;
        internal DevExpress.XtraEditors.LookUpEdit LuePayrunPeriod;
        internal DevExpress.XtraEditors.LookUpEdit LueGrdLvlCode;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label54;
        internal DevExpress.XtraEditors.LookUpEdit LueEmploymentStatus;
        private System.Windows.Forms.Label label55;
        internal DevExpress.XtraEditors.LookUpEdit LueSystemType;
        public DevExpress.XtraEditors.TextEdit TxtEmpCode;
        private System.Windows.Forms.Label label56;
        public DevExpress.XtraEditors.SimpleButton BtnEmpCode;
        private System.Windows.Forms.Label label57;
        public DevExpress.XtraEditors.TextEdit TxtEmpName;
        public DevExpress.XtraEditors.SimpleButton BtnEmpCode2;
        private System.Windows.Forms.TabPage TpgFile;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ProgressBar PbUpload3;
        private System.Windows.Forms.ProgressBar PbUpload2;
        private DevExpress.XtraEditors.CheckEdit ChkFile3;
        internal DevExpress.XtraEditors.TextEdit TxtFile3;
        public DevExpress.XtraEditors.SimpleButton BtnFile3;
        private DevExpress.XtraEditors.CheckEdit ChkFile2;
        internal DevExpress.XtraEditors.TextEdit TxtFile2;
        private System.Windows.Forms.Label label2;
        public DevExpress.XtraEditors.SimpleButton BtnFile2;
        private DevExpress.XtraEditors.CheckEdit ChkFile;
        private System.Windows.Forms.ProgressBar PbUpload;
        internal DevExpress.XtraEditors.TextEdit TxtFile;
        private System.Windows.Forms.Label label25;
        public DevExpress.XtraEditors.SimpleButton BtnFile;
        private System.Windows.Forms.OpenFileDialog OD;
        public DevExpress.XtraEditors.SimpleButton BtnLogWorkPeriod;
    }
}