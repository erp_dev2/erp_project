﻿#region Update
/*
    04/07/2017 [TKG] New reporting
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptPurchasedItemByIt : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmRptPurchasedItemByIt(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sl.SetLueYr(LueYr, string.Empty);
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                Sl.SetLueMth(LueMth);
                Sm.SetLue(LueMth, CurrentDateTime.Substring(4, 2));
                Sl.SetLueItCtCode(ref LueItCtCode, string.Empty);
                SetGrd();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 10;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Item's Code", 
                        "Item's Name", 
                        "Item's Category",
                        "Currency",
                        "Month 1",
                        
                        //6-9
                        "Month 2",
                        "Month 3",
                        "Month 4",
                        "Total"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        100, 200, 200, 80, 130, 
                        
                        //6-9
                        130, 130, 130, 150
                    }
                );

            Sm.GrdFormatDec(Grd1, new int[] { 5, 6, 7, 8, 9 }, 0);
            Sm.SetGrdProperty(Grd1, false);
        }

        private void ClearGrdTitle()
        {
            Grd1.Cols[5].Text = "Month 1";
            Grd1.Cols[6].Text = "Month 2";
            Grd1.Cols[7].Text = "Month 3";
            Grd1.Cols[8].Text = "Month 4";
        }

        private void SetGrdTitle(int c, string YrMth)
        {
            Grd1.Cols[c].Text = string.Concat(Sm.MonthName(YrMth.Substring(4, 2)), " ", Sm.Left(YrMth, 4));
        }

        private void SetSQL(ref StringBuilder SQL, string Filter1, string Filter2, string Index, bool IsUnion)
        {
            if (IsUnion) SQL.AppendLine(" Union All ");

            SQL.AppendLine("Select ItCode, ItCtCode, CurCode, ");
            if (Index == "1") SQL.AppendLine("Amt As Amt1, 0.00 As Amt2, 0.00 As Amt3, 0.00 As Amt4 ");
            if (Index == "2") SQL.AppendLine("0.00 As Amt1, Amt As Amt2, 0.00 As Amt3, 0.00 As Amt4 ");
            if (Index == "3") SQL.AppendLine("0.00 As Amt1, 0.00 As Amt2, Amt As Amt3, 0.00 As Amt4 ");
            if (Index == "4") SQL.AppendLine("0.00 As Amt1, 0.00 As Amt2, 0.00 As Amt3, Amt As Amt4 ");

            SQL.AppendLine("From (");

            SQL.AppendLine("Select T1.ItCode, T1.ItCtCode, T1.CurCode, ");
            SQL.AppendLine("T1.Amt+(T1.Amt*0.01*IfNull(T2.TaxRate, 0.00))+(T1.Amt*0.01*IfNull(T3.TaxRate, 0.00))+(T1.Amt*0.01*IfNull(T4.TaxRate, 0.00)) As Amt ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select D.ItCode, H.ItCtCode, ");
            SQL.AppendLine("    E.CurCode, A.TaxCode1, A.TaxCode2, A.TaxCode3, ");
            SQL.AppendLine("    G.Qty* ");
            SQL.AppendLine("    (F.UPrice- ");
            SQL.AppendLine("    (F.UPrice*B.Discount*0.01)- ");
            SQL.AppendLine("    ((1.00/B.Qty)*B.DiscountAmt)+ ");
            SQL.AppendLine("    ((1.00/B.Qty)*B.RoundingValue))  ");
            SQL.AppendLine("    As Amt ");
            SQL.AppendLine("    From TblPOHdr A ");
            SQL.AppendLine("    Inner Join TblPODtl B On A.DocNo=B.DocNo And B.CancelInd='N' ");
            SQL.AppendLine("    Inner Join TblPORequestDtl C On B.PORequestDocNo=C.DocNo And B.PORequestDNo=C.DNo ");
            SQL.AppendLine("    Inner Join TblMaterialRequestDtl D On C.MaterialRequestDocNo=D.DocNo And C.MaterialRequestDNo=D.DNo ");
            SQL.AppendLine("    Inner Join TblQtHdr E On C.QtDocNo=E.DocNo  ");
            SQL.AppendLine("    Inner Join TblQtDtl F On C.QtDocNo=F.DocNo And C.QtDNo=F.DNo ");
            SQL.AppendLine("    Inner Join ( ");
            SQL.AppendLine("        Select G2.DocNo, G2.DNo, Sum(QtyPurchase) As Qty ");
            SQL.AppendLine("        From TblPOHdr G1 ");
            SQL.AppendLine("        Inner Join TblPODtl G2 On G1.DocNo=G2.DocNo And G2.CancelInd='N' ");
            SQL.AppendLine("        Inner Join TblRecvVdDtl G3 On G2.DocNo=G3.PODocNo And G2.DNo=G3.PODNo And G3.CancelInd='N' And G3.Status='A' ");
            SQL.AppendLine("        Where G1.Status='A' ");
            SQL.AppendLine(Filter1.Replace("A.", "G1"));
            SQL.AppendLine("        And Left(G1.DocDt, 6)=@YrMth" + Index);
            SQL.AppendLine("        Group By G2.DocNo, G2.DNo ");
            SQL.AppendLine("    ) G On B.DocNo=G.DocNo And B.DNo=G.DNo ");
            SQL.AppendLine("    Inner Join TblItem H On D.ItCode=H.ItCode ");
            SQL.AppendLine(Filter1);
            SQL.AppendLine(Filter2);
            SQL.AppendLine("    Where A.Status In ('O', 'A') ");
            
            SQL.AppendLine("    And Left(A.DocDt, 6)=@YrMth" + Index);
            SQL.AppendLine(" ) T1 ");
            SQL.AppendLine("Left Join TblTax T2 On T1.TaxCode1=T2.TaxCode ");
            SQL.AppendLine("Left Join TblTax T3 On T1.TaxCode2=T3.TaxCode ");
            SQL.AppendLine("Left Join TblTax T4 On T1.TaxCode3=T4.TaxCode ");
            SQL.AppendLine(") Tbl ");
        }

        override protected void ShowData()
        {
            ClearGrdTitle();
            Sm.ClearGrd(Grd1, false);
            if (Sm.IsLueEmpty(LueYr, "Year") || Sm.IsLueEmpty(LueMth, "Month")) return;

            Cursor.Current = Cursors.WaitCursor;

            var DtTemp4 = Sm.ConvertDate(string.Concat(Sm.GetLue(LueYr), Sm.GetLue(LueMth), "01"));
            var DtTemp3 = DtTemp4.AddMonths(-1);
            var DtTemp2 = DtTemp4.AddMonths(-2);
            var DtTemp1 = DtTemp4.AddMonths(-3);

            var Dt4 = string.Concat(Sm.GetLue(LueYr), Sm.GetLue(LueMth));
            var Dt3 = String.Format("{0:yyyyMM}", DtTemp3);
            var Dt2 = String.Format("{0:yyyyMM}", DtTemp2);
            var Dt1 = String.Format("{0:yyyyMM}", DtTemp1);

            SetGrdTitle(5, Dt1);
            SetGrdTitle(6, Dt2);
            SetGrdTitle(7, Dt3);
            SetGrdTitle(8, Dt4);

            var cm = new MySqlCommand();
            string Filter1 = " ", Filter2 = string.Empty;

            Sm.CmParam<string>(ref cm, "@YrMth1", Dt1);
            Sm.CmParam<string>(ref cm, "@YrMth2", Dt2);
            Sm.CmParam<string>(ref cm, "@YrMth3", Dt3);
            Sm.CmParam<string>(ref cm, "@YrMth4", Dt4);

            if (ChkItCode.Checked)
                Sm.FilterStr(ref Filter1, ref cm, TxtItCode.Text, new string[] { "H.ItCode", "H.ItName"  });
            
            if (ChkItCtCode.Checked)
            {
                Filter2 = " And H.ItCtCode=@ItCtCode ";
                Sm.CmParam<string>(ref cm, "@ItCtCode", Sm.GetLue(LueItCtCode));
            }

            var subSQL = new StringBuilder();

            SetSQL(ref subSQL, Filter1, Filter2, "1", false);
            SetSQL(ref subSQL, Filter1, Filter2, "2", true);
            SetSQL(ref subSQL, Filter1, Filter2, "3", true);
            SetSQL(ref subSQL, Filter1, Filter2, "4", true);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select X1.ItCode, X3.ItName, X1.ItCtCode, X2.ItCtName, X1.CurCode, Sum(X1.Amt1) Amt1, Sum(X1.Amt2) Amt2, Sum(X1.Amt3) Amt3, Sum(X1.Amt4) Amt4 ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine(subSQL.ToString());
            SQL.AppendLine(") X1 ");
            SQL.AppendLine("Inner Join TblItemCategory X2 On X1.ItCtCode=X2.ItCtCode ");
            SQL.AppendLine("Inner Join TblItem X3 On X1.ItCode=X3.ItCode ");
            SQL.AppendLine("Group By X1.ItCode, X3.ItName, X1.ItCtCode, X2.ItCtName, X1.CurCode ");
            SQL.AppendLine("Order By X1.ItCode, X2.ItCtName, X1.ItCtCode, X1.CurCode; ");

            try
            {
                Sm.ShowDataInGrid(
                     ref Grd1, ref cm, SQL.ToString(),
                     new string[]
                        {
                            //0
                            "ItCode", 

                            //1-5
                            "ItName", "ItCtName", "CurCode", "Amt1", "Amt2", 
                            
                            //6-7
                            "Amt3", "Amt4"
                        },
                     (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                     {
                         Grd.Cells[Row, 0].Value = Row + 1;
                         Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                         Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                         Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                         Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                         Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                         Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                         Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                         Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                         Grd.Cells[Row, 9].Value = Sm.GetGrdDec(Grd, Row, 5) + Sm.GetGrdDec(Grd, Row, 6) + Sm.GetGrdDec(Grd, Row, 7) + Sm.GetGrdDec(Grd, Row, 8);
                     }, true, false, false, false
                 );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.HideSubtotals(Grd1);
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 5, 6, 7, 8, 9 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void LueItCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCtCode, new Sm.RefreshLue2(Sl.SetLueItCtCode), string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkItCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Item's category");
        }

        #endregion

        #endregion

    }
}
