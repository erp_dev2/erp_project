﻿#region Update
/*
    19/12/2020 [WED/IMS] new apps
    13/04/2021 [WED/IMS] untuk PR For Service, ketika SO Contract nya diisi, item nya hanya melihat item dari List of Item di SOContract nya
    14/04/2021 [WED/IMS] untuk PR For Service, ketika SO Contract nya diisi, balikin lagi ke keadaan semula (tanpa lihat list of item di SO Contract)
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmMaterialRequest3Dlg7 : RunSystem.FrmBase4
    {
        #region Field

        private FrmMaterialRequest3 mFrmParent;
        private string mSQL = string.Empty;
        private int mRow = 0;

        #endregion

        #region Constructor

        public FrmMaterialRequest3Dlg7(FrmMaterialRequest3 FrmParent, int Row)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mRow = Row;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sl.SetLueItCtCode(ref LueItCtCode, string.Empty);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 6;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Item's Code",
                    "",
                    "Local Code",
                    "Item's Name",
                    "Specification"
                },
                 new int[] 
                {
                    //0
                    50,

                    //1-5
                    120, 20, 120, 200, 250
                }
            );
            Sm.GrdColButton(Grd1, new int[] { 2 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 3, 4, 5 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select ItCode, ItCodeInternal, ItName, Specification ");
            SQL.AppendLine("From TblItem ");
            SQL.AppendLine("Where ActInd = 'Y' ");
            SQL.AppendLine("And ServiceItemInd = 'Y' ");
            if (mFrmParent.mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select 1 ");
                SQL.AppendLine("    From TblGroupItemCategory T ");
                SQL.AppendLine("    Where T.ItCtCode = ItCtCode ");
                SQL.AppendLine("    And GrpCode In ");
                SQL.AppendLine("    ( ");
                SQL.AppendLine("        Select GrpCode ");
                SQL.AppendLine("        From TblUser ");
                SQL.AppendLine("        Where UserCode = @UserCode ");
                SQL.AppendLine("     ) ");
                SQL.AppendLine(") ");
            }
            //if (mFrmParent.TxtSOCDocNo.Text.Length > 0 && mFrmParent.mMenuCodeForPRService)
            //{
            //    SQL.AppendLine("And ItCode In ( ");
            //    SQL.AppendLine("    Select ItCode ");
            //    SQL.AppendLine("    From TblSOContractDtl ");
            //    SQL.AppendLine("    Where DocNo = @SOContractDocNo ");
            //    SQL.AppendLine("    And DocNo In ");
            //    SQL.AppendLine("    ( ");
            //    SQL.AppendLine("        Select DocNo ");
            //    SQL.AppendLine("        From TblSOContractHdr ");
            //    SQL.AppendLine("        Where DocNo = @SOContractDocNo ");
            //    SQL.AppendLine("        And CancelInd = 'N' ");
            //    SQL.AppendLine("        And Status = 'A' ");
            //    SQL.AppendLine("    ) ");
            //    SQL.AppendLine(") ");
            //}

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " And 0=0 ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@SOContractDocNo", mFrmParent.TxtSOCDocNo.Text);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "ItCode", "ItCodeInternal", "ItName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueItCtCode), "ItCtCode", true);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By ItName; ",
                    new string[] 
                    { "ItCode", "ItCodeInternal", "ItName", "Specification" },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.Grd1.Cells[mRow, 40].Value = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
                mFrmParent.Grd1.Cells[mRow, 41].Value = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 3);
                mFrmParent.Grd1.Cells[mRow, 42].Value = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 4);
                mFrmParent.Grd1.Cells[mRow, 43].Value = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 5);

                this.Close();
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void LueItCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCtCode, new Sm.RefreshLue2(Sl.SetLueItCtCode), string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkItCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Item's category");
        }

        #endregion

        #endregion

    }
}
