﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPublisher : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "";
        internal FrmPublisherFind FrmFind;

        #endregion

        #region Constructor

        public FrmPublisher(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            BtnPrint.Visible = false;
            SetFormControl(mState.View);
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtPbsCode, TxtPbsEmpCode, TxtEmpName, TxtDeptName, TxtPosition, TxtRegisterNo, DtePbsDt, MeeLocation
                    }, true);
                    TxtPbsCode.Focus();
                    ChkActiveInd.Properties.ReadOnly = true;
                    BtnEmpCode.Enabled = false;
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtPbsCode, TxtRegisterNo, DtePbsDt, MeeLocation
                    }, false);
                    TxtPbsCode.Focus();
                    ChkActiveInd.Checked = true;
                    BtnEmpCode.Enabled = true;
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtPbsCode, TxtPbsEmpCode, TxtEmpName, TxtDeptName, TxtPosition, TxtRegisterNo, DtePbsDt, MeeLocation
                    }, true);
                    ChkActiveInd.Properties.ReadOnly = false;
                    ChkActiveInd.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
               TxtPbsCode, TxtPbsEmpCode, TxtEmpName, TxtDeptName, TxtPosition, TxtRegisterNo, DtePbsDt, MeeLocation
            });
            ChkActiveInd.Checked = false;
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
           if (FrmFind == null) FrmFind = new FrmPublisherFind(this);
           Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
            DtePbsDt.DateTime = Sm.ConvertDate(Sm.ServerCurrentDateTime());
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtPbsCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into TblPublisher(PbsCode, PbsEmpCode, PbsDt, ActInd, PbsRegisterNo, PbsLocation, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@PbsCode, @PbsEmpCode, @PbsDt, @ActInd, @PbsRegisterNo, @PbsLocation, @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("  update ActInd=@ActInd, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@PbsCode", TxtPbsCode.Text);
                Sm.CmParam<String>(ref cm, "@PbsEmpCode", TxtPbsEmpCode.Text);
                Sm.CmParamDt(ref cm, "@PbsDt", Sm.GetDte(DtePbsDt));
                Sm.CmParam<String>(ref cm, "@ActInd", ChkActiveInd.Checked ? "Y" : "N");
                Sm.CmParam<String>(ref cm, "@PbsRegisterNo", TxtRegisterNo.Text);
                Sm.CmParam<String>(ref cm, "@PbsLocation", MeeLocation.Text);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.ExecCommand(cm);

                ShowData(TxtPbsCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string PbsCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@PbsCode", PbsCode);
                Sm.ShowDataInCtrl(
                        ref cm,
                        "Select A.PbsCode, A.PbsEmpCode, B.EmpName, C.Deptname, D.PosName, A.PbsDt, A.ActInd, A.PbsRegisterNo, A.PbsLocation From TblPublisher A " +
                        "Inner Join tblEmployee B On A.PbsEmpCode = B.EmpCode "+
                        "Inner join tblDepartment C On B.DeptCode = C.DeptCode "+
                        "Left join TblPosition D On B.PosCode = D.PosCode "+
                        "Where A.PbsCode=@PbsCode",
                        new string[] 
                        {
                           "PbsCode", 
                           "PbsEmpCode", "EmpName", "Deptname", "posName", "PbsDt", 
                           "ActInd", "PbsRegisterNo", "PbsLocation"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtPbsCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtPbsEmpCode.EditValue = Sm.DrStr(dr, c[1]);
                            TxtEmpName.EditValue = Sm.DrStr(dr, c[2]);
                            TxtDeptName.EditValue = Sm.DrStr(dr, c[3]);
                            TxtPosition.EditValue = Sm.DrStr(dr, c[4]);
                            Sm.SetDte(DtePbsDt, Sm.DrStr(dr, c[5]));
                            ChkActiveInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[6]), "Y");
                            TxtRegisterNo.EditValue = Sm.DrStr(dr, c[7]);
                            MeeLocation.EditValue = Sm.DrStr(dr, c[8]);
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtPbsCode, "Publisher code", false) ||
                Sm.IsTxtEmpty(TxtPbsEmpCode, "Publisher name", false) ||
                Sm.IsDteEmpty(DtePbsDt, "Publiser date") ||
                Sm.IsTxtEmpty(TxtRegisterNo, "Register number", false) ||
                Sm.IsMeeEmpty(MeeLocation, "Location")||
                IsPbsCodeExisted()||
                IsPbsCodeAlreadyNonActive();
        }

        private bool IsPbsCodeExisted()
        {
            if (!TxtPbsCode.Properties.ReadOnly && Sm.IsDataExist("Select PbsCode From Tblpublisher Where PbsCode='" + TxtPbsCode.Text + "' Limit 1"))
            {
                Sm.StdMsg(mMsgType.Warning, "Publisher code ( " + TxtPbsCode.Text + " ) already existed.");
                return true;
            }
            return false;
        }

        private bool IsPbsCodeAlreadyNonActive()
        {
            if (Sm.IsDataExist("Select PbsCode From Tblpublisher Where PbsCode='" + TxtPbsCode.Text + "' And ActInd ='N' Limit 1"))
            {
                Sm.StdMsg(mMsgType.Warning, "Publisher code ( " + TxtPbsCode.Text + " ) already non active.");
                return true;
            }
            return false;
        }

        #endregion

        private void BtnEmpCode_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmPublisherDlg(this));
        }

        #endregion
    }
}
