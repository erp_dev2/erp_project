﻿#region Update
/*
    02/05/2020 [TKG/IMS] tambah bank account, tambahan tax untuk inventory
    10/06/2020 [DITA/IMS] Validasi ketika data sudah di incoming payment kan maka tidak bisa di cancel
    12/06/2020 [DITA/IMS] rombak termin dp 2
    18/06/2020 [DITA/IMS] perhitungan outstanding amount dilakukan on the fly --> validasi saat isi termindp(1)
    02/07/2020 [DITA/IMS] Tambah jurnal untuk termin/dp II
    07/07/2020 [WED/IMS] journal IMS, project code diganti customer code
    09/07/2020 [VIN/IMS] printout baru
    06/08/2020 [VIN/IMS] Show data termin DP 2 yang SO Contract nya belum di termin DP 1 kan 
    24/09/2020 [WED/IMS] tambah informasi No dari SO Contract berdasarkan parameter IsDetailShowColumnNumber. Remark header bisa di edit berkali kali berdasarkan parameter IsRemarkHdrEditableOvertime
    27/10/2020 [DITA/IMS] Perubahan printout termin/dp 2 disamakan dengan termin/dp 1
    27/10/2020 [DITA/IMS] perubahan rumus perhitungan outstanding amount
    02/11/2020 [DITA/IMS] Penambahan quality of goods di printout
    18/11/2020 [IBL/IMS] perubahan rumus perhitungan outstanding amount
    19/11/2020 [IBL/IMS] penambahan rumus nilai default pada kolom Termin DP1 di tab Service dan tab Inventory
    25/11/2020 [ICA/IMS] Mengubah description journal menjadi "Sales Invoice for Project : DocNo"
    03/12/2020 [DITA/IMS] Sales invoice for project sudah tidak terbentuk voucher lagi, semua yg berhubungan dgn vr di command. karna vr terbentuk melalui incoming payment
    07/12/2020 [DITA/IMS] ubah nama menu + bug di set grid penamaan kolom belom berubah
    16/12/2020 [DITA/IMS] Specification ambil dari remark detail so contract
    16/12/2020 [DITA/IMS] Printout -> ttd berdasarkan approval tertinggi
    17/12/2020 [DITA/IMS] Tambah approval
    27/01/2021 [IBL/IMS] Tambah inputan local docno dan due date
    01/02/2021 [WED/IMS] bisa tarik DO Verify yang dari DO Customer
    29/03/2021 [VIN/IMS] event edit value changed di Lue tax code 2 dan 4
    02/04/2021 [WED/IMS] di journal nya tambah save informasi SO Contract
    18/05/2021 [DITA/IMS] untuk tax pada journal diubah menjadi nilai credit (mau itu minus atau plus)
    24/05/2021 [VIN/IMS] ganti validasi IsGrdEmpty
    10/06/2021 [ICA/IMS] menambah kolom Rate, Journal dikalikan dengan Rate bukan ambil dari tblcurrencyrate
    10/06/2021 [BRI/IMS] otomatisasi nilai Tax di service/inventory
    11/06/2021 [BRI/IMS] Menambahkan "SO Contract amount after tax"
    13/06/2021 [TKG/IMS] tambah amount before tax
    13/06/2021 [TKG/IMS] Merubah rumus outstanding di tab soc information menjadi amount bfr tax -ardp bfr tax - amt sli bfr tax yang sudah pernah dibuat dengan soc yang sama
    16/06/2021 [VIN/IMS] tambah item dismantle
    17/06/2021 [TKG/IMS] merubah tampilan layar
    23/06/2021 [BRI/IMS] memunculkan local doc di detail tab inventory dan service
    23/06/2021 [HAR/IMS] BUG : ComputeOutstandingAmt Nilai outstanding masih salah karena dikalikan nilai prosentase tanpa dibagi 100
    29/06/2021 [ICA/IMS] penyesuaian journal : Uang Muka dikalikan dengan ExcRate ARDP
    05/07/2021 [ICA/IMS] pembulatan TerminDP saat pembentukan journal
    23/07/2021 [IBL/IMS] Saat savejournal, jika ada coa kepala 5 dan cost center kosong, maka CC otomatis terisi costcenter keuangan.
    29/07/2021 [IBL/IMS] BUG: journal cancel belum menyimpan CCCode
    30/07/2021 [VIN/IMS] feedback source journal socontractdp2 / sli for project
    16/09/2021 [IBL/ALL] validasi coa kosong saat savejournal, berdasarkan parameter IsCheckCOAJournalNotExists
 */
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using System.IO;

#endregion

namespace RunSystem
{
    public partial class FrmSOContractDownpayment2 : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mDocNo = string.Empty, mMainCurCode = string.Empty;
        private decimal mOutstandingAmt = 0m;
        private bool
            mIsAutoJournalActived = false,
            mIsRemarkHdrEditableOvertime = false,
            mIsCheckCOAJournalNotExists = false;
        internal FrmSOContractDownpayment2Find FrmFind;
        internal bool mIsDetailShowColumnNumber = false;
        private string mCostCenterFormulaForAPAR = string.Empty;
        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmSOContractDownpayment2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Sales Invoice For Project";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                Sl.SetLueCurCode(ref LueCurCode);
                Sm.SetLue(LueCurCode, mMainCurCode);
                Sl.SetLueBankAcCode(ref LueBankAcCode);

                Tc1.SelectedTabPage = Tp2;
                Sl.SetLueTaxCode(new List<DevExpress.XtraEditors.LookUpEdit> { LueTaxCode, LueTaxCode4 });

                Tc1.SelectedTabPage = Tp3;
                SetLueOptionCode(ref LueOption);
                LueOption.Visible = false;
                Tc1.SelectedTabPage = Tp1;
                Sl.SetLueTaxCode(new List<DevExpress.XtraEditors.LookUpEdit> { LueTaxCode2, LueTaxCode3 });

                Tc1.SelectedTabPage = Tp1;

                SetFormControl(mState.View);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }

                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void GetParameter()
        {
            mMainCurCode = Sm.GetParameter("MainCurCode");
            mIsAutoJournalActived = Sm.GetParameterBoo("IsAutoJournalActived");
            mIsRemarkHdrEditableOvertime = Sm.GetParameterBoo("IsRemarkHdrEditableOvertime");
            mIsDetailShowColumnNumber = Sm.GetParameterBoo("IsDetailShowColumnNumber");
            mCostCenterFormulaForAPAR = Sm.GetParameter("CostCenterFormulaForAPAR");
            mIsCheckCOAJournalNotExists = Sm.GetParameterBoo("IsCheckCOAJournalNotExists");

            if (mCostCenterFormulaForAPAR.Length == 0) mCostCenterFormulaForAPAR = "1";
        }

        private void SetGrd()
        {
            #region Grid 1 - Service
            Grd1.Cols.Count = 25;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                  Grd1,
                  new string[] 
                    {
                        //0
                        "DNo",
                        
                        //1-5
                        "",
                        "Verification#",
                        "Verification DNo#",
                        "",
                        "Warehouse",

                        //6-10
                        "Item's Code",
                        "Item's Name",
                        "Local Code",
                        "Specification",
                        "Batch#",
                        
                        //11-15
                        "Source",
                        "Quantity",
                        "UoM",
                        "Price",
                        "Amount",

                        //16-20
                        "Amount DP",
                        "Amount Sales Invoice",
                        "Tax(1)",
                        "Tax(2)",
                        "After Tax",

                        //21-24
                        "SO Contract's"+Environment.NewLine+"No", 
                        "Item's Code"+Environment.NewLine+"Dismantle",
                        "Item's Name"+Environment.NewLine+"Dismantle",
                        "Local DO"+Environment.NewLine+"Verification"
                    },
                   new int[] 
                    {
                        //0
                        0,
 
                        //1-5
                        20, 150, 0, 20, 200,
                        
                        //6-10
                        130, 200, 100, 200, 180,
                        
                        //11-15
                        180, 120, 80, 130, 130,
                        
                        //16-20
                        130, 130, 130, 130, 130, 

                        //21-24
                        100, 150, 200, 150             
                    }
              );
            Sm.GrdColButton(Grd1, new int[] { 1, 4 });
            Sm.GrdFormatDec(Grd1, new int[] { 12, 14, 15, 16, 17, 18, 19, 20}, 0);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24 });
            if (!mIsDetailShowColumnNumber) Sm.GrdColInvisible(Grd1, new int[] { 21 });
            Grd1.Cols[21].Move(7);
            Grd1.Cols[22].Move(9);
            Grd1.Cols[23].Move(10);
            Grd1.Cols[24].Move(3);
            #endregion

            #region Grid 2 - Inventory
            Grd2.Cols.Count = 23;
            Grd2.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd2,
                new string[] 
                    {
                        //0
                        "DNo",
                        
                        //1-5
                        "",
                        "Verification#",
                        "Verification DNo#",
                        "",
                        "Warehouse",

                        //6-10
                        "Item's Code",
                        "Item's Name",
                        "Local Code",
                        "Specification",
                        "Batch#",
                        
                        //11-15
                        "Source",
                        "Quantity",
                        "UoM",
                        "Price",
                        "Amount",

                        //16-20
                        "Amount DP",
                        "Amount Sales Invoice",
                        "Tax(1)",
                        "Tax(2)",
                        "After Tax",

                        //21-22
                        "SO Contract's" + Environment.NewLine + "No",
                        "Local DO"+Environment.NewLine+"Verification"
                    },
                 new int[] 
                    {
                        //0
                        0,
 
                        //1-5
                        20, 150, 0, 20, 200,
                        
                        //6-10
                        130, 200, 100, 200, 180,
                        
                        //11-15
                        180, 120, 80, 130, 130, 
                        
                        //16-20
                        130, 130, 130, 130, 200,

                        //21-22
                        100, 150
                    }
            );
            Sm.GrdColButton(Grd2, new int[] { 1, 4 });
            Sm.GrdFormatDec(Grd2, new int[] { 12, 14, 15, 16, 17, 18, 19, 20 }, 0);
            Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22 });
            if (!mIsDetailShowColumnNumber) Sm.GrdColInvisible(Grd2, new int[] { 21 });
            Grd2.Cols[21].Move(7);
            Grd2.Cols[22].Move(3);
            #endregion

            #region Grid 3 - Add Discount & Tax
            Grd3.Cols.Count = 11;
            Grd3.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd3,
                    new string[] 
                    {
                        //0
                        "",

                        //1-5
                        "Account#",
                        "Description",
                        "",
                        "Debit"+Environment.NewLine+"Amount",
                        "",

                        //6-10
                        "Credit"+Environment.NewLine+"Amount",
                        "OptCode",
                        "Option",
                        "Remark",
                        ""
                    },
                     new int[] 
                    {
                        //0
                        20,

                        //1-5
                        150, 300, 20, 100, 20, 
                        //6-10
                        100, 0, 0, 400, 0
                    }
                );
            Sm.GrdColButton(Grd3, new int[] { 0 });
            Sm.GrdColCheck(Grd3, new int[] { 3, 5, 10 });
            Sm.GrdColReadOnly(true, true, Grd3, new int[] { 0, 1, 2, 4, 6, 7, 8, 9, 10 });
            Sm.GrdFormatDec(Grd3, new int[] { 4, 6 }, 0);
            Sm.GrdColInvisible(Grd3, new int[] { 1, 7, 10 }, false);
            #endregion
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, MeeCancelReason, ChkCancelInd, MeeRemark, TxtTaxInvoiceNo, 
                        TxtTaxInvoiceNo2, TxtTaxInvoiceNo3, DteTaxInvoiceDt, DteTaxInvoiceDt2, DteTaxInvoiceDt3, 
                        LueTaxCode, LueTaxCode2, LueTaxCode3, LueBankAcCode, TxtTaxInvoiceNo4, LueTaxCode4, DteTaxInvoiceDt4,
                        TxtCurrentAmt, TxtOthersAmt, TxtBalanceAmt, MeeSKBDN,
                        MeeOriginCountry, MeeQualityOfGoods, TxtLocalDocNo, DteDueDt, TxtRate
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 16 });
                    Sm.GrdColReadOnly(true, true, Grd2, new int[] { 1, 16 });
                    Sm.GrdColReadOnly(true, true, Grd3, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 });
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, MeeRemark, TxtTaxInvoiceNo, TxtTaxInvoiceNo2, TxtTaxInvoiceNo3, 
                        DteTaxInvoiceDt, DteTaxInvoiceDt2, DteTaxInvoiceDt3, LueTaxCode, LueTaxCode2, 
                        LueTaxCode3, TxtTaxInvoiceNo4, LueTaxCode4, DteTaxInvoiceDt4, LueBankAcCode,
                        MeeSKBDN, MeeOriginCountry, MeeQualityOfGoods, TxtLocalDocNo, DteDueDt, TxtRate
                    }, false);
                    TxtRate.EditValue = Sm.FormatNum(1, 0);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1, 16 });
                    Sm.GrdColReadOnly(false, true, Grd2, new int[] { 1, 16 });
                    Sm.GrdColReadOnly(false, true, Grd3, new int[] { 0, 3, 4, 5, 6, 8, 9 });
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeCancelReason, ChkCancelInd }, false);
                    if (mIsRemarkHdrEditableOvertime) Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeRemark }, false);
                    MeeCancelReason.Focus();
                    break;
            }
        }

        internal void ClearData()
        {
            ChkCancelInd.Checked = false;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, MeeCancelReason, TxtCtCode, LueCurCode, 
                MeeRemark, TxtTaxInvoiceNo, TxtTaxInvoiceNo2, TxtTaxInvoiceNo3, DteTaxInvoiceDt, 
                DteTaxInvoiceDt2, DteTaxInvoiceDt3, LueTaxCode, LueTaxCode2, LueTaxCode3, TxtSOContractDocNo,
                TxtSOContractDownpaymentDocNo, TxtProjectCode, TxtProjectName, TxtPONo, MeeSOContractRemark, LueBankAcCode, TxtPtName,
                MeeSKBDN, MeeOriginCountry, MeeQualityOfGoods, TxtLocalDocNo, DteDueDt
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
            {
                TxtAmt, TxtTaxAmt, TxtTaxAmt2, TxtTaxAmt3, TxtTaxAmt4,
                TxtSOContractDownpaymentAmt, TxtOutstandingAmt, TxtSOContractAmt, TxtCurrentAmt, 
                TxtOthersAmt, TxtBalanceAmt, TxtSOContractAmtAfterTax, TxtAmtBeforeTax
            }, 0);
            TxtRate.EditValue = Sm.FormatNum(1, 0);
            ClearGrd1();
            ClearGrd2();
            ClearGrd3();
        }

        internal void ClearGrd1()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 12, 14, 15, 16, 17, 18, 19, 20 });
            Sm.FocusGrd(Grd1, 0, 0);
        }

        internal void ClearGrd2()
        {
            Sm.ClearGrd(Grd2, true);
            Sm.SetGrdNumValueZero(ref Grd2, 0, new int[] { 12, 14, 15, 16, 17, 18, 19, 20 });
            Sm.FocusGrd(Grd2, 0, 0);
        }

        internal void ClearGrd3()
        {
            Grd3.Rows.Clear();
            Grd3.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd3, 0, new int[] { 4, 6 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmSOContractDownpayment2Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                TxtStatus.EditValue = "Outstanding";
                Sm.SetDteCurrentDate(DteDocDt);
                Sm.SetLue(LueCurCode, mMainCurCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, string.Empty, false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }
        private void BtnPrint_Click(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false) || Sm.StdMsgYN("Print", string.Empty) == DialogResult.No) return;
            ParPrint();
        }
        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            ComputeOutstandingAmt();
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "SOContractDownpayment2", "TblSOContractDownpayment2Hdr");
            //string VoucherRequestDocNo = GenerateVoucherRequestDocNo(); 
            var cml = new List<MySqlCommand>();

            cml.Add(SaveSOContractDownpayment2(DocNo));
            if (Grd3.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd3.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd3, Row, 1).Length > 0) cml.Add(SaveSOContractDownpayment2Dtl3(DocNo, Row));
            }

            if (mIsAutoJournalActived) cml.Add(SaveJournal(DocNo));
            Sm.ExecCommands(cml);
            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsClosingJournalInvalid(Sm.GetDte(DteDocDt)) ||
                Sm.IsLueEmpty(LueBankAcCode, "Bank account") ||
                IsGrdEmpty() ||
                IsDoHasBeenChosee() ||
                IsGrdValueNotValid() ||
                IsCurCodeInvalid() ||
                IsJournalSettingInvalid();
        }

        private bool IsDoHasBeenChosee()
        {
            string DocNoDNo1 = string.Empty, DocNoDNo2 = string.Empty;
            var SQL = new StringBuilder();
            var SQL2 = new StringBuilder();

            SQL.AppendLine("Select A.DocNo ");
            SQL.AppendLine("From TblSOContractDownpayment2Hdr A ");
            SQL.AppendLine("Inner Join TblSOCOntractDownpayment2Dtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("    And A.CancelInd = 'N' And A.Status In ('O', 'A') ");
            SQL.AppendLine("    And Find_In_Set(Concat(B.DOCtVerifyDocNo, B.DOCtVerifyDNo), @Param) ");
            SQL.AppendLine("Limit 1; ");

            SQL2.AppendLine("Select A.DocNo ");
            SQL2.AppendLine("From TblSOContractDownpayment2Hdr A ");
            SQL2.AppendLine("Inner Join TblSOCOntractDownpayment2Dtl2 B On A.DocNo = B.DocNo ");
            SQL2.AppendLine("    And A.CancelInd = 'N' And A.Status In ('O', 'A') ");
            SQL2.AppendLine("    And Find_In_Set(Concat(B.DOCtVerifyDocNo, B.DOCtVerifyDNo), @Param) ");
            SQL2.AppendLine("Limit 1; ");

            if(Grd1.Rows.Count > 1)
            {
                for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
                {
                    if (DocNoDNo1.Length > 0) DocNoDNo1 += ",";
                    DocNoDNo1 += Sm.GetGrdStr(Grd1, i, 2) + Sm.GetGrdStr(Grd1, i, 3);
                }

                if (DocNoDNo1.Length > 0)
                {
                    if (Sm.IsDataExist(SQL.ToString(), DocNoDNo1))
                    {
                        Sm.StdMsg(mMsgType.Warning, "One of this DO document is already processed in another document#" + Sm.GetValue(SQL.ToString(), DocNoDNo1));
                        return true;
                    }
                }
            }

            if (Grd2.Rows.Count > 1)
            {
                for (int i = 0; i < Grd2.Rows.Count - 1; ++i)
                {
                    if (DocNoDNo2.Length > 0) DocNoDNo2 += ",";
                    DocNoDNo2 += Sm.GetGrdStr(Grd2, i, 2) + Sm.GetGrdStr(Grd2, i, 3);
                }

                if (DocNoDNo2.Length > 0)
                {
                    if (Sm.IsDataExist(SQL2.ToString(), DocNoDNo2))
                    {
                        Sm.StdMsg(mMsgType.Warning, "One of this DO document is already processed in another document#" + Sm.GetValue(SQL2.ToString(), DocNoDNo2));
                        return true;
                    }
                }
            }

            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count <= 1 && Grd2.Rows.Count <= 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 DO#.");
                return true;
            }
           
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            decimal mTDP1Amt = 0m,
             mTDP1Amt2 = 0m,
             mOutstandingAfterInsertTDP1Amt1 = 0m,
             mOutstandingAfterInsertTDP1Amt2 = 0m;
            if (Grd1.Rows.Count > 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.IsGrdValueEmpty(Grd1, Row, 17, false, "Termin DP(2) could not be empty or zero.")) { return true; }
                    mTDP1Amt += Sm.GetGrdDec(Grd1, Row, 16);
                }
                mOutstandingAfterInsertTDP1Amt1 = mOutstandingAmt - mTDP1Amt;
                if (mOutstandingAfterInsertTDP1Amt1 < 0)
                {
                    Sm.StdMsg(mMsgType.Warning,
                   "Total Termin DP(1) cannot bigger than Outstanding Amount ." +Environment.NewLine +Environment.NewLine +
                   "Total Termin DP(1) : Rp. " + Sm.FormatNum(mTDP1Amt, 0) + Environment.NewLine +
                   "Total Outstanding Amount : Rp. " + Sm.FormatNum(mOutstandingAmt, 0));
                    return true;
                }
            }
            if (Grd2.Rows.Count > 0)
            {
                for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                {
                    if (Sm.IsGrdValueEmpty(Grd2, Row, 17, false, "Termin DP(2) could not be empty or zero.")) { return true; }
                    mTDP1Amt2 += Sm.GetGrdDec(Grd2, Row, 16);
                }
                mOutstandingAfterInsertTDP1Amt2 = mOutstandingAmt - mTDP1Amt2;
                if (mOutstandingAfterInsertTDP1Amt2 < 0)
                {
                   Sm.StdMsg(mMsgType.Warning,
                   "Total Termin DP(1) cannot bigger than Outstanding Amount." + Environment.NewLine + Environment.NewLine +
                   "Total Termin DP(1) : Rp. " + Sm.FormatNum(mTDP1Amt2, 0) + Environment.NewLine +
                   "Total Outstanding Amount : Rp. " + Sm.FormatNum(mOutstandingAmt, 0));
                    return true;
                }
            }

            return false;
        }

        private bool IsCurCodeInvalid()
        {
            if (Sm.GetLue(LueCurCode) == mMainCurCode)
            {
                if (decimal.Parse(TxtRate.Text) != 1)
                {
                    Sm.StdMsg(mMsgType.Warning, "Rate must be 1,00");
                    return true;
                }
                return false;
            }

            if (Sm.GetLue(LueCurCode) != mMainCurCode)
            {
                if (decimal.Parse(TxtRate.Text) == 1)
                {
                    Sm.StdMsg(mMsgType.Warning, "Rate can't be 1,00");
                    return true;
                }
                return false;
            }
            return false;
        }

        private bool IsJournalSettingInvalid()
        {
            if (!mIsAutoJournalActived || !mIsCheckCOAJournalNotExists) return false;

            string
                CustomerAcNoAR = Sm.GetParameter("CustomerAcNoAR"),
                CustomerAcNoDownpayment = Sm.GetParameter("CustomerAcNoDownpayment"),
                AcNoForForeignCurrencyExchangeGains = Sm.GetParameter("AcNoForForeignCurrencyExchangeGains"),
                CustomerAcNoNonInvoice = Sm.GetParameter("CustomerAcNoNonInvoice");

            var Msg =
                    "Journal's setting is invalid." + Environment.NewLine +
                    "Please contact Finance/Accounting department." + Environment.NewLine;

            //parameter
            if (CustomerAcNoAR.Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, Msg + "Parameter CustomerAcNoAR is empty.");
                return true;
            }
            if (CustomerAcNoDownpayment.Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, Msg + "Parameter CustomerAcNoDownpayment is empty.");
                return true;
            }
            if (AcNoForForeignCurrencyExchangeGains.Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, Msg + "Parameter AcNoForForeignCurrencyExchangeGains is empty.");
                return true;
            }
            if (CustomerAcNoNonInvoice.Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, Msg + "Parameter CustomerAcNoNonInvoice is empty.");
                return true;
            }

            //table
            if (IsJournalSettingInvalid_Tax(Msg, "AcNo2")) return true;

            if (IsJournalSettingInvalid_DiscCost(Msg)) return true;

            return false;
        }

        private bool IsJournalSettingInvalid_Tax(string Msg, string AcNoCol)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string TaxName = string.Empty;

            SQL.AppendLine("Select TaxName From TblTax Where " + AcNoCol + " Is Null ");
            SQL.AppendLine("And TaxCode In (@TaxCode1,@TaxCode2,@TaxCode3,@TaxCode4) ");
            SQL.AppendLine("Limit 1; ");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<string>(ref cm, "@TaxCode1", Sm.GetLue(LueTaxCode));
            Sm.CmParam<string>(ref cm, "@TaxCode2", Sm.GetLue(LueTaxCode2));
            Sm.CmParam<string>(ref cm, "@TaxCode3", Sm.GetLue(LueTaxCode3));
            Sm.CmParam<string>(ref cm, "@TaxCode4", Sm.GetLue(LueTaxCode4));

            TaxName = Sm.GetValue(cm);

            if (TaxName.Length > 0)
            {
                Sm.StdMsg(mMsgType.Warning, Msg + (AcNoCol == "AcNo" ? "Input" : "Output") + " Tax's COA Account  (" + TaxName + ") is empty.");
                return true;
            }

            return false;
        }

        private bool IsJournalSettingInvalid_DiscCost(string Msg)
        {
            if (Grd3.Rows.Count > 1)
            {
                for (int row = 0; row < Grd3.Rows.Count - 1; row++)
                {
                    if (Sm.GetGrdStr(Grd3, row, 1).Length == 0)
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Disc, Cost, Etc COA Account " + Sm.GetGrdStr(Grd3, row, 2) + " is empty.");
                    }
                }
            }

            return false;
        }

        private MySqlCommand SaveSOContractDownpayment2(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string r = string.Empty;

            SQL.AppendLine("Set @Dt:=CurrentDateTime();");

            SQL.AppendLine("Insert Into TblSOContractDownpayment2Hdr(DocNo, DocDt, Status, CancelReason, CancelInd, CurCode, SOContractDocNo, SOContractDownpaymentDocNo, OutstandingAmt, Amt, BankAcCode, Remark, ");
            SQL.AppendLine("TaxInvoiceNo, TaxInvoiceNo2, TaxInvoiceNo3, TaxInvoiceNo4, ");
            SQL.AppendLine("TaxInvoiceDt, TaxInvoiceDt2, TaxInvoiceDt3, TaxInvoiceDt4, ");
            SQL.AppendLine("TaxCode, TaxCode2, TaxCode3, TaxCode4, ");
            SQL.AppendLine("TaxAmt, TaxAmt2, TaxAmt3, TaxAmt4, OthersAmt, CurrentAmt, BalanceAmt, SKBDN, OriginCountry, QualityOfGoods, LocalDocNo, DueDt, ExcRate, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'O', Null, 'N', @CurCode, @SOContractDocNo, @SOContractDownpaymentDocNo, @OutstandingAmt, @Amt, @BankAcCode, @Remark, ");
            SQL.AppendLine("@TaxInvoiceNo, @TaxInvoiceNo2, @TaxInvoiceNo3, @TaxInvoiceNo4, ");
            SQL.AppendLine("@TaxInvoiceDt, @TaxInvoiceDt2, @TaxInvoiceDt3, @TaxInvoiceDt4, ");
            SQL.AppendLine("@TaxCode, @TaxCode2, @TaxCode3, @TaxCode4,  ");
            SQL.AppendLine("@TaxAmt, @TaxAmt2, @TaxAmt3, @TaxAmt4, @OthersAmt, @CurrentAmt, @BalanceAmt, @SKBDN, @OriginCountry, @QualityOfGoods, @LocalDocNo, @DueDt, @ExcRate, ");
            SQL.AppendLine("@UserCode, @Dt); ");

            if (IsNeedApproval())
            {
                SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
                SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @UserCode, CurrentDateTime() ");
                SQL.AppendLine("From TblDocApprovalSetting T Where T.DocType='SOCD2' ");

                SQL.AppendLine("And (T.StartAmt=0 ");
                SQL.AppendLine("Or T.StartAmt<=IfNull(( ");
                SQL.AppendLine("    Select (ifnull(B.Amt, 0)+ ifnull(B.AmtBOM, 0)) SOContractAmt ");
                SQL.AppendLine("    From TblSOContractDownpayment2Hdr A ");
                SQL.AppendLine("    Inner Join TblSOContractHdr B On A.SOContractDocno = B.DocNo  ");
                SQL.AppendLine("    Where A.DocNo = @DocNo ");
                SQL.AppendLine("), 0)) ");

                SQL.AppendLine("; ");
            }

            SQL.AppendLine("Update TblSOContractDownpayment2Hdr ");
            SQL.AppendLine("Set Status = 'A' ");
            SQL.AppendLine("Where DocNo = @DocNo ");
            SQL.AppendLine("And Not Exists ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select DocNo ");
            SQL.AppendLine("    From TblDocApproval ");
            SQL.AppendLine("    Where DocNo = @DocNo ");
            SQL.AppendLine("    And DocType = 'SOCD2' ");
            SQL.AppendLine(");     ");

            for (int row = 0; row < Grd1.Rows.Count; row++)
            {
                if (Sm.GetGrdStr(Grd1, row, 2).Length > 0)
                {
                    r = row.ToString();

                    SQL.AppendLine("Insert Into TblSOContractDownpayment2Dtl(DocNo, DNo, DOCtVerifyDocNo, DOCtVerifyDNo, Qty, UPrice, Amt, TerminDP, TerminDP2, TaxAmt, TaxAmt2, AfterTax, CreateBy, CreateDt) ");
                    SQL.AppendLine("Values(@DocNo, @DNo_1_" + r + ", @DOCtVerifyDocNo_1_" + r + ", @DOCtVerifyDNo_1_" + r + ", @Qty_1_" + r + ", @UPrice_1_" + r + ", @Amt_1_" + r + ", @TerminDP_1_" + r + ", @TerminDP2_1_" + r + ", @TaxAmt_1_" + r + ", @TaxAmt2_1_" + r + ", @AfterTax_1_" + r + ", @UserCode, @Dt); ");

                    Sm.CmParam<String>(ref cm, "@DNo_1_" + r, Sm.Right(string.Concat("00000", (row + 1).ToString()), 6));
                    Sm.CmParam<String>(ref cm, "@DOCtVerifyDocNo_1_" + r, Sm.GetGrdStr(Grd1, row, 2));
                    Sm.CmParam<String>(ref cm, "@DOCtVerifyDNo_1_" + r, Sm.GetGrdStr(Grd1, row, 3));
                    Sm.CmParam<Decimal>(ref cm, "@Qty_1_" + r, Sm.GetGrdDec(Grd1, row, 12));
                    Sm.CmParam<Decimal>(ref cm, "@UPrice_1_" + r, Sm.GetGrdDec(Grd1, row, 14));
                    Sm.CmParam<Decimal>(ref cm, "@Amt_1_" + r, Sm.GetGrdDec(Grd1, row, 15));
                    Sm.CmParam<Decimal>(ref cm, "@TerminDP_1_" + r, Sm.GetGrdDec(Grd1, row, 16));
                    Sm.CmParam<Decimal>(ref cm, "@TerminDP2_1_" + r, Sm.GetGrdDec(Grd1, row, 17));
                    Sm.CmParam<Decimal>(ref cm, "@TaxAmt_1_" + r, Sm.GetGrdDec(Grd1, row, 18));
                    Sm.CmParam<Decimal>(ref cm, "@TaxAmt2_1_" + r, Sm.GetGrdDec(Grd1, row, 19));
                    Sm.CmParam<Decimal>(ref cm, "@AfterTax_1_" + r, Sm.GetGrdDec(Grd1, row, 20));
                }
            }

            for (int row = 0; row < Grd2.Rows.Count; row++)
            {
                if (Sm.GetGrdStr(Grd2, row, 2).Length > 0)
                {
                    r = row.ToString();

                    SQL.AppendLine("Insert Into TblSOContractDownpayment2Dtl2(DocNo, DNo, DOCtVerifyDocNo, DOCtVerifyDNo, Qty, UPrice, Amt, TerminDP, TerminDP2, TaxAmt, TaxAmt2, AfterTax, CreateBy, CreateDt) ");
                    SQL.AppendLine("Values(@DocNo, @DNo_2_" + r + ", @DOCtVerifyDocNo_2_" + r + ", @DOCtVerifyDNo_2_" + r + ", @Qty_2_" + r + ", @UPrice_2_" + r + ", @Amt_2_" + r + ", @TerminDP_2_" + r + ", @TerminDP2_2_" + r + ", @TaxAmt_2_" + r + ", @TaxAmt2_2_" + r + ", @AfterTax_2_" + r + ", @UserCode, @Dt); ");

                    Sm.CmParam<String>(ref cm, "@DNo_2_" + r, Sm.Right(string.Concat("00000", (row + 1).ToString()), 6));
                    Sm.CmParam<String>(ref cm, "@DOCtVerifyDocNo_2_" + r, Sm.GetGrdStr(Grd2, row, 2));
                    Sm.CmParam<String>(ref cm, "@DOCtVerifyDNo_2_" + r, Sm.GetGrdStr(Grd2, row, 3));
                    Sm.CmParam<Decimal>(ref cm, "@Qty_2_" + r, Sm.GetGrdDec(Grd2, row, 12));
                    Sm.CmParam<Decimal>(ref cm, "@UPrice_2_" + r, Sm.GetGrdDec(Grd2, row, 14));
                    Sm.CmParam<Decimal>(ref cm, "@Amt_2_" + r, Sm.GetGrdDec(Grd2, row, 15));
                    Sm.CmParam<Decimal>(ref cm, "@TerminDP_2_" + r, Sm.GetGrdDec(Grd2, row, 16));
                    Sm.CmParam<Decimal>(ref cm, "@TerminDP2_2_" + r, Sm.GetGrdDec(Grd2, row, 17));
                    Sm.CmParam<Decimal>(ref cm, "@TaxAmt_2_" + r, Sm.GetGrdDec(Grd2, row, 18));
                    Sm.CmParam<Decimal>(ref cm, "@TaxAmt2_2_" + r, Sm.GetGrdDec(Grd2, row, 19));
                    Sm.CmParam<Decimal>(ref cm, "@AfterTax_2_" + r, Sm.GetGrdDec(Grd2, row, 20));
                }
            }

           
            //SQL.AppendLine("Insert Into TblVoucherRequestHdr ");
            //SQL.AppendLine("(DocNo, DocDt, CancelInd, Status, ");
            //SQL.AppendLine("DeptCode, DocType, VoucherDocNo, AcType, BankAcCode, PaymentType, GiroNo, BankCode, OpeningDt, DueDt, PIC, DocEnclosure, CurCode, Amt, PaymentUser, Remark, CreateBy, CreateDt) ");
            //SQL.AppendLine("Values (@VoucherRequestDocNo, @DocDt, 'N', 'A', Null, ");
            //SQL.AppendLine("'64', Null, 'D', Null, Null, Null, Null, Null, Null, Null, 0, @CurCode, @Amt, Null, @Remark, @UserCode, CurrentDateTime()); ");

            //SQL.AppendLine("Insert Into TblVoucherRequestDtl(DocNo, DNo, Description, Amt, Remark, CreateBy, CreateDt) ");
            //SQL.AppendLine("Values (@VoucherRequestDocNo, '001', Concat('Termin/DP (II) : ', @DocNo, ' (', @CtName, '). '), @Amt, @Remark, @UserCode, CurrentDateTime()); ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            //Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo", VoucherRequestDocNo);
            Sm.CmParam<String>(ref cm, "@SOContractDocNo", TxtSOContractDocNo.Text);
            Sm.CmParam<String>(ref cm, "@SOContractDownpaymentDocNo", TxtSOContractDownpaymentDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CtName", TxtCtCode.Text);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<Decimal>(ref cm, "@OutstandingAmt", decimal.Parse(TxtOutstandingAmt.Text));
            Sm.CmParam<Decimal>(ref cm, "@Amt", decimal.Parse(TxtAmt.Text));
            Sm.CmParam<String>(ref cm, "@BankAcCode", Sm.GetLue(LueBankAcCode));
            Sm.CmParam<String>(ref cm, "@TaxCode", Sm.GetLue(LueTaxCode));
            Sm.CmParam<String>(ref cm, "@TaxInvoiceNo", TxtTaxInvoiceNo.Text);
            Sm.CmParamDt(ref cm, "@TaxInvoiceDt", Sm.GetDte(DteTaxInvoiceDt));
            Sm.CmParam<Decimal>(ref cm, "@TaxAmt", decimal.Parse(TxtTaxAmt.Text));
            Sm.CmParam<String>(ref cm, "@TaxCode2", Sm.GetLue(LueTaxCode2));
            Sm.CmParam<String>(ref cm, "@TaxInvoiceNo2", TxtTaxInvoiceNo2.Text);
            Sm.CmParamDt(ref cm, "@TaxInvoiceDt2", Sm.GetDte(DteTaxInvoiceDt2));
            Sm.CmParam<Decimal>(ref cm, "@TaxAmt2", decimal.Parse(TxtTaxAmt2.Text));
            Sm.CmParam<String>(ref cm, "@TaxCode3", Sm.GetLue(LueTaxCode3));
            Sm.CmParam<String>(ref cm, "@TaxInvoiceNo3", TxtTaxInvoiceNo3.Text);
            Sm.CmParamDt(ref cm, "@TaxInvoiceDt3", Sm.GetDte(DteTaxInvoiceDt3));
            Sm.CmParam<Decimal>(ref cm, "@TaxAmt3", decimal.Parse(TxtTaxAmt3.Text));
            Sm.CmParam<String>(ref cm, "@TaxCode4", Sm.GetLue(LueTaxCode4));
            Sm.CmParam<String>(ref cm, "@TaxInvoiceNo4", TxtTaxInvoiceNo4.Text);
            Sm.CmParamDt(ref cm, "@TaxInvoiceDt4", Sm.GetDte(DteTaxInvoiceDt4));
            Sm.CmParam<Decimal>(ref cm, "@TaxAmt4", decimal.Parse(TxtTaxAmt4.Text));
            Sm.CmParam<Decimal>(ref cm, "@OthersAmt", decimal.Parse(TxtOthersAmt.Text));
            Sm.CmParam<Decimal>(ref cm, "@CurrentAmt", decimal.Parse(TxtCurrentAmt.Text));
            Sm.CmParam<Decimal>(ref cm, "@BalanceAmt", decimal.Parse(TxtBalanceAmt.Text));
            Sm.CmParam<Decimal>(ref cm, "@ExcRate", decimal.Parse(TxtRate.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@SKBDN", MeeSKBDN.Text);
            Sm.CmParam<String>(ref cm, "@OriginCountry", MeeOriginCountry.Text);
            Sm.CmParam<String>(ref cm, "@QualityOfGoods", MeeQualityOfGoods.Text);
            Sm.CmParam<String>(ref cm, "@LocalDocNo", TxtLocalDocNo.Text);
            Sm.CmParamDt(ref cm, "@DueDt", Sm.GetDte(DteDueDt));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveSOContractDownpayment2Dtl3(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblSOContractDownpayment2Dtl3(DocNo, DNo, AcNo, DAmt, CAmt, OptAcDesc,  AcInd,  Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DNo, @AcNo, @DAmt, @CAmt, @OptAcDesc,  @AcInd, @Remark, @UserCode, CurrentDateTime()); "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@AcNo", Sm.GetGrdStr(Grd3, Row, 1));
            Sm.CmParam<Decimal>(ref cm, "@DAmt", Sm.GetGrdDec(Grd3, Row, 4));
            Sm.CmParam<Decimal>(ref cm, "@CAmt", Sm.GetGrdDec(Grd3, Row, 6));
            Sm.CmParam<String>(ref cm, "@OptAcDesc", Sm.GetGrdStr(Grd3, Row, 7));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd3, Row, 9));
            Sm.CmParam<String>(ref cm, "@AcInd", Sm.GetGrdBool(Grd3, Row, 10) ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            bool IsJournalDocNo2Exists = Sm.IsDataExist("Select 1 From TblDOCtVerifyHdr Where DocNo = @Param And JournalDocNo2 Is Not Null; ", TxtDocNo.Text);

            if (Sm.StdMsgYN("Save", string.Empty) == DialogResult.No || IsCancelledDataNotValid(IsJournalDocNo2Exists)) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditSOContractDownpaymentHdr());

            if (mIsAutoJournalActived && ChkCancelInd.Checked && !IsJournalDocNo2Exists) cml.Add(SaveJournal2());
            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsCancelledDataNotValid(bool IsJournalDocNo2Exists)
        {
            bool IsDataAlreadyCancelled = Sm.IsDataExist("Select 1 From TblSOContractDownpayment2Hdr Where CancelInd = 'Y' And DocNo = @Param; ", TxtDocNo.Text);
            bool ChangeToCancel = ChkCancelInd.Checked && !IsDataAlreadyCancelled;

            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDocNotCancelled() ||
                IsDataCancelledAlready()||
                IsDataAlreadyProcessedToIncomingPayment(ChangeToCancel) ||
                IsDataAlreadyProcessedToDPSettlement(ChangeToCancel) ||
                (!IsJournalDocNo2Exists && ChkCancelInd.Checked && Sm.IsClosingJournalInvalid(mIsAutoJournalActived, true, Sm.GetDte(DteDocDt)))
                ;
        }

        private bool IsDataAlreadyProcessedToIncomingPayment(bool ChangeToCancel)
        {
            if (!ChangeToCancel) return false;

            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 ");
            SQL.AppendLine("From TblIncomingPaymentHdr A ");
            SQL.AppendLine("Inner Join TblIncomingPaymentDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("Where B.InvoiceDocNo = @Param ");
            SQL.AppendLine("And A.CancelInd = 'N' ");
            SQL.AppendLine("Limit 1; ");

            if (Sm.IsDataExist(SQL.ToString(), TxtDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This data already processed to Incoming Payment.");
                return true;
            }

            return false;
        }

        private bool IsDataAlreadyProcessedToDPSettlement(bool ChangeToCancel)
        {
            if (!ChangeToCancel) return false;

            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 ");
            SQL.AppendLine("From tblsocontractdpsettlement2hdr  ");
            SQL.AppendLine("Where SOContractDownpayment2DocNo = @Param ");
            SQL.AppendLine("And CancelInd = 'N' ");
            SQL.AppendLine("Limit 1; ");

            if (Sm.IsDataExist(SQL.ToString(), TxtDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This data already processed to SO Contract Downpayment Settlement(II).");
                return true;
            }

            return false;
        }


        private bool IsDocNotCancelled()
        {
            if (mIsRemarkHdrEditableOvertime) return false;

            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this document.");
                return true;
            }
            return false;
        }

        private bool IsDataCancelledAlready()
        {
            return Sm.IsDataExist(
                "Select 1 From TblSOContractDownpayment2Hdr Where (CancelInd = 'Y' Or Status = 'C')  And DocNo=@Param;",
                TxtDocNo.Text,
                "This document already cancelled.");
        }

        private bool IsDataAlreadyProcessed()
        {
            return Sm.IsDataExist(
                "Select 1 " +
                "From TblSOContractDownpayment2Hdr A, TblVoucherRequestHdr B " +
                "Where A.VoucherRequestDocNo=B.DocNo And B.VoucherDocNo Is Not Null And B.CancelInd='N' And A.DocNo=@Param;",
                TxtDocNo.Text,
                "This document already processed into voucher.");
        }

        private MySqlCommand EditSOContractDownpaymentHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblSOContractDownpayment2Hdr Set ");
            SQL.AppendLine("    CancelInd=@CancelInd, CancelReason=@CancelReason, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N'; ");

            if (mIsRemarkHdrEditableOvertime)
            {
                SQL.AppendLine("Update TblSOContractDownpayment2HDr Set Remark = @Remark Where DocNo = @DocNo; ");
            }

            //SQL.AppendLine("Update TblVoucherRequestHdr Set CancelInd=@CancelInd, CancelReason=@CancelReason, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            //SQL.AppendLine("Where DocNo In (Select VoucherRequestDocNo From TblSOContractDownpayment2Hdr Where DOcNo = @DocNo And CancelInd = 'Y') And CancelInd='N' And Status In ('O', 'A'); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowSOContractDownpayment2Hdr(DocNo);
                ShowSOContractDownpayment2Dtl(DocNo);
                ShowSOContractDownpayment2Dtl2(DocNo);
                ShowSOContractDownpayment2Dtl3(DocNo);
                ComputeAmtBeforeTax();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowSOContractDownpayment2Hdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelReason, A.CancelInd, A.SOContractDocNo, A.SOContractDownpaymentDocNo, A.OutstandingAmt, C.CtCode, C.CtName, A.CurCode, A.Amt, A.BankAcCode, A.Remark, ");
            SQL.AppendLine("Case A.Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' When 'O' Then 'Outstanding' End As StatusDesc,   ");
            SQL.AppendLine("A.TaxInvoiceNo, A.TaxInvoiceNo2, A.TaxInvoiceNo3, A.TaxInvoiceNo4, ");
            SQL.AppendLine("A.TaxInvoiceDt, A.TaxInvoiceDt2, A.TaxInvoiceDt3, A.TaxInvoiceDt4, ");
            SQL.AppendLine("IFNULL(A.TaxCode,'1') TaxCode, IFNULL(A.TaxCode2,'1') TaxCode2, A.TaxCode3, A.TaxCode4, ");
            SQL.AppendLine("A.TaxAmt, A.TaxAmt2, A.TaxAmt3, A.TaxAmt4, ");
            SQL.AppendLine("G.ProjectCode, G.ProjectName, B.PONo, D.PtName, B.Remark As SOContractRemark, A2.Amt SOContractDownpaymentAmt, B.Amt+AmtBom SOContractAmt,  ");
            SQL.AppendLine("A.OthersAmt, A.CurrentAmt, A.BalanceAmt, A.OutstandingAmt, A.SKBDN, A.OriginCountry, A.QualityOfGoods, A.LocalDocNo, A.DueDt, A.ExcRate, SUM(H.Amt) AS AfterTax ");
            SQL.AppendLine("From TblSOContractDownpayment2Hdr A ");
            SQL.AppendLine("left Join TblSOContractDownpaymentHdr A2 On A.SOContractDownpaymentDocNo = A2.DocNo ");
            SQL.AppendLine("Inner Join TblSOContractHdr B On A.SOContractDocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblCustomer C On B.CtCode=C.CtCode ");
            SQL.AppendLine("Left Join TblPaymentTerm D On B.PtCode=D.PtCode ");
            SQL.AppendLine("Left Join TblBOQHdr E On B.BOQDocNo=E.DocNo ");
            SQL.AppendLine("Left Join TblLOPHdr F On E.LOPDocNo=F.DocNO ");
            SQL.AppendLine("Left Join TblProjectGroup G On F.PGCode=G.PGCode ");
            SQL.AppendLine("Inner Join TblSOContractDtl H On B.DocNo=H.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo", 
                        //1-5
                        "DocDt", "CancelReason", "CancelInd", "CtCode", "CtName", 
                        //6-10
                        "CurCode", "Amt", "BankAcCode", "Remark", "TaxInvoiceNo", 
                        //11-15
                        "TaxInvoiceNo2", "TaxInvoiceNo3", "TaxInvoiceNo4", "TaxInvoiceDt", "TaxInvoiceDt2", 
                        //16-20
                        "TaxInvoiceDt3", "TaxInvoiceDt4", "TaxCode", "TaxCode2", "TaxCode3", 
                        //21-25
                        "TaxCode4", "TaxAmt", "TaxAmt2", "TaxAmt3", "TaxAmt4",
                        //26-30
                        "SOContractDocNo", "SOContractDownpaymentDocNo", "OutstandingAmt", "SOContractDownpaymentAmt", "ProjectCode",
                        //31-35
                        "ProjectName", "PONo", "PtName", "SOContractRemark", "SOContractAmt",
                        //36-40
                        "OthersAmt", "CurrentAmt", "BalanceAmt", "SKBDN", "OriginCountry",
                        //41-45
                        "QualityOfGoods", "StatusDesc", "LocalDocNo", "DueDt", "ExcRate",
                        //46
                        "AfterTax"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        MeeCancelReason.EditValue = Sm.DrStr(dr, c[2]);
                        ChkCancelInd.Checked = Sm.DrStr(dr, c[3]) == "Y";
                        TxtCtCode.EditValue = Sm.DrStr(dr, c[5]);
                        Sm.SetLue(LueCurCode, Sm.DrStr(dr, c[6]));
                        TxtAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[7]), 0);
                        Sm.SetLue(LueBankAcCode, Sm.DrStr(dr, c[8]));
                        MeeRemark.EditValue = Sm.DrStr(dr, c[9]);
                        TxtTaxInvoiceNo.EditValue = Sm.DrStr(dr, c[10]);
                        TxtTaxInvoiceNo2.EditValue = Sm.DrStr(dr, c[11]);
                        TxtTaxInvoiceNo3.EditValue = Sm.DrStr(dr, c[12]);
                        TxtTaxInvoiceNo4.EditValue = Sm.DrStr(dr, c[13]);
                        Sm.SetDte(DteTaxInvoiceDt, Sm.DrStr(dr, c[14]));
                        Sm.SetDte(DteTaxInvoiceDt2, Sm.DrStr(dr, c[15]));
                        Sm.SetDte(DteTaxInvoiceDt3, Sm.DrStr(dr, c[16]));
                        Sm.SetDte(DteTaxInvoiceDt4, Sm.DrStr(dr, c[17]));
                        Sm.SetLue(LueTaxCode, Sm.DrStr(dr, c[18]));
                        Sm.SetLue(LueTaxCode2, Sm.DrStr(dr, c[19]));
                        Sm.SetLue(LueTaxCode3, Sm.DrStr(dr, c[20]));
                        Sm.SetLue(LueTaxCode4, Sm.DrStr(dr, c[21]));
                        TxtTaxAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[22]), 0);
                        TxtTaxAmt2.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[23]), 0);
                        TxtTaxAmt3.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[24]), 0);
                        TxtTaxAmt4.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[25]), 0);
                        TxtSOContractDocNo.EditValue = Sm.DrStr(dr, c[26]);
                        TxtSOContractDownpaymentDocNo.EditValue = Sm.DrStr(dr, c[27]);
                        TxtOutstandingAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[28]), 0);
                        TxtSOContractDownpaymentAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[29]), 0);
                        TxtProjectCode.EditValue = Sm.DrStr(dr, c[30]);
                        TxtProjectName.EditValue = Sm.DrStr(dr, c[31]);
                        TxtPONo.EditValue = Sm.DrStr(dr, c[32]);
                        TxtPtName.EditValue = Sm.DrStr(dr, c[33]);
                        MeeSOContractRemark.EditValue = Sm.DrStr(dr, c[34]);
                        TxtSOContractAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[35]), 0);
                        TxtOthersAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[36]), 0);
                        TxtCurrentAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[37]), 0);
                        TxtBalanceAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[38]), 0);
                        MeeSKBDN.EditValue = Sm.DrStr(dr, c[39]);
                        MeeOriginCountry.EditValue = Sm.DrStr(dr, c[40]);
                        MeeQualityOfGoods.EditValue = Sm.DrStr(dr, c[41]);
                        TxtStatus.EditValue = Sm.DrStr(dr, c[42]);
                        TxtLocalDocNo.EditValue = Sm.DrStr(dr, c[43]);
                        Sm.SetDte(DteDueDt, Sm.DrStr(dr, c[44]));
                        TxtRate.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[45]), 0);
                        TxtSOContractAmtAfterTax.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[46]), 0);

                    }, true
                );
        }

        private void ShowSOContractDownpayment2Dtl(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@SOContractDocNo", TxtSOContractDocNo.Text);

            var SQL = new StringBuilder();

            #region Old Code
            //SQL.AppendLine("Select H.No, B.DNo, B.DOCtVerifyDocNo, B.DOCtVerifyDNo, ");
            //SQL.AppendLine("G.WhsName, D.ItCode, E.ItName, E.ItCodeInternal, E.Specification, D.BatchNo, D.Source, ");
            //SQL.AppendLine("B.Qty, E.SalesUomCode, B.UPrice, B.Amt, B.TerminDP, B.TerminDP2, B.TaxAmt, B.TaxAmt2, B.AfterTax ");
            //SQL.AppendLine("From TblSOContractDownpayment2Hdr A ");
            //SQL.AppendLine("Inner Join TblSOContractDownpayment2Dtl B On A.DocNo=B.DocNo ");
            //SQL.AppendLine("Inner Join TblDOCtVerifyHdr C On B.DOCtVerifyDocNo=C.DocNo ");
            //SQL.AppendLine("Inner Join TblDOCtVerifyDtl D On B.DOCtVerifyDocNo=D.DocNo And B.DOCtVerifyDNo=D.DNo ");
            //SQL.AppendLine("Inner Join TblItem E On D.ItCode=E.ItCode ");
            //SQL.AppendLine("Left Join TblDOCt2Hdr F On C.DOCt2DocNo=F.DocNo ");
            //SQL.AppendLine("Left Join TblWarehouse G On F.WhsCode=G.WhsCode ");
            //SQL.AppendLine("Left Join ");
            //SQL.AppendLine("( ");
            //SQL.AppendLine("    Select T1.DocNo, T2.DNo, T5.ItCode, Group_Concat(Distinct No) No ");
            //SQL.AppendLine("    From TblDOCt2Hdr T1 ");
            //SQL.AppendLine("    Inner Join TblDOCt2Dtl T2 On T1.DocNo = T2.DocNo ");
            //SQL.AppendLine("    Inner Join TblDRHdr T3 On T1.DRDocNo = T3.DocNo ");
            //SQL.AppendLine("    Inner Join TblDRDtl T4 On T3.DocNo ");
            //SQL.AppendLine("    Inner Join TblSOContractDtl T5 On T4.SODocNo = T5.DocNo And T4.SODNo = T5.DNo ");
            //SQL.AppendLine("        And T5.DocNo In (Select SOContractDocNo From TblSOContractDownpayment2Hdr Where DocNo = @DocNo) ");
            //SQL.AppendLine("    Group By T1.DocNo, T2.DNo, T5.ItCode ");
            //SQL.AppendLine(") H On D.DOCt2DocNo = H.DocNo And D.DOCt2DNo = H.DNo And D.ItCode = H.ItCode ");
            //SQL.AppendLine("Where A.DocNo=@DocNo ");
            //SQL.AppendLine("Order By B.DNo; ");
            #endregion

            SQL.AppendLine("Select H.No, B.DNo, B.DOCtVerifyDocNo, B.DOCtVerifyDNo, ");
            SQL.AppendLine("G.WhsName, D.ItCode, E.ItName, E.ItCodeInternal, E.Specification, D.BatchNo, D.Source, ");
            SQL.AppendLine("B.Qty, E.SalesUomCode, B.UPrice, B.Amt, B.TerminDP, B.TerminDP2, B.TaxAmt, B.TaxAmt2, B.AfterTax, ");
            SQL.AppendLine("I.ItCode ItCodeDismantle, I.ItName ItNameDismantle, A.LocalDocNo ");
            SQL.AppendLine("From TblSOContractDownpayment2Hdr A ");
            SQL.AppendLine("Inner Join TblSOContractDownpayment2Dtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("    And A.DocNo = @DocNo ");
            SQL.AppendLine("Inner Join TblDOCtVerifyHdr C On B.DOCtVerifyDocNo = C.DocNo ");
            SQL.AppendLine("Inner Join TblDOCtVerifyDtl D On C.DocNo = D.DocNo And B.DOCtVerifyDNo = D.DNo ");
            SQL.AppendLine("Inner Join TblItem E On D.ItCode = E.ItCode ");
            SQL.AppendLine("Inner Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select Distinct T1.DocNo, T2.WhsCode, T3.DNo, T4.SOContractDocNo, T4.SOContractDNo ");
            SQL.AppendLine("    From TblDOCtVerifyHdr T1 ");
            SQL.AppendLine("    Inner Join TblDOCt2Hdr T2 On T1.DOCt2DocNo = T2.DocNo ");
            SQL.AppendLine("        And T1.DOCt2DocNo Is Not Null ");
            SQL.AppendLine("    Inner Join TblDOCtVerifyDtl T3 On T1.DocNO = T3.DocNo ");
            SQL.AppendLine("    Inner Join TblDOCt2Dtl T4 On T2.DOcNo = T4.DocNo And T3.DOCt2DNo = T4.DNo ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select Distinct T1.DocNo, T2.WhsCode, T3.DNo, T2.SOContractDocNo, T4.SOContractDNo ");
            SQL.AppendLine("    From TblDOCtVerifyHdr T1 ");
            SQL.AppendLine("    Inner Join TblDOCtHdr T2 On T1.DOCtDocNo = T2.DocNo ");
            SQL.AppendLine("        And T1.DOCtDocNo Is Not Null ");
            SQL.AppendLine("    Inner Join TblDOCtVerifyDtl T3 On T1.DocNo = T3.DocNo ");
            SQL.AppendLine("    Inner Join TblDOCtDtl T4 On T2.DocNo = T4.DocNo And T3.DOCtDNo = T4.DNo ");
            SQL.AppendLine(") F On D.DocNo = F.DocNo And D.DNo = F.DNo ");
            SQL.AppendLine("Inner Join TblWarehouse G On F.WhsCode = G.WhsCode ");
            SQL.AppendLine("Inner Join TblSOContractDtl H On F.SOContractDocNo = H.DocNo And F.SOContractDNo = H.DNo ");
            SQL.AppendLine("    Left JOIN  ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        SELECT A.DocNo, A.DNo, A.ItCode, C.ItCodeDismantle From TblSOContractDtl A ");
            SQL.AppendLine("        Inner Join TblSOContractDtl4 B On A.DocNo = B.DocNo  ");
            SQL.AppendLine("            And A.DocNo = @SOContractDocNo  ");
            SQL.AppendLine("            And A.ItCode = B.ItCode  And A.No = B.No  ");
            SQL.AppendLine("        Inner Join TblBOQDtl2 C On B.BOQDocNo = C.DocNo  ");
            SQL.AppendLine("           And A.ItCode = C.ItCode And B.SeqNo = C.SeqNo  ");
            SQL.AppendLine("        Left Join TblItem D On C.ItCodeDismantle = D.ItCode ");
            SQL.AppendLine("     ) C2 ON H.DocNo=C2.DocNo AND H.DNo=C2.DNo AND H.ItCode=C2.ItCode ");
            SQL.AppendLine("     LEFT JOIN tblitem I ON C2.ItCodeDismantle=I.ItCode  ");
            SQL.AppendLine("Order By B.DNo; ");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo",
 
                    //1-5
                    "DOCtVerifyDocNo", "DOCtVerifyDNo", "WhsName", "ItCode", "ItName", 

                    //6-10
                    "ItCodeInternal", "Specification", "BatchNo", "Source", "Qty",
                    
                    //11-15
                    "SalesUomCode", "UPrice",  "Amt",  "TerminDP", "TerminDP2",
                    
                    //16-20
                    "TaxAmt", "TaxAmt2", "AfterTax", "No", "ItCodeDismantle",

                    //21-22
                    "ItNameDismantle", "LocalDocNo"
                    
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 10);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 12);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 13);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 14);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 15);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 16);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 17);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 18);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 19);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 20);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 21);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 22);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] {12, 14, 15, 16, 17, 18, 19, 20 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ShowSOContractDownpayment2Dtl2(string DocNo)
        {

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            #region Old Code
            //SQL.AppendLine("Select H.No, B.DNo, B.DOCtVerifyDocNo, B.DOCtVerifyDNo, ");
            //SQL.AppendLine("G.WhsName, D.ItCode, E.ItName, E.ItCodeInternal, E.Specification, D.BatchNo, D.Source, ");
            //SQL.AppendLine("B.Qty, E.SalesUomCode, B.UPrice, B.Amt, B.TerminDP, B.TerminDP2, B.TaxAmt, B.TaxAmt2, B.AfterTax ");
            //SQL.AppendLine("From TblSOContractDownpayment2Hdr A ");
            //SQL.AppendLine("Inner Join TblSOContractDownpayment2Dtl2 B On A.DocNo=B.DocNo ");
            //SQL.AppendLine("Inner Join TblDOCtVerifyHdr C On B.DOCtVerifyDocNo=C.DocNo ");
            //SQL.AppendLine("Inner Join TblDOCtVerifyDtl D On B.DOCtVerifyDocNo=D.DocNo And B.DOCtVerifyDNo=D.DNo ");
            //SQL.AppendLine("Inner Join TblItem E On D.ItCode=E.ItCode ");
            //SQL.AppendLine("Left Join TblDOCt2Hdr F On C.DOCt2DocNo=F.DocNo ");
            //SQL.AppendLine("Left Join TblWarehouse G On F.WhsCode=G.WhsCode ");
            //SQL.AppendLine("Left Join ");
            //SQL.AppendLine("( ");
            //SQL.AppendLine("    Select T1.DocNo, T2.DNo, T5.ItCode, Group_Concat(Distinct No) No ");
            //SQL.AppendLine("    From TblDOCt2Hdr T1 ");
            //SQL.AppendLine("    Inner Join TblDOCt2Dtl T2 On T1.DocNo = T2.DocNo ");
            //SQL.AppendLine("    Inner Join TblDRHdr T3 On T1.DRDocNo = T3.DocNo ");
            //SQL.AppendLine("    Inner Join TblDRDtl T4 On T3.DocNo ");
            //SQL.AppendLine("    Inner Join TblSOContractDtl T5 On T4.SODocNo = T5.DocNo And T4.SODNo = T5.DNo ");
            //SQL.AppendLine("        And T5.DocNo In (Select SOContractDocNo From TblSOContractDownpayment2Hdr Where DocNo = @DocNo) ");
            //SQL.AppendLine("    Group By T1.DocNo, T2.DNo, T5.ItCode ");
            //SQL.AppendLine(") H On D.DOCt2DocNo = H.DocNo And D.DOCt2DNo = H.DNo And D.ItCode = H.ItCode ");
            //SQL.AppendLine("Where A.DocNo=@DocNo ");
            //SQL.AppendLine("Order By B.DNo; ");
            #endregion

            SQL.AppendLine("Select H.No, B.DNo, B.DOCtVerifyDocNo, B.DOCtVerifyDNo, ");
            SQL.AppendLine("G.WhsName, D.ItCode, E.ItName, E.ItCodeInternal, E.Specification, D.BatchNo, D.Source, ");
            SQL.AppendLine("B.Qty, E.SalesUomCode, B.UPrice, B.Amt, B.TerminDP, B.TerminDP2, B.TaxAmt, B.TaxAmt2, B.AfterTax, A.LocalDocNo ");
            SQL.AppendLine("From TblSOContractDownpayment2Hdr A ");
            SQL.AppendLine("Inner Join TblSOContractDownpayment2Dtl2 B On A.DocNo = B.DocNo ");
            SQL.AppendLine("    And A.DocNo = @DocNo ");
            SQL.AppendLine("Inner Join TblDOCtVerifyHdr C On B.DOCtVerifyDocNo = C.DocNo ");
            SQL.AppendLine("Inner Join TblDOCtVerifyDtl D On C.DocNo = D.DocNo And B.DOCtVerifyDNo = D.DNo ");
            SQL.AppendLine("Inner Join TblItem E On D.ItCode = E.ItCode ");
            SQL.AppendLine("Inner Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select Distinct T1.DocNo, T2.WhsCode, T3.DNo, T4.SOContractDocNo, T4.SOContractDNo ");
            SQL.AppendLine("    From TblDOCtVerifyHdr T1 ");
            SQL.AppendLine("    Inner Join TblDOCt2Hdr T2 On T1.DOCt2DocNo = T2.DocNo ");
            SQL.AppendLine("        And T1.DOCt2DocNo Is Not Null ");
            SQL.AppendLine("    Inner Join TblDOCtVerifyDtl T3 On T1.DocNO = T3.DocNo ");
            SQL.AppendLine("    Inner Join TblDOCt2Dtl T4 On T2.DOcNo = T4.DocNo And T3.DOCt2DNo = T4.DNo ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select Distinct T1.DocNo, T2.WhsCode, T3.DNo, T2.SOContractDocNo, T4.SOContractDNo ");
            SQL.AppendLine("    From TblDOCtVerifyHdr T1 ");
            SQL.AppendLine("    Inner Join TblDOCtHdr T2 On T1.DOCtDocNo = T2.DocNo ");
            SQL.AppendLine("        And T1.DOCtDocNo Is Not Null ");
            SQL.AppendLine("    Inner Join TblDOCtVerifyDtl T3 On T1.DocNo = T3.DocNo ");
            SQL.AppendLine("    Inner Join TblDOCtDtl T4 On T2.DocNo = T4.DocNo And T3.DOCtDNo = T4.DNo ");
            SQL.AppendLine(") F On D.DocNo = F.DocNo And D.DNo = F.DNo ");
            SQL.AppendLine("Inner Join TblWarehouse G On F.WhsCode = G.WhsCode ");
            SQL.AppendLine("Inner Join TblSOContractDtl H On F.SOContractDocNo = H.DocNo And F.SOContractDNo = H.DNo ");
            SQL.AppendLine("Order By B.DNo; ");

            Sm.ShowDataInGrid(
                ref Grd2, ref cm, SQL.ToString(),
                new string[] 
                { 
                   //0
                    "DNo",
 
                    //1-5
                    "DOCtVerifyDocNo", "DOCtVerifyDNo", "WhsName", "ItCode", "ItName", 

                    //6-10
                    "ItCodeInternal", "Specification", "BatchNo", "Source", "Qty",
                    
                    //11-15
                    "SalesUomCode", "UPrice",  "Amt",  "TerminDP", "TerminDP2",
                    
                    //16-20
                    "TaxAmt", "TaxAmt2", "AfterTax", "No", "LocalDocNo"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 10);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 12);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 13);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 14);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 15);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 16);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 17);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 18);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 19);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 20);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 12, 14, 15, 16, 17, 18, 19, 20 });
            Sm.FocusGrd(Grd2, 0, 1);
        }

        private void ShowSOContractDownpayment2Dtl3(string DocNo)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();
            SQL.AppendLine("Select A.AcNo, B.AcDesc, A.AcInd, A.DAmt, A.CAmt, A.OptAcDesc, C.OptDesc, A.Remark ");
            SQL.AppendLine("From TblSOContractDownpayment2Dtl3 A ");
            SQL.AppendLine("Inner Join TblCOA B ");
            SQL.AppendLine("Left Join TblOption C On A.OptAcDesc=C.OptCode And OptCat='AccountDescriptionOnSalesInvoice' ");
            SQL.AppendLine("Where A.DocNo=@DocNo And A.AcNo=B.AcNo Order By A.DNo;");

            Sm.ShowDataInGrid(
                ref Grd3, ref cm, SQL.ToString(),
                new string[] 
                { 
                    "AcNo", 
                    "AcDesc", "AcInd", "DAmt", "CAmt", "OptAcDesc",
                    "OptDesc", "Remark"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 10, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);

                    if (Sm.GetGrdBool(Grd3, Row, 10))
                    {
                        if (Sm.GetGrdDec(Grd3, Row, 4) > 0)
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                        else
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 5, 2);
                    }
                }, false, false, true, false
            );
            Sm.SetGrdBoolValueFalse(Grd3, Grd3.Rows.Count - 1, new int[] { 3, 5 });
            Sm.SetGrdNumValueZero(ref Grd3, Grd3.Rows.Count - 1, new int[] { 4, 6 });
            Sm.FocusGrd(Grd3, 0, 1);
        }

        #endregion

        #region Additional Method

        internal void ComputeAmtBeforeTax()
        {
            decimal AmtBeforeTax = 0m;

            for (int r = 0; r <= Grd1.Rows.Count - 1; r++)
                if (Sm.GetGrdStr(Grd1, r, 15).Length > 0)
                    AmtBeforeTax += Sm.GetGrdDec(Grd1, r, 15);

            for (int r = 0; r <= Grd2.Rows.Count - 1; r++)
                if (Sm.GetGrdStr(Grd2, r, 15).Length > 0)
                    AmtBeforeTax += Sm.GetGrdDec(Grd2, r, 15);

            TxtAmtBeforeTax.Text = Sm.FormatNum(AmtBeforeTax, 0);
        }

        private bool IsNeedApproval()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 ");
            SQL.AppendLine("From TblDocApprovalSetting ");
            SQL.AppendLine("Where DocType = 'SOCD2' ");

            return Sm.IsDataExist(SQL.ToString());
        }

        private void ComputeDueDate()
        {

        }

        private void ParPrint()
        {
            var l = new List<SOCDHdr>();
            var ldtl = new List<SOCDDtl>();
            var ldtl2 = new List<SOCDDtl2>();
            var lsign = new List<SOCDSign>();
            string[] TableName = { "SOCDHdr", "SOCDDtl", "SOCDDtl2", "SOCDSign" };
            List<IList> myLists = new List<IList>();
            int Nomor = 1;

            #region Header

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, ");
            SQL.AppendLine("@CompanyFooterImage As CompanyFooterImage, ");

            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyAddressCity',  ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyPhoneNumber', I.BankName, H.CurCode, H.BankAcNo,  ");

            SQL.AppendLine("B.CtName, B.Address CtAddress, DATE_FORMAT(A.DocDt,'%d %M %Y') As PODate ");
            SQL.AppendLine(",  ");
            SQL.AppendLine("ifnull(D.TaxName,' ') TaxName, ifnull(E.TaxName, ' ') TaxName2, ");
            SQL.AppendLine("ifnull(F.TaxName,' ') TaxName3, ifnull(G.TaxName, ' ') TaxName4, ");
            SQL.AppendLine("DATE_FORMAT(A.DocDt, '%d') Dt, ");
            SQL.AppendLine("Case monthname(A.DocDt)  ");
            SQL.AppendLine("When 'January' Then 'Januari' When 'February' Then 'Februari' When 'March' Then 'Maret'  ");
            SQL.AppendLine("When 'April' Then 'April' When 'May' Then 'Mei' When 'June' Then 'Juni' When 'July' Then 'Juli' When 'August' Then 'Agustus'  ");
            SQL.AppendLine("When 'September' Then 'September' When 'October' Then 'Oktober' When 'November' Then 'Nopember' When 'December' Then 'Desember' ");
            SQL.AppendLine("END AS MONTH ");
            SQL.AppendLine(", DATE_FORMAT(A.DocDt, '%Y') Year, C.QualityOfGoods,  ");

            #region Sign
            SQL.AppendLine("(SELECT empname SOContractTerminDPPrintOutSignName1 FROM tblemployee A INNER JOIN  tblparameter B ON A.EmpCode=B.ParValue WHERE  parcode ='SOContractTerminDPPrintOutSignName1') AS 'SOContractTerminDPPrintOutSignName1' ,  ");
            SQL.AppendLine("(SELECT empname SOContractTerminDPPrintOutSignName2 FROM tblemployee A INNER JOIN  tblparameter B ON A.EmpCode=B.ParValue WHERE  parcode ='SOContractTerminDPPrintOutSignName2') AS 'SOContractTerminDPPrintOutSignName2' ,   ");
            SQL.AppendLine("(SELECT empname SOContractTerminDPPrintOutSignName3 FROM tblemployee A INNER JOIN  tblparameter B ON A.EmpCode=B.ParValue WHERE  parcode ='SOContractTerminDPPrintOutSignName3') AS 'SOContractTerminDPPrintOutSignName3',  ");
            SQL.AppendLine("(SELECT parvalue MaxLimitAmtForSOContractDPSign FROM tblparameter WHERE parcode ='MaxLimitAmtForSOContractDPSign') AS 'MaxLimitAmtForSOContractDPSign',  ");
            SQL.AppendLine("(SELECT parvalue MinLimitAmtForSOContractDPSign FROM tblparameter WHERE parcode ='MinLimitAmtForSOContractDPSign') AS 'MinLimitAmtForSOContractDPSign', ");
            SQL.AppendLine("(SELECT C.PosName FROM tblemployee A INNER JOIN  tblparameter B  ON  A.EmpCode=B.ParValue INNER JOIN tblposition C ON A.PosCode=C.PosCode WHERE  parcode ='SOContractTerminDPPrintOutSignName1') As PosName1, ");
            SQL.AppendLine("(SELECT C.PosName FROM tblemployee A INNER JOIN  tblparameter B  ON  A.EmpCode=B.ParValue INNER JOIN tblposition C ON A.PosCode=C.PosCode WHERE  parcode ='SOContractTerminDPPrintOutSignName2') As PosName2, ");
            SQL.AppendLine("(SELECT C.PosName FROM tblemployee A INNER JOIN  tblparameter B  ON  A.EmpCode=B.ParValue INNER JOIN tblposition C ON A.PosCode=C.PosCode WHERE  parcode ='SOContractTerminDPPrintOutSignName3') As PosName3 ");
            #endregion

            SQL.AppendLine("FROM tblsocontracthdr A ");
            SQL.AppendLine("inner join tblcustomer B ON A.CtCode=B.CtCode ");
            SQL.AppendLine("inner JOIN tblsocontractdownpayment2hdr C ON A.DocNo=C.SOContractDocNo ");
            SQL.AppendLine("left JOIN tbltax D ON C.TaxCode=D.TaxCode ");
            SQL.AppendLine("LEFT JOIN tbltax E ON C.TaxCode2=E.TaxCode ");
            SQL.AppendLine("LEFT JOIN tbltax F ON C.TaxCode3=F.TaxCode ");
            SQL.AppendLine("LEFT JOIN tbltax G ON C.TaxCode4=G.TaxCode  ");
            SQL.AppendLine("inner JOIN tblbankaccount H ON C.BankAcCode=H.BankAcCode ");
            SQL.AppendLine("INNER JOIN tblbank I ON H.BankCode=I.BankCode ");
            SQL.AppendLine("WHERE A.DocNo=@SOContractDocNo AND C.DocNo=@DocNo ");



            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                Sm.CmParam<String>(ref cm, "@CompanyFooterImage", "FooterImage.png");
                Sm.CmParam<String>(ref cm, "@SOContractDocNo", TxtSOContractDocNo.Text);
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);


                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                {
                    //0
                    "CompanyLogo",

                    //1-5
                    "CompanyName",
                    "CompanyAddress",
                    "CompanyAddressCity", 
                    "CtAddress",
                    "PODate",

                    //6-10
                    "TaxName",
                    "TaxName2",
                    "TaxName3",
                    "TaxName4",
                    "CompanyPhoneNumber",


                    "BankName",
                    "CurCode",
                    "BankAcNo",
                    "CompanyFooterImage",
                    "Dt",

                    //16-20
                    "MONTH",
                    "YEAR",
                    "SOContractTerminDPPrintOutSignName1",
                    "SOContractTerminDPPrintOutSignName2",
                    "SOContractTerminDPPrintOutSignName3",

                    //21-25
                    "MaxLimitAmtForSOContractDPSign",
                    "MinLimitAmtForSOContractDPSign",
                    "PosName1",
                    "PosName2",
                    "PosName3",

                    //26
                    "QualityOfGoods"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new SOCDHdr()
                        {
                            CompanyLogo = Sm.DrStr(dr, c[0]),

                            CompanyName = Sm.DrStr(dr, c[1]),
                            CompanyAddress = Sm.DrStr(dr, c[2]),
                            CompanyAddressCity = Sm.DrStr(dr, c[3]),
                            CtAddress = Sm.DrStr(dr, c[4]),
                            PODate = Sm.DrStr(dr, c[5]),

                            CtName = TxtCtCode.Text,
                            DocNo = TxtDocNo.Text,
                            DocDt = DteDocDt.Text,
                            CustomerPO = TxtPONo.Text,
                            Quality = MeeQualityOfGoods.Text,

                            OriginCountry = MeeOriginCountry.Text,
                            SKBDN = MeeSKBDN.Text,
                            Remark = MeeRemark.Text,
                            Amount = decimal.Parse(TxtAmt.Text),
                            Terbilang = Sm.Terbilang(decimal.Parse(TxtAmt.Text)),

                            TaxName = Sm.DrStr(dr, c[6]),
                            TaxName2 = Sm.DrStr(dr, c[7]),
                            TaxName3 = Sm.DrStr(dr, c[8]),
                            TaxName4 = Sm.DrStr(dr, c[9]),

                            TaxAmount = decimal.Parse(TxtTaxAmt2.Text),
                            CompanyPhoneNumber = Sm.DrStr(dr, c[10]),
                            BankName = Sm.DrStr(dr, c[11]),
                            CurCode = Sm.DrStr(dr, c[12]),
                            BankAcNo = Sm.DrStr(dr, c[13]),


                            TaxAmount2 = decimal.Parse(TxtTaxAmt3.Text),
                            TaxAmount3 = decimal.Parse(TxtTaxAmt.Text),
                            TaxAmount4 = decimal.Parse(TxtTaxAmt4.Text),
                            CompanyFooterImage = Sm.DrStr(dr, c[14]),
                            Date = Sm.DrStr(dr, c[15]),

                            Month = Sm.DrStr(dr, c[16]),
                            Year = Sm.DrStr(dr, c[17]),
                            SOContractTerminDPPrintOutSignName1 = Sm.DrStr(dr, c[18]),
                            SOContractTerminDPPrintOutSignName2 = Sm.DrStr(dr, c[19]),
                            SOContractTerminDPPrintOutSignName3 = Sm.DrStr(dr, c[20]),

                            mMaxLimitAmtForSOContractDPSign = Sm.DrDec(dr, c[21]),
                            mMinLimitAmtForSOContractDPSign = Sm.DrDec(dr, c[22]),
                            PosName1 = Sm.DrStr(dr, c[23]),
                            PosName2 = Sm.DrStr(dr, c[24]),
                            PosName3 = Sm.DrStr(dr, c[25]),

                            QualityofGoods = Sm.DrStr(dr, c[26])

                        });
                    }
                }
                dr.Close();
            }
            myLists.Add(l);

            #endregion

            #region Detail
            if (Grd1.Rows.Count > 1)
            {
                for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
                {

                    ldtl.Add(new SOCDDtl()
                    {
                        No = Nomor,

                        SOContractNo = Sm.GetGrdStr(Grd1, i, 21),
                        LocalCode = Sm.GetGrdStr(Grd1, i, 8),
                        ItName = Sm.GetGrdStr(Grd1, i, 7),
                        Specification = Sm.GetGrdStr(Grd1, i, 9),
                        Qty = Sm.GetGrdDec(Grd1, i, 12),
                        Uom = Sm.GetGrdStr(Grd1, i, 13),

                        Price = Sm.GetGrdDec(Grd1, i, 14),
                        TotalPrice = Sm.GetGrdDec(Grd1, i, 15),
                        TotalAmount = Sm.GetGrdDec(Grd1, i, 17)

                    });
                    Nomor += 1;

                }
            }
            else
            {
                for (int i = 0; i < Grd2.Rows.Count - 1; ++i)
                {

                    ldtl.Add(new SOCDDtl()
                    {
                        No = Nomor,

                        SOContractNo = Sm.GetGrdStr(Grd2, i, 21),
                        LocalCode = Sm.GetGrdStr(Grd2, i, 8),
                        ItName = Sm.GetGrdStr(Grd2, i, 7),
                        Specification = Sm.GetGrdStr(Grd2, i, 9),
                        Qty = Sm.GetGrdDec(Grd2, i, 12),
                        Uom = Sm.GetGrdStr(Grd2, i, 13),

                        Price = Sm.GetGrdDec(Grd2, i, 14),
                        TotalPrice = Sm.GetGrdDec(Grd2, i, 15),
                        TotalAmount = Sm.GetGrdDec(Grd2, i, 17)

                    });
                    Nomor += 1;

                }
            }

            myLists.Add(ldtl);



            #region Spec Detail
            var cmDtl = new MySqlCommand();

            var SQLDtl = new StringBuilder();
            using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl.Open();
                cmDtl.Connection = cnDtl;

                SQLDtl.AppendLine("SELECT C.Remark Specification ");
                SQLDtl.AppendLine("FROM tblsocontracthdr A ");
                SQLDtl.AppendLine("inner JOIN tblsocontractdownpayment2hdr B ON A.DocNo=B.SOContractDocNo ");
                SQLDtl.AppendLine("left JOIN TblSOContractdtl C ON A.DocNo=C.DocNo ");
                SQLDtl.AppendLine("Where B.DocNo=@DocNo Order By C.DNo ");

                cmDtl.CommandText = SQLDtl.ToString();

                Sm.CmParam<String>(ref cmDtl, "@DocNo", TxtDocNo.Text);

                var drDtl = cmDtl.ExecuteReader();
                var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                    {
                     //0
                     "Specification" 
                    });
                if (drDtl.HasRows)
                {
                    while (drDtl.Read())
                    {
                        ldtl2.Add(new SOCDDtl2()
                        {
                            Specification = Sm.DrStr(drDtl, cDtl[0])
                        });
                    }
                }
                drDtl.Close();
            }
            myLists.Add(ldtl2);
            #endregion 

            #region Signature

            var cmDtl2 = new MySqlCommand();

            var SQLDtl2 = new StringBuilder();
            using (var cnDtl2 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl2.Open();
                cmDtl2.Connection = cnDtl2;

                SQLDtl2.AppendLine("Select Concat(Upper(left(T4.UserName,1)),Substring(Lower(T4.UserName), 2, Length(T4.UserName))) As UserName, ");
                SQLDtl2.AppendLine("Concat(IfNull(T7.ParValue, ''), T2.UserCode, '.JPG') As Signature, T6.PosName ");
                SQLDtl2.AppendLine("From ");
                SQLDtl2.AppendLine("( ");
                SQLDtl2.AppendLine("    Select A.DocNo, Max(C.Level) MaxLvl ");
                SQLDtl2.AppendLine("    From TblSOContractDownpayment2Hdr A ");
                SQLDtl2.AppendLine("    Inner Join TblDocapproval B On A.DocNo = B.DocNo ");
                SQLDtl2.AppendLine("    Inner JOin TblDocApprovalSetting C On B.DocType = C.DocType And B.ApprovalDNo = C.DNo ");
                SQLDtl2.AppendLine("    Group By A.DocNo ");
                SQLDtl2.AppendLine(") T1 ");
                SQLDtl2.AppendLine("Inner Join TblDocapproval T2 On T1.DocNo = T2.DocNo ");
                SQLDtl2.AppendLine("Inner Join TblDocApprovalSetting T3 On T2.DocType = T3.DocType  ");
                SQLDtl2.AppendLine("    And T2.ApprovalDNo = T3.DNo And T3.`Level` = T1.MaxLvl ");
                SQLDtl2.AppendLine("Inner Join TblUser T4 On T2.UserCode = T4.UserCode ");
                SQLDtl2.AppendLine("Left Join TblEmployee T5 On T4.UserCode=T5.UserCode  ");
                SQLDtl2.AppendLine("Left Join TblPosition T6 On T5.PosCode=T6.PosCode  ");
                SQLDtl2.AppendLine("Left Join TblParameter T7 On T7.ParCode = 'ImgFileSignature' ");
                SQLDtl2.AppendLine("Where T1.DocNo = @DocNo ");
                SQLDtl2.AppendLine("Group  By T7.ParValue, T2.UserCode, T4.UserName, T6.PosName; ");


                cmDtl2.CommandText = SQLDtl2.ToString();
                Sm.CmParam<String>(ref cmDtl2, "@DocNo", TxtDocNo.Text);
                var drDtl2 = cmDtl2.ExecuteReader();
                var cDtl2 = Sm.GetOrdinal(drDtl2, new string[] 
                            {

                             //0
                             "Signature" ,

                             //1-5
                             "UserName" ,
                             "PosName"
                             
            
                            });
                if (drDtl2.HasRows)
                {
                    while (drDtl2.Read())
                    {
                        lsign.Add(new SOCDSign()
                        {
                            Signature = Sm.DrStr(drDtl2, cDtl2[0]),
                            UserName = Sm.DrStr(drDtl2, cDtl2[1]),
                            POSName = Sm.DrStr(drDtl2, cDtl2[2])

                        });
                    }
                }
                drDtl2.Close();
            }
            myLists.Add(lsign);


            #endregion

            #endregion

            Sm.PrintReport("SOContractDp_1", myLists, TableName, false);
            Sm.PrintReport("SOContractDp_2", myLists, TableName, false);
            Sm.PrintReport("SOContractDp_3", myLists, TableName, false);

        }


        private MySqlCommand SaveJournal(string DocNo)
        {
            var SQL = new StringBuilder();
            var CurCode = Sm.GetLue(LueCurCode);
            decimal ExcRateARDP = Sm.GetValueDec("Select ExcRate From TblSOContractDownpaymentHdr Where DocNo = @Param ", TxtSOContractDownpaymentDocNo.Text);

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Update TblSOContractDownpayment2Hdr Set JournalDocNo=@JournalDocNo Where DocNo=@DocNo;");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@JournalDocNo, ");
            SQL.AppendLine("@DocDt, ");
            SQL.AppendLine("Concat('Sales Invoice for Project : ', @DocNo), ");
            SQL.AppendLine("@MenuCode, (Select MenuDesc From TblMenu Where MenuCode=@MenuCode), ");
            SQL.AppendLine("@Remark, @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("T.AcNo, T.DAmt, T.CAMt, Null As Remark, @CreateBy As CreateBy, CurrentDateTime() As CreateDt ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From (");

            // PIUTANG USAHA PROYEK
            SQL.AppendLine("    SELECT CONCAT(B.ParValue, D.CtCode) AcNo, ");
            
            SQL.AppendLine("    (@ExcRate * A.AfterTax) DAmt, 0.00 As CAmt ");
            SQL.AppendLine("    FROM tblsocontractdownpayment2dtl A ");
            SQL.AppendLine("    INNER JOIN tblparameter B ON B.ParCode = 'CustomerAcNoAR' AND B.ParValue IS NOT NULL ");
            SQL.AppendLine("    AND A.DocNo = @DocNo ");
            SQL.AppendLine("    INNER JOIN tblsocontractdownpayment2hdr C ON A.DocNo = C.DocNo ");
            SQL.AppendLine("    INNER JOIN tblsocontracthdr D ON C.SOContractDocNo = D.DocNo ");

            SQL.AppendLine("    UNION ALL ");

            SQL.AppendLine("    SELECT CONCAT(B.ParValue, D.CtCode) AcNo, ");
           
            SQL.AppendLine("     (@ExcRate * A.AfterTax) DAmt, 0.00 As CAmt ");
            SQL.AppendLine("     FROM tblsocontractdownpayment2dtl2 A ");
            SQL.AppendLine("     INNER JOIN tblparameter B ON B.ParCode = 'CustomerAcNoAR' AND B.ParValue IS NOT NULL ");
            SQL.AppendLine("     AND A.DocNo = @DocNo ");
            SQL.AppendLine("     INNER JOIN tblsocontractdownpayment2hdr C ON A.DocNo = C.DocNo ");
            SQL.AppendLine("     INNER JOIN tblsocontracthdr D ON C.SOContractDocNo = D.DocNo ");


            // UANG MUKA
            SQL.AppendLine("    UNION ALL ");

            SQL.AppendLine("    SELECT CONCAT(D.Parvalue, C.CtCode) AcNo,  ");
            
            SQL.AppendLine("    (@ExcRateARDP * ROUND(B.TerminDP, 2)) AS DAmt, 0.00 AS CAmt ");
            SQL.AppendLine("    FROM tblsocontractdownpayment2hdr A ");
            SQL.AppendLine("    INNER JOIN tblsocontractdownpayment2dtl B ON A.DocNo = B.DocNo AND A.DocNo = @DocNo ");
            SQL.AppendLine("    INNER JOIN tblsocontracthdr C ON A.SOContractDocNo = C.DocNo ");
            SQL.AppendLine("    INNER JOIN tblparameter D ON D.Parcode = 'CustomerAcNoDownpayment' AND D.Parvalue IS NOT NULL ");

            SQL.AppendLine("    UNION ALL ");

            SQL.AppendLine("    SELECT CONCAT(D.Parvalue, C.CtCode) AcNo,  ");
            
            SQL.AppendLine("    (@ExcRateARDP * ROUND(B.TerminDP, 2)) AS DAmt, 0.00 AS CAmt ");
            SQL.AppendLine("    FROM tblsocontractdownpayment2hdr A ");
            SQL.AppendLine("    INNER JOIN tblsocontractdownpayment2dtl2 B ON A.DocNo = B.DocNo AND A.DocNo = @DocNo ");
            SQL.AppendLine("    INNER JOIN tblsocontracthdr C ON A.SOContractDocNo = C.DocNo ");
            SQL.AppendLine("    INNER JOIN tblparameter D ON D.Parcode = 'CustomerAcNoDownpayment' AND D.Parvalue IS NOT NULL ");

            //sisa uang muka

            #region old
            //if (ExcRateARDP > 0)
            //{
            //    SQL.AppendLine("    UNION ALL ");

            //    SQL.AppendLine("    SELECT D.Parvalue AcNo,  ");
            //    SQL.AppendLine("    Case ");
            //    SQL.AppendLine("        When ((@ExcRate * ROUND(B.TerminDP, 2))-(@ExcRateARDP * ROUND(B.TerminDP, 2))) <= 0 then 0.00 ");
            //    SQL.AppendLine("        else((@ExcRate * ROUND(B.TerminDP, 2))-(@ExcRateARDP * ROUND(B.TerminDP, 2))) ");
            //    SQL.AppendLine("    End As DAmt, ");
            //    SQL.AppendLine("    Case ");
            //    SQL.AppendLine("        When ((@ExcRate * ROUND(B.TerminDP, 2))-(@ExcRateARDP * ROUND(B.TerminDP, 2))) >= 0 then 0.00 ");
            //    SQL.AppendLine("        else (-((@ExcRate * ROUND(B.TerminDP, 2))-(@ExcRateARDP * ROUND(B.TerminDP, 2)))) ");
            //    SQL.AppendLine("    End As CAmt ");
            //    SQL.AppendLine("    FROM tblsocontractdownpayment2hdr A ");
            //    SQL.AppendLine("    INNER JOIN tblsocontractdownpayment2dtl2 B ON A.DocNo = B.DocNo AND A.DocNo = @DocNo ");
            //    SQL.AppendLine("    INNER JOIN tblsocontracthdr C ON A.SOContractDocNo = C.DocNo ");
            //    SQL.AppendLine("    INNER JOIN tblparameter D ON D.Parcode = 'AcNoForForeignCurrencyExchangeGains' AND D.Parvalue IS NOT NULL ");
            //}
            #endregion 

            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select ParValue As AcNo, ");
            SQL.AppendLine("        0.00 As DAmt, ");
            SQL.AppendLine("        0.00 As CAmt ");
            SQL.AppendLine("        From TblParameter Where ParCode='AcNoForForeignCurrencyExchangeGains' And ParValue Is Not Null ");

            //PIUTANG UNINVOICE
            SQL.AppendLine("    UNION ALL ");

            SQL.AppendLine("    SELECT CONCAT(B.ParValue, D.CtCode) AcNo, ");
            SQL.AppendLine("    0.00 AS DAmt,  ");
            
            SQL.AppendLine("    (@ExcRate * A.Amt) CAmt ");
            SQL.AppendLine("    FROM tblsocontractdownpayment2dtl A ");
            SQL.AppendLine("    INNER JOIN tblparameter B ON B.ParCode = 'CustomerAcNoNonInvoice' AND B.ParValue IS NOT NULL ");
            SQL.AppendLine("    AND A.DocNo = @DocNo ");
            SQL.AppendLine("    INNER JOIN tblsocontractdownpayment2hdr C ON A.DocNo = C.DocNo ");
            SQL.AppendLine("    INNER JOIN tblsocontracthdr D ON C.SOContractDocNo = D.DocNo ");

            SQL.AppendLine("    UNION ALL ");

            SQL.AppendLine("    SELECT CONCAT(B.ParValue, D.CtCode) AcNo, ");
            SQL.AppendLine("    0.00 AS DAmt, ");
            
            SQL.AppendLine("     (@ExcRate * A.Amt) CAmt ");
            SQL.AppendLine("     FROM tblsocontractdownpayment2dtl2 A ");
            SQL.AppendLine("     INNER JOIN tblparameter B ON B.ParCode = 'CustomerAcNoNonInvoice' AND B.ParValue IS NOT NULL ");
            SQL.AppendLine("     AND A.DocNo = @DocNo ");
            SQL.AppendLine("     INNER JOIN tblsocontractdownpayment2hdr C ON A.DocNo = C.DocNo ");
            SQL.AppendLine("     INNER JOIN tblsocontracthdr D ON C.SOContractDocNo = D.DocNo ");

           // HUTANG PAJAK
            SQL.AppendLine("     UNION ALL ");

            SQL.AppendLine("     SELECT B.AcNo2 AcNo,  ");
            SQL.AppendLine("     0.00 As DAmt,  ");
            
            SQL.AppendLine("    (@ExcRate * A.TaxAmt) As CAmt ");
            SQL.AppendLine("    FROM tblsocontractdownpayment2hdr A ");
            SQL.AppendLine("   INNER JOIN tbltax B ON A.TaxCode = B.TaxCode AND A.DocNo = @DocNo ");
            SQL.AppendLine("         AND B.AcNo2 IS NOT NULL ");
                    
            SQL.AppendLine("   UNION ALL ");

            SQL.AppendLine("    SELECT B.AcNo2 AcNo,  ");
            SQL.AppendLine("     0.00 As DAmt,  ");
            
            SQL.AppendLine("   (@ExcRate * A.TaxAmt2) As CAmt ");
            SQL.AppendLine("   FROM tblsocontractdownpayment2hdr A ");
            SQL.AppendLine("    INNER JOIN tbltax B ON A.TaxCode2 = B.TaxCode AND A.DocNo = @DocNo ");
            SQL.AppendLine("        AND B.AcNo2 IS NOT NULL ");
                    
            SQL.AppendLine("    UNION ALL ");

            SQL.AppendLine("     SELECT B.AcNo2 AcNo,  ");
            SQL.AppendLine("     0.00 As DAmt,  ");
            
            SQL.AppendLine("   (@ExcRate * A.TaxAmt3) As CAmt ");
            SQL.AppendLine("   FROM tblsocontractdownpayment2hdr A ");
            SQL.AppendLine("   INNER JOIN tbltax B ON A.TaxCode3 = B.TaxCode AND A.DocNo = @DocNo ");
            SQL.AppendLine("       AND B.AcNo2 IS NOT NULL ");

            SQL.AppendLine("   UNION ALL ");

            SQL.AppendLine("    SELECT B.AcNo2 AcNo,  ");
            SQL.AppendLine("     0.00 As DAmt,  ");
            
            SQL.AppendLine("     (@ExcRate * A.TaxAmt4) As CAmt ");
            SQL.AppendLine("     FROM tblsocontractdownpayment2hdr A ");
            SQL.AppendLine("     INNER JOIN tbltax B ON A.TaxCode4 = B.TaxCode AND A.DocNo = @DocNo ");
            SQL.AppendLine("     AND B.AcNo2 IS NOT NULL ");
        

            //List of COA

            SQL.AppendLine("    Union All ");
            SQL.AppendLine("        Select B.AcNo, ");
            
            SQL.AppendLine("        (@ExcRate * B.DAmt) DAmt, ");
            
            SQL.AppendLine("        (@ExcRate * B.CAmt) CAmt ");
            SQL.AppendLine("        From TblSOCOntractDownpayment2Hdr A ");
            SQL.AppendLine("        Inner Join TblSOCOntractDownpayment2Dtl3 B On A.DocNo=B.DocNo ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");


            SQL.AppendLine("    ) Tbl ");
            SQL.AppendLine("    Where AcNo Is Not Null ");
            SQL.AppendLine("    Group By AcNo  ");
            SQL.AppendLine(") T;  ");

            if (mCostCenterFormulaForAPAR == "2")
            {
                SQL.AppendLine("Update TblJournalHdr A ");
                SQL.AppendLine("Inner Join TblSOCOntractDownpayment2Hdr B On A.DocNo = B.JournalDocNo ");
                SQL.AppendLine("Inner Join TblParameter C On C.ParCode = 'CCCodeForJournalAPAR' And C.ParValue Is Not Null ");
                SQL.AppendLine("Set A.CCCode = C.ParValue ");
                SQL.AppendLine("Where B.DocNo = @DocNo ");
                SQL.AppendLine("    And A.CCCode Is Null ");
                SQL.AppendLine("    And Exists ( ");
                SQL.AppendLine("        Select 1 From TblJournalDtl ");
                SQL.AppendLine("        Where DocNo = A.DocNo And AcNo like '5.%' ");
                SQL.AppendLine("    ); ");
            }

            SQL.AppendLine("Update TblJournalDtl ");
            SQL.AppendLine("Set SOContractDocNo = @SOContractDocNo ");
            SQL.AppendLine("Where DocNo = @JournalDocNo ");
            SQL.AppendLine("And SOContractDocNo Is Null; ");

            SQL.AppendLine("Update TblJournalDtl A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select DAmt, CAmt From (");
            SQL.AppendLine("        Select Sum(DAmt) as DAmt, Sum(CAmt) as CAmt ");
            SQL.AppendLine("        From TblJournalDtl Where DocNo=@JournalDocNo ");
            SQL.AppendLine("    ) Tbl ");
            SQL.AppendLine(") B On 0=0 ");
            SQL.AppendLine("Set ");
            SQL.AppendLine("    A.DAmt=Case When B.DAmt<B.CAmt Then Abs(B.CAmt-B.DAmt) Else 0 End, ");
            SQL.AppendLine("    A.CAmt=Case When B.DAmt>B.CAmt Then Abs(B.DAmt-B.CAmt) Else 0 End ");
            SQL.AppendLine("Where A.DocNo=@JournalDocNo ");
            SQL.AppendLine("And A.AcNo In ( ");
            SQL.AppendLine("    Select ParValue From TblParameter ");
            SQL.AppendLine("    Where ParCode='AcNoForForeignCurrencyExchangeGains' ");
            SQL.AppendLine("    And ParValue Is Not Null ");
            SQL.AppendLine("    );");

            SQL.AppendLine("Delete From TblJournalDtl ");
            SQL.AppendLine("Where DocNo=@JournalDocNo ");
            SQL.AppendLine("And (DAmt=0 And CAmt=0) ");
            SQL.AppendLine("And AcNo In ( ");
            SQL.AppendLine("    Select ParValue From TblParameter ");
            SQL.AppendLine("    Where ParCode='AcNoForForeignCurrencyExchangeGains' ");
            SQL.AppendLine("    And ParValue Is Not Null ");
            SQL.AppendLine("    );");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@SOContractDocNo", TxtSOContractDocNo.Text);
            Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(Sm.GetDte(DteDocDt), 1));
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@MainCurCode", mMainCurCode);
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<Decimal>(ref cm, "@ExcRate", Decimal.Parse(TxtRate.Text));
            Sm.CmParam<Decimal>(ref cm, "@ExcRateARDP", ExcRateARDP == 0 ? Decimal.Parse(TxtRate.Text) : ExcRateARDP);
            Sm.CmParam<String>(ref cm, "@Remark", string.Concat(MeeRemark.Text, " | SO Contract# : ", TxtSOContractDocNo.Text));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveJournal2()
        {
            var SQL = new StringBuilder();
            var CurrentDt = Sm.ServerCurrentDate();
            var DocDt = Sm.GetDte(DteDocDt);
            var IsClosingJournalUseCurrentDt = Sm.IsClosingJournalUseCurrentDt(DocDt);

            SQL.AppendLine("Update TblSOContractDownpayment2Hdr Set JournalDocNo2=@JournalDocNo Where DocNo=@DocNo And CancelInd='Y';");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, Remark, CCCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, ");
            if (IsClosingJournalUseCurrentDt)
                SQL.AppendLine("Replace(CurDate(), '-', ''), ");
            else
                SQL.AppendLine("DocDt, ");
            SQL.AppendLine("Concat('Cancelling ', JnDesc) As JnDesc, ");
            SQL.AppendLine("MenuCode, MenuDesc, Remark, CCCode, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblJournalHdr Where DocNo In (Select JournalDocNo From TblSOContractDownpayment2Hdr Where DocNo=@DocNo And CancelInd='Y');");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, SOContractDocNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, DNo, AcNo, CAMt As DAmt, DAmt As CAmt, SOContractDocNo, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblJournalDtl Where DocNo In (Select JournalDocNo From TblSOContractDownpayment2Hdr Where DocNo=@DocNo And CancelInd='Y');");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (IsClosingJournalUseCurrentDt)
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(CurrentDt, 1));
            else
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(DocDt, 1));
              
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        //private string GenerateVoucherRequestDocNo()
        //{
        //    var SQL = new StringBuilder();
        //    SQL.Append("Select Concat( ");
        //    SQL.Append("@Yr,'/', @Mth, '/', ");
        //    SQL.Append("( ");
        //    SQL.Append("    Select IfNull((");
        //    SQL.Append("        Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) As Numb ");
        //    SQL.Append("        From ( ");
        //    SQL.Append("            Select Convert(Substring(DocNo, 7, 4), Decimal) As DocNo ");
        //    SQL.Append("            From TblVoucherRequestHdr ");
        //    SQL.Append("            Where Left(DocNo, 5)= Concat(@Yr,'/', @Mth) ");
        //    SQL.Append("            Order By Substring(DocNo, 7, 5) Desc Limit 1 ");
        //    SQL.Append("        ) As Temp ");
        //    SQL.Append("    ), '0001') As Number ");
        //    SQL.Append("), '/', ");
        //    SQL.Append("(Select DocAbbr From TblDocAbbreviation Where DocType='VoucherRequest') ");
        //    SQL.Append(") As DocNo ");
        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };
        //    Sm.CmParam<String>(ref cm, "@Yr", Sm.GetDte(DteDocDt).Substring(2, 2));
        //    Sm.CmParam<String>(ref cm, "@Mth", Sm.GetDte(DteDocDt).Substring(4, 2));
        //    return Sm.GetValue(cm);
        //}

        private void LueRequestEdit(
            iGrid Grd,
            DevExpress.XtraEditors.LookUpEdit Lue,
            ref iGCell fCell,
            ref bool fAccept,
            TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, 0).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, 0));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        private void SetLueOptionCode(ref DXE.LookUpEdit Lue)
        {
            try
            {
                Sm.SetLue2(ref Lue, "Select OptCode As Col1, OptDesc As Col2 From TblOption Where OptCat='AccountDescriptionOnSalesInvoice';", 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        internal string GetSelectedAcNo()
        {
            var SQL = string.Empty;
            if (Grd3.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd3, Row, 1).Length != 0)
                        SQL += "##" + Sm.GetGrdStr(Grd3, Row, 1) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        
        internal void ComputeOutstandingAmt()
        {
            //decimal mTDPAmtOld = 0m;
            //decimal mTDPAmt = Decimal.Parse(TxtSOContractDownpaymentAmt.Text);
            //decimal mSOContractAmt = Decimal.Parse(TxtSOContractAmt.Text);

            //string mTDPAmtOldTemp = string.Empty; string mTDPAmtOldTemp2 = string.Empty, mSOCD2Amt = string.Empty;

            #region 27/10/2020 Old code by dita
            //mTDPAmtOldTemp = Sm.GetValue("Select IfNull(SUM(B.TerminDP), 0.00) from TblSOContractDownpayment2Hdr A Inner Join TblSOContractDownpayment2Dtl B On A.DocNo = B.DocNo WHERE A.SOContractDownpaymentDocNo=@Param And A.CancelInd = 'N'; ", TxtSOContractDownpaymentDocNo.Text);
            //mTDPAmtOldTemp2 = Sm.GetValue("Select IfNull(SUM(B.TerminDP), 0.00) from TblSOContractDownpayment2Hdr A Inner Join TblSOContractDownpayment2Dtl2 B On A.DocNo = B.DocNo WHERE A.SOContractDownpaymentDocNo=@Param And A.CancelInd = 'N'; ", TxtSOContractDownpaymentDocNo.Text);

            //if (TxtSOContractDownpaymentDocNo.Text.Length != 0)
            //{
            //    if (mTDPAmtOldTemp.Length > 0) mTDPAmtOld += Decimal.Parse(mTDPAmtOldTemp);
            //    if (mTDPAmtOldTemp2.Length > 0) mTDPAmtOld += Decimal.Parse(mTDPAmtOldTemp2);
            //}
            //mOutstandingAmt = mTDPAmt - mTDPAmtOld;
            #endregion

            #region 18/11/2020 Old code by dita
            //mTDPAmtOldTemp = Sm.GetValue("Select IfNull(SUM(B.TerminDP), 0.00) from TblSOContractDownpayment2Hdr A Inner Join TblSOContractDownpayment2Dtl B On A.DocNo = B.DocNo WHERE A.SOContractDocNo=@Param And A.CancelInd = 'N'; ", TxtSOContractDocNo.Text);
            //mTDPAmtOldTemp2 = Sm.GetValue("Select IfNull(SUM(B.TerminDP), 0.00) from TblSOContractDownpayment2Hdr A Inner Join TblSOContractDownpayment2Dtl2 B On A.DocNo = B.DocNo WHERE A.SOContractDocNo=@Param And A.CancelInd = 'N'; ", TxtSOContractDocNo.Text);

            //if (TxtSOContractDocNo.Text.Length != 0)
            //{
            //    if (mTDPAmtOldTemp.Length > 0) mTDPAmtOld += Decimal.Parse(mTDPAmtOldTemp);
            //    if (mTDPAmtOldTemp2.Length > 0) mTDPAmtOld += Decimal.Parse(mTDPAmtOldTemp2);
            //}
            //mOutstandingAmt = mSOContractAmt - mTDPAmtOld;

            //// mOutstandingAmt = mTDPAmt - mTDPAmtNew + mTDPAmtOld;
            //TxtOutstandingAmt.EditValue = Sm.FormatNum(mOutstandingAmt, 0);
            #endregion

            //mSOCD2Amt = Sm.GetValue("Select IfNull(SUM(Amt), 0.00) from TblSOContractDownpayment2Hdr WHERE SOContractDocNo=@Param And CancelInd = 'N' And Status In ('O', 'A') ; ", TxtSOContractDocNo.Text);

            //if(TxtSOContractDocNo.Text.Length > 0 )
            //{
            //    mOutstandingAmt = mSOContractAmt - mTDPAmt - Decimal.Parse(mSOCD2Amt);
            //    TxtOutstandingAmt.EditValue = Sm.FormatNum(mOutstandingAmt, 0);
            //}

            decimal SOContractAmt = 0m, SOContractDownpaymentAmtBeforeTax = 0m, OtherAmtBeforeTax = 0m;
            string Value = string.Empty;

            if (TxtSOContractAmt.Text.Length > 0) SOContractAmt = decimal.Parse(TxtSOContractAmt.Text);

            Value = Sm.GetValue(
                "Select Sum((Percentage/100)*SOContractAmt) " +
                "From TblSOContractDownpaymentHdr " +
                "Where SOContractDocNo=@Param " +
                "And CancelInd = 'N' " +
                "And Status In ('O', 'A'); ",
                TxtSOContractDocNo.Text);

            if (Value.Length > 0) SOContractDownpaymentAmtBeforeTax = decimal.Parse(Value);

            Value = string.Empty;

            if (TxtDocNo.Text.Length == 0)
            {
                Value = Sm.GetValue(
                    "Select Sum(B.Amt) " +
                    "From TblSOContractDownpayment2Hdr A, TblSOContractDownpayment2Dtl B " +
                    "Where A.SOContractDocNo=@Param " +
                    "And A.DocNo=B.DocNo " +
                    "And A.CancelInd = 'N' " +
                    "And A.Status In ('O', 'A'); ",
                    TxtSOContractDocNo.Text);

                if (Value.Length > 0) OtherAmtBeforeTax += decimal.Parse(Value);

                Value = string.Empty;

                Value = Sm.GetValue(
                    "Select Sum(B.Amt) " +
                    "From TblSOContractDownpayment2Hdr A, TblSOContractDownpayment2Dtl2 B " +
                    "Where A.SOContractDocNo=@Param " +
                    "And A.DocNo=B.DocNo " +
                    "And A.CancelInd = 'N' " +
                    "And A.Status In ('O', 'A'); ",
                    TxtSOContractDocNo.Text);

                if (Value.Length > 0) OtherAmtBeforeTax += decimal.Parse(Value);
            }
            else
            {
                Value = Sm.GetValue(
                    "Select Sum(B.Amt) " +
                    "From TblSOContractDownpayment2Hdr A, TblSOContractDownpayment2Dtl B " +
                    "Where A.SOContractDocNo=@Param1 " +
                    "And A.DocNo<>@Param2 " +
                    "And A.DocNo=B.DocNo " +
                    "And A.CancelInd = 'N' " +
                    "And A.Status In ('O', 'A'); ",
                    TxtSOContractDocNo.Text, TxtDocNo.Text, string.Empty);

                if (Value.Length > 0) OtherAmtBeforeTax += decimal.Parse(Value);

                Value = string.Empty;

                Value = Sm.GetValue(
                    "Select Sum(B.Amt) " +
                    "From TblSOContractDownpayment2Hdr A, TblSOContractDownpayment2Dtl2 B " +
                    "Where A.SOContractDocNo=@Param1 " +
                    "And A.DocNo<>@Param2 " +
                    "And A.DocNo=B.DocNo " +
                    "And A.CancelInd = 'N' " +
                    "And A.Status In ('O', 'A'); ",
                    TxtSOContractDocNo.Text, TxtDocNo.Text, string.Empty);

                if (Value.Length > 0) OtherAmtBeforeTax += decimal.Parse(Value);
            }

            mOutstandingAmt = SOContractAmt - SOContractDownpaymentAmtBeforeTax - OtherAmtBeforeTax;
            TxtOutstandingAmt.EditValue = Sm.FormatNum(mOutstandingAmt, 0);
        }
       

        internal void ComputeAmt()
        {
            if (TxtDocNo.Text.Length > 0) return;

            decimal
                Amt = 0m,
                TaxAmt = 0m,
                TaxAmt2 = 0m,
                TaxAmt3 = 0m,
                TaxAmt4 = 0m,
                TaxRate = 0m,
                TaxRate2 = 0m,
                TaxRate3 = 0m,
                TaxRate4 = 0m
                ;
            string 
                TaxCode = Sm.GetLue(LueTaxCode),
                TaxCode2 = Sm.GetLue(LueTaxCode2),
                TaxCode3 = Sm.GetLue(LueTaxCode3),
                TaxCode4 = Sm.GetLue(LueTaxCode4);

            if (TaxCode.Length != 0) TaxRate = Sm.GetValueDec("Select TaxRate From TblTax Where TaxCode=@Param;", TaxCode);
            if (TaxCode2.Length != 0) TaxRate2 = Sm.GetValueDec("Select TaxRate From TblTax Where TaxCode=@Param;", TaxCode2);
            if (TaxCode3.Length != 0) TaxRate3 = Sm.GetValueDec("Select TaxRate From TblTax Where TaxCode=@Param;", TaxCode3);
            if (TaxCode4.Length != 0) TaxRate4 = Sm.GetValueDec("Select TaxRate From TblTax Where TaxCode=@Param;", TaxCode4);

            if (Grd1.Rows.Count > 1)
            {
                for (int r = 0; r< Grd1.Rows.Count; r++)
                {
                    if (Sm.GetGrdStr(Grd1, r, 2).Length > 0)
                    {
                        Grd1.Cells[r, 17].Value = (Sm.GetGrdDec(Grd1, r, 15) - Sm.GetGrdDec(Grd1, r, 16));
                        Grd1.Cells[r, 18].Value = TaxRate2*0.01m * (Sm.GetGrdDec(Grd1, r, 17));
                        Grd1.Cells[r, 19].Value = TaxRate3 * 0.01m * (Sm.GetGrdDec(Grd1, r, 17));
                        Grd1.Cells[r, 20].Value = (Sm.GetGrdDec(Grd1, r, 17) + Sm.GetGrdDec(Grd1, r, 18)) + Sm.GetGrdDec(Grd1, r, 19);
                        Amt += Sm.GetGrdDec(Grd1, r, 20);
                        TaxAmt2 += Sm.GetGrdDec(Grd1, r, 18);
                        TaxAmt3 += Sm.GetGrdDec(Grd1, r, 19);
                    }
                }       
            }

            if (Grd2.Rows.Count > 1)
            {
                for (int r = 0; r < Grd2.Rows.Count; r++)
                {
                    if (Sm.GetGrdStr(Grd2, r, 2).Length > 0)
                    {
                        Grd2.Cells[r, 17].Value = (Sm.GetGrdDec(Grd2, r, 15) - Sm.GetGrdDec(Grd2, r, 16));
                        Grd2.Cells[r, 18].Value = TaxRate * 0.01m * (Sm.GetGrdDec(Grd2, r, 17));
                        Grd2.Cells[r, 19].Value = TaxRate4 * 0.01m * (Sm.GetGrdDec(Grd2, r, 17));
                        Grd2.Cells[r, 20].Value = (Sm.GetGrdDec(Grd2, r, 17) + Sm.GetGrdDec(Grd2, r, 18)) + Sm.GetGrdDec(Grd2, r, 19);
                        Amt += Sm.GetGrdDec(Grd2, r, 20);
                        TaxAmt += Sm.GetGrdDec(Grd2, r, 18);
                        TaxAmt4 += Sm.GetGrdDec(Grd2, r, 19);
                    }
                }
            }
            for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
            {
                string AcType = Sm.GetValue("Select AcType From TblCoa Where AcNo=@Param;", Sm.GetGrdStr(Grd3, Row, 1));
                if (Sm.GetGrdBool(Grd3, Row, 3))
                {
                    if (AcType == "D")
                        Amt += Sm.GetGrdDec(Grd3, Row, 4);
                    else
                        Amt -= Sm.GetGrdDec(Grd3, Row, 4);
                }
                if (Sm.GetGrdBool(Grd3, Row, 5))
                {
                    if (AcType == "C")
                        Amt += Sm.GetGrdDec(Grd3, Row, 6);
                    else
                        Amt -= Sm.GetGrdDec(Grd3, Row, 6);
                }
            }
            //Amt += (TaxAmt + TaxAmt2 + TaxAmt3);

            TxtTaxAmt.Text = Sm.FormatNum(TaxAmt, 0);
            TxtTaxAmt2.Text = Sm.FormatNum(TaxAmt2, 0);
            TxtTaxAmt3.Text = Sm.FormatNum(TaxAmt3, 0);
            TxtTaxAmt4.Text = Sm.FormatNum(TaxAmt4, 0);
            TxtAmt.Text = Sm.FormatNum(Amt, 0);

            ComputeOutstandingAmt();
            TxtCurrentAmt.EditValue = Sm.FormatNum(TxtAmt.Text, 0);
            TxtBalanceAmt.EditValue = Sm.FormatNum(Decimal.Parse(TxtSOContractDownpaymentAmt.Text) - Decimal.Parse(TxtOthersAmt.Text) - Decimal.Parse(TxtCurrentAmt.Text), 0);
        }

        #endregion

        #endregion

        #region Event

        #region Grd

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1)
            {
                if (!Sm.IsTxtEmpty(TxtSOContractDocNo, "SO contract#", false))
                        Sm.FormShowDialog(new FrmSOContractDownpayment2Dlg2(this));    
            }

            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmDOCtVerify("***");
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
            ComputeAmt();
            ComputeAmtBeforeTax();
        }

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    if (e.ColIndex == 1)
                    {
                        e.DoDefault = false;
                        if (e.KeyChar == Char.Parse(" "))
                            Sm.FormShowDialog(new FrmSOContractDownpayment2Dlg2(this));
                    }
                }
            }
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmDOCtVerify("***");
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }
        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (e.ColIndex == 16 && Sm.GetGrdDec(Grd1, e.RowIndex, 16) > 0 && BtnSave.Enabled)
            {
                ComputeAmt();
            }
        }
       

        private void Grd2_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1)
            {
                if (!Sm.IsTxtEmpty(TxtSOContractDocNo, "SO contract#", false))
                    Sm.FormShowDialog(new FrmSOContractDownpayment2Dlg3(this));
            }

            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd2, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmDOCtVerify("***");
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd2, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        private void Grd2_KeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd2, e, BtnSave);
            Sm.GrdKeyDown(Grd2, e, BtnFind, BtnSave);
            ComputeAmt();
            ComputeAmtBeforeTax();
        }

        private void Grd2_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    if (e.ColIndex == 1)
                    {
                        e.DoDefault = false;
                        if (e.KeyChar == Char.Parse(" "))
                            Sm.FormShowDialog(new FrmSOContractDownpayment2Dlg3(this));
                    }
                }
            }
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd2, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmDOCtVerify("***");
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd2, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        private void Grd2_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (e.ColIndex == 16 && Sm.GetGrdDec(Grd2, e.RowIndex, 16) > 0 && BtnSave.Enabled)
                ComputeAmt();
        }
       


        private void Grd3_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 4 && Sm.GetGrdDec(Grd3, e.RowIndex, 4) > 0)
                {
                    Grd3.Cells[e.RowIndex, 6].Value = 0m;
                }

                if (e.ColIndex == 6 && Sm.GetGrdDec(Grd3, e.RowIndex, 6) > 0)
                {
                    Grd3.Cells[e.RowIndex, 4].Value = 0m;
                }

                if (e.ColIndex == 3 || e.ColIndex == 5)
                    Grd3.Cells[e.RowIndex, 10].Value = Sm.GetGrdBool(Grd3, e.RowIndex, e.ColIndex);

                if (Sm.IsGrdColSelected(new int[] { 3, 4, 5, 6 }, e.ColIndex))
                    ComputeAmt();
            }
        }

        private void Grd3_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0 && TxtDocNo.Text.Length == 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmSOContractDownpayment2Dlg(this));
            }

            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 8 }, e.ColIndex))
            {
                LueRequestEdit(Grd3, LueOption, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd3, e.RowIndex);
                Sm.SetGrdNumValueZero(ref Grd3, Grd3.Rows.Count - 1, new int[] { 4, 6 });
                SetLueOptionCode(ref LueOption);
            }
        }

        private void Grd3_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && TxtDocNo.Text.Length == 0) Sm.FormShowDialog(new FrmSOContractDownpayment2Dlg(this));
        }

        private void Grd3_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.GrdRemoveRow(Grd3, e, BtnSave);
                ComputeAmt();
                Sm.GrdEnter(Grd3, e);
                Sm.GrdTabInLastCell(Grd3, e, BtnFind, BtnSave);
            }
        }

        #endregion

        #region Misc Control Event

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        private void TxtTaxInvoiceNo_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtTaxInvoiceNo);
        }

        private void LueTaxCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueTaxCode, new Sm.RefreshLue1(Sl.SetLueTaxCode));
                ComputeAmt();
            }
        }

        private void TxtTaxInvoiceNo2_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtTaxInvoiceNo2);
        }

        private void LueTaxCode2_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueTaxCode2, new Sm.RefreshLue1(Sl.SetLueTaxCode));
                ComputeAmt();
            }
        }

        private void TxtTaxInvoiceNo3_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtTaxInvoiceNo3);
        }

        private void LueTaxCode3_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueTaxCode3, new Sm.RefreshLue1(Sl.SetLueTaxCode));
                ComputeAmt();
            }
        }

        private void TxtTaxInvoiceNo4_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtTaxInvoiceNo4);
        }

        private void LueTaxCode4_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueTaxCode4, new Sm.RefreshLue1(Sl.SetLueTaxCode));
                ComputeAmt();
            }
        }

        private void LueOption_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueOption, new Sm.RefreshLue1(SetLueOptionCode));
        }

        private void LueOption_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd3, ref fAccept, e);
        }

        private void LueOption_Leave(object sender, EventArgs e)
        {
            if (LueOption.Visible && fAccept && fCell.ColIndex == 8)
            {
                if (Sm.GetLue(LueOption).Length == 0)
                {
                    Grd3.Cells[fCell.RowIndex, 7].Value =
                    Grd3.Cells[fCell.RowIndex, 8].Value = null;
                }
                else
                {
                    Grd3.Cells[fCell.RowIndex, 7].Value = Sm.GetLue(LueOption);
                    Grd3.Cells[fCell.RowIndex, 8].Value = LueOption.GetColumnValue("Col2");
                }
                LueOption.Visible = false;
            }
        }

        private void LueBankAcCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueBankAcCode, new Sm.RefreshLue1(Sl.SetLueBankAcCode));
        }

        private void BtnSOContractDocNo_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormShowDialog(new FrmSOContractDownpayment2Dlg4(this));
        }
        private void BtnSOContractDocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtSOContractDocNo, "SO contract#", false))
            {
                var f = new FrmSOContract2("***");
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtSOContractDocNo.Text;
                f.ShowDialog();
            }
        }

        private void BtnSOContractDownpaymentDocNo_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtSOContractDocNo, "SO Contract Downpayment#", false))
            {
                var f = new FrmSOContractDownpayment("***");
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtSOContractDownpaymentDocNo.Text;
                f.ShowDialog();
            }
        }

        private void TxtRate_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
                Sm.FormatNumTxt(TxtRate, 0);
        }

        #endregion

        #endregion

        #region report class

        public class SOCDHdr
        {
            public string CompanyLogo { get; set; }
            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyAddressCity { get; set; }
            public string CompanyPhoneNumber { get; set; }

            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string CustomerPO { get; set; }
            public string PODate { get; set; }
            public string Quality { get; set; }
            public string OriginCountry { get; set; }
            public string Remark { get; set; }
            public string CtAddressCity { get; set; }
            public string CtAddress { get; set; }
            public string CtName { get; set; }
            public string Terbilang { get; set; }
            public string SKBDN { get; set; }
            public decimal Amount { get; set; }
            public decimal TaxAmount { get; set; }
            public decimal TaxAmount2 { get; set; }
            public decimal TaxAmount3 { get; set; }
            public decimal TaxAmount4 { get; set; }

            public string TaxName { get; set; }
            public string TaxName2 { get; set; }
            public string TaxName3 { get; set; }
            public string TaxName4 { get; set; }
            public string BankName { get; set; }
            public string CurCode { get; set; }
            public string BankAcNo { get; set; }
            public string CompanyFooterImage { get; set; }
            public string Date { get; set; }
            public string Month { get; set; }
            public string Year { get; set; }
            public decimal mMaxLimitAmtForSOContractDPSign { get; set; }
            public decimal mMinLimitAmtForSOContractDPSign { get; set; }
            public string SOContractTerminDPPrintOutSignName1 { get; set; }
            public string SOContractTerminDPPrintOutSignName2 { get; set; }
            public string SOContractTerminDPPrintOutSignName3 { get; set; }
            public string PosName1 { get; set; }
            public string PosName2 { get; set; }
            public string PosName3 { get; set; }

            public string QualityofGoods { get; set; }
        }
        public class SOCDDtl
        {
            public int No { get; set; }
            public string LocalCode { get; set; }
            public string SOContractNo { get; set; }
            public string ItName { get; set; }
            public string Specification { get; set; }
            public decimal Qty { get; set; }
            public string Uom { get; set; }
            public decimal Price { get; set; }
            public decimal TotalPrice { get; set; }
            public decimal TotalAmount { get; set; }
        }

        public class SOCDDtl2
        {
            public string Specification { get; set; }

        }

        private class SOCDSign
        {
            public string Signature { get; set; }
            public string UserName { get; set; }
            public string POSName { get; set; }

        }

        #endregion  
    }
}
