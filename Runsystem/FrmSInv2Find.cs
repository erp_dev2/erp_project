﻿#region Update
/*
    02/07/2019 [WED] integrasi SInv BETA dengan SInv standard (dihilangkan Source nya)
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmSInv2Find : RunSystem.FrmBase2
    {
        #region Field

        private FrmSInv2 mFrmParent;
        string mSQL;

        #endregion

        #region Constructor

        public FrmSInv2Find(FrmSInv2 FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -30);
                Sl.SetLueCtCode(ref LueCtCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select * From ( ");
            SQL.AppendLine("  Select X.DocNo, X.DocDt, D.CtCode, F.CtName, B.SectionNo, ");
            SQL.AppendLine("  X.LocalDocNo, ");
            SQL.AppendLine("   Case ");
            SQL.AppendLine("   When D.Status  = 'C' Then 'Cancelled' ");
            SQL.AppendLine("   When D.Status  = 'P' Then 'Planning' ");
            SQL.AppendLine("   When D.Status  = 'R' Then 'Released' ");
            SQL.AppendLine("   When D.Status  = 'F' Then 'Fullfiled' ");
            SQL.AppendLine("   End As StatusSinv, ");
            SQL.AppendLine("  Case B.SectionNo ");
            SQL.AppendLine("      When '1' Then A.Cnt1 ");
            SQL.AppendLine("      When '2' Then A.cnt2 ");
            SQL.AppendLine("      When '3' Then A.Cnt3 ");
            SQL.AppendLine("      When '4' Then A.Cnt4 ");
            SQL.AppendLine("      When '5' Then A.Cnt5 ");
            SQL.AppendLine("      When '6' Then A.Cnt6 ");
            SQL.AppendLine("      When '7' Then A.Cnt7 ");
            SQL.AppendLine("      When '8' Then A.Cnt8 ");
            SQL.AppendLine("      When '9' Then A.Cnt9 ");
            SQL.AppendLine("      When '10' Then A.Cnt10 ");
            SQL.AppendLine("      When '11' Then A.Cnt11 ");
            SQL.AppendLine("      When '12' Then A.Cnt12 ");
            SQL.AppendLine("      When '13' Then A.Cnt13 ");
            SQL.AppendLine("      When '14' Then A.Cnt14 ");
            SQL.AppendLine("      When '15' Then A.Cnt15 ");
            SQL.AppendLine("      When '16' Then A.Cnt16 ");
            SQL.AppendLine("      When '17' Then A.Cnt17 ");
            SQL.AppendLine("      When '18' Then A.Cnt18 ");
            SQL.AppendLine("      When '19' Then A.Cnt19 ");
            SQL.AppendLine("      When '20' Then A.Cnt20 ");
            SQL.AppendLine("      When '21' Then A.Cnt21 ");
            SQL.AppendLine("      When '22' Then A.Cnt22 ");
            SQL.AppendLine("      When '23' Then A.Cnt23 ");
            SQL.AppendLine("      When '24' Then A.Cnt24 ");
            SQL.AppendLine("      When '25' Then A.Cnt25 ");
            SQL.AppendLine("  End As Cnt, ");
            SQL.AppendLine("   Case B.SectionNo ");
            SQL.AppendLine("      When '1' Then A.Seal1 ");
            SQL.AppendLine("      When '2' Then A.Seal2 ");
            SQL.AppendLine("      When '3' Then A.Seal3 ");
            SQL.AppendLine("      When '4' Then A.Seal4 ");
            SQL.AppendLine("      When '5' Then A.Seal5 ");
            SQL.AppendLine("      When '6' Then A.Seal6 ");
            SQL.AppendLine("      When '7' Then A.Seal7 ");
            SQL.AppendLine("      When '8' Then A.Seal8 ");
            SQL.AppendLine("      When '9' Then A.Seal9 ");
            SQL.AppendLine("      When '10' Then A.Seal10 ");
            SQL.AppendLine("      When '11' Then A.Seal11 ");
            SQL.AppendLine("      When '12' Then A.Seal12 ");
            SQL.AppendLine("      When '13' Then A.Seal13 ");
            SQL.AppendLine("      When '14' Then A.Seal14 ");
            SQL.AppendLine("      When '15' Then A.Seal15 ");
            SQL.AppendLine("      When '16' Then A.Seal16 ");
            SQL.AppendLine("      When '17' Then A.Seal17 ");
            SQL.AppendLine("      When '18' Then A.Seal18 ");
            SQL.AppendLine("      When '19' Then A.Seal19 ");
            SQL.AppendLine("      When '20' Then A.Seal20 ");
            SQL.AppendLine("      When '21' Then A.Seal21 ");
            SQL.AppendLine("      When '22' Then A.Seal22 ");
            SQL.AppendLine("      When '23' Then A.Seal23 ");
            SQL.AppendLine("      When '24' Then A.Seal24 ");
            SQL.AppendLine("      When '25' Then A.Seal25 ");
            SQL.AppendLine("  End As Seal, B.ItCode, E.ItName, B.SODOcNo,  ");
            SQL.AppendLine("   G.PackagingUnitUomCode, B.QtyPackagingUnit, B.Qty, ");
            SQL.AppendLine("   J.PriceUomCode, B.QtyInventory, E.InventoryUomCode,");
            SQL.AppendLine("   A.createBy, A.CreateDt, A.LastUpDt, A.LastUpBy ");
            SQL.AppendLine("  From TblSinv X ");
            SQL.AppendLine("  Inner Join TblPlHdr A On X.PLDocNo = A.DocNo ");
            //SQL.AppendLine("       And X.Source = '2'  ");
            SQL.AppendLine("  Inner Join TblPlDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("  Inner Join TblSIHdr C On A.SIDocNo = C.DocNo");
            SQL.AppendLine("  Inner Join TblSp D On C.SPDocNo = D.DocNo ");
            SQL.AppendLine("  Inner Join TblItem E On B.ItCode=E.ItCode");
            SQL.AppendLine("  Inner Join TblCustomer F On D.CtCode= F.CtCode ");
            SQL.AppendLine("  Inner Join TblSoDtl G On B.SODocNo = G.DocNo And B.SODNo = G.Dno");
            SQL.AppendLine("     Inner Join TblSOHdr H On G.DocNo = H.DocNo And B.SODocNo = H.DocNo  ");
            SQL.AppendLine("     Inner Join TblCtQtDtl I On H.CtQtDocNo = I.DocNo And G.CtQtDNo = I.Dno ");
            SQL.AppendLine("     Inner Join TblItemPriceHdr J On I.ItemPriceDocNo = J.DocNo ");
            SQL.AppendLine("     Inner Join TblitemPriceDtl K On I.ItemPriceDocNo = K.DocNo And I.ItemPriceDNo = K.Dno ");
            SQL.AppendLine("  Where (X.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine(" ) T ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 25;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document Number", 
                        "Document"+Environment.NewLine+"Date",
                        "Status",
                        "Customer",
                        "Container",

                        //6-10
                        "Seal",
                        "Item Code",
                        "",
                        "Item Name",
                        "SO",
                      
                        
                        //11-15
                        "",
                        "Packaging",
                        "Quantity"+Environment.NewLine+"Packaging",
                        "Quantity"+Environment.NewLine+"Sales",
                        "UoM",
                        

                        //16-20
                        "Quantity"+Environment.NewLine+"Inventory",
                        "UoM",
                        "Created"+Environment.NewLine+"By",
                        "Created"+Environment.NewLine+"Date", 
                        "Created"+Environment.NewLine+"Time",
 
                        //21-24
                        "Last"+Environment.NewLine+"Updated By",
                        "Last"+Environment.NewLine+"Updated Date", 
                        "Last"+Environment.NewLine+"Updated Time",
                        "Local"+Environment.NewLine+"Document"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        130, 80, 100, 200, 100,   
                        
                        //6-10
                        100, 80, 20, 200, 130,    
                        
                        //11-15
                        20, 100, 100, 100, 80,    
                        
                        //16-20
                        100, 80, 100, 100, 100, 

                        //21-24
                        100, 100, 100, 130
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 8, 11 });
            Sm.GrdFormatDec(Grd1, new int[] { 13, 14, 16 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2, 19, 22 });
            Sm.GrdFormatTime(Grd1, new int[] { 20, 21 });
            Grd1.Cols[24].Move(2);
            Sm.GrdColInvisible(Grd1, new int[] { 7, 8, 11, 18, 19, 20, 21, 22, 23, 24 }, false);
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 9, 10, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 7, 8, 11, 18, 19, 20, 21, 22, 23, 24 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, new string[] { "T.DocNo", "T.LocalDocNo" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCode), "T.CtCode", true);
                Sm.FilterStr(ref Filter, ref cm,  TxtItCode.Text, new string [] {"T.ItCode", "T.ItName"});
                Sm.FilterStr(ref Filter, ref cm, TxtSODocNo.Text, "T.SODocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtLocalDocNo.Text, "T.LocalDocNo", false);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By T.DocNo, T.SectionNo;",
                        new string[]
                        {
                            //0
                            "DocNo",

                            //1-5
                            "DocDt", "StatusSinv", "CtName", "Cnt", "Seal",  
                            
                            //6-10
                            "ItCode", "ItName", "SODOcNo", "PackagingUnitUomCode", "QtyPackagingUnit",  
                            
                            //11-15
                            "Qty", "PriceUomCode", "QtyInventory", "InventoryUomCode", "CreateBy", 
                            
                            //16-19
                            "CreateDt",  "LastUpBy", "LastUpDt", "LocalDocNo"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 12, 9);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 13, 10);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 14, 11);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 15, 12);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 16, 13);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 17, 14);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 18, 15);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 19, 16);
                            Sm.SetGrdValue("T", Grd1, dr, c, Row, 20, 16);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 21, 17);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 22, 18);
                            Sm.SetGrdValue("T", Grd1, dr, c, Row, 23, 18);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 24, 19);
                        }, true, true, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
               mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
               this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            e.DoDefault = false;
            if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length != 0)
            {
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 7);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 11 && Sm.GetGrdStr(Grd1, e.RowIndex, 10).Length != 0)
            {
                var f = new FrmSO2(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 10);
                f.ShowDialog();
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            //Grd1.Rows.AutoHeight();
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

       

        #endregion

        #region Event

        #region Misc Control Event

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");
        }

        private void ChkSODocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "SO#");
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender); 
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void TxtSODocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void TxtLocalDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkLocalDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Local document#");
        }

        #endregion

        #endregion
    }
}
