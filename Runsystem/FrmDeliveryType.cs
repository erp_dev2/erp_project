﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmDeliveryType : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "";
        internal FrmDeliveryTypeFind FrmFind;

        #endregion

        #region Constructor

        public FrmDeliveryType(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetFormControl(mState.View);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDTCode, TxtDTName, MeeRemark
                    }, true);
                    TxtDTCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDTCode, TxtDTName, MeeRemark
                    }, false);
                    TxtDTCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDTName, MeeRemark
                    }, false);
                    TxtDTName.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDTCode, TxtDTName, MeeRemark
            });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmDeliveryTypeFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDTCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDTCode, "", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblDeliveryType Where DTCode=@DTCode" };
                Sm.CmParam<String>(ref cm, "@DTCode", TxtDTCode.Text);
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into TblDeliveryType(DTCode, DTName, CtQtInd,  Remark, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@DTCode, @DTName, @CtQtInd, @Remark, @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("   Update DTName=@DTName, CtQtInd=@CtQtInd, Remark=@Remark, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@DTCode", TxtDTCode.Text);
                Sm.CmParam<String>(ref cm, "@DTName", TxtDTName.Text);
                Sm.CmParam<String>(ref cm, "@CtQtInd", ChkCtQtInd.Checked?"Y":"N");
                Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.ExecCommand(cm);

                ShowData(TxtDTCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string DTCtCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@DTCode", DTCtCode);
                Sm.ShowDataInCtrl(
                        ref cm,
                        "Select DTCode, DTName, CtQtInd, Remark From TblDeliveryType Where DTCode=@DTCode",
                        new string[] 
                        {
                            "DTCode", "DTName", "CtQtInd", "Remark"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtDTCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtDTName.EditValue = Sm.DrStr(dr, c[1]);
                            ChkCtQtInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y");
                            MeeRemark.EditValue = Sm.DrStr(dr, c[3]);
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDTCode, "Delivery type code", false) ||
                Sm.IsTxtEmpty(TxtDTName, "Delivery type name", false) ||
                IsDTCodeExisted();
        }

        private bool IsDTCodeExisted()
        {
            if (!TxtDTCode.Properties.ReadOnly && Sm.IsDataExist("Select DTCode From TblDeliveryType Where DTCode='" + TxtDTCode.Text + "' Limit 1"))
            {
                Sm.StdMsg(mMsgType.Warning, "Delivery type code ( " + TxtDTCode.Text + " ) already existed.");
                return true;
            }
            return false;
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDTCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtDTCode);
        }

        private void TxtDTName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtDTName);
        }

        #endregion

        #endregion
    }
}
