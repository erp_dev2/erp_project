﻿#region Update
/*
 *  [ICA/SIER] new apps 
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmSalesInvoiceNotes : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "";
        internal FrmSalesInvoiceNotesFind FrmFind;

        #endregion

        #region Constructor

        public FrmSalesInvoiceNotes(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            BtnPrint.Visible = false;
            Sl.SetLueCtCtCode(ref LueCtCtCode);
            SetFormControl(mState.View);
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        LueCtCtCode, MeeNotes
                    }, true);
                    LueCtCtCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        LueCtCtCode, MeeNotes
                    }, false);
                    LueCtCtCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(LueCtCtCode, true);
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        MeeNotes
                    }, false);
                    LueCtCtCode.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                LueCtCtCode, MeeNotes
            });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmSalesInvoiceNotesFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsLueEmpty(LueCtCtCode, "")) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into TblSalesInvoiceNotes (CtCtCode, Notes, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@CtCtCode, @Notes, @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("   Update Notes=@Notes, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@CtCtCode", Sm.GetLue(LueCtCtCode));
                Sm.CmParam<String>(ref cm, "@Notes", MeeNotes.Text);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.ExecCommand(cm);

                ShowData(Sm.GetLue(LueCtCtCode));
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string CtCtCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@CtCtCode", CtCtCode);
                Sm.ShowDataInCtrl(
                        ref cm,
                        "Select * From TblSalesInvoiceNotes Where CtCtCode=@CtCtCode",
                        new string[] 
                        {
                            "CtCtCode", "Notes"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            Sm.SetLue(LueCtCtCode,  Sm.DrStr(dr, c[0]));
                            MeeNotes.EditValue = Sm.DrStr(dr, c[1]);
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsLueEmpty(LueCtCtCode, "Customer Category") ||
                Sm.IsTxtEmpty(MeeNotes, "Notes", false);
                IsCtCtCodeExisted();
        }

        private bool IsCtCtCodeExisted()
        {
            if (!LueCtCtCode.Properties.ReadOnly && Sm.IsDataExist("Select CtCtCode From TblSalesInvoiceNotes Where CtCtCode='" + Sm.GetLue(LueCtCtCode) + "' Limit 1"))
            {
                Sm.StdMsg(mMsgType.Warning, "Customer category code ( " + LueCtCtCode.Text + " ) already existed.");
                return true;
            }
            return false;
        }

        #endregion

        #endregion
    }
}
