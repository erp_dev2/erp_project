﻿#region Update
/* 
    04/04/2020 [TKG/IMS] New application
    15/06/2020 [VIN/IMS] tambah validasi cancel dari voucher request special RHA
    03/11/2020 [TKG/IMS] tambah payroll group
    21/07/2021 [TKG/IMS] isi department di TblRHADtl untuk kebutuhan journal
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

//using System.IO;
//using System.Net;
//using Renci.SshNet;

#endregion

namespace RunSystem
{
    public partial class FrmRHA16 : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string
            mMenuCode = string.Empty,
            mAccessInd = string.Empty,
            mDocNo = string.Empty,
            mMainCurCode = string.Empty;
        internal FrmRHA16Find FrmFind;
        
        #endregion

        #region Constructor

        public FrmRHA16(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Religius Holiday Allowance";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                Sl.SetLuePayrollGrpCode(ref LuePGCode);
                Sl.SetLueCurCode(ref LueCurCode);
                base.FrmLoad(sender, e);
                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grid 1

            Grd1.Cols.Count = 14;
            Grd1.FrozenArea.ColCount = 3;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "", 

                        //1-5
                        "Employee's"+Environment.NewLine+"Code",
                        "Employee's Name",
                        "Position",
                        "Department",
                        "Religion",

                        //6-10
                        "Join",
                        "Resign",
                        "Salary (A)", // Grade
                        "Grade's"+Environment.NewLine+"Allowance (B)", // Grade
                        "Employee's"+Environment.NewLine+"Allowance (C)", // Employee

                        //11-13
                        "A+B+C",
                        "Prorated",
                        "Amount"
                    },
                     new int[] 
                    {
                        //0
                        20,
                        
                        //1-5
                        100, 200, 180, 180, 150,  
                        
                        //6-10
                        80, 80, 130, 130, 130, 

                        //11-13
                        130, 100, 130
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 0 });
            Sm.GrdFormatDate(Grd1, new int[] { 6, 7 });
            Sm.GrdColCheck(Grd1, new int[] { 12 });
            Sm.GrdFormatDec(Grd1, new int[] { 8, 9, 10, 11, 12, 13 }, 0);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 });
            
            #endregion

            #region Grid 2

            Grd2.Cols.Count = 4;
            Grd2.ReadOnly = true;

            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[] 
                    {
                        //0
                        "Checked By", 
                        
                        //1-3
                        "Status",
                        "Date",
                        "Remark"
                    },
                    new int[] { 150, 100, 100, 400 }
                );
            Sm.GrdFormatDate(Grd2, new int[] { 2 });

            #endregion
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, MeeCancelReason, ChkCancelInd, DteHolidayDt, LuePGCode, MeeRemark 
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0 });
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { DteDocDt, DteHolidayDt, LuePGCode, MeeRemark }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0 });
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeCancelReason, ChkCancelInd }, false);
                    MeeCancelReason.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, MeeCancelReason, TxtStatus, DteHolidayDt, 
                LuePGCode, LueCurCode, MeeRemark
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtAmt }, 0);
            ChkCancelInd.Checked = false;
            ClearGrd();
        }

        private void ClearGrd()
        {
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
            Sm.SetGrdBoolValueFalse(Grd1, 0, new int[] { 12 });
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 8, 9, 10, 11, 13 });

            Grd2.Rows.Clear();
            Grd2.Rows.Count = 1;
        }

        private void ClearData2()
        {
            TxtAmt.EditValue = Sm.FormatNum(0m, 0);
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
            Sm.SetGrdBoolValueFalse(Grd1, 0, new int[] { 12 });
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 8, 9, 10, 11, 13 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmRHA16Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sm.SetLue(LueCurCode, mMainCurCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || 
                IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "RHA", "TblRHAHdr");
            
            var cml = new List<MySqlCommand>();

            cml.Add(SaveRHAHdr(DocNo));
            for (int r = 0; r< Grd1.Rows.Count; r++)
                if (Sm.GetGrdStr(Grd1, r, 1).Length > 0) cml.Add(SaveRHADtl(DocNo, r));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsDteEmpty(DteHolidayDt, "Holiday date") ||
                Sm.IsLueEmpty(LuePGCode, "Payroll's group") ||
                Sm.IsTxtEmpty(TxtAmt, "Total amount", true) ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                IsEmployeeWithTheSameRHAYrExisted() ||
                IsEmployeeResigneeExisted()
                ;
        }

        private bool IsEmployeeResigneeExisted()
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string EmpCode = string.Empty, Filter = string.Empty;
            if (Grd1.Rows.Count >= 1)
            {
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, r, 1);
                    if (EmpCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(EmpCode=@EmpCode0" + r.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode0" + r.ToString(), EmpCode);
                    }
                }
            }
            if (Filter.Length>0)
                Filter = " And (" + Filter + ") ";
            else
                Filter = " And 1=0 ";

            SQL.AppendLine("Select Concat(EmpName, ' (', EmpCode, ')') ");
            SQL.AppendLine("From TblEmployee ");
            SQL.AppendLine("Where ResignDt Is Not Null ");
            SQL.AppendLine("And ResignDt<@HolidayDt ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("Limit 1;");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@HolidayDt", Sm.GetDte(DteHolidayDt));

            EmpCode = Sm.GetValue(cm);

            if (EmpCode.Length > 0)
            {
                Sm.StdMsg(mMsgType.Warning, 
                    "Employee : " + EmpCode + Environment.NewLine +
                    "This employee already resigned."
                    );
                return true;
            }
            return false;
        }

        private bool IsEmployeeWithTheSameRHAYrExisted()
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string EmpCode = string.Empty, Filter = string.Empty;
            if (Grd1.Rows.Count >= 1)
            {
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, r, 1);
                    if (EmpCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(EmpCode=@EmpCode0" + r.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode0" + r.ToString(), EmpCode);
                    }
                }
            }
            if (Filter.Length > 0)
                Filter = " And (" + Filter + ") ";
            else
                Filter = " And 1=0 ";

            SQL.AppendLine("Select Concat(EmpName, ' (', EmpCode, ')')  From TblEmployee ");
            SQL.AppendLine("Where EmpCode In (");
            SQL.AppendLine("    Select T2.EmpCode ");
            SQL.AppendLine("    From TblRHAHdr T1 ");
            SQL.AppendLine("    Inner Join TblRHADtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    And T1.Status In ('O', 'A') ");
            SQL.AppendLine("    And Left(HolidayDt, 4)=@HolidayYr ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("Limit 1;");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@HolidayYr", Sm.Left(Sm.GetDte(DteHolidayDt), 4));

            EmpCode = Sm.GetValue(cm);

            if (EmpCode.Length > 0)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Employee : " + EmpCode + Environment.NewLine +
                    "This employee already processed."
                    );
                return true;
            }
            return false;
        }   

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 employee.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int r = 0; r < Grd1.Rows.Count - 1; r++)
            {
                if (
                    Sm.IsGrdValueEmpty(Grd1, r, 2, false, "Employee is empty.") ||
                    Sm.IsGrdValueEmpty(Grd1, r, 13, true, "Residual holiday allowance amount should be bigger than 0.00.")
                ) return true;
            }
            return false;
        }

        private MySqlCommand SaveRHAHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblRHAHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelReason, CancelInd, Status, HolidayDt, PGCode, CurCode, Amt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, Null, 'N', 'A', @HolidayDt, @PGCode, @CurCode, @Amt, @Remark, @UserCode, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParamDt(ref cm, "@HolidayDt", Sm.GetDte(DteHolidayDt));
            Sm.CmParam<String>(ref cm, "@PGCode", Sm.GetLue(LuePGCode));
            Sm.CmParam<String>(ref cm, "@CurCode", mMainCurCode);
            Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtAmt.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveRHADtl(string DocNo, int Row)
        {
            // Value = Salary
            // Value2 = Grade's Fixed Allowance
            // Value3 = Employee's Fixed Allowance
            // Value4 = Value+Value2+Value3

            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblRHADtl(DocNo, EmpCode, DeptCode, Value, Value2, Value3, Value4, Amt, ProrateInd, CreateBy, CreateDt) " +
                    "Values (@DocNo, @EmpCode, (Select DeptCode From TblEmployee Where EmpCode=@EmpCode), @Value, @Value2, @Value3, @Value4, @Amt, @ProrateInd, @UserCode, CurrentDateTime());"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<Decimal>(ref cm, "@Value", Sm.GetGrdDec(Grd1, Row, 8));
            Sm.CmParam<Decimal>(ref cm, "@Value2", Sm.GetGrdDec(Grd1, Row, 9));
            Sm.CmParam<Decimal>(ref cm, "@Value3", Sm.GetGrdDec(Grd1, Row, 10));
            Sm.CmParam<Decimal>(ref cm, "@Value4", Sm.GetGrdDec(Grd1, Row, 11));
            Sm.CmParam<String>(ref cm, "@ProrateInd", Sm.GetGrdBool(Grd1, Row, 12) ? "Y" : "N");
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd1, Row, 13));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || 
                IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(CancelRLPHdr());
            
            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDocumentNotCancelled() ||
                IsDataAlreadyCancelled() ||
                IsDataProcessedAlready()
                ;
        }

        private bool IsDocumentNotCancelled()
        {
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this document.");
                return true;
            }
            return false;
        }

        private bool IsDataAlreadyCancelled()
        {
            return Sm.IsDataExist(
                "Select DocNo From TblRHAHdr Where (CancelInd='Y' Or Status='C') And DocNo=@Param;",
                TxtDocNo.Text,
                "This document already cancelled."
                );
        }
        private bool IsDataProcessedAlready()
        {
            return Sm.IsDataExist(
                "Select 1 From TblVoucherRequestSpecialHdr A Inner Join TblVoucherRequestSpecialDtl B On A.DocNo=B.DocNo Where A.CancelInd='N' And B.DocNo2=@Param;",
                TxtDocNo.Text,
                "Data already processed into voucher Request RHA.");
        }
        //private bool IsDataAlreadyProcessed()
        //{
        //    return Sm.IsDataExist(
        //        "Select DocNo From TblVoucherHdr Where CancelInd='N' And VoucherRequestDocNo=@Param;",
        //        TxtVoucherRequestDocNo.Text,
        //        "This document already processed to voucher."
        //        );
        //}

        private MySqlCommand CancelRLPHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblRHAHdr Set ");
            SQL.AppendLine("    CancelReason=@CancelReason, CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N' And Status In ('O', 'A') ");
            
            SQL.AppendLine("; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowRHAHdr(DocNo);
                ShowRHADtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowRHAHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelReason, A.CancelInd, ");
            SQL.AppendLine("Case A.Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' Else 'Outstanding' End As StatusDesc, ");
            SQL.AppendLine("A.HolidayDt, A.CurCode, A.Amt, A.PGCode, A.Remark ");
            SQL.AppendLine("From TblRHAHdr A ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInCtrl(
                 ref cm, SQL.ToString(),
                 new string[] 
                {
                    //0
                    "DocNo",
                    
                    //1-5
                    "DocDt", "CancelReason", "CancelInd", "StatusDesc", "HolidayDt", 
                    
                    //6-9
                    "CurCode", "Amt", "PGCode", "Remark"
                },
                 (MySqlDataReader dr, int[] c) =>
                 {
                     TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                     Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                     MeeCancelReason.EditValue = Sm.DrStr(dr, c[2]);
                     ChkCancelInd.Checked = Sm.DrStr(dr, c[3]) == "Y";
                     TxtStatus.EditValue = Sm.DrStr(dr, c[4]);
                     Sm.SetDte(DteHolidayDt, Sm.DrStr(dr, c[5]));
                     Sm.SetLue(LueCurCode, Sm.DrStr(dr, c[6]));
                     TxtAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[7]), 0);
                     Sm.SetLue(LuePGCode, Sm.DrStr(dr, c[8]));
                     MeeRemark.EditValue = Sm.DrStr(dr, c[9]);
                 }, true
             );
        }

        private void ShowRHADtl(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.EmpCode, B.EmpName, C.PosName, D.DeptName, E.OptDesc As Religious, ");
            SQL.AppendLine("B.JoinDt, B.ResignDt, ");
            SQL.AppendLine("A.Value, A.Value2, A.Value3, A.Value4, A.ProrateInd, A.Amt ");
            SQL.AppendLine("From TblRHADtl A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode=B.EmpCode ");
            SQL.AppendLine("Left Join TblPosition C On B.PosCode=C.PosCode ");
            SQL.AppendLine("Left Join TblDepartment D On B.DeptCode=D.DeptCode ");
            SQL.AppendLine("Left Join TblOption E On B.Religion=E.OptCode And E.OptCat='Religion' ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Order By D.DeptName, B.EmpName;");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            
            Sm.ShowDataInGrid(
                    ref Grd1, ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "EmpCode", 

                        //1-5
                        "EmpName", "PosName", "DeptName", "Religious", "JoinDt",

                        //6-10
                        "ResignDt", "Value", "Value2", "Value3", "Value4", 
                        
                        //11-12
                        "ProrateInd", "Amt", 
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 12, 11);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 12);
                    }, false, false, true, false
            );
            Sm.SetGrdBoolValueFalse(Grd1, Grd1.Rows.Count - 1, new int[] { 12 });
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 8, 9, 10, 11, 13 });
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mMainCurCode = Sm.GetParameter("MainCurCode");
        }

        internal void ComputeAmt()
        {
            var Amt = 0m;
            for (int r = 0; r < Grd1.Rows.Count; r++)
                Amt += Sm.GetGrdDec(Grd1, r, 13);
            TxtAmt.EditValue = Sm.FormatNum(Amt, 0);
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void DteHolidayDt_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) ClearData2();
        }

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        private void LuePGCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LuePGCode, new Sm.RefreshLue1(Sl.SetLuePayrollGrpCode));
        }

        #endregion

        #region Grid Event

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    if (e.ColIndex == 0 && !Sm.IsDteEmpty(DteHolidayDt, "Holiday date") && !Sm.IsLueEmpty(LuePGCode, "Payroll's group"))
                    {
                        e.DoDefault = false;
                        if (e.KeyChar == Char.Parse(" "))
                            Sm.FormShowDialog(new FrmRHA16Dlg(this, Sm.GetDte(DteHolidayDt), Sm.GetLue(LuePGCode)));
                    }
                }
            }
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && !Sm.IsDteEmpty(DteHolidayDt, "Holiday date") && !Sm.IsLueEmpty(LuePGCode, "Payroll's group"))
                Sm.FormShowDialog(new FrmRHA16Dlg(this, Sm.GetDte(DteHolidayDt), Sm.GetLue(LuePGCode)));
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0)
            {
                Sm.GrdRemoveRow(Grd1, e, BtnSave);
                ComputeAmt();
            }
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        #endregion

        #endregion
    }
}
