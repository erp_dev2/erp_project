﻿namespace RunSystem
{
    partial class FrmChangePassword
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmChangePassword));
            this.TxtUser = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtUserName = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtOldPwd = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtNewPwd = new DevExpress.XtraEditors.TextEdit();
            this.BtnShowOldPass = new DevExpress.XtraEditors.SimpleButton();
            this.BtnShowNewPassword = new DevExpress.XtraEditors.SimpleButton();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtUser.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtUserName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtOldPwd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtNewPwd.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(619, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.panel1.Size = new System.Drawing.Size(100, 363);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnCancel.Size = new System.Drawing.Size(100, 31);
            this.BtnCancel.Visible = false;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnSave.Size = new System.Drawing.Size(100, 31);
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDelete.Size = new System.Drawing.Size(100, 31);
            this.BtnDelete.Visible = false;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnEdit.Size = new System.Drawing.Size(100, 31);
            this.BtnEdit.Visible = false;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnInsert.Size = new System.Drawing.Size(100, 31);
            this.BtnInsert.Visible = false;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnFind.Size = new System.Drawing.Size(100, 31);
            this.BtnFind.Visible = false;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnPrint.Size = new System.Drawing.Size(100, 31);
            this.BtnPrint.Visible = false;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.BtnShowNewPassword);
            this.panel2.Controls.Add(this.BtnShowOldPass);
            this.panel2.Controls.Add(this.TxtNewPwd);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.TxtOldPwd);
            this.panel2.Controls.Add(this.TxtUserName);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.TxtUser);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.panel2.Size = new System.Drawing.Size(619, 363);
            // 
            // TxtUser
            // 
            this.TxtUser.EnterMoveNextControl = true;
            this.TxtUser.Location = new System.Drawing.Point(161, 88);
            this.TxtUser.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.TxtUser.Name = "TxtUser";
            this.TxtUser.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtUser.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUser.Properties.Appearance.Options.UseBackColor = true;
            this.TxtUser.Properties.Appearance.Options.UseFont = true;
            this.TxtUser.Properties.MaxLength = 50;
            this.TxtUser.Size = new System.Drawing.Size(396, 28);
            this.TxtUser.TabIndex = 10;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.CausesValidation = false;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(111, 93);
            this.label2.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 22);
            this.label2.TabIndex = 9;
            this.label2.Text = "User";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtUserName
            // 
            this.TxtUserName.EnterMoveNextControl = true;
            this.TxtUserName.Location = new System.Drawing.Point(161, 123);
            this.TxtUserName.Margin = new System.Windows.Forms.Padding(7, 8, 7, 8);
            this.TxtUserName.Name = "TxtUserName";
            this.TxtUserName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtUserName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUserName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtUserName.Properties.Appearance.Options.UseFont = true;
            this.TxtUserName.Properties.MaxLength = 80;
            this.TxtUserName.Size = new System.Drawing.Size(396, 28);
            this.TxtUserName.TabIndex = 12;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(61, 129);
            this.label1.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 22);
            this.label1.TabIndex = 11;
            this.label1.Text = "User Name";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtOldPwd
            // 
            this.TxtOldPwd.EditValue = "";
            this.TxtOldPwd.EnterMoveNextControl = true;
            this.TxtOldPwd.Location = new System.Drawing.Point(161, 157);
            this.TxtOldPwd.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
            this.TxtOldPwd.Name = "TxtOldPwd";
            this.TxtOldPwd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtOldPwd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.TxtOldPwd.Properties.Appearance.Options.UseFont = true;
            this.TxtOldPwd.Properties.Appearance.Options.UseForeColor = true;
            this.TxtOldPwd.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.TxtOldPwd.Properties.MaxLength = 255;
            this.TxtOldPwd.Properties.PasswordChar = '*';
            this.TxtOldPwd.Size = new System.Drawing.Size(396, 30);
            this.TxtOldPwd.TabIndex = 14;
            this.TxtOldPwd.ToolTip = "Password";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(41, 165);
            this.label3.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(117, 22);
            this.label3.TabIndex = 13;
            this.label3.Text = "Old Password";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(31, 201);
            this.label4.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(124, 22);
            this.label4.TabIndex = 16;
            this.label4.Text = "New Password";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtNewPwd
            // 
            this.TxtNewPwd.EditValue = "";
            this.TxtNewPwd.EnterMoveNextControl = true;
            this.TxtNewPwd.Location = new System.Drawing.Point(161, 195);
            this.TxtNewPwd.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
            this.TxtNewPwd.Name = "TxtNewPwd";
            this.TxtNewPwd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtNewPwd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.TxtNewPwd.Properties.Appearance.Options.UseFont = true;
            this.TxtNewPwd.Properties.Appearance.Options.UseForeColor = true;
            this.TxtNewPwd.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.TxtNewPwd.Properties.MaxLength = 255;
            this.TxtNewPwd.Properties.PasswordChar = '*';
            this.TxtNewPwd.Size = new System.Drawing.Size(396, 30);
            this.TxtNewPwd.TabIndex = 17;
            this.TxtNewPwd.ToolTip = "Password";
            // 
            // BtnShowOldPass
            // 
            this.BtnShowOldPass.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnShowOldPass.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnShowOldPass.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnShowOldPass.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnShowOldPass.Appearance.Options.UseBackColor = true;
            this.BtnShowOldPass.Appearance.Options.UseFont = true;
            this.BtnShowOldPass.Appearance.Options.UseForeColor = true;
            this.BtnShowOldPass.Appearance.Options.UseTextOptions = true;
            this.BtnShowOldPass.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnShowOldPass.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnShowOldPass.Image = ((System.Drawing.Image)(resources.GetObject("BtnShowOldPass.Image")));
            this.BtnShowOldPass.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnShowOldPass.Location = new System.Drawing.Point(563, 157);
            this.BtnShowOldPass.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.BtnShowOldPass.Name = "BtnShowOldPass";
            this.BtnShowOldPass.Size = new System.Drawing.Size(34, 33);
            this.BtnShowOldPass.TabIndex = 15;
            this.BtnShowOldPass.ToolTip = "Show / Hide Old Password";
            this.BtnShowOldPass.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnShowOldPass.ToolTipTitle = "Run System";
            this.BtnShowOldPass.Click += new System.EventHandler(this.BtnShowOldPass_Click);
            // 
            // BtnShowNewPassword
            // 
            this.BtnShowNewPassword.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnShowNewPassword.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnShowNewPassword.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnShowNewPassword.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnShowNewPassword.Appearance.Options.UseBackColor = true;
            this.BtnShowNewPassword.Appearance.Options.UseFont = true;
            this.BtnShowNewPassword.Appearance.Options.UseForeColor = true;
            this.BtnShowNewPassword.Appearance.Options.UseTextOptions = true;
            this.BtnShowNewPassword.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnShowNewPassword.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnShowNewPassword.Image = ((System.Drawing.Image)(resources.GetObject("BtnShowNewPassword.Image")));
            this.BtnShowNewPassword.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnShowNewPassword.Location = new System.Drawing.Point(560, 196);
            this.BtnShowNewPassword.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.BtnShowNewPassword.Name = "BtnShowNewPassword";
            this.BtnShowNewPassword.Size = new System.Drawing.Size(34, 33);
            this.BtnShowNewPassword.TabIndex = 18;
            this.BtnShowNewPassword.ToolTip = "Show /Hide New Password";
            this.BtnShowNewPassword.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnShowNewPassword.ToolTipTitle = "Run System";
            this.BtnShowNewPassword.Click += new System.EventHandler(this.BtnShowNewPassword_Click);
            // 
            // FrmChangePassword
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 22F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(719, 363);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "FrmChangePassword";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmChangePassword_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmChangePassword_FormClosed);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtUser.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtUserName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtOldPwd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtNewPwd.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.TextEdit TxtUserName;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.TextEdit TxtUser;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.TextEdit TxtOldPwd;
        private DevExpress.XtraEditors.TextEdit TxtNewPwd;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        public DevExpress.XtraEditors.SimpleButton BtnShowOldPass;
        public DevExpress.XtraEditors.SimpleButton BtnShowNewPassword;
    }
}