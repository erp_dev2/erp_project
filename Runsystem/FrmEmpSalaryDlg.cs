﻿#region Update
/*
    26/09/2017 [TKG] tambah employee's salary for social security.
    16/10/2017 [TKG] filter berdasarkan otorisasi group thd department
    25/10/2017 [TKG] tambah performance status.
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmEmpSalaryDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmEmpSalary mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmEmpSalaryDlg(FrmEmpSalary FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mFrmParent.mIsFilterByDeptHR?"Y":"N");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 10;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Employee's Code",
                        "Employee's Name",
                        "Old Code",
                        "Position",
                        "Department",

                        //6-9
                        "Site",
                        "Join Date",
                        "Basic Salary",
                        "Performance"+Environment.NewLine+"Status"
                    },
                     new int[] 
                    {
                        //0
                        50,
 
                        //1-5
                        100, 300, 80, 180, 180,
                        
                        //6-8
                        180, 100, 130, 0
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 7 });
            Sm.GrdFormatDec(Grd1, new int[] { 8 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3, 4, 7, 9 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3, 4, 7 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.EmpCode, A.EmpName, A.EmpCodeOld, B.PosName, C.DeptName, E.SiteName, A.JoinDt, D.BasicSalary, A.PerformanceStatus ");
            SQL.AppendLine("From TblEmployee A ");
            SQL.AppendLine("Left Join TblPosition B On A.PosCode=B.PosCode ");
            SQL.AppendLine("Left Join TblDepartment C On A.DeptCode=C.DeptCode ");
            SQL.AppendLine("Left Join TblGradeLevelHdr D On A.GrdLvlCode=D.GrdLvlCode ");
            SQL.AppendLine("Left Join TblSite E On A.SiteCode=E.SiteCode ");
            SQL.AppendLine("Where (A.ResignDt Is Null Or (A.ResignDt Is Not Null And A.ResignDt>@CurrentDate)) ");
            if (mFrmParent.mIsFilterByDeptHR)
            {
                SQL.AppendLine("And A.DeptCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupDepartment ");
                SQL.AppendLine("    Where DeptCode=IfNull(A.DeptCode, '') ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            if (mFrmParent.mIsFilterBySiteHR)
            {
                SQL.AppendLine("And A.SiteCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupSite ");
                SQL.AppendLine("    Where SiteCode=IfNull(A.SiteCode, '') ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@CurrentDate", Sm.ServerCurrentDateTime());
                Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "A.EmpCode", "A.EmpName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "A.DeptCode", true);
                
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By DeptName, EmpName;",
                        new string[] 
                        { 
                             //0
                             "EmpCode", 
                             
                             //1-5
                             "EmpName", 
                             "EmpCodeOld", 
                             "PosName",
                             "DeptName",
                             "SiteName",
                             
                             //6-8
                             "JoinDt",
                             "BasicSalary",
                             "PerformanceStatus"
                         },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                int Row = Grd1.CurRow.Index;

                string EmpCode = Sm.GetGrdStr(Grd1, Row, 1);

                mFrmParent.TxtEmpCode.EditValue = EmpCode;
                mFrmParent.TxtEmpName.EditValue = Sm.GetGrdStr(Grd1, Row, 2);
                mFrmParent.TxtEmpCodeOld.EditValue = Sm.GetGrdStr(Grd1, Row, 3);
                mFrmParent.TxtPosCode.EditValue = Sm.GetGrdStr(Grd1, Row, 4);
                mFrmParent.TxtDeptCode.EditValue = Sm.GetGrdStr(Grd1, Row, 5);
                mFrmParent.TxtSiteCode.EditValue = Sm.GetGrdStr(Grd1, Row, 6);
                Sm.SetDte(mFrmParent.DteJoinDt, Sm.GetGrdDate(Grd1, Row, 7).Substring(0, 8));
                Sm.SetLue(mFrmParent.LuePerformanceStatus, Sm.GetGrdStr(Grd1, Row, 9));

                mFrmParent.ShowEmployeeSalary(EmpCode);
                mFrmParent.ShowEmployeeAllowanceDeduction(EmpCode, "A");
                mFrmParent.ShowEmployeeAllowanceDeduction(EmpCode, "D");
                mFrmParent.ShowEmployeeSalarySS(EmpCode);
                mFrmParent.ShowEmployeeClaim(EmpCode);
                mFrmParent.ShowEmployeePerformance(EmpCode);

                this.Close();
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        #endregion

        #region Misc Control Nethod

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, mFrmParent.mIsFilterByDeptHR?"Y":"N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        #endregion

        #endregion

    }
}
