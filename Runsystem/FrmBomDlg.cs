﻿#region Update
/*
    31/10/2019 [DITA/IMS] tambah informasi ItCodeInternal dan Specifications
    07/11/2019 [WED/IMS] ItCodeInternal dan Specification kebalik
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmBomDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmBom mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmBomDlg(FrmBom FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sl.SetLueItCtCode(ref LueItCtCode);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 8;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Item's Code", 
                        "",
                        "Item's Name",
                        "UoM",
                        "Item's Category",

                        //6-7
                        "Specification",
                        "Item's Code"+Environment.NewLine+"Internal"
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 2 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 3, 4, 5, 6, 7 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 4, 7 });
            if (!mFrmParent.mIsBOMShowSpecifications) Sm.GrdColInvisible(Grd1, new int[] { 6 });
            Sm.SetGrdProperty(Grd1, true);
            Grd1.Cols[7].Move(3);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 4, 7 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdAutoSize(Grd1);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.ItCode, A.ItName, A.PlanningUomCode, B.ItCtName, A.Specification, A.ItCodeInternal ");
            SQL.AppendLine("From TblItem A, TblItemCategory B ");
            SQL.AppendLine("Where A.ItCtCode=B.ItCtCode And A.PlanningItemInd='Y' ");

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " And 0=0 ";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "A.ItCode", "A.ItName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueItCtCode), "A.ItCtCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.ItName",
                        new string[] 
                        { 
                            //0
                            "ItCode",

                            //1-5
                            "ItName",
                            "PlanningUomCode",
                            "ItCtName",
                            "ItCodeInternal",
                            "Specification"
                         },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 4);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.TxtItCode.Text = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
                mFrmParent.TxtItName.Text = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 3);
                mFrmParent.TxtUom.Text = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 4);
                this.Close();
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }
        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void LueItCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCtCode, new Sm.RefreshLue1(Sl.SetLueItCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkItCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Item's category");
        }

        #endregion

        #endregion

    }
}
