﻿namespace RunSystem
{
    partial class FrmClaim
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmClaim));
            this.TxtClaimName = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtClaimCode = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.ChkActInd = new DevExpress.XtraEditors.CheckEdit();
            this.TxtPlavonPeriod = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtAcDesc = new DevExpress.XtraEditors.TextEdit();
            this.BtnAcNo = new DevExpress.XtraEditors.SimpleButton();
            this.TxtAcNo = new DevExpress.XtraEditors.TextEdit();
            this.LblAcNo = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtClaimName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtClaimCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPlavonPeriod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.TxtAcDesc);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.BtnAcNo);
            this.panel2.Controls.Add(this.TxtPlavonPeriod);
            this.panel2.Controls.Add(this.TxtAcNo);
            this.panel2.Controls.Add(this.LblAcNo);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.ChkActInd);
            this.panel2.Controls.Add(this.TxtClaimName);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.TxtClaimCode);
            this.panel2.Controls.Add(this.label1);
            // 
            // TxtClaimName
            // 
            this.TxtClaimName.EnterMoveNextControl = true;
            this.TxtClaimName.Location = new System.Drawing.Point(110, 54);
            this.TxtClaimName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtClaimName.Name = "TxtClaimName";
            this.TxtClaimName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtClaimName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtClaimName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtClaimName.Properties.Appearance.Options.UseFont = true;
            this.TxtClaimName.Properties.MaxLength = 80;
            this.TxtClaimName.Size = new System.Drawing.Size(319, 20);
            this.TxtClaimName.TabIndex = 13;
            this.TxtClaimName.Validated += new System.EventHandler(this.TxtClaimName_Validated);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(33, 57);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 14);
            this.label2.TabIndex = 12;
            this.label2.Text = "Claim Name";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtClaimCode
            // 
            this.TxtClaimCode.EnterMoveNextControl = true;
            this.TxtClaimCode.Location = new System.Drawing.Point(110, 32);
            this.TxtClaimCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtClaimCode.Name = "TxtClaimCode";
            this.TxtClaimCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtClaimCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtClaimCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtClaimCode.Properties.Appearance.Options.UseFont = true;
            this.TxtClaimCode.Properties.MaxLength = 16;
            this.TxtClaimCode.Size = new System.Drawing.Size(76, 20);
            this.TxtClaimCode.TabIndex = 10;
            this.TxtClaimCode.Validated += new System.EventHandler(this.TxtClaimCode_Validated);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(36, 35);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 14);
            this.label1.TabIndex = 9;
            this.label1.Text = "Claim Code";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkActInd
            // 
            this.ChkActInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkActInd.Location = new System.Drawing.Point(191, 31);
            this.ChkActInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkActInd.Name = "ChkActInd";
            this.ChkActInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkActInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkActInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkActInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkActInd.Properties.Appearance.Options.UseFont = true;
            this.ChkActInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkActInd.Properties.Caption = "Active";
            this.ChkActInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkActInd.Size = new System.Drawing.Size(80, 22);
            this.ChkActInd.TabIndex = 11;
            // 
            // TxtPlavonPeriod
            // 
            this.TxtPlavonPeriod.EnterMoveNextControl = true;
            this.TxtPlavonPeriod.Location = new System.Drawing.Point(110, 75);
            this.TxtPlavonPeriod.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPlavonPeriod.Name = "TxtPlavonPeriod";
            this.TxtPlavonPeriod.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPlavonPeriod.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPlavonPeriod.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.TxtPlavonPeriod.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPlavonPeriod.Properties.Appearance.Options.UseFont = true;
            this.TxtPlavonPeriod.Properties.Appearance.Options.UseForeColor = true;
            this.TxtPlavonPeriod.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtPlavonPeriod.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtPlavonPeriod.Properties.MaxLength = 16;
            this.TxtPlavonPeriod.Size = new System.Drawing.Size(76, 20);
            this.TxtPlavonPeriod.TabIndex = 15;
            this.TxtPlavonPeriod.Validated += new System.EventHandler(this.TxtPlavonPeriod_Validated);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(22, 78);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 14);
            this.label3.TabIndex = 14;
            this.label3.Text = "Plavon Period";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(188, 78);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(32, 14);
            this.label4.TabIndex = 16;
            this.label4.Text = "Year";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtAcDesc
            // 
            this.TxtAcDesc.EnterMoveNextControl = true;
            this.TxtAcDesc.Location = new System.Drawing.Point(267, 97);
            this.TxtAcDesc.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcDesc.Name = "TxtAcDesc";
            this.TxtAcDesc.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcDesc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcDesc.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcDesc.Properties.Appearance.Options.UseFont = true;
            this.TxtAcDesc.Properties.MaxLength = 80;
            this.TxtAcDesc.Properties.ReadOnly = true;
            this.TxtAcDesc.Size = new System.Drawing.Size(368, 20);
            this.TxtAcDesc.TabIndex = 22;
            // 
            // BtnAcNo
            // 
            this.BtnAcNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnAcNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnAcNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAcNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnAcNo.Appearance.Options.UseBackColor = true;
            this.BtnAcNo.Appearance.Options.UseFont = true;
            this.BtnAcNo.Appearance.Options.UseForeColor = true;
            this.BtnAcNo.Appearance.Options.UseTextOptions = true;
            this.BtnAcNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnAcNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnAcNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnAcNo.Image")));
            this.BtnAcNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnAcNo.Location = new System.Drawing.Point(640, 94);
            this.BtnAcNo.Name = "BtnAcNo";
            this.BtnAcNo.Size = new System.Drawing.Size(16, 21);
            this.BtnAcNo.TabIndex = 23;
            this.BtnAcNo.ToolTip = "Find COA\'s Account";
            this.BtnAcNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnAcNo.ToolTipTitle = "Run System";
            this.BtnAcNo.Click += new System.EventHandler(this.BtnAcNo_Click);
            // 
            // TxtAcNo
            // 
            this.TxtAcNo.EnterMoveNextControl = true;
            this.TxtAcNo.Location = new System.Drawing.Point(110, 97);
            this.TxtAcNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAcNo.Name = "TxtAcNo";
            this.TxtAcNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAcNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAcNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAcNo.Properties.Appearance.Options.UseFont = true;
            this.TxtAcNo.Properties.MaxLength = 40;
            this.TxtAcNo.Properties.ReadOnly = true;
            this.TxtAcNo.Size = new System.Drawing.Size(152, 20);
            this.TxtAcNo.TabIndex = 21;
            // 
            // LblAcNo
            // 
            this.LblAcNo.AutoSize = true;
            this.LblAcNo.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblAcNo.ForeColor = System.Drawing.Color.Black;
            this.LblAcNo.Location = new System.Drawing.Point(13, 100);
            this.LblAcNo.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblAcNo.Name = "LblAcNo";
            this.LblAcNo.Size = new System.Drawing.Size(89, 14);
            this.LblAcNo.TabIndex = 20;
            this.LblAcNo.Text = "COA\'s Account";
            this.LblAcNo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // FrmClaim
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 233);
            this.Name = "FrmClaim";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtClaimName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtClaimCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPlavonPeriod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcDesc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAcNo.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.TextEdit TxtClaimName;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtClaimCode;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.CheckEdit ChkActInd;
        private System.Windows.Forms.Label label4;
        internal DevExpress.XtraEditors.TextEdit TxtPlavonPeriod;
        private System.Windows.Forms.Label label3;
        internal DevExpress.XtraEditors.TextEdit TxtAcDesc;
        public DevExpress.XtraEditors.SimpleButton BtnAcNo;
        internal DevExpress.XtraEditors.TextEdit TxtAcNo;
        private System.Windows.Forms.Label LblAcNo;
    }
}