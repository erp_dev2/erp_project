﻿#region Update
/*
    21/11/2018 [TKG] New application.
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using System.Drawing;
using System.Text;

using TenTec.Windows.iGridLib;
using MySql.Data.MySqlClient;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
#endregion

namespace RunSystem
{
    public partial class FrmRptProjectMonthlyCostDlg : RunSystem.FrmBase9
    {
        #region Field

        private FrmRptProjectMonthlyCost mFrmParent;
        internal string mCCtCode = string.Empty;

        #endregion

        #region Constructor

        public FrmRptProjectMonthlyCostDlg(FrmRptProjectMonthlyCost FrmParent)
        {
            try
            {
                InitializeComponent();
                mFrmParent = FrmParent;
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            
            base.FrmLoad(sender, e);
            SetGrd(); 
            ShowData();
        }


        private void SetGrd()
        {
            Grd1.Cols.Count = 6;
            Grd1.FrozenArea.ColCount = 1;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "Document#", 
                        "Date",
                        "Item's Code",
                        "Item's Name",
                        "Amount"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        130, 100, 100, 250, 150                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 2 });
            Sm.GrdFormatDec(Grd1, new int[] { 5 }, 0);
            Sm.SetGrdProperty(Grd1, false);
        }

        #endregion

        #region Show data

        private void ShowData()
        {
            Cursor.Current = Cursors.WaitCursor;

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@CCtCode", mCCtCode);
            Sm.CmParam<String>(ref cm, "@YrMth", string.Concat(TxtYr.Text, TxtMth.Text));

            SQL.AppendLine("Select A.DocNo, A.DocDt, B.ItCode, E.ItName, ");
            SQL.AppendLine("B.Qty*D.UPrice*D.ExcRate As Amt  ");
            SQL.AppendLine("From TblDODeptHdr A ");
            SQL.AppendLine("Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And B.CancelInd='N' ");
            SQL.AppendLine("Inner Join TblItemCostCategory C On A.CCCode=C.CCCode And B.ItCode=C.ItCode And C.CCtCode=@CCtCode ");
            SQL.AppendLine("Inner Join TblStockPrice D On B.Source=D.Source ");
            SQL.AppendLine("Inner Join TblItem E On B.ItCode=E.ItCode ");
            SQL.AppendLine("Where A.CCCode Is Not Null ");
            SQL.AppendLine("And Left(A.DocDt, 6)=@YrMth ");
            SQL.AppendLine("Order By A.DocNo, B.ItCode;");
            
            try
            {
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        SQL.ToString(),
                        new string[]
                        {
                            //0
                            "DocNo",

                            //1-4
                            "DocDt", "ItCode", "ItName", "Amt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                        }, false, false, false, false
                    );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 5 });
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #endregion
    }
}
