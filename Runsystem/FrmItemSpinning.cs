﻿#region
/*
    01/11/2017 [TKG] Aplikasi baru
    13/11/2017 [TKG] menggunakan button untuk memilih item
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmItemSpinning : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmItemSpinningFind FrmFind;

        #endregion

        #region Constructor

        public FrmItemSpinning(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetFormControl(mState.View);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { ChkActInd, TxtItName }, true);
                    BtnItCode.Enabled = false;
                    TxtItCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ TxtItName }, false);
                    BtnItCode.Enabled = true;
                    TxtItCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { TxtItName, ChkActInd }, false);
                    TxtItName.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit>{ TxtItCode, TxtItName });
            ChkActInd.Checked = false;
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmItemSpinningFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
            ChkActInd.Checked = true;
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtItCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtItCode, string.Empty, false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() 
                { CommandText = "Delete From TblItemSpinning Where ItCode=@ItCode;" };
                Sm.CmParam<String>(ref cm, "@ItCode", TxtItCode.Text);
                Sm.ExecCommand(cm);
                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into TblItemSpinning(ItCode, ActInd, ItName, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@ItCode, @ActInd, @ItName, @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("   Update ");
                SQL.AppendLine("        ActInd=@ActInd, ");
                SQL.AppendLine("        ItName=@ItName, ");
                SQL.AppendLine("        LastUpBy=@UserCode, ");
                SQL.AppendLine("        LastUpDt=CurrentDateTime(); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };

                Sm.CmParam<String>(ref cm, "@ItCode", TxtItCode.Text);
                Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked ? "Y" : "N");
                Sm.CmParam<String>(ref cm, "@ItName", TxtItName.Text);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                Sm.ExecCommand(cm);

                ShowData(TxtItCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string ItCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@ItCode", ItCode);
                Sm.ShowDataInCtrl(
                        ref cm,
                        "Select ItCode, ItName, ActInd From TblItemSpinning Where ItCode=@ItCode;",
                        new string[] 
                        { "ItCode", "ItName", "ActInd" },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtItCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtItName.EditValue = Sm.DrStr(dr, c[1]);
                            ChkActInd.Checked = Sm.DrStr(dr, c[2]) == "Y";
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtItCode, "Item's code", false) ||
                Sm.IsTxtEmpty(TxtItName, "Item's name", false) ||
                IsItemSpinningExisted();
        }

        private bool IsItemSpinningExisted()
        {
            if (!TxtItCode.Properties.ReadOnly)
            {
                return
                    Sm.IsDataExist(
                    "Select 1 From TblItemSpinning Where ItCode=@Param;",
                    TxtItCode.Text,
                    "Item's code ( " + TxtItCode.Text + " ) already existed."
                    );
            }
            return false;
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtItCode);
        }

        private void TxtItName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtItName);
        }      

        #endregion

        private void BtnItCode_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmItemSpinningDlg(this));
        }

       #endregion

    }
}
