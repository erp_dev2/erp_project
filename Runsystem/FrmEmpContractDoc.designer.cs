﻿namespace RunSystem
{
    partial class FrmEmpContractDoc
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmEmpContractDoc));
            this.BtnEmpCode2 = new DevExpress.XtraEditors.SimpleButton();
            this.TxtGrade = new DevExpress.XtraEditors.TextEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtPosition = new DevExpress.XtraEditors.TextEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtDeptName = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtEmpName = new DevExpress.XtraEditors.TextEdit();
            this.label15 = new System.Windows.Forms.Label();
            this.BtnEmpCode = new DevExpress.XtraEditors.SimpleButton();
            this.TxtEmpCode = new DevExpress.XtraEditors.TextEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.ChkActInd = new DevExpress.XtraEditors.CheckEdit();
            this.ChkRenewal = new DevExpress.XtraEditors.CheckEdit();
            this.DteStartDt = new DevExpress.XtraEditors.DateEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.DteEndDt = new DevExpress.XtraEditors.DateEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.TxtSalary = new DevExpress.XtraEditors.TextEdit();
            this.label20 = new System.Windows.Forms.Label();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.label11 = new System.Windows.Forms.Label();
            this.LblEmploymentStatus = new System.Windows.Forms.Label();
            this.LueEmploymentStatus = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtLocalDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.TxtSite = new DevExpress.XtraEditors.TextEdit();
            this.label17 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.TxtPosition2 = new DevExpress.XtraEditors.TextEdit();
            this.TxtEmpName2 = new DevExpress.XtraEditors.TextEdit();
            this.TxtEmpCode2 = new DevExpress.XtraEditors.TextEdit();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.LblEmpCodeManagement = new System.Windows.Forms.Label();
            this.BtnEmpCode3 = new DevExpress.XtraEditors.SimpleButton();
            this.label12 = new System.Windows.Forms.Label();
            this.TxtContractExt = new DevExpress.XtraEditors.TextEdit();
            this.label16 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.Grd1 = new TenTec.Windows.iGridLib.iGrid();
            this.BtnBOM = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGrade.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPosition.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDeptName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkRenewal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteEndDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteEndDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSalary.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueEmploymentStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLocalDocNo.Properties)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSite.Properties)).BeginInit();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPosition2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpName2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpCode2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtContractExt.Properties)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(884, 0);
            this.panel1.Size = new System.Drawing.Size(70, 473);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Size = new System.Drawing.Size(884, 473);
            // 
            // BtnEmpCode2
            // 
            this.BtnEmpCode2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnEmpCode2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEmpCode2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEmpCode2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEmpCode2.Appearance.Options.UseBackColor = true;
            this.BtnEmpCode2.Appearance.Options.UseFont = true;
            this.BtnEmpCode2.Appearance.Options.UseForeColor = true;
            this.BtnEmpCode2.Appearance.Options.UseTextOptions = true;
            this.BtnEmpCode2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnEmpCode2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnEmpCode2.Image = ((System.Drawing.Image)(resources.GetObject("BtnEmpCode2.Image")));
            this.BtnEmpCode2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnEmpCode2.Location = new System.Drawing.Point(445, 49);
            this.BtnEmpCode2.Name = "BtnEmpCode2";
            this.BtnEmpCode2.Size = new System.Drawing.Size(22, 21);
            this.BtnEmpCode2.TabIndex = 19;
            this.BtnEmpCode2.ToolTip = "Show Employee";
            this.BtnEmpCode2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnEmpCode2.ToolTipTitle = "Run System";
            this.BtnEmpCode2.Click += new System.EventHandler(this.BtnEmpCode2_Click_1);
            // 
            // TxtGrade
            // 
            this.TxtGrade.EnterMoveNextControl = true;
            this.TxtGrade.Location = new System.Drawing.Point(136, 160);
            this.TxtGrade.Margin = new System.Windows.Forms.Padding(5);
            this.TxtGrade.Name = "TxtGrade";
            this.TxtGrade.Properties.Appearance.BackColor = System.Drawing.Color.Silver;
            this.TxtGrade.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtGrade.Properties.Appearance.Options.UseBackColor = true;
            this.TxtGrade.Properties.Appearance.Options.UseFont = true;
            this.TxtGrade.Properties.MaxLength = 100;
            this.TxtGrade.Properties.ReadOnly = true;
            this.TxtGrade.Size = new System.Drawing.Size(331, 20);
            this.TxtGrade.TabIndex = 29;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(59, 163);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(71, 14);
            this.label6.TabIndex = 28;
            this.label6.Text = "Grade Level";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPosition
            // 
            this.TxtPosition.EnterMoveNextControl = true;
            this.TxtPosition.Location = new System.Drawing.Point(136, 116);
            this.TxtPosition.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPosition.Name = "TxtPosition";
            this.TxtPosition.Properties.Appearance.BackColor = System.Drawing.Color.Silver;
            this.TxtPosition.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPosition.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPosition.Properties.Appearance.Options.UseFont = true;
            this.TxtPosition.Properties.MaxLength = 100;
            this.TxtPosition.Properties.ReadOnly = true;
            this.TxtPosition.Size = new System.Drawing.Size(331, 20);
            this.TxtPosition.TabIndex = 25;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(81, 120);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 14);
            this.label5.TabIndex = 24;
            this.label5.Text = "Position";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDeptName
            // 
            this.TxtDeptName.EnterMoveNextControl = true;
            this.TxtDeptName.Location = new System.Drawing.Point(136, 94);
            this.TxtDeptName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDeptName.Name = "TxtDeptName";
            this.TxtDeptName.Properties.Appearance.BackColor = System.Drawing.Color.Silver;
            this.TxtDeptName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDeptName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDeptName.Properties.Appearance.Options.UseFont = true;
            this.TxtDeptName.Properties.MaxLength = 100;
            this.TxtDeptName.Properties.ReadOnly = true;
            this.TxtDeptName.Size = new System.Drawing.Size(331, 20);
            this.TxtDeptName.TabIndex = 23;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(57, 98);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 14);
            this.label3.TabIndex = 22;
            this.label3.Text = "Department";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtEmpName
            // 
            this.TxtEmpName.EnterMoveNextControl = true;
            this.TxtEmpName.Location = new System.Drawing.Point(136, 72);
            this.TxtEmpName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtEmpName.Name = "TxtEmpName";
            this.TxtEmpName.Properties.Appearance.BackColor = System.Drawing.Color.Silver;
            this.TxtEmpName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEmpName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEmpName.Properties.Appearance.Options.UseFont = true;
            this.TxtEmpName.Properties.MaxLength = 100;
            this.TxtEmpName.Properties.ReadOnly = true;
            this.TxtEmpName.Size = new System.Drawing.Size(331, 20);
            this.TxtEmpName.TabIndex = 21;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(27, 75);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(103, 14);
            this.label15.TabIndex = 20;
            this.label15.Text = "Employee\'s Name";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnEmpCode
            // 
            this.BtnEmpCode.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnEmpCode.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEmpCode.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEmpCode.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEmpCode.Appearance.Options.UseBackColor = true;
            this.BtnEmpCode.Appearance.Options.UseFont = true;
            this.BtnEmpCode.Appearance.Options.UseForeColor = true;
            this.BtnEmpCode.Appearance.Options.UseTextOptions = true;
            this.BtnEmpCode.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnEmpCode.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnEmpCode.Image = ((System.Drawing.Image)(resources.GetObject("BtnEmpCode.Image")));
            this.BtnEmpCode.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnEmpCode.Location = new System.Drawing.Point(417, 49);
            this.BtnEmpCode.Name = "BtnEmpCode";
            this.BtnEmpCode.Size = new System.Drawing.Size(22, 21);
            this.BtnEmpCode.TabIndex = 18;
            this.BtnEmpCode.ToolTip = "Find Employee";
            this.BtnEmpCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnEmpCode.ToolTipTitle = "Run System";
            this.BtnEmpCode.Click += new System.EventHandler(this.BtnEmpCode_Click);
            // 
            // TxtEmpCode
            // 
            this.TxtEmpCode.EnterMoveNextControl = true;
            this.TxtEmpCode.Location = new System.Drawing.Point(136, 50);
            this.TxtEmpCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtEmpCode.Name = "TxtEmpCode";
            this.TxtEmpCode.Properties.Appearance.BackColor = System.Drawing.Color.Silver;
            this.TxtEmpCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEmpCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEmpCode.Properties.Appearance.Options.UseFont = true;
            this.TxtEmpCode.Properties.MaxLength = 50;
            this.TxtEmpCode.Properties.ReadOnly = true;
            this.TxtEmpCode.Size = new System.Drawing.Size(278, 20);
            this.TxtEmpCode.TabIndex = 17;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(30, 53);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(100, 14);
            this.label4.TabIndex = 16;
            this.label4.Text = "Employee\'s Code";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(136, 28);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(120, 20);
            this.DteDocDt.TabIndex = 15;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(97, 31);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 14);
            this.label2.TabIndex = 14;
            this.label2.Text = "Date";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(136, 6);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.Silver;
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 30;
            this.TxtDocNo.Properties.ReadOnly = true;
            this.TxtDocNo.Size = new System.Drawing.Size(185, 20);
            this.TxtDocNo.TabIndex = 11;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(57, 9);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 14);
            this.label1.TabIndex = 10;
            this.label1.Text = "Document#";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkActInd
            // 
            this.ChkActInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkActInd.Location = new System.Drawing.Point(327, 6);
            this.ChkActInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkActInd.Name = "ChkActInd";
            this.ChkActInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkActInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkActInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkActInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkActInd.Properties.Appearance.Options.UseFont = true;
            this.ChkActInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkActInd.Properties.Caption = "Active";
            this.ChkActInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkActInd.Size = new System.Drawing.Size(61, 22);
            this.ChkActInd.TabIndex = 12;
            // 
            // ChkRenewal
            // 
            this.ChkRenewal.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkRenewal.Location = new System.Drawing.Point(394, 6);
            this.ChkRenewal.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkRenewal.Name = "ChkRenewal";
            this.ChkRenewal.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkRenewal.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkRenewal.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkRenewal.Properties.Appearance.Options.UseBackColor = true;
            this.ChkRenewal.Properties.Appearance.Options.UseFont = true;
            this.ChkRenewal.Properties.Appearance.Options.UseForeColor = true;
            this.ChkRenewal.Properties.Caption = "Renewal";
            this.ChkRenewal.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkRenewal.Size = new System.Drawing.Size(80, 22);
            this.ChkRenewal.TabIndex = 13;
            // 
            // DteStartDt
            // 
            this.DteStartDt.EditValue = null;
            this.DteStartDt.EnterMoveNextControl = true;
            this.DteStartDt.Location = new System.Drawing.Point(136, 270);
            this.DteStartDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteStartDt.Name = "DteStartDt";
            this.DteStartDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteStartDt.Properties.Appearance.Options.UseFont = true;
            this.DteStartDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteStartDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteStartDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteStartDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteStartDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteStartDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteStartDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteStartDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteStartDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteStartDt.Size = new System.Drawing.Size(120, 20);
            this.DteStartDt.TabIndex = 39;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Red;
            this.label7.Location = new System.Drawing.Point(66, 273);
            this.label7.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(64, 14);
            this.label7.TabIndex = 38;
            this.label7.Text = "Start Date";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteEndDt
            // 
            this.DteEndDt.EditValue = null;
            this.DteEndDt.EnterMoveNextControl = true;
            this.DteEndDt.Location = new System.Drawing.Point(136, 292);
            this.DteEndDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteEndDt.Name = "DteEndDt";
            this.DteEndDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteEndDt.Properties.Appearance.Options.UseFont = true;
            this.DteEndDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteEndDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteEndDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteEndDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteEndDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteEndDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteEndDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteEndDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteEndDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteEndDt.Size = new System.Drawing.Size(120, 20);
            this.DteEndDt.TabIndex = 41;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(72, 295);
            this.label8.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(58, 14);
            this.label8.TabIndex = 40;
            this.label8.Text = "End Date";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSalary
            // 
            this.TxtSalary.EnterMoveNextControl = true;
            this.TxtSalary.Location = new System.Drawing.Point(136, 204);
            this.TxtSalary.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSalary.Name = "TxtSalary";
            this.TxtSalary.Properties.Appearance.BackColor = System.Drawing.Color.Silver;
            this.TxtSalary.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSalary.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSalary.Properties.Appearance.Options.UseFont = true;
            this.TxtSalary.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtSalary.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtSalary.Properties.ReadOnly = true;
            this.TxtSalary.Size = new System.Drawing.Size(120, 20);
            this.TxtSalary.TabIndex = 33;
            this.TxtSalary.Visible = false;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(62, 207);
            this.label20.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(68, 14);
            this.label20.TabIndex = 32;
            this.label20.Text = "Basic Salary";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label20.Visible = false;
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(136, 314);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 400;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(400, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(331, 20);
            this.MeeRemark.TabIndex = 43;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(83, 317);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(47, 14);
            this.label11.TabIndex = 42;
            this.label11.Text = "Remark";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblEmploymentStatus
            // 
            this.LblEmploymentStatus.AutoSize = true;
            this.LblEmploymentStatus.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblEmploymentStatus.ForeColor = System.Drawing.Color.Black;
            this.LblEmploymentStatus.Location = new System.Drawing.Point(16, 185);
            this.LblEmploymentStatus.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblEmploymentStatus.Name = "LblEmploymentStatus";
            this.LblEmploymentStatus.Size = new System.Drawing.Size(114, 14);
            this.LblEmploymentStatus.TabIndex = 30;
            this.LblEmploymentStatus.Text = "Employment Status";
            this.LblEmploymentStatus.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueEmploymentStatus
            // 
            this.LueEmploymentStatus.EnterMoveNextControl = true;
            this.LueEmploymentStatus.Location = new System.Drawing.Point(136, 182);
            this.LueEmploymentStatus.Margin = new System.Windows.Forms.Padding(5);
            this.LueEmploymentStatus.Name = "LueEmploymentStatus";
            this.LueEmploymentStatus.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEmploymentStatus.Properties.Appearance.Options.UseFont = true;
            this.LueEmploymentStatus.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEmploymentStatus.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueEmploymentStatus.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEmploymentStatus.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueEmploymentStatus.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEmploymentStatus.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueEmploymentStatus.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEmploymentStatus.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueEmploymentStatus.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueEmploymentStatus.Properties.DropDownRows = 30;
            this.LueEmploymentStatus.Properties.MaxLength = 1;
            this.LueEmploymentStatus.Properties.NullText = "[Empty]";
            this.LueEmploymentStatus.Properties.PopupWidth = 300;
            this.LueEmploymentStatus.Size = new System.Drawing.Size(200, 20);
            this.LueEmploymentStatus.TabIndex = 31;
            this.LueEmploymentStatus.ToolTip = "F4 : Show/hide list";
            this.LueEmploymentStatus.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueEmploymentStatus.EditValueChanged += new System.EventHandler(this.LueEmploymentStatus_EditValueChanged);
            // 
            // TxtLocalDocNo
            // 
            this.TxtLocalDocNo.EnterMoveNextControl = true;
            this.TxtLocalDocNo.Location = new System.Drawing.Point(136, 226);
            this.TxtLocalDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLocalDocNo.Name = "TxtLocalDocNo";
            this.TxtLocalDocNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLocalDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLocalDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLocalDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtLocalDocNo.Properties.MaxLength = 30;
            this.TxtLocalDocNo.Properties.ReadOnly = true;
            this.TxtLocalDocNo.Size = new System.Drawing.Size(185, 20);
            this.TxtLocalDocNo.TabIndex = 35;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(35, 229);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(95, 14);
            this.label9.TabIndex = 34;
            this.label9.Text = "Local Document";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.TxtSite);
            this.panel3.Controls.Add(this.label17);
            this.panel3.Controls.Add(this.panel5);
            this.panel3.Controls.Add(this.TxtContractExt);
            this.panel3.Controls.Add(this.label16);
            this.panel3.Controls.Add(this.TxtDocNo);
            this.panel3.Controls.Add(this.TxtLocalDocNo);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.label9);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.LblEmploymentStatus);
            this.panel3.Controls.Add(this.DteDocDt);
            this.panel3.Controls.Add(this.LueEmploymentStatus);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Controls.Add(this.MeeRemark);
            this.panel3.Controls.Add(this.TxtEmpCode);
            this.panel3.Controls.Add(this.label11);
            this.panel3.Controls.Add(this.BtnEmpCode);
            this.panel3.Controls.Add(this.TxtSalary);
            this.panel3.Controls.Add(this.label15);
            this.panel3.Controls.Add(this.label20);
            this.panel3.Controls.Add(this.TxtEmpName);
            this.panel3.Controls.Add(this.DteEndDt);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.label8);
            this.panel3.Controls.Add(this.TxtDeptName);
            this.panel3.Controls.Add(this.DteStartDt);
            this.panel3.Controls.Add(this.label5);
            this.panel3.Controls.Add(this.label7);
            this.panel3.Controls.Add(this.TxtPosition);
            this.panel3.Controls.Add(this.ChkRenewal);
            this.panel3.Controls.Add(this.label6);
            this.panel3.Controls.Add(this.ChkActInd);
            this.panel3.Controls.Add(this.TxtGrade);
            this.panel3.Controls.Add(this.BtnEmpCode2);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(884, 340);
            this.panel3.TabIndex = 9;
            // 
            // TxtSite
            // 
            this.TxtSite.EnterMoveNextControl = true;
            this.TxtSite.Location = new System.Drawing.Point(136, 138);
            this.TxtSite.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSite.Name = "TxtSite";
            this.TxtSite.Properties.Appearance.BackColor = System.Drawing.Color.Silver;
            this.TxtSite.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSite.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSite.Properties.Appearance.Options.UseFont = true;
            this.TxtSite.Properties.MaxLength = 100;
            this.TxtSite.Properties.ReadOnly = true;
            this.TxtSite.Size = new System.Drawing.Size(331, 20);
            this.TxtSite.TabIndex = 27;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(102, 141);
            this.label17.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(28, 14);
            this.label17.TabIndex = 26;
            this.label17.Text = "Site";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel5.Controls.Add(this.TxtPosition2);
            this.panel5.Controls.Add(this.TxtEmpName2);
            this.panel5.Controls.Add(this.TxtEmpCode2);
            this.panel5.Controls.Add(this.label14);
            this.panel5.Controls.Add(this.label13);
            this.panel5.Controls.Add(this.LblEmpCodeManagement);
            this.panel5.Controls.Add(this.BtnEmpCode3);
            this.panel5.Controls.Add(this.label12);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel5.Location = new System.Drawing.Point(483, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(401, 340);
            this.panel5.TabIndex = 44;
            // 
            // TxtPosition2
            // 
            this.TxtPosition2.EnterMoveNextControl = true;
            this.TxtPosition2.Location = new System.Drawing.Point(115, 96);
            this.TxtPosition2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPosition2.Name = "TxtPosition2";
            this.TxtPosition2.Properties.Appearance.BackColor = System.Drawing.Color.Silver;
            this.TxtPosition2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPosition2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPosition2.Properties.Appearance.Options.UseFont = true;
            this.TxtPosition2.Properties.MaxLength = 100;
            this.TxtPosition2.Properties.ReadOnly = true;
            this.TxtPosition2.Size = new System.Drawing.Size(278, 20);
            this.TxtPosition2.TabIndex = 50;
            // 
            // TxtEmpName2
            // 
            this.TxtEmpName2.EnterMoveNextControl = true;
            this.TxtEmpName2.Location = new System.Drawing.Point(115, 74);
            this.TxtEmpName2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtEmpName2.Name = "TxtEmpName2";
            this.TxtEmpName2.Properties.Appearance.BackColor = System.Drawing.Color.Silver;
            this.TxtEmpName2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEmpName2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEmpName2.Properties.Appearance.Options.UseFont = true;
            this.TxtEmpName2.Properties.MaxLength = 100;
            this.TxtEmpName2.Properties.ReadOnly = true;
            this.TxtEmpName2.Size = new System.Drawing.Size(278, 20);
            this.TxtEmpName2.TabIndex = 48;
            // 
            // TxtEmpCode2
            // 
            this.TxtEmpCode2.EnterMoveNextControl = true;
            this.TxtEmpCode2.Location = new System.Drawing.Point(115, 52);
            this.TxtEmpCode2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtEmpCode2.Name = "TxtEmpCode2";
            this.TxtEmpCode2.Properties.Appearance.BackColor = System.Drawing.Color.Silver;
            this.TxtEmpCode2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEmpCode2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEmpCode2.Properties.Appearance.Options.UseFont = true;
            this.TxtEmpCode2.Properties.MaxLength = 50;
            this.TxtEmpCode2.Properties.ReadOnly = true;
            this.TxtEmpCode2.Size = new System.Drawing.Size(254, 20);
            this.TxtEmpCode2.TabIndex = 45;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(7, 14);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(182, 17);
            this.label14.TabIndex = 64;
            this.label14.Text = "Management Representative";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(62, 98);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(49, 14);
            this.label13.TabIndex = 49;
            this.label13.Text = "Position";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblEmpCodeManagement
            // 
            this.LblEmpCodeManagement.AutoSize = true;
            this.LblEmpCodeManagement.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblEmpCodeManagement.ForeColor = System.Drawing.Color.Black;
            this.LblEmpCodeManagement.Location = new System.Drawing.Point(11, 54);
            this.LblEmpCodeManagement.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblEmpCodeManagement.Name = "LblEmpCodeManagement";
            this.LblEmpCodeManagement.Size = new System.Drawing.Size(100, 14);
            this.LblEmpCodeManagement.TabIndex = 44;
            this.LblEmpCodeManagement.Text = "Employee\'s Code";
            this.LblEmpCodeManagement.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnEmpCode3
            // 
            this.BtnEmpCode3.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnEmpCode3.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEmpCode3.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEmpCode3.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEmpCode3.Appearance.Options.UseBackColor = true;
            this.BtnEmpCode3.Appearance.Options.UseFont = true;
            this.BtnEmpCode3.Appearance.Options.UseForeColor = true;
            this.BtnEmpCode3.Appearance.Options.UseTextOptions = true;
            this.BtnEmpCode3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnEmpCode3.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnEmpCode3.Image = ((System.Drawing.Image)(resources.GetObject("BtnEmpCode3.Image")));
            this.BtnEmpCode3.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnEmpCode3.Location = new System.Drawing.Point(371, 50);
            this.BtnEmpCode3.Name = "BtnEmpCode3";
            this.BtnEmpCode3.Size = new System.Drawing.Size(22, 21);
            this.BtnEmpCode3.TabIndex = 46;
            this.BtnEmpCode3.ToolTip = "Find Employee";
            this.BtnEmpCode3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnEmpCode3.ToolTipTitle = "Run System";
            this.BtnEmpCode3.Click += new System.EventHandler(this.BtnEmpCode3_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(8, 77);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(103, 14);
            this.label12.TabIndex = 47;
            this.label12.Text = "Employee\'s Name";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtContractExt
            // 
            this.TxtContractExt.EnterMoveNextControl = true;
            this.TxtContractExt.Location = new System.Drawing.Point(136, 248);
            this.TxtContractExt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtContractExt.Name = "TxtContractExt";
            this.TxtContractExt.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtContractExt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtContractExt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtContractExt.Properties.Appearance.Options.UseFont = true;
            this.TxtContractExt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtContractExt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtContractExt.Size = new System.Drawing.Size(64, 20);
            this.TxtContractExt.TabIndex = 37;
            this.TxtContractExt.Validated += new System.EventHandler(this.TxtContractExt_Validated);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(10, 251);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(120, 14);
            this.label16.TabIndex = 36;
            this.label16.Text = "Contract Extension#";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.Grd1);
            this.panel4.Controls.Add(this.BtnBOM);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(0, 340);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(884, 133);
            this.panel4.TabIndex = 10;
            // 
            // Grd1
            // 
            this.Grd1.BackColorEvenRows = System.Drawing.Color.WhiteSmoke;
            this.Grd1.BackColorOddRows = System.Drawing.Color.White;
            this.Grd1.CurCellBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.Grd1.DefaultAutoGroupRow.Height = 21;
            this.Grd1.DefaultRow.Height = 20;
            this.Grd1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd1.FrozenArea.ColCount = 3;
            this.Grd1.FrozenArea.SortFrozenRows = true;
            this.Grd1.GridLines.Mode = TenTec.Windows.iGridLib.iGGridLinesMode.None;
            this.Grd1.GroupBox.BackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintBackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Grd1.GroupBox.Visible = true;
            this.Grd1.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.UseXPStyles = false;
            this.Grd1.Location = new System.Drawing.Point(0, 23);
            this.Grd1.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.Grd1.Name = "Grd1";
            this.Grd1.ProcessTab = false;
            this.Grd1.ReadOnly = true;
            this.Grd1.RowMode = true;
            this.Grd1.RowModeHasCurCell = true;
            this.Grd1.RowTextStartColNear = 3;
            this.Grd1.ScrollBarSettings.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.SearchAsType.Mode = TenTec.Windows.iGridLib.iGSearchAsTypeMode.Seek;
            this.Grd1.SearchAsType.SearchCol = null;
            this.Grd1.SingleClickEdit = true;
            this.Grd1.Size = new System.Drawing.Size(884, 110);
            this.Grd1.TabIndex = 37;
            this.Grd1.TreeCol = null;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.VScrollBar.Visibility = TenTec.Windows.iGridLib.iGScrollBarVisibility.Always;
            // 
            // BtnBOM
            // 
            this.BtnBOM.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnBOM.Dock = System.Windows.Forms.DockStyle.Top;
            this.BtnBOM.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnBOM.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnBOM.ForeColor = System.Drawing.Color.AliceBlue;
            this.BtnBOM.Location = new System.Drawing.Point(0, 0);
            this.BtnBOM.Name = "BtnBOM";
            this.BtnBOM.Size = new System.Drawing.Size(884, 23);
            this.BtnBOM.TabIndex = 43;
            this.BtnBOM.Text = "Contract Extensions History";
            this.BtnBOM.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.BtnBOM.UseVisualStyleBackColor = false;
            // 
            // FrmEmpContractDoc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(954, 473);
            this.Name = "FrmEmpContractDoc";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TxtGrade.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPosition.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDeptName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkRenewal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteEndDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteEndDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSalary.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueEmploymentStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLocalDocNo.Properties)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSite.Properties)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPosition2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpName2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpCode2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtContractExt.Properties)).EndInit();
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public DevExpress.XtraEditors.SimpleButton BtnEmpCode2;
        public DevExpress.XtraEditors.TextEdit TxtGrade;
        private System.Windows.Forms.Label label6;
        public DevExpress.XtraEditors.TextEdit TxtPosition;
        private System.Windows.Forms.Label label5;
        public DevExpress.XtraEditors.TextEdit TxtDeptName;
        private System.Windows.Forms.Label label3;
        public DevExpress.XtraEditors.TextEdit TxtEmpName;
        private System.Windows.Forms.Label label15;
        public DevExpress.XtraEditors.SimpleButton BtnEmpCode;
        public DevExpress.XtraEditors.TextEdit TxtEmpCode;
        private System.Windows.Forms.Label label4;
        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Label label1;
        internal DevExpress.XtraEditors.DateEdit DteEndDt;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private DevExpress.XtraEditors.CheckEdit ChkRenewal;
        private DevExpress.XtraEditors.CheckEdit ChkActInd;
        public DevExpress.XtraEditors.TextEdit TxtSalary;
        private System.Windows.Forms.Label label20;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private System.Windows.Forms.Label label11;
        public DevExpress.XtraEditors.DateEdit DteStartDt;
        private System.Windows.Forms.Label LblEmploymentStatus;
        internal DevExpress.XtraEditors.LookUpEdit LueEmploymentStatus;
        internal DevExpress.XtraEditors.TextEdit TxtLocalDocNo;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        internal DevExpress.XtraEditors.TextEdit TxtContractExt;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Button BtnBOM;
        protected internal TenTec.Windows.iGridLib.iGrid Grd1;
        protected System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label LblEmpCodeManagement;
        public DevExpress.XtraEditors.SimpleButton BtnEmpCode3;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label17;
        public DevExpress.XtraEditors.TextEdit TxtSite;
        public DevExpress.XtraEditors.TextEdit TxtEmpCode2;
        public DevExpress.XtraEditors.TextEdit TxtPosition2;
        public DevExpress.XtraEditors.TextEdit TxtEmpName2;
    }
}