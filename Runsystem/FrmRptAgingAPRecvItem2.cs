﻿#region Update
/*
    28/07/2022 [MYA/PRODUCT] Penyesuaian reporting aging uninvoice AP (poin 1 di requirement - 1.1, 1.2, 1.3)
    12/08/2022 [RDA/PRODUCT] muncul warning ketika no data
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

#endregion

namespace RunSystem
{
    public partial class FrmRptAgingAPRecvItem2 : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;
        private bool
            mIsSiteMandatory = false,
            mIsBOMShowSpecifications = false,
            mIsRptAgingAPRecvItemShowInvoicedData = false,
            mIsRecvExpeditionEnabled = false,
            mIsRptAgingAPRecvItemShowTotalIDR = false,
            mIsFilterByItCt = false,
            mIsFilterBySite = false,
            mIsFilterByCC = false,
            mIsFilterByDept = false,
            mIsFilterByWarehouse = false;
        #endregion

        #region Constructor

        public FrmRptAgingAPRecvItem2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                //SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -90);
                Sm.SetPeriod(ref DteDueDt1, ref DteDueDt2, ref ChkDueDt, -360);
                Sl.SetLueItCtCodeFilterByItCt(ref LueItCtCode, mIsFilterByItCt ? "Y" : "N");
                Sl.SetLueVdCode(ref LueVdCode);
                Sl.SetLueVdCtCode(ref LueVdCtCode);
                Sl.SetLueSiteCode(ref LueSiteCode, string.Empty, mIsFilterBySite ? "Y" : "N");
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mIsSiteMandatory = Sm.GetParameterBoo("mIsSiteMandatory");
            mIsBOMShowSpecifications = Sm.GetParameterBoo("IsBOMShowSpecifications");
            mIsRptAgingAPRecvItemShowInvoicedData = Sm.GetParameterBoo("IsRptAgingAPRecvItemShowInvoicedData");
            mIsRecvExpeditionEnabled = Sm.GetParameterBoo("IsRecvExpeditionEnabled");
            mIsRptAgingAPRecvItemShowTotalIDR = Sm.GetParameterBoo("IsRptAgingAPRecvItemShowTotalIDR");
            mIsFilterByItCt = Sm.GetParameterBoo("IsFilterByItCt");
            mIsFilterBySite = Sm.GetParameterBoo("IsFilterBySite");
            mIsFilterByCC = Sm.GetParameterBoo("IsFilterByCC");
            mIsFilterByDept = Sm.GetParameterBoo("IsFilterByDept");
            mIsFilterByWarehouse = Sm.GetParameterBoo("IsFilterByWarehouse");
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();
            var SQL2 = new StringBuilder();
            var SQL3 = new StringBuilder();

            SQL2.AppendLine(" (T1.Qty*(");
            SQL2.AppendLine("((T1.QtyPurchase/T1.Qty)*T5.UPrice)-");
            SQL2.AppendLine("(((T1.QtyPurchase/T1.Qty)*T5.UPrice)*T3.Discount*0.01)-");
            SQL2.AppendLine("((1.00/T1.Qty)*(T1.QtyPurchase/T1.Qty)*T3.DiscountAmt)+");
            SQL2.AppendLine("((1.00/T1.Qty)*(T1.QtyPurchase/T1.Qty)*T3.RoundingValue) ");
            SQL2.AppendLine(")) ");

            SQL3.AppendLine(" (T1.Qty*(");
            SQL3.AppendLine("((T1.QtyPurchase/T1.Qty)*T5.UPrice)-");
            SQL3.AppendLine("(((T1.QtyPurchase/T1.Qty)*T5.UPrice)*T3.Discount*0.01)-");
            SQL3.AppendLine("((1.00/T1.Qty)*(T1.QtyPurchase/T1.Qty)*T3.DiscountAmt)+");
            SQL3.AppendLine("((1.00/T1.Qty)*(T1.QtyPurchase/T1.Qty)*T3.RoundingValue) ");
            SQL3.AppendLine("))*T16.ExcRate ");

            var subSQL = SQL2.ToString();
            var subSQL2 = SQL3.ToString();

            SQL.AppendLine("Select A.DocDt, A.Periode, A.RecvNo, A.PONo, ");
            SQL.AppendLine("C.VdName, A.ItCtName, A.DueDt, A.CurCode, A.Amount, A.TaxAmt, A.UnInvoiceAmount, A.BalanceAmount, ");
            if (mIsRptAgingAPRecvItemShowTotalIDR)
                SQL.AppendLine("A.TotalIDR, ");
            else
                SQL.AppendLine("0.00 TotalIDR, ");
            SQL.AppendLine("Case When IfNull(A.AgingDays, '')='' Then 0.00 Else A.AgingDays End As AgingDays, ");
            SQL.AppendLine("A.AgingCurrent, A.Aging1To30, A.Aging31To60, A.Aging61To90, A.Aging91To120, A.AgingOver120, ");
            SQL.AppendLine("G.DeptName, F.EntName, D.SiteName, G.PtName, A.ItCodeInternal, A.ItName, A.Specification, A.PIDocNo, A.PIDocDt ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select T.DocDt, T.Periode, T.RecvNo, T.PONo, T.VdCode, T.ItCtCode, ");
            SQL.AppendLine("    ItCat.ItCtName, T.DueDt, T.CurCode, T.ItCodeInternal, T.ItName, T.Specification, ");
            SQL.AppendLine("    Sum(T.Amount) As Amount, ");
            if (mIsRptAgingAPRecvItemShowTotalIDR)
                SQL.AppendLine("    Sum(T.TotalIDR) As TotalIDR, ");
            SQL.AppendLine("    Sum(T.TaxAmt) As TaxAmt, Sum(T.UnInvoiceAmount + T.UnInvoiceTaxAmt) As UnInvoiceAmount, ");
            SQL.AppendLine("    Sum((T.Amount + T.TaxAmt)-(T.UnInvoiceAmount + T.UnInvoiceTaxAmt)) As BalanceAmount, ");
            SQL.AppendLine("    AgingDays, ");
            SQL.AppendLine("    Sum(Case when (AgingDays < 0.00) then ");
            SQL.AppendLine("        (Amount+TaxAmt) - (UnInvoiceAmount + UnInvoiceTaxAmt) else 0.00 ");
            SQL.AppendLine("    end) As AgingCurrent, ");
            SQL.AppendLine("    Sum(Case when (AgingDays > 0.00 And AgingDays < 31.00) then ");
            SQL.AppendLine("        (Amount+TaxAmt) - (UnInvoiceAmount + UnInvoiceTaxAmt) else 0.00 ");
            SQL.AppendLine("    end) As Aging1To30,	");
            SQL.AppendLine("    Sum(Case when (AgingDays > 30.00 And AgingDays < 61.00) then ");
            SQL.AppendLine("    (Amount+TaxAmt) - (UnInvoiceAmount + UnInvoiceTaxAmt) else 0.00 ");
            SQL.AppendLine("    end) As Aging31To60, ");
            SQL.AppendLine("    Sum(Case when (AgingDays > 60.00 And AgingDays < 91.00) then ");
            SQL.AppendLine("    (Amount+TaxAmt) - (UnInvoiceAmount + UnInvoiceTaxAmt) else 00.00 ");
            SQL.AppendLine("    end) As Aging61To90,	");
            SQL.AppendLine("    Sum(Case when (AgingDays > 90.00 And AgingDays < 121.00) then ");
            SQL.AppendLine("    (Amount+TaxAmt) - (UnInvoiceAmount + UnInvoiceTaxAmt) else 0.00 ");
            SQL.AppendLine("    end) As Aging91To120,	");
            SQL.AppendLine("    Sum(Case when (AgingDays > 120.00) then ");
            SQL.AppendLine("    (Amount+TaxAmt) - (UnInvoiceAmount + UnInvoiceTaxAmt) else 0.00 ");
            SQL.AppendLine("    end) As AgingOver120, T.PIDocNo, T.PIDocDt ");
            SQL.AppendLine("    From (");
            SQL.AppendLine("        Select T2.DocDt, Concat(Left(T2.DocDt, 4), '-',Substring(T2.DocDt, 5, 2)) As Periode, ");
            SQL.AppendLine("        T8.VdCode, T2.DocNo As RecvNo, ");
            SQL.AppendLine("        T8.DocNo As PONo, T13.ItCtCode, T6.CurCode, T13.ItCodeInternal, T13.ItName, T13.Specification, ");

            SQL.AppendLine(subSQL);
            SQL.AppendLine("As Amount, ");

            SQL.AppendLine(" (");
            SQL.AppendLine(subSQL);
            SQL.AppendLine("* (IfNull(T9.TaxRate, 0.00)*0.01))+");

            SQL.AppendLine(" (");
            SQL.AppendLine(subSQL);
            SQL.AppendLine("* (IfNull(T10.TaxRate, 0.00)*0.01))+");

            SQL.AppendLine(" (");
            SQL.AppendLine(subSQL);
            SQL.AppendLine("* (IfNull(T11.TaxRate, 0.00)*0.01))");

            SQL.AppendLine("As TaxAmt, ");

            SQL.AppendLine("Case When T12.RecvVdDocNo Is Null Then 0.00 Else  ");
            SQL.AppendLine(subSQL);
            SQL.AppendLine("End As UnInvoiceAmount, ");

            SQL.AppendLine("Case When T12.RecvVdDocNo Is Null Then 0.00 Else  ");

            SQL.AppendLine(" (");
            SQL.AppendLine(subSQL);
            SQL.AppendLine("* (IfNull(T9.TaxRate, 0.00)*0.01))+ ");

            SQL.AppendLine(" (");
            SQL.AppendLine(subSQL);
            SQL.AppendLine("* (IfNull(T10.TaxRate, 0.00)*0.01))+ ");

            SQL.AppendLine(" (");
            SQL.AppendLine(subSQL);
            SQL.AppendLine("* (IfNull(T11.TaxRate, 0.00)*0.01)) ");

            SQL.AppendLine(" End As UnInvoiceTaxAmt, ");

            if(mIsRptAgingAPRecvItemShowTotalIDR)
            {
                SQL.AppendLine(" (");
                SQL.AppendLine(subSQL2);
                SQL.AppendLine(") As TotalIDR, ");
            }
            SQL.AppendLine("        T7.PtDay, ");
            SQL.AppendLine("        DATE_FORMAT(Date_Add(T2.DocDt, INTERVAL T7.PtDay DAY), '%Y%m%d') As DueDt, ");
            SQL.AppendLine("        DateDiff(");
            SQL.AppendLine("            T16.CurrentDate, Date_Add(STR_TO_DATE(T2.DocDt, '%Y%m%d'), Interval T7.PtDay DAY) ");
            SQL.AppendLine("        ) As AgingDays, T17.DocNo AS PIDocNo, T17.DocDt AS PIDocDt  ");
            SQL.AppendLine("        From TblRecvVdDtl T1 ");
            SQL.AppendLine("        Inner Join TblRecvVdHdr T2 ");
            SQL.AppendLine("            On T1.DocNo=T2.DocNo ");
            if (DteBalanceDt.Text.Length > 0)
            {
                SQL.AppendLine("            And T2.DocDt <= @BalanceDt ");
            }
            else
            {
                SQL.AppendLine("            And T2.DocDt Between @DocDt1 And @DocDt2 ");
            }
            SQL.AppendLine("        Inner Join TblPODtl T3 on T1.PODocNo=T3.DocNo And T1.PODNo=T3.DNo ");
            SQL.AppendLine("        Inner Join TblPOHdr T8 on T8.DocNo=T3.DocNo  ");
            SQL.AppendLine("        Inner Join TblPORequestDtl T4 On T4.DocNo=T3.PORequestDocNo And T4.DNo=T3.PORequestDNo ");
            SQL.AppendLine("        Inner Join TblQtDtl T5 on T5.DocNo=T4.QtDocNo And T5.DNo=T4.QtDNo ");
            SQL.AppendLine("        Inner Join TblQtHdr T6 on T6.DocNo=T5.DocNo ");
            SQL.AppendLine("        Inner Join TblItem T13 on T5.ItCode=T13.ItCode ");
            SQL.AppendLine("        Left Join TblPaymentTerm T7 on T7.PtCode=T6.PtCode ");
            SQL.AppendLine("        Left Join TblTax T9 on T9.TaxCode=T8.TaxCode1 ");
            SQL.AppendLine("        Left Join TblTax T10 on T10.TaxCode=T8.TaxCode2 ");
            SQL.AppendLine("        Left Join TblTax T11 on T11.TaxCode=T8.TaxCode3 ");
            SQL.AppendLine("        Left Join TblPurchaseInvoiceDtl T12 ");
            SQL.AppendLine("            On T12.RecvVdDocNo=T1.DocNo ");
            SQL.AppendLine("            And T12.RecvVdDNo=T1.DNo ");
            SQL.AppendLine("            And Exists( ");
            SQL.AppendLine("                Select 1 ");
            SQL.AppendLine("                From TblPurchaseInvoiceHdr ");
            SQL.AppendLine("                Where CancelInd='N' ");
            SQL.AppendLine("                And DocNo=T12.DocNo ");
            if (DteBalanceDt.Text.Length > 0)
                SQL.AppendLine("                AND DocDt <= @BalanceDt");
            SQL.AppendLine("            ) ");
            SQL.AppendLine("        Inner Join TblMaterialRequestHdr T14 On T4.MaterialrequestDocno = T14.DocNo ");
            
            if(mIsRptAgingAPRecvItemShowTotalIDR)
            {
                //SQL.AppendLine("    LEFT JOIN TblStockMovement T15 ON T1.DocNo = T15.DocNo AND T1.DNo = T15.DNo And T15.CancelInd = 'N' ");
                SQL.AppendLine("    LEFT JOIN TblStockPrice T16 On T1.Source = T16.Source ");
            }

            SQL.AppendLine("        Left Join (Select STR_TO_DATE(Left(CurrentDateTime(), 8), '%Y%m%d') As CurrentDate) T16 On 0=0 ");
            SQL.AppendLine("        LEFT JOIN TblPurchaseInvoiceHdr T17 ON T12.DocNo = T17.DocNo ");
            SQL.AppendLine("        AND T17.CancelInd='N' AND T17.DocNo=T12.DocNo ");
            if (DteBalanceDt.Text.Length > 0)
                SQL.AppendLine("        AND T17.DocDt <= @BalanceDt ");
            SQL.AppendLine("        Where T1.CancelInd='N' ");
            if (DteBalanceDt.Text.Length > 0)
            {
                SQL.AppendLine("        AND T1.DocNo NOT IN (");
                SQL.AppendLine("            SELECT T12.RecvVdDocNo ");
                SQL.AppendLine("            FROM tblpurchaseinvoicedtl T12 ");
                SQL.AppendLine("            INNER JOIN tblpurchaseinvoicehdr T17 ");
                SQL.AppendLine("                ON T12.DocNo = T17.DocNo ");
                SQL.AppendLine("                AND T17.DocDt <= @BalanceDt");
                SQL.AppendLine("            )");
            }
            if (mIsRecvExpeditionEnabled)
                SQL.AppendLine("AND T1.RejectedInd = 'N' ");
            SQL.AppendLine("        And IfNull(T1.Status, 'O') In ('O', 'A') ");
            if (!mIsRptAgingAPRecvItemShowInvoicedData)
            {
                SQL.AppendLine("        And Not Exists(");
                SQL.AppendLine("            Select 1 ");
                SQL.AppendLine("            From TblPurchaseInvoiceHdr X1, TblPurchaseInvoiceDtl X2 ");
                SQL.AppendLine("            Where X1.CancelInd='N' ");
                SQL.AppendLine("            And X1.DocNo=X2.DocNo ");
                SQL.AppendLine("            And X2.RecvVdDocNo=T1.DocNo ");
                SQL.AppendLine("            And X2.RecvVdDNo=T1.DNo ");
                SQL.AppendLine("        ) ");
            }
            SQL.AppendLine("    ) T ");
            SQL.AppendLine("    Left Join TblItemCategory ItCat on ItCat.ItCtCode=T.ItCtCode ");
            SQL.AppendLine("    Group By "); //T.Periode, T.VdCode, T.RecvNo, T.PONo, T.ItCtCode, ItCat.ItCtName, T.DueDt, T.CurCode, T.AgingDays ");
            SQL.AppendLine("    T.DocDt, T.Periode, T.RecvNo, T.PONo, T.VdCode, T.ItCtCode, ");
            SQL.AppendLine("    ItCat.ItCtName, T.DueDt, T.CurCode ");
            SQL.AppendLine(") A ");
            SQL.AppendLine("Inner Join TblRecvVdHdr B On A.RecvNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblVendor C on B.VdCode=C.VdCode ");
            SQL.AppendLine("Left Join TblSite D on B.SiteCode=D.SiteCode ");
            SQL.AppendLine("Left Join TblProfitCenter E on D.ProfitCenterCode=E.ProfitCenterCode ");
            SQL.AppendLine("Left Join TblEntity F on E.EntCode=F.EntCode ");
            SQL.AppendLine("Left Join ");
            SQL.AppendLine("( ");
	        SQL.AppendLine("    Select T1.DocNo As RecvVdDocNo, Group_Concat(Distinct T6.DeptName Separator ', ') As DeptName, T6.DeptCode, ");
	        SQL.AppendLine("    Group_Concat(Distinct T8.PtName Separator ', ') As PtName ");
	        SQL.AppendLine("    From TblRecvVdHdr T1 ");
	        SQL.AppendLine("    Inner Join TblRecvVdDtl T2 On T1.DocNo = T2.DocNo ");
	        SQL.AppendLine("    Inner Join TblPODtl T3 On T2.PODocNo = T3.DocNo And T2.PODNo = T3.DNo ");
	        SQL.AppendLine("    Inner Join TblPORequestDtl T4 On T3.PORequestDocNo = T4.DocNo And T3.PORequestDNo = T4.DNo ");
	        SQL.AppendLine("    Inner Join TblMaterialRequestHdr T5 On T4.MaterialRequestDocNo = T5.DocNo ");
	        SQL.AppendLine("    Left Join TblDepartment T6 On T5.DeptCode = T6.DeptCode ");
	        SQL.AppendLine("    Left Join TblQTHdr T7 On T4.QTDocNo = T7.DocNo ");
	        SQL.AppendLine("    Left Join TblPaymentTerm T8 On T7.PtCode = T8.PtCode ");
            if (DteBalanceDt.Text.Length > 0)
            {
                SQL.AppendLine("    Where T1.DocDt <= @BalanceDt");
            }
            else
            {
                SQL.AppendLine("    Where T1.DocDt Between @DocDt1 And @DocDt2 ");
            }
	        SQL.AppendLine("    Group By T1.DocNo ");
            SQL.AppendLine(")G On A.RecvNo = G.RecvVdDocNo ");
            SQL.AppendLine("Where 0 = 0 ");
            if (mIsFilterBySite)
            {
                SQL.AppendLine("And (D.SiteCode Is Null Or (D.SiteCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupSite ");
                SQL.AppendLine("    Where SiteCode=IfNull(D.SiteCode, '') ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ))) ");
            }
            if (mIsFilterByItCt)
            {
                SQL.AppendLine("And (A.ItCtCode Is Null Or (A.ItCtCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=IfNull(A.ItCtCode, '') ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ))) ");
            }
            if (mIsFilterByDept)
            {
                SQL.AppendLine("And (G.DeptCode Is Null Or (G.DeptCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupDepartment ");
                SQL.AppendLine("    Where DeptCode=IfNull(G.DeptCode, '') ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ))) ");
            }

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 29;
            Grd1.FrozenArea.ColCount = 4;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[]
                    {
                        //0
                        "Period",

                        //1-5
                        "Vendor",
                        "Received#",
                        "Received"+Environment.NewLine+"Date",
                        "PO#",
                        "Due"+Environment.NewLine+"Date",

                        //6-10
                        "Item's Category",
                        "Local Code",
                        "Item's Name",
                        "Specification",
                        "Term of"+Environment.NewLine+"Payment",

                        //11-15
                        "Currency",
                        "Amount",
                        "Tax",
                        "Invoice"+Environment.NewLine+"Amount",
                        "Balance",

                        //16-20
                        "Aging"+Environment.NewLine+"(Days)",
                        "Aging"+Environment.NewLine+"Current AP",
                        "Over Due"+Environment.NewLine+"1-30 Days",
                        "Over Due"+Environment.NewLine+"31-60 Days",
                        "Over Due"+Environment.NewLine+"61-90 Days",

                        //21-25
                        "Over Due"+Environment.NewLine+"91-120 Days",
                        "Over 120 Days",
                        "Department",
                        "Site",
                        "Entity",

                        //26-28
                        "Total IDR",
                        "Invoice#",
                        "Invoice"+Environment.NewLine+"Date"
                    },
                    new int[]
                    {
                        //0
                        100,

                        //1-5
                        200, 160, 80, 160, 80, 
                        
                        //6-10
                        200, 110, 200, 200, 150, 

                        //11-15
                        80, 120, 120, 120, 120, 
 
                        //16-20
                        100, 100, 100,100, 100,  

                        //21-25
                        100, 100, 200, 180, 180, 

                        //26-28
                        120, 160, 80
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 26 }, 2);
            Sm.GrdFormatDate(Grd1, new int[] { 3, 5, 28 });
            Sm.GrdColInvisible(Grd1, new int[] { 10, 23, 26 }, false);
            Sm.GrdColInvisible(Grd1, new int[] { 7, 8, 9 }, mIsBOMShowSpecifications);
            Grd1.Cols[26].Move(13);
            Grd1.Cols[27].Move(4);
            Grd1.Cols[28].Move(5);
            //if (!mIsSiteMandatory) Sm.GrdColInvisible(Grd1, new int[] { 24, 25 }, false);
        }

        override protected void HideInfoInGrd()
        {

            Sm.GrdColInvisible(Grd1, new int[] { 7, 20 }, !ChkHideInfoInGrd.Checked);
            if (mIsRptAgingAPRecvItemShowTotalIDR)
                Sm.GrdColInvisible(Grd1, new int[] { 26 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (ChkBalanceDt.Checked == false && ChkDocDt.Checked == false)
            {
                Sm.StdMsg(mMsgType.Warning, "Invoice Date or Balance As Of Date is empty"); return;
            }
            SetSQL();
            try
            {
                Cursor.Current = Cursors.WaitCursor;
               
                Grd1.GroupObject.Clear();
                Grd1.Group();

                var l = new List<Aging>();

                ProcessData1(ref l);
                if (l.Count>0) 
                    ProcessData2(ref l);
                else
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ProcessData1(ref List<Aging> l)
        { 
            l.Clear();

            string Filter = " And 0=0 ";

            var cm = new MySqlCommand();

            Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
            Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
            Sm.CmParamDt(ref cm, "@BalanceDt", Sm.GetDte(DteBalanceDt));
            Sm.CmParam(ref cm, "@UserCode", Gv.CurrentUserCode);          
            Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDueDt1), Sm.GetDte(DteDueDt2), "A.DueDt");
            Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueItCtCode), "A.ItCtCode", true);
            Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueVdCode), "B.VdCode", true);
            Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueVdCtCode), "C.VdCtCode", true);
            Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "D.SiteCode", true);
            
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = mSQL.ToString() + Filter;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "Periode", 

                    //1-5
                    "VdName", "RecvNo", "DocDt", "PONo", "ItCtName", 
                    
                    //6-10
                    "ItCodeInternal", "ItName", "Specification", "DueDt", "CurCode", 

                    //11-15
                    "Amount", "TaxAmt", "UnInvoiceAmount", "BalanceAmount", "AgingDays", 

                    //16-20
                    "AgingCurrent", "Aging1To30", "Aging31To60", "Aging61To90", "Aging91To120",  

                    //21-25
                    "AgingOver120", "DeptName", "SiteName", "EntName", "PtName", 

                    //26-28
                    "TotalIDR", "PIDocNo", "PIDocDt"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Aging()
                        {
                            Period = Sm.DrStr(dr, c[0]),
                            VdName = Sm.DrStr(dr, c[1]),
                            RecvNo = Sm.DrStr(dr, c[2]),
                            RecvDt = Sm.DrStr(dr, c[3]),
                            PONo = Sm.DrStr(dr, c[4]),
                            ItCtName = Sm.DrStr(dr, c[5]),
                            ItCodeInternal = Sm.DrStr(dr, c[6]),
                            ItName = Sm.DrStr(dr, c[7]),
                            Specification = Sm.DrStr(dr, c[8]),
                            DueDt = Sm.DrStr(dr, c[9]),
                            CurCode = Sm.DrStr(dr, c[10]),
                            Amount = decimal.Parse(Sm.FormatNum(Sm.DrDec(dr, c[11]), 0)),
                            TaxAmt = decimal.Parse(Sm.FormatNum(Sm.DrDec(dr, c[12]), 0)),
                            UnInvoiceAmount = decimal.Parse(Sm.FormatNum(Sm.DrDec(dr, c[13]), 0)),
                            BalanceAmount = decimal.Parse(Sm.FormatNum(Sm.DrDec(dr, c[14]), 0)),
                            AgingDays = decimal.Parse(Sm.DrStr(dr, c[15])),
                            AgingCurrent = decimal.Parse(Sm.FormatNum(Sm.DrDec(dr, c[16]), 0)),
                            Aging1To30 = decimal.Parse(Sm.FormatNum(Sm.DrDec(dr, c[17]), 0)),
                            Aging31To60 = decimal.Parse(Sm.FormatNum(Sm.DrDec(dr, c[18]), 0)),
                            Aging61To90 = decimal.Parse(Sm.FormatNum(Sm.DrDec(dr, c[19]), 0)),
                            Aging91To120 = decimal.Parse(Sm.FormatNum(Sm.DrDec(dr, c[20]), 0)),
                            AgingOver120 = decimal.Parse(Sm.FormatNum(Sm.DrDec(dr, c[21]), 0)),
                            DeptName = Sm.DrStr(dr, c[22]),
                            SiteName = Sm.DrStr(dr, c[23]),
                            EntName = Sm.DrStr(dr, c[24]),
                            PtName = Sm.DrStr(dr, c[25]),
                            TotalIDR = Sm.DrDec(dr, c[26]),
                            PIDocNo = Sm.DrStr(dr, c[27]),
                            PIDocDt = Sm.DrStr(dr, c[28]),
                        }) ;
                    }
                }
                dr.Close();
            }
        }

        private void ProcessData2(ref List<Aging> l)
        {
            iGRow r;
            Grd1.BeginUpdate();
            for (var i = 0; i < l.Count; i++)
            {
                r = Grd1.Rows.Add();
                r.Cells[0].Value = l[i].Period;
                r.Cells[1].Value = l[i].VdName;
                r.Cells[2].Value = l[i].RecvNo;
                r.Cells[3].Value = Sm.ConvertDate(l[i].RecvDt);
                r.Cells[4].Value = l[i].PONo;
                r.Cells[5].Value = Sm.ConvertDate(l[i].DueDt);
                r.Cells[6].Value = l[i].ItCtName;
                r.Cells[7].Value = l[i].ItCodeInternal;
                r.Cells[8].Value = l[i].ItName;
                r.Cells[9].Value = l[i].Specification;
                r.Cells[10].Value = l[i].PtName;
                r.Cells[11].Value = l[i].CurCode;
                r.Cells[12].Value = l[i].Amount;
                r.Cells[13].Value = l[i].TaxAmt;
                r.Cells[14].Value = l[i].UnInvoiceAmount;
                r.Cells[15].Value = l[i].BalanceAmount;
                if (l[i].AgingDays.ToString().Length == 0)
                    r.Cells[16].Value = 0m;
                else
                    r.Cells[16].Value = l[i].AgingDays;
                r.Cells[17].Value = l[i].AgingCurrent;
                r.Cells[18].Value = l[i].Aging1To30;
                r.Cells[19].Value = l[i].Aging31To60;
                r.Cells[20].Value = l[i].Aging61To90;
                r.Cells[21].Value = l[i].Aging91To120;
                r.Cells[22].Value = l[i].AgingOver120;
                r.Cells[23].Value = l[i].DeptName;
                r.Cells[24].Value = l[i].SiteName;
                r.Cells[25].Value = l[i].EntName;
                r.Cells[26].Value = l[i].TotalIDR;
                r.Cells[27].Value = l[i].PIDocNo;
                if(l[i].PIDocDt.ToString().Length != 0)
                    r.Cells[28].Value = Sm.ConvertDate(l[i].PIDocDt);
            }
            Grd1.GroupObject.Add(0);
            if (mIsSiteMandatory)
            {
                Grd1.GroupObject.Add(25);
                Grd1.GroupObject.Add(23);
            }
            Grd1.GroupObject.Add(1);
            Grd1.Group();
            AdjustSubtotals();
            Grd1.EndUpdate();
        }


        private void AdjustSubtotals()
        {
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ForeColor = Color.Black;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.HideSubtotals(Grd1);
            iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 26 });
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            Grd1.Rows.AutoHeight();
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        

        #endregion

        #endregion

        #endregion

        #region Event

        private void ChkVdCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Vendor");
        }

        private void ChkVdCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Vendor's Category");
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Received date");
        }

        private void ChkBalanceDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSingleSetDateEdit(this, sender, "Balance As Of date");
        }

        private void DteBalanceDt_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterSingleDteSetCheckEdit(this, sender);
            if (DteBalanceDt.EditValue != null)
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> { DteDocDt1, DteDocDt2, ChkDocDt }, true);
            else
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> { DteDocDt1, DteDocDt2, ChkDocDt }, false);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if (DteDocDt2.EditValue != null)
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> { DteBalanceDt, ChkBalanceDt }, true);
            else
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> { DteBalanceDt, ChkBalanceDt }, false);
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void ChkItCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Item's category");
        }

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue1(Sl.SetLueVdCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void LueVdCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueVdCtCode, new Sm.RefreshLue1(Sl.SetLueVdCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void LueItCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCtCode, new Sm.RefreshLue2(Sl.SetLueItCtCode), mIsFilterByItCt ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void DteDueDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDueDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void ChkDueDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Due date");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
            if (DteDocDt1.EditValue != null)
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> { DteBalanceDt, ChkBalanceDt }, true);
            else
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> { DteBalanceDt, ChkBalanceDt }, false);
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue3(Sl.SetLueSiteCode), string.Empty, mIsFilterBySite ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }

        #endregion

        #region Class
        
        private class Aging
        {
            public string Period { get; set; }
            public string VdName { get; set; }
            public string RecvNo { get; set; }
            public string RecvDt { get; set; }
            public string PONo { get; set; }
            public string ItCtName { get; set; }
            public string DueDt { get; set; }
            public string CurCode { get; set; }
            public decimal Amount { get; set; }
            public decimal TaxAmt { get; set; }
            public decimal UnInvoiceAmount { get; set; }
            public decimal BalanceAmount { get; set; }
            public decimal AgingDays { get; set; }
            public decimal AgingCurrent { get; set; }
            public decimal Aging1To30 { get; set; }
            public decimal Aging31To60 { get; set; }
            public decimal Aging61To90 { get; set; }
            public decimal Aging91To120 { get; set; }
            public decimal AgingOver120 { get; set; }
            public string DeptName { get; set; }
            public string EntName { get; set; }
            public string SiteName { get; set; }
            public string PtName { get; set; }
            public string ItCodeInternal { get; set; }
            public string ItName { get; set; }
            public string Specification { get; set; }

            public decimal TotalIDR { get; set; }
            public string PIDocNo { get; set; }
            public string PIDocDt { get; set; }
        }

        #endregion
    }
}
