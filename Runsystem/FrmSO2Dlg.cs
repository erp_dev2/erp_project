﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmSO2Dlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmSO2 mFrmParent;
        private string mSQL = string.Empty, mMenuCode = string.Empty;

        #endregion

        #region Constructor

        public FrmSO2Dlg(FrmSO2 FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mMenuCode = mFrmParent.mMenuCode;
        }

        #endregion

        #region Methods

        #region Form Load
        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            SetGrd();
            SetSQL();
            Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -7);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.ActInd, A.DteStart, A.DteEnd, A.CurCode, ");
            SQL.AppendLine("A.TrgtNett, A.NoOfShpM, A.TtlShipM, A.UomCode,  ");
            SQL.AppendLine("A.CashBack, D.CtName From TblSoQuotPromo A ");
            SQL.AppendLine("Inner Join TblCurrency B On A.CurCode = B.CurCode ");
            SQL.AppendLine("Inner Join TblCustomer D On A.CtCode = D.CtCode ");

            mSQL = SQL.ToString();
        }
        #endregion

        #region Set Grid

        private void SetGrd()
        {
            Grd1.Cols.Count = 14;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "",
                        "Document#", 
                        "Date",
                        "Active",
                        "Quotation Date "+Environment.NewLine+"Start",
                        //6-10
                        "Quotation Date "+Environment.NewLine+"End",
                        "Currency Code",
                        "Target Omset "+Environment.NewLine+" After Discount",
                        "CashBack "+Environment.NewLine+" Shipment",
                        "Cashback "+Environment.NewLine+" Amount",
                        //11-13
                        "UoM",
                        "Cashback Amount "+Environment.NewLine+" per Shipment",
                        "Customer"
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 1 } );
            Sm.GrdColCheck(Grd1, new int[] { 4 });
            Sm.GrdFormatDec(Grd1, new int[] { 8, 9, 10, 12 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 3, 5, 6 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 });
            Sm.GrdColInvisible(Grd1, new int[] { 7, 8, 9, 10, 11, 12, 13 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 7, 8, 9, 10, 11, 12, 13 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdAutoSize(Grd1);
        }
        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 1 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmSOQuotPromo(mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmSOQuotPromo(mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        #endregion

        #region Show Data
        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = "Where A.CtCode = @CtCode ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(mFrmParent.LueCtCode));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDt");

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.DocNo ",
                        new string[]
                        {
                            //0
                            "DocNo", 

                            //1-5
                            "DocDt", "ActInd", "DteStart", "DteEnd", "CurCode", 

                            "TrgtNett", "NoOfShpM", "TtlShipM", "UomCode",

                            "CashBack", "CtName",

                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("B", Grd1, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 5);

                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 12, 10);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 13, 11);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 2);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 2))
            {
                mFrmParent.TxtSOQuotPromo.Text = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 2);
                mFrmParent.ClearGrd();
                this.Hide();
            }
        }
        #endregion

        #endregion

        #region Event
        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document number");
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Document date");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }
        #endregion
    }
}
