﻿#region Update
/*
    29/01/2018 [TKG] 
        ditampilkan tanggal pembayaran (dari incoming payment yg sudah divoucher)
        Due date, yang mandatory adalah end duedate nya
    03/06/2021 [TKG] tambah informasi status invoice, settlement, collector
    28/06/2021 [VIN/KIM] tambah subtotal kolom settlement 
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptAgingARLedgerPaidItem : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;
        private bool mIsRptAgingARLedgerPaidItemShowKIMInfo = false;

        #endregion

        #region Constructor

        public FrmRptAgingARLedgerPaidItem(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                SetSQL();
                Sl.SetLueCtCode(ref LueCtCode);
                SetLueType();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -180);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'IsRptAgingARLedgerPaidItemShowKIMInfo' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsRptAgingARLedgerPaidItemShowKIMInfo": mIsRptAgingARLedgerPaidItemShowKIMInfo = ParValue == "Y"; break;
                        }
                    }
                }
                dr.Close();
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.*, ");
            SQL.AppendLine("Case T.Status ");
            SQL.AppendLine("    When 'O' Then 'Outstanding' ");
            SQL.AppendLine("    When 'F' Then 'Fulfilled' ");
            SQL.AppendLine("    When 'P' Then 'Partial' ");
            SQL.AppendLine("End As StatusDesc ");
            SQL.AppendLine("From ( ");
            
            #region Sales Invoice

            SQL.AppendLine("    Select DocDt, Period, '1' As Type, 'Sales Invoice' As TypeDesc, CurCode, CtCode, CtName, DocNo, TaxInvDocument, ");
            SQL.AppendLine("    InvoiceAmt, DownPayment, PaidAmt, Balance, DueDt, EarliestPaidDt, ");
            SQL.AppendLine("    Case When Balance<=0 Then 'F' Else  ");
            SQL.AppendLine("        Case When Balance>InvoiceAmt Then 'P' Else 'O' End  ");
            SQL.AppendLine("    End As Status, Remark, ");
            SQL.AppendLine("    EmailStatus, ARSAmt, Collector ");
            SQL.AppendLine("    From (  ");
            SQL.AppendLine("        Select A.DocDt, Concat(Left(A.DocDt, 4), '-',Substring(A.DocDt, 5, 2)) As Period, ");
            SQL.AppendLine("        A.CurCode, A.CtCode, C.CtName, A.DocNo, A.TaxInvDocument, ");
            SQL.AppendLine("        (A.TotalAmt+A.TotalTax) As InvoiceAmt, ");
            SQL.AppendLine("        A.DownPayment As DownPayment, ");
            SQL.AppendLine("        IfNull(B.PaidAmt, 0) As PaidAmt, ");
            SQL.AppendLine("        A.TotalAmt+A.TotalTax-A.DownPayment-IfNull(B.PaidAmt, 0) As Balance, ");
            SQL.AppendLine("        A.DueDt, B.EarliestPaidDt, A.Remark, ");
            if (mIsRptAgingARLedgerPaidItemShowKIMInfo)
            {
                SQL.AppendLine("        Case When D.DocNo Is Null Then Null Else 'Email has been sent.' End As EmailStatus, ");
                SQL.AppendLine("        IfNull(E.Amt, 0.00) As ARSAmt, ");
                SQL.AppendLine("        F.EmpName As Collector ");
            }
            else
            {
                SQL.AppendLine("        Null As EmailStatus, ");
                SQL.AppendLine("        0.00 As ARSAmt, ");
                SQL.AppendLine("        Null As Collector ");
            }
            SQL.AppendLine("        From TblSalesInvoiceHdr A  ");
            SQL.AppendLine("        Left Join (  ");
            SQL.AppendLine("            Select DocNo, Min(DocDt) As EarliestPaidDt, Sum(Amt) As PaidAMt  ");
	        SQL.AppendLine("            From (  ");
	        SQL.AppendLine("                Select T2.InvoiceDocNo As DocNo, T3.DocDt, T2.Amt  ");
	        SQL.AppendLine("                From TblIncomingPaymentHdr T1  ");
	        SQL.AppendLine("                Inner Join TblIncomingPaymentDtl T2 ");
            SQL.AppendLine("                    On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("                    And T2.InvoiceType='1' ");
            SQL.AppendLine("                    And T2.InvoiceDocNo In (");
            SQL.AppendLine("                        Select DocNo ");
            SQL.AppendLine("                        From TblSalesInvoiceHdr ");
            SQL.AppendLine("                        Where CancelInd='N' ");
            SQL.AppendLine("                        And DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("                        ) ");
            SQL.AppendLine("                Inner Join TblVoucherHdr T3 On T1.VoucherRequestDocNo=T3.VoucherRequestDocNo And T3.CancelInd='N' ");
            SQL.AppendLine("                Where T1.CancelInd='N' ");
            SQL.AppendLine("                And IfNull(T1.Status, 'O') In ('O', 'A') ");
	        SQL.AppendLine("            ) Tbl Group By DocNo  ");
            SQL.AppendLine("        ) B On A.DocNo=B.DocNo  ");
            SQL.AppendLine("        Inner Join (Select Left(CurrentDateTime(), 8) As  CurrentDateTime) C On 1=1 ");
            SQL.AppendLine("        Inner Join TblCustomer C On A.CtCode = C.CtCode ");
            if (mIsRptAgingARLedgerPaidItemShowKIMInfo)
            {
                SQL.AppendLine("        Left Join ( ");
                SQL.AppendLine("            Select Distinct T1.DocNo ");
                SQL.AppendLine("            From TblSalesInvoiceEmail T1, TblSalesInvoiceHdr T2 ");
                SQL.AppendLine("            Where T1.DocNo=T2.DocNo ");
                SQL.AppendLine("            And T2.CancelInd='N' ");
                SQL.AppendLine("            And T2.DocDt Between @DocDt1 And @DocDt2 ");
                SQL.AppendLine("        ) D On A.DocNo=D.DocNo ");
                SQL.AppendLine("        Left Join ( ");
                SQL.AppendLine("            Select T1.SalesInvoiceDocNo As DocNo, Sum(T1.Amt) As Amt ");
                SQL.AppendLine("            From TblARSHdr T1, TblSalesInvoiceHdr T2 ");
                SQL.AppendLine("            Where T1.SalesInvoiceDocNo=T2.DocNo ");
                SQL.AppendLine("            And T1.CancelInd='N' ");
                SQL.AppendLine("            And T2.DocDt Between @DocDt1 And @DocDt2 ");
                SQL.AppendLine("            Group By T1.SalesInvoiceDocNo ");
                SQL.AppendLine("        ) E On A.DocNo=E.DocNo ");
                SQL.AppendLine("        Left Join ( ");
                SQL.AppendLine("            Select T1.SalesInvoiceDocNo As DocNo, T3.EmpName ");
                SQL.AppendLine("            From TblSalesInvoiceControlHdr T1, TblSalesInvoiceHdr T2, TblEmployee T3 ");
                SQL.AppendLine("            Where T1.SalesInvoiceDocNo=T2.DocNo ");
                SQL.AppendLine("            And T1.CancelInd='N' ");
                SQL.AppendLine("            And T2.DocDt Between @DocDt1 And @DocDt2 ");
                SQL.AppendLine("            And T1.EmpCode=T3.EmpCode ");
                SQL.AppendLine("        ) F On A.DocNo=F.DocNo ");
            }
            SQL.AppendLine("        Where A.CancelInd='N' ");
            SQL.AppendLine("        And A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    ) Tbl ");

            #endregion
            
            SQL.AppendLine("Union All ");

            #region Sales Return Invoice
 
            SQL.AppendLine("    Select DocDt, Period, '2' As TypeDoc, 'Sales Return Invoice' As TypeDesc, CurCode, CtCode, CtName, DocNo, Null As VdInvNo, ");
            SQL.AppendLine("    InvoiceAmt, DownPayment, PaidAmt, Balance, DueDt, EarliestPaidDt, ");
            SQL.AppendLine("    Case When Balance<=0 Then 'F' Else ");
            SQL.AppendLine("        Case When Balance>InvoiceAmt Then 'P' Else 'O' End ");
            SQL.AppendLine("    End As Status, Remark, ");
            SQL.AppendLine("    Null As EmailStatus, ");
            SQL.AppendLine("    0.00 As ARSAmt, ");
            SQL.AppendLine("    Null As Collector ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("        Select A.DocDt, Concat(Left(A.DocDt, 4), '-',Substring(A.DocDt, 5, 2)) As Period, ");
            SQL.AppendLine("        A.CurCode, A.CtCode, C.Ctname,  A.DocNo, ");
            SQL.AppendLine("        A.TotalAmt As InvoiceAmt, 0 As DownPayment, IfNull(B.PaidAmt, 0) As PaidAmt, ");
            SQL.AppendLine("        A.TotalAmt-IfNull(B.PaidAmt, 0) As Balance, ");
            SQL.AppendLine("        Null As DueDt, B.EarliestPaidDt, A.Remark ");
            SQL.AppendLine("        From TblSalesReturnInvoiceHdr A ");
            SQL.AppendLine("        Left Join ( ");
            SQL.AppendLine("            Select T2.InvoiceDocNo As DocNo, Min(T3.DocDt) As EarliestPaidDt, Sum(T2.Amt) As PaidAMt ");
            SQL.AppendLine("            From TblIncomingPaymentHdr T1 ");
            SQL.AppendLine("            Inner Join TblIncomingPaymentDtl T2 ");
            SQL.AppendLine("                On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("                And T2.InvoiceType='2' ");
            SQL.AppendLine("                And T2.InvoiceDocNo In (");
            SQL.AppendLine("                    Select DocNo ");
            SQL.AppendLine("                    From TblSalesReturnInvoiceHdr  ");
            SQL.AppendLine("                    Where CancelInd='N' ");
            SQL.AppendLine("                    And DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("                    ) ");
            SQL.AppendLine("            Inner Join TblVoucherHdr T3 On T1.VoucherRequestDocNo=T3.VoucherRequestDocNo And T3.CancelInd='N' ");
            SQL.AppendLine("            Where T1.CancelInd='N' And IfNull(T1.Status, 'O') In ('O', 'A') ");
            SQL.AppendLine("            Group By T2.InvoiceDocNo ");
            SQL.AppendLine("        ) B On A.DocNo=B.DocNo ");
            SQL.AppendLine("        Inner Join TblCustomer C On A.CtCode = C.CtCode ");
            SQL.AppendLine("        Where A.CancelInd='N' ");
            SQL.AppendLine("        And A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    ) Tbl  ");

            #endregion

            SQL.AppendLine(") T ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 22;
            Grd1.FrozenArea.ColCount = 1;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "Period",
                        "Description", 
                        "Currency", 
                        "Customer", 
                        "Invoice#", 

                        //6-10
                        "Type", 
                        "",  
                        "Tax" + Environment.NewLine + "Invoice#", 
                        "Status", 
                        "Invoice" + Environment.NewLine + "Amount", 

                        //11-15
                        "Downpayment",  
                        "Paid" + Environment.NewLine + "Amount",  
                        "Balance",  
                        "Invoice" + Environment.NewLine + "Date",
                        "Due" + Environment.NewLine + "Date",  
                        
                        //16-20
                        "Earliest" + Environment.NewLine + "Paid Date",
                        "",
                        "Remark",
                        "Email's Status",
                        "Settlement",

                        //21
                        "Collector"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        80, 80, 60, 200, 180, 
                        
                        //6-10
                        80, 20, 180, 100, 150, 

                        //11-15
                        150, 150, 150, 100, 100, 
 
                        //16-20
                        100, 20, 250, 180, 130,

                        //21
                        200
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 7, 17 });
            Sm.GrdFormatDec(Grd1, new int[] {10, 11, 12, 13, 20 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 14, 15, 16 });
            Sm.GrdColInvisible(Grd1, new int[] { 6, 14, 15 }, false);
            if (mIsRptAgingARLedgerPaidItemShowKIMInfo)
            {
                Grd1.Cols[19].Move(10);
                Grd1.Cols[20].Move(14);
                Grd1.Cols[21].Move(16);
            }
            else
                Sm.GrdColInvisible(Grd1, new int[] { 19, 20, 21 }, false);
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 8, 9, 10, 11, 12, 13, 14, 15, 16, 18, 19, 20, 21 });
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 14, 15 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDueDt1), Sm.GetDte(DteDueDt2), "T.DueDt");
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueType), "T.Type", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCode), "T.CtCode", true);


                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + 
                        " Order By T.Period, T.Type, T.CurCode, T.CtName, T.DocNo; ",
                        new string[]
                        { 
                            //0
                            "Period", 

                            //1-5
                            "TypeDesc", "CurCode", "CtName", "DocNo", "Type",

                            //6-10
                            "TaxInvDocument", "StatusDesc", "InvoiceAmt", "Downpayment", "PaidAmt",  

                            //11-15
                            "Balance", "DocDt", "DueDt", "EarliestPaidDt", "Remark",

                            //16-18
                            "EmailStatus", "ARSAmt", "Collector"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 10);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 11);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 14, 12);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 15, 13);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 16, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 15);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 16);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 17);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 18);
                        }, true, false, false, false
                    );
                Grd1.GroupObject.Add(1);
                Grd1.GroupObject.Add(2);
                Grd1.GroupObject.Add(3);
                Grd1.Group();
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.HideSubtotals(Grd1);
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 10, 11, 12, 13, 20 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            Grd1.Rows.AutoHeight();
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            if (e.ColIndex == 7 && Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length != 0)
            {
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 6) == "1")
                {
                    var f1 = new FrmSalesInvoice("***");
                    f1.Tag = "***";
                    f1.WindowState = FormWindowState.Normal;
                    f1.StartPosition = FormStartPosition.CenterScreen;
                    f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 5);
                    f1.ShowDialog();
                }
                else
                {
                    var f2 = new FrmSalesReturnInvoice("***");
                    f2.Tag = "***";
                    f2.WindowState = FormWindowState.Normal;
                    f2.StartPosition = FormStartPosition.CenterScreen;
                    f2.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 5);
                    f2.ShowDialog();
                }
            }

            if (e.ColIndex == 17 && Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length != 0)
            {
                var f2 = new FrmRptAgingARLedgerPaidItemDlg(this, Sm.GetGrdStr(Grd1, e.RowIndex, 5), Sm.GetGrdStr(Grd1, e.RowIndex, 4));
                f2.Tag = "***";
                f2.WindowState = FormWindowState.Normal;
                f2.StartPosition = FormStartPosition.CenterScreen;
                f2.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 7 && Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length != 0)
            {
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 6) == "1")
                {
                    var f1 = new FrmSalesInvoice("***");
                    f1.Tag = "***";
                    f1.WindowState = FormWindowState.Normal;
                    f1.StartPosition = FormStartPosition.CenterScreen;
                    f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 5);
                    f1.ShowDialog();
                }
                else
                {
                    var f2 = new FrmSalesReturnInvoice("***");
                    f2.Tag = "***";
                    f2.WindowState = FormWindowState.Normal;
                    f2.StartPosition = FormStartPosition.CenterScreen;
                    f2.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 5);
                    f2.ShowDialog();
                }
            }
            if (e.ColIndex == 17 && Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length != 0)
            {
                var f2 = new FrmRptAgingARLedgerPaidItemDlg(this, Sm.GetGrdStr(Grd1, e.RowIndex, 5), Sm.GetGrdStr(Grd1, e.RowIndex, 4));
                f2.Tag = "***";
                f2.WindowState = FormWindowState.Normal;
                f2.StartPosition = FormStartPosition.CenterScreen;
                f2.ShowDialog();
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Additional Method

        private void SetLueType()
        {
            Sm.SetLue2(
                ref LueType,
                "Select '1' As Col1, 'Sales Invoice' As Col2 " +
                "Union All Select '2' As Col1, 'Sales Return Invoice' As Col2;",
                0, 35, false, true, "Code", "Type", "Col2", "Col1");
        }


        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");
        }

        private void LueType_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.CompareStr(Sm.GetLue(LueType), "<Refresh>")) LueType.EditValue = null;
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkType_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Type");
        }

        private void DteDueDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDueDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void ChkDueDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Due date");
        }

        #endregion

        #endregion
    }
}
