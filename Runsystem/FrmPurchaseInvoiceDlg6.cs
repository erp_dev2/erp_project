﻿#region update 
/*
 *  20/10/2022 [ICA/AMKA] new apps 
 *  27/10/2022 [ICA/AMKA] menambah validasi APDP yg muncul sesuai dengan Tax yg dipilih dan mengcopy tax APDP ke grid parent sesuai tax pada header parent
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPurchaseInvoiceDlg6 : RunSystem.FrmBase4
    {
        #region Field

        private FrmPurchaseInvoice mFrmParent;
        private string mSQL = string.Empty, mVdCode = string.Empty, mTaxCodes = string.Empty;
        private bool mIsFilterByWhsCode = false;

        #endregion

        #region Constructor

        public FrmPurchaseInvoiceDlg6(FrmPurchaseInvoice FrmParent, string VdCode, string TaxCodes)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mVdCode = VdCode;
            mTaxCodes = TaxCodes;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -30);

                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 18;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[]
                    {
                        //0
                        "No",

                        //1-5
                        "",
                        "Document#",
                        "Date",
                        "Downpayment"+Environment.NewLine+"Before Tax",
                        "Tax Code",

                        //6-10
                        "Tax 1",
                        "Tax Rate 1",
                        "Tax 1 Amount",
                        "Tax Code2",
                        "Tax 2",

                        //11-15
                        "TaxRate 3",
                        "Tax 2 Amount",
                        "Tax Code3",
                        "Tax 3",
                        "Tax Rate 3",

                        //16-17
                        "Tax 3 Amount",
                        "Downpayment"+Environment.NewLine+"After Tax"
                    },
                    new int[]
                    {
                        //0
                        50,

                        //1-5
                        20, 150, 120, 120, 0, 
                        
                        //6-10
                        150, 100, 120, 0, 150, 
                        
                        //11-15
                        100, 120, 0, 150, 100, 
                        
                        
                        //16-17
                        120, 120,
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdFormatDate(Grd1, new int[] { 3 });
            Sm.GrdColReadOnly(Grd1, new int[] { 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17 });
            Sm.GrdFormatDec(Grd1, new int[] { 4, 7, 8, 11, 12, 15, 16, 17 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 5, 7, 9, 11, 13, 15 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT A.DocNo, A.DocDt, (A.AmtBefTax - IfNull(Downpayment, 0.00) - IfNull(G.Amt, 0.00)) APDownpaymentAmt, C.TaxName, D.TaxName TaxName2, E.TaxName TaxName3, ");
            SQL.AppendLine("A.TaxCode, A.TaxCode2, A.TaxCode3, C.TaxRate, D.TaxRate TaxRate2, E.TaxRate TaxRate3, ");
            SQL.AppendLine("(C.TaxRate/100)*(A.AmtBefTax - IfNull(Downpayment, 0.00) - IfNull(G.Amt, 0.00)) TaxAmt, (D.TaxRate/100)*(A.AmtBefTax - IfNull(Downpayment, 0.00) - IfNull(G.Amt, 0.00)) TaxAmt2, ");
            SQL.AppendLine("(E.TaxRate/100)*(A.AmtBefTax - IfNull(Downpayment, 0.00) - IfNull(G.Amt, 0.00)) TaxAmt3 ");
            SQL.AppendLine("FROM TblAPDownpayment A ");
            SQL.AppendLine("INNER JOIN TblVoucherRequestHdr B ON A.VoucherRequestDocNo = B.DocNo AND A.VdCode = @VdCode  ");
            SQL.AppendLine("        And B.CancelInd='N' ");
            SQL.AppendLine("        And B.Status<>'C' ");
            SQL.AppendLine("        And B.VoucherDocNo Is Not Null ");
            SQL.AppendLine("Left JOIN TblTax C ON A.TaxCode = C.TaxCode ");
            SQL.AppendLine("LEFT JOIN TblTax D ON A.TaxCode2 = D.TaxCode ");
            SQL.AppendLine("LEFT JOIN TblTax E ON A.TaxCode3 = E.TaxCode ");
            SQL.AppendLine("LEFT JOIN ( ");
            SQL.AppendLine("	SELECT B.APDownpaymentDocNo, Sum(B.DownpaymentBefTax) Downpayment ");
            SQL.AppendLine("	FROM TblPurchaseInvoiceHdr A ");
            SQL.AppendLine("	INNER JOIN TblPurchaseInvoiceDtl8 B ON A.DocNo = B.DocNo  ");
            SQL.AppendLine("	WHERE A.CancelInd = 'N' ");
            SQL.AppendLine("	GROUP BY B.APDownpaymentDocNo ");
            SQL.AppendLine(")F ON A.DocNo = F.APDownpaymentDocNo ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T1.VdCode, Sum(T1.Amt) As Amt ");
            SQL.AppendLine("    From TblReturnAPDownpayment T1 ");
            SQL.AppendLine("    Inner Join TblVoucherRequestHdr T2 ");
            SQL.AppendLine("        On T1.VoucherRequestDocNo=T2.DocNo ");
            SQL.AppendLine("        And T2.CancelInd='N' ");
            SQL.AppendLine("        And T2.Status<>'C' ");
            SQL.AppendLine("        And T2.VoucherDocNo Is Not Null ");
            SQL.AppendLine("    Where T1.VdCode=@VdCode And T1.Status<>'C' And T1.CancelInd='N' ");
            SQL.AppendLine("    Group By T1.VdCode "); 
            SQL.AppendLine(") G On A.VdCode = G.VdCode ");
            SQL.AppendLine("WHERE A.CancelInd = 'N' ");
            SQL.AppendLine("	AND A.Status = 'A' ");
            SQL.AppendLine("    And A.DocDt Between @DocDt1 and @DocDt2 ");
            SQL.AppendLine("    And (A.TaxCode Is Null Or Find_In_Set(A.TaxCode, @TaxCodes)) ");
            SQL.AppendLine("    And (A.TaxCode2 Is Null Or Find_In_Set(A.TaxCode2, @TaxCodes)) ");
            SQL.AppendLine("    And (A.TaxCode3 Is Null Or Find_In_Set(A.TaxCode3, @TaxCodes)) ");

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();
                string DocNo = string.Empty,
                    Filter = string.Empty;

                if (mFrmParent.Grd7.Rows.Count >= 1)
                {
                    for (int r = 0; r < mFrmParent.Grd7.Rows.Count; r++)
                    {
                        DocNo = Sm.GetGrdStr(mFrmParent.Grd7, r, 1);
                        if (DocNo.Length != 0)
                        {
                            if (Filter.Length > 0) Filter += " Or ";
                            Filter += " (A.DocNo=@DocNo0" + r.ToString() + ") ";
                            Sm.CmParam<String>(ref cm, "@DocNo0" + r.ToString(), DocNo);
                        }
                    }
                }

                if (Filter.Length != 0)
                    Filter = " And Not ( " + Filter + ") ";
                else
                    Filter = " ";

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.CmParam<String>(ref cm, "@VdCode", mVdCode);
                Sm.CmParam<String>(ref cm, "@TaxCodes", mTaxCodes);
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By A.DocNo Desc;",
                    new string[]
                    { 
                        //0
                        "DocNo",
                        
                        //1-5
                        "DocDt", "APDownpaymentAmt", "TaxCode", "TaxName", "TaxRate", 
            
                        //6-10
                        "TaxAmt", "TaxCode2", "TaxName2", "TaxRate2", "TaxAmt2", 
                        
                        //11-14
                        "TaxCode3", "TaxName3", "TaxRate3", "TaxAmt3"

                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Grd.Cells[Row, 1].Value = false;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 1);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 5);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 9);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 10);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 12);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 13);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 14);
                        Grd1.Cells[Row, 17].Value = Sm.GetGrdDec(Grd1, Row, 4) + Sm.GetGrdDec(Grd1, Row, 8) + Sm.GetGrdDec(Grd1, Row, 12) + Sm.GetGrdDec(Grd1, Row, 16);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsDataAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd7.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd7, Row1, 1, Grd1, Row2, 2); //DocNo
                        Sm.CopyGrdValue(mFrmParent.Grd7, Row1, 2, Grd1, Row2, 4); //APDPAmt
                        Sm.CopyGrdValue(mFrmParent.Grd7, Row1, 3, Grd1, Row2, 5); //TaxCode
                        Sm.CopyGrdValue(mFrmParent.Grd7, Row1, 4, Grd1, Row2, 6); //TaxName
                        Sm.CopyGrdValue(mFrmParent.Grd7, Row1, 5, Grd1, Row2, 7); //TaxRate
                        Sm.CopyGrdValue(mFrmParent.Grd7, Row1, 6, Grd1, Row2, 8); //TaxAmt
                        Sm.CopyGrdValue(mFrmParent.Grd7, Row1, 7, Grd1, Row2, 9); //TaxCode2
                        Sm.CopyGrdValue(mFrmParent.Grd7, Row1, 8, Grd1, Row2, 10); //TaxName2
                        Sm.CopyGrdValue(mFrmParent.Grd7, Row1, 9, Grd1, Row2, 11); //TaxRate2
                        Sm.CopyGrdValue(mFrmParent.Grd7, Row1, 10, Grd1, Row2, 12); //TaxAmt2
                        Sm.CopyGrdValue(mFrmParent.Grd7, Row1, 11, Grd1, Row2, 13); //TaxCode3
                        Sm.CopyGrdValue(mFrmParent.Grd7, Row1, 12, Grd1, Row2, 14); //TaxName3
                        Sm.CopyGrdValue(mFrmParent.Grd7, Row1, 13, Grd1, Row2, 15); //TaxRate3
                        Sm.CopyGrdValue(mFrmParent.Grd7, Row1, 14, Grd1, Row2, 16); //TaxAmt3
                        Sm.CopyGrdValue(mFrmParent.Grd7, Row1, 15, Grd1, Row2, 17); //AfterTax
                        mFrmParent.Grd7.Rows.Add();

                        mFrmParent.ComputeDownpayment(Row1);
                        mFrmParent.ReIndexColumnTax(Row1);
                        Sm.SetGrdNumValueZero(ref mFrmParent.Grd7, mFrmParent.Grd7.Rows.Count - 1, new int[] { 2, 5, 6, 9, 10, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25 });
                    }
                }
            }
            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 received document.");
        }

        private bool IsDataAlreadyChosen(int Row)
        {
            for (int row = 0; row <= mFrmParent.Grd7.Rows.Count - 1; row++)
                if (Sm.CompareStr(
                    Sm.GetGrdStr(mFrmParent.Grd7, row, 1),
                    Sm.GetGrdStr(Grd1, Row, 2)
                    )) return true;
            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    Grd1.Cells[Row, 1].Value = !IsSelected;
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        #endregion

        #endregion

    }
}
