﻿#region Update
/*
    19/05/2020 [DITA/IMS] new apps
    15/12/2020 [WED/IMS] tarik item Transfer Request Project yg aktif dan approved
    11/03/2021 [TKG/IMS] menambah heat number
    22/03/2021 [WED/IMS] item yang muncul mintanya dari batch yg sesuai dengan Project Code nya --> https://trello.com/c/fJsdaA7t/901-bug-do-to-other-warehouse-for-project
    20/04/2021 [TKG/IMS] ubah query
    21/04/2021 [TKG/IMS] ubah validasi
    18/10/2021 [VIN/IMS] ubah validasi item berdasarkan project code 
    29/10/2021 [VIN/IMS] bug validasi item berdasarkan project code 
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmDOWhs4Dlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmDOWhs4 mFrmParent;
        private string mSQL = string.Empty;
        internal int mNumberOfInventoryUomCode = 1;

        #endregion

        #region Constructor

        public FrmDOWhs4Dlg(FrmDOWhs4 FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ TxtBom }, true);
                SetNumberOfInventoryUomCode();
                SetGrd();
                SetSQL();
                Sl.SetLueItCtCode(ref LueItCtCode);
                Sl.SetLueWhsCode(ref LueWhsCode);
                
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private string GetSQL(string Filter, string Filter2)
        {
            var SQL = new StringBuilder();

            #region Old Code

            //SQL.AppendLine("Select T1.TRPDNo, T1.ItCode, T3.ItName, T4.ItCtName, T6.CCtName, ");
            //SQL.AppendLine("T3.InventoryUOMCode, T3.InventoryUOMCode2, T3.InventoryUOMCode3, T2.WhsCode, T2.WhsName, T1.Qty, T1.Qty2, T1.Qty3, ");
            //SQL.AppendLine("T3.ItGrpCode, IfNull(T2.StockQty, 0.00) StockQty, IfNull(T2.StockQty2, 0.00) StockQty2, IfNull(T2.StockQty3, 0.00) StockQty3, ");
            //SQL.AppendLine("T3.ItCodeInternal, T3.Specification, T2.Source, T2.Lot, T2.Bin, T2.PropCode, T2.BatchNo ");
            //if (mFrmParent.mIsDOWhs4HeatNumberEnabled)
            //    SQL.AppendLine(", T7.Value2 As HeatNumber ");
            //else
            //    SQL.AppendLine(", Null As HeatNumber ");
            //SQL.AppendLine("From  ");
            //SQL.AppendLine("( ");
            //SQL.AppendLine("    Select X1.ItCode, X1.DNo TRPDNo, (X1.Qty - IfNull(X2.Qty, 0.00)) Qty, (X1.Qty2 - IfNull(X2.Qty2, 0.00)) Qty2, (X1.Qty3 - IfNull(X2.Qty3, 0.00)) Qty3 ");
            //SQL.AppendLine("    From TblTransferRequestProjectDtl X1 ");
            //SQL.AppendLine("    Left Join ");
            //SQL.AppendLine("    ( ");
            //SQL.AppendLine("        Select B.TransferRequestProjectDocNo, B.TransferRequestProjectDNo, Sum(C.Qty) Qty, Sum(C.Qty2) Qty2, Sum(C.Qty3) Qty3 ");
            //SQL.AppendLine("        From TblDOWhs4Hdr A ");
            //SQL.AppendLine("        Inner Join TblDOWhs4Dtl B On A.DocNo = B.DocNo ");
            //SQL.AppendLine("        Inner Join TblTransferRequestWhsDtl C On B.TransferRequestWhsDocNo = C.DocNo And B.TransferRequestWhsDNo = C.DNo ");
            //SQL.AppendLine("        Inner Join TblTransferRequestWhsHdr D On C.DocNo = D.DocNo  And D.CancelInd = 'N' And D.Status = 'A' ");
            //SQL.AppendLine("        Group By B.TransferRequestProjectDocNo, B.TransferRequestProjectDNo ");
            //SQL.AppendLine("    ) X2 On X1.DocNo = X2.TransferRequestProjectDocNo And X1.DNo = X2.TransferRequestProjectDNo ");
            //SQL.AppendLine("    Where X1.WhsCode Is Null And X1.DocNo = @TransferRequestProjectDocNo ");
            //SQL.AppendLine("    And X1.ProcessInd In ('O', 'P') ");
            //SQL.AppendLine("    And X1.CancelInd = 'N' And X1.Status = 'A' ");
            //SQL.AppendLine(") T1 ");
            //SQL.AppendLine("Inner Join ");
            //SQL.AppendLine("( ");
            //SQL.AppendLine("    Select A.ItCode, A.Source, A.Lot, A.Bin, A.PropCode, A.BatchNo, ");
            //SQL.AppendLine("    A.WhsCode, H.WhsName, ");
            //SQL.AppendLine("    IfNull(A.StockQty, 0)-IfNull(B.TransferredQty, 0)-IfNull(C.DOQty, 0) As StockQty, ");
            //SQL.AppendLine("    IfNull(A.StockQty2, 0)-IfNull(B.TransferredQty2, 0)-IfNull(C.DOQty2, 0) As StockQty2, ");
            //SQL.AppendLine("    IfNull(A.StockQty3, 0)-IfNull(B.TransferredQty3, 0)-IfNull(C.DOQty3, 0) As StockQty3 ");
            //SQL.AppendLine("    From ( ");
            //SQL.AppendLine("        Select ItCode, Source, Lot, Bin, PropCode, BatchNo, WhsCode, ");
            //SQL.AppendLine("        Sum(Qty) As StockQty, Sum(Qty2) As StockQty2, Sum(Qty3) As StockQty3 ");
            //SQL.AppendLine("        From TblStockSummary ");
            //SQL.AppendLine("        Where Qty<>0 ");
            //SQL.AppendLine("        And BatchNo = @ProjectCode ");
            //// SQL.AppendLine("    And WhsCode=@WhsCode ");
            //SQL.AppendLine(Filter.Replace("X.", "").Replace("Y.", ""));
            //if (TxtBom.Text.Length > 0)
            //{
            //    SQL.AppendLine("     And ItCode In ( ");
            //    SQL.AppendLine("        Select DocCode From TblBomDtl ");
            //    SQL.AppendLine("        Where DocType = '1' ");
            //    SQL.AppendLine("        And DocNo=@BomDocNo ");
            //    SQL.AppendLine(Filter.Replace("X.", "").Replace("Y.", ""));
            //    SQL.AppendLine("        ) ");
            //}
            //SQL.AppendLine("        Group By ItCode, Source, Lot, Bin, PropCode, BatchNo, WhsCode ");
            //SQL.AppendLine("    ) A ");
            //SQL.AppendLine("    Left join ( ");
            //SQL.AppendLine("        Select T2.ItCode, ");
            //SQL.AppendLine("        Sum(T2.Qty) As TransferredQty, ");
            //SQL.AppendLine("        Sum(T2.Qty2) As TransferredQty2, ");
            //SQL.AppendLine("        Sum(T2.Qty3) As TransferredQty3 ");
            //SQL.AppendLine("        From TblTransferRequestWhsHdr T1 ");
            //SQL.AppendLine("        Inner Join TblTransferRequestWhsDtl T2 On T1.DocNo=T2.DocNo And T2.ProcessInd ='O' ");
            //SQL.AppendLine(Filter.Replace("X.", "T2.").Replace("Y.", "T1."));
            //SQL.AppendLine("        Where T1.CancelInd='N' ");
            //SQL.AppendLine("        And IfNull(T1.Status, 'O') In ('O', 'A') ");
            //// SQL.AppendLine("    And T1.WhsCode=@WhsCode ");
            //SQL.AppendLine("        Group By T2.ItCode ");
            //SQL.AppendLine("    ) B On A.ItCode=B.ItCode ");
            //SQL.AppendLine("    Left join ( ");
            //SQL.AppendLine("        Select T4.ItCode, T4.Source, T4.BatchNo, T4.PropCode, T4.Lot, T4.Bin, ");
            //SQL.AppendLine("        Sum(T4.Qty) As DOQty, ");
            //SQL.AppendLine("        Sum(T4.Qty2) As DOQty2, ");
            //SQL.AppendLine("        Sum(T4.Qty3) As DOQty3 ");
            //SQL.AppendLine("        From TblTransferRequestWhsHdr T1 ");
            //SQL.AppendLine("        Inner Join TblTransferRequestWhsDtl T2 On T1.DocNo=T2.DocNo And T2.ProcessInd='F' ");
            //SQL.AppendLine(Filter.Replace("X.", "T2.").Replace("Y.", "T1."));
            //SQL.AppendLine("        Inner Join TblDOWhsHdr T3 ");
            //SQL.AppendLine("            On T3.TransferRequestWhsDocNo Is Not Null ");
            //SQL.AppendLine("            And T2.DocNo=IfNull(T3.TransferRequestWhsDocNo, '') ");
            //SQL.AppendLine("            And T3.CancelInd ='N' ");
            //SQL.AppendLine("            And T3.Status In ('A', 'O') ");
            //SQL.AppendLine("        Inner Join TblDOWhsDtl T4 ");
            //SQL.AppendLine("            On T3.DocNo=T4.DocNo ");
            //SQL.AppendLine("            And T2.ItCode=T4.ItCode ");
            //SQL.AppendLine("            And T4.ProcessInd='O' ");
            //SQL.AppendLine("            And T4.CancelInd='N' ");
            //SQL.AppendLine(Filter.Replace("X.", "T4.").Replace("Y.", "T1."));
            //SQL.AppendLine("        Where T1.CancelInd='N' ");
            //SQL.AppendLine("        And IfNull(T1.Status, 'O')='A' ");
            //// SQL.AppendLine("    And T1.WhsCode=@WhsCode ");
            //SQL.AppendLine("        Group By T4.ItCode, T4.Source, T4.BatchNo, T4.PropCode, T4.Lot, T4.Bin ");
            //SQL.AppendLine("    ) C On A.ItCode=C.ItCode And A.Source = C.Source And A.BatchNo = C.BatchNo And A.Lot = C.Lot And A.Bin = C.Bin And A.PropCode = C.PropCode ");
            //SQL.AppendLine("    Inner Join TblWarehouse H On H.WhsCode = A.WhsCode " + Filter2.Replace("X.", "A.").Replace("Y.", "H."));
            //SQL.AppendLine("    Where (IfNull(A.StockQty, 0)-IfNull(B.TransferredQty, 0)-IfNull(C.DOQty, 0))>0 ");
            //SQL.AppendLine(") T2 On T1.ItCode = T2.ItCode ");
            //SQL.AppendLine("Inner Join TblItem T3 On T1.ItCode = T3.ItCode ");
            //SQL.AppendLine("Inner Join TblItemCategory T4 On T3.ItCtCode = T4.ItCtCode ");
            //SQL.AppendLine("Left Join TblItemCostCategory T5 On T1.ItCode = T5.ItCode And T5.CCCode = @CCCode ");
            //SQL.AppendLine("Left Join TblCostCategory T6 On T5.CCCode = T6.CCCode And T5.CCtCode = T6.CCtCode ");
            //if (mFrmParent.mIsDOWhs4HeatNumberEnabled)
            //    SQL.AppendLine("Left Join TblSourceInfo T7 On T2.Source=T7.Source ");

            #endregion

            SQL.AppendLine("Select T2.DNo As TRPDNo, T5.ItName, T6.ItCtName, T8.CCtName, T5.ItCodeInternal, T5.Specification, T5.ItGrpCode, ");
            SQL.AppendLine("T5.InventoryUOMCode, T5.InventoryUOMCode2, T5.InventoryUOMCode3, ");
            SQL.AppendLine("T2.Qty-IfNull(T3.Qty, 0.00) As Qty, ");
            SQL.AppendLine("T2.Qty2-IfNull(T3.Qty2, 0.00) As Qty2, "); 
            SQL.AppendLine("T2.Qty3-IfNull(T3.Qty3, 0.00) As Qty3, "); 
            SQL.AppendLine("T4.WhsCode, T10.WhsName, T4.Lot, T4.Bin, T4.ItCode, T4.PropCode, T4.BatchNo, T4.Source,  ");
            SQL.AppendLine("IfNull(T4.Qty, 0.00) As StockQty, IfNull(T4.Qty2, 0.00) As StockQty2, IfNull(T4.Qty3, 0.00) As StockQty3, ");
            if (mFrmParent.mIsDOWhs4HeatNumberEnabled)
                SQL.AppendLine("T9.Value2 As HeatNumber ");
            else
                SQL.AppendLine("Null As HeatNumber ");
            SQL.AppendLine("From TblTransferRequestProjectHdr T1 ");
            SQL.AppendLine("Inner Join TblTransferRequestProjectDtl T2  ");
	        SQL.AppendLine("    On T1.DocNo=T2.DocNo "); 
	        SQL.AppendLine("    And T2.CancelInd='N' "); 
	        SQL.AppendLine("    And T2.Status In ('O', 'A')  ");
	        SQL.AppendLine("    And T2.WhsCode Is Null ");
            SQL.AppendLine("Left Join ( ");
	        SQL.AppendLine("    Select B.DocNo, B.DNo, Sum(F.Qty) As Qty, Sum(F.Qty2) As Qty2, Sum(F.Qty3) As Qty3 ");
	        SQL.AppendLine("    From TblTransferRequestProjectHdr A ");
	        SQL.AppendLine("    Inner Join TblTransferRequestProjectDtl B  ");
		    SQL.AppendLine("        On A.DocNo=B.DocNo  ");
		    SQL.AppendLine("        And B.CancelInd='N' "); 
		    SQL.AppendLine("        And B.Status In ('O', 'A')  ");
		    SQL.AppendLine("        And B.WhsCode Is Null ");
	        SQL.AppendLine("    Inner Join TblDOWhs4Hdr C On A.DocNo=C.TransferRequestProjectDocNo And C.Status In ('O', 'A') ");
	        SQL.AppendLine("    Inner Join TblDOWhs4Dtl D On C.DocNo=D.DocNo And B.DocNo=D.TransferRequestProjectDocNo And B.DNo=D.TransferRequestProjectDNo ");
	        SQL.AppendLine("    Inner Join TblTransferRequestWhsHdr E On D.TransferRequestWhsDocNo=E.DocNo And E.CancelInd='N' And E.Status In ('O', 'A') ");
	        SQL.AppendLine("    Inner Join TblTransferRequestWhsDtl F On E.DocNo=F.DocNo And D.TransferRequestWhsDocNo=F.DocNo And D.TransferRequestWhsDNo=F.DNo  ");
	        SQL.AppendLine("    Where A.CancelInd='N'  ");
	        SQL.AppendLine("    And A.Status In ('O', 'A') ");
            SQL.AppendLine("    And A.DocNo=@TransferRequestProjectDocNo ");
	        SQL.AppendLine("    Group By B.DocNo, B.DNo ");
            SQL.AppendLine(") T3 On T2.DocNo=T3.DocNo And T2.DNo=T3.DNo ");
            SQL.AppendLine("Left  Join ( ");
	        SQL.AppendLine("    Select B.DocNo, B.DNo, C.WhsCode, C.Lot, C.Bin, C.ItCode, C.BatchNo, C.PropCode, C.Source, ");
	        SQL.AppendLine("    C.Qty-IfNull(D.Qty, 0.00) As Qty,  ");
	        SQL.AppendLine("    C.Qty2-IfNull(D.Qty2, 0.00) As Qty2, ");
	        SQL.AppendLine("    C.Qty3-IfNull(D.Qty3, 0.00) As Qty3 ");
	        SQL.AppendLine("    From TblTransferRequestProjectHdr A ");
	        SQL.AppendLine("    Inner Join TblTransferRequestProjectDtl B  ");
		    SQL.AppendLine("        On A.DocNo=B.DocNo  ");
		    SQL.AppendLine("        And B.CancelInd='N' "); 
		    SQL.AppendLine("        And B.Status In ('O', 'A')  ");
		    SQL.AppendLine("        And B.WhsCode Is Null ");
            SQL.AppendLine("    Inner Join TblStockSummary C On B.ItCode=C.ItCode  ");
            SQL.AppendLine(Filter);
	        SQL.AppendLine("    Left Join ( ");
		    SQL.AppendLine("        Select X5.WhsCode, X6.Lot, X6.Bin, X6.Source,  ");
		    SQL.AppendLine("        Sum(X6.Qty) As Qty, Sum(X6.Qty2) As Qty2, Sum(X6.Qty3) As Qty3 ");
		    SQL.AppendLine("        From TblTransferRequestProjectHdr X1 ");
		    SQL.AppendLine("        Inner Join TblTransferRequestProjectDtl X2  ");
			SQL.AppendLine("            On X1.DocNo=X2.DocNo  ");
			SQL.AppendLine("            And X2.CancelInd='N'  ");
			SQL.AppendLine("            And X2.Status In ('O', 'A') "); 
			SQL.AppendLine("            And X2.WhsCode Is Null ");
		    SQL.AppendLine("        Inner Join TblDOWhs4Hdr X3 On X1.DocNo=X3.TransferRequestProjectDocNo And X3.Status In ('O', 'A') ");
		    SQL.AppendLine("        Inner Join TblDOWhs4Dtl X4 On X3.DocNo=X4.DocNo And X2.DocNo=X4.TransferRequestProjectDocNo And X2.DNo=X4.TransferRequestProjectDNo ");
		    SQL.AppendLine("        Inner Join TblDOWhsHdr X5 On X4.DOWhsDocNo=X5.DocNo And X5.CancelInd='N' And X5.Status In ('O', 'A') ");
		    SQL.AppendLine("        Inner Join TblDOWhsDtl X6 On X5.DocNo=X6.DocNo And X4.DOWhsDocNo=X6.DocNo And X4.DOWhsDNo=X6.DNo And X6.CancelInd='N'  ");
		    SQL.AppendLine("        Where X1.CancelInd='N' "); 
		    SQL.AppendLine("        And X1.Status In ('O', 'A') ");
		    SQL.AppendLine("        And X6.Qty-X4.RecvQty>0.00 ");
            SQL.AppendLine("        And X1.DocNo=@TransferRequestProjectDocNo ");
            
	        SQL.AppendLine("    ) D  ");
		    SQL.AppendLine("        On C.WhsCode=D.WhsCode ");
		    SQL.AppendLine("        And C.Lot=D.Lot ");
		    SQL.AppendLine("        And C.Bin=D.Bin ");
		    SQL.AppendLine("        And C.Source=D.Source ");
	        SQL.AppendLine("    Where A.CancelInd='N'  ");
	        SQL.AppendLine("    And A.Status In ('O', 'A') ");
	        SQL.AppendLine("    And C.Qty-IfNull(D.Qty, 0.00)>0.00 ");
            SQL.AppendLine("    And A.DocNo=@TransferRequestProjectDocNo ");
            SQL.AppendLine(") T4 On T2.DocNo=T4.DocNo And T2.DNo=T4.DNo ");
            SQL.AppendLine("Inner Join TblItem T5 On T2.ItCode=T5.ItCode ");
            SQL.AppendLine(Filter2);
            SQL.AppendLine("Inner Join TblItemCategory T6 On T5.ItCtCode=T6.ItCtCode ");
            SQL.AppendLine("Left Join TblItemCostCategory T7 On T5.ItCode=T7.ItCode And T7.CCCode=@CCCode ");
            SQL.AppendLine("Left Join TblCostCategory T8 On T7.CCCode=T8.CCCode And T7.CCtCode=T8.CCtCode ");
            if (mFrmParent.mIsDOWhs4HeatNumberEnabled)
                SQL.AppendLine("Left Join TblSourceInfo T9 On T4.Source=T9.Source ");
            SQL.AppendLine("Left Join TblWarehouse T10 On T4.WhsCode=T10.WhsCode ");
            SQL.AppendLine("Where T1.CancelInd='N' ");
            SQL.AppendLine("And T1.Status In ('O', 'A') ");
            SQL.AppendLine("And T1.DocNo=@TransferRequestProjectDocNo ");
            SQL.AppendLine("And T2.Qty-IfNull(T3.Qty, 0.00)>0.00 ");
            SQL.AppendLine("And T4.BatchNo = @ProjectCode ");
            SQL.AppendLine("Order By T5.ItName, T10.WhsName; ");
            
            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 28;
            Grd1.FrozenArea.ColCount = 6;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "", 
                        "Item's Code", 
                        "", 
                        "Item's Name", 
                        "Item's Category",

                        //6-10
                        "Stock",
                        "UoM ",
                        "Stock",
                        "UoM",
                        "Stock",
                        
                        //11-15
                        "UoM",
                        "Cost Category",
                        "Group",
                        "Local Code",
                        "Specification",

                        //16-20
                        "Warehouse Code",
                        "Warehouse",
                        "TRPDNo",
                        "Quantity 1",
                        "Quantity 2",

                        //21-25
                        "Quantity 3",
                        "Source",
                        "Batch#",
                        "Prop",
                        "Lot",

                        //26-27
                        "Bin",
                        "Heat Number"
                    },
                     new int[] 
                    {
                        //0
                        50,
 
                        //1-5
                        20, 100, 20, 250, 180,
                        
                        //6-10
                        100, 80, 100, 80, 100,
                        
                        //11-15
                        80, 200, 150, 180, 300,

                        //16-20
                        0, 200, 100, 100, 100, 

                        //21-25
                        100, 200, 200, 100, 100, 

                        //26-27
                        100, 200
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColButton(Grd1, new int[] { 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 6, 8, 10, 19, 20, 21 }, 0);
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 5, 8, 9, 10, 11, 13, 14, 16, 18, 20, 21, 22, 23, 24, 25, 26 }, false);
            Grd1.Cols[21].Move(10);
            Grd1.Cols[20].Move(8);
            Grd1.Cols[19].Move(6);
            if (mFrmParent.mIsItGrpCodeShow)
            {
                Grd1.Cols[13].Visible = true;
                Grd1.Cols[13].Move(5);
            }
            if (!mFrmParent.mIsBOMShowSpecifications) Sm.GrdColInvisible(Grd1, new int[] { 15 });
            Grd1.Cols[14].Move(2);
            Grd1.Cols[17].Move(6);
            if (mFrmParent.mIsDOWhs4HeatNumberEnabled)
                Grd1.Cols[27].Move(24);
            else
                Grd1.Cols[27].Visible = false;
            ShowInventoryUomCode();
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 5, 14, 22, 23, 24, 25, 26 }, !ChkHideInfoInGrd.Checked);
        }

        private void ShowInventoryUomCode()
        {
            if (mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 8, 9, 20 }, true);

            if (mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 8, 9, 10, 11, 21 }, true);
        }

        override protected void ShowData()
        {
            try
            {  
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ", Filter2 = " ";

                var cm = new MySqlCommand();

                if (mFrmParent.Grd1.Rows.Count >= 1)
                {
                    for (int r = 0; r<mFrmParent.Grd1.Rows.Count; r++)
                    {
                        if (Sm.GetGrdStr(mFrmParent.Grd1, r, 3).Length != 0)
                        {
                            Filter += (" And (Concat(C.Source, C.WhsCode) <> Concat(@Source" + r.ToString() + ", @WhsCode" + r.ToString() + ")) ");
                            Sm.CmParam<String>(ref cm, "@Source" + r.ToString(), Sm.GetGrdStr(mFrmParent.Grd1, r, 26));
                            Sm.CmParam<String>(ref cm, "@WhsCode" + r.ToString(), Sm.GetGrdStr(mFrmParent.Grd1, r, 19));
                        }
                    }
                }
                //if (Filter.Length>0) Filter2 = Filter;
                Sm.CmParam<String>(ref cm, "@TransferRequestProjectDocNo", mFrmParent.TxtTransferRequestProjectDocNo.Text);
                Sm.CmParam<String>(ref cm, "@BomDocNo", TxtBom.Text);
                Sm.CmParam<String>(ref cm, "@ProjectCode", mFrmParent.TxtProjectCode.Text);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueWhsCode), "C.WhsCode", true);
                Sm.FilterStr(ref Filter2, ref cm, TxtItCode.Text, new string[] { "T5.ItCode", "T5.ItName" });
                Sm.FilterStr(ref Filter2, ref cm, Sm.GetLue(LueItCtCode), "T5.ItCtCode", true);
                
                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, GetSQL(Filter, Filter2),
                    new string[] 
                    { 
                        //0
                        "ItCode",

                        //1-5
                        "ItName", "ItCtName", "StockQty", "InventoryUomCode", "StockQty2", 
                        
                        //6-10
                        "InventoryUomCode2", "StockQty3", "InventoryUomCode3", "CCtName", "ItGrpCode",

                        //11-15
                        "ItCodeInternal", "Specification", "WhsCode", "WhsName", "TRPDNo", 

                        //16-20
                        "Qty", "Qty2", "Qty3", "Source", "BatchNo", 
                        
                        //21-24
                        "PropCode", "Lot", "Bin", "HeatNumber"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 2);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 10);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 12);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 13);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 14);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 15);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 16);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 17);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 18);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 19);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 20);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 21);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 22);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 23);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 27, 24);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                mFrmParent.Grd1.BeginUpdate();
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsItCodeAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 3, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 4, Grd1, Row2, 4);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 5, Grd1, Row2, 6);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 7, Grd1, Row2, 7);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 8, Grd1, Row2, 8);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 10, Grd1, Row2, 9);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 11, Grd1, Row2, 10);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 13, Grd1, Row2, 11);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 15, Grd1, Row2, 12);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 16, Grd1, Row2, 13);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 17, Grd1, Row2, 14);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 18, Grd1, Row2, 15);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 19, Grd1, Row2, 16);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 20, Grd1, Row2, 17);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 25, Grd1, Row2, 18);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 6, Grd1, Row2, 19);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 9, Grd1, Row2, 20);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 12, Grd1, Row2, 21);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 26, Grd1, Row2, 22);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 27, Grd1, Row2, 23);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 28, Grd1, Row2, 24);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 29, Grd1, Row2, 25);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 30, Grd1, Row2, 26);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 36, Grd1, Row2, 27);
                        mFrmParent.Grd1.Cells[Row1, 32].Value = false;

                        mFrmParent.Grd1.Rows.Add();

                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 5, 6, 8, 9, 11, 12 });
                        Sm.SetGrdBoolValueFalse(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 32 });
                    }
                }
                mFrmParent.Grd1.EndUpdate();
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 item.");
        }

        private bool IsItCodeAlreadyChosen(int Row)
        {
            // ItCode, WhsCode, Source, BatchNo, PropCode, Lot, Bin
            string ItCode = string.Concat(
                Sm.GetGrdStr(Grd1, Row, 2),
                Sm.GetGrdStr(Grd1, Row, 16),
                Sm.GetGrdStr(Grd1, Row, 22),
                Sm.GetGrdStr(Grd1, Row, 23),
                Sm.GetGrdStr(Grd1, Row, 24),
                Sm.GetGrdStr(Grd1, Row, 25),
                Sm.GetGrdStr(Grd1, Row, 26)
                );

            for (int Index = 0; Index < mFrmParent.Grd1.Rows.Count - 1; Index++)
            {
                if (Sm.CompareStr(
                    string.Concat(
                    Sm.GetGrdStr(mFrmParent.Grd1, Index, 3), 
                    Sm.GetGrdStr(mFrmParent.Grd1, Index, 19), 
                    Sm.GetGrdStr(mFrmParent.Grd1, Index, 26), 
                    Sm.GetGrdStr(mFrmParent.Grd1, Index, 27), 
                    Sm.GetGrdStr(mFrmParent.Grd1, Index, 28), 
                    Sm.GetGrdStr(mFrmParent.Grd1, Index, 29), 
                    Sm.GetGrdStr(mFrmParent.Grd1, Index, 30)
                    ), 
                    ItCode))
                {
                    return true;
                }
            }
            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    Grd1.Cells[Row, 1].Value = !IsSelected;
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        private void SetNumberOfInventoryUomCode()
        {
            string NumberOfInventoryUomCode = Sm.GetParameter("NumberOfInventoryUomCode");
            if (NumberOfInventoryUomCode.Length == 0)
                mNumberOfInventoryUomCode = 1;
            else
                mNumberOfInventoryUomCode = int.Parse(NumberOfInventoryUomCode);
        }

        #endregion

        #region Event

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void ChkItCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Item's category");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LueItCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCtCode, new Sm.RefreshLue1(Sl.SetLueItCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }
        private void BtnBom_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmDOWhs4Dlg2(this));
        }

        private void TxtBom_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }
        private void ChkBom_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Bom");
        }
        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue1(Sl.SetLueWhsCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }
        private void ChkWhsCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Warehouse");
        }
        #endregion
    }
}
