﻿#region Update
/*
    [VIN/IMS] tambah kolom project code, project name, customer po#
    [WED/YK] bug alias kolom
    19/07/2022 [TYO/PRODUCT] Menampilkan status canceled saat status = C
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmProjectImplementationRevisionFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmProjectImplementationRevision mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmProjectImplementationRevisionFind(FrmProjectImplementationRevision FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                Sl.SetLueCtCode(ref LueCtCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T1.DocNo, T1.DocDt, ");
            SQL.AppendLine("T1.PRJIDocNo, ");
            SQL.AppendLine("Case T1.Status When 'O' Then 'Outstanding' When 'A' Then 'Approve' When 'C' Then 'Cancelled' End As Status, ");
            SQL.AppendLine("A.SAAddress, B.CtName, A.BOQDocNo, T.Amt, ");
            SQL.AppendLine("C.ItCode, D.ItCodeInternal, D.ItName, D.Specification, E.CtItCode, E.CtItName, C.DeliveryDt, ");
            SQL.AppendLine("T1.CreateBy, T1.CreateDt, T1.LastUpBy, T1.LastUpDt, ");
            SQL.AppendLine("IFNULL(I.ProjectName, G.ProjectName) ProjectName, ");
            SQL.AppendLine("IFNULL(I.ProjectCode, A.ProjectCode2) ProjectCode, IFNULL(A.PONo, H.DocNo) CustomerPO ");
            SQL.AppendLine("From TblProjectImplementationRevisionHdr T1 ");
            SQL.AppendLine("Inner Join TblSOContractRevisionHdr T On T1.SOCRDocNo = T.DocNo ");
            SQL.AppendLine("Inner Join TblSOContractHdr A On T.SOCDocNo = A.DocNo");
            SQL.AppendLine("Inner Join tblCustomer B on A.CtCode = B.CtCode ");
            SQL.AppendLine("Inner Join TblSOContractDtl C On A.DocNo=C.DocNo ");
            SQL.AppendLine("Inner Join TblItem D On C.ItCode=D.ItCode ");
            SQL.AppendLine("Left Join TblCustomerItem E On C.ItCode=E.ItCode And A.CtCode=E.CtCode ");
            SQL.AppendLine("Inner Join TblBOQHdr F On A.BOQDocNo=F.DocNo ");
            SQL.AppendLine("Inner Join TblLOPHdr G On F.LOPDocNo=G.DocNo ");
			SQL.AppendLine("left JOIN tblnoticetoproceed H ON G.DocNo=H.LOPDocNo ");
            SQL.AppendLine("LEFT JOIN tblprojectgroup I ON G.PGCode=I.PGCode ");
            if (mFrmParent.mIsFilterBySite)
            {
                SQL.AppendLine("    And (G.SiteCode Is Null Or ( ");
                SQL.AppendLine("    G.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(G.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("            ) ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    )) ");
            }
            SQL.AppendLine("Where (T1.DocDt Between @DocDt1 And @DocDt2) ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 26;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Document#", 
                    "Date",
                    "Status",
                    "Cancel",
                    "Project Implementation#",
                    
                    //6-10
                    "Customer",
                    "Bill Of Quantity",
                    "Shipping"+Environment.NewLine+"Address",
                    "Amount",
                    "Item's Code",

                    //11-15
                    "Local Code",
                    "Item's Name",
                    "Customer's"+Environment.NewLine+"Item Code",
                    "Customer's"+Environment.NewLine+"Item Name",
                    "Specification",

                    //16-20
                    "Delivery Date",
                    "Created"+Environment.NewLine+"By",
                    "Created"+Environment.NewLine+"Date", 
                    "Created"+Environment.NewLine+"Time", 
                    "Last"+Environment.NewLine+"Updated By",

                    //21-25
                    "Last"+Environment.NewLine+"Updated Date", 
                    "Last"+Environment.NewLine+"Updated Time",
                    "Project Name",
                    "Project Code",
                    "Customer PO#"

                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    180, 80, 80, 60, 130, 
                    
                    //6-10
                    200, 130, 200, 120, 100,  

                    //11-15
                    100, 250, 100, 200, 250, 

                    //16-20
                    100, 100, 100, 100, 100, 

                    //21-23
                    100, 100, 150, 150, 150
                }
            );
            Sm.GrdColCheck(Grd1, new int[] { 4 });
            Sm.GrdFormatDec(Grd1, new int[] { 9 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2, 16, 18, 21 });
            Sm.GrdFormatTime(Grd1, new int[] { 19, 22 });
            Sm.GrdColInvisible(Grd1, new int[] { 4, 10, 11, 17, 18, 19, 20, 21, 22 }, false);
            if (!mFrmParent.mIsCustomerItemNameMandatory)
                Sm.GrdColInvisible(Grd1, new int[] { 13, 14 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 10, 11, 17, 18, 19, 20, 21, 22 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "T1.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtProjectImplementationDocNo.Text, new string[] { "T1.PRJIDocNo", "A.LocalDocNo" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCode), "A.CtCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "C.ItCode", "D.ItName", "D.ItCodeInternal" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By T1.DocNo Desc; ",
                        new string[]
                        {

                            //0
                            "DocNo", 

                            //1-5
                            "DocDt", "Status", "PRJIDocNo", "CtName", "BOQDocNo", 
                            
                            //6-10
                            "SAAddress", "Amt", "ItCode", "ItCodeInternal", "ItName", 
                            
                            //11-15
                            "CtItCode", "CtItName", "Specification","DeliveryDt", "CreateBy", 

                            //16-20
                            "CreateDt", "LastUpBy", "LastUpDt", "ProjectName", "ProjectCode",

                            //21
                            "CustomerPO"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 13);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 16, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 15);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 18, 16);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 19, 16);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 17);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 21, 18);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 22, 18);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 19);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 20);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 21);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt1).Length == 0) DteDocDt1.EditValue = DteDocDt2.EditValue;
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void TxtProjectImplementationDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkProjectImplementationDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Project Implementation#");
        }

        #endregion 

    }
}
