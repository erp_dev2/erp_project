﻿#region Update
/*
    06/12/2020 [WED/IMS] new apps
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

using FastReport;
using FastReport.Data;
using System.IO;
using System.Net;
using System.Threading;

#endregion

namespace RunSystem
{
    public partial class FrmDOCt10 : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string
            mMenuCode = string.Empty, mAccessInd = string.Empty, mDocNo = string.Empty,
            mCtCode = string.Empty, mCityCode = string.Empty, mCntCode = string.Empty;
        internal FrmDOCt10Find FrmFind;
        private string mDocType = "31", mMainCurCode = string.Empty;
        private bool 
            mIsDOCtNeedShippingAddressInd = false,
            mIsAutoJournalActived = false,
            mIsAcNoForSaleUseItemCategory = false,
            mIsDOCtAmtRounded = false,
            mIsDOCt3NotUseJournal = false
            ;
        internal int mNumberOfInventoryUomCode = 1;
        internal bool 
            mIsBOMShowSpecifications = false,
            mIsDOCt3ShowProjectInfo = false,
            mIsInspectionSheetMandatory = false,
            mIsDOCt3AllowToUploadFile = false,
            mIsSalesTransactionShowSOContractRemark = false,
            mIsSalesTransactionUseItemNotes = false
            ;

        internal string
            mHostAddrForFTPClient = string.Empty,
            mPortForFTPClient = string.Empty,
            mUsernameForFTPClient = string.Empty,
            mPasswordForFTPClient = string.Empty,
            mSharedFolderForFTPClient = string.Empty,
            mFileSizeMaxUploadFTPClient = string.Empty;

        private byte[] downloadedData;

        #endregion

        #region Constructor

        public FrmDOCt10(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "DO To Customer (Replaced Items)";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                LblSAName.ForeColor = mIsDOCtNeedShippingAddressInd? Color.Red:Color.Black;
                if (mIsInspectionSheetMandatory) LblInspectionSheet.ForeColor = Color.Red;

                Sm.SetLookUpEdit(LueFontSize, new string[] { "6", "7", "8", "9", "10", "11", "12" });
                Sm.SetLue(LueFontSize, "9");
                base.FrmLoad(sender, e);
                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grid 1

            Grd1.Cols.Count = 29;
            Grd1.FrozenArea.ColCount = 4;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",
                        
                        //1-5
                        "Cancel",
                        "Old Cancel",
                        "",
                        "Item's Code",
                        "Local Code",
                        
                        //6-10
                        "Item's Name",
                        "Foreign Name",
                        "Property Code",
                        "Property",
                        "Batch#",

                        //11-15
                        "Source",
                        "Lot",
                        "Bin",
                        "Stock",
                        "Quantity",

                        //16-20
                        "UoM",
                        "Stock",
                        "Quantity",
                        "UoM",
                        "Stock",

                        //21-25
                        "Quantity",
                        "UoM",
                        "Remark",
                        "Notes",
                        "SO Contract#",

                        //26-28
                        "SO Contract D#",
                        "No",
                        "SO Contract Remark"
                    },
                     new int[] 
                    {
                        //0
                        0,
                        
                        //1-5
                        0, 50, 20, 100, 100, 
                        
                        //6-10
                        250, 180, 0, 0, 250, 
                        
                        //11-15
                        180, 60, 60, 80, 80, 
                        
                        //16-20
                        80, 80, 80, 80, 80, 
                        
                        //21-25
                        80, 80, 250, 250, 200,

                        //26-29
                        0, 60, 0
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1, 2 });
            Sm.GrdFormatDec(Grd1, new int[] { 14, 15, 17, 18, 20, 21 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 3 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 16, 17, 19, 20, 22, 25, 26, 27, 28 });
            Sm.GrdColInvisible(Grd1, new int[] { 0, 1, 2, 4, 7, 8, 9, 11, 12, 13, 17, 18, 19, 20, 21, 22, 28 }, false);
            if (!mIsSalesTransactionUseItemNotes) Sm.GrdColInvisible(Grd1, new int[] { 24 }, false);
            //if (!mIsBOMShowSpecifications) Sm.GrdColInvisible(Grd1, new int[] { 43 });
            Grd1.Cols[5].Move(7);
            Grd1.Cols[27].Move(4);
            Grd1.Cols[25].Move(4);

            #endregion

            #region Grid 2

            Grd2.Cols.Count = 18;
            Grd2.FrozenArea.ColCount = 3;
            Grd2.ReadOnly = false;

            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[] 
                    {
                        //0
                        "",
                        
                        //1-5
                        "Item's Code",
                        "Item's Name",
                        "Outstanding",
                        "Replaced",
                        "UoM",

                        //6-10
                        "Outstanding",
                        "Replaced",
                        "UoM",
                        "Outstanding",
                        "Replaced",

                        //11-15
                        "UoM",
                        "Local Code",
                        "Project Code",
                        "Project Name",
                        "Customer's PO#",

                        //16-17
                        "SO Contract" +Environment.NewLine+ "Remark",
                        "SO COntract No"
                    },
                     new int[] 
                    {
                        //0
                        20,
 
                        //1-5
                        100, 200, 100, 100, 80, 
                        
                        //6-10
                        100, 100, 80, 100, 100, 
                        
                        //11-15
                        80, 120, 130, 200, 130,

                        //16-17
                        200, 0
                    }
                );
            Sm.GrdColButton(Grd2, new int[] { 0 });
            Sm.GrdFormatDec(Grd2, new int[] { 3, 4, 6, 7, 9, 10 }, 0);
            Sm.GrdColInvisible(Grd2, new int[] { 1, 6, 7, 8, 9, 10, 11, 13, 14, 15, 17 }, false);
            Sm.GrdColReadOnly(true, true, Grd2, new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17 });
            Grd2.Cols[12].Move(3);
            if (mIsDOCt3ShowProjectInfo) Sm.GrdColInvisible(Grd2, new int[] { 13, 14, 15 }, true);
            if (!mIsSalesTransactionShowSOContractRemark) Sm.GrdColInvisible(Grd2, new int[] { 16 }, false);
            
            #endregion

            ShowInventoryUomCode();
        }

        private void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4, 7, 9, 11, 12, 13 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd2, new int[] { 1 }, !ChkHideInfoInGrd.Checked);
            ShowInventoryUomCode();
        }

        private void ShowInventoryUomCode()
        {
            if (mNumberOfInventoryUomCode == 2)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 17, 18, 19 }, true);
                Sm.GrdColInvisible(Grd2, new int[] { 6, 7, 8 }, true);
            }

            if (mNumberOfInventoryUomCode == 3)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 17, 18, 19, 20, 21, 22 }, true);
                Sm.GrdColInvisible(Grd2, new int[] { 6, 7, 8, 9, 10, 11 }, true);
            }
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, MeeCancelReason, ChkCancelInd, LueWhsCode, TxtSAName, 
                        TxtCtDONo, TxtCtReturnNo, TxtProductDeliveryNo, MeeRemark, TxtInspectionSheet,
                        TxtFile, TxtFile2, TxtFile3
                    }, true);
                    BtnRecvCtDocNo1.Enabled = false;
                    BtnSAName.Enabled = false;
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 15, 18, 21, 23, 24 });
                    Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0 });
                    Sm.GrdColInvisible(Grd1, new int[] { 14, 17, 20 }, false);
                    BtnFile.Enabled = false;
                    BtnDownload.Enabled = false;
                    BtnFile2.Enabled = false;
                    BtnDownload2.Enabled = false;
                    BtnFile3.Enabled = false;
                    BtnDownload3.Enabled = false;
                    if (TxtDocNo.Text.Length > 0 && mIsDOCt3AllowToUploadFile)
                        BtnDownload.Enabled = true;
                    ChkFile.Enabled = false;
                    if (TxtDocNo.Text.Length > 0 && mIsDOCt3AllowToUploadFile)
                        BtnDownload2.Enabled = true;
                    ChkFile2.Enabled = false;
                    if (TxtDocNo.Text.Length > 0 && mIsDOCt3AllowToUploadFile)
                        BtnDownload3.Enabled = true;
                    ChkFile3.Enabled = false;
                    TxtDocNo.Focus();
                    BtnCustomerShipAddress.Enabled = false;
                    BtnSAName.Enabled = false;
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueWhsCode, TxtCtDONo, TxtCtReturnNo, TxtProductDeliveryNo, 
                        MeeRemark , TxtInspectionSheet
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 15, 18, 21, 23, 24 });
                    Sm.GrdColReadOnly(false, true, Grd2, new int[] { 0 });
                    Sm.GrdColInvisible(Grd1, new int[] { 14 }, true);
                    if (mNumberOfInventoryUomCode == 2)
                        Sm.GrdColInvisible(Grd1, new int[] { 17 }, true);
                    if (mNumberOfInventoryUomCode == 3)
                        Sm.GrdColInvisible(Grd1, new int[] { 17, 20 }, true);
                    BtnRecvCtDocNo1.Enabled = true;
                    BtnCustomerShipAddress.Enabled = true;
                    BtnSAName.Enabled = true;
                    if (mIsDOCt3AllowToUploadFile)
                    {
                        BtnFile.Enabled = true;
                        BtnDownload.Enabled = true;
                        BtnFile2.Enabled = true;
                        BtnDownload2.Enabled = true;
                        BtnFile3.Enabled = true;
                        BtnDownload3.Enabled = true;
                    }
                    ChkFile.Enabled = true;
                    ChkFile2.Enabled = true;
                    ChkFile3.Enabled = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeCancelReason, ChkCancelInd }, false);
                    TxtDocNo.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            mCtCode = string.Empty;
            mCityCode = string.Empty;
            mCntCode = string.Empty;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, MeeCancelReason, TxtRecvCtDocNo, TxtCtCode,
                LueWhsCode, TxtSAName, MeeSAAddress, TxtCity, TxtCountry, 
                TxtPostalCd, TxtPhone, TxtFax, TxtEmail, TxtMobile,
                TxtCtDONo, TxtCtReturnNo, TxtProductDeliveryNo, MeeRemark, TxtJournalDocNo, 
                TxtJournalDocNo2, TxtInspectionSheet, TxtFile, TxtFile2, TxtFile3
            });
            ChkCancelInd.Checked = false;
            ClearGrd();
            ChkFile.Checked = false;
            PbUpload.Value = 0;
            ChkFile2.Checked = false;
            PbUpload2.Value = 0;
            ChkFile3.Checked = false;
            PbUpload3.Value = 0;
        }

        internal void ClearData2()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtSAName, MeeSAAddress, TxtCity, TxtCountry, TxtPostalCd, 
                TxtPhone, TxtFax, TxtEmail, TxtMobile
            });
        }

        private void ClearGrd()
        {
            ClearGrd1();
            ClearGrd2();
        }

        private void ClearGrd1()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdBoolValueFalse(ref Grd1, 0, new int[] { 1, 2 });
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 14, 15, 17, 18, 20, 21 });
        }

        private void ClearGrd2()
        {
            Sm.ClearGrd(Grd2, true);
            Sm.SetGrdNumValueZero(ref Grd2, 0, new int[] { 3, 4, 6, 7, 9, 10 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmDOCt10Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sl.SetLueWhsCode(ref LueWhsCode, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, string.Empty, false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }
        private void BtnPrint_Click(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false) || Sm.StdMsgYN("Print", string.Empty) == DialogResult.No) return;
            ParPrint(TxtDocNo.Text);
        }
        private void BtnFile_Click(object sender, EventArgs e)
        {
            try
            {
                ChkFile.Checked = true;
                OD.InitialDirectory = "c:";
                OD.Filter = "PDF files (*.pdf)|*.pdf|Compressed Files(*.rar;*.zip)|*.RAR;*.ZIP";
                OD.FilterIndex = 1;
                OD.ShowDialog();

                TxtFile.Text = OD.FileName;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnFile2_Click(object sender, EventArgs e)
        {
            try
            {
                ChkFile2.Checked = true;
                OD.InitialDirectory = "c:";
                OD.Filter = "PDF files (*.pdf)|*.pdf|Compressed Files(*.rar;*.zip)|*.RAR;*.ZIP";
                OD.FilterIndex = 1;
                OD.ShowDialog();

                TxtFile2.Text = OD.FileName;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnFile3_Click(object sender, EventArgs e)
        {
            try
            {
                ChkFile3.Checked = true;
                OD.InitialDirectory = "c:";
                OD.Filter = "PDF files (*.pdf)|*.pdf|Compressed Files(*.rar;*.zip)|*.RAR;*.ZIP";
                OD.FilterIndex = 1;
                OD.ShowDialog();

                TxtFile3.Text = OD.FileName;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnDownload_Click(object sender, EventArgs e)
        {
            DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, TxtFile.Text, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
            SFD.FileName = TxtFile.Text;
            SFD.DefaultExt = "pdf";
            SFD.AddExtension = true;

            if (!Sm.IsTxtEmpty(TxtFile, "File", false) && downloadedData != null && downloadedData.Length != 0)
            {

                if (SFD.ShowDialog() == DialogResult.OK)
                {
                    Application.DoEvents();

                    //Write the bytes to a file
                    FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                    newFile.Write(downloadedData, 0, downloadedData.Length);
                    newFile.Close();
                    MessageBox.Show("Saved Successfully");
                }
            }
            else
                MessageBox.Show("No File was Downloaded Yet!");
        }

        private void BtnDownload2_Click(object sender, EventArgs e)
        {
            DownloadFile2(mHostAddrForFTPClient, mPortForFTPClient, TxtFile2.Text, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
            SFD.FileName = TxtFile2.Text;
            SFD.DefaultExt = "pdf";
            SFD.AddExtension = true;

            if (!Sm.IsTxtEmpty(TxtFile2, "File", false) && downloadedData != null && downloadedData.Length != 0)
            {

                if (SFD.ShowDialog() == DialogResult.OK)
                {
                    Application.DoEvents();

                    //Write the bytes to a file
                    FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                    newFile.Write(downloadedData, 0, downloadedData.Length);
                    newFile.Close();
                    MessageBox.Show("Saved Successfully");
                }
            }
            else
                MessageBox.Show("No File was Downloaded Yet!");
        }

        private void BtnDownload3_Click(object sender, EventArgs e)
        {
            DownloadFile2(mHostAddrForFTPClient, mPortForFTPClient, TxtFile3.Text, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
            SFD.FileName = TxtFile3.Text;
            SFD.DefaultExt = "pdf";
            SFD.AddExtension = true;

            if (!Sm.IsTxtEmpty(TxtFile3, "File", false) && downloadedData != null && downloadedData.Length != 0)
            {

                if (SFD.ShowDialog() == DialogResult.OK)
                {
                    Application.DoEvents();

                    //Write the bytes to a file
                    FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                    newFile.Write(downloadedData, 0, downloadedData.Length);
                    newFile.Close();
                    MessageBox.Show("Saved Successfully");
                }
            }
            else
                MessageBox.Show("No File was Downloaded Yet!");
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", "", mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "DOCt3", "TblDOCt3Hdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveDOCt3Hdr(DocNo));

            for (int r = 0; r < Grd2.Rows.Count; r++)
                if (Sm.GetGrdStr(Grd2, r, 1).Length > 0) cml.Add(SaveDOCt3Dtl2(DocNo, r));

            for (int r = 0; r < Grd1.Rows.Count; r++)
                if (Sm.GetGrdStr(Grd1, r, 4).Length > 0) cml.Add(SaveDOCt3Dtl(DocNo, r));

            cml.Add(EditRecvCt());

            if (!mIsDOCt3NotUseJournal)
            {
                if (mIsAutoJournalActived) cml.Add(SaveJournal(DocNo));
            }

            Sm.ExecCommands(cml);

            if (mIsDOCt3AllowToUploadFile && TxtFile.Text.Length > 0 && TxtFile.Text != "openFileDialog1")
                UploadFile(DocNo);
            if (mIsDOCt3AllowToUploadFile && TxtFile2.Text.Length > 0 && TxtFile2.Text != "openFileDialog1")
                UploadFile2(DocNo);
            if (mIsDOCt3AllowToUploadFile && TxtFile3.Text.Length > 0 && TxtFile3.Text != "openFileDialog1")
                UploadFile3(DocNo);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            RecomputeOutstanding();
            RecomputeStock();
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsTxtEmpty(TxtRecvCtDocNo, "Received#", false) ||
                (mIsInspectionSheetMandatory && Sm.IsTxtEmpty(TxtInspectionSheet , "Inspection Sheet#", false))||
                (mIsDOCtNeedShippingAddressInd && Sm.IsTxtEmpty(TxtSAName, "Shipping address", false)) ||
                IsGrdEmpty() ||
                IsGrdExceedMaxRecords() ||
                IsGrdValueNotValid() ||
                IsGrdValueNotValid2() ||
                Sm.IsClosingJournalInvalid(Sm.GetDte(DteDocDt)) ||
                Sm.IsDocDtNotValid(
                    Sm.CompareStr(Sm.GetParameter("InventoryDocDtValidInd"), "Y"),
                    Sm.GetDte(DteDocDt)) 
                    ;

        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 item.");
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {

            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "DO data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            string Msg = string.Empty;
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, Row, 4, false, "Item is empty.")) return true;

                Msg =
                   "Item's Code : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine +
                   "Item's Name : " + Sm.GetGrdStr(Grd1, Row, 6) + Environment.NewLine +
                   "Property : " + Sm.GetGrdStr(Grd1, Row, 9) + Environment.NewLine +
                   "Batch# : " + Sm.GetGrdStr(Grd1, Row, 10) + Environment.NewLine +
                   "Source : " + Sm.GetGrdStr(Grd1, Row, 11) + Environment.NewLine +
                   "Lot : " + Sm.GetGrdStr(Grd1, Row, 12) + Environment.NewLine +
                   "Bin : " + Sm.GetGrdStr(Grd1, Row, 13) + Environment.NewLine + Environment.NewLine;

                if (Sm.GetGrdDec(Grd1, Row, 15) <= 0m)
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Quantity should be greater than 0.");
                    return true;
                }

                if (Sm.GetGrdDec(Grd1, Row, 14) < Sm.GetGrdDec(Grd1, Row, 15))
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Stock should not be less than DO's quantity.");
                    return true;
                }

                if (Grd1.Cols[18].Visible)
                {
                    if (Sm.GetGrdDec(Grd1, Row, 18) <= 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Quantity (2) should be greater than 0.");
                        return true;
                    }

                    if (Sm.GetGrdDec(Grd1, Row, 17) < Sm.GetGrdDec(Grd1, Row, 18))
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Stock (2) should not be less than DO's quantity (2).");
                        return true;
                    }
                }

                if (Grd1.Cols[21].Visible)
                {
                    if (Sm.GetGrdDec(Grd1, Row, 21) <= 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Quantity (3) should be greater than 0.");
                        return true;
                    }
                    if (Sm.GetGrdDec(Grd1, Row, 20) < Sm.GetGrdDec(Grd1, Row, 21))
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Stock (3) should not be less than DO's quantity (3).");
                        return true;
                    }
                }
            }
            return false;
        }

        private bool IsGrdValueNotValid2()
        {
            for (int Row = 0; Row < Grd2.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd2, Row, 1).Length > 0)
                { 
                    //if (IsItemAlreadyProcessed(Row))
                    //{
                    //    Sm.StdMsg(mMsgType.Warning, 
                    //        "Item's Code : " + Sm.GetGrdStr(Grd2, Row, 1) + Environment.NewLine +
                    //        "Item's Name : " + Sm.GetGrdStr(Grd2, Row, 2) + Environment.NewLine + Environment.NewLine +
                    //        "This item already processed to DO."
                    //    );
                    //    return true;    
                    //}
                    //if (IsItemAlreadyCancelled(Row))
                    //{
                    //    Sm.StdMsg(mMsgType.Warning,
                    //        "Item's Code : " + Sm.GetGrdStr(Grd2, Row, 1) + Environment.NewLine +
                    //        "Item's Name : " + Sm.GetGrdStr(Grd2, Row, 2) + Environment.NewLine + Environment.NewLine +
                    //        "This item already cancelled."
                    //    );
                    //    return true;
                    //}

                    if (Sm.GetGrdDec(Grd2, Row, 4) > Sm.GetGrdDec(Grd2, Row, 3))
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "Item's Code : " + Sm.GetGrdStr(Grd2, Row, 1) + Environment.NewLine +
                            "Item's Name : " + Sm.GetGrdStr(Grd2, Row, 2) + Environment.NewLine +
                            "Received Quantity : " + Sm.FormatNum(Sm.GetGrdDec(Grd2, Row, 3), 0) + Environment.NewLine +
                            "Replaced Quantity : " + Sm.FormatNum(Sm.GetGrdDec(Grd2, Row, 4), 0) + Environment.NewLine + 
                            "Replaced quantity should not bigger than received quantity."
                            );
                        return true;
                    }

                    if (Grd2.Cols[7].Visible)
                    {
                        if (Sm.GetGrdDec(Grd2, Row, 7) > Sm.GetGrdDec(Grd2, Row, 6))
                        {
                            Sm.StdMsg(mMsgType.Warning,
                                "Item's Code : " + Sm.GetGrdStr(Grd2, Row, 1) + Environment.NewLine +
                                "Item's Name : " + Sm.GetGrdStr(Grd2, Row, 2) + Environment.NewLine +
                                "Received Quantity : " + Sm.FormatNum(Sm.GetGrdDec(Grd2, Row, 6), 0) + Environment.NewLine +
                                "Replaced Quantity : " + Sm.FormatNum(Sm.GetGrdDec(Grd2, Row, 7), 0) + Environment.NewLine +
                                "Replaced quantity should not bigger than received quantity."
                                );
                            return true;
                        }
                    }

                    if (Grd2.Cols[10].Visible)
                    {
                        if (Sm.GetGrdDec(Grd2, Row, 10) > Sm.GetGrdDec(Grd2, Row, 9))
                        {
                            Sm.StdMsg(mMsgType.Warning,
                                "Item's Code : " + Sm.GetGrdStr(Grd2, Row, 1) + Environment.NewLine +
                                "Item's Name : " + Sm.GetGrdStr(Grd2, Row, 2) + Environment.NewLine +
                                "Received Quantity : " + Sm.FormatNum(Sm.GetGrdDec(Grd2, Row, 9), 0) + Environment.NewLine +
                                "Replaced Quantity : " + Sm.FormatNum(Sm.GetGrdDec(Grd2, Row, 10), 0) + Environment.NewLine +
                                "Replaced quantity should not bigger than received quantity."
                                );
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        //private bool IsItemAlreadyProcessed(int r)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Select DocNo From TblRecvCtDtl2 ");
        //    SQL.AppendLine("Where DocNo=@DocNo ");
        //    SQL.AppendLine("And ItCode=@ItCode ");
        //    SQL.AppendLine("And DOCt3DocNo Is Not Null; ");

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };

        //    Sm.CmParam<String>(ref cm, "@DocNo", TxtRecvCtDocNo.Text);
        //    Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd2, r, 1));

        //    return Sm.IsDataExist(cm);
        //}

        //private bool IsItemAlreadyCancelled(int r)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Select DocNo From TblRecvCtDtl2 ");
        //    SQL.AppendLine("Where DocNo=@DocNo ");
        //    SQL.AppendLine("And ItCode=@ItCode; ");

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };

        //    Sm.CmParam<String>(ref cm, "@DocNo", TxtRecvCtDocNo.Text);
        //    Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd2, r, 1));

        //    return Sm.IsDataExist(cm)?false:true;
        //}

        private MySqlCommand SaveDOCt3Hdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblDOCt3Hdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelReason, CancelInd, WhsCode, RecvCtDocNo, CtCode, ");
            SQL.AppendLine("SAName, SAAddress, SACityCode, SACntCode, SAPostalCd, SAPhone, SAFax, SAEmail, SAMobile, ");
            SQL.AppendLine("CtDONo, CtReturnNo, ProductDeliveryNo, InspectionSheetNo, JournalDocNo, JournalDocNo2, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, Null, 'N', @WhsCode, @RecvCtDocNo, @CtCode, ");
            SQL.AppendLine("@SAName, @SAAddress, @SACityCode, @SACntCode, @SAPostalCd, @SAPhone, @SAFax, @SAEmail, @SAMobile, ");
            SQL.AppendLine("@CtDONo, @CtReturnNo, @ProductDeliveryNo, @InspectionSheetNo, Null, Null, @Remark, @UserCode, CurrentDateTime());");
            
            var cm = new MySqlCommand(){ CommandText = SQL.ToString() };
            
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            Sm.CmParam<String>(ref cm, "@RecvCtDocNo", TxtRecvCtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CtCode", mCtCode);
            Sm.CmParam<String>(ref cm, "@SAName", TxtSAName.Text);
            Sm.CmParam<String>(ref cm, "@SAAddress", MeeSAAddress.Text);
            Sm.CmParam<String>(ref cm, "@SACityCode", mCityCode);
            Sm.CmParam<String>(ref cm, "@SACntCode", mCntCode);
            Sm.CmParam<String>(ref cm, "@SAPostalCd", TxtPostalCd.Text);
            Sm.CmParam<String>(ref cm, "@SAPhone", TxtPhone.Text);
            Sm.CmParam<String>(ref cm, "@SAFax", TxtFax.Text);
            Sm.CmParam<String>(ref cm, "@SAEmail", TxtEmail.Text);
            Sm.CmParam<String>(ref cm, "@SAMobile", TxtMobile.Text);
            Sm.CmParam<String>(ref cm, "@CtDONo", TxtCtDONo.Text);
            Sm.CmParam<String>(ref cm, "@CtReturnNo", TxtCtReturnNo.Text);
            Sm.CmParam<String>(ref cm, "@ProductDeliveryNo", TxtProductDeliveryNo.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@InspectionSheetNo", TxtInspectionSheet.Text);


            return cm;
        }

        private MySqlCommand SaveDOCt3Dtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblDOCt3Dtl ");
            SQL.AppendLine("(DocNo, DNo, CancelInd, ItCode, PropCode, BatchNo, Source, Lot, Bin, Qty, Qty2, Qty3, SOContractDocNo, SOContractDNo, Remark, Notes, CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@DocNo, @DNo, 'N', @ItCode, @PropCode, @BatchNo, @Source, @Lot, @Bin, @Qty, @Qty2, @Qty3, @SOContractDocNo, @SOContractDNo, @Remark, @Notes, @UserCode, CurrentDateTime());");

            SQL.AppendLine("Insert Into TblStockMovement ");
            SQL.AppendLine("(DocType, DocNo, DNo, CancelInd, DocDt, WhsCode, Lot, Bin, ");
            SQL.AppendLine("ItCode, PropCode, BatchNo, Source, Qty, Qty2, Qty3, ");
            SQL.AppendLine("Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocType, @DocNo, @DNo, 'N', @DocDt, @WhsCode, @Lot, @Bin, ");
            SQL.AppendLine("@ItCode, @PropCode, @BatchNo, @Source, -1*@Qty, -1*@Qty2, -1*@Qty3, ");
            SQL.AppendLine("@MovementRemark, @UserCode, CurrentDateTime());");

            SQL.AppendLine("Update TblStockSummary Set ");
            SQL.AppendLine("    Qty=Qty-@Qty, ");
            SQL.AppendLine("    Qty2=Qty2-@Qty2, ");
            SQL.AppendLine("    Qty3=Qty3-@Qty3, ");
            SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where Source=@Source ");
            SQL.AppendLine("And Lot=@Lot ");
            SQL.AppendLine("And Bin=@Bin ");
            SQL.AppendLine("And WhsCode=@WhsCode; ");

            var cm = new MySqlCommand(){ CommandText =SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 4));
            Sm.CmParam<String>(ref cm, "@PropCode", Sm.GetGrdStr(Grd1, Row, 8));
            Sm.CmParam<String>(ref cm, "@BatchNo", Sm.GetGrdStr(Grd1, Row, 10));
            Sm.CmParam<String>(ref cm, "@Source", Sm.GetGrdStr(Grd1, Row, 11));
            Sm.CmParam<String>(ref cm, "@Lot", Sm.GetGrdStr(Grd1, Row, 12));
            Sm.CmParam<String>(ref cm, "@Bin", Sm.GetGrdStr(Grd1, Row, 13));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 15));
            Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd1, Row, 18));
            Sm.CmParam<Decimal>(ref cm, "@Qty3", Sm.GetGrdDec(Grd1, Row, 21));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 23));
            Sm.CmParam<String>(ref cm, "@Notes", Sm.GetGrdStr(Grd1, Row, 24));
            Sm.CmParam<String>(ref cm, "@SOContractDocNo", Sm.GetGrdStr(Grd1, Row, 25));
            Sm.CmParam<String>(ref cm, "@SOContractDNo", Sm.GetGrdStr(Grd1, Row, 26));

            var RemarkH = MeeRemark.Text;
            var RemarkD = Sm.GetGrdStr(Grd1, Row, 23);
            if (RemarkH.Length > 0)
            {
                if (RemarkD.Length > 0)
                    Sm.CmParam<String>(ref cm, "@MovementRemark", string.Concat(RemarkH, " ( ", RemarkD, " )"));
                else
                    Sm.CmParam<String>(ref cm, "@MovementRemark", RemarkH);
            }
            else
            { 
                if (RemarkD.Length > 0)
                    Sm.CmParam<String>(ref cm, "@MovementRemark", RemarkD);
                else
                    Sm.CmParam<String>(ref cm, "@MovementRemark", string.Empty);
            }

            Sm.CmParam<String>(ref cm, "@DocType", mDocType);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveDOCt3Dtl2(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblDOCt3Dtl2 ");
            SQL.AppendLine("(DocNo, DNo, ItCode, RecvCtQty, RecvCtQty2, RecvCtQty3, DOCt3Qty, DOCt3Qty2, DOCt3Qty3, CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@DocNo, @DNo, @ItCode, @RecvCtQty, @RecvCtQty2, @RecvCtQty3, @DOCt3Qty, @DOCt3Qty2, @DOCt3Qty3, @UserCode, CurrentDateTime());");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@RecvCtDocNo", TxtRecvCtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd2, Row, 1));
            Sm.CmParam<Decimal>(ref cm, "@RecvCtQty", Sm.GetGrdDec(Grd2, Row, 3));
            Sm.CmParam<Decimal>(ref cm, "@RecvCtQty2", Sm.GetGrdDec(Grd2, Row, 6));
            Sm.CmParam<Decimal>(ref cm, "@RecvCtQty3", Sm.GetGrdDec(Grd2, Row, 9));
            Sm.CmParam<Decimal>(ref cm, "@DOCt3Qty", Sm.GetGrdDec(Grd2, Row, 4));
            Sm.CmParam<Decimal>(ref cm, "@DOCt3Qty2", Sm.GetGrdDec(Grd2, Row, 7));
            Sm.CmParam<Decimal>(ref cm, "@DOCt3Qty3", Sm.GetGrdDec(Grd2, Row, 10));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand EditRecvCt()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblRecvCtDtl2 A ");
            SQL.AppendLine("Left Join (");
            SQL.AppendLine("    Select T.ItCode, Sum(T.DOCt3Qty) Qty, Sum(T.DOCt3Qty2) Qty2, Sum(T.DOCt3Qty3) Qty3 ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("        Select T2.ItCode, DOCt3Qty, DOCt3Qty2, DOCt3Qty3 ");
            SQL.AppendLine("        From TblDOCt3Hdr T1 ");
            SQL.AppendLine("        Inner Join TblDOCt3Dtl2 T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("        Where T1.RecvCtDocNo=@DocNo ");
            SQL.AppendLine("        And T1.CancelInd='N' ");
            SQL.AppendLine("    ) T Group By T.ItCode Having Sum(T.DOCt3Qty)<>0.00 ");
            SQL.AppendLine(") B On A.ItCode=B.ItCode ");
            SQL.AppendLine("Set ");
            SQL.AppendLine("    A.DOCt3Qty=IfNull(B.Qty, 0.00), ");
            SQL.AppendLine("    A.DOCt3Qty2=IfNull(B.Qty2, 0.00), ");
            SQL.AppendLine("    A.DOCt3Qty3=IfNull(B.Qty3, 0.00) ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            SQL.AppendLine("Update TblRecvCtHdr Set DOCt3Ind='O' Where DocNo=@DocNo And ReplaceInd='Y'; ");

            SQL.AppendLine("Update TblRecvCtHdr Set DOCt3Ind='F'");
            SQL.AppendLine("Where DocNo=@DocNo And ReplaceInd='Y' ");
            SQL.AppendLine("And Not Exists(Select 1 From TblRecvCtDtl2 Where DocNo=@DocNo And RecvCtQty<>DOCt3Qty Limit 1);");

            SQL.AppendLine("Update TblRecvCtHdr Set DOCt3Ind='P'");
            SQL.AppendLine("Where DocNo=@DocNo And ReplaceInd='Y' ");
            SQL.AppendLine("And Exists(Select 1 From TblRecvCtDtl2 Where DocNo=@DocNo And RecvCtQty<>0.00 And RecvCtQty<>DOCt3Qty Limit 1);");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtRecvCtDocNo.Text);

            return cm;
        }

        private MySqlCommand SaveJournal(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblDOCt3Hdr Set ");
            SQL.AppendLine("    JournalDocNo=");
            SQL.AppendLine(Sm.GetNewJournalDocNo(Sm.GetDte(DteDocDt), 1));
            SQL.AppendLine(" Where DocNo=@DocNo; ");

            SQL.AppendLine("Insert Into TblJournalHdr ");
            SQL.AppendLine("(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.JournalDocNo, ");
            SQL.AppendLine("A.DocDt, ");
            SQL.AppendLine("Concat('DO To Customer (Replaced Item) : ', A.DocNo) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, ");
            SQL.AppendLine("(Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, ");
            SQL.AppendLine("B.CCCode, A.Remark, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblDOCt3Hdr A ");
            SQL.AppendLine("Left Join TblWarehouse B On A.WhsCode=B.WhsCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, @EntCode, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From (");
            SQL.AppendLine("        Select D.ParValue As AcNo, C.ExcRate*C.UPrice*B.Qty As DAmt, 0.00 As CAmt ");
            SQL.AppendLine("        From TblDOCt3Hdr A ");
            SQL.AppendLine("        Inner join TblDOCt3Dtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
            SQL.AppendLine("        Inner Join TblParameter D On D.ParCode='AcNoForCOGS' And D.ParValue Is Not Null ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select E.AcNo As AcNo, 0.00 As DAmt, C.ExcRate*C.UPrice*B.Qty As CAmt ");
            SQL.AppendLine("        From TblDOCt3Hdr A ");
            SQL.AppendLine("        Inner join TblDOCt3Dtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
            SQL.AppendLine("        Inner join TblItem D On B.ItCode=D.ItCode ");
            SQL.AppendLine("        Inner join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo Is Not Null ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");
            SQL.AppendLine("    ) Tbl ");
            SQL.AppendLine("    Where AcNo Is Not Null ");
            SQL.AppendLine("    Group By AcNo ");
            SQL.AppendLine(") B On 1=1 ");
            SQL.AppendLine("Where A.DocNo In ( ");
            SQL.AppendLine("    Select JournalDocNo ");
            SQL.AppendLine("    From TblDOCt3Hdr ");
            SQL.AppendLine("    Where DocNo=@DocNo ");
            SQL.AppendLine("    And JournalDocNo Is Not Null ");
            SQL.AppendLine("    ); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetEntityWarehouse(Sm.GetLue(LueWhsCode)));

            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty) == DialogResult.No || IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();
            var IsCancel = ChkCancelInd.Checked;
            cml.Add(EditDOCt3Hdr());

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                Grd1.Cells[r, 1].Value = IsCancel;
                if (Sm.GetGrdStr(Grd1, r, 4).Length > 0 && !Sm.GetGrdBool(Grd1, r, 2) && Sm.GetGrdBool(Grd1, r, 1))
                    cml.Add(EditDOCt3Dtl(r));
            }
            
            cml.Add(EditRecvCt());
            if (!mIsDOCt3NotUseJournal)
            {
                if (mIsAutoJournalActived) cml.Add(SaveJournal());
            }

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                Sm.IsMeeEmpty(MeeCancelReason, "Cancellation reason") ||
                IsDataAlreadyCancelled();
        }

        private bool IsDataAlreadyCancelled()
        {
            return Sm.IsDataExist("Select 1 From TblDOCt3Hdr Where CancelInd='Y' And DocNo=@Param;", TxtDocNo.Text);
        }

        private MySqlCommand EditDOCt3Hdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblDOCt3Hdr Set CancelReason=@CancelReason, CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N'; ");

            SQL.AppendLine("Update TblDOCt3Dtl Set CancelInd='Y' Where DocNo=@DocNo; ");

            SQL.AppendLine("Update TblDOCt3Dtl2 Set ");
            SQL.AppendLine("    DOCt3Qty=0.00, ");
            SQL.AppendLine("    DOCt3Qty2=0.00, ");
            SQL.AppendLine("    DOCt3Qty3=0.00 ");
            SQL.AppendLine("Where DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand EditDOCt3Dtl(int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblStockMovement ");
            SQL.AppendLine("(DocType, DocNo, DNo, CancelInd, DocDt, WhsCode, Lot, Bin, ");
            SQL.AppendLine("ItCode, PropCode, BatchNo, Source, Qty, Qty2, Qty3, ");
            SQL.AppendLine("Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocType, @DocNo, @DNo, 'Y', @DocDt, @WhsCode, @Lot, @Bin, ");
            SQL.AppendLine("@ItCode, @PropCode, @BatchNo, @Source, @Qty, @Qty2, @Qty3, ");
            SQL.AppendLine("@Remark, @UserCode, CurrentDateTime());");

            SQL.AppendLine("Update TblStockSummary Set ");
            SQL.AppendLine("    Qty=Qty+@Qty, ");
            SQL.AppendLine("    Qty2=Qty2+@Qty2, ");
            SQL.AppendLine("    Qty3=Qty3+@Qty3, ");
            SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where Source=@Source ");
            SQL.AppendLine("And Lot=@Lot ");
            SQL.AppendLine("And Bin=@Bin ");
            SQL.AppendLine("And WhsCode=@WhsCode;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.GetGrdStr(Grd1, Row, 0));
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 4));
            Sm.CmParam<String>(ref cm, "@PropCode", Sm.GetGrdStr(Grd1, Row, 8));
            Sm.CmParam<String>(ref cm, "@BatchNo", Sm.GetGrdStr(Grd1, Row, 10));
            Sm.CmParam<String>(ref cm, "@Source", Sm.GetGrdStr(Grd1, Row, 11));
            Sm.CmParam<String>(ref cm, "@Lot", Sm.GetGrdStr(Grd1, Row, 12));
            Sm.CmParam<String>(ref cm, "@Bin", Sm.GetGrdStr(Grd1, Row, 13));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 15));
            Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd1, Row, 18));
            Sm.CmParam<Decimal>(ref cm, "@Qty3", Sm.GetGrdDec(Grd1, Row, 21));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 23));
            Sm.CmParam<String>(ref cm, "@DocType", mDocType);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal()
        {
            var SQL = new StringBuilder();
            var CurrentDt = Sm.ServerCurrentDate();
            var DocDt = Sm.GetDte(DteDocDt);
            var IsClosingJournalUseCurrentDt = Sm.IsClosingJournalUseCurrentDt(DocDt);

            SQL.AppendLine("Update TblDOCt3Hdr Set JournalDocNo2=@JournalDocNo ");
            SQL.AppendLine("Where DocNo=@DocNo And JournalDocNo Is Not Null; ");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, ");
            if (IsClosingJournalUseCurrentDt)
                SQL.AppendLine("Replace(CurDate(), '-', ''), ");
            else
                SQL.AppendLine("DocDt, ");
            SQL.AppendLine("Concat('Cancelling ', JnDesc) As JnDesc, ");
            SQL.AppendLine("MenuCode, MenuDesc, Remark, CreateBy, CreateDt ");
            SQL.AppendLine("From TblJournalHdr ");
            SQL.AppendLine("Where DocNo In (Select JournalDocNo From TblDOCt3Hdr Where DocNo=@DocNo And JournalDocNo Is Not Null); ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, DNo, AcNo, CAMt As DAmt, DAmt As CAmt, EntCode, CreateBy, CreateDt ");
            SQL.AppendLine("From TblJournalDtl ");
            SQL.AppendLine("Where DocNo In (Select JournalDocNo From TblDOCt3Hdr Where DocNo=@DocNo And JournalDocNo Is Not Null); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (IsClosingJournalUseCurrentDt)
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(CurrentDt, 1));
            else
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(DocDt, 1));

            return cm;
        }


        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowDOCt3Hdr(DocNo);
                ShowDOCt3Dtl(DocNo);
                ShowDOCt3Dtl2(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowDOCt3Hdr(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelReason, A.CancelInd, A.WhsCode, A.RecvCtDocNo, A.CtCode, D.CtName, ");
            SQL.AppendLine("A.SAName, A.SAAddress, A.SACityCode, B.CityName, A.SACntCode, C.CntName, A.SAPostalCd, ");
            SQL.AppendLine("A.SAPhone, A.SAFax, A.SAEmail, A.SAMobile, A.CtDONo, A.CtReturnNo, A.ProductDeliveryNo, A.InspectionSheetNo, A.Remark, A.JournalDocNo, A.JournalDocNo2, ");
            SQL.AppendLine("A.FileName, A.FileName2, A.FileName3");
            SQL.AppendLine("From TblDOCt3Hdr A  ");
            SQL.AppendLine("Left Join TblCity B On A.SACityCode = B.CityCode ");
            SQL.AppendLine("Left Join TblCountry C On A.SACntCode = C.CntCode ");
            SQL.AppendLine("Inner Join TblCustomer D On A.CtCode = D.CtCode ");
            SQL.AppendLine("Where DocNo=@DocNo;");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "CancelReason", "CancelInd", "WhsCode", "RecvCtDocNo", 
                        
                        //6-10
                        "CtCode", "CtName", "SAName",  "SAAddress", "SACityCode", 
                        
                        //11-15
                        "CityName", "SACntCode", "CntName", "SAPostalCd", "SAPhone", 
                        
                        //16-20
                        "SAFax", "SAEmail", "SAMobile", "CtDONo", "CtReturnNo", 
                        
                        //21-25
                        "ProductDeliveryNo", "Remark", "JournalDocNo", "JournalDocNo2", "InspectionSheetNo",

                        //26-28
                        "FileName", "FileName2", "FileName3"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        MeeCancelReason.EditValue = Sm.DrStr(dr, c[2]);
                        ChkCancelInd.Checked = Sm.DrStr(dr, c[3]) == "Y";
                        Sl.SetLueWhsCode(ref LueWhsCode, Sm.DrStr(dr, c[4]));
                        TxtRecvCtDocNo.EditValue = Sm.DrStr(dr, c[5]);
                        mCtCode = Sm.DrStr(dr, c[6]);
                        TxtCtCode.EditValue = Sm.DrStr(dr, c[7]);
                        TxtSAName.EditValue = Sm.DrStr(dr, c[8]);
                        MeeSAAddress.EditValue = Sm.DrStr(dr, c[9]);
                        mCityCode = Sm.DrStr(dr, c[10]);
                        TxtCity.EditValue = Sm.DrStr(dr, c[11]);
                        mCntCode = Sm.DrStr(dr, c[12]);
                        TxtCountry.EditValue = Sm.DrStr(dr, c[13]);
                        TxtPostalCd.EditValue = Sm.DrStr(dr, c[14]);
                        TxtPhone.EditValue = Sm.DrStr(dr, c[15]);
                        TxtFax.EditValue = Sm.DrStr(dr, c[16]);
                        TxtEmail.EditValue = Sm.DrStr(dr, c[17]);
                        TxtMobile.EditValue = Sm.DrStr(dr, c[18]);
                        TxtCtDONo.EditValue = Sm.DrStr(dr, c[19]);
                        TxtCtReturnNo.EditValue = Sm.DrStr(dr, c[20]);
                        TxtProductDeliveryNo.EditValue = Sm.DrStr(dr, c[21]);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[22]);
                        TxtJournalDocNo.EditValue = Sm.DrStr(dr, c[23]);
                        TxtJournalDocNo2.EditValue = Sm.DrStr(dr, c[24]);
                        TxtInspectionSheet.EditValue = Sm.DrStr(dr, c[25]);
                        TxtFile.EditValue = Sm.DrStr(dr, c[26]);
                        TxtFile2.EditValue = Sm.DrStr(dr, c[27]);
                        TxtFile3.EditValue = Sm.DrStr(dr, c[28]);

                    }, true
                );
        }

        private void ShowDOCt3Dtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.CancelInd, A.ItCode, B.ItCodeInternal, B.ItName, B.ForeignName, ");
            SQL.AppendLine("A.PropCode, C.PropName, A.BatchNo, A.Source, A.Lot, A.Bin, ");
            SQL.AppendLine("A.Qty, A.Qty2, A.Qty3, ");
            SQL.AppendLine("B.InventoryUomCode, B.InventoryUomCode2, B.InventoryUomCode3, A.Remark, A.Notes, ");
            SQL.AppendLine("A.SOContractDocNo, A.SOContractDNo, D.No, D.Remark As SOContractRemark ");
            SQL.AppendLine("From TblDOCt3Dtl A ");
            SQL.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
            SQL.AppendLine("Left Join TblProperty C On A.PropCode=C.PropCode ");
            SQL.AppendLine("Left Join TblSOContractDtl D On A.SOContractDocNo = D.DocNo And A.SOContractDNo = D.DNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 

                    //1-5
                    "CancelInd", "ItCode", "ItCodeInternal", "ItName", "ForeignName", 
                    
                    //6-10
                    "PropCode", "PropName", "BatchNo", "Source", "Lot", 
                    
                    //11-15
                    "Bin", "Qty", "InventoryUomCode", "Qty2", "InventoryUomCode2", 
                    
                    //16-20
                    "Qty3", "InventoryUomCode3", "Remark", "Notes", "SOContractDocNo",

                    //21-23
                    "SOContractDNo", "No", "SOContractRemark"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 10);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 12);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 13);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 14);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 15);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 16);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 17);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 18);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 19);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 20);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 21);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 27, 22);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 28, 23);
                    Sm.SetGrdNumValueZero(ref Grd1, Row, new int[] { 14, 17, 20 });
                }, false, false, false, false
            );
            Grd1.Rows.Add();
            Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 1, 2 });
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 14, 15, 17, 18, 20, 21 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ShowDOCt3Dtl2(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.ItCode, B.ItName, ");
            SQL.AppendLine("A.RecvCtQty, A.DOCt3Qty, B.InventoryUomCode, ");
            SQL.AppendLine("A.RecvCtQty2, A.DOCt3Qty2, B.InventoryUomCode2, ");
            SQL.AppendLine("A.RecvCtQty3, A.DOCt3Qty3, B.InventoryUomCode3, B.ItCodeInternal, ");
            if (mIsSalesTransactionShowSOContractRemark)
                SQL.AppendLine("D.SOCRemark, ");
            else
                SQL.AppendLine("Null As SOCRemark, ");
            if (mIsDOCt3ShowProjectInfo)
                SQL.AppendLine("D.ProjectCode, D.ProjectName, D.PONo, D.SOCNo ");
            else
                SQL.AppendLine("Null ProjectCode, Null As ProjectName, Null As PONo, Null As SOCNo ");
            SQL.AppendLine("From TblDOCt3Dtl2 A ");
            SQL.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
            if (mIsDOCt3ShowProjectInfo)
            {
                #region Old Code by wed
                //SQL.AppendLine("Inner Join TblDOCt3Hdr C On A.DocNo=C.DocNo ");
                //SQL.AppendLine("Left Join ( ");
                //SQL.AppendLine("    Select A.DocNo, B.ItCode, ");
                //SQL.AppendLine("    Group_Concat(Distinct IfNull(D.ProjectCode, '-')) ProjectCode, ");
                //SQL.AppendLine("    Group_Concat(Distinct IfNull(D.ProjectName, '-')) ProjectName, ");
                //SQL.AppendLine("    Group_Concat(Distinct IfNull(D.PONo, '-')) PONo, ");
                //SQL.AppendLine("    Group_Concat(Distinct IfNull(D.SOCRemark, '-')) SOCRemark, ");
                //SQL.AppendLine("    Group_Concat(Distinct IfNull(D.SOCNo, '')) SOCNo ");
                //SQL.AppendLine("    From TblRecvCtDtl A "); ;
                //SQL.AppendLine("    Left Join TblDOCt2Dtl B On A.DOCtDocNo=B.DocNo And A.DOCtDNo=B.DNo ");
                //SQL.AppendLine("    Left Join TblDOCt2Hdr C On A.DOCtDocNo=C.DocNo ");
                //SQL.AppendLine("    Left Join ( ");
                //SQL.AppendLine("        Select X1.DocNo, X5.ProjectCode, X5.ProjectName, X2.PONo, group_concat(distinct X.Remark separator '#' ) As SOCRemark, ");
                //SQL.AppendLine("        Group_Concat(distinct X.No separator ',' ) As SOCNo ");
                //SQL.AppendLine("        From TblDRDtl X1 ");
                //SQL.AppendLine("        Inner Join TblSOContractHdr X2 On X1.SODocNo = X2.DocNo ");
                //SQL.AppendLine("        INNER JOIN tblsocontractDtl X ON X1.SODocNo = X.DocNo And X1.SODNo = X.DNo ");
                //SQL.AppendLine("        Inner Join TblBOQHdr X3 On X2.BOQDocNo = X3.DocNo ");
                //SQL.AppendLine("        Inner Join TblLOPHdr X4 On X3.LOPDocNo = X4.DocNo ");
                //SQL.AppendLine("        Inner Join TblProjectGroup X5 On X4.PGCode = X5.PGCode ");
                //SQL.AppendLine("        Where X1.DocNo In ( ");
                //SQL.AppendLine("            Select T3.DRDocNo ");
                //SQL.AppendLine("            From TblRecvCtHdr T1 ");
                //SQL.AppendLine("            Inner Join TblRecvCtDtl T2 On T1.DocNo=T2.DocNo ");
                //SQL.AppendLine("            Inner Join TblDOCt2Hdr T3 On T2.DOCtDocNo=T3.DocNo And T3.DRDocNo Is Not Null ");
                //SQL.AppendLine("            Inner Join TblDOCt3Hdr T4 On T1.DocNo=T4.RecvCtDocNo And T4.DocNo=@DocNo ");
                //SQL.AppendLine("        ) ");
                //SQL.AppendLine("    ) D On C.DRDocNo=D.DocNo ");
                //SQL.AppendLine("    Group By A.DocNo, B.ItCode ");
                //SQL.AppendLine(") D ");
                //SQL.AppendLine("    On C.RecvCtDocNo=D.DocNo ");
                //SQL.AppendLine("    And A.ItCode=D.ItCode ");
                #endregion

                SQL.AppendLine("Inner Join TblDOCt3Hdr C On A.DocNo = C.DocNo ");
                SQL.AppendLine("Left Join ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select T1.DocNo, T4.ItCode, ");
                SQL.AppendLine("    Group_Concat(Distinct IfNull(T9.ProjectCode, '-')) ProjectCode, ");
                SQL.AppendLine("    Group_Concat(Distinct IfNull(T9.ProjectName, '-')) ProjectName, ");
                SQL.AppendLine("    Group_Concat(Distinct IfNull(T6.PONo, '-')) PONo, ");
                SQL.AppendLine("    Group_Concat(Distinct IfNull(T5.Remark, '-')) SOCRemark, ");
                SQL.AppendLine("    Group_Concat(Distinct IfNull(T5.No, '-')) SOCNo ");
                SQL.AppendLine("    From TblRecvCtHdr T1 ");
                SQL.AppendLine("    Inner Join TblRecvCtDtl T2 On T1.DocNo = T2.DocNo And T1.DocNo In (Select RecvCtDocNo From TblDOCt3Hdr Where DocNo = @DocNo) ");
                SQL.AppendLine("    Inner Join TblDOCtHdr T3 On T2.DOCtDocNo = T3.DocNo ");
                SQL.AppendLine("    Inner Join TblDOCtDtl T4 On T3.DocNo =T4.DocNo And T2.DOCtDNo = T4.DNo ");
                SQL.AppendLine("    Inner Join TblSOContractDtl T5 On T3.SOContractDocNo = T5.DocNo And T4.SOContractDNo = T5.DNo ");
                SQL.AppendLine("    Inner Join TblSOContractHdr T6 On T5.DOcNo = T6.DocNo ");
                SQL.AppendLine("    Inner Join TblBOQHdr T7 ON T6.BOQDocNo = T7.DocNo ");
                SQL.AppendLine("    Inner Join TblLOPHdr T8 On T7.LOPDocNo = T8.DocNo ");
                SQL.AppendLine("    Left Join TblProjectGroup T9 On T8.PGCode = T9.PGCode ");
                SQL.AppendLine("    Group By T2.DocNo, T4.ItCode ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select T1.DocNo, T4.ItCode, ");
                SQL.AppendLine("    Group_Concat(Distinct IfNull(T9.ProjectCode, '-')) ProjectCode, ");
                SQL.AppendLine("    Group_Concat(Distinct IfNull(T9.ProjectName, '-')) ProjectName, ");
                SQL.AppendLine("    Group_Concat(Distinct IfNull(T6.PONo, '-')) PONo, ");
                SQL.AppendLine("    Group_Concat(Distinct IfNull(T5.Remark, '-')) SOCRemark, ");
                SQL.AppendLine("    Group_Concat(Distinct IfNull(T5.No, '-')) SOCNo ");
                SQL.AppendLine("    From TblRecvCtHdr T1 ");
                SQL.AppendLine("    Inner Join TblRecvCtDtl T2 On T1.DocNo = T2.DocNo And T1.DocNo In (Select RecvCtDocNo From TblDOCt3Hdr Where DocNo = @DocNo) ");
                SQL.AppendLine("    Inner Join TblDOCt2Dtl T4 On T2.DOCtDocNo =T4.DocNo And T2.DOCtDNo = T4.DNo ");
                SQL.AppendLine("    Inner Join TblSOContractDtl T5 On T4.SOContractDocNo = T5.DocNo And T4.SOContractDNo = T5.DNo ");
                SQL.AppendLine("    Inner Join TblSOContractHdr T6 On T5.DOcNo = T6.DocNo ");
                SQL.AppendLine("    Inner Join TblBOQHdr T7 ON T6.BOQDocNo = T7.DocNo ");
                SQL.AppendLine("    Inner Join TblLOPHdr T8 On T7.LOPDocNo = T8.DocNo ");
                SQL.AppendLine("    Left Join TblProjectGroup T9 On T8.PGCode = T9.PGCode ");
                SQL.AppendLine("    Group By T2.DocNo, T4.ItCode ");
                SQL.AppendLine(") D On C.RecvCtDocNo=D.DocNo ");
                SQL.AppendLine("    And A.ItCode=D.ItCode ");
            }
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd2, ref cm, SQL.ToString(),
                new string[] 
                    { 
                        //0
                        "DNo", 

                        //1-5
                        "ItCode", "ItName", "RecvCtQty", "DOCt3Qty", "InventoryUomCode", 

                        //6-10
                        "RecvCtQty2", "DOCt3Qty2", "InventoryUomCode2", "RecvCtQty3", "DOCt3Qty3", 
                        
                        //11-15
                        "InventoryUomCode3", "ItCodeInternal", "ProjectCode", "ProjectName", "PONo",

                        //16-17
                        "SOCRemark", "SOCNo"
                    },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 10);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 11);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 12);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 13);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 14);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 15);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 16);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 17);
                }, false, false, false, false
            );
            Grd2.Rows.Add();
            Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 3, 4, 6, 7, 9, 10 });
            Sm.FocusGrd(Grd2, 0, 1);
        }

        #endregion

        #region Additional Method

        internal string SelectedSOContract()
        {
            string s = string.Empty;

            if (Grd1.Rows.Count > 1)
            {
                for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
                {
                    if (s.Length > 0) s += ",";
                    s += Sm.GetGrdStr(Grd1, i, 25) + Sm.GetGrdStr(Grd1, i, 26);
                }
            }

            return s.Length == 0 ? "XXX" : s;
        }

        private void GetParameter()
        {
            string NumberOfInventoryUomCode = Sm.GetParameter("NumberOfInventoryUomCode");
            if (NumberOfInventoryUomCode.Length != 0) mNumberOfInventoryUomCode = int.Parse(NumberOfInventoryUomCode);
            mIsDOCtNeedShippingAddressInd = Sm.GetParameterBoo("IsDOCtNeedShippingAddressInd");
            mIsAutoJournalActived = Sm.GetParameterBoo("IsAutoJournalActived");
            mIsAcNoForSaleUseItemCategory = Sm.GetParameterBoo("IsAcNoForSaleUseItemCategory");
            mIsDOCtAmtRounded = Sm.GetParameterBoo("IsDOCtAmtRounded");
            mMainCurCode = Sm.GetParameter("MainCurCode");
            mIsBOMShowSpecifications = Sm.GetParameterBoo("IsBOMShowSpecifications");
            mIsDOCt3ShowProjectInfo = Sm.GetParameterBoo("IsDOCt3ShowProjectInfo");
            mIsInspectionSheetMandatory = Sm.GetParameterBoo("IsInspectionSheetMandatory");
            mIsDOCt3AllowToUploadFile = Sm.GetParameterBoo("IsDOCt3AllowToUploadFile");
            mHostAddrForFTPClient = Sm.GetParameter("HostAddrForFTPClient");
            mPortForFTPClient = Sm.GetParameter("PortForFTPClient");
            mUsernameForFTPClient = Sm.GetParameter("UsernameForFTPClient");
            mPasswordForFTPClient = Sm.GetParameter("PasswordForFTPClient");
            mSharedFolderForFTPClient = Sm.GetParameter("SharedFolderForFTPClient");
            mFileSizeMaxUploadFTPClient = Sm.GetParameter("FileSizeMaxUploadFTPClient");
            mIsSalesTransactionShowSOContractRemark = Sm.GetParameterBoo("IsSalesTransactionShowSOContractRemark");
            mIsSalesTransactionUseItemNotes = Sm.GetParameterBoo("IsSalesTransactionUseItemNotes");
            mIsDOCt3NotUseJournal = Sm.GetParameterBoo("IsDOCt3NotUseJournal");
        }

        internal void SetLueShippingAddress(ref DXE.LookUpEdit Lue, string CtCode, string DocNo)
        {
            try
            {
                var SQL = new StringBuilder();

                if (DocNo.Length== 0)
                {
                    SQL.AppendLine("Select Dno As Col1, Name As Col2, Address As Col3, Phone As Col4 ");
                    SQL.AppendLine("From TblCustomerShipAddress Where CtCode='" + CtCode + "' Order By DNo;");
                }
                else
                {
                    SQL.AppendLine("Select Distinct '000' As Col1, SAName As Col2, SAAddress As Col3, SAPhone As Col4 ");
                    SQL.AppendLine("From TblDOCtHdr Where DocNo='" + DocNo + "';");
                }

                Sm.SetLue4(
                    ref Lue, SQL.ToString(),
                    0, 35, 100, 30, false, true, true, true,  "Code", "Name", "Address", "Phone", "Col2", "Col2");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void RecomputeOutstanding()
        {
            string ItCode = string.Empty;
            var SQL = new StringBuilder();

            SQL.AppendLine("Select ItCode, ");
            SQL.AppendLine("RecvCtQty-DOCt3Qty As Qty, ");
            SQL.AppendLine("RecvCtQty2-DOCt3Qty2 As Qty2, ");
            SQL.AppendLine("RecvCtQty3-DOCt3Qty3 As Qty3 ");
            SQL.AppendLine("From TblRecvCtDtl2 ");
            SQL.AppendLine("Where DocNo=@DocNo Order By ItCode;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand();
                cm.Connection = cn;
                Sm.CmParam<String>(ref cm, "@DocNo", TxtRecvCtDocNo.Text);
                cm.CommandText = SQL.ToString();
                cm.CommandTimeout = 600;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                    { 
                        //0
                        "ItCode", 
                        
                        //1-3
                        "Qty", "Qty2", "Qty3" 
                    });

                if (dr.HasRows)
                {
                    Grd2.BeginUpdate();
                    while (dr.Read())
                    {
                        ItCode = Sm.DrStr(dr, 0);
                        for (int row = 0; row < Grd2.Rows.Count - 1; row++)
                        {
                            if (Sm.CompareStr(Sm.GetGrdStr(Grd2, row, 1), ItCode))
                            {
                                Sm.SetGrdValue("N", Grd2, dr, c, row, 3, 1);
                                Sm.SetGrdValue("N", Grd2, dr, c, row, 6, 2);
                                Sm.SetGrdValue("N", Grd2, dr, c, row, 9, 3);
                                break;
                            }
                        }
                    }
                    Grd2.EndUpdate();
                }
                dr.Close();
            }
        }

        private void RecomputeStock()
        {
            string Filter = string.Empty, Source = string.Empty, Lot = string.Empty, Bin = string.Empty;
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Source, Lot, Bin, Qty, Qty2, Qty3 From TblStockSummary Where WhsCode=@WhsCode ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand();
                cm.Connection = cn;
                Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
                if (Grd1.Rows.Count != 1)
                {
                    int No = 1;
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    {
                        if (Sm.GetGrdStr(Grd1, Row, 11).Length != 0)
                        {
                            Sm.GenerateSQLConditionForInventory(ref cm, ref Filter, No, ref Grd1, Row, 11);
                            No += 1;
                        }
                    }
                }
                if (Filter.Length == 0)
                    Filter = " And 0=1 ";
                else
                    Filter = " And (" + Filter + ")";

                cm.CommandText = SQL.ToString() + Filter;
                cm.CommandTimeout = 600;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                    { 
                        //0
                        "Source", 
                        
                        //1-5
                        "Lot", "Bin", "Qty", "Qty2", "Qty3" 
                    });

                if (dr.HasRows)
                {
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        Source = Sm.DrStr(dr, 0);
                        Lot = Sm.DrStr(dr, 1);
                        Bin = Sm.DrStr(dr, 2);
                        for (int row = 0; row < Grd1.Rows.Count - 1; row++)
                        {
                            if (
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, row, 11), Source) &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, row, 12), Lot) &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, row, 13), Bin)
                                )
                            {
                                Sm.SetGrdValue("N", Grd1, dr, c, row, 14, 3);
                                Sm.SetGrdValue("N", Grd1, dr, c, row, 17, 4);
                                Sm.SetGrdValue("N", Grd1, dr, c, row, 20, 5);
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        internal void RecomputeSummary()
        {
            decimal Qty = 0m, Qty2 = 0m, Qty3 = 0m;
            string ItCode = string.Empty;

            Grd2.BeginUpdate();
            for (int r = 0; r < Grd2.Rows.Count; r++)
            {
                ItCode = Sm.GetGrdStr(Grd2, r, 1);
                Qty = 0m;
                Qty2 = 0m; 
                Qty3 = 0m;
                if (ItCode.Length != 0)
                {
                    for (int i = 0; i < Grd1.Rows.Count; i++)
                    {
                        if (Sm.CompareStr(ItCode, Sm.GetGrdStr(Grd1, i, 4)))
                        {
                            Qty += Sm.GetGrdDec(Grd1, i, 15);
                            Qty2 += Sm.GetGrdDec(Grd1, i, 18);
                            Qty3 += Sm.GetGrdDec(Grd1, i, 21);
                        }
                    }
                }
                Grd2.Cells[r, 4].Value = Qty;
                Grd2.Cells[r, 7].Value = Qty2;
                Grd2.Cells[r, 10].Value = Qty3;
            }
            Grd2.EndUpdate();
            
        }

        private void ShowShippingAddressData(string CtCode, string ShipName)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@CtCode", CtCode);
            Sm.CmParam<String>(ref cm, "@ShipName", ShipName);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select A.Address, A.CityCode, B.CityName, A.CntCode, C.CntName, A.PostalCd, A.Phone, A.Fax, A.Email, A.Mobile " +
                    "From TblCustomerShipAddress A " +
                    "Left Join TblCity B On A.CityCode = B.CityCode " +
                    "Left Join TblCountry C On A.CntCode = C.CntCode Where A.CtCode=@CtCode And A.Name=@ShipName ",
                    new string[] 
                    { 
                        "Address", 
                        "CityCode", "CityName", "CntCode", "CntName", "PostalCd", 
                        "Phone", "Fax", "Email", "Mobile" 
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        MeeSAAddress.EditValue = Sm.DrStr(dr, c[0]);
                        mCityCode = Sm.DrStr(dr, c[1]);
                        TxtCity.EditValue = Sm.DrStr(dr, c[2]);
                        mCntCode = Sm.DrStr(dr, c[3]);
                        TxtCountry.EditValue = Sm.DrStr(dr, c[4]);
                        TxtPostalCd.EditValue = Sm.DrStr(dr, c[5]);
                        TxtPhone.EditValue = Sm.DrStr(dr, c[6]);
                        TxtFax.EditValue = Sm.DrStr(dr, c[7]);
                        TxtEmail.EditValue = Sm.DrStr(dr, c[8]);
                        TxtMobile.EditValue = Sm.DrStr(dr, c[9]);
                    }, false
                );
        }

        private MySqlCommand UpdateDOCtFile(string DocNo, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblDOCt3Hdr Set ");
            SQL.AppendLine("    FileName=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo  ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);

            return cm;
        }
        private MySqlCommand UpdateDOCtFile2(string DocNo, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblDOCt3Hdr Set ");
            SQL.AppendLine("    FileName2=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo  ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);

            return cm;
        }
        private MySqlCommand UpdateDOCtFile3(string DocNo, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblDOCt3Hdr Set ");
            SQL.AppendLine("    FileName3=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo  ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);

            return cm;
        }

        private void UploadFile(string DocNo)
        {
            if (IsUploadFileNotValid()) return;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile.Text));
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", TxtFile.Text));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);

                PbUpload.Invoke(
                    (MethodInvoker)delegate { PbUpload.Maximum = (int)file.Length; });

                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);
                    PbUpload.Invoke(
                        (MethodInvoker)delegate
                        {
                            PbUpload.Value = (int)file.Position;
                        });
                }
            }
            while (bytesRead != 0);

            Sm.StdMsg(mMsgType.Info, "File uploaded successfully");

            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdateDOCtFile(DocNo, toUpload.Name));
            Sm.ExecCommands(cml);
        }

        private void UploadFile2(string DocNo)
        {
            if (IsUploadFileNotValid2()) return;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile2.Text));
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", TxtFile2.Text));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);

                PbUpload2.Invoke(
                    (MethodInvoker)delegate { PbUpload2.Maximum = (int)file.Length; });

                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);
                    PbUpload2.Invoke(
                        (MethodInvoker)delegate
                        {
                            PbUpload2.Value = (int)file.Position;
                        });
                }
            }
            while (bytesRead != 0);

            Sm.StdMsg(mMsgType.Info, "File uploaded successfully");

            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdateDOCtFile2(DocNo, toUpload.Name));
            Sm.ExecCommands(cml);
        }

        private void UploadFile3(string DocNo)
        {
            if (IsUploadFileNotValid3()) return;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile3.Text));
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", TxtFile3.Text));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);

                PbUpload3.Invoke(
                    (MethodInvoker)delegate { PbUpload3.Maximum = (int)file.Length; });

                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);
                    PbUpload3.Invoke(
                        (MethodInvoker)delegate
                        {
                            PbUpload3.Value = (int)file.Position;
                        });
                }
            }
            while (bytesRead != 0);

            Sm.StdMsg(mMsgType.Info, "File uploaded successfully");

            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdateDOCtFile3(DocNo, toUpload.Name));
            Sm.ExecCommands(cml);
        }

        private bool IsUploadFileNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                IsFTPClientDataNotValid() ||
                IsFileSizeNotvalid() ||
                IsFileNameAlreadyExisted();
        }

        private bool IsUploadFileNotValid2()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                IsFTPClientDataNotValid2() ||
                IsFileSizeNotvalid2() ||
                IsFileNameAlreadyExisted2();
        }

        private bool IsUploadFileNotValid3()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                IsFTPClientDataNotValid3() ||
                IsFileSizeNotvalid3() ||
                IsFileNameAlreadyExisted3();
        }

        private bool IsFTPClientDataNotValid()
        {

            if (mIsDOCt3AllowToUploadFile && TxtFile.Text.Length > 0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }


            if (mIsDOCt3AllowToUploadFile && TxtFile.Text.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }

            if (mIsDOCt3AllowToUploadFile && TxtFile.Text.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }


            if (mIsDOCt3AllowToUploadFile && TxtFile.Text.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }

            return false;
        }

        private bool IsFTPClientDataNotValid2()
        {

            if (mIsDOCt3AllowToUploadFile && TxtFile2.Text.Length > 0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }

            if (mIsDOCt3AllowToUploadFile && TxtFile2.Text.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }

            if (mIsDOCt3AllowToUploadFile && TxtFile2.Text.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }

            if (mIsDOCt3AllowToUploadFile && TxtFile2.Text.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }
            return false;
        }

        private bool IsFTPClientDataNotValid3()
        {

            if (mIsDOCt3AllowToUploadFile && TxtFile3.Text.Length > 0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }


            if (mIsDOCt3AllowToUploadFile && TxtFile3.Text.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }


            if (mIsDOCt3AllowToUploadFile && TxtFile3.Text.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }


            if (mIsDOCt3AllowToUploadFile && TxtFile3.Text.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }
            return false;
        }

        private bool IsFileSizeNotvalid()
        {
            if (mIsDOCt3AllowToUploadFile && TxtFile.Text.Length > 0)
            {
                FileInfo f = new FileInfo(TxtFile.Text);

                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }


                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File too large to Upload.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileSizeNotvalid2()
        {
            if (mIsDOCt3AllowToUploadFile && TxtFile2.Text.Length > 0)
            {
                FileInfo f = new FileInfo(TxtFile2.Text);
                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }

                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File too large to Upload.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileSizeNotvalid3()
        {
            if (mIsDOCt3AllowToUploadFile && TxtFile3.Text.Length > 0)
            {
                FileInfo f = new FileInfo(TxtFile3.Text);
                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }

                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File too large to Upload.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted()
        {
            if (mIsDOCt3AllowToUploadFile && TxtFile.Text.Length > 0)
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile.Text));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select DocNo From TblMaterialRequestHdr ");
                SQL.AppendLine("Where FileName=@FileName ");
                SQL.AppendLine("And CancelInd = 'N' ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted2()
        {
            if (mIsDOCt3AllowToUploadFile && TxtFile2.Text.Length > 0)
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile2.Text));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select DocNo From TblMaterialRequestHdr ");
                SQL.AppendLine("Where FileName2=@FileName ");
                SQL.AppendLine("And CancelInd = 'N' ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted3()
        {
            if (mIsDOCt3AllowToUploadFile && TxtFile3.Text.Length > 0)
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile3.Text));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select DocNo From TblMaterialRequestHdr ");
                SQL.AppendLine("Where FileName3=@FileName ");
                SQL.AppendLine("And CancelInd = 'N' ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        private void DownloadFile(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                this.Text = "Connecting...";
                Application.DoEvents();

                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                this.Text = "Retrieving Information...";
                Application.DoEvents();

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                this.Text = "Downloading File...";
                Application.DoEvents();

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Set up progress bar
                PbUpload.Value = 0;
                PbUpload.Maximum = dataLength;
                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        PbUpload.Value = PbUpload.Maximum;

                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);

                        if (PbUpload.Value + bytesRead <= PbUpload.Maximum)
                        {
                            PbUpload.Value += bytesRead;

                            PbUpload.Refresh();
                            Application.DoEvents();
                        }
                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                MessageBox.Show("There was an error connecting to the FTP Server.");
            }
        }

        private void DownloadFile2(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                this.Text = "Connecting...";
                Application.DoEvents();

                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                this.Text = "Retrieving Information...";
                Application.DoEvents();

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                this.Text = "Downloading File...";
                Application.DoEvents();

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Set up progress bar
                PbUpload2.Value = 0;
                PbUpload2.Maximum = dataLength;
                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        PbUpload2.Value = PbUpload2.Maximum;

                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);

                        if (PbUpload2.Value + bytesRead <= PbUpload2.Maximum)
                        {
                            PbUpload2.Value += bytesRead;

                            PbUpload2.Refresh();
                            Application.DoEvents();
                        }

                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                MessageBox.Show("There was an error connecting to the FTP Server.");
            }
        }

        private void DownloadFile3(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                this.Text = "Connecting...";
                Application.DoEvents();

                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                this.Text = "Retrieving Information...";
                Application.DoEvents();

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                this.Text = "Downloading File...";
                Application.DoEvents();

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Set up progress bar
                PbUpload3.Value = 0;
                PbUpload3.Maximum = dataLength;
                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        PbUpload3.Value = PbUpload3.Maximum;

                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);

                        if (PbUpload3.Value + bytesRead <= PbUpload3.Maximum)
                        {
                            PbUpload3.Value += bytesRead;

                            PbUpload3.Refresh();
                            Application.DoEvents();
                        }

                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                MessageBox.Show("There was an error connecting to the FTP Server.");
            }
        }

        #endregion

        #region parprint

        private void ParPrint(string DocNo)
        {
            string Doctitle = Sm.GetParameter("DocTitle");
            var l = new List<DOCRHeader>();
            var ldtl = new List<DOCRDetail>();
            var ldtl2 = new List<DOCRDetail2>();
            var lSign = new List<DOCRSign>();


            string[] TableName = { "DOCRHeader", "DOCRDetail", "DOCRDetail2", "DOCRSign" };
            List<IList> myLists = new List<IList>();
            
            
            DateTime DocDtNow = Sm.ConvertDateTime(Sm.ServerCurrentDateTime()).AddMonths(-1);

            #region Header

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyAddressCity' ");
           
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                {
                    //0
                    "CompanyLogo",

                    //1-5
                    "CompanyName",
                    "CompanyAddress",
                    "CompanyAddressCity",
                    
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new DOCRHeader()
                        {
                            CompanyLogo = Sm.DrStr(dr, c[0]),

                            CompanyName = Sm.DrStr(dr, c[1]),
                            CompanyAddress = Sm.DrStr(dr, c[2]),
                            CompanyAddressCity = Sm.DrStr(dr, c[3]),
                            CustomerDO = TxtCtDONo.Text,
                            Customer = TxtCtCode.Text,

                            ProductDelivery = TxtProductDeliveryNo.Text,
                            CustomerReturn = TxtCtReturnNo.Text,
                            RemarkHeader = MeeRemark.Text,
                            Date = DteDocDt.Text,
                            InspectionSheet = TxtInspectionSheet.Text
                        });
                    }
                }
                dr.Close();
            }
            myLists.Add(l);

            #endregion

            #region Detail

            for (int i = 0; i < Grd2.Rows.Count - 1; ++i)
            {                
                ldtl.Add(new DOCRDetail()
                {
                    ProjectCode = Sm.GetGrdStr(Grd2, i, 13),
                    ProjectName = Sm.GetGrdStr(Grd2, i, 14),
                    POCode = Sm.GetGrdStr(Grd2, i, 15),
                    SOContractRemark = Sm.GetGrdStr(Grd2, i, 16),
                    SOContractNo = Sm.GetGrdStr(Grd2, i, 17),
                    
                });                
            }
            myLists.Add(ldtl);

            #endregion 

            #region Detail2

            for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
            {                
                ldtl2.Add(new DOCRDetail2()
                {
                    ItemName = Sm.GetGrdStr(Grd1, i, 6),
                    Remark = Sm.GetGrdStr(Grd1, i, 23),
                    ItLocalCode = Sm.GetGrdStr(Grd1, i, 5),
                    Qty = Sm.GetGrdDec(Grd1, i, 15),
                    UOM = Sm.GetGrdStr(Grd1, i, 16),
                    No = Sm.GetGrdStr(Grd1, i, 27),
                    Specification = Sm.GetGrdStr(Grd1, i, 28),
                });                
            }
            myLists.Add(ldtl2);

            #endregion

            #region Sign

            var SQL2 = new StringBuilder();
            var cm2 = new MySqlCommand();

            SQL2.AppendLine("Select A.EmpName, B.DeptName, C.PosName ");
            SQL2.AppendLine("FROM TBLEmployee A ");
            SQL2.AppendLine("INNER JOIN tbldepartment B ON A.DeptCode= B.DeptCode ");
            SQL2.AppendLine("INNER JOIN tblposition C ON A.PosCode=C.PosCode ");
            SQL2.AppendLine("INNER JOIN  tblparameter D ON A.EmpCode=D.ParValue and D.ParCode='DOCRPrintOutSignName' ");

            using (var cn2 = new MySqlConnection(Gv.ConnectionString))
            {
                cn2.Open();
                cm2.Connection = cn2;
                cm2.CommandText = SQL2.ToString();
                var dr2 = cm2.ExecuteReader();
                var c2 = Sm.GetOrdinal(dr2, new string[] 
                {
                    "EmpName",
                    "DeptName",
                    "PosName"
                });
                if (dr2.HasRows)
                {
                    while (dr2.Read())
                    {
                        lSign.Add(new DOCRSign()
                        {
                            Employee = Sm.DrStr(dr2, c2[0]),
                            Department = Sm.DrStr(dr2, c2[1]),
                            Position = Sm.DrStr(dr2, c2[2]),
                            
                        });
                    }
                }
                dr2.Close();
            }
            myLists.Add(lSign);

            #endregion


            if (Sm.GetParameter("DocTitle") == "IMS")
                Sm.PrintReport("DOCRIMS", myLists, TableName, false);
        }
        #endregion 

        #endregion

        #region Event

        #region Misc Control Event

        private void LueFontSize_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
                Grd1.Font = new Font(Grd1.Font.FontFamily.Name.ToString(), int.Parse(Sm.GetLue(LueFontSize)));
        }

        private void ChkHideInfoInGrd_CheckedChanged(object sender, EventArgs e)
        {
            HideInfoInGrd();
        }

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue2(Sl.SetLueWhsCode), string.Empty);
            ClearGrd1();
        }

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        private void TxtCtDONo_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtCtDONo);
        }

        private void TxtCtReturnNo_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtCtReturnNo);
        }

        private void TxtProductDeliveryNo_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtProductDeliveryNo);
        }

        private void TxtInspectionSheet_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtInspectionSheet);

        }

        private void ChkFile_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile.Checked == false)
            {
                TxtFile.EditValue = string.Empty;
            }
        }

        private void ChkFile2_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile2.Checked == false)
            {
                TxtFile2.EditValue = string.Empty;
            }
        }

        private void ChkFile3_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile3.Checked == false)
            {
                TxtFile3.EditValue = string.Empty;
            }
        }

        #endregion

        #region Button Event

        private void BtnCustomerShipAddress_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtCtCode, "Customer", false))
                Sm.FormShowDialog(new FrmDOCt10Dlg4(this, mCtCode));
        }

        private void BtnRecvCtDocNo1_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmDOCt10Dlg(this));
        }

        private void BtnRecvCtDocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtRecvCtDocNo, "Received#", false))
            {
                var f1 = new FrmRecvCt(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = TxtRecvCtDocNo.Text;
                f1.ShowDialog();
            }
        }

        private void BtnSAName_Click(object sender, EventArgs e)
        {
            if (mCtCode.Length==0) return;
            try
            {
                var f = new FrmCustomer(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mCtCode = mCtCode;
                f.ShowDialog();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #endregion

        #region Grid Event

        private void Grd2_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && TxtDocNo.Text.Length == 0 && e.ColIndex == 0 && TxtRecvCtDocNo.Text.Length != 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmDOCt10Dlg2(this, TxtRecvCtDocNo.Text));
            }
        }

        private void Grd2_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && BtnSave.Enabled && TxtDocNo.Text.Length == 0 && TxtRecvCtDocNo.Text.Length != 0)
                Sm.FormShowDialog(new FrmDOCt10Dlg2(this, TxtRecvCtDocNo.Text));
        }

        private void Grd2_KeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd2, e, BtnSave);
            Sm.GrdKeyDown(Grd2, e, BtnFind, BtnSave);
        }

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    if (e.ColIndex == 3 && TxtRecvCtDocNo.Text.Length != 0 && !Sm.IsLueEmpty(LueWhsCode, "Warehouse"))
                    {
                        e.DoDefault = false;
                        if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmDOCt10Dlg3(this, Sm.GetLue(LueWhsCode)));
                    }
                    if (Sm.IsGrdColSelected(new int[] { 3, 15, 18, 21, 23 }, e.ColIndex))
                    {
                        Sm.GrdRequestEdit(Grd1, e.RowIndex);
                        Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 1, 2 });
                        Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 14, 15, 17, 18, 20, 21 });
                    }
                }
                else
                {
                    if (e.ColIndex == 1 && (Sm.GetGrdBool(Grd1, e.RowIndex, 2) || Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length == 0))
                        e.DoDefault = false;
                }
            }
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0)
            {
                if (BtnSave.Enabled && e.KeyCode == Keys.Delete)
                {
                    Sm.GrdRemoveRow(Grd1, e, BtnSave);
                    RecomputeSummary();
                }
            }
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && BtnSave.Enabled && TxtDocNo.Text.Length == 0 && !Sm.IsLueEmpty(LueWhsCode, "Warehouse"))
                Sm.FormShowDialog(new FrmDOCt10Dlg3(this, Sm.GetLue(LueWhsCode)));
        }

        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 15, 18, 21 }, e);
            Sm.GrdAfterCommitEditTrimString(Grd1, new int[] { 23 }, e);

            if (e.ColIndex == 15)
            {
                Sm.ComputeQtyBasedOnConvertionFormula("12", Grd1, e.RowIndex, 4, 15, 18, 21, 16, 19, 22);
                Sm.ComputeQtyBasedOnConvertionFormula("13", Grd1, e.RowIndex, 4, 15, 21, 18, 16, 22, 19);
            }

            if (e.ColIndex == 18)
            {
                Sm.ComputeQtyBasedOnConvertionFormula("21", Grd1, e.RowIndex, 4, 18, 15, 21, 19, 16, 22);
                Sm.ComputeQtyBasedOnConvertionFormula("23", Grd1, e.RowIndex, 4, 18, 21, 15, 19, 22, 16);
            }

            if (e.ColIndex == 21)
            {
                Sm.ComputeQtyBasedOnConvertionFormula("31", Grd1, e.RowIndex, 4, 21, 15, 18, 22, 16, 19);
                Sm.ComputeQtyBasedOnConvertionFormula("32", Grd1, e.RowIndex, 4, 21, 18, 15, 22, 19, 16);
            }

            if (e.ColIndex == 15 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 16), Sm.GetGrdStr(Grd1, e.RowIndex, 19)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 18, Grd1, e.RowIndex, 15);

            if (e.ColIndex == 15 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 16), Sm.GetGrdStr(Grd1, e.RowIndex, 22)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 21, Grd1, e.RowIndex, 15);

            if (e.ColIndex == 18 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 19), Sm.GetGrdStr(Grd1, e.RowIndex, 22)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 21, Grd1, e.RowIndex, 18);

            if (Sm.IsGrdColSelected(new int[] { 1, 15, 18, 21 }, e.ColIndex))
                RecomputeSummary();
        }

        #endregion

        #region class report
        private class DOCRHeader
        {
            public string CompanyLogo { get; set; }
            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyAddressCity { get; set; }
            public string CustomerDO { get; set; }

            public string ProjectCode { get; set; }
            public string ProjectName { get; set; }
            public string Customer { get; set; }
            public string Date { get; set; }
            public string ProductDelivery { get; set; }

            public string CustomerReturn { get; set; }
            public string CustomerPO { get; set; }
            public string RemarkHeader { get; set; }
            public string Employee { get; set; }
            public string Department { get; set; }
            public string Position { get; set; }
            public string InspectionSheet { get; set; }


        }

        private class DOCRDetail
        {
            public string ProjectName { get; set; }
            public string ProjectCode { get; set; }
            public string POCode { get; set; }
            public string SOContractRemark { get; set; }
            public string SOContractNo { get; set; }
        }
        

        private class DOCRDetail2
        {
            public string ItemName { get; set; }
            public string ItLocalCode { get; set; }
            public decimal Qty { get; set; }
            public string UOM { get; set; }
            public string No { get; set; }
            public string Specification { get; set; }
            public string Remark { get; set; }

        }

        private class DOCRSign
        {
          
            public string Employee { get; set; }
            public string Department { get; set; }
            public string Position { get; set; }
        }
        #endregion

    }
}
