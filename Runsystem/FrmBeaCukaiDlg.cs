﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmBeaCukaiDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmBeaCukai mFrmParent;
        string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmBeaCukaiDlg(FrmBeaCukai FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Packing List";
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -30);
                Sl.SetLueCtCode(ref LueCtCode);
                SetGrd();
                SetSQL();
                base.FrmLoad(sender, e);
                ShowData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetSQL()
        {
            var SQL = new StringBuilder();

            {
                SQL.AppendLine("Select Distinct A.DocNo, A.DocDt, C.CtCode, D.CtName, ");
                SQL.AppendLine("E.SectionNo, ");

                SQL.AppendLine("Case E.SectionNo ");
                for (int i=1;i<=25;i++)
                    SQL.AppendLine("    When '"+i.ToString()+"' Then Cnt"+i.ToString());
                SQL.AppendLine(" End As Cnt, ");

                SQL.AppendLine("Case E.SectionNo ");
                for (int i = 1;i<=25;i++)
                    SQL.AppendLine("    When '" + i.ToString() + "' Then Seal" + i.ToString());
                SQL.AppendLine(" End As Seal ");

                SQL.AppendLine("From TblPLHdr A ");
                SQL.AppendLine("Inner Join TblSIHdr B On A.SIDocNo=B.DocNo ");
                SQL.AppendLine("Inner Join TblSP C ");
                SQL.AppendLine("    On B.SPDocNo=C.DocNo ");
                SQL.AppendLine("    And C.Status In ('R', 'F') ");
                SQL.AppendLine("Inner Join TblCustomer D On C.CtCode=D.CtCode ");
                SQL.AppendLine("Inner Join TblPLDtl E On A.DocNo=E.DocNo And E.ProcessInd<>'F' ");
                SQL.AppendLine("Where Concat(A.DocNo, E.SectionNo) Not In  ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select Concat(PlDocNo, SectionNo) From TblbeaCukai Where CancelInd = 'N' ");
                SQL.AppendLine(") ");
            }

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
           
            {
                Grd1.Cols.Count = 9;
                Grd1.FrozenArea.ColCount = 2;
                Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "PL#", 
                        "",
                        "Date",
                        "Customer Code",
                        "Customer",

                        //6-8
                        "Container",
                        "Seal",
                        "Section#"
                    },
                    new int[] 
                    {
                        //0
                        50, 

                        //1-5
                        140, 20, 80, 0, 250,   
                    
                        //6-8
                        200, 200, 0
                    }
                );
                Sm.GrdColButton(Grd1, new int[] { 2 });
                Sm.GrdFormatDate(Grd1, new int[] { 3 });
                Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 3, 4, 5, 6, 7, 8 });
                Sm.GrdColInvisible(Grd1, new int[] { 2, 8 }, false);
            }
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " And 0=0 ";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDt");
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCode), "C.CtCode", true);
                {
                    Sm.ShowDataInGrid(
                            ref Grd1, ref cm,
                            mSQL + Filter + " Order By A.DocDt, A.DocNo;",
                            new string[]
                        {
                            //0
                            "DocNo", 

                            //1-5
                            "DocDt", "CtCode", "CtName", "Cnt", "Seal",

                            //6
                            "SectionNo"
                        },
                            (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                            {
                                Grd.Cells[Row, 0].Value = Row + 1;
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                                Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 1);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                            }, true, false, false, false
                        );
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            try
            {
                if (Sm.IsFindGridValid(Grd1, 1))
                {
                    int Row = Grd1.CurRow.Index;
                    string DocNo = Sm.GetGrdStr(Grd1, Row, 1);
                    {
                       mFrmParent.TxtPLDocNo.EditValue = DocNo;
                       mFrmParent.mCtCode = Sm.GetGrdStr(Grd1, Row, 4);
                       mFrmParent.TxtCtCode.EditValue = Sm.GetGrdStr(Grd1, Row, 5);
                       mFrmParent.TxtCntName.EditValue = Sm.GetGrdStr(Grd1, Row, 6);
                    }
                    mFrmParent.mSectionNo = Sm.GetGrdStr(Grd1, Row, 8);
                    mFrmParent.ShowSOInfo();
                   
                    this.Close();
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
           
                {
                    var f = new FrmPL(mFrmParent.mMenuCode);
                    f.Tag = mFrmParent.mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                    f.ShowDialog();
                }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
           
                {
                    var f = new FrmPL(mFrmParent.mMenuCode);
                    f.Tag = mFrmParent.mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                    f.ShowDialog();
                }
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Date");
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");
        }

        #endregion

        #endregion

    }
}
