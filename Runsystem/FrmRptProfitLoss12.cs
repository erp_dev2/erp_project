﻿#region Update
/*
    22/09/2022 [IBL/TWC] New Apps
    02/12/2022 [HPH/TWC] Membuat filter multi profit center
*/
#endregion

#region Namespace

using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sl = RunSystem.SetLue;
using Sm = RunSystem.StdMtd;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptProfitLoss12 : RunSystem.FrmBase6
    {
        #region Field

        private List<String> mlProfitCenter = null;

        private string
            mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;
        private string
            mThisYear = string.Empty,
            mPrevYear = string.Empty,
            mMth = string.Empty,
            mMth1 = string.Empty;
        private bool
            mIsBudget2YearlyFormat = false,
            mIsMRBudgetBasedOnBudgetCategory = false,
            mIsCASUsedForBudget = false,
            mIsOutgoingPaymentUseBudget = false,
            mIsVoucherRequestSSUseBudget = false,
            mIsVoucherRequestPayrollUseBudget = false,
            mIsTravelRequestUseBudget = false,
            mIsEmpClaimRequestUseBudget = false,
            mIsAllProfitCenterSelected = false,
            mIsMRShowEstimatedPrice = false,
            mIsBudgetCalculateFromEstimatedPrice = false,
            mIsBudgetTransactionCanUseOtherDepartmentBudget = false,
            mIsBudgetUseMRProcessInd = false;



        private string
            mMRAvailableBudgetSubtraction = string.Empty,
            mBudgetBasedOn = string.Empty,
            mReqTypeForNonBudget = string.Empty,
            mMainCurCode = string.Empty,
            mBudgetUsedFormula = string.Empty;

        #endregion

        #region Constructor

        public FrmRptProfitLoss12(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method
        protected override void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();

                string CurrentDateTime = Sm.ServerCurrentDateTime();
                mlProfitCenter = new List<String>();
                SetCcbProfitCenterCode(ref CcbProfitCenterCode);
                Sl.SetLueYr(LueYr, "");
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                Sl.SetLueMth(LueMth);
                Sm.SetLue(LueMth, CurrentDateTime.Substring(4, 2));
                Sl.SetLuePeriod(LuePeriod);
                SetTimeStampVariable();
                SetGrd();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetGrd()
        {
            Grd1.ReadOnly = true;
            Grd1.Header.Rows.Count = 2;
            Grd1.Cols.Count = 18;
            Grd1.FrozenArea.ColCount = 1;

            SetGrdHdr(ref Grd1, 0, 0, 50, "No.", 2);
            SetGrdHdr(ref Grd1, 0, 1, 120, "COA#", 2);
            SetGrdHdr(ref Grd1, 0, 2, 250, "URAIAN", 2);
            SetGrdHdr(ref Grd1, 0, 3, 180, "REALISASI" + Environment.NewLine + "SD " + mMth + " " + mPrevYear, 2);
            SetGrdHdr(ref Grd1, 0, 4, 180, "REALISASI" + Environment.NewLine + "SD " + mMth + " " + mThisYear, 2);
            SetGrdHdr(ref Grd1, 0, 5, 180, "ANGGARAN" + Environment.NewLine + "SD " + mMth1 + " " + mThisYear + Environment.NewLine + "(RUPS)", 2);
            SetGrdHdr(ref Grd1, 0, 6, 180, "ANGGARAN" + Environment.NewLine + "SD " + mMth1 + " " + mThisYear + Environment.NewLine + "(PELAKSANAAN)", 2);
            SetGrdHdr(ref Grd1, 0, 7, 180, "ANGGARAN" + Environment.NewLine + "SD " + mMth1 + " " + mThisYear + Environment.NewLine + "(REVISI)", 2);
            SetGrdHdr(ref Grd1, 0, 8, 180, "ANGGARAN" + Environment.NewLine + mThisYear + Environment.NewLine + "(RUPS)", 2);
            SetGrdHdr(ref Grd1, 0, 9, 180, "ANGGARAN" + Environment.NewLine + mThisYear + Environment.NewLine + "(PELAKSANAAN)", 2);
            SetGrdHdr(ref Grd1, 0, 10, 180, "ANGGARAN" + Environment.NewLine + mThisYear + Environment.NewLine + "(REVISI)", 2);
            SetGrdHdr2(ref Grd1, 1, 11, "CAPAIAN", 7, mThisYear, mPrevYear);

            Sm.GrdFormatDec(Grd1, new int[] { 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, !ChkHideInfoInGrd.Checked);
        }

        protected override void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsLueEmpty(LueYr, "Year") ||
                Sm.IsLueEmpty(LueMth, "Month")
                ) return;
            var Yr = Sm.GetLue(LueYr);
            var Mth = Sm.GetLue(LueMth);

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                SetTimeStampVariable();
                SetGrd();

                var l = new List<COARptProfitLoss>();
                var l1 = new List<RptProfitLoss>();
                var l2 = new List<COAJournalProfitLoss>();
                var l3 = new List<COABudgetMonthToDate>();
                var l4 = new List<COABudgetThisYear>();

                SetProfitCenter();
                GetCOAProfitLoss(ref l);
                PrepDataProfitLossFormula(ref l1);
                if (l.Count > 0)
                {
                    ProcesCOAJournal(ref l, ref l2);
                    ProcessBudgetMonthToDt(ref l, ref l3);
                    ProcessBudgetThisYr(ref l, ref l4);
                    ProcessCOARptProfitLoss(ref l, ref l2, ref l3, ref l4);
                    ProcessRptProfitLoss(ref l, ref l1);
                    ProcessFormulaCalculation(ref l1);
                    ProcessGrid(ref l1);
                }
                else
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }
        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mIsBudget2YearlyFormat = Sm.GetParameterBoo("IsBudget2YearlyFormat");
            mIsMRBudgetBasedOnBudgetCategory = Sm.GetParameterBoo("IsMRBudgetBasedOnBudgetCategory");
            mIsCASUsedForBudget = Sm.GetParameterBoo("IsCASUsedForBudget");
            mIsOutgoingPaymentUseBudget = Sm.GetParameterBoo("IsOutgoingPaymentUseBudget");
            mIsVoucherRequestSSUseBudget = Sm.GetParameterBoo("IsVoucherRequestSSUseBudget");
            mIsVoucherRequestPayrollUseBudget = Sm.GetParameterBoo("IsVoucherRequestPayrollUseBudget");
            mIsTravelRequestUseBudget = Sm.GetParameterBoo("IsTravelRequestUseBudget");
            mIsEmpClaimRequestUseBudget = Sm.GetParameterBoo("IsEmpClaimRequestUseBudget");
            mIsMRShowEstimatedPrice = Sm.GetParameterBoo("IsMRShowEstimatedPrice");
            mIsBudgetCalculateFromEstimatedPrice = Sm.GetParameterBoo("IsBudgetCalculateFromEstimatedPrice");
            mIsBudgetTransactionCanUseOtherDepartmentBudget = Sm.GetParameterBoo("IsBudgetTransactionCanUseOtherDepartmentBudget");
            mIsBudgetUseMRProcessInd = Sm.GetParameterBoo("IsBudgetUseMRProcessInd");

            mMRAvailableBudgetSubtraction = Sm.GetParameter("MRAvailableBudgetSubtraction");
            mBudgetBasedOn = Sm.GetParameter("BudgetBasedOn");
            mReqTypeForNonBudget = Sm.GetParameter("ReqTypeForNonBudget");
            mMainCurCode = Sm.GetParameter("MainCurCode");
            mBudgetUsedFormula = Sm.GetParameter("BudgetUsedFormula");
            if (mBudgetUsedFormula.Length == 0) mBudgetUsedFormula = "1";
        }

        private void SetProfitCenter()
        {
            mIsAllProfitCenterSelected = false;

            if (CcbProfitCenterCode.Text.Length > 0)
            {
                if (!ChkProfitCenterCode.Checked) mIsAllProfitCenterSelected = true;
                if (mIsAllProfitCenterSelected) return;

                bool IsCompleted = false, IsFirst = true;

                mlProfitCenter.Clear();

                while (!IsCompleted)
                    SetProfitCenter(ref IsFirst, ref IsCompleted);
            }
        }

        private void SetProfitCenter(ref bool IsFirst, ref bool IsCompleted)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string Filter = string.Empty, ProfitCenterCode = string.Empty;
            bool IsExisted = false;
            int i = 0;

            SQL.AppendLine("Select Distinct ProfitCenterCode From TblProfitCenter ");
            if (IsFirst)
            {
                if (ChkProfitCenterCode.Checked)
                {
                    SQL.AppendLine("    Where Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ");
                    Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                    IsCompleted = false;
                }
                else
                    IsCompleted = true;
                IsFirst = false;
            }
            else
            {
                SQL.AppendLine("    Where Parent Is Not Null ");
                foreach (var x in mlProfitCenter.Distinct())
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += " (Parent=@ProfitCenterCode" + i.ToString() + ") ";

                    Sm.CmParam<String>(ref cm, "@ProfitCenterCode" + i.ToString(), x);
                    i++;
                }
                if (Filter.Length == 0)
                    SQL.AppendLine("    And 1=0 ");
                else
                {
                    SQL.AppendLine("    And (" + Filter + ") ");
                }
                IsCompleted = true;
            }
            SQL.AppendLine("Order By ProfitCenterCode;");

            cm.CommandText = SQL.ToString();

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ProfitCenterCode" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        if (IsFirst)
                            mlProfitCenter.Add(ProfitCenterCode);
                        else
                        {
                            ProfitCenterCode = Sm.DrStr(dr, c[0]);
                            IsExisted = false;
                            foreach (var x in mlProfitCenter.Where(w => Sm.CompareStr(w, ProfitCenterCode)))
                                IsExisted = true;
                            if (!IsExisted)
                            {
                                mlProfitCenter.Add(ProfitCenterCode);
                                IsCompleted = false;
                            }
                        }
                    }
                }
                else
                    IsCompleted = true;
                dr.Close();
            }
        }

        private void SetCcbProfitCenterCode(ref CheckedComboBoxEdit Ccb)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Col, Col2 From (");
            SQL.AppendLine("    Select ProfitCenterName As Col, ProfitCenterCode As Col2 ");
            SQL.AppendLine("    From TblProfitCenter ");
            SQL.AppendLine("    Where ProfitCenterCode In ( ");
            SQL.AppendLine("        Select ProfitCenterCode From TblGroupProfitCenter ");
            SQL.AppendLine("        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine(") Tbl Order By Col2; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetCcb(ref Ccb, cm);
        }

        private string GetCcbProfitCenterCode()
        {
            var Value = Sm.GetCcb(CcbProfitCenterCode);
            if (Value.Length == 0) return string.Empty;
            return GetProfitCenterCode(Value).Replace(", ", ",");
        }

        private string GetProfitCenterCode(string Value)
        {
            if (Value.Length == 0) return string.Empty;

            return
                Sm.GetValue(
                    "Select Group_Concat(T.Code Separator ', ') As Code " +
                    "From (Select ProfitCenterCode As Code From TblProfitCenter Where Find_In_Set(ProfitCenterName, @Param) ) T; ",
                    Value.Replace(", ", ","));
        }

       

        private void GetCOAProfitLoss(ref List<COARptProfitLoss> l)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select RptId, AcNo ");
            SQL.AppendLine("From TblRptProfitLossDtl ");
            SQL.AppendLine("Order By AcNo; ");

            var cm = new MySqlCommand();
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "RptId", "AcNo" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new COARptProfitLoss()
                        {
                            RptId = Sm.DrStr(dr, c[0]),
                            AcNo = Sm.DrStr(dr, c[1]),
                            Amt1 = 0m,
                            Amt2 = 0m,
                            Amt3 = 0m,
                            Amt4 = 0m,
                            Amt5 = 0m,
                            Amt6 = 0m,
                            Amt7 = 0m,
                            Amt8 = 0m,
                        });
                    }
                }
                dr.Close();
            }
        }

        private void PrepDataProfitLossFormula(ref List<RptProfitLoss> l1)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.RptId, Group_Concat(B.AcNo Separator '+') As AcNo, ");
            SQL.AppendLine("A.AcName, A.Formula, A.ParentInd ");
            SQL.AppendLine("From TblRptProfitLossHdr A ");
            SQL.AppendLine("Left Join TblRptProfitLossDtl B On A.RptId = B.RptId ");
            SQL.AppendLine("Group By RptId ");
            SQL.AppendLine("Order By RptId; ");

            var cm = new MySqlCommand();
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "RptId", "AcNo", "AcName", "Formula", "ParentInd" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l1.Add(new RptProfitLoss()
                        {
                            RptId = Sm.DrStr(dr, c[0]),
                            AcNo = Sm.DrStr(dr, c[1]),
                            AcName = Sm.DrStr(dr, c[2]),
                            Formula = Sm.DrStr(dr, c[3]),
                            ParentInd = Sm.DrStr(dr, c[4]) == "Y",
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcesCOAJournal(ref List<COARptProfitLoss> l, ref List<COAJournalProfitLoss> l2)
        {
            int i = 0, j = 0;
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            var AcNo = l.GroupBy(x => x.AcNo).Select(y => y.First()).Distinct();
            string Query = string.Empty;

            if (CcbProfitCenterCode.Text.Length > 0)
            {

                Query += "    And A.CCCode Is Not Null ";
                Query += "    And A.CCCode In ( ";
                Query += "        Select Distinct CCCode ";
                Query += "       From TblCostCenter ";
                Query += "        Where ActInd='Y' ";
                Query += "        And ProfitCenterCode Is Not Null ";
                Query += "        And ProfitCenterCode In ( ";
                Query += "            Select Distinct ProfitCenterCode From TblGroupProfitCenter ";
                Query += "            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ";
                Query += "       ) ";


                if (!mIsAllProfitCenterSelected)
                {
                    var Filter = string.Empty;
                    int k = 0;
                    foreach (var x in mlProfitCenter.Distinct())
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += " (ProfitCenterCode=@ProfitCenterCode" + k.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@ProfitCenterCode" + k.ToString(), x);
                        k++;
                    }
                    if (Filter.Length == 0)
                        Query += "    And 1=0 ";
                    else
                        Query += "    And (" + Filter + ") ";
                }
                else
                {
                    if (ChkProfitCenterCode.Checked)
                    {
                        Query += "    And Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ";
                        if (ChkProfitCenterCode.Checked)
                            Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode()); // multiEntCode.Contains("Consolidate") ? "" : multiEntCode);
                    }

                }
                Query += "       ) ";
            }

            SQL.AppendLine("Select T.AcNo, T.PrevYrInd, T.Amt, T.Yr, T.Mth From ( ");
            SQL.AppendLine("Select B.AcNo, 'N' As PrevYrInd, Left(A.DocDt, 4) Yr, Substring(A.DocDt, 5, 2) Mth, ");
            SQL.AppendLine("Sum(Case When C.AcType='D' Then B.DAmt-B.CAmt Else B.CAmt-B.DAmt End) As Amt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join TblJournalDtl B ON A.DocNo=B.DocNo ");
            SQL.AppendLine("    And B.AcNo Not In (Select Distinct Parent From TblCOA Where Parent Is Not Null) ");
            SQL.AppendLine("    And ( ");
            foreach (var y in AcNo)
            {
                if (i != 0) SQL.Append(" Or ");

                SQL.AppendLine("B.AcNo Like '" + y.AcNo + "%' ");

                i += 1;
            }
            SQL.AppendLine("    ) ");
            SQL.AppendLine("Inner Join TblCOA C On B.AcNo=C.AcNo And C.ActInd='Y' ");
            SQL.AppendLine("Where Left(A.Period, 4)=@ThisYr ");
            if (Sm.GetLue(LuePeriod).Length > 0)
                SQL.AppendLine("And Left(A.Period, 6)<Concat(@ThisYr, @Period) ");
            else
                SQL.AppendLine("And Left(A.DocDt, 6)<=Concat(@ThisYr, @Mth) ");
            SQL.AppendLine(Query);
            SQL.AppendLine("Group By B.AcNo ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select B.AcNo, 'Y' As PrevYrInd, Left(A.DocDt, 4) Yr, Substring(A.DocDt, 5, 2) Mth, ");
            SQL.AppendLine("Sum(Case When C.AcType='D' Then B.DAmt-B.CAmt Else B.CAmt-B.DAmt End) As Amt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join TblJournalDtl B ON A.DocNo=B.DocNo ");
            SQL.AppendLine("    And B.AcNo Not In (Select Distinct Parent From TblCOA Where Parent Is Not Null) ");
            SQL.AppendLine("    And ( ");
            foreach (var y in AcNo)
            {
                if (j != 0) SQL.Append(" Or ");

                SQL.AppendLine("B.AcNo Like '" + y.AcNo + "%' ");

                j += 1;
            }
            SQL.AppendLine("    ) ");
            SQL.AppendLine("Inner Join TblCOA C On B.AcNo=C.AcNo And C.ActInd='Y' ");
            SQL.AppendLine("Where Left(A.DocDt, 4)=@PrevYr ");
            if (Sm.GetLue(LuePeriod).Length > 0)
                SQL.AppendLine("And Left(A.Period, 6)<Concat(@PrevYr, @Period) ");
            else
                SQL.AppendLine("And Left(A.DocDt, 6)<=Concat(@PrevYr, @Mth) ");           
            SQL.AppendLine(Query);
            SQL.AppendLine("Group By B.AcNo ");
            SQL.AppendLine(") T ");
            SQL.AppendLine("Order By T.AcNo, T.Yr, T.Mth ; ");

            Sm.CmParam<String>(ref cm, "@ThisYr", mThisYear);
            Sm.CmParam<String>(ref cm, "@PrevYr", mPrevYear);
            Sm.CmParam<String>(ref cm, "@Mth", Sm.GetLue(LueMth));
            Sm.CmParam<String>(ref cm, "@Period", Sm.GetLue(LuePeriod));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "PrevYrInd", "Yr", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l2.Add(new COAJournalProfitLoss()
                        {
                            AcNo = Sm.DrStr(dr, c[0]),
                            PrevYrInd = Sm.DrStr(dr, c[1]) == "Y",
                            Yr = Sm.DrStr(dr, c[2]),
                            Amt = Sm.DrDec(dr, c[3]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessBudgetMonthToDt(ref List<COARptProfitLoss> l, ref List<COABudgetMonthToDate> l3)
        {
            int i = 0;
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            var AcNo = l.GroupBy(x => x.AcNo).Select(y => y.First()).Distinct();
            string Query = string.Empty;

            if (CcbProfitCenterCode.Text.Length > 0)
            {

                Query += "    And C.CCCode Is Not Null ";
                Query += "    And C.CCCode In ( ";
                Query += "        Select Distinct CCCode ";
                Query += "       From TblCostCenter ";
                Query += "        Where ActInd='Y' ";
                Query += "        And ProfitCenterCode Is Not Null ";
                Query += "        And ProfitCenterCode In ( ";
                Query += "            Select Distinct ProfitCenterCode From TblGroupProfitCenter ";
                Query += "            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ";
                Query += "       ) ";


                if (!mIsAllProfitCenterSelected)
                {
                    var Filter = string.Empty;
                    int k = 0;
                    foreach (var x in mlProfitCenter.Distinct())
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += " (ProfitCenterCode=@ProfitCenterCode" + k.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@ProfitCenterCode" + k.ToString(), x);
                        k++;
                    }
                    if (Filter.Length == 0)
                        Query += "    And 1=0 ";
                    else
                        Query += "    And (" + Filter + ") ";
                }
                else
                {
                    if (ChkProfitCenterCode.Checked)
                    {
                        Query += "    And Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ";
                        if (ChkProfitCenterCode.Checked)
                            Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode()); // multiEntCode.Contains("Consolidate") ? "" : multiEntCode);
                    }

                }
                Query += "       ) ";
            }

            SQL.AppendLine("Select T.AcNo, Sum(T.Amt1) RequestedAmt, Sum(T.Amt3) UsedAmt, Sum(T.Amt2) BudgetAmt ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("	Select C.AcNo, A.Amt1, A.Amt2, ");
            SQL.AppendLine(Sm.SelectUsedBudget());
            SQL.AppendLine("	As Amt3 ");
            SQL.AppendLine("	From TblBudgetSummary A ");
            SQL.AppendLine("	Inner Join TblBudgetCategory B On A.BCCode=B.BCCode ");
            SQL.AppendLine("	Left Join TblCostCenter C On B.CCCode = C.CCCode ");
            SQL.AppendLine("		And C.AcNo Not In (Select Distinct Parent From TblCOA Where Parent Is Not Null) ");
            SQL.AppendLine("		And ( ");
            foreach (var y in AcNo)
            {
                if (i != 0) SQL.Append(" Or ");

                SQL.AppendLine("C.AcNo Like '" + y.AcNo + "%' ");

                i += 1;
            }
            SQL.AppendLine("        ) ");
            SQL.AppendLine(ComputeUsedBudget(2, "", mThisYear, Sm.GetLue(LueMth), "", true));
            if (Sm.GetLue(LuePeriod).Length > 0)
            {
                if (decimal.Parse(Sm.GetLue(LuePeriod)) <= 12)
                    SQL.AppendLine("Where A.Yr = @Yr And A.Mth <= @Period ");
                else
                    SQL.AppendLine("Where A.Yr = @Yr And A.Mth <= '12' ");
            }
            else
                SQL.AppendLine("Where A.Yr = @Yr And A.Mth <= @Mth ");
            SQL.AppendLine(Query);
            SQL.AppendLine(")T ");
            SQL.AppendLine("Group By T.AcNo ");
            SQL.AppendLine("Order By T.AcNo; ");
           

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@Yr", mThisYear);
                Sm.CmParam<String>(ref cm, "@Mth", Sm.GetLue(LueMth));
                Sm.CmParam<String>(ref cm, "@Period", Sm.GetLue(LuePeriod));

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "RequestedAmt", "UsedAmt", "BudgetAmt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l3.Add(new COABudgetMonthToDate()
                        {
                            AcNo = Sm.DrStr(dr, c[0]),
                            RequestedAmt = Sm.DrDec(dr, c[1]),
                            UsedAmt = Sm.DrDec(dr, c[2]),
                            BudgetAmt = Sm.DrDec(dr, c[3]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessBudgetThisYr(ref List<COARptProfitLoss> l, ref List<COABudgetThisYear> l4)
        {
            int i = 0;
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            var AcNo = l.GroupBy(x => x.AcNo).Select(y => y.First()).Distinct();
            string Query = string.Empty;

            if (CcbProfitCenterCode.Text.Length > 0)
            {

                Query += "    And C.CCCode Is Not Null ";
                Query += "    And C.CCCode In ( ";
                Query += "        Select Distinct CCCode ";
                Query += "       From TblCostCenter ";
                Query += "        Where ActInd='Y' ";
                Query += "        And ProfitCenterCode Is Not Null ";
                Query += "        And ProfitCenterCode In ( ";
                Query += "            Select Distinct ProfitCenterCode From TblGroupProfitCenter ";
                Query += "            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ";
                Query += "       ) ";


                if (!mIsAllProfitCenterSelected)
                {
                    var Filter = string.Empty;
                    int k = 0;
                    foreach (var x in mlProfitCenter.Distinct())
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += " (ProfitCenterCode=@ProfitCenterCode" + k.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@ProfitCenterCode" + k.ToString(), x);
                        k++;
                    }
                    if (Filter.Length == 0)
                        Query += "    And 1=0 ";
                    else
                        Query += "    And (" + Filter + ") ";
                }
                else
                {
                    if (ChkProfitCenterCode.Checked)
                    {
                        Query += "    And Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ";
                        if (ChkProfitCenterCode.Checked)
                            Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode()); // multiEntCode.Contains("Consolidate") ? "" : multiEntCode);
                    }

                }
                Query += "       ) ";
            }

            SQL.AppendLine("Select T.AcNo, Sum(T.Amt1) RequestedAmt, Sum(T.Amt3) UsedAmt, Sum(T.Amt2) BudgetAmt ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("	Select C.AcNo, A.Amt1, A.Amt2, ");
            SQL.AppendLine(Sm.SelectUsedBudget());
            SQL.AppendLine("	As Amt3 ");
            SQL.AppendLine("	From TblBudgetSummary A ");
            SQL.AppendLine("	Inner Join TblBudgetCategory B On A.BCCode=B.BCCode ");
            SQL.AppendLine("	Left Join TblCostCenter C On B.CCCode = C.CCCode ");
            SQL.AppendLine("		And C.AcNo Not In (Select Distinct Parent From TblCOA Where Parent Is Not Null) ");
            SQL.AppendLine("		And ( ");
            foreach (var y in AcNo)
            {
                if (i != 0) SQL.Append(" Or ");

                SQL.AppendLine("C.AcNo Like '" + y.AcNo + "%' ");

                i += 1;
            }
            SQL.AppendLine("        ) ");
            SQL.AppendLine(ComputeUsedBudget(2, "", mThisYear, Sm.GetLue(LueMth), "", true));
            SQL.AppendLine("Where A.Yr = @Yr ");
            SQL.AppendLine(Query);
            SQL.AppendLine(")T ");
            SQL.AppendLine("Group By T.AcNo ");
            SQL.AppendLine("Order By T.AcNo; ");
       

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@Yr", mThisYear);

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "AcNo", "RequestedAmt", "UsedAmt", "BudgetAmt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l4.Add(new COABudgetThisYear()
                        {
                            AcNo = Sm.DrStr(dr, c[0]),
                            RequestedAmt = Sm.DrDec(dr, c[1]),
                            UsedAmt = Sm.DrDec(dr, c[2]),
                            BudgetAmt = Sm.DrDec(dr, c[3]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessCOARptProfitLoss(ref List<COARptProfitLoss> l, ref List<COAJournalProfitLoss> l2, ref List<COABudgetMonthToDate> l3, ref List<COABudgetThisYear> l4)
        {
            for (int i = 0; i < l.Count(); i++)
            {
                //proses journal
                for (int j = 0; j < l2.Count(); j++)
                {
                    if (l2[j].AcNo.Length >= l[i].AcNo.Length)
                    {
                        if (l2[j].AcNo.Substring(0, l[i].AcNo.Length) == l[i].AcNo)
                        {
                            if (l2[j].PrevYrInd)
                                l[i].Amt1 += l2[j].Amt;
                            else
                                l[i].Amt2 += l2[j].Amt;
                        }
                    }
                }

                //proses budget month to date
                for (int j = 0; j < l3.Count(); j++)
                {
                    if (l3[j].AcNo.Length >= l[i].AcNo.Length)
                    {
                        if (l3[j].AcNo.Substring(0, l[i].AcNo.Length) == l[i].AcNo)
                        {
                            l[i].Amt3 += l3[j].RequestedAmt;
                            l[i].Amt4 += l3[j].UsedAmt;
                            l[i].Amt5 += l3[j].BudgetAmt;
                        }
                    }
                }

                //proses budget this year
                for (int j = 0; j < l4.Count(); j++)
                {
                    if (l4[j].AcNo.Length >= l[i].AcNo.Length)
                    {
                        if (l4[j].AcNo.Substring(0, l[i].AcNo.Length) == l[i].AcNo)
                        {
                            l[i].Amt6 += l4[j].RequestedAmt;
                            l[i].Amt7 += l4[j].UsedAmt;
                            l[i].Amt8 += l4[j].BudgetAmt;
                        }
                    }
                }
            }
        }

        private void ProcessRptProfitLoss(ref List<COARptProfitLoss> l, ref List<RptProfitLoss> l1)
        {
            foreach (var x in l.OrderBy(o => o.RptId))
            {
                foreach (var y in l1.OrderBy(o => o.RptId))
                {
                    if (y.RptId == x.RptId)
                    {
                        y.Amt1 += x.Amt1;
                        y.Amt2 += x.Amt2;
                        y.Amt3 += x.Amt3;
                        y.Amt4 += x.Amt4;
                        y.Amt5 += x.Amt5;
                        y.Amt6 += x.Amt6;
                        y.Amt7 += x.Amt7;
                        y.Amt8 += x.Amt8;
                    }
                }
            }
        }

        private void ProcessFormulaCalculation(ref List<RptProfitLoss> l1)
        {
            char[] delimiters = { '+', '-' };

            foreach (var x in l1.Where(w => w.Formula.Length > 0))
            {
                string[] ArraySQLFormula = x.Formula.Split(delimiters);
                string Formula = x.Formula, Formula2 = x.Formula, Formula3 = x.Formula, Formula4 = x.Formula,
                       Formula5 = x.Formula, Formula6 = x.Formula, Formula7 = x.Formula, Formula8 = x.Formula;
                var lFormula = new List<Formula>();
                ArraySQLFormula = ArraySQLFormula.Where(val => val != "").ToArray();

                for (int i = 0; i < ArraySQLFormula.Count(); i++)
                {
                    Formula = Formula.Replace(ArraySQLFormula[i], "#" + ArraySQLFormula[i] + "#");
                    Formula2 = Formula2.Replace(ArraySQLFormula[i], "#" + ArraySQLFormula[i] + "#");
                    Formula3 = Formula3.Replace(ArraySQLFormula[i], "#" + ArraySQLFormula[i] + "#");
                    Formula4 = Formula4.Replace(ArraySQLFormula[i], "#" + ArraySQLFormula[i] + "#");
                    Formula5 = Formula5.Replace(ArraySQLFormula[i], "#" + ArraySQLFormula[i] + "#");
                    Formula6 = Formula6.Replace(ArraySQLFormula[i], "#" + ArraySQLFormula[i] + "#");
                    Formula7 = Formula7.Replace(ArraySQLFormula[i], "#" + ArraySQLFormula[i] + "#");
                    Formula8 = Formula8.Replace(ArraySQLFormula[i], "#" + ArraySQLFormula[i] + "#");

                    ArraySQLFormula[i] = ArraySQLFormula[i].Replace(ArraySQLFormula[i], "#" + ArraySQLFormula[i] + "#");
                    lFormula.Add(new Formula()
                    {
                        RptId = ArraySQLFormula[i],
                        Amt1 = 0m,
                        Amt2 = 0m,
                        Amt3 = 0m,
                        Amt4 = 0m,
                        Amt5 = 0m,
                        Amt6 = 0m,
                        Amt7 = 0m,
                        Amt8 = 0m,
                    });
                }

                for (int ind = 0; ind < lFormula.Count; ind++)
                {
                    for (int j = 0; j < l1.Count; j++)
                    {
                        if (lFormula[ind].RptId == "#" + l1[j].RptId + "#")
                        {
                            lFormula[ind].Amt1 = l1[j].Amt1;
                            lFormula[ind].Amt2 = l1[j].Amt2;
                            lFormula[ind].Amt3 = l1[j].Amt3;
                            lFormula[ind].Amt4 = l1[j].Amt4;
                            lFormula[ind].Amt5 = l1[j].Amt5;
                            lFormula[ind].Amt6 = l1[j].Amt6;
                            lFormula[ind].Amt7 = l1[j].Amt7;
                            lFormula[ind].Amt8 = l1[j].Amt8;
                        }
                    }
                }

                for (int i = 0; i < ArraySQLFormula.Count(); i++)
                {
                    for (int j = 0; j < lFormula.Count; j++)
                    {
                        if (ArraySQLFormula[i].ToString() == lFormula[j].RptId)
                        {
                            string oldS = ArraySQLFormula[j].ToString();
                            string newS = lFormula[j].Amt1.ToString(),
                                newS2 = lFormula[j].Amt2.ToString(),
                                newS3 = lFormula[j].Amt3.ToString(),
                                newS4 = lFormula[j].Amt4.ToString(),
                                newS5 = lFormula[j].Amt5.ToString(),
                                newS6 = lFormula[j].Amt6.ToString(),
                                newS7 = lFormula[j].Amt7.ToString(),
                                newS8 = lFormula[j].Amt8.ToString();

                            Formula = Formula.Replace(oldS, newS);
                            Formula2 = Formula2.Replace(oldS, newS2);
                            Formula3 = Formula3.Replace(oldS, newS3);
                            Formula4 = Formula4.Replace(oldS, newS4);
                            Formula5 = Formula5.Replace(oldS, newS5);
                            Formula6 = Formula6.Replace(oldS, newS6);
                            Formula7 = Formula7.Replace(oldS, newS7);
                            Formula8 = Formula8.Replace(oldS, newS8);
                        }
                    }
                }

                x.Amt1 = Decimal.Parse(Sm.GetValue("Select " + Formula + " "));
                x.Amt2 = Decimal.Parse(Sm.GetValue("Select " + Formula2 + " "));
                x.Amt3 = Decimal.Parse(Sm.GetValue("Select " + Formula3 + " "));
                x.Amt4 = Decimal.Parse(Sm.GetValue("Select " + Formula4 + " "));
                x.Amt5 = Decimal.Parse(Sm.GetValue("Select " + Formula5 + " "));
                x.Amt6 = Decimal.Parse(Sm.GetValue("Select " + Formula6 + " "));
                x.Amt7 = Decimal.Parse(Sm.GetValue("Select " + Formula7 + " "));
                x.Amt8 = Decimal.Parse(Sm.GetValue("Select " + Formula8 + " "));

                Array.Clear(ArraySQLFormula, 0, ArraySQLFormula.Count());
                lFormula.Clear();
            }
        }

        private void ProcessGrid(ref List<RptProfitLoss> l1)
        {
            Sm.ClearGrd(Grd1, false);
            int Row = 0;
            Grd1.BeginUpdate();

            foreach (var x in l1.OrderBy(o => o.RptId))
            {
                Grd1.Rows.Add();

                Grd1.Cells[Grd1.Rows.Count - 1, 0].Value = Row + 1;
                Grd1.Cells[Grd1.Rows.Count - 1, 1].Value = x.AcNo;
                Grd1.Cells[Grd1.Rows.Count - 1, 2].Value = x.AcName;
                Grd1.Cells[Grd1.Rows.Count - 1, 3].Value = Sm.FormatNum(x.Amt1, 0);
                Grd1.Cells[Grd1.Rows.Count - 1, 4].Value = Sm.FormatNum(x.Amt2, 0);
                Grd1.Cells[Grd1.Rows.Count - 1, 5].Value = Sm.FormatNum(x.Amt3, 0);
                Grd1.Cells[Grd1.Rows.Count - 1, 6].Value = Sm.FormatNum(x.Amt4, 0);
                Grd1.Cells[Grd1.Rows.Count - 1, 7].Value = Sm.FormatNum(x.Amt5, 0);
                Grd1.Cells[Grd1.Rows.Count - 1, 8].Value = Sm.FormatNum(x.Amt6, 0);
                Grd1.Cells[Grd1.Rows.Count - 1, 9].Value = Sm.FormatNum(x.Amt7, 0);
                Grd1.Cells[Grd1.Rows.Count - 1, 10].Value = Sm.FormatNum(x.Amt8, 0);
                Grd1.Cells[Grd1.Rows.Count - 1, 11].Value = Sm.FormatNum((x.Amt1 != 0 ? x.Amt2 / x.Amt1 : 0m) * 100, 0);
                Grd1.Cells[Grd1.Rows.Count - 1, 12].Value = Sm.FormatNum((x.Amt3 != 0 ? x.Amt2 / x.Amt3 : 0m) * 100, 0);
                Grd1.Cells[Grd1.Rows.Count - 1, 13].Value = Sm.FormatNum((x.Amt4 != 0 ? x.Amt2 / x.Amt4 : 0m) * 100, 0);
                Grd1.Cells[Grd1.Rows.Count - 1, 14].Value = Sm.FormatNum((x.Amt5 != 0 ? x.Amt2 / x.Amt5 : 0m) * 100, 0);
                Grd1.Cells[Grd1.Rows.Count - 1, 15].Value = Sm.FormatNum((x.Amt6 != 0 ? x.Amt2 / x.Amt6 : 0m) * 100, 0);
                Grd1.Cells[Grd1.Rows.Count - 1, 16].Value = Sm.FormatNum((x.Amt7 != 0 ? x.Amt2 / x.Amt7 : 0m) * 100, 0);
                Grd1.Cells[Grd1.Rows.Count - 1, 17].Value = Sm.FormatNum((x.Amt8 != 0 ? x.Amt2 / x.Amt8 : 0m) * 100, 0);
                
                if (x.ParentInd)
                    for (int i = 1; i < Grd1.Cols.Count; i++)
                        Grd1.Cells[Row, i].Font = new Font(Grd1.Font, FontStyle.Bold);

                Row += 1;
            }

            Grd1.EndUpdate();
        }

        public string ComputeUsedBudget(byte type, string DeptCode, string Yr, string Mth, string BCCode, bool IsBudgetYearly)
        {
            var Selection = new StringBuilder();
            var Closure = new StringBuilder();
            var Table = new StringBuilder();
            var SQL = new StringBuilder();

            if (mBudgetUsedFormula == "1")
            {
                #region Default Transaction

                Selection.AppendLine("Left Join ( ");
                Selection.AppendLine("    Select X1.DeptCode, X2.BCCode, Left(X3.DocDt, 4) Yr, ");
                if (!mIsBudget2YearlyFormat)
                    Selection.AppendLine("    Substring(X3.DocDt, 5, 2) Mth, ");
                else
                    Selection.AppendLine("    '00' As Mth, ");
                Selection.AppendLine("    Sum(X4.Amt) Amt ");

                Closure.AppendLine("    Group By X1.DeptCode, X2.BCCode, Left(X3.DocDt, 4) ");
                if (!mIsBudget2YearlyFormat)
                    Closure.AppendLine("    , Substring(X3.DocDt, 5, 2) ");

                Table.AppendLine(") TbXXX On A.BCCode = TbXXX.BCCode And A.DeptCode = TbXXX.DeptCode And A.Yr = TbXXX.Yr ");
                if (!mIsBudget2YearlyFormat)
                    Table.AppendLine("And A.Mth = TbXXX.Mth ");


                #region VR

                if (type == 1)
                {
                    SQL.AppendLine("    IfNull(( ");
                    SQL.AppendLine("        Select Sum(Amt) ");
                }
                else
                {
                    SQL.AppendLine(Selection.ToString()
                        .Replace("X1.", "T.")
                        .Replace("X2.", "T.")
                        .Replace("X3.", "T.")
                        .Replace("X4.", "T.")
                        );
                }
                SQL.AppendLine("From ( ");
                SQL.AppendLine("Select DeptCode, BCCode, Left(DocDt, 4) Yr,  ");
                if (!mIsBudget2YearlyFormat)
                    SQL.AppendLine("    Substring(DocDt, 5, 2) Mth, ");
                else
                    SQL.AppendLine("    '00' As Mth, ");
                SQL.AppendLine("Amt, DocNo, DocDt ");
                SQL.AppendLine("From TblVoucherRequestHdr ");
                SQL.AppendLine("Where 0 = 0 ");
                SQL.AppendLine("And Find_In_Set(DocType, ");
                SQL.AppendLine("IfNull((Select ParValue From TblParameter Where ParValue Is Not Null And Parcode = 'VoucherDocTypeBudget'), ''))  ");
                SQL.AppendLine("And CancelInd = 'N' ");
                SQL.AppendLine("And Status In('O', 'A') ");
                SQL.AppendLine("And ReqType Is Not Null ");
                SQL.AppendLine("And DeptCode2 Is null ");
                if (type == 1)
                {
                    SQL.AppendLine("        And DeptCode = @DeptCode ");
                    if (!mIsBudget2YearlyFormat)
                        SQL.AppendLine("        And Left(DocDt, 6) = Left(@DocDt, 6) ");
                    else
                        SQL.AppendLine("        And Left(DocDt, 4) = Left(@DocDt, 4) ");
                    SQL.AppendLine("        And IfNull(BCCode, '') = IfNull(@BCCode, '') ");
                }
                if (mIsBudgetTransactionCanUseOtherDepartmentBudget)
                {
                    SQL.AppendLine("Union all ");
                    SQL.AppendLine("Select DeptCode2 as DeptCode, BCCode, Left(DocDt, 4) Yr,  ");
                    if (!mIsBudget2YearlyFormat)
                        SQL.AppendLine("    Substring(DocDt, 5, 2) Mth, ");
                    else
                        SQL.AppendLine("    '00' As Mth, ");
                    SQL.AppendLine("Amt, DocNo, DocDt ");
                    SQL.AppendLine("From TblVoucherRequestHdr ");
                    SQL.AppendLine("Where 0 = 0 ");
                    SQL.AppendLine("And Find_In_Set(DocType, ");
                    SQL.AppendLine("IfNull((Select ParValue From TblParameter Where ParValue Is Not Null And Parcode = 'VoucherDocTypeBudget'), ''))  ");
                    SQL.AppendLine("And CancelInd = 'N' ");
                    SQL.AppendLine("And Status In('O', 'A') ");
                    SQL.AppendLine("And ReqType Is Not Null ");
                    SQL.AppendLine("And DeptCode2 Is Not null ");
                    if (type == 1)
                    {
                        SQL.AppendLine("        And DeptCode2 = @DeptCode ");
                        if (!mIsBudget2YearlyFormat)
                            SQL.AppendLine("        And Left(DocDt, 6) = Left(@DocDt, 6) ");
                        else
                            SQL.AppendLine("        And Left(DocDt, 4) = Left(@DocDt, 4) ");
                        SQL.AppendLine("        And IfNull(BCCode, '') = IfNull(@BCCode, '') ");
                    }
                }
                SQL.AppendLine(") T ");

                SQL.AppendLine("        Where 0 = 0 ");
                if (type == 1) SQL.AppendLine("        And T.DocNo <> @DocNo ");
                if (type == 1)
                {
                    SQL.AppendLine("    ), 0.00) * -1 ");
                    SQL.AppendLine("- ");
                }
                else
                {
                    SQL.AppendLine(Closure.ToString()
                        .Replace("X1.", "T.")
                        .Replace("X2.", "T.")
                        .Replace("X3.", "T.")
                        );
                    if (mIsBudgetTransactionCanUseOtherDepartmentBudget)
                    {
                        SQL.AppendLine(") Tb1 On A.BCCode = Tb1.BCCode And A.DeptCode = Tb1.DeptCode And A.Yr = Tb1.Yr ");
                        if (!mIsBudget2YearlyFormat)
                            SQL.AppendLine("And A.Mth = Tb1.Mth ");
                    }
                    else
                    {
                        SQL.AppendLine(Table.ToString().Replace("TbXXX", "Tb1"));
                    }
                }

                #endregion

                #region Travel Request All (Selain PHT)

                if (type == 1)
                {
                    SQL.AppendLine("    IfNull(( ");
                    SQL.AppendLine("        Select Sum(B.Amt1+B.Amt2+B.Amt3+B.Amt4+B.Amt5+B.Amt6+B.Detasering) ");
                }
                else
                {
                    SQL.AppendLine(Selection.ToString()
                        .Replace("X1.", "C.")
                        .Replace("X2.", "B.")
                        .Replace("X3.", "A.")
                        .Replace("X4.Amt", "B.Amt1 + B.Amt2 + B.Amt3 + B.Amt4 + B.Amt5 + B.Amt6 + B.Detasering")
                        );
                }
                SQL.AppendLine("        From TblTravelRequestHdr A ");
                SQL.AppendLine("        Inner Join TblTravelRequestDtl7 B On A.DocNo = B.DocNo ");
                SQL.AppendLine("        Inner Join TblEmployee C On B.PICCode = C.EmpCode ");
                SQL.AppendLine("        Where 0 = 0 ");
                if (type == 1) SQL.AppendLine("        And A.DocNo <> @DocNo ");
                SQL.AppendLine("        And A.CancelInd = 'N' ");
                SQL.AppendLine("        And A.Status In ('O', 'A') ");
                if (type == 1)
                {
                    SQL.AppendLine("        And C.DeptCode = @DeptCode ");
                    if (!mIsBudget2YearlyFormat)
                        SQL.AppendLine("        And Left(A.DocDt, 6) = Left(@DocDt, 6) ");
                    else
                        SQL.AppendLine("        And Left(A.DocDt, 4) = Left(@DocDt, 4) ");
                    SQL.AppendLine("        And IfNull(B.BCCode, '') = IfNull(@BCCode, '') ");
                }
                SQL.AppendLine("        And Not Exists (Select 1 From TblMenu Where Param = 'FrmTravelRequest5') ");
                if (type == 1)
                {
                    SQL.AppendLine("    ), 0.00) ");
                    SQL.AppendLine("- ");
                }
                else
                {
                    SQL.AppendLine(Closure.ToString()
                        .Replace("X1.", "C.")
                        .Replace("X2.", "B.")
                        .Replace("X3.", "A.")
                        );
                    SQL.AppendLine(Table.ToString().Replace("TbXXX", "Tb2"));
                }

                #endregion

                #region MR

                if (mMRAvailableBudgetSubtraction == "1")
                {
                    if (type == 1)
                    {
                        SQL.AppendLine("    IfNull(( ");
                        if (mIsMRShowEstimatedPrice && mIsBudgetCalculateFromEstimatedPrice)
                            SQL.AppendLine("        Select Sum(T.Qty*Case When T.EximInd = 'Y' Then T.UPrice Else T.EstPrice End) ");
                        else
                            SQL.AppendLine("        Select Sum(T.Qty*T.UPrice) ");
                    }
                    else
                    {
                        SQL.AppendLine(Selection.ToString()
                        .Replace("X1.", "T.")
                        .Replace("X2.", "T.")
                        .Replace("X3.", "T.")
                        .Replace("X4.Amt", (mIsMRShowEstimatedPrice && mIsBudgetCalculateFromEstimatedPrice) ? "T.Qty * Case When T.EximInd = 'Y' Then T.UPrice Else T.EstPrice End" : "T.Qty * T.UPrice")
                        );
                    }

                    SQL.AppendLine("From ( ");
                    SQL.AppendLine("Select DeptCode, BCCode, Left(DocDt, 4) Yr,  ");
                    if (!mIsBudget2YearlyFormat)
                        SQL.AppendLine("    Substring(DocDt, 5, 2) Mth, ");
                    else
                        SQL.AppendLine("    '00' As Mth, ");
                    SQL.AppendLine("B.Qty, A.EximInd, B.UPrice, B.EstPrice, DocDt ");
                    SQL.AppendLine("From TblMaterialRequestHdr A, TblMaterialRequestDtl B ");
                    SQL.AppendLine("Where A.DocNo=B.DocNo ");
                    SQL.AppendLine("And A.ReqType='1' ");
                    SQL.AppendLine("And B.CancelInd='N' ");
                    SQL.AppendLine("And B.Status<>'C' ");
                    if (mIsBudgetUseMRProcessInd) SQL.AppendLine("And A.ProcessInd = 'F'  ");
                    SQL.AppendLine("And DeptCode2 Is null ");
                    if (type == 1)
                    {
                        if (mBudgetBasedOn == "1")
                        {
                            SQL.AppendLine(" And A.DeptCode=@DeptCode  ");
                        }
                        if (mBudgetBasedOn == "2")
                            SQL.AppendLine("        And A.SiteCode=@DeptCode ");
                        if (mIsMRBudgetBasedOnBudgetCategory)
                        {
                            SQL.AppendLine("        And A.BCCode=@BCCode ");
                        }
                        if (!mIsBudget2YearlyFormat)
                            SQL.AppendLine("        And Left(A.DocDt, 6)=Left(@DocDt, 6) ");
                        else
                            SQL.AppendLine("        And Left(A.DocDt, 4)=Left(@DocDt, 4) ");
                        SQL.AppendLine("       And A.DocNo<>@DocNo ");
                    }
                    if (mIsBudgetTransactionCanUseOtherDepartmentBudget)
                    {
                        SQL.AppendLine("Union all ");
                        SQL.AppendLine("Select DeptCode2 as DeptCode, BCCode, Left(DocDt, 4) Yr,  ");
                        if (!mIsBudget2YearlyFormat)
                            SQL.AppendLine("    Substring(DocDt, 5, 2) Mth, ");
                        else
                            SQL.AppendLine("    '00' As Mth, ");
                        SQL.AppendLine("B.Qty, A.EximInd, B.UPrice, B.EstPrice, DocDt ");
                        SQL.AppendLine("From TblMaterialRequestHdr A, TblMaterialRequestDtl B ");
                        SQL.AppendLine("Where A.DocNo=B.DocNo ");
                        SQL.AppendLine("And A.ReqType='1' ");
                        SQL.AppendLine("And B.CancelInd='N' ");
                        SQL.AppendLine("And B.Status<>'C' ");
                        if (mIsBudgetUseMRProcessInd) SQL.AppendLine("And A.ProcessInd = 'F'  ");
                        SQL.AppendLine("And DeptCode2 Is Not null ");
                        if (type == 1)
                        {
                            if (mBudgetBasedOn == "1")
                            {
                                SQL.AppendLine(" And A.DeptCode2=@DeptCode  ");
                            }
                            if (mBudgetBasedOn == "2")
                                SQL.AppendLine("        And A.SiteCode=@DeptCode ");
                            if (mIsMRBudgetBasedOnBudgetCategory)
                            {
                                SQL.AppendLine("        And A.BCCode=@BCCode ");
                            }
                            if (!mIsBudget2YearlyFormat)
                                SQL.AppendLine("        And Left(A.DocDt, 6)=Left(@DocDt, 6) ");
                            else
                                SQL.AppendLine("        And Left(A.DocDt, 4)=Left(@DocDt, 4) ");
                            SQL.AppendLine("       And A.DocNo<>@DocNo ");
                        }
                    }
                    SQL.AppendLine(") T ");

                    if (type == 1)
                    {
                        SQL.AppendLine("    ), 0.00) ");
                        SQL.AppendLine("+ ");
                    }
                    else
                    {
                        SQL.AppendLine(Closure.ToString()
                       .Replace("X1.", "T.")
                       .Replace("X2.", "T.")
                       .Replace("X3.", "T.")
                       );

                        if (mIsBudgetTransactionCanUseOtherDepartmentBudget)
                        {
                            SQL.AppendLine(") Tb3 On A.BCCode = Tb3.BCCode And A.DeptCode = Tb3.DeptCode And A.Yr = Tb3.Yr ");
                            if (!mIsBudget2YearlyFormat)
                                SQL.AppendLine("And A.Mth = Tb3.Mth ");
                        }
                        else
                        {
                            SQL.AppendLine(Table.ToString().Replace("TbXXX", "Tb3"));
                        }
                    }
                }

                if (mMRAvailableBudgetSubtraction == "2")
                {
                    var AmtTxt = new StringBuilder();

                    AmtTxt.AppendLine("    X2.Amt -  ");
                    AmtTxt.AppendLine("    ( ");
                    AmtTxt.AppendLine("      IfNull( X1.DiscountAmt *  ");
                    AmtTxt.AppendLine("          Case When X1.CurCode = @MainCurCode Then 1.00 Else ");
                    AmtTxt.AppendLine("          IfNull(( ");
                    AmtTxt.AppendLine("              Select Amt From TblCurrencyRate  ");
                    AmtTxt.AppendLine("              Where RateDt<=X1.DocDt And CurCode1=X1.CurCode And CurCode2=@MainCurCode  ");
                    AmtTxt.AppendLine("              Order By RateDt Desc Limit 1  ");
                    AmtTxt.AppendLine("        ), 0.00) End ");
                    AmtTxt.AppendLine("      , 0.00) ");
                    AmtTxt.AppendLine("    ) ");

                    if (type == 1)
                    {
                        SQL.AppendLine("    IfNull(( ");

                        #region Old Code

                        //SQL.AppendLine("        Select Sum(E.Qty*D.UPrice)  ");
                        //SQL.AppendLine("        From TblMaterialRequestHdr A  ");
                        //SQL.AppendLine("        Inner Join TblMaterialRequestDtl B on A.Docno = B.DocNo ");
                        //SQL.AppendLine("        Inner Join TblPORequestDtl C  On B.DocNo=C.MaterialRequestDocNo And B.DNo = C.MaterialRequestDNo ");
                        //SQL.AppendLine("        Inner Join TblQTDtl D On D.DocNo = C.QtDocNo And D.DNo = C.QtDNo ");
                        //SQL.AppendLine("        Inner Join TblPODtl E on E.PORequestDocNo = C.DocNo And E.PORequestDNo = C.DNo And E.CancelInd = 'N' ");

                        //if (mBudgetBasedOn == "1")
                        //    SQL.AppendLine("        And A.DeptCode=@DeptCode ");
                        //if (mBudgetBasedOn == "2")
                        //    SQL.AppendLine("        And A.SiteCode=@DeptCode ");
                        //SQL.AppendLine("        And A.ReqType='1' ");
                        //SQL.AppendLine("        And B.CancelInd='N' ");
                        //SQL.AppendLine("        And B.Status<>'C' ");
                        //SQL.AppendLine("        And Left(A.DocDt, 6)=Left(@DocDt, 6) ");
                        //SQL.AppendLine("       And A.DocNo<>@DocNo ");

                        #endregion

                        SQL.AppendLine("    Select Sum( ");
                        SQL.AppendLine(AmtTxt.ToString());
                        SQL.AppendLine("    ) ");
                    }
                    else
                    {
                        SQL.AppendLine(Selection.ToString()
                        .Replace("X1.", "X2.")
                        .Replace("X2.", "X2.")
                        .Replace("X3.", "X2.")
                        .Replace("X4.Amt", AmtTxt.ToString())
                        );
                    }
                    SQL.AppendLine("    From TblPOHdr X1 ");
                    SQL.AppendLine("    Inner Join ( ");
                    SQL.AppendLine("        Select A.DeptCode, E.DocNo, ");
                    if (type == 2)
                        SQL.AppendLine("        Left(A.DocDt, 6) DocDt, A.BCCode, ");
                    SQL.AppendLine("        Sum( ");
                    SQL.AppendLine("        ((((100.00-D.Discount)*0.01)*(D.Qty*G.UPrice))-D.DiscountAmt+D.RoundingValue)  ");
                    SQL.AppendLine("        * Case When F.CurCode=@MainCurCode Then 1.00 Else  ");
                    SQL.AppendLine("        IfNull((  ");
                    SQL.AppendLine("            Select Amt From TblCurrencyRate  ");
                    SQL.AppendLine("            Where RateDt<=E.DocDt And CurCode1=F.CurCode And CurCode2=@MainCurCode  ");
                    SQL.AppendLine("            Order By RateDt Desc Limit 1  ");
                    SQL.AppendLine("        ), 0.00) End  ");
                    SQL.AppendLine("        ) As Amt  ");
                    SQL.AppendLine("        From TblMaterialRequestHdr A  ");
                    SQL.AppendLine("        Inner Join TblMaterialRequestDtl B  ");
                    SQL.AppendLine("            On A.DocNo=B.DocNo ");
                    if (type == 1)
                    {
                        if (!mIsBudget2YearlyFormat)
                            SQL.AppendLine("            And Left(A.DocDt, 6)=Left(@DocDt, 6) ");
                        else
                            SQL.AppendLine("            And Left(A.DocDt, 4)=Left(@DocDt, 4) ");
                        SQL.AppendLine("            And A.DocNo <> @DocNo ");
                    }
                    SQL.AppendLine("            And B.CancelInd='N'  ");
                    SQL.AppendLine("            And B.Status In ('A', 'O')  ");
                    //SQL.AppendLine("            And A.Status='A'  ");
                    SQL.AppendLine("            And A.Reqtype='1'   ");

                    if (type == 1)
                    {
                        if (mBudgetBasedOn == "1")
                            SQL.AppendLine("        And A.DeptCode=@DeptCode  ");
                        if (mBudgetBasedOn == "2")
                            SQL.AppendLine("        And A.SiteCode=@DeptCode ");
                        if (mIsMRBudgetBasedOnBudgetCategory)
                            SQL.AppendLine("        And A.BCCode=@BCCode ");
                    }

                    SQL.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.CancelInd='N' And C.Status='A'  ");
                    SQL.AppendLine("        Inner Join TblPODtl D On C.DocNo=D.PORequestDocNo And C.DNo=D.PORequestDNo And D.CancelInd='N'  ");
                    SQL.AppendLine("        Inner Join TblPOHdr E On D.DocNo=E.DocNo And E.Status='A'  ");
                    SQL.AppendLine("        Inner Join TblQtHdr F On C.QtDocNo=F.DocNo  ");
                    SQL.AppendLine("        Inner Join TblQtDtl G On C.QtDocNo=G.DocNo And C.QtDNo=G.DNo  ");
                    SQL.AppendLine("        Group By A.DeptCode, E.DocNo ");
                    if (type == 2)
                        SQL.AppendLine("        , Left(A.DocDt, 6), A.BCCode ");
                    SQL.AppendLine("    ) X2 On X1.DocNo = X2.DocNo ");
                    SQL.AppendLine("    Group By X2.DeptCode ");
                    if (type == 2)
                        SQL.AppendLine("    , X2.BCCode, X2.DocDt ");
                    if (type == 1)
                    {
                        SQL.AppendLine("    ),0.00) ");
                        SQL.AppendLine("+ ");
                    }
                    else
                    {
                        SQL.AppendLine(Table.ToString().Replace("TbXXX", "Tb3"));
                    }
                }

                #endregion

                #region PO Qty Cancel

                if (type == 1)
                {
                    SQL.AppendLine("    IfNull(( ");
                    SQL.AppendLine("        Select Sum(A.Qty*D.UPrice) ");
                }
                else
                {
                    SQL.AppendLine(Selection.ToString()
                        .Replace("X1.", "E.")
                        .Replace("X2.", "E.")
                        .Replace("X3.", "A.")
                        .Replace("X4.Amt", "A.Qty * D.UPrice")
                        );
                }

                SQL.AppendLine("        From TblPOQtyCancel A ");
                SQL.AppendLine("        Inner Join TblPODtl B On A.PODocNo=B.DocNo And A.PODNo=B.DNo ");
                SQL.AppendLine("        Inner Join TblPORequestDtl C On B.PORequestDocNo=C.DocNo And B.PORequestDNo=C.DNo ");
                SQL.AppendLine("        Inner Join TblMaterialRequestDtl D On C.MaterialRequestDocNo=D.DocNo And C.MaterialRequestDNo=D.DNo ");
                SQL.AppendLine("        Inner Join TblMaterialRequestHdr E ");
                SQL.AppendLine("            On D.DocNo=E.DocNo  ");
                if (type == 1)
                {
                    if (mBudgetBasedOn == "1")
                        SQL.AppendLine("        And E.DeptCode=@DeptCode ");
                    if (mBudgetBasedOn == "2")
                        SQL.AppendLine("        And E.SiteCode=@DeptCode ");
                    if (mIsMRBudgetBasedOnBudgetCategory)
                        SQL.AppendLine("        And E.BCCode=@BCCode ");
                }

                SQL.AppendLine("            And E.ReqType='1' ");
                if (type == 1)
                {
                    if (!mIsBudget2YearlyFormat)
                        SQL.AppendLine("            And Left(E.DocDt, 6)=Left(@DocDt, 6) ");
                    else
                        SQL.AppendLine("            And Left(E.DocDt, 4)=Left(@DocDt, 4) ");
                    SQL.AppendLine("            And E.DocNo<>@DocNo ");
                }
                SQL.AppendLine("        Where A.CancelInd='N' ");
                if (type == 1)
                {
                    SQL.AppendLine("    ),0.00) ");
                    SQL.AppendLine("+ ");
                }
                else
                {
                    SQL.AppendLine(Closure.ToString()
                            .Replace("X1.", "E.")
                            .Replace("X2.", "E.")
                            .Replace("X3.", "A.")
                            );
                    SQL.AppendLine(Table.ToString().Replace("TbXXX", "Tb5"));
                }

                #endregion

                #region VC

                if (type == 1)
                {
                    SQL.AppendLine("IfNull(( ");
                    SQL.AppendLine("    Select Sum(T.Amt) Amt ");
                }
                else
                {
                    SQL.AppendLine(Selection.ToString()
                        .Replace("X1.", "T.")
                        .Replace("X2.", "T.")
                        .Replace("X3.", "T.")
                        .Replace("X4.", "T.")
                        );
                }
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select Case when A.AcType = 'D' Then IfNull(A.Amt, 0.00) Else IfNull(A.Amt*-1, 0.00) END As Amt ");
                if (type == 2)
                    SQL.AppendLine("        , D.DeptCode, D.BCCode, Left(A.DocDt, 6) DocDt ");
                SQL.AppendLine("        From TblVoucherHdr A  ");
                SQL.AppendLine("        Inner Join  ");
                SQL.AppendLine("        ( ");
                SQL.AppendLine("            Select X1.DocNo, X1.VoucherRequestDocNo   ");
                SQL.AppendLine("            From TblVoucherHdr X1  ");
                SQL.AppendLine("            Inner Join TblVoucherRequestHdr X2 ON X2.DocNo = X1.VoucherRequestDocNo  ");
                SQL.AppendLine("                And X1.DocType = '58' ");
                SQL.AppendLine("                AND X1.CancelInd = 'N'  ");
                SQL.AppendLine("                AND X2.Status In ('O', 'A')  ");
                SQL.AppendLine("        ) B ON A.DocNo = B.DocNo ");
                SQL.AppendLine("        Inner Join TblCashAdvanceSettlementHdr C On B.VoucherRequestDocNo = C.VoucherRequestDocNo ");
                SQL.AppendLine("        Inner Join ");
                SQL.AppendLine("        ( ");
                SQL.AppendLine("            Select X2.DocNo, X2.VoucherDocNo, X3.DeptCode, X3.BCCode ");
                SQL.AppendLine("            From TblVoucherHdr X1  ");
                SQL.AppendLine("            Inner Join TblCashAdvanceSettlementDtl X2 ON X1.DocNo = X2.VoucherDocNo ");
                SQL.AppendLine("            INNER JOIN TblVoucherRequestHdr X3 ON X1.VoucherRequestDocNo = X3.DocNo ");
                SQL.AppendLine("                AND X1.DocType = '56' ");
                SQL.AppendLine("                AND X1.CancelInd = 'N' ");
                SQL.AppendLine("                AND X3.Status In ('O', 'A') ");
                SQL.AppendLine("        ) D ON C.DocNo = D.DocNo ");
                SQL.AppendLine("        Where 0 = 0 ");
                if (type == 1)
                {
                    SQL.AppendLine("        And A.DocNo <> @DocNo ");
                    SQL.AppendLine("        And D.DeptCode = @DeptCode ");
                    if (!mIsBudget2YearlyFormat)
                        SQL.AppendLine("        And Left(A.DocDt, 6) = Left(@DocDt, 6) ");
                    else
                        SQL.AppendLine("        And Left(A.DocDt, 4) = Left(@DocDt, 4) ");
                    SQL.AppendLine("        And IfNull(D.BCCode, '') = IfNull(@BCCode, '')  ");
                }
                SQL.AppendLine("    ) T ");

                if (type == 1)
                    SQL.AppendLine("), 0.00 ) ");
                else
                {
                    SQL.AppendLine(Closure.ToString()
                            .Replace("X1.", "T.")
                        .Replace("X2.", "T.")
                        .Replace("X3.", "T.")
                            );
                    SQL.AppendLine(Table.ToString().Replace("TbXXX", "Tb6"));
                }

                #endregion

                #region CAS

                if (mIsCASUsedForBudget)
                {
                    if (type == 1)
                    {
                        SQL.AppendLine("- ");
                        SQL.AppendLine(" IfNull(( ");
                        SQL.AppendLine("    Select Sum(T.Amt) Amt ");
                    }
                    else
                    {
                        SQL.AppendLine(Selection.ToString()
                        .Replace("X1.", "T.")
                        .Replace("X2.", "T.")
                        .Replace("X3.", "T.")
                        .Replace("X4.", "T.")
                        );
                    }
                    SQL.AppendLine("From ( ");
                    SQL.AppendLine("        Select Left(A.DocDt, 4) Yr, Substring(A.DocDt, 5, 2) As Mth, C.BCCode, C.DeptCode, B.Amt, A.DocDt ");
                    SQL.AppendLine("        From TblCashAdvanceSettlementHdr A ");
                    SQL.AppendLine("        Inner Join TblCashAdvanceSettlementDtl B On A.DocNo = B.DocNo ");
                    SQL.AppendLine("            And A.`Status` = 'A' And A.CancelInd = 'N' ");
                    SQL.AppendLine("            And A.DocStatus = 'F' ");
                    SQL.AppendLine("            And B.CCtCode Is Not Null ");
                    if (type == 1)
                    {
                        if (!mIsBudget2YearlyFormat)
                            SQL.AppendLine("            And Left(A.DocDt, 6) = Left(@DocDt, 6) ");
                        else
                            SQL.AppendLine("            And Left(A.DocDt, 4) = Left(@DocDt, 4) ");
                    }
                    SQL.AppendLine("        Inner Join TblBudgetCategory C On B.CCtCode = C.CCtCode ");
                    SQL.AppendLine("            And C.CCtCode Is Not Null ");
                    if (type == 1)
                    {
                        SQL.AppendLine("            And C.BCCode = @BCCode ");
                        SQL.AppendLine("            And C.DeptCOde = @DeptCode ");
                        SQL.AppendLine("        Where 0 = 0 ");
                        SQL.AppendLine("        And A.DocNo != @DocNo ");
                    }
                    SQL.AppendLine("    ) T ");
                    if (type == 1)
                        SQL.AppendLine("), 0.00) ");
                    else
                    {
                        SQL.AppendLine(Closure.ToString()
                            .Replace("X1.", "T.")
                        .Replace("X2.", "T.")
                        .Replace("X3.", "T.")
                            );
                        SQL.AppendLine(Table.ToString().Replace("TbXXX", "Tb7"));
                    }
                }

                #endregion

                #region OP

                if (mIsOutgoingPaymentUseBudget)
                {
                    if (type == 1)
                    {
                        SQL.AppendLine("- ");
                        SQL.AppendLine(" IfNull(( ");
                        SQL.AppendLine("    Select Sum(A.Amt) Amt ");
                    }
                    else
                    {
                        SQL.AppendLine(Selection.ToString()
                       .Replace("X1.", "B.")
                       .Replace("X2.", "A.")
                       .Replace("X3.", "A.")
                       .Replace("X4.", "A.")
                       );
                    }
                    SQL.AppendLine("    From TblOutgoingPaymentHdr A ");
                    SQL.AppendLine("    Inner Join ");
                    SQL.AppendLine("    ( ");
                    SQL.AppendLine("        Select T1.DocNo, IfNull(T3.DeptCode, IfNull(T4.DeptCode, '')) DeptCode ");
                    SQL.AppendLine("        From TblOutgoingPaymentHdr T1 ");
                    SQL.AppendLine("        Inner Join TblOutgoingPaymentDtl T2 On T1.DocNo = T2.DocNo ");
                    SQL.AppendLine("        Left Join TblPurchaseInvoiceHdr T3 On T2.InvoiceDocNo = T3.DocNo ");
                    SQL.AppendLine("        Left Join TblPurchaseReturnInvoiceHdr T4 On T2.InvoiceDocNo = T4.DocNo ");
                    SQL.AppendLine("        Where T2.DNo = '001' ");
                    SQL.AppendLine("    ) B On A.DocNo = B.DocNo ");
                    SQL.AppendLine("    Where A.BCCode Is Not Null ");
                    if (type == 1)
                    {
                        SQL.AppendLine("    And A.BCCode = @BCCode ");
                        if (!mIsBudget2YearlyFormat)
                            SQL.AppendLine("    And Left(A.DocDt, 6) = Left(@DocDt, 6) ");
                        else
                            SQL.AppendLine("    And Left(A.DocDt, 4) = Left(@DocDt, 4) ");
                        SQL.AppendLine("    And B.DeptCode = @DeptCode ");
                        SQL.AppendLine("    And A.DocNo != @DocNo ");
                    }
                    SQL.AppendLine("    And A.CancelInd = 'N' ");
                    SQL.AppendLine("    And A.Status In ('O', 'A') ");
                    if (type == 1)
                        SQL.AppendLine("), 0.00) ");
                    else
                    {
                        SQL.AppendLine(Closure.ToString()
                            .Replace("X1.", "B.")
                       .Replace("X2.", "A.")
                       .Replace("X3.", "A.")
                            );
                        SQL.AppendLine(Table.ToString().Replace("TbXXX", "Tb8"));
                    }
                }

                #endregion

                #region EmpClaim

                if (mIsEmpClaimRequestUseBudget)
                {
                    if (type == 1)
                    {
                        SQL.AppendLine("- ");
                        SQL.AppendLine(" IfNull(( ");
                        SQL.AppendLine("    Select Sum(A.Amt) Amt ");
                    }
                    else
                    {
                        SQL.AppendLine(Selection.ToString()
                       .Replace("X1.", "B.")
                       .Replace("X2.", "A.")
                       .Replace("X3.", "A.")
                       .Replace("X4.", "A.")
                       );
                    }

                    SQL.AppendLine("    From TblEmpClaimHdr A ");
                    SQL.AppendLine("    Inner Join TblEmployee B On A.EmpCode = B.EmpCode ");
                    SQL.AppendLine("    Where A.BCCode Is Not Null ");
                    if (type == 1)
                    {
                        SQL.AppendLine("    And A.BCCode = @BCCode ");
                        SQL.AppendLine("    And B.DeptCode = @DeptCode ");
                    }
                    SQL.AppendLine("    And A.CancelInd = 'N' ");
                    SQL.AppendLine("    And A.Status In ('O', 'A') ");
                    if (type == 1)
                    {
                        SQL.AppendLine("    And A.DocNo != @DocNo ");
                        if (!mIsBudget2YearlyFormat)
                            SQL.AppendLine("    And Left(A.DocDt, 6) = Left(@DocDt, 6) ");
                        else
                            SQL.AppendLine("    And Left(A.DocDt, 4) = Left(@DocDt, 4) ");
                    }
                    if (type == 1)
                        SQL.AppendLine("), 0.00) ");
                    else
                    {
                        SQL.AppendLine(Closure.ToString()
                            .Replace("X1.", "B.")
                       .Replace("X2.", "A.")
                       .Replace("X3.", "A.")
                            );
                        SQL.AppendLine(Table.ToString().Replace("TbXXX", "Tb9"));
                    }
                }

                #endregion

                #region VRSS

                if (mIsVoucherRequestSSUseBudget)
                {
                    if (type == 1)
                    {
                        SQL.AppendLine("- ");
                        SQL.AppendLine(" IfNull(( ");
                        SQL.AppendLine("    Select Sum(A.Amt) Amt ");
                    }
                    else
                    {
                        SQL.AppendLine(Selection.ToString()
                       .Replace("X1.", "A.")
                       .Replace("X2.", "A.")
                       .Replace("X3.", "A.")
                       .Replace("X4.", "A.")
                       );
                    }
                    SQL.AppendLine("    From TblVoucherRequestSSHdr A ");
                    SQL.AppendLine("    Where A.BCCode Is Not Null ");
                    if (type == 1) SQL.AppendLine("    And A.BCCode = @BCCode ");
                    SQL.AppendLine("    And A.CancelInd = 'N' ");
                    if (type == 1)
                    {
                        SQL.AppendLine("    And A.DocNo != @DocNo ");
                        SQL.AppendLine("    And A.DeptCode = @DeptCode ");
                    }
                    SQL.AppendLine("    And A.Status In ('O', 'A') ");
                    if (type == 1)
                    {
                        if (!mIsBudget2YearlyFormat)
                            SQL.AppendLine("    And Left(A.DocDt, 6) = Left(@DocDt, 6) ");
                        else
                            SQL.AppendLine("    And Left(A.DocDt, 4) = Left(@DocDt, 4) ");
                    }
                    if (type == 1)
                        SQL.AppendLine("), 0.00) ");
                    else
                    {
                        SQL.AppendLine(Closure.ToString()
                            .Replace("X1.", "A.")
                       .Replace("X2.", "A.")
                       .Replace("X3.", "A.")
                            );
                        SQL.AppendLine(Table.ToString().Replace("TbXXX", "Tb10"));
                    }
                }

                #endregion

                #region VRPayroll

                if (mIsVoucherRequestPayrollUseBudget)
                {
                    if (type == 1)
                    {
                        SQL.AppendLine("- ");
                        SQL.AppendLine(" IfNull(( ");
                        SQL.AppendLine("    Select Sum(A.Amt) Amt ");
                    }
                    else
                    {
                        SQL.AppendLine(Selection.ToString()
                       .Replace("X1.", "A.")
                       .Replace("X2.", "A.")
                       .Replace("X3.", "A.")
                       .Replace("X4.", "A.")
                       );
                    }
                    SQL.AppendLine("    From TblVoucherRequestPayrollHdr A ");
                    SQL.AppendLine("    Where A.BCCode Is Not Null ");
                    if (type == 1)
                    {
                        SQL.AppendLine("    And A.BCCode = @BCCode ");
                        SQL.AppendLine("    And A.DeptCode = @DeptCode ");
                        SQL.AppendLine("    And A.DocNo != @DocNo ");
                        if (!mIsBudget2YearlyFormat)
                            SQL.AppendLine("    And Left(A.DocDt, 6) = Left(@DocDt, 6) ");
                        else
                            SQL.AppendLine("    And Left(A.DocDt, 4) = Left(@DocDt, 4) ");
                    }
                    SQL.AppendLine("    And A.CancelInd = 'N' ");
                    SQL.AppendLine("    And A.Status In ('O', 'A') ");
                    if (type == 1)
                        SQL.AppendLine("), 0.00) ");
                    else
                    {
                        SQL.AppendLine(Closure.ToString()
                            .Replace("X1.", "A.")
                       .Replace("X2.", "A.")
                       .Replace("X3.", "A.")
                            );
                        SQL.AppendLine(Table.ToString().Replace("TbXXX", "Tb11"));
                    }
                }

                #endregion

                #region Travel Request PHT

                if (mIsTravelRequestUseBudget)
                {
                    if (type == 1)
                    {
                        SQL.AppendLine("    - IfNull(( ");
                        SQL.AppendLine("        Select Sum(B.Amt1+B.Amt2+B.Amt3+B.Amt4+B.Amt5+B.Amt6+B.Detasering) ");
                    }
                    else
                    {
                        SQL.AppendLine(Selection.ToString()
                            .Replace("X1.", "C.")
                            .Replace("X2.", "B.")
                            .Replace("X3.", "A.")
                            .Replace("X4.Amt", "B.Amt1 + B.Amt2 + B.Amt3 + B.Amt4 + B.Amt5 + B.Amt6 + B.Detasering")
                            );
                    }
                    SQL.AppendLine("        From TblTravelRequestHdr A ");
                    SQL.AppendLine("        Inner Join TblTravelRequestDtl7 B On A.DocNo = B.DocNo ");
                    SQL.AppendLine("        Inner Join TblEmployee C On A.PICCode = C.EmpCode ");
                    SQL.AppendLine("        Where 0 = 0 ");
                    if (type == 1) SQL.AppendLine("        And A.DocNo <> @DocNo ");
                    SQL.AppendLine("        And A.CancelInd = 'N' ");
                    SQL.AppendLine("        And A.Status In ('O', 'A') ");
                    if (type == 1)
                    {
                        SQL.AppendLine("        And C.DeptCode = @DeptCode ");
                        if (!mIsBudget2YearlyFormat)
                            SQL.AppendLine("        And Left(A.DocDt, 6) = Left(@DocDt, 6) ");
                        else
                            SQL.AppendLine("        And Left(A.DocDt, 4) = Left(@DocDt, 4) ");
                        SQL.AppendLine("        And IfNull(A.BCCode, '') = IfNull(@BCCode, '') ");
                    }
                    SQL.AppendLine("        And Exists (Select 1 From TblMenu Where Param = 'FrmTravelRequest5') ");
                    if (type == 1)
                    {
                        SQL.AppendLine("    ), 0.00) ");
                    }
                    else
                    {
                        SQL.AppendLine(Closure.ToString()
                            .Replace("X1.", "C.")
                            .Replace("X2.", "B.")
                            .Replace("X3.", "A.")
                            );
                        SQL.AppendLine(Table.ToString().Replace("TbXXX", "Tb12"));
                    }
                }

                #endregion

                #endregion
            }
            else
            {
                #region based on Journal

                if (type == 1)
                {
                    SQL.AppendLine("    IfNull(( ");
                    SQL.AppendLine("        Select Sum(Case T5.AcType When 'D' Then (T2.DAmt-T2.CAmt) Else (T2.CAmt - T2.DAmt)) Amt ");
                }
                else
                {
                    SQL.AppendLine("Left Join ( ");
                    SQL.AppendLine("    Select T3.DeptCode, T4.BCCode, Left(T1.DocDt, 4) Yr, ");
                    if (!mIsBudget2YearlyFormat)
                        SQL.AppendLine("    Substring(T1.DocDt, 5, 2) Mth, ");
                    else
                        SQL.AppendLine("    '00' As Mth, ");
                    SQL.AppendLine("    Sum(Case T5.AcType When 'D' Then (T2.DAmt-T2.CAmt) Else (T2.CAmt - T2.DAmt) End) Amt ");
                }

                SQL.AppendLine("From TblJournalHdr T1 ");
                SQL.AppendLine("Inner Join TblJournalDtl T2 On T1.DocNo = T2.DocNo ");
                SQL.AppendLine("    And T1.CancelInd = 'N' ");

                if (Yr.Length > 0 && Mth.Length > 0)
                {
                    if (!mIsBudget2YearlyFormat)
                        SQL.AppendLine("        And Left(T1.DocDt, 6) = Left(@DocDt, 6) ");
                    else
                        SQL.AppendLine("        And Left(T1.DocDt, 4) = Left(@DocDt, 4) ");
                }

                SQL.AppendLine("Inner Join TblCostCenter T3 On T1.CCCode = T3.CCCode ");
                if (DeptCode.Length > 0) SQL.AppendLine("    And T3.DeptCode = @DeptCode ");
                SQL.AppendLine("Inner Join TblBudgetCategory T4 On ");
                SQL.AppendLine("    T2.AcNo = T4.AcNo  ");
                if (BCCode.Length > 0) SQL.AppendLine("And Find_In_Set(T4.BCCode, @BCCode) ");
                SQL.AppendLine("Inner Join TblCOA T5 On T2.AcNo = T5.AcNo And T5.ActInd = 'Y' ");

                if (type == 1)
                {
                    SQL.AppendLine("    ), 0.00) ");
                }
                else
                {
                    SQL.AppendLine("    Group By T3.DeptCode, T4.BCCode, Left(T1.DocDt, 4) ");
                    if (!mIsBudget2YearlyFormat)
                        Closure.AppendLine("    , Substring(T1.DocDt, 5, 2) ");
                    SQL.AppendLine(") TbJN On A.BCCode = TbJN.BCCode And A.DeptCode = TbJN.DeptCode And A.Yr = TbJN.Yr ");
                    if (!mIsBudget2YearlyFormat)
                        SQL.AppendLine("And A.Mth = TbJN.Mth ");
                }

                #endregion
            }

            return SQL.ToString();
        }

        private void SetGrdHdr(ref iGrid Grd, int row, int col, int ColWidth, string Title, int RowSpan)
        {
            Grd.Cols[col].Width = ColWidth;
            Grd.Header.Cells[row, col].Value = Title;
            Grd.Header.Cells[row, col].TextAlign = iGContentAlignment.MiddleCenter;
            Grd.Header.Cells[row, col].SpanRows = RowSpan;
        }

        private void SetGrdHdr2(ref iGrid Grd, int row, int col, string Title, int SpanCols, string CurrYr, string PrevYr)
        {
            Grd.Header.Cells[row, col].Value = Title;
            Grd.Header.Cells[row, col].TextAlign = iGContentAlignment.MiddleCenter;
            Grd.Header.Cells[row, col].SpanCols = SpanCols;
            Grd.Cols[col].Width = 1400;

            SetGrdHdr(ref Grd1, 0, col, 180, "REAL " + CurrYr + Environment.NewLine + "REAL " + PrevYr, 1);
            SetGrdHdr(ref Grd1, 0, col + 1, 180, "REAL " + CurrYr + Environment.NewLine + "ANGG RUPS " + Environment.NewLine +" S/D " + mMth1 + " " + CurrYr, 1);
            SetGrdHdr(ref Grd1, 0, col + 2, 180, "REAL " + CurrYr + Environment.NewLine + "ANGG PLKSN " + Environment.NewLine + " S/D " + mMth1 + " " + CurrYr, 1);
            SetGrdHdr(ref Grd1, 0, col + 3, 180, "REAL " + CurrYr + Environment.NewLine + "ANGG REVISI " + Environment.NewLine + " S/D " + mMth1 + " " + CurrYr, 1);
            SetGrdHdr(ref Grd1, 0, col + 4, 180, "REAL " + CurrYr + Environment.NewLine + "ANGG RUPS " + CurrYr, 1);
            SetGrdHdr(ref Grd1, 0, col + 5, 180, "REAL " + CurrYr + Environment.NewLine + "ANGG PELAKS " + CurrYr, 1);
            SetGrdHdr(ref Grd1, 0, col + 6, 180, "REAL " + CurrYr + Environment.NewLine + "ANGG REVISI " + CurrYr, 1);
        }

        private string MonthName(string Mth)
        {
            switch (Mth)
            {
                case "01":
                    return "JANUARI";
                case "02":
                    return "FEBRUARI";
                case "03":
                    return "MARET";
                case "04":
                    return "APRIL";
                case "05":
                    return "MEI";
                case "06":
                    return "JUNI";
                case "07":
                    return "JULI";
                case "08":
                    return "AGUSTUS";
                case "09":
                    return "SEPTEMBER";
                case "10":
                    return "OKTOBER";
                case "11":
                    return "NOVEMBER";
                case "12":
                    return "DESEMBER";
            }
            return (Mth == "" ? "" : "PERIODE " + Mth);
        }

        private void SetTimeStampVariable()
        {
            mThisYear = Sm.GetLue(LueYr);
            mPrevYear = (Int32.Parse(mThisYear) - 1).ToString();
            if (Sm.GetLue(LuePeriod).Length > 0)
            {
                mMth = MonthName(Sm.GetLue(LuePeriod));
                if (decimal.Parse(Sm.GetLue(LuePeriod)) > 12)
                    mMth1 = MonthName("12");
                else
                    mMth1 = MonthName(Sm.GetLue(LuePeriod));
            }
            else
            {
                mMth = MonthName(Sm.GetLue(LueMth));
                mMth1 = MonthName(Sm.GetLue(LueMth));
            }

        }
        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        private void ChkPeriod_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Period");
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LuePeriod_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void CcbProfitCenterCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterCcbSetCheckEdit(this, sender);
        }

        private void ChkProfitCenterCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetCheckedComboBoxEdit(this, sender, "Multi Profit Center");
        }




        #endregion

        #endregion

        #region Class
        class RptProfitLoss
        {
            public string RptId { get; set; }
            public string AcNo { get; set; }
            public string AcName { get; set; }
            public string Formula { get; set; }
            public bool ParentInd { get; set; }
            public decimal Amt1 { get; set; }
            public decimal Amt2 { get; set; }
            public decimal Amt3 { get; set; }
            public decimal Amt4 { get; set; }
            public decimal Amt5 { get; set; }
            public decimal Amt6 { get; set; }
            public decimal Amt7 { get; set; }
            public decimal Amt8 { get; set; }
        }

        class COARptProfitLoss
        {
            public string RptId { get; set; }
            public string AcNo { get; set; }
            public decimal Amt1 { get; set; }
            public decimal Amt2 { get; set; }
            public decimal Amt3 { get; set; }
            public decimal Amt4 { get; set; }
            public decimal Amt5 { get; set; }
            public decimal Amt6 { get; set; }
            public decimal Amt7 { get; set; }
            public decimal Amt8 { get; set; }
        }

        class COAJournalProfitLoss
        {
            public string AcNo { get; set; }
            public bool PrevYrInd { get; set; }
            public string Yr { get; set; }
            public decimal Amt { get; set; }
        }

        class COABudgetMonthToDate
        {
            public string AcNo { get; set; }
            public decimal RequestedAmt { get; set; }
            public decimal BudgetAmt { get; set; }
            public decimal UsedAmt { get; set; }
        }

        class COABudgetThisYear
        {
            public string AcNo { get; set; }
            public decimal RequestedAmt { get; set; }
            public decimal BudgetAmt { get; set; }
            public decimal UsedAmt { get; set; }
        }

        class Formula
        {
            public string RptId { get; set; }
            public decimal Amt1 { get; set; }
            public decimal Amt2 { get; set; }
            public decimal Amt3 { get; set; }
            public decimal Amt4 { get; set; }
            public decimal Amt5 { get; set; }
            public decimal Amt6 { get; set; }
            public decimal Amt7 { get; set; }
            public decimal Amt8 { get; set; }
        }

        #endregion
    }
}