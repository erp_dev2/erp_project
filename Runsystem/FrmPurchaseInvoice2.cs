﻿#region Update
/*
    27/11/2019 [TKG/KBN] New application
    03/03/2020 [WED/SIER] tambah TaxCode2 dan TaxCode3
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPurchaseInvoice2 : RunSystem.FrmBase15
    {
        #region Field

        internal string mMenuCode = string.Empty,
            mSiteCode = string.Empty,
            mHeaderDate = string.Empty,
            mDetailDate = string.Empty,
            compare1 = string.Empty,
            compare2 = string.Empty;
        internal bool
            mProcFormatDocNo = false,
            mIsSiteMandatory = false,
            mIsFilterBySite = false,
            mIsAPDownpaymentUseEntity = false,
            mIsDocInformationPIMandatory = false,
            mIsComparedToDetailDate = false,
            mIsPIDueDateAutoFill = false,
            mIsPIUseQRCodeInvoice = false,
            mIsGroupPaymentTermActived = false,
            mIsEProcUseDigitalInvoice = false
            ;

        internal int mStateIndicator = 0;
        internal bool mIsAutoGeneratePurchaseLocalDocNo = false, mIsFilterByDept = false;
        private bool
            mIsPIWithZeroAmtProcessToOP = false,
            mIsAutoJournalActived = false,
            mIsPIShowWarningAPDP = false,
            mIsRemarkForJournalMandatory = false,
            mIsPurchaseInvoiceShowDataAfterInsert = false,
            mIsPurchaseInvoiceUseCOATaxInd = false,
            mIsPIPaymentTypeMandatory = false,
            mIsPIDeptEqualToPORDept = false,
            mIsTaxAliasMandatory = false,
            mIsPIAmtRoundOff = false,
            mIsPIQRCodeWarningEnabled = false,
            mIsPIDocInfoEditable = false
            ;
        private iGCopyPasteManager fCopyPasteManager;
        internal bool mIsShowForeignName = false;
        public string
            mVoucherCodeFormatType = "1",
            mMInd = "N",
            mMainCurCode = string.Empty,
            mPIQRCodeTaxDocType = string.Empty,
            mPIQRCodeDPTaxDocType = string.Empty,
            mPurchaseInvoiceQRCodeTaxCodeDefault = string.Empty,
            mAcNoForRoundingCost = string.Empty,
            mOldServiceCode1 = string.Empty,
            mOldServiceCode2 = string.Empty,
            mOldServiceCode3 = string.Empty,
            mOldServiceNote1 = string.Empty,
            mOldServiceNote2 = string.Empty,
            mOldServiceNote3 = string.Empty,
            mPortForFTPClient = string.Empty,
            mHostAddrForFTPClient = string.Empty,
            mSharedFolderForFTPClient = string.Empty,
            mUsernameForFTPClient = string.Empty,
            mPasswordForFTPClient = string.Empty,
            mFileSizeMaxUploadFTPClient = string.Empty,
            mDigitalInvoiceHost = string.Empty,
            mDigitalInvoiceFolder = string.Empty;
        internal bool
            mIsUseMInd = false,
            mIsUseActivePeriod = false,
            mIsPurchaseInvoiceDeptMandatory = false,
            mIsPIAutoShowPOTax = false,
            mIsPIAllowToUploadFile = false;
        private decimal
            mAmt = 0m,
            mPIRoundedMaxValIfJournalNotBalance = 0m;

        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmPurchaseInvoice2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                GetParameter();
                SetGrd();

                SetLueDeptCode(ref LueDeptCode, string.Empty);
                Sl.SetLueOption(ref LuePaymentType, "VoucherPaymentType");
                Sl.SetLueTaxCode(ref LueTaxCode1);
                Sl.SetLueTaxCode(ref LueTaxCode2);
                Sl.SetLueTaxCode(ref LueTaxCode3);
                if (mIsSiteMandatory) LblSiteCode.ForeColor = Color.Red;
                if (mIsPurchaseInvoiceDeptMandatory) LblDeptCode.ForeColor = Color.Red;
                if (mIsPIPaymentTypeMandatory) LblPaymentType.ForeColor = Color.Red;
                if (mIsPurchaseInvoiceDeptMandatory) LblDeptCode.ForeColor = Color.Red;
                if (!mIsPIDeptEqualToPORDept) Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueDeptCode }, false);
                if (!mIsAutoGeneratePurchaseLocalDocNo) Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtLocalDocNo }, false);
                if (!mIsPIDueDateAutoFill) 
                    BtnDueDt.Visible = false;
                else
                    BtnDueDt.Enabled = true;
                LueTaxCode1.Visible = LueTaxCode2.Visible = LueTaxCode3.Visible = false;
                DteTaxInvoiceDt.Visible = false;
                ClearData();
                DteDocDt.Focus();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 50;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                Grd1,
                new string[] 
                {
                     //0
                    "DNo",

                    //1-5
                    "",
                    "Received#",
                    "Received"+Environment.NewLine+"DNo",
                    "",
                    "PO#",
                    
                    //6-10
                    "",
                    "Item's Code",
                    "",
                    "Item's Name",    
                    "Quantity",
                    
                    //11-15
                    "UoM",
                    "Term of"+Environment.NewLine+"Payment",
                    "Currency",
                    "Unit"+Environment.NewLine+"Price",
                    "Discount"+Environment.NewLine+"%",
                    
                    //16-20
                    "Discount"+Environment.NewLine+"Amount",
                    "Rounding"+Environment.NewLine+"Value",
                    "Tax 1"+Environment.NewLine+"For Information Only",
                    "Tax 2"+Environment.NewLine+"For Information Only",
                    "Tax 3"+Environment.NewLine+"For Information Only",

                    //21-25
                    "Total Without Tax",
                    "Delivery Type",
                    "Remark",
                    "ItScCode",
                    "Sub-Category",

                    //26-30
                    "Remark From MR",
                    "Received Local#",
                    "Site Code",
                    "Site",
                    "Received"+Environment.NewLine+"Date",

                    //31-35
                    "Foreign Name",
                    "DO#",
                    "MR Local#",
                    "Estimated"+Environment.NewLine+"Downpayment",
                    "",

                    //36-40
                    "File Name",
                    "Tax Document#"+Environment.NewLine+"(1)",
                    "Tax Date"+Environment.NewLine+"(1)",
                    "Tax Code"+Environment.NewLine+"(1)",
                    "Tax"+Environment.NewLine+"(1)",

                    //41-45
                    "Tax Amount",
                    "Tax Document#"+Environment.NewLine+"(2)",
                    "Tax Date"+Environment.NewLine+"(2)",
                    "Tax Code"+Environment.NewLine+"(2)",
                    "Tax"+Environment.NewLine+"(2)",

                    //46-49
                    "Tax Document#"+Environment.NewLine+"(3)",
                    "Tax Date"+Environment.NewLine+"(3)",
                    "Tax Code"+Environment.NewLine+"(3)",
                    "Tax"+Environment.NewLine+"(3)"
                },
                new int[] 
                {
                    //0
                    0, 

                    //1-5
                    20, 170, 0, 20, 130,
                    
                    //6-10
                    20, 100, 20, 200, 100, 
                    
                    //11-15
                    80, 0, 70, 100, 100, 
                    
                    //16-20
                    100, 100, 0, 0, 0, 
                    
                    //21-23
                    150, 0, 400, 100, 150,

                    //26-30
                    0, 0, 0, 150, 80,

                    //31-34
                    180, 0, 0, 0, 0,

                    //36-40
                    0, 150, 100, 0, 200,
                    
                    //41-45
                    150, 150, 100, 0, 200, 

                    //46-49
                    150, 100, 0, 200
                }
            );
            Sm.GrdColButton(Grd1, new int[] { 1, 4, 6, 8, 35 });
            Sm.GrdFormatDate(Grd1, new int[] { 30, 38, 43, 47 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 3, 5, 7, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 36, 39, 41, 44, 48 });
            Sm.GrdFormatDec(Grd1, new int[] { 10, 14, 15, 16, 17, 18, 19, 20, 21, 34, 41 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 3, 7, 8, 15, 16, 17, 18, 19, 20, 24, 25, 26, 28, 29, 30, 31, 32, 34, 35, 36, 37, 38, 42, 43, 46, 47 }, false);
            Grd1.Cols[30].Move(4);
            //Grd1.Cols[37].Move(20);
            //Grd1.Cols[38].Move(21);
            //Grd1.Cols[39].Move(22);
            //Grd1.Cols[40].Move(23);
            //Grd1.Cols[41].Move(24);
            Grd1.Cols[49].Move(23);
            Grd1.Cols[48].Move(23);
            Grd1.Cols[47].Move(23);
            Grd1.Cols[46].Move(23);
            Grd1.Cols[45].Move(23);
            Grd1.Cols[44].Move(23);
            Grd1.Cols[43].Move(23);
            Grd1.Cols[42].Move(23);
            Grd1.Cols[40].Move(23);
            Grd1.Cols[39].Move(23);
            Grd1.Cols[38].Move(23);
            Grd1.Cols[37].Move(23);
            Grd1.Cols[41].Move(23);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 7, 8, 15, 16, 17, 18, 19, 20, 26, 27, 30, 32, 37, 38, 42, 43, 46, 47 }, !ChkHideInfoInGrd.Checked);
        }

        private void ClearData()
        {
            mAmt = 0m;
            mSiteCode = string.Empty;
            mDetailDate = string.Empty;
            mHeaderDate = string.Empty;
            compare1 = string.Empty;
            compare2 = string.Empty;

            mOldServiceCode1 = mOldServiceCode2 = mOldServiceCode3 = mOldServiceNote1 = mOldServiceNote2 = mOldServiceNote3 = string.Empty;

            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtLocalDocNo, DteDocDt, LueVdCode, TxtVdInvNo, DtePaymentDt,
                DteVdInvDt, LuePaymentType, DteDueDt, TxtCurCode, LueDeptCode, 
                TxtSiteCode, MeeRemark, DteTaxInvoiceDt, LueTaxCode1, LueTaxCode2, 
                LueTaxCode3, DteTaxInvoiceDt2, DteTaxInvoiceDt3
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit>{ TxtTotalWithoutTax, TxtTotalWithTax, TxtAmt }, 0);

            ClearGrd1();
            Sm.FocusGrd(Grd1, 0, 1);

            mStateIndicator = 1;
            SetLueDeptCode(ref LueDeptCode, string.Empty);
            Sm.SetDteCurrentDate(DteDocDt);
            SetLueVdCode(ref LueVdCode, string.Empty);
        }

        #region Clear Grid

        private void ClearGrd1()
        {
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 10, 14, 15, 16, 17, 18, 19, 20, 21, 34, 41 });
        }

        #endregion

        #endregion

        #region Button Method

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            var l1 = new List<RecvVd>();
            var l2 = new List<PI>();
            try
            {
                InsertData(sender, e, ref l1, ref l2);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                l1.Clear();
                l2.Clear();
                Cursor.Current = Cursors.Default;
            }
        }

        private void BtnDueDt_Click(object sender, EventArgs e)
        {
            if (mIsPIDueDateAutoFill)
            {
                if (DteVdInvDt.Text.Length > 0 && Grd1.Rows.Count > 1)
                {
                    ComputeDueDt();
                }
                else
                    Sm.StdMsg(mMsgType.Warning, "Received list and Vendor Invoice Date should not be empty.");
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 1 && !Sm.IsLueEmpty(LueVdCode, "Vendor"))
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmPurchaseInvoice2Dlg(this, Sm.GetLue(LueVdCode)));
            }

            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmRecvVd(mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }

            if (e.ColIndex == 6 && Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmPO(mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 5);
                f.ShowDialog();
            }

            if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 7);
                f.ShowDialog();
            }

            if (Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length == 0 && e.ColIndex == 22)
                e.DoDefault = false;

            if (Sm.IsGrdColSelected(new int[] { 37, 38, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49 }, e.ColIndex))
            {
                if (e.ColIndex == 38) Sm.DteRequestEdit(Grd1, DteTaxInvoiceDt, ref fCell, ref fAccept, e);
                if (e.ColIndex == 40) Sm.LueRequestEdit(ref Grd1, ref LueTaxCode1, ref fCell, ref fAccept, e);
                if (e.ColIndex == 43) Sm.DteRequestEdit(Grd1, DteTaxInvoiceDt2, ref fCell, ref fAccept, e);
                if (e.ColIndex == 45) Sm.LueRequestEdit(ref Grd1, ref LueTaxCode2, ref fCell, ref fAccept, e);
                if (e.ColIndex == 47) Sm.DteRequestEdit(Grd1, DteTaxInvoiceDt3, ref fCell, ref fAccept, e);
                if (e.ColIndex == 49) Sm.LueRequestEdit(ref Grd1, ref LueTaxCode3, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd1, e.RowIndex);
                Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 10, 14, 15, 16, 17, 18, 19, 20, 21, 34, 41 });
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            ComputeAmt();
            Sm.GrdEnter(Grd1, e);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1 && !Sm.IsLueEmpty(LueVdCode, "Vendor")) 
                Sm.FormShowDialog(new FrmPurchaseInvoice2Dlg(this, Sm.GetLue(LueVdCode)));

            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmRecvVd(mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }

            if (e.ColIndex == 6 && Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length != 0)
            {
                var f = new FrmPO(mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 5);
                f.ShowDialog();
            }

            if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 7);
                f.ShowDialog();
            }
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditTrimString(Grd1, new int[] { 37 }, e);
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 41 }, e);
            //if (e.ColIndex == 37) ComputeAmt();
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e, ref List<RecvVd> l1, ref List<PI> l2)
        {
            mSiteCode = Sm.GetGrdStr(Grd1, 0, 28);
            var EntCode = GetEntity();

            if (mIsPIDeptEqualToPORDept)
            {
                if (IsDeptInvalid()) return;
            }

            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid(EntCode)) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();
            string 
                TaxCode = " ", TaxInvoiceNo = " ", TaxInvoiceDt = " ",
                TaxCode2 = " ", TaxInvoiceNo2 = " ", TaxInvoiceDt2 = " ",
                TaxCode3 = " ", TaxInvoiceNo3 = " ", TaxInvoiceDt3 = " ",
                DocNo = string.Empty, JournalDocNo = string.Empty;

            int v = 0, DNo = 0;

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 2).Length > 0)
                {
                    l1.Add(new RecvVd()
                    {
                        RecvVdDocNo = Sm.GetGrdStr(Grd1, r, 2),
                        RecvVdDNo = Sm.GetGrdStr(Grd1, r, 3),
                        TaxInvoiceNo = Sm.GetGrdStr(Grd1, r, 37),
                        TaxInvoiceDt = Sm.GetGrdDate(Grd1, r, 38),
                        TaxCode = Sm.GetGrdStr(Grd1, r, 39),
                        TaxInvoiceNo2 = Sm.GetGrdStr(Grd1, r, 42),
                        TaxInvoiceDt2 = Sm.GetGrdDate(Grd1, r, 43),
                        TaxCode2 = Sm.GetGrdStr(Grd1, r, 44),
                        TaxInvoiceNo3 = Sm.GetGrdStr(Grd1, r, 46),
                        TaxInvoiceDt3 = Sm.GetGrdDate(Grd1, r, 47),
                        TaxCode3 = Sm.GetGrdStr(Grd1, r, 48),
                        TaxAmt = Sm.GetGrdDec(Grd1, r, 41),
                        Amt = Sm.GetGrdDec(Grd1, r, 21),
                        Remark = Sm.GetGrdStr(Grd1, r, 23)
                    });
                }
            }

            foreach(var x in l1.OrderBy(o => o.TaxCode)
                .ThenBy(t1 => t1.TaxInvoiceNo)
                .ThenBy(t2 => t2.TaxInvoiceDt)
                .ThenBy(t3 => t3.TaxCode2)
                .ThenBy(t4 => t4.TaxInvoiceNo2)
                .ThenBy(t5 => t5.TaxInvoiceDt2)
                .ThenBy(t6 => t6.TaxCode3)
                .ThenBy(t7 => t7.TaxInvoiceNo3)
                .ThenBy(t8 => t8.TaxInvoiceDt3)
                )
            {
                if (Sm.CompareStr(TaxCode, x.TaxCode) &&
                    Sm.CompareStr(TaxInvoiceNo, x.TaxInvoiceNo) &&
                    Sm.CompareStr(TaxInvoiceDt, x.TaxInvoiceDt) &&
                    Sm.CompareStr(TaxCode2, x.TaxCode2) &&
                    Sm.CompareStr(TaxInvoiceNo2, x.TaxInvoiceNo2) &&
                    Sm.CompareStr(TaxInvoiceDt2, x.TaxInvoiceDt2) &&
                    Sm.CompareStr(TaxCode3, x.TaxCode3) &&
                    Sm.CompareStr(TaxInvoiceNo3, x.TaxInvoiceNo3) &&
                    Sm.CompareStr(TaxInvoiceDt3, x.TaxInvoiceDt3)
                    )
                {
                    l2[v - 1].TaxAmt += x.TaxAmt;
                    l2[v - 1].Amt += x.Amt;
                }
                else
                {
                    v++;
                    l2.Add(new PI()
                    {
                        No = v,
                        TaxInvoiceNo = x.TaxInvoiceNo,
                        TaxInvoiceDt = x.TaxInvoiceDt,
                        TaxCode = x.TaxCode,
                        TaxInvoiceNo2 = x.TaxInvoiceNo2,
                        TaxInvoiceDt2 = x.TaxInvoiceDt2,
                        TaxCode2 = x.TaxCode2,
                        TaxInvoiceNo3 = x.TaxInvoiceNo3,
                        TaxInvoiceDt3 = x.TaxInvoiceDt3,
                        TaxCode3 = x.TaxCode3,
                        TaxAmt = x.TaxAmt,
                        Amt = x.Amt
                    });
                }

                TaxCode = x.TaxCode;
                TaxInvoiceNo = x.TaxInvoiceNo;
                TaxInvoiceDt = x.TaxInvoiceDt;
                TaxCode2 = x.TaxCode2;
                TaxInvoiceNo2 = x.TaxInvoiceNo2;
                TaxInvoiceDt2 = x.TaxInvoiceDt2;
                TaxCode3 = x.TaxCode3;
                TaxInvoiceNo3 = x.TaxInvoiceNo3;
                TaxInvoiceDt3 = x.TaxInvoiceDt3;
            }

            foreach (var i in l2)
            {
                DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "PurchaseInvoice", "TblPurchaseInvoiceHdr", i.No.ToString());

                cml.Add(SavePurchaseInvoiceHdr(DocNo, i));

                DNo = 0;
                foreach (var j in l1.Where(w => 
                    Sm.CompareStr(w.TaxCode, i.TaxCode) &&
                    Sm.CompareStr(w.TaxInvoiceNo, i.TaxInvoiceNo) &&
                    Sm.CompareStr(w.TaxInvoiceDt, i.TaxInvoiceDt) &&
                    Sm.CompareStr(w.TaxCode2, i.TaxCode2) &&
                    Sm.CompareStr(w.TaxInvoiceNo2, i.TaxInvoiceNo2) &&
                    Sm.CompareStr(w.TaxInvoiceDt2, i.TaxInvoiceDt2) &&
                    Sm.CompareStr(w.TaxCode3, i.TaxCode3) &&
                    Sm.CompareStr(w.TaxInvoiceNo3, i.TaxInvoiceNo3) &&
                    Sm.CompareStr(w.TaxInvoiceDt3, i.TaxInvoiceDt3)
                    ))
                {
                    DNo += 1;
                    cml.Add(SavePurchaseInvoiceDtl(DocNo, DNo, j));
                }

                if (mIsAutoJournalActived) cml.Add(SaveJournal(DocNo, 1));
            }

          
            Sm.ExecCommands(cml);

            Sm.StdMsg(mMsgType.Info, "Process is completed.");
            ClearData();
        }

        private bool IsInsertedDataNotValid(string EntCode)
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueVdCode, "Vendor") ||
                (mIsPIPaymentTypeMandatory && Sm.IsLueEmpty(LuePaymentType, "Requested payment type")) ||
                Sm.IsDteEmpty(DteDueDt, "Due date") ||
                (mIsPurchaseInvoiceDeptMandatory && Sm.IsLueEmpty(LueDeptCode, "Department")) ||
                (mIsRemarkForJournalMandatory && Sm.IsMeeEmpty(MeeRemark, "Remark")) ||
                Sm.IsClosingJournalInvalid(Sm.GetDte(DteDocDt)) ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                IsCurrencyNotValid() ||
                IsRecvVdInvalid() ||
                IsSiteNotValid() ||
                IsDateNotValid()
                ;
        }

        private bool IsDateNotValid()
        {
            mHeaderDate = Sm.GetDte(DteDocDt);

            if (mIsComparedToDetailDate)
            {
                if (Grd1.Rows.Count != 1)
                {
                    //loop grid untuk mendapatkan tanggal terbaru
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    {
                        if (Sm.CompareDtTm(compare1, Sm.GetGrdDate(Grd1, Row, 30)) < 0)
                        {
                            compare1 = Sm.GetGrdDate(Grd1, Row, 30);
                            compare2 = compare1;
                        }
                        else
                            compare2 = compare1;
                    }
                    mDetailDate = compare2;

                    if (Sm.CompareDtTm(mHeaderDate, mDetailDate) < 0)
                    {
                        Sm.StdMsg(mMsgType.Warning, "Document's date should not be earlier than received date.");
                        DteDocDt.Focus();
                        return true;
                    }
                }
            }
            return false;
        }

        private bool IsSiteNotValid()
        {
            TxtSiteCode.EditValue = Sm.GetGrdStr(Grd1, 0, 29);

            if (!mIsSiteMandatory) return false;

            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (!Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 28), mSiteCode))
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "Received# : " + Sm.GetGrdStr(Grd1, Row, 2) + Environment.NewLine +
                            "PO# : " + Sm.GetGrdStr(Grd1, Row, 5) + Environment.NewLine +
                            "Item Code : " + Sm.GetGrdStr(Grd1, Row, 7) + Environment.NewLine +
                            "Item Name : " + Sm.GetGrdStr(Grd1, Row, 9) + Environment.NewLine +
                            "Site Name : " + Sm.GetGrdStr(Grd1, Row, 29) + Environment.NewLine + Environment.NewLine +
                            "Invalid site."
                            );
                        return true;
                    }
                }
            }

            return false;
        }

        private string GetEntity()
        {
            if (mSiteCode.Length == 0 || !mIsAPDownpaymentUseEntity) return string.Empty;
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.EntCode ");
            SQL.AppendLine("From TblSite A ");
            SQL.AppendLine("Inner Join TblProfitCenter B On A.ProfitCenterCode=B.ProfitCenterCode ");
            SQL.AppendLine("Where A.SiteCode=@SiteCode;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@SiteCode", mSiteCode);

            return Sm.GetValue(cm);
        }

        private bool IsRecvVdInvalid()
        {
            if (Grd1.Rows.Count <= 1) return false;
            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 2).Length > 0)
                {
                    if (IsRecvVdInvalid1(r)) return true;
                    if (IsRecvVdInvalid2(r)) return true;
                }
            }
            return false;
        }

        private bool IsRecvVdInvalid1(int r)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo ");
            SQL.AppendLine("From TblPurchaseInvoiceHdr A, TblPurchaseInvoiceDtl B ");
            SQL.AppendLine("Where A.CancelInd='N' ");
            SQL.AppendLine("And A.DocNo=B.DocNo ");
            SQL.AppendLine("And B.RecvVdDocNo=@DocNo ");
            SQL.AppendLine("And B.RecvVdDNo=@DNo;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", Sm.GetGrdStr(Grd1, r, 2));
            Sm.CmParam<String>(ref cm, "@DNo", Sm.GetGrdStr(Grd1, r, 3));
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Received# : " + Sm.GetGrdStr(Grd1, r, 2) + Environment.NewLine +
                    "PO# : " + Sm.GetGrdStr(Grd1, r, 5) + Environment.NewLine +
                    "Item's code : " + Sm.GetGrdStr(Grd1, r, 7) + Environment.NewLine +
                    "Item's name : " + Sm.GetGrdStr(Grd1, r, 9) + Environment.NewLine + Environment.NewLine +
                    "This received# already invoiced.");
                return true;
            }
            return false;
        }

        private bool IsRecvVdInvalid2(int r)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo ");
            SQL.AppendLine("From TblRecvVdDtl ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And DNo=@DNo ");
            SQL.AppendLine("And (CancelInd='Y' Or Status='C'); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", Sm.GetGrdStr(Grd1, r, 2));
            Sm.CmParam<String>(ref cm, "@DNo", Sm.GetGrdStr(Grd1, r, 3));
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Received# : " + Sm.GetGrdStr(Grd1, r, 2) + Environment.NewLine +
                    "PO# : " + Sm.GetGrdStr(Grd1, r, 5) + Environment.NewLine +
                    "Item's code : " + Sm.GetGrdStr(Grd1, r, 7) + Environment.NewLine +
                    "Item's name : " + Sm.GetGrdStr(Grd1, r, 9) + Environment.NewLine + Environment.NewLine +
                    "This received# already cancelled.");
                return true;
            }
            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 record in received document list.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            if (Grd1.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.IsGrdValueEmpty(Grd1, Row, 2, false, "Received# is empty.")) return true;
            }
            return false;
        }

        private bool IsCurrencyNotValid()
        {
            bool NotValid = false;
            string CurCode = mMainCurCode;

            if (Grd1.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 13).Length != 0)
                    {
                        if (CurCode.Length != 0 && !Sm.CompareStr(CurCode, Sm.GetGrdStr(Grd1, Row, 13)))
                        {
                            NotValid = true;
                            break;
                        }
                        CurCode = Sm.GetGrdStr(Grd1, Row, 13);
                    }
                }
            }

            if (NotValid) Sm.StdMsg(mMsgType.Warning, "One purchase invoice# only allowed 1 currency type.");
            return NotValid;
        }

        private int GetNumberOfDocument()
        {
            bool IsTaxable = false, IsNotTaxable = false;
            int n=0;
            if (Grd1.Rows.Count > 1)
            {
                for (int r = 0; r< Grd1.Rows.Count - 1; r++)
                {
                    if (Sm.GetGrdStr(Grd1, r, 2).Length > 0)
                    {
                        if (!IsTaxable && Sm.GetGrdBool(Grd1, r, 37)) n += 1;
                        if (!IsNotTaxable && !Sm.GetGrdBool(Grd1, r, 37)) n += 1;           
                    }
                }
            }
            return n;
        }

        private MySqlCommand SavePurchaseInvoiceHdr(string DocNo, PI x)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblPurchaseInvoiceHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, ProcessInd, LocalDocNo, SeqNo, DeptCode, ItSCCode, Mth, Yr, Revision, ");
            SQL.AppendLine("VdCode, VdInvNo, VdInvDt, PaymentType, DueDt, ");
            SQL.AppendLine("TaxInvoiceNo, TaxInvoiceDt, TaxInvoiceNo2, TaxInvoiceDt2, TaxInvoiceNo3, TaxInvoiceDt3, TaxCurCode, ");
            SQL.AppendLine("TaxRateAmt, TaxCode1, TaxCode2, TaxCode3, TaxAmt, DownPayment, CurCode, Amt, SiteCode, JournalDocNo, COATaxInd, Remark, ");
            SQL.AppendLine("TaxAlias1, TaxAlias2, TaxAlias3, ServiceCode1, ServiceCode2, ServiceCode3, ServiceNote1, ServiceNote2, ServiceNote3, ");
            SQL.AppendLine("PaymentDt, CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@DocNo, @DocDt, 'N', 'O', @LocalDocNo, Null, @DeptCode, Null, Null, Null, Null, ");
            SQL.AppendLine("@VdCode, @VdInvNo, @VdInvDt, @PaymentType, @DueDt, ");
            SQL.AppendLine("@TaxInvoiceNo, @TaxInvoiceDt, @TaxInvoiceNo2, @TaxInvoiceDt2, @TaxInvoiceNo3, @TaxInvoiceDt3, @TaxCurCode, ");
            SQL.AppendLine("@TaxRateAmt, @TaxCode1, @TaxCode2, @TaxCode3, @TaxAmt, 0.00, @CurCode, @Amt, @SiteCode, Null, 'N', @Remark, ");
            SQL.AppendLine("Null, Null, Null, Null, Null, Null, Null, Null, Null, ");
            SQL.AppendLine("@PaymentDt, @UserCode, CurrentDateTime());");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@LocalDocNo", TxtLocalDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@VdCode", Sm.GetLue(LueVdCode));
            Sm.CmParam<String>(ref cm, "@VdInvNo", TxtVdInvNo.Text);
            Sm.CmParamDt(ref cm, "@VdInvDt", Sm.GetDte(DteVdInvDt));
            Sm.CmParam<String>(ref cm, "@PaymentType", Sm.GetLue(LuePaymentType));
            Sm.CmParamDt(ref cm, "@DueDt", Sm.GetDte(DteDueDt));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetGrdStr(Grd1, 0, 13));
            Sm.CmParam<String>(ref cm, "@SiteCode", mSiteCode);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParamDt(ref cm, "@PaymentDt", Sm.GetDte(DtePaymentDt));
            Sm.CmParam<Decimal>(ref cm, "@Amt", x.Amt);
            Sm.CmParam<String>(ref cm, "@TaxInvoiceNo", x.TaxInvoiceNo);
            Sm.CmParamDt(ref cm, "@TaxInvoiceDt", x.TaxInvoiceDt);
            Sm.CmParam<String>(ref cm, "@TaxInvoiceNo2", x.TaxInvoiceNo2);
            Sm.CmParamDt(ref cm, "@TaxInvoiceDt2", x.TaxInvoiceDt2);
            Sm.CmParam<String>(ref cm, "@TaxInvoiceNo3", x.TaxInvoiceNo3);
            Sm.CmParamDt(ref cm, "@TaxInvoiceDt3", x.TaxInvoiceDt3);
            Sm.CmParam<String>(ref cm, "@TaxCurCode", mMainCurCode);
            Sm.CmParam<Decimal>(ref cm, "@TaxRateAmt", 1m);
            Sm.CmParam<String>(ref cm, "@TaxCode1", x.TaxCode);
            Sm.CmParam<String>(ref cm, "@TaxCode2", x.TaxCode2);
            Sm.CmParam<String>(ref cm, "@TaxCode3", x.TaxCode3);
            Sm.CmParam<Decimal>(ref cm, "@TaxAmt", x.TaxAmt);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SavePurchaseInvoiceDtl(string DocNo, int DNo, RecvVd x)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblPurchaseInvoiceDtl(DocNo, DNo, RecvVdDocNo, RecvVdDNo, FileName, Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DNo, @RecvVdDocNo, @RecvVdDNo, Null, @Remark, @UserCode, CurrentDateTime()); "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00"+DNo.ToString(), 3));
            Sm.CmParam<String>(ref cm, "@RecvVdDocNo", x.RecvVdDocNo);
            Sm.CmParam<String>(ref cm, "@RecvVdDNo", x.RecvVdDNo);
            Sm.CmParam<String>(ref cm, "@Remark", x.Remark);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal(string DocNo, int v)
        {
            var SQL = new StringBuilder();
            var EntCode = Sm.GetValue(
                     "Select B.EntCode " +
                     "From TblSite A, TblProfitCenter B " +
                     "Where A.SiteCode=@Param " +
                     "And A.ProfitCenterCode=B.ProfitCenterCode;",
                     mSiteCode
                     );

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Update TblPurchaseInvoiceHdr Set ");
            SQL.AppendLine("    JournalDocNo=");
            SQL.AppendLine(Sm.GetNewJournalDocNo(Sm.GetDte(DteDocDt), v));
            SQL.AppendLine("Where DocNo=@DocNo;");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select JournalDocNo, ");
            SQL.AppendLine("DocDt, ");
            SQL.AppendLine("Concat('Purchase Invoice : ', @DocNo) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, (Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, Remark, ");
            SQL.AppendLine("CreateBy, CreateDt ");
            SQL.AppendLine("From TblPurchaseInvoiceHdr Where DocNo=@DocNo; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, @EntCode, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From (");

            //Hutang Uninvoice

            SQL.AppendLine("        Select Concat(E.ParValue, A.VdCode) As AcNo, ");
            SQL.AppendLine("        (C.Qty*D.UPrice*D.ExcRate) As DAmt, ");
            SQL.AppendLine("        0.00 As CAmt ");
            SQL.AppendLine("        From TblPurchaseInvoiceHdr A ");
            SQL.AppendLine("        Inner Join TblPurchaseInvoiceDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("        Inner Join TblRecvVdDtl C On B.RecvVdDocNo=C.DocNo And B.RecvVdDNo=C.DNo ");
            SQL.AppendLine("        Inner Join TblStockPrice D On C.Source=D.Source ");
            SQL.AppendLine("        Inner Join TblParameter E On E.ParCode='VendorAcNoUnInvoiceAP' And E.ParValue Is Not Null ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");

            //PPN Masukan
            //if (TxtTotalTaxAmt1.Text.Length > 0 && decimal.Parse(TxtTotalTaxAmt1.Text)!=0)
            //{
            if (Sm.CompareStr(mMainCurCode, TxtCurCode.Text))
            {
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("        Select B.AcNo1 As AcNo, ");
                SQL.AppendLine("        Case When (A.Amt*B.TaxRate*0.01)>=0.00 Then (A.Amt*B.TaxRate*0.01) Else 0.00 End As DAmt, ");
                SQL.AppendLine("        Case When (A.Amt*B.TaxRate*0.01)>=0.00 Then 0.00 Else Abs(A.Amt*B.TaxRate*0.01) End As CAmt ");
                SQL.AppendLine("        From TblPurchaseInvoiceHdr A ");
                SQL.AppendLine("        Inner Join TblTax B On A.TaxCode1=B.TaxCode And B.AcNo1 is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo And A.TaxCode1 Is Not Null ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("        Select B.AcNo1 As AcNo, ");
                SQL.AppendLine("        Case When (A.Amt*B.TaxRate*0.01)>=0.00 Then (A.Amt*B.TaxRate*0.01) Else 0.00 End As DAmt, ");
                SQL.AppendLine("        Case When (A.Amt*B.TaxRate*0.01)>=0.00 Then 0.00 Else Abs(A.Amt*B.TaxRate*0.01) End As CAmt ");
                SQL.AppendLine("        From TblPurchaseInvoiceHdr A ");
                SQL.AppendLine("        Inner Join TblTax B On A.TaxCode2=B.TaxCode And B.AcNo1 is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo And A.TaxCode2 Is Not Null ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("        Select B.AcNo1 As AcNo, ");
                SQL.AppendLine("        Case When (A.Amt*B.TaxRate*0.01)>=0.00 Then (A.Amt*B.TaxRate*0.01) Else 0.00 End As DAmt, ");
                SQL.AppendLine("        Case When (A.Amt*B.TaxRate*0.01)>=0.00 Then 0.00 Else Abs(A.Amt*B.TaxRate*0.01) End As CAmt ");
                SQL.AppendLine("        From TblPurchaseInvoiceHdr A ");
                SQL.AppendLine("        Inner Join TblTax B On A.TaxCode3=B.TaxCode And B.AcNo1 is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo And A.TaxCode3 Is Not Null ");
            }
            else
            {
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("        Select B.AcNo1 As AcNo, ");
                SQL.AppendLine("        Case When (A.Amt*B.TaxRate*0.01*A.TaxRateAmt)>=0.00 Then (A.Amt*B.TaxRate*0.01*A.TaxRateAmt) Else 0.00 End As DAmt, ");
                SQL.AppendLine("        Case When (A.Amt*B.TaxRate*0.01*A.TaxRateAmt)>=0.00 Then 0.00 Else Abs(A.Amt*B.TaxRate*0.01*A.TaxRateAmt) End As CAmt ");
                SQL.AppendLine("        From TblPurchaseInvoiceHdr A ");
                SQL.AppendLine("        Inner Join TblTax B On A.TaxCode1=B.TaxCode And B.AcNo1 is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo And A.TaxCode1 Is Not Null ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("        Select B.AcNo1 As AcNo, ");
                SQL.AppendLine("        Case When (A.Amt*B.TaxRate*0.01*A.TaxRateAmt)>=0.00 Then (A.Amt*B.TaxRate*0.01*A.TaxRateAmt) Else 0.00 End As DAmt, ");
                SQL.AppendLine("        Case When (A.Amt*B.TaxRate*0.01*A.TaxRateAmt)>=0.00 Then 0.00 Else Abs(A.Amt*B.TaxRate*0.01*A.TaxRateAmt) End As CAmt ");
                SQL.AppendLine("        From TblPurchaseInvoiceHdr A ");
                SQL.AppendLine("        Inner Join TblTax B On A.TaxCode2=B.TaxCode And B.AcNo1 is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo And A.TaxCode2 Is Not Null ");

                SQL.AppendLine("    Union All ");
                SQL.AppendLine("        Select B.AcNo1 As AcNo, ");
                SQL.AppendLine("        Case When (A.Amt*B.TaxRate*0.01*A.TaxRateAmt)>=0.00 Then (A.Amt*B.TaxRate*0.01*A.TaxRateAmt) Else 0.00 End As DAmt, ");
                SQL.AppendLine("        Case When (A.Amt*B.TaxRate*0.01*A.TaxRateAmt)>=0.00 Then 0.00 Else Abs(A.Amt*B.TaxRate*0.01*A.TaxRateAmt) End As CAmt ");
                SQL.AppendLine("        From TblPurchaseInvoiceHdr A ");
                SQL.AppendLine("        Inner Join TblTax B On A.TaxCode3=B.TaxCode And B.AcNo1 is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo And A.TaxCode3 Is Not Null ");
            }
            //}

            //Uang muka pembelian
            if (Sm.CompareStr(mMainCurCode, TxtCurCode.Text))
            {
                SQL.AppendLine("        Union All ");
                SQL.AppendLine("        Select Concat(B.ParValue, A.VdCode) As AcNo, ");
                SQL.AppendLine("        0.00 As DAmt, ");
                SQL.AppendLine("        A.Downpayment As CAmt ");
                SQL.AppendLine("        From TblPurchaseInvoiceHdr A ");
                SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='VendorAcNoDownPayment' And B.ParValue Is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
            }
            else
            {
                SQL.AppendLine("        Union All ");
                SQL.AppendLine("        Select Concat(C.ParValue, A.VdCode) As AcNo, ");
                SQL.AppendLine("        0.00 As DAmt, B.ExcRate*B.Amt As CAmt ");
                SQL.AppendLine("        From TblPurchaseInvoiceHdr A ");
                SQL.AppendLine("        Inner Join TblPurchaseInvoiceDtl5 B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Inner Join TblParameter C On C.ParCode='VendorAcNoDownPayment' And C.ParValue Is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
            }

            //Hutang Usaha

            if (Sm.CompareStr(mMainCurCode, TxtCurCode.Text))
            {
                SQL.AppendLine("        Union All ");
                SQL.AppendLine("        Select Concat(B.ParValue, A.VdCode) As AcNo, ");
                SQL.AppendLine("        0 As DAmt, ");
                SQL.AppendLine("        A.Amt+A.TaxAmt-A.Downpayment As CAmt ");
                SQL.AppendLine("        From TblPurchaseInvoiceHdr A ");
                SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='VendorAcNoAP' And B.ParValue Is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
            }
            else
            {
                SQL.AppendLine("        Union All ");
                SQL.AppendLine("        Select Concat(B.ParValue, A.VdCode) As AcNo, ");
                SQL.AppendLine("        0.00 As DAmt, ");
                SQL.AppendLine("        IfNull(( ");
                SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("        ), 0.00) * ");
                SQL.AppendLine("        (A.Amt+A.TaxAmt-A.Downpayment) As CAmt ");
                SQL.AppendLine("        From TblPurchaseInvoiceHdr A ");
                SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='VendorAcNoAP' And B.ParValue Is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
            }

            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select B.AcNo, ");
            if (!Sm.CompareStr(mMainCurCode, TxtCurCode.Text))
            {
                SQL.AppendLine("        IfNull(( ");
                SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("        ), 0.00) * ");
            }
            SQL.AppendLine("        B.DAmt, ");
            if (!Sm.CompareStr(mMainCurCode, TxtCurCode.Text))
            {
                SQL.AppendLine("        IfNull(( ");
                SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("        ), 0.00) * ");
            }
            SQL.AppendLine("        B.CAmt ");
            SQL.AppendLine("        From TblPurchaseInvoiceHdr A ");
            SQL.AppendLine("        Inner Join TblPurchaseInvoiceDtl4 B On A.DocNo=B.DocNo ");
            SQL.AppendLine("        Inner Join TblParameter C On C.ParCode='VendorAcNoAP' And C.ParValue Is Not Null ");
            SQL.AppendLine("        Where A.DocNo=@DocNo And Concat(C.ParValue, A.VdCode)<>B.AcNo ");

            //Laba rugi selisih kurs
            if (!Sm.CompareStr(mMainCurCode, TxtCurCode.Text))
            {
                SQL.AppendLine("        Union All ");
                SQL.AppendLine("        Select ParValue As AcNo, ");
                SQL.AppendLine("        0.00 As DAmt, ");
                SQL.AppendLine("        0.00 As CAmt ");
                SQL.AppendLine("        From TblParameter Where ParCode='AcNoForForeignCurrencyExchangeGains' And ParValue Is Not Null ");
            }
            SQL.AppendLine("    ) Tbl ");
            SQL.AppendLine("    Where AcNo Is Not Null ");
            SQL.AppendLine("    Group By AcNo ");
            SQL.AppendLine(") B On 1=1 ");
            SQL.AppendLine("Where A.DocNo In ( ");
            SQL.AppendLine("    Select JournalDocNo ");
            SQL.AppendLine("    From TblPurchaseInvoiceHdr ");
            SQL.AppendLine("    Where DocNo=@DocNo ");
            SQL.AppendLine("    And JournalDocNo Is Not Null ");
            SQL.AppendLine("    ); ");

            if (!Sm.CompareStr(mMainCurCode, TxtCurCode.Text))
            {
                SQL.AppendLine("Update TblJournalDtl A ");
                SQL.AppendLine("Inner Join ( ");
                SQL.AppendLine("    Select DAmt, CAmt From (");
                SQL.AppendLine("        Select Sum(DAmt) as DAmt, Sum(CAmt) as CAmt ");
                SQL.AppendLine("        From TblJournalDtl ");
                SQL.AppendLine("        Where DocNo In ( ");
                SQL.AppendLine("            Select JournalDocNo ");
                SQL.AppendLine("            From TblPurchaseInvoiceHdr ");
                SQL.AppendLine("            Where DocNo=@DocNo ");
                SQL.AppendLine("            And JournalDocNo Is Not Null ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ) Tbl ");
                SQL.AppendLine(") B On 1=1 ");
                SQL.AppendLine("Set ");
                SQL.AppendLine("    A.DAmt=Case When B.DAmt<B.CAmt Then Abs(B.CAmt-B.DAmt) Else 0.00 End, ");
                SQL.AppendLine("    A.CAmt=Case When B.DAmt>B.CAmt Then Abs(B.DAmt-B.CAmt) Else 0.00 End ");
                SQL.AppendLine("Where A.DocNo In ( ");
                SQL.AppendLine("    Select JournalDocNo ");
                SQL.AppendLine("    From TblPurchaseInvoiceHdr ");
                SQL.AppendLine("    Where DocNo=@DocNo ");
                SQL.AppendLine("    And JournalDocNo Is Not Null ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine("And A.AcNo In ( ");
                SQL.AppendLine("    Select ParValue From TblParameter ");
                SQL.AppendLine("    Where ParCode='AcNoForForeignCurrencyExchangeGains' ");
                SQL.AppendLine("    And ParValue Is Not Null ");
                SQL.AppendLine("    );");
            }
            else
            {
                SQL.AppendLine("Update TblJournalDtl A ");
                SQL.AppendLine("Inner Join ( ");
                SQL.AppendLine("    Select DAmt, CAmt From (");
                SQL.AppendLine("        Select Sum(DAmt) as DAmt, Sum(CAmt) as CAmt ");
                SQL.AppendLine("        From TblJournalDtl ");
                SQL.AppendLine("        Where DocNo=@JournalDocNo ");
                SQL.AppendLine("    ) Tbl ");
                SQL.AppendLine(") B On 1=1 ");
                SQL.AppendLine("Set ");
                SQL.AppendLine("    A.DAmt=Case When B.DAmt=B.CAmt Then ");
                SQL.AppendLine("        A.DAmt Else ");
                SQL.AppendLine("        Case When Abs(B.DAmt-B.CAmt)<@PIRoundedMaxValIfJournalNotBalance Then ");
                SQL.AppendLine("            A.DAmt-(B.DAmt-B.CAmt) Else A.DAmt End ");
                SQL.AppendLine("        End ");
                SQL.AppendLine("Where A.DocNo=@JournalDocNo ");
                SQL.AppendLine("And A.AcNo In ( ");
                SQL.AppendLine("        Select Concat(B.ParValue, A.VdCode) As AcNo ");
                SQL.AppendLine("        From TblPurchaseInvoiceHdr A ");
                SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='VendorAcNoUnInvoiceAP' And B.ParValue Is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("    );");
            }
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@MainCurCode", mMainCurCode);
            Sm.CmParam<String>(ref cm, "@CurCode", TxtCurCode.Text);
            Sm.CmParam<String>(ref cm, "@EntCode", EntCode);
            Sm.CmParam<Decimal>(ref cm, "@PIRoundedMaxValIfJournalNotBalance", mPIRoundedMaxValIfJournalNotBalance == 0m ? 1m : mPIRoundedMaxValIfJournalNotBalance);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        #endregion

        #endregion

        #region Additional Method

        private bool IsDeptInvalid()
        {
            LueDeptCode.EditValue = null;

            if (Grd1.Rows.Count <= 1) return false;

            var cm = new MySqlCommand();
            string DocNo = string.Empty, DNo = string.Empty, Filter = string.Empty;

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                DocNo = Sm.GetGrdStr(Grd1, r, 2);
                DNo = Sm.GetGrdStr(Grd1, r, 3);

                if (DocNo.Length != 0)
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += "(A.DocNo=@DocNo" + r.ToString() + " And A.DNo=@DNo" + r.ToString() + ") ";
                    Sm.CmParam<String>(ref cm, "@DocNo" + r.ToString(), DocNo);
                    Sm.CmParam<String>(ref cm, "@DNo" + r.ToString(), DNo);
                }
            }

            if (Filter.Length == 0) return false;

            Filter = " Where ( " + Filter + ") ";

            var SQL = new StringBuilder();
            bool IsFirst = true;
            string
                PODocNo = string.Empty,
                ItName = string.Empty,
                DeptCode = string.Empty,
                DeptName = string.Empty;

            DocNo = string.Empty;

            SQL.AppendLine("Select D.DeptCode, A.DocNo, A.PODocNo, G.ItName, E.DeptName ");
            SQL.AppendLine("From TblRecvVdDtl A ");
            SQL.AppendLine("Inner Join TblPODtl B On A.PODocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblPORequestDtl C On B.PORequestDocNo=C.DocNo ");
            SQL.AppendLine("Inner Join TblMaterialRequestHdr D On C.MaterialRequestDocNo=D.DocNo ");
            SQL.AppendLine("Inner Join TblDepartment E On D.DeptCode=E.DeptCode ");
            SQL.AppendLine("Inner Join TblMaterialRequestDtl F On C.MaterialRequestDocNo=F.DocNo And C.MaterialRequestDNo=F.DNo ");
            SQL.AppendLine("Inner Join TblItem G On F.ItCode=G.ItCode ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("Order By D.DeptCode;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    "DeptCode", 
                    "DocNo", 
                    "PODocNo", 
                    "ItName", 
                    "DeptName" 
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        if (IsFirst)
                        {
                            IsFirst = false;
                            DeptCode = Sm.DrStr(dr, c[0]);
                            DocNo = Sm.DrStr(dr, c[1]);
                            PODocNo = Sm.DrStr(dr, c[2]);
                            ItName = Sm.DrStr(dr, c[3]);
                            DeptName = Sm.DrStr(dr, c[4]);
                        }
                        else
                        {
                            if (!Sm.CompareStr(DeptCode, Sm.DrStr(dr, c[0])))
                            {
                                Sm.StdMsg(
                                    mMsgType.Warning,
                                    "1." + Environment.NewLine +
                                    "Received# : " + DocNo + Environment.NewLine +
                                    "PO# : " + PODocNo + Environment.NewLine +
                                    "Item : " + ItName + Environment.NewLine +
                                    "Department : " + DeptName + Environment.NewLine + Environment.NewLine +

                                    "2." + Environment.NewLine +
                                    "Received# : " + Sm.DrStr(dr, c[1]) + Environment.NewLine +
                                    "PO# : " + Sm.DrStr(dr, c[2]) + Environment.NewLine +
                                    "Item : " + Sm.DrStr(dr, c[3]) + Environment.NewLine +
                                    "Department : " + Sm.DrStr(dr, c[4]) + Environment.NewLine + Environment.NewLine +

                                    "One invoice should be processed from one department."
                                    );
                                dr.Close();
                                return true;
                            }
                        }
                    }
                }
                dr.Close();
                if (DeptCode.Length > 0)
                {
                    Sm.SetLue(LueDeptCode, DeptCode);
                    return false;
                }
                Sm.StdMsg(mMsgType.Warning, "Department is empty.");
                return true;
            }
        }

        private void ComputeDueDt()
        {
            int temp = 0; int temp2 = 0; int TOP = 0;
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                var SQL = new StringBuilder();
                var cm = new MySqlCommand();

                SQL.AppendLine("Select Round(E.PtDay, 0) As PtDay ");
                SQL.AppendLine("From TblRecvVdDtl A ");
                SQL.AppendLine("Inner Join TblPODtl B On A.PODocNo = B.DocNo And A.PODNo = B.DNo ");
                SQL.AppendLine("Inner Join TblPORequestDtl C On B.PORequestDocNo = C.DocNo And B.PORequestDNo = C.DNo ");
                SQL.AppendLine("Inner Join TblQtHdr D On C.QtDocNo = D.DocNo ");
                SQL.AppendLine("Inner Join TblPaymentTerm E On D.PtCode = E.PtCode ");
                SQL.AppendLine("Where A.DocNo = @RecvVdDocNo And A.DNo = @RecvVdDNo; ");

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = SQL.ToString();
                    Sm.CmParam<String>(ref cm, "@RecvVdDocNo", Sm.GetGrdStr(Grd1, Row, 2));
                    Sm.CmParam<String>(ref cm, "@RecvVdDNo", Sm.GetGrdStr(Grd1, Row, 3));
                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] { "PtDay" });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            temp = Int32.Parse(Sm.DrStr(dr, 0));
                            if (temp2 == 0) temp2 = temp;
                            else
                            {
                                if (temp2 > temp) temp2 = temp;
                            }
                        }
                    }
                    dr.Close();
                }
                TOP = temp2;
            }

            DteDueDt.DateTime = Sm.ConvertDate(Sm.GetDte(DteVdInvDt)).AddDays(TOP);
        }

        private void GetParameter()
        {
            string MenuCodeForDocWithMInd = Sm.GetParameter("MenuCodeForDocWithMInd");
            mIsUseMInd = Sm.GetParameterBoo("IsUseMInd");
            mIsAutoGeneratePurchaseLocalDocNo = Sm.GetParameterBoo("IsAutoGeneratePurchaseLocalDocNo");
            mIsSiteMandatory = Sm.GetParameterBoo("IsSiteMandatory");
            mIsFilterBySite = Sm.GetParameterBoo("IsFilterBySite");
            mIsAPDownpaymentUseEntity = Sm.GetParameterBoo("IsAPDownpaymentUseEntity");
            mMainCurCode = Sm.GetParameter("MainCurCode");
            mProcFormatDocNo = Sm.GetParameter("ProcFormatDocNo") == "1";
            mIsFilterByDept = Sm.GetParameterBoo("IsFilterByDept");
            mIsAutoJournalActived = Sm.GetParameterBoo("IsAutoJournalActived");
            mIsComparedToDetailDate = Sm.GetParameterBoo("IsComparedToDetailDate");
            mIsShowForeignName = Sm.GetParameter("IsShowForeignName") == "N";
            mIsPIShowWarningAPDP = Sm.GetParameter("IsPIShowWarningAPDP") == "N";
            mIsDocInformationPIMandatory = Sm.GetParameterBoo("IsDocInformationPIMandatory");
            mIsPIDueDateAutoFill = Sm.GetParameterBoo("IsPIDueDateAutoFill");
            mIsRemarkForJournalMandatory = Sm.GetParameterBoo("IsRemarkForJournalMandatory");
            mIsPIUseQRCodeInvoice = Sm.GetParameterBoo("IsPIUseQRCodeInvoice");
            mPIQRCodeTaxDocType = Sm.GetParameter("PIQRCodeTaxDocType");
            mPIQRCodeDPTaxDocType = Sm.GetParameter("PIQRCodeDPTaxDocType");
            mPurchaseInvoiceQRCodeTaxCodeDefault = Sm.GetParameter("PurchaseInvoiceQRCodeTaxCodeDefault");
            mIsPurchaseInvoiceDeptMandatory = Sm.GetParameterBoo("IsPurchaseInvoiceDeptMandatory");
            mIsPurchaseInvoiceShowDataAfterInsert = Sm.GetParameterBoo("IsPurchaseInvoiceShowDataAfterInsert");
            mIsPurchaseInvoiceUseCOATaxInd = Sm.GetParameterBoo("IsPurchaseInvoiceUseCOATaxInd");
            mIsPIAutoShowPOTax = Sm.GetParameterBoo("IsPIAutoShowPOTax");
            mIsPIPaymentTypeMandatory = Sm.GetParameterBoo("IsPIPaymentTypeMandatory");
            mIsGroupPaymentTermActived = Sm.GetParameterBoo("IsGroupPaymentTermActived");
            mIsPIDeptEqualToPORDept = Sm.GetParameterBoo("IsPIDeptEqualToPORDept");
            mIsTaxAliasMandatory = Sm.GetParameterBoo("IsTaxAliasMandatory");
            mVoucherCodeFormatType = Sm.GetParameter("VoucherCodeFormatType");
            mIsPIWithZeroAmtProcessToOP = Sm.GetParameterBoo("IsPIWithZeroAmtProcessToOP");
            mIsPIAmtRoundOff = Sm.GetParameterBoo("IsPIAmtRoundOff");
            mAcNoForRoundingCost = Sm.GetParameter("AcNoForRoundingCost");
            mIsPIAllowToUploadFile = Sm.GetParameterBoo("IsPIAllowToUploadFile");
            mHostAddrForFTPClient = Sm.GetParameter("HostAddrForFTPClient");
            mSharedFolderForFTPClient = Sm.GetParameter("SharedFolderForFTPClient");
            mUsernameForFTPClient = Sm.GetParameter("UsernameForFTPClient");
            mPasswordForFTPClient = Sm.GetParameter("PasswordForFTPClient");
            mPortForFTPClient = Sm.GetParameter("PortForFTPClient");
            mFileSizeMaxUploadFTPClient = Sm.GetParameter("FileSizeMaxUploadFTPClient");
            mIsEProcUseDigitalInvoice = Sm.GetParameterBoo("IsEProcUseDigitalInvoice");
            mDigitalInvoiceHost = Sm.GetParameter("DigitalInvoiceHost");
            mDigitalInvoiceFolder = Sm.GetParameter("DigitalInvoiceFolder");
            mIsPIQRCodeWarningEnabled = Sm.GetParameterBoo("IsPIQRCodeWarningEnabled");
            mPIRoundedMaxValIfJournalNotBalance = Sm.GetParameterDec("PIRoundedMaxValIfJournalNotBalance");
            mIsPIDocInfoEditable = Sm.GetParameterBoo("IsPIDocInfoEditable");
        }

        internal void SetLueDeptCode(ref DXE.LookUpEdit Lue, string DeptCode)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select T.DeptCode As Col1, T.DeptName As Col2 ");
                SQL.AppendLine("From TblDepartment T ");
                SQL.AppendLine("Where (T.ActInd='Y' ");
                if (DeptCode.Length != 0)
                    SQL.AppendLine("Or T.DeptCode=@DeptCode ");
                if ((mIsFilterByDept) && (mStateIndicator == 1))
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select DeptCode From TblGroupDepartment ");
                    SQL.AppendLine("    Where DeptCode=T.DeptCode ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode=@UserCode ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine(") ");
                }
                SQL.AppendLine(") ");
                SQL.AppendLine("Order By DeptName; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };

                if (DeptCode.Length != 0)
                    Sm.CmParam<String>(ref cm, "@DeptCode", DeptCode);
                if (mIsFilterByDept)
                    Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        internal string GetSelectedRecvVd()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length != 0 &&
                        Sm.GetGrdStr(Grd1, Row, 3).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL +=
                            "'" +
                            Sm.GetGrdStr(Grd1, Row, 2) +
                            Sm.GetGrdStr(Grd1, Row, 3) +
                            "'";
                    }
                }
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }

        private void ComputeTotalWithoutTax()
        {
            decimal TotalWithoutTax = 0m;

            if (Grd1.Rows.Count >= 1)
            {
                for (int r= 0; r< Grd1.Rows.Count; r++)
                    if (Sm.GetGrdStr(Grd1, r, 2).Length > 0 && Sm.GetGrdStr(Grd1, r, 21).Length > 0)
                        TotalWithoutTax += Sm.GetGrdDec(Grd1, r, 21);
            }
            TxtTotalWithoutTax.EditValue = Sm.FormatNum(TotalWithoutTax, 0);
        }

        private void ComputeTax(int r)
        {
            decimal Tax = 0m;
            string TaxCode = Sm.GetGrdStr(Grd1, r, 39);
            if (TaxCode.Length > 0 && Sm.GetGrdStr(Grd1, r, 21).Length > 0)
                Tax += GetTaxRate(TaxCode) * 0.01m * Sm.GetGrdDec(Grd1, r, 21);

            string TaxCode2 = Sm.GetGrdStr(Grd1, r, 44);
            if (TaxCode2.Length > 0 && Sm.GetGrdStr(Grd1, r, 21).Length > 0)
                Tax += GetTaxRate(TaxCode2) * 0.01m * Sm.GetGrdDec(Grd1, r, 21);

            string TaxCode3 = Sm.GetGrdStr(Grd1, r, 48);
            if (TaxCode3.Length > 0 && Sm.GetGrdStr(Grd1, r, 21).Length > 0)
                Tax += GetTaxRate(TaxCode3) * 0.01m * Sm.GetGrdDec(Grd1, r, 21);

            Grd1.Cells[r, 41].Value = Tax;
        }

        private void ComputeTotalWithTax()
        {
            decimal Total = 0m, Tax = 0m;
            if (TxtTotalWithoutTax.Text.Length > 0) Total = decimal.Parse(TxtTotalWithoutTax.Text);
            if (Grd1.Rows.Count >= 1)
            {
                for (int r=0; r<Grd1.Rows.Count;r++)
                    if (Sm.GetGrdStr(Grd1, r, 2).Length > 0 && Sm.GetGrdStr(Grd1, r, 41).Length > 0)
                        Tax += Sm.GetGrdDec(Grd1, r, 41);
            }
            TxtTotalWithTax.EditValue = Sm.FormatNum(Total+Tax, 0);
        }

        internal void ComputeAmt()
        {
            decimal TotalWithTax = 0m;
            ComputeTotalWithoutTax();
            ComputeTotalWithTax();
            if (TxtTotalWithTax.Text.Length != 0) TotalWithTax = decimal.Parse(TxtTotalWithTax.Text);
            TxtAmt.EditValue = Sm.FormatNum(TotalWithTax, 0);
        }

        private decimal GetTaxRate(string TaxCode)
        {
            var TaxRate = Sm.GetValue("Select TaxRate from TblTax Where TaxCode=@Param;", TaxCode);
            if (TaxRate.Length != 0) return decimal.Parse(TaxRate);
            return 0m;
        }

        private void SetLueVdCode(ref DXE.LookUpEdit Lue, string VdCode)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            if (VdCode.Length != 0)
            {
                SQL.AppendLine("Select VdCode As Col1, VdName As Col2 ");
                SQL.AppendLine("From TblVendor Where VdCode=@VdCode;");
            }
            else
            {
                SQL.AppendLine("Select Distinct A.VdCode As Col1, C.VdName As Col2 ");
                SQL.AppendLine("From TblRecvVdHdr A ");
                SQL.AppendLine("Inner Join TblRecvVdDtl B ");
                SQL.AppendLine("    On A.DocNo=B.DocNo ");
                SQL.AppendLine("    And B.CancelInd='N' ");
                SQL.AppendLine("    And IfNull(B.Status, 'O')='A' ");
                SQL.AppendLine("    And Not Exists( ");
                SQL.AppendLine("        Select 1 ");
                SQL.AppendLine("        From TblPurchaseInvoiceHdr T1, TblPurchaseInvoiceDtl T2 ");
                SQL.AppendLine("        Where T1.DocNo=T2.DocNo ");
                SQL.AppendLine("        And T1.CancelInd='N' ");
                SQL.AppendLine("        And T2.RecvVdDocNo=B.DocNo ");
                SQL.AppendLine("        And T2.RecvVdDNo=B.DNo ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("Inner Join TblVendor C On A.VdCode=C.VdCode ");
                if (mIsGroupPaymentTermActived)
                {
                    SQL.AppendLine("Inner Join TblPODtl D On B.PODocNo=D.DocNo And B.PODNo=D.DNo And D.CancelInd='N' ");
                    SQL.AppendLine("Inner Join TblPORequestDtl E On D.PORequestDocNo=E.DocNo And D.PORequestDNo=E.DNo ");
                    SQL.AppendLine("Inner Join TblQtHdr F On E.QtDocNo=F.DocNo ");
                    SQL.AppendLine("    And F.PtCode Is Not Null ");
                    SQL.AppendLine("    And F.PtCode In (");
                    SQL.AppendLine("        Select PtCode From TblGroupPaymentTerm ");
                    SQL.AppendLine("        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }
                SQL.AppendLine("Where 1=1 ");
                if (mIsFilterBySite)
                {
                    SQL.AppendLine("And (A.SiteCode Is Null Or (A.SiteCode Is Not Null ");
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupSite ");
                    SQL.AppendLine("    Where SiteCode=IfNull(A.SiteCode, '') ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode=@UserCode ");
                    SQL.AppendLine("        ) ");
                    SQL.AppendLine("    ))) ");
                }
                SQL.AppendLine("Order By C.VdName;");
            }
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@VdCode", VdCode);
            cm.CommandText = SQL.ToString();

            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void ShowPreviousInvoiceHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select DocDt, VdCode, VdInvNo, VdInvDt, DueDt, DeptCode " +
                    "From TblPurchaseInvoiceHdr Where DocNo=@DocNo;",
                    new string[] 
                    { 
                        //0
                        "DocDt",  
                        
                        //1-5
                        "VdCode", "VdInvNo", "VdInvDt", "DueDt", "DeptCode"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[0]));
                        SetLueVdCode(ref LueVdCode, Sm.DrStr(dr, c[1]));
                        Sm.SetLue(LueVdCode, Sm.DrStr(dr, c[1]));
                        TxtVdInvNo.EditValue = Sm.DrStr(dr, c[2]);
                        Sm.SetDte(DteVdInvDt, Sm.DrStr(dr, c[3]));
                        Sm.SetDte(DteDueDt, Sm.DrStr(dr, c[4]));
                        SetLueDeptCode(ref LueDeptCode, Sm.DrStr(dr, c[5]));
                        Sm.SetLue(LueDeptCode, Sm.DrStr(dr, c[5]));
                    }, true
                );
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue2(SetLueVdCode), string.Empty);
        }

        private void LueVdCode_Validated(object sender, EventArgs e)
        {
            ClearGrd1();
            ComputeAmt();
        }

        private void TxtVdInvNo_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtVdInvNo);
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue2(SetLueDeptCode), string.Empty);
        }

        private void LuePaymentType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePaymentType, new Sm.RefreshLue2(Sl.SetLueOption), "VoucherPaymentType");
        }

        private void DteVdInvDt_EditValueChanged(object sender, EventArgs e)
        {
            if (mIsPIDueDateAutoFill) DteDueDt.Text = null;
        }

        private void DteTaxInvoiceDt_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd1, ref fAccept, e);
        }

        private void DteTaxInvoiceDt_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DteTaxInvoiceDt, ref fCell, ref fAccept);
        }

        private void LueTaxCode1_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueTaxCode1_Leave(object sender, EventArgs e)
        {
            if (LueTaxCode1.Visible && fAccept && fCell.ColIndex == 40)
            {
                if (Sm.GetLue(LueTaxCode1).Length == 0)
                {
                    Grd1.Cells[fCell.RowIndex, 39].Value = null;
                    Grd1.Cells[fCell.RowIndex, 40].Value = null;
                }
                else
                {
                    Grd1.Cells[fCell.RowIndex, 39].Value = Sm.GetLue(LueTaxCode1);
                    Grd1.Cells[fCell.RowIndex, 40].Value = LueTaxCode1.GetColumnValue("Col2");
                }
                ComputeTax(fCell.RowIndex);
                ComputeAmt();
                LueTaxCode1.Visible = false;
            }
        }

        private void LueTaxCode1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueTaxCode1, new Sm.RefreshLue1(Sl.SetLueTaxCode));
            ComputeAmt();
        }

        private void DteTaxInvoiceDt2_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd1, ref fAccept, e);
        }

        private void DteTaxInvoiceDt2_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DteTaxInvoiceDt2, ref fCell, ref fAccept);
        }

        private void DteTaxInvoiceDt3_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd1, ref fAccept, e);
        }

        private void DteTaxInvoiceDt3_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DteTaxInvoiceDt3, ref fCell, ref fAccept);
        }

        private void LueTaxCode2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueTaxCode2, new Sm.RefreshLue1(Sl.SetLueTaxCode));
            ComputeAmt();
        }

        private void LueTaxCode2_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueTaxCode2_Leave(object sender, EventArgs e)
        {
            if (LueTaxCode2.Visible && fAccept && fCell.ColIndex == 45)
            {
                if (Sm.GetLue(LueTaxCode2).Length == 0)
                {
                    Grd1.Cells[fCell.RowIndex, 44].Value = null;
                    Grd1.Cells[fCell.RowIndex, 45].Value = null;
                }
                else
                {
                    Grd1.Cells[fCell.RowIndex, 44].Value = Sm.GetLue(LueTaxCode2);
                    Grd1.Cells[fCell.RowIndex, 45].Value = LueTaxCode2.GetColumnValue("Col2");
                }
                ComputeTax(fCell.RowIndex);
                ComputeAmt();
                LueTaxCode2.Visible = false;
            }
        }

        private void LueTaxCode3_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueTaxCode3, new Sm.RefreshLue1(Sl.SetLueTaxCode));
            ComputeAmt();
        }

        private void LueTaxCode3_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueTaxCode3_Leave(object sender, EventArgs e)
        {
            if (LueTaxCode3.Visible && fAccept && fCell.ColIndex == 49)
            {
                if (Sm.GetLue(LueTaxCode3).Length == 0)
                {
                    Grd1.Cells[fCell.RowIndex, 48].Value = null;
                    Grd1.Cells[fCell.RowIndex, 49].Value = null;
                }
                else
                {
                    Grd1.Cells[fCell.RowIndex, 48].Value = Sm.GetLue(LueTaxCode3);
                    Grd1.Cells[fCell.RowIndex, 49].Value = LueTaxCode3.GetColumnValue("Col2");
                }
                ComputeTax(fCell.RowIndex);
                ComputeAmt();
                LueTaxCode3.Visible = false;
            }
        }

        #endregion

        #endregion
        
        #region Class

        private class PI
        {
            public int No { get; set; }
            public string TaxInvoiceNo { get; set; }
            public string TaxInvoiceDt { get; set; }
            public string TaxCode { get; set; }
            public string TaxInvoiceNo2 { get; set; }
            public string TaxInvoiceDt2 { get; set; }
            public string TaxCode2 { get; set; }
            public string TaxInvoiceNo3 { get; set; }
            public string TaxInvoiceDt3 { get; set; }
            public string TaxCode3 { get; set; }
            public decimal TaxAmt { get; set; }
            public decimal Amt { get; set; }
        }

        private class RecvVd
        {
            public string RecvVdDocNo { get; set; }
            public string RecvVdDNo { get; set; }
            public string TaxInvoiceNo { get; set; }
            public string TaxInvoiceDt { get; set; }
            public string TaxCode { get; set; }
            public string TaxInvoiceNo2 { get; set; }
            public string TaxInvoiceDt2 { get; set; }
            public string TaxCode2 { get; set; }
            public string TaxInvoiceNo3 { get; set; }
            public string TaxInvoiceDt3 { get; set; }
            public string TaxCode3 { get; set; }
            public decimal TaxAmt { get; set; }
            public decimal Amt { get; set; }
            public string Remark { get; set; }
        }

        #endregion

    }
}
