﻿#region Update
/*
    06/10/2017 [WED] bug fixing loop ke vendor
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptInvoice : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmRptInvoice(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -180);
                SetGrd();
                SetSQL();
                Sl.SetLueVdCode(ref LueVdCode);
                SetLueType();
                SetLueStatus();
                Sm.SetLue(LueStatus, "OP");
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();
            
            SQL.AppendLine("Select Tbl1.DocType, ");
            SQL.AppendLine("Case Tbl1.DocType When '1' Then 'Purchase Invoice' Else 'Purchase Return Invoice' End As DocTypeDesc, ");
            SQL.AppendLine("Tbl1.DocNo, Tbl1.DocDt, Tbl3.DeptName, Tbl1.DueDt, Tbl1.VdCode, Tbl2.VdName, Tbl1.CurCode, Tbl1.Amt1, Tbl1.Amt2, Tbl1.Amt3, ");
            SQL.AppendLine("Case Tbl1.ProcessInd When 'O' Then 'Outstanding' When 'P' Then 'Partial' When 'F' Then 'Fullfilled' End As ProcessIndDesc, Tbl1.Remark ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select '1' As DocType, X1.DocNo, X1.DocDt, X1.DeptCode, X1.DueDt, X1.VdCode, X1.CurCode, X2.Amt1, X2.Amt2, X2.Amt3, ");
            SQL.AppendLine("    Case When X2.Amt2 = 0 Then ");
	        SQL.AppendLine("        Case When X2.Amt1 = 0 Then 'F' Else 'O' End ");
            SQL.AppendLine("        Else ");
	        SQL.AppendLine("            Case When X2.Amt2>=X2.Amt1 Then 'F' Else 'P' End ");
            SQL.AppendLine("        End As ProcessInd, X2.Remark ");
            SQL.AppendLine("    From TblPurchaseInvoiceHdr X1 ");
            SQL.AppendLine("    Inner Join ( ");
	        SQL.AppendLine("        Select DocNo, Amt1, Amt2, Amt3, Remark ");
	        SQL.AppendLine("        From ( ");
            SQL.AppendLine("            Select A.DocNo, (A.Amt+A.TaxAmt-A.DownPayment) As Amt1, IfNull(B.Amt, 0) As Amt2, IfNull(C.Amt, 0) As Amt3, D.Remark   ");
		    SQL.AppendLine("            From TblPurchaseInvoiceHdr A "); 
		    SQL.AppendLine("            Left Join ( ");
			SQL.AppendLine("                Select PurchaseInvoiceDocNo, Sum(Amt) Amt ");
			SQL.AppendLine("                From ( ");
			SQL.AppendLine("                    Select T2.InvoiceDocNo As PurchaseInvoiceDocNo, T2.Amt ");
			SQL.AppendLine("                    From TblOutgoingPaymentHdr T1 ");
			SQL.AppendLine("                    Inner Join TblOutgoingPaymentDtl T2 On T1.DocNo=T2.DocNo And T2.InvoiceType='1' "); 
			SQL.AppendLine("                    Where T1.CancelInd='N' "); 
			SQL.AppendLine("                    And IfNull(T1.Status, 'O')<>'C' ");
			SQL.AppendLine("                    Union All ");
			SQL.AppendLine("                        Select PurchaseInvoiceDocNo, Amt ");
			SQL.AppendLine("                        From TblApsHdr ");
			SQL.AppendLine("                        Where CancelInd='N' ");
			SQL.AppendLine("                ) T Group By PurchaseInvoiceDocNo ");
		    SQL.AppendLine("            ) B On A.DocNo=B.PurchaseInvoiceDocNo ");
            SQL.AppendLine("            Left Join ( ");
            SQL.AppendLine("                Select PurchaseInvoiceDocNo, Sum(Amt) Amt ");
            SQL.AppendLine("                From ( ");
            SQL.AppendLine("                    Select T2.InvoiceDocNo As PurchaseInvoiceDocNo, T2.Amt ");
            SQL.AppendLine("                    From TblOutgoingPaymentHdr T1 ");
            SQL.AppendLine("                    Inner Join TblOutgoingPaymentDtl T2 On T1.DocNo=T2.DocNo And T2.InvoiceType='1' ");
            SQL.AppendLine("                    Inner Join TblVoucherRequestHdr T3 On T1.VoucherRequestDocNo=T3.DocNo ");
            SQL.AppendLine("                    Inner Join TblVoucherHdr T4 On T3.VoucherDocNo=T4.DocNo And T4.CancelInd='N' ");
            SQL.AppendLine("                    Where T1.CancelInd='N' ");
            SQL.AppendLine("                    And IfNull(T1.Status, 'O')<>'C' ");
            SQL.AppendLine("                ) T Group By PurchaseInvoiceDocNo ");
            SQL.AppendLine("            ) C On A.DocNo=C.PurchaseInvoiceDocNo ");
            SQL.AppendLine("            Left Join TblOutgoingPaymentDtl D On A.DocNo = D.InvoiceDocNo "); 
            SQL.AppendLine("            Where A.CancelInd='N' ");
	        SQL.AppendLine("        ) Tbl ");
            SQL.AppendLine("    ) X2 On X1.DocNo=X2.DocNo ");
            SQL.AppendLine("    Where X1.CancelInd='N' ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("        Select '2' As DocType, X1.DocNo, X1.DocDt, X1.DeptCode, Null As DueDt, X1.VdCode, ");
            SQL.AppendLine("        X1.CurCode, X2.Amt1, X2.Amt2, X2.Amt3, ");
            SQL.AppendLine("        Case When X2.Amt2 = 0 Then ");
	        SQL.AppendLine("            Case When X2.Amt1 = 0 Then 'F' Else 'O' End ");
            SQL.AppendLine("        Else ");
	        SQL.AppendLine("            Case When X2.Amt2>=X2.Amt1 Then 'F' Else 'P' End ");
            SQL.AppendLine("        End As ProcessInd, X2.Remark "); //56
            SQL.AppendLine("        From TblPurchaseReturnInvoiceHdr X1 ");
            SQL.AppendLine("        Inner Join ( ");
            SQL.AppendLine("            Select DocNo, Amt1, Amt2, Amt3, Remark ");
	        SQL.AppendLine("            From ( ");
            SQL.AppendLine("                Select A.DocNo, A.Amt As Amt1, IfNull(B.Amt, 0) As Amt2, IfNull(C.Amt, 0) As Amt3, D.Remark "); 
		    SQL.AppendLine("                From TblPurchaseReturnInvoiceHdr A ");
		    SQL.AppendLine("                Left Join ( ");
            SQL.AppendLine("                    Select T2.InvoiceDocNo As DocNo, Sum(T2.Amt) As Amt  ");
			SQL.AppendLine("                    From TblOutgoingPaymentHdr T1 ");
			SQL.AppendLine("                    Inner Join TblOutgoingPaymentDtl T2 On T1.DocNo=T2.DocNo And T2.InvoiceType='2' "); 
			SQL.AppendLine("                    Where T1.CancelInd='N' "); 
			SQL.AppendLine("                    And IfNull(T1.Status, 'O')<>'C' ");
			SQL.AppendLine("                    Group By T2.InvoiceDocNo ");
		    SQL.AppendLine("                ) B On A.DocNo=B.DocNo ");
            SQL.AppendLine("                Left Join ( ");
            SQL.AppendLine("                    Select T2.InvoiceDocNo As DocNo, Sum(T2.Amt) As Amt, T2.Remark ");
            SQL.AppendLine("                    From TblOutgoingPaymentHdr T1 ");
            SQL.AppendLine("                    Inner Join TblOutgoingPaymentDtl T2 On T1.DocNo=T2.DocNo And T2.InvoiceType='2' ");
            SQL.AppendLine("                    Inner Join TblVoucherRequestHdr T3 On T1.VoucherRequestDocNo=T3.DocNo ");
            SQL.AppendLine("                    Inner Join TblVoucherHdr T4 On T3.VoucherDocNo=T4.DocNo And T4.CancelInd='N' ");
            SQL.AppendLine("                    Where T1.CancelInd='N' ");
            SQL.AppendLine("                    And IfNull(T1.Status, 'O')<>'C' ");
            SQL.AppendLine("                    Group By T2.InvoiceDocNo ");
            SQL.AppendLine("                ) C On A.DocNo=C.DocNo ");
            SQL.AppendLine("                Left Join TblOutGoingpaymentDtl D On A.Docno  = D.InvoiceDocNo ");
            SQL.AppendLine("                Where A.CancelInd='N' ");
	        SQL.AppendLine("            ) Tbl ");
            SQL.AppendLine("        ) X2 On X1.DocNo=X2.DocNo ");
            SQL.AppendLine("        Where X1.CancelInd='N' ");
            SQL.AppendLine("    ) Tbl1 ");
            SQL.AppendLine("Inner Join TblVendor Tbl2 On Tbl1.VdCode=Tbl2.VdCode ");
            SQL.AppendLine("Left Join TblDepartment Tbl3 On Tbl1.DeptCode=Tbl3.DeptCode ");

            mSQL = SQL.ToString();//
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 17;
            Grd1.FrozenArea.ColCount = 1;
            
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Type", 
                        "Type",
                        "Document#",
                        "",
                        "Date",

                        //6-10
                        "Due Date",
                        "Vendor",
                        "",
                        "Vendor",
                        "Currency",
                        
                        //11-15
                        "Invoice Amount",
                        "OP Amount",
                        "Paid Amount" + Environment.NewLine + "(Voucher)",
                        "Status",
                        "Department",

                        "Description"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        0, 150, 120, 20, 80,

                        //6-10
                        80, 0, 20, 300, 70,

                        //11-15
                        120, 120, 120, 80, 150,
                        
                        200
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 4, 8 });
            Sm.GrdFormatDec(Grd1, new int[] { 11, 12, 13 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 5, 6 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 4, 5, 7, 8 }, false);
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 5, 6, 7, 9, 10, 11, 12, 13, 14, 15, 16 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4, 5, 8 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " Where 0=0 ";
                var cm = new MySqlCommand();

                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "Tbl1.DocDt");
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueVdCode), "Tbl1.VdCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueType), "Tbl1.DocType", true);
                if (Sm.GetLue(LueStatus) == "OP")
                    Filter += " And Tbl1.ProcessInd In ('O', 'P') ";
                else
                    Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueStatus), "Tbl1.ProcessInd", true);

                Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                mSQL + Filter + " Order By Tbl1.VdCode, Tbl2.VdName, Tbl1.DocType, Tbl1.CurCode, Tbl1.ProcessInd, Tbl1.DocDt;",
                new string[]
                    {
                        //0
                        "DocType", 

                        //1-5
                        "DocTypeDesc", "DocNo", "DocDt", "DueDt", "VdCode",

                        //6-10
                        "VdName", "CurCode", "Amt1", "Amt2", "Amt3", 
 
                        //11-13
                        "ProcessIndDesc", "DeptName", "Remark"
                    },
                (
                    MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 6, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 8);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 9);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 10);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 12);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 13);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 3).Length != 0)
            {
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 1) == "1")
                {
                    var f1 = new FrmPurchaseInvoice(mMenuCode);
                    f1.Tag = mMenuCode;
                    f1.Text = Sm.GetValue("Select MenuDesc From TblMenu Where Param = 'FrmPurchaseInvoice' Limit 1 ");
                    f1.WindowState = FormWindowState.Normal;
                    f1.StartPosition = FormStartPosition.CenterScreen;
                    f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 3);
                    f1.ShowDialog();
                }
                else
                {
                    var f2 = new FrmPurchaseReturnInvoice(mMenuCode);
                    f2.Tag = mMenuCode;
                    f2.WindowState = FormWindowState.Normal;
                    f2.StartPosition = FormStartPosition.CenterScreen;
                    f2.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 3);
                    f2.ShowDialog();
                }
            }

            if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length != 0)
            {
                var f3 = new FrmVendor(mMenuCode);
                f3.Tag = mMenuCode;
                f3.Text = "Master Vendor";
                f3.mMenuCode = string.Empty;
                f3.WindowState = FormWindowState.Normal;
                f3.StartPosition = FormStartPosition.CenterScreen;
                f3.mVdCode = Sm.GetGrdStr(Grd1, e.RowIndex, 7);
                f3.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 3).Length != 0)
            {
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 1) == "1")
                {
                    var f1 = new FrmPurchaseInvoice(mMenuCode);
                    f1.Tag = mMenuCode;
                    f1.Text = Sm.GetValue("Select MenuDesc From TblMenu Where Param = 'FrmPurchaseInvoice' Limit 1 ");
                    f1.WindowState = FormWindowState.Normal;
                    f1.StartPosition = FormStartPosition.CenterScreen;
                    f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 3);
                    f1.ShowDialog();
                }
                else
                {
                    var f2 = new FrmPurchaseReturnInvoice(mMenuCode);
                    f2.Tag = mMenuCode;
                    f2.WindowState = FormWindowState.Normal;
                    f2.StartPosition = FormStartPosition.CenterScreen;
                    f2.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 3);
                    f2.ShowDialog();
                }
            }

            if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length != 0)
            {
                var f3 = new FrmVendor(mMenuCode);
                f3.Tag = mMenuCode;
                f3.Text = "Master Vendor";
                f3.mMenuCode = string.Empty;
                f3.WindowState = FormWindowState.Normal;
                f3.StartPosition = FormStartPosition.CenterScreen;
                f3.mVdCode = Sm.GetGrdStr(Grd1, e.RowIndex, 7);
                f3.ShowDialog();
            }
        }

        #endregion

        #region Misc Control Nethod

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Method

        private void SetLueType()
        {
            Sm.SetLue2(
                ref LueType,
                "Select '1' As Col1, 'Purchase Invoice' As Col2 " +
                "Union All Select '2' As Col1, 'Purchase Return Invoice' As Col2;",
                0, 35, false, true, "Code", "Type", "Col2", "Col1");
        }

        private void SetLueStatus()
        {
            Sm.SetLue2(
                ref LueStatus,
                "Select 'O' As Col1, 'Outstanding' As Col2 " +
                "Union All Select 'P' As Col1, 'Partial' As Col2 " +
                "Union All Select 'F' As Col1, 'Fulfilled' As Col2 " +
                "Union All Select 'OP' As Col1, 'Outstanding+Partial' As Col2;",
                0, 35, false, true, "Code", "Status", "Col2", "Col1");
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Date");
        }

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue1(Sl.SetLueVdCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkVdCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Vendor");
        }

        private void LueType_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.CompareStr(Sm.GetLue(LueType), "<Refresh>")) LueType.EditValue = null;
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkType_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Type");
        }

        private void LueStatus_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.CompareStr(Sm.GetLue(LueStatus), "<Refresh>")) LueStatus.EditValue = null;
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkStatus_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Status");
        }

        #endregion

        #endregion
       
    }
}
