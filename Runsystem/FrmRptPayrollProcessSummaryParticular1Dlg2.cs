﻿#region Update
/*
    29/01/2020 [RF]
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using System.Drawing;
using System.Text;

using TenTec.Windows.iGridLib;
using MySql.Data.MySqlClient;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
#endregion

namespace RunSystem
{
    public partial class FrmRptPayrollProcessSummaryParticular1Dlg2 : RunSystem.FrmBase9
    {
        #region Field

        private FrmRptPayrollProcessSummaryParticular1 mFrmParent;
        private string mSQL = string.Empty, mPayrunCode = string.Empty, mEmpCode = string.Empty;

        #endregion

        #region Constructor

        public FrmRptPayrollProcessSummaryParticular1Dlg2(
            FrmRptPayrollProcessSummaryParticular1 FrmParent,
            String PayrunCode,
            string EmpCode
            )
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mPayrunCode = PayrunCode;
            mEmpCode = EmpCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                base.FrmLoad(sender, e);
                SetGrd();
                ShowData();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 6;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.RowHeader.Visible = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "OT Formula's" +Environment.NewLine+ "Code", 
                        "Date", 
                        "Hour", 
                        "Amount", 
                        "Holiday Indicator", 
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        100, 130, 150, 150, 150
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 
                3, 4
            }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 
                2
            });
            Sm.GrdColInvisible(Grd1, new int[] { 
                1
            }, false);

            Sm.GrdColReadOnly(Grd1, new int[] { 
                0, 
                1, 2, 3, 4, 5
            }, false);

            Sm.SetGrdProperty(Grd1, false);
        }

        private string GetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.PayrunCode, A.EmpCode, A.OTDt, A.OTFormulaCode, A.Hr, A.Amt, A.HolidayInd ");
            SQL.AppendLine("From TblEmployeeOT A");
            SQL.AppendLine("Where A.EmpCode=@EmpCode ");
            SQL.AppendLine("And A.PayrunCode=@PayrunCode ");
            SQL.AppendLine("Order By A.OTDt; ");

            return SQL.ToString();
        }

        private void ShowData()
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@EmpCode", mEmpCode);
            Sm.CmParam<String>(ref cm, "@PayrunCode", mPayrunCode);

            Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    GetSQL(),
                    new string[]
                    {
                        //0
                        "OTFormulaCode",

                        //1-4
                        "OTDt",
                        "Hr",
                        "Amt",
                        "HolidayInd"
                        
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 5, 4);
                        
                    }, false, false, false, false
                );
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 
                 3, 4
            });
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        #endregion

        #endregion

        #region Event

        private void ChkHideInfoInGrd_CheckedChanged(object sender, EventArgs e)
        {
            Sm.GrdColInvisible(Grd1, new int[] { 
                1  
                }, !ChkHideInfoInGrd.Checked);
        }

        #endregion
    }
}
