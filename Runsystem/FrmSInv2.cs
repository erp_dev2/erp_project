﻿#region Update
/*
    21/01/2019 [WED] new application. IOK. Beda di penentuan nilai THC, bukan berdasarkan parameter tapi hardcode
                     if(Size == 40") THC = 145 if(Size == 20") THC = 95
    13/02/2019 [DITA] buat parameter untuk nilai THC feet 20 dan feet 40 
    21/02/2019 [DITA] Validasi ocean freight saat insert namun detail container kosong
    08/04/2019 [MEY] memperbaiki decimal point amount(hapus round di query detail 2) IOK 
    10/04/2019 [WED] rounding decimal point printout query detail dihapus
    18/04/2019 [WED] Terbilang masih belum tepat
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

using FastReport;
using FastReport.Data;
using System.IO;
using System.Net;
using System.Threading;
#endregion

namespace RunSystem
{
    public partial class FrmSInv2 : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string 
            mMenuCode = "", mAccessInd = "", mDocNo = "",
            mIsFormPrintOutSInv;
        internal FrmSInv2Find FrmFind;
        internal bool mIsCustomerItemNameMandatory = false;
        private bool 
            mIsLocalDocNoAccesible = false,
            mIsNotCopySalesLocalDocNo = false;
        private string mLocalDocument = "0";
        decimal TotalAmt = 0m, THC = 0m;
        iGCell fCell;
        bool fAccept;
        private string
            mPortForFTPClient = string.Empty,
            mHostAddrForFTPClient = string.Empty,
            mSharedFolderForFTPClient = string.Empty,
            mUsernameForFTPClient = string.Empty,
            mPasswordForFTPClient = string.Empty,
            mFileSizeMaxUploadFTPClient = string.Empty,
            mFormatFTPClient = string.Empty,
            mTHC20Feet = string.Empty,
            mTHC40Feet = string.Empty;
        private byte[] downloadedData;
        #endregion

        #region Constructor

        public FrmSInv2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Sales Invoice";

            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            GetParameter();
            SetGrd();
            SetFormControl(mState.View);

            if (mLocalDocument == "1")
                LblLocalDoc.Text = "      No. Invoice";

            base.FrmLoad(sender, e);
            LueItCode1.Visible = false;
            LueItCode2.Visible = false;
            LueItCode3.Visible = false;
            LueItCode4.Visible = false;
            LueItCode5.Visible = false;
            LueItCode6.Visible = false;
            LueItCode7.Visible = false;
            LueItCode8.Visible = false;
            LueItCode9.Visible = false;
            LueItCode10.Visible = false;
            LueItCode11.Visible = false;
            LueItCode12.Visible = false;
            LueItCode13.Visible = false;
            LueItCode14.Visible = false;
            LueItCode15.Visible = false;
            LueItCode16.Visible = false;
            LueItCode17.Visible = false;
            LueItCode18.Visible = false;
            LueItCode19.Visible = false;
            LueItCode20.Visible = false;
            LueItCode21.Visible = false;
            LueItCode22.Visible = false;
            LueItCode23.Visible = false;
            LueItCode24.Visible = false;
            LueItCode25.Visible = false;


            SetLueCtCode(ref LueCtCode);
            
            if (mDocNo.Length != 0)
            {
                ShowData(mDocNo);
                BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            SetGrdItem(new List<iGrid> 
                { 
                    Grd11, Grd12, Grd13, Grd14, Grd15,
                    Grd16, Grd17, Grd18, Grd19, Grd20,
                    Grd21, Grd22, Grd23, Grd24, Grd25,
                    Grd26, Grd27, Grd28, Grd29, Grd30,
                    Grd31, Grd32, Grd33, Grd34, Grd35
                });

            #region Grid1
            Grd1.Cols.Count = 23;
            Grd1.FrozenArea.ColCount = 4;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "",
                        
                        //1-5
                        "Item's"+Environment.NewLine+"Code",
                        "Item's"+Environment.NewLine+"Name",
                        "",
                        "Item's"+Environment.NewLine+"Local Code",
                        "SO#",

                        //6-10
                        "",
                        "SO DNo",
                        "Packaging"+Environment.NewLine+"Unit",
                        "Outstanding"+Environment.NewLine+"Packaging"+Environment.NewLine+"Quantity",
                        "Packaging"+Environment.NewLine+"Quantity",

                        //11-15
                        "Balance",
                        "Outstanding"+Environment.NewLine+"SO",
                        "Requested"+Environment.NewLine+"Quantity (Sales)",
                        "Balance",
                        "UoM"+Environment.NewLine+"(Sales)",

                        //16-19
                        "Requested"+Environment.NewLine+"Quantity (Inventory)",
                        "UoM"+Environment.NewLine+"(Inventory)",
                        "Currency",
                        "Unit"+Environment.NewLine+"Price",
                        "Amount",

                        //21-22
                        "Delivery"+Environment.NewLine+"Date",
                        "Remark"
                    },
                     new int[] 
                    {
                        20, 
                        80, 200, 20, 150, 150, 
                        20, 50, 100, 100, 100,   
                        80, 100, 100, 80, 80, 
                        130, 80, 80, 100, 100,
                        100, 150,
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 21 });
            Sm.GrdColReadOnly(Grd1, new int[] { 1, 2, 4, 5, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22 });
            Sm.GrdFormatDec(Grd1, new int[] { 8, 9, 10, 11, 12, 13, 14, 16, 19, 20}, 0);
            Sm.GrdColButton(Grd1, new int[] { 0, 3, 6});
            Sm.GrdColInvisible(Grd1, new int[] { 1, 4, 7 }, false);
            #endregion
        }

        private void SetGrdItem(List<iGrid> ListofGrd)
        {
            ListofGrd.ForEach(Grd =>
            {
                Grd.Cols.Count = 17;
                Grd.FrozenArea.ColCount = 2;
                Grd.ReadOnly = false;
                Sm.GrdHdrWithColWidth(
                        Grd,
                        new string[] 
                        {
                        //0  
                        "Item's"+Environment.NewLine+"Code",
                        //1-5
                        "Item's"+Environment.NewLine+"Name",
                        "",
                        "Item's"+Environment.NewLine+"Local Code",
                        "SO#",
                        "",
                        //6-10
                        "SO DNo",
                        "Packaging"+Environment.NewLine+"Unit",
                        "Packaging"+Environment.NewLine+"Quantity",
                        "Requested"+Environment.NewLine+"Quantity (Sales)",
                        "UoM"+Environment.NewLine+"(Sales)",
                        //11-15
                        "Requested"+Environment.NewLine+"Quantity (Inventory)",
                        "UoM"+Environment.NewLine+"(Inventory)",
                        "Currency",
                        "Unit"+Environment.NewLine+"Price",
                        "FOB"+Environment.NewLine+"Price",
                        //16
                        "Amount",
                        },
                        new int[] 
                        { 
                            80, 
                            200, 20, 100, 150, 20, 
                            50, 100, 100, 100, 80, 
                            100, 80, 80, 100, 100,
                            150
                        }
                    );
                Sm.GrdColReadOnly(Grd, new int[] { 3, 4, 6, 7, 9, 10, 11, 12, 13, 14, 15, 16 });
                Sm.GrdFormatDec(Grd, new int[] { 8, 9, 11, 14, 15, 16 }, 0);
                Sm.GrdColButton(Grd, new int[] {  2, 5 });
                Sm.GrdColInvisible(Grd, new int[] { 0, 3, 6 }, false);
            });
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, LueCtCode, TxtPLDocNo, LueNotify, TxtSIDocNo, TxtLocalDocNo,
                        TxtSalesContract, TxtLcNo, LCDocDt, MeeAccount, MeeIssued, MeeIssued2, MeeIssued3,
                        MeeIssued4, MeeOrder, MeeOrder2, MeeOrder3, MeeOrder4, MeeRemark, MeeRemark2,
                        TxtCnt1, TxtCnt2, TxtCnt3, TxtCnt4, TxtCnt5, 
                        TxtCnt6, TxtCnt7, TxtCnt8, TxtCnt9, TxtCnt10,
                        TxtCnt11, TxtCnt12, TxtCnt13, TxtCnt14, TxtCnt15,
                        TxtCnt16, TxtCnt17, TxtCnt18, TxtCnt19, TxtCnt20,
                        TxtCnt21, TxtCnt22, TxtCnt23, TxtCnt24, TxtCnt25,
                        TxtSeal1, TxtSeal2, TxtSeal3, TxtSeal4, TxtSeal5, 
                        TxtSeal6, TxtSeal7, TxtSeal8, TxtSeal9, TxtSeal10,
                        TxtSeal11, TxtSeal12, TxtSeal13, TxtSeal14, TxtSeal15, 
                        TxtSeal16, TxtSeal17, TxtSeal18, TxtSeal19, TxtSeal20,
                        TxtSeal21, TxtSeal22, TxtSeal23, TxtSeal24, TxtSeal25, 
                        TxtTotal1, TxtTotal2, TxtTotal3, TxtTotal4, TxtTotal5, 
                        TxtTotal6, TxtTotal7, TxtTotal8, TxtTotal9, TxtTotal10,
                        TxtTotal11, TxtTotal12, TxtTotal13, TxtTotal14, TxtTotal15,
                        TxtTotal16, TxtTotal17, TxtTotal18, TxtTotal19, TxtTotal20,
                        TxtTotal21, TxtTotal22, TxtTotal23, TxtTotal24, TxtTotal25,
                        TxtFreight1, TxtFreight2, TxtFreight3, TxtFreight4, TxtFreight5, 
                        TxtFreight6, TxtFreight7, TxtFreight8, TxtFreight9, TxtFreight10,
                        TxtFreight11, TxtFreight12, TxtFreight13, TxtFreight14, TxtFreight15,
                        TxtFreight16, TxtFreight17, TxtFreight18, TxtFreight19, TxtFreight20,
                        TxtFreight21, TxtFreight22, TxtFreight23, TxtFreight24, TxtFreight25
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd11, new int[] { 0, 1, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 });
                    Sm.GrdColReadOnly(true, true, Grd12, new int[] { 0, 1, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 });
                    Sm.GrdColReadOnly(true, true, Grd13, new int[] { 0, 1, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 });
                    Sm.GrdColReadOnly(true, true, Grd14, new int[] { 0, 1, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 });
                    Sm.GrdColReadOnly(true, true, Grd15, new int[] { 0, 1, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 });
                    Sm.GrdColReadOnly(true, true, Grd16, new int[] { 0, 1, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 });
                    Sm.GrdColReadOnly(true, true, Grd17, new int[] { 0, 1, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 });
                    Sm.GrdColReadOnly(true, true, Grd18, new int[] { 0, 1, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 });
                    Sm.GrdColReadOnly(true, true, Grd19, new int[] { 0, 1, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 });
                    Sm.GrdColReadOnly(true, true, Grd20, new int[] { 0, 1, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 });
                    Sm.GrdColReadOnly(true, true, Grd21, new int[] { 0, 1, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 });
                    Sm.GrdColReadOnly(true, true, Grd22, new int[] { 0, 1, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 });
                    Sm.GrdColReadOnly(true, true, Grd23, new int[] { 0, 1, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 });
                    Sm.GrdColReadOnly(true, true, Grd24, new int[] { 0, 1, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 });
                    Sm.GrdColReadOnly(true, true, Grd25, new int[] { 0, 1, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 });
                    Sm.GrdColReadOnly(true, true, Grd26, new int[] { 0, 1, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 });
                    Sm.GrdColReadOnly(true, true, Grd27, new int[] { 0, 1, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 });
                    Sm.GrdColReadOnly(true, true, Grd28, new int[] { 0, 1, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 });
                    Sm.GrdColReadOnly(true, true, Grd29, new int[] { 0, 1, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 });
                    Sm.GrdColReadOnly(true, true, Grd30, new int[] { 0, 1, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 });
                    Sm.GrdColReadOnly(true, true, Grd31, new int[] { 0, 1, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 });
                    Sm.GrdColReadOnly(true, true, Grd32, new int[] { 0, 1, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 });
                    Sm.GrdColReadOnly(true, true, Grd33, new int[] { 0, 1, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 });
                    Sm.GrdColReadOnly(true, true, Grd34, new int[] { 0, 1, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 });
                    Sm.GrdColReadOnly(true, true, Grd35, new int[] { 0, 1, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 });
                    BtnPL.Enabled = false;
                    BtnPL2.Enabled = true;
                    BtnTemplate.Enabled = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    {   
                        DteDocDt, LueCtCode,LueNotify,  MeeAccount, TxtLocalDocNo,
                        TxtSalesContract, TxtLcNo, LCDocDt, MeeIssued, MeeIssued2, MeeIssued3,
                        MeeIssued4, MeeOrder, MeeOrder2, MeeOrder3, MeeOrder4,MeeRemark, MeeRemark2,
                        TxtCnt1, TxtCnt2, TxtCnt3, TxtCnt4, TxtCnt5, 
                        TxtCnt6, TxtCnt7, TxtCnt8, TxtCnt9, TxtCnt10,
                        TxtCnt11, TxtCnt12, TxtCnt13, TxtCnt14, TxtCnt15,
                        TxtCnt16, TxtCnt17, TxtCnt18, TxtCnt19, TxtCnt20,
                        TxtCnt21, TxtCnt22, TxtCnt23, TxtCnt24, TxtCnt25,
                        TxtSeal1, TxtSeal2, TxtSeal3, TxtSeal4, TxtSeal5, 
                        TxtSeal6, TxtSeal7, TxtSeal8, TxtSeal9, TxtSeal10,
                        TxtSeal11, TxtSeal12, TxtSeal13, TxtSeal14, TxtSeal15,
                        TxtSeal16, TxtSeal17, TxtSeal18, TxtSeal19, TxtSeal20,
                        TxtSeal21, TxtSeal22, TxtSeal23, TxtSeal24, TxtSeal25,
                        TxtFreight1, TxtFreight2, TxtFreight3, TxtFreight4, TxtFreight5, 
                        TxtFreight6, TxtFreight7, TxtFreight8, TxtFreight9, TxtFreight10,
                        TxtFreight11, TxtFreight12, TxtFreight13, TxtFreight14, TxtFreight15,
                        TxtFreight16, TxtFreight17, TxtFreight18, TxtFreight19, TxtFreight20,
                        TxtFreight21, TxtFreight22, TxtFreight23, TxtFreight24, TxtFreight25
                    }, false);
                    BtnPL.Enabled = true;
                    BtnPL2.Enabled = true;
                    BtnTemplate.Enabled = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    var cm = new MySqlCommand()
                    {
                        CommandText =
                            "Select A.DocNo From TblSP A "+ 
                            "Inner Join TblSIHdr B On A.DocNo = B.SPDocNo " +
                            "Where A.Status <> 'P' And B.DocNo=@DocNo "
                    };
                    Sm.CmParam<String>(ref cm, "@DocNo", TxtSIDocNo.Text);
                    
                        if (Sm.IsDataExist(cm))
                        {
                            if (mIsLocalDocNoAccesible)
                            {
                                Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                                {   
                                    MeeAccount, TxtLocalDocNo,
                                    TxtSalesContract, DteDocDt, TxtLcNo, LCDocDt, MeeIssued, MeeIssued2, MeeIssued3,
                                    MeeIssued4, MeeOrder, MeeOrder2, MeeOrder3, MeeOrder4,MeeRemark, MeeRemark2,
                                }, false);
                            }
                            else
                            {
                                Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                                {   
                                    MeeAccount,
                                    TxtSalesContract, DteDocDt, TxtLcNo, LCDocDt, MeeIssued, MeeIssued2, MeeIssued3,
                                    MeeIssued4, MeeOrder, MeeOrder2, MeeOrder3, MeeOrder4,MeeRemark, MeeRemark2,
                                }, false);
                            }
                        }
                        else
                        {
                            if (mIsLocalDocNoAccesible)
                            {
                                Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                                {   
                                    MeeAccount, TxtLocalDocNo,
                                    TxtSalesContract, DteDocDt, TxtLcNo, LCDocDt, MeeIssued, MeeIssued2, MeeIssued3,
                                    MeeIssued4, MeeOrder, MeeOrder2, MeeOrder3, MeeOrder4,MeeRemark, MeeRemark2,
                                    TxtCnt1, TxtCnt2, TxtCnt3, TxtCnt4, TxtCnt5, 
                                    TxtCnt6, TxtCnt7, TxtCnt8, TxtCnt9, TxtCnt10,
                                    TxtCnt11, TxtCnt12, TxtCnt13, TxtCnt14, TxtCnt15,
                                    TxtCnt16, TxtCnt17, TxtCnt18, TxtCnt19, TxtCnt20,
                                    TxtCnt21, TxtCnt22, TxtCnt23, TxtCnt24, TxtCnt25,
                                    TxtSeal1, TxtSeal2, TxtSeal3, TxtSeal4, TxtSeal5, 
                                    TxtSeal6, TxtSeal7, TxtSeal8, TxtSeal9, TxtSeal10,
                                    TxtSeal11, TxtSeal12, TxtSeal13, TxtSeal14, TxtSeal15, 
                                    TxtSeal16, TxtSeal17, TxtSeal18, TxtSeal19, TxtSeal20,
                                    TxtSeal21, TxtSeal22, TxtSeal23, TxtSeal24, TxtSeal25, 
                                    TxtFreight1, TxtFreight2, TxtFreight3, TxtFreight4, TxtFreight5, 
                                    TxtFreight6, TxtFreight7, TxtFreight8, TxtFreight9, TxtFreight10,
                                    TxtFreight11, TxtFreight12, TxtFreight13, TxtFreight14, TxtFreight15,
                                    TxtFreight16, TxtFreight17, TxtFreight18, TxtFreight19, TxtFreight20,
                                    TxtFreight21, TxtFreight22, TxtFreight23, TxtFreight24, TxtFreight25
                                }, false);
                            }
                            else
                            {
                                Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                                {   
                                    MeeAccount,
                                    TxtSalesContract, DteDocDt, TxtLcNo, LCDocDt, MeeIssued, MeeIssued2, MeeIssued3,
                                    MeeIssued4, MeeOrder, MeeOrder2, MeeOrder3, MeeOrder4,MeeRemark, MeeRemark2,
                                    TxtCnt1, TxtCnt2, TxtCnt3, TxtCnt4, TxtCnt5, 
                                    TxtCnt6, TxtCnt7, TxtCnt8, TxtCnt9, TxtCnt10,
                                    TxtCnt11, TxtCnt12, TxtCnt13, TxtCnt14, TxtCnt15,
                                    TxtCnt16, TxtCnt17, TxtCnt18, TxtCnt19, TxtCnt20,
                                    TxtCnt21, TxtCnt22, TxtCnt23, TxtCnt24, TxtCnt25,
                                    TxtSeal1, TxtSeal2, TxtSeal3, TxtSeal4, TxtSeal5, 
                                    TxtSeal6, TxtSeal7, TxtSeal8, TxtSeal9, TxtSeal10,
                                    TxtSeal11, TxtSeal12, TxtSeal13, TxtSeal14, TxtSeal15, 
                                    TxtSeal16, TxtSeal17, TxtSeal18, TxtSeal19, TxtSeal20,
                                    TxtSeal21, TxtSeal22, TxtSeal23, TxtSeal24, TxtSeal25, 
                                    TxtFreight1, TxtFreight2, TxtFreight3, TxtFreight4, TxtFreight5, 
                                    TxtFreight6, TxtFreight7, TxtFreight8, TxtFreight9, TxtFreight10,
                                    TxtFreight11, TxtFreight12, TxtFreight13, TxtFreight14, TxtFreight15,
                                    TxtFreight16, TxtFreight17, TxtFreight18, TxtFreight19, TxtFreight20,
                                    TxtFreight21, TxtFreight22, TxtFreight23, TxtFreight24, TxtFreight25
                                }, false);
                            }
                        }
                    BtnPL.Enabled = false;
                    BtnPL2.Enabled = true;
                    DteDocDt.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, TxtLocalDocNo, DteDocDt, LueCtCode, TxtPLDocNo, LueNotify, MeeAccount,TxtSIDocNo, 
                TxtSalesContract, TxtLcNo, LCDocDt, MeeIssued, MeeIssued2, MeeIssued3,
                MeeIssued4, MeeOrder, MeeOrder2, MeeOrder3, MeeOrder4, MeeRemark, MeeRemark2,
                TxtCnt1, TxtCnt2, TxtCnt3, TxtCnt4, TxtCnt5, 
                TxtCnt6, TxtCnt7, TxtCnt8, TxtCnt9, TxtCnt10,
                TxtCnt11, TxtCnt12, TxtCnt13, TxtCnt14, TxtCnt15,
                TxtCnt16, TxtCnt17, TxtCnt18, TxtCnt19, TxtCnt20,
                TxtCnt21, TxtCnt22, TxtCnt23, TxtCnt24, TxtCnt25,
                TxtSeal1, TxtSeal2, TxtSeal3, TxtSeal4, TxtSeal5, 
                TxtSeal6, TxtSeal7, TxtSeal8, TxtSeal9, TxtSeal10,
                TxtSeal11, TxtSeal12, TxtSeal13, TxtSeal14, TxtSeal15,
                TxtSeal16, TxtSeal17, TxtSeal18, TxtSeal19, TxtSeal20,
                TxtSeal21, TxtSeal22, TxtSeal23, TxtSeal24, TxtSeal25,
            });
            ChkFOB.Checked = false;

            Sm.SetControlNumValueZero(new List<DXE.TextEdit>
            {
                TxtTotal1, TxtTotal2, TxtTotal3, TxtTotal4, TxtTotal5, 
                TxtTotal6, TxtTotal7, TxtTotal8, TxtTotal9, TxtTotal10,
                TxtTotal11, TxtTotal12, TxtTotal13, TxtTotal14, TxtTotal15, 
                TxtTotal16, TxtTotal17, TxtTotal18, TxtTotal19, TxtTotal20,
                TxtTotal21, TxtTotal22, TxtTotal23, TxtTotal24, TxtTotal25,
                TxtFreight1, TxtFreight2, TxtFreight3, TxtFreight4, TxtFreight5, 
                TxtFreight6, TxtFreight7, TxtFreight8, TxtFreight9, TxtFreight10,
                TxtFreight11, TxtFreight12, TxtFreight13, TxtFreight14, TxtFreight15,
                TxtFreight16, TxtFreight17, TxtFreight18, TxtFreight19, TxtFreight20,
                TxtFreight21, TxtFreight22, TxtFreight23, TxtFreight24, TxtFreight25
            }, 0);
            
            ClearGrd(new List<iGrid>(){
                Grd11, Grd12, Grd13, Grd14, Grd15,
                Grd16, Grd17, Grd18, Grd19, Grd20,
                Grd21, Grd22, Grd23, Grd24, Grd25, 
                Grd26, Grd27, Grd28, Grd29, Grd30,
                Grd31, Grd32, Grd33, Grd34, Grd35, 
                Grd1
            });
        }

        private void ClearGrd(List<iGrid> ListOfGrd)
        {
            ListOfGrd.ForEach(Grd =>
            {
                Sm.ClearGrd(Grd, true);
                Sm.SetGrdNumValueZero(ref Grd, 0, new int[] { 8, 9, 11, 14, 15, 16 });
                Sm.FocusGrd(Grd, 0, 0);
            });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmSInv2Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);

            Sm.SetDteCurrentDate(DteDocDt);
            LCDocDt.DateTime = Sm.ConvertDate(Sm.ServerCurrentDateTime());
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                InsertData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            ParPrint();
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = "";
            if (TxtDocNo.Text.Length == 0)
            {
                DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "SInv", "TblSinv");
            }
            else
            {
                DocNo = TxtDocNo.Text;
            }

            var cml = new List<MySqlCommand>();

            cml.Add(SaveSInv(DocNo));
                      
            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsLueEmpty(LueCtCode, "Customer") ||
                Sm.IsTxtEmpty(TxtPLDocNo, "Shipping Instruction", false) ||
                Sm.IsLueEmpty(LueNotify, "Notify Party") ||
                Sm.IsMeeEmpty(MeeAccount, "Account of Messrs") ||
                IsGrdEmpty() ||
                IsGrdQtyNotValid() ||
                IsSPCancelled()||
                IsSPFinal() ||
                IsOceanFreightNotValid()
                ;
        }

        private bool IsOceanFreightNotValid()
        {
            for (int Tab = 1; Tab <= 25; Tab++)
            {
                if (Tab == 1)
                {
                    if (Decimal.Parse(TxtFreight1.Text) != 0 && Grd11.Rows.Count == 1)
                    {
                        Sm.StdMsg(mMsgType.Warning, "Container's Detail in No.1 is empty");
                        tabControl1.SelectTab("tabPage1");
                        return true;
                    }
                }
                if (Tab == 2)
                {
                    if (Decimal.Parse(TxtFreight2.Text) != 0 && Grd12.Rows.Count == 1)
                    {
                        Sm.StdMsg(mMsgType.Warning, "Container's Detail in No.2 is empty");
                        tabControl1.SelectTab("tabPage2");
                        return true;
                    }
                }
                if (Tab == 3)
                {
                    if (Decimal.Parse(TxtFreight3.Text) != 0 && Grd13.Rows.Count == 1)
                    {
                        Sm.StdMsg(mMsgType.Warning, "Container's Detail in No.3 is empty");
                        tabControl1.SelectTab("tabPage4");
                        return true;
                    }
                }
                if (Tab == 4)
                {
                    if (Decimal.Parse(TxtFreight4.Text) != 0 && Grd14.Rows.Count == 1)
                    {
                        Sm.StdMsg(mMsgType.Warning, "Container's Detail in No.4 is empty");
                        tabControl1.SelectTab("tabPage5");
                        return true;
                    }
                }
                if (Tab == 5)
                {
                    if (Decimal.Parse(TxtFreight5.Text) != 0 && Grd15.Rows.Count == 1)
                    {
                        Sm.StdMsg(mMsgType.Warning, "Container's Detail in No.5 is empty");
                        tabControl1.SelectTab("tabPage5");
                        return true;
                    }
                }
                if (Tab == 6)
                {
                    if (Decimal.Parse(TxtFreight6.Text) != 0 && Grd16.Rows.Count == 1)
                    {
                        Sm.StdMsg(mMsgType.Warning, "Container's Detail in No.6 is empty");
                        tabControl1.SelectTab("tabPage6");
                        return true;
                    }
                }
                if (Tab == 7)
                {
                    if (Decimal.Parse(TxtFreight7.Text) != 0 && Grd17.Rows.Count == 1)
                    {
                        Sm.StdMsg(mMsgType.Warning, "Container's Detail in No.7 is empty");
                        tabControl1.SelectTab("tabPage7");
                        return true;
                    }
                }
                if (Tab == 8)
                {
                    if (Decimal.Parse(TxtFreight8.Text) != 0 && Grd18.Rows.Count == 1)
                    {
                        Sm.StdMsg(mMsgType.Warning, "Container's Detail in No.8 is empty");
                        tabControl1.SelectTab("tabPage8");
                        return true;
                    }
                }
                if (Tab == 9)
                {
                    if (Decimal.Parse(TxtFreight9.Text) != 0 && Grd19.Rows.Count == 1)
                    {
                        Sm.StdMsg(mMsgType.Warning, "Container's Detail in No.9 is empty");
                        tabControl1.SelectTab("tabPage9");
                        return true;
                    }
                }
                if (Tab == 10)
                {
                    if (Decimal.Parse(TxtFreight10.Text) != 0 && Grd20.Rows.Count == 1)
                    {
                        Sm.StdMsg(mMsgType.Warning, "Container's Detail in No.10 is empty");
                        tabControl1.SelectTab("tabPage10");
                        return true;
                    }
                }
                if (Tab == 11)
                {
                    if (Decimal.Parse(TxtFreight11.Text) != 0 && Grd21.Rows.Count == 1)
                    {
                        Sm.StdMsg(mMsgType.Warning, "Container's Detail in No.11 is empty");
                        tabControl1.SelectTab("tabPage11");
                        return true;
                    }
                }
                if (Tab == 12)
                {
                    if (Decimal.Parse(TxtFreight12.Text) != 0 && Grd22.Rows.Count == 1)
                    {
                        Sm.StdMsg(mMsgType.Warning, "Container's Detail in No.12 is empty");
                        tabControl1.SelectTab("tabPage12");
                        return true;
                    }
                }
                if (Tab == 13)
                {
                    if (Decimal.Parse(TxtFreight13.Text) != 0 && Grd23.Rows.Count == 1)
                    {
                        Sm.StdMsg(mMsgType.Warning, "Container's Detail in No.13 is empty");
                        tabControl1.SelectTab("tabPage13");
                        return true;
                    }
                }
                if (Tab == 14)
                {
                    if (Decimal.Parse(TxtFreight14.Text) != 0 && Grd24.Rows.Count == 1)
                    {
                        Sm.StdMsg(mMsgType.Warning, "Container's Detail in No.14 is empty");
                        tabControl1.SelectTab("tabPage14");
                        return true;
                    }
                }
                if (Tab == 15)
                {
                    if (Decimal.Parse(TxtFreight15.Text) != 0 && Grd25.Rows.Count == 1)
                    {
                        Sm.StdMsg(mMsgType.Warning, "Container's Detail in No.15 is empty");
                        tabControl1.SelectTab("tabPage15");
                        return true;
                    }
                }
                if (Tab == 16)
                {
                    if (Decimal.Parse(TxtFreight16.Text) != 0 && Grd26.Rows.Count == 1)
                    {
                        Sm.StdMsg(mMsgType.Warning, "Container's Detail in No.16 is empty");
                        tabControl1.SelectTab("tabPage16");
                        return true;
                    }
                }
                if (Tab == 17)
                {
                    if (Decimal.Parse(TxtFreight17.Text) != 0 && Grd27.Rows.Count == 1)
                    {
                        Sm.StdMsg(mMsgType.Warning, "Container's Detail in No.17 is empty");
                        tabControl1.SelectTab("tabPage17");
                        return true;
                    }
                }
                if (Tab == 18)
                {
                    if (Decimal.Parse(TxtFreight18.Text) != 0 && Grd28.Rows.Count == 1)
                    {
                        Sm.StdMsg(mMsgType.Warning, "Container's Detail in No.18 is empty");
                        tabControl1.SelectTab("tabPage18");
                        return true;
                    }
                }
                if (Tab == 19)
                {
                    if (Decimal.Parse(TxtFreight19.Text) != 0 && Grd29.Rows.Count == 1)
                    {
                        Sm.StdMsg(mMsgType.Warning, "Container's Detail in No.19 is empty");
                        tabControl1.SelectTab("tabPage19");
                        return true;
                    }
                }
                if (Tab == 20)
                {
                    if (Decimal.Parse(TxtFreight20.Text) != 0 && Grd30.Rows.Count == 1)
                    {
                        Sm.StdMsg(mMsgType.Warning, "Container's Detail in No.20 is empty");
                        tabControl1.SelectTab("tabPage20");
                        return true;
                    }
                }
                if (Tab == 21)
                {
                    if (Decimal.Parse(TxtFreight21.Text) != 0 && Grd31.Rows.Count == 1)
                    {
                        Sm.StdMsg(mMsgType.Warning, "Container's Detail in No.21 is empty");
                        tabControl1.SelectTab("tabPage21");
                        return true;
                    }
                }
                if (Tab == 22)
                {
                    if (Decimal.Parse(TxtFreight22.Text) != 0 && Grd32.Rows.Count == 1)
                    {
                        Sm.StdMsg(mMsgType.Warning, "Container's Detail in No.22 is empty");
                        tabControl1.SelectTab("tabPage22");
                        return true;
                    }
                }

                if (Tab == 23)
                {
                    if (Decimal.Parse(TxtFreight23.Text) != 0 && Grd33.Rows.Count == 1)
                    {
                        Sm.StdMsg(mMsgType.Warning, "Container's Detail in No.23 is empty");
                        tabControl1.SelectTab("tabPage23");
                        return true;
                    }
                }
                if (Tab == 24)
                {
                    if (Decimal.Parse(TxtFreight24.Text) != 0 && Grd34.Rows.Count == 1)
                    {
                        Sm.StdMsg(mMsgType.Warning, "Container's Detail in No.24 is empty");
                        tabControl1.SelectTab("tabPage24");
                        return true;
                    }
                }
                if (Tab == 25)
                {
                    if (Decimal.Parse(TxtFreight25.Text) != 0 && Grd35.Rows.Count == 1)
                    {
                        Sm.StdMsg(mMsgType.Warning, "Container's Detail in No.25 is empty");
                        tabControl1.SelectTab("tabPage25");
                        return true;
                    }
                }

            }

            return false;

        }

        private bool IsSPFinal()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select A.DocNo From TblSP A " +
                    "Inner Join TblSIHdr B On A.DocNo = B.SPDocNo " +
                    "Where A.Status = 'F' And B.DocNo=@DocNo "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtSIDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This document already Final.");
                return true;
            }
            return false;
        }

        private bool IsSPCancelled()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select A.DocNo From TblSP A " +
                    "Inner Join TblSIHdr B On A.DocNo = B.SPDocNo " +
                    "Where A.Status = 'C' And B.DocNo=@DocNo "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtSIDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This document already cancelled.");
                return true;
            }
            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 Item.");
                return true;
            }
        return false;
        }

        private bool IsGrdQtyNotValid()
        {
            string Msg = "";

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                Msg =
                   "Item's Code : " + Sm.GetGrdStr(Grd1, Row, 1) + Environment.NewLine +
                   "Item's Name : " + Sm.GetGrdStr(Grd1, Row, 2) + Environment.NewLine +
                   "SO : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine +
                   "Item's Local : " + Sm.GetGrdStr(Grd1, Row, 5) + Environment.NewLine +
                   "Packaging Unit : " + Sm.GetGrdStr(Grd1, Row, 8) + Environment.NewLine;

                if (
                    Sm.IsGrdValueEmpty(Grd1, Row, 10, true, Msg + "Requested Quantity (Packaging Unit) should not be 0.") ||
                    Sm.IsGrdValueEmpty(Grd1, Row, 13, true, Msg + "Requested Quantity (Sales) should not be 0.") ||
                    Sm.IsGrdValueEmpty(Grd1, Row, 16, true, Msg + "Requested Quantity (Inventory) should not be 0.")
                    )
                    return true;



                if (Sm.GetGrdDec(Grd1, Row, 11) != 0)
                {
                    Sm.StdMsg(mMsgType.Warning,
                        Msg +
                        "Requested quantity ( " + Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 10), 0) +
                        " ) not balance with outstanding quantity ( " + Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 9), 0) +
                        " ).");
                    return true;
                }

                if (Sm.GetGrdDec(Grd1, Row, 14) !=0)
                {
                    Sm.StdMsg(mMsgType.Warning,
                        Msg +
                        "Requested quantity ( " + Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 13), 0) +
                        " ) not balance with outstanding quantity ( " + Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 12), 0) +
                        " ).");
                    return true;
                }
            }
         
            return false;
        }



        private MySqlCommand SaveSInv(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblSinv(DocNo, LocalDocNo, DocDt, Source, PLDocno, Remark, Remark2, Createby, CreateDt) values (@DocNo, @LocalDocNo, @DocDt, '2', @PLDocNo, @Remark, @Remark2, @UserCode, CurrentDateTime())  ");
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine(" Update DocDt=@DocDt, LocalDocNo=@LocalDocNo, Remark=@Remark, Remark2=@Remark2, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

            SQL.AppendLine("Insert Into TblPLHdr(DocNo, DocDt, SIDocNo, LocalDocNo,  Account, InvoiceNo, SalesContractNo, LCNo, LCDt, Issued1, ");
            SQL.AppendLine("Issued2, Issued3, Issued4, ToOrder1, ToOrder2, ToOrder3, ToOrder4, ");
            SQL.AppendLine("Cnt1, Cnt2, Cnt3, Cnt4, Cnt5, ");
            SQL.AppendLine("Cnt6, Cnt7, Cnt8, Cnt9, Cnt10, ");
            SQL.AppendLine("Cnt11, Cnt12, Cnt13, Cnt14, Cnt15, ");
            SQL.AppendLine("Cnt16, Cnt17, Cnt18, Cnt19, Cnt20, ");
            SQL.AppendLine("Cnt21, Cnt22, Cnt23, Cnt24, Cnt25, ");
            SQL.AppendLine("Seal1, Seal2, Seal3, Seal4, Seal5, ");
            SQL.AppendLine("Seal6, Seal7, Seal8, Seal9, Seal10, ");
            SQL.AppendLine("Seal11, Seal12, Seal13, Seal14, Seal15, ");
            SQL.AppendLine("Seal16, Seal17, Seal18, Seal19, Seal20, ");
            SQL.AppendLine("Seal21, Seal22, Seal23, Seal24, Seal25, ");
            SQL.AppendLine("Freight1, Freight2, Freight3, Freight4, Freight5, ");
            SQL.AppendLine("Freight6, Freight7, Freight8, Freight9, Freight10, ");
            SQL.AppendLine("Freight11, Freight12, Freight13, Freight14, Freight15, ");
            SQL.AppendLine("Freight16, Freight17, Freight18, Freight19, Freight20, ");
            SQL.AppendLine("Freight21, Freight22, Freight23, Freight24, Freight25, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@PLDocNo, @DocDt, @SIDocNo, @LocalDocNo, @Account, @InvoiceNo, @SalesContractNo, @LCNo, @LCDt, @Issued1, ");
            SQL.AppendLine("@Issued2, @Issued3, @Issued4, @ToOrder1, @ToOrder2, @ToOrder3, @ToOrder4, ");
            SQL.AppendLine("@Cnt1, @Cnt2, @Cnt3, @Cnt4, @Cnt5,");
            SQL.AppendLine("@Cnt6, @Cnt7, @Cnt8, @Cnt9, @Cnt10,");
            SQL.AppendLine("@Cnt11, @Cnt12, @Cnt13, @Cnt14, @Cnt15, ");
            SQL.AppendLine("@Cnt16, @Cnt17, @Cnt18, @Cnt19, @Cnt20,");
            SQL.AppendLine("@Cnt21, @Cnt22, @Cnt23, @Cnt24, @Cnt25, ");
            SQL.AppendLine("@Seal1, @Seal2, @Seal3, @Seal4, @Seal5, ");
            SQL.AppendLine("@Seal6, @Seal7, @Seal8, @Seal9, @Seal10, ");
            SQL.AppendLine("@Seal11, @Seal12, @Seal13, @Seal14, @Seal15, ");
            SQL.AppendLine("@Seal16, @Seal17, @Seal18, @Seal19, @Seal20, ");
            SQL.AppendLine("@Seal21, @Seal22, @Seal23, @Seal24, @Seal25, ");
            SQL.AppendLine("@Freight1, @Freight2, @Freight3, @Freight4, @Freight5, ");
            SQL.AppendLine("@Freight6, @Freight7, @Freight8, @Freight9, @Freight10, ");
            SQL.AppendLine("@Freight11, @Freight12, @Freight13, @Freight14, @Freight15, ");
            SQL.AppendLine("@Freight16, @Freight17, @Freight18, @Freight19, @Freight20, ");
            SQL.AppendLine("@Freight21, @Freight22, @Freight23, @Freight24, @Freight25, ");
            SQL.AppendLine("@UserCode, CurrentDateTime()) ");
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("    Update ");
            if (!mIsNotCopySalesLocalDocNo)
                SQL.AppendLine("    LocalDocNo=@LocalDocNo, ");
            SQL.AppendLine("    Account=@Account, InvoiceNo=@InvoiceNo, SalesContractNo=@SalesContractNo, LCNo=@LCNo, LCDt=@LCDt, Issued1=@Issued1, ");
            SQL.AppendLine("    Issued2=@Issued2, Issued3=@Issued3, Issued4=@Issued4, ToOrder1=@ToOrder1, ToOrder2=@ToOrder2, ToOrder3=@ToOrder3, ");
            SQL.AppendLine("    Cnt1=@Cnt1, Cnt2=@Cnt2, Cnt3=@Cnt3, Cnt4=@Cnt4, Cnt5=@Cnt5, ");
            SQL.AppendLine("    Cnt6=@Cnt6, Cnt7=@Cnt7, Cnt8=@Cnt8, Cnt9=@Cnt9, Cnt10=@Cnt10, ");
            SQL.AppendLine("    Cnt11=@Cnt11, Cnt12=@Cnt12, Cnt13=@Cnt13, Cnt14=@Cnt14, Cnt15=@Cnt15, ");
            SQL.AppendLine("    Cnt16=@Cnt16, Cnt17=@Cnt17, Cnt18=@Cnt18, Cnt19=@Cnt19, Cnt20=@Cnt20, ");
            SQL.AppendLine("    Cnt21=@Cnt21, Cnt22=@Cnt22, Cnt23=@Cnt23, Cnt24=@Cnt24, Cnt25=@Cnt25, ");
            SQL.AppendLine("    Seal1=@Seal1, Seal2=@Seal2, Seal3=@Seal3, Seal4=@Seal4, Seal5=@Seal5, ");
            SQL.AppendLine("    Seal6=@Seal6, Seal7=@Seal7, Seal8=@Seal8, Seal9=@Seal9, Seal10=@Seal10, ");
            SQL.AppendLine("    Seal11=@Seal11, Seal12=@Seal12, Seal13=@Seal13, Seal14=@Seal14, Seal15=@Seal15, ");
            SQL.AppendLine("    Seal16=@Seal16, Seal17=@Seal17, Seal18=@Seal18, Seal19=@Seal19, Seal20=@Seal20, ");
            SQL.AppendLine("    Seal21=@Seal21, Seal22=@Seal22, Seal23=@Seal23, Seal24=@Seal24, Seal25=@Seal25, ");
            SQL.AppendLine("    Freight1=@Freight1, Freight2=@Freight2, Freight3=@Freight3, Freight4=@Freight4, Freight5=@Freight5, ");
            SQL.AppendLine("    Freight6=@Freight6, Freight7=@Freight7, Freight8=@Freight8, Freight9=@Freight9, Freight10=@Freight10, ");
            SQL.AppendLine("    Freight11=@Freight11, Freight12=@Freight12, Freight13=@Freight13, Freight14=@Freight14, Freight15=@Freight15, ");
            SQL.AppendLine("    Freight16=@Freight16, Freight17=@Freight17, Freight18=@Freight18, Freight19=@Freight19, Freight20=@Freight20, ");
            SQL.AppendLine("    Freight21=@Freight21, Freight22=@Freight22, Freight23=@Freight23, Freight24=@Freight24, Freight25=@Freight25, ");
            SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

            //SQL.AppendLine("Delete From TblPLDtl Where DocNo = @PLDocNo ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@LocalDocNo", TxtLocalDocNo.Text);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@SIDocNo", TxtSIDocNo.Text);
            Sm.CmParam<String>(ref cm, "@PLDocNo", TxtPLDocNo.Text);
            Sm.CmParam<String>(ref cm, "@Account", MeeAccount.Text);
            Sm.CmParam<String>(ref cm, "@InvoiceNo", DocNo);
            Sm.CmParam<String>(ref cm, "@SalesContractNo", TxtSalesContract.Text);
            Sm.CmParam<String>(ref cm, "@LCNo", TxtLcNo.Text);
            Sm.CmParamDt(ref cm, "@LCDt", Sm.GetDte(LCDocDt));
            Sm.CmParam<String>(ref cm, "@Issued1", MeeIssued.Text);
            Sm.CmParam<String>(ref cm, "@Issued2", MeeIssued2.Text);
            Sm.CmParam<String>(ref cm, "@Issued3", MeeIssued3.Text);
            Sm.CmParam<String>(ref cm, "@Issued4", MeeIssued4.Text);
            Sm.CmParam<String>(ref cm, "@ToOrder1", MeeOrder.Text);
            Sm.CmParam<String>(ref cm, "@ToOrder2", MeeOrder2.Text);
            Sm.CmParam<String>(ref cm, "@ToOrder3", MeeOrder3.Text);
            Sm.CmParam<String>(ref cm, "@ToOrder4", MeeOrder4.Text);
            Sm.CmParam<String>(ref cm, "@Cnt1", TxtCnt1.Text);
            Sm.CmParam<String>(ref cm, "@Cnt2", TxtCnt2.Text);
            Sm.CmParam<String>(ref cm, "@Cnt3", TxtCnt3.Text);
            Sm.CmParam<String>(ref cm, "@Cnt4", TxtCnt4.Text);
            Sm.CmParam<String>(ref cm, "@Cnt5", TxtCnt5.Text);
            Sm.CmParam<String>(ref cm, "@Cnt6", TxtCnt6.Text);
            Sm.CmParam<String>(ref cm, "@Cnt7", TxtCnt7.Text);
            Sm.CmParam<String>(ref cm, "@Cnt8", TxtCnt8.Text);
            Sm.CmParam<String>(ref cm, "@Cnt9", TxtCnt9.Text);
            Sm.CmParam<String>(ref cm, "@Cnt10", TxtCnt10.Text);
            Sm.CmParam<String>(ref cm, "@Cnt11", TxtCnt11.Text);
            Sm.CmParam<String>(ref cm, "@Cnt12", TxtCnt12.Text);
            Sm.CmParam<String>(ref cm, "@Cnt13", TxtCnt13.Text);
            Sm.CmParam<String>(ref cm, "@Cnt14", TxtCnt14.Text);
            Sm.CmParam<String>(ref cm, "@Cnt15", TxtCnt15.Text);
            Sm.CmParam<String>(ref cm, "@Cnt16", TxtCnt16.Text);
            Sm.CmParam<String>(ref cm, "@Cnt17", TxtCnt17.Text);
            Sm.CmParam<String>(ref cm, "@Cnt18", TxtCnt18.Text);
            Sm.CmParam<String>(ref cm, "@Cnt19", TxtCnt19.Text);
            Sm.CmParam<String>(ref cm, "@Cnt20", TxtCnt20.Text);
            Sm.CmParam<String>(ref cm, "@Cnt21", TxtCnt21.Text);
            Sm.CmParam<String>(ref cm, "@Cnt22", TxtCnt22.Text);
            Sm.CmParam<String>(ref cm, "@Cnt23", TxtCnt23.Text);
            Sm.CmParam<String>(ref cm, "@Cnt24", TxtCnt24.Text);
            Sm.CmParam<String>(ref cm, "@Cnt25", TxtCnt25.Text);
            Sm.CmParam<String>(ref cm, "@Seal1", TxtSeal1.Text);
            Sm.CmParam<String>(ref cm, "@Seal2", TxtSeal2.Text);
            Sm.CmParam<String>(ref cm, "@Seal3", TxtSeal3.Text);
            Sm.CmParam<String>(ref cm, "@Seal4", TxtSeal4.Text);
            Sm.CmParam<String>(ref cm, "@Seal5", TxtSeal5.Text);
            Sm.CmParam<String>(ref cm, "@Seal6", TxtSeal6.Text);
            Sm.CmParam<String>(ref cm, "@Seal7", TxtSeal7.Text);
            Sm.CmParam<String>(ref cm, "@Seal8", TxtSeal8.Text);
            Sm.CmParam<String>(ref cm, "@Seal9", TxtSeal9.Text);
            Sm.CmParam<String>(ref cm, "@Seal10", TxtSeal10.Text);
            Sm.CmParam<String>(ref cm, "@Seal11", TxtSeal11.Text);
            Sm.CmParam<String>(ref cm, "@Seal12", TxtSeal12.Text);
            Sm.CmParam<String>(ref cm, "@Seal13", TxtSeal13.Text);
            Sm.CmParam<String>(ref cm, "@Seal14", TxtSeal14.Text);
            Sm.CmParam<String>(ref cm, "@Seal15", TxtSeal15.Text);
            Sm.CmParam<String>(ref cm, "@Seal16", TxtSeal16.Text);
            Sm.CmParam<String>(ref cm, "@Seal17", TxtSeal17.Text);
            Sm.CmParam<String>(ref cm, "@Seal18", TxtSeal18.Text);
            Sm.CmParam<String>(ref cm, "@Seal19", TxtSeal19.Text);
            Sm.CmParam<String>(ref cm, "@Seal20", TxtSeal20.Text);
            Sm.CmParam<String>(ref cm, "@Seal21", TxtSeal21.Text);
            Sm.CmParam<String>(ref cm, "@Seal22", TxtSeal22.Text);
            Sm.CmParam<String>(ref cm, "@Seal23", TxtSeal23.Text);
            Sm.CmParam<String>(ref cm, "@Seal24", TxtSeal24.Text);
            Sm.CmParam<String>(ref cm, "@Seal25", TxtSeal25.Text);
            Sm.CmParam<Decimal>(ref cm, "@Freight1", Decimal.Parse(TxtFreight1.Text));
            Sm.CmParam<Decimal>(ref cm, "@Freight2", Decimal.Parse(TxtFreight2.Text));
            Sm.CmParam<Decimal>(ref cm, "@Freight3", Decimal.Parse(TxtFreight3.Text));
            Sm.CmParam<Decimal>(ref cm, "@Freight4", Decimal.Parse(TxtFreight4.Text));
            Sm.CmParam<Decimal>(ref cm, "@Freight5", Decimal.Parse(TxtFreight5.Text));
            Sm.CmParam<Decimal>(ref cm, "@Freight6", Decimal.Parse(TxtFreight6.Text));
            Sm.CmParam<Decimal>(ref cm, "@Freight7", Decimal.Parse(TxtFreight7.Text));
            Sm.CmParam<Decimal>(ref cm, "@Freight8", Decimal.Parse(TxtFreight8.Text));
            Sm.CmParam<Decimal>(ref cm, "@Freight9", Decimal.Parse(TxtFreight9.Text));
            Sm.CmParam<Decimal>(ref cm, "@Freight10", Decimal.Parse(TxtFreight10.Text));
            Sm.CmParam<Decimal>(ref cm, "@Freight11", Decimal.Parse(TxtFreight11.Text));
            Sm.CmParam<Decimal>(ref cm, "@Freight12", Decimal.Parse(TxtFreight12.Text));
            Sm.CmParam<Decimal>(ref cm, "@Freight13", Decimal.Parse(TxtFreight13.Text));
            Sm.CmParam<Decimal>(ref cm, "@Freight14", Decimal.Parse(TxtFreight14.Text));
            Sm.CmParam<Decimal>(ref cm, "@Freight15", Decimal.Parse(TxtFreight15.Text));
            Sm.CmParam<Decimal>(ref cm, "@Freight16", Decimal.Parse(TxtFreight16.Text));
            Sm.CmParam<Decimal>(ref cm, "@Freight17", Decimal.Parse(TxtFreight17.Text));
            Sm.CmParam<Decimal>(ref cm, "@Freight18", Decimal.Parse(TxtFreight18.Text));
            Sm.CmParam<Decimal>(ref cm, "@Freight19", Decimal.Parse(TxtFreight19.Text));
            Sm.CmParam<Decimal>(ref cm, "@Freight20", Decimal.Parse(TxtFreight20.Text));
            Sm.CmParam<Decimal>(ref cm, "@Freight21", Decimal.Parse(TxtFreight21.Text));
            Sm.CmParam<Decimal>(ref cm, "@Freight22", Decimal.Parse(TxtFreight22.Text));
            Sm.CmParam<Decimal>(ref cm, "@Freight23", Decimal.Parse(TxtFreight23.Text));
            Sm.CmParam<Decimal>(ref cm, "@Freight24", Decimal.Parse(TxtFreight24.Text));
            Sm.CmParam<Decimal>(ref cm, "@Freight25", Decimal.Parse(TxtFreight25.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@Remark2", MeeRemark2.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                ShowSinv(DocNo);
                ShowPLDtl(TxtPLDocNo.Text, "1", ref Grd11, TxtTotal1, TxtFreight1);
                ShowPLDtl(TxtPLDocNo.Text, "2", ref Grd12, TxtTotal2, TxtFreight2);
                ShowPLDtl(TxtPLDocNo.Text, "3", ref Grd13, TxtTotal3, TxtFreight3);
                ShowPLDtl(TxtPLDocNo.Text, "4", ref Grd14, TxtTotal4, TxtFreight4);
                ShowPLDtl(TxtPLDocNo.Text, "5", ref Grd15, TxtTotal5, TxtFreight5);
                ShowPLDtl(TxtPLDocNo.Text, "6", ref Grd16, TxtTotal6, TxtFreight6);
                ShowPLDtl(TxtPLDocNo.Text, "7", ref Grd17, TxtTotal7, TxtFreight7);
                ShowPLDtl(TxtPLDocNo.Text, "8", ref Grd18, TxtTotal8, TxtFreight8);
                ShowPLDtl(TxtPLDocNo.Text, "9", ref Grd19, TxtTotal9, TxtFreight9);
                ShowPLDtl(TxtPLDocNo.Text, "10", ref Grd20, TxtTotal10, TxtFreight10);
                ShowPLDtl(TxtPLDocNo.Text, "11", ref Grd21, TxtTotal11, TxtFreight11);
                ShowPLDtl(TxtPLDocNo.Text, "12", ref Grd22, TxtTotal12, TxtFreight12);
                ShowPLDtl(TxtPLDocNo.Text, "13", ref Grd23, TxtTotal13, TxtFreight13);
                ShowPLDtl(TxtPLDocNo.Text, "14", ref Grd24, TxtTotal14, TxtFreight14);
                ShowPLDtl(TxtPLDocNo.Text, "15", ref Grd25, TxtTotal15, TxtFreight15);
                ShowPLDtl(TxtPLDocNo.Text, "16", ref Grd26, TxtTotal16, TxtFreight16);
                ShowPLDtl(TxtPLDocNo.Text, "17", ref Grd27, TxtTotal17, TxtFreight17);
                ShowPLDtl(TxtPLDocNo.Text, "18", ref Grd28, TxtTotal18, TxtFreight18);
                ShowPLDtl(TxtPLDocNo.Text, "19", ref Grd29, TxtTotal19, TxtFreight19);
                ShowPLDtl(TxtPLDocNo.Text, "20", ref Grd30, TxtTotal20, TxtFreight20);
                ShowPLDtl(TxtPLDocNo.Text, "21", ref Grd31, TxtTotal21, TxtFreight21);
                ShowPLDtl(TxtPLDocNo.Text, "22", ref Grd32, TxtTotal22, TxtFreight22);
                ShowPLDtl(TxtPLDocNo.Text, "23", ref Grd33, TxtTotal23, TxtFreight23);
                ShowPLDtl(TxtPLDocNo.Text, "24", ref Grd34, TxtTotal24, TxtFreight24);
                ShowPLDtl(TxtPLDocNo.Text, "25", ref Grd35, TxtTotal25, TxtFreight25);
                ShowSOData3();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        public void ShowDataPL(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ShowPLHdr2(DocNo);
                ShowPLDtl(TxtPLDocNo.Text, "1", ref Grd11, TxtTotal1, TxtFreight1);
                ShowPLDtl(TxtPLDocNo.Text, "2", ref Grd12, TxtTotal2, TxtFreight2);
                ShowPLDtl(TxtPLDocNo.Text, "3", ref Grd13, TxtTotal3, TxtFreight3);
                ShowPLDtl(TxtPLDocNo.Text, "4", ref Grd14, TxtTotal4, TxtFreight4);
                ShowPLDtl(TxtPLDocNo.Text, "5", ref Grd15, TxtTotal5, TxtFreight5);
                ShowPLDtl(TxtPLDocNo.Text, "6", ref Grd16, TxtTotal6, TxtFreight6);
                ShowPLDtl(TxtPLDocNo.Text, "7", ref Grd17, TxtTotal7, TxtFreight7);
                ShowPLDtl(TxtPLDocNo.Text, "8", ref Grd18, TxtTotal8, TxtFreight8);
                ShowPLDtl(TxtPLDocNo.Text, "9", ref Grd19, TxtTotal9, TxtFreight9);
                ShowPLDtl(TxtPLDocNo.Text, "10", ref Grd20, TxtTotal10, TxtFreight10);
                ShowPLDtl(TxtPLDocNo.Text, "11", ref Grd21, TxtTotal11, TxtFreight11);
                ShowPLDtl(TxtPLDocNo.Text, "12", ref Grd22, TxtTotal12, TxtFreight12);
                ShowPLDtl(TxtPLDocNo.Text, "13", ref Grd23, TxtTotal13, TxtFreight13);
                ShowPLDtl(TxtPLDocNo.Text, "14", ref Grd24, TxtTotal14, TxtFreight14);
                ShowPLDtl(TxtPLDocNo.Text, "15", ref Grd25, TxtTotal15, TxtFreight15);
                ShowPLDtl(TxtPLDocNo.Text, "16", ref Grd26, TxtTotal16, TxtFreight16);
                ShowPLDtl(TxtPLDocNo.Text, "17", ref Grd27, TxtTotal17, TxtFreight17);
                ShowPLDtl(TxtPLDocNo.Text, "18", ref Grd28, TxtTotal18, TxtFreight18);
                ShowPLDtl(TxtPLDocNo.Text, "19", ref Grd29, TxtTotal19, TxtFreight19);
                ShowPLDtl(TxtPLDocNo.Text, "20", ref Grd30, TxtTotal20, TxtFreight20);
                ShowPLDtl(TxtPLDocNo.Text, "21", ref Grd31, TxtTotal21, TxtFreight21);
                ShowPLDtl(TxtPLDocNo.Text, "22", ref Grd32, TxtTotal22, TxtFreight22);
                ShowPLDtl(TxtPLDocNo.Text, "23", ref Grd33, TxtTotal23, TxtFreight23);
                ShowPLDtl(TxtPLDocNo.Text, "24", ref Grd34, TxtTotal24, TxtFreight24);
                ShowPLDtl(TxtPLDocNo.Text, "25", ref Grd35, TxtTotal25, TxtFreight25);
                ShowSOData3();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.Insert);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowSinv(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select X.DocNo, X.LocalDocNo, X.DocDt, C.CtCode, X.PLDOcNo, B.SpCtNotifyParty, A.Account, A.InvoiceNo, A.SalesContractNo, A.LCNo, " +
                    "A.LcDt, A.Issued1, A.Issued2, A.Issued3, A.Issued4, "+
                    "A.ToOrder1, A.ToOrder2, A.ToOrder3, A.ToOrder4, "+
                    "A.Cnt1, A.Cnt2, A.Cnt3, A.Cnt4, A.Cnt5, " +
                    "A.Cnt6, A.Cnt7, A.Cnt8, A.Cnt9, A.Cnt10, " +
                    "A.Cnt11, A.Cnt12, A.Cnt13, A.Cnt14, A.Cnt15, " +
                    "A.Cnt16, A.Cnt17, A.Cnt18, A.Cnt19, A.Cnt20, " +
                    "A.Cnt21, A.Cnt22, A.Cnt23, A.Cnt24, A.Cnt25, " +
                    "A.Seal1, A.Seal2, A.Seal3, A.Seal4, A.Seal5, " +
                    "A.Seal6, A.Seal7, A.Seal8, A.Seal9, A.Seal10, " +
                    "A.Seal11, A.Seal12, A.Seal13, A.Seal14, A.Seal15, " +
                    "A.Seal16, A.Seal17, A.Seal18, A.Seal19, A.Seal20, " +
                    "A.Seal21, A.Seal22, A.Seal23, A.Seal24, A.Seal25, " +
                    "A.Size1, A.Size2, A.Size3, A.Size4, A.Size5, " +
                    "A.Size6, A.Size7, A.Size8, A.Size9, A.Size10, " +
                    "A.Size11, A.Size12, A.Size13, A.Size14, A.Size15, " +
                    "A.Size16, A.Size17, A.Size18, A.Size19, A.Size20, " +
                    "A.Size21, A.Size22, A.Size23, A.Size24, A.Size25, " +
                    "A.Freight1, A.Freight2, A.Freight3, A.Freight4, A.Freight5, "+
                    "A.Freight6, A.Freight7, A.Freight8, A.Freight9, A.Freight10, " +
                    "A.Freight11, A.Freight12, A.Freight13, A.Freight14, A.Freight15, " +
                    "A.Freight16, A.Freight17, A.Freight18, A.Freight19, A.Freight20, " +
                    "A.Freight21, A.Freight22, A.Freight23, A.Freight24, A.Freight25, " +
                    "A.SIDocNo, X.Remark, X.Remark2  " +
                    "From TblSinv X "+
                    "Inner Join TblPLHdr A On X.PlDocNo = A.DocNo "+
                    "Inner Join TblSIhdr B On A.SIDocNo = B.DocNo "+
                    "Inner Join TblSP C On B.SpDocNo = C.DocNo " +
                    "Where X.DocNo=@DocNo",
                    new string[] 
                    { 
                        //0
                        "DocNo", 
                        //1-5
                        "LocalDocNo", "DocDt", "CtCode", "PLDOcNo", "SpCtNotifyParty",    
                        //6-10
                        "Account","InvoiceNo", "SalesContractNo", "LCNo", "LcDt",    
                        //11-15
                        "Issued1","Issued2", "Issued3", "Issued4", "ToOrder1",    
                         //16-20
                        "ToOrder2", "ToOrder3","ToOrder4", "Cnt1", "Cnt2",     
                        //21-25
                        "Cnt3","Cnt4","Cnt5","Cnt6", "Cnt7",
                        //26-30
                        "Cnt8","Cnt9","Cnt10","Cnt11", "Cnt12",
                        //31-35
                        "Cnt13","Cnt14","Cnt15","Cnt16", "Cnt17",
                        //35-40
                        "Cnt18","Cnt19","Cnt20","Cnt21", "Cnt22", 
                        //41-45
                        "Cnt23","Cnt24","Cnt25","Seal1", "Seal2",
                        //46-50
                        "Seal3", "Seal4","Seal5","Seal6", "Seal7",
                        //51-55
                        "Seal8", "Seal9","Seal10","Seal11", "Seal12",
                        //56-60
                        "Seal13", "Seal14", "Seal15","Seal16", "Seal17",
                        //61-65
                        "Seal18", "Seal19","Seal20","Seal21", "Seal22",
                        //66-70
                        "Seal23", "Seal24", "Seal25", "Freight1", "Freight2",  
                        //71-75
                        "Freight3","Freight4", "Freight5", "Freight6", "Freight7", 
                        //76-80
                        "Freight8", "Freight9", "Freight10", "Freight11", "Freight12",   
                        //81-85
                        "Freight13","Freight14", "Freight15", "Freight16", "Freight17", 
                        //86-90
                        "Freight18", "Freight19", "Freight20", "Freight21", "Freight22",   
                        //91-95
                        "Freight23","Freight24", "Freight25", "SIDocNo", "Remark", 
                        //96
                        "Remark2"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        TxtLocalDocNo.EditValue = Sm.DrStr(dr, c[1]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[2]));
                        Sm.SetLue(LueCtCode, Sm.DrStr(dr, c[3]));
                        TxtPLDocNo.EditValue = Sm.DrStr(dr, c[4]);
                        Sm.SetLue(LueNotify, Sm.DrStr(dr, c[5]));
                        MeeAccount.EditValue = Sm.DrStr(dr, c[6]);
                        TxtSalesContract.EditValue = Sm.DrStr(dr, c[8]);
                        TxtLcNo.EditValue = Sm.DrStr(dr, c[9]);
                        Sm.SetDte(LCDocDt, Sm.DrStr(dr, c[10]));
                        MeeIssued.EditValue = Sm.DrStr(dr, c[11]);
                        MeeIssued2.EditValue = Sm.DrStr(dr, c[12]);
                        MeeIssued3.EditValue = Sm.DrStr(dr, c[13]);
                        MeeIssued4.EditValue = Sm.DrStr(dr, c[14]);
                        MeeOrder.EditValue = Sm.DrStr(dr, c[15]);
                        MeeOrder2.EditValue = Sm.DrStr(dr, c[16]);
                        MeeOrder3.EditValue = Sm.DrStr(dr, c[17]);
                        MeeOrder4.EditValue = Sm.DrStr(dr, c[18]);
                        TxtCnt1.Text = Sm.DrStr(dr, c[19]);
                        TxtCnt2.Text = Sm.DrStr(dr, c[20]);
                        TxtCnt3.Text = Sm.DrStr(dr, c[21]);
                        TxtCnt4.Text = Sm.DrStr(dr, c[22]);
                        TxtCnt5.Text = Sm.DrStr(dr, c[23]);
                        TxtCnt6.Text = Sm.DrStr(dr, c[24]);
                        TxtCnt7.Text = Sm.DrStr(dr, c[25]);
                        TxtCnt8.Text = Sm.DrStr(dr, c[26]);
                        TxtCnt9.Text = Sm.DrStr(dr, c[27]);
                        TxtCnt10.Text = Sm.DrStr(dr, c[28]);
                        TxtCnt11.Text = Sm.DrStr(dr, c[29]);
                        TxtCnt12.Text = Sm.DrStr(dr, c[30]);
                        TxtCnt13.Text = Sm.DrStr(dr, c[31]);
                        TxtCnt14.Text = Sm.DrStr(dr, c[32]);
                        TxtCnt15.Text = Sm.DrStr(dr, c[33]);
                        TxtCnt16.Text = Sm.DrStr(dr, c[34]);
                        TxtCnt17.Text = Sm.DrStr(dr, c[35]);
                        TxtCnt18.Text = Sm.DrStr(dr, c[36]);
                        TxtCnt19.Text = Sm.DrStr(dr, c[37]);
                        TxtCnt20.Text = Sm.DrStr(dr, c[38]);
                        TxtCnt21.Text = Sm.DrStr(dr, c[39]);
                        TxtCnt22.Text = Sm.DrStr(dr, c[40]);
                        TxtCnt23.Text = Sm.DrStr(dr, c[41]);
                        TxtCnt24.Text = Sm.DrStr(dr, c[42]);
                        TxtCnt25.Text = Sm.DrStr(dr, c[43]);
                        TxtSeal1.Text = Sm.DrStr(dr, c[44]);
                        TxtSeal2.Text = Sm.DrStr(dr, c[45]);
                        TxtSeal3.Text = Sm.DrStr(dr, c[46]);
                        TxtSeal4.Text = Sm.DrStr(dr, c[47]);
                        TxtSeal5.Text = Sm.DrStr(dr, c[48]);
                        TxtSeal6.Text = Sm.DrStr(dr, c[49]);
                        TxtSeal7.Text = Sm.DrStr(dr, c[50]);
                        TxtSeal8.Text = Sm.DrStr(dr, c[51]);
                        TxtSeal9.Text = Sm.DrStr(dr, c[52]);
                        TxtSeal10.Text = Sm.DrStr(dr, c[53]);
                        TxtSeal11.Text = Sm.DrStr(dr, c[54]);
                        TxtSeal12.Text = Sm.DrStr(dr, c[55]);
                        TxtSeal13.Text = Sm.DrStr(dr, c[56]);
                        TxtSeal14.Text = Sm.DrStr(dr, c[57]);
                        TxtSeal15.Text = Sm.DrStr(dr, c[58]);
                        TxtSeal16.Text = Sm.DrStr(dr, c[59]);
                        TxtSeal17.Text = Sm.DrStr(dr, c[60]);
                        TxtSeal18.Text = Sm.DrStr(dr, c[61]);
                        TxtSeal19.Text = Sm.DrStr(dr, c[62]);
                        TxtSeal20.Text = Sm.DrStr(dr, c[63]);
                        TxtSeal21.Text = Sm.DrStr(dr, c[64]);
                        TxtSeal22.Text = Sm.DrStr(dr, c[65]);
                        TxtSeal23.Text = Sm.DrStr(dr, c[66]);
                        TxtSeal24.Text = Sm.DrStr(dr, c[67]);
                        TxtSeal25.Text = Sm.DrStr(dr, c[68]);
                        TxtFreight1.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[69]), 0);
                        TxtFreight2.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[70]), 0);
                        TxtFreight3.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[71]), 0);
                        TxtFreight4.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[72]), 0);
                        TxtFreight5.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[73]), 0);
                        TxtFreight6.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[74]), 0);
                        TxtFreight7.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[75]), 0);
                        TxtFreight8.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[76]), 0);
                        TxtFreight9.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[77]), 0);
                        TxtFreight10.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[78]), 0);
                        TxtFreight11.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[79]), 0);
                        TxtFreight12.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[80]), 0);
                        TxtFreight13.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[81]), 0);
                        TxtFreight14.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[82]), 0);
                        TxtFreight15.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[83]), 0);
                        TxtFreight16.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[84]), 0);
                        TxtFreight17.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[85]), 0);
                        TxtFreight18.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[86]), 0);
                        TxtFreight19.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[87]), 0);
                        TxtFreight20.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[88]), 0);
                        TxtFreight21.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[89]), 0);
                        TxtFreight22.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[90]), 0);
                        TxtFreight23.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[91]), 0);
                        TxtFreight24.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[92]), 0);
                        TxtFreight25.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[93]), 0);
                        TxtSIDocNo.Text = Sm.DrStr(dr, c[94]);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[95]);
                        MeeRemark2.EditValue = Sm.DrStr(dr, c[96]);
                    }, true
                );
        }


        private void ShowPLHdr2(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select A.DocNo, A.DocDt, A.SIDocNo, C.CtCode, A.SiDOcNo, B.SpCtNotifyParty, A.Account, A.InvoiceNo, A.SalesContractNo, A.LCNo, " +
                    "A.LcDt, A.Issued1, A.Issued2, A.Issued3, A.Issued4, " +
                    "A.ToOrder1, A.ToOrder2, A.ToOrder3, A.ToOrder4, " +
                    "A.Cnt1, A.Cnt2, A.Cnt3, A.Cnt4, A.Cnt5, "+
                    "A.Cnt6, A.Cnt7, A.Cnt8, A.Cnt9, A.Cnt10, " +
                    "A.Cnt11, A.Cnt12, A.Cnt13, A.Cnt14, A.Cnt15, "+
                    "A.Cnt16, A.Cnt17, A.Cnt18, A.Cnt19, A.Cnt20, " +
                    "A.Cnt21, A.Cnt22, A.Cnt23, A.Cnt24, A.Cnt25, " +
                    "A.Seal1, A.Seal2, A.Seal3, A.Seal4, A.Seal5, " +
                    "A.Seal6, A.Seal7, A.Seal8, A.Seal9, A.Seal10, " +
                    "A.Seal11, A.Seal12, A.Seal13, A.Seal14, A.Seal15, " +
                    "A.Seal16, A.Seal17, A.Seal18, A.Seal19, A.Seal20, " +
                    "A.Seal21, A.Seal22, A.Seal23, A.Seal24, A.Seal25, "+
                    "A.Remark, A.Remark2 " +
                    "From TblPLHdr A " +
                    "Inner Join TblSIhdr B On A.SIDocNo = B.DocNo " +
                    "Inner Join TblSP C On B.SpDocNo = C.DocNo " +
                    "Where A.DocNo=@DocNo",
                    new string[] 
                    { 
                        //0
                        "DocNo", 
                        //1-5
                        "DocDt", "CtCode", "SIDOcNo", "SpCtNotifyParty", "Account",   
                        //6-10
                        "InvoiceNo", "SalesContractNo", "LCNo", "LcDt", "Issued1",  
                        //11-15
                        "Issued2", "Issued3", "Issued4", "ToOrder1", "ToOrder2",   
                         //16-20
                        "ToOrder3","ToOrder4", "Cnt1", "Cnt2", "Cnt3",   
                        //21-25
                        "Cnt4","Cnt5", "Cnt6", "Cnt7", "Cnt8",   
                        //26-30
                        "Cnt9","Cnt10", "Cnt11", "Cnt12", "Cnt13",  
                        //31-35
                        "Cnt14","Cnt15", "Cnt16", "Cnt17", "Cnt18", 
                        //36-40
                        "Cnt19","Cnt20", "Cnt21", "Cnt22", "Cnt23",  
                        //41-45
                        "Cnt24","Cnt25", "Seal1", "Seal2", "Seal3", 
                        //46-50
                        "Seal4","Seal5", "Seal6", "Seal7", "Seal8",  
                        //51-55
                        "Seal9","Seal10", "Seal11", "Seal12", "Seal13", 
                        //56-60
                        "Seal14", "Seal15", "Seal16", "Seal17", "Seal18",  
                        //61-65
                        "Seal19","Seal20", "Seal21", "Seal22", "Seal23", 
                        //66-69
                        "Seal24", "Seal25", "Remark", "Remark2"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtSIDocNo.EditValue = Sm.DrStr(dr, c[3]);
                        Sm.SetLue(LueNotify, Sm.DrStr(dr, c[4]));
                        MeeAccount.EditValue = Sm.DrStr(dr, c[5]);
                        TxtSalesContract.EditValue = Sm.DrStr(dr, c[7]);
                        TxtLcNo.EditValue = Sm.DrStr(dr, c[8]);
                        Sm.SetDte(LCDocDt, Sm.DrStr(dr, c[9]));
                        MeeIssued.EditValue = Sm.DrStr(dr, c[10]);
                        MeeIssued2.EditValue = Sm.DrStr(dr, c[11]);
                        MeeIssued3.EditValue = Sm.DrStr(dr, c[12]);
                        MeeIssued4.EditValue = Sm.DrStr(dr, c[13]);
                        MeeOrder.EditValue = Sm.DrStr(dr, c[14]);
                        MeeOrder2.EditValue = Sm.DrStr(dr, c[15]);
                        MeeOrder3.EditValue = Sm.DrStr(dr, c[16]);
                        MeeOrder4.EditValue = Sm.DrStr(dr, c[17]);
                        TxtCnt1.EditValue = Sm.DrStr(dr, c[18]);
                        TxtCnt2.EditValue = Sm.DrStr(dr, c[19]);
                        TxtCnt3.EditValue = Sm.DrStr(dr, c[20]);
                        TxtCnt4.EditValue = Sm.DrStr(dr, c[21]);
                        TxtCnt5.EditValue = Sm.DrStr(dr, c[22]);
                        TxtCnt6.EditValue = Sm.DrStr(dr, c[23]);
                        TxtCnt7.EditValue = Sm.DrStr(dr, c[24]);
                        TxtCnt8.EditValue = Sm.DrStr(dr, c[25]);
                        TxtCnt9.EditValue = Sm.DrStr(dr, c[26]);
                        TxtCnt10.EditValue = Sm.DrStr(dr, c[27]);
                        TxtCnt11.EditValue = Sm.DrStr(dr, c[28]);
                        TxtCnt12.EditValue = Sm.DrStr(dr, c[29]);
                        TxtCnt13.EditValue = Sm.DrStr(dr, c[30]);
                        TxtCnt14.EditValue = Sm.DrStr(dr, c[31]);
                        TxtCnt15.EditValue = Sm.DrStr(dr, c[32]);
                        TxtCnt16.EditValue = Sm.DrStr(dr, c[33]);
                        TxtCnt17.EditValue = Sm.DrStr(dr, c[34]);
                        TxtCnt18.EditValue = Sm.DrStr(dr, c[35]);
                        TxtCnt19.EditValue = Sm.DrStr(dr, c[36]);
                        TxtCnt20.EditValue = Sm.DrStr(dr, c[37]);
                        TxtCnt21.EditValue = Sm.DrStr(dr, c[38]);
                        TxtCnt22.EditValue = Sm.DrStr(dr, c[39]);
                        TxtCnt23.EditValue = Sm.DrStr(dr, c[40]);
                        TxtCnt24.EditValue = Sm.DrStr(dr, c[41]);
                        TxtCnt25.EditValue = Sm.DrStr(dr, c[42]);
                        TxtSeal1.EditValue = Sm.DrStr(dr, c[43]);
                        TxtSeal2.EditValue = Sm.DrStr(dr, c[44]);
                        TxtSeal3.EditValue = Sm.DrStr(dr, c[45]);
                        TxtSeal4.EditValue = Sm.DrStr(dr, c[46]);
                        TxtSeal5.EditValue = Sm.DrStr(dr, c[47]);
                        TxtSeal6.EditValue = Sm.DrStr(dr, c[48]);
                        TxtSeal7.EditValue = Sm.DrStr(dr, c[49]);
                        TxtSeal8.EditValue = Sm.DrStr(dr, c[50]);
                        TxtSeal9.EditValue = Sm.DrStr(dr, c[51]);
                        TxtSeal10.EditValue = Sm.DrStr(dr, c[52]);
                        TxtSeal11.EditValue = Sm.DrStr(dr, c[53]);
                        TxtSeal12.EditValue = Sm.DrStr(dr, c[54]);
                        TxtSeal13.EditValue = Sm.DrStr(dr, c[55]);
                        TxtSeal14.EditValue = Sm.DrStr(dr, c[56]);
                        TxtSeal15.EditValue = Sm.DrStr(dr, c[57]);
                        TxtSeal16.EditValue = Sm.DrStr(dr, c[58]);
                        TxtSeal17.EditValue = Sm.DrStr(dr, c[59]);
                        TxtSeal18.EditValue = Sm.DrStr(dr, c[60]);
                        TxtSeal19.EditValue = Sm.DrStr(dr, c[61]);
                        TxtSeal20.EditValue = Sm.DrStr(dr, c[62]);
                        TxtSeal21.EditValue = Sm.DrStr(dr, c[63]);
                        TxtSeal22.EditValue = Sm.DrStr(dr, c[64]);
                        TxtSeal23.EditValue = Sm.DrStr(dr, c[65]);
                        TxtSeal24.EditValue = Sm.DrStr(dr, c[66]);
                        TxtSeal25.EditValue = Sm.DrStr(dr, c[67]);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[68]);
                        MeeRemark2.EditValue = Sm.DrStr(dr, c[69]);
                    }, true
                );
        }

        private void ShowPLDtl(string DocNo, string SectionNo, ref iGrid ItemGrd, DXE.TextEdit TxtTotal, DXE.TextEdit TxtFreight)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@SectionNo", SectionNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.ItCode, B.ItCodeInternal, B.ItName, A.SODOcNo, A.SODno, ");
            SQL.AppendLine("C.PackagingUnitUomCode, A.QtyPackagingUnit, A.Qty, A.QtyInventory, G.PriceUomCode, B.InventoryUomCode, ");
            SQL.AppendLine("G.CurCode, F.UPrice, (F.UPrice * A.Qty) As Amount ");
            SQL.AppendLine("From TblPLDtl A  ");
            SQL.AppendLine("Inner Join TblItem B On A.ItCode = B.ItCode ");
            SQL.AppendLine("Inner Join TblSODtl C On A.SoDocNo = C.DocNo And A.SoDno = C.Dno ");
            SQL.AppendLine("Inner Join TblSOHdr D On C.DocNo = D.DocNo And A.SODocNo = D.DocNo  ");
            SQL.AppendLine("Inner Join TblCtQtDtl F On D.CtQtDocNo = F.DocNo And C.CtQtDNo = F.Dno ");
            SQL.AppendLine("Inner Join TblItemPriceHdr G On F.ItemPriceDocNo = G.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("And A.SectionNo=@SectionNo ");
            SQL.AppendLine("Order By A.DNo, A.ItCode ;");

            Sm.ShowDataInGrid(
                ref ItemGrd, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "ItCode", 

                    //1-5
                    "ItName", "ItCodeInternal", "SODOcNo", "SODNo", "PackagingUnitUomCode",  
 
                    //6-10
                    "QtyPackagingUnit", "Qty", "QtyInventory", "PriceUomCode", "InventoryUomCode",

                    //11-13
                    "CurCode", "UPrice", "Amount"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11,8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 10);

                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 12);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 13);
                    ComputeTotal(Grd, TxtTotal);
                    ComputeFOB(Grd, TxtFreight, TxtTotal, string.Concat("Size", SectionNo));
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref ItemGrd, ItemGrd.Rows.Count - 1, new int[] { 8, 9, 11 });
        }
        
        public void ShowSOData(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select H.ItCode, I.ItName, I.ItCodeInternal, D.DocNo, C.DNo,  ");
            SQL.AppendLine("C.PackagingUnitUomCode, 0 As QtyPackagingUnit,  ");
            SQL.AppendLine("ifnull(B.QtyPackagingUnit, 0) As OutstandingPackaging, ");
            SQL.AppendLine("0 As Qty, G.PriceUomCode, ifnull(B.Qty, 0) AS OutstandingQty, ");
            SQL.AppendLine("0 As QtyInventory, I.InventoryUomCode, C.DeliveryDt, B.Remark   ");
            SQL.AppendLine("From TblPLHdr X");
            SQL.AppendLine("Inner Join TblPlDtl Y On X.DocNo = Y.DocNo ");
            SQL.AppendLine("Inner Join TblSIHdr A On X.SIDocNo = A.DocNo  ");
            SQL.AppendLine("Inner join TblSIDtl B On A.DocNo = B.DocNo  ");
            SQL.AppendLine("Inner Join TblSODtl C On B.SODocNo = C.DocNo And B.SODNo = C.DNo  ");
            SQL.AppendLine("Inner Join TblSOHdr D On C.DocNo = D.DocNo And B.SODocNo = D.DocNo  ");
            SQL.AppendLine("Inner Join TblCtQtDtl F On D.CtQtDocNo = F.DocNo And C.CtQtDNo = F.Dno ");
            SQL.AppendLine("Inner Join TblItemPriceHdr G On F.ItemPriceDocNo = G.DocNo ");
            SQL.AppendLine("Inner Join TblitemPriceDtl H On F.ItemPriceDocNo = H.DocNo And F.ItemPriceDNo = H.Dno ");
            SQL.AppendLine("Inner Join TblItem I On H.ItCode = I.ItCode  ");
            //SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo And D.DocNo Not In (Select SODocNo From TblPlDtl) Order By B.DNo ");


            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", TxtPLDocNo.Text);

            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "ItCode",
 
                    //1-5
                    "ItName", "ItCodeInternal", "DocNo",  "DNo", "PackagingUnitUomCode", 
                    
                    //6-10
                    "OutstandingPackaging", "QtyPackagingUnit", "OutstandingQty", "Qty", "PriceUomCode", 
                    
                    //11-14
                    "QtyInventory", "InventoryUomCode", "DeliveryDt", "Remark"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Grd1.Cells[Row, 0].Value = Row + 1;
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 7);
                    Grd.Cells[Row, 11].Value = Sm.GetGrdDec(Grd, Row, 9) - Sm.GetGrdDec(Grd, Row, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 9);
                    Grd.Cells[Row, 14].Value = Sm.GetGrdDec(Grd, Row, 12) - Sm.GetGrdDec(Grd, Row, 13);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 11);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 12);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 18, 13);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 14);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 9, 10, 11, 12, 13, 14, 16 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        public void ShowSOData2(string DocNo, string Dno, iGrid Grdxx, int RowInd)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, B.Dno, F.ItCode, B.PackagingUnitUomCOde, ");
            SQL.AppendLine("G.PriceUomCode, I.InventoryUomCOde ");
            SQL.AppendLine("From TblSOHdr A  ");
            SQL.AppendLine("Inner Join TblSODtl B On A.DocNo = B.DocNo  ");
            SQL.AppendLine("Inner Join TblCtQtDtl F On A.CtQtDocNo = F.DocNo And B.CtQtDNo = F.Dno  ");
            SQL.AppendLine("Inner Join TblItemPriceHdr G On F.ItemPriceDocNo = G.DocNo  ");
            SQL.AppendLine("Inner Join TblitemPriceDtl H On F.ItemPriceDocNo = H.DocNo And F.ItemPriceDNo = H.Dno  ");
            SQL.AppendLine("Inner Join TblItem I On H.ItCode = I.ItCode   ");
            SQL.AppendLine("Where A.DocNo=@DocNo And B.Dno = @DNo  ");


            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Dno);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                cm.CommandTimeout = 100;
                using (var dr = cm.ExecuteReader())
                {
                    var c = Sm.GetOrdinal(dr,
                        new string[] 
                        { 
                            //0
                           "DocNo",  

                            //1-3
                           "DNo", "PackagingUnitUomCOde", "PriceUomCode", "InventoryUomCOde"
                        }
                        );
                    if (dr.HasRows)
                    {
                        Grdxx.ProcessTab = true;
                        Grdxx.BeginUpdate();
                        while (dr.Read())
                        {
                            for (int Row = 0; Row < Grdxx.Rows.Count - 1; Row++)
                            {
                                if (Sm.CompareStr(DocNo, Sm.DrStr(dr, 0)) &&
                                    Sm.CompareStr(Dno, Sm.DrStr(dr, 1)))
                                {
                                    Sm.SetGrdValue("S", Grdxx, dr, c, RowInd, 7, 2);
                                    Grdxx.Cells[RowInd, 8].Value = Grdxx.Cells[RowInd, 9].Value = Grdxx.Cells[RowInd, 11].Value = 0;
                                    Sm.SetGrdValue("S", Grdxx, dr, c, RowInd, 10, 3);
                                    Sm.SetGrdValue("S", Grdxx, dr, c, RowInd, 12, 4);
                                    break;
                                }
                            }
                        }
                        Grdxx.EndUpdate();
                    }
                    dr.Close();
                    ComputeUom(Grdxx);
                }
            }
        }

        public void ShowSOData3()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select H.ItCode, I.ItName, I.ItCodeInternal, D.DocNo, C.DNo,  ");
            SQL.AppendLine("C.PackagingUnitUomCode, B.QtyPackagingUnit As QtyPackagingUnit,  ");
            SQL.AppendLine("ifnull(B.QtyPackagingUnit, 0) As OutstandingPackaging, ");
            SQL.AppendLine("B.Qty As Qty, G.PriceUomCode, ifnull(B.Qty, 0) AS OutstandingQty, ");
            SQL.AppendLine("B.QtyInventory As QtyInventory, I.InventoryUomCode, G.CurCode, F.UPrice, (F.UPrice * B.Qty) As Amount, ");
            SQL.AppendLine("C.DeliveryDt, B.Remark   ");
            SQL.AppendLine("From TblPLHdr X");
            SQL.AppendLine("Inner Join TblSIHdr A On X.SIDocNo = A.DocNo ");
            SQL.AppendLine("Inner join TblSIDtl B On A.DocNo = B.DocNo  ");
            SQL.AppendLine("Inner Join TblSODtl C On B.SODocNo = C.DocNo And B.SODNo = C.DNo  ");
            SQL.AppendLine("Inner Join TblSOHdr D On C.DocNo = D.DocNo And B.SODocNo = D.DocNo  ");
            SQL.AppendLine("Inner Join TblCtQtDtl F On D.CtQtDocNo = F.DocNo And C.CtQtDNo = F.Dno ");
            SQL.AppendLine("Inner Join TblItemPriceHdr G On F.ItemPriceDocNo = G.DocNo ");
            SQL.AppendLine("Inner Join TblitemPriceDtl H On F.ItemPriceDocNo = H.DocNo And F.ItemPriceDNo = H.Dno ");
            SQL.AppendLine("Inner Join TblItem I On H.ItCode = I.ItCode  ");
            SQL.AppendLine("Where X.DocNo=@DocNo  Order By B.DNo ");


            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", TxtPLDocNo.Text);

            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "ItCode",
 
                    //1-5
                    "ItName", "ItCodeInternal", "DocNo",  "DNo", "PackagingUnitUomCode", 
                    
                    //6-10
                    "OutstandingPackaging", "QtyPackagingUnit", "OutstandingQty", "Qty", "PriceUomCode", 
                    
                    //11-15
                    "QtyInventory", "InventoryUomCode", "CurCode", "Uprice", "Amount", 
                    
                    //16-17
                    "DeliveryDt","Remark" 
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Grd1.Cells[Row, 0].Value = Row + 1;
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 7);
                    Grd.Cells[Row, 11].Value = Sm.GetGrdDec(Grd, Row, 9) - Sm.GetGrdDec(Grd, Row, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 9);
                    Grd.Cells[Row, 14].Value = Sm.GetGrdDec(Grd, Row, 12) - Sm.GetGrdDec(Grd, Row, 13);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 11);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 12);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 13);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 14);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 15);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 21, 16);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 17);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 9, 10, 11, 12, 13, 14, 16, 19, 20 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            THC = Decimal.Parse(Sm.GetParameter("THC"));
            mIsFormPrintOutSInv = Sm.GetParameter("FormPrintOutSInv");
            mIsCustomerItemNameMandatory = Sm.GetParameter("IsCustomerItemNameMandatory") == "Y";
            mIsLocalDocNoAccesible = Sm.GetParameter("IsLocalDocNoAccesible") == "Y";
            mIsNotCopySalesLocalDocNo = Sm.GetParameter("IsNotCopySalesLocalDocNo") == "Y";
            mLocalDocument = Sm.GetParameter("LocalDocument");
            mHostAddrForFTPClient = Sm.GetParameter("HostAddrForFTPClient");
            mSharedFolderForFTPClient = Sm.GetParameter("SharedFolderForFTPClient");
            mUsernameForFTPClient = Sm.GetParameter("UsernameForFTPClient");
            mPasswordForFTPClient = Sm.GetParameter("PasswordForFTPClient");
            mPortForFTPClient = Sm.GetParameter("PortForFTPClient");
            mFileSizeMaxUploadFTPClient = Sm.GetParameter("FileSizeMaxUploadFTPClient");
            mTHC20Feet = Sm.GetParameter("THC20Feet");
            mTHC40Feet = Sm.GetParameter("THC40Feet");
        }

        private void ComputeTotal(iGrid Grd, DXE.TextEdit TxtTotalKubik)
        {
            decimal Total = 0m;
            for (int Row = 0; Row <= Grd.Rows.Count - 1; Row++)
                if (Sm.GetGrdStr(Grd, Row, 0).Length != 0)
                {
                    Total += Sm.GetGrdDec(Grd, Row, 9);
                    TotalAmt += Sm.GetGrdDec(Grd, Row, 16);
                }
            TxtTotalKubik.Text = Sm.FormatNum(Total, 0);

        }

        private void ComputeFOB(iGrid Grd, DXE.TextEdit TxtFreight, DXE.TextEdit TxtTotalKubik, string Size)
        {
            decimal freight, TotalKubik, x1, x2, x3, TotalPrice = 0m;
            string mTHC = string.Empty;
            freight = Decimal.Parse(TxtFreight.Text);
            TotalKubik = Decimal.Parse(TxtTotalKubik.Text);
            mTHC = Sm.GetValue("Select " + Size + " From TblPLHdr Where DocNo = @Param; ", TxtPLDocNo.Text);
            if (mTHC.Length <= 0) mTHC = "0";
            else
            {
                if (mTHC == "40") mTHC = mTHC40Feet;
                if (mTHC == "20") mTHC = mTHC20Feet;
            }
            for (int Row = 0; Row <= Grd.Rows.Count - 1; Row++)
            if (Sm.GetGrdStr(Grd, Row, 0).Length != 0)
            {
                TotalPrice += Sm.GetGrdDec(Grd, Row, 16);
            }

            //for (int Row = 0; Row <= Grd.Rows.Count - 1; Row++)
            //{
            //    if (Sm.GetGrdStr(Grd, Row, 0).Length != 0 && TxtTotalKubik.Text.Length !=0)
            //    {
            //        Amt = (Sm.GetGrdDec(Grd, Row, 9) / TotalKubik) * TotalPrice;
            //        Frg = (Sm.GetGrdDec(Grd, Row, 9) / TotalKubik) * freight;
            //        thc = (Sm.GetGrdDec(Grd, Row, 9) / TotalKubik) * THC;
            //        Grd.Cells[Row, 15].Value = Amt - (Frg - thc);
            //    }
            //}

            for (int Row = 0; Row <= Grd.Rows.Count - 1; Row++)
            {
                if (freight > 0)
                {
                    if (Sm.GetGrdStr(Grd, Row, 0).Length != 0 && TxtTotalKubik.Text.Length != 0)
                    {
                        x1 = (Sm.GetGrdDec(Grd, Row, 9)) * Sm.GetGrdDec(Grd, Row, 14);
                        x2 = (Sm.GetGrdDec(Grd, Row, 9) / TotalKubik);
                        //Grd.Cells[Row, 15].Value = x1 - (x2 * (freight - THC));
                        Grd.Cells[Row, 15].Value = x1 - (x2 * (freight - Decimal.Parse(mTHC)));
                    }
                }
                else
                {
                    if (Sm.GetGrdStr(Grd, Row, 0).Length != 0 && TxtTotalKubik.Text.Length != 0)
                    {
                        x1 = (Sm.GetGrdDec(Grd, Row, 9)) * Sm.GetGrdDec(Grd, Row, 14);
                        Grd.Cells[Row, 15].Value = x1;
                    }
                }
            }
        }

   
        private void SetLueBL(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select OptCode As Col1, OptDesc As Col2 From TblOption Where OptCat = 'BLRequirement'  Order By OptDesc",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }
    
        public void ComputeUom(iGrid GrdXX)
        {
            decimal QtyPackaging = 0m, QtyUomConvert12 = 0m, QtyUomConvert21 = 0m;
            string UomSales, UomSales1, UomInv;

            for (int Row = 0; Row < GrdXX.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(GrdXX, Row, 7).Length != 0)
                {
                    try
                    {
                        QtyPackaging = Sm.GetGrdDec(GrdXX, Row, 8);
                        UomSales = Sm.GetGrdStr(GrdXX, Row, 10);
                        UomInv = Sm.GetGrdStr(GrdXX, Row, 12);
                        UomSales1 = Sm.GetValue("Select SalesUomCode From TblItem Where ItCode = '" + Sm.GetGrdStr(GrdXX, Row, 0) + "' ");

                        QtyUomConvert12 = Decimal.Parse(Sm.GetValue("Select Qty From TblItemPackagingUnit Where ItCode = '" + Sm.GetGrdStr(GrdXX, Row, 0) + "' And UomCode = '" + Sm.GetGrdStr(GrdXX, Row, 7) + "' "));
                        QtyUomConvert21 = Decimal.Parse(Sm.GetValue("Select Qty2 From TblItemPackagingUnit Where ItCode = '" + Sm.GetGrdStr(GrdXX, Row, 0) + "' And UomCode = '" + Sm.GetGrdStr(GrdXX, Row, 7) + "' "));

                        if (UomSales1 == UomSales)
                        {
                            GrdXX.Cells[Row, 9].Value = QtyPackaging * QtyUomConvert12;
                            //GrdXX.Cells[Row, 11].Value = Sm.GetGrdDec(GrdXX, Row, 9) - Sm.GetGrdDec(GrdXX, Row, 10);
                            //GrdXX.Cells[Row, 14].Value = Sm.GetGrdDec(GrdXX, Row, 12) - Sm.GetGrdDec(GrdXX, Row, 13);

                            if (UomSales == UomInv)
                            {
                                GrdXX.Cells[Row, 11].Value = Sm.GetGrdDec(GrdXX, Row, 9);
                            }
                            else
                            {
                                if (UomInv == UomSales1)
                                {
                                    GrdXX.Cells[Row, 11].Value = QtyPackaging * QtyUomConvert12;
                                }
                                else
                                {
                                    GrdXX.Cells[Row, 11].Value = QtyPackaging * QtyUomConvert21;
                                }
                            }
                        }
                        else
                        {
                            GrdXX.Cells[Row, 9].Value = QtyPackaging * QtyUomConvert21;
                            //GrdXX.Cells[Row, 11].Value = Sm.GetGrdDec(GrdXX, Row, 9) - Sm.GetGrdDec(GrdXX, Row, 10);
                            //GrdXX.Cells[Row, 14].Value = Sm.GetGrdDec(GrdXX, Row, 12) - Sm.GetGrdDec(GrdXX, Row, 13);

                            if (UomSales == UomInv)
                            {
                                GrdXX.Cells[Row, 11].Value = Sm.GetGrdDec(GrdXX, Row, 9);
                            }
                            else
                            {
                                if (UomInv == UomSales1)
                                {
                                    GrdXX.Cells[Row, 11].Value = QtyPackaging * QtyUomConvert12;
                                }
                                else
                                {
                                    GrdXX.Cells[Row, 11].Value = QtyPackaging * QtyUomConvert21;
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("" + ex + "");
                    }
                }
            }
        }

        private void SetLueCtCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            //if (TxtDocNo.Text.Length == 0)
            //{
            //    SQL.AppendLine("Select Distinct A.CtCode As Col1, B.CtName As Col2 ");
            //    SQL.AppendLine("From ( ");
            //    SQL.AppendLine("Select Distinct CtCode ");
            //    SQL.AppendLine("From TblSOHdr ");
            //    SQL.AppendLine("Where OverSeaInd = 'Y' And Status = 'O' And CancelInd= 'N' ");
            //}
            //else
            //{
                SQL.AppendLine("Select Distinct A.CtCode As Col1, B.CtName As Col2 ");
                SQL.AppendLine("From ( ");
                SQL.AppendLine("Select Distinct CtCode ");
                SQL.AppendLine("From TblSOHdr ");
                SQL.AppendLine("Where OverSeaInd = 'Y' And Status = 'O' And CancelInd= 'N' ");
                SQL.AppendLine("Union All ");
                SQL.AppendLine("Select Distinct CtCode  ");
                SQL.AppendLine("From TblSP ");
            //}
            SQL.AppendLine(")A ");
            SQL.AppendLine("Inner Join TblCustomer B On A.CtCode= B.CtCode And B.ActInd = 'Y' ");
            SQL.AppendLine("Order By B.CtName");

            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void LueRequestEdit(
          iGrid Grd,
          DevExpress.XtraEditors.LookUpEdit Lue,
          ref iGCell fCell,
          ref bool fAccept,
          TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, 0).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, 0));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        
        private bool IsDataExists(string SQL, string Param, string Warning)
        {
            var cm = new MySqlCommand() { CommandText = SQL };
            Sm.CmParam<String>(ref cm, "@Param", Param);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, Warning);
                return true;
            }
            return false;
        }

        private void SetLueNotify(ref LookUpEdit Lue, string CtCode)
        {
            Sm.SetLue1(
               ref Lue,
               "Select  NotifyParty As Col1 From TblCustomerNotifyParty  " +
               "Where CtCode= '" + CtCode + "' Order By NotifyParty ",
               "Notify Party");
        }

        private void SetLueItCode(ref DXE.LookUpEdit Lue, string CtCode, string SIDocNo)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select E.ItCode As Col1, F.ItName As Col2, ");
                SQL.AppendLine("A.DocNo As Col3, Concat(A.DocNo, B.DNo) As Col4  ");
                SQL.AppendLine("From TblSOHdr A ");
                SQL.AppendLine("Inner Join TblSODtl B On A.DocNo=B.DocNo and B.ProcessInd<>'F' ");
                SQL.AppendLine("Inner Join TblCtQtDtl C On A.CtQtDocNo=C.DocNo And B.CtQtDNo=C.DNo ");
                SQL.AppendLine("Inner Join TblItemPriceHdr D On C.ItemPriceDocNo=D.DOcNo ");
                SQL.AppendLine("Inner Join TblItemPriceDtl E On C.ItemPriceDocNo=E.DOcNo And C.ItemPriceDNo = E.DNo ");
                SQL.AppendLine("Inner Join TblItem F On E.ItCode = F.ItCode ");
                SQL.AppendLine("Inner join TblSIDtl G On A.DocNo = G.SODocNo And B.Dno = G.SODno ");
                SQL.AppendLine("Where A.CancelInd='N' And A.OverSeaInd = 'Y' ");
                SQL.AppendLine("And A.Status Not In ('M', 'F') ");
                SQL.AppendLine("And A.CtCode='"+CtCode+"' And G.DocNo = '"+SIDocNo+"'   ");
            
                Sm.SetLue4(
                    ref Lue,
                    SQL.ToString(),
                    30, 80, 60, 0, true, true, true, false, "Item Code", "Item Name", "SO", "DNo", "Col2", "Col4");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void ParPrint()
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "Document number", false) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;
     
            string Doctitle = Sm.GetParameter("DocTitle");

            #region Printout IOK

            if (Doctitle == "IOK")
            {
                var l = new List<SInvHdr>();
                var ldtl = new List<SInvDtl>();
                var ldtl2 = new List<SInvDtl2>();
                var ldtl3 = new List<SInvDtl3>();

                
                string Fob = "";
                string SMark = "N";
                if (ChkFOB.Checked == true)
                {
                    Fob = "Y";
                }
                else
                {
                    Fob = "N";
                }

                if (ChkShippingMark.Checked == true)
                {
                    SMark = "Y";
                }

                string[] TableName = { "SInvHdr", "SInvDtl", "SInvDtl2", "SInvDtl3" };
                List<IList> myLists = new List<IList>();
                var cm = new MySqlCommand();

                #region Header

                var SQL = new StringBuilder();

                SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyAddressCity', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyPhone', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle5') As 'CompanyFax', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='Shipper1') As 'Shipper1', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='Shipper2') As 'Shipper2', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='Shipper3') As 'Shipper3', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='Shipper4') As 'Shipper4', ");
                SQL.AppendLine("X.DocNo, X.LocalDocNo, DATE_FORMAT(X.DocDt,'%d %M %Y') As DocDt, X.PLDOcNo, B.SpCtNotifyParty, A.Account, A.InvoiceNo, A.SalesContractNo, A.LCNo, ");
                SQL.AppendLine(" DATE_FORMAT(A.LcDt,'%M %d, %Y') As LcDt, A.Issued1, A.Issued2, A.Issued3, A.Issued4,  ");
                SQL.AppendLine("A.ToOrder1, A.ToOrder2, A.ToOrder3, A.ToOrder4, A.Remark, DATE_FORMAT(C.StfDt,'%d %M %Y') As StfDt, ");
                SQL.AppendLine("D.PortName As PortLoading, E.PortName As PortDischarge, X.Remark As RemarkInv, X.Remark2, '" + Fob + "' As FOB, C.SpName ");
                SQL.AppendLine("From TblSinv X  ");
                SQL.AppendLine("Inner Join TblPLHdr A On X.PlDocNo = A.DocNo ");
                SQL.AppendLine("Inner Join TblSIhdr B On A.SIDocNo = B.DocNo ");
                SQL.AppendLine("Inner Join TblSP C On B.SpDocNo = C.DocNo ");
                SQL.AppendLine("Inner Join TblPort D On B.SpPortCode1 = D.PortCode ");
                SQL.AppendLine("Inner Join TblPort E On B.SpPortCode2 = E.PortCode ");
                SQL.AppendLine("Where X.DocNo=@DocNo ");

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = SQL.ToString();
                    Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                    Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] 
                        {
                        //0
                         "CompanyLogo",

                         //1-5
                         "CompanyName",
                         "CompanyAddress",
                         "CompanyAddressCity",
                         "CompanyPhone",
                         "CompanyFax",
                         //6-10
                         "DocNo",
                         "DocDt",
                         "PLDocNo",
                         "SpCtNotifyParty",
                         "Account",
                         //11-15
                         "SalesContractNo",
                         "LCNo",
                         "LCDt",
                         "Issued1",
                         "Issued2",
                         //16-20
                         "Issued3",
                         "Issued4",
                         "ToOrder1",
                         "ToOrder2",
                         "ToOrder3",
                         //21-25
                         "ToOrder4",
                         "Remark",
                         "StfDt",
                         "PortLoading",
                         "PortDischarge",
                         //26-30
                         "RemarkInv",
                         "Remark2",
                         "FOB",
                         "SPName",
                         "LocalDocNo",
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            l.Add(new SInvHdr()
                            {
                                CompanyLogo = Sm.DrStr(dr, c[0]),
                                CompanyName = Sm.DrStr(dr, c[1]),
                                CompanyAddress = Sm.DrStr(dr, c[2]),
                                CompanyAddressCity = Sm.DrStr(dr, c[3]),
                                CompanyPhone = Sm.DrStr(dr, c[4]),
                                CompanyFax = Sm.DrStr(dr, c[5]),
                                DocNo = Sm.DrStr(dr, c[6]),
                                DocDt = Sm.DrStr(dr, c[7]),
                                PL = Sm.DrStr(dr, c[8]),
                                NotifyParty = Sm.DrStr(dr, c[9]),
                                Account = Sm.DrStr(dr, c[10]),
                                SalesContract = Sm.DrStr(dr, c[11]),
                                LCNo = Sm.DrStr(dr, c[12]),
                                LCDate = Sm.DrStr(dr, c[13]),
                                Issued1 = Sm.DrStr(dr, c[14]),
                                Issued2 = Sm.DrStr(dr, c[15]),
                                Issued3 = Sm.DrStr(dr, c[16]),
                                Issued4 = Sm.DrStr(dr, c[17]),
                                ToOrder1 = Sm.DrStr(dr, c[18]),
                                ToOrder2 = Sm.DrStr(dr, c[19]),
                                ToOrder3 = Sm.DrStr(dr, c[20]),
                                ToOrder4 = Sm.DrStr(dr, c[21]),
                                Remark = Sm.DrStr(dr, c[22]),
                                Stufing = Sm.DrStr(dr, c[23]),
                                PortLoading = Sm.DrStr(dr, c[24]),
                                PortDischarge = Sm.DrStr(dr, c[25]),
                                RemarkInv = Sm.DrStr(dr, c[26]),
                                Note = Sm.DrStr(dr, c[27]),
                                FOB = Sm.DrStr(dr, c[28]),
                                SpName = Sm.DrStr(dr, c[29]),
                                SM = SMark,
                                LocalDocNo = Sm.DrStr(dr, c[30]),
                                PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
                            });
                        }
                    }
                    dr.Close();
                }
                myLists.Add(l);
                #endregion

                #region Detail
                var cmDtl = new MySqlCommand();


                var SQLDtl = new StringBuilder();
                using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
                {
                    string thc = Sm.GetValue("Select Concat(Right(OptDesc, 1),' ', 'HC') From TblOption Where OptCat = 'ContainerSize' Limit 1;");

                    cnDtl.Open();
                    cmDtl.Connection = cnDtl;

                    SQLDtl.AppendLine("Select X.DocNo, A2.Dno, A2.SectionNo, A2.ItCode, B.ForeignName As ItName, ");
                    SQLDtl.AppendLine("A2.Qty, G.PriceUomCode, Round(A2.QtyInventory, 4) As QtyInventory, B.InventoryUomCode, A2.PLNo, ifnull(A2.QtyPL, 0) As QtyPL,  ");
                    SQLDtl.AppendLine("(A2.QtyPackagingUnit * H.NW) As NW, (Select Parvalue From Tblparameter Where parCode = 'SIWeightUom') As NGuom,  ");
                    SQLDtl.AppendLine("(A2.QtyPackagingUnit * H.GW) As GW, F3.userName,  ");
                    SQLDtl.AppendLine("G.CurCode, F.UPrice, (F.UPrice * A2.Qty) As Amount,");
                    SQLDtl.AppendLine("Case ");
                    SQLDtl.AppendLine("When A2.SectionNo = '1' Then Concat('Container', ': ', A.Cnt1, '   ', ifnull(concat('1x',A.Size1, @thc), ''), '   ', 'Seal', ': ', A.seal1) ");
                    SQLDtl.AppendLine("When A2.SectionNo = '2' Then Concat('Container', ': ', A.Cnt2, '   ', ifnull(concat('1x',A.Size2, @thc), ''), '   ', 'Seal', ': ', A.seal2) ");
                    SQLDtl.AppendLine("When A2.SectionNo = '3' Then Concat('Container', ': ', A.Cnt3, '   ', ifnull(concat('1x',A.Size3, @thc), ''), '   ', 'Seal', ': ', A.seal3) ");
                    SQLDtl.AppendLine("When A2.SectionNo = '4' Then Concat('Container', ': ', A.Cnt4, '   ', ifnull(concat('1x',A.Size4, @thc), ''), '   ', 'Seal', ': ', A.seal4) ");
                    SQLDtl.AppendLine("When A2.SectionNo = '5' Then Concat('Container', ': ', A.Cnt5, '   ', ifnull(concat('1x',A.Size5, @thc), ''), '   ', 'Seal', ': ', A.seal5) ");
                    SQLDtl.AppendLine("When A2.SectionNo = '6' Then Concat('Container', ': ', A.Cnt6, '   ', ifnull(concat('1x',A.Size6, @thc), ''), '   ', 'Seal', ': ', A.seal6) ");
                    SQLDtl.AppendLine("When A2.SectionNo = '7' Then Concat('Container', ': ', A.Cnt7, '   ', ifnull(concat('1x',A.Size7, @thc), ''), '   ', 'Seal', ': ', A.seal7) ");
                    SQLDtl.AppendLine("When A2.SectionNo = '8' Then Concat('Container', ': ', A.Cnt8, '   ', ifnull(concat('1x',A.Size8, @thc), ''), '   ', 'Seal', ': ', A.seal8) ");
                    SQLDtl.AppendLine("When A2.SectionNo = '9' Then Concat('Container', ': ', A.Cnt9, '   ', ifnull(concat('1x',A.Size9, @thc), ''), '   ', 'Seal', ': ', A.seal9) ");
                    SQLDtl.AppendLine("When A2.SectionNo = '10' Then Concat('Container', ': ', A.Cnt10, '   ', ifnull(concat('1x',A.Size10, @thc), ''), '   ', 'Seal', ': ', A.seal10) ");
                    SQLDtl.AppendLine("When A2.SectionNo = '11' Then Concat('Container', ': ', A.Cnt11, '   ', ifnull(concat('1x',A.Size11, @thc), ''), '   ', 'Seal', ': ', A.seal11) ");
                    SQLDtl.AppendLine("When A2.SectionNo = '12' Then Concat('Container', ': ', A.Cnt12, '   ', ifnull(concat('1x',A.Size12, @thc), ''), '   ', 'Seal', ': ', A.seal12) ");
                    SQLDtl.AppendLine("When A2.SectionNo = '13' Then Concat('Container', ': ', A.Cnt13, '   ', ifnull(concat('1x',A.Size13, @thc), ''), '   ', 'Seal', ': ', A.seal13) ");
                    SQLDtl.AppendLine("When A2.SectionNo = '14' Then Concat('Container', ': ', A.Cnt14, '   ', ifnull(concat('1x',A.Size14, @thc), ''), '   ', 'Seal', ': ', A.seal14) ");
                    SQLDtl.AppendLine("When A2.SectionNo = '15' Then Concat('Container', ': ', A.Cnt15, '   ', ifnull(concat('1x',A.Size15, @thc), ''), '   ', 'Seal', ': ', A.seal15) ");
                    SQLDtl.AppendLine("When A2.SectionNo = '16' Then Concat('Container', ': ', A.Cnt16, '   ', ifnull(concat('1x',A.Size16, @thc), ''), '   ', 'Seal', ': ', A.seal16) ");
                    SQLDtl.AppendLine("When A2.SectionNo = '17' Then Concat('Container', ': ', A.Cnt17, '   ', ifnull(concat('1x',A.Size17, @thc), ''), '   ', 'Seal', ': ', A.seal17) ");
                    SQLDtl.AppendLine("When A2.SectionNo = '18' Then Concat('Container', ': ', A.Cnt18, '   ', ifnull(concat('1x',A.Size18, @thc), ''), '   ', 'Seal', ': ', A.seal18) ");
                    SQLDtl.AppendLine("When A2.SectionNo = '19' Then Concat('Container', ': ', A.Cnt19, '   ', ifnull(concat('1x',A.Size19, @thc), ''), '   ', 'Seal', ': ', A.seal19) ");
                    SQLDtl.AppendLine("When A2.SectionNo = '20' Then Concat('Container', ': ', A.Cnt20, '   ', ifnull(concat('1x',A.Size20, @thc), ''), '   ', 'Seal', ': ', A.seal20) ");
                    SQLDtl.AppendLine("When A2.SectionNo = '21' Then Concat('Container', ': ', A.Cnt21, '   ', ifnull(concat('1x',A.Size21, @thc), ''), '   ', 'Seal', ': ', A.seal21) ");
                    SQLDtl.AppendLine("When A2.SectionNo = '22' Then Concat('Container', ': ', A.Cnt22, '   ', ifnull(concat('1x',A.Size22, @thc), ''), '   ', 'Seal', ': ', A.seal22) ");
                    SQLDtl.AppendLine("When A2.SectionNo = '23' Then Concat('Container', ': ', A.Cnt23, '   ', ifnull(concat('1x',A.Size23, @thc), ''), '   ', 'Seal', ': ', A.seal23) ");
                    SQLDtl.AppendLine("When A2.SectionNo = '24' Then Concat('Container', ': ', A.Cnt24, '   ', ifnull(concat('1x',A.Size24, @thc), ''), '   ', 'Seal', ': ', A.seal24) ");
                    SQLDtl.AppendLine("When A2.SectionNo = '25' Then Concat('Container', ': ', A.Cnt25, '   ', ifnull(concat('1x',A.Size25, @thc), ''), '   ', 'Seal', ': ', A.seal25) ");
                    SQLDtl.AppendLine("end as Cnt, ");
                    SQLDtl.AppendLine("Case ");
                    SQLDtl.AppendLine("When A2.SectionNo = '1' Then A.Freight1 - Case A.Size1 When '40' Then 145 When '20' Then 95 Else 0 End ");
                    SQLDtl.AppendLine("When A2.SectionNo = '2' Then A.Freight2 - Case A.Size2 When '40' Then 145 When '20' Then 95 Else 0 End ");
                    SQLDtl.AppendLine("When A2.SectionNo = '3' Then A.Freight3 - Case A.Size3 When '40' Then 145 When '20' Then 95 Else 0 End ");
                    SQLDtl.AppendLine("When A2.SectionNo = '4' Then A.Freight4 - Case A.Size4 When '40' Then 145 When '20' Then 95 Else 0 End ");
                    SQLDtl.AppendLine("When A2.SectionNo = '5' Then A.Freight5 - Case A.Size5 When '40' Then 145 When '20' Then 95 Else 0 End ");
                    SQLDtl.AppendLine("When A2.SectionNo = '6' Then A.Freight6 - Case A.Size6 When '40' Then 145 When '20' Then 95 Else 0 End ");
                    SQLDtl.AppendLine("When A2.SectionNo = '7' Then A.Freight7 - Case A.Size7 When '40' Then 145 When '20' Then 95 Else 0 End ");
                    SQLDtl.AppendLine("When A2.SectionNo = '8' Then A.Freight8 - Case A.Size8 When '40' Then 145 When '20' Then 95 Else 0 End ");
                    SQLDtl.AppendLine("When A2.SectionNo = '9' Then A.Freight9 - Case A.Size9 When '40' Then 145 When '20' Then 95 Else 0 End ");
                    SQLDtl.AppendLine("When A2.SectionNo = '10' Then A.Freight10 - Case A.Size10 When '40' Then 145 When '20' Then 95 Else 0 End ");
                    SQLDtl.AppendLine("When A2.SectionNo = '11' Then A.Freight11 - Case A.Size11 When '40' Then 145 When '20' Then 95 Else 0 End ");
                    SQLDtl.AppendLine("When A2.SectionNo = '12' Then A.Freight12 - Case A.Size12 When '40' Then 145 When '20' Then 95 Else 0 End ");
                    SQLDtl.AppendLine("When A2.SectionNo = '13' Then A.Freight13 - Case A.Size13 When '40' Then 145 When '20' Then 95 Else 0 End ");
                    SQLDtl.AppendLine("When A2.SectionNo = '14' Then A.Freight14 - Case A.Size14 When '40' Then 145 When '20' Then 95 Else 0 End ");
                    SQLDtl.AppendLine("When A2.SectionNo = '15' Then A.Freight15 - Case A.Size15 When '40' Then 145 When '20' Then 95 Else 0 End ");
                    SQLDtl.AppendLine("When A2.SectionNo = '16' Then A.Freight16 - Case A.Size16 When '40' Then 145 When '20' Then 95 Else 0 End ");
                    SQLDtl.AppendLine("When A2.SectionNo = '17' Then A.Freight17 - Case A.Size17 When '40' Then 145 When '20' Then 95 Else 0 End ");
                    SQLDtl.AppendLine("When A2.SectionNo = '18' Then A.Freight18 - Case A.Size18 When '40' Then 145 When '20' Then 95 Else 0 End ");
                    SQLDtl.AppendLine("When A2.SectionNo = '19' Then A.Freight19 - Case A.Size19 When '40' Then 145 When '20' Then 95 Else 0 End ");
                    SQLDtl.AppendLine("When A2.SectionNo = '20' Then A.Freight20 - Case A.Size20 When '40' Then 145 When '20' Then 95 Else 0 End ");
                    SQLDtl.AppendLine("When A2.SectionNo = '21' Then A.Freight21 - Case A.Size21 When '40' Then 145 When '20' Then 95 Else 0 End ");
                    SQLDtl.AppendLine("When A2.SectionNo = '22' Then A.Freight22 - Case A.Size22 When '40' Then 145 When '20' Then 95 Else 0 End ");
                    SQLDtl.AppendLine("When A2.SectionNo = '23' Then A.Freight23 - Case A.Size23 When '40' Then 145 When '20' Then 95 Else 0 End ");
                    SQLDtl.AppendLine("When A2.SectionNo = '24' Then A.Freight24 - Case A.Size24 When '40' Then 145 When '20' Then 95 Else 0 End ");
                    SQLDtl.AppendLine("When A2.SectionNo = '25' Then A.Freight25 - Case A.Size25 When '40' Then 145 When '20' Then 95 Else 0 End ");
                    SQLDtl.AppendLine("end as Freight, ");
                    SQLDtl.AppendLine("Case ");
                    SQLDtl.AppendLine("When A2.SectionNo = '1' Then A.Cnt1 ");
                    SQLDtl.AppendLine("When A2.SectionNo = '2' Then A.Cnt2 ");
                    SQLDtl.AppendLine("When A2.SectionNo = '3' Then A.Cnt3 ");
                    SQLDtl.AppendLine("When A2.SectionNo = '4' Then A.Cnt4 ");
                    SQLDtl.AppendLine("When A2.SectionNo = '5' Then A.Cnt5 ");
                    SQLDtl.AppendLine("When A2.SectionNo = '6' Then A.Cnt6 ");
                    SQLDtl.AppendLine("When A2.SectionNo = '7' Then A.Cnt7 ");
                    SQLDtl.AppendLine("When A2.SectionNo = '8' Then A.Cnt8 ");
                    SQLDtl.AppendLine("When A2.SectionNo = '9' Then A.Cnt9 ");
                    SQLDtl.AppendLine("When A2.SectionNo = '10' Then A.Cnt10 ");
                    SQLDtl.AppendLine("When A2.SectionNo = '11' Then A.Cnt11 ");
                    SQLDtl.AppendLine("When A2.SectionNo = '12' Then A.Cnt12 ");
                    SQLDtl.AppendLine("When A2.SectionNo = '13' Then A.Cnt13 ");
                    SQLDtl.AppendLine("When A2.SectionNo = '14' Then A.Cnt14 ");
                    SQLDtl.AppendLine("When A2.SectionNo = '15' Then A.Cnt15 ");
                    SQLDtl.AppendLine("When A2.SectionNo = '16' Then A.Cnt16 ");
                    SQLDtl.AppendLine("When A2.SectionNo = '17' Then A.Cnt17 ");
                    SQLDtl.AppendLine("When A2.SectionNo = '18' Then A.Cnt18 ");
                    SQLDtl.AppendLine("When A2.SectionNo = '19' Then A.Cnt19 ");
                    SQLDtl.AppendLine("When A2.SectionNo = '20' Then A.Cnt20 ");
                    SQLDtl.AppendLine("When A2.SectionNo = '21' Then A.Cnt21 ");
                    SQLDtl.AppendLine("When A2.SectionNo = '22' Then A.Cnt22 ");
                    SQLDtl.AppendLine("When A2.SectionNo = '23' Then A.Cnt23 ");
                    SQLDtl.AppendLine("When A2.SectionNo = '24' Then A.Cnt24 ");
                    SQLDtl.AppendLine("When A2.SectionNo = '25' Then A.Cnt25 ");
                    SQLDtl.AppendLine("end as CntName, ");
                    SQLDtl.AppendLine("if(J.Qty =0, 0, if(G.PriceUomCode = B.SalesUomCode2, A2.Qty, (J.Qty2*A2.Qty)/J.Qty)) As Qty2, A2.Remark ");
                    SQLDtl.AppendLine("From TblSInv X ");
                    SQLDtl.AppendLine("Inner Join TblPlhdr A On A.DocNo = X.PlDocNo ");
                    SQLDtl.AppendLine("Inner Join TblPlDtl A2 On A.DocNo = A2.DocNo ");
                    SQLDtl.AppendLine("Inner Join TblItem B On A2.ItCode = B.ItCode  ");
                    SQLDtl.AppendLine("Inner Join TblSODtl C On A2.SoDocNo = C.DocNo And A2.SoDno = C.Dno ");
                    SQLDtl.AppendLine("Inner Join TblSOHdr D On C.DocNo = D.DocNo And A2.SODocNo = D.DocNo  ");
                    SQLDtl.AppendLine("Inner Join TblCtQtDtl F On D.CtQtDocNo = F.DocNo And C.CtQtDNo = F.Dno ");
                    SQLDtl.AppendLine("Inner Join TblCtQtHdr F2 On D.CtQtDocNo = F2.DocNo ");
                    SQLDtl.AppendLine("Inner Join TblUser F3 On F2.SpCode = F3.UserCode ");
                    SQLDtl.AppendLine("Inner Join TblItemPriceHdr G On F.ItemPriceDocNo = G.DocNo ");
                    SQLDtl.AppendLine("Left Join TblItemPackagingUnit J On C.PackagingUnitUomCode = J.UomCOde And A2.ItCode = J.ItCode ");
                    SQLDtl.AppendLine("left Join ");
                    SQLDtl.AppendLine("( ");
                    SQLDtl.AppendLine("  select ItCode, UomCode, Nw, GW From TblItempackagingunit ");
                    SQLDtl.AppendLine(")H On H.ItCode = A2.ItCode And  C.PackagingUnitUomCode = H.UomCode ");
                    //SQLDtl.AppendLine("Left Join (Select ParValue As THC From tblParameter Where ParCode = 'THC') I On 0=0 ");
                    SQLDtl.AppendLine("Where X.DocNo=@DocNo ");
                    SQLDtl.AppendLine("Order by  A2.Dno, CntName ");

                    cmDtl.CommandText = SQLDtl.ToString();

                    Sm.CmParam<String>(ref cmDtl, "@DocNo", TxtDocNo.Text);
                    Sm.CmParam<String>(ref cmDtl, "@thc", thc);

                    var drDtl = cmDtl.ExecuteReader();
                    var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                    {
                     //0
                     "Cnt" ,

                     //1-5
                     "ItCode" ,
                     "ItName",
                     "PLNo",
                     "QtyPL",
                     "Qty",

                     //6-8
                     "QtyInventory",
                     "NW",
                     "GW",
                     "Username",
                     "SectionNo",
                     //11-13
                     "CurCode",
                     "UPrice",
                     "Amount",
                     "Freight",
                     "Qty2",
                     "Remark",
                     "DNo"
                    });
                    if (drDtl.HasRows)
                    {
                        while (drDtl.Read())
                        {
                            ldtl.Add(new SInvDtl()
                            {
                                Cnt = Sm.DrStr(drDtl, cDtl[0]),
                                ItCode = Sm.DrStr(drDtl, cDtl[1]),
                                ItName = Sm.DrStr(drDtl, cDtl[2]),
                                PLNo = Sm.DrStr(drDtl, cDtl[3]),
                                QtyPL = Sm.DrDec(drDtl, cDtl[4]),
                                Qty = Sm.DrDec(drDtl, cDtl[5]),
                                QtyInventory = Sm.DrDec(drDtl, cDtl[6]),
                                NW = Sm.DrDec(drDtl, cDtl[7]),
                                GW = Sm.DrDec(drDtl, cDtl[8]),
                                Username = Sm.DrStr(drDtl, cDtl[9]),
                                SectionNo = Sm.DrDec(drDtl, cDtl[10]),
                                Currency = Sm.DrStr(drDtl, cDtl[11]),
                                Price = Sm.DrDec(drDtl, cDtl[12]),
                                Amount = Sm.DrDec(drDtl, cDtl[13]),
                                Freight = Sm.DrDec(drDtl, cDtl[14]),
                                Qty2 = Sm.DrDec(drDtl, cDtl[15]),
                                Remark = Sm.DrStr(drDtl, cDtl[16]),
                                Dno = Sm.DrStr(drDtl, cDtl[17]),
                            });
                        }
                    }
                    drDtl.Close();
                }
                myLists.Add(ldtl);
                #endregion

                #region Detail2
                var cmDtl2 = new MySqlCommand();

                var SQLDtl2 = new StringBuilder();
                using (var cnDtl2 = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl2.Open();
                    cmDtl2.Connection = cnDtl2;

                    SQLDtl2.AppendLine("Select X2.DocNo, Round(SUM(X2.Amount), 2) As Amount, (SUM(X2.Amount) - X2.TotalFre) As Totally, X2.TotalFre From ( ");
                    SQLDtl2.AppendLine("Select X.DocNo, (F.UPrice * A2.Qty) As Amount,  ");
                    SQLDtl2.AppendLine("G.TotalFre  ");
                    SQLDtl2.AppendLine("From TblSInv X  ");
                    SQLDtl2.AppendLine("Inner Join TblPlhdr A On A.DocNo = X.PlDocNo  ");
                    SQLDtl2.AppendLine("Inner Join TblPlDtl A2 On A.DocNo = A2.DocNo ");
                    SQLDtl2.AppendLine("Inner Join TblItem B On A2.ItCode = B.ItCode  ");
                    SQLDtl2.AppendLine("Inner Join TblSODtl C On A2.SoDocNo = C.DocNo And A2.SoDno = C.Dno ");
                    SQLDtl2.AppendLine("Inner Join TblSOHdr D On C.DocNo = D.DocNo And A2.SODocNo = D.DocNo ");
                    SQLDtl2.AppendLine("Inner Join TblCtQtDtl F On D.CtQtDocNo = F.DocNo And C.CtQtDNo = F.Dno ");
                    SQLDtl2.AppendLine("Inner Join TblCtQtHdr F2 On D.CtQtDocNo = F2.DocNo ");
                    SQLDtl2.AppendLine("Inner Join ( ");
                    SQLDtl2.AppendLine("Select S.Docno, SUM(S.f1+S.f2+S.f3+S.f4+S.f5+ ");
                    SQLDtl2.AppendLine("S.f6+S.F7+S.F8+S.f9+S.f10+S.f11+S.f12+S.f13+S.f14+S.f15+S.f16+S.f17+S.f18+S.f19+S.f20+S.f21+S.f22+S.f23+S.f24+S.f25) As totalFre ");
                    SQLDtl2.AppendLine("From ( ");
                    SQLDtl2.AppendLine("Select Distinct X.DocNo,  ");
                    SQLDtl2.AppendLine("    if(A.Freight1 != 0, (A.Freight1-Case A.Size1 When '40' Then 145 When '20' Then 95 Else 0 End), 0) As F1, ");
                    SQLDtl2.AppendLine("     if(A.Freight2 != 0, (A.Freight2-Case A.Size2 When '40' Then 145 When '20' Then 95 Else 0 End), 0) As F2, ");
                    SQLDtl2.AppendLine("     if(A.Freight3 != 0, (A.Freight3-Case A.Size3 When '40' Then 145 When '20' Then 95 Else 0 End), 0) As F3, ");
                    SQLDtl2.AppendLine("     if(A.Freight4 != 0, (A.Freight4-Case A.Size4 When '40' Then 145 When '20' Then 95 Else 0 End), 0) As F4, ");
                    SQLDtl2.AppendLine("     if(A.Freight5 != 0, (A.Freight5-Case A.Size5 When '40' Then 145 When '20' Then 95 Else 0 End), 0) As F5, ");
                    SQLDtl2.AppendLine("     if(A.Freight6 != 0, (A.Freight6-Case A.Size6 When '40' Then 145 When '20' Then 95 Else 0 End), 0) As F6, ");
                    SQLDtl2.AppendLine("     if(A.Freight7 != 0, (A.Freight7-Case A.Size7 When '40' Then 145 When '20' Then 95 Else 0 End), 0) As F7, ");
                    SQLDtl2.AppendLine("     if(A.Freight8 != 0, (A.Freight8-Case A.Size8 When '40' Then 145 When '20' Then 95 Else 0 End), 0) As F8, ");
                    SQLDtl2.AppendLine("     if(A.Freight9 != 0, (A.Freight9-Case A.Size9 When '40' Then 145 When '20' Then 95 Else 0 End), 0) As F9, ");
                    SQLDtl2.AppendLine("     if(A.Freight10 != 0, (A.Freight10-Case A.Size10 When '40' Then 145 When '20' Then 95 Else 0 End), 0) As F10, ");
                    SQLDtl2.AppendLine("     if(A.Freight11 != 0, (A.Freight11-Case A.Size11 When '40' Then 145 When '20' Then 95 Else 0 End), 0) As F11, ");
                    SQLDtl2.AppendLine("     if(A.Freight12 != 0, (A.Freight12-Case A.Size12 When '40' Then 145 When '20' Then 95 Else 0 End), 0) As F12, ");
                    SQLDtl2.AppendLine("     if(A.Freight13 != 0, (A.Freight13-Case A.Size13 When '40' Then 145 When '20' Then 95 Else 0 End), 0) As F13, ");
                    SQLDtl2.AppendLine("     if(A.Freight14 != 0, (A.Freight14-Case A.Size14 When '40' Then 145 When '20' Then 95 Else 0 End), 0) As F14, ");
                    SQLDtl2.AppendLine("     if(A.Freight15 != 0, (A.Freight15-Case A.Size15 When '40' Then 145 When '20' Then 95 Else 0 End), 0) As F15, ");
                    SQLDtl2.AppendLine("     if(A.Freight16 != 0, (A.Freight16-Case A.Size16 When '40' Then 145 When '20' Then 95 Else 0 End), 0) As F16, ");
                    SQLDtl2.AppendLine("     if(A.Freight17 != 0, (A.Freight17-Case A.Size17 When '40' Then 145 When '20' Then 95 Else 0 End), 0) As F17, ");
                    SQLDtl2.AppendLine("     if(A.Freight18 != 0, (A.Freight18-Case A.Size18 When '40' Then 145 When '20' Then 95 Else 0 End), 0) As F18, ");
                    SQLDtl2.AppendLine("     if(A.Freight19 != 0, (A.Freight19-Case A.Size19 When '40' Then 145 When '20' Then 95 Else 0 End), 0) As F19, ");
                    SQLDtl2.AppendLine("     if(A.Freight20 != 0, (A.Freight20-Case A.Size20 When '40' Then 145 When '20' Then 95 Else 0 End), 0) As F20, ");
                    SQLDtl2.AppendLine("     if(A.Freight21 != 0, (A.Freight21-Case A.Size21 When '40' Then 145 When '20' Then 95 Else 0 End), 0) As F21, ");
                    SQLDtl2.AppendLine("     if(A.Freight22 != 0, (A.Freight22-Case A.Size22 When '40' Then 145 When '20' Then 95 Else 0 End), 0) As F22, ");
                    SQLDtl2.AppendLine("     if(A.Freight23 != 0, (A.Freight23-Case A.Size23 When '40' Then 145 When '20' Then 95 Else 0 End), 0) As F23, ");
                    SQLDtl2.AppendLine("     if(A.Freight24 != 0, (A.Freight24-Case A.Size24 When '40' Then 145 When '20' Then 95 Else 0 End), 0) As F24, ");
                    SQLDtl2.AppendLine("     if(A.Freight25 != 0, (A.Freight25-Case A.Size25 When '40' Then 145 When '20' Then 95 Else 0 End), 0) As F25 ");
                    SQLDtl2.AppendLine("    From TblSInv X   ");
                    SQLDtl2.AppendLine("    Inner Join TblPlhdr A On A.DocNo = X.PlDocNo ");
                    SQLDtl2.AppendLine("    Inner Join TblPlDtl A2 On A.DocNo = A2.DocNo   ");
                    //SQLDtl2.AppendLine("    Left Join ( ");
                    //SQLDtl2.AppendLine("     Select ParValue As THC From tblParameter Where ParCode = 'THC' ");
                    //SQLDtl2.AppendLine("     ) B On 0=0  ");
                    SQLDtl2.AppendLine("    Where X.DocNo = @Docno ");
                    SQLDtl2.AppendLine(" )S ");
                    SQLDtl2.AppendLine("    Where S.DocNo = @Docno )G On X.DocNo = G.Docno ");
                    SQLDtl2.AppendLine("Where X.DocNo=@DocNo ");
                    SQLDtl2.AppendLine(")X2 ");
                    SQLDtl2.AppendLine("Group by X2.DocNo ");

                    cmDtl2.CommandText = SQLDtl2.ToString();

                    Sm.CmParam<String>(ref cmDtl2, "@DocNo", TxtDocNo.Text);

                    var drDtl2 = cmDtl2.ExecuteReader();
                    var cDtl2 = Sm.GetOrdinal(drDtl2, new string[] 
                    {
                     //0
                     "Amount",
                     "Totally",
                     "TotalFre",
                    });
                    if (drDtl2.HasRows)
                    {
                        while (drDtl2.Read())
                        {
                            ldtl2.Add(new SInvDtl2()
                            {
                                Amount = Sm.DrDec(drDtl2, cDtl2[0]),
                                Terbilang = Convert(Sm.DrDec(drDtl2, cDtl2[0])),
                                Totally = Sm.DrDec(drDtl2, cDtl2[1]),
                                TotalFre = Sm.DrDec(drDtl2, cDtl2[2]),
                            });
                        }
                    }
                    drDtl2.Close();
                }
                myLists.Add(ldtl2);
                #endregion

                #region Detail 3
                var cmDtl3 = new MySqlCommand();

                var SQLDtl3 = new StringBuilder();
                using (var cnDtl3 = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl3.Open();
                    cmDtl3.Connection = cnDtl3;

                    SQLDtl3.AppendLine("Select Count(X.Size) As Jumlah, X.Size ");
                    SQLDtl3.AppendLine("From ( ");
                    SQLDtl3.AppendLine("Select B.OptDesc As Size From TblPlHdr A ");
                    SQLDtl3.AppendLine("Inner Join TblOpTion B On A.Size1 = B.OptCode And B.OptCat = 'ContainerSize'  ");
                    SQLDtl3.AppendLine("Where DocNo = @DocNo ");
                    SQLDtl3.AppendLine("Union all ");
                    SQLDtl3.AppendLine("Select B.OptDesc As Size From TblPlHdr A ");
                    SQLDtl3.AppendLine("Inner Join TblOpTion B On A.Size2 = B.OptCode And B.OptCat = 'ContainerSize'  ");
                    SQLDtl3.AppendLine("Where DocNo = @DocNo ");
                    SQLDtl3.AppendLine("Union all ");
                    SQLDtl3.AppendLine("Select B.OptDesc As Size From TblPlHdr A ");
                    SQLDtl3.AppendLine("Inner Join TblOpTion B On A.Size3 = B.OptCode And B.OptCat = 'ContainerSize'  ");
                    SQLDtl3.AppendLine("Where DocNo = @DocNo ");
                    SQLDtl3.AppendLine("Union all ");
                    SQLDtl3.AppendLine("Select B.OptDesc As Size From TblPlHdr A ");
                    SQLDtl3.AppendLine("Inner Join TblOpTion B On A.Size4 = B.OptCode And B.OptCat = 'ContainerSize'   ");
                    SQLDtl3.AppendLine("Where DocNo = @DocNo ");
                    SQLDtl3.AppendLine("Union all ");
                    SQLDtl3.AppendLine("Select B.OptDesc As Size From TblPlHdr A ");
                    SQLDtl3.AppendLine("Inner Join TblOpTion B On A.Size5 = B.OptCode And B.OptCat = 'ContainerSize'   ");
                    SQLDtl3.AppendLine("Where DocNo = @DocNo ");
                    SQLDtl3.AppendLine("Union all ");
                    SQLDtl3.AppendLine("Select B.OptDesc As Size From TblPlHdr A ");
                    SQLDtl3.AppendLine("Inner Join TblOpTion B On A.Size6 = B.OptCode And B.OptCat = 'ContainerSize'  ");
                    SQLDtl3.AppendLine("Where DocNo = @DocNo ");
                    SQLDtl3.AppendLine("Union all ");
                    SQLDtl3.AppendLine("Select B.OptDesc As Size From TblPlHdr A ");
                    SQLDtl3.AppendLine("Inner Join TblOpTion B On A.Size7 = B.OptCode And B.OptCat = 'ContainerSize'  ");
                    SQLDtl3.AppendLine("Where DocNo = @DocNo ");
                    SQLDtl3.AppendLine("Union all ");
                    SQLDtl3.AppendLine("Select B.OptDesc As Size From TblPlHdr A ");
                    SQLDtl3.AppendLine("Inner Join TblOpTion B On A.Size8 = B.OptCode And B.OptCat = 'ContainerSize'  ");
                    SQLDtl3.AppendLine("Where DocNo = @DocNo");
                    SQLDtl3.AppendLine("Union all");
                    SQLDtl3.AppendLine("Select B.OptDesc As Size From TblPlHdr A ");
                    SQLDtl3.AppendLine("Inner Join TblOpTion B On A.Size9 = B.OptCode And B.OptCat = 'ContainerSize'  ");
                    SQLDtl3.AppendLine("Where DocNo = @DocNo");
                    SQLDtl3.AppendLine("Union all");
                    SQLDtl3.AppendLine("Select B.OptDesc As Size From TblPlHdr A ");
                    SQLDtl3.AppendLine("Inner Join TblOpTion B On A.Size10 = B.OptCode And B.OptCat = 'ContainerSize'  ");
                    SQLDtl3.AppendLine("Where DocNo = @DocNo");
                    SQLDtl3.AppendLine("Union all");
                    SQLDtl3.AppendLine("Select B.OptDesc As Size From TblPlHdr A ");
                    SQLDtl3.AppendLine("Inner Join TblOpTion B On A.Size11 = B.OptCode And B.OptCat = 'ContainerSize'  ");
                    SQLDtl3.AppendLine("Where DocNo = @DocNo");
                    SQLDtl3.AppendLine("Union all");
                    SQLDtl3.AppendLine("Select B.OptDesc As Size From TblPlHdr A ");
                    SQLDtl3.AppendLine("Inner Join TblOpTion B On A.Size12 = B.OptCode And B.OptCat = 'ContainerSize'  ");
                    SQLDtl3.AppendLine("Where DocNo = @DocNo");
                    SQLDtl3.AppendLine("Union all");
                    SQLDtl3.AppendLine("Select B.OptDesc As Size From TblPlHdr A ");
                    SQLDtl3.AppendLine("Inner Join TblOpTion B On A.Size13 = B.OptCode And B.OptCat = 'ContainerSize'  ");
                    SQLDtl3.AppendLine("Where DocNo = @DocNo");
                    SQLDtl3.AppendLine("Union all");
                    SQLDtl3.AppendLine("Select B.OptDesc As Size From TblPlHdr A ");
                    SQLDtl3.AppendLine("Inner Join TblOpTion B On A.Size14 = B.OptCode And B.OptCat = 'ContainerSize'  ");
                    SQLDtl3.AppendLine("Where DocNo = @DocNo");
                    SQLDtl3.AppendLine("Union all");
                    SQLDtl3.AppendLine("Select B.OptDesc As Size From TblPlHdr A ");
                    SQLDtl3.AppendLine("Inner Join TblOpTion B On A.Size15 = B.OptCode And B.OptCat = 'ContainerSize'  ");
                    SQLDtl3.AppendLine("Where DocNo = @DocNo");
                    SQLDtl3.AppendLine("Union all ");
                    SQLDtl3.AppendLine("Select B.OptDesc As Size From TblPlHdr A ");
                    SQLDtl3.AppendLine("Inner Join TblOpTion B On A.Size16 = B.OptCode And B.OptCat = 'ContainerSize'  ");
                    SQLDtl3.AppendLine("Where DocNo = @DocNo ");
                    SQLDtl3.AppendLine("Union all ");
                    SQLDtl3.AppendLine("Select B.OptDesc As Size From TblPlHdr A ");
                    SQLDtl3.AppendLine("Inner Join TblOpTion B On A.Size17 = B.OptCode And B.OptCat = 'ContainerSize'  ");
                    SQLDtl3.AppendLine("Where DocNo = @DocNo ");
                    SQLDtl3.AppendLine("Union all ");
                    SQLDtl3.AppendLine("Select B.OptDesc As Size From TblPlHdr A ");
                    SQLDtl3.AppendLine("Inner Join TblOpTion B On A.Size18 = B.OptCode And B.OptCat = 'ContainerSize'  ");
                    SQLDtl3.AppendLine("Where DocNo = @DocNo");
                    SQLDtl3.AppendLine("Union all");
                    SQLDtl3.AppendLine("Select B.OptDesc As Size From TblPlHdr A ");
                    SQLDtl3.AppendLine("Inner Join TblOpTion B On A.Size19 = B.OptCode And B.OptCat = 'ContainerSize'  ");
                    SQLDtl3.AppendLine("Where DocNo = @DocNo");
                    SQLDtl3.AppendLine("Union all");
                    SQLDtl3.AppendLine("Select B.OptDesc As Size From TblPlHdr A ");
                    SQLDtl3.AppendLine("Inner Join TblOpTion B On A.Size20 = B.OptCode And B.OptCat = 'ContainerSize'  ");
                    SQLDtl3.AppendLine("Where DocNo = @DocNo");
                    SQLDtl3.AppendLine("Union all");
                    SQLDtl3.AppendLine("Select B.OptDesc As Size From TblPlHdr A ");
                    SQLDtl3.AppendLine("Inner Join TblOpTion B On A.Size21 = B.OptCode And B.OptCat = 'ContainerSize'  ");
                    SQLDtl3.AppendLine("Where DocNo = @DocNo");
                    SQLDtl3.AppendLine("Union all");
                    SQLDtl3.AppendLine("Select B.OptDesc As Size From TblPlHdr A ");
                    SQLDtl3.AppendLine("Inner Join TblOpTion B On A.Size22 = B.OptCode And B.OptCat = 'ContainerSize'  ");
                    SQLDtl3.AppendLine("Where DocNo = @DocNo");
                    SQLDtl3.AppendLine("Union all");
                    SQLDtl3.AppendLine("Select B.OptDesc As Size From TblPlHdr A ");
                    SQLDtl3.AppendLine("Inner Join TblOpTion B On A.Size23 = B.OptCode And B.OptCat = 'ContainerSize'  ");
                    SQLDtl3.AppendLine("Where DocNo = @DocNo");
                    SQLDtl3.AppendLine("Union all");
                    SQLDtl3.AppendLine("Select B.OptDesc As Size From TblPlHdr A ");
                    SQLDtl3.AppendLine("Inner Join TblOpTion B On A.Size24 = B.OptCode And B.OptCat = 'ContainerSize'  ");
                    SQLDtl3.AppendLine("Where DocNo = @DocNo");
                    SQLDtl3.AppendLine("Union all");
                    SQLDtl3.AppendLine("Select B.OptDesc As Size From TblPlHdr A ");
                    SQLDtl3.AppendLine("Inner Join TblOpTion B On A.Size25 = B.OptCode And B.OptCat = 'ContainerSize'  ");
                    SQLDtl3.AppendLine("Where DocNo = @DocNo");
                    SQLDtl3.AppendLine(")X ");
                    SQLDtl3.AppendLine("group By X.Size");
                    SQLDtl3.AppendLine("having count(X.Size)>=1");

                    cmDtl3.CommandText = SQLDtl3.ToString();

                    Sm.CmParam<String>(ref cmDtl3, "@DocNo", TxtPLDocNo.Text);

                    var drDtl3 = cmDtl3.ExecuteReader();
                    var cDtl3 = Sm.GetOrdinal(drDtl3, new string[] 
                    {
                     //0
                     "Jumlah" ,

                     //1-5
                     "Size" ,
                    });
                    if (drDtl3.HasRows)
                    {
                        while (drDtl3.Read())
                        {
                            ldtl3.Add(new SInvDtl3()
                            {
                                Jumlah = Sm.DrStr(drDtl3, cDtl3[0]),
                                Size = Sm.DrStr(drDtl3, cDtl3[1]),
                            });
                        }
                    }
                    drDtl3.Close();
                }
                myLists.Add(ldtl3);
                #endregion

            #endregion

                if (ChkFOB.Checked == false)
                {
                    Sm.PrintReport(mIsFormPrintOutSInv, myLists, TableName, false);
                }
                else
                {
                    Sm.PrintReport("SInv2", myLists, TableName, false);
                }
            }

            #region PrintOut Mai

            if (Doctitle == "MAI")
            {
                var l2 = new List<SInvHdrMai>();
                var ldtl4 = new List<SInvDtl4>();
                var ldtl5 = new List<SInvDtl5>();
                var ldtl6 = new List<SInvDtl6>();

                string[] TableName = { "SInvHdrMai", "SInvDtl4", "SInvDtl5", "SInvDtl6" };
                List<IList> myLists = new List<IList>();


                #region HeaderMai
                var cm1 = new MySqlCommand();
                var SQL1 = new StringBuilder();

                SQL1.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
                SQL1.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
                SQL1.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyAddressCity', ");
                SQL1.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyPhone', ");
                SQL1.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle5') As 'CompanyFax', ");
                SQL1.AppendLine("(Select ParValue From TblParameter Where Parcode='Shipper1') As 'Shipper1', ");
                SQL1.AppendLine("(Select ParValue From TblParameter Where Parcode='Shipper2') As 'Shipper2', ");
                SQL1.AppendLine("(Select ParValue From TblParameter Where Parcode='Shipper3') As 'Shipper3', ");
                SQL1.AppendLine("(Select ParValue From TblParameter Where Parcode='Shipper4') As 'Shipper4', ");
                SQL1.AppendLine("X.DocNo, X.LocalDocNo, DATE_FORMAT(X.DocDt,'%M %d, %Y') As DocDt, X.PLDOcNo, B.SpCtNotifyParty, B.SpCtNotifyParty2, A.Account, A.InvoiceNo, A.SalesContractNo, ");
                SQL1.AppendLine("A.LCNo, DATE_FORMAT(A.LcDt,'%M %d, %Y') As LcDt, DATE_FORMAT(C.StfDt,'%M %d, %Y') As StfDt, D.PortName As PortLoading, ");
                SQL1.AppendLine("E.PortName As PortDischarge, B.SPPlaceDelivery, X.Remark2, C.SpName, B2.SOLocal, E.PortName, A.LocalDocNo As PLLocal, B2.CtPONo ");
                SQL1.AppendLine("From TblSinv X ");
                SQL1.AppendLine("Inner Join TblPLHdr A On X.PlDocNo = A.DocNo ");
                SQL1.AppendLine("Inner Join TblSIhdr B On A.SIDocNo = B.DocNo ");
                SQL1.AppendLine("Left Join ( ");
                SQL1.AppendLine("		Select  A.DocNo As SIDOCno, Group_concat(Distinct C.LocalDocNo separator ', ' )As SoLocal, Group_concat(Distinct C.CtPONo separator ', ' )As CtPONo ");
                SQL1.AppendLine("       From tblsidtl A ");
                SQL1.AppendLine("		Inner Join TblSodtl B On A.SODocNo=B.DocNo And A.SODNo=B.Dno ");
                SQL1.AppendLine("		Inner JOin TblSoHdr C On B.DocNo=C.DocNo ");
                SQL1.AppendLine("		Group By A.DocNo ");
                SQL1.AppendLine(")B2 On B.DocNo=B2.SiDocNo ");
                SQL1.AppendLine("Inner Join TblSP C On B.SpDocNo = C.DocNo ");
                SQL1.AppendLine("Inner Join TblPort D On B.SpPortCode1 = D.PortCode ");
                SQL1.AppendLine("Inner Join TblPort E On B.SpPortCode2 = E.PortCode ");
                SQL1.AppendLine("Where X.DocNo=@DocNo ");

                using (var cn1 = new MySqlConnection(Gv.ConnectionString))
                {
                    cn1.Open();
                    cm1.Connection = cn1;
                    cm1.CommandText = SQL1.ToString();
                    Sm.CmParam<String>(ref cm1, "@DocNo", TxtDocNo.Text);
                    Sm.CmParam<String>(ref cm1, "@CompanyLogo", @Sm.CompanyLogo());
                    var dr1 = cm1.ExecuteReader();
                    var c1 = Sm.GetOrdinal(dr1, new string[] 
                        {
                        //0
                         "CompanyLogo",

                         //1-5
                         "CompanyName",
                         "CompanyAddress",
                         "CompanyAddressCity",
                         "CompanyPhone",
                         "CompanyFax",
                         //6-10
                         "Shipper1",
                         "Shipper2",
                         "Shipper3",
                         "Shipper4",
                         "DocNo",
                         //11-15
                         "LocalDocNo",
                         "DocDt",
                         "PLDocNo",
                         "SpCtNotifyParty",
                         "SpCtNotifyParty2",
                         //16-20
                         "Account",
                         "InvoiceNo",
                         "SalesContractNo",
                         "LCNo",
                         "LcDt",
                         //21-25
                         "StfDt",
                         "PortLoading",
                         "PortDischarge",
                         "SPPlaceDelivery",
                         "Remark2",
                         //26-30
                         "SPName",
                         "SOLocal",
                         "PortName",
                         "PLLocal",
                         "CtPONo",
                        });
                    if (dr1.HasRows)
                    {
                        while (dr1.Read())
                        {
                            l2.Add(new SInvHdrMai()
                            {
                                CompanyLogo = Sm.DrStr(dr1, c1[0]),

                                CompanyName = Sm.DrStr(dr1, c1[1]),
                                CompanyAddress = Sm.DrStr(dr1, c1[2]),
                                CompanyAddressCity = Sm.DrStr(dr1, c1[3]),
                                CompanyPhone = Sm.DrStr(dr1, c1[4]),
                                CompanyFax = Sm.DrStr(dr1, c1[5]),

                                Shipper1 = Sm.DrStr(dr1, c1[6]),
                                Shipper2 = Sm.DrStr(dr1, c1[7]),
                                Shipper3 = Sm.DrStr(dr1, c1[8]),
                                Shipper4 = Sm.DrStr(dr1, c1[9]),
                                DocNo = Sm.DrStr(dr1, c1[10]),

                                LocalDocNo = Sm.DrStr(dr1, c1[11]),
                                DocDt = Sm.DrStr(dr1, c1[12]),
                                PLDocNo = Sm.DrStr(dr1, c1[13]),
                                SpCtNotifyParty = Sm.DrStr(dr1, c1[14]),
                                SpCtNotifyParty2 = Sm.DrStr(dr1, c1[15]),

                                Account = Sm.DrStr(dr1, c1[16]),
                                InvoiceNo = Sm.DrStr(dr1, c1[17]),
                                SalesContractNo = Sm.DrStr(dr1, c1[18]),
                                LCNo = Sm.DrStr(dr1, c1[19]),
                                LcDt = Sm.DrStr(dr1, c1[20]),

                                StfDt = Sm.DrStr(dr1, c1[21]),
                                PortLoading = Sm.DrStr(dr1, c1[22]),
                                PortDischarge = Sm.DrStr(dr1, c1[23]),
                                SPPlaceDelivery = Sm.DrStr(dr1, c1[24]),
                                Note = Sm.DrStr(dr1, c1[25]),

                                SPName = Sm.DrStr(dr1, c1[26]),
                                SOLocal = Sm.DrStr(dr1, c1[27]),
                                PortName = Sm.DrStr(dr1, c1[28]),
                                PLLocal = Sm.DrStr(dr1, c1[29]),
                                CtPONo = Sm.DrStr(dr1, c1[30]),

                                PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
                            });
                        }
                    }
                    dr1.Close();
                }
                myLists.Add(l2);
                #endregion

                #region Detail4
                var cmDtl4 = new MySqlCommand();


                var SQLDtl4 = new StringBuilder();
                using (var cnDtl4 = new MySqlConnection(Gv.ConnectionString))
                {
                    string thc = Sm.GetValue("Select Concat(Right(OptDesc, 1),' ', 'HC') From TblOption Where OptCat = 'ContainerSize' Limit 1;");

                    cnDtl4.Open();
                    cmDtl4.Connection = cnDtl4;

                    SQLDtl4.AppendLine("Select X.DocNo, A2.SectionNo, A2.ItCode, ");
                    //D.ForeignName As ItName, ");

                    if (mIsCustomerItemNameMandatory)
                        SQLDtl4.AppendLine("IfNull(N.CtItName, D.ItName) As ItName, ");
                    else
                        SQLDtl4.AppendLine("D.ItName, ");
                    SQLDtl4.AppendLine("A2.Qty, J.PriceUomCode, Round(A2.QtyInventory, 4) As QtyInventory,");
                    SQLDtl4.AppendLine("D.InventoryUomCode, A2.PLNo, ifnull(A2.QtyPL, 0) As QtyPL, Round(if(S.NWRate =0, (A2.QtyPackagingUnit * L.NW), S.NWRate),2) As NW, ");
                    SQLDtl4.AppendLine("(Select Parvalue From Tblparameter Where parCode = 'SIWeightUom') As NGuom, Round(if(S.GWRate =0, (A2.QtyPackagingUnit * L.GW), S.GWRate),2) As GW, I.UserName,");
                    SQLDtl4.AppendLine("J.CurCode, G.UPrice, Round((G.UPrice * Round(A2.Qty, 4)), 2) As Amount,");
                    SQLDtl4.AppendLine("if(K.Qty =0, 0, if(J.PriceUomCode = D.SalesUomCode2, A2.Qty, (K.Qty2*A2.Qty)/K.Qty)) As Qty2, ");
                    SQLDtl4.AppendLine("D.HSCode As SPHSCOde, M.ItGrpName, D.ItCodeInternal, N.CtItCode, O.DTNAme, P.OptDesc As Material, ");
                    SQLDtl4.AppendLine("Q.OptDesc As Colour, R.PtName As Top, E.PackagingUnitUomCode, (A2.Qty /A2.QtyPackagingUnit) As QtyPcs, A2.QtyPackagingUnit  ");
                    SQLDtl4.AppendLine("From TblSInv X ");
                    SQLDtl4.AppendLine("Inner Join TblPlhdr A On A.DocNo = X.PlDocNo ");
                    SQLDtl4.AppendLine("Inner Join TblSIhdr B On A.SIDocNo = B.DocNo ");
                    SQLDtl4.AppendLine("Inner Join TblPlDtl A2 On A.DocNo = A2.DocNo ");
                    SQLDtl4.AppendLine("Inner Join TblItem D On A2.ItCode = D.ItCode ");
                    SQLDtl4.AppendLine("Inner Join TblSODtl E On A2.SoDocNo = E.DocNo And A2.SoDno = E.Dno ");
                    SQLDtl4.AppendLine("Inner Join TblSOHdr F On E.DocNo = F.DocNo And A2.SODocNo = F.DocNo ");
                    SQLDtl4.AppendLine("Inner Join TblCtQtDtl G On F.CtQtDocNo = G.DocNo And E.CtQtDNo = G.Dno ");
                    SQLDtl4.AppendLine("Inner Join TblCtQtHdr H On F.CtQtDocNo = H.DocNo ");
                    SQLDtl4.AppendLine("Left Join TblUser I On H.SpCode = I.UserCode ");
                    SQLDtl4.AppendLine("Inner Join TblItemPriceHdr J On G.ItemPriceDocNo = J.DocNo ");
                    SQLDtl4.AppendLine("Left Join TblItemPackagingUnit K On E.PackagingUnitUomCode = K.UomCOde And A2.ItCode = K.ItCode ");
                    SQLDtl4.AppendLine("left Join ");
                    SQLDtl4.AppendLine("( ");
                    SQLDtl4.AppendLine("  select ItCode, Qty, UomCode, Nw, GW From TblItempackagingunit ");
                    SQLDtl4.AppendLine("	 )L On L.ItCode = A2.ItCode And  E.PackagingUnitUomCode = L.UomCode ");
                    //SQLDtl4.AppendLine("Left Join (Select ParValue As THC From tblParameter Where ParCode = 'THC') Z On 0=0 ");
                    SQLDtl4.AppendLine("Left Join TblItemGroup M On D.ItGrpCode = M.ItGrpCode ");
                    SQLDtl4.AppendLine("Left Join TblCustomerItem N On D.ItCode=N.ItCode And F.CtCode=N.CtCode ");
                    SQLDtl4.AppendLine("Left Join TblDeliveryType O On H.ShpMCode=O.DTCode ");
                    SQLDtl4.AppendLine("Left Join TblOption P On P.OptCat='ItemInformation5' And D.Information5=P.OptCode ");
                    SQLDtl4.AppendLine("Left Join TblOption Q On Q.OptCat='ItemInformation1' And D.Information1=Q.OptCode ");
                    SQLDtl4.AppendLine("Left join TblPaymentTerm R On H.PtCode=R.PtCode ");
                    SQLDtl4.AppendLine("Inner Join TblSIDtl S On A.SiDocNo = S.DocNo And A2.SODocNo = S.SoDocNo And A2.SODno = S.SODno ");
                    SQLDtl4.AppendLine("Where X.DocNo=@DocNo ");
                    //  SQLDtl4.AppendLine("Order by  CntName ");

                    cmDtl4.CommandText = SQLDtl4.ToString();

                    Sm.CmParam<String>(ref cmDtl4, "@DocNo", TxtDocNo.Text);

                    var drDtl4 = cmDtl4.ExecuteReader();
                    var cDtl4 = Sm.GetOrdinal(drDtl4, new string[] 
                    {
                     //0
                     "DocNo" ,

                     //1-5
                     "SectionNo",
                     "ItCode" ,
                     "ItName",
                     "Qty",
                     "PriceUomCode",

                     //6-10
                     "QtyInventory",
                     "InventoryUomCode",
                     "PLNo",
                     "QtyPL",
                     "NW",

                     //11-15
                     "Nguom",
                     "GW",
                     "Username",
                     "CurCode",
                     "UPrice",

                     //16-20
                     "Amount",
                     "Qty2",
                     "SPHSCOde",
                     "ItGrpName",
                     "ItCodeInternal",

                     //21-25
                     "CtItCode",
                     "DTName",
                     "Material",
                     "Colour",
                     "Top",

                     //26
                     "PackagingUnitUomCode",
                     "QtyPcs",
                     "QtyPackagingUnit"


                    });
                    if (drDtl4.HasRows)
                    {
                        int nomor = 0;

                        while (drDtl4.Read())
                        {
                            nomor = nomor + 1;
                            ldtl4.Add(new SInvDtl4()
                            {
                                nomor=nomor,
                                DocNo = Sm.DrStr(drDtl4, cDtl4[0]),

                                SectionNo = Sm.DrStr(drDtl4, cDtl4[1]),
                                ItCode = Sm.DrStr(drDtl4, cDtl4[2]),
                                ItName = Sm.DrStr(drDtl4, cDtl4[3]),
                                Qty = Sm.DrDec(drDtl4, cDtl4[4]),
                                PriceUomCode = Sm.DrStr(drDtl4, cDtl4[5]),

                                QtyInventory = Sm.DrDec(drDtl4, cDtl4[6]),
                                InventoryUomCode = Sm.DrStr(drDtl4, cDtl4[7]),
                                PLNo = Sm.DrStr(drDtl4, cDtl4[8]),
                                QtyPL = Sm.DrDec(drDtl4, cDtl4[9]),
                                NW = Sm.DrDec(drDtl4, cDtl4[10]),

                                Nguom = Sm.DrStr(drDtl4, cDtl4[11]),
                                GW = Sm.DrDec(drDtl4, cDtl4[12]),
                                Username = Sm.DrStr(drDtl4, cDtl4[13]),
                                Currency = Sm.DrStr(drDtl4, cDtl4[14]),
                                Price = Sm.DrDec(drDtl4, cDtl4[15]),

                                Amount = Sm.DrDec(drDtl4, cDtl4[16]),
                                Qty2 = Sm.DrDec(drDtl4, cDtl4[17]),
                                SPHSCOde = Sm.DrStr(drDtl4, cDtl4[18]),
                                ItGrpName = Sm.DrStr(drDtl4, cDtl4[19]),

                                ItCodeInternal = Sm.DrStr(drDtl4, cDtl4[20]),
                                CtItCode = Sm.DrStr(drDtl4, cDtl4[21]),
                                DTName = Sm.DrStr(drDtl4, cDtl4[22]),
                                Material = Sm.DrStr(drDtl4, cDtl4[23]),
                                Colour = Sm.DrStr(drDtl4, cDtl4[24]),
                                Top = Sm.DrStr(drDtl4, cDtl4[25]),
                                PackagingUnitUomCode = Sm.DrStr(drDtl4, cDtl4[26]),
                                QtyPcs = Sm.DrDec(drDtl4, cDtl4[27]),
                                QtyPackagingUnit = Sm.DrDec(drDtl4, cDtl4[28]),


                            });
                        }
                        // nomor = 0;
                    }
                    drDtl4.Close();
                }
                myLists.Add(ldtl4);
                #endregion

                #region Detail5
                var cmDtl5 = new MySqlCommand();


                var SQLDtl5 = new StringBuilder();
                using (var cnDtl5 = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl5.Open();
                    cmDtl5.Connection = cnDtl5;

                    SQLDtl5.AppendLine("Select X2.DocNo, Sum(X2.Amount)As Amount From ( ");
                    SQLDtl5.AppendLine("Select X.DocNo, Round((G.UPrice * Round(A2.Qty, 4)), 2) As Amount");
                    SQLDtl5.AppendLine("From TblSInv X ");
                    SQLDtl5.AppendLine("Inner Join TblPlhdr A On A.DocNo = X.PlDocNo ");
                    SQLDtl5.AppendLine("Inner Join TblSIhdr B On A.SIDocNo = B.DocNo ");
                    SQLDtl5.AppendLine("Inner Join TblPlDtl A2 On A.DocNo = A2.DocNo ");
                    SQLDtl5.AppendLine("Inner Join TblItem D On A2.ItCode = D.ItCode ");
                    SQLDtl5.AppendLine("Inner Join TblSODtl E On A2.SoDocNo = E.DocNo And A2.SoDno = E.Dno ");
                    SQLDtl5.AppendLine("Inner Join TblSOHdr F On E.DocNo = F.DocNo And A2.SODocNo = F.DocNo ");
                    SQLDtl5.AppendLine("Inner Join TblCtQtDtl G On F.CtQtDocNo = G.DocNo And E.CtQtDNo = G.Dno ");
                    SQLDtl5.AppendLine("Inner Join TblCtQtHdr H On F.CtQtDocNo = H.DocNo ");
                    SQLDtl5.AppendLine("Inner Join TblUser I On H.SpCode = I.UserCode ");
                    SQLDtl5.AppendLine("Inner Join TblItemPriceHdr J On G.ItemPriceDocNo = J.DocNo ");
                    SQLDtl5.AppendLine("Left Join TblItemPackagingUnit K On E.PackagingUnitUomCode = K.UomCOde And A2.ItCode = K.ItCode ");
                    SQLDtl5.AppendLine("left Join ");
                    SQLDtl5.AppendLine("( ");
                    SQLDtl5.AppendLine("  select ItCode, UomCode, Nw, GW From TblItempackagingunit ");
                    SQLDtl5.AppendLine("	 )L On L.ItCode = A2.ItCode And  E.PackagingUnitUomCode = L.UomCode ");
                    SQLDtl5.AppendLine("Left Join (Select ParValue As THC From tblParameter Where ParCode = 'THC') Z On 0=0 ");
                    SQLDtl5.AppendLine("Where X.DocNo=@DocNo ");
                    SQLDtl5.AppendLine(")X2");
                    SQLDtl5.AppendLine("Group by X2.DocNo ");

                    cmDtl5.CommandText = SQLDtl5.ToString();

                    Sm.CmParam<String>(ref cmDtl5, "@DocNo", TxtDocNo.Text);

                    var drDtl5 = cmDtl5.ExecuteReader();
                    var cDtl5 = Sm.GetOrdinal(drDtl5, new string[] 
                    {
                     //0
                     "DocNo" ,
                     "Amount",
                    });
                    if (drDtl5.HasRows)
                    {
                        while (drDtl5.Read())
                        {
                            ldtl5.Add(new SInvDtl5()
                            {
                                DocNo = Sm.DrStr(drDtl5, cDtl5[0]),
                                Amount = Sm.DrDec(drDtl5, cDtl5[1]),
                                Terbilang = Convert(Sm.DrDec(drDtl5, cDtl5[1])),

                            });
                        }
                    }
                    drDtl5.Close();
                }
                myLists.Add(ldtl5);
                #endregion

                #region Detail6
                var cmDtl6 = new MySqlCommand();

                var SQLDtl6 = new StringBuilder();
                using (var cnDtl6 = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl6.Open();
                    cmDtl6.Connection = cnDtl6;

                    SQLDtl6.AppendLine("Select X2.DocNo, SUM(X2.Amount) As Amount, Round(X2.Additional,2)As Additional, Round(X2.Disc,2)As Disc, ");
                    SQLDtl6.AppendLine("SUM(X2.Amount)+Round(X2.Additional,2)-Round(X2.Disc,2)As GrandTot From ( ");
                    SQLDtl6.AppendLine("	Select X.DocNo, Round(F.UPrice * Round(B.Qty, 4), 2)As Amount, Ifnull(I.Additional,0) As Additional, Ifnull(J.Disc,0) As Disc ");
                    SQLDtl6.AppendLine("	From TblSInv X ");
                    SQLDtl6.AppendLine("	Inner Join TblPlhdr A On X.PLDocNo = A.DocNo ");
                    SQLDtl6.AppendLine("	Inner Join TblPlDtl B On A.DocNo = B.DocNo ");
                    SQLDtl6.AppendLine("	Inner Join TblSODtl C On B.SoDocNo = C.DocNo And B.SoDno = C.Dno ");
                    SQLDtl6.AppendLine("	Inner Join TblSOHdr D On C.DocNo = D.DocNo And B.SODocNo = D.DocNo ");
                    SQLDtl6.AppendLine("	Inner Join TblCtQtDtl F On D.CtQtDocNo = F.DocNo And C.CtQtDNo = F.Dno ");
                    SQLDtl6.AppendLine("	Left Join TblDOCt2Hdr H On A.DocNo=H.PLDocNo ");
                    SQLDtl6.AppendLine("	Left Join ");
                    SQLDtl6.AppendLine("	( ");
                    SQLDtl6.AppendLine("	select Distinct T.DocNo As SIDocNo, T1.DoctDocNo, T.Additional from ");
                    SQLDtl6.AppendLine("	( ");
                    SQLDtl6.AppendLine("		Select A.Docno, sum(if(Damt=0, Camt, Damt)) As Additional ");
                    SQLDtl6.AppendLine("		From TblSalesInvoiceHdr A ");
                    SQLDtl6.AppendLine("		left Join TblsalesInvoiceDtl2 C On A.DocNo = C.DocNo ");
                    SQLDtl6.AppendLine("		Where A.CancelInd = 'N' And OptAcDesc = '1' "); //-- AcInd = 'Y' And 
                    SQLDtl6.AppendLine("		Group By  A.DocNo ");
                    SQLDtl6.AppendLine("		)T ");
                    SQLDtl6.AppendLine("	Inner Join TblSalesInvoicedtl T1 On T.DocNo=T1.Docno ");
                    SQLDtl6.AppendLine("	)I On H.DocNo = I.DOCtDocNo ");
                    SQLDtl6.AppendLine("	Left Join ");
                    SQLDtl6.AppendLine("	( ");
                    SQLDtl6.AppendLine("		select Distinct T2.DocNo As SIDocNo, T3.DoctDocNo, T2.Disc from ");
                    SQLDtl6.AppendLine("		( ");
                    SQLDtl6.AppendLine("			Select A.Docno, sum(if(Damt=0, Camt, Damt)) As Disc ");
                    SQLDtl6.AppendLine("			From TblSalesInvoiceHdr A ");
                    SQLDtl6.AppendLine("			left Join TblsalesInvoiceDtl2 C On A.DocNo = C.DocNo ");
                    SQLDtl6.AppendLine("			Where A.CancelInd = 'N' And OptAcDesc = '2' "); //-- AcInd = 'Y' And 
                    SQLDtl6.AppendLine("			Group By  A.DocNo ");
                    SQLDtl6.AppendLine("		)T2 ");
                    SQLDtl6.AppendLine("		Inner Join TblSalesInvoicedtl T3 On T2.DocNo=T3.Docno	");
                    SQLDtl6.AppendLine("	)J On H.DocNo = J.DOCtDocNo ");
                    SQLDtl6.AppendLine("	 Where X.DocNo=@DocNo ");
                    SQLDtl6.AppendLine(")X2 ");
                    SQLDtl6.AppendLine("Group by X2.DocNo ");

                    cmDtl6.CommandText = SQLDtl6.ToString();

                    Sm.CmParam<String>(ref cmDtl6, "@DocNo", TxtDocNo.Text);

                    var drDtl6 = cmDtl6.ExecuteReader();
                    var cDtl6 = Sm.GetOrdinal(drDtl6, new string[] 
                    {
                      //0
                     "DocNo",

                     //1-4
                     "Amount", "Additional", "Disc", "GrandTot"
                    });
                    if (drDtl6.HasRows)
                    {
                        while (drDtl6.Read())
                        {
                            ldtl6.Add(new SInvDtl6()
                            {
                                DocNo = Sm.DrStr(drDtl6, cDtl6[0]),
                                Amount = Sm.DrDec(drDtl6, cDtl6[1]),
                                Additional = Sm.DrStr(drDtl6, cDtl6[2]),
                                Disc = Sm.DrStr(drDtl6, cDtl6[3]),
                                GrandTot = Sm.DrDec(drDtl6, cDtl6[4]),
                                Terbilang = Convert(Sm.DrDec(drDtl6, cDtl6[4])),
                            });
                        }
                    }
                    drDtl6.Close();
                }
                myLists.Add(ldtl6);
                #endregion

                
                #endregion

                if (ChkFOB.Checked == false)
                {
                    Sm.PrintReport(mIsFormPrintOutSInv, myLists, TableName, false);
                }
                else
                {
                    Sm.PrintReport("SInv2", myLists, TableName, false);
                }

            }

        }


        #region Convert To Words

        private static string[] _ones =
            {
                "Zero",
                "One",
                "Two",
                "Three",
                "Four",
                "Five",
                "Six",
                "Seven",
                "Eight",
                "Nine"
            };

        private static string[] _teens =
            {
                "Ten",
                "Eleven",
                "Twelve",
                "Thirteen",
                "Fourteen",
                "Fifteen",
                "Sixteen",
                "Seventeen",
                "Eighteen",
                "Nineteen"
            };

        private static string[] _tens =
            {
                "",
                "Ten",
                "Twenty",
                "Thirty",
                "Forty",
                "Fifty",
                "Sixty",
                "Seventy",
                "Eighty",
                "Ninety"
            };

        // US Nnumbering:
        private static string[] _thousands =
            {
                "",
                "Thousand",
                "Million",
                "Billion",
                "Trillion",
                "Quadrillion"
            };



        private static string Convert(decimal value)
        {
            string digits, temp;
            bool showThousands = false;
            bool allZeros = true;

            StringBuilder builder = new StringBuilder();
            digits = ((long)value).ToString();
            for (int i = digits.Length - 1; i >= 0; i--)
            {
                int ndigit = (int)(digits[i] - '0');
                int column = (digits.Length - (i + 1));

                switch (column % 3)
                {
                    case 0:        // Ones position
                        showThousands = true;
                        if (i == 0)
                        {
                            temp = String.Format("{0} ", _ones[ndigit]);
                        }
                        else if (digits[i - 1] == '1')
                        {
                            temp = String.Format("{0} ", _teens[ndigit]);
                            i--;
                        }
                        else if (ndigit != 0)
                        {
                            temp = String.Format("{0} ", _ones[ndigit]);
                        }
                        else
                        {
                            temp = String.Empty;
                            if (digits[i - 1] != '0' || (i > 1 && digits[i - 2] != '0'))
                                showThousands = true;
                            else
                                showThousands = false;
                        }

                        if (showThousands)
                        {
                            if (column > 0)
                            {
                                temp = String.Format("{0}{1}{2}",
                                    temp,
                                    _thousands[column / 3],
                                    allZeros ? " " : " ");
                            }
                            allZeros = false;
                        }
                        builder.Insert(0, temp);
                        break;

                    case 1:        // Tens column
                        if (ndigit > 0)
                        {
                            temp = String.Format("{0}{1}",
                                _tens[ndigit],
                                (digits[i + 1] != '0') ? " " : " ");
                            builder.Insert(0, temp);
                        }
                        break;

                    case 2:        // Hundreds column
                        if (ndigit > 0)
                        {
                            temp = String.Format("{0} Hundred ", _ones[ndigit]);
                            builder.Insert(0, temp);
                        }
                        break;
                }
            }

            string cents = value.ToString();
            decimal cettt = Decimal.Parse(cents.Substring(cents.Length - 2, 2));
            string cent = Sm.Terbilang2(cettt);
            builder.AppendFormat("Dollars And " + cent + " Cents # ", (value - (long)value) * 100);

            return String.Format("{0}{1} ",
                Char.ToUpper(builder[0]),
                builder.ToString(1, builder.Length - 1));
        }
        #endregion

        private void DownloadFileKu(string TxtFile)
        {
            SFD1.FileName = TxtFile;
            SFD1.DefaultExt = "pdf";
            SFD1.AddExtension = true;
            if (SFD1.ShowDialog() == DialogResult.OK)
            {
                DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, TxtFile, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
                Application.DoEvents();

                //Write the bytes to a file
                FileStream newFile = new FileStream(SFD1.FileName, FileMode.Create);
                newFile.Write(downloadedData, 0, downloadedData.Length);
                newFile.Close();
                MessageBox.Show("Saved Successfully");
            }
            else
                MessageBox.Show("No File was Downloaded Yet!");
        }

        private void DownloadFile(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                //this.Text = string.Concat(this.Text, "         ", "Connecting...");
                Application.DoEvents();

                if (mFormatFTPClient == "1")
                {
                    #region type 1
                    FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                    //this.Text = string.Concat(this.Text, "         ", "Retrieving Information...");
                    Application.DoEvents();

                    //Get the file size first (for progress bar)
                    request.Method = WebRequestMethods.Ftp.GetFileSize;
                    request.Credentials = new NetworkCredential(username, password);
                    request.UsePassive = true;
                    request.UseBinary = true;
                    request.KeepAlive = true;

                    int dataLength = (int)request.GetResponse().ContentLength;

                    //this.Text = string.Concat(this.Text, "         ", "Downloading File...");
                    Application.DoEvents();

                    //Now get the actual data
                    request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                    request.Method = WebRequestMethods.Ftp.DownloadFile;
                    request.Credentials = new NetworkCredential(username, password);
                    request.UsePassive = true;
                    request.UseBinary = true;
                    request.KeepAlive = false;

                    //Streams
                    FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                    Stream reader = response.GetResponseStream();

                    //Download to memory
                    MemoryStream memStream = new MemoryStream();
                    byte[] buffer = new byte[1024];

                    while (true)
                    {
                        Application.DoEvents();

                        int bytesRead = reader.Read(buffer, 0, buffer.Length);

                        if (bytesRead == 0)
                        {
                            Application.DoEvents();
                            break;
                        }
                        else
                        {
                            memStream.Write(buffer, 0, bytesRead);
                        }
                    }

                    downloadedData = memStream.ToArray();

                    reader.Close();
                    memStream.Close();
                    response.Close();
                    #endregion
                }
                else
                {
                    #region  type 2
                    FtpWebRequest request = FtpWebRequest.Create("ftp://" + FTPAddress + "/" + filename) as FtpWebRequest;
                    //this.Text = string.Concat(this.Text, "         ", "Retrieving Information...");
                    Application.DoEvents();

                    //Get the file size first (for progress bar)
                    request.Method = WebRequestMethods.Ftp.GetFileSize;
                    request.Credentials = new NetworkCredential(username, password);
                    request.UsePassive = true;
                    request.UseBinary = true;
                    request.KeepAlive = true;

                    int dataLength = (int)request.GetResponse().ContentLength;

                    //this.Text = string.Concat(this.Text, "         ", "Downloading File...");
                    Application.DoEvents();

                    //Now get the actual data
                    request = FtpWebRequest.Create("ftp://" + FTPAddress + "/" + filename) as FtpWebRequest;
                    request.Method = WebRequestMethods.Ftp.DownloadFile;
                    request.Credentials = new NetworkCredential(username, password);
                    request.UsePassive = true;
                    request.UseBinary = true;
                    request.KeepAlive = false;

                    //Streams
                    FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                    Stream reader = response.GetResponseStream();

                    //Download to memory
                    MemoryStream memStream = new MemoryStream();
                    byte[] buffer = new byte[1024];

                    while (true)
                    {
                        Application.DoEvents();

                        int bytesRead = reader.Read(buffer, 0, buffer.Length);

                        if (bytesRead == 0)
                        {
                            Application.DoEvents();
                            break;
                        }
                        else
                        {
                            memStream.Write(buffer, 0, bytesRead);
                        }
                    }

                    downloadedData = memStream.ToArray();

                    reader.Close();
                    memStream.Close();
                    response.Close();
                    #endregion
                }
            }
            catch (Exception exc)
            {
                Sm.StdMsg(mMsgType.Warning, exc.ToString());
            }
        }


        #endregion

        #endregion

        #region Event

        private void BtnSI_Click_1(object sender, EventArgs e)
        {
            if (!Sm.IsLueEmpty(LueCtCode, "Customer"))
            {
                Sm.FormShowDialog(new FrmSInv2Dlg(this, Sm.GetLue(LueCtCode)));
            }
            if (TxtPLDocNo.Text.Length != 0)
            {
                TxtLocalDocNo.EditValue = mIsNotCopySalesLocalDocNo ? "" : Sm.GetValue("Select LocalDocNo From TblPLhdr Where DocNo='" + TxtPLDocNo.Text + "' ");
            }
        }

        private void LueCtCode_EditValueChanged_1(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(SetLueCtCode));

            ClearGrd(new List<iGrid>(){
                Grd11, Grd12, Grd13, Grd14, Grd15,
                Grd16, Grd17, Grd18, Grd19, Grd20,
                Grd21, Grd22, Grd23, Grd24, Grd25, Grd1
            });

            if (Sm.GetLue(LueCtCode).Length == 0)
            {
                LueNotify.EditValue = null;
                Sm.SetControlReadOnly(LueNotify, true);
            }
            else
            {
                SetLueNotify(ref LueNotify, Sm.GetLue(LueCtCode));
                Sm.SetControlReadOnly(LueNotify, false);
            }
        }

        private void LueNotify_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueNotify, new Sm.RefreshLue2(SetLueNotify), Sm.GetLue(LueCtCode));
        }

        private void BtnSI2_Click(object sender, EventArgs e)
        {
            if (TxtPLDocNo.Text.Length != 0)
            {
                var f1 = new FrmPL(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = TxtPLDocNo.Text;
                f1.ShowDialog();
            }
        }

        private void TxtFreight1_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtFreight1, 0);
            //ComputeTotal(Grd11, TxtTotal1);
            ComputeFOB(Grd11, TxtFreight1, TxtTotal1, "Size1");
        }

        private void TxtFreight2_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtFreight2, 0);
            //ComputeTotal(Grd12, TxtTotal2);
            ComputeFOB(Grd12, TxtFreight2, TxtTotal2, "Size2");
        }

        private void TxtFreight3_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtFreight3, 0);
            ComputeFOB(Grd13, TxtFreight3, TxtTotal3, "Size3");
        }

        private void TxtFreight4_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtFreight4, 0);
            ComputeFOB(Grd14, TxtFreight4, TxtTotal4, "Size4");
        }

        private void TxtFreight5_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtFreight5, 0);
            ComputeFOB(Grd15, TxtFreight5, TxtTotal5, "Size5");
        }

        private void TxtFreight6_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtFreight6, 0);
            ComputeFOB(Grd16, TxtFreight6, TxtTotal6, "Size6");
        }

        private void TxtFreight7_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtFreight7, 0);
            ComputeFOB(Grd17, TxtFreight7, TxtTotal7, "Size7");
        }

        private void TxtFreight8_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtFreight8, 0);
            ComputeFOB(Grd18, TxtFreight8, TxtTotal8, "Size8");
        }

        private void TxtFreight9_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtFreight9, 0);
            ComputeFOB(Grd19, TxtFreight9, TxtTotal9, "Size9");
        }

        private void TxtFreight10_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtFreight10, 0);
            ComputeFOB(Grd20, TxtFreight10, TxtTotal10, "Size10");
        }

        private void TxtFreight11_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtFreight11, 0);
            ComputeFOB(Grd21, TxtFreight11, TxtTotal11, "Size11");
        }

        private void TxtFreight12_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtFreight12, 0);
            ComputeFOB(Grd22, TxtFreight12, TxtTotal12, "Size12");
        }

        private void TxtFreight13_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtFreight13, 0);
            ComputeFOB(Grd23, TxtFreight13, TxtTotal13, "Size13");
        }

        private void TxtFreight14_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtFreight14, 0);
            ComputeFOB(Grd24, TxtFreight14, TxtTotal14, "Size14");
        }

        private void TxtFreight15_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtFreight15, 0);
            ComputeFOB(Grd25, TxtFreight15, TxtTotal15, "Size15");
        }

        private void TxtFreight16_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtFreight16, 0);
            ComputeFOB(Grd26, TxtFreight16, TxtTotal16, "Size16");
        }

        private void TxtFreight17_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtFreight17, 0);
            ComputeFOB(Grd27, TxtFreight17, TxtTotal17, "Size17");
        }

        private void TxtFreight18_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtFreight18, 0);
            ComputeFOB(Grd28, TxtFreight18, TxtTotal18, "Size18");
        }

        private void TxtFreight19_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtFreight19, 0);
            ComputeFOB(Grd29, TxtFreight19, TxtTotal19, "Size19");
        }

        private void TxtFreight20_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtFreight20, 0);
            ComputeFOB(Grd30, TxtFreight20, TxtTotal20, "Size20");
        }

        private void TxtFreight21_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtFreight21, 0);
            ComputeFOB(Grd31, TxtFreight21, TxtTotal21, "Size21");
        }

        private void TxtFreight22_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtFreight22, 0);
            ComputeFOB(Grd32, TxtFreight22, TxtTotal22, "Size22");
        }

        private void TxtFreight23_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtFreight23, 0);
            ComputeFOB(Grd33, TxtFreight23, TxtTotal23, "Size23");
        }

        private void TxtFreight24_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtFreight24, 0);
            ComputeFOB(Grd34, TxtFreight24, TxtTotal24, "Size24");
        }

        private void TxtFreight25_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtFreight25, 0);
            ComputeFOB(Grd35, TxtFreight25, TxtTotal25, "Size25");
        }

        private void BtnDownload_Click(object sender, EventArgs e)
        {
            List<string> FileName = new List<string>();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.LocalDocNo, A.FileName1, A.FileName2, A.FileName3, A.FileName4, A.FileName5, ");
            SQL.AppendLine("A.FileName6, A.FileName7, A.FileName8, A.FileName9, A.FileName10, ");
            SQL.AppendLine("A.FileName11, A.FileName12, A.FileName13, A.FileName14, A.FileName15, ");
            SQL.AppendLine("A.FileName16, A.FileName17, A.FileName18, A.FileName19, A.FileName20 ");
            SQL.AppendLine("From TblAttachmentFile A ");
            SQL.AppendLine("Where A.LocalDocNo=@LocalDocNo;");

            Sm.CmParam<String>(ref cm, "@localDocNo", TxtLocalDocNo.Text);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "FileName1", 
                        //1-5
                        "FileName2", "FileName3", "FileName4", "FileName5", "FileName6",  
                        //6-10
                        "FileName7", "FileName8", "FileName9", "FileName10", "FileName11", 
                        //11-15
                        "FileName12", "FileName13", "FileName14", "FileName15", "FileName16", 
                        //16-19
                        "FileName17", "FileName18", "FileName19", "FileName20"                         
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        if (Sm.DrStr(dr, c[0]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[0]));
                        if (Sm.DrStr(dr, c[1]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[1]));
                        if (Sm.DrStr(dr, c[2]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[2]));
                        if (Sm.DrStr(dr, c[3]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[3]));
                        if (Sm.DrStr(dr, c[4]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[4]));
                        if (Sm.DrStr(dr, c[5]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[5]));
                        if (Sm.DrStr(dr, c[6]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[6]));
                        if (Sm.DrStr(dr, c[7]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[7]));
                        if (Sm.DrStr(dr, c[8]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[8]));
                        if (Sm.DrStr(dr, c[9]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[9]));
                        if (Sm.DrStr(dr, c[10]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[10]));
                        if (Sm.DrStr(dr, c[11]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[11]));
                        if (Sm.DrStr(dr, c[12]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[12]));
                        if (Sm.DrStr(dr, c[13]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[13]));
                        if (Sm.DrStr(dr, c[14]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[14]));
                        if (Sm.DrStr(dr, c[15]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[15]));
                        if (Sm.DrStr(dr, c[16]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[16]));
                        if (Sm.DrStr(dr, c[17]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[17]));
                        if (Sm.DrStr(dr, c[18]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[18]));
                        if (Sm.DrStr(dr, c[19]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[19]));
                    }, false
                );
            if (FileName.Count > 0)
            {
                for (int i = 0; i < FileName.Count; i++)
                {
                    DownloadFileKu(FileName[i]);
                }
            }
            else
            {
                Sm.StdMsg(mMsgType.Warning, "No Attachment File");
            }
        }

        private void BtnTemplate_Click(object sender, EventArgs e)
        {
            if (!Sm.IsLueEmpty(LueCtCode, "Customer"))
            {
                Sm.FormShowDialog(new FrmSInv2Dlg2(this));
            }
        }

        #endregion        

        #region Report Class

        private class SInvHdr
        {
            public string CompanyLogo { set; get; }
            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyAddressCity { get; set; }
            public string CompanyPhone { get; set; }
            public string CompanyFax { get; set; }
            public string DocNo { get; set; }
            public string LocalDocNo { get; set; }
            public string DocDt { get; set; }
            public string Account { get; set; }
            public string NotifyParty { get; set; }
            public string PL { get; set; }
            public decimal Freight { get; set; }
            public string SalesContract { get; set; }
            public string LCNo { get; set; }
            public string LCDate { get; set; }
            public string Issued1 { get; set; }
            public string Issued2 { get; set; }
            public string Issued3 { get; set; }
            public string Issued4 { get; set; }
            public string ToOrder1 { get; set; }
            public string ToOrder2 { get; set; }
            public string ToOrder3 { get; set; }
            public string ToOrder4 { get; set; }
            public string PortLoading { get; set; }
            public string PortDischarge { get; set; }
            public string Stufing { get; set; }
            public string Remark { get; set; }
            public string RemarkInv { get; set; }
            public string Note { get; set; }
            public string FOB { get; set; }
            public string SpName { get; set; }
            public string SM { get; set; }
            public string PrintBy { get; set; }
        }

        private class SInvDtl
        {
            public string Cnt { get; set; }
            public string ItCode { get; set; }
            public string ItName { get; set; }
            public string PLNo { get; set; }
            public decimal QtyPL { get; set; }
            public decimal Qty { get; set; }
            public decimal Qty2 { get; set; }
            public decimal QtyInventory { get; set; }
            public decimal NW { get; set; }
            public decimal GW { get; set; }
            public string Username { get; set; }
            public decimal SectionNo { get; set; }
            public string Currency { get; set; }
            public decimal Price { get; set; }
            public decimal Amount { get; set; }
            public decimal Freight { get; set; }
            public string Remark { get; set; }
            public string Dno { get; set; }
        }

        private class SInvDtl2
        {
            public decimal Amount { get; set; }
            public string Terbilang { get; set; }
            public decimal Totally { get; set; }
            public decimal TotalFre { get; set; }
        }

        private class SInvDtl3
        {
            public string Jumlah { get; set; }
            public string Size { get; set; }
        }

        private class SInvHdrMai
        {
            public string CompanyLogo { set; get; }
            public string CompanyName { get; set; }
            public string CompanyAddressCity { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyPhone { get; set; }
            public string CompanyFax { get; set; }
            public string Shipper1 { get; set; }
            public string Shipper2 { get; set; }
            public string Shipper3 { get; set; }
            public string Shipper4 { get; set; }
            public string DocNo { get; set; }
            public string LocalDocNo { get; set; }
            public string DocDt { get; set; }
            public string PLDocNo { get; set; }
            public string SPBLNo { get; set; }
            public string SpCtNotifyParty { get; set; }
            public string SpCtNotifyParty2 { get; set; }
            public string Account { get; set; }
            public string InvoiceNo { get; set; }
            public string SalesContractNo { get; set; }
            public string LCNo { get; set; }
            public string LcDt { get; set; }
            public string StfDt { get; set; }
            public string PortLoading { get; set; }
            public string PortDischarge { get; set; }
            public string SPPlaceDelivery { get; set; }
            public string Note { get; set; }
            public string FOB { get; set; }
            public string SPName { get; set; }
            public string SOLocal { get; set; }
            public decimal Freight { get; set; }
            public string PrintBy { get; set; }
            public string PortName { get; set; }
            public string PLLocal { get; set; }
            public string CtPONo { get; set; }

        }

        private class SInvDtl4
        {
            public int nomor { get; set; }
            public string DocNo { get; set; }

            public string ItCode { get; set; }
            public string ItName { get; set; }
            public decimal Qty { get; set; }
            public string PriceUomCode { get; set; }
            public string PLNo { get; set; }

            public decimal QtyPL { get; set; }
            public decimal Qty2 { get; set; }
            public decimal QtyInventory { get; set; }
            public string InventoryUomCode { get; set; }
            public decimal NW { get; set; }

            public string Nguom { get; set; }
            public decimal GW { get; set; }
            public string Username { get; set; }
            public string SectionNo { get; set; }
            public string Currency { get; set; }

            public decimal Price { get; set; }
            public decimal Amount { get; set; }
            public string SPHSCOde { get; set; }
            public string ItGrpName { get; set; }

            public string ItCodeInternal { get; set; }
            public string CtItCode { get; set; }
            public string DTName { get; set; }
            public string Material { get; set; }
            public string Colour { get; set; }
            public string Top { get; set; }
            public string PackagingUnitUomCode { get; set; }
            public decimal QtyPackagingUnit { get; set; }
            public decimal QtyPcs { get; set; }
        }

        private class SInvDtl5
        {
            public string DocNo { get; set; }
            public decimal Amount { get; set; }
            public string Terbilang { get; set; }

        }

        private class SInvDtl6
        {
            public string DocNo { get; set; }
            public decimal Amount { get; set; }
            public string Additional { get; set; }
            public string Disc { get; set; }
            public decimal GrandTot { get; set; }
            public string Terbilang { get; set; }
        }


        #endregion

    }
}
