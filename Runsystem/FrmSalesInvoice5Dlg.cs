﻿#region Update
/*
    21/08/2018 [WED] ambil dari project implementation yang sudah approved
    16/10/2018 [DITA] ketentuan settled di project delivery bisa masuk ke sales invoice for project
    27/11/2018 [MEY] Penambahan nama project di sales invoice for project berdasarkan customer dan project implementation yang sudah di settle.
    17/12/2018 [WED] bobot percentage
    11/09/2019 [WED] ubah alur ke SOContractRevision
    13/09/2019 [TKG] filter Otorisasi group thd site
    25/09/2019 [WED/YK] ambil Customer nya dari LOP
    08/05/2020 [DITA/YK] ubah amount ke amount2
    23/09/2020 [WED/IMS] tambah amtBOM agar bisa ambil nilai dari SO Contract Amount IMS
    23/10/2020 [DITA/VIR] based on param IsSLIForProjectTotalAmountBasedOnEstimatedPrice, amount ambil dari estimated price PRJI Detail
    02/03/2023 [MYA/MNET] Penyesuaian menu Sales Invoice for Project ( dampak frm baru prji + jurnal yang terbentuk+reprocess+jurnal after approve)
    04/03/2023 [WED/MNET] query PRJI masih ambil dari dtl pertama, harusnya dtl5 berdasarkan parameter IsWBS2SummarizedWBS1
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmSalesInvoice5Dlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmSalesInvoice5 mFrmParent;
        string mSQL = string.Empty, mCtCode = string.Empty, mProjectName = string.Empty,mMenuCode="XXX";

        #endregion

        #region Constructor

        public FrmSalesInvoice5Dlg(FrmSalesInvoice5 FrmParent, string CtCode, string ProjectName)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mCtCode = CtCode;
            mProjectName = ProjectName;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -30);
                Sl.SetLueProjectTaskCode(ref LueTaskCode);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DNo, D.StageName, E.TaskName, A.BobotPercentage,  ");
            if (mFrmParent.mIsSLIForProjectTotalAmountBasedOnEstimatedPrice)
                SQL.AppendLine("A.EstimatedAmt as Amt ");
            else
                SQL.AppendLine("((A.BobotPercentage * 0.01 * C.Amt2) + (A.BobotPercentage * 0.01 * C.AmtBOM)) as Amt ");
            SQL.AppendLine("From " + mFrmParent.TblProjectImplementationDtl + " A ");
            SQL.AppendLine("Inner Join TblProjectImplementationHdr B On A.DocNo = B.DocNo  ");
            SQL.AppendLine("    And B.CancelInd = 'N' And B.Status = 'A' And A.SettledInd = 'Y' And A.InvoicedInd = 'N' ");
            if (mProjectName.Length > 0)
            {
                SQL.AppendLine(" And A.DocNo = @ProjectName ");
            }
            if (mFrmParent.mIsStageShouldBeSettled)
            {
                SQL.AppendLine("And Not Exists (Select 1 From " + mFrmParent.TblProjectImplementationDtl + " T   ");
                SQL.AppendLine("Where T.SettledInd = 'N' and T.InvoicedInd = 'N' and T.StageCode=A.StageCode ");
                SQL.AppendLine("And T.DocNo = A.DocNo) ");
            }
            SQL.AppendLine("INner Join TblSOContractRevisionHdr B1 On B.SOContractDocno = B1.DocNo ");
            SQL.AppendLine("Inner Join TblSOContractHdr C On B1.SOCDocNo = C.DocNo And C.CurCode = @CurCode ");
            SQL.AppendLine("Inner Join TblProjectStage D On A.StageCode = D.StageCode ");
            SQL.AppendLine("Inner Join TblProjectTask E On A.TaskCode = E.TaskCode ");
            SQL.AppendLine("Inner Join TblBOQHdr F On C.BOQDocNo=F.DocNo ");
            SQL.AppendLine("Inner Join TblLOPHdr G On F.LOPDocNo=G.DocNo And G.CtCode = @CtCode ");
            if (mFrmParent.mIsFilterBySite)
            {
                SQL.AppendLine("And (G.SiteCode Is Null Or ( ");
                SQL.AppendLine("    G.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(G.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("            ) ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine(")) ");
            }


            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 9;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No",

                     //1-5
                    "",
                    "Project Implementation#",
                    "Project Implementation D#",
                    "",
                    "Stage",

                    //6-8
                    "Task",
                    "Bobot (%)",
                    "Amount"
                },
                 new int[] 
                {
                    //0
                    50, 

                    //1-5
                    20, 145, 0, 20, 150,

                    //6-8
                    150, 100, 120
                }
            );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColButton(Grd1, new int[] { 4 });
            Sm.GrdFormatDec(Grd1, new int[] { 7, 8 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 3, 4 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 3, 5, 6, 7, 8 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " Where Locate(Concat('##', A.DocNo, '##', A.DNo, '##'), @SelectedProjectImplementation)<1 ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@CtCode", mCtCode);
                Sm.CmParam<String>(ref cm, "@ProjectName", mProjectName);
                Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(mFrmParent.LueCurCode));
                Sm.CmParam<String>(ref cm, "@SelectedProjectImplementation", mFrmParent.GetSelectedProjectImplementation());

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "B.DocDt");
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueTaskCode), "A.TaskCode", true);
                
                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, mSQL + Filter + " Order By A.DocNo, A.DNo; ",
                    new string[] 
                    { 
                        //0
                        "DocNo", 
                        
                        //1-5
                        "DNo", "StageName", "TaskName", "BobotPercentage", "Amt"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd1.Cells[Row, 0].Value = Row + 1;
                        Grd1.Cells[Row, 1].Value = false;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 4);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 5);
                    }, true, false, false, false
                ); 
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsDataAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 2, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 3, Grd1, Row2, 3);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 5, Grd1, Row2, 5);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 6, Grd1, Row2, 6);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 7, Grd1, Row2, 7);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 8, Grd1, Row2, 8);

                        mFrmParent.Grd1.Rows.Add();
                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 7, 8 });

                        mFrmParent.ComputeAmt();
                    }
                }
            }
            mFrmParent.ComputeAmt();
            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 task.");
        }

        private bool IsDataAlreadyChosen(int Row)
        {
            string key = Sm.GetGrdStr(Grd1, Row, 2) + Sm.GetGrdStr(Grd1, Row, 3);
            for (int Index = 0; Index < mFrmParent.Grd1.Rows.Count - 1; Index++)
                if (Sm.CompareStr(key, 
                        Sm.GetGrdStr(mFrmParent.Grd1, Index, 2)+
                        Sm.GetGrdStr(mFrmParent.Grd1, Index, 3)
                    ))
                    return true;
            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmProjectImplementation(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmProjectImplementation(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    Grd1.Cells[Row, 1].Value = !IsSelected;
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt1).Length == 0) DteDocDt1.EditValue = DteDocDt2.EditValue;
        }

        private void LueTaskCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueTaskCode, new Sm.RefreshLue1(Sl.SetLueProjectTaskCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkTaskCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Task");
        }

        #endregion

        #endregion
    }
}
