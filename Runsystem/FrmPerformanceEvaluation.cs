﻿#region Update
/*
    31/03/2018 [TKG] tambah filter site dan level 
 *  03/12/2018 [HAR] nilai target achievment diakumulasi
 *  11/12/2018 [HAR] bug nilai achievment
 *  13/12/2022 [BRI/TWC] tambah kolom berdasarkan param IsPerfomanceEvaluationshowResultDesc 
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;
using System.Data;


using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

#endregion

namespace RunSystem
{
    public partial class FrmPerformanceEvaluation : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string 
            mMenuCode = string.Empty, 
            mAccessInd = string.Empty, 
            mDocNo = string.Empty, 
            mCity = string.Empty, 
            mCnt = string.Empty, 
            mExpCity = string.Empty;
        internal bool
            mIsFilterBySiteHR = false,
            mIsFilterByLevelHR = false,
            mIsPerfomanceEvaluationshowResultDesc = false;
        internal FrmPerformanceEvaluationFind FrmFind;
        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmPerformanceEvaluation(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Performance Evaluation";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                Sl.SetLuePosCode(ref LuePosCode);
                SetLueTrainingCode(ref LueTrainingCode);
                LueTrainingCode.Visible = false;

                base.FrmLoad(sender, e);
                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void GetParameter()
        {
            mIsFilterBySiteHR = Sm.GetParameterBoo("IsFilterBySiteHR");
            mIsFilterByLevelHR = Sm.GetParameterBoo("IsFilterByLevelHR");
            mIsPerfomanceEvaluationshowResultDesc = Sm.GetParameterBoo("IsPerfomanceEvaluationshowResultDesc");
        }

        #region Set Grid

        private void SetGrd()
        {
            #region Grid 1 - KPI Process

            Grd1.Cols.Count = 10;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "DNo",

                    //1-5
                    "KPI Process DNo",
                    "Result" + Environment.NewLine + "Indicator",
                    "Bobot",
                    "Unit of Measurement",
                    "Year / Month",
                    
                    //6-9
                    "Type",
                    "Target",
                    "Achievement",
                    "Performance Result" + Environment.NewLine + "Description"
                },
                new int[] 
                {
                    //0
                    0,

                    //1-5
                    20, 250, 80, 150, 100,  
                    
                    //6-9
                    80, 150, 150, 250,
                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 3, 7, 8 }, 0);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8 });
            Sm.GrdColInvisible(Grd1, new int[] { 0, 1, 3, 6 }, false);
            if (!mIsPerfomanceEvaluationshowResultDesc)
                Sm.GrdColInvisible(Grd1, new int[] { 9 }, false);

            #endregion

            #region Grid 2 - Competence

            Grd2.Cols.Count = 8;

            Sm.GrdHdrWithColWidth(
                Grd2,
                new string[] 
                {
                    //0
                    "DNo", 
                    
                    //1-5
                    "CompetenceDNo",
                    "Competence Code",
                    "Competence Name",
                    "Minimum"+Environment.NewLine+"Score",
                    "Score",

                    //6-7
                    "Training Code",
                    "Training"
                },
                new int[] 
                {
                    //0
                    0,

                    //1-5
                    0, 0, 200, 80, 80,

                    //6-7
                    0, 150
                }
            );
            Sm.GrdColInvisible(Grd2, new int[] { 0, 1, 2, 6 }, false);
            Sm.GrdFormatDec(Grd2, new int[] { 4, 5 }, 0);
            Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 1, 2, 3, 4, 6 });

            #endregion
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 3, 6 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd2, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
        }

        #endregion

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, TxtKPIDocNo, LuePosCode, MeeRemark, LueTrainingCode
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 9 });
                    Grd2.ReadOnly = true;
                    BtnKPIDocNo.Enabled = false;
                    BtnKPIDocNo2.Enabled = true;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                       DteDocDt, MeeRemark, LueTrainingCode
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 9 });
                    Grd2.ReadOnly = false;
                    BtnKPIDocNo.Enabled = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    BtnKPIDocNo.Enabled = false;
                    Grd2.ReadOnly = true;
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                  TxtDocNo, DteDocDt, TxtKPIDocNo, LuePosCode, MeeRemark, LueTrainingCode
            });
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 0);
        }

        internal void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 3, 7, 8 });
            Grd2.Rows.Clear();
            Grd2.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd2, 0, new int[] { 4, 5 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmPerformanceEvaluationFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            //if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            //SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    UpdateData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        private void BtnKPIDocNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmPerformanceEvaluationDlg(this));
        }

        private void BtnKPIDocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtKPIDocNo, "KPI#", false))
            {
                var f = new FrmKPI(mMenuCode);
                f.Tag = mMenuCode;
                f.Text = "KPI";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtKPIDocNo.Text;
                f.ShowDialog();
            }
        }

        #endregion

        #region Grid Method

        private void Grd2_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.IsTxtEmpty(TxtKPIDocNo, "KPI#", false);
                return;
            }

            if (Sm.IsGrdColSelected(new int[] { 1, 2, 3, 4, 5, 6, 7 }, e.ColIndex) && BtnSave.Enabled)
            {
                if (Sm.IsGrdColSelected(new int[] { 7 }, e.ColIndex))
                {
                    LueRequestEdit(Grd2, LueTrainingCode, ref fCell, ref fAccept, e);
                    SetLueTrainingCode(ref LueTrainingCode);
                }
                Sm.GrdRequestEdit(Grd2, e.RowIndex);
                Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 4, 5 });
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "PerformanceEvaluation", "TblPerformanceEvaluationHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SavePerformanceEvaluationHdr(DocNo));

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0) cml.Add(SavePerformanceEvaluationDtl(DocNo, Row));

            for (int Row2 = 0; Row2 < Grd2.Rows.Count; Row2++)
            {
                if (Sm.GetGrdStr(Grd2, Row2, 1).Length > 0)
                {
                    cml.Add(SavePerformanceEvaluationDtl2(DocNo, Row2));
                    cml.Add(UpdateEmployeeCompetence(Sm.GetLue(LuePosCode), Row2));
                }
            }

            Sm.ExecCommands(cml);
            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsTxtEmpty(TxtKPIDocNo, "KPI#", false) ||
                IsGrd1Empty() ||
                IsGrd2Empty() ||
                IsGrdExceedMaxRecords();
        }

        private bool IsGrdExceedMaxRecords()
        {
            if ((Grd1.Rows.Count > 1000) || (Grd2.Rows.Count > 1000))
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Data entered ( KPI Evaluation : " + (Grd1.Rows.Count - 1).ToString() + " ; Competence : " + (Grd2.Rows.Count - 1).ToString() + " ) exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsGrd1Empty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 KPI Evaluation.");
                return true;
            }
            return false;
        }

        private bool IsGrd2Empty()
        {
            if (Grd2.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 Competence.");
                return true;
            }
            return false;
        }

        private MySqlCommand SavePerformanceEvaluationHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblPerformanceEvaluationHdr(DocNo, DocDt, KPIDocNo, PosCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @KPIDocNo, @PosCode, @Remark, @CreateBy, CurrentDateTime()); ");
            
            var cm = new MySqlCommand(){ CommandText =SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@KPIDocNo", TxtKPIDocNo.Text);
            Sm.CmParam<String>(ref cm, "@PosCode", Sm.GetLue(LuePosCode));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SavePerformanceEvaluationDtl(string DocNo, int Row)
        {
            var SQLDtl = new StringBuilder();

            if (mIsPerfomanceEvaluationshowResultDesc)
            {
                SQLDtl.AppendLine("Insert Into TblPerformanceEvaluationDtl(DocNo, DNo, KPIProcessDNo, ResultDesc, CreateBy, CreateDt) ");
                SQLDtl.AppendLine("Values(@DocNo, @DNo, @KPIProcessDNo, @ResultDesc, @CreateBy, CurrentDateTime()); ");
            }
            else
            {
                SQLDtl.AppendLine("Insert Into TblPerformanceEvaluationDtl(DocNo, DNo, KPIProcessDNo, CreateBy, CreateDt) ");
                SQLDtl.AppendLine("Values(@DocNo, @DNo, @KPIProcessDNo, @CreateBy, CurrentDateTime()); ");
            }

            var cm = new MySqlCommand() { CommandText = SQLDtl.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@KPIProcessDNo", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<String>(ref cm, "@ResultDesc", Sm.GetGrdStr(Grd1, Row, 9));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SavePerformanceEvaluationDtl2(string DocNo, int Row)
        {
            var SQLDtl2 = new StringBuilder();

            SQLDtl2.AppendLine("Insert Into TblPerformanceEvaluationDtl2(DocNo, DNo, CompetenceDNo, CompetenceCode, Score, TrainingCode, CreateBy, CreateDt) ");
            SQLDtl2.AppendLine("Values(@DocNo, @DNo, @CompetenceDNo, @CompetenceCode, @Score, @TrainingCode, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQLDtl2.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@CompetenceDNo", Sm.GetGrdStr(Grd2, Row, 1));
            Sm.CmParam<String>(ref cm, "@CompetenceCode", Sm.GetGrdStr(Grd2, Row, 2));
            Sm.CmParam<Decimal>(ref cm, "@Score", Sm.GetGrdDec(Grd2, Row, 5));
            Sm.CmParam<String>(ref cm, "@TrainingCode", Sm.GetGrdStr(Grd2, Row, 6));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand UpdateEmployeeCompetence(string PosCode, int Row)
        {
            var SQLDtl3 = new StringBuilder();

            SQLDtl3.AppendLine("Insert Into TblEmployeeCompetence(EmpCode, DNo, CompetenceCode, Description, Score, CreateBy, CreateDt) ");
            SQLDtl3.AppendLine("Select A.EmpCode, @DNo, @CompetenceCode, 'Performance Evaluation', @Score, @CreateBy, CurrentDateTime() ");
            SQLDtl3.AppendLine("From TblEmployee A ");
            SQLDtl3.AppendLine("Where (A.ResignDt Is Null And A.PosCode = @PosCode) ");
            SQLDtl3.AppendLine("On Duplicate Key ");
            SQLDtl3.AppendLine("    Update ");
            SQLDtl3.AppendLine("    CompetenceCode = @CompetenceCode, Score = @Score, LastUpBy = @CreateBy, LastUpDt = CurrentDateTime(); ");

            var cm = new MySqlCommand() { CommandText = SQLDtl3.ToString() };

            Sm.CmParam<String>(ref cm, "@PosCode", PosCode);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@CompetenceCode", Sm.GetGrdStr(Grd2, Row, 2));
            Sm.CmParam<Decimal>(ref cm, "@Score", Sm.GetGrdDec(Grd2, Row, 5));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Update Data

        private void UpdateData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            for (int Row2 = 0; Row2 < Grd2.Rows.Count; Row2++)
            {
                if (Sm.GetGrdStr(Grd2, Row2, 1).Length > 0)
                {
                    cml.Add(UpdatePerformanceEvaluation(TxtDocNo.Text, Row2));
                    cml.Add(UpdateEmployeeCompetence(Sm.GetLue(LuePosCode), Row2));
                }
            }

            Sm.ExecCommands(cml);
            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsTxtEmpty(TxtKPIDocNo, "KPI#", false) ||
                IsGrdValueNotValid() ||
                IsGrd1Empty() ||
                IsGrd2Empty() ||
                IsGrdExceedMaxRecords();
        }

        private bool IsGrdValueNotValid()
        {
            for(int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, Row, 3, false, "Competence Name is empty")) return true;
            }
            
            return false;
        }

        private MySqlCommand UpdatePerformanceEvaluation(string DocNo, int Row)
        {
            var SQLDtl2 = new StringBuilder();

            SQLDtl2.AppendLine("Update TblPerformanceEvaluationDtl2 Set Score = @Score, TrainingCode = @TrainingCode, LastUpBy = @CreateBy, LastUpDt = CurrentDateTime() ");
            SQLDtl2.AppendLine("Where DocNo = @DocNo And DNo = @DNo; ");

            var cm = new MySqlCommand() { CommandText = SQLDtl2.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.GetGrdStr(Grd2, Row, 0));
            Sm.CmParam<Decimal>(ref cm, "@Score", Sm.GetGrdDec(Grd2, Row, 5));
            Sm.CmParam<String>(ref cm, "@TrainingCode", Sm.GetGrdStr(Grd2, Row, 6));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowPerformanceEvaluationHdr(DocNo);
                ShowPerformanceEvaluationDtl(DocNo);
                ShowPerformanceEvaluationDtl2(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowPerformanceEvaluationHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.KPIDocNo, B.PosCode, A.Remark ");
            SQL.AppendLine("From TblPerformanceEvaluationHdr A ");
            SQL.AppendLine("Inner Join TblKPIHdr B On A.KPIDocNo = B.DocNo ");
            SQL.AppendLine("Where A.DocNo = @DocNo; ");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo",

                        //1-4
                        "DocDt", "KPIDocNo", "PosCode", "Remark"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        TxtKPIDocNo.EditValue = Sm.DrStr(dr, c[2]);
                        Sm.SetLue(LuePosCode, Sm.DrStr(dr, c[3]));
                        MeeRemark.EditValue = Sm.DrStr(dr, c[4]);
                    }, true
                );
        }

        private void ShowPerformanceEvaluationDtl(string DocNo)
        {
            var SQLDtl = new StringBuilder();

            SQLDtl.AppendLine("Select B.DNo, B.KPIProcessDNo, E.Result, E.Bobot, E.Uom, E.Option, E.Target, E.Type, D.Value, ");
            if (mIsPerfomanceEvaluationshowResultDesc)
                SQLDtl.AppendLine("B.ResultDesc ");
            else
                SQLDtl.AppendLine("Null As ResultDesc ");
            SQLDtl.AppendLine("From TblPerformanceEvaluationHdr A ");
            SQLDtl.AppendLine("Inner Join TblPerformanceEvaluationDtl B On A.DocNo = B.DocNo");
            SQLDtl.AppendLine("Inner Join TblKPIProcessHdr C On A.KPIDocNo = C.KPIDocNo ");
            SQLDtl.AppendLine("Inner Join TblKPIProcessDtl D On C.DocNo = D.DocNo And B.KPIProcessDNo = D.DNo ");
            SQLDtl.AppendLine("Inner Join TblKPIDtl E On A.KPIDocNo = E.DocNo And E.DNo = D.KPIDNo ");
            SQLDtl.AppendLine("Where A.DocNo = @DocNo Order By B.DNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQLDtl.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 

                    //1-5
                    "KPIProcessDNo", "Result", "Bobot", "Uom", "Option",

                    //6-9
                    "Type", "Target", "Value", "ResultDesc"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 6);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 7, 7);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 8, 8);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 9, 9);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 3, 7, 8 });
            Sm.FocusGrd(Grd1, 0, 1);
          
        }

        private void ShowPerformanceEvaluationDtl2(string DocNo)
        {
            var SQLDtl2 = new StringBuilder();

            SQLDtl2.AppendLine("Select B.DNo, B.CompetenceDNo, B.CompetenceCode, D.CompetenceName, C.MinScore, B.Score, B.TrainingCode, E.TrainingName ");
            SQLDtl2.AppendLine("From TblPerformanceEvaluationHdr A ");
            SQLDtl2.AppendLine("Inner Join TblPerformanceEvaluationDtl2 B On A.DocNo = B.DocNo ");
            SQLDtl2.AppendLine("Inner Join TblOrganizationalStructureDtl4 C On A.PosCode = C.PosCode And B.CompetenceDNo = C.DNo ");
            SQLDtl2.AppendLine("Inner Join TblCompetence D On B.CompetenceCode = D.CompetenceCode ");
            SQLDtl2.AppendLine("Left Join TblTraining E On B.TrainingCode = E.TrainingCode ");
            SQLDtl2.AppendLine("Where A.DocNo = @DocNo Order By B.DNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd2, ref cm, SQLDtl2.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 

                    //1-5
                    "CompetenceDNo", "CompetenceCode", "CompetenceName", "MinScore", "Score",

                    //6-7
                    "TrainingCode", "TrainingName"

                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 7);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 4, 5 });
            Sm.FocusGrd(Grd2, 0, 1);

        }

        #endregion

        #region Additional Method

        private static void SetLueTrainingCode(ref LookUpEdit Lue)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select T.TrainingCode As Col1, T.TrainingName As Col2 ");
                SQL.AppendLine("From TblTraining T ");
                SQL.AppendLine("Order By T.TrainingName; ");

                Sm.SetLue2(
                    ref Lue, SQL.ToString(),
                    0, 35, false, true, "Code", "Training", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void LueRequestEdit(
          iGrid Grd,
          DevExpress.XtraEditors.LookUpEdit Lue,
          ref iGCell fCell,
          ref bool fAccept,
          TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;
            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        internal void ShowKPIProcess(string DocNo)
        {
            var SQLDtl = new StringBuilder();

            SQLDtl.AppendLine("Select B.DNo, C.Result, C.Bobot, C.Uom, C.Option, AVG(C.Target) As target, C.Type, ");
            SQLDtl.AppendLine("if(C.Type = 'SUM', SUM(B.Value), AVG(B.Value)) As value ");
            SQLDtl.AppendLine("From TblKPIProcessHdr A ");
            SQLDtl.AppendLine("Inner Join TblKPIProcessDtl B On A.DocNo = B.Docno");
            SQLDtl.AppendLine("Inner Join TblKPIDtl C On A.KPIDocNo = C.DocNo And B.KPIDno = C.DNo ");
            SQLDtl.AppendLine("Where C.DocNo=@DocNo And A.CancelInd = 'N' ");
            SQLDtl.AppendLine("Group BY B.DNo, C.Result, C.Bobot, C.Uom ");
            SQLDtl.AppendLine("Order By B.DNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQLDtl.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 

                    //1-5
                    "Result", "Bobot", "Uom", "Option", "Type",

                    //6-7
                    "Target", "Value"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 8, 7);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 3, 7, 8 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        internal void ShowCompetence(string PosCode)
        {
            var SQLDtl2 = new StringBuilder();

            SQLDtl2.AppendLine("Select A.DNo, A.CompetenceCode, B.CompetenceName, A.MinScore, 0 As Score ");
            SQLDtl2.AppendLine("From TblOrganizationalStructureDtl4 A ");
            SQLDtl2.AppendLine("Inner Join TblCompetence B On A.CompetenceCode = B.CompetenceCode ");
            SQLDtl2.AppendLine("Where A.PosCode = @PosCode Order By A.DNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@PosCode", PosCode);
            Sm.ShowDataInGrid(
                ref Grd2, ref cm, SQLDtl2.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 

                    //1-4
                    "CompetenceCode", "CompetenceName", "MinScore", "Score"

                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 4, 5 });
            Sm.FocusGrd(Grd2, 0, 1);
        }

        #endregion

        #region Misc Control Methods

        private void LueTrainingCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueTrainingCode, new Sm.RefreshLue1(SetLueTrainingCode));
        }

        private void LueTrainingCode_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd2, ref fAccept, e);
        }

        private void LueTrainingCode_Leave(object sender, EventArgs e)
        {
            if (LueTrainingCode.Visible && fAccept && fCell.ColIndex == 7)
            {
                if (Sm.GetLue(LueTrainingCode).Length == 0)
                    Grd2.Cells[fCell.RowIndex, 6].Value =
                    Grd2.Cells[fCell.RowIndex, 7].Value = null;
                else
                {
                    Grd2.Cells[fCell.RowIndex, 6].Value = Sm.GetLue(LueTrainingCode);
                    Grd2.Cells[fCell.RowIndex, 7].Value = LueTrainingCode.GetColumnValue("Col2");
                }
                LueTrainingCode.Visible = false;
            }
        }

        #endregion

        #endregion


    }
}
