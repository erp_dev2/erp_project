﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRegion : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode =string.Empty, mAccessInd = string.Empty;
        internal FrmRegionFind FrmFind; 

        #endregion

        #region Constructor

        public FrmRegion(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            BtnPrint.Visible = false;
            SetFormControl(mState.View);
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtRegionCode, TxtRegionName
                    }, true);
                    TxtRegionCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtRegionCode, TxtRegionName
                    }, false);
                    TxtRegionCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(TxtRegionCode, true);
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                       TxtRegionName
                    }, false);
                    TxtRegionName.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtRegionCode, TxtRegionName
            });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmRegionFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtRegionCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtRegionCode, "", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblRegion Where RegionCode=@RegionCode" };
                Sm.CmParam<String>(ref cm, "@RegionCode", TxtRegionCode.Text);
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into TblRegion(RegionCode, RegionName,CreateBy, CreateDt ) ");
                SQL.AppendLine("Values(@RegionCode, @RegionName,@UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("   Update RegionName=@RegionName,LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@RegionCode", TxtRegionCode.Text);
                Sm.CmParam<String>(ref cm, "@RegionName", TxtRegionName.Text);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.ExecCommand(cm);

                ShowData(TxtRegionCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string BankCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@RegionCode", BankCode);
                Sm.ShowDataInCtrl(
                        ref cm,
                        "Select * From TblRegion Where RegionCode = @RegionCode",
                        new string[] 
                        {
                            "RegionCode", 
                            "RegionName"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtRegionCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtRegionName.EditValue = Sm.DrStr(dr, c[1]);
                           
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtRegionCode, "Region code", false) ||
                Sm.IsTxtEmpty(TxtRegionName, "Region Name", false) ||
                IsRegionCodeExisted();
        }

        private bool IsRegionCodeExisted()
        {
            if (!TxtRegionCode.Properties.ReadOnly && Sm.IsDataExist("Select RegionCode From TblRegion Where RegionCode ='" + TxtRegionCode.Text + "' Limit 1"))
            {
                Sm.StdMsg(mMsgType.Warning, "Region code ( " + TxtRegionCode.Text + " ) already existed.");
                return true;
            }
            return false;
        }


        #endregion

       
        #endregion

      
        #region Event

        #region Misc Control Event
        private void TxtRegionCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtRegionCode);
        }
        private void TxtRegionName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtRegionName);
        }


        #endregion

        #endregion

    }
}
