﻿namespace RunSystem
{
    partial class FrmBom3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmBom3));
            this.TxtQty = new DevExpress.XtraEditors.TextEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.TxtUomCode = new DevExpress.XtraEditors.TextEdit();
            this.BtnSOContractDocNo = new DevExpress.XtraEditors.SimpleButton();
            this.TxtItName = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtSOContractDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtItCode = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtItCodeInternal = new DevExpress.XtraEditors.TextEdit();
            this.MeeSpecification = new DevExpress.XtraEditors.MemoExEdit();
            this.LblItCodeInternal = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtProjectCode = new DevExpress.XtraEditors.TextEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtProjectName = new DevExpress.XtraEditors.TextEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.BtnSOContractDocNo2 = new DevExpress.XtraEditors.SimpleButton();
            this.ChkFileName = new DevExpress.XtraEditors.CheckEdit();
            this.BtnDownload = new DevExpress.XtraEditors.SimpleButton();
            this.PbUpload = new System.Windows.Forms.ProgressBar();
            this.BtnBrowseFile = new DevExpress.XtraEditors.SimpleButton();
            this.TxtFileName = new DevExpress.XtraEditors.TextEdit();
            this.label14 = new System.Windows.Forms.Label();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.ChkCancelInd = new DevExpress.XtraEditors.CheckEdit();
            this.label27 = new System.Windows.Forms.Label();
            this.MeeCancelReason = new DevExpress.XtraEditors.MemoExEdit();
            this.TxtRevNo = new DevExpress.XtraEditors.TextEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.BtnCopy = new DevExpress.XtraEditors.SimpleButton();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.label11 = new System.Windows.Forms.Label();
            this.SFD = new System.Windows.Forms.SaveFileDialog();
            this.OD = new System.Windows.Forms.OpenFileDialog();
            this.TxtPONo = new DevExpress.XtraEditors.TextEdit();
            this.Tc1 = new DevExpress.XtraTab.XtraTabControl();
            this.Tp1 = new DevExpress.XtraTab.XtraTabPage();
            this.panel5 = new System.Windows.Forms.Panel();
            this.TxtStatus = new DevExpress.XtraEditors.TextEdit();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.LueDeptCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label13 = new System.Windows.Forms.Label();
            this.LueBomGroupCode = new DevExpress.XtraEditors.LookUpEdit();
            this.ChkServiceItemInd = new DevExpress.XtraEditors.CheckEdit();
            this.TxtRevision = new DevExpress.XtraEditors.TextEdit();
            this.Tp2 = new DevExpress.XtraTab.XtraTabPage();
            this.panel6 = new System.Windows.Forms.Panel();
            this.Grd2 = new TenTec.Windows.iGridLib.iGrid();
            this.panel7 = new System.Windows.Forms.Panel();
            this.TxtDrawingApprovalDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.BtnDrawingApprovalDocNo2 = new DevExpress.XtraEditors.SimpleButton();
            this.BtnDrawingApprovalDocNo = new DevExpress.XtraEditors.SimpleButton();
            this.Tp3 = new DevExpress.XtraTab.XtraTabPage();
            this.panel4 = new System.Windows.Forms.Panel();
            this.TxtQty1 = new DevExpress.XtraEditors.TextEdit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtQty.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtUomCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSOContractDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItCodeInternal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeSpecification.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProjectCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProjectName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFileName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFileName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCancelReason.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRevNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPONo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Tc1)).BeginInit();
            this.Tc1.SuspendLayout();
            this.Tp1.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDeptCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBomGroupCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkServiceItemInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRevision.Properties)).BeginInit();
            this.Tp2.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).BeginInit();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDrawingApprovalDocNo.Properties)).BeginInit();
            this.Tp3.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtQty1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Size = new System.Drawing.Size(70, 562);
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnPrint.Click += new System.EventHandler(this.BtnPrint_Click);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.Tc1);
            this.panel2.Size = new System.Drawing.Size(772, 373);
            // 
            // panel3
            // 
            this.panel3.Location = new System.Drawing.Point(0, 373);
            this.panel3.Size = new System.Drawing.Size(772, 189);
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Location = new System.Drawing.Point(0, 540);
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // Grd1
            // 
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 21;
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.Size = new System.Drawing.Size(772, 189);
            this.Grd1.TabIndex = 53;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // TxtQty
            // 
            this.TxtQty.EnterMoveNextControl = true;
            this.TxtQty.Location = new System.Drawing.Point(167, 236);
            this.TxtQty.Margin = new System.Windows.Forms.Padding(5);
            this.TxtQty.Name = "TxtQty";
            this.TxtQty.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtQty.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtQty.Properties.Appearance.Options.UseBackColor = true;
            this.TxtQty.Properties.Appearance.Options.UseFont = true;
            this.TxtQty.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtQty.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtQty.Properties.ReadOnly = true;
            this.TxtQty.Size = new System.Drawing.Size(87, 20);
            this.TxtQty.TabIndex = 43;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Location = new System.Drawing.Point(109, 239);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(54, 14);
            this.label9.TabIndex = 42;
            this.label9.Text = "Quantity";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtUomCode
            // 
            this.TxtUomCode.EnterMoveNextControl = true;
            this.TxtUomCode.Location = new System.Drawing.Point(257, 236);
            this.TxtUomCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtUomCode.Name = "TxtUomCode";
            this.TxtUomCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtUomCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUomCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtUomCode.Properties.Appearance.Options.UseFont = true;
            this.TxtUomCode.Properties.MaxLength = 16;
            this.TxtUomCode.Properties.ReadOnly = true;
            this.TxtUomCode.Size = new System.Drawing.Size(113, 20);
            this.TxtUomCode.TabIndex = 44;
            // 
            // BtnSOContractDocNo
            // 
            this.BtnSOContractDocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnSOContractDocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSOContractDocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSOContractDocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSOContractDocNo.Appearance.Options.UseBackColor = true;
            this.BtnSOContractDocNo.Appearance.Options.UseFont = true;
            this.BtnSOContractDocNo.Appearance.Options.UseForeColor = true;
            this.BtnSOContractDocNo.Appearance.Options.UseTextOptions = true;
            this.BtnSOContractDocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnSOContractDocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnSOContractDocNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnSOContractDocNo.Image")));
            this.BtnSOContractDocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnSOContractDocNo.Location = new System.Drawing.Point(372, 89);
            this.BtnSOContractDocNo.Name = "BtnSOContractDocNo";
            this.BtnSOContractDocNo.Size = new System.Drawing.Size(24, 21);
            this.BtnSOContractDocNo.TabIndex = 25;
            this.BtnSOContractDocNo.ToolTip = "Find SO Contract";
            this.BtnSOContractDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnSOContractDocNo.ToolTipTitle = "Run System";
            this.BtnSOContractDocNo.Click += new System.EventHandler(this.BtnSOContractDocNo_Click);
            // 
            // TxtItName
            // 
            this.TxtItName.EnterMoveNextControl = true;
            this.TxtItName.Location = new System.Drawing.Point(167, 173);
            this.TxtItName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtItName.Name = "TxtItName";
            this.TxtItName.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtItName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtItName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtItName.Properties.Appearance.Options.UseFont = true;
            this.TxtItName.Properties.MaxLength = 250;
            this.TxtItName.Properties.ReadOnly = true;
            this.TxtItName.Size = new System.Drawing.Size(569, 20);
            this.TxtItName.TabIndex = 37;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(37, 176);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(126, 14);
            this.label3.TabIndex = 36;
            this.label3.Text = "Finished Good\'s Name";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSOContractDocNo
            // 
            this.TxtSOContractDocNo.EnterMoveNextControl = true;
            this.TxtSOContractDocNo.Location = new System.Drawing.Point(167, 89);
            this.TxtSOContractDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSOContractDocNo.Name = "TxtSOContractDocNo";
            this.TxtSOContractDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtSOContractDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSOContractDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSOContractDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtSOContractDocNo.Properties.MaxLength = 16;
            this.TxtSOContractDocNo.Properties.ReadOnly = true;
            this.TxtSOContractDocNo.Size = new System.Drawing.Size(204, 20);
            this.TxtSOContractDocNo.TabIndex = 24;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Red;
            this.label6.Location = new System.Drawing.Point(80, 92);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(83, 14);
            this.label6.TabIndex = 23;
            this.label6.Text = "SO Contract#";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(167, 3);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 16;
            this.TxtDocNo.Properties.ReadOnly = true;
            this.TxtDocNo.Size = new System.Drawing.Size(204, 20);
            this.TxtDocNo.TabIndex = 14;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(72, 6);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 14);
            this.label1.TabIndex = 13;
            this.label1.Text = "Bill Of Material#";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtItCode
            // 
            this.TxtItCode.EnterMoveNextControl = true;
            this.TxtItCode.Location = new System.Drawing.Point(167, 152);
            this.TxtItCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtItCode.Name = "TxtItCode";
            this.TxtItCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtItCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtItCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtItCode.Properties.Appearance.Options.UseFont = true;
            this.TxtItCode.Properties.MaxLength = 250;
            this.TxtItCode.Properties.ReadOnly = true;
            this.TxtItCode.Size = new System.Drawing.Size(204, 20);
            this.TxtItCode.TabIndex = 33;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(40, 155);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(123, 14);
            this.label2.TabIndex = 32;
            this.label2.Text = "Finished Good\'s Code";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtItCodeInternal
            // 
            this.TxtItCodeInternal.EnterMoveNextControl = true;
            this.TxtItCodeInternal.Location = new System.Drawing.Point(167, 194);
            this.TxtItCodeInternal.Margin = new System.Windows.Forms.Padding(5);
            this.TxtItCodeInternal.Name = "TxtItCodeInternal";
            this.TxtItCodeInternal.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtItCodeInternal.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtItCodeInternal.Properties.Appearance.Options.UseBackColor = true;
            this.TxtItCodeInternal.Properties.Appearance.Options.UseFont = true;
            this.TxtItCodeInternal.Properties.MaxLength = 30;
            this.TxtItCodeInternal.Properties.ReadOnly = true;
            this.TxtItCodeInternal.Size = new System.Drawing.Size(204, 20);
            this.TxtItCodeInternal.TabIndex = 39;
            // 
            // MeeSpecification
            // 
            this.MeeSpecification.EnterMoveNextControl = true;
            this.MeeSpecification.Location = new System.Drawing.Point(167, 215);
            this.MeeSpecification.Margin = new System.Windows.Forms.Padding(5);
            this.MeeSpecification.Name = "MeeSpecification";
            this.MeeSpecification.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.MeeSpecification.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSpecification.Properties.Appearance.Options.UseBackColor = true;
            this.MeeSpecification.Properties.Appearance.Options.UseFont = true;
            this.MeeSpecification.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSpecification.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeSpecification.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSpecification.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeSpecification.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSpecification.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeSpecification.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeSpecification.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeSpecification.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeSpecification.Properties.MaxLength = 400;
            this.MeeSpecification.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeSpecification.Properties.ReadOnly = true;
            this.MeeSpecification.Properties.ShowIcon = false;
            this.MeeSpecification.Size = new System.Drawing.Size(569, 20);
            this.MeeSpecification.TabIndex = 41;
            this.MeeSpecification.ToolTip = "F4 : Show/hide text";
            this.MeeSpecification.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeSpecification.ToolTipTitle = "Run System";
            // 
            // LblItCodeInternal
            // 
            this.LblItCodeInternal.AutoSize = true;
            this.LblItCodeInternal.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblItCodeInternal.ForeColor = System.Drawing.Color.Black;
            this.LblItCodeInternal.Location = new System.Drawing.Point(9, 197);
            this.LblItCodeInternal.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblItCodeInternal.Name = "LblItCodeInternal";
            this.LblItCodeInternal.Size = new System.Drawing.Size(154, 14);
            this.LblItCodeInternal.TabIndex = 38;
            this.LblItCodeInternal.Text = "Finished Good\'s Local Code";
            this.LblItCodeInternal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(88, 218);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(75, 14);
            this.label4.TabIndex = 40;
            this.label4.Text = "Specification";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtProjectCode
            // 
            this.TxtProjectCode.EnterMoveNextControl = true;
            this.TxtProjectCode.Location = new System.Drawing.Point(167, 110);
            this.TxtProjectCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtProjectCode.Name = "TxtProjectCode";
            this.TxtProjectCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtProjectCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtProjectCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtProjectCode.Properties.Appearance.Options.UseFont = true;
            this.TxtProjectCode.Properties.MaxLength = 250;
            this.TxtProjectCode.Properties.ReadOnly = true;
            this.TxtProjectCode.Size = new System.Drawing.Size(204, 20);
            this.TxtProjectCode.TabIndex = 28;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(117, 113);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(46, 14);
            this.label5.TabIndex = 27;
            this.label5.Text = "Project";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtProjectName
            // 
            this.TxtProjectName.EnterMoveNextControl = true;
            this.TxtProjectName.Location = new System.Drawing.Point(374, 110);
            this.TxtProjectName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtProjectName.Name = "TxtProjectName";
            this.TxtProjectName.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtProjectName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtProjectName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtProjectName.Properties.Appearance.Options.UseFont = true;
            this.TxtProjectName.Properties.MaxLength = 250;
            this.TxtProjectName.Properties.ReadOnly = true;
            this.TxtProjectName.Size = new System.Drawing.Size(363, 20);
            this.TxtProjectName.TabIndex = 29;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(67, 134);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(96, 14);
            this.label8.TabIndex = 30;
            this.label8.Text = "Customer\'s PO#";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnSOContractDocNo2
            // 
            this.BtnSOContractDocNo2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnSOContractDocNo2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSOContractDocNo2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSOContractDocNo2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSOContractDocNo2.Appearance.Options.UseBackColor = true;
            this.BtnSOContractDocNo2.Appearance.Options.UseFont = true;
            this.BtnSOContractDocNo2.Appearance.Options.UseForeColor = true;
            this.BtnSOContractDocNo2.Appearance.Options.UseTextOptions = true;
            this.BtnSOContractDocNo2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnSOContractDocNo2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnSOContractDocNo2.Image = ((System.Drawing.Image)(resources.GetObject("BtnSOContractDocNo2.Image")));
            this.BtnSOContractDocNo2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnSOContractDocNo2.Location = new System.Drawing.Point(397, 89);
            this.BtnSOContractDocNo2.Name = "BtnSOContractDocNo2";
            this.BtnSOContractDocNo2.Size = new System.Drawing.Size(24, 21);
            this.BtnSOContractDocNo2.TabIndex = 26;
            this.BtnSOContractDocNo2.ToolTip = "Show SO Contract\'s Information";
            this.BtnSOContractDocNo2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnSOContractDocNo2.ToolTipTitle = "Run System";
            this.BtnSOContractDocNo2.Click += new System.EventHandler(this.BtnSOContractDocNo2_Click);
            // 
            // ChkFileName
            // 
            this.ChkFileName.Location = new System.Drawing.Point(343, 9);
            this.ChkFileName.Name = "ChkFileName";
            this.ChkFileName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkFileName.Properties.Appearance.Options.UseFont = true;
            this.ChkFileName.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkFileName.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkFileName.Properties.Caption = " ";
            this.ChkFileName.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkFileName.Size = new System.Drawing.Size(20, 22);
            this.ChkFileName.TabIndex = 15;
            this.ChkFileName.ToolTip = "Remove filter";
            this.ChkFileName.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkFileName.ToolTipTitle = "Run System";
            // 
            // BtnDownload
            // 
            this.BtnDownload.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDownload.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDownload.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDownload.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDownload.Appearance.Options.UseBackColor = true;
            this.BtnDownload.Appearance.Options.UseFont = true;
            this.BtnDownload.Appearance.Options.UseForeColor = true;
            this.BtnDownload.Appearance.Options.UseTextOptions = true;
            this.BtnDownload.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDownload.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDownload.Image = ((System.Drawing.Image)(resources.GetObject("BtnDownload.Image")));
            this.BtnDownload.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDownload.Location = new System.Drawing.Point(389, 9);
            this.BtnDownload.Name = "BtnDownload";
            this.BtnDownload.Size = new System.Drawing.Size(24, 21);
            this.BtnDownload.TabIndex = 17;
            this.BtnDownload.ToolTip = "Download File";
            this.BtnDownload.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDownload.ToolTipTitle = "Run System";
            this.BtnDownload.Click += new System.EventHandler(this.BtnDownload_Click);
            // 
            // PbUpload
            // 
            this.PbUpload.Location = new System.Drawing.Point(41, 31);
            this.PbUpload.Name = "PbUpload";
            this.PbUpload.Size = new System.Drawing.Size(297, 23);
            this.PbUpload.TabIndex = 18;
            // 
            // BtnBrowseFile
            // 
            this.BtnBrowseFile.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnBrowseFile.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnBrowseFile.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnBrowseFile.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnBrowseFile.Appearance.Options.UseBackColor = true;
            this.BtnBrowseFile.Appearance.Options.UseFont = true;
            this.BtnBrowseFile.Appearance.Options.UseForeColor = true;
            this.BtnBrowseFile.Appearance.Options.UseTextOptions = true;
            this.BtnBrowseFile.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnBrowseFile.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnBrowseFile.Image = ((System.Drawing.Image)(resources.GetObject("BtnBrowseFile.Image")));
            this.BtnBrowseFile.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnBrowseFile.Location = new System.Drawing.Point(364, 9);
            this.BtnBrowseFile.Name = "BtnBrowseFile";
            this.BtnBrowseFile.Size = new System.Drawing.Size(24, 21);
            this.BtnBrowseFile.TabIndex = 16;
            this.BtnBrowseFile.ToolTip = "Browse File";
            this.BtnBrowseFile.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnBrowseFile.ToolTipTitle = "Run System";
            // 
            // TxtFileName
            // 
            this.TxtFileName.EnterMoveNextControl = true;
            this.TxtFileName.Location = new System.Drawing.Point(41, 10);
            this.TxtFileName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFileName.Name = "TxtFileName";
            this.TxtFileName.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtFileName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFileName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFileName.Properties.Appearance.Options.UseFont = true;
            this.TxtFileName.Properties.MaxLength = 250;
            this.TxtFileName.Properties.ReadOnly = true;
            this.TxtFileName.Size = new System.Drawing.Size(297, 20);
            this.TxtFileName.TabIndex = 14;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(10, 14);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(24, 14);
            this.label14.TabIndex = 13;
            this.label14.Text = "File";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(167, 322);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 250;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(569, 20);
            this.MeeRemark.TabIndex = 52;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(116, 325);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(47, 14);
            this.label7.TabIndex = 51;
            this.label7.Text = "Remark";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkCancelInd
            // 
            this.ChkCancelInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkCancelInd.Location = new System.Drawing.Point(571, 23);
            this.ChkCancelInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkCancelInd.Name = "ChkCancelInd";
            this.ChkCancelInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkCancelInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkCancelInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkCancelInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseFont = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkCancelInd.Properties.Caption = "Cancel";
            this.ChkCancelInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkCancelInd.Size = new System.Drawing.Size(61, 22);
            this.ChkCancelInd.TabIndex = 17;
            this.ChkCancelInd.CheckedChanged += new System.EventHandler(this.ChkCancelInd_CheckedChanged);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(78, 27);
            this.label27.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(85, 14);
            this.label27.TabIndex = 15;
            this.label27.Text = "Cancel Reason";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeCancelReason
            // 
            this.MeeCancelReason.EnterMoveNextControl = true;
            this.MeeCancelReason.Location = new System.Drawing.Point(167, 24);
            this.MeeCancelReason.Margin = new System.Windows.Forms.Padding(5);
            this.MeeCancelReason.Name = "MeeCancelReason";
            this.MeeCancelReason.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.Appearance.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeCancelReason.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeCancelReason.Properties.MaxLength = 250;
            this.MeeCancelReason.Properties.PopupFormSize = new System.Drawing.Size(400, 20);
            this.MeeCancelReason.Properties.ShowIcon = false;
            this.MeeCancelReason.Size = new System.Drawing.Size(397, 20);
            this.MeeCancelReason.TabIndex = 16;
            this.MeeCancelReason.ToolTip = "F4 : Show/hide text";
            this.MeeCancelReason.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeCancelReason.ToolTipTitle = "Run System";
            this.MeeCancelReason.Validated += new System.EventHandler(this.MeeCancelReason_Validated);
            // 
            // TxtRevNo
            // 
            this.TxtRevNo.EnterMoveNextControl = true;
            this.TxtRevNo.Location = new System.Drawing.Point(167, 68);
            this.TxtRevNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtRevNo.Name = "TxtRevNo";
            this.TxtRevNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtRevNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtRevNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtRevNo.Properties.Appearance.Options.UseFont = true;
            this.TxtRevNo.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtRevNo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtRevNo.Properties.ReadOnly = true;
            this.TxtRevNo.Size = new System.Drawing.Size(47, 20);
            this.TxtRevNo.TabIndex = 21;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(104, 71);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(59, 14);
            this.label10.TabIndex = 20;
            this.label10.Text = "Revision#";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnCopy
            // 
            this.BtnCopy.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnCopy.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCopy.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCopy.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCopy.Appearance.Options.UseBackColor = true;
            this.BtnCopy.Appearance.Options.UseFont = true;
            this.BtnCopy.Appearance.Options.UseForeColor = true;
            this.BtnCopy.Appearance.Options.UseTextOptions = true;
            this.BtnCopy.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnCopy.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnCopy.Image = ((System.Drawing.Image)(resources.GetObject("BtnCopy.Image")));
            this.BtnCopy.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnCopy.Location = new System.Drawing.Point(372, 150);
            this.BtnCopy.Name = "BtnCopy";
            this.BtnCopy.Size = new System.Drawing.Size(24, 21);
            this.BtnCopy.TabIndex = 34;
            this.BtnCopy.ToolTip = "Copy Component And Material Information";
            this.BtnCopy.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnCopy.ToolTipTitle = "Run System";
            this.BtnCopy.Click += new System.EventHandler(this.BtnCopy_Click);
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(167, 46);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(149, 20);
            this.DteDocDt.TabIndex = 19;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Red;
            this.label11.Location = new System.Drawing.Point(130, 49);
            this.label11.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(33, 14);
            this.label11.TabIndex = 18;
            this.label11.Text = "Date";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // OD
            // 
            this.OD.FileName = "openFileDialog1";
            // 
            // TxtPONo
            // 
            this.TxtPONo.EnterMoveNextControl = true;
            this.TxtPONo.Location = new System.Drawing.Point(167, 131);
            this.TxtPONo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPONo.Name = "TxtPONo";
            this.TxtPONo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtPONo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPONo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPONo.Properties.Appearance.Options.UseFont = true;
            this.TxtPONo.Properties.MaxLength = 250;
            this.TxtPONo.Properties.ReadOnly = true;
            this.TxtPONo.Size = new System.Drawing.Size(204, 20);
            this.TxtPONo.TabIndex = 31;
            // 
            // Tc1
            // 
            this.Tc1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Tc1.HeaderAutoFill = DevExpress.Utils.DefaultBoolean.True;
            this.Tc1.Location = new System.Drawing.Point(0, 0);
            this.Tc1.Name = "Tc1";
            this.Tc1.SelectedTabPage = this.Tp1;
            this.Tc1.Size = new System.Drawing.Size(772, 373);
            this.Tc1.TabIndex = 11;
            this.Tc1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.Tp1,
            this.Tp2,
            this.Tp3});
            // 
            // Tp1
            // 
            this.Tp1.Appearance.Header.Options.UseTextOptions = true;
            this.Tp1.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp1.Controls.Add(this.panel5);
            this.Tp1.Name = "Tp1";
            this.Tp1.Size = new System.Drawing.Size(766, 345);
            this.Tp1.Text = "General";
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel5.Controls.Add(this.TxtQty1);
            this.panel5.Controls.Add(this.TxtStatus);
            this.panel5.Controls.Add(this.label16);
            this.panel5.Controls.Add(this.label15);
            this.panel5.Controls.Add(this.LueDeptCode);
            this.panel5.Controls.Add(this.label13);
            this.panel5.Controls.Add(this.LueBomGroupCode);
            this.panel5.Controls.Add(this.ChkServiceItemInd);
            this.panel5.Controls.Add(this.TxtRevision);
            this.panel5.Controls.Add(this.MeeCancelReason);
            this.panel5.Controls.Add(this.MeeRemark);
            this.panel5.Controls.Add(this.label7);
            this.panel5.Controls.Add(this.TxtPONo);
            this.panel5.Controls.Add(this.label1);
            this.panel5.Controls.Add(this.DteDocDt);
            this.panel5.Controls.Add(this.TxtDocNo);
            this.panel5.Controls.Add(this.label11);
            this.panel5.Controls.Add(this.label6);
            this.panel5.Controls.Add(this.BtnCopy);
            this.panel5.Controls.Add(this.TxtSOContractDocNo);
            this.panel5.Controls.Add(this.TxtItCodeInternal);
            this.panel5.Controls.Add(this.MeeSpecification);
            this.panel5.Controls.Add(this.TxtRevNo);
            this.panel5.Controls.Add(this.LblItCodeInternal);
            this.panel5.Controls.Add(this.label3);
            this.panel5.Controls.Add(this.label4);
            this.panel5.Controls.Add(this.label10);
            this.panel5.Controls.Add(this.TxtQty);
            this.panel5.Controls.Add(this.TxtItName);
            this.panel5.Controls.Add(this.label9);
            this.panel5.Controls.Add(this.ChkCancelInd);
            this.panel5.Controls.Add(this.TxtUomCode);
            this.panel5.Controls.Add(this.BtnSOContractDocNo);
            this.panel5.Controls.Add(this.label27);
            this.panel5.Controls.Add(this.label2);
            this.panel5.Controls.Add(this.TxtItCode);
            this.panel5.Controls.Add(this.label8);
            this.panel5.Controls.Add(this.TxtProjectName);
            this.panel5.Controls.Add(this.label5);
            this.panel5.Controls.Add(this.TxtProjectCode);
            this.panel5.Controls.Add(this.BtnSOContractDocNo2);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(766, 345);
            this.panel5.TabIndex = 12;
            // 
            // TxtStatus
            // 
            this.TxtStatus.EnterMoveNextControl = true;
            this.TxtStatus.Location = new System.Drawing.Point(168, 301);
            this.TxtStatus.Margin = new System.Windows.Forms.Padding(5);
            this.TxtStatus.Name = "TxtStatus";
            this.TxtStatus.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtStatus.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtStatus.Properties.Appearance.Options.UseBackColor = true;
            this.TxtStatus.Properties.Appearance.Options.UseFont = true;
            this.TxtStatus.Properties.MaxLength = 30;
            this.TxtStatus.Properties.ReadOnly = true;
            this.TxtStatus.Size = new System.Drawing.Size(204, 20);
            this.TxtStatus.TabIndex = 50;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(121, 304);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(42, 14);
            this.label16.TabIndex = 49;
            this.label16.Text = "Status";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Red;
            this.label15.Location = new System.Drawing.Point(90, 282);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(73, 14);
            this.label15.TabIndex = 47;
            this.label15.Text = "Department";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueDeptCode
            // 
            this.LueDeptCode.EnterMoveNextControl = true;
            this.LueDeptCode.Location = new System.Drawing.Point(167, 279);
            this.LueDeptCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueDeptCode.Name = "LueDeptCode";
            this.LueDeptCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.Appearance.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueDeptCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueDeptCode.Properties.DropDownRows = 30;
            this.LueDeptCode.Properties.NullText = "[Empty]";
            this.LueDeptCode.Properties.PopupWidth = 350;
            this.LueDeptCode.Size = new System.Drawing.Size(350, 20);
            this.LueDeptCode.TabIndex = 48;
            this.LueDeptCode.ToolTip = "F4 : Show/hide list";
            this.LueDeptCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueDeptCode.EditValueChanged += new System.EventHandler(this.LueDeptCode_EditValueChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Red;
            this.label13.Location = new System.Drawing.Point(87, 260);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(76, 14);
            this.label13.TabIndex = 45;
            this.label13.Text = "Bom\'s Group";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueBomGroupCode
            // 
            this.LueBomGroupCode.EnterMoveNextControl = true;
            this.LueBomGroupCode.Location = new System.Drawing.Point(167, 257);
            this.LueBomGroupCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueBomGroupCode.Name = "LueBomGroupCode";
            this.LueBomGroupCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBomGroupCode.Properties.Appearance.Options.UseFont = true;
            this.LueBomGroupCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBomGroupCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueBomGroupCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBomGroupCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueBomGroupCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBomGroupCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueBomGroupCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBomGroupCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueBomGroupCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueBomGroupCode.Properties.DropDownRows = 30;
            this.LueBomGroupCode.Properties.NullText = "[Empty]";
            this.LueBomGroupCode.Properties.PopupWidth = 350;
            this.LueBomGroupCode.Size = new System.Drawing.Size(350, 20);
            this.LueBomGroupCode.TabIndex = 46;
            this.LueBomGroupCode.ToolTip = "F4 : Show/hide list";
            this.LueBomGroupCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueBomGroupCode.EditValueChanged += new System.EventHandler(this.LueBomGroupCode_EditValueChanged);
            // 
            // ChkServiceItemInd
            // 
            this.ChkServiceItemInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkServiceItemInd.Location = new System.Drawing.Point(407, 150);
            this.ChkServiceItemInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkServiceItemInd.Name = "ChkServiceItemInd";
            this.ChkServiceItemInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkServiceItemInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkServiceItemInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkServiceItemInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkServiceItemInd.Properties.Appearance.Options.UseFont = true;
            this.ChkServiceItemInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkServiceItemInd.Properties.Caption = "Service";
            this.ChkServiceItemInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkServiceItemInd.Properties.ReadOnly = true;
            this.ChkServiceItemInd.Size = new System.Drawing.Size(72, 22);
            this.ChkServiceItemInd.TabIndex = 35;
            this.ChkServiceItemInd.CheckedChanged += new System.EventHandler(this.ChkServiceItemInd_CheckedChanged);
            // 
            // TxtRevision
            // 
            this.TxtRevision.EnterMoveNextControl = true;
            this.TxtRevision.Location = new System.Drawing.Point(217, 68);
            this.TxtRevision.Margin = new System.Windows.Forms.Padding(5);
            this.TxtRevision.Name = "TxtRevision";
            this.TxtRevision.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtRevision.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtRevision.Properties.Appearance.Options.UseBackColor = true;
            this.TxtRevision.Properties.Appearance.Options.UseFont = true;
            this.TxtRevision.Properties.MaxLength = 16;
            this.TxtRevision.Properties.ReadOnly = true;
            this.TxtRevision.Size = new System.Drawing.Size(155, 20);
            this.TxtRevision.TabIndex = 22;
            // 
            // Tp2
            // 
            this.Tp2.Appearance.Header.Options.UseTextOptions = true;
            this.Tp2.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp2.Controls.Add(this.panel6);
            this.Tp2.Name = "Tp2";
            this.Tp2.Size = new System.Drawing.Size(766, 345);
            this.Tp2.Text = "Drawing Approval";
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel6.Controls.Add(this.Grd2);
            this.panel6.Controls.Add(this.panel7);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(766, 345);
            this.panel6.TabIndex = 22;
            // 
            // Grd2
            // 
            this.Grd2.BackColorEvenRows = System.Drawing.Color.WhiteSmoke;
            this.Grd2.BackColorOddRows = System.Drawing.Color.White;
            this.Grd2.CurCellBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.Grd2.DefaultAutoGroupRow.Height = 21;
            this.Grd2.DefaultRow.Height = 20;
            this.Grd2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd2.FrozenArea.ColCount = 3;
            this.Grd2.FrozenArea.SortFrozenRows = true;
            this.Grd2.GridLines.Mode = TenTec.Windows.iGridLib.iGGridLinesMode.None;
            this.Grd2.GroupBox.BackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd2.GroupBox.HintBackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd2.GroupBox.HintForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Grd2.GroupBox.Visible = true;
            this.Grd2.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd2.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd2.Header.UseXPStyles = false;
            this.Grd2.Location = new System.Drawing.Point(0, 34);
            this.Grd2.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.Grd2.Name = "Grd2";
            this.Grd2.ProcessTab = false;
            this.Grd2.RowMode = true;
            this.Grd2.RowModeHasCurCell = true;
            this.Grd2.RowTextStartColNear = 3;
            this.Grd2.ScrollBarSettings.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd2.SearchAsType.Mode = TenTec.Windows.iGridLib.iGSearchAsTypeMode.Seek;
            this.Grd2.SearchAsType.SearchCol = null;
            this.Grd2.SingleClickEdit = true;
            this.Grd2.Size = new System.Drawing.Size(766, 311);
            this.Grd2.TabIndex = 31;
            this.Grd2.TreeCol = null;
            this.Grd2.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd2.VScrollBar.Visibility = TenTec.Windows.iGridLib.iGScrollBarVisibility.Always;
            this.Grd2.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd2_RequestEdit);
            this.Grd2.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd2_EllipsisButtonClick);
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.TxtDrawingApprovalDocNo);
            this.panel7.Controls.Add(this.label12);
            this.panel7.Controls.Add(this.BtnDrawingApprovalDocNo2);
            this.panel7.Controls.Add(this.BtnDrawingApprovalDocNo);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel7.Location = new System.Drawing.Point(0, 0);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(766, 34);
            this.panel7.TabIndex = 30;
            // 
            // TxtDrawingApprovalDocNo
            // 
            this.TxtDrawingApprovalDocNo.EnterMoveNextControl = true;
            this.TxtDrawingApprovalDocNo.Location = new System.Drawing.Point(124, 7);
            this.TxtDrawingApprovalDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDrawingApprovalDocNo.Name = "TxtDrawingApprovalDocNo";
            this.TxtDrawingApprovalDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDrawingApprovalDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDrawingApprovalDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDrawingApprovalDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDrawingApprovalDocNo.Properties.MaxLength = 16;
            this.TxtDrawingApprovalDocNo.Properties.ReadOnly = true;
            this.TxtDrawingApprovalDocNo.Size = new System.Drawing.Size(211, 20);
            this.TxtDrawingApprovalDocNo.TabIndex = 27;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(8, 10);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(111, 14);
            this.label12.TabIndex = 26;
            this.label12.Text = "Drawing Approval#";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnDrawingApprovalDocNo2
            // 
            this.BtnDrawingApprovalDocNo2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDrawingApprovalDocNo2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDrawingApprovalDocNo2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDrawingApprovalDocNo2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDrawingApprovalDocNo2.Appearance.Options.UseBackColor = true;
            this.BtnDrawingApprovalDocNo2.Appearance.Options.UseFont = true;
            this.BtnDrawingApprovalDocNo2.Appearance.Options.UseForeColor = true;
            this.BtnDrawingApprovalDocNo2.Appearance.Options.UseTextOptions = true;
            this.BtnDrawingApprovalDocNo2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDrawingApprovalDocNo2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDrawingApprovalDocNo2.Image = ((System.Drawing.Image)(resources.GetObject("BtnDrawingApprovalDocNo2.Image")));
            this.BtnDrawingApprovalDocNo2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDrawingApprovalDocNo2.Location = new System.Drawing.Point(365, 7);
            this.BtnDrawingApprovalDocNo2.Name = "BtnDrawingApprovalDocNo2";
            this.BtnDrawingApprovalDocNo2.Size = new System.Drawing.Size(24, 21);
            this.BtnDrawingApprovalDocNo2.TabIndex = 29;
            this.BtnDrawingApprovalDocNo2.ToolTip = "Show Drawing Approval\'s Information";
            this.BtnDrawingApprovalDocNo2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDrawingApprovalDocNo2.ToolTipTitle = "Run System";
            this.BtnDrawingApprovalDocNo2.Click += new System.EventHandler(this.BtnDrawingApprovalDocNo2_Click);
            // 
            // BtnDrawingApprovalDocNo
            // 
            this.BtnDrawingApprovalDocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDrawingApprovalDocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDrawingApprovalDocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDrawingApprovalDocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDrawingApprovalDocNo.Appearance.Options.UseBackColor = true;
            this.BtnDrawingApprovalDocNo.Appearance.Options.UseFont = true;
            this.BtnDrawingApprovalDocNo.Appearance.Options.UseForeColor = true;
            this.BtnDrawingApprovalDocNo.Appearance.Options.UseTextOptions = true;
            this.BtnDrawingApprovalDocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDrawingApprovalDocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDrawingApprovalDocNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnDrawingApprovalDocNo.Image")));
            this.BtnDrawingApprovalDocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDrawingApprovalDocNo.Location = new System.Drawing.Point(340, 7);
            this.BtnDrawingApprovalDocNo.Name = "BtnDrawingApprovalDocNo";
            this.BtnDrawingApprovalDocNo.Size = new System.Drawing.Size(24, 21);
            this.BtnDrawingApprovalDocNo.TabIndex = 28;
            this.BtnDrawingApprovalDocNo.ToolTip = "Find Drawing Approval Document";
            this.BtnDrawingApprovalDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDrawingApprovalDocNo.ToolTipTitle = "Run System";
            this.BtnDrawingApprovalDocNo.Click += new System.EventHandler(this.BtnDrawingApprovalDocNo_Click);
            // 
            // Tp3
            // 
            this.Tp3.Appearance.Header.Options.UseTextOptions = true;
            this.Tp3.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp3.Controls.Add(this.panel4);
            this.Tp3.Name = "Tp3";
            this.Tp3.Size = new System.Drawing.Size(766, 345);
            this.Tp3.Text = "Attachment Files";
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel4.Controls.Add(this.TxtFileName);
            this.panel4.Controls.Add(this.PbUpload);
            this.panel4.Controls.Add(this.ChkFileName);
            this.panel4.Controls.Add(this.BtnBrowseFile);
            this.panel4.Controls.Add(this.label14);
            this.panel4.Controls.Add(this.BtnDownload);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(766, 345);
            this.panel4.TabIndex = 12;
            // 
            // TxtQty1
            // 
            this.TxtQty1.EnterMoveNextControl = true;
            this.TxtQty1.Location = new System.Drawing.Point(374, 236);
            this.TxtQty1.Margin = new System.Windows.Forms.Padding(5);
            this.TxtQty1.Name = "TxtQty1";
            this.TxtQty1.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtQty1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtQty1.Properties.Appearance.Options.UseBackColor = true;
            this.TxtQty1.Properties.Appearance.Options.UseFont = true;
            this.TxtQty1.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtQty1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtQty1.Properties.ReadOnly = true;
            this.TxtQty1.Size = new System.Drawing.Size(10, 20);
            this.TxtQty1.TabIndex = 53;
            // 
            // FrmBom3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 562);
            this.Name = "FrmBom3";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtQty.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtUomCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSOContractDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItCodeInternal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeSpecification.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProjectCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProjectName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFileName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFileName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCancelReason.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRevNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPONo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Tc1)).EndInit();
            this.Tc1.ResumeLayout(false);
            this.Tp1.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDeptCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBomGroupCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkServiceItemInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRevision.Properties)).EndInit();
            this.Tp2.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).EndInit();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDrawingApprovalDocNo.Properties)).EndInit();
            this.Tp3.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtQty1.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal DevExpress.XtraEditors.TextEdit TxtQty;
        private System.Windows.Forms.Label label9;
        internal DevExpress.XtraEditors.TextEdit TxtUomCode;
        public DevExpress.XtraEditors.SimpleButton BtnSOContractDocNo;
        internal DevExpress.XtraEditors.TextEdit TxtItName;
        private System.Windows.Forms.Label label3;
        internal DevExpress.XtraEditors.TextEdit TxtSOContractDocNo;
        private System.Windows.Forms.Label label6;
        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Label label1;
        internal DevExpress.XtraEditors.TextEdit TxtItCode;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtItCodeInternal;
        private System.Windows.Forms.Label LblItCodeInternal;
        private System.Windows.Forms.Label label4;
        internal DevExpress.XtraEditors.TextEdit TxtProjectCode;
        private System.Windows.Forms.Label label5;
        internal DevExpress.XtraEditors.TextEdit TxtProjectName;
        private System.Windows.Forms.Label label8;
        public DevExpress.XtraEditors.SimpleButton BtnSOContractDocNo2;
        private DevExpress.XtraEditors.CheckEdit ChkFileName;
        public DevExpress.XtraEditors.SimpleButton BtnDownload;
        private System.Windows.Forms.ProgressBar PbUpload;
        public DevExpress.XtraEditors.SimpleButton BtnBrowseFile;
        internal DevExpress.XtraEditors.TextEdit TxtFileName;
        private System.Windows.Forms.Label label14;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private System.Windows.Forms.Label label7;
        internal DevExpress.XtraEditors.MemoExEdit MeeSpecification;
        internal DevExpress.XtraEditors.TextEdit TxtRevNo;
        private System.Windows.Forms.Label label10;
        private DevExpress.XtraEditors.CheckEdit ChkCancelInd;
        private System.Windows.Forms.Label label27;
        private DevExpress.XtraEditors.MemoExEdit MeeCancelReason;
        public DevExpress.XtraEditors.SimpleButton BtnCopy;
        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.SaveFileDialog SFD;
        private System.Windows.Forms.OpenFileDialog OD;
        internal DevExpress.XtraEditors.TextEdit TxtPONo;
        private DevExpress.XtraTab.XtraTabControl Tc1;
        private DevExpress.XtraTab.XtraTabPage Tp1;
        private System.Windows.Forms.Panel panel5;
        private DevExpress.XtraTab.XtraTabPage Tp2;
        private System.Windows.Forms.Panel panel6;
        private DevExpress.XtraTab.XtraTabPage Tp3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label12;
        internal DevExpress.XtraEditors.TextEdit TxtDrawingApprovalDocNo;
        public DevExpress.XtraEditors.SimpleButton BtnDrawingApprovalDocNo;
        public DevExpress.XtraEditors.SimpleButton BtnDrawingApprovalDocNo2;
        internal DevExpress.XtraEditors.TextEdit TxtRevision;
        private System.Windows.Forms.Panel panel7;
        protected internal TenTec.Windows.iGridLib.iGrid Grd2;
        private System.Windows.Forms.Label label13;
        private DevExpress.XtraEditors.LookUpEdit LueBomGroupCode;
        internal DevExpress.XtraEditors.CheckEdit ChkServiceItemInd;
        private System.Windows.Forms.Label label15;
        private DevExpress.XtraEditors.LookUpEdit LueDeptCode;
        internal DevExpress.XtraEditors.TextEdit TxtStatus;
        private System.Windows.Forms.Label label16;
        internal DevExpress.XtraEditors.TextEdit TxtQty1;
    }
}