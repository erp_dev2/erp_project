﻿#region update

/*
    31/05/2022 [SET/PRODUCT] Menu dialog baru
    15/06/2022 [SET/PRODUCT] Penyesuaian
    28/06/2022 [SET/PRODUCT] menyesuaikan source data merujuk dari menu Type of Expenses
*/

#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmAcquisitionDebtSecuritiesDlg3 : RunSystem.FrmBase4
    {
        #region Field

        private FrmAcquisitionDebtSecurities mFrmParent;
        private string mSQL = string.Empty;
        private int mRow = 0;
        //private int mRow = 0;

        #endregion

        #region Constructor

        public FrmAcquisitionDebtSecuritiesDlg3(FrmAcquisitionDebtSecurities FrmParent, int Row) //(FrmTemplate FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mRow = Row;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                //Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            //SQL.AppendLine("SELECT A.DocCode, A.DocType, A.DocDt, A.ActInd, A.AcNo, B.AcDesc, A.Rate, A.Amt, A.Remark, A.TaxInd ");
            //SQL.AppendLine("FROM tblexpensestype A ");
            //SQL.AppendLine("INNER JOIN tblcoa B ON A.AcNo = B.AcNo ");
            //SQL.AppendLine("WHERE A.ActInd = 'Y' ");

            SQL.AppendLine("Select A.ExpensesCode DocCode, B.DocType, A.DocDt, A.ActInd, B.AcNo, C.AcDesc, B.Rate, B.Amt, B.Remark, B.TaxInd, B.DNo ");
            SQL.AppendLine("From TblExpensesTypeHdr A ");
            SQL.AppendLine("Inner Join TblExpensesTypeDtl B On A.ExpensesCode = B.ExpensesCode ");
            SQL.AppendLine("Inner Join TblCOA C On B.AcNo = C.AcNo ");
            SQL.AppendLine("Where A.ActInd = 'Y' ");
            SQL.AppendLine("And A.TransactionCode = @TransactionCode ");
            SQL.AppendLine("And Concat(A.ExpensesCode,'#',B.DNo) Not In (" + mFrmParent.GetSelectedExpenses() + ") ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 13;
            //Grd1.ReadOnly = true;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[]
                    {
                        //0
                        "No.",

                        //1-5
                        "",
                        "Document Type Code#",
                        "Document Type",
                        "Date",
                        "Active",

                        //6-10
                        "CoA Account#",
                        "CoA Name",
                        "Rate", 
                        "Amount",
                        "Remark",

                        //11-12
                        "TaxInd",
                        "DNo"
                    },
                     new int[]
                    {
                        //0
                        50,

                        //1-5
                        20, 150, 150, 100, 50, 

                        //6-10
                        100, 150, 100, 100, 150,

                        //11
                        0, 0
                    }
                );
            //Sm.GrdColButton(Grd1, new int[] { 2 });
            Sm.GrdColCheck(Grd1, new int[] { 1, 5, 11 });
            Sm.GrdFormatDate(Grd1, new int[] { 4 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 3, 4, 5, 6, 7, 8, 9, 10 });
            Sm.GrdFormatDec(Grd1, new int[] { 8, 9 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 2, 11, 12 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            //if (
            //    Sm.IsDteEmpty(DteDocDt1, "Start date") ||
            //    Sm.IsDteEmpty(DteDocDt2, "End date") ||
            //    Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
            //    ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@TransactionCode", mFrmParent.mMenuCode);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                Sm.FilterStr(ref Filter, ref cm, TxtDocTypeCode.Text, new string[] { "DocCode", "DocType" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.CreateDt;",
                        new string[]
                        {
                            //0
                            "DocCode", 

                            //1-5
                            "DocType", "DocDt", "ActInd", "AcNo", "AcDesc", 

                            //6-8
                            "Rate", "Amt", "Remark", "TaxInd", "DNo"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = false;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 10);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 3);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if(Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 1, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 2, Grd1, Row2, 3);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 4, Grd1, Row2, 6);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 5, Grd1, Row2, 7);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 10, Grd1, Row2, 8);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 11, Grd1, Row2, 9);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 12, Grd1, Row2, 11);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 13, Grd1, Row2, 12);
                        //mFrmParent.Grd2.Cells[Row1, 6].Value = mFrmParent.DteDocDt2.Text;

                        mFrmParent.Grd1.Rows.Add();
                        Sm.SetGrdNumValueZero(ref mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 8, 10, 11 });
                    }
                }
            }

            this.Close();
        }

        #endregion

        #region Event

        private void ChkCoAAcc_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "CoA Account");
        }

        private void TxtCoAAcc_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        #endregion
    }
}
