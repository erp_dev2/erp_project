﻿#region Update
/*
    02/07/2019 [DITA] Reporting Baru KIM 
    11/07/2019 [DITA] tambah kolom total price
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptIncomingPaymentTracker : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = "", mAccessInd = "", mSQL = "";

        #endregion

        #region Constructor

        public FrmRptIncomingPaymentTracker(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
            Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -30);
            SetGrd();
            Sl.SetLueCtCode(ref LueCtCode);
            base.FrmLoad(sender, e);
        }

        private string GetSQL(string Filter)
        {

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo , A.CtCode, G.CtName, A.DocDt, C.DocNo AS SalesInvoiceDocNo, C.CurCode, D.Qty, D.UPriceAfterTax, ");
            SQL.AppendLine("E.DocNo AS DOCtDocNo, F.ItCode, H.ItName, H.SalesUOMCode, I.ItCtCode, I.ItCtName ");
            SQL.AppendLine("From TblIncomingPaymentHdr A ");
            SQL.AppendLine("Inner Join TblIncomingPaymentDtl B On A.Docno = B.docno ");
            SQL.AppendLine("Inner Join TblSalesInvoiceHdr C On B.InvoiceDocNo = C.DocNo  ");
            SQL.AppendLine("Inner Join TblSalesInvoiceDtl D On C.DocNo = D.DocNo ");
            SQL.AppendLine("Inner Join TblDOctHdr E On D.DOCtDocNo= E.DocNo ");
            SQL.AppendLine("Inner Join TblDOctDtl F On F.DocNo = E.DocNo And D.DOCtDNo = F.DNo ");
            SQL.AppendLine("Inner Join TblCustomer G On G.CtCode = A.CtCode ");
            SQL.AppendLine("Inner Join TblItem H On H.ItCode = F.ItCode ");
            SQL.AppendLine("Inner Join TblItemCategory I On I.ItCtCode = H.ItCtCode ");
            SQL.AppendLine("Where D.DocType = '3' And A.DocDt Between @DocDt1 And @DocDt2 And A.CancelInd='N' ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("Order By A.DocDt, A.DocNo, B.DNo; ");

            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 19;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Customer"+Environment.NewLine+"Code",
                        "Customer"+Environment.NewLine+"Name",
                        "Incoming Payment#",
                        "",
                        "Date",
                        
                        //6-10
                        "Sales Invoice#",
                        "",
                        "DO to Customer#",
                        "",
                        "Item Category"+Environment.NewLine+"Code",

                        //11-15
                        "Item Category"+Environment.NewLine+"Name",
                        "Item Code",
                        "Item Name",
                        "Quantity",
                        "UoM",

                        //16-18
                        "Unit Price",
                        "Currency",
                        "Total Price"
                       
                    },
                    new int[] 
                    {
                        50,
                        100, 180, 180, 20, 80,
                        180, 20, 180, 20, 100,
                        200, 100, 150, 80, 80,
                        100, 80, 100
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 14, 16, 18 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 5 });
            Sm.GrdColButton(Grd1, new int[] { 4, 7, 9 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 5, 6, 8, 10, 11, 12, 13, 14, 15, 16, 17,18 }, false);
            Sm.GrdColInvisible(Grd1, new int[] { 1, 4, 7, 9, 10, 12  }, false);
            Sm.SetGrdProperty(Grd1, false);
            Grd1.Cols[18].Move(17);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 4, 7, 9, 10, 12 }, !ChkHideInfoInGrd.Checked);
           
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " ";

                var cm = new MySqlCommand();
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, new string[] { "A.DocNo"});
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCode), "A.CtCode", true);
             

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        GetSQL(Filter),
                        new string[]
                        { 
                            //0
                        "CtCode", 

                        //1-5
                        "CtName", "DocNo", "DocDt", "SalesInvoiceDocNo", "DOCtDocNo",

                        //6-10
                        "ItCtCode", "ItCtName", "ItCode", "ItName", "Qty",
 
                        //11-13
                        "SalesUOMCode", "UPriceAfterTax", "CurCode"
                        
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 8, 5);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 10, 6);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 11, 7);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 12, 8);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 13, 9);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 14, 10);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 15, 11);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 16, 12);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 17, 13);
                            Grd.Cells[Row, 18].Value = Sm.GetGrdDec(Grd1, Row, 14) * Sm.GetGrdDec(Grd1, Row, 16);

                        }, true, false, false, true
                    );
                Grd1.GroupObject.Add(3);
                Grd1.GroupObject.Add(6);
                Grd1.GroupObject.Add(8);
                Grd1.Group();
                AdjustSubtotals();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }

           
        }
        private void AdjustSubtotals()
        {
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ForeColor = Color.Black;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.HideSubtotals(Grd1);
            iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 14, 16, 18});
        }


        #endregion

        #region Misc Control Nethod

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event
        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }
        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }
        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 3).Length > 0)
            {
                var f = new FrmIncomingPayment(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 3);
                f.ShowDialog();
            }
            if (e.ColIndex == 7 && Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length > 0)
            {
                var f = new FrmSalesInvoice3(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                f.ShowDialog();
            }
            if (e.ColIndex == 9 && Sm.GetGrdStr(Grd1, e.RowIndex, 8).Length > 0)
            {
                var f = new FrmDOCt(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 8);
                f.ShowDialog();
            }
        }

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 3).Length > 0)
            {
                e.DoDefault = false;
                var f = new FrmIncomingPayment(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 3);
                f.ShowDialog();
            }
            if (e.ColIndex == 7 && Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length > 0)
            {
                e.DoDefault = false;
                var f = new FrmSalesInvoice3(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                f.ShowDialog();
            }
            if (e.ColIndex == 9 && Sm.GetGrdStr(Grd1, e.RowIndex, 8).Length > 0)
            {
                e.DoDefault = false;
                var f = new FrmDOCt(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 8);
                f.ShowDialog();
            }
        }

        #endregion

        

        #endregion

      
      

    }
}
