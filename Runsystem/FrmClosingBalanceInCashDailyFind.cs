﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmClosingBalanceInCashDailyFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmClosingBalanceInCashDaily mFrmParent;

        #endregion

        #region Constructor

        public FrmClosingBalanceInCashDailyFind(FrmClosingBalanceInCashDaily FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sl.SetLueYr(LueYr, "");
                Sl.SetLueMth(LueMth);
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 16;
            Grd1.FrozenArea.ColCount = 4;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#", 
                        "Closing"+Environment.NewLine+"Date",
                        "Year",
                        "Month",
                        "Bank Account",
                       
                        
                        //6-10
                        "Bank Account Name",
                        "Amount",
                        "Entity",
                        "Description",
                        "Created"+Environment.NewLine+"By",   

                        //11-15
                        "Created"+Environment.NewLine+"Date",  
                        "Created"+Environment.NewLine+"Time", 
                        "Last"+Environment.NewLine+"Updated By", 
                        "Last"+Environment.NewLine+"Updated Date", 
                        "Last"+Environment.NewLine+"Updated Time"
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 2, 11, 14 });
            Sm.GrdFormatTime(Grd1, new int[] { 12, 15 });
            Sm.GrdFormatDec(Grd1, new int[] { 7 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 5, 8, 9, 10, 11, 12, 13, 14, 15 }, false);
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 5, 9, 10, 11, 12, 13, 14, 15 }, !ChkHideInfoInGrd.Checked);
            if (mFrmParent.mIsEntityMandatory) Sm.GrdColInvisible(Grd1, new int[] { 8 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdAutoSize(Grd1);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();
                var SQL = new StringBuilder();

                SQL.AppendLine("Select * From ( ");
                SQL.AppendLine("    Select A.DocNo, A.DocDt, Left(A.ClosingDt, 4) As Yr, Substring(A.ClosingDt, 5, 2) MthNum, ");
                SQL.AppendLine("    Case When Substring(A.ClosingDt, 5, 2) = '01' Then 'January'  ");
                SQL.AppendLine("    When Substring(A.ClosingDt, 5, 2) = '02' Then 'Febuary'  ");
                SQL.AppendLine("    When Substring(A.ClosingDt, 5, 2) = '03' Then 'March'  ");
                SQL.AppendLine("    When Substring(A.ClosingDt, 5, 2) = '04' Then 'April'  ");
                SQL.AppendLine("    When Substring(A.ClosingDt, 5, 2) = '05' Then 'May'  ");
                SQL.AppendLine("    When Substring(A.ClosingDt, 5, 2) = '06' Then 'June'  ");
                SQL.AppendLine("    When Substring(A.ClosingDt, 5, 2) = '07' Then 'July'  ");
                SQL.AppendLine("    When Substring(A.ClosingDt, 5, 2) = '08' Then 'August' ");
                SQL.AppendLine("    When Substring(A.ClosingDt, 5, 2) = '09' Then 'September'  ");
                SQL.AppendLine("    When Substring(A.ClosingDt, 5, 2) = '10' Then 'October'  ");
                SQL.AppendLine("    When Substring(A.ClosingDt, 5, 2) = '11' Then 'November'  ");
                SQL.AppendLine("    When Substring(A.ClosingDt, 5, 2) = '12' Then 'December'  ");
                SQL.AppendLine("    End As Mth, ");
                SQL.AppendLine("    B.BankAcCode, Concat(C.BankAcNm, C.bankAcNo) As BankAcNm, B.Amt, ");
                SQL.AppendLine("    E.EntName, ");
                SQL.AppendLine("    Concat( ");
                SQL.AppendLine("        Case When D.BankName Is Not Null Then Concat(D.BankName, ' : ') Else '' End, ");
                SQL.AppendLine("        Case When C.BankAcNo Is Not Null  ");
                SQL.AppendLine("        Then Concat(C.BankAcNo, ' [', IfNull(C.BankAcNm, ''), ']') ");
                SQL.AppendLine("        Else IfNull(C.BankAcNm, '') End ");
                SQL.AppendLine("    ) As BankAcDesc, ");
                SQL.AppendLine("    B.CreateBy, B.CreateDt, B.LastUpBy, B.LastUpDt ");
                SQL.AppendLine("    From TblClosingBalanceInCashDailyHdr A ");
                SQL.AppendLine("    Inner Join TblClosingbalanceInCashDailyDtl B On A.DocNo = B.DocNo ");
                SQL.AppendLine("    Inner Join TblBankAccount C On B.BankAcCode = C.BankAcCode And C.HiddenInd='N' ");
                if (mFrmParent.mIsFilterByBankAccount)
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select BankAcCode From TblGroupBankAccount ");
                    SQL.AppendLine("    Where BankAcCode=C.BankAcCode ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode=@UserCode ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine(") ");
                }
                SQL.AppendLine("    Left Join TblBank D On C.BankCode=D.BankCode ");
                SQL.AppendLine("    Left Join TblEntity E On C.EntCode=E.EntCode ");
                SQL.AppendLine(")T  ");

                string Filter = "Where 0=0 ";
                

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtBankAcCode.Text, new string[] { "T.BankAcCode", "T.BankAcNm" });
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "T.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueYr), "T.Yr", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueMth), "T.MthNum", true);
                
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,SQL.ToString() + Filter + " Order By T.DocNo;",
                        new string[]
                        {
                            //0
                            "DocNo", 
                                
                            //1-5
                            "DocDt", "Yr", "Mth", "BankAcCode", "BankAcNm",    
                            
                            //6-10
                            "Amt", "EntName", "BankAcDesc", "CreateBy", "CreateDt", 
                            
                            //11-12
                            "LastUpBy", "LastUpDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 12, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 11);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 14, 12);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 15, 12);
                        }, true, false, false, true
                    );
                Grd1.GroupObject.Add(1);
                Grd1.Group();
                AdjustSubtotals();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        private void AdjustSubtotals()
        {
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ForeColor = Color.Black;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.HideSubtotals(Grd1);
            iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 7 });
        }


        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #endregion

        #region Event

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document number");
        }

        private void ChkBankAcCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Bank Account");
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LueYr_EditValueChanged(object sender, EventArgs e)
        {
           
        }

        private void LueMth_EditValueChanged(object sender, EventArgs e)
        {
           
        }

        private void TxtBankAcCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }
        #endregion
    }
}
