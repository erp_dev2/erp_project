﻿namespace RunSystem
{
    partial class FrmPORevision2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmPORevision2));
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.ChkCancelInd = new DevExpress.XtraEditors.CheckEdit();
            this.TxtStatus = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtPODocNo = new DevExpress.XtraEditors.TextEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtQtOld = new DevExpress.XtraEditors.TextEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtDiscountAmtOld = new DevExpress.XtraEditors.TextEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.TxtDiscountAmtNew = new DevExpress.XtraEditors.TextEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtQtNew = new DevExpress.XtraEditors.TextEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.TxtItCode = new DevExpress.XtraEditors.TextEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.TxtQtyOld = new DevExpress.XtraEditors.TextEdit();
            this.label11 = new System.Windows.Forms.Label();
            this.TxtTotalOld = new DevExpress.XtraEditors.TextEdit();
            this.label13 = new System.Windows.Forms.Label();
            this.TxtTotalNew = new DevExpress.XtraEditors.TextEdit();
            this.label14 = new System.Windows.Forms.Label();
            this.TxtQtyNew = new DevExpress.XtraEditors.TextEdit();
            this.LblNewQty = new System.Windows.Forms.Label();
            this.BtnQtNew = new DevExpress.XtraEditors.SimpleButton();
            this.BtnPODocNo = new DevExpress.XtraEditors.SimpleButton();
            this.TxtPORDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label15 = new System.Windows.Forms.Label();
            this.TxtUnitPriceOld = new DevExpress.XtraEditors.TextEdit();
            this.label16 = new System.Windows.Forms.Label();
            this.TxtUnitPriceNew = new DevExpress.XtraEditors.TextEdit();
            this.label17 = new System.Windows.Forms.Label();
            this.TxtDiscOld = new DevExpress.XtraEditors.TextEdit();
            this.label18 = new System.Windows.Forms.Label();
            this.TxtDiscNew = new DevExpress.XtraEditors.TextEdit();
            this.label19 = new System.Windows.Forms.Label();
            this.TxtRoundingValueOld = new DevExpress.XtraEditors.TextEdit();
            this.label20 = new System.Windows.Forms.Label();
            this.TxtRoundingValueNew = new DevExpress.XtraEditors.TextEdit();
            this.label21 = new System.Windows.Forms.Label();
            this.TxtVdCode = new DevExpress.XtraEditors.TextEdit();
            this.label22 = new System.Windows.Forms.Label();
            this.Grd1 = new TenTec.Windows.iGridLib.iGrid();
            this.BtnPODocNo2 = new DevExpress.XtraEditors.SimpleButton();
            this.TxtLocalDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label23 = new System.Windows.Forms.Label();
            this.MeePORemarkHdrOld = new DevExpress.XtraEditors.MemoExEdit();
            this.label24 = new System.Windows.Forms.Label();
            this.MeePORemarkDtlOld = new DevExpress.XtraEditors.MemoExEdit();
            this.label25 = new System.Windows.Forms.Label();
            this.MeePORemarkDtlNew = new DevExpress.XtraEditors.MemoExEdit();
            this.label26 = new System.Windows.Forms.Label();
            this.MeePORemarkHdrNew = new DevExpress.XtraEditors.MemoExEdit();
            this.label27 = new System.Windows.Forms.Label();
            this.BtnPORequestDocNo = new DevExpress.XtraEditors.SimpleButton();
            this.Tc1 = new DevExpress.XtraTab.XtraTabControl();
            this.Tp1 = new DevExpress.XtraTab.XtraTabPage();
            this.TxtAddendumAvailability = new DevExpress.XtraEditors.TextEdit();
            this.LblAddendumAvailability = new System.Windows.Forms.Label();
            this.TxtGrandTotal = new DevExpress.XtraEditors.TextEdit();
            this.LblGrandTotal = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.DteEstTimeArrivalNew = new DevExpress.XtraEditors.DateEdit();
            this.DteEstTimeArrivalOld = new DevExpress.XtraEditors.DateEdit();
            this.BtnPOServiceDocNo = new DevExpress.XtraEditors.SimpleButton();
            this.label31 = new System.Windows.Forms.Label();
            this.TxtPOServiceDocNo = new DevExpress.XtraEditors.TextEdit();
            this.Tp4 = new DevExpress.XtraTab.XtraTabPage();
            this.ChkFile = new DevExpress.XtraEditors.CheckEdit();
            this.BtnDownload = new DevExpress.XtraEditors.SimpleButton();
            this.PbUpload = new System.Windows.Forms.ProgressBar();
            this.BtnFile = new DevExpress.XtraEditors.SimpleButton();
            this.TxtFile = new DevExpress.XtraEditors.TextEdit();
            this.label34 = new System.Windows.Forms.Label();
            this.Tp2 = new DevExpress.XtraTab.XtraTabPage();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.LueTaxCode = new DevExpress.XtraEditors.LookUpEdit();
            this.DteEstRecvDt = new DevExpress.XtraEditors.DateEdit();
            this.label28 = new System.Windows.Forms.Label();
            this.Tp3 = new DevExpress.XtraTab.XtraTabPage();
            this.panel3 = new System.Windows.Forms.Panel();
            this.OD = new System.Windows.Forms.OpenFileDialog();
            this.SFD = new System.Windows.Forms.SaveFileDialog();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPODocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtQtOld.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDiscountAmtOld.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDiscountAmtNew.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtQtNew.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtQtyOld.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalOld.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalNew.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtQtyNew.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPORDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtUnitPriceOld.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtUnitPriceNew.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDiscOld.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDiscNew.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRoundingValueOld.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRoundingValueNew.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVdCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLocalDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeePORemarkHdrOld.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeePORemarkDtlOld.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeePORemarkDtlNew.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeePORemarkHdrNew.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Tc1)).BeginInit();
            this.Tc1.SuspendLayout();
            this.Tp1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAddendumAvailability.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGrandTotal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteEstTimeArrivalNew.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteEstTimeArrivalNew.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteEstTimeArrivalOld.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteEstTimeArrivalOld.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPOServiceDocNo.Properties)).BeginInit();
            this.Tp4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile.Properties)).BeginInit();
            this.Tp2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueTaxCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteEstRecvDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteEstRecvDt.Properties)).BeginInit();
            this.Tp3.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(794, 0);
            this.panel1.Size = new System.Drawing.Size(70, 496);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.Tc1);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Size = new System.Drawing.Size(794, 496);
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(86, 27);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(129, 20);
            this.DteDocDt.TabIndex = 12;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(47, 30);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 14);
            this.label2.TabIndex = 11;
            this.label2.Text = "Date";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(86, 6);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 30;
            this.TxtDocNo.Properties.ReadOnly = true;
            this.TxtDocNo.Size = new System.Drawing.Size(237, 20);
            this.TxtDocNo.TabIndex = 10;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(7, 9);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 14);
            this.label1.TabIndex = 9;
            this.label1.Text = "Document#";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkCancelInd
            // 
            this.ChkCancelInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkCancelInd.Location = new System.Drawing.Point(256, 48);
            this.ChkCancelInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkCancelInd.Name = "ChkCancelInd";
            this.ChkCancelInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkCancelInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkCancelInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkCancelInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseFont = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkCancelInd.Properties.Caption = "Cancel";
            this.ChkCancelInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkCancelInd.ShowToolTips = false;
            this.ChkCancelInd.Size = new System.Drawing.Size(67, 22);
            this.ChkCancelInd.TabIndex = 15;
            this.ChkCancelInd.Visible = false;
            // 
            // TxtStatus
            // 
            this.TxtStatus.EditValue = "";
            this.TxtStatus.EnterMoveNextControl = true;
            this.TxtStatus.Location = new System.Drawing.Point(86, 49);
            this.TxtStatus.Margin = new System.Windows.Forms.Padding(5);
            this.TxtStatus.Name = "TxtStatus";
            this.TxtStatus.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtStatus.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtStatus.Properties.Appearance.Options.UseBackColor = true;
            this.TxtStatus.Properties.Appearance.Options.UseFont = true;
            this.TxtStatus.Properties.MaxLength = 16;
            this.TxtStatus.Properties.ReadOnly = true;
            this.TxtStatus.Size = new System.Drawing.Size(166, 20);
            this.TxtStatus.TabIndex = 14;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(36, 52);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 14);
            this.label3.TabIndex = 13;
            this.label3.Text = "Status";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPODocNo
            // 
            this.TxtPODocNo.EnterMoveNextControl = true;
            this.TxtPODocNo.Location = new System.Drawing.Point(207, 28);
            this.TxtPODocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPODocNo.Name = "TxtPODocNo";
            this.TxtPODocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtPODocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPODocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPODocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtPODocNo.Properties.MaxLength = 30;
            this.TxtPODocNo.Properties.ReadOnly = true;
            this.TxtPODocNo.Size = new System.Drawing.Size(166, 20);
            this.TxtPODocNo.TabIndex = 20;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(171, 31);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(32, 14);
            this.label4.TabIndex = 19;
            this.label4.Text = "PO#";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtQtOld
            // 
            this.TxtQtOld.EnterMoveNextControl = true;
            this.TxtQtOld.Location = new System.Drawing.Point(207, 139);
            this.TxtQtOld.Margin = new System.Windows.Forms.Padding(5);
            this.TxtQtOld.Name = "TxtQtOld";
            this.TxtQtOld.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtQtOld.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtQtOld.Properties.Appearance.Options.UseBackColor = true;
            this.TxtQtOld.Properties.Appearance.Options.UseFont = true;
            this.TxtQtOld.Properties.MaxLength = 30;
            this.TxtQtOld.Properties.ReadOnly = true;
            this.TxtQtOld.Size = new System.Drawing.Size(166, 20);
            this.TxtQtOld.TabIndex = 35;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(83, 142);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(120, 14);
            this.label5.TabIndex = 34;
            this.label5.Text = "Previous Quotation#";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDiscountAmtOld
            // 
            this.TxtDiscountAmtOld.EnterMoveNextControl = true;
            this.TxtDiscountAmtOld.Location = new System.Drawing.Point(207, 249);
            this.TxtDiscountAmtOld.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDiscountAmtOld.Name = "TxtDiscountAmtOld";
            this.TxtDiscountAmtOld.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDiscountAmtOld.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDiscountAmtOld.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDiscountAmtOld.Properties.Appearance.Options.UseFont = true;
            this.TxtDiscountAmtOld.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtDiscountAmtOld.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtDiscountAmtOld.Properties.ReadOnly = true;
            this.TxtDiscountAmtOld.Size = new System.Drawing.Size(166, 20);
            this.TxtDiscountAmtOld.TabIndex = 45;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(74, 253);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(129, 14);
            this.label9.TabIndex = 44;
            this.label9.Text = "Previous Disc. Amount";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDiscountAmtNew
            // 
            this.TxtDiscountAmtNew.EnterMoveNextControl = true;
            this.TxtDiscountAmtNew.Location = new System.Drawing.Point(578, 250);
            this.TxtDiscountAmtNew.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDiscountAmtNew.Name = "TxtDiscountAmtNew";
            this.TxtDiscountAmtNew.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDiscountAmtNew.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDiscountAmtNew.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDiscountAmtNew.Properties.Appearance.Options.UseFont = true;
            this.TxtDiscountAmtNew.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtDiscountAmtNew.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtDiscountAmtNew.Size = new System.Drawing.Size(166, 20);
            this.TxtDiscountAmtNew.TabIndex = 81;
            this.TxtDiscountAmtNew.Validated += new System.EventHandler(this.TxtDiscountAmtNew_Validated);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Red;
            this.label6.Location = new System.Drawing.Point(465, 253);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(109, 14);
            this.label6.TabIndex = 80;
            this.label6.Text = "New Disc. Amount";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtQtNew
            // 
            this.TxtQtNew.EnterMoveNextControl = true;
            this.TxtQtNew.Location = new System.Drawing.Point(578, 140);
            this.TxtQtNew.Margin = new System.Windows.Forms.Padding(5);
            this.TxtQtNew.Name = "TxtQtNew";
            this.TxtQtNew.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtQtNew.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtQtNew.Properties.Appearance.Options.UseBackColor = true;
            this.TxtQtNew.Properties.Appearance.Options.UseFont = true;
            this.TxtQtNew.Properties.MaxLength = 30;
            this.TxtQtNew.Properties.ReadOnly = true;
            this.TxtQtNew.Size = new System.Drawing.Size(166, 20);
            this.TxtQtNew.TabIndex = 71;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Red;
            this.label7.Location = new System.Drawing.Point(474, 143);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(100, 14);
            this.label7.TabIndex = 70;
            this.label7.Text = "New Quotation#";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(206, 359);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 400;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(550, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(538, 20);
            this.MeeRemark.TabIndex = 55;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(156, 362);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(47, 14);
            this.label8.TabIndex = 54;
            this.label8.Text = "Remark";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtItCode
            // 
            this.TxtItCode.EnterMoveNextControl = true;
            this.TxtItCode.Location = new System.Drawing.Point(207, 95);
            this.TxtItCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtItCode.Name = "TxtItCode";
            this.TxtItCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtItCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtItCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtItCode.Properties.Appearance.Options.UseFont = true;
            this.TxtItCode.Properties.MaxLength = 250;
            this.TxtItCode.Properties.ReadOnly = true;
            this.TxtItCode.Size = new System.Drawing.Size(538, 20);
            this.TxtItCode.TabIndex = 30;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(135, 98);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(68, 14);
            this.label10.TabIndex = 29;
            this.label10.Text = "Item Name";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtQtyOld
            // 
            this.TxtQtyOld.EnterMoveNextControl = true;
            this.TxtQtyOld.Location = new System.Drawing.Point(207, 161);
            this.TxtQtyOld.Margin = new System.Windows.Forms.Padding(5);
            this.TxtQtyOld.Name = "TxtQtyOld";
            this.TxtQtyOld.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtQtyOld.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtQtyOld.Properties.Appearance.Options.UseBackColor = true;
            this.TxtQtyOld.Properties.Appearance.Options.UseFont = true;
            this.TxtQtyOld.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtQtyOld.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtQtyOld.Properties.ReadOnly = true;
            this.TxtQtyOld.Size = new System.Drawing.Size(166, 20);
            this.TxtQtyOld.TabIndex = 37;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(149, 165);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(54, 14);
            this.label11.TabIndex = 36;
            this.label11.Text = "Quantity";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotalOld
            // 
            this.TxtTotalOld.EnterMoveNextControl = true;
            this.TxtTotalOld.Location = new System.Drawing.Point(207, 271);
            this.TxtTotalOld.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotalOld.Name = "TxtTotalOld";
            this.TxtTotalOld.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtTotalOld.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotalOld.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotalOld.Properties.Appearance.Options.UseFont = true;
            this.TxtTotalOld.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotalOld.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotalOld.Properties.ReadOnly = true;
            this.TxtTotalOld.Size = new System.Drawing.Size(166, 20);
            this.TxtTotalOld.TabIndex = 47;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(103, 275);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(100, 14);
            this.label13.TabIndex = 46;
            this.label13.Text = "Previous Amount";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotalNew
            // 
            this.TxtTotalNew.EnterMoveNextControl = true;
            this.TxtTotalNew.Location = new System.Drawing.Point(578, 272);
            this.TxtTotalNew.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotalNew.Name = "TxtTotalNew";
            this.TxtTotalNew.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotalNew.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotalNew.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotalNew.Properties.Appearance.Options.UseFont = true;
            this.TxtTotalNew.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotalNew.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotalNew.Size = new System.Drawing.Size(166, 20);
            this.TxtTotalNew.TabIndex = 83;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(494, 275);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(80, 14);
            this.label14.TabIndex = 82;
            this.label14.Text = "New Amount";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtQtyNew
            // 
            this.TxtQtyNew.EnterMoveNextControl = true;
            this.TxtQtyNew.Location = new System.Drawing.Point(578, 162);
            this.TxtQtyNew.Margin = new System.Windows.Forms.Padding(5);
            this.TxtQtyNew.Name = "TxtQtyNew";
            this.TxtQtyNew.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtQtyNew.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtQtyNew.Properties.Appearance.Options.UseBackColor = true;
            this.TxtQtyNew.Properties.Appearance.Options.UseFont = true;
            this.TxtQtyNew.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtQtyNew.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtQtyNew.Size = new System.Drawing.Size(166, 20);
            this.TxtQtyNew.TabIndex = 74;
            this.TxtQtyNew.Validated += new System.EventHandler(this.TxtQtyNew_Validated);
            // 
            // LblNewQty
            // 
            this.LblNewQty.AutoSize = true;
            this.LblNewQty.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblNewQty.ForeColor = System.Drawing.Color.Black;
            this.LblNewQty.Location = new System.Drawing.Point(491, 165);
            this.LblNewQty.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblNewQty.Name = "LblNewQty";
            this.LblNewQty.Size = new System.Drawing.Size(83, 14);
            this.LblNewQty.TabIndex = 73;
            this.LblNewQty.Text = "New Quantity";
            this.LblNewQty.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnQtNew
            // 
            this.BtnQtNew.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnQtNew.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnQtNew.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnQtNew.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnQtNew.Appearance.Options.UseBackColor = true;
            this.BtnQtNew.Appearance.Options.UseFont = true;
            this.BtnQtNew.Appearance.Options.UseForeColor = true;
            this.BtnQtNew.Appearance.Options.UseTextOptions = true;
            this.BtnQtNew.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnQtNew.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnQtNew.Image = ((System.Drawing.Image)(resources.GetObject("BtnQtNew.Image")));
            this.BtnQtNew.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnQtNew.Location = new System.Drawing.Point(748, 139);
            this.BtnQtNew.Name = "BtnQtNew";
            this.BtnQtNew.Size = new System.Drawing.Size(24, 21);
            this.BtnQtNew.TabIndex = 72;
            this.BtnQtNew.ToolTip = "Find Vendor\'s Quotation";
            this.BtnQtNew.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnQtNew.ToolTipTitle = "Run System";
            this.BtnQtNew.Click += new System.EventHandler(this.BtnQtNew_Click);
            // 
            // BtnPODocNo
            // 
            this.BtnPODocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnPODocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPODocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPODocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPODocNo.Appearance.Options.UseBackColor = true;
            this.BtnPODocNo.Appearance.Options.UseFont = true;
            this.BtnPODocNo.Appearance.Options.UseForeColor = true;
            this.BtnPODocNo.Appearance.Options.UseTextOptions = true;
            this.BtnPODocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnPODocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnPODocNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnPODocNo.Image")));
            this.BtnPODocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnPODocNo.Location = new System.Drawing.Point(379, 26);
            this.BtnPODocNo.Name = "BtnPODocNo";
            this.BtnPODocNo.Size = new System.Drawing.Size(24, 21);
            this.BtnPODocNo.TabIndex = 21;
            this.BtnPODocNo.ToolTip = "Find PO";
            this.BtnPODocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnPODocNo.ToolTipTitle = "Run System";
            this.BtnPODocNo.Click += new System.EventHandler(this.BtnPODocNo_Click);
            // 
            // TxtPORDocNo
            // 
            this.TxtPORDocNo.EnterMoveNextControl = true;
            this.TxtPORDocNo.Location = new System.Drawing.Point(207, 50);
            this.TxtPORDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPORDocNo.Name = "TxtPORDocNo";
            this.TxtPORDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtPORDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPORDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPORDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtPORDocNo.Properties.MaxLength = 30;
            this.TxtPORDocNo.Properties.ReadOnly = true;
            this.TxtPORDocNo.Size = new System.Drawing.Size(166, 20);
            this.TxtPORDocNo.TabIndex = 24;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(122, 53);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(81, 14);
            this.label15.TabIndex = 23;
            this.label15.Text = "PO Request#";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtUnitPriceOld
            // 
            this.TxtUnitPriceOld.EnterMoveNextControl = true;
            this.TxtUnitPriceOld.Location = new System.Drawing.Point(207, 183);
            this.TxtUnitPriceOld.Margin = new System.Windows.Forms.Padding(5);
            this.TxtUnitPriceOld.Name = "TxtUnitPriceOld";
            this.TxtUnitPriceOld.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtUnitPriceOld.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUnitPriceOld.Properties.Appearance.Options.UseBackColor = true;
            this.TxtUnitPriceOld.Properties.Appearance.Options.UseFont = true;
            this.TxtUnitPriceOld.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtUnitPriceOld.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtUnitPriceOld.Properties.ReadOnly = true;
            this.TxtUnitPriceOld.Size = new System.Drawing.Size(166, 20);
            this.TxtUnitPriceOld.TabIndex = 39;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(95, 186);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(108, 14);
            this.label16.TabIndex = 38;
            this.label16.Text = "Previous Unit Price";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtUnitPriceNew
            // 
            this.TxtUnitPriceNew.EnterMoveNextControl = true;
            this.TxtUnitPriceNew.Location = new System.Drawing.Point(578, 184);
            this.TxtUnitPriceNew.Margin = new System.Windows.Forms.Padding(5);
            this.TxtUnitPriceNew.Name = "TxtUnitPriceNew";
            this.TxtUnitPriceNew.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtUnitPriceNew.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUnitPriceNew.Properties.Appearance.Options.UseBackColor = true;
            this.TxtUnitPriceNew.Properties.Appearance.Options.UseFont = true;
            this.TxtUnitPriceNew.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtUnitPriceNew.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtUnitPriceNew.Size = new System.Drawing.Size(166, 20);
            this.TxtUnitPriceNew.TabIndex = 76;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(486, 187);
            this.label17.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(88, 14);
            this.label17.TabIndex = 75;
            this.label17.Text = "New Unit Price";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDiscOld
            // 
            this.TxtDiscOld.EnterMoveNextControl = true;
            this.TxtDiscOld.Location = new System.Drawing.Point(207, 205);
            this.TxtDiscOld.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDiscOld.Name = "TxtDiscOld";
            this.TxtDiscOld.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDiscOld.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDiscOld.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDiscOld.Properties.Appearance.Options.UseFont = true;
            this.TxtDiscOld.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtDiscOld.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtDiscOld.Properties.ReadOnly = true;
            this.TxtDiscOld.Size = new System.Drawing.Size(166, 20);
            this.TxtDiscOld.TabIndex = 41;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(100, 208);
            this.label18.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(103, 14);
            this.label18.TabIndex = 40;
            this.label18.Text = "Previous Discount";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDiscNew
            // 
            this.TxtDiscNew.EnterMoveNextControl = true;
            this.TxtDiscNew.Location = new System.Drawing.Point(578, 206);
            this.TxtDiscNew.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDiscNew.Name = "TxtDiscNew";
            this.TxtDiscNew.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDiscNew.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDiscNew.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDiscNew.Properties.Appearance.Options.UseFont = true;
            this.TxtDiscNew.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtDiscNew.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtDiscNew.Size = new System.Drawing.Size(166, 20);
            this.TxtDiscNew.TabIndex = 78;
            this.TxtDiscNew.Validated += new System.EventHandler(this.TxtDiscNew_Validated);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Red;
            this.label19.Location = new System.Drawing.Point(491, 209);
            this.label19.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(83, 14);
            this.label19.TabIndex = 77;
            this.label19.Text = "New Discount";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtRoundingValueOld
            // 
            this.TxtRoundingValueOld.EnterMoveNextControl = true;
            this.TxtRoundingValueOld.Location = new System.Drawing.Point(207, 227);
            this.TxtRoundingValueOld.Margin = new System.Windows.Forms.Padding(5);
            this.TxtRoundingValueOld.Name = "TxtRoundingValueOld";
            this.TxtRoundingValueOld.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtRoundingValueOld.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtRoundingValueOld.Properties.Appearance.Options.UseBackColor = true;
            this.TxtRoundingValueOld.Properties.Appearance.Options.UseFont = true;
            this.TxtRoundingValueOld.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtRoundingValueOld.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtRoundingValueOld.Properties.ReadOnly = true;
            this.TxtRoundingValueOld.Size = new System.Drawing.Size(166, 20);
            this.TxtRoundingValueOld.TabIndex = 43;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(111, 230);
            this.label20.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(92, 14);
            this.label20.TabIndex = 42;
            this.label20.Text = "Rounding Value";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtRoundingValueNew
            // 
            this.TxtRoundingValueNew.EnterMoveNextControl = true;
            this.TxtRoundingValueNew.Location = new System.Drawing.Point(578, 228);
            this.TxtRoundingValueNew.Margin = new System.Windows.Forms.Padding(5);
            this.TxtRoundingValueNew.Name = "TxtRoundingValueNew";
            this.TxtRoundingValueNew.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtRoundingValueNew.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtRoundingValueNew.Properties.Appearance.Options.UseBackColor = true;
            this.TxtRoundingValueNew.Properties.Appearance.Options.UseFont = true;
            this.TxtRoundingValueNew.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtRoundingValueNew.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtRoundingValueNew.Size = new System.Drawing.Size(166, 20);
            this.TxtRoundingValueNew.TabIndex = 58;
            this.TxtRoundingValueNew.Validated += new System.EventHandler(this.TxtRoundingValueNew_Validated);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Black;
            this.label21.Location = new System.Drawing.Point(482, 231);
            this.label21.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(92, 14);
            this.label21.TabIndex = 79;
            this.label21.Text = "Rounding Value";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtVdCode
            // 
            this.TxtVdCode.EnterMoveNextControl = true;
            this.TxtVdCode.Location = new System.Drawing.Point(207, 117);
            this.TxtVdCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtVdCode.Name = "TxtVdCode";
            this.TxtVdCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtVdCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtVdCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtVdCode.Properties.Appearance.Options.UseFont = true;
            this.TxtVdCode.Properties.MaxLength = 250;
            this.TxtVdCode.Properties.ReadOnly = true;
            this.TxtVdCode.Size = new System.Drawing.Size(538, 20);
            this.TxtVdCode.TabIndex = 33;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(156, 120);
            this.label22.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(47, 14);
            this.label22.TabIndex = 32;
            this.label22.Text = "Vendor";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Grd1
            // 
            this.Grd1.BackColorEvenRows = System.Drawing.Color.WhiteSmoke;
            this.Grd1.BackColorOddRows = System.Drawing.Color.White;
            this.Grd1.DefaultAutoGroupRow.Height = 21;
            this.Grd1.DefaultRow.Height = 20;
            this.Grd1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd1.FrozenArea.ColCount = 3;
            this.Grd1.FrozenArea.SortFrozenRows = true;
            this.Grd1.GroupBox.BackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintBackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Grd1.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.UseXPStyles = false;
            this.Grd1.Location = new System.Drawing.Point(0, 0);
            this.Grd1.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.Grd1.Name = "Grd1";
            this.Grd1.ProcessTab = false;
            this.Grd1.ReadOnly = true;
            this.Grd1.RowTextStartColNear = 3;
            this.Grd1.ScrollBarSettings.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.SearchAsType.Mode = TenTec.Windows.iGridLib.iGSearchAsTypeMode.Seek;
            this.Grd1.SearchAsType.SearchCol = null;
            this.Grd1.SingleClickEdit = true;
            this.Grd1.Size = new System.Drawing.Size(766, 131);
            this.Grd1.TabIndex = 17;
            this.Grd1.TreeCol = null;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // BtnPODocNo2
            // 
            this.BtnPODocNo2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnPODocNo2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPODocNo2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPODocNo2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPODocNo2.Appearance.Options.UseBackColor = true;
            this.BtnPODocNo2.Appearance.Options.UseFont = true;
            this.BtnPODocNo2.Appearance.Options.UseForeColor = true;
            this.BtnPODocNo2.Appearance.Options.UseTextOptions = true;
            this.BtnPODocNo2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnPODocNo2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnPODocNo2.Image = ((System.Drawing.Image)(resources.GetObject("BtnPODocNo2.Image")));
            this.BtnPODocNo2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnPODocNo2.Location = new System.Drawing.Point(406, 26);
            this.BtnPODocNo2.Name = "BtnPODocNo2";
            this.BtnPODocNo2.Size = new System.Drawing.Size(24, 21);
            this.BtnPODocNo2.TabIndex = 22;
            this.BtnPODocNo2.ToolTip = "Show Data PO";
            this.BtnPODocNo2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnPODocNo2.ToolTipTitle = "Run System";
            this.BtnPODocNo2.Click += new System.EventHandler(this.BtnPODocNo2_Click);
            // 
            // TxtLocalDocNo
            // 
            this.TxtLocalDocNo.EnterMoveNextControl = true;
            this.TxtLocalDocNo.Location = new System.Drawing.Point(207, 6);
            this.TxtLocalDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLocalDocNo.Name = "TxtLocalDocNo";
            this.TxtLocalDocNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLocalDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLocalDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLocalDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtLocalDocNo.Properties.MaxLength = 30;
            this.TxtLocalDocNo.Size = new System.Drawing.Size(166, 20);
            this.TxtLocalDocNo.TabIndex = 18;
            this.TxtLocalDocNo.Validated += new System.EventHandler(this.TxtLocalDocNo_Validated);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Black;
            this.label23.Location = new System.Drawing.Point(160, 10);
            this.label23.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(43, 14);
            this.label23.TabIndex = 17;
            this.label23.Text = "Local#";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeePORemarkHdrOld
            // 
            this.MeePORemarkHdrOld.EnterMoveNextControl = true;
            this.MeePORemarkHdrOld.Location = new System.Drawing.Point(207, 293);
            this.MeePORemarkHdrOld.Margin = new System.Windows.Forms.Padding(5);
            this.MeePORemarkHdrOld.Name = "MeePORemarkHdrOld";
            this.MeePORemarkHdrOld.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.MeePORemarkHdrOld.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeePORemarkHdrOld.Properties.Appearance.Options.UseBackColor = true;
            this.MeePORemarkHdrOld.Properties.Appearance.Options.UseFont = true;
            this.MeePORemarkHdrOld.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeePORemarkHdrOld.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeePORemarkHdrOld.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeePORemarkHdrOld.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeePORemarkHdrOld.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeePORemarkHdrOld.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeePORemarkHdrOld.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeePORemarkHdrOld.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeePORemarkHdrOld.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeePORemarkHdrOld.Properties.MaxLength = 400;
            this.MeePORemarkHdrOld.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeePORemarkHdrOld.Properties.ReadOnly = true;
            this.MeePORemarkHdrOld.Properties.ShowIcon = false;
            this.MeePORemarkHdrOld.Size = new System.Drawing.Size(166, 20);
            this.MeePORemarkHdrOld.TabIndex = 49;
            this.MeePORemarkHdrOld.ToolTip = "F4 : Show/hide text";
            this.MeePORemarkHdrOld.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeePORemarkHdrOld.ToolTipTitle = "Run System";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(44, 296);
            this.label24.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(159, 14);
            this.label24.TabIndex = 48;
            this.label24.Text = "Previous PO Remark Header";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeePORemarkDtlOld
            // 
            this.MeePORemarkDtlOld.EnterMoveNextControl = true;
            this.MeePORemarkDtlOld.Location = new System.Drawing.Point(207, 315);
            this.MeePORemarkDtlOld.Margin = new System.Windows.Forms.Padding(5);
            this.MeePORemarkDtlOld.Name = "MeePORemarkDtlOld";
            this.MeePORemarkDtlOld.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.MeePORemarkDtlOld.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeePORemarkDtlOld.Properties.Appearance.Options.UseBackColor = true;
            this.MeePORemarkDtlOld.Properties.Appearance.Options.UseFont = true;
            this.MeePORemarkDtlOld.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeePORemarkDtlOld.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeePORemarkDtlOld.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeePORemarkDtlOld.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeePORemarkDtlOld.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeePORemarkDtlOld.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeePORemarkDtlOld.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeePORemarkDtlOld.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeePORemarkDtlOld.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeePORemarkDtlOld.Properties.MaxLength = 400;
            this.MeePORemarkDtlOld.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeePORemarkDtlOld.Properties.ReadOnly = true;
            this.MeePORemarkDtlOld.Properties.ShowIcon = false;
            this.MeePORemarkDtlOld.Size = new System.Drawing.Size(166, 20);
            this.MeePORemarkDtlOld.TabIndex = 51;
            this.MeePORemarkDtlOld.ToolTip = "F4 : Show/hide text";
            this.MeePORemarkDtlOld.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeePORemarkDtlOld.ToolTipTitle = "Run System";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(53, 318);
            this.label25.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(150, 14);
            this.label25.TabIndex = 50;
            this.label25.Text = "Previous PO Remark Detail";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeePORemarkDtlNew
            // 
            this.MeePORemarkDtlNew.EnterMoveNextControl = true;
            this.MeePORemarkDtlNew.Location = new System.Drawing.Point(578, 316);
            this.MeePORemarkDtlNew.Margin = new System.Windows.Forms.Padding(5);
            this.MeePORemarkDtlNew.Name = "MeePORemarkDtlNew";
            this.MeePORemarkDtlNew.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeePORemarkDtlNew.Properties.Appearance.Options.UseFont = true;
            this.MeePORemarkDtlNew.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeePORemarkDtlNew.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeePORemarkDtlNew.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeePORemarkDtlNew.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeePORemarkDtlNew.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeePORemarkDtlNew.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeePORemarkDtlNew.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeePORemarkDtlNew.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeePORemarkDtlNew.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeePORemarkDtlNew.Properties.MaxLength = 10000;
            this.MeePORemarkDtlNew.Properties.PopupFormSize = new System.Drawing.Size(300, 20);
            this.MeePORemarkDtlNew.Properties.ShowIcon = false;
            this.MeePORemarkDtlNew.Size = new System.Drawing.Size(166, 20);
            this.MeePORemarkDtlNew.TabIndex = 87;
            this.MeePORemarkDtlNew.ToolTip = "F4 : Show/hide text";
            this.MeePORemarkDtlNew.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeePORemarkDtlNew.ToolTipTitle = "Run System";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(444, 319);
            this.label26.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(130, 14);
            this.label26.TabIndex = 86;
            this.label26.Text = "New PO Remark Detail";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeePORemarkHdrNew
            // 
            this.MeePORemarkHdrNew.EnterMoveNextControl = true;
            this.MeePORemarkHdrNew.Location = new System.Drawing.Point(578, 294);
            this.MeePORemarkHdrNew.Margin = new System.Windows.Forms.Padding(5);
            this.MeePORemarkHdrNew.Name = "MeePORemarkHdrNew";
            this.MeePORemarkHdrNew.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeePORemarkHdrNew.Properties.Appearance.Options.UseFont = true;
            this.MeePORemarkHdrNew.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeePORemarkHdrNew.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeePORemarkHdrNew.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeePORemarkHdrNew.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeePORemarkHdrNew.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeePORemarkHdrNew.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeePORemarkHdrNew.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeePORemarkHdrNew.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeePORemarkHdrNew.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeePORemarkHdrNew.Properties.MaxLength = 10000;
            this.MeePORemarkHdrNew.Properties.PopupFormSize = new System.Drawing.Size(300, 20);
            this.MeePORemarkHdrNew.Properties.ShowIcon = false;
            this.MeePORemarkHdrNew.Size = new System.Drawing.Size(166, 20);
            this.MeePORemarkHdrNew.TabIndex = 85;
            this.MeePORemarkHdrNew.ToolTip = "F4 : Show/hide text";
            this.MeePORemarkHdrNew.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeePORemarkHdrNew.ToolTipTitle = "Run System";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(435, 297);
            this.label27.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(139, 14);
            this.label27.TabIndex = 84;
            this.label27.Text = "New PO Remark Header";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnPORequestDocNo
            // 
            this.BtnPORequestDocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnPORequestDocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPORequestDocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPORequestDocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPORequestDocNo.Appearance.Options.UseBackColor = true;
            this.BtnPORequestDocNo.Appearance.Options.UseFont = true;
            this.BtnPORequestDocNo.Appearance.Options.UseForeColor = true;
            this.BtnPORequestDocNo.Appearance.Options.UseTextOptions = true;
            this.BtnPORequestDocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnPORequestDocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnPORequestDocNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnPORequestDocNo.Image")));
            this.BtnPORequestDocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnPORequestDocNo.Location = new System.Drawing.Point(379, 48);
            this.BtnPORequestDocNo.Name = "BtnPORequestDocNo";
            this.BtnPORequestDocNo.Size = new System.Drawing.Size(24, 21);
            this.BtnPORequestDocNo.TabIndex = 25;
            this.BtnPORequestDocNo.ToolTip = "Show Data PO Request";
            this.BtnPORequestDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnPORequestDocNo.ToolTipTitle = "Run System";
            this.BtnPORequestDocNo.Click += new System.EventHandler(this.BtnPORequestDocNo_Click);
            // 
            // Tc1
            // 
            this.Tc1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Tc1.HeaderAutoFill = DevExpress.Utils.DefaultBoolean.True;
            this.Tc1.Location = new System.Drawing.Point(0, 74);
            this.Tc1.Name = "Tc1";
            this.Tc1.SelectedTabPage = this.Tp1;
            this.Tc1.Size = new System.Drawing.Size(794, 422);
            this.Tc1.TabIndex = 16;
            this.Tc1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.Tp1,
            this.Tp4,
            this.Tp2,
            this.Tp3});
            // 
            // Tp1
            // 
            this.Tp1.Appearance.Header.Options.UseTextOptions = true;
            this.Tp1.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp1.Controls.Add(this.TxtAddendumAvailability);
            this.Tp1.Controls.Add(this.LblAddendumAvailability);
            this.Tp1.Controls.Add(this.TxtGrandTotal);
            this.Tp1.Controls.Add(this.LblGrandTotal);
            this.Tp1.Controls.Add(this.label33);
            this.Tp1.Controls.Add(this.label32);
            this.Tp1.Controls.Add(this.DteEstTimeArrivalNew);
            this.Tp1.Controls.Add(this.DteEstTimeArrivalOld);
            this.Tp1.Controls.Add(this.BtnPOServiceDocNo);
            this.Tp1.Controls.Add(this.label31);
            this.Tp1.Controls.Add(this.TxtPOServiceDocNo);
            this.Tp1.Controls.Add(this.BtnPORequestDocNo);
            this.Tp1.Controls.Add(this.MeePORemarkDtlNew);
            this.Tp1.Controls.Add(this.label4);
            this.Tp1.Controls.Add(this.label26);
            this.Tp1.Controls.Add(this.TxtPODocNo);
            this.Tp1.Controls.Add(this.MeePORemarkHdrNew);
            this.Tp1.Controls.Add(this.BtnPODocNo);
            this.Tp1.Controls.Add(this.label27);
            this.Tp1.Controls.Add(this.label5);
            this.Tp1.Controls.Add(this.MeePORemarkDtlOld);
            this.Tp1.Controls.Add(this.TxtQtOld);
            this.Tp1.Controls.Add(this.label25);
            this.Tp1.Controls.Add(this.label9);
            this.Tp1.Controls.Add(this.MeePORemarkHdrOld);
            this.Tp1.Controls.Add(this.TxtDiscountAmtOld);
            this.Tp1.Controls.Add(this.label24);
            this.Tp1.Controls.Add(this.label7);
            this.Tp1.Controls.Add(this.TxtLocalDocNo);
            this.Tp1.Controls.Add(this.TxtQtNew);
            this.Tp1.Controls.Add(this.label23);
            this.Tp1.Controls.Add(this.BtnQtNew);
            this.Tp1.Controls.Add(this.BtnPODocNo2);
            this.Tp1.Controls.Add(this.label6);
            this.Tp1.Controls.Add(this.TxtVdCode);
            this.Tp1.Controls.Add(this.TxtDiscountAmtNew);
            this.Tp1.Controls.Add(this.label22);
            this.Tp1.Controls.Add(this.label8);
            this.Tp1.Controls.Add(this.TxtRoundingValueNew);
            this.Tp1.Controls.Add(this.MeeRemark);
            this.Tp1.Controls.Add(this.label21);
            this.Tp1.Controls.Add(this.label10);
            this.Tp1.Controls.Add(this.TxtRoundingValueOld);
            this.Tp1.Controls.Add(this.TxtItCode);
            this.Tp1.Controls.Add(this.label20);
            this.Tp1.Controls.Add(this.label11);
            this.Tp1.Controls.Add(this.TxtDiscNew);
            this.Tp1.Controls.Add(this.TxtQtyOld);
            this.Tp1.Controls.Add(this.label19);
            this.Tp1.Controls.Add(this.label13);
            this.Tp1.Controls.Add(this.TxtDiscOld);
            this.Tp1.Controls.Add(this.TxtTotalOld);
            this.Tp1.Controls.Add(this.label18);
            this.Tp1.Controls.Add(this.label14);
            this.Tp1.Controls.Add(this.TxtUnitPriceNew);
            this.Tp1.Controls.Add(this.TxtTotalNew);
            this.Tp1.Controls.Add(this.label17);
            this.Tp1.Controls.Add(this.LblNewQty);
            this.Tp1.Controls.Add(this.TxtUnitPriceOld);
            this.Tp1.Controls.Add(this.TxtQtyNew);
            this.Tp1.Controls.Add(this.label16);
            this.Tp1.Controls.Add(this.label15);
            this.Tp1.Controls.Add(this.TxtPORDocNo);
            this.Tp1.Name = "Tp1";
            this.Tp1.Size = new System.Drawing.Size(788, 394);
            this.Tp1.Text = "General";
            // 
            // TxtAddendumAvailability
            // 
            this.TxtAddendumAvailability.EnterMoveNextControl = true;
            this.TxtAddendumAvailability.Location = new System.Drawing.Point(575, 50);
            this.TxtAddendumAvailability.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAddendumAvailability.Name = "TxtAddendumAvailability";
            this.TxtAddendumAvailability.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAddendumAvailability.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAddendumAvailability.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAddendumAvailability.Properties.Appearance.Options.UseFont = true;
            this.TxtAddendumAvailability.Properties.MaxLength = 30;
            this.TxtAddendumAvailability.Properties.ReadOnly = true;
            this.TxtAddendumAvailability.Size = new System.Drawing.Size(169, 20);
            this.TxtAddendumAvailability.TabIndex = 59;
            // 
            // LblAddendumAvailability
            // 
            this.LblAddendumAvailability.AutoSize = true;
            this.LblAddendumAvailability.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblAddendumAvailability.ForeColor = System.Drawing.Color.Black;
            this.LblAddendumAvailability.Location = new System.Drawing.Point(447, 53);
            this.LblAddendumAvailability.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblAddendumAvailability.Name = "LblAddendumAvailability";
            this.LblAddendumAvailability.Size = new System.Drawing.Size(125, 14);
            this.LblAddendumAvailability.TabIndex = 58;
            this.LblAddendumAvailability.Text = "Addendum Availability";
            this.LblAddendumAvailability.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtGrandTotal
            // 
            this.TxtGrandTotal.EnterMoveNextControl = true;
            this.TxtGrandTotal.Location = new System.Drawing.Point(575, 27);
            this.TxtGrandTotal.Margin = new System.Windows.Forms.Padding(5);
            this.TxtGrandTotal.Name = "TxtGrandTotal";
            this.TxtGrandTotal.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtGrandTotal.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtGrandTotal.Properties.Appearance.Options.UseBackColor = true;
            this.TxtGrandTotal.Properties.Appearance.Options.UseFont = true;
            this.TxtGrandTotal.Properties.MaxLength = 30;
            this.TxtGrandTotal.Properties.ReadOnly = true;
            this.TxtGrandTotal.Size = new System.Drawing.Size(169, 20);
            this.TxtGrandTotal.TabIndex = 57;
            // 
            // LblGrandTotal
            // 
            this.LblGrandTotal.AutoSize = true;
            this.LblGrandTotal.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblGrandTotal.ForeColor = System.Drawing.Color.Black;
            this.LblGrandTotal.Location = new System.Drawing.Point(482, 31);
            this.LblGrandTotal.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblGrandTotal.Name = "LblGrandTotal";
            this.LblGrandTotal.Size = new System.Drawing.Size(91, 14);
            this.LblGrandTotal.TabIndex = 56;
            this.LblGrandTotal.Text = "Grand Total PO";
            this.LblGrandTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(0, 339);
            this.label33.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(203, 14);
            this.label33.TabIndex = 52;
            this.label33.Text = "Previous Est Received Date Revision";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(391, 340);
            this.label32.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(183, 14);
            this.label32.TabIndex = 88;
            this.label32.Text = "New Est Received Date Revision";
            this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteEstTimeArrivalNew
            // 
            this.DteEstTimeArrivalNew.EditValue = null;
            this.DteEstTimeArrivalNew.EnterMoveNextControl = true;
            this.DteEstTimeArrivalNew.Location = new System.Drawing.Point(578, 337);
            this.DteEstTimeArrivalNew.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteEstTimeArrivalNew.Name = "DteEstTimeArrivalNew";
            this.DteEstTimeArrivalNew.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteEstTimeArrivalNew.Properties.Appearance.Options.UseFont = true;
            this.DteEstTimeArrivalNew.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteEstTimeArrivalNew.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteEstTimeArrivalNew.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteEstTimeArrivalNew.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteEstTimeArrivalNew.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteEstTimeArrivalNew.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteEstTimeArrivalNew.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteEstTimeArrivalNew.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteEstTimeArrivalNew.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteEstTimeArrivalNew.Size = new System.Drawing.Size(166, 20);
            this.DteEstTimeArrivalNew.TabIndex = 89;
            // 
            // DteEstTimeArrivalOld
            // 
            this.DteEstTimeArrivalOld.EditValue = null;
            this.DteEstTimeArrivalOld.EnterMoveNextControl = true;
            this.DteEstTimeArrivalOld.Location = new System.Drawing.Point(207, 337);
            this.DteEstTimeArrivalOld.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteEstTimeArrivalOld.Name = "DteEstTimeArrivalOld";
            this.DteEstTimeArrivalOld.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.DteEstTimeArrivalOld.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteEstTimeArrivalOld.Properties.Appearance.Options.UseBackColor = true;
            this.DteEstTimeArrivalOld.Properties.Appearance.Options.UseFont = true;
            this.DteEstTimeArrivalOld.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteEstTimeArrivalOld.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteEstTimeArrivalOld.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteEstTimeArrivalOld.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteEstTimeArrivalOld.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteEstTimeArrivalOld.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteEstTimeArrivalOld.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteEstTimeArrivalOld.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteEstTimeArrivalOld.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteEstTimeArrivalOld.Size = new System.Drawing.Size(166, 20);
            this.DteEstTimeArrivalOld.TabIndex = 53;
            // 
            // BtnPOServiceDocNo
            // 
            this.BtnPOServiceDocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnPOServiceDocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPOServiceDocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPOServiceDocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPOServiceDocNo.Appearance.Options.UseBackColor = true;
            this.BtnPOServiceDocNo.Appearance.Options.UseFont = true;
            this.BtnPOServiceDocNo.Appearance.Options.UseForeColor = true;
            this.BtnPOServiceDocNo.Appearance.Options.UseTextOptions = true;
            this.BtnPOServiceDocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnPOServiceDocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnPOServiceDocNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnPOServiceDocNo.Image")));
            this.BtnPOServiceDocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnPOServiceDocNo.Location = new System.Drawing.Point(379, 71);
            this.BtnPOServiceDocNo.Name = "BtnPOServiceDocNo";
            this.BtnPOServiceDocNo.Size = new System.Drawing.Size(24, 21);
            this.BtnPOServiceDocNo.TabIndex = 28;
            this.BtnPOServiceDocNo.ToolTip = "Show Data PO Request";
            this.BtnPOServiceDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnPOServiceDocNo.ToolTipTitle = "Run System";
            this.BtnPOServiceDocNo.Click += new System.EventHandler(this.BtnPOServiceDocNo_Click);
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.Black;
            this.label31.Location = new System.Drawing.Point(107, 75);
            this.label31.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(96, 14);
            this.label31.TabIndex = 26;
            this.label31.Text = "PO For Service#";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPOServiceDocNo
            // 
            this.TxtPOServiceDocNo.EnterMoveNextControl = true;
            this.TxtPOServiceDocNo.Location = new System.Drawing.Point(207, 72);
            this.TxtPOServiceDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPOServiceDocNo.Name = "TxtPOServiceDocNo";
            this.TxtPOServiceDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtPOServiceDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPOServiceDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPOServiceDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtPOServiceDocNo.Properties.MaxLength = 30;
            this.TxtPOServiceDocNo.Properties.ReadOnly = true;
            this.TxtPOServiceDocNo.Size = new System.Drawing.Size(166, 20);
            this.TxtPOServiceDocNo.TabIndex = 27;
            // 
            // Tp4
            // 
            this.Tp4.Appearance.Header.Options.UseTextOptions = true;
            this.Tp4.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp4.Controls.Add(this.ChkFile);
            this.Tp4.Controls.Add(this.BtnDownload);
            this.Tp4.Controls.Add(this.PbUpload);
            this.Tp4.Controls.Add(this.BtnFile);
            this.Tp4.Controls.Add(this.TxtFile);
            this.Tp4.Controls.Add(this.label34);
            this.Tp4.Name = "Tp4";
            this.Tp4.Size = new System.Drawing.Size(766, 131);
            this.Tp4.Text = "Upload File";
            // 
            // ChkFile
            // 
            this.ChkFile.Location = new System.Drawing.Point(357, 23);
            this.ChkFile.Name = "ChkFile";
            this.ChkFile.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkFile.Properties.Appearance.Options.UseFont = true;
            this.ChkFile.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkFile.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkFile.Properties.Caption = " ";
            this.ChkFile.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkFile.Size = new System.Drawing.Size(20, 22);
            this.ChkFile.TabIndex = 72;
            this.ChkFile.ToolTip = "Remove filter";
            this.ChkFile.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkFile.ToolTipTitle = "Run System";
            this.ChkFile.CheckedChanged += new System.EventHandler(this.ChkFile_CheckedChanged);
            // 
            // BtnDownload
            // 
            this.BtnDownload.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDownload.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDownload.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDownload.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDownload.Appearance.Options.UseBackColor = true;
            this.BtnDownload.Appearance.Options.UseFont = true;
            this.BtnDownload.Appearance.Options.UseForeColor = true;
            this.BtnDownload.Appearance.Options.UseTextOptions = true;
            this.BtnDownload.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDownload.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDownload.Image = ((System.Drawing.Image)(resources.GetObject("BtnDownload.Image")));
            this.BtnDownload.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDownload.Location = new System.Drawing.Point(399, 23);
            this.BtnDownload.Name = "BtnDownload";
            this.BtnDownload.Size = new System.Drawing.Size(26, 21);
            this.BtnDownload.TabIndex = 74;
            this.BtnDownload.ToolTip = "DownloadFile";
            this.BtnDownload.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDownload.ToolTipTitle = "Run System";
            this.BtnDownload.Click += new System.EventHandler(this.BtnDownload_Click);
            // 
            // PbUpload
            // 
            this.PbUpload.Location = new System.Drawing.Point(49, 46);
            this.PbUpload.Name = "PbUpload";
            this.PbUpload.Size = new System.Drawing.Size(367, 23);
            this.PbUpload.TabIndex = 75;
            // 
            // BtnFile
            // 
            this.BtnFile.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnFile.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFile.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFile.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFile.Appearance.Options.UseBackColor = true;
            this.BtnFile.Appearance.Options.UseFont = true;
            this.BtnFile.Appearance.Options.UseForeColor = true;
            this.BtnFile.Appearance.Options.UseTextOptions = true;
            this.BtnFile.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnFile.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnFile.Image = ((System.Drawing.Image)(resources.GetObject("BtnFile.Image")));
            this.BtnFile.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnFile.Location = new System.Drawing.Point(374, 23);
            this.BtnFile.Name = "BtnFile";
            this.BtnFile.Size = new System.Drawing.Size(23, 19);
            this.BtnFile.TabIndex = 73;
            this.BtnFile.ToolTip = "BrowseFile";
            this.BtnFile.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnFile.ToolTipTitle = "Run System";
            this.BtnFile.Click += new System.EventHandler(this.BtnFile_Click);
            // 
            // TxtFile
            // 
            this.TxtFile.EnterMoveNextControl = true;
            this.TxtFile.Location = new System.Drawing.Point(49, 24);
            this.TxtFile.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFile.Name = "TxtFile";
            this.TxtFile.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFile.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFile.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFile.Properties.Appearance.Options.UseFont = true;
            this.TxtFile.Properties.MaxLength = 16;
            this.TxtFile.Size = new System.Drawing.Size(300, 20);
            this.TxtFile.TabIndex = 71;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.Black;
            this.label34.Location = new System.Drawing.Point(18, 28);
            this.label34.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(24, 14);
            this.label34.TabIndex = 70;
            this.label34.Text = "File";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Tp2
            // 
            this.Tp2.Appearance.Header.Options.UseTextOptions = true;
            this.Tp2.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp2.Controls.Add(this.label30);
            this.Tp2.Controls.Add(this.label29);
            this.Tp2.Controls.Add(this.LueTaxCode);
            this.Tp2.Controls.Add(this.DteEstRecvDt);
            this.Tp2.Controls.Add(this.label28);
            this.Tp2.Name = "Tp2";
            this.Tp2.Size = new System.Drawing.Size(766, 131);
            this.Tp2.Text = "Information Only";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.Red;
            this.label30.Location = new System.Drawing.Point(472, 24);
            this.label30.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(169, 14);
            this.label30.TabIndex = 21;
            this.label30.Text = "( It is not updating PO\'s tax )";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Black;
            this.label29.Location = new System.Drawing.Point(19, 46);
            this.label29.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(144, 14);
            this.label29.TabIndex = 19;
            this.label29.Text = "Estimated Received Date";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueTaxCode
            // 
            this.LueTaxCode.EnterMoveNextControl = true;
            this.LueTaxCode.Location = new System.Drawing.Point(167, 21);
            this.LueTaxCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueTaxCode.Name = "LueTaxCode";
            this.LueTaxCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode.Properties.Appearance.Options.UseFont = true;
            this.LueTaxCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueTaxCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueTaxCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueTaxCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTaxCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueTaxCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueTaxCode.Properties.DropDownRows = 30;
            this.LueTaxCode.Properties.NullText = "[Empty]";
            this.LueTaxCode.Properties.PopupWidth = 320;
            this.LueTaxCode.Size = new System.Drawing.Size(302, 20);
            this.LueTaxCode.TabIndex = 18;
            this.LueTaxCode.ToolTip = "F4 : Show/hide list";
            this.LueTaxCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueTaxCode.EditValueChanged += new System.EventHandler(this.LueTaxCode_EditValueChanged);
            // 
            // DteEstRecvDt
            // 
            this.DteEstRecvDt.EditValue = null;
            this.DteEstRecvDt.EnterMoveNextControl = true;
            this.DteEstRecvDt.Location = new System.Drawing.Point(167, 43);
            this.DteEstRecvDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteEstRecvDt.Name = "DteEstRecvDt";
            this.DteEstRecvDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteEstRecvDt.Properties.Appearance.Options.UseFont = true;
            this.DteEstRecvDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteEstRecvDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteEstRecvDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteEstRecvDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteEstRecvDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteEstRecvDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteEstRecvDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteEstRecvDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteEstRecvDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteEstRecvDt.Size = new System.Drawing.Size(129, 20);
            this.DteEstRecvDt.TabIndex = 20;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Black;
            this.label28.Location = new System.Drawing.Point(136, 24);
            this.label28.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(27, 14);
            this.label28.TabIndex = 17;
            this.label28.Text = "Tax";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Tp3
            // 
            this.Tp3.Appearance.Header.Options.UseTextOptions = true;
            this.Tp3.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp3.Controls.Add(this.Grd1);
            this.Tp3.Name = "Tp3";
            this.Tp3.Size = new System.Drawing.Size(766, 131);
            this.Tp3.Text = "Approval";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.TxtStatus);
            this.panel3.Controls.Add(this.TxtDocNo);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.ChkCancelInd);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.DteDocDt);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(794, 74);
            this.panel3.TabIndex = 8;
            // 
            // OD
            // 
            this.OD.FileName = "openFileDialog1";
            // 
            // FrmPORevision2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(864, 496);
            this.Name = "FrmPORevision2";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPODocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtQtOld.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDiscountAmtOld.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDiscountAmtNew.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtQtNew.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtItCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtQtyOld.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalOld.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalNew.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtQtyNew.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPORDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtUnitPriceOld.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtUnitPriceNew.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDiscOld.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDiscNew.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRoundingValueOld.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRoundingValueNew.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVdCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLocalDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeePORemarkHdrOld.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeePORemarkDtlOld.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeePORemarkDtlNew.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeePORemarkHdrNew.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Tc1)).EndInit();
            this.Tc1.ResumeLayout(false);
            this.Tp1.ResumeLayout(false);
            this.Tp1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAddendumAvailability.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGrandTotal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteEstTimeArrivalNew.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteEstTimeArrivalNew.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteEstTimeArrivalOld.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteEstTimeArrivalOld.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPOServiceDocNo.Properties)).EndInit();
            this.Tp4.ResumeLayout(false);
            this.Tp4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile.Properties)).EndInit();
            this.Tp2.ResumeLayout(false);
            this.Tp2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueTaxCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteEstRecvDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteEstRecvDt.Properties)).EndInit();
            this.Tp3.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.CheckEdit ChkCancelInd;
        private System.Windows.Forms.Label label4;
        internal DevExpress.XtraEditors.TextEdit TxtStatus;
        private System.Windows.Forms.Label label3;
        public DevExpress.XtraEditors.SimpleButton BtnPODocNo;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label9;
        internal DevExpress.XtraEditors.TextEdit TxtDiscountAmtNew;
        private System.Windows.Forms.Label label6;
        public DevExpress.XtraEditors.SimpleButton BtnQtNew;
        internal DevExpress.XtraEditors.TextEdit TxtQtNew;
        private System.Windows.Forms.Label label7;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label13;
        internal DevExpress.XtraEditors.TextEdit TxtTotalNew;
        private System.Windows.Forms.Label label14;
        internal DevExpress.XtraEditors.TextEdit TxtQtyNew;
        private System.Windows.Forms.Label LblNewQty;
        public DevExpress.XtraEditors.TextEdit TxtPODocNo;
        public DevExpress.XtraEditors.TextEdit TxtQtOld;
        public DevExpress.XtraEditors.TextEdit TxtDiscountAmtOld;
        public DevExpress.XtraEditors.TextEdit TxtQtyOld;
        public DevExpress.XtraEditors.TextEdit TxtItCode;
        public DevExpress.XtraEditors.TextEdit TxtTotalOld;
        public DevExpress.XtraEditors.TextEdit TxtPORDocNo;
        private System.Windows.Forms.Label label15;
        public DevExpress.XtraEditors.TextEdit TxtUnitPriceNew;
        private System.Windows.Forms.Label label17;
        public DevExpress.XtraEditors.TextEdit TxtUnitPriceOld;
        private System.Windows.Forms.Label label16;
        public DevExpress.XtraEditors.TextEdit TxtDiscNew;
        private System.Windows.Forms.Label label19;
        public DevExpress.XtraEditors.TextEdit TxtDiscOld;
        private System.Windows.Forms.Label label18;
        public DevExpress.XtraEditors.TextEdit TxtRoundingValueNew;
        private System.Windows.Forms.Label label21;
        public DevExpress.XtraEditors.TextEdit TxtRoundingValueOld;
        private System.Windows.Forms.Label label20;
        public DevExpress.XtraEditors.TextEdit TxtVdCode;
        private System.Windows.Forms.Label label22;
        protected TenTec.Windows.iGridLib.iGrid Grd1;
        public DevExpress.XtraEditors.SimpleButton BtnPODocNo2;
        internal DevExpress.XtraEditors.TextEdit TxtLocalDocNo;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        public DevExpress.XtraEditors.MemoExEdit MeePORemarkDtlNew;
        public DevExpress.XtraEditors.MemoExEdit MeePORemarkDtlOld;
        public DevExpress.XtraEditors.MemoExEdit MeePORemarkHdrOld;
        public DevExpress.XtraEditors.MemoExEdit MeePORemarkHdrNew;
        public DevExpress.XtraEditors.SimpleButton BtnPORequestDocNo;
        private DevExpress.XtraTab.XtraTabControl Tc1;
        private DevExpress.XtraTab.XtraTabPage Tp1;
        private DevExpress.XtraTab.XtraTabPage Tp2;
        private DevExpress.XtraTab.XtraTabPage Tp3;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label29;
        internal DevExpress.XtraEditors.DateEdit DteEstRecvDt;
        private System.Windows.Forms.Label label28;
        internal DevExpress.XtraEditors.LookUpEdit LueTaxCode;
        private System.Windows.Forms.Label label31;
        public DevExpress.XtraEditors.TextEdit TxtPOServiceDocNo;
        public DevExpress.XtraEditors.SimpleButton BtnPOServiceDocNo;
        internal DevExpress.XtraEditors.DateEdit DteEstTimeArrivalNew;
        internal DevExpress.XtraEditors.DateEdit DteEstTimeArrivalOld;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label32;
        private DevExpress.XtraTab.XtraTabPage Tp4;
        private DevExpress.XtraEditors.CheckEdit ChkFile;
        public DevExpress.XtraEditors.SimpleButton BtnDownload;
        private System.Windows.Forms.ProgressBar PbUpload;
        public DevExpress.XtraEditors.SimpleButton BtnFile;
        internal DevExpress.XtraEditors.TextEdit TxtFile;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.OpenFileDialog OD;
        private System.Windows.Forms.SaveFileDialog SFD;
        internal DevExpress.XtraEditors.TextEdit TxtGrandTotal;
        private System.Windows.Forms.Label LblGrandTotal;
        internal DevExpress.XtraEditors.TextEdit TxtAddendumAvailability;
        private System.Windows.Forms.Label LblAddendumAvailability;
    }
}