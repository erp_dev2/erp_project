﻿#region Update
/* 
    03/05/2017 [TKG] tambah filter pada saat edit data, uom tidak bisa diedit berdasarkan parameter IsAbleToEditItemUom
    29/12/2017 [TKG] item category yg aktif yg ditampilkan.
    04/04/2018 [TKG] bug item saat category diganti
    22/05/2018 [WED] tambah field Panjang, lebar, tinggi, volume, di tab sales data
    12/11/2018 [MEY] tambah checkbox untuk service item 
    17/11/2018 [HAR] BUG layout mnumpuk
    12/09/2019 [TKG/IMS] berdasarkan parameter IsItemItCodeInternalMandatory, Kode item lokal harus diisi atau tidak.
    09/10/2019 [DITA/IMS] tambah warning question saat local code existed
    28/10/2019 [TKG/IMS] tambah moving average currency dan price
    17/11/2019 [TKG/IMS] hilangkan moving average currency dan price
    06/01/2020 [TKG/SIER] tambah weight dan weight uom
    12/05/2020 [IBL/SIER] Ubah file upload menjadi seperti master employee
    16/06/2020 [TKG/SRN] Berdasarkan parameter IsItCodeUseItSeqNo, Saat membuat kode item yg baru, nomor urut yg digunakana dari column ItSeqNo  
    08/10/2020 [TKG/KSM] tambah old code 
    20/12/2020 [WED/IMS] kalau diakses dari menu MaterialRequest4, item category nya yg muncul berdasarkan parameter ItCtCodeForPRService
    02/03/2020 [TKG/IMS] ubah proses insert+update untuk uom yg sama
    29/09/2021 [NJP/KSM] Menambah Parameter IsItemConversionRate1Editable untuk Kolom Conversion Rate Beserta Validasinya Tidak Boleh di isi dengan 0.
    23/01/2022 [TKG/GSS] ubah GetParameter()
    30/08/2022 [MYA/SIER] karakter pada kolom spesifikasi ditambah menjadi 1.500 karakter
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using System.IO;
using System.Drawing.Imaging;

using System.Net;
using System.Threading;

#endregion

namespace RunSystem
{
    public partial class FrmItem : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string 
            mMenuCode = string.Empty, mAccessInd = string.Empty,
            mItCode = string.Empty; //if this application is called from other application
        internal bool
            mIsFilterByItCt = false,
            mIsAccessUsed = false; //Used In Document Approval (PO Request)
        internal FrmItemFind FrmFind;
        private string
            mItCodeInternalNotEmptyInd = string.Empty,
            mSIWeightUom = string.Empty,
            mProcFormatDocNo = string.Empty,
            mItemVolumeUom = string.Empty,
            mItemLWHUom = string.Empty,
            mItemPicture = string.Empty,
            mItCodeSeqNo = string.Empty,
            mItCtCodeForPOService = string.Empty
            ;
        private int NumberOfInventoryUomCode = 1, NumberOfSalesUomCode = 1;
        private bool 
            mIsItemInventoryNotShowCalculationData = false,
            mIsItGrpCodeMandatoryInMasterItem = false,
            mIsAbleToEditItemUom = false,
            mIsInsert = true,
            mIsAbleToEditItemCategory = false,
            mIsItemItCodeInternalMandatory = false,
            mIsItemAllowToUploadFile = false,
            mIsItCodeUseItSeqNo = false,
            mIsItemConversionRate1Editable = false; //Tab Sales Kolom Item Conversi Rate Editable 


        private string
          mPortForFTPClient = string.Empty,
          mHostAddrForFTPClient = string.Empty,
          mSharedFolderForFTPClient = string.Empty,
          mUsernameForFTPClient = string.Empty,
          mPasswordForFTPClient = string.Empty,
          mFileSizeMaxUploadFTPClient = string.Empty,
          mFormatFTPClient = string.Empty;

        private byte[] downloadedData;

        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmItem(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Master Item";

            try
            {
                GetParameter();
                SetGrd();

                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                BtnPrint.Visible = false;
                SetFormControl(mState.View);

                tabControl1.SelectTab("TpgPurchasing");
                Sl.SetLueVdCode(ref LueVdCode);
                Sl.SetLuePGCode(ref LuePGCode);
                Sl.SetLueTaxCode(new List<DevExpress.XtraEditors.LookUpEdit> { LueTaxCode1, LueTaxCode2, LueTaxCode3 });
                Sl.SetLueUomCode(new List<DevExpress.XtraEditors.LookUpEdit> { 
                    LuePurchaseUomCode, LueLengthUomCode, LueHeightUomCode, LueWidthUomCode, LueVolumeUomCode, 
                    LueDiameterUomCode, LueWeightUomCode 
                });
                
                tabControl1.SelectTab("TpgSales");
                Sl.SetLueUomCode(new List<DevExpress.XtraEditors.LookUpEdit> { LueSalesUomCode, LueSalesUomCode2 });

                tabControl1.SelectTab("TpgInventory");
                Sl.SetLueUomCode(new List<DevExpress.XtraEditors.LookUpEdit> { LueInventoryUomCode, LueInventoryUomCode2, LueInventoryUomCode3 });
                SetLueControlByDeptCode(ref LueControlByDeptCode, "");

                tabControl1.SelectTab("TpgPlanning");
                Sl.SetLueUomCode(new List<DevExpress.XtraEditors.LookUpEdit> { LuePlanningUomCode, LuePlanningUomCode2 });
                
                tabControl1.SelectTab("TpgProperty");
                Sl.SetLueOption(ref LueInformation1, "ItemInformation1");
                Sl.SetLueOption(ref LueInformation2, "ItemInformation2");
                Sl.SetLueOption(ref LueInformation3, "ItemInformation3");
                Sl.SetLueOption(ref LueInformation4, "ItemInformation4");
                Sl.SetLueOption(ref LueInformation5, "ItemInformation5");
                
                tabControl1.SelectTab("TpgGeneral");
                Sl.SetLueItBrCode(ref LueItBrCode);
                if (mIsItemItCodeInternalMandatory) LblItCodeInternal.ForeColor = Color.Red;
                if (mIsItGrpCodeMandatoryInMasterItem)
                    LblItGrpCode.ForeColor = Color.Red;

                SetItCodeInternalNotEmptyInd();

                // Check Number Of Inventory UOM                
                //LueInventoryUomCode2.Visible = (NumberOfInventoryUomCode >= 2);
                //LueInventoryUomCode3.Visible = (NumberOfInventoryUomCode >= 3);

                // Check Number Of Sales UOM             
                //LueSalesUomCode2.Visible = (NumberOfSalesUomCode >= 2);
                TxtSalesConvertUOM2.Visible = (NumberOfSalesUomCode >= 2);
                LblSalesUOM1.Visible = (NumberOfSalesUomCode >= 2);

                Sl.SetLueUomCode(ref LueUomCode);
                LueUomCode.Visible = false;
                LueProperty.Visible = false;

                //if this application is called from other application
                if (mItCode.Length != 0)
                {
                    ShowData(mItCode);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible =
                    BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = mIsAccessUsed;
                    tabControl1.SelectTab("TpgInventory");
                } 
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 4;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdr(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "No",

                        //1-3
                        "Warehouse Name",
                        "In Stock",
                        "Committed"
                    });

            Sm.GrdFormatDec(Grd1, new int[] { 2, 3 }, 0);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3 });
            Sm.SetGrdProperty(Grd1, true);

            Grd2.Cols.Count = 6;
            Grd2.FrozenArea.ColCount = 1;
            Sm.GrdHdr(
                    Grd2, new string[] 
                    {
                        //0
                        "No", 

                        //1-5
                        "Asset's Code", 
                        "Asset's Name",
                        "Work Center", 
                        "Active", 
                        "Purchase Date"
                    }
                );
            Sm.GrdColCheck(Grd2, new int[] { 3, 4 });
            Sm.GrdFormatDate(Grd2, new int[] { 5 });
            Sm.SetGrdProperty(Grd2, true);

            GrdSalesPckUnit.Cols.Count = 18;
            GrdSalesPckUnit.FrozenArea.ColCount = 1;
            Sm.GrdHdrWithColWidth(
                    GrdSalesPckUnit, new string[] 
                    {
                        //0
                        "Packaging UoM", 

                        //1-5
                        "Packaging"+Environment.NewLine+"UoM", 
                        "Conversion Rate",
                        "Sales UOM 1", 
                        "Conversion Rate",
                        "Sales UOM 2", 

                         //6-10
                        "Gross Weight", 
                        "UoM",
                        "Net Weight", 
                        "UoM",
                        "Length",
                        
                        //11-15
                        "UoM",
                        "Width",
                        "UoM",
                        "Height",
                        "UoM",
                        
                        //16-17
                        "Volume",
                        "UoM"
                    },
                    new int[]
                    { 
                        0, 
                        200, 120, 80, 120, 80, 
                        120, 80, 120, 80, 80,
                        80, 80, 80, 80, 80, 
                        80, 80
                    }
                );
            if (NumberOfSalesUomCode >= 2)
                Sm.GrdColInvisible(GrdSalesPckUnit, new int[] { 0 });
            else
                Sm.GrdColInvisible(GrdSalesPckUnit, new int[] { 0, 4, 5 });

            Sm.GrdFormatDec(GrdSalesPckUnit, new int[] { 2, 4, 6, 8, 10, 12, 14, 16 }, 0);

            if (mIsItemConversionRate1Editable)
                Sm.GrdColReadOnly(true, true, GrdSalesPckUnit, new int[] { 3, 5, 7, 9, 11, 13, 15, 16, 17 });
            else Sm.GrdColReadOnly(true, true, GrdSalesPckUnit, new int[] { 2, 3, 5, 7, 9, 11, 13, 15, 16, 17 });
            Sm.SetGrdProperty(GrdSalesPckUnit, false);

            GrdProperty.Cols.Count = 2;
            GrdProperty.FrozenArea.ColCount = 1;
            Sm.GrdHdrWithColWidth(
                    GrdProperty, new string[] 
                    {
                        "Property Code", "Property"+Environment.NewLine+"Name",
                    },
                    new int[] { 80, 200 }
                );
            Sm.GrdColReadOnly(true, true, GrdProperty, new int[] { 0 });
            Sm.GrdColInvisible(GrdProperty, new int[] { 0 });
            Sm.SetGrdProperty(GrdProperty, false);

            Grd3.Cols.Count = 5;
            Grd3.FrozenArea.ColCount = 3;

            Sm.GrdHdrWithColWidth(
                    Grd3,
                    new string[] 
                    {
                        //0
                        "D No",
                        //1-4
                        "File Name",
                        "U",
                        "D",
                        "File Name2"
                    },
                     new int[] 
                    {
                        0, 
                        250, 20, 20, 250
                    }
                );

            Sm.GrdColInvisible(Grd3, new int[] { 0, 4 }, false);
            Sm.GrdColButton(Grd3, new int[] { 2 }, "1");
            Sm.GrdColButton(Grd3, new int[] { 3 }, "2");
            Sm.GrdColReadOnly(Grd3, new int[] { 0, 1, 4 });
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtItCode, TxtItName, TxtForeignName, TxtItCodeInternal, TxtItCodeOld, LueItCtCode, 
                        LueItScCode, LueItBrCode, LueItGrpCode, MeeSpecification, MeeRemark, 
                        LueVdCode, LuePurchaseUomCode, LuePGCode, LueTaxCode1, LueTaxCode2, 
                        LueTaxCode3, LueLengthUomCode, LueHeightUomCode, LueWidthUomCode, LueVolumeUomCode, 
                        LueDiameterUomCode, LueSalesUomCode, LueSalesUomCode2, TxtSalesConvertUOM1, TxtSalesConvertUOM2, 
                        LueInventoryUomCode, LueInventoryUomCode2, LueInventoryUomCode3, LuePlanningUomCode, LuePlanningUomCode2,
                        TxtMinOrder, TxtLength, TxtHeight, TxtWidth, TxtVolume, 
                        TxtDiameter, TxtMinStock, TxtMaxStock, TxtReOrderStock, ChkActInd, 
                        ChkInventoryItemInd, ChkSalesItemInd, ChkPurchaseItemInd, ChkFixedItemInd, ChkPlanningItemInd, 
                        ChkServiceItemInd, ChkTaxLiableInd, ChkControlByDeptCode, LueControlByDeptCode, TxtInStockQty, 
                        TxtCommittedQty, TxtRequestedQty, TxtOrderredQty, TxtInvConvertUOM12, TxtInvConvertUOM13, 
                        TxtInvConvertUOM21, TxtInvConvertUOM23, TxtInvConvertUOM31, TxtInvConvertUOM32, TxtSource, 
                        LueInformation1, LueInformation2, TxtPlanningUomCodeConvert12, TxtPlanningUomCodeConvert21, LueInformation3, 
                        LueInformation4, LueInformation5, TxtItemRequestDocNo, TxtHSCode, TxtWeight, 
                        LueWeightUomCode
                    }, true);
                    Sm.GrdColReadOnly(true, true, GrdSalesPckUnit, new int[] { 0, 1, 2, 3, 4, 5, 6, 8, 10, 12, 14 });
                    Sm.GrdColReadOnly(true, true, GrdProperty, new int[] { 0, 1 });
                    BtnItem.Enabled = false;
                    TxtItCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtItName, TxtForeignName, TxtItCodeInternal, TxtItCodeOld, LueItCtCode,  
                        LueItBrCode, LueItGrpCode, MeeSpecification, MeeRemark, LueVdCode, 
                        LuePurchaseUomCode, LuePGCode, LueTaxCode1, LueTaxCode2, LueTaxCode3, 
                        LueLengthUomCode, LueHeightUomCode, LueDiameterUomCode, LueWidthUomCode, LueVolumeUomCode, 
                        LueSalesUomCode, LueSalesUomCode2, TxtSalesConvertUOM1, TxtSalesConvertUOM2, LueInventoryUomCode, 
                        LueInventoryUomCode2, LueInventoryUomCode3, LuePlanningUomCode, LuePlanningUomCode2, TxtMinOrder, 
                        TxtLength, TxtHeight, TxtWidth, TxtVolume, TxtDiameter,
                        TxtMinStock, TxtMaxStock, TxtReOrderStock, ChkActInd, ChkInventoryItemInd, 
                        ChkSalesItemInd, ChkPurchaseItemInd, ChkFixedItemInd, ChkPlanningItemInd, ChkServiceItemInd, 
                        ChkTaxLiableInd, LueControlByDeptCode, TxtInvConvertUOM12, TxtInvConvertUOM13, TxtInvConvertUOM21, 
                        TxtInvConvertUOM23, TxtInvConvertUOM31, TxtInvConvertUOM32, TxtPlanningUomCodeConvert12, TxtPlanningUomCodeConvert21, 
                        LueInformation1, LueInformation2, LueInformation3, LueInformation4, LueInformation5, 
                        TxtHSCode, TxtWeight, LueWeightUomCode
                    }, false);

                    if (mIsItemConversionRate1Editable)
                        Sm.GrdColReadOnly(false, true, GrdSalesPckUnit, new int[] { 1, 2, 4, 6, 8, 10, 12, 14 });
                    else
                        Sm.GrdColReadOnly(false, true, GrdSalesPckUnit, new int[] { 1, 4, 6, 8, 10, 12, 14 });
                    
                    Sm.GrdColReadOnly(false, true, GrdProperty, new int[] { 1 });
                    TxtItCode.Focus();
                    BtnItem.Enabled = true;
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtItName, TxtForeignName, TxtItCodeInternal, TxtItCodeOld, LueItScCode, 
                        LueItBrCode, LueItGrpCode, MeeSpecification, MeeRemark, LueVdCode, 
                        LuePGCode, LueTaxCode1, LueTaxCode2, LueTaxCode3, LueLengthUomCode, 
                        LueHeightUomCode, LueDiameterUomCode, LueWidthUomCode, LueVolumeUomCode, TxtSalesConvertUOM1, 
                        TxtSalesConvertUOM2, TxtMinOrder, TxtLength, TxtHeight, TxtWidth, 
                        TxtVolume, TxtDiameter, TxtMinStock, TxtMaxStock, TxtReOrderStock, 
                        ChkActInd, ChkInventoryItemInd, ChkSalesItemInd, ChkPurchaseItemInd, ChkFixedItemInd, 
                        ChkPlanningItemInd, ChkServiceItemInd, ChkTaxLiableInd, LueControlByDeptCode, TxtInvConvertUOM12, 
                        TxtInvConvertUOM13, TxtInvConvertUOM21, TxtInvConvertUOM23, TxtInvConvertUOM31, TxtInvConvertUOM32, 
                        TxtPlanningUomCodeConvert12, TxtPlanningUomCodeConvert21, LueInformation1, LueInformation2, LueInformation3, 
                        LueInformation4, LueInformation5, TxtHSCode, TxtWeight, LueWeightUomCode
                    }, false);
                    if (mIsAbleToEditItemUom)
                    {
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                        { 
                            LuePurchaseUomCode, LueSalesUomCode, LueSalesUomCode2, LueInventoryUomCode, LueInventoryUomCode2, 
                            LueInventoryUomCode3, LuePlanningUomCode, LuePlanningUomCode2
                        }, false);
                    }
                    if (mIsAbleToEditItemCategory) 
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueItCtCode }, false);
                    if (mIsItemConversionRate1Editable)
                        Sm.GrdColReadOnly(false, true, GrdSalesPckUnit, new int[] { 1, 2, 4, 6, 8, 10, 12, 14 });
                    else
                        Sm.GrdColReadOnly(false, true, GrdSalesPckUnit, new int[] { 1, 4, 6, 8, 10, 12, 14 });
                    Sm.GrdColReadOnly(false, true, GrdProperty, new int[] { 1 });
                    GrdProperty.Rows.Add();
                    TxtItName.Focus();
                    break;
            }
        }

        public void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtItCode, TxtItName, TxtForeignName, TxtItCodeInternal, TxtItCodeOld, 
                LueItCtCode, LueItScCode, LueItBrCode, LueItGrpCode, MeeSpecification, 
                MeeRemark, LueVdCode, LuePurchaseUomCode, LuePGCode, LueTaxCode1, 
                LueTaxCode2, LueTaxCode3, LueLengthUomCode, LueHeightUomCode, LueDiameterUomCode, 
                LueWidthUomCode, LueVolumeUomCode, LueSalesUomCode, LueSalesUomCode2, TxtSalesConvertUOM1, 
                TxtSalesConvertUOM2, LueInventoryUomCode, LueInventoryUomCode2, LueInventoryUomCode3, LuePlanningUomCode, 
                LuePlanningUomCode2, LueControlByDeptCode, TxtSource, LueInformation1, LueInformation2, 
                LueInformation3, LueInformation4, LueInformation5, TxtHSCode, LueWeightUomCode
            });

            Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
            {
                TxtMinOrder, TxtLength, TxtHeight, TxtWidth, TxtVolume, 
                TxtDiameter, TxtSalesConvertUOM1, TxtSalesConvertUOM2, TxtMinStock, TxtMaxStock, 
                TxtReOrderStock, TxtInStockQty, TxtCommittedQty, TxtRequestedQty, TxtOrderredQty, 
                TxtInvConvertUOM12, TxtInvConvertUOM13, TxtInvConvertUOM21, TxtInvConvertUOM23, TxtInvConvertUOM31, 
                TxtInvConvertUOM32, TxtPlanningUomCodeConvert12, TxtPlanningUomCodeConvert21, TxtWeight
            }, 0);

            ChkActInd.Checked = ChkInventoryItemInd.Checked = ChkSalesItemInd.Checked = 
            ChkPurchaseItemInd.Checked = ChkFixedItemInd.Checked = ChkPlanningItemInd.Checked = ChkServiceItemInd.Checked =
            ChkTaxLiableInd.Checked = ChkControlByDeptCode.Checked = false;
            PicItem.Image = null;
            TxtSalesConvertUOM1.Text = "1";
            TxtSalesConvertUOM2.Text = "1";
            Sm.ClearGrd(Grd1, false);
            Sm.FocusGrd(Grd1, 0, 1);

            Sm.ClearGrd(Grd2, false);
            Sm.FocusGrd(Grd2, 0, 1);

            Sm.ClearGrd(Grd3, true);
            PicItem.Image = null;

            Sm.ClearGrd(GrdSalesPckUnit, true);
            Sm.SetGrdNumValueZero(ref GrdSalesPckUnit, 0, new int[] { 6, 8, 10, 12, 14, 16 });
            GrdSalesPckUnit.Cells[0, 2].Value = 
            GrdSalesPckUnit.Cells[0, 4].Value = 1m;
            GrdSalesPckUnit.Cells[0, 7].Value = 
            GrdSalesPckUnit.Cells[0, 9].Value = mSIWeightUom;
            GrdSalesPckUnit.Cells[0, 11].Value =
            GrdSalesPckUnit.Cells[0, 13].Value =
            GrdSalesPckUnit.Cells[0, 15].Value = mItemLWHUom;
            GrdSalesPckUnit.Cells[0, 17].Value = mItemVolumeUom;
            Sm.FocusGrd(GrdSalesPckUnit, 0, 1);

            Sm.ClearGrd(GrdProperty, true);
        }

        private void LueRequestEdit(
                    iGrid Grd,
                    DevExpress.XtraEditors.LookUpEdit Lue,
                    ref iGCell fCell,
                    ref bool fAccept,
                    TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        private void SetNumberOfUOMDefault()
        {
            if (NumberOfInventoryUomCode < 2) Sm.SetLue(LueInventoryUomCode2, Sm.GetLue(LueInventoryUomCode));
            if (NumberOfInventoryUomCode < 3) Sm.SetLue(LueInventoryUomCode3, Sm.GetLue(LueInventoryUomCode2));

            if (NumberOfSalesUomCode < 2) Sm.SetLue(LueSalesUomCode2, Sm.GetLue(LueSalesUomCode)); 
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmItemFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                InsertDataClick();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtItCode, "", false)) return;
            mIsInsert = false;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                SetNumberOfUOMDefault();

                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                string ItCode = string.Empty;

                if (mIsInsert)
                {
                    var ItCtCode = Sm.GetLue(LueItCtCode);
                    var ItCtCodeLength = ItCtCode.Length + 1;
                    if (TxtItCode.Text.Length == 0)
                    {
                        if (mIsItCodeUseItSeqNo)
                        {
                            ItCode = Sm.GetValue(
                                 "Select Concat(@Param1,'-', " +
                                 "   IfNull((Select Right(Concat(Repeat('0', " + mItCodeSeqNo + "), Convert(V+1, Char)), " + mItCodeSeqNo + ") From ( " +
                                 "      Select ItSeqNo As V " +
                                 "      From TblItem " +
                                 "      Where Left(ItCode, @Param2)=Concat(@Param1, '-') " +
                                 "      Order By ItSeqNo Desc Limit 1 " +
                                 "      ) As Tbl), Right(Concat(Repeat('0', " + mItCodeSeqNo + "), '1'), "+mItCodeSeqNo+") " +
                                 "      )) As ItCode; ",
                                 ItCtCode, (ItCtCode.Length + 1).ToString(), string.Empty);
                        }
                        else
                        {
                            ItCode = Sm.GetValue(
                                 "Select Concat(@Param1,'-', " +
                                 "   IfNull((Select Right(Concat('00000', Convert(ItCode+1, Char)), 5) From ( " +
                                 "       Select Convert(Right(ItCode, 5), Decimal) As ItCode " +
                                 "      From TblItem " +
                                 "      Where Left(ItCode, @Param2)=Concat(@Param1, '-') " +
                                 "      Order By Right(ItCode, 5) Desc Limit 1 " +
                                 "       ) As TblItemTemp), '00001')) As ItCode; ",
                                 ItCtCode, (ItCtCode.Length + 1).ToString(), string.Empty);
                        }
                    }
                    else
                        ItCode = TxtItCode.Text;
                }
                else
                {
                    ItCode = TxtItCode.Text;
                }
                var cml = new List<MySqlCommand>();

                cml.Add(SaveItem(ItCode));
                cml.Add(DeletePackagingUnit(ItCode));
                cml.Add(DeletePackagingUnitInventory(ItCode));
                cml.Add(DeleteItemProperty(ItCode));
                cml.Add(DeleteItemFile(ItCode));

                for (int Row = 0; Row < GrdSalesPckUnit.Rows.Count; Row++)
                    if (Sm.GetGrdStr(GrdSalesPckUnit, Row, 0).Length > 0) cml.Add(SavePackagingUnit(ItCode, Row));
                for (int Row = 0; Row < GrdProperty.Rows.Count; Row++)
                    if (Sm.GetGrdStr(GrdProperty, Row, 0).Length > 0) cml.Add(SaveItemProperty(ItCode, Row));
                for (int Row = 0; Row < Grd3.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd3, Row, 1).Length > 0) cml.Add(SaveItemFile(ItCode, Row));

                Sm.ExecCommands(cml);

                for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd3, Row, 1).Length > 0)
                    {
                        if (mIsItemAllowToUploadFile && Sm.GetGrdStr(Grd3, Row, 1).Length > 0 && Sm.GetGrdStr(Grd3, Row, 1) != "openFileDialog1")
                        {
                            if (Sm.GetGrdStr(Grd3, Row, 1) != Sm.GetGrdStr(Grd3, Row, 4))
                            {
                                UploadFile(ItCode, Row, Sm.GetGrdStr(Grd3, Row, 1));
                            }
                        }
                    }
                }

                ShowData(ItCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }


        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string ItCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                ShowItem(ItCode);
                ShowAsset(ItCode);
                ShowPackagingUnit(ItCode);
                ShowItemProperty(ItCode);
                ShowItemFile(ItCode);

                if (!mIsItemInventoryNotShowCalculationData || mItCode.Length != 0)
                {
                    ShowWhsInfo(ItCode);
                    ComputeInStockQty();
                    ComputeCommittedQty();
                    ComputeRequestedQty(ItCode);
                    ComputeOrderredQty(ItCode);
                }
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        public void ShowData2(string ItCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ShowItem2(ItCode);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.Insert);
                Cursor.Current = Cursors.Default;
            }
        }

        public void ShowItem(string ItCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.* From TblItem T Where T.ItCode=@ItCode ");
            if (mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=T.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@ItCode", ItCode);
            if (mIsFilterByItCt) Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    {
                        //0
                        "ItCode",
                        
                        //1-5
                        "ItName", "ForeignName", "ItCtCode", "ItScCode", "ItBrCode", 
                        
                        //6-10
                        "Specification", "Remark", "VdCode", "PurchaseUomCode", "TaxCode1", 
                        
                        //11-15
                        "TaxCode2", "TaxCode3", "LengthUomCode", "HeightUomCode", "WidthUomCode", 
                        
                        //16-20
                        "VolumeUomCode", "SalesUomCode", "InventoryUomCode", "InventoryUomCode2", "InventoryUomCode3",  
                        
                        //21-25
                        "PlanningUomCode", "ItPropCode1", "ItPropCode2", "ItPropCode3", "ItPropCode4",  
                        
                        //26-30
                        "ItPropCode5", "ItPropCode6", "ItPropCode7", "ItPropCode8", "ItPropCode9", 
                        
                        //31-35
                        "ItPropCode10", "ItPropCode11", "ItPropCode12", "ItPropCode13", "ItPropCode14",  
                        
                        //36-40
                        "ItPropCode15", "MinOrder", "Length", "Height", "Width",
                        
                        //41-45
                        "Volume", "MinStock", "MaxStock","ReOrderStock", "ActInd", 
                        
                        //46-50
                        "InventoryItemInd", "SalesItemInd", "PurchaseItemInd", "FixedItemInd", "PlanningItemInd",
                        
                        //51-55
                        "TaxLiableInd", "ControlByDeptCode", "DiameterUomCode", "Diameter", "ItCodeInternal",
 
                        //56-60
                        "PGCode", "SalesUomCode2", "SalesUomCodeConvert12", "SalesUomCodeConvert21", "InventoryUomCodeConvert12",

                        //61-65
                        "InventoryUomCodeConvert13", "InventoryUomCodeConvert21", "InventoryUomCodeConvert23", "InventoryUomCodeConvert31", "InventoryUomCodeConvert32",

                        //66-70
                        "PlanningUomCode2", "ItGrpCode", "Information1", "Information2", "Information3", 
                        
                        //71-75
                        "Information4", "Information5", "PlanningUomCodeConvert12", "PlanningUomCodeConvert21", "ItemRequestDocNo",

                        //76-80
                        "HSCode", "ServiceItemInd", "Weight", "WeightUomCode", "ItCodeOld"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        if (TxtSource.Text.Length == 0)
                        {
                            TxtItCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtItName.EditValue = Sm.DrStr(dr, c[1]);
                            TxtForeignName.EditValue = Sm.DrStr(dr, c[2]);
                            TxtItCodeInternal.EditValue = Sm.DrStr(dr, c[55]);
                            TxtInvConvertUOM12.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[60]), 0);
                            TxtInvConvertUOM13.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[61]), 0);
                            TxtInvConvertUOM21.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[62]), 0);
                            TxtInvConvertUOM23.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[63]), 0);
                            TxtInvConvertUOM31.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[64]), 0);
                            TxtInvConvertUOM32.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[65]), 0);
                        }
                        Sl.SetLueItCtCodeActive(ref LueItCtCode, Sm.DrStr(dr, c[3]));
                        SetLueItScCode(ref LueItScCode, Sm.GetLue(LueItCtCode));
                        Sm.SetLue(LueItScCode, Sm.DrStr(dr, c[4])); 
                        Sm.SetLue(LueItBrCode, Sm.DrStr(dr, c[5]));
                        MeeSpecification.EditValue = Sm.DrStr(dr, c[6]); 
                        MeeRemark.EditValue = Sm.DrStr(dr, c[7]); 
                        Sm.SetLue(LueVdCode, Sm.DrStr(dr, c[8]));
                        Sm.SetLue(LuePurchaseUomCode, Sm.DrStr(dr, c[9]));
                        Sm.SetLue(LueTaxCode1, Sm.DrStr(dr, c[10])); 
                        Sm.SetLue(LueTaxCode2, Sm.DrStr(dr, c[11]));                 
                        Sm.SetLue(LueTaxCode3, Sm.DrStr(dr, c[12]));
                        Sm.SetLue(LueLengthUomCode, Sm.DrStr(dr, c[13]));
                        Sm.SetLue(LueHeightUomCode, Sm.DrStr(dr, c[14]));
                        Sm.SetLue(LueWidthUomCode, Sm.DrStr(dr, c[15]));
                        Sm.SetLue(LueVolumeUomCode, Sm.DrStr(dr, c[16]));
                        Sm.SetLue(LueInventoryUomCode, Sm.DrStr(dr, c[18]));
                        Sm.SetLue(LueInventoryUomCode2, Sm.DrStr(dr, c[19]));
                        Sm.SetLue(LueInventoryUomCode3, Sm.DrStr(dr, c[20]));
                        Sm.SetLue(LueSalesUomCode, Sm.DrStr(dr, c[17]));
                        Sm.SetLue(LueSalesUomCode2, Sm.DrStr(dr, c[57])); 
                        Sm.SetLue(LuePlanningUomCode, Sm.DrStr(dr, c[21]));
                        Sm.SetLue(LuePlanningUomCode2, Sm.DrStr(dr, c[66]));
                        TxtMinOrder.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[37]), 0);
                        TxtLength.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[38]), 0);
                        TxtHeight.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[39]), 0);
                        TxtWidth.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[40]), 0);
                        TxtVolume.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[41]), 0);
                        TxtMinStock.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[42]), 0);
                        TxtMaxStock.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[43]), 0);
                        TxtReOrderStock.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[44]), 0);
                        ChkActInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[45]), "Y");
                        ChkInventoryItemInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[46]), "Y");
                        ChkSalesItemInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[47]), "Y");
                        ChkPurchaseItemInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[48]), "Y");
                        ChkFixedItemInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[49]), "Y");
                        ChkPlanningItemInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[50]), "Y");
                        ChkTaxLiableInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[51]), "Y");
                        SetLueControlByDeptCode(ref LueControlByDeptCode, Sm.DrStr(dr, c[52]));
                        Sm.SetLue(LueControlByDeptCode, Sm.DrStr(dr, c[52]));
                        Sm.SetLue(LueDiameterUomCode, Sm.DrStr(dr, c[53]));
                        TxtDiameter.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[54]), 0);
                        Sm.SetLue(LuePGCode, Sm.DrStr(dr, c[56]));
                        TxtSalesConvertUOM1.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[58]), 0);
                        TxtSalesConvertUOM2.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[59]), 0);
                        SetLueItGrpCode(ref LueItGrpCode, Sm.DrStr(dr, c[67]));
                        Sm.SetLue(LueItGrpCode, Sm.DrStr(dr, c[67]));
                        Sm.SetLue(LueInformation1, Sm.DrStr(dr, c[68]));
                        Sm.SetLue(LueInformation2, Sm.DrStr(dr, c[69]));
                        Sm.SetLue(LueInformation3, Sm.DrStr(dr, c[70]));
                        Sm.SetLue(LueInformation4, Sm.DrStr(dr, c[71]));
                        Sm.SetLue(LueInformation5, Sm.DrStr(dr, c[72]));
                        TxtPlanningUomCodeConvert12.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[73]), 0);
                        TxtPlanningUomCodeConvert21.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[74]), 0);
                        TxtItemRequestDocNo.EditValue = Sm.DrStr(dr, c[75]);
                        TxtHSCode.EditValue = Sm.DrStr(dr, c[76]);
                        ChkServiceItemInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[77]), "Y");
                        TxtWeight.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[78]), 0);
                        Sm.SetLue(LueWeightUomCode, Sm.DrStr(dr, c[79]));
                        TxtItCodeOld.EditValue = Sm.DrStr(dr, c[80]);
                    }, true
                );
        }

        public void ShowItem2(string ItCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select ItCode, ItName, ");
            SQL.AppendLine("'Y' As ActInd, ItCodeInternal, ForeignName, InventoryItemInd, SalesItemInd, PurchaseItemInd, ");
            SQL.AppendLine("FixedItemInd, PlanningItemInd, ItCtCode, ItScCode, ItBrCode, ItGrpCode, ItemRequestDocNo, ");
            SQL.AppendLine("Specification, Remark, TaxLiableInd, VdCode, PurchaseUomCode, PGCode, MinOrder, ");
            SQL.AppendLine("TaxCode1, TaxCode2, TaxCode3, Length, Height, Width, Volume, Diameter, ");
            SQL.AppendLine("LengthUomCode, HeightUomCode, WidthUomCode, VolumeUomCode, DiameterUomCode, ");
            SQL.AppendLine("SalesUomCode, SalesUomCode2, SalesUomCodeConvert12, SalesUomCodeConvert21, InventoryUomCode, ");
            SQL.AppendLine("InventoryUomCode2, InventoryUomCode3, InventoryUomCodeConvert12, InventoryUomCodeConvert13, ");
            SQL.AppendLine("InventoryUomCodeConvert21, InventoryUomCodeConvert23, InventoryUomCodeConvert31, InventoryUomCodeConvert32, ");
            SQL.AppendLine("MinStock, MaxStock, ReOrderStock, ControlByDeptCode, PlanningUomCode, PlanningUomCode2, ");
            SQL.AppendLine("PlanningUomCodeConvert12, PlanningUomCodeConvert21, Information1, Information2, Information3, ");
            SQL.AppendLine("Information4, Information5, ItPropCode1, ItPropCode2, ItPropCode3, ItPropCode4, ");
            SQL.AppendLine("ItPropCode5, ItPropCode6, ItPropCode7, ItPropCode8, ItPropCode9, ItPropCode10, ");
            SQL.AppendLine("ItPropCode11, ItPropCode12, ItPropCode13, ItPropCode14, ItPropCode15, ");
            SQL.AppendLine("Weight, WeightUomCode, ");
            SQL.AppendLine("CreateBy, CreateDt, LastUpBy, LastUpDt ");
            SQL.AppendLine("From TblItem T Where T.ItCode=@ItCode ");
            if (mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=T.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select Null As ItCode, A.ItName, 'Y' As ActInd, ");
            SQL.AppendLine("A.ItCodeInternal, A.ForeignName, A.InventoryItemInd, A.SalesItemInd, A.PurchaseItemInd, ");
            SQL.AppendLine("A.FixedItemInd, A.PlanningItemInd, A.ItCtCode, A.ItScCode, A.ItBrCode, A.ItGrpCode, A.DocNo As ItemRequestDocNo, ");
            SQL.AppendLine("A.Specification, A.Remark, A.TaxLiableInd, Null As VdCode, Null As PurchaseUomCode, Null As PGCode, 0 As MinOrder, ");
            SQL.AppendLine("Null As TaxCode1, Null As TaxCode2, Null As TaxCode3, 0 As Length, 0 As Height, 0 As Width, 0 As Volume, 0 As Diameter, ");
            SQL.AppendLine("Null As LengthUomCode, Null As HeightUomCode, Null As WidthUomCode, Null As VolumeUomCode, Null As DiameterUomCode, ");
            SQL.AppendLine("Null As SalesUomCode, Null As SalesUomCode2, 0 As SalesUomCodeConvert12, 0 As SalesUomCodeConvert21, Null As InventoryUomCode, ");
            SQL.AppendLine("Null As InventoryUomCode2, Null As InventoryUomCode3, 0 As InventoryUomCodeConvert12, 0 As InventoryUomCodeConvert13, ");
            SQL.AppendLine("0 As InventoryUomCodeConvert21, 0 As InventoryUomCodeConvert23, 0 As InventoryUomCodeConvert31, 0 As InventoryUomCodeConvert32, ");
            SQL.AppendLine("0 As MinStock, 0 As MaxStock, 0 As ReOrderStock, Null As ControlByDeptCode, Null As PlanningUomCode, Null As PlanningUomCode2, ");
            SQL.AppendLine("0 As PlanningUomCodeConvert12, 0 As PlanningUomCodeConvert21, Null As Information1, Null As Information2, Null As Information3, ");
            SQL.AppendLine("Null As Information4, Null As Information5, Null As ItPropCode1, Null As ItPropCode2, Null As ItPropCode3, Null As ItPropCode4, ");
            SQL.AppendLine("Null As ItPropCode5, Null As ItPropCode6, Null As ItPropCode7, Null As ItPropCode8, Null As ItPropCode9, Null As ItPropCode10, ");
            SQL.AppendLine("Null As ItPropCode11, Null As ItPropCode12, Null As ItPropCode13, Null As ItPropCode14, Null As ItPropCode15, ");
            SQL.AppendLine("0.00 As Weight, Null As WeightUomCode, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("From TblItemRequest A Where A.DocNo = @ItCode ");
            if (mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=A.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@ItCode", ItCode);
            if (mIsFilterByItCt) Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    {
                        //0
                        "ItCode",
                        
                        //1-5
                        "ItName", "ForeignName", "ItCtCode", "ItScCode", "ItBrCode", 
                        
                        //6-10
                        "Specification", "Remark", "VdCode", "PurchaseUomCode", "TaxCode1", 
                        
                        //11-15
                        "TaxCode2", "TaxCode3", "LengthUomCode", "HeightUomCode", "WidthUomCode", 
                        
                        //16-20
                        "VolumeUomCode", "SalesUomCode", "InventoryUomCode", "InventoryUomCode2", "InventoryUomCode3",  
                        
                        //21-25
                        "PlanningUomCode", "ItPropCode1", "ItPropCode2", "ItPropCode3", "ItPropCode4",  
                        
                        //26-30
                        "ItPropCode5", "ItPropCode6", "ItPropCode7", "ItPropCode8", "ItPropCode9", 
                        
                        //31-35
                        "ItPropCode10", "ItPropCode11", "ItPropCode12", "ItPropCode13", "ItPropCode14",  
                        
                        //36-40
                        "ItPropCode15", "MinOrder", "Length", "Height", "Width",
                        
                        //41-45
                        "Volume", "MinStock", "MaxStock","ReOrderStock", "ActInd", 
                        
                        //46-50
                        "InventoryItemInd", "SalesItemInd", "PurchaseItemInd", "FixedItemInd", "PlanningItemInd",
                        
                        //51-55
                        "TaxLiableInd", "ControlByDeptCode", "DiameterUomCode", "Diameter", "ItCodeInternal",
 
                        //56-60
                        "PGCode", "SalesUomCode2", "SalesUomCodeConvert12", "SalesUomCodeConvert21", "InventoryUomCodeConvert12",

                        //61-65
                        "InventoryUomCodeConvert13", "InventoryUomCodeConvert21", "InventoryUomCodeConvert23", "InventoryUomCodeConvert31", "InventoryUomCodeConvert32",

                        //66-70
                        "PlanningUomCode2", "ItGrpCode", "Information1", "Information2", "Information3", 
                        
                        //71-75
                        "Information4", "Information5", "PlanningUomCodeConvert12", "PlanningUomCodeConvert21", "ItemRequestDocNo",

                        //76-77
                        "Weight", "WeightUomCode"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        if (TxtSource.Text.Length == 0)
                        {
                            TxtItCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtItName.EditValue = Sm.DrStr(dr, c[1]);
                            TxtForeignName.EditValue = Sm.DrStr(dr, c[2]);
                            TxtItCodeInternal.EditValue = Sm.DrStr(dr, c[55]);
                            TxtInvConvertUOM12.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[60]), 0);
                            TxtInvConvertUOM13.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[61]), 0);
                            TxtInvConvertUOM21.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[62]), 0);
                            TxtInvConvertUOM23.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[63]), 0);
                            TxtInvConvertUOM31.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[64]), 0);
                            TxtInvConvertUOM32.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[65]), 0);
                        }
                        Sl.SetLueItCtCodeActive(ref LueItCtCode, Sm.DrStr(dr, c[3]));
                        SetLueItScCode(ref LueItScCode, Sm.GetLue(LueItCtCode));
                        Sm.SetLue(LueItScCode, Sm.DrStr(dr, c[4]));
                        Sm.SetLue(LueItBrCode, Sm.DrStr(dr, c[5]));
                        MeeSpecification.EditValue = Sm.DrStr(dr, c[6]);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[7]);
                        Sm.SetLue(LueVdCode, Sm.DrStr(dr, c[8]));
                        Sm.SetLue(LuePurchaseUomCode, Sm.DrStr(dr, c[9]));
                        Sm.SetLue(LueTaxCode1, Sm.DrStr(dr, c[10]));
                        Sm.SetLue(LueTaxCode2, Sm.DrStr(dr, c[11]));
                        Sm.SetLue(LueTaxCode3, Sm.DrStr(dr, c[12]));
                        Sm.SetLue(LueLengthUomCode, Sm.DrStr(dr, c[13]));
                        Sm.SetLue(LueHeightUomCode, Sm.DrStr(dr, c[14]));
                        Sm.SetLue(LueWidthUomCode, Sm.DrStr(dr, c[15]));
                        Sm.SetLue(LueVolumeUomCode, Sm.DrStr(dr, c[16]));
                        Sm.SetLue(LueInventoryUomCode, Sm.DrStr(dr, c[18]));
                        Sm.SetLue(LueInventoryUomCode2, Sm.DrStr(dr, c[19]));
                        Sm.SetLue(LueInventoryUomCode3, Sm.DrStr(dr, c[20]));
                        Sm.SetLue(LueSalesUomCode, Sm.DrStr(dr, c[17]));
                        Sm.SetLue(LueSalesUomCode2, Sm.DrStr(dr, c[57]));
                        Sm.SetLue(LuePlanningUomCode, Sm.DrStr(dr, c[21]));
                        Sm.SetLue(LuePlanningUomCode2, Sm.DrStr(dr, c[66]));
                        TxtMinOrder.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[37]), 0);
                        TxtLength.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[38]), 0);
                        TxtHeight.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[39]), 0);
                        TxtWidth.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[40]), 0);
                        TxtVolume.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[41]), 0);
                        TxtMinStock.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[42]), 0);
                        TxtMaxStock.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[43]), 0);
                        TxtReOrderStock.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[44]), 0);
                        ChkActInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[45]), "Y");
                        ChkInventoryItemInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[46]), "Y");
                        ChkSalesItemInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[47]), "Y");
                        ChkPurchaseItemInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[48]), "Y");
                        ChkFixedItemInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[49]), "Y");
                        ChkPlanningItemInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[50]), "Y");
                        ChkTaxLiableInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[51]), "Y");
                        SetLueControlByDeptCode(ref LueControlByDeptCode, Sm.DrStr(dr, c[52]));
                        Sm.SetLue(LueControlByDeptCode, Sm.DrStr(dr, c[52]));
                        Sm.SetLue(LueDiameterUomCode, Sm.DrStr(dr, c[53]));
                        TxtDiameter.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[54]), 0);
                        Sm.SetLue(LuePGCode, Sm.DrStr(dr, c[56]));
                        TxtSalesConvertUOM1.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[58]), 0);
                        TxtSalesConvertUOM2.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[59]), 0);
                        SetLueItGrpCode(ref LueItGrpCode, Sm.DrStr(dr, c[67]));
                        Sm.SetLue(LueItGrpCode, Sm.DrStr(dr, c[67]));
                        Sm.SetLue(LueInformation1, Sm.DrStr(dr, c[68]));
                        Sm.SetLue(LueInformation2, Sm.DrStr(dr, c[69]));
                        Sm.SetLue(LueInformation3, Sm.DrStr(dr, c[70]));
                        Sm.SetLue(LueInformation4, Sm.DrStr(dr, c[71]));
                        Sm.SetLue(LueInformation5, Sm.DrStr(dr, c[72]));
                        TxtPlanningUomCodeConvert12.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[73]), 0);
                        TxtPlanningUomCodeConvert21.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[74]), 0);
                        TxtItemRequestDocNo.EditValue = Sm.DrStr(dr, c[75]);
                        TxtWeight.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[76]), 0);
                        Sm.SetLue(LueWeightUomCode, Sm.DrStr(dr, c[77]));
                    }, true
                );
        }

        private void ShowAsset(string ItCode)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@ItCode", ItCode);
            Sm.ShowDataInGrid(
                    ref Grd2, ref cm,
                    "Select AssetCode, AssetName, AssetType, ActiveInd, AssetDt " +
                    "From TblAsset Where ItCode=@ItCode Order By AssetCode;",
                    new string[]
                    {
                        //0
                        "AssetCode", 
                            
                        //1-4
                        "AssetName", "AssetType", "ActiveInd", "AssetDt"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 5, 4);
                    }, false, false, false, true
                );
            Sm.FocusGrd(Grd2, 0, 1);
        }

        private void ShowPackagingUnit(string ItCode)
        {
            var cm = new MySqlCommand();
            var SQLPU = new StringBuilder();

            SQLPU.AppendLine("Select A.UOMCode, B.UOMName, A.Qty, A.Qty2, A.GW, A.NW, ");
            SQLPU.AppendLine("D1.UOMName As SalesUOM1, D2.UOMName As SalesUOM2, ");
            SQLPU.AppendLine("A.Length, A.Width, A.Height, A.Volume ");
            SQLPU.AppendLine("From TblItemPackagingUnit A ");
            SQLPU.AppendLine("Inner Join TblUOM B On A.UOMCode=B.UOMCode ");
            SQLPU.AppendLine("Inner Join TblItem C On A.ItCode=C.ItCode ");
            SQLPU.AppendLine("Inner Join TblUOM D1 On D1.UOMCode=C.SalesUOMCode ");
            SQLPU.AppendLine("Inner Join TblUOM D2 On D2.UOMCode=C.SalesUOMCode2 ");
            SQLPU.AppendLine("Where A.ItCode=@ItCode Order By A.UOMCode; ");
            SQLPU.AppendLine(" ");


            Sm.CmParam<String>(ref cm, "@ItCode", ItCode);
            Sm.ShowDataInGrid(
                    ref GrdSalesPckUnit, ref cm, SQLPU.ToString(),
                    new string[]
                    {
                        //0
                        "UOMCode", 
                            
                        //1-5
                        "UOMName", "Qty", "Qty2", "SalesUOM1", "SalesUOM2",

                        //6-10
                        "GW", "NW", "Length", "Width", "Height", 
                        
                        //11
                        "Volume"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 4);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 6);
                        Grd.Cells[Row, 7].Value = mSIWeightUom;
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                        Grd.Cells[Row, 9].Value = mSIWeightUom;
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 8);
                        Grd.Cells[Row, 11].Value = mItemLWHUom;
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 9);
                        Grd.Cells[Row, 13].Value = mItemLWHUom;
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 10);
                        Grd.Cells[Row, 15].Value = mItemLWHUom;
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 11);
                        Grd.Cells[Row, 17].Value = mItemVolumeUom;
                    }, false, false, true, false
                );
            Sm.FocusGrd(GrdSalesPckUnit, 0, 1);
        }

        private void ShowItemProperty(string ItCode)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@ItCode", ItCode);
            Sm.ShowDataInGrid(
                    ref GrdProperty, ref cm,
                    "Select B.PropCode, B.PropName " +
                    "From TblItemProperty A " +
                    "Inner Join TblProperty B On A.PropCode=B.PropCode " +
                    "Where A.ItCode=@ItCode Order By A.PropCode",
                    new string[]{ "PropCode", "PropName" },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    }, false, false, false, false
                );
        }

        private void ShowItemFile(string ItCode)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@ItCode", ItCode);
            Sm.ShowDataInGrid(
                    ref Grd3, ref cm,
                   "select DNo, FileName from  TblItemFile Where ItCode=@ItCode Order By Dno",

                    new string[] 
                    { 
                        "Dno",
                        "FileName"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd3, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd3, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd3, dr, c, Row, 4, 1);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd3, 0, 0);
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                //Sm.IsTxtEmpty(TxtItCode, "Item code", false) ||
                Sm.IsTxtEmpty(TxtItName, "Item name", false) ||
                (mIsItemItCodeInternalMandatory && Sm.IsTxtEmpty(TxtItCodeInternal, "Item's local code", false)) ||
                IsItNameExisted() ||
                IsItCodeInternalExisted() ||
                IsItCodeInternalEmpty() ||
                (mIsItGrpCodeMandatoryInMasterItem && Sm.IsLueEmpty(LueItGrpCode, "Item's group")) ||
                Sm.IsLueEmpty(LueItCtCode, "Item's category") ||
                Sm.IsLueEmpty(LuePurchaseUomCode, "Item's purchase UoM") ||
                Sm.IsLueEmpty(LueSalesUomCode, "Item's sales UoM") ||
                Sm.IsLueEmpty(LueSalesUomCode2, "Item's sales uom (2)") ||
                IsDuplicatePackageUOM() ||
                Sm.IsLueEmpty(LueInventoryUomCode, "Item's inventory UoM") ||
                Sm.IsLueEmpty(LueInventoryUomCode2, "Item's inventory uom (2)") ||
                Sm.IsLueEmpty(LueInventoryUomCode3, "Item's inventory uom (3)") ||
                Sm.IsLueEmpty(LuePlanningUomCode, "Item's planning UoM") ||
                Sm.IsLueEmpty(LuePlanningUomCode2, "Item's planning uom (2)") ||
                IsItCodeExisted() ||
                IsUomConvertNotValid();
        }

        private bool IsUomConvertNotValid()
        {
            var PlanningUomCode = Sm.GetLue(LuePlanningUomCode);
            decimal PlanningUomCodeConvert12 = 0m, PlanningUomCodeConvert21 = 0m;

            if (TxtPlanningUomCodeConvert12.Text.Length > 0)
                PlanningUomCodeConvert12 = decimal.Parse(TxtPlanningUomCodeConvert12.Text);
            if (TxtPlanningUomCodeConvert21.Text.Length > 0)
                PlanningUomCodeConvert21 = decimal.Parse(TxtPlanningUomCodeConvert21.Text);

            if (Sm.CompareStr(PlanningUomCode, Sm.GetLue(LuePlanningUomCode2)))
            {
                if (PlanningUomCodeConvert12 != 1)
                {
                    Sm.StdMsg(mMsgType.Warning, "Invalid planning uom convertion.");
                    TxtPlanningUomCodeConvert12.Focus();
                    return true;
                }
                if (PlanningUomCodeConvert21 != 1)
                {
                    Sm.StdMsg(mMsgType.Warning, "Invalid planning uom convertion.");
                    TxtPlanningUomCodeConvert21.Focus();
                    return true;
                }
            }
            else
            {
                if (PlanningUomCodeConvert12 == 1)
                {
                    Sm.StdMsg(mMsgType.Warning, "Invalid planning uom convertion.");
                    TxtPlanningUomCodeConvert12.Focus();
                    return true;
                }
                if (PlanningUomCodeConvert21 == 1)
                {
                    Sm.StdMsg(mMsgType.Warning, "Invalid planning uom convertion.");
                    TxtPlanningUomCodeConvert21.Focus();
                    return true;
                }
            }

            return false;
        }

        private bool IsDuplicatePackageUOM()
        {
            bool rslt = false;
            for (int intX=0; intX < GrdSalesPckUnit.Rows.Count; intX++)
                for (int intY=intX+1; intY < GrdSalesPckUnit.Rows.Count; intY++)
                    if ((Sm.GetGrdStr(GrdSalesPckUnit, intX, 0) != string.Empty) &&
                        (Sm.GetGrdStr(GrdSalesPckUnit, intX, 0) == Sm.GetGrdStr(GrdSalesPckUnit, intY, 0)))
                    {
                        rslt = true;
                    }
            if (rslt) Sm.StdMsg(mMsgType.Warning, "Duplicate Sales Packaging Unit Found.");
            return rslt;
        }

        private bool IsItNameExisted()
        {
            var cm = new MySqlCommand() { CommandText = "Select ItCode From TblItem Where ItName=@ItName And ItCode<>@ItCode Limit 1;" };
            Sm.CmParam<String>(ref cm, "@ItCode", TxtItCode.Text.Length == 0 ? "XXX" : TxtItCode.Text);
            Sm.CmParam<String>(ref cm, "@ItName", TxtItName.Text);

            string ItCode = Sm.GetValue(cm);
            if (ItCode.Length == 0)
                return false;
            else
            {
                return Sm.StdMsgYN("Question",
                    "Item Code : " + ItCode + Environment.NewLine +
                    "Item Name : " + TxtItName.Text + Environment.NewLine + Environment.NewLine +
                    "Existing item code With the same name." + Environment.NewLine +
                    "Do you want to continue to save this data ?"
                    ) == DialogResult.No;
            }
        }

        private bool IsItCodeInternalExisted()
        {
            var cm = new MySqlCommand() { CommandText = "Select ItCode From TblItem Where ItCodeInternal=@ItCodeInternal And ItCode<>@ItCode Limit 1;" };
            Sm.CmParam<String>(ref cm, "@ItCode", TxtItCode.Text.Length == 0 ? "XXX" : TxtItCode.Text);
            Sm.CmParam<String>(ref cm, "@ItCodeInternal", TxtItCodeInternal.Text);

            string ItCode = Sm.GetValue(cm);
            if (ItCode.Length == 0)
                return false;
            else
            {
                return Sm.StdMsgYN("Question",
                    "Item Code : " + ItCode + Environment.NewLine +
                    "Item local code : " + TxtItCodeInternal.Text + Environment.NewLine + Environment.NewLine +
                    "Existing item code With the same local code." + Environment.NewLine +
                    "Do you want to continue to save this data ?"
                    ) == DialogResult.No;
            }
        }

        private bool IsItCodeInternalEmpty()
        {
            if (mItCodeInternalNotEmptyInd != "Y") return false;

            if (TxtItCodeInternal.Text.Length == 0)
            {
                return Sm.StdMsgYN("Question",
                    "Item Name : " + TxtItName.Text + Environment.NewLine + Environment.NewLine +
                    "Item local code is empty." + Environment.NewLine +
                    "Do you want to continue to save this data ?"
                    ) == DialogResult.No;
            }
            return false;
        }

        private bool IsItCodeExisted()
        {
            if (!TxtItCode.Properties.ReadOnly && Sm.IsDataExist("Select 1 From TblItem Where ItCode='" + TxtItCode.Text + "' Limit 1"))
            {
                Sm.StdMsg(mMsgType.Warning, "Item code ( " + TxtItCode.Text + " ) already existed.");
                return true;
            }
            return false;
        }

        private MySqlCommand SaveItem(string ItCode)
        {
            var SQL = new StringBuilder();

            if (mIsInsert)
            {
                SQL.AppendLine("Insert Into TblItem(ItCode, ItName, ItSeqNo, ForeignName, ItCodeInternal, ItCodeOld, ItCtCode, ItScCode, ItemRequestDocNo, ");
                SQL.AppendLine("    ItBrCode, ItGrpCode, Specification, Remark, VdCode, PurchaseUomCode, PGCode, ");
                SQL.AppendLine("    TaxCode1, TaxCode2, TaxCode3, LengthUomCode, HeightUomCode, DiameterUomCode, ");
                SQL.AppendLine("    WidthUomCode, VolumeUomCode, WeightUomCode, SalesUomCode, SalesUomCode2, SalesUomCodeConvert12, ");
                SQL.AppendLine("    SalesUomCodeConvert21, InventoryUomCode, InventoryUomCode2, InventoryUomCode3, PlanningUomCode, PlanningUomCode2, ");
                SQL.AppendLine("    MinOrder, Length, Height, Width, Volume, Weight, Diameter, ");
                SQL.AppendLine("    MinStock, MaxStock, ReOrderStock, ControlByDeptCode, ActInd, InventoryItemInd, ");
                SQL.AppendLine("    SalesItemInd, PurchaseItemInd, FixedItemInd, PlanningItemInd, ServiceItemInd,TaxLiableInd, ");
                SQL.AppendLine("    InventoryUomCodeConvert12, InventoryUomCodeConvert13, InventoryUomCodeConvert21, InventoryUomCodeConvert23, ");
                SQL.AppendLine("    InventoryUomCodeConvert31, InventoryUomCodeConvert32, ");
                SQL.AppendLine("    PlanningUomCodeConvert12, PlanningUomCodeConvert21, ");
                SQL.AppendLine("    Information1, Information2, Information3, Information4, Information5, HSCode, ");
                SQL.AppendLine("    CreateBy, CreateDt)");
                SQL.AppendLine("Values(@ItCode, @ItName, @ItSeqNo, @ForeignName, @ItCodeInternal, @ItCodeOld, @ItCtCode, @ItScCode, @ItemRequestDocNo, ");
                SQL.AppendLine("    @ItBrCode, @ItGrpCode, @Specification, @Remark, @VdCode, @PurchaseUomCode, @PGCode, ");
                SQL.AppendLine("    @TaxCode1, @TaxCode2, @TaxCode3, @LengthUomCode, @HeightUomCode, @DiameterUomCode, ");
                SQL.AppendLine("    @WidthUomCode, @VolumeUomCode, @WeightUomCode, @SalesUomCode, @SalesUomCode2, ");
                SQL.AppendLine("    Case When @SalesUomCode=@SalesUomCode2 Then 1.00 Else @SalesUomCodeConvert12 End, ");
                SQL.AppendLine("    Case When @SalesUomCode=@SalesUomCode2 Then 1.00 Else @SalesUomCodeConvert21 End, ");
                SQL.AppendLine("    @InventoryUomCode, @InventoryUomCode2, @InventoryUomCode3, @PlanningUomCode, @PlanningUomCode2, ");
                SQL.AppendLine("    @MinOrder, @Length, @Height, @Width, @Volume, @Weight, @Diameter, ");
                SQL.AppendLine("    @MinStock, @MaxStock, @ReOrderStock, @ControlByDeptCode, @ActInd, @InventoryItemInd, ");
                SQL.AppendLine("    @SalesItemInd, @PurchaseItemInd, @FixedItemInd, @PlanningItemInd, @ServiceItemInd, @TaxLiableInd, ");
                SQL.AppendLine("    Case When @InventoryUomCode=@InventoryUomCode2 Then 1.00 Else @InventoryUomCodeConvert12 End, ");
                SQL.AppendLine("    Case When @InventoryUomCode=@InventoryUomCode3 Then 1.00 Else @InventoryUomCodeConvert13 End, ");
                SQL.AppendLine("    Case When @InventoryUomCode=@InventoryUomCode2 Then 1.00 Else @InventoryUomCodeConvert21 End, ");
                SQL.AppendLine("    Case When @InventoryUomCode2=@InventoryUomCode3 Then 1.00 Else @InventoryUomCodeConvert23 End, ");
                SQL.AppendLine("    Case When @InventoryUomCode=@InventoryUomCode3 Then 1.00 Else @InventoryUomCodeConvert31 End, ");
                SQL.AppendLine("    Case When @InventoryUomCode2=@InventoryUomCode3 Then 1.00 Else @InventoryUomCodeConvert32 End, ");
                SQL.AppendLine("    Case When @PlanningUomCode=@PlanningUomCode2 Then 1.00 Else @PlanningUomCodeConvert12 End, ");
                SQL.AppendLine("    Case When @PlanningUomCode=@PlanningUomCode2 Then 1.00 Else @PlanningUomCodeConvert21 End, ");
                SQL.AppendLine("    @Information1, @Information2, @Information3, @Information4, @Information5, @HSCode, ");
                SQL.AppendLine("    @UserCode, CurrentDateTime()); ");
            }
            else
            {
                SQL.AppendLine("Update TblItem Set ");
                if (mProcFormatDocNo == "1")
                    SQL.AppendLine(" ItCtCode = @ItCtCode, ");
                SQL.AppendLine("        ItName=@ItName, ForeignName=@ForeignName, ItCodeInternal=@ItCodeInternal, ItCodeOld=@ItCodeOld, ItCtCode=@ItCtCode, ItScCode=@ItScCode, ItemRequestDocNo = @ItemRequestDocNo, ");
                SQL.AppendLine("        ItBrCode=@ItBrCode, ItGrpCode=@ItGrpCode, Specification=@Specification, Remark=@Remark, VdCode=@VdCode, PurchaseUomCode=@PurchaseUomCode, PGCode=@PGCode, ");
                SQL.AppendLine("        TaxCode1=@TaxCode1, TaxCode2=@TaxCode2, TaxCode3=@TaxCode3, LengthUomCode=@LengthUomCode, HeightUomCode=@HeightUomCode, DiameterUomCode=@DiameterUomCode, ");
                SQL.AppendLine("        WidthUomCode=@WidthUomCode, VolumeUomCode=@VolumeUomCode, WeightUomCode=@WeightUomCode, SalesUomCode=@SalesUomCode, SalesUomCode2=@SalesUomCode2, ");
                SQL.AppendLine("        SalesUomCodeConvert12=Case When @SalesUomCode=@SalesUomCode2 Then 1.00 Else @SalesUomCodeConvert12 End, ");
                SQL.AppendLine("        SalesUomCodeConvert21=Case When @SalesUomCode=@SalesUomCode2 Then 1.00 Else @SalesUomCodeConvert21 End, ");
                SQL.AppendLine("        InventoryUomCode=@InventoryUomCode, InventoryUomCode2=@InventoryUomCode2, InventoryUomCode3=@InventoryUomCode3, PlanningUomCode=@PlanningUomCode, PlanningUomCode2=@PlanningUomCode2, ");
                SQL.AppendLine("        MinOrder=@MinOrder, Length=@Length, Height=@Height, Width=@Width, Volume=@Volume, Weight=@Weight, Diameter=@Diameter, ");
                SQL.AppendLine("        MinStock=@MinStock, MaxStock=@MaxStock, ReOrderStock=@ReOrderStock, ControlByDeptCode=@ControlByDeptCode, ActInd=@ActInd, InventoryItemInd=@InventoryItemInd, ");
                SQL.AppendLine("        SalesItemInd=@SalesItemInd, PurchaseItemInd=@PurchaseItemInd, FixedItemInd=@FixedItemInd, PlanningItemInd=@PlanningItemInd, ServiceItemInd = @ServiceItemInd, TaxLiableInd=@TaxLiableInd, ");
                SQL.AppendLine("        InventoryUomCodeConvert12=Case When @InventoryUomCode=@InventoryUomCode2 Then 1.00 Else @InventoryUomCodeConvert12 End, ");
                SQL.AppendLine("        InventoryUomCodeConvert13=Case When @InventoryUomCode=@InventoryUomCode3 Then 1.00 Else @InventoryUomCodeConvert13 End, ");
                SQL.AppendLine("        InventoryUomCodeConvert21=Case When @InventoryUomCode2=@InventoryUomCode Then 1.00 Else @InventoryUomCodeConvert21 End, ");
                SQL.AppendLine("        InventoryUomCodeConvert23=Case When @InventoryUomCode2=@InventoryUomCode3 Then 1.00 Else @InventoryUomCodeConvert23 End, ");
                SQL.AppendLine("        InventoryUomCodeConvert31=Case When @InventoryUomCode3=@InventoryUomCode Then 1.00 Else @InventoryUomCodeConvert31 End, ");
                SQL.AppendLine("        InventoryUomCodeConvert32=Case When @InventoryUomCode3=@InventoryUomCode2 Then 1.00 Else @InventoryUomCodeConvert32 End, ");
                SQL.AppendLine("        PlanningUomCodeConvert12=Case When @PlanningUomCode=@PlanningUomCode2 Then 1.00 Else @PlanningUomCodeConvert12 End, ");
                SQL.AppendLine("        PlanningUomCodeConvert21=Case When @PlanningUomCode=@PlanningUomCode2 Then 1.00 Else @PlanningUomCodeConvert21 End, ");
                SQL.AppendLine("        Information1=@Information1, Information2=@Information2, Information3=@Information3, Information4=@Information4, Information5=@Information5, HSCode=@HSCode, ");
                SQL.AppendLine("        LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
                SQL.AppendLine("Where ItCode=@ItCode; ");
            }
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@ItCode", ItCode);
            Sm.CmParam<String>(ref cm, "@ItName", TxtItName.Text);
            Sm.CmParam<String>(ref cm, "@ItCodeOld", TxtItCodeOld.Text);
            if (mIsItCodeUseItSeqNo)
                Sm.CmParam<Decimal>(ref cm, "@ItSeqNo", decimal.Parse(Sm.Right(ItCode, int.Parse(mItCodeSeqNo))));
            else
                Sm.CmParam<Decimal>(ref cm, "@ItSeqNo", 0m);
            Sm.CmParam<String>(ref cm, "@ForeignName", TxtForeignName.Text);
            Sm.CmParam<String>(ref cm, "@ItCodeInternal", TxtItCodeInternal.Text);
            Sm.CmParam<String>(ref cm, "@ItemRequestDocNo", TxtItemRequestDocNo.Text);
            Sm.CmParam<String>(ref cm, "@ItCtCode", Sm.GetLue(LueItCtCode));
            Sm.CmParam<String>(ref cm, "@ItScCode", Sm.GetLue(LueItScCode));
            Sm.CmParam<String>(ref cm, "@ItBrCode", Sm.GetLue(LueItBrCode));
            Sm.CmParam<String>(ref cm, "@ItGrpCode", Sm.GetLue(LueItGrpCode));
            Sm.CmParam<String>(ref cm, "@Specification", MeeSpecification.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@VdCode", Sm.GetLue(LueVdCode));
            Sm.CmParam<String>(ref cm, "@PurchaseUomCode", Sm.GetLue(LuePurchaseUomCode));
            Sm.CmParam<String>(ref cm, "@PGCode", Sm.GetLue(LuePGCode));
            Sm.CmParam<String>(ref cm, "@TaxCode1", Sm.GetLue(LueTaxCode1));
            Sm.CmParam<String>(ref cm, "@TaxCode2", Sm.GetLue(LueTaxCode2));
            Sm.CmParam<String>(ref cm, "@TaxCode3", Sm.GetLue(LueTaxCode3));
            Sm.CmParam<String>(ref cm, "@LengthUomCode", Sm.GetLue(LueLengthUomCode));
            Sm.CmParam<String>(ref cm, "@HeightUomCode", Sm.GetLue(LueHeightUomCode));
            Sm.CmParam<String>(ref cm, "@WidthUomCode", Sm.GetLue(LueWidthUomCode));
            Sm.CmParam<String>(ref cm, "@VolumeUomCode", Sm.GetLue(LueVolumeUomCode));
            Sm.CmParam<String>(ref cm, "@WeightUomCode", Sm.GetLue(LueWeightUomCode));
            Sm.CmParam<String>(ref cm, "@DiameterUomCode", Sm.GetLue(LueDiameterUomCode));
            Sm.CmParam<String>(ref cm, "@SalesUomCode", Sm.GetLue(LueSalesUomCode));
            Sm.CmParam<String>(ref cm, "@SalesUomCode2", Sm.GetLue(LueSalesUomCode2));
            Sm.CmParam<Decimal>(ref cm, "@SalesUomCodeConvert12", Decimal.Parse(TxtSalesConvertUOM1.Text));
            Sm.CmParam<Decimal>(ref cm, "@SalesUomCodeConvert21", Decimal.Parse(TxtSalesConvertUOM2.Text));
            Sm.CmParam<String>(ref cm, "@InventoryUomCode", Sm.GetLue(LueInventoryUomCode));
            Sm.CmParam<String>(ref cm, "@InventoryUomCode2", Sm.GetLue(LueInventoryUomCode2));
            Sm.CmParam<String>(ref cm, "@InventoryUomCode3", Sm.GetLue(LueInventoryUomCode3));
            Sm.CmParam<String>(ref cm, "@PlanningUomCode", Sm.GetLue(LuePlanningUomCode));
            Sm.CmParam<String>(ref cm, "@PlanningUomCode2", Sm.GetLue(LuePlanningUomCode2));
            Sm.CmParam<Decimal>(ref cm, "@MinOrder", Decimal.Parse(TxtMinOrder.Text));
            Sm.CmParam<Decimal>(ref cm, "@Length", Decimal.Parse(TxtLength.Text));
            Sm.CmParam<Decimal>(ref cm, "@Height", Decimal.Parse(TxtHeight.Text));
            Sm.CmParam<Decimal>(ref cm, "@Width", Decimal.Parse(TxtWidth.Text));
            Sm.CmParam<Decimal>(ref cm, "@Volume", Decimal.Parse(TxtVolume.Text));
            Sm.CmParam<Decimal>(ref cm, "@Weight", Decimal.Parse(TxtWeight.Text));
            Sm.CmParam<Decimal>(ref cm, "@Diameter", Decimal.Parse(TxtDiameter.Text));
            Sm.CmParam<Decimal>(ref cm, "@MinStock", Decimal.Parse(TxtMinStock.Text));
            Sm.CmParam<Decimal>(ref cm, "@MaxStock", Decimal.Parse(TxtMaxStock.Text));
            Sm.CmParam<Decimal>(ref cm, "@ReOrderStock", Decimal.Parse(TxtReOrderStock.Text));
            Sm.CmParam<String>(ref cm, "@ControlByDeptCode", Sm.GetLue(LueControlByDeptCode));
            Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@InventoryItemInd", ChkInventoryItemInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@SalesItemInd", ChkSalesItemInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@PurchaseItemInd", ChkPurchaseItemInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@FixedItemInd", ChkFixedItemInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@PlanningItemInd", ChkPlanningItemInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@ServiceItemInd", ChkServiceItemInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@TaxLiableInd", ChkTaxLiableInd.Checked ? "Y" : "N");
            Sm.CmParam<Decimal>(ref cm, "@InventoryUomCodeConvert12", Decimal.Parse(TxtInvConvertUOM12.Text));
            Sm.CmParam<Decimal>(ref cm, "@InventoryUomCodeConvert13", Decimal.Parse(TxtInvConvertUOM13.Text));
            Sm.CmParam<Decimal>(ref cm, "@InventoryUomCodeConvert21", Decimal.Parse(TxtInvConvertUOM21.Text));
            Sm.CmParam<Decimal>(ref cm, "@InventoryUomCodeConvert23", Decimal.Parse(TxtInvConvertUOM23.Text));
            Sm.CmParam<Decimal>(ref cm, "@InventoryUomCodeConvert31", Decimal.Parse(TxtInvConvertUOM31.Text));
            Sm.CmParam<Decimal>(ref cm, "@InventoryUomCodeConvert32", Decimal.Parse(TxtInvConvertUOM32.Text));
            Sm.CmParam<Decimal>(ref cm, "@PlanningUomCodeConvert12", Decimal.Parse(TxtPlanningUomCodeConvert12.Text));
            Sm.CmParam<Decimal>(ref cm, "@PlanningUomCodeConvert21", Decimal.Parse(TxtPlanningUomCodeConvert21.Text));
            Sm.CmParam<String>(ref cm, "@Information1", Sm.GetLue(LueInformation1));
            Sm.CmParam<String>(ref cm, "@Information2", Sm.GetLue(LueInformation2));
            Sm.CmParam<String>(ref cm, "@Information3", Sm.GetLue(LueInformation3));
            Sm.CmParam<String>(ref cm, "@Information4", Sm.GetLue(LueInformation4));
            Sm.CmParam<String>(ref cm, "@Information5", Sm.GetLue(LueInformation5));
            Sm.CmParam<String>(ref cm, "@HSCode", TxtHSCode.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand DeletePackagingUnit(string ItCode)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Delete From TblItemPackagingUnit " +
                    "Where ItCode=@ItCode; "
            };
            Sm.CmParam<String>(ref cm, "@ItCode", ItCode);

            return cm;
        }

        private MySqlCommand SavePackagingUnit(string ItCode, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblItemPackagingUnit(ItCode, UomCode, Qty, Qty2, GW, NW, Length, Width, Height, Volume, CreateBy, CreateDt) " +
                    "Values(@ItCode, @UomCode, @Qty, @Qty2, @GW, @NW, @Length, @Width, @Height, @Volume, @CreateBy, CurrentDateTime()); "
            };
            Sm.CmParam<String>(ref cm, "@ItCode", ItCode);
            Sm.CmParam<String>(ref cm, "@UomCode", Sm.GetGrdStr(GrdSalesPckUnit, Row, 0));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(GrdSalesPckUnit, Row, 2));
            Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(GrdSalesPckUnit, Row, 4));
            Sm.CmParam<Decimal>(ref cm, "@GW", Sm.GetGrdDec(GrdSalesPckUnit, Row, 6));
            Sm.CmParam<Decimal>(ref cm, "@NW", Sm.GetGrdDec(GrdSalesPckUnit, Row, 8));
            Sm.CmParam<Decimal>(ref cm, "@Length", Sm.GetGrdDec(GrdSalesPckUnit, Row, 10));
            Sm.CmParam<Decimal>(ref cm, "@Width", Sm.GetGrdDec(GrdSalesPckUnit, Row, 12));
            Sm.CmParam<Decimal>(ref cm, "@Height", Sm.GetGrdDec(GrdSalesPckUnit, Row, 14));
            Sm.CmParam<Decimal>(ref cm, "@Volume", Sm.GetGrdDec(GrdSalesPckUnit, Row, 16));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand DeletePackagingUnitInventory(string ItCode)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Delete From TblItemPackagingUnit2 Where ItCode=@ItCode; "
            };
            Sm.CmParam<String>(ref cm, "@ItCode", ItCode);

            return cm;
        }

        private MySqlCommand DeleteItemProperty(string ItCode)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Delete From TblItemProperty Where ItCode=@ItCode; "
            };
            Sm.CmParam<String>(ref cm, "@ItCode", ItCode);

            return cm;
        }

        private MySqlCommand DeleteItemFile(string ItCode)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Delete From TblItemFile Where ItCode=@ItCode; "
            };
            Sm.CmParam<String>(ref cm, "@ItCode", ItCode);

            return cm;
        }

        private MySqlCommand SaveItemProperty(string ItCode, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblItemProperty(ItCode, PropCode, CreateBy, CreateDt) " +
                    "Values(@ItCode, @PropCode, @CreateBy, CurrentDateTime()); "
            };
            Sm.CmParam<String>(ref cm, "@ItCode", ItCode);
            Sm.CmParam<String>(ref cm, "@PropCode", Sm.GetGrdStr(GrdProperty, Row, 0));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveItemFile(string ItCode, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblItemFile(ItCode, DNo, FileName, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@ItCode, @DNo, @FileName, ");
            SQL.AppendLine("@UserCode, CurrentDateTime()); ");

            var cm = new MySqlCommand()
            {
                CommandText = SQL.ToString()
            };
            Sm.CmParam<String>(ref cm, "@ItCode", ItCode);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@FileName", Sm.GetGrdStr(Grd3, Row, 1));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);


            return cm;
        }

        private MySqlCommand UpdateItemFile(string ItCode, int Row, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblItemFile Set ");
            SQL.AppendLine("    FileName=@FileName ");
            SQL.AppendLine("Where ItCode=@ItCode and DNo = @DNo ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@ItCode", ItCode);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));

            return cm;
        }

        #endregion

        #region Additional Method

        internal protected void InsertDataClick()
        {
            mIsInsert = true;
            ClearData();
            SetFormControl(mState.Insert);

            ChkActInd.Checked =
            ChkPurchaseItemInd.Checked =
            ChkInventoryItemInd.Checked = true;

            SetLueControlByDeptCode(ref LueControlByDeptCode, string.Empty);
            SetLueItCtCode(ref LueItCtCode, string.Empty);
            SetLueItGrpCode(ref LueItGrpCode, string.Empty);
        }

        private void SetLueItCtCode(ref LookUpEdit Lue, string Code)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.ItCtCode As Col1, T.ItCtName As Col2 From TblItemCategory T ");
            if (Code.Length > 0)
            {
                SQL.AppendLine("Where (T.ItCtCode=@Code Or ");
                SQL.AppendLine("(T.ActInd='Y' ");
                if (Sm.GetParameter("IsFilterByItCt") == "Y")
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                    SQL.AppendLine("    Where ItCtCode=T.ItCtCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }
                SQL.AppendLine(")) ");
            }
            else
            {
                SQL.AppendLine("Where T.ActInd='Y' ");
                if (Sm.GetParameter("IsFilterByItCt") == "Y")
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                    SQL.AppendLine("    Where ItCtCode=T.ItCtCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }
            }
            if (mMenuCode == Sm.GetValue("Select MenuCode From TblMenu Where param = 'FrmMaterialRequest4' Limit 1;"))
            {
                SQL.AppendLine("And Find_In_Set(T.ItCtCode, @ItCtCodeForPOService) ");
            }
            SQL.AppendLine("Order By T.ItCtName;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            if (Code.Length > 0)
                Sm.CmParam<String>(ref cm, "@Code", Code);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@ItCtCodeForPOService", mItCtCodeForPOService);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (Code.Length > 0) Sm.SetLue(Lue, Code);
        }

        private void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'IsAbleToEditItemCategory', 'IsItemItCodeInternalMandatory', 'ItCodeSeqNo', 'ItCtCodeForPOService', 'IsItemConversionRate1Editable', ");
            SQL.AppendLine("'IsAbleToEditItemUom', 'IsFilterByItCt', 'IsItCodeUseItSeqNo', 'ItemVolumeUom', 'ItemLWHUom', ");
            SQL.AppendLine("'IsItemAllowToUploadFile', 'SIWeightUoM', 'FileSizeMaxUploadFTPClient', 'ProcFormatDocNo', 'IsItGrpCodeMandatoryInMasterItem', ");
            SQL.AppendLine("'ItemInformationTitle1', 'ItemInformationTitle2', 'ItemInformationTitle3', 'ItemInformationTitle4', 'ItemInformationTitle5', ");
            SQL.AppendLine("'SharedFolderForFTPClient', 'HostAddrForFTPClient', 'NumberOfSalesUomCode', 'NumberOfInventoryUomCode', 'IsItemInventoryNotShowCalculationData', ");
            SQL.AppendLine("'UsernameForFTPClient', 'PasswordForFTPClient', 'PortForFTPClient' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsItemInventoryNotShowCalculationData": mIsItemInventoryNotShowCalculationData = ParValue == "Y"; break;
                            case "IsItGrpCodeMandatoryInMasterItem": mIsItGrpCodeMandatoryInMasterItem = ParValue == "Y"; break;
                            case "IsItemAllowToUploadFile": mIsItemAllowToUploadFile = ParValue == "Y"; break;
                            case "IsItCodeUseItSeqNo": mIsItCodeUseItSeqNo = ParValue == "Y"; break;
                            case "IsFilterByItCt": mIsFilterByItCt = ParValue == "Y"; break;
                            case "IsItemConversionRate1Editable": mIsItemConversionRate1Editable = ParValue == "Y"; break;
                            case "IsItemItCodeInternalMandatory": mIsItemItCodeInternalMandatory = ParValue == "Y"; break;
                            case "IsAbleToEditItemCategory": mIsAbleToEditItemCategory = ParValue == "Y"; break;
                            case "IsAbleToEditItemUom": mIsAbleToEditItemUom = ParValue == "Y"; break;

                            //string
                            case "ItemVolumeUom": mItemVolumeUom = ParValue; break;
                            case "ItemLWHUom": mItemLWHUom = ParValue; break;
                            case "SIWeightUoM": mSIWeightUom = ParValue; break;
                            case "FileSizeMaxUploadFTPClient": mFileSizeMaxUploadFTPClient = ParValue; break;
                            case "ProcFormatDocNo": mProcFormatDocNo = ParValue; break;
                            case "ItemInformationTitle1": if (ParValue.Length > 0) LblInformation1.Text = ParValue; break;
                            case "ItemInformationTitle2": if (ParValue.Length > 0) LblInformation2.Text = ParValue; break;
                            case "ItemInformationTitle3": if (ParValue.Length > 0) LblInformation3.Text = ParValue; break;
                            case "ItemInformationTitle4": if (ParValue.Length > 0) LblInformation4.Text = ParValue; break;
                            case "ItemInformationTitle5": if (ParValue.Length > 0) LblInformation5.Text = ParValue; break;
                            case "ItCtCodeForPOService": mItCtCodeForPOService = ParValue; break;
                            case "ItCodeSeqNo": mItCodeSeqNo = ParValue; break;
                            case "HostAddrForFTPClient": mHostAddrForFTPClient = ParValue; break;
                            case "SharedFolderForFTPClient": mSharedFolderForFTPClient = ParValue; break;
                            case "UsernameForFTPClient": mUsernameForFTPClient = ParValue; break;
                            case "PasswordForFTPClient": mPasswordForFTPClient = ParValue; break;
                            case "PortForFTPClient": mPortForFTPClient = ParValue; break;

                            //integer
                            case "NumberOfInventoryUomCode": if (ParValue.Length>0) NumberOfInventoryUomCode = int.Parse(ParValue); break;
                            case "NumberOfSalesUomCode": if (ParValue.Length > 0) NumberOfSalesUomCode = int.Parse(ParValue); break;

                        }
                    }
                }
                dr.Close();
            }
            if (mItCodeSeqNo.Length == 0) mItCodeSeqNo = "5";
        }

        private void ComputeItemConversion()
        {
            decimal QtyConversion = 0m, Qty2 = 0m;
            for (int Row = 0; Row < GrdSalesPckUnit.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(GrdSalesPckUnit, Row, 0).Length != 0)
                {
                    QtyConversion = Decimal.Parse(TxtSalesConvertUOM2.Text);
                    for (int Row2 = 0; Row2 < GrdSalesPckUnit.Rows.Count - 1; Row2++)
                    {
                        if (Sm.GetGrdStr(GrdSalesPckUnit, Row2, 0).Length != 0)
                        {
                            Qty2 = QtyConversion * Sm.GetGrdDec(GrdSalesPckUnit, Row, 4);
                        }
                        GrdSalesPckUnit.Cells[Row, 2].Value = Sm.FormatNum(Qty2, 0);
                    }
                }
            }
        }

        private void SetItCodeInternalNotEmptyInd()
        {
            mItCodeInternalNotEmptyInd = Sm.GetParameter("ItCodeInternalNotEmptyInd");
        }

        private void ShowWhsInfo(string ItCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.WhsName, IfNull(B.Qty, 0) As QtyInStock, (-1*IfNull(C.Qty2, 0)) As QtyCommitted ");
            SQL.AppendLine("From TblWarehouse A ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select WhsCode, Sum(Qty) Qty ");
            SQL.AppendLine("    From TblStockSummary ");
            SQL.AppendLine("    Where ItCode=@ItCode And Qty<>0 ");
            SQL.AppendLine("    Group By WhsCode ");
            SQL.AppendLine(") B On A.WhsCode=B.WhsCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select A.WhsCode, Sum(B.Qty) As Qty2 ");
            SQL.AppendLine("    From TblDOWhsHdr A ");
            SQL.AppendLine("    Inner Join TblDOWhsDtl B On A.DocNo=B.DocNo And B.CancelInd='N' And B.ProcessInd='O' And B.ItCode=@ItCode ");
            SQL.AppendLine("    Where A.CancelInd='N' And A.Status='O' ");
            SQL.AppendLine("    Group By A.WhsCode ");
            SQL.AppendLine(") C On A.WhsCode=C.WhsCode ");
            //SQL.AppendLine("Left Join ( ");
            //SQL.AppendLine("    Select A.WhsCode2 As WhsCode, Sum(B.Qty) As Qty3 ");
            //SQL.AppendLine("    From TblDOWhsHdr A ");
            //SQL.AppendLine("    Inner Join TblDOWhsDtl B On A.DocNo=B.DocNo And B.CancelInd='N' And B.ProcessInd='O' And B.ItCode=@ItCode ");
            //SQL.AppendLine("    Group By A.WhsCode ");
            //SQL.AppendLine(") D On A.WhsCode=D.WhsCode ");
            SQL.AppendLine("Where IfNull(B.Qty, 0)<>0 Or (-1*IfNull(C.Qty2, 0))<>0 ");
            SQL.AppendLine("Order By A.WhsName;");


            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@ItCode", ItCode);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[]{ "WhsName", "QtyInStock", "QtyCommitted" },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Grd.Cells[Row, 0].Value = Row + 1;
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 2);
                }, false, false, false, true
            );
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ComputeInStockQty()
        {
            decimal Qty = 0m;
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                Qty += Sm.GetGrdDec(Grd1, Row, 2);
            TxtInStockQty.Text = Sm.FormatNum(Qty, 0);
        }

        private void ComputeCommittedQty()
        {
            decimal Qty = 0m;
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                Qty += Sm.GetGrdDec(Grd1, Row, 3);
            TxtCommittedQty.Text = Sm.FormatNum(Qty, 0);
        }

        private void ComputeOrderredQty(string ItCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Sum(Qty) Qty From (");
            SQL.AppendLine("    Select A.Qty-IfNull(D.Qty2, 0)-IfNull(E.Qty3, 0) As Qty  ");
            SQL.AppendLine("    From TblPODtl A ");
            SQL.AppendLine("    Inner Join TblPORequestDtl B On A.PORequestDocNo=B.DocNo And A.PORequestDNo=B.DNo ");
            SQL.AppendLine("    Inner Join TblMaterialRequestDtl C On B.MaterialRequestDocNo=C.DocNo And B.MaterialRequestDNo=C.DNo And C.ItCode=@ItCode ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select T1.PODocNo As DocNo, T1.PODNo As DNo, Sum(T1.QtyPurchase) As Qty2 ");
            SQL.AppendLine("        From TblRecvVdDtl T1 ");
            SQL.AppendLine("        Inner Join TblPODtl T2 On T1.PODocNo=T2.DocNo And T1.PODNo=T2.DNo And T2.ProcessInd<>'F' ");
            SQL.AppendLine("        Where T1.CancelInd='N' And T1.ItCode=@ItCode ");
            SQL.AppendLine("        Group By T1.PODocNo, T1.PODNo ");
            SQL.AppendLine("    ) D On A.DocNo=D.DocNo And A.DNo=D.DNo ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select T1.PODocNo As DocNo, T1.PODNo As DNo, Sum(T1.Qty) As Qty3 ");
            SQL.AppendLine("        From TblPOQtyCancel T1 ");
            SQL.AppendLine("        Inner Join TblPODtl T2 On T1.PODocNo=T2.DocNo And T1.PODNo=T2.DNo And T2.ProcessInd<>'F' ");
            SQL.AppendLine("        Inner Join TblPORequestDtl T3 On T2.PORequestDocNo=T3.DocNo And T2.PORequestDNo=T3.DNo ");
            SQL.AppendLine("        Inner Join TblMaterialRequestDtl T4 On T3.MaterialRequestDocNo=T4.DocNo And T3.MaterialRequestDNo=T4.DNo And T4.ItCode=@ItCode ");
            SQL.AppendLine("        And T1.CancelInd='N' ");
            SQL.AppendLine("        Group By T1.PODocNo, T1.PODNo ");
            SQL.AppendLine("    ) E On A.DocNo=E.DocNo And A.DNo=E.DNo ");
            SQL.AppendLine("    Where A.ProcessInd<>'F' And A.CancelInd='N' ");
            SQL.AppendLine(") T; ");

            var cm = new MySqlCommand(){ CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@ItCode", ItCode);
            string Qty = Sm.GetValue(cm);
            if (Qty.Length != 0)
                TxtOrderredQty.EditValue = Sm.FormatNum(decimal.Parse(Qty), 0);
            else
                TxtOrderredQty.EditValue = Sm.FormatNum(0, 0);
        }

        private void ComputeRequestedQty(string ItCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Sum(Qty) Qty From (");
            SQL.AppendLine("    Select A.Qty-IfNull(B.Qty, 0)-ifnull(C.Qty, 0) As Qty  ");
            SQL.AppendLine("    From TblMaterialRequestDtl A ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select T2.MaterialRequestDocNo As DocNo, T2.MaterialRequestDNo As DNo, Sum(T1.Qty) As Qty ");
            SQL.AppendLine("        From TblPODtl T1 ");
            SQL.AppendLine("        Inner Join TblPORequestDtl T2 On T1.PORequestDocNo=T2.DocNo And T1.PORequestDNo=T2.DNo ");
            SQL.AppendLine("        Inner Join TblMaterialRequestDtl T3 On T2.MaterialRequestDocNo=T3.DocNo And T2.MaterialRequestDNo=T3.DNo And T3.ProcessInd<>'F' And T3.ItCode=@ItCode ");
            SQL.AppendLine("        Where T1.CancelInd='N' ");
            SQL.AppendLine("        Group By T2.MaterialRequestDocNo, T2.MaterialRequestDNo ");
            SQL.AppendLine("    ) B On A.DocNo=B.DocNo And A.DNo=B.DNo ");
            SQL.AppendLine("    Left Join TblMRQtyCancel C On A.DocNo = C.MRDocNo And A.DNo = C.MRDNo And C.CancelInd = 'N' ");
            SQL.AppendLine("    Where A.ProcessInd<>'F' And A.CancelInd='N' And A.Status<>'C' And A.ItCode=@ItCode ");
            SQL.AppendLine(") T; ");

            var cm = new MySqlCommand()
            {
                CommandText = SQL.ToString()
            };
            Sm.CmParam<String>(ref cm, "@ItCode", ItCode);
            string Qty = Sm.GetValue(cm);
            if (Qty.Length != 0)
                TxtRequestedQty.EditValue = Sm.FormatNum(decimal.Parse(Qty), 0);
            else
                TxtRequestedQty.EditValue = Sm.FormatNum(0, 0);
        }

        private void ShowDataInCtrl(ref MySqlCommand cm, string SQL, string[] ColumnTitle, RefreshData rd, bool ShowNoDataInd)
        {
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL;
                cm.CommandTimeout = 600;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, ColumnTitle);
                if (!dr.HasRows)
                {
                    if (ShowNoDataInd)
                    {
                        if (mItCode.Length==0)
                            Sm.StdMsg(mMsgType.NoData, string.Empty);
                        else
                            Sm.StdMsg(mMsgType.Warning, "No authorized to see this information.");
                    }
                }
                else
                {
                    while (dr.Read()) rd(dr, c);
                }
                dr.Close();
                dr.Dispose();
                cm.Dispose();
            }
        }

        #region Setlue

        //private void SetLueItCtCode(ref LookUpEdit Lue, string ItCode)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Select Col1, Col2 From ( ");
        //    SQL.AppendLine("Select ItCtCode As Col1, ItCtName As Col2 From TblItemCategory Where ActInd = 'Y'  ");
        //    if (TxtItCode.Text.Length != 0)
        //    {
        //        SQL.AppendLine("Union All ");
        //        SQL.AppendLine("Select A.ItCtCode As Col1, A.ItCtName As Col2 From TblItemCategory A ");
        //        SQL.AppendLine("Inner Join TblItem B On A.ItCtCode = B.ItCtCode ");
        //        SQL.AppendLine("Where B.ItCode = '" + ItCode + "' ");
        //    }
        //    SQL.AppendLine(")Tbl Order By Col2");

        //    Sm.SetLue2(
        //        ref Lue,
        //        SQL.ToString(),
        //        0, 35, false, true, "Code", "Name", "Col2", "Col1");
        //}

        private void SetLueItGrpCode(ref LookUpEdit Lue, string ItGrpCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Col1, Col2 From ( ");
            SQL.AppendLine("Select ItGrpCode As Col1, Concat(ItGrpName, ' (', ItGrpCode, ')') As Col2 From TblItemGroup Where ActInd = 'Y'  ");
            if (ItGrpCode.Length != 0)
            {
                SQL.AppendLine("Union All ");
                SQL.AppendLine("Select ItGrpCode As Col1, Concat(ItGrpName, ' (', ItGrpCode, ')') As Col2 From TblItemGroup Where ItGrpCode = '" + ItGrpCode + "' ");
            }
            SQL.AppendLine(") Tbl Order By Col2");

            Sm.SetLue2(
                ref Lue,
                SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void SetLueItScCode(ref LookUpEdit Lue, string ItCtCode)
        {
            var SQL = new StringBuilder();

            if (TxtItCode.Text.Length != 0)
            {
                SQL.AppendLine("Select Col1, Col2 From (");
                SQL.AppendLine("Select ItScCode As Col1, ItScName As Col2 ");
                SQL.AppendLine("From TblItemSubCategory ");
                SQL.AppendLine("Where ActInd = 'Y' And ItCtCode='" + ItCtCode + "'  ");
                SQL.AppendLine("Union All");
                SQL.AppendLine("Select B.ItScCode As Col1, A.ItScName As Col2 ");
                SQL.AppendLine("From TblItemSubCategory A ");
                SQL.AppendLine("Inner Join TblItem B On A.ItScCode = B.ItScCode ");
                SQL.AppendLine("Where B.ItCode='" + TxtItCode.Text + "'  ");
                SQL.AppendLine(")Tbl Order By Col2 ");
            }
            else
            {
                SQL.AppendLine("Select ItScCode As Col1, ItScName As Col2 ");
                SQL.AppendLine("From TblItemSubCategory ");
                SQL.AppendLine("Where ActInd = 'Y' And ItCtCode='" + ItCtCode + "' Order By ItScName ");
            }

            Sm.SetLue2(
                ref Lue,
                SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void SetLueControlByDeptCode(ref DXE.LookUpEdit Lue, string DeptCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct DeptCode As Col1, DeptName As Col2 From (");
            SQL.AppendLine("    Select DeptCode, DeptName ");
            SQL.AppendLine("    From TblDepartment Where IfNull(ControlItemInd, '')='Y' ");
            if (DeptCode.Length != 0)
            {
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select DeptCode, DeptName From TblDepartment Where DeptCode='" + DeptCode + "' ");
            }
            SQL.AppendLine(") T Order By DeptName; ");

            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #endregion

        #region FTP

        private void DownloadFile(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);
                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                //MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                Sm.StdMsg(mMsgType.Warning, "There was an error connecting to the FTP Server.");
            }
        }

        private void UploadFile(string ItCode, int Row, string FileName)
        {
            if (IsUploadFileNotValid(Row, FileName)) return;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", FileName));
            long mFileSize = toUpload.Length;
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", FileName));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);


                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);

                }
            }
            while (bytesRead != 0);

            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdateItemFile(ItCode, Row, toUpload.Name));
            Sm.ExecCommands(cml);
        }

        private bool IsUploadFileNotValid(int Row, string FileName)
        {
            return
                IsFTPClientDataNotValid(Row, FileName) ||
                IsFileSizeNotvalid(Row, FileName) ||
                IsFileNameAlreadyExisted(Row, FileName)
             ;
        }

        private bool IsFTPClientDataNotValid(int Row, string FileName)
        {

            if (mIsItemAllowToUploadFile && FileName.Length > 0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }

            if (mIsItemAllowToUploadFile && FileName.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }

            if (mIsItemAllowToUploadFile && FileName.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }

            if (mIsItemAllowToUploadFile && FileName.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }
            return false;
        }

        private bool IsFileSizeNotvalid(int Row, string FileName)
        {
            if (mIsItemAllowToUploadFile && FileName.Length > 0)
            {
                FileInfo f = new FileInfo(FileName);
                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }

                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File too large to Upload in row " + (Row + 1));
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted(int Row, string FileName)
        {
            if (mIsItemAllowToUploadFile && FileName.Length > 0 && Sm.GetGrdStr(Grd3, Row, 1) != Sm.GetGrdStr(Grd3, Row, 4))
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", FileName));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select FileName From TblEmployeeFile ");
                SQL.AppendLine("Where FileName=@FileName ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtItCode);
        }

        private void TxtItName_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtItName);
        }

        private void TxtItCodeOld_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtItCodeOld);
        }

        private void TxtForeignName_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtForeignName);
        }

        private void TxtItCodeInternal_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtItCodeInternal);
        }

        private void LueItCtCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueItCtCode, new Sm.RefreshLue2(SetLueItCtCode), string.Empty);
                var ItCtCode = Sm.GetLue(LueItCtCode);
                LueItScCode.EditValue = null;
                if (ItCtCode.Length != 0) SetLueItScCode(ref LueItScCode, ItCtCode);
                Sm.SetControlReadOnly(LueItScCode, ItCtCode.Length == 0);
            }
        }

        private void LueItScCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueItScCode, new Sm.RefreshLue2(SetLueItScCode), Sm.GetLue(LueItCtCode));
        }

        private void LueItBrCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueItBrCode, new Sm.RefreshLue1(Sl.SetLueItBrCode));
        }

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue1(Sl.SetLueVdCode));
        }

        private void LuePurchaseUomCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LuePurchaseUomCode, new Sm.RefreshLue1(Sl.SetLueUomCode));
                if (Sm.GetLue(LuePurchaseUomCode).Length != 0)
                {
                    Sl.SetLueUomCode(new List<DevExpress.XtraEditors.LookUpEdit> 
                    { 
                        LueSalesUomCode,
                        LueSalesUomCode2,
                        LueInventoryUomCode, 
                        LueInventoryUomCode2, 
                        LueInventoryUomCode3, 
                        LuePlanningUomCode,
                        LuePlanningUomCode2
                    });

                    LueSalesUomCode.EditValue = LueSalesUomCode2.EditValue =
                    LueInventoryUomCode.EditValue = LueInventoryUomCode2.EditValue = LueInventoryUomCode3.EditValue =
                    LuePlanningUomCode.EditValue = LuePlanningUomCode2.EditValue =
                    LuePurchaseUomCode.EditValue;

                    //var Value = Sm.FormatNum(1, 0);
                    //TxtInvConvertUOM12.EditValue = Value;
                    //TxtInvConvertUOM13.EditValue = Value;
                    //TxtInvConvertUOM21.EditValue = Value;
                    //TxtInvConvertUOM23.EditValue = Value;
                    //TxtInvConvertUOM31.EditValue = Value;
                    //TxtInvConvertUOM32.EditValue = Value;
                    //TxtSalesConvertUOM2.EditValue = Value;
                    //TxtSalesConvertUOM1.EditValue = Value;
                    //TxtPlanningUomCodeConvert12.EditValue = Value;
                    //TxtPlanningUomCodeConvert21.EditValue = Value;
                }
            }
        }

        private void TxtMinOrder_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtMinOrder, 0);
        }

        private void LueTaxCode1_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueTaxCode1, new Sm.RefreshLue1(Sl.SetLueTaxCode));
        }

        private void LueTaxCode2_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueTaxCode2, new Sm.RefreshLue1(Sl.SetLueTaxCode));
        }

        private void LueTaxCode3_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueTaxCode3, new Sm.RefreshLue1(Sl.SetLueTaxCode));
        }

        private void TxtLength_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtLength, 0);
        }

        private void LueLengthUomCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueLengthUomCode, new Sm.RefreshLue1(Sl.SetLueUomCode));
        }

        private void TxtHeight_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtHeight, 0);
        }

        private void LueHeightUomCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueHeightUomCode, new Sm.RefreshLue1(Sl.SetLueUomCode));
        }

        private void TxtWidth_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtWidth, 0);
        }

        private void LueWidthUomCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueWidthUomCode, new Sm.RefreshLue1(Sl.SetLueUomCode));
        }

        private void TxtWeight_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtWeight, 0);
        }

        private void LueWeightUomCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueWeightUomCode, new Sm.RefreshLue1(Sl.SetLueUomCode));
        }

        private void TxtVolume_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtVolume, 0);
        }

        private void TxtDiameter_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtDiameter, 0);
        }

        private void LueVolumeUomCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueVolumeUomCode, new Sm.RefreshLue1(Sl.SetLueUomCode));
        }

        private void LueDiameterUomCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueDiameterUomCode, new Sm.RefreshLue1(Sl.SetLueUomCode));
        }

        private void LueSalesUomCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueSalesUomCode, new Sm.RefreshLue1(Sl.SetLueUomCode));
                LblSalesUOM1.Text = LueSalesUomCode.Text;
                SetNumberOfUOMDefault();
                for (int intX = 0; intX < GrdSalesPckUnit.Rows.Count; intX++)
                    GrdSalesPckUnit.Cells[intX, 3].Value = LueSalesUomCode.Text;
            }
        }

        private void LueSalesUomCode2_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueSalesUomCode2, new Sm.RefreshLue1(Sl.SetLueUomCode));
                LblSalesUOM2.Text = LueSalesUomCode2.Text;
                for (int intX = 0; intX < GrdSalesPckUnit.Rows.Count; intX++)
                    GrdSalesPckUnit.Cells[intX, 5].Value = LueSalesUomCode2.Text;
            }
        }

        private void LueInventoryUomCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueInventoryUomCode, new Sm.RefreshLue1(Sl.SetLueUomCode));
                if (Sm.GetLue(LueInventoryUomCode).Length != 0)
                {
                    Sl.SetLueUomCode(new List<DevExpress.XtraEditors.LookUpEdit> 
                    { 
                        LueSalesUomCode,
                        LuePlanningUomCode
                    });

                    LueSalesUomCode.EditValue =
                    LuePlanningUomCode.EditValue =
                    LueInventoryUomCode.EditValue;
                }
            }
        }

        private void LueInventoryUomCode2_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueInventoryUomCode2, new Sm.RefreshLue1(Sl.SetLueUomCode));
                if (Sm.GetLue(LueInventoryUomCode2).Length != 0)
                {
                    Sl.SetLueUomCode(new List<DevExpress.XtraEditors.LookUpEdit> 
                    { 
                        LueInventoryUomCode3,
                        LueSalesUomCode2,
                        LuePlanningUomCode2
                    });

                    LueInventoryUomCode3.EditValue =
                    LueSalesUomCode2.EditValue =
                    LuePlanningUomCode2.EditValue =
                    LueInventoryUomCode2.EditValue;
                }
            }
        }

        private void LueInventoryUomCode3_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueInventoryUomCode3, new Sm.RefreshLue1(Sl.SetLueUomCode));
        }

        private void TxtMinStock_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtMinStock, 0);
        }

        private void TxtMaxStock_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtMaxStock, 0);
        }

        private void TxtReOrderStock_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtReOrderStock, 0);
        }

        private void LueControlByDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueControlByDeptCode, new Sm.RefreshLue2(SetLueControlByDeptCode), "");
                ChkControlByDeptCode.Checked = (Sm.GetLue(LueControlByDeptCode).Length != 0);
            }
        }

        private void LuePGCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LuePGCode, new Sm.RefreshLue1(Sl.SetLuePGCode));
        }

        private void LuePlanningUomCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LuePlanningUomCode, new Sm.RefreshLue1(Sl.SetLueUomCode));
        }

        private void LuePlanningUomCode2_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LuePlanningUomCode2, new Sm.RefreshLue1(Sl.SetLueUomCode));
        }
       
        private void LueUomCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueUomCode, new Sm.RefreshLue1(Sl.SetLueUomCode));
        }

        private void LueUomCode_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(GrdSalesPckUnit, ref fAccept, e);
        }

        private void LueUomCode_Leave(object sender, EventArgs e)
        {
            if (LueUomCode.Visible && fAccept && fCell.ColIndex == 1)
            {
                if (Sm.GetLue(LueUomCode).Length == 0)
                    GrdSalesPckUnit.Cells[fCell.RowIndex, 0].Value =
                    GrdSalesPckUnit.Cells[fCell.RowIndex, 1].Value = null;
                else
                {
                    GrdSalesPckUnit.Cells[fCell.RowIndex, 0].Value = Sm.GetLue(LueUomCode);
                    GrdSalesPckUnit.Cells[fCell.RowIndex, 1].Value = LueUomCode.GetColumnValue("Col2");
                }
            }
        }

        private void LueUomCode_Validated(object sender, EventArgs e)
        {
            LueUomCode.Visible = false;
        }


        private void TxtInvConvertUOM12_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtInvConvertUOM12, 0);
        }

        private void TxtInvConvertUOM13_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtInvConvertUOM13, 0);
        }

        private void TxtInvConvertUOM21_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtInvConvertUOM21, 0);
        }

        private void TxtInvConvertUOM23_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtInvConvertUOM23, 0);
        }

        private void TxtInvConvertUOM31_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtInvConvertUOM31, 0);
        }

        private void TxtInvConvertUOM32_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtInvConvertUOM32, 0);
        }

        private void TxtPlanningUomCodeConvert12_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtPlanningUomCodeConvert12, 0);
        }

        private void TxtPlanningUomCodeConvert21_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtPlanningUomCodeConvert21, 0);
        }

        private void LueItGrpCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueItGrpCode, new Sm.RefreshLue2(SetLueItGrpCode), string.Empty);
        }

        private void LueInformation1_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueInformation1, new Sm.RefreshLue2(Sl.SetLueOption), "ItemInformation1");
        }

        private void LueInformation2_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueInformation2, new Sm.RefreshLue2(Sl.SetLueOption), "ItemInformation2");
        }

        private void LueInformation3_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueInformation3, new Sm.RefreshLue2(Sl.SetLueOption), "ItemInformation3");
        }

        private void LueInformation4_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueInformation4, new Sm.RefreshLue2(Sl.SetLueOption), "ItemInformation4");
        }

        private void LueInformation5_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueInformation5, new Sm.RefreshLue2(Sl.SetLueOption), "ItemInformation5");
        }

        #endregion

        #region Grd Event

        private void LueProperty_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueProperty, new Sm.RefreshLue1(Sl.SetLueItPropCode));
        }

        private void LueProperty_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(GrdProperty, ref fAccept, e);
        }

        private void LueProperty_Leave(object sender, EventArgs e)
        {
            if (LueProperty.Visible && fAccept && fCell.ColIndex == 1)
            {
                if (Sm.GetLue(LueProperty).Length == 0)
                    GrdProperty.Cells[fCell.RowIndex, 0].Value =
                    GrdProperty.Cells[fCell.RowIndex, 1].Value = null;
                else
                {
                    GrdProperty.Cells[fCell.RowIndex, 0].Value = Sm.GetLue(LueProperty);
                    GrdProperty.Cells[fCell.RowIndex, 1].Value = LueProperty.GetColumnValue("Col2");
                }
                LueProperty.Visible = false;
            }
        }


        private void GrdProperty_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditTrimString(GrdProperty, new int[] { 0 }, e);
        }

        private void GrdProperty_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(GrdProperty, e, BtnSave);
            Sm.GrdKeyDown(GrdProperty, e, BtnFind, BtnSave);
        }

        private void GrdProperty_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 0 }, e.ColIndex) && BtnSave.Enabled)
                Sm.GrdRequestEdit(GrdProperty, e.RowIndex);

            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 1 }, e.ColIndex))
            {
                LueRequestEdit(GrdProperty, LueProperty, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(GrdProperty, e.RowIndex);
                Sl.SetLueItPropCode(ref LueProperty);
            }
        }

        private void GrdInv2_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditTrimString(Grd2, new int[] { 0 }, e);
        }

        private void GrdSalesPckUnit_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 1)
                {
                    LueRequestEdit(GrdSalesPckUnit, LueUomCode, ref fCell, ref fAccept, e);

                    Sm.GrdRequestEdit(GrdSalesPckUnit, e.RowIndex);
                    GrdSalesPckUnit.Cells[e.RowIndex, 2].Value = 1m;
                    GrdSalesPckUnit.Cells[e.RowIndex, 3].Value = LueSalesUomCode.Text;
                    GrdSalesPckUnit.Cells[e.RowIndex, 4].Value = 1m;
                    GrdSalesPckUnit.Cells[e.RowIndex, 5].Value = LueSalesUomCode2.Text;
                    GrdSalesPckUnit.Cells[e.RowIndex, 6].Value = 0m;
                    GrdSalesPckUnit.Cells[e.RowIndex, 7].Value = mSIWeightUom;
                    GrdSalesPckUnit.Cells[e.RowIndex, 8].Value = 0m;
                    GrdSalesPckUnit.Cells[e.RowIndex, 9].Value = mSIWeightUom;
                    GrdSalesPckUnit.Cells[e.RowIndex, 10].Value = 0m;
                    GrdSalesPckUnit.Cells[e.RowIndex, 11].Value = mItemLWHUom;
                    GrdSalesPckUnit.Cells[e.RowIndex, 12].Value = 0m;
                    GrdSalesPckUnit.Cells[e.RowIndex, 13].Value = mItemLWHUom;
                    GrdSalesPckUnit.Cells[e.RowIndex, 14].Value = 0m;
                    GrdSalesPckUnit.Cells[e.RowIndex, 15].Value = mItemLWHUom;
                    GrdSalesPckUnit.Cells[e.RowIndex, 16].Value = 0m;
                    GrdSalesPckUnit.Cells[e.RowIndex, 17].Value = mItemVolumeUom;
                }


                //GrdSalesPckUnit.Cells[GrdSalesPckUnit.Rows.Count - 1, 2].Value = 1m;
                //GrdSalesPckUnit.Cells[GrdSalesPckUnit.Rows.Count - 1, 3].Value = LueSalesUomCode.Text;
                //GrdSalesPckUnit.Cells[GrdSalesPckUnit.Rows.Count - 1, 4].Value = 1m;
                //GrdSalesPckUnit.Cells[GrdSalesPckUnit.Rows.Count - 1, 5].Value = LueSalesUomCode2.Text;
                //GrdSalesPckUnit.Cells[GrdSalesPckUnit.Rows.Count - 1, 6].Value = 0m;
                //GrdSalesPckUnit.Cells[GrdSalesPckUnit.Rows.Count - 1, 7].Value = mSIWeightUom;
                //GrdSalesPckUnit.Cells[GrdSalesPckUnit.Rows.Count - 1, 8].Value = 0m;
                //GrdSalesPckUnit.Cells[GrdSalesPckUnit.Rows.Count - 1, 9].Value = mSIWeightUom;
            }
        }


        private void GrdSalesPckUnit_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(GrdSalesPckUnit, e, BtnSave);
            Sm.GrdKeyDown(GrdSalesPckUnit, e, BtnFind, BtnSave);
        }

        private void GrdSalesPckUnit_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(GrdSalesPckUnit, new int[] { 2, 4, 6, 8, 10, 12, 14, 16 }, e);

            if ((e.ColIndex == 2) && (Sm.GetGrdDec(GrdSalesPckUnit, e.RowIndex, 2) <= 0) && (Sm.GetGrdStr(GrdSalesPckUnit, e.RowIndex, 0).Length > 0))
            {
                Sm.StdMsg(mMsgType.Warning, "You need more than 0.");
                GrdSalesPckUnit.Cells[e.RowIndex, 2].Value = 1m;
            }

            if ((e.ColIndex == 4)&&(!mIsItemConversionRate1Editable))
                ComputeItemConversion();
            
            if (Sm.IsGrdColSelected(new int[] { 10, 12, 14 }, e.ColIndex))
            {
                decimal mLength = 0m, mWidth = 0m, mHeight = 0m, mVolume = 0m;
                if (Sm.GetGrdStr(GrdSalesPckUnit, e.RowIndex, 0).Length > 0)
                {
                    mLength = Sm.GetGrdDec(GrdSalesPckUnit, e.RowIndex, 10);
                    mWidth = Sm.GetGrdDec(GrdSalesPckUnit, e.RowIndex, 12);
                    mHeight = Sm.GetGrdDec(GrdSalesPckUnit, e.RowIndex, 14);
                    mVolume = mLength * mWidth * mHeight;
                }
                GrdSalesPckUnit.Cells[e.RowIndex, 16].Value = mVolume;

            }
        }

        private void Grd3_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3)
            {
                if (Sm.GetGrdStr(Grd3, e.RowIndex, 1).Length > 0)
                {
                    DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, Sm.GetGrdStr(Grd3, e.RowIndex, 1), mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
                    SFD.FileName = Sm.GetGrdStr(Grd3, e.RowIndex, 1);
                    SFD.DefaultExt = "pdf";
                    SFD.AddExtension = true;

                    if (!Sm.IsGrdValueEmpty(Grd3, e.RowIndex, 1, false, "File name is empty") && downloadedData != null && downloadedData.Length != 0)
                    {
                        if (SFD.ShowDialog() == DialogResult.OK)
                        {
                            Application.DoEvents();

                            //Write the bytes to a file
                            FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                            newFile.Write(downloadedData, 0, downloadedData.Length);
                            newFile.Close();
                            Sm.StdMsg(mMsgType.Info, "File Downloaded");
                        }
                    }
                    else
                        Sm.StdMsg(mMsgType.Warning, "No File Downloaded");
                }
            }

            if (BtnSave.Enabled)
            {

                if (e.ColIndex == 2)
                {
                    Sm.GrdRequestEdit(Grd3, e.RowIndex);
                    OD.InitialDirectory = "c:";
                    OD.Filter = "Image files (*.PNG;*.JPG;*.JPEG)|*.PNG;*.JPG;*.JPEG" +
                                "|PDF files (*.pdf)|*.pdf" +
                                "|Word files (*.doc;*docx)|*.doc;*docx" +
                                "|Excel files (*.xls;*xlsx;*.csv)|*.xls;*xlsx;*.csv" +
                                "|Text files (*.txt)|*.txt";
                    OD.FilterIndex = 2;
                    OD.ShowDialog();
                    Grd3.Cells[e.RowIndex, 1].Value = OD.FileName;
                }
            }
        }

        private void Grd3_RequestEdit(object sender, iGRequestEditEventArgs e)
        {

            if (Sm.IsGrdColSelected(new int[] { 0, 1, 2, 3 }, e.ColIndex) && BtnSave.Enabled)
            {
                Sm.GrdRequestEdit(Grd3, e.RowIndex);
            }

            if (e.ColIndex == 3)
            {
                Sm.GrdRequestEdit(Grd3, e.RowIndex);
                if (Sm.GetGrdStr(Grd3, e.RowIndex, 1).Length > 0)
                {
                    DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, Sm.GetGrdStr(Grd3, e.RowIndex, 1), mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
                    SFD.FileName = Sm.GetGrdStr(Grd3, e.RowIndex, 1);
                    SFD.DefaultExt = "pdf";
                    SFD.AddExtension = true;

                    if (!Sm.IsGrdValueEmpty(Grd3, e.RowIndex, 1, false, "File name is empty") && downloadedData != null && downloadedData.Length != 0)
                    {
                        if (SFD.ShowDialog() == DialogResult.OK)
                        {
                            Application.DoEvents();

                            //Write the bytes to a file
                            FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                            newFile.Write(downloadedData, 0, downloadedData.Length);
                            newFile.Close();
                            Sm.StdMsg(mMsgType.Info, "File Downloaded");
                        }
                    }
                    else
                        Sm.StdMsg(mMsgType.Warning, "No File Downloaded");
                }
            }
        }

        private void Grd3_RequestCellToolTipText(object sender, iGRequestCellToolTipTextEventArgs e)
        {
            if (e.ColIndex == 2)
            {
                e.Text = "Upload";
            }
            else if (e.ColIndex == 3)
            {
                e.Text = "Download";
                e.GetType();
            }
        }

        private void Grd3_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd3, e, BtnSave);
            Sm.GrdEnter(Grd3, e);
            Sm.GrdTabInLastCell(Grd3, e, BtnFind, BtnSave);
        }

        #endregion

        #region Button Event

        private void BtnItem_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmItemDlg(this));
        }

        private void BtnShowFile_Click(object sender, EventArgs e)
        {
            if (TxtItCode.Text != null && TxtItCode.Text.Length != 0)
            {
                try
                {
                    string location = Sm.GetValue("Select ParValue From TblParameter Where ParCode = 'FileItem'");
                    string ItCode = TxtItCode.Text;
                    string[] fileArray = Directory.GetFiles(location);
                    foreach (string name in fileArray)
                    {
                        string ext = Path.GetExtension(name);
                        string file = Path.GetFileName(name);
                        file = file.Substring(0, file.Length - ext.Length);
                        if (file == ItCode)
                        {
                            System.Diagnostics.Process.Start(name);
                        }
                    }
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
            }
            else
            {
                Sm.StdMsg(mMsgType.Warning, "Item Code Is Empty");
            }
        }

        private void BtnShowImage_Click(object sender, EventArgs e)
        {
            if (TxtItCode.EditValue != null && TxtItCode.Text.Length != 0)
            {
                try
                {
                    string location = Sm.GetParameter("ImgFileItem");
                    string[] fileArray = Directory.GetFiles(location);
                    string[] fileExtensionArray = new string[11] { ".jpg", ".png", ".bmp", ".tif", ".jpeg", ".gif", ".dib", ".rle", ".jpe", ".jfif", ".tiff" };
                    foreach (string name in fileArray)
                    {
                        foreach (string ext in fileExtensionArray)
                        {
                            if (name == String.Concat(location, "\\", TxtItCode.Text, ext))
                            {
                                PicItem.Image = Image.FromFile(String.Concat(location, "\\", TxtItCode.Text, ext));
                                break;
                            }
                        }
                    }

                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
            }
            else
            {
                Sm.StdMsg(mMsgType.Warning, "Item Code Is Empty");
            }
        }

        

        private void BtnPicture_Click(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtItCode, "Item code", false)) return;

            try
            {
                string DBServer = Sm.GetParameter("DBServer");
                string location = Sm.GetParameter("ImgFileItem");

                if (DBServer.Length != 0 && location.Length != 0 && Sm.CompareStr(DBServer, Gv.Server))
                {
                    if (Directory.Exists(location))
                    {
                        string ImageFile = String.Concat(location, "\\", TxtItCode.Text);
                        foreach (string fe in new string[11] 
                                { ".jpg", ".png", ".bmp", ".jpeg", ".gif", ".dib", 
                                  ".rle", ".jpe", ".jfif", ".tiff", ".tif" })
                        {
                            if (File.Exists(String.Concat(ImageFile, fe)))
                            {
                                PicItem.Image = Image.FromFile(String.Concat(ImageFile, fe));
                                mItemPicture = String.Concat(location, TxtItCode.Text, fe);
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnDownload_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "Images|*.png;*.bmp;*.jpg";
            ImageFormat format = ImageFormat.Png;
            sfd.FileName = TxtItCode.Text;
            if (PicItem.Image != null && sfd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string ext = System.IO.Path.GetExtension(sfd.FileName);
                switch (ext)
                {
                    case ".jpg":
                        format = ImageFormat.Jpeg;
                        break;
                    case ".bmp":
                        format = ImageFormat.Bmp;
                        break;
                }
                PicItem.Image.Save(sfd.FileName, format);
            }
        }

        #endregion

        #endregion
    }
}
