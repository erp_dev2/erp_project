﻿#region Update
/*
    07/08/2018 [TKG] New Feature
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmWorkCenterBOMFormulationDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmWorkCenterBOMFormulation mFrmParent;
        private string mSQL = string.Empty, mDocDt = string.Empty;

        #endregion

        #region Constructor

        public FrmWorkCenterBOMFormulationDlg(FrmWorkCenterBOMFormulation FrmParent, string DocDt)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mDocDt = DocDt;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 6;
            Grd1.FrozenArea.ColCount = 1;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",
                        
                        //1-5
                        "",
                        "Work Center Code",
                        "Work Center Name",
                        "Formula Code",
                        "Bill of Material"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        20, 130, 200, 130, 200
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 3, 4, 5 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 4 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocName, B.BOMFormulationCode, C.DocName As BOMName ");
            SQL.AppendLine("From TblWorkCenterHdr A ");
            SQL.AppendLine("Left Join (  ");
            SQL.AppendLine("    Select T1.WorkCenterCode As DocNo, T1.BOMFormulationCode  ");
            SQL.AppendLine("    From TblWorkcenterBOMFormulation T1  ");
            SQL.AppendLine("    Inner Join TblWorkcenterHdr T2  ");
            SQL.AppendLine("        On T1.WorkCenterCode=T2.DocNo  ");
            SQL.AppendLine("        And T2.DocDt<=@DocDt  ");
            SQL.AppendLine("        And T2.ActiveInd = 'Y' ");
            SQL.AppendLine("    Inner Join (  ");
            SQL.AppendLine("        Select T3a.WorkCenterCode, Max(T3a.Date) As Date  ");
            SQL.AppendLine("        From TblWorkcenterBOMFormulation T3a  ");
            SQL.AppendLine("        Inner Join TblWorkcenterHdr T3b  ");
            SQL.AppendLine("            On T3a.WorkCenterCode=T3b.DocNo  ");
            SQL.AppendLine("            And T3b.ActiveInd = 'Y'  ");
            SQL.AppendLine("        Where T3a.Date<=@DocDt  ");
            SQL.AppendLine("        Group By T3a.WorkCenterCode  ");
            SQL.AppendLine("    ) T3  ");
            SQL.AppendLine("        On T1.WorkCenterCode=T3.WorkCenterCode  ");
            SQL.AppendLine("        And T1.Date=T3.Date  ");
            SQL.AppendLine(") B On A.DocNo=B.DocNo  ");
            SQL.AppendLine("Left Join TblBOMHdr C On B.BOMFormulationCode=C.DocNo ");
            SQL.AppendLine("Where  A.ActiveInd = 'Y' ");

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = string.Empty, DocNo= string.Empty;                
                var cm = new MySqlCommand();

                if (mFrmParent.Grd1.Rows.Count >= 1)
                {
                    for (int r = 0; r<mFrmParent.Grd1.Rows.Count; r++)
                    {
                        DocNo = Sm.GetGrdStr(mFrmParent.Grd1, r, 0);
                        if (DocNo.Length != 0)
                        {
                            if (Filter.Length > 0) Filter += " And ";
                            Filter += " (A.DocNo<>@DocNo0" + r.ToString() + ") ";
                            Sm.CmParam<String>(ref cm, "@DocNo0" + r.ToString(), DocNo);
                        }
                    }
                }

                if (Filter.Length > 0)
                    Filter = " And (" + Filter + ") ";
                else
                    Filter = " ";

                Sm.CmParamDt(ref cm, "@DocDt", mDocDt);
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, new string[] { "A.DocNo", "A.DocName" });
                
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.DocName, C.DocName;",
                        new string[] 
                        { 
                            "DocNo", 

                            "DocName", 
                            "BOMFormulationCode", 
                            "BOMName" 
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = false;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsDataAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 0, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 2, Grd1, Row2, 3);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 3, Grd1, Row2, 4);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 5, Grd1, Row2, 5);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 6, Grd1, Row2, 4);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 8, Grd1, Row2, 5);

                        mFrmParent.Grd1.Rows.Add();
                    }
                }
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 work center.");
        }

        private bool IsDataAlreadyChosen(int r)
        {
            string DocNo = Sm.GetGrdStr(Grd1, r, 2);
            for (int i = 0; i < mFrmParent.Grd1.Rows.Count - 1; i++)
                if (Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd1, i, 0), DocNo)) return true;
            return false;
        }


        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    Grd1.Cells[Row, 1].Value = !IsSelected;
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Work center");
        }

        #endregion

        #endregion

    }
}
