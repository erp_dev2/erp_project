﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmTenderQuotationRequestDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmTenderQuotationRequest mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmTenderQuotationRequestDlg(FrmTenderQuotationRequest FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                this.Text = "List of Active Tender";
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -30);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 16;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No",

                    //1-5
                    "Document#",
                    "",
                    "Date",
                    "Tender Name",
                    "MR#",
                    
                    //6-10
                    "",
                    "SiteCode",
                    "Site",
                    "Item's Code", 
                    "Item's Name",

                    //11-15
                    "ItScCode",
                    "Quantity",
                    "Currency",
                    "Estimated"+Environment.NewLine+"Price",
                    "MRDNo"
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    160, 20, 80, 200, 150, 
                    
                    //6-10
                    20, 0, 180, 100, 250, 
                    
                    //11-15
                    0, 100, 80, 120, 0
                }
            );
            Sm.GrdColButton(Grd1, new int[] { 2, 6 });
            Sm.GrdColReadOnly(Grd1, new int[] { 1, 3, 4, 5, 7, 8, 9, 10, 11, 12, 13, 14, 15 });
            Sm.GrdFormatDec(Grd1, new int[] { 12, 14 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 3 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 6, 7, 9, 11, 15 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }


        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 6, 9 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.TenderName, A.MaterialRequestDocNo, B.SiteCode, E.SiteName, ");
            SQL.AppendLine("C.ItCode, D.ItName, D.ItScCode, C.Qty, C.CurCode, C.EstPrice, A.MaterialRequestDNo ");
            SQL.AppendLine("From TblTender A ");
            SQL.AppendLine("Inner Join TblMaterialRequestHdr B On A.MaterialRequestDocNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblMaterialRequestDtl C On A.MaterialRequestDocNo = C.DocNo And A.MaterialRequestDNo = C.DNo ");
            SQL.AppendLine("Inner Join TblItem D On C.ItCode = D.ItCode ");
            SQL.AppendLine("Left Join TblSite E On B.SiteCode = E.SiteCode ");
            SQL.AppendLine("Where A.ActInd = 'Y' And A.Status = 'O' ");

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            if (Sm.IsDteEmpty(DteDocDt1, "Start Date") ||
                Sm.IsDteEmpty(DteDocDt2, "End Date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " And 0 = 0 ";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDt");

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter +
                    " Order By A.CreateDt Desc;",
                    new string[] 
                    { 
                    
                        //0
                        "DocNo",
                        
                        //1-5
                        "DocDt", "TenderName", "MaterialRequestDocNo", "SiteCode", "SiteName", 
                        
                        //6-10
                        "ItCode", "ItName", "ItScCode", "Qty", "CurCode",   
                        
                        //11-12
                        "EstPrice", "MaterialRequestDNo"
                        
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 8);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 10);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 12);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ClearData2();

                int Row = Grd1.CurRow.Index;

                mFrmParent.TxtTenderDocNo.EditValue = Sm.GetGrdStr(Grd1, Row, 1);
                mFrmParent.mTenderDocNo = Sm.GetGrdStr(Grd1, Row, 1);
                mFrmParent.mMRDocNo = Sm.GetGrdStr(Grd1, Row, 5);
                mFrmParent.mMRDNo = Sm.GetGrdStr(Grd1, Row, 15);
                mFrmParent.TxtItCode.EditValue = Sm.GetGrdStr(Grd1, Row, 9);
                mFrmParent.TxtItName.EditValue = Sm.GetGrdStr(Grd1, Row, 10);
                mFrmParent.TxtQty.EditValue = Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 12), 0);
                mFrmParent.TxtEstCurCode.EditValue = Sm.GetGrdStr(Grd1, Row, 13);
                mFrmParent.TxtEstPrice.EditValue = Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 14), 0);

                this.Close();
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                ChooseData();
            }
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f1 = new FrmTender(mFrmParent.mMenuCode);
                f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f1.Text = Sm.GetMenuDesc("FrmTender");
                f1.Tag = mFrmParent.mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f1.ShowDialog();
            }

            if (e.ColIndex == 6 && Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length != 0)
            {
                e.DoDefault = false;
                var f1 = new FrmMaterialRequest(mFrmParent.mMenuCode);
                f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 5);
                f1.Text = Sm.GetMenuDesc("FrmMaterialRequest");
                f1.Tag = mFrmParent.mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 5);
                f1.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f1 = new FrmTender(mFrmParent.mMenuCode);
                f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f1.Text = Sm.GetMenuDesc("FrmTender");
                f1.Tag = mFrmParent.mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f1.ShowDialog();
            }

            if (e.ColIndex == 6 && Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length != 0)
            {
                var f1 = new FrmMaterialRequest(mFrmParent.mMenuCode);
                f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 5);
                f1.Text = Sm.GetMenuDesc("FrmMaterialRequest");
                f1.Tag = mFrmParent.mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 5);
                f1.ShowDialog();
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control events

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt1).Length == 0) DteDocDt1.EditValue = DteDocDt2.EditValue;
        }

        #endregion

        #endregion

    }
}
