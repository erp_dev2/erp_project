﻿#region Update
/*
    27/11/2019 [TKG/KBN] New Application
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPurchaseInvoice2Dlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmPurchaseInvoice2 mFrmParent;
        private string mSQL = string.Empty, mVdCode = string.Empty;

        #endregion

        #region Constructor

        public FrmPurchaseInvoice2Dlg(FrmPurchaseInvoice2 FrmParent, string VdCode)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mVdCode = VdCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -180);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 34;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "", 
                        "Document#",
                        "DNo", 
                        "Date",
                        "PO#",
                        
                        //6-10
                        "Item's"+Environment.NewLine+"Code",
                        "", 
                        "Item's Name", 
                        "Quantity",
                        "UoM",
                        
                        //11-15
                        "Term of"+Environment.NewLine+"Payment",
                        "Currency",
                        "Unit"+Environment.NewLine+"Price",
                        "Discount"+Environment.NewLine+"%",
                        "Discount"+Environment.NewLine+"Amount",
                        
                        //16-20
                        "Rounding"+Environment.NewLine+"Value",
                        "Tax 1"+Environment.NewLine+"For Information Only",
                        "Tax 2"+Environment.NewLine+"For Information Only",
                        "Tax 3"+Environment.NewLine+"For Information Only",
                        "Total",

                        //21-25
                        "Delivery Type",
                        "ItScCode",
                        "Sub-Category", 
                        "Remark From MR",
                        "Received Local#",

                        //26-30
                        "Site Code",
                        "Site",
                        "Foreign Name",
                        "DO#",
                        "MR Local#",

                        //31-33
                        "Estimated"+Environment.NewLine+"Downpayment",
                        "Project"+Environment.NewLine+"Name",
                        "LOP#" 
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        20, 150, 0, 80, 150, 
                        
                        //6-10
                        80, 20, 200, 100, 80,  
                        
                        //11-15
                        150, 80, 100, 100, 100, 

                        //16-20
                        100, 100, 100, 100, 130, 

                        //21-25
                        150, 0, 150, 200, 130, 

                        //26-30
                        0, 150, 180, 130, 130,

                        //31-33
                        130, 250, 100
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColButton(Grd1, new int[] { 7 });
            Sm.GrdColReadOnly(Grd1, new int[] { 2, 3, 4, 5, 6, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32 });
            Sm.GrdFormatDec(Grd1, new int[] { 9, 13, 14, 15, 16, 17, 18, 19, 20, 31 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 4 });
            if (Sm.GetParameter("ProcFormatDocNo") == "0")
            {
                Sm.GrdColInvisible(Grd1, new int[] { 3, 6, 7, 14, 15, 16, 17, 18, 19, 22, 23, 24, 26, 27, 28, 29, 32,33 }, false);
            }
            else
            {
                Sm.GrdColInvisible(Grd1, new int[] { 3, 6, 7, 14, 15, 16, 17, 18, 19, 22, 24, 26, 27, 28, 29,32,33 }, false);
                Grd1.Cols[23].Move(9);
            }

            if (!mFrmParent.mIsShowForeignName)
            {
                Grd1.Cols[28].Visible = true;
                Grd1.Cols[28].Move(10);
            }
            Grd1.Cols[25].Move(3);
            Grd1.Cols[30].Move(3);
            if (mFrmParent.mIsSiteMandatory)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 27 }, true);
                Grd1.Cols[27].Move(7);
            }
            Grd1.Cols[29].Move(5);
            Grd1.Cols[31].Move(9);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 6, 7, 14, 15, 16, 17, 18, 19, 24, 29, 32 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, B.DNo, A.DocDt, B.PODocNo, B.ItCode, I.ItName, B.QtyPurchase, I.PurchaseUomCode, N.DTName, ");
            SQL.AppendLine("J.PtName, G.CurCode, H.UPrice, D.Discount, ((B.QtyPurchase/D.Qty)*D.DiscountAmt) As DiscountAmt, D.RoundingValue, ");
            SQL.AppendLine("(((B.QtyPurchase* H.UPrice * Case When IfNull(D.Discount, 0)=0 Then 1 Else (100-D.Discount)/100 End)-((B.QtyPurchase/D.Qty)*D.DiscountAmt)+((B.QtyPurchase/D.Qty)*D.RoundingValue))*Case When IfNull(K.TaxRate, 0)=0 Then 0 Else K.TaxRate/100 End) As TaxAmt1, ");
            SQL.AppendLine("(((B.QtyPurchase* H.UPrice * Case When IfNull(D.Discount, 0)=0 Then 1 Else (100-D.Discount)/100 End)-((B.QtyPurchase/D.Qty)*D.DiscountAmt)+((B.QtyPurchase/D.Qty)*D.RoundingValue))*Case When IfNull(L.TaxRate, 0)=0 Then 0 Else L.TaxRate/100 End) As TaxAmt2, ");
            SQL.AppendLine("(((B.QtyPurchase* H.UPrice * Case When IfNull(D.Discount, 0)=0 Then 1 Else (100-D.Discount)/100 End)-((B.QtyPurchase/D.Qty)*D.DiscountAmt)+((B.QtyPurchase/D.Qty)*D.RoundingValue))*Case When IfNull(M.TaxRate, 0)=0 Then 0 Else M.TaxRate/100 End) As TaxAmt3,  ");
            SQL.AppendLine("    ((B.QtyPurchase* H.UPrice * Case When IfNull(D.Discount, 0)=0 Then 1 Else (100-D.Discount)/100 End)-((B.QtyPurchase/D.Qty)*D.DiscountAmt)+((B.QtyPurchase/D.Qty)*D.RoundingValue)) As Total, I.ItScCode, O.ItScName, F.Remark As RemarkMR, A.LocalDocNo, ");
            SQL.AppendLine("C.SiteCode, P.SiteName, I.ForeignName, A.VdDONo, ");
            SQL.AppendLine("Q.MaterialRequestLocalDocNo, IfNull(R.APDownpaymentAmt, 0.00) As APDownpaymentAmt , D.LOPDocNo, S.ProjectName ");
            SQL.AppendLine("From TblRecvVdHdr A ");
            SQL.AppendLine("Inner Join TblRecvVdDtl B On A.DocNo=B.DocNo And B.CancelInd='N' And IfNull(B.Status, 'O')='A' And Concat(B.DocNo, B.DNo) Not In ( ");
            SQL.AppendLine("    Select Concat(T2.RecvVdDocNo, T2.RecvVdDNo) From TblPurchaseInvoiceHdr T1, TblPurchaseInvoiceDtl T2 ");
            SQL.AppendLine("    Where T1.DocNo=T2.DocNo And T1.CancelInd='N') ");
            SQL.AppendLine("Inner Join TblPOHdr C On B.PODocNo=C.DocNo ");
            SQL.AppendLine("Inner Join TblPODtl D On B.PODocNo=D.DocNo And B.PODNo=D.DNo ");
            SQL.AppendLine("Inner Join TblPORequestDtl E On D.PORequestDocNo=E.DocNo And D.PORequestDNo=E.DNo ");
            SQL.AppendLine("Inner Join TblMaterialRequestDtl F On E.MaterialRequestDocNo=F.DocNo And E.MaterialRequestDNo=F.DNo ");
            SQL.AppendLine("Inner Join TblQtHdr G On E.QtDocNo=G.DocNo ");
            if (mFrmParent.mIsGroupPaymentTermActived)
            {
                SQL.AppendLine("    And G.PtCode Is Not Null ");
                SQL.AppendLine("    And G.PtCode In (");
                SQL.AppendLine("        Select PtCode From TblGroupPaymentTerm ");
                SQL.AppendLine("        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Inner Join TblQtDtl H On E.QtDocNo=H.DocNo And E.QtDNo=H.DNo ");
            SQL.AppendLine("Inner Join TblItem I On B.ItCode=I.ItCode ");
            SQL.AppendLine("Left Join TblPaymentTerm J On G.PtCode=J.PtCode ");
            SQL.AppendLine("Left Join TblTax K On C.TaxCode1=K.TaxCode ");
            SQL.AppendLine("Left Join TblTax L On C.TaxCode2=L.TaxCode ");
            SQL.AppendLine("Left Join TblTax M On C.TaxCode3=M.TaxCode ");
            SQL.AppendLine("Left Join TblDeliveryType N On G.DTCode=N.DTCode ");
            SQL.AppendLine("left Join TblItemSubcategory O On I.ItScCode = O.ItScCode ");
            SQL.AppendLine("Left Join TblSite P On A.SiteCode=P.SiteCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T1.DocNo, ");
            SQL.AppendLine("	Group_Concat(Distinct T5.LocalDocNo Separator ', ') As MaterialRequestLocalDocNo ");
            SQL.AppendLine("    From TblRecvVdHdr T1 ");
            SQL.AppendLine("    Inner Join TblRecvVdDtl T2 On T1.DocNo=T2.DocNo And T2.CancelInd='N' ");
            SQL.AppendLine("    Inner Join TblPODtl T3 On T2.PODocNo=T3.DocNo And T2.PODNo=T3.DNo ");
            SQL.AppendLine("    Inner Join TblPORequestDtl T4 On T3.PORequestDocNo=T4.DocNo And T3.PORequestDNo=T4.DNo ");
            SQL.AppendLine("    Inner Join TblMaterialRequestHdr T5 On T4.MaterialRequestDocNo=T5.DocNo And T5.LocalDocNo Is Not Null ");
            SQL.AppendLine("    Where T1.VdCode=@VdCode ");
            SQL.AppendLine("    And T1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    Group By T1.DocNo ");
            SQL.AppendLine(") Q On A.DocNo=Q.DocNo ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T1.PODocNo, Sum(T1.Amt) As APDownpaymentAmt ");
            SQL.AppendLine("    From TblAPDownpayment T1 ");
            SQL.AppendLine("    Inner Join TblVoucherHdr T2 On T1.VoucherRequestDocNo=T2.VoucherRequestDocNo And T2.CancelInd='N' ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    And T1.Status='A' ");
            SQL.AppendLine("    And T1.PODocNo In ( ");
            SQL.AppendLine("        Select Distinct X2.PODocNo ");
            SQL.AppendLine("        From TblRecvVdHdr X1 ");
            SQL.AppendLine("        Inner Join TblRecvVdDtl X2 ");
            SQL.AppendLine("            On X1.DocNo=X2.DocNo ");
            SQL.AppendLine("            And X2.CancelInd='N' ");
            SQL.AppendLine("            And IfNull(X2.Status, 'O')='A' ");
            SQL.AppendLine("        Where X1.VdCode=@VdCode ");
            SQL.AppendLine("        And X1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("    Group By T1.PODocNo ");
            SQL.AppendLine(") R On B.PODocNo=R.PODocNo ");
            SQL.AppendLine("Left Join TblLOPHdr S On D.LOPDocNo = S.DocNo  ");
            SQL.AppendLine("Where A.VdCode=@VdCode ");
            SQL.AppendLine("And A.DocDt Between @DocDt1 And @DocDt2 ");
            if (mFrmParent.mIsFilterBySite)
            {
                SQL.AppendLine("And (A.SiteCode Is Null Or (A.SiteCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupSite ");
                SQL.AppendLine("    Where SiteCode=IfNull(A.SiteCode, '') ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ))) ");
            }
            
            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();
                string 
                    DocNo = string.Empty, 
                    DNo = string.Empty,
                    Filter = string.Empty;

                if (mFrmParent.Grd1.Rows.Count >= 1)
                {
                    for (int r = 0; r < mFrmParent.Grd1.Rows.Count; r++)
                    {
                        DocNo = Sm.GetGrdStr(mFrmParent.Grd1, r, 2);
                        DNo = Sm.GetGrdStr(mFrmParent.Grd1, r, 3);
                        if (DocNo.Length != 0)
                        {
                            if (Filter.Length > 0) Filter += " Or ";
                            Filter += " (B.DocNo=@DocNo0" + r.ToString() + " And B.DNo=@DNo0" + r.ToString() + ") ";
                            Sm.CmParam<String>(ref cm, "@DocNo0" + r.ToString(), DocNo);
                            Sm.CmParam<String>(ref cm, "@DNo0" + r.ToString(), DNo);
                        }
                    }
                }

                if (Filter.Length != 0)
                    Filter = " And Not ( " + Filter + ") ";
                else
                    Filter = " ";

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.CmParam<String>(ref cm, "@VdCode", mVdCode);
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtVdDONo.Text, "A.VdDONo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "B.ItCode", "I.ItName", "I.ForeignName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.CreateDt Desc;",
                        new string[] 
                        { 
                            //0
                            "DocNo",
                            
                            //1-5
                            "DNo", "DocDt", "PODocNo", "ItCode", "ItName", 
                
                            //6-10
                            "QtyPurchase", "PurchaseUomCode", "PtName", "CurCode", "UPrice", 
                
                            //11-15
                            "Discount", "DiscountAmt", "RoundingValue", "TaxAmt1", "TaxAmt2", 
                            
                            //16-20
                            "TaxAmt3", "Total", "DTName", "ItScCode", "ItScName",
 
                            //21-25
                            "RemarkMR", "LocalDocNo", "SiteCode", "SiteName", "ForeignName",

                            //26-30
                            "VdDONo", "MaterialRequestLocalDocNo", "APDownpaymentAmt", "ProjectName" , "LOPDocNo"
                            
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = false;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 10);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 11);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 12);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 13);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 14);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 15);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 16);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 17);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 18);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 19);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 20);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 21);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 22);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 23);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 27, 24);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 28, 25);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 29, 26);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 30, 27);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 31, 28);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 32, 29);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 33, 30);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsDataAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 2, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 3, Grd1, Row2, 3);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 5, Grd1, Row2, 5);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 7, Grd1, Row2, 6);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 9, Grd1, Row2, 8);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 10, Grd1, Row2, 9);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 11, Grd1, Row2, 10);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 12, Grd1, Row2, 11);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 13, Grd1, Row2, 12);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 14, Grd1, Row2, 13);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 15, Grd1, Row2, 14);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 16, Grd1, Row2, 15);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 17, Grd1, Row2, 16);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 18, Grd1, Row2, 17);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 19, Grd1, Row2, 18);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 20, Grd1, Row2, 19);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 21, Grd1, Row2, 20);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 22, Grd1, Row2, 21);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 24, Grd1, Row2, 22);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 26, Grd1, Row2, 24);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 27, Grd1, Row2, 25);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 28, Grd1, Row2, 26);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 29, Grd1, Row2, 27);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 30, Grd1, Row2, 4);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 31, Grd1, Row2, 28);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 32, Grd1, Row2, 29);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 33, Grd1, Row2, 30);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 34, Grd1, Row2, 31);
                        mFrmParent.Grd1.Cells[Row1, 23].Value = null;
                        mFrmParent.Grd1.Cells[Row1, 37].Value = null;
                        mFrmParent.Grd1.Cells[Row1, 38].Value = null;
                        mFrmParent.Grd1.Cells[Row1, 39].Value = null;
                        mFrmParent.Grd1.Cells[Row1, 40].Value = null;
                        mFrmParent.Grd1.Cells[Row1, 41].Value = 0m;
                        mFrmParent.ComputeAmt();
                        mFrmParent.Grd1.Rows.Add();
                        Sm.SetGrdNumValueZero(ref mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 10, 14, 15, 16, 17, 18, 19, 20, 21, 34, 41 });
                    }
                }
            }
            mFrmParent.TxtCurCode.EditValue = Sm.GetGrdStr(mFrmParent.Grd1, 0, 13);
            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 received document.");
        }

        private bool IsDataAlreadyChosen(int Row)
        {
            for (int Index = 0; Index <= mFrmParent.Grd1.Rows.Count - 1; Index++)
                if (Sm.CompareStr(
                    Sm.GetGrdStr(mFrmParent.Grd1, Index, 2) + Sm.GetGrdStr(mFrmParent.Grd1, Index, 3),
                    Sm.GetGrdStr(Grd1, Row, 2) + Sm.GetGrdStr(Grd1, Row, 3)
                    )) return true;
            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 7 && Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length != 0)
                Sm.ShowItemInfo(mFrmParent.mMenuCode, Sm.GetGrdStr(Grd1, e.RowIndex, 6));
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 7 && Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length != 0)
                Sm.ShowItemInfo(mFrmParent.mMenuCode, Sm.GetGrdStr(Grd1, e.RowIndex, 6));
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    Grd1.Cells[Row, 1].Value = !IsSelected;
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void TxtVdDONo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkVdDONo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Vendor's document#");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        #endregion

        #endregion
    }
}
