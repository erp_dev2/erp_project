﻿#region Update
/*
    30/12/2020 [WED/PHT] new apps
    12/01/2021 [TKG/PHT] hanya ss program tertentu berdasarkan parameter SSProgramSalarySSUMK yang salarynya dibandingkan dengan UMK.    
    30/04/2021 [WED/PHT] employee yang muncul hanya yg employee nya terdapat SSProgram terpilih berdasarkan data SS Program di tab Salary SS menu ESADA
    30/04/2021 [WED/PHT] Salary untuk SSProgram sesuai parameter SSPCodeReferredMonthlySalaryOnCalculateSalarySS
    03/05/2021 [WED/PHT] employee yang muncul jadinya bukan dari ESADA, tapi dari Employee Social Security Amendment
    11/05/2021 [IBL/PHT] component salary. kolom amount mengambil dari start dt yang terbaru
    15/06/2021 [TKG/PHT] bug saat save
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmEmpCalculateSalarySS : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mDocNo = string.Empty;
        internal FrmEmpCalculateSalarySSFind FrmFind;
        internal bool
            mIsFilterBySiteHR = false,
            mIsFilterByDeptHR = false
            ;
        private int mInitRowsCount = 0, mCurrRowsCount = 0;
        private string mSSPCodeReferredMonthlySalaryOnCalculateSalarySS = string.Empty;
        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmEmpCalculateSalarySS(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Calculate Salary Social Security";

                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                Sl.SetLueSSPCode(ref LueSSPCode);
                Sl.SetLueSiteCode(ref LueSiteCode, string.Empty, mIsFilterBySiteHR ? "Y" : "N");
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mIsFilterByDeptHR ? "Y" : "N");
                SetLueComponentCode(ref LueComponentCode);
                SetGrd();
                SetFormControl(mState.View);
                LueComponentCode.Visible = false;

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grid 1 - Employee
            Grd1.Cols.Count = 14;
            Grd1.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                Grd1,
                new string[] 
                {
                    //0
                    "DNo",

                    //1-5
                    "No.",
                    "Employee's Code",
                    "",
                    "Employee's Code Old",
                    "Employee's Name",

                    //6-10
                    "Site",
                    "Department",
                    "Division",
                    "Grade",
                    "Level",

                    //11-13
                    "Amount",
                    "GrdSalaryCode",
                    "LevelCode"
                },
                new int[] 
                { 
                    0, 
                    60, 120, 20, 180, 200, 
                    180, 200, 180, 100, 100, 
                    150, 0, 0
                }
            );
            Sm.GrdColButton(Grd1, new int[] { 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 11 }, 0);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 });
            Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 3, 4, 12, 13 });
            #endregion

            #region Grid 2 - Component
            Grd2.Cols.Count = 3;
            Grd2.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                Grd2,
                new string[] 
                {
                    //0
                    "DNo",

                    //1-2
                    "ADCode",
                    "Component"
                },
                new int[] 
                { 
                    0, 
                    0, 180
                }
            );
            Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 1 });
            Sm.GrdColInvisible(Grd2, new int[] { 0, 1 });

            #endregion
        }

        protected override void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 4 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd2, new int[] { 1 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, LueSSPCode, MeeRemark,
                        DteStartDt, DteEndDt, LueSiteCode, LueDeptCode, 
                        LueComponentCode
                    }, true);
                    Grd1.ReadOnly = true;
                    Grd2.ReadOnly = true;
                    BtnProcessData.Enabled = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueSSPCode, MeeRemark, DteStartDt, DteEndDt, LueSiteCode, 
                        LueDeptCode, LueComponentCode
                    }, false);
                    Grd1.ReadOnly = false;
                    Grd2.ReadOnly = false;
                    BtnProcessData.Enabled = true;
                    DteDocDt.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, LueSSPCode, MeeRemark,
                DteStartDt, DteEndDt, LueSiteCode, LueDeptCode, 
                LueComponentCode
            });
            ClearGrd();
            BtnProcessData.Focus();
        }

        internal void ClearGrd()
        {
            Grd1.Rows.Clear();
            Sm.ClearGrd(Grd2, true);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmEmpCalculateSalarySSFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sm.SetDteCurrentDate(DteStartDt);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowEmpCalculateSalarySSHdr(DocNo);
                ShowEmpCalculateSalarySSDtl(DocNo);
                ShowEmpCalculateSalarySSDtl2(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }

        }

        private void ShowEmpCalculateSalarySSHdr(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select DocNo, DocDt, StartDt, EndDt, SSPCode, SiteCode, DeptCode, Remark ");
            SQL.AppendLine("From TblEmpCalculateSalarySSHdr A ");
            SQL.AppendLine("Where DocNo = @DocNo; ");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[] 
                {
                    //0
                    "DocNo",

                    //1-5
                    "DocDt", "StartDt", "EndDt", "SSPCode", "SiteCode", 

                    //6-7
                    "DeptCode", "Remark"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                    Sm.SetDte(DteStartDt, Sm.DrStr(dr, c[2]));
                    Sm.SetDte(DteEndDt, Sm.DrStr(dr, c[3]));
                    Sm.SetLue(LueSSPCode, Sm.DrStr(dr, c[4]));
                    Sm.SetLue(LueSiteCode, Sm.DrStr(dr, c[5]));
                    Sm.SetLue(LueDeptCode, Sm.DrStr(dr, c[6]));
                    MeeRemark.EditValue = Sm.DrStr(dr, c[7]);
                }, true
            );
        }

        private void ShowEmpCalculateSalarySSDtl(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.DNo, A.EmpCode, B.EmpCodeOld, B.EmpName, D.SiteName, C.DeptName, ");
            SQL.AppendLine("E.DivisionName, F.GrdSalaryName, G.LevelName, A.Amt, A.GrdSalaryCode, A.LevelCode ");
            SQL.AppendLine("From TblEmpCalculateSalarySSDtl A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode = B.EmpCode ");
            SQL.AppendLine("    And A.DocNo = @DocNo ");
            SQL.AppendLine("Inner Join TblDepartment C On B.DeptCode = C.DeptCode ");
            SQL.AppendLine("Left Join TblSite D On B.SiteCode = D.SiteCode ");
            SQL.AppendLine("Left Join TblDivision E On B.DivisionCode = E.DivisionCode ");
            SQL.AppendLine("Left Join TblGradeSalaryHdr F On A.GrdSalaryCode = F.GrdSalaryCode ");
            SQL.AppendLine("Left Join TblLevelHdr G On A.LevelCode = G.LevelCode ");
            SQL.AppendLine("Order By A.DNo; ");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    "DNo",
                    "EmpCode", "EmpCodeOld", "EmpName", "SiteName", "DeptName",
                    "DivisionName", "GrdSalaryName", "LevelName", "Amt", "GrdSalaryCode",
                    "LevelCode"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Grd.Cells[Row, 1].Value = Row + 1;
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 10);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 11);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ShowEmpCalculateSalarySSDtl2(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.DNo, A.ComponentCode, B.ADName ");
            SQL.AppendLine("From TblEmpCalculateSalarySSDtl2 A ");
            SQL.AppendLine("Inner Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select 'Salary' As ADCode, 'Salary' As ADName ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select ADCode, ADName ");
            SQL.AppendLine("    From TblAllowanceDeduction ");
            SQL.AppendLine("    Where ADType = 'A' ");
            SQL.AppendLine(") B On A.ComponentCode = B.ADCode ");
            SQL.AppendLine("Where A.DocNo = @DocNo ");
            SQL.AppendLine("Order By A.DNo; ");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd2, ref cm, SQL.ToString(),
                new string[] 
                { 
                    "DNo",
                    "ComponentCode", "ADName"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd2, 0, 0);
        }

        #endregion

        #region Save Data

        private void InsertData()
        {
            if (
                Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No ||
                IsDataNotValid()
                ) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = string.Empty;
            DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "EmpCalculateSalarySS", "TblEmpCalculateSalarySSHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveEmpCalculateSalarySSHdr(DocNo));

            if (Grd1.Rows.Count > 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0)
                    {
                        cml.Add(SaveEmpCalculateSalarySSDtl(DocNo, Row));
                        cml.Add(UpdateEmployeeSalarySS(DocNo, Row));
                    }
                }
            }

            if (Grd2.Rows.Count > 0)
                for (int Row = 0; Row < Grd2.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd2, Row, 1).Length > 0)
                        cml.Add(SaveEmpCalculateSalarySSDtl2(DocNo, Row));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsDteEmpty(DteStartDt, "Start Date") ||
                Sm.IsDteEmpty(DteEndDt, "End Date") ||
                IsStartEndDateInvalid() ||
                IsGrdEmpty() ||
                IsGrdValueNotValid()
                ;
        }

        private bool IsStartEndDateInvalid()
        {
            if (Sm.CompareDtTm(Sm.GetDte(DteStartDt), Sm.GetDte(DteEndDt)) > 0)
            {
                Sm.StdMsg(mMsgType.Warning, "End date is earlier than start date.");
                return true;
            }

            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd2.Rows.Count <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 component on the list.");
                Sm.FocusGrd(Grd2, 0, 2);
                return true;
            }

            if (Grd1.Rows.Count <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 employee on the list.");
                BtnProcessData.Focus();
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            string Comp1 = string.Empty, Comp2 = string.Empty;

            for (int Row = 0; Row < Grd2.Rows.Count; Row++)
            {
                Comp1 = Sm.GetGrdStr(Grd2, Row, 1);
                for (int Row2 = (Row + 1); Row2 < Grd2.Rows.Count; ++Row2)
                {
                    Comp2 = Sm.GetGrdStr(Grd2, Row2, 1);

                    if (Sm.CompareStr(Comp1, Comp2))
                    {
                        Sm.StdMsg(mMsgType.Warning, "Duplicate component found in row #" + (Row2+1));
                        Sm.FocusGrd(Grd2, Row2, 2);
                        return true;
                    }
                }
            }

            return false;
        }

        private MySqlCommand SaveEmpCalculateSalarySSHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblEmpCalculateSalarySSHdr(DocNo, DocDt, StartDt, EndDt, ");
            SQL.AppendLine("SSPCode, SiteCode, DeptCode, ");
            SQL.AppendLine("Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @StartDt, @EndDt, ");
            SQL.AppendLine("@SSPCode, @SiteCode, @DeptCode, ");
            SQL.AppendLine("@Remark, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));
            Sm.CmParam<String>(ref cm, "@SSPCode", Sm.GetLue(LueSSPCode));
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveEmpCalculateSalarySSDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblEmpCalculateSalarySSDtl(DocNo, DNo, EmpCode, GrdSalaryCode, LevelCode, Amt, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @EmpCode, @GrdSalaryCode, @LevelCode, @Amt, ");
            SQL.AppendLine("@CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("0000000000" + (Row + 1).ToString(), 10));
            Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd1, Row, 11));
            Sm.CmParam<String>(ref cm, "@GrdSalaryCode", Sm.GetGrdStr(Grd1, Row, 12));
            Sm.CmParam<String>(ref cm, "@LevelCode", Sm.GetGrdStr(Grd1, Row, 13));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand UpdateEmployeeSalarySS(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            //string DNo = Sm.GetValue("Select Max(DNo) From TblEmployeeSalarySS Where EmpCode = @Param", Sm.GetGrdStr(Grd1, Row, 2));
            //if (DNo.Length == 0) DNo = "001";
            //else DNo = Sm.Right(string.Concat("000", (Int32.Parse(DNo) + 1).ToString()), 3);

            SQL.AppendLine("Set @Dt:=CurrentDateTime(); ");

            SQL.AppendLine("Delete From TblEmployeeSalarySS Where EmpCode=@EmpCode And SSPCode=@SSPCode; ");

            SQL.AppendLine("Insert Into TblEmployeeSalarySS(EmpCode, DNo, SSPCode, StartDt, EndDt, Amt, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@EmpCode, ");
            SQL.AppendLine("Right(Concat('00', Cast(Cast(IfNull((Select Val From (Select Max(DNo) Val From TblEmployeeSalarySS T Where EmpCode=@EmpCode) T), '000') As Decimal(3, 0))+1 As Varchar(3))), 3), ");
            SQL.AppendLine("@SSPCode, @StartDt, @EndDt, @Amt, @UserCode, @Dt); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd1, Row, 2));
            //Sm.CmParam<String>(ref cm, "@DNo", DNo);
            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd1, Row, 11));
            Sm.CmParam<String>(ref cm, "@SSPCode", Sm.GetLue(LueSSPCode));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveEmpCalculateSalarySSDtl2(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblEmpCalculateSalarySSDtl2(DocNo, DNo, ComponentCode, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @ComponentCode, ");
            SQL.AppendLine("@CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("0000000000" + (Row + 1).ToString(), 10));
            Sm.CmParam<String>(ref cm, "@ComponentCode", Sm.GetGrdStr(Grd2, Row, 1));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Grid

        private void Grd2_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && !Grd2.ReadOnly)
            {
                if (e.ColIndex == 2)
                {
                    LueRequestEdit(Grd2, LueComponentCode, ref fCell, ref fAccept, e);
                    Sm.GrdRequestEdit(Grd2, e.RowIndex);
                    mCurrRowsCount = mInitRowsCount = Grd2.Rows.Count;
                    Sm.ClearGrd(Grd1, false);
                }
            }
        }

        private void Grd2_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled && !Grd2.ReadOnly)
            {
                Sm.GrdKeyDown(Grd2, e, BtnFind, BtnSave);
                Sm.GrdRemoveRow(Grd2, e, BtnSave);
                mCurrRowsCount = Grd2.Rows.Count;

                if (mInitRowsCount != mCurrRowsCount)
                {
                    Sm.ClearGrd(Grd1, false);
                    mInitRowsCount = Grd2.Rows.Count;
                }
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled && !Grd1.ReadOnly)
            {
                Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
                Sm.GrdRemoveRow(Grd1, e, BtnSave);
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length > 0)
            {
                var f = new FrmEmployee(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mEmpCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }            
        }

        #endregion

        #region Additional Method

        private bool IsGrd2NotEmpty()
        {
            if (Grd2.Rows.Count <= 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 component on the list.");
                Sm.FocusGrd(Grd2, 0, 2);
                return true;
            }

            return false;
        }

        private void GetParameter()
        {
            mIsFilterBySiteHR = Sm.GetParameterBoo("IsFilterBySiteHR");
            mIsFilterByDeptHR = Sm.GetParameterBoo("IsFilterByDeptHR");
            mSSPCodeReferredMonthlySalaryOnCalculateSalarySS = Sm.GetParameter("SSPCodeReferredMonthlySalaryOnCalculateSalarySS");
        }

        private void SetLueComponentCode(ref DXE.LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 'Salary' As Col1, 'Salary' As Col2 ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select T.ADCode As Col1, T.ADName As Col2 ");
            SQL.AppendLine("From TblAllowanceDeduction T ");
            SQL.AppendLine("Where T.ADType = 'A' ");
            SQL.AppendLine("; ");

            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void LueRequestEdit(
          iGrid Grd,
          DevExpress.XtraEditors.LookUpEdit Lue,
          ref iGCell fCell,
          ref bool fAccept,
          TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;
            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        private void ProcessData()
        {
            var l = new List<Data>();
            var l2 = new List<MinimumWages>();
            var l3 = new List<Salary>();
            var l4 = new List<LevelAllowance>();

            string Sal = string.Empty, ADCode = string.Empty, SelectedEmpCode = string.Empty;
            var IsUseUMK = IsSSProgramUseUMK();
            ProcessDataEmployee(ref l);
            if (l.Count > 0)
            {
                GetSalaryCode(ref Sal);
                GetADCode(ref ADCode);
                GetSelectedEmpCode(ref l, ref SelectedEmpCode);

                if (Sal.Length > 0) ProcessSalary(ref l3, SelectedEmpCode);
                if (ADCode.Length > 0) ProcessLevelAllowance(ref l4, SelectedEmpCode, ADCode);

                if (IsUseUMK) GetMinimumWages(ref l2, SelectedEmpCode);
                ProcessAmount(ref l, ref l3, ref l4);
                if (IsUseUMK && l2.Count > 0) 
                    CompareToMinimumWages(ref l, ref l2);
                ShowDataToGrd(ref l);
            }
            else
                Sm.StdMsg(mMsgType.NoData, string.Empty);

            l.Clear(); l2.Clear(); l3.Clear(); l4.Clear();
            Sal = string.Empty; ADCode = string.Empty; SelectedEmpCode = string.Empty;
        }

        private bool IsSSProgramUseUMK()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 From TblSSProgram A, TblParameter B ");
            SQL.AppendLine("Where B.ParCode='SSProgramSalarySSUMK' ");
            SQL.AppendLine("And B.ParValue Is Not Null ");
            SQL.AppendLine("And Find_In_Set(A.SSPCode, B.ParValue) ");
            SQL.AppendLine("And A.SSPCode=@Param ");
            SQL.AppendLine("Limit 1; ");

            return Sm.IsDataExist(SQL.ToString(), Sm.GetLue(LueSSPCode));
        }

        private void ProcessDataEmployee(ref List<Data> l)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.EmpCode, A.EmpCodeOld, A.EmpName, F.SiteName, B.DeptName, C.DivisionName, ");
            SQL.AppendLine("D.GrdSalaryName, E.LevelName, A.SiteCode, A.GrdLvlCode, A.LevelCode ");
            SQL.AppendLine("From TblEmployee A ");
            SQL.AppendLine("Inner Join TblDepartment B On A.DeptCode = B.DeptCode ");
            if (Sm.GetLue(LueSiteCode).Length > 0)
                SQL.AppendLine("    And A.SiteCode = @SiteCode ");
            if (Sm.GetLue(LueDeptCode).Length > 0)
                SQL.AppendLine("    And A.DeptCode = @DeptCode ");
            SQL.AppendLine("Left Join TblDivision C On A.DivisionCode = C.DivisionCode ");
            SQL.AppendLine("Left Join TblGradeSalaryHdr D On A.GrdLvlCode = D.GrdSalaryCode ");
            SQL.AppendLine("Left Join TblLevelHdr E On A.LevelCode = E.LevelCode ");
            SQL.AppendLine("Left Join TblSite F On A.SiteCode = F.SiteCode ");
            SQL.AppendLine("Where (A.ResignDt Is Null Or (A.ResignDt Is Not Null And A.ResignDt > @CurrentDate)) ");
            SQL.AppendLine("And A.EmpCode In ");
            SQL.AppendLine("( ");
            //SQL.AppendLine("    Select Distinct EmpCode ");
            //SQL.AppendLine("    From TblEmployeeSalarySS ");
            //SQL.AppendLine("    Where SSPCode = @SSPCode ");
            SQL.AppendLine("    Select Distinct EmpCode ");
            SQL.AppendLine("    From TblEmployeeSS T1 ");
            SQL.AppendLine("    Inner Join TblSS T2 On T1.SSCode = T2.SSCode ");
            SQL.AppendLine("        And Find_In_Set(T2.SSPCode, @SSPCode) ");
            SQL.AppendLine(") ");
            SQL.AppendLine("Order By A.EmpName; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParamDt(ref cm, "@CurrentDate", Sm.ServerCurrentDateTime());
                Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
                Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
                Sm.CmParam<String>(ref cm, "@SSPCode", Sm.GetLue(LueSSPCode));
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                {
                    //0
                    "EmpCode",

                    //1-5
                    "EmpCodeOld",
                    "EmpName",
                    "SiteName",
                    "DeptName",
                    "DivisionName",

                    //6-10
                    "GrdSalaryName",
                    "LevelName",
                    "SiteCode",
                    "GrdLvlCode",
                    "LevelCode"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Data()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),

                            EmpCodeOld = Sm.DrStr(dr, c[1]),
                            EmpName = Sm.DrStr(dr, c[2]),
                            SiteName = Sm.DrStr(dr, c[3]),
                            DeptName = Sm.DrStr(dr, c[4]),
                            DivisionName = Sm.DrStr(dr, c[5]),
                            
                            GrdSalaryName = Sm.DrStr(dr, c[6]),
                            LevelName = Sm.DrStr(dr, c[7]),
                            SiteCode = Sm.DrStr(dr, c[8]),
                            GrdSalaryCode = Sm.DrStr(dr, c[9]),
                            LevelCode = Sm.DrStr(dr, c[10]),
                            Amt = 0m
                        });
                    }
                }
                dr.Close();
            }
        }

        private void GetSalaryCode(ref string Sal)
        {
            for (int i = 0; i < Grd2.Rows.Count; ++i)
            {
                if (Sm.GetGrdStr(Grd2, i, 1).Length > 0)
                {
                    if (Sm.GetGrdStr(Grd2, i, 1) == "Salary")
                    {
                        Sal = "Salary";
                        break;
                    }
                }
            }
        }

        private void GetADCode(ref string ADCode)
        {
            for (int i = 0; i < Grd2.Rows.Count; ++i)
            {
                if (Sm.GetGrdStr(Grd2, i, 1).Length > 0)
                {
                    if (Sm.GetGrdStr(Grd2, i, 1) != "Salary")
                    {
                        if (ADCode.Length > 0) ADCode += ",";
                        ADCode += Sm.GetGrdStr(Grd2, i, 1);
                    }
                }
            }
        }

        private void GetSelectedEmpCode(ref List<Data> l, ref string SelectedEmpCode)
        {
            foreach (var x in l)
            {
                if (SelectedEmpCode.Length > 0) SelectedEmpCode += ",";
                SelectedEmpCode += x.EmpCode;
            }
        }

        private void ProcessSalary(ref List<Salary> l, string SelectedEmpCode)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool mIsReferredToMonthlySalary = IsReferredToMonthlySalary();

            if (mSSPCodeReferredMonthlySalaryOnCalculateSalarySS.Length == 0 ||
                (mSSPCodeReferredMonthlySalaryOnCalculateSalarySS.Length > 0 && !mIsReferredToMonthlySalary)
                )
            {
                SQL.AppendLine("Select A.EmpCode, Sum(C.Amt) Amt ");
                SQL.AppendLine("From TblEmployee A ");
                SQL.AppendLine("Inner Join TblGradeSalaryHdr B On A.GrdLvlCode = B.GrdSalaryCode ");
                SQL.AppendLine("    And Find_In_Set(A.EmpCode, @SelectedEmpCode) ");
                SQL.AppendLine("Inner Join TblGradeSalaryDtl C On B.GrdSalaryCode = C.GrdSalaryCode ");
                SQL.AppendLine("    And TimeStampdiff(Year, A.JoinDt, left(Replace(CurDate(), '-', ''), 8)) = C.WorkPeriod ");
                SQL.AppendLine("    And (TimeStampdiff(Month, A.JoinDt, left(Replace(CurDate(), '-', ''), 8)) Mod 12) = C.Month ");
                SQL.AppendLine("Group By A.EmpCode; ");
            }
            else
            {
                SQL.AppendLine("Select A.EmpCode, A.Amt ");
                SQL.AppendLine("From TblEmployeeSalary A ");
                SQL.AppendLine("Inner Join ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select EmpCode, Max(StartDt) As StartDt ");
                SQL.AppendLine("    From TblEmployeeSalary ");
                SQL.AppendLine("    Where Find_In_Set(EmpCode, @SelectedEmpCode) ");
                SQL.AppendLine("    And ActInd = 'Y' ");
                SQL.AppendLine("    Group By EmpCode ");
                SQL.AppendLine(") B On A.EmpCode = B.EmpCode And A.StartDt = B.StartDt ");
                SQL.AppendLine("Where Find_In_Set(A.EmpCode, @SelectedEmpCode) ");
                SQL.AppendLine("And A.ActInd = 'Y' ");
                SQL.AppendLine("; ");
            }

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@SelectedEmpCode", SelectedEmpCode);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Salary()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            Amt = Sm.DrDec(dr, c[1]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private bool IsReferredToMonthlySalary()
        {
            if(mSSPCodeReferredMonthlySalaryOnCalculateSalarySS.Length == 0) return false;

            string SelectedSSPCode = Sm.GetLue(LueSSPCode);
            string[] ReferredSSPCode = mSSPCodeReferredMonthlySalaryOnCalculateSalarySS.Split(',');
            bool mFlag = false;

            foreach (var x in ReferredSSPCode)
            {
                if (x == SelectedSSPCode)
                {
                    mFlag = true;
                    break;
                }
            }

            return mFlag;

        }

        private void ProcessLevelAllowance(ref List<LevelAllowance> l, string SelectedEmpCode, string ADCode)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.EmpCode, B.ADCode, Sum(B.Amt) Amt ");
            SQL.AppendLine("From TblEmployee A ");
            SQL.AppendLine("Inner Join TblLevelDtl2 B On A.LevelCode = B.LevelCode ");
            SQL.AppendLine("    And Find_In_set(A.EmpCode, @SelectedEmpCode) ");
            SQL.AppendLine("    And Find_In_Set(B.ADCode, @ADCode) ");
            SQL.AppendLine("Group By A.EmpCode, B.ADCode; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@SelectedEmpCode", SelectedEmpCode);
                Sm.CmParam<String>(ref cm, "@ADCode", ADCode);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "EmpCode", "ADCode", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new LevelAllowance()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            ADCode = Sm.DrStr(dr, c[1]),
                            Amt = Sm.DrDec(dr, c[2]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private void GetMinimumWages(ref List<MinimumWages> l, string SelectedEmpCode)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select Distinct A.SiteCode, C.Amt ");
            SQL.AppendLine("From TblEmployee A ");
            SQL.AppendLine("Inner Join TblRegionalMinimumWagesHdr B On B.CancelInd = 'N' ");
            SQL.AppendLine("    And Find_In_set(A.EmpCode, @SelectedEmpCode) ");
            SQL.AppendLine("    And (B.StartDt <= @StartDt Or B.EndDt >= @EndDt) ");
            SQL.AppendLine("    And A.SiteCode Is Not Null ");
            SQL.AppendLine("Inner Join TblRegionalMinimumWagesDtl C On B.DocNo = C.DocNo ");
            SQL.AppendLine("    And A.SiteCode = C.SiteCode; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@SelectedEmpCode", SelectedEmpCode);
                Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
                Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "SiteCode", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new MinimumWages()
                        {
                            SiteCode = Sm.DrStr(dr, c[0]),
                            Amt = Sm.DrDec(dr, c[1]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessAmount(ref List<Data> l, ref List<Salary> l3, ref List<LevelAllowance> l4)
        {
            if (l3.Count > 0)
            {
                foreach (var x in l3)
                {
                    foreach(var y in l.Where(w => w.EmpCode == x.EmpCode))
                    {
                        y.Amt += x.Amt;
                    }
                }
            }

            if (l4.Count > 0)
            {
                foreach(var x in l4)
                {
                    foreach(var y in l.Where(w => w.EmpCode == x.EmpCode))
                    {
                        y.Amt += x.Amt;
                    }
                }
            }
        }

        private void CompareToMinimumWages(ref List<Data> l, ref List<MinimumWages> l2)
        {
            foreach (var x in l.Where(w => w.SiteCode.Length > 0))
            {
                foreach(var y in l2.Where(w => w.SiteCode == x.SiteCode))
                {
                    if (x.Amt < y.Amt) x.Amt = y.Amt;
                }
            }
        }

        private void ShowDataToGrd(ref List<Data> l)
        {
            Sm.ClearGrd(Grd1, false);
            int Row = 0;
            Grd1.BeginUpdate();

            foreach(var x in l)
            {
                Grd1.Rows.Add();

                Grd1.Cells[Row, 1].Value = Row + 1;
                Grd1.Cells[Row, 2].Value = x.EmpCode;
                Grd1.Cells[Row, 4].Value = x.EmpCodeOld;
                Grd1.Cells[Row, 5].Value = x.EmpName;
                Grd1.Cells[Row, 6].Value = x.SiteName;
                Grd1.Cells[Row, 7].Value = x.DeptName;
                Grd1.Cells[Row, 8].Value = x.DivisionName;
                Grd1.Cells[Row, 9].Value = x.GrdSalaryName;
                Grd1.Cells[Row, 10].Value = x.LevelName;
                Grd1.Cells[Row, 11].Value = Sm.FormatNum(x.Amt, 0);
                Grd1.Cells[Row, 12].Value = x.GrdSalaryCode;
                Grd1.Cells[Row, 13].Value = x.LevelCode;

                Row += 1;
            }

            Grd1.EndUpdate();
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void LueSSPCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueSSPCode, new Sm.RefreshLue1(Sl.SetLueSSPCode));
            }
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue3(Sl.SetLueSiteCode), string.Empty, mIsFilterBySiteHR ? "Y" : "N");
                ClearGrd();
            }
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, mIsFilterByDeptHR ? "Y" : "N");
                ClearGrd();
            }
        }

        private void LueComponentCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueComponentCode, new Sm.RefreshLue1(SetLueComponentCode));
            }
        }

        private void LueComponentCode_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.LueKeyDown(Grd1, ref fAccept, e);
            }
        }

        private void LueComponentCode_Leave(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (LueComponentCode.Visible && fAccept && fCell.ColIndex == 2)
                {
                    if (Sm.GetLue(LueComponentCode).Length == 0)
                        Grd2.Cells[fCell.RowIndex, 1].Value =
                        Grd2.Cells[fCell.RowIndex, 2].Value = null;
                    else
                    {
                        Grd2.Cells[fCell.RowIndex, 1].Value = Sm.GetLue(LueComponentCode);
                        Grd2.Cells[fCell.RowIndex, 2].Value = LueComponentCode.GetColumnValue("Col2");
                    }
                    LueComponentCode.Visible = false;
                }
            }
        }

        #endregion

        #region Button Click

        private void BtnProcessData_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled &&
                !Grd1.ReadOnly && 
                !Sm.IsDteEmpty(DteStartDt, "Start Date") &&
                !Sm.IsDteEmpty(DteEndDt, "End Date") &&
                !Sm.IsLueEmpty(LueSSPCode, "SS Program") &&
                !IsGrd2NotEmpty()
                )
            {
                ProcessData();
            }
        }

        #endregion

        #endregion

        #region Class

        private class Data
        {
            public string EmpCode { get; set; }
            public string EmpCodeOld { get; set; }
            public string EmpName { get; set; }
            public string SiteCode { get; set; }
            public string SiteName { get; set; }
            public string DeptName { get; set; }
            public string DivisionName { get; set; }
            public string GrdSalaryName { get; set; }
            public string LevelName { get; set; }
            public string GrdSalaryCode { get; set; }
            public string LevelCode { get; set; }
            public decimal Amt { get; set; }
        }

        private class Component
        {
            public string EmpCode { get; set; }
            public decimal Amt { get; set; }
        }

        private class Salary
        {
            public string EmpCode { get; set; }
            public decimal Amt { get; set; }
        }

        private class LevelAllowance
        {
            public string EmpCode { get; set; }
            public string ADCode { get; set; }
            public decimal Amt { get; set; }
        }

        private class MinimumWages
        {
            public string SiteCode { get; set; }
            public decimal Amt { get; set; }
        }

        #endregion
    }
}
