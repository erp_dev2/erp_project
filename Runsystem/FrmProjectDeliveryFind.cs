﻿#region Update
/*
    28/10/2019 [WED/YK] DocType 1 untuk standart Project System
 *  18/08/2020 [HAR/YK] bug saat Unhide (kolom todak tedefinisi)
    16/09/2020 [DITA/YK] bug kolom create by tidak muncul
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmProjectDeliveryFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmProjectDelivery mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmProjectDeliveryFind(FrmProjectDelivery FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            this.Text = mFrmParent.Text;
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
            SetGrd();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 13;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Document#", 
                    "Date",
                    "Cancel",
                    "PRJI#",
                    "Amount",
                    
                    //6-10
                    "Remark",
                    "Created"+Environment.NewLine+"By", 
                    "Created"+Environment.NewLine+"Date", 
                    "Created"+Environment.NewLine+"Time",
                    "Last"+Environment.NewLine+"Updated By", 
                   
                    //11-12
                    "Last"+Environment.NewLine+"Updated Date", 
                    "Last"+Environment.NewLine+"Updated Time"
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    150, 80, 60, 130, 180, 
                    
                    //6-10
                    200, 100, 100, 100, 150, 
                    
                    //11-12
                    150, 150
                }
            );
            Sm.GrdFormatDate(Grd1, new int[] { 2, 8, 11 });
            Sm.GrdColCheck(Grd1, new int[] { 3 });
            Sm.GrdFormatTime(Grd1, new int[] { 9, 12 });
            Sm.GrdColInvisible(Grd1, new int[] { 7, 8, 9, 10, 11, 12 }, false);
            Sm.GrdFormatDec(Grd1, new int[] { 5 }, 0);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 7, 8, 9, 10, 11, 12 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " And 1=1 ";

                var cm = new MySqlCommand();
                var SQL = new StringBuilder();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, new string[] { "T.DocNo" });
                Sm.FilterStr(ref Filter, ref cm, TxtPRJIDocNo.Text, new string[] { "T.PRJIDocNo" });

                SQL.AppendLine("Select T.DocNo, T.DocDt, T.CancelInd, T.PRJIDocNo, T.Amt, T.Remark, ");
                SQL.AppendLine("T.CreateBy, T.CreateDt, T.LastUpBy, T.LastUpDt ");
                SQL.AppendLine("From TblProjectDeliveryHdr T ");
                if (mFrmParent.mIsFilterBySite)
                {
                    SQL.AppendLine("Inner Join TblProjectImplementationHdr A On T.PRJIDocNo = A.DocNo ");
                    SQL.AppendLine("Inner Join TblSOContractRevisionHdr A1 On A.SOContractDocNo = A1.DocNo ");
                    SQL.AppendLine("Inner Join TblSOContractHdr B On A1.SOCDocNo = B.DocNo ");
                    SQL.AppendLine("Inner Join TblBOQHdr C On B.BOQDocNo = C.DocNo ");
                    SQL.AppendLine("Inner Join TblCustomer D On C.CtCode = D.CtCode ");
                    SQL.AppendLine("Inner Join TblLopHdr E On E.DocNo = C.LOPDocNo ");
                    SQL.AppendLine("And (E.SiteCode Is Null Or ( ");
                    SQL.AppendLine("    E.SiteCode Is Not Null ");
                    SQL.AppendLine("    And Exists( ");
                    SQL.AppendLine("        Select 1 From TblGroupSite ");
                    SQL.AppendLine("        Where SiteCode=IfNull(E.SiteCode, '') ");
                    SQL.AppendLine("        And GrpCode In ( ");
                    SQL.AppendLine("            Select GrpCode From TblUser ");
                    SQL.AppendLine("            Where UserCode=@UserCode ");
                    SQL.AppendLine("            ) ");
                    SQL.AppendLine("        ) ");
                    SQL.AppendLine(")) ");
                }
                SQL.AppendLine("Where T.DocType = '1' And (T.DocDt Between @DocDt1 And @DocDt2) ");

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, SQL.ToString() + Filter + " Order By T.DocDt Desc, T.DocNo; ",
                    new string[]
                    {
                        //0
                        "DocNo", 
                            
                        //1-5
                        "DocDt", "CancelInd", "PRJIDocNo", "Amt", "Remark", 

                        //6-9
                        "CreateBy", "CreateDt", "LastUpBy", "LastUpDt"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("T", Grd, dr, c, Row, 9, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 11, 9);
                        Sm.SetGrdValue("T", Grd, dr, c, Row, 12, 9);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt1).Length == 0) DteDocDt1.EditValue = DteDocDt2.EditValue;
        }

        private void TxtPRJIDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkPRJIDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Project#");
        }

        #endregion

        #endregion
    }
}
