﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;
#endregion

namespace RunSystem
{
    public partial class FrmProject : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "";
        internal string mDocNo = string.Empty;
        internal FrmProjectFind FrmFind;

        #endregion

        #region Constructor

        public FrmProject(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Project";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                BtnPrint.Visible = false;
                SetFormControl(mState.View);
                Sl.SetLueProfitCenterCode(ref LueProfitCenterCode);
                SetLueParentCode(ref LueParent);

                base.FrmLoad(sender, e);
                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, ChkActInd, LueProfitCenterCode, TxtAcCode, TxtAcDesc, LueParent, DteStartDt, DteEndDt, TxtLevel
                    }, true);
                    ChkActInd.Properties.ReadOnly = true;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                       DteDocDt, LueProfitCenterCode, TxtAcCode, TxtAcDesc, LueParent, DteStartDt, DteEndDt, TxtLevel
                    }, false);
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtAcCode, TxtAcDesc, LueParent, ChkActInd
                    }, false);
                    ChkActInd.Properties.ReadOnly = false;
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                  TxtDocNo, DteDocDt, ChkActInd, LueProfitCenterCode, LueParent, TxtAcCode, TxtAcDesc, DteStartDt, DteEndDt
            });
            Sm.SetControlNumValueZero(new List<TextEdit> 
            {
                  TxtLevel            
            }, 0);
            ChkActInd.Checked = false;
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmProjectFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                ChkActInd.Checked = true;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    CancelData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "Project", "TblProject");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveProject(DocNo));

            Sm.ExecCommands(cml);
            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsTxtEmpty(TxtAcCode, "Account", false)||
                Sm.IsLueEmpty(LueProfitCenterCode, "Project / Profit Center") ||
                Sm.IsTxtEmpty(TxtAcDesc, "Project / Profit Center Account Description", false) ||
                Sm.IsTxtEmpty(TxtLevel, "Level", true)||
                IsProjectAlreadyCreated() ||
                IsAccountAlreadyExist()
                ;
        }

        private bool IsProjectAlreadyCreated()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select DocNo From TblProject "+
                    "Where ParentDocNo is null "+
                    "And ProfitcenterCode = @ProfitCenterCode "+
                    "And ActInd='Y' And Level=@Level ;"
            };
            Sm.CmParam<String>(ref cm, "@ProfitCenterCode", Sm.GetLue(LueProfitCenterCode));
            Sm.CmParam<String>(ref cm, "@Level", TxtLevel.Text);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This project / profit center has been created.");
                ChkActInd.Checked = false;
                return true;
            }
            return false;
        }

        private bool IsAccountAlreadyExist()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select AcCode From TblProject " +
                    "Where AcCode = @AcCode " +
                    "And ActInd='Y' ;"
            };
            Sm.CmParam<String>(ref cm, "@AcCode", TxtAcCode.Text);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This account already exists.");
                ChkActInd.Checked = false;
                return true;
            }
            return false;
        }

        private MySqlCommand SaveProject(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblProject(DocNo, DocDt, ActInd, AcCode, ProfitCenterCode, AcDesc, StartDt, EndDt, ParentDocNo, Level, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @ActInd, @AcCode, @ProfitCenterCode, @AcDesc, @StartDt, @EndDt, @ParentDocNo, @Level, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@AcCode", TxtAcCode.Text);
            Sm.CmParam<String>(ref cm, "@ProfitCenterCode", Sm.GetLue(LueProfitCenterCode));
            Sm.CmParam<String>(ref cm, "@AcDesc", TxtAcDesc.Text);
            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));
            Sm.CmParam<String>(ref cm, "@ParentDocNo", Sm.GetLue(LueParent));
            Sm.CmParam<Decimal>(ref cm, "@Level", Decimal.Parse(TxtLevel.Text));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        #endregion

        #region Cancel data

        private void CancelData()
        {
            if (IsCancelledDataNotValid() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(UpdateProject());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsCancelledDataNotValid()
        {
            return
                IsProjectAlreadyNonActive() ||
                Sm.IsTxtEmpty(TxtAcCode, "Account", false) ||
                Sm.IsLueEmpty(LueProfitCenterCode, "Project / Profit Center") ||
                Sm.IsTxtEmpty(TxtAcDesc, "Project / Profit Center Account Description", false) ||
                Sm.IsTxtEmpty(TxtLevel, "Level", true)
                ;
        }

        private MySqlCommand UpdateProject()
        {

            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblProject Set ");
            SQL.AppendLine("    ACCode=@AcCode, AcDesc=@AcDesc, ParentDocNo=@ParentDocNo, ActInd=@ActInd, Level=@Level, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@AcCode", TxtAcCode.Text);
            Sm.CmParam<String>(ref cm, "@AcDesc", TxtAcDesc.Text);
            Sm.CmParam<String>(ref cm, "@ParentDocNo", Sm.GetLue(LueParent));
            Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked ? "Y" : "N");
            Sm.CmParam<Decimal>(ref cm, "@Level", decimal.Parse(TxtLevel.Text));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private bool IsProjectAlreadyNonActive()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select DocNo From TblProject " +
                    "Where DocNo=@DocNo And ActInd='N' ;"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This Document already non active.");
                ChkActInd.Checked = false;
                return true;
            }
            return false;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowProject(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowProject(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo, DocDt, ActInd, AcCode, profitcenterCode, AcDesc, ParentDocNo, StartDt, EndDt, Level From tblProject");
            SQL.AppendLine("Where DocNo = @DocNo; ");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo",

                        //1-5
                        "DocDt", "ActInd", "AcCode", "profitcenterCode", "AcDesc", 

                        //6-9
                        "ParentDocNo",  "StartDt", "EndDt", "Level"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        ChkActInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y");
                        TxtAcCode.EditValue = Sm.DrStr(dr, c[3]);
                        Sm.SetLue(LueProfitCenterCode, Sm.DrStr(dr, c[4]));
                        TxtAcDesc.EditValue = Sm.DrStr(dr, c[5]);
                        Sm.SetLue(LueParent, Sm.DrStr(dr, c[6]));
                        Sm.SetDte(DteStartDt, Sm.DrStr(dr, c[7]));
                        Sm.SetDte(DteEndDt, Sm.DrStr(dr, c[8]));
                        TxtLevel.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[9]), 0);
                    }, true
                );
        }

        #endregion

        #region Additional Method

        public static void SetLueParentCode(ref LookUpEdit Lue)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select * From ( ");
                SQL.AppendLine("    Select A.DocNo As Col1, Concat(A.AcCode, ' ', B.profitCenterName) As Col2  ");
                SQL.AppendLine("    From TblProject A  ");
                SQL.AppendLine("    Inner Join tblProfitCenter B On A.ProfitCenterCode = B.ProfitCenterCode  ");
                SQL.AppendLine("    Where ParentDocNo is null  ");
                SQL.AppendLine("    Union all  ");
                SQL.AppendLine("    Select DocNo, Concat(AcCode, ' ', AcDesc)  ");
                SQL.AppendLine("    From TblProject  ");
                SQL.AppendLine("    Where ParentDocNo is not null  ");
                SQL.AppendLine(")X Order By X.Col2 ; ");

                Sm.SetLue2(
                    ref Lue, SQL.ToString(),
                    0, 35, false, true, "Code", "Description", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        

        #endregion

        #region Event
        private void LueParent_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueParent, new Sm.RefreshLue1(SetLueParentCode));
            if (BtnSave.Enabled)
            {
                if (Sm.GetLue(LueParent).Length > 0)
                {
                    string DocNo = Sm.GetLue(LueParent);
                    Sm.SetDteCurrentDate(DteDocDt);
                    Sm.SetDte(DteStartDt, Sm.GetValue("Select StartDt From TblProject Where DocNo = '"+DocNo+"' "));
                    Sm.SetDte(DteEndDt, Sm.GetValue("Select EndDt From TblProject Where DocNo = '"+DocNo+"' "));
                }
            }
        }

        private void LueProfitCenterCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueProfitCenterCode, new Sm.RefreshLue1(Sl.SetLueProfitCenterCode));
        }

        private void TxtLevel_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtLevel, 0);
        }
        #endregion

        
    }
}
