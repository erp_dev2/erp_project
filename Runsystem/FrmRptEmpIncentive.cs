﻿#region Update
/*
    05/07/2020 [HAR/IMS] BUG nilai total work permit dan absent gak muncul, nilai jabatan bisa menampung lbh dari 1 allowance
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using DXE = DevExpress.XtraEditors;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptEmpIncentive : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = "", mAccessInd = "", mSQL = "";
        private string mADCodeFunctional = string.Empty;
        private string mADCodePositionalAllowance = string.Empty;

        #endregion

        #region Constructor

        public FrmRptEmpIncentive(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                Sl.SetLueDeptCode(ref LueDeptCode);
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -7);
                Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
                {
                    TxtProsentase
                }, 0);
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void SetSQL()
        {
            decimal effectiveDay = 0m;
            string dayPlus;
            string DateEnd = Sm.GetDte(DteDocDt2);
            string DateStart = Sm.GetDte(DteDocDt1);


            int Yr1 = Convert.ToInt32(Sm.Left(DateStart, 4));
            int Mth1 = Convert.ToInt32(DateStart.Substring(4, 2));
            int Day1 = Convert.ToInt32(Sm.Right(Sm.Left(DateStart, 4), 2));

            int Yr2 = Convert.ToInt32(Sm.Left(DateEnd, 4));
            int Mth2 = Convert.ToInt32(DateEnd.Substring(4, 2));
            int Day2 = Convert.ToInt32(Sm.Right(Sm.Left(DateEnd, 4), 2));

            DateTime date1 = new DateTime(Yr1, Mth1, Day1);
            DateTime date2 = new DateTime(Yr2, Mth2, Day2);

            int TotalMth = (((date2.Year - date1.Year) * 12) + (date2.Month - date1.Month)) ;

            var dat = new DateTime(Yr1, Mth1, Day1);

            for (int ctr = 0; ctr <= TotalMth; ctr++)
            {
                effectiveDay = effectiveDay + ShowDate(dat.AddMonths(ctr).ToString("yyyy"), dat.AddMonths(ctr).ToString("MM"));
            }

            var SQL = new StringBuilder();

            SQL.AppendLine("SET @EffectiveDay = '"+effectiveDay+"' ;");

            SQL.AppendLine("Select A.EmpCode, A.EmpName, A.EmpCodeOld, A.JoinDt, ");
            SQL.AppendLine("A.GrdlvlCode, B.GrdLvlName, A.CostGroup, A.DeptCode, C.DeptName,  ");
            SQL.AppendLine("A.PGCode, A.BankAcNo, A.PosCode, D.PosName, Ifnull(E.Dinas, 0) Dinas, Ifnull(F.CT, 0) CT, ");
            SQL.AppendLine("Ifnull(G.CP, 0) CP, Ifnull(H.CD, 0) CD, Ifnull(I.CH, 0) CH, Ifnull(J.JH, 0) JH, ");
            SQL.AppendLine("ifnull((ifnull(G.CP, 0)+ifnull(H.CD, 0)+ifnull(I.CH, 0)+ifnull(J.JH, 0)), 0) As Tdkhdr, @EffectiveDay As EffectiveDay, ifnull(B.BasicSalary, 0) As BasicSalary, ");
            SQL.AppendLine("ifnull(K.FuncTional, 0) FuncTional, ifnull(L.Jabatan, 0) Jabatan");
            SQL.AppendLine("From tblEmployee A ");
            SQL.AppendLine("Left Join TblGradeLevelhdr B On A.GrdlvlCode = B.GrdlvlCode ");
            SQL.AppendLine("Inner Join TblDepartment C On A.DeptCode = C.DeptCode ");
            SQL.AppendLine("Left Join TblPosition D On A.PosCode = D.PosCode ");
            SQL.AppendLine("Left Join  ");
            SQL.AppendLine("( ");
	        SQL.AppendLine("   Select A.EmpCode, Count(B.DocNo) As Dinas From tblEmpleavehdr A  ");
            SQL.AppendLine("   Inner Join TblEmpleaveDtl B On A.DocNo = b.DocNO  ");
            SQL.AppendLine("   Where FIND_in_SET(A.LeaveCode, (Select parvalue From tblparameter Where parCode = 'LeaveCodeOnDuties'))  ");
	        SQL.AppendLine("   And Left(B.LeaveDt, 6) in (   ");
            for (int ctr = 0; ctr <= TotalMth; ctr++)
            {
                dayPlus = dat.AddMonths(ctr).ToString("yyyyMM");
                
                if (ctr == TotalMth)
                    SQL.AppendLine("'" + dayPlus + "' ");
                else
                    SQL.AppendLine(" '" + dayPlus + "', ");

            }
            SQL.AppendLine( ") ");
            SQL.AppendLine("   And A.cancelInd = 'N' And A.Status = 'A'  ");
            SQL.AppendLine("   Group By A.EmpCode  ");
            SQL.AppendLine(")E On A.EmpCode = E.EmpCode ");
            SQL.AppendLine("Left Join  ");
            SQL.AppendLine("( ");
	        SQL.AppendLine("    Select A.EmpCode, Count(B.DocNo) As CT From tblEmpleavehdr A  ");
            SQL.AppendLine("   Inner Join TblEmpleaveDtl B On A.DocNo = b.DocNO  ");
            SQL.AppendLine("   Where A.LeaveCode in  (Select parvalue From tblparameter Where parCode = 'AnnualLeaveCode')  ");
            SQL.AppendLine("   And Left(B.LeaveDt, 6) in (   ");
            for (int ctr = 0; ctr <= TotalMth; ctr++)
            {
                dayPlus = dat.AddMonths(ctr).ToString("yyyyMM");
                if (ctr == TotalMth)
                    SQL.AppendLine("'" + dayPlus + "' ");
                else
                    SQL.AppendLine(" '" + dayPlus + "', ");
            }
            SQL.AppendLine(") ");
            SQL.AppendLine("    And A.cancelInd = 'N' And A.Status = 'A'  ");
            SQL.AppendLine("   Group By A.EmpCode  ");
            SQL.AppendLine(")F On A.EmpCode = F.EmpCode ");
            SQL.AppendLine("Left Join   ");
            SQL.AppendLine("(  ");
            SQL.AppendLine("    Select EmpCode, Count(B.DocNo) As CP From tblEmpleavehdr A  ");
            SQL.AppendLine("    Inner Join TblEmpleaveDtl B On A.DocNo = b.DocNO  ");
            SQL.AppendLine("    Where FIND_in_SET(A.LeaveCode, (Select parvalue From tblparameter Where parCode = 'LeaveCodeUrgentPerformances'))  ");
            SQL.AppendLine("   And Left(B.LeaveDt, 6) in (   ");
            for (int ctr = 0; ctr <= TotalMth; ctr++)
            {
                dayPlus = dat.AddMonths(ctr).ToString("yyyyMM");
                if (ctr == TotalMth)
                    SQL.AppendLine("'" + dayPlus + "' ");
                else
                    SQL.AppendLine(" '" + dayPlus + "', ");
            }
            SQL.AppendLine(") ");
            SQL.AppendLine("    And A.cancelInd = 'N' And A.Status = 'A'  ");
            SQL.AppendLine("    Group By A.EmpCode  ");
            SQL.AppendLine(")G On A.EmpCode = G.EmpCode ");
            SQL.AppendLine("Left Join   ");
            SQL.AppendLine("(  ");
            SQL.AppendLine("    Select EmpCode, Count(B.DocNo) As CD From tblEmpleavehdr A  ");
            SQL.AppendLine("    Inner Join TblEmpleaveDtl B On A.DocNo = b.DocNO  ");
            SQL.AppendLine("    Where FIND_in_SET(A.LeaveCode, (Select parvalue From tblparameter Where parCode = 'LeaveCodeHospitalPerformances'))  ");
            SQL.AppendLine("   And Left(B.LeaveDt, 6) in (   ");
            for (int ctr = 0; ctr <= TotalMth; ctr++)
            {
                dayPlus = dat.AddMonths(ctr).ToString("yyyyMM");
                if (ctr == TotalMth)
                    SQL.AppendLine("'" + dayPlus + "' ");
                else
                    SQL.AppendLine(" '" + dayPlus + "', ");
            }
            SQL.AppendLine(") ");
            SQL.AppendLine("    And A.cancelInd = 'N' And A.Status = 'A'  ");
            SQL.AppendLine("    Group By A.EmpCode  ");
            SQL.AppendLine(")H On A.EmpCode = H.EmpCode   ");
            SQL.AppendLine("Left Join  ");
            SQL.AppendLine("(  ");
            SQL.AppendLine("    Select EmpCode, Count(B.DocNo) As CH From tblEmpleavehdr A  ");
            SQL.AppendLine("    Inner Join TblEmpleaveDtl B On A.DocNo = b.DocNO  ");
            SQL.AppendLine("    Where FIND_in_SET(A.LeaveCode, (Select parvalue From tblparameter Where parCode = 'LeaveCodePregnants'))  ");
            SQL.AppendLine("   And Left(B.LeaveDt, 6) in (   ");
            for (int ctr = 0; ctr <= TotalMth; ctr++)
            {
                dayPlus = dat.AddMonths(ctr).ToString("yyyyMM");
                if (ctr == TotalMth)
                    SQL.AppendLine("'" + dayPlus + "' ");
                else
                    SQL.AppendLine(" '" + dayPlus + "', ");
            }
            SQL.AppendLine(") ");
            SQL.AppendLine("    And A.cancelInd = 'N' And A.Status = 'A'  ");
            SQL.AppendLine("    Group By A.EmpCode  ");
            SQL.AppendLine(")I On A.EmpCode = I.EmpCode  ");
            SQL.AppendLine("Left Join   ");
            SQL.AppendLine("(   ");
            SQL.AppendLine("    Select EmpCode, Count(B.DocNo) As JH From tblEmpleavehdr A   ");
            SQL.AppendLine("    Inner Join TblEmpleaveDtl B On A.DocNo = b.DocNO   ");
            SQL.AppendLine("    Where FIND_in_SET(A.LeaveCode, (Select parvalue From tblparameter Where parCode = 'WastedTimeLeaveCode'))   ");
	        SQL.AppendLine("   And Left(B.LeaveDt, 6) in (   ");
            for (int ctr = 0; ctr <= TotalMth; ctr++)
            {
                dayPlus = dat.AddMonths(ctr).ToString("yyyyMM");
                if (ctr == TotalMth)
                    SQL.AppendLine("'" + dayPlus + "' ");
                else
                    SQL.AppendLine(" '" + dayPlus + "', ");
            }
            SQL.AppendLine( ") ");
            SQL.AppendLine("    And A.cancelInd = 'N' And A.Status = 'A'   ");
            SQL.AppendLine("    Group By A.EmpCode   ");
            SQL.AppendLine(")J On A.EmpCode = J.EmpCode   ");
            SQL.AppendLine("Left Join   ");
            SQL.AppendLine("(  ");
	        SQL.AppendLine("    Select distinct A.EmpCode, ifnUll(B.Amt, C.Amt) As Functional  ");
	        SQL.AppendLine("    From tblEmployee A  ");
            SQL.AppendLine("    Inner Join TblGradelevelDtl B ON A.GrdlvlCode = B.GrdLvlCode And B.AdCode = @ADCodeFunctional  ");
	        SQL.AppendLine("    Left Join TblEmployeeAllowanceDeduction C On A.EmpCode = C.EmpCode And B.AdCode = @ADCodeFunctional  ");
            SQL.AppendLine(")K On A.EmpCode = K.EmpCode  ");
            SQL.AppendLine("Left Join   ");
            SQL.AppendLine("(  ");
	        SQL.AppendLine("    Select distinct A.EmpCode, SUM(ifnUll(B.Amt, C.Amt)) As jabatan  ");
	        SQL.AppendLine("    From tblEmployee A  ");
	        SQL.AppendLine("    Inner Join TblGradelevelDtl B ON A.GrdlvlCode = B.GrdLvlCode ");
            //SQL.AppendLine("    And B.AdCode = @ADCodePositionalAllowance  ");
            SQL.AppendLine("    And FIND_in_SET(B.AdCode, (Select parvalue From tblparameter Where parCode = 'ADCodePositionalAllowance'))  ");
	        SQL.AppendLine("    Left Join TblEmployeeAllowanceDeduction C On A.EmpCode = C.EmpCode ");
            //SQL.AppendLine("    And B.AdCode = @ADCodePositionalAllowance  ");
            SQL.AppendLine("    And FIND_in_SET(B.AdCode, (Select parvalue From tblparameter Where parCode = 'ADCodePositionalAllowance'))  ");
            SQL.AppendLine("    GROUP By A.EmpCode ");
            SQL.AppendLine(")L On A.EmpCode = L.EmpCode  ");
            SQL.AppendLine("Where (A.ResignDt Is Not Null And A.ResignDt>='" + Sm.GetDte(DteDocDt2).Substring(0, 8) + "' ) ");
            SQL.AppendLine("Or A.ResignDt Is Null ");


            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 25;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",
                        //1-5
                        "Employee's"+Environment.NewLine+"Code", 
                        "Employee's"+Environment.NewLine+"Name",
                        "Old Code", 
                        "Join Date",
                        "Grade",
                        //6-10
                        "Cost Group",
                        "Department",
                        "Payroll"+Environment.NewLine+"Group",
                        "Bank"+Environment.NewLine+"Account",
                        "Position",
                        //11-15
                        "on Duty"+Environment.NewLine+"Leave",
                        "Yearly"+Environment.NewLine+"Leave",
                        "Important"+Environment.NewLine+"Leave",
                        "Hospitality"+Environment.NewLine+"Leave",
                        "Pregnant"+Environment.NewLine+"Leave",
                        //16-20
                        "Off day",
                        "Absent",
                        "Effective"+Environment.NewLine+"Day",
                        "Salary",
                        "Allowance"+Environment.NewLine+"(jabatan)",
                        //21-24
                        "Allowance"+Environment.NewLine+"(functional)",
                        "Prosentase",
                        "Total",
                        "Work period"

                    },
                    new int[] 
                    {
                        //0
                        50,
                        //1-5
                        80, 200, 80, 120, 150,
                        //6-10
                        100, 150, 120, 120, 120,
                        //11-15
                        100, 100, 100, 100, 100, 
                        //16-20
                        100, 100, 100, 150, 150, 
                        //21-24
                        150, 80, 150, 120
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 4 });
            Sm.GrdFormatDec(Grd1, new int[] { 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] {  }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] {  }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)||
                Sm.IsTxtEmpty(TxtProsentase, "Prosentase", true)
                ) return;
            try
            {
                SetSQL();

                Cursor.Current = Cursors.WaitCursor;
                string Filter = " And 0=0 ";

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.CmParam<String>(ref cm, "@ADCodeFunctional", mADCodeFunctional);
                Sm.CmParam<String>(ref cm, "@ADCodePositionalAllowance", mADCodePositionalAllowance);

                Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "A.EmpCode", "A.EmpCodeOld", "A.EmpName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "A.DeptCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, 
                         mSQL + Filter + "Order By A.EmpName " ,
                        new string[]
                        {
                            //0
                            "EmpCode", 
                            //1-5
                            "EmpName", "EmpCodeOld", "JoinDt", "GrdLvlname", "CostGroup", 
                            //6-9
                            "DeptName", "PGCode", "BankAcNo", "PosName", "Dinas",
                            //10-15
                            "CT", "CP", "CD", "CH", "JH",
                            //16-20
                            "Tdkhdr", "EffectiveDay", "BasicSalary", "jabatan","Functional"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);

                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);

                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 15);

                            Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 16);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 17);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 18);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 19);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 20);

                        }, true, false, false, false
                    );

                ComputeAmount();
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24 });

            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #region additional Method

        private void GetParameter()
        {
            mADCodeFunctional = Sm.GetParameter("ADCodeFunctional");
            mADCodePositionalAllowance = Sm.GetParameter("ADCodePositionalAllowance");
        }

        private void ComputeAmount()
        {
            decimal salary, Functional, Positional = 0m;
            if (Grd1.Rows.Count > 1)
            {
                for (int i = 0; i < Grd1.Rows.Count; i++)
                {
                    if (Sm.GetGrdStr(Grd1, i, 1).Length > 0)
                    {
                        salary = Sm.GetGrdDec(Grd1, i, 19);
                        Functional = Sm.GetGrdDec(Grd1, i, 20);
                        Positional = Sm.GetGrdDec(Grd1, i, 21);
                        Grd1.Cells[i, 22].Value = Decimal.Parse(TxtProsentase.Text);
                        Grd1.Cells[i, 23].Value = (salary + Functional + Positional) * (0.01m * Decimal.Parse(TxtProsentase.Text));
                        Grd1.Cells[i, 24].Value = ComputeMonthBetweenTwoDate(Sm.GetGrdDate(Grd1, i, 4), Sm.GetDte(DteDocDt2).Substring(0, 8));
                    }
                }
            }
        }

        private decimal ComputeMonthBetweenTwoDate(string StartDt, string EndDt)
        {
            string DateEnd = EndDt;
            string DateStart = StartDt;


            int Yr1 = Convert.ToInt32(Sm.Left(DateStart, 4));
            int Mth1 = Convert.ToInt32(DateStart.Substring(4, 2));
            int Day1 = Convert.ToInt32(Sm.Right(Sm.Left(DateStart, 6), 2));

            int Yr2 = Convert.ToInt32(Sm.Left(DateEnd, 4));
            int Mth2 = Convert.ToInt32(DateEnd.Substring(4, 2));
            int Day2 = Convert.ToInt32(Sm.Right(Sm.Left(DateEnd, 6), 2));

            DateTime date1 = new DateTime(Yr1, Mth1, Day1);
            DateTime date2 = new DateTime(Yr2, Mth2, Day2);

            int TotalMth = (((date2.Year - date1.Year) * 12) + (date2.Month - date1.Month)) + 1;

            return TotalMth;
        }

        private decimal ShowDate(string getYr, string getMth)
        {
            var lHoliday = new List<DateHoliday>();
            ProcessHoliday(ref lHoliday, getYr, getMth);

            string Yr = getMth == "01" ? Convert.ToString(Decimal.Parse(getYr) - 1) : getYr;
            string Mth = getMth == "01" ? "12" : Sm.Right(string.Concat("00", Convert.ToString(Decimal.Parse(getMth) - 1)), 2);
            var DtMin = String.Concat(Yr, Mth, "01");
            var DtMax = String.Concat(Yr, Mth,
                Sm.Right(String.Concat("00", DateTime.DaysInMonth(Convert.ToInt32(Yr), Convert.ToInt32(Mth))), 2));
            decimal DayWeek = 0m;
            decimal EffectiveDay = 0m;


            if (DtMin.Length >= 8 &&
                DtMax.Length >= 8 &&
                decimal.Parse(DtMin) <= decimal.Parse(DtMax))
            {
                DateTime Dt1 = new DateTime(
                       Int32.Parse(DtMin.Substring(0, 4)),
                       Int32.Parse(DtMin.Substring(4, 2)),
                       Int32.Parse(DtMin.Substring(6, 2)),
                       0, 0, 0);

                DateTime Dt2 = new DateTime(
                    Int32.Parse(DtMax.Substring(0, 4)),
                    Int32.Parse(DtMax.Substring(4, 2)),
                    Int32.Parse(DtMax.Substring(6, 2)),
                    0, 0, 0);

                DateTime TempDt = Dt1;

                var TotalDays = (Dt2 - Dt1).Days + 1;
                EffectiveDay = TotalDays;

                var s = new List<string>();

                s.Add(
                    Dt1.Year.ToString() +
                    ("00" + Dt1.Month.ToString()).Substring(("00" + Dt1.Month.ToString()).Length - 2, 2) +
                    ("00" + Dt1.Day.ToString()).Substring(("00" + Dt1.Day.ToString()).Length - 2, 2)
                );

                for (int i = 0; i < TotalDays; i++)
                {
                    Dt1 = Dt1.AddDays(1);
                    s.Add(
                        Dt1.Year.ToString() +
                        ("00" + Dt1.Month.ToString()).Substring(("00" + Dt1.Month.ToString()).Length - 2, 2) +
                        ("00" + Dt1.Day.ToString()).Substring(("00" + Dt1.Day.ToString()).Length - 2, 2)
                    );
                }

                s.ForEach(i =>
                {
                    if (lHoliday.Count() > 1)
                    {
                        lHoliday.ForEach(x =>
                        {
                            if (TempDt.ToString("yyyyMMdd") == x.HolDt)
                            {
                                if ((int)TempDt.DayOfWeek != 0 || (int)TempDt.DayOfWeek != 6)
                                {
                                    DayWeek += 1;
                                }
                            }
                        });
                    }

                    if ((int)TempDt.DayOfWeek == 0 || (int)TempDt.DayOfWeek == 6)
                    {
                        DayWeek += 1;
                    }

                    TempDt = TempDt.AddDays(1);
                });

            }

            EffectiveDay = EffectiveDay - DayWeek;
            return EffectiveDay;
        }

        private void ProcessHoliday(ref List<DateHoliday> l, string getYr, string getMth)
        {
            l.Clear();

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            string Yr = getMth == "01" ? Convert.ToString(Decimal.Parse(getYr) - 1) : getYr;
            string Mth = getMth == "01" ? "12" : Sm.Right(string.Concat("00", Convert.ToString(Decimal.Parse(getMth) - 1)), 2);
            var DtMin = String.Concat(Yr, Mth, "01");
            var DtMax = String.Concat(Yr, Mth,
                Sm.Right(String.Concat("00", DateTime.DaysInMonth(Convert.ToInt32(Yr), Convert.ToInt32(Mth))), 2));


            int Yr1 = int.Parse(Sm.Left(DtMin, 4));
            int Yr2 = int.Parse(Sm.Left(DtMax, 4));

            SQL.AppendLine("Select Distinct HolDt From ( ");
            SQL.AppendLine("Select HolDt From TblHoliday ");
            SQL.AppendLine("Where HolDt Between @StartDt And @EndDt ");
            for (int i = Yr1; i <= Yr2; i++)
            {
                SQL.AppendLine("Union All ");
                SQL.AppendLine("Select Concat(@Yr" + i.ToString() + ", Right(HolDt, 4)) As HolDt From TblHoliday ");
                SQL.AppendLine("Where RoutineInd='Y' ");
                SQL.AppendLine("And HolDt<=@EndDt ");
                SQL.AppendLine("And Concat(@Yr" + i.ToString() + ", Right(HolDt, 4)) Between @StartDt And @EndDt  ");

                Sm.CmParam<string>(ref cm, "@Yr" + i.ToString(), i.ToString());
            }
            SQL.AppendLine(") T Order By HolDt;");
            Sm.CmParamDt(ref cm, "@StartDt", DtMin);
            Sm.CmParamDt(ref cm, "@EndDt", DtMax);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "HolDt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new DateHoliday()
                        {
                            HolDt = Sm.DrStr(dr, c[0])
                        });
                    }
                }
                dr.Close();
            }

            //return totalWday = TotalDays - DayWeek;
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event
        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void TxtProsentase_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtProsentase, 0);
        }

        #endregion

        #region Class

        private class DateHoliday
        {
            public string HolDt { get; set; }
        }

        private class Period
        {
            public string YrMth { get; set; }
        }
        #endregion 

        

    }
}
