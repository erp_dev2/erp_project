﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmAssesment : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmAssesmentFind FrmFind;

        #endregion

        #region Constructor

        public FrmAssesment(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            SetFormControl(mState.View);

            SetGrd();
            base.FrmLoad(sender, e);
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 2;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[]{ "DNo", "Description" },
                    new int[]{ 0, 600 }
                );
            Sm.GrdColReadOnly(Grd1, new int[] { 0 });
            Sm.GrdColInvisible(Grd1, new int[] { 0 }, false);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtAsmCode, TxtAsmName, MeeRemark
                    }, true);
                    Grd1.ReadOnly = true;
                    TxtAsmCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtAsmCode, TxtAsmName, MeeRemark
                    }, false);
                    Grd1.ReadOnly = false;
                    TxtAsmCode.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtAsmCode, TxtAsmName, MeeRemark
            });
            Sm.FocusGrd(Grd1, 0, 0);
            Sm.ClearGrd(Grd1, true);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmAssesmentFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;

                Cursor.Current = Cursors.WaitCursor;
                var cml = new List<MySqlCommand>();

                cml.Add(SaveAssesmentHdr());

                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0)
                        cml.Add(SaveAssesmentDtl(Row));

                Sm.ExecCommands(cml);

                ShowData(TxtAsmCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdEnter(Grd1, e);
            Sm.GrdTabInLastCell(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && e.ColIndex == 1) Sm.GrdRequestEdit(Grd1, e.RowIndex);
        }


        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (e.ColIndex == 1 && Sm.GetGrdStr(Grd1, e.RowIndex, e.ColIndex).Length == 0)
                Grd1.Cells[e.RowIndex, e.ColIndex].Value = Sm.GetGrdStr(Grd1, e.RowIndex, e.ColIndex).Trim();
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtAsmCode, "Assesment code", false) ||
                Sm.IsTxtEmpty(TxtAsmName, "Assesment name", false) ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                IsGrdExceedMaxRecords();
        }


        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 data in list.");
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                if (Sm.IsGrdValueEmpty(Grd1, Row, 1, false, "Description is empty.")) return true;
            return false;
        }

        private MySqlCommand SaveAssesmentHdr()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblAssesmentHdr(AsmCode, AsmName, Remark, CreateBy, CreateDt) " +
                    "Values (@AsmCode, @AsmName, @Remark, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@AsmCode", TxtAsmCode.Text);
            Sm.CmParam<String>(ref cm, "@AsmName", TxtAsmName.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveAssesmentDtl(int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblAssesmentDtl(AsmCode, DNo, Description, CreateBy, CreateDt) " +
                    "Values(@AsmCode, @DNo, @Description, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@AsmCode", TxtAsmCode.Text);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("000" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@Description", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowAssesmentHdr(DocNo);
                ShowAssesmentDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }

        }

        private void ShowAssesmentHdr(string AsmCode)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@AsmCode", AsmCode);

            Sm.ShowDataInCtrl(
                        ref cm,
                        "Select AsmCode, AsmName, Remark " +
                        "From TblAssesmentHdr Where AsmCode=@AsmCode",
                        new string[] 
                        {
                            "AsmCode", "AsmName", "Remark"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtAsmCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtAsmName.EditValue = Sm.DrStr(dr, c[1]);
                            MeeRemark.EditValue = Sm.DrStr(dr, c[2]);
                        }, true
                     );
        }
        private void ShowAssesmentDtl(string AsmCode)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@AsmCode", AsmCode);
            Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    "Select DNo, Description " +
                    "From TblAssesmentDtl Where AsmCode=@AsmCode Order By DNo ",
                    new string[] 
                    { 
                       "DNo", "Description"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 1);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #endregion 

        #region Event

        #region Misc Control Event

        private void TxtAsmCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtAsmCode);
        }

        private void TxtAsmName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtAsmName);
        }

        #endregion

        #endregion
    }
}
