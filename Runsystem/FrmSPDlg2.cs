﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmSPDlg2 : RunSystem.FrmBase4
    {
        #region Field

        private FrmSP mFrmParent;
        private string mSQL = string.Empty;
        private string mCtCode = string.Empty;
        private string mCode = string.Empty;

        #endregion

        #region Constructor

        public FrmSPDlg2(FrmSP FrmParent, string CtCode, string  Code)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mCtCode = CtCode;
            mCode = Code;
        }

        #endregion

        #region Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);

                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.Name, A.Address, B.CityName, C.CntName, A.Phone, A.EmAil ");
            SQL.AppendLine("from tblcustomershipaddress A ");
            SQL.AppendLine("Inner join TblCity B on A.CityCode = B.CityCode ");
            SQL.AppendLine("Left Join TblCountry C On A.CntCode = C.CntCode   ");
            SQL.AppendLine("Where A.CtCode=@CtCode ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 7;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Name", 
                        "Address",
                        "City Name",
                        "Country",
                        "Phone",

                        //6-10
                        "Email",
                    },
                    new int[] 
                    {
                        //0
                        30,

                        //1-5
                        180, 250, 120, 100, 80, 
                        
                        //6-10
                        120
                    }
                );
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 6});
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] {  }, !ChkHideInfoInGrd.Checked);
        }


        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@CtCode", mCtCode);
                Sm.FilterStr(ref Filter, ref cm, TxtName.Text, "A.Name", false);
                Sm.FilterStr(ref Filter, ref cm, TxtAddress.Text, "A.Address", false);


                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.Name;",
                        new string[]
                        { 
                            //0
                            "Name",

                            //1-5
                            "Address", "CityName", "CntName", "Phone", "Email",
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                if (Grd1.Rows.Count <= 1)
                    Sm.FocusGrd(Grd1, 0, 0);
                else
                    Sm.FocusGrd(Grd1, 1, 2);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 0))
            {
                if (mCode == "1")
                {
                    mFrmParent.TxtNameDelivery.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
                    mFrmParent.MeeDelivery.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 2);
                    this.Hide();
                }
                else
                {
                    mFrmParent.TxtNameReceipt.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
                    mFrmParent.MeeReceipt.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 2);
                    this.Hide();
                }
            }
        }

        #region Grid Method

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        private void ChkName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Shipping Name#");
        }

        private void ChkAddress_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Shipping Address#");
        }

        private void TxtName_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void TxtAddress_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void Grd1_CellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }
        #endregion
        
    }
}
