﻿ #region Update
/*
    13/03/2018 [TKG] tambah import indicator dari PO
    08/04/2018 [TKG] tambah remark
    17/03/2019 [TKG] tambah informasi kawasan berikat
    17/03/2019 [TKG] tambah informasi kawasan berikat (non document indicator, packaging, packaging quantity
    15/04/2019 [TKG] tambah nomor pengajuan berikat (KBSubmissionNo)
    04/11/2019 [DITA/IMS] tambah informasi Specifications
    28/11/2019 [WED/IMS] tambah kolom project code, project name, customer PO#
    28/01/2020 [WED/YK] receiving yg bukan auto DO tidak bisa di find disini
    26/05/2020 [HAR/ALL] tambah button action berdasarkan parmeter :  mIsButtonUsageHistoryActived
    03/09/2020 [WED/IMS] Project Code dan Project Name ganti link query
    11/03/2021 [TKG/IMS] menambah heat number
    31/03/2021 [VIN/IMS] project code ambil dari MR
    06/01/2022 [ISD/PHT] tambah filter untuk department berdasarkan parameter IsFilterByDept
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRecvVdFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmRecvVd mFrmParent;
        private string mSQL = string.Empty;
        private bool mIsInventoryShowTotalQty = false;

        #endregion

        #region Constructor

        public FrmRecvVdFind(FrmRecvVd FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                Sl.SetLueWhsCode(ref LueWhsCode, string.Empty);
                Sl.SetLueVdCode(ref LueVdCode);
                ChkExcludedCancelledItem.Checked = true;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mFrmParent.mIsShowForeignName = Sm.GetParameterBoo("IsShowForeignName");
            mIsInventoryShowTotalQty = Sm.GetParameterBoo("IsInventoryShowTotalQty");
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, B.CancelInd, ");
            SQL.AppendLine("(Case B.Status ");
            SQL.AppendLine("    When 'O' Then 'Outstanding' ");
            SQL.AppendLine("    When 'A' Then 'Approved' ");
            SQL.AppendLine("    When 'C' Then 'Cancelled' ");
            SQL.AppendLine("Else '' End) As StatusDesc, ");
            SQL.AppendLine("E.WhsName, D.VdName, A.VdDONo, B.PODocNo, G.ImportInd, ");
            SQL.AppendLine("B.ItCode, C.ItCodeInternal, C.ItName, C.ForeignName, B.BatchNo, B.Lot, B.Bin, ");
            SQL.AppendLine("B.QtyPurchase, C.PurchaseUomCode, B.Qty, C.InventoryUomCode, B.Qty2, C.InventoryUomCode2, B.Qty3, C.InventoryUomCode3, ");
            SQL.AppendLine("A.Remark As RemarkH, B.Remark As RemarkD, A.CreateBy, A.CreateDt, B.LastUpBy, B.LastUpDt, F.ItGrpName, B.DNo, A.LocalDocNo, C.Specification, ");
            SQL.AppendLine("A.KBPackaging, A.KBPackagingQty, A.KBContractNo, A.KBContractDt, A.KBPLNo, A.KBPLDt, A.KBRegistrationNo, A.KBRegistrationDt, A.KBNonDocInd, A.KBSubmissionNo, A.CustomsDocCode ");
            if (mFrmParent.mIsBOMShowSpecifications)
                SQL.AppendLine(", L.PGCode ProjectCode, L.PGName ProjectName, K.PONo ");
            else
                SQL.AppendLine(", Null As projectCode, Null AS ProjectName, Null As PONo ");
            if (mFrmParent.mIsRecvVdHeatNumberEnabled)
                SQL.AppendLine(", B.HeatNumber ");
            else
                SQL.AppendLine(", Null As HeatNumber ");

            SQL.AppendLine("From TblRecvVdHdr A ");
            SQL.AppendLine("Inner Join TblRecvVdDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblItem C On B.ItCode=C.ItCode ");
            SQL.AppendLine("Left Join TblVendor D On A.VdCode=D.VdCode ");
            SQL.AppendLine("Inner Join TblWarehouse E On A.WhsCode=E.WhsCode ");
            SQL.AppendLine("    And Exists( ");
            SQL.AppendLine("        Select 1 From TblGroupWarehouse ");
            SQL.AppendLine("        Where WhsCode=E.WhsCode ");
            SQL.AppendLine("        And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("Left Join TblItemGroup F On C.ItGrpCode=F.ItGrpCode ");
            SQL.AppendLine("Inner Join TblPOHdr G On B.PODocNo=G.DocNo ");
            SQL.AppendLine("Inner Join TblPODtl H On B.PODocNo=H.DocNo And B.PODNo=H.DNo ");
            SQL.AppendLine("Inner Join TblPORequestDtl I On H.PORequestDocNo=I.DocNo And H.PORequestDNo=I.DNo ");
            SQL.AppendLine("Inner Join TblMaterialRequestHdr J On I.MaterialRequestDocNo=J.DocNo ");
            if (mFrmParent.mIsFilterBySite)
            {
                SQL.AppendLine("    And (J.SiteCode Is Null Or ( ");
                SQL.AppendLine("    J.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(J.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("            ) ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine(")) ");
            }
            if (mFrmParent.mIsFilterByDept)
            {
                SQL.AppendLine("    And (J.DeptCode Is Null Or ( ");
                SQL.AppendLine("    J.DeptCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupDepartment ");
                SQL.AppendLine("    Where DeptCode=IfNull(J.DeptCode, '') ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(")) ");
            }
            if (mFrmParent.mIsBOMShowSpecifications)
            {
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select T32.DocNo, T32.DNo, Group_Concat(Distinct IfNull(T7.ProjectCode, T4.ProjectCode2)) ProjectCode, ");
                SQL.AppendLine("    Group_Concat(Distinct IfNull(T7.ProjectName, T6.ProjectName)) ProjectName, ");
                SQL.AppendLine("    Group_Concat(Distinct IfNull(T4.PONo, '')) PONo ");
                SQL.AppendLine("    From TblMaterialRequestHdr T1 ");
                SQL.AppendLine("    Inner join TblMaterialRequestDtl T2 On T1.DocNo = T2.DocNo ");
                SQL.AppendLine("        And T1.SOCDocNo Is not Null ");
                SQL.AppendLine("    Inner join TblPORequestDtl T3 On T2.DocNo = T3.MaterialRequestDocNo ");
                SQL.AppendLine("        And T2.DNo = T3.MaterialRequestDNo ");
                SQL.AppendLine("    Inner join TblPODtl T31 On T31.PORequestDocNo = T3.DocNo And T31.PORequestDNo = T3.DNo ");
                SQL.AppendLine("    Inner Join TblRecvVdDtl T32 On T32.PODocNo = T31.DocNo And T32.PODNo = T31.DNo ");
                SQL.AppendLine("    Inner Join TblSOContractHdr T4 On T1.SOCDocNo = T4.DocNo ");
                SQL.AppendLine("    Inner Join TblBOQHdr T5 On T4.BOQDocNo = T5.DocNo ");
                SQL.AppendLine("    Inner Join TblLOPHdr T6 On T5.LOPDocNo = T6.DocNo ");
                SQL.AppendLine("    Left Join TblProjectGroup T7 On T6.PGCode = T7.PGCode ");
                SQL.AppendLine("    Group By T3.DocNo, T3.DNo ");
                SQL.AppendLine(") K On B.DocNo = K.DocNo And B.DNo = K.DNo ");
                SQL.AppendLine("Left Join TblProjectGroup L On J.PGCode = L.PGCode ");
            }
            SQL.AppendLine("Where A.POInd='Y' ");
            SQL.AppendLine("And A.DocDt Between @DocDt1 And @DocDt2 ");
            if (mFrmParent.mIsFilterBySite)
            {
                SQL.AppendLine("    And (A.SiteCode Is Null Or ( ");
                SQL.AppendLine("    A.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(A.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("            ) ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine(")) ");
            }
            if (mFrmParent.mIsFilterByDept)
            {
                SQL.AppendLine("    And (A.DeptCode Is Null Or ( ");
                SQL.AppendLine("    A.DeptCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupDepartment ");
                SQL.AppendLine("    Where DeptCode=IfNull(A.DeptCode, '') ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(")) ");
            }
            if (mFrmParent.mMenuCodeForRecvVdAutoCreateDO)
                SQL.AppendLine("And A.DocNo In (Select Distinct RecvVdDocNo From TblDODeptHdr Where RecvVdDocNo Is Not Null) ");
            else
                SQL.AppendLine("And A.DocNo Not In (Select Distinct RecvVdDocNo From TblDODeptHdr Where RecvVdDocNo Is Not Null) ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 53;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#", 
                        "Date",
                        "Cancel",
                        "Status",
                        "",
                        
                        //6-10
                        "Warehouse",
                        "Vendor",
                        "DO#", 
                        "PO#",
                        "Import",
                        
                        //11-15
                        "Item's Code",
                        "Item's Name",
                        "Local Code",
                        "Batch#",
                        "Lot",
                        
                        //16-20
                        "Bin",
                        "Quantity"+Environment.NewLine+"(Purchase)",
                        "UoM"+Environment.NewLine+"(Purchase)",
                        "Quantity"+Environment.NewLine+"(Inventory)",
                        "UoM"+Environment.NewLine+"(Inventory)",
                        
                        //21-25
                        "Quantity"+Environment.NewLine+"(Inventory 2)",
                        "UoM"+Environment.NewLine+"(Inventory 2)",
                        "Quantity"+Environment.NewLine+"(Inventory 3)",
                        "UoM"+Environment.NewLine+"(Inventory 3)",
                        "Document's Remark",

                        //26-30
                        "Item's Remark",
                        "Created By",
                        "Created Date", 
                        "Created Time", 
                        "Last Updated By", 
                        
                        //31-35
                        "Last Updated Date", 
                        "Last Updated Time",
                        "Group",
                        "DNo",
                        "Foreign Name",

                        //36-40
                        "Local Document",
                        "Packaging",
                        "Packaging Quantity",
                        "Contract#",
                        "Contract Date",

                        //41-45
                        "Packing List#",
                        "Packing List Date",
                        "Registration#",
                        "Registration Date",
                        "Submission#",

                        //46-50
                        "Non Document",
                        "Customs Document Code", 
                        "Specification",
                        "Project Code",
                        "Project Name",

                        //51-54
                        "Customer PO#",
                        "Heat Number"
                        
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        180, 80, 50, 80, 20, 
                        
                        //6-10
                        200, 200, 100, 130, 60, 
                        
                        //11-15
                        100, 250, 100, 200, 60, 
                        
                        //16-20
                        60, 80, 80, 80, 80, 
                        
                        //21-25
                        80, 80, 80, 80, 250, 
                        
                        //26-30
                        250, 120, 120, 120, 120, 
                        
                        //31-35
                        120, 120, 150, 0, 230, 
                        
                        //36-40
                        140, 150, 130, 150, 120, 

                        //41-45
                        150, 120, 150, 120, 150, 
                        
                        //46-50
                        100, 180, 300, 100, 200,

                        //51-52
                        120, 200
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 3, 10, 46 });
            Sm.GrdColButton(Grd1, new int[] { 5 });
            Sm.GrdFormatDec(Grd1, new int[] { 17, 19, 21, 23, 38 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2, 28, 31, 40, 42, 44 });
            Sm.GrdFormatTime(Grd1, new int[] { 29, 32 });

            Grd1.Cols[36].Move(2);
            if (mFrmParent.mIsShowForeignName)
            {
                Grd1.Cols[35].Move(15);
                Sm.GrdColInvisible(Grd1, new int[] { 2, 8, 11, 15, 16, 19, 21, 22, 23, 24, 27, 28, 29, 30, 31, 32, 33, 34, 36 }, false);
            } 
            else
                Sm.GrdColInvisible(Grd1, new int[] { 2, 8, 11, 15, 16, 19, 21, 22, 23, 24, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36 }, false);
            if (!mFrmParent.mIsKawasanBerikatEnabled) Sm.GrdColInvisible(Grd1, new int[] { 37, 38, 39, 40, 41, 42, 43, 44, 45, 46 }, false);
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 52 });
            ShowInventoryUomCode();
            if (!mFrmParent.mIsRecvVdNeedApproval)
                Sm.GrdColInvisible(Grd1, new int[] { 4, 5 }, false);
            if (mFrmParent.mIsItGrpCodeShow)
            {
                Grd1.Cols[33].Visible = true;
                Grd1.Cols[33].Move(16);
            }
            if (!mFrmParent.mIsBOMShowSpecifications) Sm.GrdColInvisible(Grd1, new int[] { 48, 49, 50, 51 });
            Grd1.Cols[48].Move(27);
            if (mFrmParent.mIsRecvVdHeatNumberEnabled)
                Grd1.Cols[52].Move(16);
            else
                Grd1.Cols[52].Visible = false;

            Sm.SetGrdProperty(Grd1 , false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 8, 11, 15, 16, 27, 28, 29, 30, 31, 32, 36 }, !ChkHideInfoInGrd.Checked);
        }

        private void ShowInventoryUomCode()
        {
            if (mFrmParent.mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 21, 22 }, true);

            if (mFrmParent.mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 21, 22, 23, 24 }, true);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                if (ChkExcludedCancelledItem.Checked)
                    Filter += " And B.CancelInd='N' ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, new string[] { "A.DocNo", "A.LocalDocNo" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueWhsCode), "A.WhsCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtPODocNo.Text, "B.PODocNo", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueVdCode), "A.VdCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "B.ItCode", "C.ItCodeInternal", "C.ItName" });
                Sm.FilterStr(ref Filter, ref cm, TxtBatchNo.Text, "B.BatchNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtLot.Text, "B.Lot", false);
                Sm.FilterStr(ref Filter, ref cm, TxtBin.Text, "B.Bin", false);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.CreateDt Desc;",
                        new string[]
                        {
                            //0
                            "DocNo", 

                            //1-5
                            "DocDt", "CancelInd", "StatusDesc", "WhsName", "VdName", 
                            
                            //6-10
                            "VdDONo", "PODocNo", "ImportInd", "ItCode", "ItName", 

                            //11-15
                            "ItCodeInternal", "BatchNo", "Lot", "Bin", "QtyPurchase", 
                            
                            //16-20
                            "PurchaseUomCode", "Qty", "InventoryUomCode", "Qty2", "InventoryUomCode2", 
                            
                            //21-25
                            "Qty3", "InventoryUomCode3", "RemarkH", "RemarkD", "CreateBy", 
                            
                            //26-30
                            "CreateDt", "LastUpBy", "LastUpDt", "ItGrpName", "DNo", 
                            
                            //31-35
                            "ForeignName", "LocalDocNo", "KBPackaging", "KBPackagingQty", "KBContractNo", 
                            
                            //36-40
                            "KBContractDt", "KBPLNo", "KBPLDt", "KBRegistrationNo", "KBRegistrationDt", 
                            
                            //41-45
                            "KBSubmissionNo", "KBNonDocInd", "CustomsDocCode", "Specification", "ProjectCode", 

                            //46-48
                            "ProjectName", "PONo", "HeatNumber"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 14);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 15);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 16);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 17);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 18);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 19);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 20);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 23, 21);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 22);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 23);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 24);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 27, 25);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 28, 26);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 29, 26);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 30, 27);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 31, 28);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 32, 28);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 33, 29);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 34, 30);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 35, 31);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 36, 32);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 37, 33);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 38, 34);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 39, 35);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 40, 36);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 41, 37);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 42, 38);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 43, 39);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 44, 40);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 45, 41);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 46, 42);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 47, 43);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 48, 44);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 49, 45);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 50, 46);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 51, 47);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 52, 48);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            e.DoDefault = false;
            if (e.ColIndex == 5) ShowApprovalInfo(e.RowIndex);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 5) ShowApprovalInfo(e.RowIndex);
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        private void ShowApprovalInfo(int Row)
        {
            var SQL = new StringBuilder();
            int Index = 0;
            string Msg = string.Empty;

            SQL.AppendLine("Select UserCode, Case Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' Else 'Outstanding' End As StatusDesc, ");
            SQL.AppendLine("Case When LastUpDt Is Not Null Then  ");
            SQL.AppendLine("Concat(Substring(LastUpDt, 7, 2), '/', Substring(LastUpDt, 5, 2), '/', Left(LastUpDt, 4))  ");
            SQL.AppendLine("Else Null End As LastUpDt, Remark ");
            SQL.AppendLine("From TblDocApproval ");
            SQL.AppendLine("Where DocType='RecvVd' ");
            SQL.AppendLine("And Status In ('A', 'C') ");
            SQL.AppendLine("And DocNo=@DocNo And DNo=@DNo ");
            SQL.AppendLine("Order By ApprovalDNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<String>(ref cm, "@DNo", Sm.GetGrdStr(Grd1, Row, 32));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                cm.CommandTimeout = 600;
                using (var dr = cm.ExecuteReader())
                {
                    var c = Sm.GetOrdinal(dr,
                         new string[] { "UserCode", "StatusDesc", "LastUpDt", "Remark" }
                    );
                    while (dr.Read())
                    {
                        Index++;
                        Msg +=
                            "No : " + Index.ToString() + Environment.NewLine +
                            "User : " + Sm.DrStr(dr, 0) + Environment.NewLine +
                            "Status : " + Sm.DrStr(dr, 1) + Environment.NewLine +
                            "Date : " + Sm.DrStr(dr, 2) + Environment.NewLine +
                            (
                            Sm.DrStr(dr, 3).Length==0?
                            string.Empty:
                            "Remark : " + Sm.DrStr(dr, 3) + Environment.NewLine
                            ) + Environment.NewLine;
                    }
                    cm.Dispose();
                }
            }
            if (Msg.Length != 0)
                Sm.StdMsg(mMsgType.Info, Msg);
            else
                Sm.StdMsg(mMsgType.Info, "No data approved/cancelled.");
        }

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue2(Sl.SetLueWhsCode), string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkWhsCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Warehouse");
        }

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue1(Sl.SetLueVdCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkVdCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Vendor");
        }

        private void TxtPODocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkPODocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "PO#");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void TxtBatchNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBatchNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Batch#");
        }

        private void TxtLot_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkLot_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Lot");
        }

        private void TxtBin_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBin_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Bin");
        }

        private void BtnRefresh_Click(object sender, EventArgs e)
        {
            if (mFrmParent.mIsButtonUsageHistoryActived)
                Sm.InsertButtonUsageHistory(Gv.VersionNo, mFrmParent.mMenuCode, this.Text, "REFRESH");
        }

        #endregion

        #endregion 
    
    }
}
