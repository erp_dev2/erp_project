﻿#region Update
/*
    02/06/2017 [ARI] tambah button, mechanic ambil dari employee
    23/11/2017 [HAR] tambah parameter filter by site
    16/11/2018 [HAR] bisa input mechanic 
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmMechanic : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmMechanicFind FrmFind;
        internal bool mIsFilterBySite = false;

        #endregion

        #region Constructor

        public FrmMechanic(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Mechanic";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                BtnPrint.Visible = false;
                GetParameter();
                SetFormControl(mState.View);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtMechanicCode, TxtMechanicName, ChkActInd }, true);
                    BtnMechanic.Enabled = false;
                    TxtMechanicCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtMechanicCode, TxtMechanicName}, false);
                    BtnMechanic.Enabled = true;
                    BtnMechanic.Focus();
                    break;
                case mState.Edit:
                    ChkActInd.Properties.ReadOnly = false;
                    ChkActInd.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> { TxtMechanicCode, TxtMechanicName });
            ChkActInd.Checked = false;
        }

        private void GetParameter()
        {
            mIsFilterBySite = Sm.GetParameter("IsFilterBySite") == "Y";
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmMechanicFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
            ChkActInd.Checked = true;
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtMechanicCode, string.Empty, false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Delete", "") == DialogResult.No || Sm.IsTxtEmpty(TxtMechanicCode, string.Empty, false)) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblMechanic Where MechanicCode=@MechanicCode" };
                Sm.CmParam<String>(ref cm, "@MechanicCode", TxtMechanicCode.Text);
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsDataNotValid()) return;
                
                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into TblMechanic(mechanicCode, mechanicName, ActInd, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@MechanicCode, @MechanicName, 'Y', @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("   Update mechanicName=@MechanicName, ActInd=@ActInd, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@MechanicCode", TxtMechanicCode.Text);
                Sm.CmParam<String>(ref cm, "@MechanicName", TxtMechanicName.Text);
                Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked ? "Y" : "N");
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.ExecCommand(cm);

                ShowData(TxtMechanicCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string MechanicCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@MechanicCode", MechanicCode);

                Sm.ShowDataInCtrl(
                        ref cm,
                        "Select MechanicCode, MechanicName, ActInd From TblMechanic Where MechanicCode=@MechanicCode;",
                        new string[]{ "MechanicCode", "MechanicName", "ActInd" },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtMechanicCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtMechanicName.EditValue = Sm.DrStr(dr, c[1]);
                            ChkActInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y");
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtMechanicCode, "Mechanic's code", false) ||
                Sm.IsTxtEmpty(TxtMechanicName, "Mechanic's name", false) ||
                IsMechanicCodeExisted();
        }

        private bool IsMechanicCodeExisted()
        {
            if (!TxtMechanicCode.Properties.ReadOnly)
            {
                if (Sm.IsDataExist(
                    "Select 1 From TblMechanic Where mechanicCode=@Param;", 
                    TxtMechanicCode.Text))
                {
                    Sm.StdMsg(mMsgType.Warning, "Mechanic code ( " + TxtMechanicCode.Text + " ) already existed.");
                    return true;
                }
            }
            return false;
        }

        #endregion

        #endregion

        #region Event

        private void BtnMechanic_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmMechanicDlg(this));
        }

        #endregion
    }
}
