﻿namespace RunSystem
{
    partial class FrmEmpWL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmEmpWL));
            this.BtnEmpCode = new DevExpress.XtraEditors.SimpleButton();
            this.TxtDeptCode = new DevExpress.XtraEditors.TextEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.TxtPosCode = new DevExpress.XtraEditors.TextEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.TxtEmpCodeOld = new DevExpress.XtraEditors.TextEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtEmpName = new DevExpress.XtraEditors.TextEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtEmpCode = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.LueWLCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.ChkCancelInd = new DevExpress.XtraEditors.CheckEdit();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.label17 = new System.Windows.Forms.Label();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label18 = new System.Windows.Forms.Label();
            this.DteStartDt = new DevExpress.XtraEditors.DateEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.DteEndDt = new DevExpress.XtraEditors.DateEdit();
            this.LblEndDt = new System.Windows.Forms.Label();
            this.MeeReason1 = new DevExpress.XtraEditors.MemoExEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.MeeReason2 = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeReason4 = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeReason3 = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeReason5 = new DevExpress.XtraEditors.MemoExEdit();
            this.TxtSiteCode = new DevExpress.XtraEditors.TextEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.OD = new System.Windows.Forms.OpenFileDialog();
            this.SFD = new System.Windows.Forms.SaveFileDialog();
            this.panel3 = new System.Windows.Forms.Panel();
            this.ChkFile = new DevExpress.XtraEditors.CheckEdit();
            this.TxtFile = new DevExpress.XtraEditors.TextEdit();
            this.PbUpload = new System.Windows.Forms.ProgressBar();
            this.label10 = new System.Windows.Forms.Label();
            this.BtnFile = new DevExpress.XtraEditors.SimpleButton();
            this.TxtReferenceNo = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.LueEmpWLCt = new DevExpress.XtraEditors.LookUpEdit();
            this.label11 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDeptCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPosCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpCodeOld.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWLCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteEndDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteEndDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeReason1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeReason2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeReason4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeReason3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeReason5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSiteCode.Properties)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtReferenceNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueEmpWLCt.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(881, 0);
            this.panel1.Size = new System.Drawing.Size(70, 461);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.LueEmpWLCt);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.TxtReferenceNo);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.LueWLCode);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Controls.Add(this.TxtSiteCode);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.MeeReason5);
            this.panel2.Controls.Add(this.MeeReason4);
            this.panel2.Controls.Add(this.MeeReason3);
            this.panel2.Controls.Add(this.MeeReason2);
            this.panel2.Controls.Add(this.MeeReason1);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.DteEndDt);
            this.panel2.Controls.Add(this.LblEndDt);
            this.panel2.Controls.Add(this.DteStartDt);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.BtnEmpCode);
            this.panel2.Controls.Add(this.TxtDeptCode);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.TxtPosCode);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.TxtEmpCodeOld);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.TxtEmpName);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.TxtEmpCode);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.ChkCancelInd);
            this.panel2.Controls.Add(this.DteDocDt);
            this.panel2.Controls.Add(this.label17);
            this.panel2.Controls.Add(this.TxtDocNo);
            this.panel2.Controls.Add(this.label18);
            this.panel2.Size = new System.Drawing.Size(881, 461);
            // 
            // BtnEmpCode
            // 
            this.BtnEmpCode.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnEmpCode.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEmpCode.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEmpCode.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEmpCode.Appearance.Options.UseBackColor = true;
            this.BtnEmpCode.Appearance.Options.UseFont = true;
            this.BtnEmpCode.Appearance.Options.UseForeColor = true;
            this.BtnEmpCode.Appearance.Options.UseTextOptions = true;
            this.BtnEmpCode.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnEmpCode.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnEmpCode.Image = ((System.Drawing.Image)(resources.GetObject("BtnEmpCode.Image")));
            this.BtnEmpCode.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnEmpCode.Location = new System.Drawing.Point(395, 47);
            this.BtnEmpCode.Name = "BtnEmpCode";
            this.BtnEmpCode.Size = new System.Drawing.Size(24, 21);
            this.BtnEmpCode.TabIndex = 16;
            this.BtnEmpCode.ToolTip = "Find Employee";
            this.BtnEmpCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnEmpCode.ToolTipTitle = "Run System";
            this.BtnEmpCode.Click += new System.EventHandler(this.BtnEmpCode_Click);
            // 
            // TxtDeptCode
            // 
            this.TxtDeptCode.EnterMoveNextControl = true;
            this.TxtDeptCode.Location = new System.Drawing.Point(134, 131);
            this.TxtDeptCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDeptCode.Name = "TxtDeptCode";
            this.TxtDeptCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDeptCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDeptCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDeptCode.Properties.Appearance.Options.UseFont = true;
            this.TxtDeptCode.Properties.MaxLength = 250;
            this.TxtDeptCode.Properties.ReadOnly = true;
            this.TxtDeptCode.Size = new System.Drawing.Size(729, 20);
            this.TxtDeptCode.TabIndex = 24;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(58, 134);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(73, 14);
            this.label9.TabIndex = 23;
            this.label9.Text = "Department";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPosCode
            // 
            this.TxtPosCode.EnterMoveNextControl = true;
            this.TxtPosCode.Location = new System.Drawing.Point(134, 110);
            this.TxtPosCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPosCode.Name = "TxtPosCode";
            this.TxtPosCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtPosCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPosCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPosCode.Properties.Appearance.Options.UseFont = true;
            this.TxtPosCode.Properties.MaxLength = 250;
            this.TxtPosCode.Properties.ReadOnly = true;
            this.TxtPosCode.Size = new System.Drawing.Size(729, 20);
            this.TxtPosCode.TabIndex = 22;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(82, 113);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(49, 14);
            this.label8.TabIndex = 21;
            this.label8.Text = "Position";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtEmpCodeOld
            // 
            this.TxtEmpCodeOld.EnterMoveNextControl = true;
            this.TxtEmpCodeOld.Location = new System.Drawing.Point(134, 89);
            this.TxtEmpCodeOld.Margin = new System.Windows.Forms.Padding(5);
            this.TxtEmpCodeOld.Name = "TxtEmpCodeOld";
            this.TxtEmpCodeOld.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtEmpCodeOld.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEmpCodeOld.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEmpCodeOld.Properties.Appearance.Options.UseFont = true;
            this.TxtEmpCodeOld.Properties.MaxLength = 50;
            this.TxtEmpCodeOld.Properties.ReadOnly = true;
            this.TxtEmpCodeOld.Size = new System.Drawing.Size(255, 20);
            this.TxtEmpCodeOld.TabIndex = 20;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(9, 92);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(122, 14);
            this.label6.TabIndex = 19;
            this.label6.Text = "Employee\'s Old Code";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtEmpName
            // 
            this.TxtEmpName.EnterMoveNextControl = true;
            this.TxtEmpName.Location = new System.Drawing.Point(134, 68);
            this.TxtEmpName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtEmpName.Name = "TxtEmpName";
            this.TxtEmpName.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtEmpName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEmpName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEmpName.Properties.Appearance.Options.UseFont = true;
            this.TxtEmpName.Properties.MaxLength = 250;
            this.TxtEmpName.Properties.ReadOnly = true;
            this.TxtEmpName.Size = new System.Drawing.Size(729, 20);
            this.TxtEmpName.TabIndex = 18;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(28, 71);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(103, 14);
            this.label5.TabIndex = 17;
            this.label5.Text = "Employee\'s Name";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtEmpCode
            // 
            this.TxtEmpCode.EnterMoveNextControl = true;
            this.TxtEmpCode.Location = new System.Drawing.Point(134, 47);
            this.TxtEmpCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtEmpCode.Name = "TxtEmpCode";
            this.TxtEmpCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtEmpCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEmpCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEmpCode.Properties.Appearance.Options.UseFont = true;
            this.TxtEmpCode.Properties.MaxLength = 50;
            this.TxtEmpCode.Properties.ReadOnly = true;
            this.TxtEmpCode.Size = new System.Drawing.Size(255, 20);
            this.TxtEmpCode.TabIndex = 15;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(31, 50);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(100, 14);
            this.label3.TabIndex = 14;
            this.label3.Text = "Employee\'s Code";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueWLCode
            // 
            this.LueWLCode.EnterMoveNextControl = true;
            this.LueWLCode.Location = new System.Drawing.Point(134, 173);
            this.LueWLCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueWLCode.Name = "LueWLCode";
            this.LueWLCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWLCode.Properties.Appearance.Options.UseFont = true;
            this.LueWLCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWLCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueWLCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWLCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueWLCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWLCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueWLCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWLCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueWLCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueWLCode.Properties.DropDownRows = 30;
            this.LueWLCode.Properties.NullText = "[Empty]";
            this.LueWLCode.Properties.PopupWidth = 800;
            this.LueWLCode.Size = new System.Drawing.Size(729, 20);
            this.LueWLCode.TabIndex = 28;
            this.LueWLCode.ToolTip = "F4 : Show/hide list";
            this.LueWLCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueWLCode.EditValueChanged += new System.EventHandler(this.LueWLCode_EditValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(41, 176);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(90, 14);
            this.label4.TabIndex = 27;
            this.label4.Text = "Warning Letter";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkCancelInd
            // 
            this.ChkCancelInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkCancelInd.Location = new System.Drawing.Point(395, 3);
            this.ChkCancelInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkCancelInd.Name = "ChkCancelInd";
            this.ChkCancelInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkCancelInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkCancelInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkCancelInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseFont = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkCancelInd.Properties.Caption = "Cancel";
            this.ChkCancelInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkCancelInd.Size = new System.Drawing.Size(72, 22);
            this.ChkCancelInd.TabIndex = 11;
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(134, 25);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(163, 20);
            this.DteDocDt.TabIndex = 13;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Red;
            this.label17.Location = new System.Drawing.Point(98, 28);
            this.label17.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(33, 14);
            this.label17.TabIndex = 12;
            this.label17.Text = "Date";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(134, 4);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 30;
            this.TxtDocNo.Properties.ReadOnly = true;
            this.TxtDocNo.Size = new System.Drawing.Size(255, 20);
            this.TxtDocNo.TabIndex = 10;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(58, 7);
            this.label18.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(73, 14);
            this.label18.TabIndex = 9;
            this.label18.Text = "Document#";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteStartDt
            // 
            this.DteStartDt.EditValue = null;
            this.DteStartDt.EnterMoveNextControl = true;
            this.DteStartDt.Location = new System.Drawing.Point(134, 195);
            this.DteStartDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteStartDt.Name = "DteStartDt";
            this.DteStartDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteStartDt.Properties.Appearance.Options.UseFont = true;
            this.DteStartDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteStartDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteStartDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteStartDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteStartDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteStartDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteStartDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteStartDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteStartDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteStartDt.Size = new System.Drawing.Size(163, 20);
            this.DteStartDt.TabIndex = 30;
            this.DteStartDt.EditValueChanged += new System.EventHandler(this.DteStartDt_EditValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(67, 198);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 14);
            this.label2.TabIndex = 29;
            this.label2.Text = "Start Date";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteEndDt
            // 
            this.DteEndDt.EditValue = null;
            this.DteEndDt.EnterMoveNextControl = true;
            this.DteEndDt.Location = new System.Drawing.Point(134, 217);
            this.DteEndDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteEndDt.Name = "DteEndDt";
            this.DteEndDt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.DteEndDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteEndDt.Properties.Appearance.Options.UseBackColor = true;
            this.DteEndDt.Properties.Appearance.Options.UseFont = true;
            this.DteEndDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteEndDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteEndDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteEndDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteEndDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteEndDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteEndDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteEndDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteEndDt.Properties.ReadOnly = true;
            this.DteEndDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteEndDt.Size = new System.Drawing.Size(163, 20);
            this.DteEndDt.TabIndex = 32;
            // 
            // LblEndDt
            // 
            this.LblEndDt.AutoSize = true;
            this.LblEndDt.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblEndDt.ForeColor = System.Drawing.Color.Black;
            this.LblEndDt.Location = new System.Drawing.Point(73, 220);
            this.LblEndDt.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.LblEndDt.Name = "LblEndDt";
            this.LblEndDt.Size = new System.Drawing.Size(58, 14);
            this.LblEndDt.TabIndex = 31;
            this.LblEndDt.Text = "End Date";
            this.LblEndDt.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeReason1
            // 
            this.MeeReason1.EnterMoveNextControl = true;
            this.MeeReason1.Location = new System.Drawing.Point(11, 303);
            this.MeeReason1.Margin = new System.Windows.Forms.Padding(5);
            this.MeeReason1.Name = "MeeReason1";
            this.MeeReason1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeReason1.Properties.Appearance.Options.UseFont = true;
            this.MeeReason1.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeReason1.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeReason1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeReason1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeReason1.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeReason1.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeReason1.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeReason1.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeReason1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeReason1.Properties.MaxLength = 850;
            this.MeeReason1.Properties.PopupFormSize = new System.Drawing.Size(850, 20);
            this.MeeReason1.Properties.ShowIcon = false;
            this.MeeReason1.Size = new System.Drawing.Size(850, 20);
            this.MeeReason1.TabIndex = 38;
            this.MeeReason1.ToolTip = "F4 : Show/hide text";
            this.MeeReason1.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeReason1.ToolTipTitle = "Run System";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Red;
            this.label12.Location = new System.Drawing.Point(14, 284);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(267, 14);
            this.label12.TabIndex = 37;
            this.label12.Text = "Violation of the Company Rules and Regulations";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeReason2
            // 
            this.MeeReason2.EnterMoveNextControl = true;
            this.MeeReason2.Location = new System.Drawing.Point(11, 324);
            this.MeeReason2.Margin = new System.Windows.Forms.Padding(5);
            this.MeeReason2.Name = "MeeReason2";
            this.MeeReason2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeReason2.Properties.Appearance.Options.UseFont = true;
            this.MeeReason2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeReason2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeReason2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeReason2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeReason2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeReason2.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeReason2.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeReason2.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeReason2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeReason2.Properties.MaxLength = 850;
            this.MeeReason2.Properties.PopupFormSize = new System.Drawing.Size(850, 20);
            this.MeeReason2.Properties.ShowIcon = false;
            this.MeeReason2.Size = new System.Drawing.Size(850, 20);
            this.MeeReason2.TabIndex = 39;
            this.MeeReason2.ToolTip = "F4 : Show/hide text";
            this.MeeReason2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeReason2.ToolTipTitle = "Run System";
            // 
            // MeeReason4
            // 
            this.MeeReason4.EnterMoveNextControl = true;
            this.MeeReason4.Location = new System.Drawing.Point(11, 366);
            this.MeeReason4.Margin = new System.Windows.Forms.Padding(5);
            this.MeeReason4.Name = "MeeReason4";
            this.MeeReason4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeReason4.Properties.Appearance.Options.UseFont = true;
            this.MeeReason4.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeReason4.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeReason4.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeReason4.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeReason4.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeReason4.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeReason4.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeReason4.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeReason4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeReason4.Properties.MaxLength = 850;
            this.MeeReason4.Properties.PopupFormSize = new System.Drawing.Size(850, 20);
            this.MeeReason4.Properties.ShowIcon = false;
            this.MeeReason4.Size = new System.Drawing.Size(850, 20);
            this.MeeReason4.TabIndex = 41;
            this.MeeReason4.ToolTip = "F4 : Show/hide text";
            this.MeeReason4.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeReason4.ToolTipTitle = "Run System";
            // 
            // MeeReason3
            // 
            this.MeeReason3.EnterMoveNextControl = true;
            this.MeeReason3.Location = new System.Drawing.Point(11, 345);
            this.MeeReason3.Margin = new System.Windows.Forms.Padding(5);
            this.MeeReason3.Name = "MeeReason3";
            this.MeeReason3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeReason3.Properties.Appearance.Options.UseFont = true;
            this.MeeReason3.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeReason3.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeReason3.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeReason3.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeReason3.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeReason3.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeReason3.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeReason3.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeReason3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeReason3.Properties.MaxLength = 850;
            this.MeeReason3.Properties.PopupFormSize = new System.Drawing.Size(850, 20);
            this.MeeReason3.Properties.ShowIcon = false;
            this.MeeReason3.Size = new System.Drawing.Size(850, 20);
            this.MeeReason3.TabIndex = 40;
            this.MeeReason3.ToolTip = "F4 : Show/hide text";
            this.MeeReason3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeReason3.ToolTipTitle = "Run System";
            // 
            // MeeReason5
            // 
            this.MeeReason5.EnterMoveNextControl = true;
            this.MeeReason5.Location = new System.Drawing.Point(11, 387);
            this.MeeReason5.Margin = new System.Windows.Forms.Padding(5);
            this.MeeReason5.Name = "MeeReason5";
            this.MeeReason5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeReason5.Properties.Appearance.Options.UseFont = true;
            this.MeeReason5.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeReason5.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeReason5.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeReason5.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeReason5.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeReason5.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeReason5.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeReason5.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeReason5.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeReason5.Properties.MaxLength = 850;
            this.MeeReason5.Properties.PopupFormSize = new System.Drawing.Size(850, 20);
            this.MeeReason5.Properties.ShowIcon = false;
            this.MeeReason5.Size = new System.Drawing.Size(850, 20);
            this.MeeReason5.TabIndex = 42;
            this.MeeReason5.ToolTip = "F4 : Show/hide text";
            this.MeeReason5.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeReason5.ToolTipTitle = "Run System";
            // 
            // TxtSiteCode
            // 
            this.TxtSiteCode.EnterMoveNextControl = true;
            this.TxtSiteCode.Location = new System.Drawing.Point(134, 152);
            this.TxtSiteCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSiteCode.Name = "TxtSiteCode";
            this.TxtSiteCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtSiteCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSiteCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSiteCode.Properties.Appearance.Options.UseFont = true;
            this.TxtSiteCode.Properties.MaxLength = 250;
            this.TxtSiteCode.Properties.ReadOnly = true;
            this.TxtSiteCode.Size = new System.Drawing.Size(729, 20);
            this.TxtSiteCode.TabIndex = 26;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(103, 155);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(28, 14);
            this.label7.TabIndex = 25;
            this.label7.Text = "Site";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // OD
            // 
            this.OD.FileName = "openFileDialog1";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.ChkFile);
            this.panel3.Controls.Add(this.TxtFile);
            this.panel3.Controls.Add(this.PbUpload);
            this.panel3.Controls.Add(this.label10);
            this.panel3.Controls.Add(this.BtnFile);
            this.panel3.Location = new System.Drawing.Point(12, 410);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(411, 49);
            this.panel3.TabIndex = 43;
            // 
            // ChkFile
            // 
            this.ChkFile.Location = new System.Drawing.Point(362, 5);
            this.ChkFile.Name = "ChkFile";
            this.ChkFile.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkFile.Properties.Appearance.Options.UseFont = true;
            this.ChkFile.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkFile.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkFile.Properties.Caption = " ";
            this.ChkFile.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkFile.Size = new System.Drawing.Size(20, 22);
            this.ChkFile.TabIndex = 46;
            this.ChkFile.ToolTip = "Remove filter";
            this.ChkFile.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkFile.ToolTipTitle = "Run System";
            this.ChkFile.CheckedChanged += new System.EventHandler(this.ChkFile_CheckedChanged);
            // 
            // TxtFile
            // 
            this.TxtFile.EnterMoveNextControl = true;
            this.TxtFile.Location = new System.Drawing.Point(36, 5);
            this.TxtFile.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFile.Name = "TxtFile";
            this.TxtFile.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFile.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFile.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFile.Properties.Appearance.Options.UseFont = true;
            this.TxtFile.Properties.MaxLength = 16;
            this.TxtFile.Size = new System.Drawing.Size(325, 20);
            this.TxtFile.TabIndex = 45;
            // 
            // PbUpload
            // 
            this.PbUpload.Location = new System.Drawing.Point(36, 27);
            this.PbUpload.Name = "PbUpload";
            this.PbUpload.Size = new System.Drawing.Size(325, 17);
            this.PbUpload.TabIndex = 48;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(7, 8);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(24, 14);
            this.label10.TabIndex = 44;
            this.label10.Text = "File";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnFile
            // 
            this.BtnFile.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnFile.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFile.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFile.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFile.Appearance.Options.UseBackColor = true;
            this.BtnFile.Appearance.Options.UseFont = true;
            this.BtnFile.Appearance.Options.UseForeColor = true;
            this.BtnFile.Appearance.Options.UseTextOptions = true;
            this.BtnFile.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnFile.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnFile.Image = ((System.Drawing.Image)(resources.GetObject("BtnFile.Image")));
            this.BtnFile.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnFile.Location = new System.Drawing.Point(381, 4);
            this.BtnFile.Name = "BtnFile";
            this.BtnFile.Size = new System.Drawing.Size(24, 21);
            this.BtnFile.TabIndex = 47;
            this.BtnFile.ToolTip = "BrowseFile";
            this.BtnFile.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnFile.ToolTipTitle = "Run System";
            this.BtnFile.Click += new System.EventHandler(this.BtnFile_Click);
            // 
            // TxtReferenceNo
            // 
            this.TxtReferenceNo.EnterMoveNextControl = true;
            this.TxtReferenceNo.Location = new System.Drawing.Point(134, 238);
            this.TxtReferenceNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtReferenceNo.Name = "TxtReferenceNo";
            this.TxtReferenceNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtReferenceNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtReferenceNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtReferenceNo.Properties.Appearance.Options.UseFont = true;
            this.TxtReferenceNo.Properties.MaxLength = 250;
            this.TxtReferenceNo.Size = new System.Drawing.Size(255, 20);
            this.TxtReferenceNo.TabIndex = 34;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(59, 241);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 14);
            this.label1.TabIndex = 33;
            this.label1.Text = "Reference#";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueEmpWLCt
            // 
            this.LueEmpWLCt.EnterMoveNextControl = true;
            this.LueEmpWLCt.Location = new System.Drawing.Point(134, 260);
            this.LueEmpWLCt.Margin = new System.Windows.Forms.Padding(5);
            this.LueEmpWLCt.Name = "LueEmpWLCt";
            this.LueEmpWLCt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEmpWLCt.Properties.Appearance.Options.UseFont = true;
            this.LueEmpWLCt.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEmpWLCt.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueEmpWLCt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEmpWLCt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueEmpWLCt.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEmpWLCt.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueEmpWLCt.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEmpWLCt.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueEmpWLCt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueEmpWLCt.Properties.DropDownRows = 30;
            this.LueEmpWLCt.Properties.NullText = "[Empty]";
            this.LueEmpWLCt.Properties.PopupWidth = 800;
            this.LueEmpWLCt.Size = new System.Drawing.Size(729, 20);
            this.LueEmpWLCt.TabIndex = 36;
            this.LueEmpWLCt.ToolTip = "F4 : Show/hide list";
            this.LueEmpWLCt.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(75, 263);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(56, 14);
            this.label11.TabIndex = 35;
            this.label11.Text = "Category";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // FrmEmpWL
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(951, 461);
            this.Name = "FrmEmpWL";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDeptCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPosCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpCodeOld.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWLCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteStartDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteEndDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteEndDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeReason1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeReason2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeReason4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeReason3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeReason5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSiteCode.Properties)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtReferenceNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueEmpWLCt.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public DevExpress.XtraEditors.SimpleButton BtnEmpCode;
        internal DevExpress.XtraEditors.TextEdit TxtDeptCode;
        private System.Windows.Forms.Label label9;
        internal DevExpress.XtraEditors.TextEdit TxtPosCode;
        private System.Windows.Forms.Label label8;
        internal DevExpress.XtraEditors.TextEdit TxtEmpCodeOld;
        private System.Windows.Forms.Label label6;
        internal DevExpress.XtraEditors.TextEdit TxtEmpName;
        private System.Windows.Forms.Label label5;
        internal DevExpress.XtraEditors.TextEdit TxtEmpCode;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.LookUpEdit LueWLCode;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.CheckEdit ChkCancelInd;
        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        private System.Windows.Forms.Label label17;
        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Label label18;
        internal DevExpress.XtraEditors.DateEdit DteEndDt;
        private System.Windows.Forms.Label LblEndDt;
        internal DevExpress.XtraEditors.DateEdit DteStartDt;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.MemoExEdit MeeReason1;
        private System.Windows.Forms.Label label12;
        private DevExpress.XtraEditors.MemoExEdit MeeReason5;
        private DevExpress.XtraEditors.MemoExEdit MeeReason4;
        private DevExpress.XtraEditors.MemoExEdit MeeReason3;
        private DevExpress.XtraEditors.MemoExEdit MeeReason2;
        internal DevExpress.XtraEditors.TextEdit TxtSiteCode;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.OpenFileDialog OD;
        private System.Windows.Forms.SaveFileDialog SFD;
        private System.Windows.Forms.Panel panel3;
        private DevExpress.XtraEditors.CheckEdit ChkFile;
        internal DevExpress.XtraEditors.TextEdit TxtFile;
        private System.Windows.Forms.ProgressBar PbUpload;
        private System.Windows.Forms.Label label10;
        public DevExpress.XtraEditors.SimpleButton BtnFile;
        private DevExpress.XtraEditors.LookUpEdit LueEmpWLCt;
        private System.Windows.Forms.Label label11;
        internal DevExpress.XtraEditors.TextEdit TxtReferenceNo;
        private System.Windows.Forms.Label label1;
    }
}