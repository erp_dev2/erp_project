﻿#region Update
/*
    04/03/2021 [DITA/IMS] menampilkan loop ketransaksi voucher berdasarkan parameter : IsRptShowVoucherLoop
    09/11/2021 [VIN/AMKA] tambah lup SLI based DO 
    10/11/2021 [VIN/AMKA] tambah lup return ARDP -VC
    25/11/2021 [MYA/AMKA] Menambah kolom departement yang terotorisasi grup pada menu customer deposit movement : Voucher ARDP dan cancel voucher ARDP
    25/11/2021 [MYA/AMKA] Menambah kolom departement yang terotorisasi grup pada menu customer deposit movement : Voucher Return ARDP + cancel voucher return
    25/11/2021 [MYA/AMKA] Menambah kolom departement yang terotorisasi grup pada menu customer deposit movement : Sales Invoice with DP + cancel Sales invoice with DP
    13/12/2021 [TRI/AMKA] BUG ketika refresh data dengan filter customer
    25/10/2022 [ICA/AMKA] Menambah kolom amountbeftax dan tax berdasarkan parameter IsPurchaseInvoiceUseTabToInputDownpayment\
    12/12/2022 [ICA/AMKA] Bug ketika refresh dikarenakan kondisi else belum di select taxamt dll nya
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptCustomerDepositMovement : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty,
            mDoctitle = string.Empty;
        private bool mIsRptShowVoucherLoop = false,
            mIsRptCustomerDepositMovementUseDept = false,
            mIsRptCustomerDepositMovementFilteredByDept = false,
            mIsPurchaseInvoiceUseTabToInputDownpayment = false;

        #endregion

        #region Constructor

        public FrmRptCustomerDepositMovement(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                SetSQL();
                Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -7);
                Sl.SetLueCtCode(ref LueCtCode);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {

            var SQL = new StringBuilder();

            if (mIsRptCustomerDepositMovementUseDept)
            {
                SQL.AppendLine("SELECT A.DocNo, A.OptDesc, A.DocDt, A.CtName, A.CurCode, A.DocType, A.DeptCode, A.DeptName, ");
                if (mIsPurchaseInvoiceUseTabToInputDownpayment)
                {
                    SQL.AppendLine("B.TaxName, B.TaxName2, B.TaxName3, ifnull(B.TaxAmt, 0.00) TaxAmt, IfNull(B.TaxAmt2, 0.00) TaxAmt2, IfNull(B.TaxAmt3, 0.00) TaxAmt3, ");
                    SQL.AppendLine("IfNull(B.Amt, A.Amt) Amt, IfNull(B.AmtBefTax, 0.00) AmtBefTax");
                }

                else
                    SQL.AppendLine("Null TaxName, Null TaxName2, Null TaxName3, 0.00 TaxAmt, 0.00 TaxAmt2, 0.00 TaxAmt3, A.Amt, 0.00 AmtBefTax ");
                SQL.AppendLine("FROM  ");
                SQL.AppendLine("( ");
                SQL.AppendLine("Select A.DocNo, C.OptDesc, A.DocDt, B.CtName, A.CurCode, A.Amt, A.DocType, D.DeptCode, D.DeptName, A.CtCode");
                SQL.AppendLine("From TblCustomerDepositMovement A ");
                SQL.AppendLine("Inner Join TblCustomer B On A.CtCode = B.CtCode ");
                SQL.AppendLine("Inner Join TblOption C On A.Doctype = C.OptCode And C.OptCat = 'CustomerDepositDocType'");
                SQL.AppendLine("LEFT JOIN ( ");
                SQL.AppendLine("	Select A.DocNo, A.DocType, E.DeptCode, F.DeptName");
                SQL.AppendLine("	From TblCustomerDepositMovement A ");
                SQL.AppendLine("	Inner Join TblCustomer B On A.CtCode = B.CtCode ");
                SQL.AppendLine("	Inner Join TblOption C On A.Doctype = C.OptCode And C.OptCat = 'CustomerDepositDocType' AND A.DocType IN ('01','02','07','08')");
                SQL.AppendLine("	INNER JOIN TblVoucherHdr D ON D.DocNo = A.DocNo ");
                SQL.AppendLine("	INNER JOIN tblVoucherrequesthdr E ON D.Voucherrequestdocno = E.DocNo ");
                SQL.AppendLine("	LEFT JOIN tbldepartment F ON E.DeptCode = F.DeptCode");

                SQL.AppendLine("	UNION ALL ");

                SQL.AppendLine("	Select A.DocNo, A.DocType, H.DeptCode, I.DeptName");
                SQL.AppendLine("	From TblCustomerDepositMovement A  ");
                SQL.AppendLine("	Inner Join TblCustomer B On A.CtCode = B.CtCode  ");
                SQL.AppendLine("	Inner Join TblOption C On A.Doctype = C.OptCode And C.OptCat = 'CustomerDepositDocType' AND A.DocType IN ('03','04')");
                SQL.AppendLine("	INNER JOIN tblsalesinvoicehdr D ON A.DocNo = D.DocNo ");
                SQL.AppendLine("	INNER JOIN tblsalesinvoicedtl E ON D.DocNo = E.DocNo  ");
                SQL.AppendLine("	INNER JOIN tbldocthdr F ON E.DOCtDocNo = F.DocNo AND E.DNo = '001' ");
                SQL.AppendLine("	INNER JOIN tblwarehouse G ON F.WhsCode = G.WhsCode ");
                SQL.AppendLine("	LEFT JOIN tblcostcenter H ON G.CCCode = H.CCCode ");
                SQL.AppendLine("	LEFT JOIN tbldepartment I ON H.DeptCode = I.DeptCode");
                SQL.AppendLine(")D ON D.DocNo = A.DocNo AND A.DocType = D.DocType ");
                SQL.AppendLine(")A ");
                if (mIsPurchaseInvoiceUseTabToInputDownpayment)
                {
                    SQL.AppendLine("LEFT JOIN ( ");
                    SQL.AppendLine("	Select A.DocNo, A.DocType, E.TaxName, F.TaxName TaxName2, G.TaxName TaxName3, ");
                    SQL.AppendLine("	D.TaxAmt, D.TaxAmt2, D.TaxAmt3, D.Amt, D.AmtBefTax ");
                    SQL.AppendLine("	From TblCustomerDepositMovement A ");
                    SQL.AppendLine("	Inner Join TblOption B On A.Doctype = B.OptCode And B.OptCat = 'CustomerDepositDocType' AND A.DocType IN ('01','02') ");
                    SQL.AppendLine("	INNER JOIN TblVoucherHdr C ON C.DocNo = A.DocNo ");
                    SQL.AppendLine("	INNER JOIN TblARDownpayment D ON C.VoucherRequestDocNo = D.VoucherRequestDocNo ");
                    SQL.AppendLine("	LEFT JOIN TblTax E ON D.TaxCode = E.TaxCode ");
                    SQL.AppendLine("	LEFT JOIN TblTax F ON D.TaxCode2 = F.TaxCode ");
                    SQL.AppendLine("	LEFT JOIN TblTax G ON D.TaxCode3 = G.TaxCode ");
                    SQL.AppendLine("	UNION ALL ");
                    SQL.AppendLine("	Select A.DocNo, A.DocType, GROUP_CONCAT(DISTINCT(E.TaxName)) TaxName, GROUP_CONCAT(DISTINCT(F.TaxName)) TaxName2, GROUP_CONCAT(DISTINCT(G.TaxName)) TaxName3, ");
                    SQL.AppendLine("	SUM(IfNull(D.SLITaxAmt, 0.00)) TaxAmt, SUM(IfNull(D.SLITaxAmt2, 0.00)) TaxAmt2, SUM(IFNULL(D.SLITaxAmt3, 0.00)) TaxAmt3, C.DownPayment Amt, Sum(IfNull(D.DownpaymentBefTax, 0.00)) AmtBefTax ");
                    SQL.AppendLine("	From TblCustomerDepositMovement A ");
                    SQL.AppendLine("	Inner Join TblOption B On A.Doctype = B.OptCode And B.OptCat = 'CustomerDepositDocType' AND A.DocType IN ('03','04') ");
                    SQL.AppendLine("	INNER JOIN tblsalesinvoicehdr C ON A.DocNo = C.DocNo ");
                    SQL.AppendLine("	INNER JOIN tblsalesinvoicedtl4 D ON C.DocNo = D.DocNo ");
                    SQL.AppendLine("	LEFT JOIN TblTax E ON D.TaxCode = E.TaxCode ");
                    SQL.AppendLine("	LEFT JOIN TblTax F ON D.TaxCode2 = F.TaxCode ");
                    SQL.AppendLine("	LEFT JOIN TblTax G ON D.TaxCode3 = G.TaxCode ");
                    SQL.AppendLine("	GROUP BY A.DocNo, A.DocType, C.Downpayment ");
                    SQL.AppendLine(") B ON A.DocNo = B.DocNo AND A.DocType = B.DocType ");
                }
                SQL.AppendLine("WHere 0 = 0 ");
                if (mIsRptCustomerDepositMovementFilteredByDept)
                {
                    SQL.AppendLine("AND (A.DeptCode Is Null OR (A.DeptCode is Not Null And EXISTS (  ");
                    SQL.AppendLine("SELECT 1 From tblgroupdepartment G  ");
                    SQL.AppendLine("WHERE G.DeptCode = A.DeptCode  ");
                    SQL.AppendLine("And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode)  ");
                    SQL.AppendLine(") )) ");
                }

            }
            else
            {
                SQL.AppendLine("Select A.DocNo, C.OptDesc, A.DocDt, B.CtName, A.CurCode, A.Amt, A.DocType, Null as DeptName, ");
                SQL.AppendLine("Null TaxName, Null TaxName2, Null TaxName3, 0.00 TaxAmt, 0.00 TaxAmt2, 0.00 TaxAmt3, A.Amt, 0.00 AmtBefTax ");
                SQL.AppendLine("From TblCustomerDepositMovement A ");
                SQL.AppendLine("Inner Join TblCustomer B On A.CtCode = B.CtCode ");
                SQL.AppendLine("Inner Join TblOption C On A.Doctype = C.OptCode And C.OptCat = 'CustomerDepositDocType' ");
                SQL.AppendLine("WHere 0 = 0 ");
            }


            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 17;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[]
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#",
                        "Date",
                        "Description",
                        "Customer",
                        "Currency",

                        //6-9
                        "Amount"+Environment.NewLine+"Before Tax",
                        "Tax 1",
                        "Tax 1 Amount",
                        "Tax 2",
                        "Tax 2 Amount",

                        //11-15
                        "Tax 3",
                        "Tax 3 Amount",
                        (mIsPurchaseInvoiceUseTabToInputDownpayment ? ("Amount"+Environment.NewLine+"After Tax"): "Amount"),
                        "",
                        "DocType",

                        //16
                        "Department",
                    },
                    new int[]
                    {
                        50,
                        150, 80, 300, 200, 80,
                        130, 150, 130, 150, 130,
                        150, 130, 130, 20, 0, 
                        200
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 6, 8, 10, 12, 13 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2 });
            Sm.GrdColButton(Grd1, new int[] { 14 });
            Sm.GrdColInvisible(Grd1, new int[] { 15 }, false);
            if (!mIsRptShowVoucherLoop) Sm.GrdColInvisible(Grd1, new int[] { 14 }, false);
            if (!mIsRptCustomerDepositMovementUseDept) Sm.GrdColInvisible(Grd1, new int[] { 16 }, false);
            Sm.GrdColInvisible(Grd1, new int[] { 6, 7, 8, 9, 10, 11, 12 }, mIsPurchaseInvoiceUseTabToInputDownpayment);
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 15, 16 });
            Grd1.Cols[14].Move(2);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDt");
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCode), "A.CtCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.DocDt, OptDesc, A.DocNo;",
                        new string[]
                        { 
                            //0
                            "DocNo", 

                            //1-5
                            "DocDt", "OptDesc", "CtName", "CurCode", "AmtBefTax",

                            //6-10
                            "TaxName", "TaxAmt", "TaxName2", "TaxAmt2", "TaxName3",
                            
                            //11-14
                            "TaxAmt3", "Amt", "DocType", "DeptName"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 14);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        private void GetParameter()
        {
            mIsRptShowVoucherLoop = Sm.GetParameterBoo("IsRptShowVoucherLoop");
            mIsRptCustomerDepositMovementFilteredByDept = Sm.GetParameterBoo("IsRptCustomerDepositMovementFilteredByDept");
            mIsRptCustomerDepositMovementUseDept = Sm.GetParameterBoo("IsRptCustomerDepositMovementUseDept");
            mIsPurchaseInvoiceUseTabToInputDownpayment = Sm.GetParameterBoo("IsPurchaseInvoiceUseTabToInputDownpayment");
            mDoctitle = Sm.GetParameter("Doctitle");
        }

        #endregion

        #region Grid Nethod

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {

            if (e.ColIndex == 14 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0 && (Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 15), "01") || Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 15), "02")
                || Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 15), "07") || Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 15), "08")))
            {
                var f = new FrmVoucher(mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
            if (mDoctitle == "AMKA")
            {
                if (e.ColIndex == 14 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0 && (Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 15), "03") || Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 15), "04")))
                {
                    var f = new FrmSalesInvoice3(mMenuCode);
                    f.Tag = "***";
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                    f.ShowDialog();
                }
            }
            else
            {
                if (e.ColIndex == 14 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0 && (Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 15), "03") || Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 15), "04")))
                {
                    var f = new FrmSalesInvoice(mMenuCode);
                    f.Tag = "***";
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                    f.ShowDialog();
                }
            }

        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 14 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0 && (Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 15), "01")|| Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 15), "02")
                || Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 15), "07") || Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 15), "08")))
            {
                var f = new FrmVoucher(mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
            if (mDoctitle == "AMKA")
            {
                if (e.ColIndex == 14 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0 && (Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 15), "03") || Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 15), "04")))
                {
                    var f = new FrmSalesInvoice3(mMenuCode);
                    f.Tag = "***";
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                    f.ShowDialog();
                }
            }
            else
            {
                if (e.ColIndex == 14 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0 && (Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 15), "03") || Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 15), "04")))
                {
                    var f = new FrmSalesInvoice(mMenuCode);
                    f.Tag = "***";
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                    f.ShowDialog();
                }
            }
        }

        #endregion

        #region Misc Control Nethod

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Date");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        #endregion

        #endregion
    }
}
