﻿#region Update
/*
    31/03/2020 [WED/YK] new apps
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptBusinessResultProjection : RunSystem.FrmBase6
    {
        #region Field

        private string
            mMenuCode = string.Empty,
            mAccessInd = string.Empty,
            mSQL = string.Empty,
            mJointOperationTypeForKSOLead = string.Empty,
            mJointOperationTypeForKSOMember = string.Empty,
            mJointOperationTypeForNonKSO = string.Empty,
            mSiteCode = string.Empty,
            mTypeForDetailRealization = string.Empty,
            mDescForDetailRealization = string.Empty;

        private int[] mColDec = 
        {
            15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 
            26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 
            41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 
            56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 
            71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 
            86, 87
        };

        #endregion

        #region Constructor

        public FrmRptBusinessResultProjection(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Form Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sl.SetLueYr(LueYr, string.Empty);
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                SetLueSiteCode(ref LueSiteCode);
                SetGrd();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Methods

        private void SetGrd()
        {
            Grd1.Cols.Count = 88;
            Grd1.FrozenArea.ColCount = 4;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "KSO/Non-KSO",
                    "Carry Over/New",
                    "NAS",
                    "UP",
                    "Project Code",
                    
                    //6-10
                    "Project Name", 
                    "Year",
                    "Resource", 
                    "Scope of Work",
                    "Type",

                    //11-15
                    "Item",
                    "Customer",
                    "Contract Date",
                    "Non-PPN",
                    "Business Result Projection"+Environment.NewLine+"Contract",

                    //16-20
                    "Business Result Projection"+Environment.NewLine+"RBP"+Environment.NewLine+"Budget",
                    "Business Result Projection"+Environment.NewLine+"RBP"+Environment.NewLine+"(%)",
                    "Business Result Projection"+Environment.NewLine+"Work Profit"+Environment.NewLine+"Amount",
                    "Business Result Projection"+Environment.NewLine+"Work Profit"+Environment.NewLine+"(%)",
                    "Produced Last Year"+Environment.NewLine+"Delivery",

                    //21-25
                    "Produced Last Year"+Environment.NewLine+"ROPT"+Environment.NewLine+"Budget",
                    "Produced Last Year"+Environment.NewLine+"ROPT"+Environment.NewLine+"(%)",
                    "Produced Last Year"+Environment.NewLine+"Work Profit"+Environment.NewLine+"Amount",
                    "Produced Last Year"+Environment.NewLine+"Work Profit"+Environment.NewLine+"(%)",
                    "Avaliable Project"+Environment.NewLine+"Production",

                    //26-30
                    "Avaliable Project"+Environment.NewLine+"ROPT"+Environment.NewLine+"Budget",
                    "Avaliable Project"+Environment.NewLine+"ROPT"+Environment.NewLine+"(%)",
                    "Avaliable Project"+Environment.NewLine+"Work Profit"+Environment.NewLine+"Amount",
                    "Avaliable Project"+Environment.NewLine+"Work Profit"+Environment.NewLine+"(%)",
                    "Production"+Environment.NewLine+"January",

                    //31-35
                    "Budget"+Environment.NewLine+"January",
                    "Work Profit"+Environment.NewLine+"January",
                    "Production"+Environment.NewLine+"February",
                    "Budget"+Environment.NewLine+"February",
                    "Work Profit"+Environment.NewLine+"February",

                    //36-40
                    "Production"+Environment.NewLine+"March",
                    "Budget"+Environment.NewLine+"March",
                    "Work Profit"+Environment.NewLine+"March",
                    "Production"+Environment.NewLine+"1st Quarterly",
                    "Budget"+Environment.NewLine+"1st Quarterly",

                    //41-45
                    "Work Profit"+Environment.NewLine+"1st Quarterly",
                    "Production"+Environment.NewLine+"April",
                    "Budget"+Environment.NewLine+"April",
                    "Work Profit"+Environment.NewLine+"April",
                    "Production"+Environment.NewLine+"May",

                    //46-50
                    "Budget"+Environment.NewLine+"May",
                    "Work Profit"+Environment.NewLine+"May",
                    "Production"+Environment.NewLine+"June",
                    "Budget"+Environment.NewLine+"June",
                    "Work Profit"+Environment.NewLine+"June",

                    //51-55
                    "Production"+Environment.NewLine+"2nd Quarterly",
                    "Budget"+Environment.NewLine+"2nd Quarterly",
                    "Work Profit"+Environment.NewLine+"2nd Quarterly",
                    "Production"+Environment.NewLine+"July",
                    "Budget"+Environment.NewLine+"July",

                    //56-60
                    "Work Profit"+Environment.NewLine+"July",
                    "Production"+Environment.NewLine+"August",
                    "Budget"+Environment.NewLine+"August",
                    "Work Profit"+Environment.NewLine+"August",
                    "Production"+Environment.NewLine+"September",

                    //61-65
                    "Budget"+Environment.NewLine+"September",
                    "Work Profit"+Environment.NewLine+"September",
                    "Production"+Environment.NewLine+"3rd Quarterly",
                    "Budget"+Environment.NewLine+"3rd Quarterly",
                    "Work Profit"+Environment.NewLine+"3rd Quarterly",

                    //66-70
                    "Production"+Environment.NewLine+"October",
                    "Budget"+Environment.NewLine+"October",
                    "Work Profit"+Environment.NewLine+"October",
                    "Production"+Environment.NewLine+"November",
                    "Budget"+Environment.NewLine+"November",

                    //71-75
                    "Work Profit"+Environment.NewLine+"November",
                    "Production"+Environment.NewLine+"December",
                    "Budget"+Environment.NewLine+"December",
                    "Work Profit"+Environment.NewLine+"December",
                    "Production"+Environment.NewLine+"4th Quarterly",

                    //76-80
                    "Budget"+Environment.NewLine+"4th Quarterly",
                    "Work Profit"+Environment.NewLine+"4th Quarterly",
                    "Total Projection"+Environment.NewLine+"Production",
                    "Total Projection"+Environment.NewLine+"Budget",
                    "Total Projection"+Environment.NewLine+"(%)",

                    //81-85
                    "Total Projection"+Environment.NewLine+"Work Profit",
                    "Total Projection"+Environment.NewLine+"(%)",
                    "Carry Over"+Environment.NewLine+"Production",
                    "Carry Over"+Environment.NewLine+"Budget",
                    "Carry Over"+Environment.NewLine+"(%)",

                    //86-87
                    "Carry Over"+Environment.NewLine+"Work Profit",
                    "Carry Over"+Environment.NewLine+"(%)"
                },
                new int[] 
                {
                    //0
                    50, 

                    //1-5
                    130, 100, 60, 60, 150, 
                    
                    //6-10
                    200, 80, 150, 150, 150, 

                    //11-15
                    150, 100, 100, 100, 100, 

                    //16-20
                    150, 50, 150, 150, 150, 

                    //21-25
                    150, 150, 150, 150, 150, 

                    //26-30
                    150, 150, 150, 150, 150, 

                    //31-35
                    150, 150, 150, 150, 150, 

                    //36-40
                    150, 150, 150, 150, 150, 
                    
                    //41-45
                    150, 150, 150, 150, 150, 

                    //46-50
                    150, 150, 150, 150, 150, 

                    //51-55
                    150, 150, 150, 150, 150, 

                    //56-60
                    150, 150, 150, 150, 150, 

                    //61-65
                    150, 150, 150, 150, 150, 

                    //66-70
                    150, 150, 150, 150, 150, 

                    //71-75
                    150, 150, 150, 150, 150, 

                    //76-80
                    150, 150, 150, 150, 150, 

                    //81-85
                    150, 150, 150, 150, 150,

                    //86-87
                    150, 150
                }
            );
            Sm.GrdFormatDec(Grd1, mColDec, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 0 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            if (Sm.IsLueEmpty(LueYr, "Year") || Sm.IsLueEmpty(LueSiteCode, "Site")) return;

            Sm.ClearGrd(Grd1, false);

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var l = new List<ProductionVsReceivable>();
                var l2 = new List<AuctionBranchToSite>();
                var l3 = new List<DummyCounter>();
                var l4 = new List<SiteCounter>();

                mSiteCode = Sm.GetLue(LueSiteCode);

                ProcessSiteCounter(ref l3);
                ProcessBranchToSite(ref l2);

                if (l2.Count > 0 && l3.Count > 0)
                {
                    ProcessSiteCounter2(ref l2, ref l3, ref l4);
                }

                Process1(ref l, ref l4);
                if (l.Count > 0)
                {
                    Process2(ref l);
                    Process3(ref l);
                }
                else
                {
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
                }

                l.Clear(); l2.Clear(); l3.Clear(); l4.Clear();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Methods


        private void ProcessBranchToSite(ref List<AuctionBranchToSite> l)
        {
            string sSQL = "Select OptCode, OptDesc From TblOption Where OptCat = 'AuctionBranchToSite';";
            var cm = new MySqlCommand();

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = sSQL;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "OptCode", "OptDesc" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new AuctionBranchToSite()
                        {
                            BranchCode = Sm.DrStr(dr, c[0]),
                            SiteCode = Sm.DrStr(dr, c[1])
                        });
                    }
                }
                dr.Close();
            }

            if (l.Count > 0)
            {
                foreach (var x in l)
                {
                    if (Sm.GetLue(LueSiteCode) == x.SiteCode)
                    {
                        if (mSiteCode.Length > 0) mSiteCode += ",";
                        mSiteCode += x.BranchCode;
                    }
                }

                if (mSiteCode.Length == 0) mSiteCode = Sm.GetLue(LueSiteCode);
            }
        }


        private void ProcessSiteCounter(ref List<DummyCounter> l)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.SiteCode, Count(A.DocNo) Counter ");
            SQL.AppendLine("From TblLOPHdr A ");
            SQL.AppendLine("Inner Join TblBOQHdr B On A.DocNo = B.LOPDocNo ");
            SQL.AppendLine("Inner Join TblSOContractHdr C ON B.DocNo = C.BOQDocNo ");
            SQL.AppendLine("    And C.Status = 'A' ");
            SQL.AppendLine("    And C.CancelInd = 'N' ");
            SQL.AppendLine("    And Left(C.DocDt, 4) = @Yr ");
            SQL.AppendLine("Inner Join TblProjectImplementationHdr D On C.DocNo = D.SOContractDocNo ");
            SQL.AppendLine("Group By A.SiteCode; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "SiteCode", "Counter" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new DummyCounter()
                        {
                            SiteCode = Sm.DrStr(dr, c[0]),
                            No = Sm.DrInt(dr, c[1])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ProcessSiteCounter2(ref List<AuctionBranchToSite> l, ref List<DummyCounter> l2, ref List<SiteCounter> l3)
        {
            foreach (var x in l2)
            {
                foreach (var y in l)
                {
                    if (x.SiteCode == y.BranchCode)
                    {
                        x.SiteCode = y.SiteCode;
                        break;
                    }
                }
            }

            l3 = l2.GroupBy(x => x.SiteCode)
                .Select(t => new SiteCounter()
                {
                    SiteCode = t.Key,
                    No = 0,
                    No2 = t.Sum(s => s.No)
                }).ToList();

            for (int i = 0; i < l3.Count; ++i)
            {
                if (i != 0)
                {
                    l3[i].No = l3[i - 1].No2 + 1;
                    l3[i].No2 += l3[i - 1].No2;
                }
                else
                {
                    l3[i].No = 1;
                }
            }
        }


        private void SetLueSiteCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT SiteCode Col1, SiteName Col2 ");
            SQL.AppendLine("FROM tblsite ");
            SQL.AppendLine("WHERE FIND_IN_SET(sitecode, (SELECT parvalue FROM tblparameter WHERE parcode = 'SiteCodeForAuctionInfo')) ");
            SQL.AppendLine("AND sitecode NOT IN ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    SELECT optcode ");
            SQL.AppendLine("    FROM tbloption ");
            SQL.AppendLine("    WHERE optcat = 'AuctionBranchToSite' ");
            SQL.AppendLine(") ");
            SQL.AppendLine("Order By SiteCode; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void GetParameter()
        {
            mTypeForDetailRealization = Sm.GetParameter("TypeForDetailRealization");
            mDescForDetailRealization = Sm.GetParameter("DescForDetailRealization");
            mJointOperationTypeForKSOLead = Sm.GetParameter("JointOperationTypeForKSOLead");
            mJointOperationTypeForKSOMember = Sm.GetParameter("JointOperationTypeForKSOMember");
            mJointOperationTypeForNonKSO = Sm.GetParameter("JointOperationTypeForNonKSO");

            if (mTypeForDetailRealization.Length == 0) mTypeForDetailRealization = "Carry Over,Proyek Baru";
            if (mDescForDetailRealization.Length == 0) mDescForDetailRealization = "PROYEK-PROYEK NON-KSO,PROYEK-PROYEK KSO,PROYEK-PROYEK KSO (Member) ";
        }

        private void Process1(ref List<ProductionVsReceivable> l, ref List<SiteCounter> l2)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            int mUP = 1;
            int mNas = 1;

            foreach (var x in l2)
            {
                if (Sm.GetLue(LueSiteCode) == x.SiteCode)
                {
                    mNas = x.No;
                    break;
                }
            }

            string[] mProjectDesc = mDescForDetailRealization.Split(',');
            string[] mProjectType = mTypeForDetailRealization.Split(',');

            SQL.AppendLine("SELECT T1.*, T2.ItName, T3.CtName FROM ( ");

            for (int i = 0; i < mProjectDesc.Count(); ++i)
            {
                for (int j = 0; j < mProjectType.Count(); ++j)
                {
                    SQL.AppendLine("SELECT '" + mProjectDesc[i] + "' AS ProjectDesc, '" + mProjectType[j] + "' AS ProjectDesc2, IF(F1.ParValue = '2', D.ProjectCode2, D.ProjectCOde) ProjectCode2, B.ProjectName, LEFT(D.DocDt, 4) Yr, G.OptDesc ProjectResource, ");
                    SQL.AppendLine("H.OptDesc ProjectScope, I.OptDesc ProjectType, J.ItCode, B.CtCode, DATE_FORMAT(D.DocDt, '%d %b %Y') DocDt, ");
                    SQL.AppendLine("Case C.TaxInd When 'N' Then '1' ELSE '0' END AS NonPPN, E.Amt, F.TotalResource, F.ExclPPNPercentage, IFNULL(K.Amt, 0.00) PDAmt, ");
                    SQL.AppendLine("IFNULL(L.Amt, 0.00) VAmt, IFNULL(M.Amt1, 0.00) PDAmt1, IFNULL(N.Amt1, 0.00) VAmt1, IFNULL(M.Amt2, 0.00) PDAmt2, IFNULL(N.Amt2, 0.00) VAmt2,  ");
                    SQL.AppendLine("IFNULL(M.Amt3, 0.00) PDAmt3, IFNULL(N.Amt3, 0.00) VAmt3, IFNULL(M.Amt4, 0.00) PDAmt4, IFNULL(N.Amt4, 0.00) VAmt4,  ");
                    SQL.AppendLine("IFNULL(M.Amt5, 0.00) PDAmt5, IFNULL(N.Amt5, 0.00) VAmt5, IFNULL(M.Amt6, 0.00) PDAmt6, IFNULL(N.Amt6, 0.00) VAmt6,  ");
                    SQL.AppendLine("IFNULL(M.Amt7, 0.00) PDAmt7, IFNULL(N.Amt7, 0.00) VAmt7, IFNULL(M.Amt8, 0.00) PDAmt8, IFNULL(N.Amt8, 0.00) VAmt8,  ");
                    SQL.AppendLine("IFNULL(M.Amt9, 0.00) PDAmt9, IFNULL(N.Amt9, 0.00) VAmt9, IFNULL(M.Amt10, 0.00) PDAmt10, IFNULL(N.Amt10, 0.00) VAmt10,  ");
                    SQL.AppendLine("IFNULL(M.Amt11, 0.00) PDAmt11, IFNULL(N.Amt11, 0.00) VAmt11, IFNULL(M.Amt12, 0.00) PDAmt12, IFNULL(N.Amt12, 0.00) VAmt12 ");
                    SQL.AppendLine("FROM TblSite A ");
                    SQL.AppendLine("INNER JOIN TblLOPHdr B ON A.SiteCode = B.SiteCode ");
                    SQL.AppendLine("    AND FIND_IN_SET(A.SiteCode, @SiteCode) ");
                    SQL.AppendLine("    AND B.CancelInd = 'N' ");
                    SQL.AppendLine("    AND B.STATUS = 'A' ");
                    SQL.AppendLine("INNER JOIN TblBOQHdr C ON B.DOcNo = C.LOPDocNo ");
                    SQL.AppendLine("INNER JOIN TblSOContractHdr D ON C.DocNo= D.BOQDocNo ");
                    if (i == 0)
                        SQL.AppendLine("    AND D.JOType = @JointOperationTypeForNonKSO ");
                    else if (i == 1)
                        SQL.AppendLine("    AND D.JOType = @JointOperationTypeForKSOLead ");
                    else
                        SQL.AppendLine("    AND D.JOType = @JointOperationTypeForKSOMember ");

                    if (j == 0)
                        SQL.AppendLine("And LEFT(D.DocDt, 4) < @Yr ");
                    else
                        SQL.AppendLine("And LEFT(D.DocDt, 4) = @Yr ");
                    SQL.AppendLine("INNER JOIN TblSOContractRevisionHDr E ON D.DocNo = E.SOCDocNo ");
                    SQL.AppendLine("INNER JOIN TblProjectImplementationHdr F ON E.DocNo = F.SOContractDocNo ");
                    SQL.AppendLine("    AND F.CancelInd = 'N' ");
                    SQL.AppendLine("LEFT JOIN TblParameter F1 ON F1.ParCode = 'ProjectAcNoFormula' ");
                    SQL.AppendLine("INNER JOIN TblOption G ON G.Optcat = 'ProjectResource' AND B.ProjectResource = G.OptCode ");
                    SQL.AppendLine("INNER JOIN TblOption H ON H.Optcat = 'ProjectScope' AND B.ProjectScope = H.OptCode ");
                    SQL.AppendLine("INNER JOIN TblOption I ON I.Optcat = 'ProjectType' AND B.ProjectType = I.OptCode ");
                    SQL.AppendLine("INNER JOIN TblSOContractDtl J ON D.DocNo = J.DocNo ");
                    SQL.AppendLine("LEFT JOIN ");
                    SQL.AppendLine("( ");
                    SQL.AppendLine("    SELECT T1.PRJIDocNo, SUM(T1.Amt) Amt ");
                    SQL.AppendLine("    FROM TblProjectDeliveryHdr T1 ");
                    SQL.AppendLine("    INNER JOIN TblProjectImplementationHdr T2 ON T1.PRJIDocNo = T2.DocNo ");
                    SQL.AppendLine("        AND T1.CancelInd = 'N' ");
                    SQL.AppendLine("        AND T1.STATUS = 'A' ");
                    SQL.AppendLine("        AND LEFT(T1.DocDt, 4) = (@Yr - 1) ");
                    SQL.AppendLine("    INNER JOIN TblSOContractRevisionHdr T3 ON T2.SOContractDocNo = T3.DocNo ");
                    SQL.AppendLine("    INNER JOIN TblSOContractHdr T4 ON T3.SOCDocNo = T4.DocNo ");
                    if (i == 0)
                        SQL.AppendLine("    AND T4.JOType = @JointOperationTypeForNonKSO ");
                    else if (i == 1)
                        SQL.AppendLine("    AND T4.JOType = @JointOperationTypeForKSOLead ");
                    else
                        SQL.AppendLine("    AND T4.JOType = @JointOperationTypeForKSOMember ");

                    if (j == 0)
                        SQL.AppendLine("And LEFT(T4.DocDt, 4) < @Yr ");
                    else
                        SQL.AppendLine("And LEFT(T4.DocDt, 4) = @Yr ");
                    SQL.AppendLine("    INNER JOIN TblBOQHdr T5 ON T4.BOQDocNO = T5.DocNo ");
                    SQL.AppendLine("    INNER JOIN TblLOPHdr T6 ON T5.LOPDocNo = T6.DocNo ");
                    SQL.AppendLine("        AND FIND_IN_SET(T6.SiteCode, @SiteCode) ");
                    SQL.AppendLine("        AND T6.CancelInd = 'N' ");
                    SQL.AppendLine("        AND T6.STATUS = 'A' ");
                    SQL.AppendLine(") K ON F.DocNo = K.PRJIDocNo ");
                    SQL.AppendLine("LEFT JOIN ");
                    SQL.AppendLine("( ");
                    SQL.AppendLine("    SELECT T3.PRJIDocNo, SUM(T1.Amt) Amt ");
                    SQL.AppendLine("    FROM TblVoucherHdr T1 ");
                    SQL.AppendLine("    INNER JOIN TblDroppingPaymentHdr T2 ON T1.VoucherRequestDocNo = T2.VoucherRequestDocNo ");
                    SQL.AppendLine("        AND LEFT(T1.DocDt, 4) = (@Yr - 1) ");
                    SQL.AppendLine("        AND T1.CancelInd = 'N' ");
                    SQL.AppendLine("        AND T2.CancelInd = 'N' ");
                    SQL.AppendLine("        AND T2.STATUS = 'A' ");
                    SQL.AppendLine("    INNER JOIN TblDroppingRequestHdr T3 ON T2.DRQDocNo = T3.DocNo ");
                    SQL.AppendLine("    INNER JOIN TblProjectImplementationHdr T4 ON T3.PRJIDocNo = T4.DocNo ");
                    SQL.AppendLine("        AND T4.CancelInd = 'N' ");
                    SQL.AppendLine("    INNER JOIN TblSOContractRevisionHdr T5 ON T4.SOContractDocNo = T5.DocNo ");
                    SQL.AppendLine("    INNER JOIN TblSOContractHdr T6 ON T5.SOCDocNO = T6.DocNo ");
                    if (i == 0)
                        SQL.AppendLine("    AND T6.JOType = @JointOperationTypeForNonKSO ");
                    else if (i == 1)
                        SQL.AppendLine("    AND T6.JOType = @JointOperationTypeForKSOLead ");
                    else
                        SQL.AppendLine("    AND T6.JOType = @JointOperationTypeForKSOMember ");

                    if (j == 0)
                        SQL.AppendLine("And LEFT(T6.DocDt, 4) < @Yr ");
                    else
                        SQL.AppendLine("And LEFT(T6.DocDt, 4) = @Yr ");
                    SQL.AppendLine("    INNER JOIN TblBOQHdr T7 ON T6.BOQDocNo = T7.DocNo ");
                    SQL.AppendLine("    INNER JOIN TblLOPHdr T8 ON T7.LOPDocNo = T8.DocNo ");
                    SQL.AppendLine("        AND FIND_IN_SET(T8.SiteCode, @SiteCode) ");
                    SQL.AppendLine("        AND T8.CancelInd = 'N' ");
                    SQL.AppendLine("        AND T8.STATUS = 'A' ");
                    SQL.AppendLine(") L ON F.DocNo = L.PRJIDocNo ");
                    SQL.AppendLine("LEFT JOIN ");
                    SQL.AppendLine("( ");
                    SQL.AppendLine("    SELECT T.PRJIDocNo, SUM(T.Amt1) Amt1, SUM(T.Amt2) Amt2, SUM(T.Amt3) Amt3, SUM(T.Amt4) Amt4, SUM(T.Amt5) Amt5, SUM(T.Amt6) Amt6,  ");
                    SQL.AppendLine("    SUM(T.Amt7) Amt7, SUM(T.Amt8) Amt8, SUM(T.Amt9) Amt9, SUM(T.Amt10) Amt10, SUM(T.Amt11) Amt11, SUM(T.Amt12) Amt12 ");
                    SQL.AppendLine("    FROM ");
                    SQL.AppendLine("    ( ");
                    SQL.AppendLine("        SELECT T1.PRJIDocNo, If(SUBSTR(T1.DocDt, 5, 2) = '01', T1.Amt, 0.00) Amt1, If(SUBSTR(T1.DocDt, 5, 2) = '02', T1.Amt, 0.00) Amt2,  ");
                    SQL.AppendLine("        If(SUBSTR(T1.DocDt, 5, 2) = '03', T1.Amt, 0.00) Amt3, If(SUBSTR(T1.DocDt, 5, 2) = '04', T1.Amt, 0.00) Amt4,  ");
                    SQL.AppendLine("        If(SUBSTR(T1.DocDt, 5, 2) = '05', T1.Amt, 0.00) Amt5, If(SUBSTR(T1.DocDt, 5, 2) = '06', T1.Amt, 0.00) Amt6,  ");
                    SQL.AppendLine("        If(SUBSTR(T1.DocDt, 5, 2) = '07', T1.Amt, 0.00) Amt7, If(SUBSTR(T1.DocDt, 5, 2) = '08', T1.Amt, 0.00) Amt8,  ");
                    SQL.AppendLine("        If(SUBSTR(T1.DocDt, 5, 2) = '09', T1.Amt, 0.00) Amt9, If(SUBSTR(T1.DocDt, 5, 2) = '10', T1.Amt, 0.00) Amt10,  ");
                    SQL.AppendLine("        If(SUBSTR(T1.DocDt, 5, 2) = '11', T1.Amt, 0.00) Amt11, If(SUBSTR(T1.DocDt, 5, 2) = '12', T1.Amt, 0.00) Amt12 ");
                    SQL.AppendLine("        FROM TblProjectDeliveryHdr T1 ");
                    SQL.AppendLine("        INNER JOIN TblProjectImplementationHdr T2 ON T1.PRJIDocNo = T2.DocNo ");
                    SQL.AppendLine("            AND T1.CancelInd = 'N' ");
                    SQL.AppendLine("            AND T1.STATUS = 'A' ");
                    SQL.AppendLine("            AND LEFT(T1.DocDt, 4) = @Yr ");
                    SQL.AppendLine("        INNER JOIN TblSOContractRevisionHdr T3 ON T2.SOContractDocNo = T3.DocNo ");
                    SQL.AppendLine("        INNER JOIN TblSOContractHdr T4 ON T3.SOCDocNo = T4.DocNo ");
                    if (i == 0)
                        SQL.AppendLine("    AND T4.JOType = @JointOperationTypeForNonKSO ");
                    else if (i == 1)
                        SQL.AppendLine("    AND T4.JOType = @JointOperationTypeForKSOLead ");
                    else
                        SQL.AppendLine("    AND T4.JOType = @JointOperationTypeForKSOMember ");

                    if (j == 0)
                        SQL.AppendLine("And LEFT(T4.DocDt, 4) < @Yr ");
                    else
                        SQL.AppendLine("And LEFT(T4.DocDt, 4) = @Yr ");
                    SQL.AppendLine("        INNER JOIN TblBOQHdr T5 ON T4.BOQDocNO = T5.DocNo ");
                    SQL.AppendLine("        INNER JOIN TblLOPHdr T6 ON T5.LOPDocNo = T6.DocNo ");
                    SQL.AppendLine("            AND FIND_IN_SET(T6.SiteCode, @SiteCode) ");
                    SQL.AppendLine("            AND T6.CancelInd = 'N' ");
                    SQL.AppendLine("            AND T6.STATUS = 'A' ");
                    SQL.AppendLine("    ) T ");
                    SQL.AppendLine("    GROUP BY T.PRJIDocNo ");
                    SQL.AppendLine(") M ON F.DocNo = M.PRJIDocNo ");
                    SQL.AppendLine("LEFT JOIN ");
                    SQL.AppendLine("( ");
                    SQL.AppendLine("    SELECT T.PRJIDocNo, SUM(T.Amt1) Amt1, SUM(T.Amt2) Amt2, SUM(T.Amt3) Amt3, SUM(T.Amt4) Amt4, SUM(T.Amt5) Amt5, SUM(T.Amt6) Amt6,  ");
                    SQL.AppendLine("    SUM(T.Amt7) Amt7, SUM(T.Amt8) Amt8, SUM(T.Amt9) Amt9, SUM(T.Amt10) Amt10, SUM(T.Amt11) Amt11, SUM(T.Amt12) Amt12 ");
                    SQL.AppendLine("    FROM ");
                    SQL.AppendLine("    ( ");
                    SQL.AppendLine("        SELECT T3.PRJIDocNo, If(SUBSTR(T1.DocDt, 5, 2) = '01', T1.Amt, 0.00) Amt1, If(SUBSTR(T1.DocDt, 5, 2) = '02', T1.Amt, 0.00) Amt2,  ");
                    SQL.AppendLine("        If(SUBSTR(T1.DocDt, 5, 2) = '03', T1.Amt, 0.00) Amt3, If(SUBSTR(T1.DocDt, 5, 2) = '04', T1.Amt, 0.00) Amt4,  ");
                    SQL.AppendLine("        If(SUBSTR(T1.DocDt, 5, 2) = '05', T1.Amt, 0.00) Amt5, If(SUBSTR(T1.DocDt, 5, 2) = '06', T1.Amt, 0.00) Amt6,  ");
                    SQL.AppendLine("        If(SUBSTR(T1.DocDt, 5, 2) = '07', T1.Amt, 0.00) Amt7, If(SUBSTR(T1.DocDt, 5, 2) = '08', T1.Amt, 0.00) Amt8,  ");
                    SQL.AppendLine("        If(SUBSTR(T1.DocDt, 5, 2) = '09', T1.Amt, 0.00) Amt9, If(SUBSTR(T1.DocDt, 5, 2) = '10', T1.Amt, 0.00) Amt10,  ");
                    SQL.AppendLine("        If(SUBSTR(T1.DocDt, 5, 2) = '11', T1.Amt, 0.00) Amt11, If(SUBSTR(T1.DocDt, 5, 2) = '12', T1.Amt, 0.00) Amt12 ");
                    SQL.AppendLine("        FROM TblVoucherHdr T1 ");
                    SQL.AppendLine("        INNER JOIN TblDroppingPaymentHdr T2 ON T1.VoucherRequestDocNo = T2.VoucherRequestDocNo ");
                    SQL.AppendLine("            AND LEFT(T1.DocDt, 4) = (@Yr - 1) ");
                    SQL.AppendLine("            AND T1.CancelInd = 'N' ");
                    SQL.AppendLine("            AND T2.CancelInd = 'N' ");
                    SQL.AppendLine("            AND T2.STATUS = 'A' ");
                    SQL.AppendLine("        INNER JOIN TblDroppingRequestHdr T3 ON T2.DRQDocNo = T3.DocNo ");
                    SQL.AppendLine("        INNER JOIN TblProjectImplementationHdr T4 ON T3.PRJIDocNo = T4.DocNo ");
                    SQL.AppendLine("            AND T4.CancelInd = 'N' ");
                    SQL.AppendLine("        INNER JOIN TblSOContractRevisionHdr T5 ON T4.SOContractDocNo = T5.DocNo ");
                    SQL.AppendLine("        INNER JOIN TblSOContractHdr T6 ON T5.SOCDocNO = T6.DocNo ");
                    if (i == 0)
                        SQL.AppendLine("    AND T6.JOType = @JointOperationTypeForNonKSO ");
                    else if (i == 1)
                        SQL.AppendLine("    AND T6.JOType = @JointOperationTypeForKSOLead ");
                    else
                        SQL.AppendLine("    AND T6.JOType = @JointOperationTypeForKSOMember ");

                    if (j == 0)
                        SQL.AppendLine("And LEFT(T6.DocDt, 4) < @Yr ");
                    else
                        SQL.AppendLine("And LEFT(T6.DocDt, 4) = @Yr ");
                    SQL.AppendLine("        INNER JOIN TblBOQHdr T7 ON T6.BOQDocNo = T7.DocNo ");
                    SQL.AppendLine("        INNER JOIN TblLOPHdr T8 ON T7.LOPDocNo = T8.DocNo ");
                    SQL.AppendLine("            AND FIND_IN_SET(T8.SiteCode, @SiteCode) ");
                    SQL.AppendLine("            AND T8.CancelInd = 'N' ");
                    SQL.AppendLine("            AND T8.STATUS = 'A' ");
                    SQL.AppendLine("    ) T ");
                    SQL.AppendLine("    GROUP BY T.PRJIDocNo ");
                    SQL.AppendLine(") N ON F.DocNo = N.PRJIDocNo ");

                    if (i != (mProjectDesc.Count() - 1) || j != (mProjectType.Count() - 1))
                        SQL.AppendLine("    UNION ALL ");
                }
            }

            SQL.AppendLine(") T1 ");
            SQL.AppendLine("Inner Join TblItem T2 On T1.ItCode = T2.ItCode ");
            SQL.AppendLine("Inner Join TblCustomer T3 On T1.CtCode = T3.CtCode ");
            SQL.AppendLine("Order By T1.ProjectDesc, T1.ProjectDesc2; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
                Sm.CmParam<String>(ref cm, "@SiteCode", mSiteCode);
                Sm.CmParam<String>(ref cm, "@JointOperationTypeForKSOLead", mJointOperationTypeForKSOLead);
                Sm.CmParam<String>(ref cm, "@JointOperationTypeForKSOMember", mJointOperationTypeForKSOMember);
                Sm.CmParam<String>(ref cm, "@JointOperationTypeForNonKSO", mJointOperationTypeForNonKSO);

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                {
                    //0
                    "ProjectDesc",
                    //1-5
                    "ProjectDesc2", "ProjectCode2", "ProjectName", "Yr", "ProjectResource", 
                    //6-10
                    "ProjectScope", "ProjectType", "ItName", "CtName", "DocDt", 
                    //11-15
                    "NonPPN", "Amt", "TotalResource", "ExclPPNPercentage", "PDAmt", 
                    //16-20
                    "VAmt", "PDAmt1", "VAmt1", "PDAmt2", "VAmt2", 
                    //21-25
                    "PDAmt3", "VAmt3", "PDAmt4", "VAmt4", "PDAmt5", 
                    //26-30
                    "VAmt5", "PDAmt6", "VAmt6", "PDAmt7", "VAmt7", 
                    //31-34
                    "PDAmt8", "VAmt8", "PDAmt9", "VAmt9", "PDAmt10", 
                    //36-40
                    "VAmt10", "PDAmt11", "VAmt11", "PDAmt12", "VAmt12"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new ProductionVsReceivable()
                        {
                            ProjectDesc = Sm.DrStr(dr, c[0]),
                            ProjectDesc2 = Sm.DrStr(dr, c[1]),
                            NAS = mNas.ToString(),
                            UP = mUP.ToString(),
                            ProjectCode2 = Sm.DrStr(dr, c[2]),
                            ProjectName = Sm.DrStr(dr, c[3]),
                            Yr = Sm.DrStr(dr, c[4]),
                            ProjectResource = Sm.DrStr(dr, c[5]),
                            ProjectScope = Sm.DrStr(dr, c[6]),
                            ProjectType = Sm.DrStr(dr, c[7]),
                            ItName = Sm.DrStr(dr, c[8]),
                            CtName = Sm.DrStr(dr, c[9]),
                            DocDt = Sm.DrStr(dr, c[10]),
                            NonPPN = Sm.DrStr(dr, c[11]),
                            Amt = Sm.DrDec(dr, c[12]),
                            TotalResource = Sm.DrDec(dr, c[13]),
                            ExclPPNPercentage = Sm.DrDec(dr, c[14]),
                            BRPWorkProfit = 0m,
                            BRPWorkProfitPercentage = 0m,
                            PDAmt = Sm.DrDec(dr, c[15]),
                            VAmt = Sm.DrDec(dr, c[16]),
                            PLYBudgetPercentage = 0m,
                            PLYWorkProfit = 0m,
                            PLYWorkProfitPercentage= 0m,
                            APProduction = 0m,
                            APBudget = 0m,
                            APBudgetPercentage = 0m,
                            APWorkProfit = 0m,
                            APWorkProfitPercentage = 0m,
                            PDAmt1 = Sm.DrDec(dr, c[17]),
                            VAmt1 = Sm.DrDec(dr, c[18]),
                            WorkProfitM1 = Sm.DrDec(dr, c[17]) - Sm.DrDec(dr, c[18]),
                            PDAmt2 = Sm.DrDec(dr, c[19]),
                            VAmt2 = Sm.DrDec(dr, c[20]),
                            WorkProfitM2 = Sm.DrDec(dr, c[19]) - Sm.DrDec(dr, c[20]),
                            PDAmt3 = Sm.DrDec(dr, c[21]),
                            VAmt3 = Sm.DrDec(dr, c[22]),
                            WorkProfitM3 = Sm.DrDec(dr, c[21]) - Sm.DrDec(dr, c[22]),
                            PDTotal1 = Sm.DrDec(dr, c[17]) + Sm.DrDec(dr, c[19]) + Sm.DrDec(dr, c[21]),
                            VTotal1 = Sm.DrDec(dr, c[18]) + Sm.DrDec(dr, c[20]) + Sm.DrDec(dr, c[22]),
                            WorkProfitMTotal1 = (Sm.DrDec(dr, c[17]) - Sm.DrDec(dr, c[18])) + (Sm.DrDec(dr, c[19]) - Sm.DrDec(dr, c[20])) + (Sm.DrDec(dr, c[21]) - Sm.DrDec(dr, c[22])),
                            PDAmt4 = Sm.DrDec(dr, c[23]),
                            VAmt4 = Sm.DrDec(dr, c[24]),
                            WorkProfitM4 = Sm.DrDec(dr, c[23]) - Sm.DrDec(dr, c[24]),
                            PDAmt5 = Sm.DrDec(dr, c[25]),
                            VAmt5 = Sm.DrDec(dr, c[26]),
                            WorkProfitM5 = Sm.DrDec(dr, c[25])- Sm.DrDec(dr, c[26]),
                            PDAmt6 = Sm.DrDec(dr, c[27]),
                            VAmt6 = Sm.DrDec(dr, c[28]),
                            WorkProfitM6 = Sm.DrDec(dr, c[27]) - Sm.DrDec(dr, c[28]),
                            PDTotal2 = Sm.DrDec(dr, c[23]) + Sm.DrDec(dr, c[25]) + Sm.DrDec(dr, c[27]),
                            VTotal2 = Sm.DrDec(dr, c[24]) + Sm.DrDec(dr, c[26]) + Sm.DrDec(dr, c[28]),
                            WorkProfitMTotal2 = (Sm.DrDec(dr, c[23]) - Sm.DrDec(dr, c[24])) + (Sm.DrDec(dr, c[25]) - Sm.DrDec(dr, c[26])) + (Sm.DrDec(dr, c[27]) - Sm.DrDec(dr, c[28])),
                            PDAmt7 = Sm.DrDec(dr, c[29]),
                            VAmt7 = Sm.DrDec(dr, c[30]),
                            WorkProfitM7 = Sm.DrDec(dr, c[29]) - Sm.DrDec(dr, c[30]),
                            PDAmt8 = Sm.DrDec(dr, c[31]),
                            VAmt8 = Sm.DrDec(dr, c[32]),
                            WorkProfitM8 = Sm.DrDec(dr, c[31]) - Sm.DrDec(dr, c[32]),
                            PDAmt9 = Sm.DrDec(dr, c[33]),
                            VAmt9 = Sm.DrDec(dr, c[34]),
                            WorkProfitM9 = Sm.DrDec(dr, c[33]) - Sm.DrDec(dr, c[34]),
                            PDTotal3 = Sm.DrDec(dr, c[29]) + Sm.DrDec(dr, c[31]) + Sm.DrDec(dr, c[33]),
                            VTotal3 = Sm.DrDec(dr, c[30]) + Sm.DrDec(dr, c[32]) + Sm.DrDec(dr, c[34]),
                            WorkProfitMTotal3 = (Sm.DrDec(dr, c[29]) - Sm.DrDec(dr, c[30])) + (Sm.DrDec(dr, c[31]) - Sm.DrDec(dr, c[32])) + (Sm.DrDec(dr, c[33]) - Sm.DrDec(dr, c[34])),
                            PDAmt10 = Sm.DrDec(dr, c[35]),
                            VAmt10 = Sm.DrDec(dr, c[36]),
                            WorkProfitM10 = Sm.DrDec(dr, c[35]) - Sm.DrDec(dr, c[36]),
                            PDAmt11 = Sm.DrDec(dr, c[37]),
                            VAmt11 = Sm.DrDec(dr, c[38]),
                            WorkProfitM11 = Sm.DrDec(dr, c[37]) - Sm.DrDec(dr, c[38]),
                            PDAmt12 = Sm.DrDec(dr, c[39]),
                            VAmt12 = Sm.DrDec(dr, c[40]),
                            WorkProfitM12 = Sm.DrDec(dr, c[39]) - Sm.DrDec(dr, c[40]),
                            PDTotal4 = Sm.DrDec(dr, c[35]) + Sm.DrDec(dr, c[37]) + Sm.DrDec(dr, c[39]),
                            VTotal4 = Sm.DrDec(dr, c[36]) + Sm.DrDec(dr, c[38]) + Sm.DrDec(dr, c[40]),
                            WorkProfitMTotal4 = (Sm.DrDec(dr, c[35]) - Sm.DrDec(dr, c[36])) + (Sm.DrDec(dr, c[37]) - Sm.DrDec(dr, c[38])) + (Sm.DrDec(dr, c[39]) - Sm.DrDec(dr, c[40])),
                            TPProduction = 0m,
                            TPBudget = 0m,
                            TPBudgetPercentage= 0m,
                            TPWorkProfit = 0m,
                            TPWorkProfitPercentage = 0m,
                            COProduction = 0m,
                            COBudget = 0m,
                            COBudgetPercentage = 0m,
                            COWorkProfit = 0m,
                            COWorkProfitPercentage = 0m
                        });

                        mUP += 1;
                        mNas += 1;
                    }
                }
                dr.Close();
            }
        }

        private void Process2(ref List<ProductionVsReceivable> l)
        {
            foreach (var x in l)
            {
                x.BRPWorkProfit = x.Amt - x.TotalResource;
                if (x.Amt != 0m) x.BRPWorkProfitPercentage = (x.BRPWorkProfit / x.Amt) * 100m;
                x.PLYWorkProfit = x.PDAmt - x.VAmt;
                if (x.PDAmt != 0m)
                {
                    x.PLYBudgetPercentage = (x.VAmt / x.PDAmt) * 100m;
                    x.PLYWorkProfitPercentage = (x.PLYWorkProfit / x.PDAmt) * 100m;
                }
                x.APProduction = x.Amt - x.PDAmt;
                x.APBudget = x.TotalResource - x.VAmt;
                x.APWorkProfit = x.BRPWorkProfit - x.PLYWorkProfit;
                if (x.APProduction != 0m)
                {
                    x.APBudgetPercentage = (x.APBudget / x.APProduction) * 100m;
                    x.APWorkProfitPercentage = (x.APWorkProfit / x.APProduction) * 100m;
                }
                x.TPProduction = x.PDTotal1 + x.PDTotal2 + x.PDTotal3 + x.PDTotal4;
                x.TPBudget = x.VTotal1 + x.VTotal2 + x.VTotal3 + x.VTotal4;
                x.TPWorkProfit = x.WorkProfitMTotal1 + x.WorkProfitMTotal2 + x.WorkProfitMTotal3 + x.WorkProfitMTotal4;
                if (x.TPProduction != 0m)
                {
                    x.TPBudgetPercentage = (x.TPBudget / x.TPProduction) * 100m;
                    x.TPWorkProfitPercentage = (x.TPWorkProfit / x.TPProduction) * 100m;
                }
                x.COProduction = x.APProduction - x.TPProduction;
                x.COBudget = x.APBudget - x.TPBudget;
                x.COWorkProfit = x.APWorkProfit - x.TPWorkProfit;
                if (x.COProduction != 0m)
                {
                    x.COBudgetPercentage = (x.COBudget / x.COProduction) * 100m;
                    x.COWorkProfitPercentage = (x.COWorkProfit / x.COProduction) * 100m;
                }
            }
        }

        private void Process3(ref List<ProductionVsReceivable> l)
        {
            int Row = 0;
            foreach (var x in l)
            {
                Grd1.Rows.Add();

                Grd1.Cells[Row, 0].Value = Row + 1;
                Grd1.Cells[Row, 1].Value = x.ProjectDesc;
                Grd1.Cells[Row, 2].Value = x.ProjectDesc2;
                Grd1.Cells[Row, 3].Value = x.NAS;
                Grd1.Cells[Row, 4].Value = x.UP;
                Grd1.Cells[Row, 5].Value = x.ProjectCode2;
                Grd1.Cells[Row, 6].Value = x.ProjectName;
                Grd1.Cells[Row, 7].Value = x.Yr;
                Grd1.Cells[Row, 8].Value = x.ProjectResource;
                Grd1.Cells[Row, 9].Value = x.ProjectScope;
                Grd1.Cells[Row, 10].Value = x.ProjectType;
                Grd1.Cells[Row, 11].Value = x.ItName;
                Grd1.Cells[Row, 12].Value = x.CtName;
                Grd1.Cells[Row, 13].Value = x.DocDt;
                Grd1.Cells[Row, 14].Value = x.NonPPN;
                Grd1.Cells[Row, 15].Value = x.Amt;
                Grd1.Cells[Row, 16].Value = x.TotalResource;
                Grd1.Cells[Row, 17].Value = x.ExclPPNPercentage;
                Grd1.Cells[Row, 18].Value = x.BRPWorkProfit;
                Grd1.Cells[Row, 19].Value = x.BRPWorkProfitPercentage;
                Grd1.Cells[Row, 20].Value = x.PDAmt;
                Grd1.Cells[Row, 21].Value = x.VAmt;
                Grd1.Cells[Row, 22].Value = x.PLYBudgetPercentage;
                Grd1.Cells[Row, 23].Value = x.PLYWorkProfit;
                Grd1.Cells[Row, 24].Value = x.PLYWorkProfitPercentage;
                Grd1.Cells[Row, 25].Value = x.APProduction;
                Grd1.Cells[Row, 26].Value = x.APBudget;
                Grd1.Cells[Row, 27].Value = x.APBudgetPercentage;
                Grd1.Cells[Row, 28].Value = x.APWorkProfit;
                Grd1.Cells[Row, 29].Value = x.APWorkProfitPercentage;
                Grd1.Cells[Row, 30].Value = x.PDAmt1;
                Grd1.Cells[Row, 31].Value = x.VAmt1;
                Grd1.Cells[Row, 32].Value = x.WorkProfitM1;
                Grd1.Cells[Row, 33].Value = x.PDAmt2;
                Grd1.Cells[Row, 34].Value = x.VAmt2;
                Grd1.Cells[Row, 35].Value = x.WorkProfitM2;
                Grd1.Cells[Row, 36].Value = x.PDAmt3;
                Grd1.Cells[Row, 37].Value = x.VAmt3;
                Grd1.Cells[Row, 38].Value = x.WorkProfitM3;
                Grd1.Cells[Row, 39].Value = x.PDTotal1;
                Grd1.Cells[Row, 40].Value = x.VTotal1;
                Grd1.Cells[Row, 41].Value = x.WorkProfitMTotal1;
                Grd1.Cells[Row, 42].Value = x.PDAmt4;
                Grd1.Cells[Row, 43].Value = x.VAmt4;
                Grd1.Cells[Row, 44].Value = x.WorkProfitM4;
                Grd1.Cells[Row, 45].Value = x.PDAmt5;
                Grd1.Cells[Row, 46].Value = x.VAmt5;
                Grd1.Cells[Row, 47].Value = x.WorkProfitM5;
                Grd1.Cells[Row, 48].Value = x.PDAmt6;
                Grd1.Cells[Row, 49].Value = x.VAmt6;
                Grd1.Cells[Row, 50].Value = x.WorkProfitM6;
                Grd1.Cells[Row, 51].Value = x.PDTotal2;
                Grd1.Cells[Row, 52].Value = x.VTotal2;
                Grd1.Cells[Row, 53].Value = x.WorkProfitMTotal2;
                Grd1.Cells[Row, 54].Value = x.PDAmt7;
                Grd1.Cells[Row, 55].Value = x.VAmt7;
                Grd1.Cells[Row, 56].Value = x.WorkProfitM7;
                Grd1.Cells[Row, 57].Value = x.PDAmt8;
                Grd1.Cells[Row, 58].Value = x.VAmt8;
                Grd1.Cells[Row, 59].Value = x.WorkProfitM8;
                Grd1.Cells[Row, 60].Value = x.PDAmt9;
                Grd1.Cells[Row, 61].Value = x.VAmt9;
                Grd1.Cells[Row, 62].Value = x.WorkProfitM9;
                Grd1.Cells[Row, 63].Value = x.PDTotal3;
                Grd1.Cells[Row, 64].Value = x.VTotal3;
                Grd1.Cells[Row, 65].Value = x.WorkProfitMTotal3;
                Grd1.Cells[Row, 66].Value = x.PDAmt10;
                Grd1.Cells[Row, 67].Value = x.VAmt10;
                Grd1.Cells[Row, 68].Value = x.WorkProfitM10;
                Grd1.Cells[Row, 69].Value = x.PDAmt11;
                Grd1.Cells[Row, 70].Value = x.VAmt11;
                Grd1.Cells[Row, 71].Value = x.WorkProfitM11;
                Grd1.Cells[Row, 72].Value = x.PDAmt12;
                Grd1.Cells[Row, 73].Value = x.VAmt12;
                Grd1.Cells[Row, 74].Value = x.WorkProfitM12;
                Grd1.Cells[Row, 75].Value = x.PDTotal4;
                Grd1.Cells[Row, 76].Value = x.VTotal4;
                Grd1.Cells[Row, 77].Value = x.WorkProfitMTotal4;
                Grd1.Cells[Row, 78].Value = x.TPProduction;
                Grd1.Cells[Row, 79].Value = x.TPBudget;
                Grd1.Cells[Row, 80].Value = x.TPBudgetPercentage;
                Grd1.Cells[Row, 81].Value = x.TPWorkProfit;
                Grd1.Cells[Row, 82].Value = x.TPWorkProfitPercentage;
                Grd1.Cells[Row, 83].Value = x.COProduction;
                Grd1.Cells[Row, 84].Value = x.COBudget;
                Grd1.Cells[Row, 85].Value = x.COBudgetPercentage;
                Grd1.Cells[Row, 86].Value = x.COWorkProfit;
                Grd1.Cells[Row, 87].Value = x.COWorkProfitPercentage;

                Row += 1;
            }

            Grd1.GroupObject.Add(1);
            Grd1.GroupObject.Add(2);
            Grd1.Group();
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.ShowSubtotals(Grd1, mColDec);
        }


        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue1(SetLueSiteCode));
        }

        #endregion

        #endregion

        #region Class

        private class ProductionVsReceivable
        {
            public string ProjectDesc { get; set; }
            public string ProjectDesc2 { get; set; }
            public string NAS { get; set; }
            public string UP { get; set; }
            public string ProjectCode2 { get; set; }
            public string ProjectName { get; set; }
            public string Yr { get; set; }
            public string ProjectResource { get; set; }
            public string ProjectScope { get; set; }
            public string ProjectType { get; set; }
            public string ItName { get; set; }
            public string CtName { get; set; }
            public string DocDt { get; set; }
            public string NonPPN { get; set; }
            public decimal Amt { get; set; }
            public decimal TotalResource { get; set; }
            public decimal ExclPPNPercentage { get; set; }
            public decimal BRPWorkProfit { get; set; }
            public decimal BRPWorkProfitPercentage { get; set; }
            public decimal PDAmt { get; set; }
            public decimal VAmt { get; set; }
            public decimal PLYBudgetPercentage { get; set; }
            public decimal PLYWorkProfit { get; set; }
            public decimal PLYWorkProfitPercentage { get; set; }
            public decimal APProduction { get; set; }
            public decimal APBudget { get; set; }
            public decimal APBudgetPercentage { get; set; }
            public decimal APWorkProfit { get; set; }
            public decimal APWorkProfitPercentage { get; set; }
            public decimal PDAmt1 { get; set; }
            public decimal VAmt1 { get; set; }
            public decimal WorkProfitM1 { get; set; }
            public decimal PDAmt2 { get; set; }
            public decimal VAmt2 { get; set; }
            public decimal WorkProfitM2 { get; set; }
            public decimal PDAmt3 { get; set; }
            public decimal VAmt3 { get; set; }
            public decimal WorkProfitM3 { get; set; }
            public decimal PDTotal1 { get; set; }
            public decimal VTotal1 { get; set; }
            public decimal WorkProfitMTotal1 { get; set; }
            public decimal PDAmt4 { get; set; }
            public decimal VAmt4 { get; set; }
            public decimal WorkProfitM4 { get; set; }
            public decimal PDAmt5 { get; set; }
            public decimal VAmt5 { get; set; }
            public decimal WorkProfitM5 { get; set; }
            public decimal PDAmt6 { get; set; }
            public decimal VAmt6 { get; set; }
            public decimal WorkProfitM6 { get; set; }
            public decimal PDTotal2 { get; set; }
            public decimal VTotal2 { get; set; }
            public decimal WorkProfitMTotal2 { get; set; }
            public decimal PDAmt7 { get; set; }
            public decimal VAmt7 { get; set; }
            public decimal WorkProfitM7 { get; set; }
            public decimal PDAmt8 { get; set; }
            public decimal VAmt8 { get; set; }
            public decimal WorkProfitM8 { get; set; }
            public decimal PDAmt9 { get; set; }
            public decimal VAmt9 { get; set; }
            public decimal WorkProfitM9 { get; set; }
            public decimal PDTotal3 { get; set; }
            public decimal VTotal3 { get; set; }
            public decimal WorkProfitMTotal3 { get; set; }
            public decimal PDAmt10 { get; set; }
            public decimal VAmt10 { get; set; }
            public decimal WorkProfitM10 { get; set; }
            public decimal PDAmt11 { get; set; }
            public decimal VAmt11 { get; set; }
            public decimal WorkProfitM11 { get; set; }
            public decimal PDAmt12 { get; set; }
            public decimal VAmt12 { get; set; }
            public decimal WorkProfitM12 { get; set; }
            public decimal PDTotal4 { get; set; }
            public decimal VTotal4 { get; set; }
            public decimal WorkProfitMTotal4 { get; set; }
            public decimal TPProduction { get; set; }
            public decimal TPBudget { get; set; }
            public decimal TPBudgetPercentage { get; set; }
            public decimal TPWorkProfit { get; set; }
            public decimal TPWorkProfitPercentage { get; set; }
            public decimal COProduction { get; set; }
            public decimal COBudget { get; set; }
            public decimal COBudgetPercentage { get; set; }
            public decimal COWorkProfit { get; set; }
            public decimal COWorkProfitPercentage { get; set; }
        }

        private class AuctionBranchToSite
        {
            public string BranchCode { get; set; }
            public string SiteCode { get; set; }
        }

        private class SiteCounter
        {
            public string SiteCode { get; set; }
            public int No { get; set; }
            public int No2 { get; set; }
        }

        private class DummyCounter
        {
            public string SiteCode { get; set; }
            public int No { get; set; }
        }

        #endregion
    }
}

