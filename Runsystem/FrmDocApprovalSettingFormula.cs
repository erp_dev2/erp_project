﻿#region Update
/*
    09/06/2021 [SET/PHT] Menu baru Document Approval Setting Formula
    28/06/2021 [TKG/PHT] Tambah fasilitas untuk mengupdate document approval setting
    06/07/2021 [TKG/PHT] bug isi file type approval
    13/07/2021 [TKG/PHT] dept, site, employee level dibagi jadi 2 : untuk setting dan approver
    16/07/2021 [TKG/PHT] judul level diubah menjadi Setting level
    21/07/2021 [TKG/PHT] menghilangkan approver level
    13/08/2021 [TKG/PHT] level tidak menggunakan link ke table level lagi
    24/09/2021 [TKG/PHT] Ketika updating proses data di dokumen approval setting formula karyawan yang sudah resign atau pensiun masih menjadi approval seharusnya tidak menjadi approval
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmDocApprovalSettingFormula : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmDocApprovalSettingFormulaFind FrmFind;
        private bool IsInsert = false;

        #endregion

        #region Constructor

        public FrmDocApprovalSettingFormula(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                BtnPrint.Visible = false;
                
                Tc1.SelectedTabPage = Tp2;
                SetLueDocType(ref LueApprovalType);
                Sl.SetLueSiteCode2(ref LueSiteCodeFilter, string.Empty);
                Sl.SetLueDeptCode(ref LueDeptCodeFilter, string.Empty, "N");
                
                Tc1.SelectedTabPage = Tp1;
                
                Sl.SetLueSiteCode2(ref LueSiteCode, string.Empty);
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, "N");
                Sl.SetLuePosCode(ref LuePosCode);
                SetFormControl(mState.View);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        LueDeptCode, LueSiteCode, LuePosCode
                    }, true);
                    BtnDocApprovalSetting.Enabled = false;
                    Tc1.SelectedTabPage = Tp1;
                    Tp2.PageVisible = true;
                    TxtTypeApproval.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { LueDeptCode, LueSiteCode, LuePosCode }, false);
                    BtnDocApprovalSetting.Enabled = true;
                    Tc1.SelectedTabPage = Tp1;
                    Tp2.PageVisible = false;
                    TxtTypeApproval.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { LueDeptCode, LueSiteCode, LuePosCode }, false);
                    Tc1.SelectedTabPage = Tp1;
                    Tp2.PageVisible = false;
                    TxtLevel.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtTypeApproval, TxtLevel, TxtDASDeptCode, TxtDASSiteCode, TxtDASLevelCode, 
                LueDeptCode, LueSiteCode, LuePosCode
            });
            Sm.SetControlNumValueZero(new List<DevExpress.XtraEditors.TextEdit> { TxtLevel }, 2);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmDocApprovalSettingFormulaFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            IsInsert = true;
            ClearData();
            SetFormControl(mState.Insert);
            Sm.FormShowDialog(new FrmDocApprovalSettingFormulaDlg(this));
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            IsInsert = false;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtTypeApproval, string.Empty, false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() 
                { CommandText = "Delete From TblDocApprovalFormula Where TypeApproval=@TypeApproval And Level=@Level;" };
                Sm.CmParam<String>(ref cm, "@TypeApproval", TxtTypeApproval.Text);
                Sm.CmParam<Decimal>(ref cm, "@Level", decimal.Parse(TxtLevel.Text));
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Set @Dt:=CurrentDateTime();");

                if (IsInsert)
                {
                    SQL.AppendLine("Insert Into TblDocApprovalFormula ");
                    SQL.AppendLine("(TypeApproval, Level, DASDeptCode, DASSiteCode, DASLevelCode, DeptCode, SiteCode, PosCode, CreateBy, CreateDt) ");
                    SQL.AppendLine("Select DocType, Level, DeptCode, SiteCode, LevelCode, @DeptCode, @SiteCode, @PosCode, @UserCode, @Dt ");
                    SQL.AppendLine("From TblDocApprovalSetting ");
                    SQL.AppendLine("Where DocType=@TypeApproval And Level=@Level Limit 1; ");
                }
                else
                {
                    SQL.AppendLine("Update TblDocApprovalFormula Set ");
                    SQL.AppendLine("    SiteCode=@SiteCode, DeptCode=@DeptCode, PosCode=@PosCode, LastUpBy=@UserCode, LastUpDt=@Dt ");
                    SQL.AppendLine("Where TypeApproval=@TypeApproval And Level=@Level; ");
                }

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };

                Sm.CmParam<String>(ref cm, "@TypeApproval", TxtTypeApproval.Text);
                Sm.CmParam<Decimal>(ref cm, "@Level", decimal.Parse(TxtLevel.Text));
                //Sm.CmParam<String>(ref cm, "@DASDeptCode", TxtDASDeptCode.Text);
                //Sm.CmParam<String>(ref cm, "@DASSiteCode", TxtDASSiteCode.Text);
                //Sm.CmParam<String>(ref cm, "@DASLevelCode", TxtDASLevelCode.Text);
                Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
                Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
                Sm.CmParam<String>(ref cm, "@PosCode", Sm.GetLue(LuePosCode));
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                Sm.ExecCommand(cm);

                ShowData(TxtTypeApproval.Text, decimal.Parse(TxtLevel.Text));
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string TypeApproval, decimal Level)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var SQL = new StringBuilder();
                var cm = new MySqlCommand();

                SQL.AppendLine("Select B.SiteName, C.DeptName, A.DASLevelCode, ");
                SQL.AppendLine("A.DeptCode, A.SiteCode, A.PosCode ");
                SQL.AppendLine("From TblDocApprovalFormula A ");
                SQL.AppendLine("Left Join TblSite B On A.DASSiteCode=B.SiteCode ");
                SQL.AppendLine("Left Join TblDepartment C On A.DASDeptCode=C.DeptCode ");
                SQL.AppendLine("Where TypeApproval=@TypeApproval ");
                SQL.AppendLine("And Level=@Level; ");
                
                Sm.CmParam<String>(ref cm, "@TypeApproval", TypeApproval);
                Sm.CmParam<decimal>(ref cm, "@Level", Level);
                
                Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        "SiteName", 
                        "DeptName", "DASLevelCode", "SiteCode", "DeptCode", "PosCode" 
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtTypeApproval.EditValue = TypeApproval;
                        TxtLevel.EditValue = Level;
                        TxtDASSiteCode.EditValue = Sm.DrStr(dr, c[0]);
                        TxtDASDeptCode.EditValue = Sm.DrStr(dr, c[1]);
                        TxtDASLevelCode.EditValue = Sm.DrStr(dr, c[2]);
                        Sm.SetLue(LueSiteCode, Sm.DrStr(dr, c[3]));
                        Sm.SetLue(LueDeptCode, Sm.DrStr(dr, c[4]));
                        Sm.SetLue(LuePosCode, Sm.DrStr(dr, c[5]));
                    }, true
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #endregion

        #region Additional Method

        internal void SetLueDocType(ref DXE.LookUpEdit Lue)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select OptCode As Col1, OptDesc As Col2 ");
                SQL.AppendLine("From TblOption Where OptCat='ApprovalType' ");
                SQL.AppendLine("And Find_In_Set(OptCode, ( ");
                SQL.AppendLine("    Select ParValue From TblParameter ");
                SQL.AppendLine("    Where ParCode='ApprovalTypeForDocApprovalFormula' And ParValue Is Not Null ");
                SQL.AppendLine(")) Order By OptDesc;");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                
                Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Type", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtTypeApproval, "Approval type", false) ||
                Sm.IsLueEmpty(LueDeptCode, "Approver's department") ||
                Sm.IsLueEmpty(LueSiteCode, "Approver's site") ||
                Sm.IsLueEmpty(LuePosCode, "Position") ||
                IsFormulaAlreadyExisted();
        }

        private bool IsFormulaAlreadyExisted()
        {
            if (TxtTypeApproval.Properties.ReadOnly) return false;

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select 1 ");
            SQL.AppendLine("From TblDocApprovalFormula ");
            SQL.AppendLine("Where TypeApproval=@TypeApproval And Level=@Level;");

            cm.CommandText = SQL.ToString();
            
            Sm.CmParam<String>(ref cm, "@TypeApproval", TxtTypeApproval.Text);
            Sm.CmParam<Decimal>(ref cm, "@Level", decimal.Parse(TxtLevel.Text));
            
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This formula already existed.");
                return true;
            }
            return false;
        }

        private bool IsSettingNotExisted()
        {
            if (!TxtTypeApproval.Properties.ReadOnly) return false;

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select 1 ");
            SQL.AppendLine("From TblDocApprovalSetting ");
            SQL.AppendLine("Where DocType=@DocType And Level=@Level;");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocType", TxtTypeApproval.Text);
            Sm.CmParam<Decimal>(ref cm, "@Level", decimal.Parse(TxtLevel.Text));

            if (!Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This setting already not existed.");
                return true;
            }
            return false;
        }

        private void Process1(ref List<DocApprovalFormula> l1)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select TypeApproval, Level, DASDeptCode, DASSiteCode, DASLevelCode, DeptCode, SiteCode, PosCode ");
            SQL.AppendLine("From TblDocApprovalFormula Where 1=1 ");
            if (ChkApprovalType.Checked)
                SQL.AppendLine("And TypeApproval='" + Sm.GetLue(LueApprovalType) + "' ");
            if (ChkDeptCodeFilter.Checked)
            {
                SQL.AppendLine("And DASDeptCode=@DeptCode ");
                Sm.CmParam<string>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCodeFilter));
            }
            if (ChkSiteCodeFilter.Checked)
            {
                SQL.AppendLine("And DASSiteCode=@SiteCode; ");
                Sm.CmParam<string>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCodeFilter));
            }

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    "TypeApproval", 
                    "Level", "DASDeptCode", "DASSiteCode", "DASLevelCode", "DeptCode", 
                    "SiteCode", "PosCode"
                    
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l1.Add(new DocApprovalFormula()
                        {
                            TypeApproval = Sm.DrStr(dr, c[0]),
                            Level = Sm.DrDec(dr, c[1]),
                            DASDeptCode = Sm.DrStr(dr, c[2]),
                            DASSiteCode = Sm.DrStr(dr, c[3]),
                            DASLevelCode = Sm.DrStr(dr, c[4]),
                            DeptCode = Sm.DrStr(dr, c[5]),
                            SiteCode = Sm.DrStr(dr, c[6]),
                            PosCode = Sm.DrStr(dr, c[7]),
                            UserCode = string.Empty
                        });
                    }
                }
                dr.Close();
            }
        }

        //private void Process2(ref List<DocApprovalFormula> l1, ref List<DocApprovalFormula> l2)
        //{
        //    var  l = new List<string>();
        //    foreach(var x in l1)
        //    {
        //        l= x.TypeApproval.Split(',').ToList();
        //        if (l.Count > 0)
        //        {
        //            foreach (var x2 in l)
        //            {
        //                l2.Add(new DocApprovalFormula()
        //                {
        //                    TypeApproval = x2.Trim(),
        //                    Level = x.Level,
        //                    DASDeptCode = x.DASDeptCode,
        //                    DASSiteCode = x.DASSiteCode,
        //                    DASLevelCode = x.DASLevelCode,
        //                    DeptCode = x.DeptCode,
        //                    SiteCode = x.SiteCode,
        //                    LevelCode = x.LevelCode,
        //                    PosCode = x.PosCode,
        //                    UserCode = string.Empty
        //                });
        //            }
        //            l.Clear();
        //        }
        //    }
        //    l1.Clear();
        //}

        private void Process2(ref List<DocApprovalFormula> l1, ref List<Data> l2)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            var subSQL = new StringBuilder();
            int i = 0;

            SQL.AppendLine("Select DeptCode, SiteCode, PosCode, UserCode ");
            SQL.AppendLine("From TblEmployee ");
            SQL.AppendLine("Where DeptCode Is Not Null ");
            SQL.AppendLine("And SiteCode Is Not Null ");
            SQL.AppendLine("And PosCode Is Not Null ");
            SQL.AppendLine("And UserCode Is Not Null ");
            SQL.AppendLine("And (ResignDt Is Null Or (ResignDt Is Not Null And ResignDt>@Dt))  ");

            foreach (var x in l1)
            {
                if (i > 0) subSQL.AppendLine(" Or ");
                subSQL.AppendLine("(DeptCode=@DeptCode_" + i.ToString());
                subSQL.AppendLine(" And SiteCode=@SiteCode_" + i.ToString());
                subSQL.AppendLine(" And PosCode=@PosCode_"+i.ToString() + ") ");

                Sm.CmParam<string>(ref cm, "@DeptCode_" + i.ToString(), x.DeptCode);
                Sm.CmParam<string>(ref cm, "@SiteCode_" + i.ToString(), x.SiteCode);
                Sm.CmParam<string>(ref cm, "@PosCode_" + i.ToString(), x.PosCode);

                i++;
            }
            SQL.AppendLine("And (" + subSQL.ToString() + "); ");

            Sm.CmParamDt(ref cm, "@Dt", Sm.ServerCurrentDateTime());
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    "DeptCode", 
                    "SiteCode", "PosCode", "UserCode"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l2.Add(new Data()
                        {
                            DeptCode = Sm.DrStr(dr, c[0]),
                            SiteCode = Sm.DrStr(dr, c[1]),
                            PosCode = Sm.DrStr(dr, c[2]),
                            UserCode = Sm.DrStr(dr, c[3])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process3(ref List<DocApprovalFormula> l1, ref List<Data> l2)
        {
            foreach (var x in l1)
            {
                foreach (var i in l2.Where(w =>
                    Sm.CompareStr(w.DeptCode, x.DeptCode) &&
                    Sm.CompareStr(w.SiteCode, x.SiteCode) &&
                    Sm.CompareStr(w.PosCode, x.PosCode)))
                    x.UserCode = string.Concat(x.UserCode, "##", i.UserCode, "##");
            }
            l2.Clear();
        }

        private void Process4(ref List<DocApprovalFormula> l1)
        {
            var cml = new List<MySqlCommand>();
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            int i = 0;

            SQL.AppendLine("Set @Dt:= CurrentDateTime(); ");
            foreach (var x in l1)
            {
                SQL.AppendLine("Update TblDocApprovalSetting Set ");
                SQL.AppendLine("    UserCode=@UserCode_" +i.ToString()+ ", ");
                SQL.AppendLine("    LastUpBy=@UserCode, ");
                SQL.AppendLine("    LastUpDt=@Dt ");
                SQL.AppendLine("Where DocType=@DocType_" + i.ToString());
                SQL.AppendLine(" And Level=@Level_" + i.ToString());
                SQL.AppendLine("; ");

                Sm.CmParam<string>(ref cm, "@DocType_" + i.ToString(), x.TypeApproval);
                Sm.CmParam<decimal>(ref cm, "@Level_" + i.ToString(), x.Level);
                Sm.CmParam<string>(ref cm, "@UserCode_" + i.ToString(), x.UserCode);

                i++;
            }
            l1.Clear();

            Sm.CmParam<string>(ref cm, "@UserCode", Gv.CurrentUserCode);
            cm.CommandText = SQL.ToString();
            cml.Add(cm);
            Sm.ExecCommands(cml);
        }

        #endregion

        #region Event

        #region Misc Control Event

        private void BtnProcess_Click(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Question", "Do you want to update approval setting ?") == DialogResult.No) return;

            var l1 = new List<DocApprovalFormula>();
            var l2 = new List<Data>();

            Cursor.Current = Cursors.WaitCursor;

            try
            {
                Process1(ref l1);
                if (l1.Count > 0)
                {
                    Process2(ref l1, ref l2);
                    Process3(ref l1, ref l2);
                    Process4(ref l1);
                    Sm.StdMsg(mMsgType.Info, "Process is completed.");   
                }
                else
                    Sm.StdMsg(mMsgType.NoData, string.Empty);   
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                l1.Clear();
                l2.Clear();
                Cursor.Current = Cursors.Default;
            }
        }

        private void BtnDocApprovalSetting_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmDocApprovalSettingFormulaDlg(this));
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, "N");
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue2(Sl.SetLueSiteCode2), string.Empty);
        }

        private void LuePosCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LuePosCode, new Sm.RefreshLue1(Sl.SetLuePosCode));
        }

        private void LueDeptCodeFilter_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCodeFilter, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCodeFilter_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void LueSiteCodeFilter_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue2(Sl.SetLueSiteCode2), string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkSiteCodeFilter_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }

        private void LueApprovalType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueApprovalType, new Sm.RefreshLue1(SetLueDocType));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkApprovalType_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Approval type");
        }

        #endregion

        #endregion

        #region Class

        private class DocApprovalFormula
        {
            public string TypeApproval { get; set; }
            public decimal Level { get; set; }
            public string DASDeptCode { get; set; }
            public string DASSiteCode { get; set; }
            public string DASLevelCode { get; set; }
            public string DeptCode { get; set; }
            public string SiteCode { get; set; }
            public string LevelCode { get; set; }
            public string PosCode { get; set; }
            public string UserCode { get; set; }
        }

        private class Data
        {
            public string DeptCode { get; set; }
            public string SiteCode { get; set; }
            public string LevelCode { get; set; }
            public string PosCode { get; set; }
            public string UserCode { get; set; }
        }

        #endregion
    }
}
