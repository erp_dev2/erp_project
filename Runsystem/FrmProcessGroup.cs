﻿#region Update
/*
    20/03/2018 [TKG] new application
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;
using DevExpress.XtraEditors;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmProcessGroup : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmProcessGroupFind FrmFind;

        #endregion

        #region Constructor

        public FrmProcessGroup(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetFormControl(mState.View);
                SetLuePGCode(ref LueParent);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { TxtPGCode,TxtPGName, LueParent }, true);
                    TxtPGCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { TxtPGCode,TxtPGName, LueParent }, false);
                    TxtPGCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(TxtPGCode, true);
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { TxtPGName, LueParent }, false);
                    TxtPGName.Focus();
                    break;
                default:
                    break;
            }
        }
        #endregion

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            { TxtPGCode,TxtPGName, LueParent });
        }

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmProcessGroupFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtPGCode, "", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cml = new List<MySqlCommand>();

                cml.Add(DeleteProcessGroup(TxtPGCode.Text));

                Sm.ExecCommands(cml);

                BtnCancelClick(sender, e);

            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var cml = new List<MySqlCommand>();

                cml.Add(SaveProcessGroup());

                Sm.ExecCommands(cml);

                ShowData(TxtPGCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string PGCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                ShowProcessGroup(PGCode);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowProcessGroup(string PGCode)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@PGCode", PGCode);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select PGCode, PGName, Parent FROM TblProcessGroup WHERE PGCode=@PGCode;",
                    new string[] { "PGCode", "PGName", "Parent" },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtPGCode.EditValue = Sm.DrStr(dr, c[0]);
                        TxtPGName.EditValue = Sm.DrStr(dr, c[1]);
                        Sm.SetLue(LueParent, Sm.DrStr(dr, c[2]));
                    }, true
                );
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtPGCode, "Code", false) ||
                Sm.IsTxtEmpty(TxtPGName, "Name", false) ||
                IsPGCodeExisted();
        }

        private bool IsPGCodeExisted()
        {
            if (TxtPGCode.Properties.ReadOnly) return false;

            return Sm.IsDataExist(
                    "Select 1 From TblProcessGroup Where PGCode=@Param Limit 1;",
                    TxtPGCode.Text,
                    "Code already existed.");
        }

        private MySqlCommand SaveProcessGroup()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblProcessGroup(PGCode, PGName, Parent, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@PGCode, @PGName, @Parent, @UserCode, CurrentDateTime()) ");
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("   Update PGName=@PGName, Parent=@Parent, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

            
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@PGCode", TxtPGCode.Text);
            Sm.CmParam<String>(ref cm, "@PGName", TxtPGName.Text);
            Sm.CmParam<String>(ref cm, "@Parent", Sm.GetLue(LueParent));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Delete Data

        private MySqlCommand DeleteProcessGroup(string PGCode)
        {
            var cm = new MySqlCommand() { CommandText = "DELETE FROM TblProcessGroup WHERE PGCode=@PGCode;" };
            Sm.CmParam<String>(ref cm, "@PGCode", TxtPGCode.Text);
            return cm;
        }

        #endregion

        #region Additional Method

        private void SetLuePGCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select PGCode As Col1, PGName As Col2 From TblProcessGroup Order By PGName;",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtPGCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtPGCode);
        }

        private void TxtPGName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtPGName);
        }

        private void LueParent_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueParent, new Sm.RefreshLue1(SetLuePGCode));
        }

        #endregion

        #endregion
    }
}
