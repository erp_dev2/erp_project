﻿#region Update
/*
    05/10/2017 [TKG] New Employee Service Charge Incentive  
    10/10/2017 [TKG] validasi warning letter yg sudah dicancel tidak akan diproses.
    06/01/2018 [TKG] menggunakan SCI deduction formula untuk menghitung % potongan
    07/01/2018 [TKG] menggunakan file untuk memproses data.
    08/01/2018 [TKG] tambah jumlah karyawan dan fasilitas hapus karyawan.
    09/03/2018 [TKG] saat upload file, kode karyawan kalau panjangnya kurang dari 8 karakter, ditambah 0 di depan. 
    16/11/2018 [TKG] bug saat proses resignee
    21/01/2019 [TKG] tambah validasi kalau karyawan sudah pensiun dan tanggal resign lebih kecil atau sama dengan tanggal dokumen maka akan tetap muncul
    05/04/2019 [TKG] Berdasarkan parameter SCICanBeProcessAfterResignInMonths, X bulan setelah resign atau pensiun, Service charge masih bisa diproses.
    11/04/2019 [TKG] service charge karyawan yg sudah resign masih bisa diproses setelah 1 bulan tgl end date allowance-nya. 
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using System.IO;
using FastReport;
using FastReport.Data;

#endregion

namespace RunSystem
{
    public partial class FrmEmpSCI : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmEmpSCIFind FrmFind;
        private string mEmpJobTransferPension = string.Empty;
        private decimal mSCICanBeProcessAfterResignInMonths = 0m;

        #endregion

        #region Constructor

        public FrmEmpSCI(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Employee's Service Charge Incentive";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 14;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "Employee's"+Environment.NewLine+"Code",
                        
                        //1-5
                        "Old Code",
                        "Employee's Name",
                        "Department",
                        "Position",
                        "Join"+Environment.NewLine+"Date",
                        
                        //6-10
                        "Resign"+Environment.NewLine+"Date",
                        "Amount"+Environment.NewLine+"Before Deductions",
                        "Deduction"+Environment.NewLine+"(%)",
                        "Deduction",
                        "Amount"+Environment.NewLine+"After Deductions",
                        
                        //11-13
                        "Additional",
                        "Amount",
                        "Payrun"
                    },
                     new int[] 
                    {
                        //0
                        100, 

                        //1-5
                        100, 200, 200, 180, 80, 
                        
                        //6-10
                        80, 120, 120, 120, 120, 
                        
                        //11-13
                        120, 120, 100
                    }
                );
            Sm.GrdColInvisible(Grd1, new int[] { 0, 1, 4 }, false);
            Sm.GrdFormatDate(Grd1, new int[] { 5, 6 });
            Sm.GrdFormatDec(Grd1, new int[] { 7, 8, 9, 10, 11, 12 }, 0);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 0, 1, 4, 5 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    { 
                        DteDocDt, MeeCancelReason, ChkCancelInd, ChkFileInd, LueSiteCode, 
                        TxtAmtBefDed, MeeRemark 
                    }, true);
                    BtnProcess.Enabled = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, ChkFileInd, LueSiteCode, TxtAmtBefDed, MeeRemark 
                    }, false);
                    BtnProcess.Enabled = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeCancelReason, ChkCancelInd }, false);
                    MeeCancelReason.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            { TxtDocNo, DteDocDt, MeeCancelReason, LueSiteCode, MeeRemark });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
            { TxtAmtBefDed, TxtDeduction, TxtAmtAftDed, TxtAmt, TxtNoOfEmployees }, 0);
            ChkCancelInd.Checked = false;
            ChkFileInd.Checked = false;
            ClearGrd();
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 7, 8, 9, 10, 11, 12 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmEmpSCIFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sl.SetLueSiteCode(ref LueSiteCode, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, string.Empty, false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "EmpSCI", "TblEmpSCIHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveEmpSCIHdr(DocNo));
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 0).Length > 0) cml.Add(SaveEmpSCIDtl(DocNo, Row));

            Sm.ExecCommands(cml);

            BtnInsertClick(sender, e);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueSiteCode, "Site") ||
                Sm.IsTxtEmpty(TxtAmtBefDed, "Amount", true) ||
                IsGrdEmpty() ||
                IsGrdValueNotValid();
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 employee.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int r = 0; r < Grd1.Rows.Count - 1; r++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, r, 0, false, 
                    "Row : " + r.ToString() + Environment.NewLine + "Employee's code is empty.")) return true;
                if (Sm.IsGrdValueEmpty(Grd1, r, 2, false,
                    "Row : " + r.ToString() + Environment.NewLine + "Employee's name is empty.")) return true;
            }
            return false;
        }


        private MySqlCommand SaveEmpSCIHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblEmpSCIHdr(DocNo, DocDt, CancelInd, FileInd, SiteCode, AmtBefDed, Deduction, AmtAftDed, Amt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@DocNo, @DocDt, 'N', @FileInd, @SiteCode, @AmtBefDed, @Deduction, @AmtAftDed, @Amt, @Remark, @UserCode, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@FileInd", ChkFileInd.Checked?"Y":"N");
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
            Sm.CmParam<Decimal>(ref cm, "@AmtBefDed", Decimal.Parse(TxtAmtBefDed.Text));
            Sm.CmParam<Decimal>(ref cm, "@Deduction", Decimal.Parse(TxtDeduction.Text));
            Sm.CmParam<Decimal>(ref cm, "@AmtAftDed", Decimal.Parse(TxtAmtAftDed.Text));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtAmt.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveEmpSCIDtl(string DocNo, int r)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblEmpSCIDtl(DocNo, EmpCode, AmtBefDed, PDeduction, Deduction, AmtAftDed, Additional, Amt, CreateBy, CreateDt) " +
                    "Values(@DocNo, @EmpCode, @AmtBefDed, @PDeduction, @Deduction, @AmtAftDed, @Additional, @Amt, @UserCode, CurrentDateTime());"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd1, r, 0));
            Sm.CmParam<Decimal>(ref cm, "@AmtBefDed", Sm.GetGrdDec(Grd1, r, 7));
            Sm.CmParam<Decimal>(ref cm, "@PDeduction", Sm.GetGrdDec(Grd1, r, 8));
            Sm.CmParam<Decimal>(ref cm, "@Deduction", Sm.GetGrdDec(Grd1, r, 9));
            Sm.CmParam<Decimal>(ref cm, "@AmtAftDed", Sm.GetGrdDec(Grd1, r, 10));
            Sm.CmParam<Decimal>(ref cm, "@Additional", Sm.GetGrdDec(Grd1, r, 11));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd1, r, 12));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditEmpInsPnt());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDataCancelledAlready();
        }
        private bool IsDataCancelledAlready()
        {
            return
                Sm.IsDataExist(
                    "Select 1 From TblEmpSCIHdr Where CancelInd='Y' And DocNo=@Param;",
                    TxtDocNo.Text,
                    "This document already cancelled.");
        }

        private MySqlCommand EditEmpInsPnt()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblEmpSCIHdr Set ");
            SQL.AppendLine("    CancelReason=@CancelReason, CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowEmpSCIHdr(DocNo);
                ShowEmpSCIDtl(DocNo);
                ComputeNumberOfEmployees();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowEmpSCIHdr(string DocNo)
        {

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo, DocDt, CancelReason, CancelInd, FileInd, SiteCode, ");
            SQL.AppendLine("AmtBefDed, Deduction, AmtAftDed, Amt, Remark  ");
            SQL.AppendLine("From TblEmpSCIHdr ");
            SQL.AppendLine("Where DocNo=@DocNo;");

            Sm.ShowDataInCtrl(
               ref cm, SQL.ToString(),
               new string[] { 
                    //0
                    "DocNo", 

                    //1-5
                    "DocDt", "CancelReason", "CancelInd", "FileInd", "SiteCode", 
                    
                    //6-10
                    "AmtBefDed", "Deduction", "AmtAftDed", "Amt", "Remark"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                    MeeCancelReason.EditValue = Sm.DrStr(dr, c[2]);
                    ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[3]), "Y");
                    ChkFileInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[4]), "Y");
                    Sl.SetLueSiteCode(ref LueSiteCode, Sm.DrStr(dr, c[5]));
                    TxtAmtBefDed.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[6]), 0);
                    TxtDeduction.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[7]), 0);
                    TxtAmtAftDed.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[8]), 0);
                    TxtAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[9]), 0);
                    MeeRemark.EditValue = Sm.DrStr(dr, c[10]);
                }, true
            );
        }

        private void ShowEmpSCIDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.EmpCode, B.EmpCodeOld, B.EmpName, C.DeptName, D.PosName, B.JoinDt, B.ResignDt, ");
            SQL.AppendLine("A.AmtBefDed, A.PDeduction, A.Deduction, A.AmtAftDed, A.Additional, A.Amt, A.PayrunCode ");
            SQL.AppendLine("From TblEmpSCIDtl A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode=B.EmpCode  ");
            SQL.AppendLine("Left Join TblDepartment C on B.DeptCode=C.DeptCode ");
            SQL.AppendLine("Left Join TblPosition D On B.PosCode=D.PosCode  ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By B.EmpName;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "EmpCode",
 
                    //1-5
                    "EmpCodeOld", "Empname", "DeptName",  "PosName", "JoinDt", 
                    
                    //6-10
                    "ResignDt", "AmtBefDed", "PDeduction", "Deduction", "AmtAftDed", 
                    
                    //11-13
                    "Additional", "Amt", "PayrunCode"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 6, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 12);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 13);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 7, 8, 9, 10, 11, 12 });
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mEmpJobTransferPension = Sm.GetParameter("EmpJobTransferPension");
            mSCICanBeProcessAfterResignInMonths = Sm.GetParameterDec("SCICanBeProcessAfterResignInMonths");
        }

        private void ProcessData1()
        {
            if (Sm.IsDteEmpty(DteDocDt, "Date")) return;
            if (Sm.IsTxtEmpty(TxtAmtBefDed, "Amount before deduction", true)) return;

            Cursor.Current = Cursors.WaitCursor;

            var lResult = new List<Result>();

            ClearGrd();

            try
            {
                Process1a(ref lResult);
                if (lResult.Count > 0)
                {
                    Process1b(ref lResult);
                    Process1c(ref lResult);
                }
                else
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                lResult.Clear();
                Cursor.Current = Cursors.Default;
            }
        }

        private void Process1a(ref List<Result> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.EmpCode, A.EmpCodeOld, A.EmpName, ");
            SQL.AppendLine("B.DeptName, C.PosName, A.JoinDt, A.ResignDt, ");
            SQL.AppendLine("IfNull(D.PDeduction, 0.00)+IfNull(E.DedPerc, 0.00) As PDeduction ");
            SQL.AppendLine("From TblEmployee A ");
            SQL.AppendLine("Left Join TblDepartment B On A.DeptCode=B.DeptCode ");
            SQL.AppendLine("Left Join TblPosition C On A.PosCode=C.PosCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T1.EmpCode, Sum(T2.PDeduction) PDeduction ");
            SQL.AppendLine("    From TblEmpWL T1 ");
            SQL.AppendLine("    Inner Join TblWarningLetter T2 On T1.WLCode=T2.WLCode ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    And T1.StartDt<=@DocDt ");
            SQL.AppendLine("    And T1.EndDt>=@DocDt ");
            SQL.AppendLine("    And Exists( ");
            SQL.AppendLine("        Select 1 ");
            SQL.AppendLine("        From TblEmployeeAllowanceDeduction T ");
            SQL.AppendLine("        Where T.EmpCode=T1.EmpCode ");
            SQL.AppendLine("        And T.ADCode In (Select Parvalue From TblParameter Where ParCode='ADCodeServiceCharge') ");
            SQL.AppendLine("        And ( ");
            SQL.AppendLine("        (T.StartDt Is Null And T.EndDt Is Null) Or ");
            SQL.AppendLine("        (T.StartDt Is Not Null And T.EndDt Is Null And T.StartDt<=@DocDt) Or ");
            SQL.AppendLine("        (T.StartDt Is Null And T.EndDt Is Not Null And T.EndDt>=@DocDt) Or ");
            SQL.AppendLine("        (T.StartDt Is Not Null And T.EndDt Is Not Null And T.StartDt<=@DocDt And T.EndDt>=@DocDt) ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine("    ) Group By T1.EmpCode ");
            SQL.AppendLine(") D On A.EmpCode=D.EmpCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.Value1, T2.Value2, T2.DedPerc ");
            SQL.AppendLine("    From TblSCIDeductionHdr T1, TblSCIDeductionDtl T2 ");
            SQL.AppendLine("    Where T1.DocNo=T2.DocNo  ");
            SQL.AppendLine("    And T1.CancelInd='N'  ");
            SQL.AppendLine("    And T1.ActInd='Y' ");
            SQL.AppendLine("    And T1.Status='A' ");
            SQL.AppendLine("    And T1.Sitecode=@SiteCode ");
            SQL.AppendLine(") E On DateDiff(curdate(), Str_To_Date(A.JoinDt,'%Y%m%d')) Between E.Value1 And E.Value2 ");

            if (mEmpJobTransferPension.Length > 0)
            {
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select A.EmpCode,  ");
                SQL.AppendLine("    Case When Exists( ");
                SQL.AppendLine("        Select 1 From TblPPS T ");
                SQL.AppendLine("        Where T.EmpCode=A.EmpCode ");
                SQL.AppendLine("        And CancelInd='N' ");
                SQL.AppendLine("        And Status='A' ");
                SQL.AppendLine("        And JobTransfer=@EmpJobTransferPension ");
                SQL.AppendLine("        Limit 1 ");
                SQL.AppendLine("    )  Then 'Y' Else 'N' End As IsPension ");
                SQL.AppendLine("    From TblEmployee A ");
                SQL.AppendLine("    Where A.SiteCode=@SiteCode ");
                SQL.AppendLine(") F On A.EmpCode=F.EmpCode ");
                SQL.AppendLine("Where ( ");
                SQL.AppendLine("(F.IsPension='N' And (A.ResignDt Is Null Or (A.ResignDt Is Not Null And A.ResignDt>Date_Format(Date_Add(@DocDt, Interval -1*@SCICanBeProcessAfterResignInMonths Month), '%Y%m%d')))) Or ");
                SQL.AppendLine("(F.IsPension='Y' And (A.ResignDt Is Null Or (A.ResignDt Is Not Null And A.ResignDt<=@DocDt))) ");
                SQL.AppendLine(") ");
            }
            else
            {
                SQL.AppendLine("Where (A.ResignDt Is Null Or (A.ResignDt Is Not Null And A.ResignDt>Date_Format(Date_Add(@DocDt, Interval -1*@SCICanBeProcessAfterResignInMonths Month), '%Y%m%d'))) ");
            }
            SQL.AppendLine("And A.SiteCode=@SiteCode ");
            SQL.AppendLine("And Exists( ");
            SQL.AppendLine("    Select 1 ");
            SQL.AppendLine("    From TblEmployeeAllowanceDeduction T ");
            SQL.AppendLine("    Where T.EmpCode=A.EmpCode ");
            SQL.AppendLine("    And T.ADCode In (Select ParValue From TblParameter Where ParCode='ADCodeServiceCharge') ");
            SQL.AppendLine("    And ( ");
            SQL.AppendLine("    (T.StartDt Is Null And T.EndDt Is Null) Or ");
            SQL.AppendLine("    (T.StartDt Is Not Null And T.EndDt Is Null And T.StartDt<=@DocDt) Or ");
            //SQL.AppendLine("    (T.StartDt Is Null And T.EndDt Is Not Null And T.EndDt>=@DocDt) Or ");
            //SQL.AppendLine("    (T.StartDt Is Not Null And T.EndDt Is Not Null And T.StartDt<=@DocDt And T.EndDt>=@DocDt) ");
            SQL.AppendLine("    (T.StartDt Is Null And T.EndDt Is Not Null And T.EndDt>=Date_Format(Date_Add(@DocDt, Interval -1*@SCICanBeProcessAfterResignInMonths Month), '%Y%m%d')) Or ");
            SQL.AppendLine("    (T.StartDt Is Not Null And T.EndDt Is Not Null And T.StartDt<=@DocDt And T.EndDt>=Date_Format(Date_Add(@DocDt, Interval -1*@SCICanBeProcessAfterResignInMonths Month), '%Y%m%d')) ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine(") ");
            SQL.AppendLine(";");

            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
            Sm.CmParam<String>(ref cm, "@EmpJobTransferPension", mEmpJobTransferPension);
            Sm.CmParam<Decimal>(ref cm, "@SCICanBeProcessAfterResignInMonths", mSCICanBeProcessAfterResignInMonths);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "EmpCode", 
                    
                    //1-5
                    "EmpCodeOld", 
                    "EmpName", 
                    "DeptName", 
                    "PosName", 
                    "JoinDt", 

                    //6-7
                    "ResignDt", 
                    "PDeduction"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Result()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            EmpCodeOld = Sm.DrStr(dr, c[1]),
                            EmpName = Sm.DrStr(dr, c[2]),
                            DeptName = Sm.DrStr(dr, c[3]),
                            PosName = Sm.DrStr(dr, c[4]),
                            JoinDt = Sm.DrStr(dr, c[5]),
                            ResignDt = Sm.DrStr(dr, c[6]),
                            PDeduction = Sm.DrDec(dr, c[7]),
                            AmtBefDed = 0m,
                            Deduction = 0m,
                            AmtAftDed = 0m,
                            Additional = 0m,
                            Amt = 0m
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process1b(ref List<Result> l)
        {
            int 
                NoOfEmployee = l.Count(),
                NoOfEmployeeNoDeduction = 0;
            decimal
                AmtBefDed = decimal.Parse(TxtAmtBefDed.Text),
                Deduction = 0m,
                AmtAftDed = 0m;
            decimal Amt = AmtBefDed / NoOfEmployee;
            
            for (int i = 0; i < l.Count; i++)
            {
                l[i].AmtBefDed = Amt;
                l[i].Deduction = l[i].AmtBefDed*l[i].PDeduction*0.01m;
                l[i].AmtAftDed = l[i].AmtBefDed-l[i].Deduction;
                Deduction += l[i].Deduction;
                AmtAftDed += l[i].AmtAftDed;
                if (l[i].Deduction==0m)
                    NoOfEmployeeNoDeduction+=1;
            }
            decimal Additional = NoOfEmployeeNoDeduction==0m?0m:Deduction / NoOfEmployeeNoDeduction;
            Amt=0m;
            for (int i = 0; i < l.Count; i++)
            {
                if (l[i].Deduction == 0m)
                    l[i].Additional = Additional;
                l[i].Amt = l[i].AmtAftDed + l[i].Additional;
                Amt += l[i].Amt;
            }
            TxtDeduction.Text = Sm.FormatNum(Deduction, 0);
            TxtAmtAftDed.Text = Sm.FormatNum(AmtAftDed, 0);
            TxtAmt.Text = Sm.FormatNum(Amt, 0);
        }

        private void Process1c(ref List<Result> l)
        {
            iGRow r;
            Grd1.BeginUpdate();
            Grd1.Rows.Count = 0;
            for (var i = 0; i < l.Count; i++)
            {
                r = Grd1.Rows.Add();
                r.Cells[0].Value = l[i].EmpCode;
                r.Cells[1].Value = l[i].EmpCodeOld;
                r.Cells[2].Value = l[i].EmpName;
                r.Cells[3].Value = l[i].DeptName;
                r.Cells[4].Value = l[i].PosName;
                r.Cells[5].Value = Sm.ConvertDate(l[i].JoinDt);
                if (l[i].ResignDt.Length>0) r.Cells[6].Value = Sm.ConvertDate(l[i].ResignDt);
                r.Cells[7].Value = l[i].AmtBefDed;
                r.Cells[8].Value = l[i].PDeduction;
                r.Cells[9].Value = l[i].Deduction;
                r.Cells[10].Value = l[i].AmtAftDed;
                r.Cells[11].Value = l[i].Additional;
                r.Cells[12].Value = l[i].Amt;
            }
            r = Grd1.Rows.Add();
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 7, 8, 9, 10, 11, 12 });
            Grd1.EndUpdate();
        }

        private void ProcessData2()
        {
            if (Sm.IsDteEmpty(DteDocDt, "Date")) return;
            
            Cursor.Current = Cursors.WaitCursor;

            var lResult = new List<Result>();

            ClearGrd();
            try
            {
                Process2a(ref lResult);
                if (lResult.Count > 0)
                {
                    Process2b(ref lResult);
                    Process2c(ref lResult);
                }
                else
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                lResult.Clear();
                Cursor.Current = Cursors.Default;
            }
        }

        private void Process2a(ref List<Result> l)
        {
            openFileDialog1.InitialDirectory = "c:";
            openFileDialog1.Filter = "CSV files (*.csv)|*.CSV";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.ShowDialog();

            var ListSeparator = Convert.ToChar(Sm.GetListSeparator());
            var FileName = openFileDialog1.FileName;
            var EmpCodeTemp = string.Empty;
            var AmtTemp = 0m;
            bool IsFirst = true;

            using (var rd = new StreamReader(@FileName))
            {   
                while (!rd.EndOfStream)
                {
                    var line = rd.ReadLine();
                    var arr = line.Split(ListSeparator); // line.Split(',');

                    if (IsFirst)
                        IsFirst = false;
                    else
                    {
                        if (arr[0].Trim().Length > 0)
                        {
                            EmpCodeTemp = arr[0].Trim();
                            if (EmpCodeTemp.Length < 8) EmpCodeTemp = Sm.Right(string.Concat("00000000", EmpCodeTemp), 8);
                            if (arr[1].Trim().Length > 0)
                                AmtTemp = decimal.Parse(arr[1].Trim());
                            else
                                AmtTemp = 0m;
                            l.Add(new Result()
                            {
                                EmpCode = EmpCodeTemp,
                                EmpCodeOld = string.Empty,
                                EmpName = string.Empty,
                                DeptName = string.Empty,
                                PosName = string.Empty,
                                JoinDt = string.Empty,
                                ResignDt = string.Empty,
                                PDeduction = 0m,
                                AmtBefDed = AmtTemp,
                                Deduction = 0m,
                                AmtAftDed = AmtTemp,
                                Additional = 0m,
                                Amt = AmtTemp
                            });
                        }
                    }
                }
            }
        }

        private void Process2b(ref List<Result> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            var EmpCode = string.Empty;
            var Filter = string.Empty;

            for (int i = 0; i < l.Count; i++)
            {
                if (Filter.Length > 0) Filter += " Or ";
                Filter += "(A.EmpCode=@EmpCode0" + i.ToString() + ") ";
                Sm.CmParam<String>(ref cm, "@EmpCode0" + i.ToString(), l[i].EmpCode);
            }

            if (Filter.Length > 0)
                Filter = " And (" + Filter + ") ";
            else
                Filter = " And 0=1 ";

            SQL.AppendLine("Select A.EmpCode, A.EmpCodeOld, A.EmpName, ");
            SQL.AppendLine("B.DeptName, C.PosName, A.JoinDt, A.ResignDt ");
            SQL.AppendLine("From TblEmployee A ");
            SQL.AppendLine("Left Join TblDepartment B On A.DeptCode=B.DeptCode ");
            SQL.AppendLine("Left Join TblPosition C On A.PosCode=C.PosCode ");

            if (mEmpJobTransferPension.Length > 0)
            {
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select A.EmpCode,  ");
                SQL.AppendLine("    Case When Exists( ");
                SQL.AppendLine("        Select 1 From TblPPS T ");
                SQL.AppendLine("        Where T.EmpCode=A.EmpCode ");
                SQL.AppendLine("        And CancelInd='N' ");
                SQL.AppendLine("        And Status='A' ");
                SQL.AppendLine("        And JobTransfer=@EmpJobTransferPension ");
                SQL.AppendLine("        Limit 1 ");
                SQL.AppendLine("    )  Then 'Y' Else 'N' End As IsPension ");
                SQL.AppendLine("    From TblEmployee A ");
                SQL.AppendLine("    Where A.SiteCode=@SiteCode ");
                SQL.AppendLine(") F On A.EmpCode=F.EmpCode ");
                SQL.AppendLine("Where ( ");
                SQL.AppendLine("(F.IsPension='N' And (A.ResignDt Is Null Or (A.ResignDt Is Not Null And A.ResignDt>Date_Format(Date_Add(@DocDt, Interval -1*@SCICanBeProcessAfterResignInMonths Month), '%Y%m%d')))) Or ");
                SQL.AppendLine("(F.IsPension='Y' And (A.ResignDt Is Null Or (A.ResignDt Is Not Null And A.ResignDt<=@DocDt))) ");
                SQL.AppendLine(") ");
            }
            else
            {
                SQL.AppendLine("Where (A.ResignDt Is Null Or (A.ResignDt Is Not Null And A.ResignDt>Date_Format(Date_Add(@DocDt, Interval -1*@SCICanBeProcessAfterResignInMonths Month), '%Y%m%d'))) ");
            }
            SQL.AppendLine("And A.SiteCode=@SiteCode ");
            SQL.AppendLine("And A.SiteCode Not In ( ");
            SQL.AppendLine("    Select SiteCode From TblSCIDeductionHdr Where CancelInd='N' And Status='A' And ActInd='Y' ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("And Exists( ");
            SQL.AppendLine("    Select 1 ");
            SQL.AppendLine("    From TblEmployeeAllowanceDeduction T ");
            SQL.AppendLine("    Where T.EmpCode=A.EmpCode ");
            SQL.AppendLine("    And T.ADCode In (Select ParValue From TblParameter Where ParCode='ADCodeServiceCharge') ");
            SQL.AppendLine("    And ( ");
            SQL.AppendLine("    (T.StartDt Is Null And T.EndDt Is Null) Or ");
            SQL.AppendLine("    (T.StartDt Is Not Null And T.EndDt Is Null And T.StartDt<=@DocDt) Or ");
            //SQL.AppendLine("    (T.StartDt Is Null And T.EndDt Is Not Null And T.EndDt>=@DocDt) Or ");
            //SQL.AppendLine("    (T.StartDt Is Not Null And T.EndDt Is Not Null And T.StartDt<=@DocDt And T.EndDt>=@DocDt) ");
            SQL.AppendLine("    (T.StartDt Is Null And T.EndDt Is Not Null And T.EndDt>=Date_Format(Date_Add(@DocDt, Interval -1*@SCICanBeProcessAfterResignInMonths Month), '%Y%m%d')) Or ");
            SQL.AppendLine("    (T.StartDt Is Not Null And T.EndDt Is Not Null And T.StartDt<=@DocDt And T.EndDt>=Date_Format(Date_Add(@DocDt, Interval -1*@SCICanBeProcessAfterResignInMonths Month), '%Y%m%d')) ");

            SQL.AppendLine("    ) ");
            SQL.AppendLine(") ");
            SQL.AppendLine(Filter);
            SQL.AppendLine(";");

            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
            Sm.CmParam<String>(ref cm, "@EmpJobTransferPension", mEmpJobTransferPension);
            Sm.CmParam<Decimal>(ref cm, "@SCICanBeProcessAfterResignInMonths", mSCICanBeProcessAfterResignInMonths);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "EmpCode", 
                    
                    //1-5
                    "EmpCodeOld", 
                    "EmpName", 
                    "DeptName", 
                    "PosName", 
                    "JoinDt", 

                    //6
                    "ResignDt"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        EmpCode = Sm.DrStr(dr, c[0]);
                        for (int i = 0; i < l.Count; i++)
                        { 
                            if (Sm.CompareStr(l[i].EmpCode, EmpCode))
                            {
                                l[i].EmpCodeOld = Sm.DrStr(dr, c[1]);
                                l[i].EmpName = Sm.DrStr(dr, c[2]);
                                l[i].DeptName = Sm.DrStr(dr, c[3]);
                                l[i].PosName = Sm.DrStr(dr, c[4]);
                                l[i].JoinDt = Sm.DrStr(dr, c[5]);
                                l[i].ResignDt = Sm.DrStr(dr, c[6]);
                                break;
                            }
                        }
                    }
                }
                dr.Close();
            }
        }

        private void Process2c(ref List<Result> l)
        {
            decimal Amt = 0m;
            iGRow r;
            Grd1.BeginUpdate();
            Grd1.Rows.Count = 0;
            for (var i = 0; i < l.Count; i++)
            {
                r = Grd1.Rows.Add();
                r.Cells[0].Value = l[i].EmpCode;
                r.Cells[1].Value = l[i].EmpCodeOld;
                r.Cells[2].Value = l[i].EmpName;
                r.Cells[3].Value = l[i].DeptName;
                r.Cells[4].Value = l[i].PosName;
                if (l[i].JoinDt.Length > 0) r.Cells[5].Value = Sm.ConvertDate(l[i].JoinDt);
                if (l[i].ResignDt.Length > 0) r.Cells[6].Value = Sm.ConvertDate(l[i].ResignDt);
                r.Cells[7].Value = l[i].AmtBefDed;
                r.Cells[8].Value = l[i].PDeduction;
                r.Cells[9].Value = l[i].Deduction;
                r.Cells[10].Value = l[i].AmtAftDed;
                r.Cells[11].Value = l[i].Additional;
                r.Cells[12].Value = l[i].Amt;
                Amt += l[i].Amt;
            }
            r = Grd1.Rows.Add();
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 7, 8, 9, 10, 11, 12 });
            Grd1.EndUpdate();
            TxtAmtBefDed.EditValue = Sm.FormatNum(Amt, 0);
            TxtAmtAftDed.EditValue = Sm.FormatNum(Amt, 0);
            TxtAmt.EditValue = Sm.FormatNum(Amt, 0);
        }

        private void ProcessData3()
        {
            var lResult = new List<Result>();

            try
            {
                Process3a(ref lResult);
                if (lResult.Count > 0)
                {
                    Process3b(ref lResult);
                    Process3c(ref lResult);
                }
                else
                {
                    ComputeDeduction();
                    ComputeAmtAftDed();
                    ComputeAmt();
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                lResult.Clear();
                Cursor.Current = Cursors.Default;
            }
        }

        private void Process3a(ref List<Result> l)
        {
            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 0).Length > 0)
                {
                    l.Add(new Result()
                    {
                        EmpCode = Sm.GetGrdStr(Grd1, r, 0),
                        PDeduction = Sm.GetGrdDec(Grd1, r, 8),
                        AmtBefDed = 0m,
                        Deduction = 0m,
                        AmtAftDed = 0m,
                        Additional = 0m,
                        Amt = 0m
                    });
                }
            }
               
        }

        private void Process3b(ref List<Result> l)
        {
            int
                NoOfEmployee = l.Count(),
                NoOfEmployeeNoDeduction = 0;
            decimal
                AmtBefDed = decimal.Parse(TxtAmtBefDed.Text),
                Deduction = 0m,
                AmtAftDed = 0m;
            decimal Amt = AmtBefDed / NoOfEmployee;

            for (int i = 0; i < l.Count; i++)
            {
                l[i].AmtBefDed = Amt;
                l[i].Deduction = l[i].AmtBefDed * l[i].PDeduction * 0.01m;
                l[i].AmtAftDed = l[i].AmtBefDed - l[i].Deduction;
                Deduction += l[i].Deduction;
                AmtAftDed += l[i].AmtAftDed;
                if (l[i].Deduction == 0m)
                    NoOfEmployeeNoDeduction += 1;
            }
            decimal Additional = NoOfEmployeeNoDeduction == 0m ? 0m : Deduction / NoOfEmployeeNoDeduction;
            Amt = 0m;
            for (int i = 0; i < l.Count; i++)
            {
                if (l[i].Deduction == 0m)
                    l[i].Additional = Additional;
                l[i].Amt = l[i].AmtAftDed + l[i].Additional;
                Amt += l[i].Amt;
            }
            TxtDeduction.Text = Sm.FormatNum(Deduction, 0);
            TxtAmtAftDed.Text = Sm.FormatNum(AmtAftDed, 0);
            TxtAmt.Text = Sm.FormatNum(Amt, 0);
        }

        private void Process3c(ref List<Result> l)
        {
            var EmpCode = string.Empty;
            Grd1.BeginUpdate();
            for (var i = 0; i < l.Count; i++)
            {
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, r, 0);
                    if (EmpCode.Length > 0 &&
                        Sm.CompareStr(EmpCode, l[i].EmpCode))
                    {
                        Grd1.Cells[r, 7].Value = l[i].AmtBefDed;
                        Grd1.Cells[r, 9].Value = l[i].Deduction;
                        Grd1.Cells[r, 10].Value = l[i].AmtAftDed;
                        Grd1.Cells[r, 11].Value = l[i].Additional;
                        Grd1.Cells[r, 12].Value = l[i].Amt;
                        break;
                    }
                }
            }
            
            Grd1.EndUpdate();
        }

        private void ComputeNumberOfEmployees()
        {
            var NumberOfEmployees = 0m;
            for (int r = 0; r < Grd1.Rows.Count; r++)
                if (Sm.GetGrdStr(Grd1, r, 0).Length > 0)
                    NumberOfEmployees += 1;
            TxtNoOfEmployees.EditValue = Sm.FormatNum(NumberOfEmployees, 11);
        }

        private void ComputeAmtBefDed()
        {
            var AmtBefDed = 0m;
            for (int r = 0; r < Grd1.Rows.Count; r++)
                if (Sm.GetGrdStr(Grd1, r, 7).Length > 0)
                    AmtBefDed += Sm.GetGrdDec(Grd1, r, 7);
            TxtAmtBefDed.EditValue = Sm.FormatNum(AmtBefDed, 0);
        }

        private void ComputeDeduction()
        {
            var Deduction = 0m;
            for (int r = 0; r < Grd1.Rows.Count; r++)
                if (Sm.GetGrdStr(Grd1, r, 9).Length > 0)
                    Deduction += Sm.GetGrdDec(Grd1, r, 9);
            TxtDeduction.EditValue = Sm.FormatNum(Deduction, 0);
        }

        private void ComputeAmtAftDed()
        {
            var AmtAftDed = 0m;
            for (int r = 0; r < Grd1.Rows.Count; r++)
                if (Sm.GetGrdStr(Grd1, r, 11).Length > 0)
                    AmtAftDed += Sm.GetGrdDec(Grd1, r, 11);
            TxtAmtAftDed.EditValue = Sm.FormatNum(AmtAftDed, 0);
        }

        private void ComputeAmt()
        {
            var Amt = 0m;
            for (int r = 0; r < Grd1.Rows.Count; r++)
                if (Sm.GetGrdStr(Grd1, r, 12).Length > 0)
                    Amt += Sm.GetGrdDec(Grd1, r, 12);
            TxtAmt.EditValue = Sm.FormatNum(Amt, 0);
        }

        #endregion

        #region Event

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled && TxtDocNo.Text.Length == 0)
            {
                Sm.GrdRemoveRow(Grd1, e, BtnSave);
                if (ChkFileInd.Checked)
                {
                    ComputeAmtBefDed();
                    ComputeDeduction();
                    ComputeAmtAftDed();
                    ComputeAmt();
                }
                else
                {
                    ProcessData3();
                }
                ComputeNumberOfEmployees();
            }
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void ChkFileInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (ChkFileInd.Checked)
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ TxtAmtBefDed }, true);
                else
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ TxtAmtBefDed }, false);
                Sm.SetControlEditValueNull(new List<DXE.BaseEdit> { LueSiteCode });
                Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtAmtBefDed, TxtDeduction, TxtAmtAftDed, TxtAmt }, 0);
                ClearGrd();   
            }
        }

        private void DteDocDt_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) ClearGrd();
        }

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue2(Sl.SetLueSiteCode), string.Empty);
                ClearGrd();
            }
        }

        private void TxtAmtBefDed_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.FormatNumTxt(TxtAmtBefDed, 0);
                ClearGrd();
            }
        }       

        #endregion

        #region Button Event

        private void BtnProcess_Click(object sender, EventArgs e)
        {
            if (ChkFileInd.Checked)
                ProcessData2();
            else
                ProcessData1();
            ComputeNumberOfEmployees();
        }

        #endregion

        #endregion

        #region Class

        private class Result
        {
            public string EmpCode { get; set; }
            public string EmpCodeOld { get; set; }
            public string EmpName { get; set; }
            public string DeptName { get; set; }
            public string PosName { get; set; }
            public string JoinDt { get; set; }
            public string ResignDt { get; set; }
            public decimal AmtBefDed { get; set; }
            public decimal PDeduction { get; set; }
            public decimal Deduction { get; set; }
            public decimal AmtAftDed { get; set; }
            public decimal Additional { get; set; }
            public decimal Amt { get; set; }
        }

        private class Employee
        {
            public string EmpCode { get; set; }
        }

        #endregion
    }
}
