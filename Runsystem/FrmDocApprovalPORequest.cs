﻿#region Namespace

using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;
using System.IO;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmDocApprovalPORequest : RunSystem.FrmBase9
    {
        #region Field

        internal string mDocNo = string.Empty, mDNo = string.Empty;
        private string mMaterialRequestDocNo = string.Empty, mMaterialRequestDNo = string.Empty, mMenuCode = string.Empty;
        private bool mIsUseItemConsumption = false;

        #endregion

        #region Constructor

        public FrmDocApprovalPORequest(string DocNo, string DNo, string MenuCode, bool IsUseItemConsumption)
        {
            InitializeComponent();
            mDocNo = DocNo;
            mDNo = DNo;
            mMenuCode = MenuCode;
            mIsUseItemConsumption = IsUseItemConsumption;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                SetGrd();
                ShowData1();
                ShowData2();
                ShowData3();
                ShowDocApproval();
                if (!mIsUseItemConsumption)
                {
                    label22.Visible = label23.Visible = label24.Visible =
                    label25.Visible = label26.Visible = label27.Visible = false;
                    TxtMth01.Visible = TxtMth03.Visible = TxtMth06.Visible =
                    TxtMth09.Visible = TxtMth12.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            ShowPicture();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 8;
            Grd1.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "",
                        
                        //1-5
                        "Vendor Code",
                        "Vendor",
                        "Term of Payment",
                        "Currency",
                        "Unit Price",

                        //6-7
                        "Quotation Date",
                        "Remark"
                    },
                     new int[] 
                    {
                        50,  
                        100, 150, 150, 70, 100, 
                        80, 300
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 0 });
            Sm.GrdFormatDec(Grd1, new int[] { 5 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 6 });
            Sm.GrdColInvisible(Grd1, new int[] { 1 });
            Sm.GrdColReadOnly(Grd1, new int[]{ 1, 2, 3, 4, 5, 6, 7});

            Grd2.Cols.Count = 4;
            Grd2.ReadOnly = true;

            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[] 
                    {
                        //0
                        "Checked By", 
                        
                        //1-3
                        "Status",
                        "Date",
                        "Remark"
                    },
                    new int[] 
                    {
                        150, 
                        100, 100, 400
                    }
                );
            Sm.GrdFormatDate(Grd2, new int[] { 2 });
        }

        private string GetItCode()
        {
            var cm = new MySqlCommand()
            {
                CommandText = 
                    "Select ItCode " +
                    "From TblPORequestDtl A " +
                    "Inner Join TblMaterialRequestDtl B On A.MaterialRequestDocNo=B.DocNo And A.MaterialRequestDNo=B.DNo "+
                    "Where A.DocNo=@DocNo And A.DNo=@DNo;"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", mDocNo);
            Sm.CmParam<String>(ref cm, "@DNo", mDNo);

            return (Sm.GetValue(cm));
        }

        private void ShowData1()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, H.DeptName, C.DocDt As MaterialRequestDocDt, D.ItCode, I.ItName, B.Qty, I.PurchaseUomCode, ");
            SQL.AppendLine("G.VdName, J.PtName, E.CurCode, F.UPrice, E.DocDt As QtDocDt, D.UsageDt, Trim(Concat(IfNull(B.Remark, ''), ' ', IfNull(D.Remark, ''))) As Remark, ");
            SQL.AppendLine("B.MaterialRequestDocNo, B.MaterialRequestDNo, I.MinOrder, I.MinStock, I.MaxStock, I.InventoryUomCode, I.Remark As ItemRemark, ");
            SQL.AppendLine("IfNull((Select Sum(Qty) From TblStockSummary Where Qty>0 And ItCode=@ItCode), 0) As Stock, ");
            SQL.AppendLine("(D.Qty-IfNull(( ");
            SQL.AppendLine("    Select Sum(Qty) ");
            SQL.AppendLine("    From TblPORequestDtl ");
            SQL.AppendLine("    Where MaterialRequestDocNo=B.MaterialRequestDocNo ");
            SQL.AppendLine("    And MaterialRequestDNo=B.MaterialRequestDNo ");
            SQL.AppendLine("    And Concat(DocNo, DNo)<>Concat(B.DocNo, B.DNo) ");
            SQL.AppendLine("    And (CancelInd='N' And Status<>'C') ");
            SQL.AppendLine("    ), 0)) As OutstandingMaterialRequestQty, ");

            if (mIsUseItemConsumption)
            {
                SQL.AppendLine("ifnull(K.Qty01, 0) As Mth01, ifnull(K.Qty03, 0) As Mth03, ifnull(K.Qty06, 0) As Mth06, ");
                SQL.AppendLine("ifnull(K.Qty09, 0) As Mth09, ifnull(K.Qty12, 0) As Mth12 ");
            }
            else
            {
                SQL.AppendLine("0 As Mth01, 0 As Mth03, 0 As Mth06, 0 As Mth09, 0 As Mth12 ");
            }
            SQL.AppendLine("From TblPORequestHdr A ");
            SQL.AppendLine("Inner Join TblPORequestDtl B On A.DocNo=B.DocNo And B.DNo=@DNo ");
            SQL.AppendLine("Inner Join TblMaterialRequestHdr C On B.MaterialRequestDocNo=C.DocNo ");
            SQL.AppendLine("Inner Join TblMaterialRequestDtl D On B.MaterialRequestDocNo=D.DocNo And B.MaterialRequestDNo=D.DNo ");
            SQL.AppendLine("Inner Join TblQtHdr E On B.QtDocNo=E.DocNo ");
            SQL.AppendLine("Inner Join TblQtDtl F On B.QtDocNo=F.DocNo And B.QtDNo=F.DNo ");
            SQL.AppendLine("Inner Join TblVendor G On E.VdCode=G.VdCode ");
            SQL.AppendLine("Inner Join TblDepartment H On C.DeptCode=H.DeptCode ");
            SQL.AppendLine("Inner Join TblItem I On D.ItCode=I.ItCode ");
            SQL.AppendLine("Inner Join TblPaymentTerm J On E.PtCode=J.PtCode ");
            if (mIsUseItemConsumption)
            {
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select @ItCode As ItCode, Sum(Qty01) As Qty01, Sum(Qty03) As Qty03, Sum(Qty06) As Qty06, Sum(Qty09) As Qty09, Sum(Qty12) As Qty12 ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select ");
                SQL.AppendLine("        Case Z1.Mth When '01' Then Z1.Qty Else 0.00 End As Qty01,  ");
                SQL.AppendLine("        Case Z1.Mth When '03' Then Z1.Qty Else 0.00 End As Qty03, ");
                SQL.AppendLine("        Case Z1.Mth When '06' Then Z1.Qty Else 0.00 End As Qty06, ");
                SQL.AppendLine("        Case Z1.Mth When '09' Then Z1.Qty Else 0.00 End As Qty09, ");
                SQL.AppendLine("        Case Z1.Mth When '12' Then Z1.Qty Else 0.00 End As Qty12 ");
                SQL.AppendLine("        From ( ");
                SQL.AppendLine("            Select T1.Mth, Sum(T2.Qty)  As Qty ");
                SQL.AppendLine("            From ( ");
                SQL.AppendLine("                Select convert('01' using latin1) As Mth Union All ");
                SQL.AppendLine("	            Select convert('03' using latin1) As Mth Union All ");
                SQL.AppendLine("	            Select convert('06' using latin1) As Mth Union All ");
                SQL.AppendLine("	            Select convert('09' using latin1) As Mth Union All ");
                SQL.AppendLine("	            Select convert('12' using latin1) As Mth  ");
                SQL.AppendLine("            ) T1 ");
                SQL.AppendLine("            Inner Join ( ");
                SQL.AppendLine("                Select  convert('01' using latin1) As Mth, Sum(B.Qty) As Qty ");
                SQL.AppendLine("	            From TblDODeptHdr A ");
                SQL.AppendLine("	            Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' And B.ItCode=@ItCode ");
                SQL.AppendLine("	            Where A.DocDt Between @DocDt And last_day(@DocDt) ");
                SQL.AppendLine("	            Union All ");
                SQL.AppendLine("	            Select  convert('03' using latin1) As Mth, Sum(B.Qty)/3 As Qty ");
                SQL.AppendLine("	            From TblDODeptHdr A ");
                SQL.AppendLine("	            Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' And B.ItCode=@ItCode ");
                SQL.AppendLine("		        Where A.DocDt Between @DocDt2 And last_day(@DocDt3) ");
                SQL.AppendLine("	            Union All ");
                SQL.AppendLine("	            Select  convert('06' using latin1) As Mth, Sum(B.Qty)/6 As Qty ");
                SQL.AppendLine("	            From TblDODeptHdr A ");
                SQL.AppendLine("	            Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N'  And B.ItCode=@ItCode ");
                SQL.AppendLine("		        Where A.DocDt Between @DocDt4 And last_day(@DocDt3) ");
                SQL.AppendLine("	            Union All ");
                SQL.AppendLine("	            Select  convert('09' using latin1) As Mth, Sum(B.Qty)/9 As Qty ");
                SQL.AppendLine("	            From TblDODeptHdr A ");
                SQL.AppendLine("	            Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N' And B.ItCode=@ItCode ");
                SQL.AppendLine("		        Where A.DocDt Between @DocDt5 And last_day(@DocDt3) ");
                SQL.AppendLine("	            Union All ");
                SQL.AppendLine("	            Select  convert('12' using latin1) As Mth, Sum(B.Qty)/12 As Qty ");
                SQL.AppendLine("	            From TblDODeptHdr A ");
                SQL.AppendLine("	            Inner Join TblDODeptDtl B On A.DocNo=B.DocNo And CancelInd = 'N'  And B.ItCode=@ItCode ");
                SQL.AppendLine("		        Where A.DocDt Between @DocDt6 And last_day(@DocDt3) ");
                SQL.AppendLine("            ) T2 On T1.Mth = T2.Mth  ");
                SQL.AppendLine("            Group By T1.Mth ");
                SQL.AppendLine("        )Z1 ");
                SQL.AppendLine("    )Z2 ");
                SQL.AppendLine(") K On D.ItCode=K.ItCode ");
            }
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            
            var cm = new MySqlCommand();

            DateTime DocDtNow = Sm.ConvertDateTime(Sm.ServerCurrentDateTime()).AddMonths(-1);
            
            Sm.CmParamDt(ref cm, "@DocDt", string.Concat(Sm.FormatDate(DocDtNow).Substring(0, 6), "01"));
            Sm.CmParamDt(ref cm, "@DocDt2", string.Concat(Sm.FormatDate(DocDtNow.AddMonths(-5)).Substring(0, 6), "01"));
            Sm.CmParamDt(ref cm, "@DocDt3", string.Concat(Sm.FormatDate(DocDtNow.AddMonths(-3)).Substring(0, 6), "01"));
            Sm.CmParamDt(ref cm, "@DocDt4", string.Concat(Sm.FormatDate(DocDtNow.AddMonths(-8)).Substring(0, 6), "01"));
            Sm.CmParamDt(ref cm, "@DocDt5", string.Concat(Sm.FormatDate(DocDtNow.AddMonths(-11)).Substring(0, 6), "01"));
            Sm.CmParamDt(ref cm, "@DocDt6", string.Concat(Sm.FormatDate(DocDtNow.AddMonths(-14)).Substring(0, 6), "01"));

            Sm.CmParam<String>(ref cm, "@DocNo", mDocNo);
            Sm.CmParam<String>(ref cm, "@DNo", mDNo);
            Sm.CmParam<String>(ref cm, "@ItCode", GetItCode());

            Sm.ShowDataInCtrl(
                    ref cm,
                    SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo", 
                        
                        //1-5
                        "DocDt", "DeptName", "MaterialRequestDocDt", "ItCode", "ItName", 
                        
                        //6-10
                        "Qty", "PurchaseUomCode", "VdName", "PtName", "CurCode", 
                        
                        //11-15
                        "UPrice", "QtDocDt", "UsageDt", "Remark", "MaterialRequestDocNo",
 
                        //16-20
                        "MaterialRequestDNo", "MinStock", "MaxStock", "InventoryUomCode", "Stock",
                        
                        //21-25
                        "OutstandingMaterialRequestQty", "Mth01", "Mth03", "Mth06", "Mth09",

                        //26-28
                        "Mth12", "MinOrder", "ItemRemark"

                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        TxtDeptCode.EditValue = Sm.DrStr(dr, c[2]);
                        Sm.SetDte(DteMaterialRequestDocDt, Sm.DrStr(dr, c[3]));
                        TxtItCode.EditValue = Sm.DrStr(dr, c[4]);
                        TxtItName.EditValue = Sm.DrStr(dr, c[5]);
                        TxtQty.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[6]), 0);
                        TxtPurchaseUomCode.EditValue = Sm.DrStr(dr, c[7]);
                        TxtVdCode.EditValue = Sm.DrStr(dr, c[8]);
                        TxtPtCode.EditValue = Sm.DrStr(dr, c[9]);
                        TxtCurCode.EditValue = Sm.DrStr(dr, c[10]);
                        TxtUPrice.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[11]), 0);
                        Sm.SetDte(DteQtDocDt, Sm.DrStr(dr, c[12]));
                        Sm.SetDte(DteUsageDt, Sm.DrStr(dr, c[13]));
                        MeeRemark.EditValue = Sm.DrStr(dr, c[14]);
                        mMaterialRequestDocNo = Sm.DrStr(dr, c[15]);
                        mMaterialRequestDNo = Sm.DrStr(dr, c[16]);
                        TxtMinStock.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[17]), 0);
                        TxtMaxStock.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[18]), 0);
                        TxtInventoryUomCode.EditValue = Sm.DrStr(dr, c[19]);
                        if (Sm.DrDec(dr, c[20])>0)
                            TxtStock.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[20]), 0);
                        else
                            TxtStock.EditValue = Sm.FormatNum(0, 0);
                        TxtMaterialRequestQty.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[21]), 0);
                        TxtMth01.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[22]), 2);
                        TxtMth03.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[23]), 2);
                        TxtMth06.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[24]), 2);
                        TxtMth09.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[25]), 2);
                        TxtMth12.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[26]), 2);
                        TxtMinOrder.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[27]), 2);
                        MeeItemRemark.EditValue = Sm.DrStr(dr, c[28]);
                    }, false
                );
        }

        private void ShowData2()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select F.VdName, G.PtName, D.CurCode, E.UPrice, A.Qty, D.DocDt As QtDocDt ");
            SQL.AppendLine("From TblPODtl A ");
            SQL.AppendLine("Inner Join TblPORequestDtl B On A.PORequestDocNo=B.DocNo And A.PORequestDNo=B.DNo ");
            SQL.AppendLine("Inner Join TblMaterialRequestDtl C On B.MaterialRequestDocNo=C.DocNo And B.MaterialRequestDNo=C.DNo And C.ItCode=@ItCode ");
            SQL.AppendLine("Inner Join TblQtHdr D On B.QtDocNo=D.DocNo ");
            SQL.AppendLine("Inner Join TblQtDtl E On B.QtDocNo=E.DocNo And B.QtDNo=E.DNo ");
            SQL.AppendLine("Inner Join TblVendor F On D.VdCode=F.VdCode ");
            SQL.AppendLine("Inner Join TblPaymentTerm G On D.PtCode=G.PtCode ");
            SQL.AppendLine("Inner Join TblPOHdr H On A.DocNo=H.DocNo ");
            SQL.AppendLine("Where A.CancelInd='N' ");
            SQL.AppendLine("Order By H.DocDt Desc, A.DocNo Desc, A.DNo Desc Limit 1 ");

            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@ItCode", TxtItCode.Text);

            Sm.ShowDataInCtrl(
                    ref cm,
                    SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "VdName", 
                        
                        //1-5
                        "PtName", "CurCode", "UPrice", "Qty", "QtDocDt"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtVdCode2.EditValue = Sm.DrStr(dr, c[0]);
                        TxtPtCode2.EditValue = Sm.DrStr(dr, c[1]);
                        TxtCurCode2.EditValue = Sm.DrStr(dr, c[2]);
                        TxtUPrice2.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[3]), 0);
                        TxtQty2.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[4]), 0);
                        Sm.SetDte(DteQtDocDt2, Sm.DrStr(dr, c[5]));
                    }, false
                );
        }

        private void ShowData3()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select C.VdCode, C.VdName, D.PtName, A.CurCode, B.UPrice, A.DocDt, B.Remark ");
            SQL.AppendLine("From TblQtHdr A ");
            SQL.AppendLine("Inner Join TblQtDtl B On A.DocNo=B.DocNo And B.ItCode=@ItCode ");
            SQL.AppendLine("Inner Join TblVendor C On A.VdCode=C.VdCode And C.ActInd='Y' ");
            SQL.AppendLine("Left Join TblPaymentTerm D On A.PtCode=D.PtCode ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select T1.VdCode, T2.ItCode, ");
            SQL.AppendLine("    Max(Concat(T1.DocDt, T1.DocNo, T2.DNo)) As Key1  ");
            SQL.AppendLine("    From TblQtHdr T1, TblQtDtl T2 ");
            SQL.AppendLine("    Where T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    Group By T1.VdCode, T2.ItCode ");
            SQL.AppendLine("    ) E On Concat(A.DocDt, B.DocNo, B.DNo)=E.Key1 And A.VdCode=E.VdCode And B.ItCode=E.ItCode ");
            
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@ItCode", TxtItCode.Text);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "VdCode", 

                    //1-5
                    "VdName", "PtName", "CurCode", "UPrice", "DocDt",

                    //6
                    "Remark"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("D", Grd1, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 6);
                }, false, false, false, true
            );
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ShowDocApproval()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.ApprovalDNo, B.UserName, A.LastUpDt, A.Remark, ");
            SQL.AppendLine("Case Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' When 'O' Then 'Outstanding' End As StatusDesc ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Left Join TblUser B On A.UserCode = B.UserCode ");
            SQL.AppendLine("Where A.DocType='PORequest' ");
            SQL.AppendLine("And IfNull(Status, 'O')<>'O' ");
            SQL.AppendLine("And A.DocNo=@DocNo ");
            SQL.AppendLine("And A.DNo=@DNo ");
            SQL.AppendLine("Order By A.ApprovalDNo;");

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DNo", mDNo);
            Sm.ShowDataInGrid(
                    ref Grd2, ref cm,
                    SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "UserName",
                        
                        //1-3
                        "StatusDesc","LastUpDt", "Remark"

                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd2, 0, 0);
        }

        private void ShowPicture()
        {
            if (TxtItCode.Text.Length != 0)
            {
                try
                {
                    string DBServer = Sm.GetParameter("DBServer");
                    string location = Sm.GetParameter("ImgFileItem");
                    if (DBServer.Length != 0 && location.Length!=0 && Sm.CompareStr(DBServer, Gv.Server))
                    {
                        if (Directory.Exists(location))
                        {
                            string ImageFile = String.Concat(location, "\\", TxtItCode.Text);
                            foreach (string fe in new string[11] 
                                { ".jpg", ".png", ".bmp", ".jpeg", ".gif", ".dib", 
                                  ".rle", ".jpe", ".jfif", ".tiff", ".tif" })
                            {
                                if (File.Exists(String.Concat(ImageFile, fe)))
                                {
                                    PicItem.Image = Image.FromFile(String.Concat(ImageFile, fe));
                                    break;
                                }
                            }
                        }
                    }
                }
                catch (Exception Exc)
                {
                }
            }
        }

        #endregion

        #region Grid 

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var fPORequestDlg = new FrmDocApprovalPORequestDlg(Sm.GetGrdStr(Grd1, e.RowIndex, 1), TxtItCode.Text );
                fPORequestDlg.WindowState = FormWindowState.Normal;
                fPORequestDlg.StartPosition = FormStartPosition.CenterScreen;
                fPORequestDlg.TxtVdName.EditValue = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                fPORequestDlg.TxtItName.EditValue = TxtItName.Text;
                fPORequestDlg.ShowDialog();
            }
        }

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0 )
            {
                var fPORequestDlg = new FrmDocApprovalPORequestDlg(Sm.GetGrdStr(Grd1, e.RowIndex, 1), TxtItCode.Text );
                fPORequestDlg.WindowState = FormWindowState.Normal;
                fPORequestDlg.StartPosition = FormStartPosition.CenterScreen;
                fPORequestDlg.TxtVdName.EditValue = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                fPORequestDlg.TxtItName.EditValue = TxtItName.Text;
                fPORequestDlg.ShowDialog();
            }
        }

        #endregion

        #region Button

        private void BtnMaterialRequest_Click(object sender, EventArgs e)
        {
            var fMaterialRequest = new FrmDocApprovalMaterialRequest(mMaterialRequestDocNo, mMaterialRequestDNo, false);
            fMaterialRequest.Tag = mMenuCode;
            fMaterialRequest.WindowState = FormWindowState.Normal;
            fMaterialRequest.StartPosition = FormStartPosition.CenterScreen;
            fMaterialRequest.ShowDialog();
        }

        private void BtnItem_Click(object sender, EventArgs e)
        {
            try
            {
                string MenuCode = Sm.GetValue("Select MenuCode From TblMenu Where Param='FrmItem';");
                var f = new FrmItem(MenuCode);
                f.Tag = MenuCode;
                f.mIsAccessUsed = true;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = TxtItCode.Text;
                f.ShowDialog();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion
        
        #endregion
    }
}
