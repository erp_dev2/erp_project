﻿#region Update
/*
    07/05/2021 [WED/PHT] new apps
	12/10/2021 [RDH/PHT] mengubah dialog amount terfilter berdasarkan month from dan month to dan menambahkan amount dari jurnal berdasarkan cost center code
    28/10/2021 [DITA/PHT] rombak query + filter month
    15/11/2021 [ICA/PHT] clear data kolom item to di parent ketika choose data 
    14/12/2021 [DITA/PHT] masih ada bug saat nge get remaining budget -> budget transfer sesuai month nya
    13/05/2022 [WED/PHt] bug pembagian dengan angka 0 
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmBudgetTransferCostCenterDlg2 : RunSystem.FrmBase4
    {
        #region Field

        private FrmBudgetTransferCostCenter mFrmParent;
        private string 
            mSQL = string.Empty, 
            mCCCode = string.Empty, 
            mYr = string.Empty,
            mMth1 = string.Empty,
            mMth2 = string.Empty
            ;
        private byte mDocType = 0;
        private int mCurRow = 0;
        private int[] mMthCols = { 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 };
        private bool IsRefreshed = false;
        //private int dataCounts = Int32.Parse(Sm.GetValue("Select Count(*) From TblBudgetCategory; "));
        private int offset = 0;

        private List<BTCCDlg> l = null;
        private List<Prep1> l1 = null;
        private List<CostCategory> l2 = null;
        private List<BudgetCategory> l3 = null;
        private List<CAS> l4 = null;
        private List<BudgetTransfer> l5 = null;
        private List<BudgetReceive> l6 = null;
        private List<Journal> l7 = null;

        #endregion

        #region Constructor

        public FrmBudgetTransferCostCenterDlg2(
            FrmBudgetTransferCostCenter FrmParent, 
            byte DocType, 
            string CCCode, 
            int CurRow, 
            string Yr,
            string Mth1,
            string Mth2
            )
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mDocType = DocType;
            mCCCode = CCCode;
            mCurRow = CurRow;
            mYr = Yr;
            mMth1 = Mth1;
            mMth2 = Mth2;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "List of Budget Category";

                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                BtnLoadMore.Visible = false;
                IsRefreshed = false;
                SetGrd();
                //SetSQL();

                l = new List<BTCCDlg>();
                l1 = new List<Prep1>();
                l2 = new List<CostCategory>();
                l3 = new List<BudgetCategory>();
                l4 = new List<CAS>();
                l5 = new List<BudgetTransfer>();
                l6 = new List<BudgetReceive>();
                l7 = new List<Journal>();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 24;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No",

                    //1-5
                    "",
                    "Budget Category"+Environment.NewLine+"Code", 
                    "Budget Category",
                    "Remaining Budget"+Environment.NewLine+"Amount",
                    "Amt01",

                    //6-10
                    "Amt02",
                    "Amt03",
                    "Amt04",
                    "Amt05",
                    "Amt06",

                    //11-15
                    "Amt07",
                    "Amt08",
                    "Amt09",
                    "Amt10",
                    "Amt11",

                    //16-20
                    "Amt12",
                    "Transferred Amt",
                    "Received Amt",
                    "COA",
                    "COA Description",

                    //21-23
                    "Department",
                    "Cost Category",
                    "Type"
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    20, 100, 250, 150, 0,

                    //6-10
                    0, 0, 0, 0, 0, 

                    //11-15
                    0, 0, 0, 0, 0, 

                    //16-20
                    0, 0, 0, 150, 200, 

                    //21-23
                    250, 250, 100
                }
            );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdFormatDec(Grd1, new int[] { 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18 }, 0);
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19 });
            if (mDocType == 2) Sm.GrdColInvisible(Grd1, new int[] { 1 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 19 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();
            #region Old Code
            //Select C.BCCode, C.BCName, C.AcNo, E.AcDesc, D.DeptName, F.CCtName, H.OptDesc As BudgetTypeDesc
            //From TblCompanyBudgetPlanHdr A
            //Inner Join TblCompanyBudgetPlanDtl B On A.DocNo = B.DocNo
            //    And A.CancelInd = 'N'
            //    And A.CCCode Is Not Null
            //    And A.CompletedInd = 'Y'
            //    And A.CCCode = @CCCode
            //    And A.Yr = @Yr
            //Inner Join TblBudgetCategory C On B.AcNo = C.AcNo
            //    And A.CCCode = C.CCCode
            //Inner Join TblDepartment D On C.DeptCode = D.DeptCode
            //Inner Join TblCOA E On C.AcNo = E.AcNo
            //Left Join TblCostCategory F On C.CCtCode = F.CCtCode
            //Left Join TblCompanyBudgetPlanDtl2 G On A.DocNo = G.DocNo And B.AcNo = G.AcNo
            //Left Join TblOption H On C.BudgetType = H.OptCode And H.OptCat = 'BudgetType'
            //Left Join
            //(
            //    Select T4.BCCode, T2.ItCode, Left(T1.DocDt, 6) YrMth, Sum(T2.Qty) CASQty
            //    From TblCashAdvanceSettlementHdr T1
            //    Inner Join TblCashAdvanceSettlementDtl T2 On T1.DocNo = T2.DocNo
            //        And T1.CancelInd = 'N'
            //        And T1.Status = 'A'
            //        And T2.ItCode Is Not Null
            //        And Left(T1.DocDt, 4) = @Yr
            //        And T1.CCCode = @CCCode
            //    Inner Join TblCostCategory T3 On T2.CCtCode = T3.CCtCode
            //    Inner Join TblBudgetCategory T4 On T3.AcNo = T4.AcNo And T1.CCCode = T4.CCCode
            //    Group By T4.BCCode, T2.ItCode, Left(T1.DocDt, 6)
            //) I On C.BCCode = I.BCCode And G.ItCode = I.ItCode

            //SQL.AppendLine("Select T.* From ( ");
            //SQL.AppendLine("    Select C.BCCode, C.BCName, C.AcNo, E.AcDesc, D.DeptName, F.CCtName, H.OptDesc As BudgetTypeDesc,  ");
            //SQL.AppendLine("    Sum((G.Rate01*G.Qty01) - IfNull(If(I.Mth = '01', I.CASQty*G.Rate01, 0.00), 0.00)) Amt01, ");
            //SQL.AppendLine("    Sum((G.Rate02*G.Qty02) - IfNull(If(I.Mth = '02', I.CASQty*G.Rate02, 0.00), 0.00)) Amt02, ");
            //SQL.AppendLine("    Sum((G.Rate03*G.Qty03) - IfNull(If(I.Mth = '03', I.CASQty*G.Rate03, 0.00), 0.00)) Amt03, ");
            //SQL.AppendLine("    Sum((G.Rate04*G.Qty04) - IfNull(If(I.Mth = '04', I.CASQty*G.Rate04, 0.00), 0.00)) Amt04, ");
            //SQL.AppendLine("    Sum((G.Rate05*G.Qty05) - IfNull(If(I.Mth = '05', I.CASQty*G.Rate05, 0.00), 0.00)) Amt05, ");
            //SQL.AppendLine("    Sum((G.Rate06*G.Qty06) - IfNull(If(I.Mth = '06', I.CASQty*G.Rate06, 0.00), 0.00)) Amt06, ");
            //SQL.AppendLine("    Sum((G.Rate07*G.Qty07) - IfNull(If(I.Mth = '07', I.CASQty*G.Rate07, 0.00), 0.00)) Amt07, ");
            //SQL.AppendLine("    Sum((G.Rate08*G.Qty08) - IfNull(If(I.Mth = '08', I.CASQty*G.Rate08, 0.00), 0.00)) Amt08, ");
            //SQL.AppendLine("    Sum((G.Rate09*G.Qty09) - IfNull(If(I.Mth = '09', I.CASQty*G.Rate09, 0.00), 0.00)) Amt09, ");
            //SQL.AppendLine("    Sum((G.Rate10*G.Qty10) - IfNull(If(I.Mth = '10', I.CASQty*G.Rate10, 0.00), 0.00)) Amt10, ");
            //SQL.AppendLine("    Sum((G.Rate11*G.Qty11) - IfNull(If(I.Mth = '11', I.CASQty*G.Rate11, 0.00), 0.00)) Amt11, ");
            //SQL.AppendLine("    Sum((G.Rate12*G.Qty12) - IfNull(If(I.Mth = '12', I.CASQty*G.Rate12, 0.00), 0.00)) Amt12, ");
            //SQL.AppendLine("    IfNull(J.TransferredAmt, 0.00) TransferredAmt, IfNull(K.ReceivedAmt, 0.00) ReceivedAmt ");
            //SQL.AppendLine("    From TblCompanyBudgetPlanHdr A ");
            //SQL.AppendLine("    Inner Join TblCompanyBudgetPlanDtl B On A.DocNo = B.DocNo ");
            //SQL.AppendLine("        And A.CancelInd = 'N' ");
            //SQL.AppendLine("        And A.CCCode Is Not Null ");
            //SQL.AppendLine("        And A.CompletedInd = 'Y' ");
            //SQL.AppendLine("        And A.CCCode = @CCCode ");
            //SQL.AppendLine("        And A.Yr = @Yr ");
            //SQL.AppendLine("    Inner Join TblBudgetCategory C On B.AcNo = C.AcNo ");
            //SQL.AppendLine("        And A.CCCode = C.CCCode ");
            //SQL.AppendLine("    Inner Join TblDepartment D On C.DeptCode = D.DeptCode ");
            //SQL.AppendLine("    Inner Join TblCOA E On C.AcNo = E.AcNo ");
            //SQL.AppendLine("    Left Join TblCostCategory F On C.CCtCode = F.CCtCode ");
            //SQL.AppendLine("    Left Join TblCompanyBudgetPlanDtl2 G On A.DocNo = G.DocNo And B.AcNo = G.AcNo ");
            //SQL.AppendLine("    Left Join TblOption H On C.BudgetType = H.OptCode And H.OptCat = 'BudgetType' ");
            //SQL.AppendLine("    Left Join ");
            //SQL.AppendLine("    ( ");
            //SQL.AppendLine("        Select T4.BCCode, T2.ItCode, Substr(T1.DocDt, 5, 2) Mth, Sum(T2.Qty) CASQty ");
            //SQL.AppendLine("        From TblCashAdvanceSettlementHdr T1 ");
            //SQL.AppendLine("        Inner Join TblCashAdvanceSettlementDtl T2 On T1.DocNo = T2.DocNo ");
            //SQL.AppendLine("            And T1.CancelInd = 'N' ");
            //SQL.AppendLine("            And T1.Status = 'A' ");
            //SQL.AppendLine("            And T2.ItCode Is Not Null ");
            //SQL.AppendLine("            And Left(T1.DocDt, 4) = @Yr ");
            //SQL.AppendLine("            And T1.CCCode = @CCCode ");
            //SQL.AppendLine("        Inner Join TblCostCategory T3 On T2.CCtCode = T3.CCtCode ");
            //SQL.AppendLine("        Inner Join TblBudgetCategory T4 On T3.AcNo = T4.AcNo And T1.CCCode = T4.CCCode ");
            //SQL.AppendLine("        Group By T4.BCCode, T2.ItCode, Left(T1.DocDt, 6) ");
            //SQL.AppendLine("    ) I On C.BCCode = I.BCCode And G.ItCode = I.ItCode ");
            //SQL.AppendLine("    Left Join ");
            //SQL.AppendLine("    ( ");
            //SQL.AppendLine("        Select X2.BCCode, Sum(X2.TransferredAmt) TransferredAmt ");
            //SQL.AppendLine("        From TblBudgetTransferCostCenterHdr X1 ");
            //SQL.AppendLine("        Inner Join TblBudgetTransferCostCenterDtl X2 On X1.DOcNo = X2.DocNo ");
            //SQL.AppendLine("            And X1.Status In ('A') ");
            //SQL.AppendLine("            And X1.CancelInd = 'N' ");
            //SQL.AppendLine("            And X1.Yr = @Yr ");
            //SQL.AppendLine("            And ((X1.Mth Between @Mth1 And @Mth2) Or (X1.Mth2 Between @Mth1 And @Mth2)) ");
            //SQL.AppendLine("        Group By X2.BCCode ");
            //SQL.AppendLine("    ) J On C.BCCode = J.BCCode ");
            //SQL.AppendLine("    Left Join ");
            //SQL.AppendLine("    ( ");
            //SQL.AppendLine("        Select X2.BCCode2, Sum(X2.TransferredAmt) ReceivedAmt ");
            //SQL.AppendLine("        From TblBudgetTransferCostCenterHdr X1 ");
            //SQL.AppendLine("        Inner Join TblBudgetTransferCostCenterDtl X2 On X1.DOcNo = X2.DocNo ");
            //SQL.AppendLine("            And X1.Status In ('A') ");
            //SQL.AppendLine("            And X1.CancelInd = 'N' ");
            //SQL.AppendLine("            And X1.Yr = @Yr ");
            //SQL.AppendLine("            And ((X1.Mth Between @Mth1 And @Mth2) Or (X1.Mth2 Between @Mth1 And @Mth2)) ");
            //SQL.AppendLine("        Group By X2.BCCode2 ");
            //SQL.AppendLine("    ) K On C.BCCode = K.BCCode2 ");
            //SQL.AppendLine("    Group By C.BCCode, C.BCName, C.AcNo, E.AcDesc, D.DeptName, F.CCtName, H.OptDesc ");
            //SQL.AppendLine(") T ");
            #endregion

            SQL.AppendLine("Select T.* From ( ");
            SQL.AppendLine("    Select E.CCCode, D.BCCode, D.BCName, F2.AcNo, F2.AcDesc, F.DeptName, F.DeptCode, C.CCtName, H.OptDesc As BudgetTypeDesc, ");
            SQL.AppendLine("    CASE WHEN L.Mth = '01' ");
            SQL.AppendLine("	 	THEN IFNULL(L.Amt,0.00) + Sum((G.Rate01*G.Qty01) - IfNull(If(I.Mth = '01', I.CASQty*G.Rate01, 0.00), 0.00)) ");
            SQL.AppendLine("		ELSE Sum((G.Rate01*G.Qty01) - IfNull(If(I.Mth = '01', I.CASQty*G.Rate01, 0.00), 0.00)) ");
            SQL.AppendLine("	 END Amt01, ");
            SQL.AppendLine("	 CASE WHEN L.Mth = '02' ");
            SQL.AppendLine("	 	THEN IFNULL(L.Amt,0.00) + Sum((G.Rate02*G.Qty02) - IfNull(If(I.Mth = '02', I.CASQty*G.Rate02, 0.00), 0.00)) ");
            SQL.AppendLine("		ELSE Sum((G.Rate02*G.Qty02) - IfNull(If(I.Mth = '02', I.CASQty*G.Rate02, 0.00), 0.00)) ");
            SQL.AppendLine("	 END Amt02, ");
            SQL.AppendLine("	 CASE WHEN l.Mth = '03' ");
            SQL.AppendLine("	 	THEN IFNULL(L.Amt,0.00) + Sum((G.Rate03*G.Qty03) - IfNull(If(I.Mth = '03', I.CASQty*G.Rate03, 0.00), 0.00)) ");
            SQL.AppendLine("		ELSE Sum((G.Rate03*G.Qty03) - IfNull(If(I.Mth = '03', I.CASQty*G.Rate03, 0.00), 0.00)) ");
            SQL.AppendLine("	 END Amt03, ");
            SQL.AppendLine("	 CASE WHEN L.Mth = '04'	");
            SQL.AppendLine("	 	THEN IFNULL(L.Amt,0.00) + Sum((G.Rate04*G.Qty04) - IfNull(If(I.Mth = '04', I.CASQty*G.Rate04, 0.00), 0.00)) ");
            SQL.AppendLine("		ELSE Sum((G.Rate04*G.Qty04) - IfNull(If(I.Mth = '04', I.CASQty*G.Rate04, 0.00), 0.00)) ");
            SQL.AppendLine("	 END Amt04, ");
            SQL.AppendLine("	 CASE WHEN L.Mth = '05' ");
            SQL.AppendLine("	 	THEN IFNULL(L.Amt, 0.00) + Sum((G.Rate05*G.Qty05) - IfNull(If(I.Mth = '05', I.CASQty*G.Rate05, 0.00), 0.00)) ");
            SQL.AppendLine("		ELSE Sum((G.Rate05*G.Qty05) - IfNull(If(I.Mth = '05', I.CASQty*G.Rate05, 0.00), 0.00)) ");
            SQL.AppendLine("	 END Amt05, ");
            SQL.AppendLine("	 CASE WHEN L.Mth = '06' ");
            SQL.AppendLine("	 	THEN IFNULL(L.Amt,0.00) + Sum((G.Rate06*G.Qty06) - IfNull(If(I.Mth = '06', I.CASQty*G.Rate06, 0.00), 0.00)) ");
            SQL.AppendLine("	 	ELSE Sum((G.Rate06*G.Qty06) - IfNull(If(I.Mth = '06', I.CASQty*G.Rate06, 0.00), 0.00)) ");
            SQL.AppendLine("	 END Amt06, ");
            SQL.AppendLine("	 CASE WHEN L.Mth = '07' ");
            SQL.AppendLine("	 	THEN IFNULL(L.Amt,0.00) + Sum((G.Rate07*G.Qty07) - IfNull(If(I.Mth = '07', I.CASQty*G.Rate07, 0.00), 0.00)) ");
            SQL.AppendLine("	 	ELSE Sum((G.Rate07*G.Qty07) - IfNull(If(I.Mth = '07', I.CASQty*G.Rate07, 0.00), 0.00)) ");
            SQL.AppendLine("    END Amt07, ");
            SQL.AppendLine("    CASE WHEN L.Mth = '08' ");
            SQL.AppendLine("	 	THEN IFNULL(L.Amt,0.00) + Sum((G.Rate08*G.Qty08) - IfNull(If(I.Mth = '08', I.CASQty*G.Rate08, 0.00), 0.00)) ");
            SQL.AppendLine("		ELSE Sum((G.Rate08*G.Qty08) - IfNull(If(I.Mth = '08', I.CASQty*G.Rate08, 0.00), 0.00)) ");
            SQL.AppendLine("	 END Amt08, ");
            SQL.AppendLine("    CASE WHEN L.Mth = '09' ");
            SQL.AppendLine("	 	THEN IFNULL(L.Amt,0.00) + Sum((G.Rate09*G.Qty09) - IfNull(If(I.Mth = '09', I.CASQty*G.Rate09, 0.00), 0.00)) ");
            SQL.AppendLine("		ELSE Sum((G.Rate09*G.Qty09) - IfNull(If(I.Mth = '09', I.CASQty*G.Rate09, 0.00), 0.00)) ");
            SQL.AppendLine("	 END Amt09, ");
            SQL.AppendLine("	 CASE WHEN L.Mth = '10' ");
            SQL.AppendLine("	 	THEN IFNULL(L.Amt,0.00) + Sum((G.Rate10*G.Qty10) - IfNull(If(I.Mth = '10', I.CASQty*G.Rate10, 0.00), 0.00)) ");
            SQL.AppendLine("		ELSE Sum((G.Rate10*G.Qty10) - IfNull(If(I.Mth = '10', I.CASQty*G.Rate10, 0.00), 0.00)) ");
            SQL.AppendLine("	 END Amt10, ");
            SQL.AppendLine("	 CASE WHEN L.Mth = '11' ");
            SQL.AppendLine("		 THEN IFNULL(L.Amt,0.00) + Sum((G.Rate11*G.Qty11) - IfNull(If(I.Mth = '11', I.CASQty*G.Rate11, 0.00), 0.00)) ");
            SQL.AppendLine("		 ELSE Sum((G.Rate11*G.Qty11) - IfNull(If(I.Mth = '11', I.CASQty*G.Rate11, 0.00), 0.00)) ");
            SQL.AppendLine("	 END Amt11, ");
            SQL.AppendLine("	 CASE WHEN L.Mth = '12' ");
            SQL.AppendLine("	 	 THEN IFNULL(L.Amt,0.00) + Sum((G.Rate12*G.Qty12) - IfNull(If(I.Mth = '12', I.CASQty*G.Rate12, 0.00), 0.00)) ");
            SQL.AppendLine("		 ELSE Sum((G.Rate12*G.Qty12) - IfNull(If(I.Mth = '12', I.CASQty*G.Rate12, 0.00), 0.00)) ");
            SQL.AppendLine("	 END Amt12, ");
            SQL.AppendLine("    IfNull(J.TransferredAmt, 0.00) TransferredAmt, IfNull(K.ReceivedAmt, 0.00) ReceivedAmt ");
            SQL.AppendLine("    From TblCompanyBudgetPlanHdr A ");
            SQL.AppendLine("    Inner Join TblCompanyBudgetPlanDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("        And A.CancelInd = 'N' ");
            SQL.AppendLine("        And A.CCCode Is Not Null ");
            SQL.AppendLine("        And A.CompletedInd = 'Y' ");
            SQL.AppendLine("        And A.CCCode = @CCCode ");
            SQL.AppendLine("        And A.Yr = @Yr ");
            SQL.AppendLine("    Inner Join TblCostCategory C On B.AcNo = C.AcNo And C.CCCode = @CCCode ");
            SQL.AppendLine("    Inner Join TblBudgetCategory D On C.CCtCode = D.CCtCode ");
            SQL.AppendLine("    Inner Join TblCostCenter E On A.CCCode = E.CCCode ");
            SQL.AppendLine("    Inner Join TblDepartment F On E.DeptCode = F.DeptCode ");
            SQL.AppendLine("    Inner Join TblCOA F2 On C.AcNo = F2.AcNo ");
            SQL.AppendLine("    Left Join TblCompanyBudgetPlanDtl2 G  On A.DocNo = G.DocNo And B.AcNo = G.AcNo ");
            SQL.AppendLine("    Left Join TblOption H On D.BudgetType = H.OptCode And H.OptCat = 'BudgetType' ");
            
            SQL.AppendLine("    Left Join ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select T4.BCCode, T2.ItCode, Substr(T1.DocDt, 5, 2) Mth, Sum(T2.Qty) CASQty ");
            SQL.AppendLine("        From TblCashAdvanceSettlementHdr T1 ");
            SQL.AppendLine("        Inner Join TblCashAdvanceSettlementDtl T2 On T1.DocNo = T2.DocNo ");
            SQL.AppendLine("            And T1.CancelInd = 'N' ");
            SQL.AppendLine("            And T1.Status = 'A' ");
            SQL.AppendLine("            And T2.ItCode Is Not Null ");
            SQL.AppendLine("            And Left(T1.DocDt, 4) = @Yr ");
            SQL.AppendLine("            And T1.CCCode = @CCCode ");
            SQL.AppendLine("        Inner Join TblCostCategory T3 On T2.CCtCode = T3.CCtCode ");
            SQL.AppendLine("        Inner Join TblBudgetCategory T4 On T3.CCtCode = T4.CCtCode ");
            SQL.AppendLine("        Group By T4.BCCode, T2.ItCode, Left(T1.DocDt, 6) ");
            SQL.AppendLine("    ) I On D.BCCode = I.BCCode And G.ItCode = I.ItCode ");
            SQL.AppendLine("    Left Join ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select X2.BCCode, Sum(X2.TransferredAmt) TransferredAmt ");
            SQL.AppendLine("        From TblBudgetTransferCostCenterHdr X1 ");
            SQL.AppendLine("        Inner Join TblBudgetTransferCostCenterDtl X2 On X1.DOcNo = X2.DocNo ");
            SQL.AppendLine("            And X1.Status In ('A') ");
            SQL.AppendLine("            And X1.CancelInd = 'N' ");
            SQL.AppendLine("            And X1.Yr = @Yr ");
            SQL.AppendLine("             AND X1.CCCode = @CCCode");
            if (mDocType == 1)
                SQL.AppendLine("            AND X1.Mth = @Mth1 ");
            else 
                SQL.AppendLine("            AND X1.Mth = @Mth2 ");

            SQL.AppendLine("        Group By X2.BCCode ");
            SQL.AppendLine("    ) J On D.BCCode = J.BCCode ");
            SQL.AppendLine("    Left Join ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select X2.BCCode2, Sum(X2.TransferredAmt) ReceivedAmt ");
            SQL.AppendLine("        From TblBudgetTransferCostCenterHdr X1 ");
            SQL.AppendLine("        Inner Join TblBudgetTransferCostCenterDtl X2 On X1.DOcNo = X2.DocNo ");
            SQL.AppendLine("            And X1.Status In ('A') ");
            SQL.AppendLine("            And X1.CancelInd = 'N' ");
            SQL.AppendLine("            And X1.Yr = @Yr ");
            SQL.AppendLine("             AND X1.CCCode2 = @CCCode");
            if (mDocType == 1)
                SQL.AppendLine("            AND X1.Mth2 = @Mth1  ");
            else
                SQL.AppendLine("            AND X1.Mth2 = @Mth2 ");
            SQL.AppendLine("        Group By X2.BCCode2 ");
            SQL.AppendLine("    ) K On D.BCCode = K.BCCode2 ");
            SQL.AppendLine("    LEFT JOIN ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("	 		Select LEFT(A1.DocDt, 4) Yr, SUBSTRING(A1.DocDt, 5, 2) Mth, ");
            SQL.AppendLine("			Case D1.AcType When 'D' Then ");
            SQL.AppendLine("			 	SUM((B1.DAmt - B1.CAmt)) ");
            SQL.AppendLine("			ELSE ");
            SQL.AppendLine("				SUM((B1.CAmt - B1.DAmt)) End As Amt ");
            SQL.AppendLine("			, A1.CCCode ");
            SQL.AppendLine("			, G1.BCCode ");
            SQL.AppendLine("			From tbljournalhdr A1 ");
            SQL.AppendLine("			Inner Join tbljournaldtl B1 ON A1.DocNo = B1.DocNo ");
            SQL.AppendLine("				And LEFT(A1.DocDt, 4) = @Yr ");
            SQL.AppendLine("				AND A1.MenuCode Not In (Select Menucode From TblMenu Where Param = 'CashAdvanceSettlement') ");
            SQL.AppendLine("				AND A1.DocNo Not In (Select JournalDocno From TblVoucherHdr Where Doctype = '58')  ");
            SQL.AppendLine("				AND A1.CCCode Is Not Null ");
            SQL.AppendLine("				And A1.CCCode = @CCCode ");
            SQL.AppendLine("			Inner Join ");
            SQL.AppendLine("			( ");
            SQL.AppendLine("				Select Distinct X1.CCCode, X2.AcNo ");
            SQL.AppendLine("				From TblCompanyBudgetPlanHdr X1 ");
            SQL.AppendLine("				Inner Join TblCompanyBudgetPlanDtl X2 On X1.DocNo = X2.DocNo ");
            SQL.AppendLine("					And X1.CancelInd = 'N' ");
            SQL.AppendLine("					And X1.CompletedInd = 'Y' ");
            SQL.AppendLine("					And X1.CCCode Is Not Null ");
            SQL.AppendLine("					And X1.Yr = Left(@Yr, 4) ");
            SQL.AppendLine("			      And X1.CCCode = @CCCode ");
            SQL.AppendLine("			) C1 ON A1.CCCode = C1.CCCode AND B1.AcNo = C1.AcNo ");
            SQL.AppendLine("			Inner Join tblcoa D1 ON B1.AcNo = D1.AcNo AND D1.ActInd = 'Y' ");
            SQL.AppendLine("			Inner Join TblCostCategory D2 ON B1.AcNo = D2.AcNo AND D1.ActInd = 'Y' And D2.CCCode = A1.CCCode ");
            SQL.AppendLine("			INNER JOIN tblbudgetcategory G1 ON D2.CCtcODE = G1.CCtCode ");
            SQL.AppendLine("			INNER JOIN tblcostcenter H1 ON H1.CCCode = D2.CCCode ");
            SQL.AppendLine("			GROUP BY LEFT(A1.DocDt, 4), SUBSTRING(A1.DocDt, 5, 2),  A1.CCCode, G1.BCCode ");
            SQL.AppendLine("	 ) L ON L.CCCode =  E.CCCode AND L.BCCode = D.BCCode ");
            SQL.AppendLine("    Group By D.BCCode, D.BCName, F2.AcNo, F2.AcDesc, F.DeptName, C.CCtName, H.OptDesc ");
            SQL.AppendLine(") T ");


            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            IsRefreshed = true;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                l.Clear(); l1.Clear(); l2.Clear(); l3.Clear(); l4.Clear(); l5.Clear(); l6.Clear(); l7.Clear();
                
                PrepData(ref l1, mCCCode, mYr, ref l2);
                ProcessBudgetCategory(ref l1, ref l3, ref l2, IsRefreshed);
                if (l3.Count > 0)
                {
                    ProcessCAS(ref l4);
                    ProcessBudgetTransfer(ref l5);
                    ProcessBudgetReceive(ref l6);
                    ProcessJournal(ref l7);
                    ProcessBudgetCategoryOnCASAndJournal(ref l3, ref l4, ref l7);

                    var l4sum = new List<CAS>();
                    var l7sum = new List<Journal>();

                    ProcessSumCAS(ref l4, ref l4sum);
                    ProcessSumJournal(ref l7, ref l7sum);

                    ProcessAddTransferAmt(ref l1, ref l5);
                    ProcessAddReceiveAmt(ref l1, ref l6);

                    FinalProcess(ref l, ref l1, ref l4sum, ref l7sum);
                }
                else
                {
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
                }

                #region Old Code

                //string Filter = " Where 0 = 0 ";

                //var cm = new MySqlCommand();

                //Sm.CmParam<String>(ref cm, "@CCCode", mCCCode);
                //Sm.CmParam<String>(ref cm, "@Yr", mYr);
                //Sm.CmParam<String>(ref cm, "@Mth1",mMth1);
                //Sm.CmParam<String>(ref cm, "@Mth2", mMth2);

                //Sm.FilterStr(ref Filter, ref cm, TxtBCCode.Text, new string[] { "T.BCCode", "T.BCName" });

                //Sm.ShowDataInGrid(
                //    ref Grd1, ref cm, mSQL + Filter + " Order By T.BCName;",
                //    new string[] 
                //    { 
                //        "BCCode", 
                //        "BCName", "Amt01", "Amt02", "Amt03", "Amt04",
                //        "Amt05", "Amt06", "Amt07", "Amt08", "Amt09",
                //        "Amt10", "Amt11", "Amt12", "TransferredAmt", "ReceivedAmt",
                //        "AcNo", "AcDesc", "DeptName", "CCtName", "BudgetTypeDesc"
                //    },
                //    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                //    {
                //        Grd.Cells[Row, 0].Value = Row + 1;
                //        Grd.Cells[Row, 1].Value = false;
                //        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                //        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                //        Grd.Cells[Row, 4].Value = 0m;
                //        Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 2);
                //        Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 3);
                //        Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 4);
                //        Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 5);
                //        Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 6);
                //        Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 7);
                //        Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 8);
                //        Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 9);
                //        Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 10);
                //        Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 11);
                //        Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 12);
                //        Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 13);
                //        Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 14);
                //        Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 15);
                //        Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 16);
                //        Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 17);
                //        Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 18);
                //        Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 19);
                //        Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 20);
                //    }, true, false, false, false
                //);
                #endregion

                GetRemainingAmount(mYr, mMth1, mMth2);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (mDocType == 1)
            {
                if (Grd1.Rows.Count != 0)
                {                    
                    int Row1 = 0, Row2 = 0;
                    bool IsChoose = false;

                    for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    {
                        if (Sm.GetGrdBool(Grd1, Row, 1))
                        {
                            if (IsChoose == false) IsChoose = true;

                            Row1 = mFrmParent.Grd1.Rows.Count - 1;
                            Row2 = Row;

                            mFrmParent.Grd1.Cells[Row1, 1].Value = mFrmParent.TxtCCName.Text;
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 2, Grd1, Row2, 2);
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 3, Grd1, Row2, 3);
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 4, Grd1, Row2, 4);
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 13, Grd1, Row2, 4);
                            Sm.SetGrdNumValueZero(mFrmParent.Grd1, Row1, new int[] { 5, 10, 11 });

                            mFrmParent.Grd1.Rows.Add();
                            Sm.SetGrdNumValueZero(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 4, 5, 10, 11, 13 });
                        }
                    }
                }
            }
            else
            {
                if (Sm.IsFindGridValid(Grd1, 2))
                {
                    int Row = Grd1.CurRow.Index;

                    mFrmParent.Grd1.Cells[mCurRow, 7].Value = mFrmParent.TxtCCName2.Text;
                    Sm.CopyGrdValue(mFrmParent.Grd1, mCurRow, 8, Grd1, Row, 2);
                    Sm.CopyGrdValue(mFrmParent.Grd1, mCurRow, 9, Grd1, Row, 3);
                    Sm.CopyGrdValue(mFrmParent.Grd1, mCurRow, 10, Grd1, Row, 4);
                    mFrmParent.Grd1.Cells[mCurRow, 11].Value = Sm.GetGrdDec(mFrmParent.Grd1, mCurRow, 10) + Sm.GetGrdDec(mFrmParent.Grd1, mCurRow, 5);

                    Sm.SetGrdStringValueEmpty(mFrmParent.Grd1, mCurRow, new int[] { 21, 22 });
                    Sm.SetGrdNumValueZero(mFrmParent.Grd1, mCurRow, new int[] { 23, 24, 25 });
                    this.Close();
                }
            }            
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0 && mDocType == 1)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    Grd1.Cells[Row, 1].Value = !IsSelected;
            }
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (mDocType == 2)
            {
                ChooseData();
            }
        }

        #endregion

        #region Additional Methods

        private void PrepData(ref List<Prep1> l1, string CCCode, string Yr, ref List<CostCategory> l2)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@CCCode", CCCode);
            Sm.CmParam<String>(ref cm, "@Yr", Yr);

            SQL.AppendLine("Select C.CCCode, C.CCtCode, C.CCtName, F.DeptName, F2.AcNo, F2.AcDesc, G.Rate01, G.ItCode, G.Qty01, ");
            SQL.AppendLine("G.Rate02, G.Qty02, G.Rate03, G.Qty03, G.Rate04, G.Qty04, G.Rate05, G.Qty05, ");
            SQL.AppendLine("G.Rate06, G.Qty06, G.Rate07, G.Qty07, G.Rate08, G.Qty08, G.Rate09, G.Qty09, ");
            SQL.AppendLine("G.Rate10, G.Qty10, G.Rate11, G.Qty11, G.Rate12, G.Qty12 ");
            SQL.AppendLine("From TblCompanyBudgetPlanHdr A ");
            SQL.AppendLine("Inner Join TblCompanyBudgetPlanDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("    And A.CancelInd = 'N' ");
            SQL.AppendLine("    And A.CCCode Is Not Null ");
            SQL.AppendLine("    And A.CompletedInd = 'Y' ");
            SQL.AppendLine("    And A.CCCode = @CCCode ");
            SQL.AppendLine("    And A.Yr = @Yr ");
            SQL.AppendLine("Inner Join TblCostCategory C On B.AcNo = C.AcNo And C.CCCode = @CCCode ");
            SQL.AppendLine("Inner Join TblCostCenter E On A.CCCode = E.CCCode ");
            SQL.AppendLine("Inner Join TblDepartment F On E.DeptCode = F.DeptCode ");
            SQL.AppendLine("Inner Join TblCOA F2 On C.AcNo = F2.AcNo ");
            SQL.AppendLine("Left Join TblCompanyBudgetPlanDtl2 G On A.DocNo = G.DocNo And B.AcNo = G.AcNo ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn; cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr,
                    new string[]
                    {
                        //0
                        "CCCode",
                        //1-5
                        "CCtCode", "CCtName", "DeptName", "Rate01", "Qty01",
                        //6-10
                        "Rate02", "Qty02", "Rate03", "Qty03", "Rate04",
                        //11-15
                        "Qty04", "Rate05", "Qty05", "Rate06", "Qty06",
                        //16-20
                        "Rate07", "Qty07", "Rate08", "Qty08", "Rate09",
                        //21-25
                        "Qty09", "Rate10", "Qty10", "Rate11", "Qty11",
                        //26-30
                        "Rate12", "Qty12", "AcNo", "AcDesc", "ItCode"
                    }
                );

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l1.Add(new Prep1()
                        {
                            CCCode = Sm.DrStr(dr, c[0]),
                            CCtCode = Sm.DrStr(dr, c[1]),
                            CCtName = Sm.DrStr(dr, c[2]),
                            DeptName = Sm.DrStr(dr, c[3]),
                            Rate01 = Sm.DrDec(dr, c[4]),
                            Qty01 = Sm.DrDec(dr, c[5]),
                            Rate02 = Sm.DrDec(dr, c[6]),
                            Qty02 = Sm.DrDec(dr, c[7]),
                            Rate03 = Sm.DrDec(dr, c[8]),
                            Qty03 = Sm.DrDec(dr, c[9]),
                            Rate04 = Sm.DrDec(dr, c[10]),
                            Qty04 = Sm.DrDec(dr, c[11]),
                            Rate05 = Sm.DrDec(dr, c[12]),
                            Qty05 = Sm.DrDec(dr, c[13]),
                            Rate06 = Sm.DrDec(dr, c[14]),
                            Qty06 = Sm.DrDec(dr, c[15]),
                            Rate07 = Sm.DrDec(dr, c[16]),
                            Qty07 = Sm.DrDec(dr, c[17]),
                            Rate08 = Sm.DrDec(dr, c[18]),
                            Qty08 = Sm.DrDec(dr, c[19]),
                            Rate09 = Sm.DrDec(dr, c[20]),
                            Qty09 = Sm.DrDec(dr, c[21]),
                            Rate10 = Sm.DrDec(dr, c[22]),
                            Qty10 = Sm.DrDec(dr, c[23]),
                            Rate11 = Sm.DrDec(dr, c[24]),
                            Qty11 = Sm.DrDec(dr, c[25]),
                            Rate12 = Sm.DrDec(dr, c[26]),
                            Qty12 = Sm.DrDec(dr, c[27]),
                            BCCode = string.Empty,
                            BCName = string.Empty,
                            Amt01 = 0m,
                            Amt02 = 0m,
                            Amt03 = 0m,
                            Amt04 = 0m,
                            Amt05 = 0m,
                            Amt06 = 0m,
                            Amt07 = 0m,
                            Amt08 = 0m,
                            Amt09 = 0m,
                            Amt10 = 0m,
                            Amt11 = 0m,
                            Amt12 = 0m,
                            TransferredAmt = 0m,
                            ReceivedAmt = 0m,
                            AcNo = Sm.DrStr(dr, c[28]),
                            AcDesc = Sm.DrStr(dr, c[29]),
                            BudgetTypeDesc = string.Empty,
                            ItCode = Sm.DrStr(dr, c[30]),
                        });

                        l2.Add(new CostCategory()
                        {
                            CCtCode = Sm.DrStr(dr, c[1]),
                            IsProcessed = false
                        }) ; 
                    }
                }
            }
        }

        private void ProcessBudgetCategory(ref List<Prep1> l1, ref List<BudgetCategory> l3, ref List<CostCategory> l2, bool IsRefreshed)
        {
            PrepBudgetCategory(ref l3, ref l2, IsRefreshed);
            IsRefreshed = false;
            //while (l3.Count == 0) PrepBudgetCategory(ref l3, ref l2, IsRefreshed);

            if (l3.Count > 0)
            {
                foreach (var x in l1)
                {
                    foreach (var y in l3.Where(w => w.CCtCode == x.CCtCode))
                    {
                        x.BCCode = y.BCCode;
                        x.BCName = y.BCName;
                        x.BudgetTypeDesc = y.BudgetType;
                        break;
                    }
                }
            }

            //inner join ke Budget Category
            l1.RemoveAll(w => w.BCCode.Trim().Length == 0);
        }

        private void ProcessCAS(ref List<CAS> l4)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select T2.ItCode, Substr(T1.DocDt, 5, 2) Mth, T2.Qty CASQty, T3.CCtCode ");
            SQL.AppendLine("From TblCashAdvanceSettlementHdr T1 ");
            SQL.AppendLine("Inner Join TblCashAdvanceSettlementDtl T2 On T1.DocNo = T2.DocNo ");
            SQL.AppendLine("    And T1.CancelInd = 'N' ");
            SQL.AppendLine("    And T1.Status = 'A' ");
            SQL.AppendLine("    And T2.ItCode Is Not Null ");
            SQL.AppendLine("    And Left(T1.DocDt, 4) = @Yr ");
            SQL.AppendLine("    And T1.CCCode = @CCCode ");
            SQL.AppendLine("Inner Join TblCostCategory T3 On T2.CCtCode = T3.CCtCode And T3.CCCode = @CCCode ");
            //SQL.AppendLine("Inner Join TblBudgetCategory T4 On T3.CCtCode = T4.CCtCode ");
            //SQL.AppendLine("Group By T4.BCCode, T2.ItCode, Left(T1.DocDt, 6) ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn; cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@CCCode", mCCCode);
                Sm.CmParam<String>(ref cm, "@Yr", mYr);
                cm.Prepare();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ItCode", "Mth", "CASQty", "CCtCode" });

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l4.Add(new CAS()
                        {
                            BCCode = string.Empty,
                            ItCode = Sm.DrStr(dr, c[0]),
                            Mth = Sm.DrStr(dr, c[1]),
                            CASQty = Sm.DrDec(dr, c[2]),
                            CCtCode = Sm.DrStr(dr, c[3]),
                        });
                    }
                }
            }
        }

        private void ProcessBudgetTransfer(ref List<BudgetTransfer> l5)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            var l = new List<BudgetTransfer>();

            SQL.AppendLine("Select X2.BCCode, X2.TransferredAmt TransferredAmt ");
            SQL.AppendLine("From TblBudgetTransferCostCenterHdr X1 ");
            SQL.AppendLine("Inner Join TblBudgetTransferCostCenterDtl X2 On X1.DOcNo = X2.DocNo ");
            SQL.AppendLine("    And X1.Status In ('A') ");
            SQL.AppendLine("    And X1.CancelInd = 'N' ");
            SQL.AppendLine("    And X1.Yr = @Yr ");
            SQL.AppendLine("    And X1.CCCode = @CCCode ");
            if (mDocType == 1)
                SQL.AppendLine("    AND X1.Mth = @Mth1 ");
            else
                SQL.AppendLine("    AND X1.Mth = @Mth2 ");
            SQL.AppendLine("    And X2.BCCode In ");
            SQL.AppendLine("    ( "); 
            SQL.AppendLine("        Select Distinct BCCode ");
            SQL.AppendLine("        From TblBudgetCategory ");
            SQL.AppendLine("        Where CCtCode In ");
            SQL.AppendLine("        ( ");
            SQL.AppendLine("            Select Distinct CCtCode ");
            SQL.AppendLine("            From TblCostCategory ");
            SQL.AppendLine("            Where CCCode = @CCCode ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine("    ) ");

            //SQL.AppendLine("Group By X2.BCCode ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn; cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@CCCode", mCCCode);
                Sm.CmParam<String>(ref cm, "@Yr", mYr);
                Sm.CmParam<String>(ref cm, "@Mth1", mMth1);
                Sm.CmParam<String>(ref cm, "@Mth2", mMth2);
                cm.Prepare();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "BCCode", "TransferredAmt" });

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new BudgetTransfer()
                        {
                            BCCode = Sm.DrStr(dr, c[0]),
                            TransferredAmt = Sm.DrDec(dr, c[1]),
                        });
                    }
                }
            }

            if (l.Count > 0)
            {
                l5 = l.GroupBy(g => new { g.BCCode })
                    .Select(s => new BudgetTransfer()
                    {
                        BCCode = s.Key.BCCode,
                        TransferredAmt = s.Sum(t => t.TransferredAmt)
                    }).ToList();
            }

            l.Clear();
        }

        private void ProcessBudgetReceive(ref List<BudgetReceive> l6)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            var l = new List<BudgetReceive>();

            SQL.AppendLine("Select X2.BCCode2 BCCode, X2.TransferredAmt ReceivedAmt ");
            SQL.AppendLine("From TblBudgetTransferCostCenterHdr X1 ");
            SQL.AppendLine("Inner Join TblBudgetTransferCostCenterDtl X2 On X1.DOcNo = X2.DocNo ");
            SQL.AppendLine("    And X1.Status In ('A') ");
            SQL.AppendLine("    And X1.CancelInd = 'N' ");
            SQL.AppendLine("    And X1.Yr = @Yr ");
            SQL.AppendLine("    And X1.CCCode2 = @CCCode ");
            if (mDocType == 1)
                SQL.AppendLine("    AND X1.Mth2 = @Mth1 ");
            else
                SQL.AppendLine("    AND X1.Mth2 = @Mth2 ");
            SQL.AppendLine("    And X2.BCCode2 In ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select Distinct BCCode ");
            SQL.AppendLine("        From TblBudgetCategory ");
            SQL.AppendLine("        Where CCtCode In ");
            SQL.AppendLine("        ( ");
            SQL.AppendLine("            Select Distinct CCtCode ");
            SQL.AppendLine("            From TblCostCategory ");
            SQL.AppendLine("            Where CCCode = @CCCode ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine("    ) ");

            //SQL.AppendLine("Group By X2.BCCode ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn; cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@CCCode", mCCCode);
                Sm.CmParam<String>(ref cm, "@Yr", mYr);
                Sm.CmParam<String>(ref cm, "@Mth1", mMth1);
                Sm.CmParam<String>(ref cm, "@Mth2", mMth2);
                cm.Prepare();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "BCCode", "ReceivedAmt" });

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new BudgetReceive()
                        {
                            BCCode = Sm.DrStr(dr, c[0]),
                            ReceivedAmt = Sm.DrDec(dr, c[1]),
                        });
                    }
                }
            }

            if (l.Count > 0)
            {
                l6 = l.GroupBy(g => new { g.BCCode })
                    .Select(s => new BudgetReceive()
                    {
                        BCCode = s.Key.BCCode,
                        ReceivedAmt = s.Sum(t => t.ReceivedAmt)
                    }).ToList();
            }

            l.Clear();
        }

        private void ProcessJournal(ref List<Journal> l7)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select @Yr As Yr, SUBSTRING(A1.DocDt, 5, 2) Mth, D2.CCtCode, ");
            SQL.AppendLine("Case D1.AcType When 'D' Then ");
            SQL.AppendLine("    (B1.DAmt - B1.CAmt) ");
            SQL.AppendLine("Else ");
            SQL.AppendLine("    (B1.CAmt - B1.DAmt) ");
            SQL.AppendLine("End As Amt ");
            SQL.AppendLine(", A1.CCCode ");
            SQL.AppendLine("From TblJournalHdr A1 ");
            SQL.AppendLine("Inner Join TblJournalDtl B1 ON A1.DocNo = B1.DocNo ");
            SQL.AppendLine("    And LEFT(A1.DocDt, 4) = @Yr ");
            SQL.AppendLine("    AND A1.MenuCode Not In (Select Menucode From TblMenu Where Param = 'CashAdvanceSettlement') ");
            SQL.AppendLine("    AND A1.DocNo Not In (Select JournalDocno From TblVoucherHdr Where Doctype = '58')  ");
            SQL.AppendLine("    AND A1.CCCode Is Not Null ");
            SQL.AppendLine("    And A1.CCCode = @CCCode ");
            SQL.AppendLine("Inner Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select Distinct X1.CCCode, X2.AcNo ");
            SQL.AppendLine("    From TblCompanyBudgetPlanHdr X1 ");
            SQL.AppendLine("    Inner Join TblCompanyBudgetPlanDtl X2 On X1.DocNo = X2.DocNo ");
            SQL.AppendLine("        And X1.CancelInd = 'N' ");
            SQL.AppendLine("        And X1.CompletedInd = 'Y' ");
            SQL.AppendLine("        And X1.CCCode Is Not Null ");
            SQL.AppendLine("        And X1.Yr = @Yr ");
            SQL.AppendLine("        And X1.CCCode = @CCCode ");
            SQL.AppendLine(") C1 ON A1.CCCode = C1.CCCode AND B1.AcNo = C1.AcNo ");
            SQL.AppendLine("Inner Join tblcoa D1 ON B1.AcNo = D1.AcNo AND D1.ActInd = 'Y' ");
            SQL.AppendLine("Inner Join TblCostCategory D2 ON B1.AcNo = D2.AcNo AND D1.ActInd = 'Y' And D2.CCCode = A1.CCCode ");
            //SQL.AppendLine("INNER JOIN tblbudgetcategory G1 ON D2.CCtcODE = G1.CCtCode ");
            SQL.AppendLine("INNER JOIN tblcostcenter H1 ON H1.CCCode = D2.CCCode ");
            //SQL.AppendLine("GROUP BY LEFT(A1.DocDt, 4), SUBSTRING(A1.DocDt, 5, 2),  A1.CCCode, G1.BCCode ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn; cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@CCCode", mCCCode);
                Sm.CmParam<String>(ref cm, "@Yr", mYr);
                cm.Prepare();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "Yr", "Mth", "Amt", "CCCode", "CCtCode" });

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l7.Add(new Journal()
                        {
                            BCCode = string.Empty,
                            Yr = Sm.DrStr(dr, c[0]),
                            Mth = Sm.DrStr(dr, c[1]),
                            Amt = Sm.DrDec(dr, c[2]),
                            CCCode = Sm.DrStr(dr, c[3]),
                            CCtCode = Sm.DrStr(dr, c[4])
                        });
                    }
                }
            }
        }

        private void ProcessBudgetCategoryOnCASAndJournal(ref List<BudgetCategory> l3, ref List<CAS> l4, ref List<Journal> l7)
        {
            foreach (var x in l3)
            {
                foreach (var y in l7.Where(w => w.BCCode.Trim().Length == 0 && w.CCtCode == x.CCtCode))
                {
                    y.BCCode = x.BCCode;
                    break;
                }

                foreach (var y in l4.Where(w => w.BCCode.Trim().Length == 0 && w.CCtCode == x.CCtCode))
                {
                    y.BCCode = x.BCCode;
                    break;
                }
            }

            l4.RemoveAll(w => w.BCCode.Trim().Length == 0);
            l7.RemoveAll(w => w.BCCode.Trim().Length == 0);
        }

        private void ProcessSumCAS(ref List<CAS> l4, ref List<CAS> l4sum)
        {
            if (l4.Count > 0)
            {
                l4sum = l4.GroupBy(g => new { g.BCCode, g.ItCode, g.Mth })
                    .Select(t => new CAS()
                    {
                        BCCode = t.Key.BCCode,
                        ItCode = t.Key.ItCode,
                        Mth = t.Key.Mth,
                        CASQty = t.Sum(s => s.CASQty)
                    }).ToList();
            }
        }

        private void ProcessSumJournal(ref List<Journal> l7, ref List<Journal> l7sum)
        {
            if (l7.Count > 0)
            {
                l7sum = l7.GroupBy(g => new { g.BCCode, g.Mth, g.CCCode })
                    .Select(t => new Journal()
                    {
                        BCCode = t.Key.BCCode,
                        CCCode = t.Key.CCCode,
                        Mth = t.Key.Mth,
                        Amt = t.Sum(s => s.Amt)
                    }).ToList();
            }
        }

        private void ProcessAddTransferAmt(ref List<Prep1> l1, ref List<BudgetTransfer> l5)
        {
            if (l5.Count > 0)
                foreach (var x in l5)
                    foreach (var y in l1.Where(w => w.BCCode == x.BCCode))
                        y.TransferredAmt += x.TransferredAmt;
        }

        private void ProcessAddReceiveAmt(ref List<Prep1> l1, ref List<BudgetReceive> l6)
        {
            if (l6.Count > 0)
                foreach (var x in l6)
                    foreach (var y in l1.Where(w => w.BCCode == x.BCCode))
                        y.ReceivedAmt += x.ReceivedAmt;
        }

        private void FinalProcess(ref List<BTCCDlg> l, ref List<Prep1> l1, ref List<CAS> l4sum, ref List<Journal> l7sum)
        {
            var ldummy = new List<BTCCDlg>();
            foreach (var x in l1)
            {
                x.Amt01 += x.Rate01 * x.Qty01;
                x.Amt02 += x.Rate02 * x.Qty02;
                x.Amt03 += x.Rate03 * x.Qty03;
                x.Amt04 += x.Rate04 * x.Qty04;
                x.Amt05 += x.Rate05 * x.Qty05;
                x.Amt06 += x.Rate06 * x.Qty06;
                x.Amt07 += x.Rate07 * x.Qty07;
                x.Amt08 += x.Rate08 * x.Qty08;
                x.Amt09 += x.Rate09 * x.Qty09;
                x.Amt10 += x.Rate10 * x.Qty10;
                x.Amt11 += x.Rate11 * x.Qty11;
                x.Amt12 += x.Rate12 * x.Qty12;

                if (l4sum.Count > 0)
                {
                    foreach (var y in l4sum.Where(w => w.BCCode == x.BCCode && w.ItCode == x.ItCode))
                    {
                        if (y.Mth == "01") x.Amt01 -= (y.CASQty * x.Rate01);
                        if (y.Mth == "02") x.Amt02 -= (y.CASQty * x.Rate02);
                        if (y.Mth == "03") x.Amt03 -= (y.CASQty * x.Rate03);
                        if (y.Mth == "04") x.Amt04 -= (y.CASQty * x.Rate04);
                        if (y.Mth == "05") x.Amt05 -= (y.CASQty * x.Rate05);
                        if (y.Mth == "06") x.Amt06 -= (y.CASQty * x.Rate06);
                        if (y.Mth == "07") x.Amt07 -= (y.CASQty * x.Rate07);
                        if (y.Mth == "08") x.Amt08 -= (y.CASQty * x.Rate08);
                        if (y.Mth == "09") x.Amt09 -= (y.CASQty * x.Rate09);
                        if (y.Mth == "10") x.Amt10 -= (y.CASQty * x.Rate10);
                        if (y.Mth == "11") x.Amt11 -= (y.CASQty * x.Rate11);
                        if (y.Mth == "12") x.Amt12 -= (y.CASQty * x.Rate12);
                    }
                }

                ldummy.Add(new BTCCDlg()
                {
                    BCCode = x.BCCode,
                    BCName = x.BCName,
                    Amt01 = x.Amt01,
                    Amt02 = x.Amt02,
                    Amt03 = x.Amt03,
                    Amt04 = x.Amt04,
                    Amt05 = x.Amt05,
                    Amt06 = x.Amt06,
                    Amt07 = x.Amt07,
                    Amt08 = x.Amt08,
                    Amt09 = x.Amt09,
                    Amt10 = x.Amt10,
                    Amt11 = x.Amt11,
                    Amt12 = x.Amt12,
                    AcNo = x.AcNo,
                    AcDesc = x.AcDesc,
                    DeptName = x.DeptName,
                    CCtName = x.CCtName,
                    BudgetTypeDesc = x.BudgetTypeDesc,
                    TransferredAmt = x.TransferredAmt,
                    ReceivedAmt = x.ReceivedAmt,
                    CCCode = x.CCCode
                });
            }

            l = ldummy
                .GroupBy(g => new { g.BCCode, g.BCName, g.AcNo, g.AcDesc, g.DeptName, g.CCtName, g.BudgetTypeDesc, g.TransferredAmt, g.ReceivedAmt, g.CCCode })
                .Select(t => new BTCCDlg()
                {
                    BCCode = t.Key.BCCode,
                    BCName = t.Key.BCName,
                    Amt01 = t.Sum(s => s.Amt01),
                    Amt02 = t.Sum(s => s.Amt02),
                    Amt03 = t.Sum(s => s.Amt03),
                    Amt04 = t.Sum(s => s.Amt04),
                    Amt05 = t.Sum(s => s.Amt05),
                    Amt06 = t.Sum(s => s.Amt06),
                    Amt07 = t.Sum(s => s.Amt07),
                    Amt08 = t.Sum(s => s.Amt08),
                    Amt09 = t.Sum(s => s.Amt09),
                    Amt10 = t.Sum(s => s.Amt10),
                    Amt11 = t.Sum(s => s.Amt11),
                    Amt12 = t.Sum(s => s.Amt12),
                    AcNo = t.Key.AcNo,
                    AcDesc = t.Key.AcDesc,
                    DeptName = t.Key.DeptName,
                    CCtName = t.Key.CCtName,
                    BudgetTypeDesc = t.Key.BudgetTypeDesc,
                    TransferredAmt = t.Key.TransferredAmt,
                    ReceivedAmt = t.Key.ReceivedAmt,
                    CCCode = t.Key.CCCode
                })
                .ToList();

            Grd1.BeginUpdate();
            int Row = 0;
            foreach (var x in l.OrderBy(o => o.BCName))
            {
                if (l7sum.Count > 0)
                {
                    foreach (var y in l7sum.Where(w => w.BCCode == x.BCCode && w.CCCode == x.CCCode))
                    {
                        if (y.Mth == "01") x.Amt01 += y.Amt;
                        if (y.Mth == "02") x.Amt02 += y.Amt;
                        if (y.Mth == "03") x.Amt03 += y.Amt;
                        if (y.Mth == "04") x.Amt04 += y.Amt;
                        if (y.Mth == "05") x.Amt05 += y.Amt;
                        if (y.Mth == "06") x.Amt06 += y.Amt;
                        if (y.Mth == "07") x.Amt07 += y.Amt;
                        if (y.Mth == "08") x.Amt08 += y.Amt;
                        if (y.Mth == "09") x.Amt09 += y.Amt;
                        if (y.Mth == "10") x.Amt10 += y.Amt;
                        if (y.Mth == "11") x.Amt11 += y.Amt;
                        if (y.Mth == "12") x.Amt12 += y.Amt;
                    }
                }

                Grd1.Rows.Add();

                Grd1.Cells[Row, 0].Value = Row + 1;
                Grd1.Cells[Row, 2].Value = x.BCCode;
                Grd1.Cells[Row, 3].Value = x.BCName;
                Grd1.Cells[Row, 4].Value = Sm.FormatNum(0m, 0);
                Grd1.Cells[Row, 5].Value = x.Amt01;

                Grd1.Cells[Row, 6].Value = x.Amt02;
                Grd1.Cells[Row, 7].Value = x.Amt03;
                Grd1.Cells[Row, 8].Value = x.Amt04;
                Grd1.Cells[Row, 9].Value = x.Amt05;
                Grd1.Cells[Row, 10].Value = x.Amt06;

                Grd1.Cells[Row, 11].Value = x.Amt07;
                Grd1.Cells[Row, 12].Value = x.Amt08;
                Grd1.Cells[Row, 13].Value = x.Amt09;
                Grd1.Cells[Row, 14].Value = x.Amt10;
                Grd1.Cells[Row, 15].Value = x.Amt11;

                Grd1.Cells[Row, 16].Value = x.Amt12;
                Grd1.Cells[Row, 17].Value = x.TransferredAmt;
                Grd1.Cells[Row, 18].Value = x.ReceivedAmt;
                Grd1.Cells[Row, 19].Value = x.AcNo;
                Grd1.Cells[Row, 20].Value = x.AcDesc;

                Grd1.Cells[Row, 21].Value = x.DeptName;
                Grd1.Cells[Row, 22].Value = x.CCtName;
                Grd1.Cells[Row, 23].Value = x.BudgetTypeDesc;

                Row++;
            }
            Grd1.EndUpdate();

            ldummy.Clear();
        }

        private void PrepBudgetCategory(ref List<BudgetCategory> l3, ref List<CostCategory> l2, bool IsRefreshed)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            //string CCtCodes = string.Join(",", l2.ToArray());
            //Sm.CmParam<String>(ref cm, "@CCtCode", CCtCodes);

            bool IsFirst = false;
            if (IsRefreshed)
            {
                offset = 0;
                IsFirst = true;
            }
            int limit = 500000;

            //while (offset <= dataCounts)
            //{
            SQL.AppendLine("Select A.BCCode, A.BCName, A.CCtCode, B.OptDesc As BudgetTypeDesc ");
            SQL.AppendLine("From TblBudgetCategory A ");
            SQL.AppendLine("Left Join TblOption B On A.BudgetType = B.OptCode And B.OptCat = 'BudgetType' ");
            //SQL.AppendLine("Limit " + offset + ", " + limit + " ");
            SQL.AppendLine("Where A.CCtCode In (Select Distinct CCtCode From TblCostCategory Where CCCode = @CCCode) ");
            if (ChkBCCode.Checked)
                SQL.AppendLine("And (A.BCCode Like @BCCode Or A.BCName Like @BCCode) ");
            SQL.AppendLine("; ");

            offset += limit;

            if (IsFirst)
            {
                offset += 1;
                IsFirst = false;
            }

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn; cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@CCCode", mCCCode);
                Sm.CmParam<String>(ref cm, "@BCCode", string.Concat("%", TxtBCCode.Text.Trim(), "%"));
                cm.Prepare();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "BCCode", "BCName", "CCtCode", "BudgetTypeDesc" });

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l3.Add(new BudgetCategory()
                        {
                            BCCode = Sm.DrStr(dr, c[0]),
                            BCName = Sm.DrStr(dr, c[1]),
                            CCtCode = Sm.DrStr(dr, c[2]),
                            BudgetType = Sm.DrStr(dr, c[3]),
                            IsCCtCodeExists = false
                        });
                    }
                }
            }
            //foreach (var x in l3)
            //{
            //    foreach (var y in l2)
            //    {
            //        if (x.CCtCode == y)
            //        {
            //            x.IsCCtCodeExists = true;
            //            break;
            //        }
            //    }
            //}
            //bool IsBreak = false;
            //foreach (var x in l3.Where(w => !w.IsCCtCodeExists).OrderBy(o => o.CCtCode))
            //{
            //    foreach (var y in l2.Where(w => !w.IsProcessed).OrderBy(o => o.CCtCode))
            //    {
            //        //kalau Cost Category dari Budget Category nya itu lebih kecil dari CostCategory pertama yang ada di l2, langsung break semuanya biar gak lama load nya
            //        if (
            //            (decimal.Parse(x.CCtCode) < decimal.Parse(y.CCtCode)) ||
            //            (decimal.Parse(x.CCtCode) > decimal.Parse(y.CCtCode))
            //            )
            //        {
            //            IsBreak = true;
            //            break;
            //        }

            //        if (x.CCtCode == y.CCtCode) // 1 Budget Category = 1 Cost Category
            //        {
            //            x.IsCCtCodeExists = true;
            //            y.IsProcessed = true;
            //            break;
            //        }
            //    }

            //    if (IsBreak) break;
            //}

            ////l3.RemoveAll(w => !l2.Any(t => t == w.CCtCode));
            //l3.RemoveAll(w => !w.IsCCtCodeExists);
            //}
        }

        private void ProcessAllData(ref List<BTCCDlg> l, ref List<Prep1> l1)
        {
            foreach (var x in l1)
            {
                
            };

            Grd1.BeginUpdate();
            int Row = 0;
            foreach (var x in l)
            {
                Grd1.Rows.Add();
                Grd1.Cells[Row, 0].Value = Row + 1;
                Grd1.Cells[Row, 2].Value = x.BCCode;
                Grd1.Cells[Row, 3].Value = x.BCName;

                Row += 1;
            }
            Grd1.EndUpdate();
        }

        private void GetRemainingAmount(string mYr, string mMth1, string mMth2)
        {
            if (Grd1.Rows.Count > 0)
            {
                for (int i = 0; i < Grd1.Rows.Count; ++i)
                {
                    if (Sm.GetGrdStr(Grd1, i, 2).Length > 0)
                    {
                        decimal Amt = 0m;
                        //for (int j = (Int32.Parse(mMth1) - 1); j < Int32.Parse(mMth2); ++j)
                        //{
                        int Mth = 0;
                        
                        if(mDocType == 1) Mth = Int32.Parse(mMth1)-1;
                        else if (mDocType == 2) Mth = Int32.Parse(mMth2) - 1;
                        Amt += Sm.GetGrdDec(Grd1, i, mMthCols[Mth]);
                        //}
                        Grd1.Cells[i, 4].Value = Amt - Sm.GetGrdDec(Grd1, i, 17) + Sm.GetGrdDec(Grd1, i, 18);
                    }
                }
            }
        }

        #endregion

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtBCCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
            IsRefreshed = false;
        }

        private void ChkBCCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Budget Category");
            IsRefreshed = false;
        }

        #endregion

        #region Button Click

        private void BtnLoadMore_Click(object sender, EventArgs e)
        {
            if (!IsRefreshed)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to refresh the data first.");
                BtnRefresh.Focus();
                return;
            }
        }

        #endregion

        #endregion

        #region Class

        private class Prep1
        {
            public string CCtCode { get; set; }
            public string CCCode { get; set; }
            public string BCCode { get; set; }
            public string BCName { get; set; }
            public decimal Amt01 { get; set; }
            public decimal Amt02 { get; set; }
            public decimal Amt03 { get; set; }
            public decimal Amt04 { get; set; }
            public decimal Amt05 { get; set; }
            public decimal Amt06 { get; set; }
            public decimal Amt07 { get; set; }
            public decimal Amt08 { get; set; }
            public decimal Amt09 { get; set; }
            public decimal Amt10 { get; set; }
            public decimal Amt11 { get; set; }
            public decimal Amt12 { get; set; }
            public decimal Rate01 { get; set; }
            public decimal Rate02 { get; set; }
            public decimal Rate03 { get; set; }
            public decimal Rate04 { get; set; }
            public decimal Rate05 { get; set; }
            public decimal Rate06 { get; set; }
            public decimal Rate07 { get; set; }
            public decimal Rate08 { get; set; }
            public decimal Rate09 { get; set; }
            public decimal Rate10 { get; set; }
            public decimal Rate11 { get; set; }
            public decimal Rate12 { get; set; }
            public decimal Qty01 { get; set; }
            public decimal Qty02 { get; set; }
            public decimal Qty03 { get; set; }
            public decimal Qty04 { get; set; }
            public decimal Qty05 { get; set; }
            public decimal Qty06 { get; set; }
            public decimal Qty07 { get; set; }
            public decimal Qty08 { get; set; }
            public decimal Qty09 { get; set; }
            public decimal Qty10 { get; set; }
            public decimal Qty11 { get; set; }
            public decimal Qty12 { get; set; }
            public decimal TransferredAmt { get; set; }
            public decimal ReceivedAmt { get; set; }
            public string AcNo { get; set; }
            public string AcDesc { get; set; }
            public string DeptName { get; set; }
            public string CCtName { get; set; }
            public string BudgetTypeDesc { get; set; }
            public string ItCode { get; set; }
        }

        private class BTCCDlg
        {
            public string BCCode { get; set; }
            public string BCName { get; set; }
            public decimal Amt01 { get; set; }
            public decimal Amt02 { get; set; }
            public decimal Amt03 { get; set; }
            public decimal Amt04 { get; set; }
            public decimal Amt05 { get; set; }
            public decimal Amt06 { get; set; }
            public decimal Amt07 { get; set; }
            public decimal Amt08 { get; set; }
            public decimal Amt09 { get; set; }
            public decimal Amt10 { get; set; }
            public decimal Amt11 { get; set; }
            public decimal Amt12 { get; set; }
            public decimal TransferredAmt { get; set; }
            public decimal ReceivedAmt { get; set; }
            public string AcNo { get; set; }
            public string AcDesc { get; set; }
            public string DeptName { get; set; }
            public string CCtName { get; set; }
            public string BudgetTypeDesc { get; set; }
            public string CCCode { get; set; }
        }

        private class CostCategory
        {
            public string CCtCode { get; set; }
            public bool IsProcessed { get; set; }
        }

        private class BudgetCategory
        {
            public string BCCode { get; set; }
            public string BCName { get; set; }
            public string CCtCode { get; set; }
            public string BudgetType { get; set; }
            public bool IsCCtCodeExists { get; set; }
        }

        private class CAS
        {
            public string BCCode { get; set; }
            public string ItCode { get; set; }
            public string Mth { get; set; }
            public decimal CASQty { get; set; }
            public string CCtCode { get; set; }
        }

        private class BudgetTransfer
        {
            public string BCCode { get; set; }
            public decimal TransferredAmt { get; set; }
        }

        private class BudgetReceive
        {
            public string BCCode { get; set; }
            public decimal ReceivedAmt { get; set; }
        }

        private class Journal
        {
            public string Yr { get; set; }
            public string Mth { get; set; }
            public string CCCode { get; set; }
            public decimal Amt { get; set; }
            public string BCCode { get; set; }
            public string CCtCode { get; set; }
        }

        #endregion

    }
}