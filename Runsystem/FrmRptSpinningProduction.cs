﻿#region Update
/*
    29/04/2018 [TKG] reporting baru
    16/05/2018 [HAR] printOut
    06/06/2018 [TKG] machine menjadi 2 decimal point
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Collections;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptSpinningProduction : Form
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty;
        
        #endregion

        #region Constructor

        public FrmRptSpinningProduction(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        private void SetGrd()
        {
            #region Grid 1

            Grd1.Cols.Count = 1;
            Grd1.FrozenArea.ColCount = 1;
            Sm.GrdHdrWithColWidth(
                    Grd1, 
                    new string[]{ "Machine Data" },
                    new int[]{ 150 }
                );
            Grd1.Rows.Count = 2;
            Grd1.Cells[0, 0].Value = "Total Machine";
            Grd1.Cells[1, 0].Value = "Total Running Machine";

            Sm.SetGrdProperty(Grd1, false);

            #endregion

            #region Grid 2

            Grd2.Cols.Count = 1;
            Grd2.FrozenArea.ColCount = 1;
            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[] { "Production Data" },
                    new int[] { 150 }
                );
            Grd2.Rows.Count = 1;
            Grd2.Cells[0, 0].Value = "CD";
            Sm.SetGrdProperty(Grd2, false);

            #endregion

            #region Grid 3

            Grd3.Cols.Count = 1;
            Grd3.FrozenArea.ColCount = 1;
            Sm.GrdHdrWithColWidth(
                    Grd3,
                    new string[] { "Efficiency Data" },
                    new int[] { 150 }
                );
            Sm.SetGrdProperty(Grd3, false);

            #endregion

            #region Grid 4

            Grd4.Cols.Count = 1;
            Grd4.FrozenArea.ColCount = 1;
            Sm.GrdHdrWithColWidth(
                    Grd4,
                    new string[] { "Machine Data" },
                    new int[] { 150 }
                );
            Grd4.Rows.Count = 2;
            Grd4.Cells[0, 0].Value = "Total Machine";
            Grd4.Cells[1, 0].Value = "Total Running Machine";

            Sm.SetGrdProperty(Grd4, false);

            #endregion

            #region Grid 5

            Grd5.Cols.Count = 3;
            Grd5.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd5,
                    new string[] { "Production Data", "Item's Code", "Item's Name" },
                    new int[] { 150, 130, 200 }
                );
            Grd5.Rows.Count = 1;
            Grd5.Cells[0, 0].Value = "CD";
            Sm.SetGrdProperty(Grd5, false);

            #endregion

            #region Grid 6

            Grd6.Cols.Count = 1;
            Grd6.FrozenArea.ColCount = 1;
            Sm.GrdHdrWithColWidth(
                    Grd6,
                    new string[] { "Efficiency Data" },
                    new int[] { 150 }
                );
            Sm.SetGrdProperty(Grd6, false);

            #endregion
        }

        private void Process1(ref List<WCSpinning> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select WCCode, WCName, UnitInd, PercentInd, ProdDataPerItemInd, ");
            SQL.AppendLine("TotalMachine, SeqNo, ProdFormula, FSNICode1, FSNICode2 ");
            SQL.AppendLine("From TblWCSpinning Where ActInd='Y' Order By SeqNo;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    "WCCode", 
                    "WCName", "UnitInd", "PercentInd", "ProdDataPerItemInd", "TotalMachine", 
                    "SeqNo", "ProdFormula", "FSNICode1", "FSNICode2"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new WCSpinning()
                        {
                            WCCode = Sm.DrStr(dr, c[0]),
                            WCName = Sm.DrStr(dr, c[1]),
                            UnitInd = Sm.DrStr(dr, c[2]) == "Y",
                            PercentInd = Sm.DrStr(dr, c[3]) == "Y",
                            ProdDataPerItemInd = Sm.DrStr(dr, c[4]) == "Y",
                            TotalMachine = Sm.DrDec(dr, c[5]),
                            SeqNo = Sm.DrStr(dr, c[6]),
                            ProdFormula = Sm.DrStr(dr, c[7]),
                            FSNICode1 = Sm.DrStr(dr, c[8]),
                            FSNICode2 = Sm.DrStr(dr, c[9])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process2(string Dt, ref List<DataMesin> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select WCCode, TotalMachine MesinTotalUnit, Sum(RunningMachine) MesinJalanUnit ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select A.WCCode, A.SeqNo, A.TotalMachine, B.RunningMachine ");
            SQL.AppendLine("    From TblWCSpinning A ");
            SQL.AppendLine("    Inner Join TblSpinningProduction1Hdr B ");
            SQL.AppendLine("        On A.WCCode=B.WCCode ");
            SQL.AppendLine("        And B.CancelInd='N' ");
            SQL.AppendLine("        And B.ProdDt=@ProdDt ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select A.WCCode, A.SeqNo, A.TotalMachine, B.RunningMachine ");
            SQL.AppendLine("    From TblWCSpinning A ");
            SQL.AppendLine("    Inner Join TblSpinningProduction2Hdr B ");
            SQL.AppendLine("        On A.WCCode=B.WCCode ");
            SQL.AppendLine("        And B.CancelInd='N' ");
            SQL.AppendLine("        And B.ProdDt=@ProdDt ");
            SQL.AppendLine(") T ");
            SQL.AppendLine("Group By WCCode, TotalMachine ");
            SQL.AppendLine("Having Sum(RunningMachine)>0.00 ");
            SQL.AppendLine("Order By SeqNo;");

            Sm.CmParamDt(ref cm, "@ProdDt", Dt);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { "WCCode", "MesinTotalUnit", "MesinJalanUnit" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new DataMesin()
                        {
                            WCCode = Sm.DrStr(dr, c[0]),
                            MesinTotalUnit = Sm.DrDec(dr, c[1]),
                            MesinJalanUnit = Sm.DrDec(dr, c[2]),
                            MesinTotalPersen = 100m,
                            MesinJalanPersen = 0m
                        });
                    }
                }
                dr.Close();
            }

            if (l.Count > 0)
            {
                l.ForEach(x => {
                    if (x.MesinTotalUnit == 0m)
                        x.MesinJalanPersen = 0m;
                    else
                        x.MesinJalanPersen = Math.Round((x.MesinJalanUnit / x.MesinTotalUnit)*100m, 0);
                });
            }
        }

        private void Process3(string Dt, ref List<DataProduksi1> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select WCCode, Sum(RunningMachine) Unit, Sum(Qty) Kg ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select A.WCCode, A.SeqNo, B.RunningMachine, B.Qty ");
            SQL.AppendLine("    From TblWCSpinning A ");
            SQL.AppendLine("    Inner Join TblSpinningProduction1Hdr B ");
            SQL.AppendLine("        On A.WCCode=B.WCCode ");
            SQL.AppendLine("        And B.CancelInd='N' ");
            SQL.AppendLine("        And B.ProdDt=@ProdDt ");
            SQL.AppendLine(") T ");
            SQL.AppendLine("Group By WCCode ");
            SQL.AppendLine("Having Sum(RunningMachine)>0.00 Or Sum(Qty)>0.00 ");
            SQL.AppendLine("Order By SeqNo;");

            Sm.CmParamDt(ref cm, "@ProdDt", Dt);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "WCCode", "Unit", "Kg" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new DataProduksi1()
                        {
                            WCCode = Sm.DrStr(dr, c[0]),
                            Unit = Sm.DrDec(dr, c[1]),
                            Kg = Sm.DrDec(dr, c[2]),
                            Persen = 0m
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process4(ref List<ProdEfficiencyVar> l)
        {
            var cm = new MySqlCommand();
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = "Select OptCode, OptDesc From TblOption Where OptCat='ProdEfficiencyVar' Order By OptCode;";
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "OptCode", "OptDesc" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new ProdEfficiencyVar()
                        {
                            PEVCode = Sm.DrStr(dr, c[0]),
                            PEVDesc = Sm.DrStr(dr, c[1])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process5(string Dt, ref List<DataEfisiensi> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select WCCode, PEVCode, PEVDesc, Sum(Value) Value ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select A.WCCode, A.SeqNo, B.OptCode As PEVCode, B.OptDesc As PEVDesc, D.Value ");
            SQL.AppendLine("    From TblWCSpinning A ");
            SQL.AppendLine("    Inner Join TblOption B On B.OptCat='ProdEfficiencyVar' ");
            SQL.AppendLine("    Left Join TblSpinningProduction1Hdr C ");
            SQL.AppendLine("        On A.WCCode=C.WCCode ");
            SQL.AppendLine("        And C.CancelInd='N' ");
            SQL.AppendLine("        And C.ProdDt=@ProdDt ");
            SQL.AppendLine("    Left Join TblSpinningProduction1Dtl D ");
            SQL.AppendLine("        On C.DocNo=D.DocNo ");
            SQL.AppendLine("        And B.OptCode=D.ProdEfficiencyVar ");
            SQL.AppendLine("    Where A.ProdDataPerItemInd='N' ");
            SQL.AppendLine(") T ");
            SQL.AppendLine("Group By WCCode, PEVCode, PEVDesc ");
            SQL.AppendLine("Order By SeqNo;");

            Sm.CmParamDt(ref cm, "@ProdDt", Dt);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "WCCode", "PEVCode", "PEVDesc", "Value" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new DataEfisiensi()
                        {
                            WCCode = Sm.DrStr(dr, c[0]),
                            PEVCode = Sm.DrStr(dr, c[1]),
                            PEVDesc = Sm.DrStr(dr, c[2]),
                            Value = Sm.DrDec(dr, c[3])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process6(ref List<FormulaSpinningNonItem> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.WCCode, A.PercentInd, A.TotalMachine, Case When B.Query Is Not Null Then B.TotalInd Else C.TotalInd End As TotalInd, ");
            SQL.AppendLine("B.Query As Query_1, B.Param1 As Param1_1, B.Param2 As Param2_1, B.Param3 As Param3_1, B.Param4 As Param4_1, B.Param5 As Param5_1, B.Param6 As Param6_1, ");
            SQL.AppendLine("C.Query As Query_2, B.Param1 As Param1_2, B.Param2 As Param2_2, B.Param3 As Param3_2, B.Param4 As Param4_2, B.Param5 As Param5_2, B.Param6 As Param6_2 ");
            SQL.AppendLine("From TblWCSpinning A ");
            SQL.AppendLine("Left Join TblFormulaSpinningNonItem B On A.FSNICode1=B.FSNICode And B.ActInd='Y' ");
            SQL.AppendLine("Left Join TblFormulaSpinningNonItem C On A.FSNICode2=C.FSNICode And C.ActInd='Y' ");
            SQL.AppendLine("Where ProdDataPerItemInd='N' ");
            SQL.AppendLine("Order By SeqNo;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "WCCode", 

                    //1-5
                    "PercentInd", "TotalMachine", "TotalInd", "Query_1", "Param1_1", 

                    //6-10
                    "Param2_1", "Param3_1", "Param4_1", "Param5_1", "Param6_1", 

                    //11-15
                    "Query_2", "Param1_2", "Param2_2", "Param3_2", "Param4_2", 

                    //16-17
                    "Param5_2", "Param6_2"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new FormulaSpinningNonItem()
                        {
                            WCCode = Sm.DrStr(dr, c[0]),
                            PercentInd = Sm.DrStr(dr, c[1])=="Y",
                            TotalMachine = Sm.DrDec(dr, c[2]),
                            TotalInd = Sm.DrStr(dr, c[3]) == "Y",
                            Query_1 = Sm.DrStr(dr, c[4]),
                            Param1_1 = Sm.DrStr(dr, c[5]),
                            Param2_1 = Sm.DrStr(dr, c[6]),
                            Param3_1 = Sm.DrStr(dr, c[7]),
                            Param4_1 = Sm.DrStr(dr, c[8]),
                            Param5_1 = Sm.DrStr(dr, c[9]),
                            Param6_1 = Sm.DrStr(dr, c[10]),
                            Query_2 = Sm.DrStr(dr, c[11]),
                            Param1_2 = Sm.DrStr(dr, c[12]),
                            Param2_2 = Sm.DrStr(dr, c[13]),
                            Param3_2 = Sm.DrStr(dr, c[14]),
                            Param4_2 = Sm.DrStr(dr, c[15]),
                            Param5_2 = Sm.DrStr(dr, c[16]),
                            Param6_2 = Sm.DrStr(dr, c[17])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process7(
            decimal WorkingDays,
            ref List<WCSpinning> l, 
            ref List<DataProduksi1> l2,
            ref List<FormulaSpinningNonItem> l3,
            ref List<ProdEfficiencyVar> l4,
            ref List<DataEfisiensi> l5
            )
        {
            var lFormulaParam = new List<FormulaParam>();

            foreach (var i in l.Where(w=>!w.ProdDataPerItemInd).OrderBy(o=>o.SeqNo))
            {
                foreach (var j in l2.Where(w => Sm.CompareStr(w.WCCode, i.WCCode)))
                {
                    foreach (var k in l3.Where(w => Sm.CompareStr(w.WCCode, i.WCCode) && w.PercentInd && !w.TotalInd))
                    {
                        lFormulaParam.Clear();
                        for (int x = 1; x <= 6; x++)
                        {
                            lFormulaParam.Add(new FormulaParam
                            {
                                ParamNo = "Param"+x.ToString(),
                                ParamDesc = k.Param1_1,
                                Value = 0m
                            });
                        }

                        foreach (var f in lFormulaParam)
                        {
                            if (f.ParamDesc.Length > 0)
                            {
                                if (Sm.CompareStr(f.ParamDesc, "Qty"))
                                    f.Value = j.Kg;
                                else
                                {
                                    if (Sm.CompareStr(f.ParamDesc, "Total Machine"))
                                        f.Value = i.TotalMachine;
                                    else
                                    {
                                        if (Sm.CompareStr(f.ParamDesc, "Working Days"))
                                            f.Value = WorkingDays;
                                        else
                                        {
                                            foreach (var o in l4.Where(w => Sm.CompareStr(f.ParamDesc, w.PEVDesc)))
                                            {
                                                foreach (var e in l5.Where(w =>
                                                        Sm.CompareStr(i.WCCode, w.WCCode) &&
                                                        Sm.CompareStr(o.PEVCode, w.PEVCode)))
                                                    f.Value = e.Value;
                                            }
                                        }
                                    }
                                }
                            }   
                        }
                        j.Persen = ComputePersen1(k.Query_1, ref lFormulaParam);
                    }
                }
            }
            lFormulaParam.Clear();
        }

        private void Process8(
            string Dt, 
            ref List<DataProduksi2> l, 
            ref List<DataEfisiensi> l2,
            ref List<ProdEfficiencyVar> l3,
            ref List<WCSpinning> l4)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.WCCode, A.ProdFormula, A.UnitInd, A.PercentInd, ");
            SQL.AppendLine("C.ItCode, D.ItName, C.RunningMachine, C.Qty, ");
            SQL.AppendLine("RSRPM, RSTPI, RSNE, RSAvgSPL, RSMFR, ");
            SQL.AppendLine("WSpeed, WNE, WDelMC, WDelRun, WMFR, WDel ");
            SQL.AppendLine("From TblWCSpinning A ");
            SQL.AppendLine("Inner Join TblSpinningProduction2Hdr B ");
            SQL.AppendLine("    On A.WCCode=B.WCCode ");
            SQL.AppendLine("    And B.CancelInd='N' ");
            SQL.AppendLine("    And B.ProdDt=@ProdDt ");
            SQL.AppendLine("Inner Join TblSpinningProduction2Dtl C On B.DocNo=C.DocNo ");
            SQL.AppendLine("Inner Join TblItemSpinning D On C.ItCode=D.ItCode ");
            SQL.AppendLine("Order By A.SeqNo;");

            Sm.CmParamDt(ref cm, "@ProdDt", Dt);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "WCCode", 

                    //1-5
                    "ProdFormula", "UnitInd", "PercentInd", "ItCode", "ItName", 
                    
                    //6-10
                    "RunningMachine", "Qty", "RSRPM", "RSTPI", "RSNE", 
                    
                    //11-15
                    "RSAvgSPL", "RSMFR", "WSpeed", "WNE", "WDelMC", 
                    
                    //16-18
                    "WDelRun", "WMFR", "WDel"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new DataProduksi2()
                        {
                            WCCode = Sm.DrStr(dr, c[0]),
                            ProdFormula = Sm.DrStr(dr, c[1]),
                            UnitInd = Sm.DrStr(dr, c[2])=="Y",
                            PercentInd = Sm.DrStr(dr, c[3]) == "Y",
                            ItCode = Sm.DrStr(dr, c[4]),
                            ItName = Sm.DrStr(dr, c[5]),
                            Unit = Sm.DrDec(dr, c[6]),
                            Kg = Sm.DrDec(dr, c[7]),
                            Persen = 0m,
                            RSRPM = Sm.DrDec(dr, c[8]),
	                        RSTPI = Sm.DrDec(dr, c[9]),
	                        RSNE = Sm.DrDec(dr, c[10]),
	                        RSAvgSPL = Sm.DrDec(dr, c[11]),
	                        RSMFR = Sm.DrDec(dr, c[12]),
	                        WSpeed = Sm.DrDec(dr, c[13]),
	                        WNE = Sm.DrDec(dr, c[14]),
	                        WDelMC = Sm.DrDec(dr, c[15]),
	                        WDelRun = Sm.DrDec(dr, c[16]),
	                        WMFR = Sm.DrDec(dr, c[17]),
                            WDel = Sm.DrDec(dr, c[18])
                        });
                    }
                }
                dr.Close();
            }
            if (l.Count > 0)
            {
                var WCCodeTemp = string.Empty;
                
                foreach (var x in l.OrderBy(o=>o.WCCode))
                {
                    if (x.ProdFormula == "02" && x.PercentInd)
                    {
                        if ((x.Unit * x.RSMFR)!=0m)
                        x.Persen = 100m * (x.Kg / (x.Unit * x.RSMFR));
                    }

                    if (x.ProdFormula == "03" && x.PercentInd)
                    {
                        if ((x.WDel) != 0m) x.Persen = 100m * (x.Kg /x.WDel);
                    }

                    if (!Sm.CompareStr(x.WCCode, WCCodeTemp))
                    {
                        if (x.ProdFormula=="02")
                        {
                            if (x.UnitInd && x.PercentInd)
                            {
                                var s = new List<string>(new string[] { "02", "03", "06" });

                                for (int i = 0; i <= 2; i++)
                                {
                                    l2.Add(new DataEfisiensi
                                    {
                                        WCCode = x.WCCode,
                                        PEVCode = s[i],
                                        PEVDesc = l3.FirstOrDefault(v => v.PEVCode == s[i]).PEVDesc,
                                        Value = 0m
                                    });
                                }
                            }
                        }

                        if (x.ProdFormula == "03")
                        {
                            l2.Add(new DataEfisiensi
                            {
                                WCCode = x.WCCode,
                                PEVCode = "06",
                                PEVDesc = l3.FirstOrDefault(v => v.PEVCode == "06").PEVDesc,
                                Value = 0m
                            });
                        }
                    }
                    WCCodeTemp = x.WCCode;
                }

                if (l2.Count>0)
                {
                    decimal ValueTemp = 0m, UnitTemp = 0m, KgTemp = 0m;
                    foreach(var i in l2)
                    {
                        foreach (var j in l4.Where(w => Sm.CompareStr(w.WCCode, i.WCCode)))
                        {
                            if (j.ProdFormula == "02" && j.PercentInd)
                            {
                                ValueTemp = 0m;
                                UnitTemp = 0m;
                                if (i.PEVCode == "02") //RPM
                                {
                                    foreach (var x in l.Where(w => Sm.CompareStr(w.WCCode, j.WCCode)))
                                    {
                                        ValueTemp += (x.RSRPM*x.Unit);
                                        UnitTemp += x.Unit;
                                    }
                                }

                                if (i.PEVCode == "03") //TPI (CD)
                                {
                                    foreach (var x in l.Where(w => Sm.CompareStr(w.WCCode, j.WCCode)))
                                    {
                                        ValueTemp += (x.RSTPI * x.Unit);
                                        UnitTemp += x.Unit;
                                    }
                                }

                                if (i.PEVCode == "06") //Average Count Riil
                                {
                                    foreach (var x in l.Where(w => Sm.CompareStr(w.WCCode, j.WCCode)))
                                    {
                                        ValueTemp += (x.RSNE * x.Unit);
                                        UnitTemp += x.Unit;
                                    }
                                }

                                if (UnitTemp != 0m) i.Value = ValueTemp / UnitTemp;
                            }

                            if (j.ProdFormula == "03")
                            {
                                ValueTemp = 0m;
                                KgTemp = 0m;
                                
                                foreach (var x in l.Where(w => Sm.CompareStr(w.WCCode, j.WCCode)))
                                {
                                    ValueTemp += (x.WNE * x.Kg);
                                    KgTemp += x.Kg;
                                }
                                
                                if (KgTemp != 0m) i.Value = ValueTemp / KgTemp;
                            }
                        }
                        
                    }
                }
            }

           
        }

        private void Process9(
            ref List<DataProduksi2Total> l,
            ref List<WCSpinning> l2,
            ref List<DataProduksi2> l3,
            ref List<DataEfisiensi> l4)
        {
            decimal UnitTemp = 0m, KgTemp = 0m, PersenTemp = 0m;
            foreach(var i in l2.Where(w=>w.ProdDataPerItemInd))
            {
                UnitTemp = 0m;
                KgTemp = 0m;
                PersenTemp = 0m;

                foreach (var j in l3.Where(w => Sm.CompareStr(w.WCCode, i.WCCode)))
                {
                    UnitTemp += j.Unit;
                    KgTemp += j.Kg;
                }

                if (i.ProdFormula == "02")
                { 
                    //Ring Spinning

                    decimal RPM = 0m, TPI = 0m, AverageCountRiil = 0m, RSAvgSPL = 0m;

                    foreach (var k in l4.Where(w => Sm.CompareStr(w.WCCode, i.WCCode)))
                    {
                        if (k.PEVCode == "02") RPM = k.Value;
                        if (k.PEVCode == "03") TPI = k.Value;
                        if (k.PEVCode == "06") AverageCountRiil = k.Value;   
                    }

                    foreach (var k in l3.Where(w => Sm.CompareStr(w.WCCode, i.WCCode) && w.RSAvgSPL!=0m))
                    {
                        RSAvgSPL = k.RSAvgSPL;
                        break;
                    }

                    if ((840m * 36m * TPI * AverageCountRiil * 400m) != 0)
                    {
                        if ((((RPM * 60m * 24m * RSAvgSPL * UnitTemp) / (840m * 36m * TPI * AverageCountRiil * 400m)) * 181.44m)!=0)
                            PersenTemp = (KgTemp / (((RPM * 60m * 24m * RSAvgSPL * UnitTemp) / (840m * 36m * TPI * AverageCountRiil * 400m)) * 181.44m)) * 100m;
                    }
                    
                    l.Add(new DataProduksi2Total 
                    {
                        WCCode = i.WCCode,
                        ProdFormula = "02",
                        RingSpinningInd = true,
                        WinderInd = false,
                        PackingInd = false,
                        Unit = UnitTemp,
                        Kg = KgTemp,
                        Persen = PersenTemp
                    });
                }

                if (i.ProdFormula == "03")
                {
                    //Winder

                    foreach (var k in l3.Where(w => Sm.CompareStr(w.WCCode, i.WCCode)))
                    {
                        PersenTemp += (k.Kg*k.Persen);
                    }

                    if (KgTemp != 0)
                        PersenTemp = PersenTemp / KgTemp;
                    else
                        PersenTemp = 0m;

                    l.Add(new DataProduksi2Total
                    {
                        WCCode = i.WCCode,
                        ProdFormula = "03",
                        RingSpinningInd = false,
                        WinderInd = true,
                        PackingInd = false,
                        Unit = UnitTemp,
                        Kg = KgTemp,
                        Persen = PersenTemp
                    });
                }

                if (i.ProdFormula == "01")
                {
                    //Packing

                    string WCCodeRingSpinning = string.Empty;

                    foreach (var k in l2.Where(w => w.ProdDataPerItemInd && w.ProdFormula=="02"))
                    {
                        WCCodeRingSpinning = k.WCCode;
                        break;
                    }

                    if (WCCodeRingSpinning.Length > 0)
                    {
                        foreach (var k in l2.Where(w => w.ProdDataPerItemInd && w.ProdFormula == "02"))
                        {
                            WCCodeRingSpinning = k.WCCode;
                            break;
                        }

                        foreach (var k in l3.Where(w => Sm.CompareStr(w.WCCode, WCCodeRingSpinning)))
                        {
                            PersenTemp+=(k.Unit*k.RSMFR);
                            break;
                        }
                    }

                    if (PersenTemp!=0m)
                        PersenTemp = (KgTemp / PersenTemp) * 100m;

                    l.Add(new DataProduksi2Total
                    {
                        WCCode = i.WCCode,
                        ProdFormula = "01",
                        RingSpinningInd = false,
                        WinderInd = false,
                        PackingInd = true,
                        Unit = 0m,
                        Kg = KgTemp,
                        Persen = PersenTemp
                    });
                }
            }
        }

        private void Process10(ref List<WCSpinning> l)
        {
            int i = 0;
            int c = 0;
            
            Grd1.Cols.Count = 1 + (2 * l.Where(w => !w.ProdDataPerItemInd).Count());
            foreach (var x in l.Where(w => !w.ProdDataPerItemInd).OrderBy(o=>o.SeqNo))
            {
                c = 1 + (i * 2); 
                Grd1.Cols[c].Text = string.Concat(x.WCName.Replace(" ", Environment.NewLine), Environment.NewLine, "(Unit)");
                Grd1.Cols[c].Width = 100;
                Grd1.Header.Cells[0, c].TextAlign = iGContentAlignment.TopCenter;
                Grd1.Cols[c].CellStyle.ValueType = typeof(Decimal);
                Grd1.Cols[c].CellStyle.TextAlign = iGContentAlignment.TopRight;
                Grd1.Cols[c].CellStyle.FormatString = "{0:#,##0.00}";

                c = 2 + (i * 2);
                Grd1.Cols[c].Text = string.Concat(x.WCName.Replace(" ", Environment.NewLine), Environment.NewLine, "(%)");
                Grd1.Cols[c].Width = 100;
                Grd1.Header.Cells[0, c].TextAlign = iGContentAlignment.TopCenter;
                Grd1.Cols[c].CellStyle.ValueType = typeof(Decimal);
                Grd1.Cols[c].CellStyle.TextAlign = iGContentAlignment.TopRight;
                Grd1.Cols[c].CellStyle.FormatString = "{0:#,##0.00}";

                i++;
            }

            i = 0;
            Grd2.Cols.Count = 1 + (3 * l.Where(w => !w.ProdDataPerItemInd).Count());
            foreach (var x in l.Where(w => !w.ProdDataPerItemInd).OrderBy(o => o.SeqNo))
            {
                c = 1 + (i * 3);
                Grd2.Cols[c].Text = string.Concat(x.WCName.Replace(" ", Environment.NewLine), Environment.NewLine, "(Unit)");
                Grd2.Cols[c].Width = 100;
                Grd2.Header.Cells[0, c].TextAlign = iGContentAlignment.TopCenter;
                Grd2.Cols[c].CellStyle.ValueType = typeof(Decimal);
                Grd2.Cols[c].CellStyle.TextAlign = iGContentAlignment.TopRight;
                Grd2.Cols[c].CellStyle.FormatString = "{0:#,##0}";

                c = 2 + (i * 3);
                Grd2.Cols[c].Text = string.Concat(x.WCName.Replace(" ", Environment.NewLine), Environment.NewLine, "(Kg)");
                Grd2.Cols[c].Width = 100;
                Grd2.Header.Cells[0, c].TextAlign = iGContentAlignment.TopCenter;
                Grd2.Cols[c].CellStyle.ValueType = typeof(Decimal);
                Grd2.Cols[c].CellStyle.TextAlign = iGContentAlignment.TopRight;
                Grd2.Cols[c].CellStyle.FormatString = "{0:#,##0.00}";

                c = 3 + (i * 3);
                Grd2.Cols[c].Text = string.Concat(x.WCName.Replace(" ", Environment.NewLine), Environment.NewLine, "(%)");
                Grd2.Cols[c].Width = 100;
                Grd2.Header.Cells[0, c].TextAlign = iGContentAlignment.TopCenter;
                Grd2.Cols[c].CellStyle.ValueType = typeof(Decimal);
                Grd2.Cols[c].CellStyle.TextAlign = iGContentAlignment.TopRight;
                Grd2.Cols[c].CellStyle.FormatString = "{0:#,##0.00}";

                i++;
            }

            i = 0;
            Grd3.Cols.Count = 1 + (l.Where(w => !w.ProdDataPerItemInd).Count());
            foreach (var x in l.Where(w => !w.ProdDataPerItemInd).OrderBy(o => o.SeqNo))
            {
                c = 1 + i;
                Grd3.Cols[c].Text = x.WCName.Replace(" ", Environment.NewLine);
                Grd3.Cols[c].Width = 100;
                Grd3.Header.Cells[0, c].TextAlign = iGContentAlignment.TopCenter;
                Grd3.Cols[c].CellStyle.ValueType = typeof(Decimal);
                Grd3.Cols[c].CellStyle.TextAlign = iGContentAlignment.TopRight;
                Grd3.Cols[c].CellStyle.FormatString = "{0:#,##0.000}";

                i++;
            }

            i = 0;
            Grd4.Cols.Count = 1 + (2 * l.Where(w => w.ProdDataPerItemInd).Count());
            foreach (var x in l.Where(w => w.ProdDataPerItemInd).OrderBy(o=>o.SeqNo))
            {
                c = 1 + (i * 2); 
                Grd4.Cols[c].Text = string.Concat(x.WCName.Replace(" ", Environment.NewLine), Environment.NewLine, "(Unit)");
                Grd4.Cols[c].Width = 100;
                Grd4.Header.Cells[0, c].TextAlign = iGContentAlignment.TopCenter;
                Grd4.Cols[c].CellStyle.ValueType = typeof(Decimal);
                Grd4.Cols[c].CellStyle.TextAlign = iGContentAlignment.TopRight;
                Grd4.Cols[c].CellStyle.FormatString = "{0:#,##0.00}";

                c = 2 + (i * 2);
                Grd4.Cols[c].Text = string.Concat(x.WCName.Replace(" ", Environment.NewLine), Environment.NewLine, "(%)");
                Grd4.Cols[c].Width = 100;
                Grd4.Header.Cells[0, c].TextAlign = iGContentAlignment.TopCenter;
                Grd4.Cols[c].CellStyle.ValueType = typeof(Decimal);
                Grd4.Cols[c].CellStyle.TextAlign = iGContentAlignment.TopRight;
                Grd4.Cols[c].CellStyle.FormatString = "{0:#,##0.00}";

                i++;
            }

            i = 0;
            Grd5.Cols.Count = 3 + (3 * l.Where(w => w.ProdDataPerItemInd).Count());
            foreach (var x in l.Where(w => w.ProdDataPerItemInd).OrderBy(o => o.SeqNo))
            {
                c = 3 + (i * 3);
                Grd5.Cols[c].Text = string.Concat(x.WCName.Replace(" ", Environment.NewLine), Environment.NewLine, "(Unit)");
                Grd5.Cols[c].Width = 100;
                Grd5.Header.Cells[0, c].TextAlign = iGContentAlignment.TopCenter;
                Grd5.Cols[c].CellStyle.ValueType = typeof(Decimal);
                Grd5.Cols[c].CellStyle.TextAlign = iGContentAlignment.TopRight;
                Grd5.Cols[c].CellStyle.FormatString = "{0:#,##0}";

                c = 4 + (i * 3);
                Grd5.Cols[c].Text = string.Concat(x.WCName.Replace(" ", Environment.NewLine), Environment.NewLine, "(Kg)");
                Grd5.Cols[c].Width = 100;
                Grd5.Header.Cells[0, c].TextAlign = iGContentAlignment.TopCenter;
                Grd5.Cols[c].CellStyle.ValueType = typeof(Decimal);
                Grd5.Cols[c].CellStyle.TextAlign = iGContentAlignment.TopRight;
                Grd5.Cols[c].CellStyle.FormatString = "{0:#,##0.00}";

                c = 5 + (i * 3);
                Grd5.Cols[c].Text = string.Concat(x.WCName.Replace(" ", Environment.NewLine), Environment.NewLine, "(%)");
                Grd5.Cols[c].Width = 100;
                Grd5.Header.Cells[0, c].TextAlign = iGContentAlignment.TopCenter;
                Grd5.Cols[c].CellStyle.ValueType = typeof(Decimal);
                Grd5.Cols[c].CellStyle.TextAlign = iGContentAlignment.TopRight;
                Grd5.Cols[c].CellStyle.FormatString = "{0:#,##0.00}";

                i++;
            }

            i = 0;
            Grd6.Cols.Count = 1 + (l.Where(w => w.ProdDataPerItemInd).Count());
            foreach (var x in l.Where(w => w.ProdDataPerItemInd).OrderBy(o => o.SeqNo))
            {
                c = 1 + i;
                Grd6.Cols[c].Text = x.WCName.Replace(" ", Environment.NewLine);
                Grd6.Cols[c].Width = 100;
                Grd6.Header.Cells[0, c].TextAlign = iGContentAlignment.TopCenter;
                Grd6.Cols[c].CellStyle.ValueType = typeof(Decimal);
                Grd6.Cols[c].CellStyle.TextAlign = iGContentAlignment.TopRight;
                Grd6.Cols[c].CellStyle.FormatString = "{0:#,##0.000}";

                i++;
            }

        }

        private void Process11(ref List<WCSpinning> l, ref List<WCGrd> l2, ref List<WCGrd> l3)
        {
            int i = 1;
            foreach (var x in l.Where(w => !w.ProdDataPerItemInd).OrderBy(o => o.SeqNo))
            {
                l2.Add(new WCGrd { WCCode = x.WCCode, No = i });
                i++;
            }

            i = 1;
            foreach (var x in l.Where(w => w.ProdDataPerItemInd).OrderBy(o => o.SeqNo))
            {
                l3.Add(new WCGrd { WCCode = x.WCCode, No = i });
                i++;
            }
        }

        private void Process12(ref List<WCSpinning> l, ref List<DataMesin> l2, ref List<WCGrd> l3, ref List<WCGrd> l4)
        {
            int c = 0;
            foreach (var i in l.Where(w => !w.ProdDataPerItemInd).OrderBy(o => o.SeqNo))
            {
                c = l3.Where(w => Sm.CompareStr(w.WCCode, i.WCCode)).FirstOrDefault().No;
                c -= 1;
                c = 1 + (c * 2);
                foreach (var j in l2.Where(w => Sm.CompareStr(w.WCCode, i.WCCode)))
                {
                    Grd1.Cells[0, c].Value = j.MesinTotalUnit;
                    Grd1.Cells[0, c+1].Value = j.MesinTotalPersen;
                    Grd1.Cells[1, c].Value = j.MesinJalanUnit;
                    Grd1.Cells[1, c + 1].Value = j.MesinJalanPersen;
                }
            }

            foreach (var i in l.Where(w => w.ProdDataPerItemInd).OrderBy(o => o.SeqNo))
            {
                c = l4.Where(w => Sm.CompareStr(w.WCCode, i.WCCode)).FirstOrDefault().No;
                c -= 1;
                c = 1 + (c * 2);
                foreach (var j in l2.Where(w => Sm.CompareStr(w.WCCode, i.WCCode)))
                {
                    Grd4.Cells[0, c].Value = j.MesinTotalUnit;
                    Grd4.Cells[0, c + 1].Value = j.MesinTotalPersen;
                    Grd4.Cells[1, c].Value = j.MesinJalanUnit;
                    Grd4.Cells[1, c + 1].Value = j.MesinJalanPersen;
                }
            }
        }

        private void Process13(
            ref List<ProdEfficiencyVar> l, 
            ref List<PEVGrd> l2, 
            ref List<WCSpinning> l3, 
            ref List<WCGrd> l4,
            ref List<WCGrd> l5,
            ref List<DataEfisiensi> l6, 
            ref List<DataEfisiensi> l7 )
        {
            int i = 1;
            Grd3.Rows.Count = l.Count();
            Grd6.Rows.Count = l.Count();
            foreach (var x in l.OrderBy(o => o.PEVCode))
            {
                l2.Add(new PEVGrd { PEVCode = x.PEVCode, No = i });
                Grd3.Cells[i - 1, 0].Value = x.PEVDesc;
                Grd6.Cells[i - 1, 0].Value = x.PEVDesc;
                i++;
            }

            int c = 0, r = 0;
            foreach (var x in l3.Where(w => !w.ProdDataPerItemInd).OrderBy(o => o.SeqNo))
            {
                c = l4.Where(w => Sm.CompareStr(w.WCCode, x.WCCode)).FirstOrDefault().No;
                c -= 1;
                c = 1 + c;
                foreach (var y in l6.Where(w => Sm.CompareStr(w.WCCode, x.WCCode)))
                {
                    r = l2.Where(w => Sm.CompareStr(w.PEVCode, y.PEVCode)).FirstOrDefault().No-1;
                    Grd3.Cells[r, c].Value = y.Value;
                }
            }

            foreach (var x in l3.Where(w => w.ProdDataPerItemInd).OrderBy(o => o.SeqNo))
            {
                c = l5.Where(w => Sm.CompareStr(w.WCCode, x.WCCode)).FirstOrDefault().No;
                c -= 1;
                c = 1 + c;
                foreach (var y in l7.Where(w => Sm.CompareStr(w.WCCode, x.WCCode)))
                {
                    r = l2.Where(w => Sm.CompareStr(w.PEVCode, y.PEVCode)).FirstOrDefault().No - 1;
                    Grd6.Cells[r, c].Value = y.Value;
                }
            }
        }

        private void Process14(
            ref List<WCSpinning> l,
            ref List<WCGrd> l2,
            ref List<DataProduksi1> l3)
        {
            int c = 0;
            foreach (var x in l.Where(w => !w.ProdDataPerItemInd).OrderBy(o => o.SeqNo))
            {
                c = l2.Where(w => Sm.CompareStr(w.WCCode, x.WCCode)).FirstOrDefault().No;
                c -= 1;
                c = 1 + (c*3);
                foreach (var y in l3.Where(w => Sm.CompareStr(w.WCCode, x.WCCode)))
                {
                    Grd2.Cells[0, c].Value = y.Unit;
                    Grd2.Cells[0, c+1].Value = y.Kg;
                    Grd2.Cells[0, c+2].Value = y.Persen;
                }
            }
        }

        private void Process15(
            ref List<WCSpinning> l,
            ref List<WCGrd> l2,
            ref List<DataProduksi2> l3)
        {
            int r = 0;
            foreach (var i in l3.Select(s => new { s.ItCode, s.ItName }).Distinct())
            { 
                Grd5.Rows.Add();
                Grd5.Cells[r, 1].Value = i.ItCode;
                Grd5.Cells[r, 2].Value = i.ItName;
                r++;
            }

            int c = 0;
            r = 0;
            foreach (var x in l.Where(w => w.ProdDataPerItemInd).OrderBy(o => o.SeqNo))
            {
                c = l2.Where(w => Sm.CompareStr(w.WCCode, x.WCCode)).FirstOrDefault().No;
                c -= 1;
                c = 3 + (c * 3);
                foreach (var y in l3.Where(w => Sm.CompareStr(w.WCCode, x.WCCode)))
                {
                    r=0;
                    for (int z = 0; z < Grd5.Rows.Count; z++)
                    { 
                        if (Sm.CompareStr(y.ItCode, Sm.GetGrdStr(Grd5, z, 1)))
                        {
                            r=z;
                            break;
                        }
                    }
                    Grd5.Cells[r, c].Value = y.Unit;
                    Grd5.Cells[r, c + 1].Value = y.Kg;
                    Grd5.Cells[r, c + 2].Value = y.Persen;
                }
            }
        }

        private decimal ComputePersen1(string SQL, ref List<FormulaParam> l)
        {
            var cm = new MySqlCommand() { CommandText = string.Concat("Select " , SQL, ";") };
            foreach (var x in l)
                if (x.ParamDesc.Length > 0) Sm.CmParam<Decimal>(ref cm, x.ParamNo, x.Value);
            
            return Sm.GetValueDec(cm);
        }

        #endregion

        #region Event

        private void FrmRptSpinningProduction_Load(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                DteDt.DateTime = Sm.ConvertDate(Sm.ServerCurrentDateTime());
                SetGrd();
                Sm.SetLookUpEdit(LueFontSize, new string[] { "6", "7", "8", "9", "10", "11", "12" });
                Sm.SetLue(LueFontSize, "9");
                BtnPrintOut.Enabled = true;
                BtnPrintOut.Visible = true;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnRefresh_Click(object sender, EventArgs e)
        {
            if (Sm.IsDteEmpty(DteDt, "Date")) return;

            SetGrd();

            var Dt = Sm.GetDte(DteDt).Substring(0, 8);
            var Dt1 = string.Concat(Sm.Left(Dt, 6), "01");
            var Dt2 = Dt;
            var WorkingDays = Decimal.Parse(Sm.Right(Dt, 2));

            var lWCSpinning = new List<WCSpinning>();
            var lDataMesin = new List<DataMesin>();
            var lDataProduksi1 = new List<DataProduksi1>();
            var lProdEfficiencyVar = new List<ProdEfficiencyVar>();
            var lDataEfisiensi1 = new List<DataEfisiensi>();
            var lFormulaSpinningNonItem = new List<FormulaSpinningNonItem>();

            var lDataProduksi2 = new List<DataProduksi2>();
            var lDataEfisiensi2 = new List<DataEfisiensi>();
            var lDataProduksi2Total = new List<DataProduksi2Total>();

            var lWCGrd1 = new List<WCGrd>();
            var lWCGrd2 = new List<WCGrd>();
            var lPEVGrd = new List<PEVGrd>();
        
            try
            {
                Process1(ref lWCSpinning);
                Process4(ref lProdEfficiencyVar);
                Process6(ref lFormulaSpinningNonItem);

                Process2(Dt, ref lDataMesin);
                Process3(Dt, ref lDataProduksi1);
                Process5(Dt, ref lDataEfisiensi1);
                Process7(WorkingDays, ref lWCSpinning, ref lDataProduksi1, ref lFormulaSpinningNonItem, ref lProdEfficiencyVar, ref lDataEfisiensi1);

                Process8(Dt, ref lDataProduksi2, ref lDataEfisiensi2, ref lProdEfficiencyVar, ref lWCSpinning);
                Process9(ref lDataProduksi2Total, ref lWCSpinning, ref lDataProduksi2, ref lDataEfisiensi2);

                Process10(ref lWCSpinning);
                Process11(ref lWCSpinning, ref lWCGrd1, ref lWCGrd2);
                Process12(ref lWCSpinning, ref lDataMesin, ref lWCGrd1, ref lWCGrd2);
                Process13(ref lProdEfficiencyVar, ref lPEVGrd, ref lWCSpinning, ref lWCGrd1, ref lWCGrd2, ref lDataEfisiensi1, ref lDataEfisiensi2);
                Process14(ref lWCSpinning, ref lWCGrd1, ref lDataProduksi1);
                Process15(ref lWCSpinning, ref lWCGrd2, ref lDataProduksi2);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                lWCSpinning.Clear();
                lDataMesin.Clear();
                lDataProduksi1.Clear();
                lProdEfficiencyVar.Clear();
                lDataEfisiensi1.Clear();
                lFormulaSpinningNonItem.Clear();
                lDataProduksi2.Clear();
                lDataEfisiensi2.Clear();
                lDataProduksi2Total.Clear();
                lWCGrd1.Clear();
                lWCGrd2.Clear();
                lPEVGrd.Clear();
            }
        }

        #endregion

        #region Class

        #region New

        private class Data1 // Judul Work Center (Carding, Drawing Breaker, dll)
        {
            //digunakan di harian dan akumulasi
            public string Title1 { get; set; } // Carding
            public string Title2 { get; set; } // Drawing Breaker
            public string Title3 { get; set; } // Omega Lap
            public string Title4 { get; set; }
            public string Title5 { get; set; }
            public string Title6 { get; set; }
            public string Title7 { get; set; }
            public string Title8 { get; set; }
            public string Title9 { get; set; }
            public string Title10 { get; set; }
        }

        private class Data2 // Data mesin
        {
            //digunakan di harian dan akumulasi
            public string id { get; set; } //1=Jumlah Mesin Total 2=Jumlah Mesin Jalan
            public string Remark { get; set; } //untuk id 1 isinya "Jumlah Mesin Total" untuk id 2 isinya "Jumlah Mesin Jalan"

            public decimal Unit1 { get; set; }
            public decimal Persen1 { get; set; }

            public decimal Unit2 { get; set; }
            public decimal Persen2 { get; set; }

            public decimal Unit3 { get; set; }
            public decimal Persen3 { get; set; }

            public decimal Unit4 { get; set; }
            public decimal Persen4 { get; set; }

            public decimal Unit5 { get; set; }
            public decimal Persen5 { get; set; }

            public decimal Unit6 { get; set; }
            public decimal Persen6 { get; set; }

            public decimal Unit7 { get; set; }
            public decimal Persen7 { get; set; }

            public decimal Unit8 { get; set; }
            public decimal Persen8 { get; set; }

            public decimal Unit9 { get; set; }
            public decimal Persen9 { get; set; }

            public decimal Unit10 { get; set; }
            public decimal Persen10 { get; set; }
        }

        private class Data3 // Data Produksi
        {
            //digunakan di harian dan akumulasi
            public string ItemName { get; set; }

            public decimal Unit1 { get; set; }
            public decimal Kg1 { get; set; }
            public decimal Persen1 { get; set; }

            public decimal Unit2 { get; set; }
            public decimal Kg2 { get; set; }
            public decimal Persen2 { get; set; }

            public decimal Unit3 { get; set; }
            public decimal Kg3 { get; set; }
            public decimal Persen3 { get; set; }

            public decimal Unit4 { get; set; }
            public decimal Kg4 { get; set; }
            public decimal Persen4 { get; set; }

            public decimal Unit5 { get; set; }
            public decimal Kg5 { get; set; }
            public decimal Persen5 { get; set; }

            public decimal Unit6 { get; set; }
            public decimal Kg6 { get; set; }
            public decimal Persen6 { get; set; }

            public decimal Unit7 { get; set; }
            public decimal Kg7 { get; set; }
            public decimal Persen7 { get; set; }

            public decimal Unit8 { get; set; }
            public decimal Kg8 { get; set; }
            public decimal Persen8 { get; set; }

            public decimal Unit9 { get; set; }
            public decimal Kg9 { get; set; }
            public decimal Persen9 { get; set; }

            public decimal Unit10 { get; set; }
            public decimal Kg10 { get; set; }
            public decimal Persen10 { get; set; }
        }

        private class Data4 // Data Produksi (Total)
        {
            //digunakan di harian dan akumulasi
            public decimal Unit1 { get; set; }
            public decimal Kg1 { get; set; }
            public decimal Persen1 { get; set; }

            public decimal Unit2 { get; set; }
            public decimal Kg2 { get; set; }
            public decimal Persen2 { get; set; }

            public decimal Unit3 { get; set; }
            public decimal Kg3 { get; set; }
            public decimal Persen3 { get; set; }

            public decimal Unit4 { get; set; }
            public decimal Kg4 { get; set; }
            public decimal Persen4 { get; set; }

            public decimal Unit5 { get; set; }
            public decimal Kg5 { get; set; }
            public decimal Persen5 { get; set; }

            public decimal Unit6 { get; set; }
            public decimal Kg6 { get; set; }
            public decimal Persen6 { get; set; }

            public decimal Unit7 { get; set; }
            public decimal Kg7 { get; set; }
            public decimal Persen7 { get; set; }

            public decimal Unit8 { get; set; }
            public decimal Kg8 { get; set; }
            public decimal Persen8 { get; set; }

            public decimal Unit9 { get; set; }
            public decimal Kg9 { get; set; }
            public decimal Persen9 { get; set; }

            public decimal Unit10 { get; set; }
            public decimal Kg10 { get; set; }
            public decimal Persen10 { get; set; }
        }

        private class Data5 // Data Efisiensi
        {
            public string Id { get; set; } //untuk mengurutkan
            public string Remark { get; set; } //Speed (CD), RPM, TPI (CD), dll
            public decimal Value1 { get; set; }
            public decimal Value2 { get; set; }
            public decimal Value3 { get; set; }
            public decimal Value4 { get; set; }
            public decimal Value5 { get; set; }
            public decimal Value6 { get; set; }
            public decimal Value7 { get; set; }
            public decimal Value8 { get; set; }
            public decimal Value9 { get; set; }
            public decimal Value10 { get; set; }
        }

        private class Data6 // Keterangan dokumen paling bawah (Produksi 100% RSF, AKM PRODUKSI  RSF, dll)
        {
            public string Id { get; set; } //untuk mengurutkan
            public string Remark { get; set; } //Produksi 100% RSF, AKM PRODUKSI  RSF, dll
            public decimal Value { get; set; } //nilainya
            public string Uom { get; set; } //Satuan
        }

        #endregion

        private class WCSpinning
        {
            public string WCCode { get; set; }
            public string WCName { get; set; }
            public bool UnitInd { get; set; }
            public bool PercentInd { get; set; }
            public bool ProdDataPerItemInd { get; set; }
            public decimal TotalMachine { get; set; }
            public string SeqNo { get; set; }
            public string ProdFormula { get; set; }
            public string FSNICode1 { get; set; }
            public string FSNICode2 { get; set; }    
        }

        private class DataMesin
        {
            public string WCCode { get; set; }
            public decimal MesinTotalUnit { get; set; }
            public decimal MesinTotalPersen { get; set; }
            public decimal MesinJalanUnit { get; set; }
            public decimal MesinJalanPersen { get; set; }
        }

        private class DataProduksi1
        {
            public string WCCode { get; set; }
            public decimal Unit { get; set; }
            public decimal Kg { get; set; }
            public decimal Persen { get; set; }
        }

        private class ProdEfficiencyVar
        {
            public string PEVCode { get; set; }
            public string PEVDesc { get; set; }
        }

        private class DataEfisiensi
        {
            public string WCCode { get; set; }
            public string PEVCode { get; set; }
            public string PEVDesc { get; set; }
            public decimal Value { get; set; }
        }

        private class FormulaSpinningNonItem
        {
            public string WCCode { get; set; }
            public bool PercentInd { get; set; }
            public decimal TotalMachine { get; set; }
            public bool TotalInd { get; set; }
            public string Query_1 { get; set; }
            public string Param1_1 { get; set; }
            public string Param2_1 { get; set; }
            public string Param3_1 { get; set; }
            public string Param4_1 { get; set; }
            public string Param5_1 { get; set; }
            public string Param6_1 { get; set; }
            public string Query_2 { get; set; }
            public string Param1_2 { get; set; }
            public string Param2_2 { get; set; }
            public string Param3_2 { get; set; }
            public string Param4_2 { get; set; }
            public string Param5_2 { get; set; }
            public string Param6_2 { get; set; }
        }

        private class FormulaParam
        {
            public string ParamNo { get; set; }
            public string ParamDesc { get; set; }
            public decimal Value { get; set; }
        }

        private class DataProduksi2
        {
            public string WCCode { get; set; }
            public string ProdFormula { get; set; }
            public bool UnitInd { get; set; }
            public bool PercentInd { get; set; }
            public string ItCode { get; set; }
            public string ItName { get; set; }
            public decimal Unit { get; set; }
            public decimal Kg { get; set; }
            public decimal Persen { get; set; }
            public decimal RSRPM  { get; set; }
	        public decimal RSTPI { get; set; }
	        public decimal RSNE { get; set; }
	        public decimal RSAvgSPL { get; set; }
	        public decimal RSMFR { get; set; }
	        public decimal WSpeed { get; set; }
	        public decimal WNE { get; set; }
	        public decimal WDelMC { get; set; }
	        public decimal WDelRun { get; set; }
	        public decimal WMFR { get; set; }
            public decimal WDel { get; set; }
        }

        private class DataProduksi2Total
        {
            public string WCCode { get; set; }
            public string ProdFormula { get; set; }
            public bool RingSpinningInd { get; set; }
            public bool WinderInd { get; set; }
            public bool PackingInd { get; set; }
            public decimal Unit { get; set; }
            public decimal Kg { get; set; }
            public decimal Persen { get; set; }
        }

        private class WCGrd
        {
            public string WCCode { get; set; }
            public int No { get; set; }
        }

        private class PEVGrd
        {
            public string PEVCode { get; set; }
            public int No { get; set; }
        }

        #endregion

        #region for Print

        private void BtnPrintOut_Click(object sender, EventArgs e)
        {
            try
            {
                ParPrint();
            }
            catch(Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void ParPrint()
        {
            if (Sm.StdMsgYN("Print", "") == DialogResult.No) return;
            var ld = new List<SPHdr>();
            var ldtl = new List<HdrWorkcenter>();
            var ldtl2 = new List<MachineSPDtl>();
            var ldtl3 = new List<ProduksiSPDtl>();
            var ldtl4 = new List<EfisiensiSPDtl>();


            string[] TableName = { "SP", "HdrWorkcenter", "MachineSPDtl", "ProduksiSPDtl", "EfisiensiSPDtl" };
            List<IList> myLists = new List<IList>();

            var cm = new MySqlCommand();

            #region Header

            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress',  ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyAddressCity', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyPhone',  ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle5') As 'CompanyFax' ");


            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                    {
                         //0
                         "CompanyLogo",

                         //1-5
                         "CompanyName",
                         "CompanyAddress",
                         "CompanyAddressCity",
                         "CompanyPhone",
                         "CompanyFax",
                        
                    });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ld.Add(new SPHdr()
                        {
                            CompanyLogo = Sm.DrStr(dr, c[0]),
                            CompanyName = Sm.DrStr(dr, c[1]),
                            CompanyAddress = Sm.DrStr(dr, c[2]),
                            CompanyAddressCity = Sm.DrStr(dr, c[3]),
                            CompanyPhone = Sm.DrStr(dr, c[4]),
                            CompanyFax = Sm.DrStr(dr, c[5]),
                            DocDt = String.Format("{0:dd MMM yyyy}", Sm.ConvertDateTime(Sm.GetDte(DteDt))),
                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
                        });
                    }
                }
                dr.Close();
            }
            myLists.Add(ld);

            #endregion

            #region Header Data machine
            ldtl.Add(new HdrWorkcenter()
                    {
                        Nama = "Keterangan",
                        WC1 = Grd1.Cols[1].Text.ToString().Substring(0, Grd1.Cols[1].Text.ToString().IndexOf("(")),
                        WC2 = Grd1.Cols[3].Text.ToString().Substring(0, Grd1.Cols[3].Text.ToString().IndexOf("(")),
                        WC3 = Grd1.Cols[5].Text.ToString().Substring(0, Grd1.Cols[5].Text.ToString().IndexOf("(")),
                        WC4 = Grd1.Cols[7].Text.ToString().Substring(0, Grd1.Cols[7].Text.ToString().IndexOf("(")),
                        WC5 = Grd1.Cols[9].Text.ToString().Substring(0, Grd1.Cols[9].Text.ToString().IndexOf("(")),
                        WC6 = Grd1.Cols[11].Text.ToString().Substring(0, Grd1.Cols[11].Text.ToString().IndexOf("(")),
                        WC7 = Grd4.Cols[1].Text.ToString().Substring(0, Grd4.Cols[1].Text.ToString().IndexOf("(")),
                        WC8 = Grd4.Cols[3].Text.ToString().Substring(0, Grd4.Cols[3].Text.ToString().IndexOf("(")),
                        WC9 = Grd4.Cols[5].Text.ToString().Substring(0, Grd4.Cols[5].Text.ToString().IndexOf("("))
                    });
             myLists.Add(ldtl);
            #endregion

            #region Detail data machine

            var lDataMesin = new List<DataMesin>();

            int nomor = 0;
            for (int Row = 0; Row<Grd1.Rows.Count; Row++)
            {
                nomor = nomor + 1;
                 ldtl2.Add(new MachineSPDtl()
                    {
                        no = nomor,
                        Mesin = Sm.GetGrdStr(Grd1, Row, 0),
                        Mesin1 = Sm.GetGrdDec(Grd1, Row, 1),
                        AmtMesin1 = Sm.GetGrdDec(Grd1, Row, 2),
                        Mesin2 = Sm.GetGrdDec(Grd1, Row, 3),
                        AmtMesin2 = Sm.GetGrdDec(Grd1, Row, 4),
                        Mesin3 = Sm.GetGrdDec(Grd1, Row, 5),
                        AmtMesin3 = Sm.GetGrdDec(Grd1, Row, 6),
                        Mesin4 = Sm.GetGrdDec(Grd1, Row, 7),
                        AmtMesin4 = Sm.GetGrdDec(Grd1, Row, 8),
                        Mesin5 = Sm.GetGrdDec(Grd1, Row, 9),
                        AmtMesin5 = Sm.GetGrdDec(Grd1, Row, 10),
                        Mesin6 = Sm.GetGrdDec(Grd1, Row, 11),
                        AmtMesin6 = Sm.GetGrdDec(Grd1, Row, 12),
                        Mesin7 = Sm.GetGrdDec(Grd4, Row, 1),
                        AmtMesin7 = Sm.GetGrdDec(Grd4, Row, 2),
                        Mesin8 = Sm.GetGrdDec(Grd4, Row, 3),
                        AmtMesin8 = Sm.GetGrdDec(Grd4, Row, 4),
                        Mesin9 = Sm.GetGrdDec(Grd4, Row, 5),
                        AmtMesin9 = Sm.GetGrdDec(Grd4, Row, 6)
                    }
                    );
            }

            myLists.Add(ldtl2);
            #endregion

            #region Detail data produksi

            var lDataProduksi = new List<ProduksiSPDtl>();

            int nomor1 = 0;
            for (int Row = 0; Row < Grd2.Rows.Count; Row++)
            {
                nomor1 = nomor1 + 1;
                ldtl3.Add(new ProduksiSPDtl()
                {
                    no = nomor,
                    Nama = Sm.GetGrdStr(Grd2, Row, 0),
                    Prod1 = Sm.GetGrdDec(Grd2, Row, 1),
                    KgProd1 = Sm.GetGrdDec(Grd2, Row, 2),
                    ProsenProd1 = Sm.GetGrdDec(Grd2, Row, 3),
                    Prod2 = Sm.GetGrdDec(Grd2, Row, 4),
                    KgProd2 = Sm.GetGrdDec(Grd2, Row, 5),
                    ProsenProd2 = Sm.GetGrdDec(Grd2, Row, 6),
                    Prod3 = Sm.GetGrdDec(Grd2, Row, 7),
                    KgProd3 = Sm.GetGrdDec(Grd2, Row, 8),
                    ProsenProd3 = Sm.GetGrdDec(Grd2, Row, 9),
                    Prod4 = Sm.GetGrdDec(Grd2, Row, 10),
                    KgProd4 = Sm.GetGrdDec(Grd2, Row, 11),
                    ProsenProd4 = Sm.GetGrdDec(Grd2, Row, 12),
                    Prod5 = Sm.GetGrdDec(Grd2, Row, 13),
                    KgProd5 = Sm.GetGrdDec(Grd2, Row, 14),
                    ProsenProd5 = Sm.GetGrdDec(Grd2, Row, 15),
                    Prod6 = Sm.GetGrdDec(Grd2, Row, 16),
                    KgProd6 = Sm.GetGrdDec(Grd2, Row, 17),
                    ProsenProd6 = Sm.GetGrdDec(Grd2, Row, 18),
                }
                   );
            }
            for (int Row = 0; Row < Grd5.Rows.Count; Row++)
            {
                nomor1 = nomor1 + 1;
                ldtl3.Add(new ProduksiSPDtl()
                {
                    no = nomor,

                    Nama = Sm.GetGrdStr(Grd5, Row, 2),
                    Prod7 = Sm.GetGrdDec(Grd5, Row, 3),
                    KgProd7 = Sm.GetGrdDec(Grd5, Row, 4),
                    ProsenProd7 = Sm.GetGrdDec(Grd5, Row, 5),
                    Prod8 = Sm.GetGrdDec(Grd5, Row, 6),
                    KgProd8 = Sm.GetGrdDec(Grd5, Row, 7),
                    ProsenProd8 = Sm.GetGrdDec(Grd5, Row, 8),
                    Prod9 = Sm.GetGrdDec(Grd5, Row, 9),
                    KgProd9 = Sm.GetGrdDec(Grd5, Row, 10),
                    ProsenProd9 = Sm.GetGrdDec(Grd5, Row, 11),                    
                }
                   );
            }

            myLists.Add(ldtl3);
            #endregion

            #region Detail data efisiensi 
            var lefi1 = new List<Efi1>();
            var lefi2 = new List<Efi2>();
            PEfi1(ref lefi1);
            PEfi2(ref lefi2);

            if (lefi1.Count > 0)
            {
                for (var i = 0; i < lefi1.Count; i++)
                {
                    if (lefi2.Count > 0)
                    {
                        for (var j = 0; j < lefi2.Count; j++)
                        {
                            if (lefi1[i].Nama == lefi2[j].Nama)
                            {
                                ldtl4.Add(new EfisiensiSPDtl()
                                {
                                    Nama = lefi1[i].Nama,
                                    Ef1 = lefi1[i].Ef1,
                                    Ef2 = lefi1[i].Ef2,
                                    Ef3 = lefi1[i].Ef3,
                                    Ef4 = lefi1[i].Ef4,
                                    Ef5 = lefi1[i].Ef5,
                                    Ef6 = lefi1[i].Ef6,
                                    Ef7 = lefi2[j].Ef7,
                                    Ef8 = lefi2[j].Ef8,
                                    Ef9 = lefi2[j].Ef9,
                                });
                            }
                        }
                    }
                }
                
            }

            myLists.Add(ldtl4);
            #endregion

            Sm.PrintReport("Spinning", myLists, TableName, false);
        }

        private void PEfi1(ref List<Efi1> lefi1)
        {
            for (int Row = 0; Row < Grd3.Rows.Count; Row++)
            {
                lefi1.Add(new Efi1()
                {
                    Nama = Sm.GetGrdStr(Grd3, Row, 0),
                    Ef1 = Sm.GetGrdDec(Grd3, Row, 1),
                    Ef2 = Sm.GetGrdDec(Grd3, Row, 2),
                    Ef3 = Sm.GetGrdDec(Grd3, Row, 3),
                    Ef4 = Sm.GetGrdDec(Grd3, Row, 4),
                    Ef5 = Sm.GetGrdDec(Grd3, Row, 5),
                    Ef6 = Sm.GetGrdDec(Grd3, Row, 6),
                });
            }
        }

        private void PEfi2(ref List<Efi2> lefi2)
        {
            for (int Row = 0; Row < Grd6.Rows.Count; Row++)
            {
                lefi2.Add(new Efi2()
                {
                    Nama = Sm.GetGrdStr(Grd6, Row, 0),
                    Ef7 = Sm.GetGrdDec(Grd6, Row, 1),
                    Ef8 = Sm.GetGrdDec(Grd6, Row, 2),
                    Ef9 = Sm.GetGrdDec(Grd6, Row, 3),
                });
            }
        }


        #region Class for Print

        private class SPHdr
        {
            public string CompanyLogo { set; get; }
            public string CompanyName { get; set; }
            public string CompanyAddressCity { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyPhone { get; set; }
            public string CompanyFax { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string SFCDt { get; set; }
            public string Remark { get; set; }
            public string PrintBy { get; set; }
        }

        private class HdrWorkcenter
        {
            public string Nama { set; get; }
            public string WC1 { set; get; }
            public string WC2 { set; get; }
            public string WC3 { set; get; }
            public string WC4 { set; get; }
            public string WC5 { set; get; }
            public string WC6 { set; get; }
            public string WC7 { set; get; }
            public string WC8 { set; get; }
            public string WC9 { set; get; }
        }

        private class MachineSPDtl
        {
            public decimal no { set; get; }
            public string Nama { set; get; }
            public string Mesin { set; get; }
            public decimal Mesin1 { set; get; }
            public decimal AmtMesin1 { get; set; }
            public decimal Mesin2 { set; get; }
            public decimal AmtMesin2 { get; set; }
            public decimal Mesin3 { set; get; }
            public decimal AmtMesin3 { get; set; }
            public decimal Mesin4 { set; get; }
            public decimal AmtMesin4 { get; set; }
            public decimal Mesin5 { set; get; }
            public decimal AmtMesin5 { get; set; }
            public decimal Mesin6 { set; get; }
            public decimal AmtMesin6 { get; set; }
            public decimal Mesin7 { set; get; }
            public decimal AmtMesin7 { get; set; }
            public decimal Mesin8 { set; get; }
            public decimal AmtMesin8 { get; set; }
            public decimal Mesin9 { set; get; }
            public decimal AmtMesin9 { get; set; }
        }

        private class ProduksiSPDtl
        {
            public decimal no { set; get; }
            public string Nama { set; get; }
            public decimal Prod1 { set; get; }
            public decimal KgProd1 { set; get; }
            public decimal ProsenProd1 { get; set; }
            public decimal Prod2 { set; get; }
            public decimal KgProd2 { set; get; }
            public decimal ProsenProd2 { get; set; }
            public decimal Prod3 { set; get; }
            public decimal KgProd3 { set; get; }
            public decimal ProsenProd3 { get; set; }
            public decimal Prod4 { set; get; }
            public decimal KgProd4 { set; get; }
            public decimal ProsenProd4 { get; set; }
            public decimal Prod5 { set; get; }
            public decimal KgProd5 { set; get; }
            public decimal ProsenProd5 { get; set; }
            public decimal Prod6 { set; get; }
            public decimal KgProd6 { set; get; }
            public decimal ProsenProd6 { get; set; }
            public decimal Prod7 { set; get; }
            public decimal KgProd7 { set; get; }
            public decimal ProsenProd7 { get; set; }
            public decimal Prod8 { set; get; }
            public decimal KgProd8 { set; get; }
            public decimal ProsenProd8 { get; set; }
            public decimal Prod9 { set; get; }
            public decimal KgProd9 { set; get; }
            public decimal ProsenProd9 { get; set; }
        }

        private class EfisiensiSPDtl
        {
            public string Nama { set; get; }
            public Decimal Ef1 { set; get; }
            public Decimal Ef2 { set; get; }
            public Decimal Ef3 { set; get; }
            public Decimal Ef4 { set; get; }
            public Decimal Ef5 { set; get; }
            public Decimal Ef6 { set; get; }
            public Decimal Ef7 { set; get; }
            public Decimal Ef8 { set; get; }
            public Decimal Ef9 { set; get; }
        }

        private class Efi1
        {
            public string Nama { set; get; }
            public Decimal Ef1 { set; get; }
            public Decimal Ef2 { set; get; }
            public Decimal Ef3 { set; get; }
            public Decimal Ef4 { set; get; }
            public Decimal Ef5 { set; get; }
            public Decimal Ef6 { set; get; }
            public Decimal Ef7 { set; get; }
            public Decimal Ef8 { set; get; }
            public Decimal Ef9 { set; get; }
        }

        private class Efi2
        {
            public string Nama { set; get; }
            public Decimal Ef7 { set; get; }
            public Decimal Ef8 { set; get; }
            public Decimal Ef9 { set; get; }
        }

        #endregion

        #endregion
    }

 
}
