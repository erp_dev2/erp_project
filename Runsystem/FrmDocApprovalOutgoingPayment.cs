﻿#region Namespace

using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

#endregion

namespace RunSystem
{
    public partial class FrmDocApprovalOutgoingPayment : RunSystem.FrmBase9
    {
        #region Field

        internal string mDocNo = string.Empty;
 
        #endregion

        #region Constructor

        public FrmDocApprovalOutgoingPayment(string MenuCode) 
        {
            InitializeComponent();
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                TxtDocNo.EditValue = mDocNo;
                SetGrd();
                ShowData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        private void SetGrd()
        {
            Grd1.Cols.Count = 21;
            Grd1.FrozenArea.ColCount = 1;
            Sm.GrdHdr( // GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Requested"+Environment.NewLine+"Date",
                        "Requested#", 
                        "Item",
                        "Requested"+Environment.NewLine+"Qty",
                        "PO Request"+Environment.NewLine+"Date",

                        //6-10
                        "PO Request#",
                        "Vendor", 
                        "PO Date",
                        "PO#",
                        "PO"+Environment.NewLine+"Price",
                        
                        //11-15
                        "PO"+Environment.NewLine+"Tax %",
                        "Received"+Environment.NewLine+"Date",
                        "Received#",
                        "Received"+Environment.NewLine+"Qty",
                        "Invoice"+Environment.NewLine+"Date",
                        
                        //16-20
                        "Invoice#",
                        "Invoice"+Environment.NewLine+"Amount",
                        "Outgoing"+Environment.NewLine+"Payment Date",
                        "Outgoing"+Environment.NewLine+"Payment#",
                        "Outgoing"+Environment.NewLine+"Payment Amount",
                    }
                    //,new int[] 
                    //{
                    //    //0
                    //    50,

                    //    //1-5
                    //    100, 150, 200, 120, 100, 
                        
                    //    //6-10
                    //    150, 200, 100, 150, 120, 
                        
                    //    //11-15
                    //    100, 100, 150, 120, 100, 
                        
                    //    //16-20
                    //    150, 130, 100, 150, 130
                    //}
                );
            Sm.GrdFormatDate(Grd1, new int[] { 1, 5, 8, 12, 15, 18 });
            Sm.GrdFormatDec(Grd1, new int[] { 4, 10, 11, 14, 17, 20 }, 0);
            Sm.SetGrdProperty(Grd1, false);
        }

        private void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();
                var cm = new MySqlCommand();

                SQL.AppendLine("Select ");
                SQL.AppendLine("K.DocDt As MaterialRequestDocDt, K.LocalDocNo As MaterialRequestLocalDocNo, O.ItName, L.Qty, ");
                SQL.AppendLine("I.DocDt As PORequestDocDt, I.LocalDocNo As PORequestLocalDocNo, P.VdName, ");
                SQL.AppendLine("G.DocDt As PODocDt, G.LocalDocNo As POLocalDocNo, N.UPrice, IfNull(Q.TaxRate, 0) As TaxRate, ");
                SQL.AppendLine("E.DocDt As RecvVdDocDt, E.LocalDocNo As RecvVdLocalDocNo, F.QtyPurchase As RecvVdQty, ");
                SQL.AppendLine("C.DocDt As PurchaseInvoiceDocDt, C.LocalDocNo As PurchaseInvoiceLocalDocNo, ");
                SQL.AppendLine("(N.UPrice*F.QtyPurchase)*((100+IfNull(Q.TaxRate, 0))/100) As PurchaseInvoiceAmt, ");
                SQL.AppendLine("A.DocDt As OutgoingPaymentDocDt, A.LocalDocNo As OutgoingPaymentLocalDocNo, B.Amt As OutgoingPaymentAmt ");
                SQL.AppendLine("From TblOutgoingPaymentHdr A ");
                SQL.AppendLine("Inner Join TblOutgoingPaymentDtl B On B.InvoiceType='1' And A.DocNo=B.DocNo ");
                SQL.AppendLine("Inner Join TblPurchaseInvoiceHdr C On B.InvoiceDocNo=C.DocNo ");
                SQL.AppendLine("Inner Join TblPurchaseInvoiceDtl D On C.DocNo=D.DocNo ");
                SQL.AppendLine("Inner Join TblRecvVdHdr E On D.RecvVdDocNo=E.DocNo ");
                SQL.AppendLine("Inner Join TblRecvVdDtl F On D.RecvVdDocNo=F.DocNo And D.RecvVdDNo=F.DNo ");
                SQL.AppendLine("Inner Join TblPOHdr G On F.PODocNo=G.DocNo ");
                SQL.AppendLine("Inner Join TblPODtl H On F.PODocNo=H.DocNo And F.PODNo=H.DNo ");
                SQL.AppendLine("Inner Join TblPORequestHdr I On H.PORequestDocNo=I.DocNo ");
                SQL.AppendLine("Inner Join TblPORequestDtl J On H.PORequestDocNo=J.DocNo And H.PORequestDNo=J.DNo ");
                SQL.AppendLine("Inner Join TblMaterialRequestHdr K On J.MaterialRequestDocNo=K.DocNo ");
                SQL.AppendLine("Inner Join TblMaterialRequestDtl L On J.MaterialRequestDocNo=L.DocNo And J.MaterialRequestDNo=L.DNo ");
                SQL.AppendLine("Inner Join TblQtHdr M On J.QtDocNo=M.DocNo ");
                SQL.AppendLine("Inner Join TblQtDtl N On J.QtDocNo=N.DocNo And J.QtDNo=N.DNo ");
                SQL.AppendLine("Inner Join TblItem O On L.ItCode=O.ItCode ");
                SQL.AppendLine("Inner Join TblVendor P On M.VdCode=P.VdCode ");
                SQL.AppendLine("Left Join TblTax Q On G.TaxCode1=Q.TaxCode ");
                SQL.AppendLine("Where A.DocNo=@DocNo ");
                SQL.AppendLine("Order By O.ItName, P.VdName;");

                Sm.CmParam<String>(ref cm, "@DocNo", mDocNo);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, SQL.ToString(),
                        new string[]
                        {
                            //0
                            "MaterialRequestDocDt",
                            
                            //1-5
                            "MaterialRequestLocalDocNo", 
                            "ItName", 
                            "Qty", 
                            "PORequestDocDt", 
                            "PORequestLocalDocNo", 

                            //6-10
                            "VdName", 
                            "PODocDt", 
                            "POLocalDocNo", 
                            "UPrice", 
                            "TaxRate", 
                            
                            //11-15
                            "RecvVdDocDt", 
                            "RecvVdLocalDocNo", 
                            "RecvVdQty", 
                            "PurchaseInvoiceDocDt", 
                            "PurchaseInvoiceLocalDocNo", 
                            
                            //16-19
                            "PurchaseInvoiceAmt", 
                            "OutgoingPaymentDocDt", 
                            "OutgoingPaymentLocalDocNo", 
                            "OutgoingPaymentAmt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 15);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 16);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 18, 17);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 18);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 19);
                        }, true, false, false, true
                    );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 17 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion
    }
}
