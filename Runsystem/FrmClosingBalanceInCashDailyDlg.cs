﻿#region Update
/*
    13/09/2018 [TKG] tambah fasilitas untuk memilih bank account yg mau diproses.
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmClosingBalanceInCashDailyDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmClosingBalanceInCashDaily mFrmParent;
        private string mSQL = string.Empty, mClosingDt = string.Empty;
        
        #endregion

        #region Constructor

        public FrmClosingBalanceInCashDailyDlg(FrmClosingBalanceInCashDaily FrmParent, string ClosingDt)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mClosingDt = Sm.Left(ClosingDt, 8);
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T1.BankAcCode, T2.BankAcNm, T1.Amt, ");
            SQL.AppendLine("T4.EntName, ");
            SQL.AppendLine("Concat( ");
            SQL.AppendLine("    Case When T3.BankName Is Not Null Then Concat(T3.BankName, ' : ') Else '' End, ");
            SQL.AppendLine("    Case When T2.BankAcNo Is Not Null  ");
            SQL.AppendLine("    Then Concat(T2.BankAcNo, ' [', IfNull(T2.BankAcNm, ''), ']') ");
            SQL.AppendLine("    Else IfNull(T2.BankAcNm, '') End ");
            SQL.AppendLine(") As BankAcDesc ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select T.BankAcCode, Sum(T.Amt) As Amt From (");
            SQL.AppendLine("        Select B.BankAcCode, B.Amt ");
            SQL.AppendLine("        From TblClosingBalanceInCashHdr A, TblClosingBalanceInCashDtl B ");
            SQL.AppendLine("        Where A.DocNo=B.DocNo And Concat(A.Yr, A.Mth)=@YrMth2 ");
            if (mFrmParent.mIsFilterByBankAccount)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupBankAccount ");
                SQL.AppendLine("    Where BankAcCode=B.BankAcCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select A.BankAcCode, Case A.Actype When 'C' Then -1 Else 1 End * B.Amt As Amt ");
            SQL.AppendLine("        From TblVoucherHdr A, TblVoucherDtl B ");
            SQL.AppendLine("        Where A.DocNo = B.DocNo And A.CancelInd='N' And A.DocDt between Concat(@YrMth1, '01') And @ClosingDt ");
            if (mFrmParent.mIsFilterByBankAccount)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupBankAccount ");
                SQL.AppendLine("    Where BankAcCode=A.BankAcCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select A.BankAcCode2, (Case A.Actype2 When 'C' Then -1 Else 1 End) * B.Amt * A.ExcRate As Amt ");
            SQL.AppendLine("        From TblVoucherHdr A, TblVoucherDtl B ");
            SQL.AppendLine("        Where A.DocNo=B.DocNo And A.CancelInd='N' And A.DocDt Between Concat(@YrMth1, '01') And @ClosingDt And A.AcType2 Is Not Null ");
            if (mFrmParent.mIsFilterByBankAccount)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupBankAccount ");
                SQL.AppendLine("    Where BankAcCode=A.BankAcCode2 ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("    ) T Group By T.BankAcCode ");
            SQL.AppendLine(") T1 ");
            SQL.AppendLine("Inner Join TblBankAccount T2 On T1.BankAcCode=T2.BankAcCode ");
            if (mFrmParent.mIsFilterByBankAccount)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupBankAccount ");
                SQL.AppendLine("    Where BankAcCode=T2.BankAcCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("Left Join TblBank T3 On T2.BankCode=T3.BankCode ");
            SQL.AppendLine("Left Join TblEntity T4 On T2.EntCode=T4.EntCode ");
            SQL.AppendLine("Where T1.BankAcCode Not In ( ");
            SQL.AppendLine("    Select B.BankAcCode ");
            SQL.AppendLine("    From TblClosingBalanceInCashDailyHdr A ");
            SQL.AppendLine("    Inner Join TblClosingBalanceInCashDailyDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("    Where A.ClosingDt=@ClosingDt ");
            SQL.AppendLine("    ) ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 7;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "",
                        "Code",
                        "Bank Account",
                        "Amount",
                        "Entity",

                        //6
                        "Description"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        20, 100, 200, 150, 200, 
                        
                        //6
                        400
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdFormatDec(Grd1, new int[] { 4 }, 0);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 3, 4, 5, 6 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 6 }, false);
            if (!mFrmParent.mIsEntityMandatory) Sm.GrdColInvisible(Grd1, new int[] { 5 }, false);
            Sm.SetGrdProperty(Grd1 , false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 6 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;
                string BankAcCode = string.Empty;
                var cm = new MySqlCommand();
                
                string Mth = mClosingDt.Substring(4, 2);
                string Yr = mClosingDt.Substring(0, 4);
                string Mth2 = (Mth == "01") ? "12" : Sm.Right("0" + (int.Parse(Mth) - 1).ToString(), 2);
                string Yr2 = (Mth == "01") ? ((int.Parse(Yr)) - 1).ToString() : Yr;

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@ClosingDt", mClosingDt);
                Sm.CmParam<String>(ref cm, "@Yr", Yr);
                Sm.CmParam<String>(ref cm, "@Mth", Mth);
                Sm.CmParam<String>(ref cm, "@YrMth1", Yr + Mth);
                Sm.CmParam<String>(ref cm, "@YrMth2", Yr2 + Mth2);

                if (mFrmParent.Grd1.Rows.Count >= 1)
                {
                    for (int r = 0; r < mFrmParent.Grd1.Rows.Count; r++)
                    {
                        BankAcCode = Sm.GetGrdStr(mFrmParent.Grd1, r, 1);
                        if (BankAcCode.Length != 0)
                        {
                            if (Filter.Length > 0) Filter += " Or ";
                            Filter += "(T1.BankAcCode=@BankAcCode0" + r.ToString() + ") ";
                            Sm.CmParam<String>(ref cm, "@BankAcCode0" + r.ToString(), BankAcCode);
                        }
                    }
                }
                if (Filter.Length > 0)
                    Filter = " And Not(" + Filter + ") ";
                else
                    Filter = " ";


                Sm.FilterStr(ref Filter, ref cm, TxtBankAcCode.Text, new string[] { "T2.BankAcCode", "T2.BankAcNm" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By T2.BankAcNm;",
                        new string[]
                        {
                            //0
                            "BankAcCode",

                            //1-4
                            "BankAcNm", "Amt", "EntName", "BankAcDesc"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = false;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsDataAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 1, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 2, Grd1, Row2, 3);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 3, Grd1, Row2, 4);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 4, Grd1, Row2, 5);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 5, Grd1, Row2, 6);
                        
                        mFrmParent.Grd1.Rows.Add();
                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 3 });
                    }
                }
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 bank account.");
        }

        private bool IsDataAlreadyChosen(int r)
        {
            var BankAcCode = Sm.GetGrdStr(Grd1, r, 2);
            for (int i = 0; i < mFrmParent.Grd1.Rows.Count - 1; i++)
                if (Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd1, i, 1), BankAcCode)) return true;
            return false;
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    Grd1.Cells[Row, 1].Value = !IsSelected;
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event
        
        #region Misc Control Event

        private void ChkBankAcCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Bank Account");
        }

        private void TxtBankAcCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        #endregion

        #endregion
    }
}
