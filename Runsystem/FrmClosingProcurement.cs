﻿#region Update
/*
    22/02/2023 [IBL/HEX] New apps
*/
#endregion

#region Namespace

using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using DXE = DevExpress.XtraEditors;
using Gv = RunSystem.GlobalVar;
using Sl = RunSystem.SetLue;
using Sm = RunSystem.StdMtd;

#endregion

namespace RunSystem
{
    public partial class FrmClosingProcurement : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal bool
            mIsClosingProcurementUseMRType = false;

        internal FrmClosingProcurementFind FrmFind;

        #endregion

        #region Constructor

        public FrmClosingProcurement(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Monthly Journal Entries's Closing";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetFormControl(mState.View);
                Sl.SetLueOption(ref LueType, "MRType");
                if (!mIsClosingProcurementUseMRType)
                    label4.Visible = LueType.Visible = false;
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }

        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    { DteDocDt, MeeCancelReason, ChkCancelInd, LueType, LueYr, MeeRemark }, true);
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    { DteDocDt, LueType, LueYr, MeeRemark }, false);
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    ChkCancelInd.Properties.ReadOnly = false;
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    { MeeCancelReason }, false);
                    ChkCancelInd.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit>
            { TxtDocNo, DteDocDt, MeeCancelReason, ChkCancelInd, LueType, LueYr, MeeRemark });
            ChkCancelInd.Checked = false;
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmClosingProcurementFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sl.SetLueYr(LueYr, "");
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            string DocNo = string.Empty;
            var cml = new List<MySqlCommand>();

            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;
            DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "ClosingProcurement", "TblClosingProcurement");
            cml.Add(SaveClosingProcurement(DocNo));
            Sm.ExecCommands(cml);
            ShowData(DocNo);
        }

        private MySqlCommand SaveClosingProcurement(string DocNo)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblClosingProcurement(DocNo, DocDt, CancelInd, MRType, Year, Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DocDt, 'N', @MRType, @Year, @Remark, @UserCode, CurrentDateTime()); "
            };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@MRType", Sm.GetLue(LueType));
            Sm.CmParam<String>(ref cm, "@Year", Sm.GetLue(LueYr));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                (mIsClosingProcurementUseMRType && Sm.IsLueEmpty(LueType, "Type")) ||
                Sm.IsLueEmpty(LueYr, "Year") ||
                IsClosingDtInvalid();
        }

        private bool IsClosingDtInvalid()
        {
            return Sm.IsDataExist(
                    "Select 1 From TblClosingProcurement Where CancelInd='N' And Year=@Param;", Sm.GetLue(LueYr),
                    "Invalid year.");
        }

        #endregion

        #region Edit data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditClosingProcuremment());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private MySqlCommand EditClosingProcuremment()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Update TblClosingProcurement Set CancelInd='Y', CancelReason=@CancelReason, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() " +
                    "Where DocNo=@DocNo And CancelInd='N';"
            };

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDocAlreadyCancelled();
        }


        private bool IsDocAlreadyCancelled()
        {
            return Sm.IsDataExist(
                "Select 1 From TblClosingJournal Where CancelInd='Y' And DocNo=@Param;",
                TxtDocNo.Text,
                "This document already cancelled.");
        }

        #endregion

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                ShowClosingProcurement(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }


        private void ShowClosingProcurement(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelReason, A.CancelInd, A.MRType, A.Year, A.Remark ");
            SQL.AppendLine("From TblClosingProcurement A ");
            SQL.AppendLine("Where DocNo=@DocNo; ");

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[]
                { 
                    //0
                    "DocNo",

                    //1-5
                    "DocDt", "CancelReason", "CancelInd", "MRType", "Year",

                    //6
                    "Remark"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                    MeeCancelReason.EditValue = Sm.DrStr(dr, c[2]);
                    ChkCancelInd.Checked = Sm.DrStr(dr, c[3]) == "Y";
                    Sm.SetLue(LueType, Sm.DrStr(dr, c[4]));
                    Sl.SetLueYr(LueYr, string.Empty);
                    Sm.SetLue(LueYr, Sm.DrStr(dr, c[5]));
                    MeeRemark.EditValue = Sm.DrStr(dr, c[6]);
                }, true
            );
        }

        #endregion

        #region Additional Method
        private void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'IsClosingProcurementUseMRType' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsClosingProcurementUseMRType": mIsClosingProcurementUseMRType = ParValue == "Y"; break;

                        }
                    }
                }
                dr.Close();

            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        private void LueType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueType, new Sm.RefreshLue2(Sl.SetLueOption), "MRType");
        }

        #endregion

        #endregion
    }
}
