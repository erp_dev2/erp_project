﻿#region Update
/*
    07/08/2017 [WED] tambah kolom foreign name, berdasarkan parameter IsShowForeignName
    26/12/2018 [TKG] non aktif item tidak ditampilkan.
    19/01/2021 [IBL/IMS] tambah kolom local code dan specification berdasarkan parameter IsBOMShowSpecifications
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmMutationsDlg2 : RunSystem.FrmBase4
    {
        #region Field

        private FrmMutations mFrmParent;

        #endregion

        #region Constructor

        public FrmMutationsDlg2(FrmMutations FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                Sl.SetLueItCtCode(ref LueItCtCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 13;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "", 
                        "Item's Code", 
                        "", 
                        "Local Code", 
                        "Item's Name",
                        
                        //6-10
                        "Foreign Name",
                        "Item's Category",
                        "UoM",
                        "UoM",
                        "UoM",
                        
                        //11-12
                        "Group",
                        "Specification"
                    },
                     new int[] 
                    {
                        //0
                        50,
                        //1-5
                        20, 80, 20, 80, 300, 
                        //6-10
                        150, 200, 0, 0, 0, 
                        
                        //11-12
                        100, 150
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColButton(Grd1, new int[] { 3 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 4, 7, 8, 9, 10, 11, 12 }, false);
            if (!mFrmParent.mIsShowForeignName)
                Sm.GrdColInvisible(Grd1, new int[] { 6 });
            if (mFrmParent.mIsItGrpCodeShow)
            {
                Grd1.Cols[11].Visible = true;
                Grd1.Cols[11].Move(7);
            }
            if (mFrmParent.mIsBOMShowSpecifications)
            {
                Grd1.Cols[12].Visible = true;
                Grd1.Cols[12].Move(6);
            }
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 4, 7 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();
                string Filter = " ";
                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "A.ItCode", "A.ItCodeInternal", "A.ItName", "A.ForeignName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueItCtCode), "A.ItCtCode", true);

                SQL.AppendLine("Select A.ItCode, A.ItCodeInternal, A.ItName, A.ForeignName, B.ItCtName, ");
                SQL.AppendLine("A.InventoryUomCode, A.InventoryUomCode2, A.InventoryUomCode3, A.ItGrpCode, A.Specification ");
                SQL.AppendLine("From TblItem A ");
                SQL.AppendLine("Inner Join TblItemCategory B On A.ItCtCode=B.ItCtCode ");
                SQL.AppendLine("Where A.ActInd='Y' ");
                SQL.AppendLine(Filter);
                SQL.AppendLine(" Order By A.ItName;");

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, SQL.ToString(),
                        new string[] 
                        { 
                            //0
                            "ItCode", 

                            //1-5
                            "ItCodeInternal", "ItName", "ForeignName", "ItCtName", "InventoryUomCode", 
                            
                            //6-9
                            "InventoryUomCode2", "InventoryUomCode3", "ItGrpCode", "Specification"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = false;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 9);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                mFrmParent.Grd2.BeginUpdate();
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd2.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 2, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 4, Grd1, Row2, 4);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 5, Grd1, Row2, 5);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 25, Grd1, Row2, 6);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 14, Grd1, Row2, 8);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 17, Grd1, Row2, 9);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 20, Grd1, Row2, 10);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 24, Grd1, Row2, 11);
                        Sm.CopyGrdValue(mFrmParent.Grd2, Row1, 28, Grd1, Row2, 12);
                        Sm.SetGrdNumValueZero(mFrmParent.Grd2, Row1, new int[] { 12, 13, 15, 16, 18, 19, 22, 23 });
                        mFrmParent.Grd2.Rows.Add();
                        Sm.SetGrdNumValueZero(mFrmParent.Grd2, mFrmParent.Grd2.Rows.Count - 1, new int[] { 12, 13, 15, 16, 18, 19, 22, 23 });
                    }
                }
                mFrmParent.Grd2.EndUpdate();
            }
            mFrmParent.ComputeUnitPriceBasedOnFormula();
            mFrmParent.ComputeTotalB();

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 item.");
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0 && Sm.GetGrdStr(Grd1, 0, 2).Length > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    Grd1.Cells[Row, 1].Value = !IsSelected;
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void LueItCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCtCode, new Sm.RefreshLue1(Sl.SetLueItCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkItCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Item's category");
        }

        #endregion

        #endregion
    }
}
