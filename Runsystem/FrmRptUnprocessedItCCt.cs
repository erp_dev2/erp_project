﻿#region Update
/* 
    26/07/2017 [TKG] Reporting baru
*/ 
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptUnprocessedItCCt : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;
        private List<CostCenter> l;

        #endregion

        #region Constructor

        public FrmRptUnprocessedItCCt(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                Sl.SetLueItCtCode(ref LueItCtCode);

                l = new List<CostCenter>();
                SetCostCenter(ref l);
                SetGrd();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 4+l.Count;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No",

                    //1-3
                    "Item's Code",
                    "Item's Name",
                    "Category"
                },
                new int[] 
                {
                    //0
                    50,

                    //1-3
                    80, 200, 150
                }
            );
            for (int i = 0; i < l.Count; i++)
            {
                Grd1.Cols[4+i].Text = l[i].CCName;
                Grd1.Cols[4+i].Width = 150;
                Grd1.Header.Cells[0, 4+i].TextAlign = iGContentAlignment.TopCenter;
                Grd1.Cols[4+i].CellStyle.ValueType = typeof(Boolean);
                Grd1.Cols[4+i].CellStyle.Type = iGCellType.Check;
                Grd1.Cols[4+i].CellStyle.ImageAlign = iGContentAlignment.MiddleCenter;
            }
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3 }, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
           
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                Process1();
                if (Grd1.Rows.Count > 0)
                {
                    Process2();
                    Process3();
                }
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        private void Process1()
        {
            var cm = new MySqlCommand();

            string Filter = " ";

            Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueItCtCode), "A.ItCtCode", true);

            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                "Select A.ItCode, A.ItName, B.ItCtName " +
                "From TblItem A, TblItemCategory B " +
                "Where A.ItCtCode=B.ItCtCode " + Filter + 
                " Order By A.ItName, A.ItCode;",
                new string[]{ "ItCode",  "ItName", "ItCtName" },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                }, true, false, false, false
            );
        }

        private void Process2()
        {
            string 
                Filter = " ", 
                ItCode = string.Empty, 
                CCCode = string.Empty,
                ItCodeTemp = string.Empty, 
                CCCodeTemp = string.Empty
                ;
            int r = 0;
            var cm = new MySqlCommand();

            Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueItCtCode), "B.ItCtCode", true);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText =
                    "Select A.ItCode, C.CCCode " +
                    "From TblItemCostCategory A, TblItem B, TblCostCenter C " +
                    "Where A.ItCode=B.ItCode And A.CCCode=C.CCCode " + 
                    Filter +
                    " Order By B.ItName, B.ItCode, C.CCName, C.CCCode;";
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ItCode", "CCCode" });
                if (dr.HasRows)
                {
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        ItCode = dr.GetString(c[0]);
                        CCCode = dr.GetString(c[1]);
                        if (Sm.CompareStr(ItCode, ItCodeTemp))
                        {
                            for (int x = 0; x < l.Count; x++)
                            {
                                if (Sm.CompareStr(CCCode, l[x].CCCode))
                                {
                                    Grd1.Cells[r, l[x].Col].Value = true;
                                    break;
                                }
                            }
                        }
                        else
                        {
                            for (int i = r; i < Grd1.Rows.Count; i++)
                            {
                                if (Sm.CompareStr(ItCode, Sm.GetGrdStr(Grd1, i, 1)))
                                {
                                    for (int x = 0; x < l.Count; x++)
                                    {
                                        if (Sm.CompareStr(CCCode, l[x].CCCode))
                                        {
                                            Grd1.Cells[r, l[x].Col].Value = true;
                                            break;
                                        }
                                    }
                                    r = i;
                                    break;
                                }
                            }
                            ItCodeTemp = ItCode;
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        private void Process3()
        {
            bool IsValid = true;

            Grd1.BeginUpdate();

            for (int r = Grd1.Rows.Count-1; r==0; r--)
            {
                IsValid = true;
                for (int c = 4; c<Grd1.Cols.Count-1; c++)
                {
                    if (!Sm.GetGrdBool(Grd1, r, c))
                    {
                        IsValid = false;
                        break;
                    }
                }
                if (!IsValid) Grd1.Rows.RemoveAt(r);
            }

            if (Grd1.Rows.Count > 0)
            {
                for (int r = 0; r < Grd1.Rows.Count; r++)
                    Grd1.Cells[r, 0].Value = r+1;
                Grd1.Cols.AutoWidth();
            }

            Grd1.EndUpdate();
        }

        private void SetCostCenter(ref List<CostCenter> l)
        {
            var GrdCol = 3;
            var cm = new MySqlCommand();
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = "Select CCCode, CCName From TblCostCenter Order By CCName, CCCode;";
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "CCCode", "CCName" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        GrdCol += 1;
                        l.Add(new CostCenter()
                        {
                            CCCode = Sm.DrStr(dr, c[0]),
                            CCName = Sm.DrStr(dr, c[1]),
                            Col = GrdCol
                        });
                    }
                }
                dr.Close();
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueItCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCtCode, new Sm.RefreshLue2(Sl.SetLueItCtCode), string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkItCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Item's category");
        }

        #endregion

        #endregion

        #region Class

        private class CostCenter
        {
            public string CCCode { get; set; }
            public string CCName { get; set; }
            public int Col { get; set; }
        }

        #endregion
    }
}
