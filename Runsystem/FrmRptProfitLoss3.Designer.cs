﻿namespace RunSystem
{
    partial class FrmRptProfitLoss3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LueMth = new DevExpress.XtraEditors.LookUpEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.LueYr = new DevExpress.XtraEditors.LookUpEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtProfitLoss = new DevExpress.XtraEditors.TextEdit();
            this.LblEntCode = new System.Windows.Forms.Label();
            this.LueEntCode = new DevExpress.XtraEditors.LookUpEdit();
            this.LueStartFrom = new DevExpress.XtraEditors.LookUpEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.PnlPeriod = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.ChkDocDt = new DevExpress.XtraEditors.CheckEdit();
            this.DteDocDt2 = new DevExpress.XtraEditors.DateEdit();
            this.DteDocDt1 = new DevExpress.XtraEditors.DateEdit();
            this.panel4 = new System.Windows.Forms.Panel();
            this.ChkProfitCenterCode = new DevExpress.XtraEditors.CheckEdit();
            this.CcbProfitCenterCode = new DevExpress.XtraEditors.CheckedComboBoxEdit();
            this.LblMultiEntityCode = new System.Windows.Forms.Label();
            this.LblLevel2 = new System.Windows.Forms.Label();
            this.LueLevelTo = new DevExpress.XtraEditors.LookUpEdit();
            this.LueLevelFrom = new DevExpress.XtraEditors.LookUpEdit();
            this.LblLevel = new System.Windows.Forms.Label();
            this.PnlCCCode = new System.Windows.Forms.Panel();
            this.ChkCCCode = new DevExpress.XtraEditors.CheckEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.LueCCCode = new DevExpress.XtraEditors.LookUpEdit();
            this.ChkEntCode = new DevExpress.XtraEditors.CheckEdit();
            this.LuePeriod = new DevExpress.XtraEditors.LookUpEdit();
            this.LblPeriod = new System.Windows.Forms.Label();
            this.ChkPeriod = new DevExpress.XtraEditors.CheckEdit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueMth.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueYr.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProfitLoss.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueEntCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueStartFrom.Properties)).BeginInit();
            this.PnlPeriod.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt2.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt1.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt1.Properties)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkProfitCenterCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CcbProfitCenterCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueLevelTo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueLevelFrom.Properties)).BeginInit();
            this.PnlCCCode.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCCCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCCCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkEntCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePeriod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkPeriod.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnRefresh
            // 
            this.BtnRefresh.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRefresh.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnRefresh.Appearance.Options.UseBackColor = true;
            this.BtnRefresh.Appearance.Options.UseFont = true;
            this.BtnRefresh.Appearance.Options.UseForeColor = true;
            this.BtnRefresh.Appearance.Options.UseTextOptions = true;
            this.BtnRefresh.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.LuePeriod);
            this.panel2.Controls.Add(this.LblPeriod);
            this.panel2.Controls.Add(this.ChkPeriod);
            this.panel2.Controls.Add(this.ChkEntCode);
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.LueStartFrom);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.LblEntCode);
            this.panel2.Controls.Add(this.LueEntCode);
            this.panel2.Controls.Add(this.TxtProfitLoss);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.LueMth);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.LueYr);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Size = new System.Drawing.Size(772, 135);
            // 
            // BtnChart
            // 
            this.BtnChart.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnChart.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnChart.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnChart.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnChart.Appearance.Options.UseBackColor = true;
            this.BtnChart.Appearance.Options.UseFont = true;
            this.BtnChart.Appearance.Options.UseForeColor = true;
            this.BtnChart.Appearance.Options.UseTextOptions = true;
            this.BtnChart.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // Grd1
            // 
            this.Grd1.DefaultAutoGroupRow.Height = 21;
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.FrozenArea.ColCount = 3;
            this.Grd1.FrozenArea.SortFrozenRows = true;
            this.Grd1.GridLines.Mode = TenTec.Windows.iGridLib.iGGridLinesMode.None;
            this.Grd1.GroupBox.BackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintBackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Grd1.GroupBox.Visible = true;
            this.Grd1.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.UseXPStyles = false;
            this.Grd1.ScrollBarSettings.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.SearchAsType.Mode = TenTec.Windows.iGridLib.iGSearchAsTypeMode.Seek;
            this.Grd1.SearchAsType.SearchCol = null;
            this.Grd1.Size = new System.Drawing.Size(772, 338);
            this.Grd1.TabIndex = 39;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.VScrollBar.CustomButtons.AddRange(new TenTec.Windows.iGridLib.iGScrollBarCustomButton[] {
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.ExpandAll, -1, null, true, null),
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.CollapseAll, -1, null, true, null)});
            this.Grd1.VScrollBar.Visibility = TenTec.Windows.iGridLib.iGScrollBarVisibility.Always;
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel3
            // 
            this.panel3.Location = new System.Drawing.Point(0, 135);
            this.panel3.Size = new System.Drawing.Size(772, 338);
            // 
            // LueMth
            // 
            this.LueMth.EnterMoveNextControl = true;
            this.LueMth.Location = new System.Drawing.Point(78, 23);
            this.LueMth.Margin = new System.Windows.Forms.Padding(5);
            this.LueMth.Name = "LueMth";
            this.LueMth.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMth.Properties.Appearance.Options.UseFont = true;
            this.LueMth.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMth.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueMth.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMth.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueMth.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMth.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueMth.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMth.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueMth.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueMth.Properties.DropDownRows = 13;
            this.LueMth.Properties.NullText = "[Empty]";
            this.LueMth.Properties.PopupWidth = 500;
            this.LueMth.Size = new System.Drawing.Size(75, 20);
            this.LueMth.TabIndex = 11;
            this.LueMth.ToolTip = "F4 : Show/hide list";
            this.LueMth.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(35, 27);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 14);
            this.label1.TabIndex = 10;
            this.label1.Text = "Month";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueYr
            // 
            this.LueYr.EnterMoveNextControl = true;
            this.LueYr.Location = new System.Drawing.Point(78, 2);
            this.LueYr.Margin = new System.Windows.Forms.Padding(5);
            this.LueYr.Name = "LueYr";
            this.LueYr.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueYr.Properties.Appearance.Options.UseFont = true;
            this.LueYr.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueYr.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueYr.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueYr.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueYr.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueYr.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueYr.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueYr.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueYr.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueYr.Properties.DropDownRows = 12;
            this.LueYr.Properties.NullText = "[Empty]";
            this.LueYr.Properties.PopupWidth = 500;
            this.LueYr.Size = new System.Drawing.Size(75, 20);
            this.LueYr.TabIndex = 9;
            this.LueYr.ToolTip = "F4 : Show/hide list";
            this.LueYr.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(45, 5);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(32, 14);
            this.label4.TabIndex = 8;
            this.label4.Text = "Year";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(5, 68);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 14);
            this.label2.TabIndex = 14;
            this.label2.Text = "Profit / Loss";
            // 
            // TxtProfitLoss
            // 
            this.TxtProfitLoss.EnterMoveNextControl = true;
            this.TxtProfitLoss.Location = new System.Drawing.Point(78, 66);
            this.TxtProfitLoss.Margin = new System.Windows.Forms.Padding(5);
            this.TxtProfitLoss.Name = "TxtProfitLoss";
            this.TxtProfitLoss.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtProfitLoss.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtProfitLoss.Properties.Appearance.Options.UseBackColor = true;
            this.TxtProfitLoss.Properties.Appearance.Options.UseFont = true;
            this.TxtProfitLoss.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtProfitLoss.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtProfitLoss.Properties.MaxLength = 16;
            this.TxtProfitLoss.Properties.ReadOnly = true;
            this.TxtProfitLoss.Size = new System.Drawing.Size(174, 20);
            this.TxtProfitLoss.TabIndex = 15;
            // 
            // LblEntCode
            // 
            this.LblEntCode.AutoSize = true;
            this.LblEntCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblEntCode.ForeColor = System.Drawing.Color.Black;
            this.LblEntCode.Location = new System.Drawing.Point(36, 90);
            this.LblEntCode.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblEntCode.Name = "LblEntCode";
            this.LblEntCode.Size = new System.Drawing.Size(39, 14);
            this.LblEntCode.TabIndex = 16;
            this.LblEntCode.Text = "Entity";
            this.LblEntCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueEntCode
            // 
            this.LueEntCode.EnterMoveNextControl = true;
            this.LueEntCode.Location = new System.Drawing.Point(78, 87);
            this.LueEntCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueEntCode.Name = "LueEntCode";
            this.LueEntCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEntCode.Properties.Appearance.Options.UseFont = true;
            this.LueEntCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEntCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueEntCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEntCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueEntCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEntCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueEntCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEntCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueEntCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueEntCode.Properties.DropDownRows = 30;
            this.LueEntCode.Properties.NullText = "[Empty]";
            this.LueEntCode.Properties.PopupWidth = 300;
            this.LueEntCode.Size = new System.Drawing.Size(286, 20);
            this.LueEntCode.TabIndex = 17;
            this.LueEntCode.ToolTip = "F4 : Show/hide list";
            this.LueEntCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueEntCode.EditValueChanged += new System.EventHandler(this.LueEntCode_EditValueChanged);
            // 
            // LueStartFrom
            // 
            this.LueStartFrom.EnterMoveNextControl = true;
            this.LueStartFrom.Location = new System.Drawing.Point(78, 44);
            this.LueStartFrom.Margin = new System.Windows.Forms.Padding(5);
            this.LueStartFrom.Name = "LueStartFrom";
            this.LueStartFrom.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStartFrom.Properties.Appearance.Options.UseFont = true;
            this.LueStartFrom.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStartFrom.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueStartFrom.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStartFrom.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueStartFrom.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStartFrom.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueStartFrom.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStartFrom.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueStartFrom.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueStartFrom.Properties.DropDownRows = 12;
            this.LueStartFrom.Properties.NullText = "[Empty]";
            this.LueStartFrom.Properties.PopupWidth = 100;
            this.LueStartFrom.Size = new System.Drawing.Size(75, 20);
            this.LueStartFrom.TabIndex = 13;
            this.LueStartFrom.ToolTip = "F4 : Show/hide list";
            this.LueStartFrom.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(11, 47);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 14);
            this.label3.TabIndex = 12;
            this.label3.Text = "Start from";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // PnlPeriod
            // 
            this.PnlPeriod.Controls.Add(this.label5);
            this.PnlPeriod.Controls.Add(this.label6);
            this.PnlPeriod.Controls.Add(this.ChkDocDt);
            this.PnlPeriod.Controls.Add(this.DteDocDt2);
            this.PnlPeriod.Controls.Add(this.DteDocDt1);
            this.PnlPeriod.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.PnlPeriod.Location = new System.Drawing.Point(0, 102);
            this.PnlPeriod.Name = "PnlPeriod";
            this.PnlPeriod.Size = new System.Drawing.Size(375, 29);
            this.PnlPeriod.TabIndex = 23;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(222, 8);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(11, 14);
            this.label5.TabIndex = 36;
            this.label5.Text = "-";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(77, 8);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(33, 14);
            this.label6.TabIndex = 34;
            this.label6.Text = "Date";
            // 
            // ChkDocDt
            // 
            this.ChkDocDt.Location = new System.Drawing.Point(348, 4);
            this.ChkDocDt.Name = "ChkDocDt";
            this.ChkDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkDocDt.Properties.Appearance.Options.UseFont = true;
            this.ChkDocDt.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkDocDt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkDocDt.Properties.Caption = " ";
            this.ChkDocDt.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkDocDt.Size = new System.Drawing.Size(17, 22);
            this.ChkDocDt.TabIndex = 38;
            this.ChkDocDt.ToolTip = "Remove filter";
            this.ChkDocDt.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkDocDt.ToolTipTitle = "Run System";
            this.ChkDocDt.CheckedChanged += new System.EventHandler(this.ChkDocDt_CheckedChanged);
            // 
            // DteDocDt2
            // 
            this.DteDocDt2.EditValue = null;
            this.DteDocDt2.EnterMoveNextControl = true;
            this.DteDocDt2.Location = new System.Drawing.Point(237, 5);
            this.DteDocDt2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt2.Name = "DteDocDt2";
            this.DteDocDt2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt2.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt2.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt2.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt2.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt2.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt2.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt2.Size = new System.Drawing.Size(105, 20);
            this.DteDocDt2.TabIndex = 37;
            this.DteDocDt2.EditValueChanged += new System.EventHandler(this.DteDocDt2_EditValueChanged);
            // 
            // DteDocDt1
            // 
            this.DteDocDt1.EditValue = null;
            this.DteDocDt1.EnterMoveNextControl = true;
            this.DteDocDt1.Location = new System.Drawing.Point(114, 5);
            this.DteDocDt1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt1.Name = "DteDocDt1";
            this.DteDocDt1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt1.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt1.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt1.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt1.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt1.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt1.Size = new System.Drawing.Size(105, 20);
            this.DteDocDt1.TabIndex = 35;
            this.DteDocDt1.EditValueChanged += new System.EventHandler(this.DteDocDt1_EditValueChanged);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.ChkProfitCenterCode);
            this.panel4.Controls.Add(this.CcbProfitCenterCode);
            this.panel4.Controls.Add(this.LblMultiEntityCode);
            this.panel4.Controls.Add(this.LblLevel2);
            this.panel4.Controls.Add(this.LueLevelTo);
            this.panel4.Controls.Add(this.LueLevelFrom);
            this.panel4.Controls.Add(this.LblLevel);
            this.panel4.Controls.Add(this.PnlCCCode);
            this.panel4.Controls.Add(this.PnlPeriod);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(393, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(375, 131);
            this.panel4.TabIndex = 22;
            // 
            // ChkProfitCenterCode
            // 
            this.ChkProfitCenterCode.Location = new System.Drawing.Point(347, 25);
            this.ChkProfitCenterCode.Name = "ChkProfitCenterCode";
            this.ChkProfitCenterCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkProfitCenterCode.Properties.Appearance.Options.UseFont = true;
            this.ChkProfitCenterCode.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkProfitCenterCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkProfitCenterCode.Properties.Caption = " ";
            this.ChkProfitCenterCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkProfitCenterCode.Size = new System.Drawing.Size(18, 22);
            this.ChkProfitCenterCode.TabIndex = 29;
            this.ChkProfitCenterCode.ToolTip = "Remove filter";
            this.ChkProfitCenterCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkProfitCenterCode.ToolTipTitle = "Run System";
            this.ChkProfitCenterCode.CheckedChanged += new System.EventHandler(this.ChkProfitCenterCode_CheckedChanged);
            // 
            // CcbProfitCenterCode
            // 
            this.CcbProfitCenterCode.Location = new System.Drawing.Point(115, 26);
            this.CcbProfitCenterCode.Name = "CcbProfitCenterCode";
            this.CcbProfitCenterCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CcbProfitCenterCode.Properties.DropDownRows = 30;
            this.CcbProfitCenterCode.Size = new System.Drawing.Size(229, 20);
            this.CcbProfitCenterCode.TabIndex = 28;
            this.CcbProfitCenterCode.EditValueChanged += new System.EventHandler(this.CcbProfitCenterCode_EditValueChanged);
            // 
            // LblMultiEntityCode
            // 
            this.LblMultiEntityCode.AutoSize = true;
            this.LblMultiEntityCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblMultiEntityCode.ForeColor = System.Drawing.Color.Black;
            this.LblMultiEntityCode.Location = new System.Drawing.Point(6, 29);
            this.LblMultiEntityCode.Name = "LblMultiEntityCode";
            this.LblMultiEntityCode.Size = new System.Drawing.Size(106, 14);
            this.LblMultiEntityCode.TabIndex = 27;
            this.LblMultiEntityCode.Text = "Multi Profit Center";
            // 
            // LblLevel2
            // 
            this.LblLevel2.AutoSize = true;
            this.LblLevel2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblLevel2.ForeColor = System.Drawing.Color.Black;
            this.LblLevel2.Location = new System.Drawing.Point(219, 7);
            this.LblLevel2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblLevel2.Name = "LblLevel2";
            this.LblLevel2.Size = new System.Drawing.Size(19, 14);
            this.LblLevel2.TabIndex = 25;
            this.LblLevel2.Text = "to";
            this.LblLevel2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueLevelTo
            // 
            this.LueLevelTo.EnterMoveNextControl = true;
            this.LueLevelTo.Location = new System.Drawing.Point(239, 4);
            this.LueLevelTo.Margin = new System.Windows.Forms.Padding(5);
            this.LueLevelTo.Name = "LueLevelTo";
            this.LueLevelTo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevelTo.Properties.Appearance.Options.UseFont = true;
            this.LueLevelTo.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevelTo.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueLevelTo.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevelTo.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueLevelTo.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevelTo.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueLevelTo.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevelTo.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueLevelTo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueLevelTo.Properties.DropDownRows = 30;
            this.LueLevelTo.Properties.NullText = "[Empty]";
            this.LueLevelTo.Properties.PopupWidth = 300;
            this.LueLevelTo.Size = new System.Drawing.Size(105, 20);
            this.LueLevelTo.TabIndex = 26;
            this.LueLevelTo.ToolTip = "F4 : Show/hide list";
            this.LueLevelTo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueLevelTo.EditValueChanged += new System.EventHandler(this.LueLevelTo_EditValueChanged);
            // 
            // LueLevelFrom
            // 
            this.LueLevelFrom.EnterMoveNextControl = true;
            this.LueLevelFrom.Location = new System.Drawing.Point(114, 4);
            this.LueLevelFrom.Margin = new System.Windows.Forms.Padding(5);
            this.LueLevelFrom.Name = "LueLevelFrom";
            this.LueLevelFrom.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevelFrom.Properties.Appearance.Options.UseFont = true;
            this.LueLevelFrom.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevelFrom.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueLevelFrom.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevelFrom.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueLevelFrom.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevelFrom.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueLevelFrom.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevelFrom.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueLevelFrom.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueLevelFrom.Properties.DropDownRows = 30;
            this.LueLevelFrom.Properties.NullText = "[Empty]";
            this.LueLevelFrom.Properties.PopupWidth = 300;
            this.LueLevelFrom.Size = new System.Drawing.Size(105, 20);
            this.LueLevelFrom.TabIndex = 24;
            this.LueLevelFrom.ToolTip = "F4 : Show/hide list";
            this.LueLevelFrom.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueLevelFrom.EditValueChanged += new System.EventHandler(this.LueLevelFrom_EditValueChanged);
            // 
            // LblLevel
            // 
            this.LblLevel.AutoSize = true;
            this.LblLevel.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblLevel.ForeColor = System.Drawing.Color.Black;
            this.LblLevel.Location = new System.Drawing.Point(77, 8);
            this.LblLevel.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblLevel.Name = "LblLevel";
            this.LblLevel.Size = new System.Drawing.Size(35, 14);
            this.LblLevel.TabIndex = 23;
            this.LblLevel.Text = "Level";
            this.LblLevel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // PnlCCCode
            // 
            this.PnlCCCode.Controls.Add(this.ChkCCCode);
            this.PnlCCCode.Controls.Add(this.label7);
            this.PnlCCCode.Controls.Add(this.LueCCCode);
            this.PnlCCCode.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.PnlCCCode.Location = new System.Drawing.Point(0, 74);
            this.PnlCCCode.Name = "PnlCCCode";
            this.PnlCCCode.Size = new System.Drawing.Size(375, 28);
            this.PnlCCCode.TabIndex = 30;
            // 
            // ChkCCCode
            // 
            this.ChkCCCode.Location = new System.Drawing.Point(348, 2);
            this.ChkCCCode.Name = "ChkCCCode";
            this.ChkCCCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkCCCode.Properties.Appearance.Options.UseFont = true;
            this.ChkCCCode.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkCCCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkCCCode.Properties.Caption = " ";
            this.ChkCCCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkCCCode.Size = new System.Drawing.Size(18, 22);
            this.ChkCCCode.TabIndex = 33;
            this.ChkCCCode.ToolTip = "Remove filter";
            this.ChkCCCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkCCCode.ToolTipTitle = "Run System";
            this.ChkCCCode.CheckedChanged += new System.EventHandler(this.ChkCCCode_CheckedChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(40, 6);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(72, 14);
            this.label7.TabIndex = 31;
            this.label7.Text = "Cost Center";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueCCCode
            // 
            this.LueCCCode.EnterMoveNextControl = true;
            this.LueCCCode.Location = new System.Drawing.Point(114, 3);
            this.LueCCCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueCCCode.Name = "LueCCCode";
            this.LueCCCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCCCode.Properties.Appearance.Options.UseFont = true;
            this.LueCCCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCCCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCCCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCCCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCCCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCCCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCCCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCCCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCCCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCCCode.Properties.DropDownRows = 30;
            this.LueCCCode.Properties.NullText = "[Empty]";
            this.LueCCCode.Properties.PopupWidth = 250;
            this.LueCCCode.Size = new System.Drawing.Size(228, 20);
            this.LueCCCode.TabIndex = 32;
            this.LueCCCode.ToolTip = "F4 : Show/hide list";
            this.LueCCCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCCCode.EditValueChanged += new System.EventHandler(this.LueCCCode_EditValueChanged);
            // 
            // ChkEntCode
            // 
            this.ChkEntCode.Location = new System.Drawing.Point(368, 86);
            this.ChkEntCode.Name = "ChkEntCode";
            this.ChkEntCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkEntCode.Properties.Appearance.Options.UseFont = true;
            this.ChkEntCode.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkEntCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkEntCode.Properties.Caption = " ";
            this.ChkEntCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkEntCode.Size = new System.Drawing.Size(18, 22);
            this.ChkEntCode.TabIndex = 18;
            this.ChkEntCode.ToolTip = "Remove filter";
            this.ChkEntCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkEntCode.ToolTipTitle = "Run System";
            this.ChkEntCode.CheckedChanged += new System.EventHandler(this.ChkEntCode_CheckedChanged);
            // 
            // LuePeriod
            // 
            this.LuePeriod.EnterMoveNextControl = true;
            this.LuePeriod.Location = new System.Drawing.Point(78, 108);
            this.LuePeriod.Margin = new System.Windows.Forms.Padding(5);
            this.LuePeriod.Name = "LuePeriod";
            this.LuePeriod.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePeriod.Properties.Appearance.Options.UseFont = true;
            this.LuePeriod.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePeriod.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePeriod.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePeriod.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePeriod.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePeriod.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePeriod.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePeriod.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePeriod.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePeriod.Properties.DropDownRows = 21;
            this.LuePeriod.Properties.NullText = "[Empty]";
            this.LuePeriod.Properties.PopupWidth = 300;
            this.LuePeriod.Size = new System.Drawing.Size(286, 20);
            this.LuePeriod.TabIndex = 20;
            this.LuePeriod.ToolTip = "F4 : Show/hide list";
            this.LuePeriod.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LuePeriod.EditValueChanged += new System.EventHandler(this.LuePeriod_EditValueChanged);
            // 
            // LblPeriod
            // 
            this.LblPeriod.AutoSize = true;
            this.LblPeriod.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblPeriod.Location = new System.Drawing.Point(33, 111);
            this.LblPeriod.Name = "LblPeriod";
            this.LblPeriod.Size = new System.Drawing.Size(41, 14);
            this.LblPeriod.TabIndex = 19;
            this.LblPeriod.Text = "Period";
            // 
            // ChkPeriod
            // 
            this.ChkPeriod.Location = new System.Drawing.Point(368, 106);
            this.ChkPeriod.Name = "ChkPeriod";
            this.ChkPeriod.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkPeriod.Properties.Appearance.Options.UseFont = true;
            this.ChkPeriod.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkPeriod.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkPeriod.Properties.Caption = " ";
            this.ChkPeriod.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkPeriod.Size = new System.Drawing.Size(18, 22);
            this.ChkPeriod.TabIndex = 21;
            this.ChkPeriod.ToolTip = "Remove filter";
            this.ChkPeriod.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkPeriod.ToolTipTitle = "Run System";
            this.ChkPeriod.CheckedChanged += new System.EventHandler(this.ChkPeriod_CheckedChanged);
            // 
            // FrmRptProfitLoss3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 473);
            this.Name = "FrmRptProfitLoss3";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueMth.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueYr.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProfitLoss.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueEntCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueStartFrom.Properties)).EndInit();
            this.PnlPeriod.ResumeLayout(false);
            this.PnlPeriod.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt2.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt1.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt1.Properties)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkProfitCenterCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CcbProfitCenterCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueLevelTo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueLevelFrom.Properties)).EndInit();
            this.PnlCCCode.ResumeLayout(false);
            this.PnlCCCode.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCCCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCCCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkEntCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePeriod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkPeriod.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.LookUpEdit LueMth;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.LookUpEdit LueYr;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtProfitLoss;
        private System.Windows.Forms.Label LblEntCode;
        public DevExpress.XtraEditors.LookUpEdit LueEntCode;
        private DevExpress.XtraEditors.LookUpEdit LueStartFrom;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel PnlPeriod;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private DevExpress.XtraEditors.CheckEdit ChkDocDt;
        internal DevExpress.XtraEditors.DateEdit DteDocDt2;
        internal DevExpress.XtraEditors.DateEdit DteDocDt1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel PnlCCCode;
        private DevExpress.XtraEditors.CheckEdit ChkCCCode;
        private System.Windows.Forms.Label label7;
        private DevExpress.XtraEditors.LookUpEdit LueCCCode;
        private System.Windows.Forms.Label LblLevel2;
        public DevExpress.XtraEditors.LookUpEdit LueLevelTo;
        public DevExpress.XtraEditors.LookUpEdit LueLevelFrom;
        private System.Windows.Forms.Label LblLevel;
        private DevExpress.XtraEditors.CheckEdit ChkEntCode;
        private System.Windows.Forms.Label LblMultiEntityCode;
        private DevExpress.XtraEditors.CheckEdit ChkProfitCenterCode;
        private DevExpress.XtraEditors.CheckedComboBoxEdit CcbProfitCenterCode;
        private DevExpress.XtraEditors.LookUpEdit LuePeriod;
        private System.Windows.Forms.Label LblPeriod;
        private DevExpress.XtraEditors.CheckEdit ChkPeriod;

    }
}