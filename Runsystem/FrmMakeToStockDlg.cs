﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmMakeToStockDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmMakeToStock mFrmParent;
        private string
            mSQL = string.Empty,
            mBomDocNo = string.Empty;
        private decimal mIndex = 1m;

        #endregion

        #region Constructor

        public FrmMakeToStockDlg(FrmMakeToStock FrmParent, string BomDocNo)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mBomDocNo = BomDocNo;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = "List Of Item";
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                if (mFrmParent.mIsMTSNeedBom && mFrmParent.mQty!=0)
                    mIndex = decimal.Parse(mFrmParent.TxtQty.Text)/mFrmParent.mQty;
                SetGrd();
                SetSQL();
                Sl.SetLueItCtCode(ref LueItCtCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 15;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "", 
                        "Item's Code", 
                        "Local Code",
                        "", 
                        "Item's Name", 

                        //6-10
                        "Item's Category",
                        "Quantity",
                        "UoM",
                        "Quantity",
                        "UoM",

                        //11-14
                        "Bom Qty",
                        "Convert12",
                        "Specification",
                        "Customer's"+Environment.NewLine+"Item Code"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        20, 80, 100, 20, 300, 
                        
                        //6-10
                        250, 100, 80, 100, 80,
                        
                        //11-14
                        0, 0, 130, 130
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColButton(Grd1, new int[] { 4 });
            Sm.GrdFormatDec(Grd1, new int[] { 7, 9, 11, 12 }, 0);
            Sm.GrdColReadOnly(Grd1, new int[] { 2, 3, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 });
            Grd1.Cols[13].Move(6);
            Grd1.Cols[14].Move(7);
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 4, 11, 12, 13, 14 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 4, 13, 14 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            if (!mFrmParent.mIsMTSNeedBom)
            {
                SQL.AppendLine("Select '1' As DNo, A.ItCode, A.ItCodeInternal, A.ItCtCode, B.ItCtName, A.Specification, C.CtItCode, ");
                SQL.AppendLine("0 As Qty, A.PlanningUomCode, 0 As Qty2, A.PlanningUomCode2, ");
                if (mFrmParent.mItCtRMPActual.Length > 0 || mFrmParent.mItCtRMPActual2.Length > 0)
                {
                    SQL.AppendLine("IfNull(Concat(A.ItName, ' ( ', ");
                    SQL.AppendLine("        Case A.ItCtCode  ");
                    SQL.AppendLine("            When @ItCtRMPActual Then ");
                    SQL.AppendLine("                Concat( ");
                    SQL.AppendLine("                'L: ', A.Length, ");
                    SQL.AppendLine("                ', D: ', A.Diameter ");
                    SQL.AppendLine("                ) ");
                    SQL.AppendLine("            When @ItCtRMPActual2 Then  ");
                    SQL.AppendLine("                Concat(  ");
                    SQL.AppendLine("                'L: ', A.Length, ");
                    SQL.AppendLine("                ', W: ', A.Width,  ");
                    SQL.AppendLine("                ', H: ', A.Height ");
                    SQL.AppendLine("                ) ");
                    SQL.AppendLine("        End, ");
                    SQL.AppendLine("    ' )'), A.ItName) As ItName, ");
                }
                else
                {
                    SQL.AppendLine("A.ItName, ");
                }
                SQL.AppendLine("A.PlanningUomCodeConvert12 ");
                SQL.AppendLine("From TblItem A ");
                SQL.AppendLine("Inner Join TblItemCategory B On A.ItCtCode = B.ItCtCode ");
                SQL.AppendLine("LEFT JOIN TblCustomerItem C ON A.ItCode = C.ItCode ");
                SQL.AppendLine("Where A.PlanningItemInd = 'Y' ");
                SQL.AppendLine("And A.ActInd='Y' ");
                SQL.AppendLine("And Locate(Concat('##', A.ItCode, '##'), @SelectedItem)<1 ");
            }
            else
            {
                SQL.AppendLine("Select A.DNo, A.DocCode As ItCode, B.ItCodeInternal, B.Specification, D.CtItCode, ");
                SQL.AppendLine("B.ItCtCode, C.ItCtName, ");
                SQL.AppendLine("A.Qty, B.PlanningUomCode2 As PlanningUomCode,");
                SQL.AppendLine("0 As Qty2, Null As PlanningUomCode2, B.ItName, B.PlanningUomCodeConvert12 ");
                SQL.AppendLine("From TblBomDtl A ");
                SQL.AppendLine("Inner Join TblItem B On A.DocCode=B.ItCode And B.ActInd='Y' And B.PlanningItemInd='Y' ");
                SQL.AppendLine("Inner Join TblItemCategory C On B.ItCtCode=C.ItCtCode ");
                SQL.AppendLine("LEFT JOIN TblCustomerItem D ON B.ItCode = D.ItCode ");
                SQL.AppendLine("Where A.DocNo=@BomDocNo And A.DocType<>'3' ");
                SQL.AppendLine("And Locate(Concat('##', A.DocCode, '##'), @SelectedItem)<1 ");
            }

            mSQL = 
                "Select ItCode, ItCodeInternal, ItName, ItCtName, Specification, CtItCode, " +
                "Qty, PlanningUomCode, Qty2, PlanningUomCode2, PlanningUomCodeConvert12 " +
                "From (" + SQL.ToString() + ") T ";
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = string.Empty;

                var cm = new MySqlCommand();

                if (mBomDocNo.Length > 0) Sm.CmParam<String>(ref cm, "@BomDocNo", mBomDocNo);
                if (mFrmParent.mItCtRMPActual.Length > 0) Sm.CmParam<String>(ref cm, "@ItCtRMPActual", mFrmParent.mItCtRMPActual);
                if (mFrmParent.mItCtRMPActual2.Length > 0) Sm.CmParam<String>(ref cm, "@ItCtRMPActual2", mFrmParent.mItCtRMPActual2);
                Sm.CmParam<String>(ref cm, "@SelectedItem", mFrmParent.GetSelectedItem());
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "ItCode", "ItCodeInternal", "ItName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueItCtCode), "ItCtCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL.ToString() + Filter + " Order By DNo, ItName;",
                        new string[] 
                        { 
                            //0
                            "ItCode",
 
                            //1-5
                            "ItCodeInternal", "ItName", "ItCtName", "Qty", "PlanningUomCode",

                            //6-10
                            "Qty2", "PlanningUomCode2", "PlanningUomCodeConvert12", "Specification", "CtItCode"

                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 10);
                            Grd.Cells[Row, 7].Value = Sm.GetGrdDec(Grd, Row, 11) * mIndex;
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;
            var UsageDt = Sm.GetDte(mFrmParent.DteUsageDt);
            if (UsageDt.Length > 0) UsageDt = UsageDt.Substring(0, 8);
            if (Grd1.Rows.Count != 0)
            {
                Grd1.BeginUpdate();
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsItCodeAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 1, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 2, Grd1, Row2, 3);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 3, Grd1, Row2, 5);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 4, Grd1, Row2, 7);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 5, Grd1, Row2, 8);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 6, Grd1, Row2, 9);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 7, Grd1, Row2, 10);
                        if (UsageDt.Length > 0) mFrmParent.Grd1.Cells[Row1, 8].Value = Sm.ConvertDate(UsageDt);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 10, Grd1, Row2, 11);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 11, Grd1, Row2, 12);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 12, Grd1, Row2, 13);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 13, Grd1, Row2, 14);
                        mFrmParent.Grd1.Rows.Add();
                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 4, 6, 10, 11 });
                    }
                }
                Grd1.EndUpdate();
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 item.");
        }

        private bool IsItCodeAlreadyChosen(int Row)
        {
            var ItCode = Sm.GetGrdStr(Grd1, Row, 2);
            for (int Index = 0; Index < mFrmParent.Grd1.Rows.Count - 1; Index++)
                if (Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd1, Index, 1), ItCode)) return true;
            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event
        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LueItCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCtCode, new Sm.RefreshLue1(Sl.SetLueItCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void ChkItCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Item's category");
        }
        #endregion
    }
}
