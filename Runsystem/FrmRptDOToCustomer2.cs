﻿#region Update

/*
 *  18/08/2022 [SET/PRODUCT] Add column Sales Order#, Unit Price, Total Price & button untuk melihat SODoc
 *  25/08/2022 [SET/GSS] Penyesuaian source Unite Price
 *  08/09/2022 [SET/IOK] Penyesuain reporting untuk IOK
 *  10/10/2022 [SET/IOK] perubahan source Quantity
 */ 

#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptDOToCustomer2 : FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty;
        private bool mIsDOToCust2ShowAdditional = false;

        #endregion

        #region Constructor

        public FrmRptDOToCustomer2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                GetParameter();
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                Sl.SetLueCtCode(ref LueCtCode);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private string GetSQL(string Filter1, string Filter2)
        {

            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.* From (");
            SQL.AppendLine("    Select Concat(Left(A.DocDt, 4), '-', Right(Left(A.DocDt, 6), 2)) As Period, ");
            SQL.AppendLine("    B.ItCode, D.ItName, C.CtName, A.LocalDocNo, ");
            SQL.AppendLine("    Case When A.DRDocNo Is Not Null Then 'DR' Else 'PL' End As Source, ");
            //SQL.AppendLine("    A.DocDt, Sum(B.Qty) As Qty, D.InventoryUOMCode ");
            SQL.AppendLine("    A.DocDt, D.InventoryUOMCode, ");
            SQL.AppendLine("    Case ");
            SQL.AppendLine("    When A.DRDocNo Is Not Null ");
            SQL.AppendLine("    Then L.QtyInventory Else Sum(B.Qty) ");
            SQL.AppendLine("    End As Qty ");
            if (mIsDOToCust2ShowAdditional)
                SQL.AppendLine("    , H.DocNo SODocNo, K.UPrice ");
            else
                SQL.AppendLine("    , Null SODocNo, 0.00 As UPrice ");
            SQL.AppendLine("    From TblDOCt2Hdr A ");
            SQL.AppendLine("    Inner Join TblDOCt2Dtl B On A.DocNo=B.DocNo And B.CancelInd='N' ");
            SQL.AppendLine("    Inner Join TblCustomer C on A.CtCode=C.CtCode ");
            SQL.AppendLine("    Inner Join TblItem D on B.ItCode=D.ItCode " + Filter2);
            SQL.AppendLine("    Left JOIN tbldrhdr E ON A.DRDocNo = E.DocNo ");
            SQL.AppendLine("    Left JOIN tbldrdtl F ON E.DocNo = F.DocNo ");
            SQL.AppendLine("    Left JOIN tblsodtl G ON F.SODocNo = G.DocNo AND F.SODNo = G.DNo ");
            SQL.AppendLine("    Left JOIN tblsohdr H ON G.DocNo = H.DocNo ");
            if (mIsDOToCust2ShowAdditional)
            {
                SQL.AppendLine("    Left JOIN tblctqtdtl I ON H.CtQtDocNo = I.DocNo AND G.CtQtDNo = I.DNo ");
                SQL.AppendLine("    Left JOIN tblitempricedtl K ON I.ItemPriceDocNo = K.DocNo AND I.ItemPriceDNo = K.DNo AND B.ItCode = K.ItCode ");
            }
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        SELECT L1.DocNo, L7.ItCode, SUM(L2.QtyInventory) AS QtyInventory ");
            SQL.AppendLine("        FROM TblDRhdr L1 ");
            SQL.AppendLine("        INNER JOIN TblDRDtl L2 On L1.DocNo = L2.DocNo ");
            SQL.AppendLine("        INNER JOIN TblSOHdr L3 On L2.SODocNo=L3.DocNo ");
            SQL.AppendLine("        INNER JOIN TblSODtl L4 On L2.SODocNo=L4.DocNo And L2.SODNo=L4.DNo ");
            SQL.AppendLine("        INNER JOIN TblCtQtDtl L5 On L3.CtQtDocNo=L5.DocNo And L4.CtQtDNo=L5.DNo ");
            SQL.AppendLine("        INNER JOIN TblItemPriceDtl L6 On L5.ItemPriceDocNo=L6.DocNo And L5.ItemPriceDNo=L6.DNo ");
            SQL.AppendLine("        INNER JOIN TblItem L7 On L6.ItCode=L7.ItCode ");
            SQL.AppendLine("        GROUP BY L1.DocNo, L7.ItCode ");
            SQL.AppendLine("    ) L ON F.DocNo = L.DocNo AND B.ItCode = L.ItCode ");
            SQL.AppendLine("    Where (A.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine(Filter1);
            SQL.AppendLine("    Group By Concat(Left(A.DocDt, 4), '-', Right(Left(A.DocDt, 6), 2)), ");
            SQL.AppendLine("    B.ItCode, D.ItName, C.CtName, A.LocalDocNo, ");
            SQL.AppendLine("    Case When A.DrDocno Is Not Null Then 'DR' Else 'PL' End, ");
            SQL.AppendLine("    A.DocDt, D.InventoryUOMCode ");
            SQL.AppendLine(") T Order By T.Period, T.CtName, T.ItName;");

            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 14;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5      
                        "Period", 
                        "Item's"+Environment.NewLine+"Code",
                        "Item's"+Environment.NewLine+"Name",
                        "Customer",
                        "Local#",

                        //6-10
                        "Type",
                        "Date",
                        "Quantity",
                        "UoM",
                        "Sales Order#",

                        //11-13
                        " ",
                        "Unit Price",
                        "Total Price"
                    },
                    new int[]
                {
                    50, 
                    80, 80, 250, 200, 130,
                    80, 80, 130, 80, 200,
                    20, 150, 250
                }
                );
            Sm.GrdColButton(Grd1, new int[] { 11 });
            Sm.GrdFormatDec(Grd1, new int[] { 8, 12, 13 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 7 });
            Sm.GrdColInvisible(Grd1, new int[] { 2 });
            if (!mIsDOToCust2ShowAdditional)
                Sm.GrdColInvisible(Grd1, new int[] { 10, 11, 12, 13 });
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 12, 13 });
            Grd1.Cols[10].Move(6);
            Grd1.Cols[11].Move(7);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
        }

        private void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'IsDOToCust2ShowAdditional' );");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsDOToCust2ShowAdditional": mIsDOToCust2ShowAdditional = ParValue == "Y"; break;

                            //string
                            //case "ProcFormatDocNo": IsProcFormat = ParValue; break;
                        }
                    }
                }
                dr.Close();
            }

        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();

                string Filter1 = " ", Filter2 = " ";


                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter1, ref cm, Sm.GetLue(LueCtCode), "A.CtCode", true);
                Sm.FilterStr(ref Filter2, ref cm, TxtItCode.Text, new string[] { "D.ItCode", "D.ItName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, GetSQL(Filter1, Filter2),
                        new string[]
                        {
                            //0
                            "Period",

                            //1-5
                            "ItCode", "ItName", "CtName", "LocalDocNo", "Source", 
                            
                            //6-10
                            "DocDt", "Qty", "InventoryUOMCode", "SODocNo", "UPrice"
                            
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 10);
                            Grd1.Cells[Row, 13].Value = Sm.GetGrdDec(Grd1, Row, 8) * Sm.GetGrdDec(Grd1, Row, 12);
                        }, true, true, false, false
                    );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[]{ 8 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            Grd1.Rows.AutoHeight();
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 11 && Sm.GetGrdStr(Grd1, e.RowIndex, 10).Length != 0)
            {
                var f = new FrmSO2(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 10);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 11 && Sm.GetGrdStr(Grd1, e.RowIndex, 10).Length != 0)
            {
                var f = new FrmSO2(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 10);
                f.ShowDialog();
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        #endregion

    }
}
