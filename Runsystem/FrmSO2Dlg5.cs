﻿#region Update
/*
 *  14/02/2019 [DITA] Feedback : Saat INSERT SO, setelah pilih List of Customer's Quotation, saat pilih Shipping Information, Shipping Information-nya sesuai City yang dipilih di Customer's Quotation-nya
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmSO2Dlg5 : RunSystem.FrmBase4
    {
        #region Field

        private FrmSO2 mFrmParent;
        private string mSQL = string.Empty, mCtCode= string.Empty, mCtQtDocNo= string.Empty;

        #endregion

        #region Constructor

        public FrmSO2Dlg5(FrmSO2 FrmParent, string CtCode, string CtQtDocNo)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mCtCode = CtCode;
            mCtQtDocNo = CtQtDocNo;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 13;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "DNo",
                        "Name",
                        "Address",
                        "City",
                        "City",

                        //6-10
                        "Country",
                        "Country",
                        "Postal Code",
                        "Phone", 
                        "Fax",

                        //11-12
                        "Email",
                        "Mobile"                    
                    }
                );
            Sm.GrdColInvisible(Grd1, new int[] { 1, 4, 6 });
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.Name, A.Address, ");
            SQL.AppendLine("A.CityCode, B.CityName, A.CntCode, C.CntName, A.PostalCd, ");
            SQL.AppendLine("A.Phone, A.Fax, A.Email, A.Mobile ");
            SQL.AppendLine("From TblCustomerShipAddress A ");
            SQL.AppendLine("Left Join TblCity B On A.CityCode=B.CityCode ");
            SQL.AppendLine("Left Join TblCountry C On A.CntCode=C.CntCode ");
            SQL.AppendLine("Where A.CtCode=@CtCode ");
            if (mFrmParent.mIsFilterCity && mCtQtDocNo.Length > 0)
            {
                SQL.AppendLine(" And A.CityCode Is Not Null ");
                SQL.AppendLine(" And A.CityCOde In( ");
                SQL.AppendLine(" Select CityCode From TblCtQtDtl2 Where DocNo = @DocNo) ");
            }
            
            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@CtCode", mCtCode);
                Sm.CmParam<String>(ref cm, "@DocNo", mCtQtDocNo);
                Sm.FilterStr(ref Filter, ref cm, TxtName.Text, "A.Name", false);
                Sm.FilterStr(ref Filter, ref cm, TxtCityName.Text, "B.CityName", false);
                
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + "Order By A.Name;",
                        new string[] 
                        { 
                             //0
                             "DNo", 
                             
                             //1-5
                             "Name", 
                             "Address", 
                             "CityCode", 
                             "CityName", 
                             "CntCode", 
            
                             //6-10
                             "CntName", 
                             "PostalCD", 
                             "Phone", 
                             "Fax", 
                             "Email", 
                             
                             //11
                             "Mobile"    },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                int Row = Grd1.CurRow.Index;

                mFrmParent.mSADNo = Sm.GetGrdStr(Grd1, Row, 1);
                mFrmParent.TxtSAName.EditValue = Sm.GetGrdStr(Grd1, Row, 2);
                mFrmParent.TxtAddress.EditValue = Sm.GetGrdStr(Grd1, Row, 3);
                mFrmParent.mCity = Sm.GetGrdStr(Grd1, Row, 4);
                mFrmParent.TxtCity.EditValue = Sm.GetGrdStr(Grd1, Row, 5);
                mFrmParent.mCnt = Sm.GetGrdStr(Grd1, Row, 6);
                mFrmParent.TxtCountry.EditValue = Sm.GetGrdStr(Grd1, Row, 7);
                mFrmParent.TxtPostalCd.EditValue = Sm.GetGrdStr(Grd1, Row, 8);
                mFrmParent.TxtPhone.EditValue = Sm.GetGrdStr(Grd1, Row, 9);
                mFrmParent.TxtFax.EditValue = Sm.GetGrdStr(Grd1, Row, 10);
                mFrmParent.TxtEmail.EditValue = Sm.GetGrdStr(Grd1, Row, 11);
                mFrmParent.TxtMobile.EditValue = Sm.GetGrdStr(Grd1, Row, 12);

                this.Close();
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmProductionOrder(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }

            if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmSO2(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 7);
                f.ShowDialog();
            }

            if (e.ColIndex == 10 && Sm.GetGrdStr(Grd1, e.RowIndex, 9).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 9);
                f.ShowDialog();
            }

            if (e.ColIndex == 15 && Sm.GetGrdStr(Grd1, e.RowIndex, 14).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmProductionRouting(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 14);
                f.ShowDialog();
            }

            if (e.ColIndex == 15 && Sm.GetGrdStr(Grd1, e.RowIndex, 12).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmProductionRouting(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 12);
                f.ShowDialog();
            }

            if (e.ColIndex == 17 && Sm.GetGrdStr(Grd1, e.RowIndex, 16).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmWorkCenter(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 16);
                f.ShowDialog();
            }

            if (e.ColIndex == 24 && Sm.GetGrdStr(Grd1, e.RowIndex, 23).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmProductionRouting(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 23);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmProductionOrder(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }

            if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length != 0)
            {
                var f = new FrmSO2(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 7);
                f.ShowDialog();
            }

            if (e.ColIndex == 10 && Sm.GetGrdStr(Grd1, e.RowIndex, 9).Length != 0)
            {
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 9);
                f.ShowDialog();
            }

            if (e.ColIndex == 15 && Sm.GetGrdStr(Grd1, e.RowIndex, 14).Length != 0)
            {
                var f = new FrmProductionRouting(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 14);
                f.ShowDialog();
            }

            if (e.ColIndex == 15 && Sm.GetGrdStr(Grd1, e.RowIndex, 12).Length != 0)
            {
                var f = new FrmProductionRouting(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 12);
                f.ShowDialog();
            }

            if (e.ColIndex == 17 && Sm.GetGrdStr(Grd1, e.RowIndex, 16).Length != 0)
            {
                var f = new FrmWorkCenter(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 16);
                f.ShowDialog();
            }

            if (e.ColIndex == 24 && Sm.GetGrdStr(Grd1, e.RowIndex, 23).Length != 0)
            {
                var f = new FrmProductionRouting(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 23);
                f.ShowDialog();
            }
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtName_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Name");
        }

        private void TxtCityName_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkCityName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "City");
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        #endregion

        #endregion

    }
}
