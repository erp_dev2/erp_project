﻿#region Update
/*
    20/03/2018 [TKG] new application
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

#endregion

namespace RunSystem
{
    public partial class FrmProcessGroupFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmProcessGroup mFrmParent;

        #endregion

        #region Constructor

        public FrmProcessGroupFind(FrmProcessGroup FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 10;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Code", 
                        "Name",
                        "Parent",
                        "Created"+Environment.NewLine+"By",   
                        "Created"+Environment.NewLine+"Date", 
                        
                        //6-9
                        "Created"+Environment.NewLine+"Time", 
                        "Last"+Environment.NewLine+"Updated By",
                        "Last"+Environment.NewLine+"Updated Date",
                        "Last"+Environment.NewLine+"Updated Time"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        100, 200, 200, 100, 100, 
                        
                        //6-10
                        100, 100, 100, 100
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 5, 8 });
            Sm.GrdFormatTime(Grd1, new int[] { 6, 9 });
            Sm.GrdColInvisible(Grd1, new int[] { 4, 5, 6, 7, 8, 9 }, false);
            Sm.SetGrdProperty(Grd1, false);
            Grd1.Rows.AutoHeight();
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4, 5, 6, 7, 8, 9 }, !ChkHideInfoInGrd.Checked);
        }


        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;
                var SQL = new StringBuilder();
                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtPGCode.Text, new string[] { "A.PGCode", "A.PGName" });
                
                SQL.AppendLine("Select A.PGCode, A.PGName, A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
                SQL.AppendLine("From TblProcessGroup A ");
                SQL.AppendLine("Left Join TblProcessGroup B on A.PGCode=B.PGCode ");
                SQL.AppendLine(Filter + "Order By A.PGName;");

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, SQL.ToString(),
                        new string[]
                        {
                            //0
                            "PGCode",

                            //1-5
                            "PGName",
                            "CreateBy",
                            "CreateDt", 
                            "LastUpBy", 
                            "LastUpDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 8, 5);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtPGCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkPGCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Process group");
        }

        #endregion

        #endregion
    }
}
