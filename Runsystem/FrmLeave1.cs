﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmLeave1 : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmLeaveFind FrmFind;

        #endregion

        #region Constructor

        public FrmLeave1(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetFormControl(mState.View);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtLeaveCode, TxtLeaveName, LueLGCode, TxtNoOfDay, TxtNoOfDayExpired, MeeRemark
                    }, true);
                    ChkActInd.Properties.ReadOnly = true;
                    ChkPaidInd.Properties.ReadOnly = true;
                    TxtLeaveCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtLeaveCode, TxtLeaveName, LueLGCode, TxtNoOfDay, TxtNoOfDayExpired, MeeRemark
                    }, false);
                    ChkPaidInd.Properties.ReadOnly = false;
                    TxtLeaveCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtLeaveName, LueLGCode, TxtNoOfDay, TxtNoOfDayExpired, MeeRemark
                    }, false);
                    ChkActInd.Properties.ReadOnly = false;
                    ChkPaidInd.Properties.ReadOnly = false;
                    TxtLeaveName.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            { TxtLeaveCode, TxtLeaveName, LueLGCode, MeeRemark });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtNoOfDay, TxtNoOfDayExpired }, 0);
            ChkActInd.Checked = false;
            ChkPaidInd.Checked = false;
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmLeaveFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                SetLueLGCode(ref LueLGCode, string.Empty);
                ChkActInd.Checked = true;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtLeaveCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtLeaveCode, "", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblLeave Where LeaveCode=@LeaveCode;" };
                Sm.CmParam<String>(ref cm, "@LeaveCode", TxtLeaveCode.Text);
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into TblLeave(LeaveCode, LeaveName, LGCode, ActInd, PaidInd, NoOfDay, NoOfDayExpired, Remark, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@LeaveCode, @LeaveName, @LGCode, @ActInd, @PaidInd, @NoOfDay, @NoOfDayExpired, @Remark, @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("   Update LeaveName=@LeaveName, LGCode=@LGCode, ActInd=@ActInd, PaidInd=@PaidInd, NoOfDay=@NoOfDay, NoOfDayExpired=@NoOfDayExpired, Remark=@Remark, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@LeaveCode", TxtLeaveCode.Text);
                Sm.CmParam<String>(ref cm, "@LeaveName", TxtLeaveName.Text);
                Sm.CmParam<String>(ref cm, "@LGCode", Sm.GetLue(LueLGCode));
                Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked?"Y":"N");
                Sm.CmParam<String>(ref cm, "@PaidInd", ChkPaidInd.Checked ? "Y" : "N");
                Sm.CmParam<Decimal>(ref cm, "@NoOfDay", decimal.Parse(TxtNoOfDay.Text));
                Sm.CmParam<Decimal>(ref cm, "@NoOfDayExpired", decimal.Parse(TxtNoOfDayExpired.Text));
                Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.ExecCommand(cm);

                ShowData(TxtLeaveCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string LeaveCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@LeaveCode", LeaveCode);
                Sm.ShowDataInCtrl(
                        ref cm,
                        "Select LeaveCode, LeaveName, LGCode, ActInd, PaidInd, NoOfDay, NoOfDayExpired, Remark " +
                        "From TblLeave Where LeaveCode=@LeaveCode;",
                        new string[] 
                        {
                            //0
                            "LeaveCode", 
                            //1-5
                            "LeaveName", "LGCode", "ActInd", "PaidInd", "NoOfDay", 
                            //6-7
                            "NoOfDayExpired", "Remark" 
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtLeaveCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtLeaveName.EditValue = Sm.DrStr(dr, c[1]);
                            SetLueLGCode(ref LueLGCode, Sm.DrStr(dr, c[2]));
                            Sm.SetLue(LueLGCode, Sm.DrStr(dr, c[2]));
                            ChkActInd.Checked = Sm.DrStr(dr, c[3]) == "Y";
                            ChkPaidInd.Checked = Sm.DrStr(dr, c[4]) == "Y";
                            TxtNoOfDay.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[5]), 0);
                            TxtNoOfDayExpired.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[6]), 0);
                            MeeRemark.EditValue = Sm.DrStr(dr, c[7]);
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtLeaveCode, "Leave code", false) ||
                Sm.IsTxtEmpty(TxtLeaveName, "Leave name", false) ||
                Sm.IsLueEmpty(LueLGCode, "Leave group") ||
                Sm.IsTxtEmpty(TxtNoOfDay, "Number of days", true) ||
                IsLeaveCodeExisted();
        }

        private bool IsLeaveCodeExisted()
        {
            if (!TxtLeaveCode.Properties.ReadOnly && Sm.IsDataExist("Select LeaveCode From TblLeave Where LeaveCode='" + TxtLeaveCode.Text + "' Limit 1"))
            {
                Sm.StdMsg(mMsgType.Warning, "Leave code ( " + TxtLeaveCode.Text + " ) already existed.");
                return true;
            }
            return false;
        }

        #endregion

        #region Additional Method

        private void SetLueLGCode(ref DXE.LookUpEdit Lue, string LGCode)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select LGCode As Col1, LGName As Col2 ");
                SQL.AppendLine("From TblLeaveGrp Where ActInd='Y' ");

                if (LGCode.Length != 0)
                {
                    SQL.AppendLine("Union All ");
                    SQL.AppendLine("Select LGCode As Col1, LGName As Col2 From TblLeaveGrp Where LGCode='" + LGCode + "' ");
                }
                SQL.AppendLine("Order By Col2;");
                Sm.SetLue2(
                    ref Lue, SQL.ToString(),
                    0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtLeaveCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtLeaveCode);
        }

        private void TxtLeaveName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtLeaveName);
        }

        private void TxtNoOfDay_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtNoOfDay, 0);
        }

        private void TxtNoOfDayExpired_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtNoOfDayExpired, 0);
        }

        private void LueLGCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueLGCode, new Sm.RefreshLue2(SetLueLGCode), string.Empty);

        }

        #endregion

        #endregion

    }
}
