﻿#region Update
/*
    25/08/2021 [TKG/AMKA] New reporting
    15/09/2021 [IBL/AMKA] Kolom UM Pemasok dan UM Subkon digabung.
    15/09/2021 [IBL/AMKA] Tambah amount dari opening balance
    24/09/2021 [WED/AMKA] kalkulasi amount journal berdasarkan parameter COACalculationFormula
                          Profit Center dibuat mandatory
    05/10/2021 [DITA/AMKA] desainer untuk bagian button ngebug UI nya karna sebelumnya di deploy di VS 2012
    14/10/2021 [IBL/AMKA] Kolom PENYISIHAN HUTANG dibuat menjadi positif. (Pengaruh dari kalkulasi yg melihat actype parent nya)
    10/11/2021 [SET/AMKA] Membuat filter profit center di header reporting Turn Over Finance Analysis terfilter berdasarkan group
    07/02/2022 [TRI/AMKA] menampilkan subtotal untuk kolom 14-19
    28/03/2022 [RIS/AMKA] Menambahkan fied CR AR I AP
    30/03/2022 [RIS/AMKA] Membuat printout
    05/04/2022 [RIS/AMKA] BUG : Memunculkan semua kolom pada printout
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using DXE = DevExpress.XtraEditors;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DevExpress.XtraEditors;
using System.Collections;

#endregion

namespace RunSystem
{
    public partial class FrmRptTurnOverFinanceAnalysis : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;
        private List<String> mlProfitCenter;
        private bool mIsAllProfitCenterSelected = false,
            mIsFilterByProfitCenter = false;

        #endregion

        #region Constructor

        public FrmRptTurnOverFinanceAnalysis(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method
        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                mlProfitCenter = new List<String>();
                GetParameter();
                SetCcbProfitCenterCode(ref CcbProfitCenterCode);
                ChkProfitCenterCode.Visible = false;
                var CurrentDateTime = Sm.ServerCurrentDateTime();
                Sl.SetLueYr(LueYr, string.Empty);
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                Sl.SetLueMth(LueMth);
                Sm.SetLue(LueMth, CurrentDateTime.Substring(4, 2));
                SetGrd();
                Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                        TxtTotalI, TxtTotalAP, TxtTotalAR, TxtTotalCR
                    }, true);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 20;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",
                        
                        //1-5
                        "CODE",
                        "NAME", 
                        "PENJUALAN"+Environment.NewLine+"BULAN INI",
                        "PENJUALAN S.D."+Environment.NewLine+"BULAN INI",
                        "PENJUALAN"+Environment.NewLine+"DISETAHUNKAN",
                        
                        //6-10
                        "PIUTANG"+Environment.NewLine+"USAHA",
                        "PIUTANG"+Environment.NewLine+"RETENSI",
                        "TAGIHAN"+Environment.NewLine+"BRUTO",
                        "PERSEDIAAN",
                        "HUTANG"+Environment.NewLine+"USAHA",
                        
                        //11-15
                        "DPP MATERIAL"+Environment.NewLine+"1 TAHUN",
                        "BPP"+Environment.NewLine+"1 TAHUN",
                        "UM PEMASOK "+Environment.NewLine+"DAN UM SUBKONT",
                        "UM OWNER",
                        "PENYISIHAN PIUTANG",

                        //16-19
                        "CR TO",
                        "AR TO",
                        "I TO",
                        "AP TO"
                    });
            Sm.GrdFormatDec(Grd1, new int[] { 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 0 }, false);
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 0 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdProperty(Grd1, true);
        }

        #endregion

        #region Standard Method

        private void GetParameter()
        {
            mIsFilterByProfitCenter = Sm.GetParameterBoo("IsFilterByProfitCenter");
        }

        private string GetSQL(string Alias, string subSQL1, string Filter, string subSQL2, string AcNo, bool IsNeedOpeningBalance)
        {
            var SQL = new StringBuilder();
            //string AcType = Sm.GetValue("Select AcType From TblCoa Where AcNo = @Param; ", AcNo);

            if (IsNeedOpeningBalance)
            {
                SQL.AppendLine("Left Join (");
                SQL.AppendLine("    Select ProfitCenterCode, ");
                SQL.AppendLine("    Sum( ");
                //SQL.AppendLine("        Case ");
                //SQL.AppendLine("            When T.AcType = '" + AcType + "' Then T.Amt ");
                //SQL.AppendLine("            Else T.Amt*-1 ");
                //SQL.AppendLine("        End ");
                SQL.AppendLine("        T.Amt ");
                SQL.AppendLine("    ) As Amt ");
                SQL.AppendLine("    From (");
                SQL.AppendLine("        Select T3.ProfitCenterCode, ");
                //SQL.AppendLine("        T4.AcType, ");
                //SQL.AppendLine("        Sum(Case When T4.AcType='D' Then T2.DAmt-T2.CAmt Else T2.CAmt-T2.DAmt End) As Amt ");
                SQL.AppendLine("        Sum( ");
                SQL.AppendLine("            Case When T6.ParValue='2' Then  ");
                SQL.AppendLine("                Case When T5.AcType='D' Then T2.DAmt-T2.CAmt Else T2.CAmt-T2.DAmt End ");
                SQL.AppendLine("            Else ");
                SQL.AppendLine("                Case When T4.AcType='D' Then T2.DAmt-T2.CAmt Else T2.CAmt-T2.DAmt End ");
                SQL.AppendLine("            End ");
                SQL.AppendLine("        ) As Amt ");
                SQL.AppendLine("        From TblJournalHdr T1 ");
                SQL.AppendLine("        Inner Join TblJournalDtl T2 On T1.DocNo = T2.DocNo ");
                SQL.AppendLine("            And T1.CCCode Is Not Null ");
                SQL.AppendLine("        Inner Join TblCostCenter T3 On T1.CCCode = T3.CCCode ");
                SQL.AppendLine("            And T3.ProfitCenterCode Is Not Null ");
                SQL.AppendLine("        Inner Join TblCOA T4 On T2.AcNo = T4.AcNo ");
                SQL.AppendLine("            And T4.AcNo Not In (Select Distinct Parent From TblCOA Where Parent Is Not Null) ");
                SQL.AppendLine("        Inner Join TblCOA T5 On T5.Parent Is Null And T5.Level = 1 And Left(T2.AcNo, Length(T5.AcNo)) = T5.AcNo ");
                SQL.AppendLine("        Left Join TblParameter T6 On T6.ParCode = 'COACalculationFormula' ");
                SQL.AppendLine("        Where 0 = 0 ");
                
                SQL.AppendLine(subSQL1 + Filter);
                SQL.AppendLine(subSQL2.Replace("A.", "T3."));
                SQL.AppendLine("        Group By T3.ProfitCenterCode ");
                //SQL.AppendLine("        , T2.AcNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("        Select T1.ProfitCenterCode, Sum(T2.Amt) As Amt ");
                SQL.AppendLine("        From TblCOAOpeningBalanceHdr T1 ");
                SQL.AppendLine("        Inner Join TblCOAOpeningBalanceDtl T2 On T1.DocNo = T2.DocNo ");
                SQL.AppendLine("            And T1.ProfitCenterCode Is Not Null ");
                SQL.AppendLine("            And T1.CancelInd='N' ");
                SQL.AppendLine("            And T1.Yr=@Yr ");
                SQL.AppendLine("        Inner Join TblCOA T3 On T2.AcNo = T3.AcNo ");
                SQL.AppendLine("            And T3.AcNo Not In (Select Distinct Parent From TblCOA Where Parent Is Not Null) ");
                SQL.AppendLine("        Where 0 = 0 ");
                SQL.AppendLine(subSQL1);
                SQL.AppendLine(subSQL2.Replace("A.", "T1."));
                SQL.AppendLine("        Group By T1.ProfitCenterCode ");
                SQL.AppendLine("    ) T Group By ProfitCenterCode ");
                SQL.AppendLine(") " + Alias + " On A.ProfitCenterCode=" + Alias + ".ProfitCenterCode ");
            }
            else
            {
                SQL.AppendLine("Left Join (");
                SQL.AppendLine("    Select T3.ProfitCenterCode, ");
                //SQL.AppendLine("    Sum(Case When T4.AcType='D' Then T2.DAmt-T2.CAmt Else T2.CAmt-T2.DAmt End) As Amt ");
                SQL.AppendLine("    Sum( ");
                SQL.AppendLine("        Case When T6.ParValue='2' Then  ");
                SQL.AppendLine("            Case When T5.AcType='D' Then T2.DAmt-T2.CAmt Else T2.CAmt-T2.DAmt End ");
                SQL.AppendLine("        Else ");
                SQL.AppendLine("            Case When T4.AcType='D' Then T2.DAmt-T2.CAmt Else T2.CAmt-T2.DAmt End ");
                SQL.AppendLine("        End ");
                SQL.AppendLine("    ) As Amt ");
                SQL.AppendLine("    From TblJournalHdr T1 ");
                SQL.AppendLine("    Inner Join TblJournalDtl T2 On T1.DocNo = T2.DocNo ");
                SQL.AppendLine("        And T1.CCCode Is Not Null ");
                SQL.AppendLine("    Inner Join TblCostCenter T3 On T1.CCCode = T3.CCCode ");
                SQL.AppendLine("        And T3.ProfitCenterCode Is Not Null ");
                SQL.AppendLine("    Inner Join TblCOA T4 On T2.AcNo = T4.AcNo ");
                SQL.AppendLine("        And T4.AcNo Not In (Select Parent From TblCOA Where Parent Is Not Null) ");
                SQL.AppendLine("    Inner Join TblCOA T5 On T5.Parent Is Null And T5.Level = 1 And Left(T2.AcNo, Length(T5.AcNo)) = T5.AcNo ");
                SQL.AppendLine("    Left Join TblParameter T6 On T6.ParCode = 'COACalculationFormula' ");
                SQL.AppendLine("    Where 0 = 0 ");
                SQL.AppendLine(subSQL1 + Filter);
                SQL.AppendLine(subSQL2.Replace("A.", "T3."));
                SQL.AppendLine("    Group By T3.ProfitCenterCode ");
                SQL.AppendLine(") " + Alias + " On A.ProfitCenterCode=" + Alias + ".ProfitCenterCode ");
            }
            
            return SQL.ToString();
        }

        private string GetSQL()
        { 
            var SQL = new StringBuilder();
            var SQL2 = new StringBuilder();

            var Filter1 = " And Left(T1.DocDt, 6)=@YrMth "; // hanya bulan ini saja
            var Filter2 = " And T1.DocDt>=@Dt1 And T1.DocDt<@Dt2 "; // awal tahun sampai bulan ini
            
            if (!mIsAllProfitCenterSelected)
            {
                var Filter = string.Empty;
                int i = 0;
                foreach (var x in mlProfitCenter.Distinct())
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += " (A.ProfitCenterCode=@ProfitCenter_" + i.ToString() + ") ";
                    i++;
                }
                if (Filter.Length == 0)
                    SQL2.AppendLine("    And 1=0 ");
                else
                    SQL2.AppendLine("    And (" + Filter + ") ");
            }
            else
            {
                if (ChkProfitCenterCode.Checked) SQL2.AppendLine("    And Find_In_Set(A.ProfitCenterCode, @ProfitCenterCode) ");
            }

            SQL.AppendLine("Select A.ProfitCenterCode, A.ProfitCenterName, ");
            SQL.AppendLine("IfNull(B.Amt, 0.00) As Value1, ");
            SQL.AppendLine("IfNull(C.Amt, 0.00) As Value2, ");
            SQL.AppendLine("IfNull(D.Amt, 0.00) As Value3, ");
            SQL.AppendLine("IfNull(E.Amt, 0.00) As Value4, ");
            SQL.AppendLine("IfNull(F.Amt, 0.00) As Value5, ");
            SQL.AppendLine("IfNull(G.Amt, 0.00) As Value6, ");
            SQL.AppendLine("IfNull(H.Amt, 0.00) As Value7, ");
            SQL.AppendLine("IfNull(I.Amt, 0.00) As Value8, ");
            SQL.AppendLine("IfNull(J.Amt, 0.00) As Value9, ");
            SQL.AppendLine("IfNull(K.Amt, 0.00) As Value10, ");
            SQL.AppendLine("IfNull(L.Amt, 0.00) As Value11, ");
            //Dikali (-1) : Yg dimau value nya positif, disini hasil yg didapat pasti negatif krna kalkulasi ngelihat actype parent yg mana = D. tp COA ini typenya C.
            //Jadi hasilnya pasti negatif. Maka dari itu dikali -1.
            SQL.AppendLine("Case ");
            SQL.AppendLine("    When IfNull(M.Amt, 0.00) < 0 Then IfNull(M.Amt, 0.00) * -1 ");
            SQL.AppendLine("    Else IfNull(M.Amt, 0.00) ");
            SQL.AppendLine("End As Value12 ");
            SQL.AppendLine("From TblProfitCenter A ");
            SQL.AppendLine(GetSQL("B", " And T2.AcNo Like '4%' ", Filter1, SQL2.ToString(), "", false));
            SQL.AppendLine(GetSQL("C", " And T2.AcNo Like '4%' ", Filter2, SQL2.ToString(), "4", true));
            SQL.AppendLine(GetSQL("D", " And T2.AcNo Like '1.1.3.1%' ", Filter1, SQL2.ToString(), "", false));
            SQL.AppendLine(GetSQL("E", " And T2.AcNo Like '1.1.3.2%' ", Filter1, SQL2.ToString(), "", false));
            SQL.AppendLine(GetSQL("F", " And T2.AcNo Like '1.1.3.3%' ", Filter1, SQL2.ToString(), "", false));
            SQL.AppendLine(GetSQL("G", " And T2.AcNo Like '1.1.4%' ", Filter1, SQL2.ToString(), "", false));
            SQL.AppendLine(GetSQL("H", " And T2.AcNo Like '2.1.1%' ", Filter1, SQL2.ToString(), "", false));
            SQL.AppendLine(GetSQL("I", " And T2.AcNo Like '5.1%' ", Filter2, SQL2.ToString(), "", false));
            SQL.AppendLine(GetSQL("J", " And T2.AcNo Like '5%' ", Filter2, SQL2.ToString(), "", false));
            SQL.AppendLine(GetSQL("K", " And T2.AcNo Like '1.1.5.1%' ", Filter1, SQL2.ToString(), "", false));
            SQL.AppendLine(GetSQL("L", " And T2.AcNo Like '2.1.5.1%' ", Filter1, SQL2.ToString(), "", false));
            SQL.AppendLine(GetSQL("M", " And (T2.AcNo Like '1.1.3.1.3%' Or T2.AcNo Like '1.1.3.2.3%') ", Filter1, SQL2.ToString(), "", false));
            SQL.AppendLine("Where 1=1 ");
            SQL.AppendLine(SQL2.ToString());
          
            return SQL.ToString();
        }

        #endregion

        #region Show Data

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (IsProfitCenterInvalid() || Sm.IsLueEmpty(LueYr, "Year") || Sm.IsLueEmpty(LueMth, "Month")) return;

            Cursor.Current = Cursors.WaitCursor;
            
            var cm = new MySqlCommand();
            decimal Mth = decimal.Parse(Sm.GetLue(LueMth));
            string Yr = Sm.GetLue(LueYr);
            string Dt1 = string.Concat(Yr, "0101");
            string YrMth = string.Concat(Yr, Sm.GetLue(LueMth));
            string Dt2 = Sm.Left(Sm.FormatDate(Sm.ConvertDate(string.Concat(YrMth, "01")).AddMonths(1)), 8);
            int Row1 = 0;
            
            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            Sm.CmParam<String>(ref cm, "@YrMth", YrMth); // bulan ini
            Sm.CmParamDt(ref cm, "@Dt1", Dt1); // 1 jan 
            Sm.CmParamDt(ref cm, "@Dt2", Dt2); // 1 bulan berikutnya

            try
            {
                SetProfitCenter();

                if (!mIsAllProfitCenterSelected)
                {
                    int i = 0;
                    foreach (var x in mlProfitCenter.Distinct())
                    {
                        Sm.CmParam<String>(ref cm, "@ProfitCenter_" + i.ToString(), x);
                        i++;
                    }
                }
                else
                {
                    if (ChkProfitCenterCode.Checked) Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                }

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, GetSQL(),
                    new string[]
                    {
                        //0
                        "ProfitCenterCode",

                        //1-5
                        "ProfitCenterName", "Value1", "Value2", "Value3", "Value4", 

                        //6-10
                        "Value5", "Value6", "Value7", "Value8", "Value9", 

                        //11-13
                        "Value10", "Value11", "Value12",
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                        Grd.Cells[Row, 5].Value = (Sm.GetGrdDec(Grd, Row, 4)*12m)/Mth;
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 4);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 5);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 6);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 7);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 8);
                        Grd.Cells[Row, 11].Value = (dr.GetDecimal(c[9])*12m)/Mth;
                        Grd.Cells[Row, 12].Value = (dr.GetDecimal(c[10])*12m)/Mth;
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 11);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 12);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 13);
                        if (Sm.GetGrdDec(Grd, Row, 5) == 0m)
                            Grd.Cells[Row, 16].Value = 0m;
                        else
                            Grd.Cells[Row, 16].Value = ((Sm.GetGrdDec(Grd, Row, 6) + Sm.GetGrdDec(Grd, Row, 7) + Sm.GetGrdDec(Grd, Row, 8)) / Sm.GetGrdDec(Grd, Row, 5))*365m;
                        if (Sm.GetGrdDec(Grd, Row, 5) == 0m)
                            Grd.Cells[Row, 17].Value = 0m;
                        else
                            Grd.Cells[Row, 17].Value = (Sm.GetGrdDec(Grd, Row, 6) / Sm.GetGrdDec(Grd, Row, 5)) * 365m;
                        if (Sm.GetGrdDec(Grd, Row, 11) == 0m)
                            Grd.Cells[Row, 18].Value = 0m;
                        else
                            Grd.Cells[Row, 18].Value = (Sm.GetGrdDec(Grd, Row, 9) / Sm.GetGrdDec(Grd, Row, 11)) * 365m;
                        if (Sm.GetGrdDec(Grd, Row, 12) == 0m)
                            Grd.Cells[Row, 19].Value = 0m;
                        else
                            Grd.Cells[Row, 19].Value = (Sm.GetGrdDec(Grd, Row, 10) / Sm.GetGrdDec(Grd, Row, 12)) * 365m;

                        Row1 = Row+1;
                    }, true, false, false, true
                );
                Grd1.BeginUpdate();
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 });
                if (Sm.GetGrdDec(Grd1, Row1, 5) == 0m)
                    TxtTotalCR.EditValue = 0m;
                else
                    TxtTotalCR.EditValue = Sm.FormatNum(((Sm.GetGrdDec(Grd1, Row1, 6) + Sm.GetGrdDec(Grd1, Row1, 7) + Sm.GetGrdDec(Grd1, Row1, 8)) / Sm.GetGrdDec(Grd1, Row1, 5)) * 365m, 0);
                if (Sm.GetGrdDec(Grd1, Row1, 5) == 0m)
                    TxtTotalAR.EditValue = 0m;
                else
                    TxtTotalAR.EditValue = Sm.FormatNum((Sm.GetGrdDec(Grd1, Row1, 6) / Sm.GetGrdDec(Grd1, Row1, 5)) * 365m, 0);
                if (Sm.GetGrdDec(Grd1, Row1, 11) == 0m)
                    TxtTotalI.EditValue = 0m;
                else
                    TxtTotalI.EditValue = Sm.FormatNum((Sm.GetGrdDec(Grd1, Row1, 9) / Sm.GetGrdDec(Grd1, Row1, 11)) * 365m, 0);
                if (Sm.GetGrdDec(Grd1, Row1, 12) == 0m)
                    TxtTotalAP.EditValue = 0m;
                else
                    TxtTotalAP.EditValue = Sm.FormatNum((Sm.GetGrdDec(Grd1, Row1, 10) / Sm.GetGrdDec(Grd1, Row1, 12)) * 365m, 0);
                Grd1.EndUpdate();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void PrintData()
        {
            if (Grd1.Rows.Count == 0)
            {
                Sm.StdMsg(mMsgType.NoData, string.Empty);
                return;
            }

            ParPrint();
        }

        #endregion

        #region Additional Method

        private bool IsProfitCenterInvalid()
        {
            if (Sm.GetCcb(CcbProfitCenterCode).Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Profit center is empty.");
                CcbProfitCenterCode.Focus();
                return true;
            }

            return false;
        }

        private void SetProfitCenter()
        {
            mIsAllProfitCenterSelected = false;
            if (!ChkProfitCenterCode.Checked) mIsAllProfitCenterSelected = true;
            if (mIsAllProfitCenterSelected) return;

            bool IsCompleted = false, IsFirst = true;

            mlProfitCenter.Clear();

            while (!IsCompleted)
                SetProfitCenter(ref IsFirst, ref IsCompleted);
        }

        private void SetProfitCenter(ref bool IsFirst, ref bool IsCompleted)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string Filter = string.Empty, ProfitCenterCode = string.Empty;
            bool IsExisted = false;
            int i = 0;

            SQL.AppendLine("Select Distinct ProfitCenterCode From TblProfitCenter ");
            if (IsFirst)
            {
                if (ChkProfitCenterCode.Checked)
                {
                    SQL.AppendLine("    Where Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ");
                    Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                    IsCompleted = false;
                }
                else
                    IsCompleted = true;
                IsFirst = false;
            }
            else
            {
                SQL.AppendLine("    Where Parent Is Not Null ");
                foreach (var x in mlProfitCenter.Distinct())
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += " (Parent=@ProfitCenterCode" + i.ToString() + ") ";

                    Sm.CmParam<String>(ref cm, "@ProfitCenterCode" + i.ToString(), x);
                    i++;
                }
                if (Filter.Length == 0)
                    SQL.AppendLine("    And 1=0 ");
                else
                {
                    SQL.AppendLine("    And (" + Filter + ") ");
                }
                IsCompleted = true;
            }
            SQL.AppendLine("Order By ProfitCenterCode;");

            cm.CommandText = SQL.ToString();

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ProfitCenterCode" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        if (IsFirst)
                            mlProfitCenter.Add(ProfitCenterCode);
                        else
                        {
                            ProfitCenterCode = Sm.DrStr(dr, c[0]);
                            IsExisted = false;
                            foreach (var x in mlProfitCenter.Where(w => Sm.CompareStr(w, ProfitCenterCode)))
                                IsExisted = true;
                            if (!IsExisted)
                            {
                                mlProfitCenter.Add(ProfitCenterCode);
                                IsCompleted = false;
                            }
                        }
                    }
                }
                else
                    IsCompleted = true;
                dr.Close();
            }
        }

        private void SetCcbProfitCenterCode(ref CheckedComboBoxEdit Ccb)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Col, Col2 From (");
            SQL.AppendLine("    Select T.ProfitCenterName As Col, T.ProfitCenterCode As Col2 ");
            SQL.AppendLine("    From TblProfitCenter T ");
            if (mIsFilterByProfitCenter)
            {
                SQL.AppendLine("WHERE Exists(  ");
                SQL.AppendLine("        Select 1 From TblGroupProfitCenter ");
                SQL.AppendLine("        Where ProfitCenterCode=T.ProfitCenterCode  ");
                SQL.AppendLine("        And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine(") Tbl Order By Col2; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetCcb(ref Ccb, cm);
        }

        private string GetCcbProfitCenterCode()
        {
            var Value = Sm.GetCcb(CcbProfitCenterCode);
            if (Value.Length == 0) return string.Empty;
            return GetProfitCenterCode(Value).Replace(", ", ",");
        }

        private string ProcessCcbProfitCenterCode(string Value)
        {
            if (Value.Length == 0) return string.Empty;
            return ("#" + GetProfitCenterCode(Value).Replace(", ", "# #") + "#").Replace("#", @"""");
        }

        private string GetProfitCenterCode(string Value)
        {
            if (Value.Length == 0) return string.Empty;

            return
                Sm.GetValue(
                    "Select Group_Concat(T.Code Separator ', ') As Code " +
                    "From (Select ProfitCenterCode As Code From TblProfitCenter Where Find_In_Set(ProfitCenterName, @Param) ) T; ",
                    Value.Replace(", ", ","));
        }

        private void ParPrint()
        {
            string mDocTitle = Sm.GetValue("Select ParValue From tblparameter Where ParCode='DocTitle'");

            var l = new List<RptTurnOverFinanceAnalysis>();
            var lh = new List<Hdr>();
            string[] TableName = { "RptTurnOverFinanceAnalysis", "Hdr" };
            var myLists = new List<IList>();

            #region Header

            lh.Add(new Hdr()
            {
                CompanyLogo = Sm.GetValue("SELECT Distinct @Param As CompanyLogo; ", Sm.CompanyLogo()),
                TotalCR = decimal.Parse(TxtTotalCR.Text),
                TotalAR = decimal.Parse(TxtTotalAR.Text),
                TotalI = decimal.Parse(TxtTotalI.Text),
                TotalAP = decimal.Parse(TxtTotalAP.Text)
            });

            #endregion

            #region Detail

            for (int i = 0; i < Grd1.Rows.Count; ++i)
            {
                if (Sm.GetGrdStr(Grd1, i, 1).Length > 0)
                {

                    l.Add(new RptTurnOverFinanceAnalysis()
                    {
                        Code = Sm.GetGrdStr(Grd1, i, 1),
                        Name = Sm.GetGrdStr(Grd1, i, 2),
                        MonthSales = Sm.GetGrdDec(Grd1, i, 3),
                        UpMonthSales = Sm.GetGrdDec(Grd1, i, 4),
                        YearSales = Sm.GetGrdDec(Grd1, i, 5),
                        PUsaha = Sm.GetGrdDec(Grd1, i, 6),
                        PRetensi = Sm.GetGrdDec(Grd1, i, 7),
                        Bruto = Sm.GetGrdDec(Grd1, i, 8),
                        Stock = Sm.GetGrdDec(Grd1, i, 9),
                        HUsaha = Sm.GetGrdDec(Grd1, i, 10),
                        DPPMaterial = Sm.GetGrdDec(Grd1, i, 11),
                        BPP = Sm.GetGrdDec(Grd1, i, 12),
                        UMPemasokSubKont = Sm.GetGrdDec(Grd1, i, 13),
                        UMOwner = Sm.GetGrdDec(Grd1, i, 14),
                        PPiutang = Sm.GetGrdDec(Grd1, i, 15),
                        CRTO = Sm.GetGrdDec(Grd1, i, 16),
                        ARTO = Sm.GetGrdDec(Grd1, i, 17),
                        ITO = Sm.GetGrdDec(Grd1, i, 18),
                        APTO = Sm.GetGrdDec(Grd1, i, 19),
                    });
                }
            }

            #endregion

            myLists.Add(l);
            myLists.Add(lh);

            Sm.PrintReport("RptTurnOverFinanceAnalysis", myLists, TableName, false);
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void CcbProfitCenterCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterCcbSetCheckEdit(this, sender);
        }

        private void ChkProfitCenterCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetCheckedComboBoxEdit(this, sender, "Profit center");
        }

        #endregion

        #endregion

        #region Class

        private class RptTurnOverFinanceAnalysis 
        {
            public string Code { get; set; }
            public string Name { get; set; }
            public decimal MonthSales { get; set; }
            public decimal UpMonthSales { get; set; }
            public decimal YearSales { get; set; }
            public decimal PUsaha { get; set; }
            public decimal PRetensi { get; set; }
            public decimal Bruto { get; set; }
            public decimal Stock { get; set; }
            public decimal HUsaha { get; set; }
            public decimal DPPMaterial { get; set; }
            public decimal BPP { get; set; }
            public decimal UMPemasokSubKont { get; set; }
            public decimal UMOwner { get; set; }
            public decimal PPiutang { get; set; }
            public decimal CRTO { get; set; }
            public decimal ARTO { get; set; }
            public decimal ITO { get; set; }
            public decimal APTO { get; set; }
        }

        private class Hdr
        {
            public string CompanyLogo { get; set; }
            public decimal TotalCR { get; set; }
            public decimal TotalAR { get; set; }
            public decimal TotalI { get; set; } 
            public decimal TotalAP { get; set; }
        }

        #endregion
    }
}
