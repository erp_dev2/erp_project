﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRawMaterialOpname2 : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty,
            mDocNo = string.Empty; //if this application is called from other application;
        internal FrmRawMaterialOpname2Find FrmFind;
        decimal mRMInspDefaultQueueDt = 0m;

        #endregion

        #region Constructor

        public FrmRawMaterialOpname2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Raw Material Quantity Inspection";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                SetLueDocType(ref LueDocType);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }

                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 6;
            Grd1.FrozenArea.ColCount = 3;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                         //0
                        "DNo",

                        //1-5
                        "Bin",
                        "Quantity",
                        "Description",
                        "Remark",
                        "Label"+Environment.NewLine+"Quantity"
                    },
                     new int[] 
                    {
                        //0
                        0, 

                        //1-5
                        80, 80, 200, 200, 80
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 2, 5 }, 1);
            Sm.GrdColInvisible(Grd1, new int[] { 0 }, false);
            SetGrdCanvas();
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        ChkCancelInd, DteDocDt, LueQueueNo, LueDocType, MeeRemark
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5 });
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueQueueNo, LueDocType, MeeRemark
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1, 2, 4, 5 });
                    DteDocDt.Focus();
                    break;

                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ MeeRemark }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1, 2, 4, 5 });
                    ChkCancelInd.Properties.ReadOnly = false;
                    ChkCancelInd.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, LueQueueNo, LueDocType, TxtCreateBy, 
                MeeRemark
            });
            ChkCancelInd.Checked = false;
            ClearGrd();
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 2, 5 });
            Sm.FocusGrd(Grd1, 0, 1);
        }
        
        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmRawMaterialOpname2Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                SetLueQueueNo(ref LueQueueNo, "");
                SetUserName();
             }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "RawMaterialOpname2", "TblRawMaterialOpname2Hdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveRawMaterialOpname2Hdr(DocNo));
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0) cml.Add(SaveRawMaterialOpname2Dtl(DocNo, Row));
           
            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueQueueNo, "Queue#") ||
                Sm.IsLueEmpty(LueDocType, "Type") ||
                IsDocTypeNotValid() ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                IsGrdExceedMaxRecords() ||
                IsBinNotValid()
                ;
        }

        private bool IsBinNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 1).Length>0 && IsBinNotValid(Row))
                {
                    Sm.StdMsg(mMsgType.Warning,
                        "Bin : " + Sm.GetGrdStr(Grd1, Row, 1) + Environment.NewLine +
                        "This bin already inspected in another document.");
                    return true;
                }
            }
            return false;
        }


        private bool IsBinNotValid(int Row)
        {
            var SQL = new StringBuilder();
            
            SQL.AppendLine("Select A.QueueNo ");
            SQL.AppendLine("From TblRawMaterialOpname2Hdr A, TblRawMaterialOpname2Dtl B ");
            SQL.AppendLine("Where A.CancelInd='N' ");
            SQL.AppendLine("And A.QueueNo=@QueueNo ");
            if (TxtDocNo.Text.Length > 0)
                SQL.AppendLine("And A.DocNo<>@DocNo ");
            SQL.AppendLine("And A.DocNo=B.DocNo ");
            SQL.AppendLine("And B.Shelf=@Bin; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@QueueNo", Sm.GetLue(LueQueueNo));
            Sm.CmParam<String>(ref cm, "@Bin", Sm.GetGrdStr(Grd1, Row, 1));
            if (TxtDocNo.Text.Length>0)
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            return Sm.IsDataExist(cm);
        }


        private bool IsDocTypeNotValid()
        {
            return Sm.StdMsgYN("Question", 
                "Type : " + LueDocType.GetColumnValue("Col2") + Environment.NewLine +
                "Apakah tipe dokumen sudah benar ?"
                ) == DialogResult.No; 
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "Data jumlah bahan baku masih kosong.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, Row, 1, false, "Bin still empty.") ||
                    Sm.IsGrdValueEmpty(Grd1, Row, 2, true, "Quantity should not be 0.") 
                    )
                    return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 999)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Data jumlah bahan baku (" + (Grd1.Rows.Count - 1).ToString() + ") melebihi jumlah maksimal yang ditentukan (999).");
                return true;
            }
            return false;
        }

        private MySqlCommand SaveRawMaterialOpname2Hdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblRawMaterialOpname2Hdr(DocNo, DocDt, DocType, CancelInd, QueueNo, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @DocType, 'N', @QueueNo, @Remark, @CreateBy, CurrentDateTime()); ");
            
            var cm = new MySqlCommand(){ CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DocType", Sm.GetLue(LueDocType));
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@QueueNo", Sm.GetLue(LueQueueNo));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveRawMaterialOpname2Dtl(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblRawMaterialOpname2Dtl(DocNo, DNo, Shelf, Qty, Remark, LabelQty, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DNo, @Shelf, @Qty, @Remark, @LabelQty, @CreateBy, CurrentDateTime()); "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@Shelf", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 2));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 4));
            Sm.CmParam<Decimal>(ref cm, "@LabelQty", Sm.GetGrdDec(Grd1, Row, 5));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsCancelledDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditRawMaterialOpname2Hdr());
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0) cml.Add(SaveRawMaterialOpname2Dtl(TxtDocNo.Text, Row));
           
            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsCancelledDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDataCancelledAlready() ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                IsGrdExceedMaxRecords() ||
                IsBinNotValid()
                ;
        }

        private bool IsDataCancelledAlready()
        {
            var cm = new MySqlCommand() 
            { 
                CommandText =  
                    "Select DocNo From TblRawMaterialOpname2Hdr " +
                    "Where CancelInd='Y' And DocNo=@DocNo; "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "Data already cancelled.");
                return true;
            }

            return false;
        }

        private MySqlCommand EditRawMaterialOpname2Hdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblRawMaterialOpname2Hdr Set ");
            SQL.AppendLine("    CancelInd=@CancelInd, Remark=@Remark, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo; ");

            SQL.AppendLine("Delete From TblRawMaterialOpname2Dtl Where DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowRawMaterialOpname2Hdr(DocNo);
                ShowRawMaterialOpname2Dtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowRawMaterialOpname2Hdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocType, A.DocDt, A.CancelInd, A.QueueNo, B.UserName, A.Remark ");
            SQL.AppendLine("From TblRawMaterialOpname2Hdr A ");
            SQL.AppendLine("Inner Join TblUser B On A.CreateBy=B.UserCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "CancelInd", "QueueNo", "DocType", "UserName", 
                        
                        //6
                        "Remark"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y");
                        SetLueQueueNo(ref LueQueueNo, Sm.DrStr(dr, c[3]));
                        Sm.SetLue(LueQueueNo, Sm.DrStr(dr, c[3]));
                        Sm.SetLue(LueDocType, Sm.DrStr(dr, c[4]));
                        TxtCreateBy.EditValue = Sm.DrStr(dr, c[5]);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[6]);
                    }, true
                );
        }

        private void ShowRawMaterialOpname2Dtl(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, 
                "Select DNo, Shelf, Qty, Remark, LabelQty From TblRawMaterialOpname2Dtl Where DocNo=@DocNo Order By DNo;",
                new string[]{ "DNo", "Shelf", "Qty", "Remark", "LabelQty" },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("I", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("I", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("I", Grd, dr, c, Row, 5, 4);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 2, 3, 5 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mRMInspDefaultQueueDt = Sm.GetParameterDec("RMInspDefaultQueueDt");
        }

        private void SetLueDocType(ref DXE.LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select '1' As Col1, 'Log' As Col2 ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select '2' As Col1, 'Balok' As Col2 ");

            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void SetLueQueueNo(ref DXE.LookUpEdit Lue, string QueueNo)
        {
            try
            {
                var SQL = new StringBuilder();
                var Dt = Sm.ConvertDate(Sm.ServerCurrentDateTime()).AddDays((double)mRMInspDefaultQueueDt);

                SQL.AppendLine("Select DocNo As Col1, DocNo As Col2 From TblLoadingQueue Where ");
                if (QueueNo.Length == 0)
                    SQL.AppendLine("Left(CreateDt, 8)>@Dt;");
                else
                    SQL.AppendLine("DocNo=@QueueNo;");
                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                if (QueueNo.Length == 0)
                    Sm.CmParamDt(ref cm, "@Dt", Sm.FormatDate(Dt));
                else
                    Sm.CmParam<String>(ref cm, "@QueueNo", QueueNo);
                Sm.SetLue2(ref Lue, ref cm, 0, 50, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetGrdCanvas()
        {
            iGCellStyle CanvasStyle = new iGCellStyle();
            CanvasStyle.CustomDrawFlags = TenTec.Windows.iGridLib.iGCustomDrawFlags.Foreground;
            CanvasStyle.Flags = ((TenTec.Windows.iGridLib.iGCellFlags)(TenTec.Windows.iGridLib.iGCellFlags.DisplayImage));

            Grd1.Cols[3].CellStyle =CanvasStyle;
        }

        private void SetUserName()
        {
            var cm = new MySqlCommand(){ CommandText = "Select UserName From TblUser Where UserCode=@UserCode;" };
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            TxtCreateBy.EditValue = Sm.GetValue(cm);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 1, 2, 4, 5 }, e.ColIndex))
            {
                Sm.GrdRequestEdit(Grd1, e.RowIndex);
                Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 2, 3, 5 });
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            
            if (e.KeyCode == Keys.Enter && Grd1.CurRow.Index != Grd1.Rows.Count - 1)
            {
                if (Grd1.CurCell.Col.Index == 1)
                    Sm.FocusGrd(Grd1, Grd1.CurRow.Index, 2);
                else
                {
                    if (Grd1.CurCell.Col.Index == 2)
                        Sm.FocusGrd(Grd1, Grd1.CurRow.Index, 4);
                    else
                    {
                        if (Grd1.CurCell.Col.Index == 4)
                            Sm.FocusGrd(Grd1, Grd1.CurRow.Index, 5);
                        else
                        {
                            if (Grd1.CurCell.Col.Index == 5)
                                Sm.FocusGrd(Grd1, Grd1.CurRow.Index + 1, 1);
                        }
                    }
                }
            }

            if (e.KeyCode == Keys.Tab && Grd1.CurCell != null && Grd1.CurCell.RowIndex == Grd1.Rows.Count - 1)
            {
                int LastVisibleCol = Grd1.CurCell.ColIndex;
                for (int Col = LastVisibleCol; Col <= Grd1.Cols.Count - 1; Col++)
                    if (Grd1.Cols[Col].Visible) LastVisibleCol = Col;

                if (Grd1.CurCell.Col.Order == LastVisibleCol)
                {
                    if (BtnFind.Enabled)
                        BtnFind.Focus();
                    else
                        BtnSave.Focus();
                }
            }
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditTrimString(Grd1, new int[] { 1, 4 }, e);
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 2, 5 }, e);
            if (e.ColIndex == 2) Grd1.Cells[e.RowIndex, 3].Value = Sm.GetGrdDec(Grd1, e.RowIndex, 2);
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (e.ColIndex == 2 || e.ColIndex == 5)
            {
                decimal Total = 0m;
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, e.ColIndex).Length != 0) Total += Sm.GetGrdDec(Grd1, Row, e.ColIndex);
                }
                Sm.StdMsg(mMsgType.Info, "Total : " + Sm.FormatNum(Total, 0));
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueQueueNo_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueQueueNo, new Sm.RefreshLue2(SetLueQueueNo), string.Empty);
        }

        private void LueDocType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDocType, new Sm.RefreshLue1(SetLueDocType));
        }

        #endregion

        #region Grid Event

        private void Grd1_CustomDrawCellForeground(object sender, iGCustomDrawCellEventArgs e)
        {
            if (e.ColIndex != 3) return;

            try
            {
                object myObjValue = Grd1.Cells[e.RowIndex, e.ColIndex].Value;

                if (myObjValue == null) return;

                Pen RedPen = new Pen(Color.Red, 2);
                Rectangle myBounds = e.Bounds;

                myBounds.Inflate(-2, -2);
                myBounds.Width = myBounds.Width - 1;
                myBounds.Height = myBounds.Height - 1;

                int X1 = myBounds.X, Y1 = myBounds.Y;
                Int32 myValue = Sm.GetGrdInt(Grd1, e.RowIndex, e.ColIndex);
                Int32 DivValue = (Int32)(myValue / 5);
                Int32 RemainingValue = myValue % 5;

                for (int intX = 0; intX < DivValue; intX++)
                {
                    e.Graphics.DrawLine(RedPen, X1, Y1, X1, Y1 + myBounds.Height);
                    e.Graphics.DrawLine(RedPen, X1, Y1 + myBounds.Height, X1 + myBounds.Height, Y1 + myBounds.Height);
                    e.Graphics.DrawLine(RedPen, X1 + myBounds.Height, Y1 + myBounds.Height, X1 + myBounds.Height, Y1);
                    e.Graphics.DrawLine(RedPen, X1 + myBounds.Height, Y1, X1, Y1);
                    e.Graphics.DrawLine(RedPen, X1, Y1, X1 + myBounds.Height, Y1 + myBounds.Height);

                    X1 = X1 + myBounds.Height + 5;
                }

                if (RemainingValue >= 1)
                {
                    e.Graphics.DrawLine(RedPen, X1, Y1, X1, Y1 + myBounds.Height);
                    if (RemainingValue >= 2)
                    {
                        e.Graphics.DrawLine(RedPen, X1, Y1 + myBounds.Height, X1 + myBounds.Height, Y1 + myBounds.Height);
                        if (RemainingValue >= 3)
                        {
                            e.Graphics.DrawLine(RedPen, X1 + myBounds.Height, Y1 + myBounds.Height, X1 + myBounds.Height, Y1);
                            if (RemainingValue >= 4)
                            {
                                e.Graphics.DrawLine(RedPen, X1 + myBounds.Height, Y1, X1, Y1);
                                if (RemainingValue >= 5)
                                    e.Graphics.DrawLine(RedPen, X1, Y1, X1 + myBounds.Height, Y1 + myBounds.Height);
                            }
                        }
                    }
                }

                int TotalWidth = 0;
                for (int intX = 0; intX <= e.ColIndex; intX++)
                    TotalWidth = TotalWidth + Grd1.Cols[intX].Width;

                if (X1 > TotalWidth)
                    Grd1.Cols[e.ColIndex].Width = Grd1.Cols[e.ColIndex].Width + (X1 - TotalWidth) + myBounds.Height;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #endregion
    }
}
