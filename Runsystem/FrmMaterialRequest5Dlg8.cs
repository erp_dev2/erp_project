﻿#region Update
/*
    19/11/2021 [MYA/SIER] New Apps
    24/11/2021 [MYA/SIER] FEEDBACK : Penyesuaian menu BOQ pada Detail (0103990104) Material Request SPPJB saat di loop
    17/12/2021 [MYA/SIER] BUG: Ketika input BOQ di detail MRSPPJB dan loop di kolom Category masih menampilkan List Of Job
    06/04/2022 [SET/SIER] BUG: Hanya Muncul Satu Category setelah Memilih Category Lain di BOQ MR SPPJB
    20/04/2022 [VIN/SIER] Category yang sudh dipilih tetap ditampilkan setelah dlg ini di close krn catgory bisa dipilih berkali2
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmMaterialRequest5Dlg8 : RunSystem.FrmBase4
    {
        #region Field

        private string mSQL = string.Empty;
        private FrmMaterialRequest5Dlg7 mFrmParent;
        private bool mIsFilterBySite = false;
        private iGrid mGrd;

        #endregion

        #region Constructor

        public FrmMaterialRequest5Dlg8(FrmMaterialRequest5Dlg7 FrmParent, iGrid Grd)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mGrd = Grd;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                //Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                Sl.SetLueJobCtCode(ref LueJobCtCode);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void GetParameter()
        {
            mIsFilterBySite = Sm.GetParameterBoo("IsFilterBySite");
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 9;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[]
                    {
                        //0
                        "No",

                        //1-5
                        "",
                        "Job's Code",
                        "Job's Name",
                        "Job's Category Code",
                        "Category",
                        
                        //6-8
                        "UoM",
                        "Previous Currency",
                        "Previous Price"
                    },
                    new int[]
                    {
                        //0
                        50,

                        //1-5
                        20, 100, 200, 150, 200, 
                        
                        //6-7
                        100, 120, 150
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColReadOnly(Grd1, new int[] { 2, 3, 4, 5, 6, 7, 8 });
            Sm.GrdFormatDec(Grd1, new int[] { 8 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 6, 7, 8 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 6, 7, 8 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.JobCode, A.JobName, B.JobCtCode, B.JobCtName, A.UomCode, A.CurCode, A.UPrice ");
            SQL.AppendLine("From TblJob A, TblJobCategory B ");
            SQL.AppendLine("Where A.JobCtCode=B.JobCtCode ");
            if (mIsFilterBySite)
            {
                SQL.AppendLine("And A.JobCode In ( ");
                SQL.AppendLine("    Select Distinct JobCode From TblJobSite T ");
                SQL.AppendLine("    Where Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(T.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("            ) ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("And A.ActInd='Y' ");
            //SQL.AppendLine("Group By B.JobCtCode ");

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = string.Empty, JobCode = string.Empty;

                var cm = new MySqlCommand();
                //https://trello.com/c/1NKTquSU/793-ask-category-boq-di-menu-mr-sppjb-bisa-dipilih-berkali-kali
                //if (mGrd.Rows.Count >= 1)
                //{
                //    for (int r = 0; r < mGrd.Rows.Count; r++)
                //    {
                //        JobCode = Sm.GetGrdStr(mGrd, r, 1);
                //        if (JobCode.Length != 0)
                //        {
                //            if (Filter.Length > 0) Filter += " And ";
                //            Filter += "(A.JobCtCode<>@jobCtCode_" + r.ToString() + ") ";
                //            Sm.CmParam<String>(ref cm, "@JobCtCode_" + r.ToString(), JobCode);
                //        }
                //    }
                //}

                if (Filter.Length != 0)
                    Filter = " And (" + Filter + ")";
                else
                    Filter = " ";

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueJobCtCode), "A.JobCtCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtJobCode.Text, new string[] { "A.JobCode", "A.JobName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL.ToString() + Filter + " Group By B.JobCtCode Order By A.JobName;",
                        new string[]
                        { 
                            //0
                            "JobCode",
 
                            //1-5
                            "JobName", "JobCtCode", "JobCtName", "UomCode", "CurCode",
                            
                            //6
                            "UPrice"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = false;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 6);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsDataAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mGrd.Rows.Count - 1;
                        Row2 = Row;

                        //Sm.CopyGrdValue(mGrd, Row1, 1, Grd1, Row2, 2);
                        //Sm.CopyGrdValue(mGrd, Row1, 2, Grd1, Row2, 3);
                        Sm.CopyGrdValue(mGrd, Row1, 1, Grd1, Row2, 4);
                        Sm.CopyGrdValue(mGrd, Row1, 2, Grd1, Row2, 5);
                        //Sm.CopyGrdValue(mGrd, Row1, 5, Grd1, Row2, 6);
                        //Sm.CopyGrdValue(mGrd, Row1, 6, Grd1, Row2, 7);
                        //Sm.CopyGrdValue(mGrd, Row1, 7, Grd1, Row2, 6);
                        //Sm.CopyGrdValue(mGrd, Row1, 8, Grd1, Row2, 7);
                        //mGrd.Cells[Row1, 10].Value = string.Empty;
                        mGrd.Rows.Add();
                        Sm.SetGrdNumValueZero(mGrd, Row1, new int[] { 4, 7, 8 });
                        //mFrmParent.ComputeEstPrice();
                    }
                }
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 job.");
        }

        private bool IsDataAlreadyChosen(int Row)
        {
            var JobCode = Sm.GetGrdStr(Grd1, Row, 2);
            for (int r = 0; r < mGrd.Rows.Count - 1; r++)
                if (Sm.CompareStr(Sm.GetGrdStr(mGrd, r, 1), JobCode)) return true;
            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {

        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int r = 0; r < Grd1.Rows.Count; r++)
                    Grd1.Cells[r, 1].Value = !IsSelected;
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtJobCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkJobCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Job");
        }

        private void LueJobCtCode_EditValueChanged(object sender, EventArgs e)
        {

        }

        private void ChkJobCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Job's category");
        }

        #endregion

        #endregion

    }
}
