﻿#region Update
/*
    13/02/2018 [TKG] New Application
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmItemPrice2Dlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmItemPrice2 mFrmParent;
        internal string mMenuCode = string.Empty;

        #endregion

        #region Constructor

        public FrmItemPrice2Dlg (FrmItemPrice2 FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 6;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "",
                    "Item/Asset"+Environment.NewLine+"Code",
                    "Item/Asset"+Environment.NewLine+"Name",
                    "Asset",
                    "UoM"
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    20, 130, 200, 80, 100, 
                }
            );
            Sm.GrdColCheck(Grd1, new int[] { 1, 4 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 3, 4, 5 });
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
        }

        private string GetSQL(string Filter1, string Filter2)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select * From ("); 
            SQL.AppendLine("    Select ItCode, ItName, 'N' As AssetInd, SalesUomCode As PriceUomCode "); 
            SQL.AppendLine("    From TblItem ");
            SQL.AppendLine("    Where ActInd='Y' ");
            SQL.AppendLine("    And SalesItemInd='Y' ");
            SQL.AppendLine(Filter1);
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select AssetCode As ItCode, AssetName As ItName, 'Y' As AssetInd, @AssetPriceUomCode As PriceUomCode ");
            SQL.AppendLine("    From TblAsset ");
            SQL.AppendLine("    Where ActiveInd='Y' ");
            SQL.AppendLine("    And (RentedInd='Y' Or SoldInd='Y') ");
            SQL.AppendLine(Filter2);
            SQL.AppendLine(") T "); 
            return SQL.ToString();
        }


        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();
                string Filter1 = string.Empty, Filter2 = string.Empty, Code = string.Empty;

                if (mFrmParent.Grd1.Rows.Count >= 1)
                {
                    for (int r = 0; r < mFrmParent.Grd1.Rows.Count; r++)
                    {
                        Code = Sm.GetGrdStr(mFrmParent.Grd1, r, 2);
                        if (Code.Length != 0)
                        {
                            if (Sm.GetGrdBool(mFrmParent.Grd1, r, 4))
                            {
                                if (Filter2.Length > 0) Filter2 += " And ";
                                Filter2 += "(AssetCode<>@AssetCode0" + r.ToString() + ") ";
                                Sm.CmParam<String>(ref cm, "@AssetCode0" + r.ToString(), Code);
                            }
                            else
                            {
                                if (Filter1.Length > 0) Filter1 += " And ";
                                Filter1 += "(ItCode<>@ItCode0" + r.ToString() + ") ";
                                Sm.CmParam<String>(ref cm, "@ItCode0" + r.ToString(), Code);
                            }
                        }
                    }
                }

                if (Filter1.Length > 0)
                    Filter1 = " And (" + Filter1 + ")";
                else
                    Filter1 = " ";

                if (Filter2.Length > 0)
                    Filter2 = " And (" + Filter2 + ")";
                else
                    Filter2 = " ";

                Sm.CmParam<string>(ref cm, "@AssetPriceUomCode", mFrmParent.mAssetPriceUomCode);
                if (ChkItCode.Checked)
                {
                    Sm.FilterStr(ref Filter1, ref cm, TxtItCode.Text, new string[] { "ItCode", "ItName" });
                    Sm.FilterStr(ref Filter2, ref cm, TxtItCode.Text, new string[] { "AssetCode", "AssetName" });
                }
                
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        GetSQL(Filter1, Filter2) + " Order By T.AssetInd Desc, T.ItName;",
                        new string[] 
                        { 
                        
                            //0
                            "ItCode",

                            //1-3
                            "ItName", "AssetInd", "PriceUomCode"

                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = false;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsItCodeAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 2, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 3, Grd1, Row2, 3);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 4, Grd1, Row2, 4);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 6, Grd1, Row2, 5);
                        mFrmParent.Grd1.Cells[Row1, 5].Value = 0m;

                        mFrmParent.Grd1.Rows.Add();
                        mFrmParent.Grd1.Cells[mFrmParent.Grd1.Rows.Count - 1, 5].Value = 0;
                    }
                }
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 item.");
        }

        private bool IsItCodeAlreadyChosen(int Row)
        {
            var ItCode = Sm.GetGrdStr(Grd1, Row, 2);
            for (int r = 0; r < mFrmParent.Grd1.Rows.Count - 1; r++)
                if (Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd1, r, 2), ItCode)) 
                    return true;
            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    Grd1.Cells[Row, 1].Value = !IsSelected;
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item/Asset");
        }

        #endregion

        #endregion

    }
}
