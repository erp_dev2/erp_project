﻿#region Update
/*
    17/01/2019 [TKG] konversi quantity berdasarkan formula secara otomatis utk uom3
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmTransferRequestWhs2 : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mDocNo = string.Empty;
        internal bool mIsShowForeignName = false;
        internal int mNumberOfInventoryUomCode = 1;
        internal FrmTransferRequestWhs2Find FrmFind;

        #endregion

        #region Constructor

        public FrmTransferRequestWhs2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Warehouse's Transfer Request (With Batch#)";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                Sl.SetLueWhsCode(ref LueWhsCode);
                SetFormControl(mState.View);
                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 22;
            Grd1.FrozenArea.ColCount = 3;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",
                        
                        //1-5
                        "",
                        "Item's Code",
                        "",
                        "Item's Name",
                        "Foreign Name",

                        //6-10
                        "Property Code",
                        "Property",
                        "Batch#",
                        "Source",
                        "Lot",

                        //11-15
                        "Bin",
                        "Stock",
                        "Requested",
                        "UoM",
                        "Stock",
                        
                        //16-20
                        "Requested",
                        "UoM",
                        "Stock",
                        "Requested",
                        "UoM",
                        
                        //21
                        "Remark"
                    },
                     new int[] 
                    {
                        //0
                        0,

                        //1-5
                        20, 80, 20, 200, 100, 

                        //6-10
                        0, 80, 150, 170, 60,

                        //11-15
                        60, 100, 100, 60, 100, 
                        
                        //16-20
                        100, 60, 100, 100, 60, 
                        
                        //21
                        300
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 12, 13, 15, 16, 18, 19 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 1, 3 });
            Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 3, 5, 6, 9, 15, 16, 17, 18, 19, 20 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 14, 15, 17, 18, 20 });
            if (mIsShowForeignName) Sm.GrdColInvisible(Grd1, new int[] { 5 }, true);
            ShowInventoryUomCode();
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 9 }, !ChkHideInfoInGrd.Checked);
        }

        private void ShowInventoryUomCode()
        {
            if (mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 15, 16, 17 }, true);

            if (mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 15, 16, 17, 18, 19, 20 }, true);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { DteDocDt, LueWhsCode, LueWhsCode2, MeeCancelReason, ChkCancelInd, MeeRemark }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 13, 16, 19, 21 });
                    Sm.GrdColInvisible(Grd1, new int[] { 12, 15, 18 }, false);
                    ChkCancelInd.Properties.ReadOnly = true;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { DteDocDt, LueWhsCode, LueWhsCode2, MeeRemark }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1, 13, 16, 19, 21 });
                    Sm.GrdColInvisible(Grd1, new int[] { 12 }, true);
                    if (mNumberOfInventoryUomCode == 2)
                        Sm.GrdColInvisible(Grd1, new int[] { 15 }, true);
                    if (mNumberOfInventoryUomCode == 3)
                        Sm.GrdColInvisible(Grd1, new int[] { 15, 18 }, true);
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeCancelReason, ChkCancelInd }, false);
                    MeeCancelReason.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            { 
                TxtDocNo, DteDocDt, LueWhsCode, LueWhsCode2, MeeCancelReason, 
                TxtDOWhs3DocNo, MeeRemark 
            });
            ChkCancelInd.Checked = false;
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 12, 13, 15, 16, 18, 19 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmTransferRequestWhs2Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sl.SetLueWhsCode(ref LueWhsCode2, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 1)
                {
                    e.DoDefault = false;
                    if (e.KeyChar == Char.Parse(" ") && !Sm.IsLueEmpty(LueWhsCode, "Warehouse"))
                        Sm.FormShowDialog(new FrmTransferRequestWhs2Dlg(this, Sm.GetLue(LueWhsCode)));
                }

                if (Sm.IsGrdColSelected(new int[] { 13, 16, 19, 21 }, e.ColIndex))
                {
                    Sm.GrdRequestEdit(Grd1, e.RowIndex);
                    Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 12, 13, 15, 16, 18, 19 });
                }
            }

            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
                Sm.ShowItemInfo("XXX", Sm.GetGrdStr(Grd1, e.RowIndex, 2));
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdEnter(Grd1, e);
            Sm.GrdTabInLastCell(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1 && !Sm.IsLueEmpty(LueWhsCode, "Warehouse"))
                Sm.FormShowDialog(new FrmTransferRequestWhs2Dlg(this, Sm.GetLue(LueWhsCode)));

            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
                Sm.ShowItemInfo("XXX", Sm.GetGrdStr(Grd1, e.RowIndex, 2));
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditTrimString(Grd1, new int[] { 21 }, e);
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 13, 16, 19 }, e);

            if (e.ColIndex == 13)
            {
                Sm.ComputeQtyBasedOnConvertionFormula("12", Grd1, e.RowIndex, 2, 13, 16, 19, 14, 17, 20);
                Sm.ComputeQtyBasedOnConvertionFormula("13", Grd1, e.RowIndex, 2, 13, 19, 16, 14, 20, 17);
            }

            if (e.ColIndex == 16)
            {
                Sm.ComputeQtyBasedOnConvertionFormula("21", Grd1, e.RowIndex, 2, 16, 13, 19, 17, 14, 20);
                Sm.ComputeQtyBasedOnConvertionFormula("23", Grd1, e.RowIndex, 2, 16, 19, 13, 17, 20, 14);
            }

            if (e.ColIndex == 19)
            {
                Sm.ComputeQtyBasedOnConvertionFormula("31", Grd1, e.RowIndex, 2, 19, 13, 16, 20, 14, 17);
                Sm.ComputeQtyBasedOnConvertionFormula("32", Grd1, e.RowIndex, 2, 19, 16, 13, 20, 17, 14);
            }

            if (e.ColIndex == 13 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 14), Sm.GetGrdStr(Grd1, e.RowIndex, 17)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 16, Grd1, e.RowIndex, 13);

            if (e.ColIndex == 13 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 14), Sm.GetGrdStr(Grd1, e.RowIndex, 20)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 19, Grd1, e.RowIndex, 13);

            if (e.ColIndex == 16 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 17), Sm.GetGrdStr(Grd1, e.RowIndex, 20)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 19, Grd1, e.RowIndex, 16);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "TransferRequestWhs2", "TblTransferRequestWhs2Hdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveTransferRequestWhs2Hdr(DocNo));

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0)
                    cml.Add(SaveTransferRequestWhs2Dtl(DocNo, Row));

            Sm.ExecCommands(cml);

            BtnInsertClick(sender, e);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueWhsCode, "Warehouse (from)") ||
                Sm.IsLueEmpty(LueWhsCode2, "Warehouse (to)") ||
                IsWhsCode2Invalid() ||
                IsGrdEmpty() ||
                IsGrdExceedMaxRecords() ||
                IsGrdValueNotValid();
        }

        private bool IsWhsCode2Invalid()
        { 
            if (Sm.CompareStr(Sm.GetLue(LueWhsCode), Sm.GetLue(LueWhsCode2)))
            {
                Sm.StdMsg(mMsgType.Warning, "You should input different warehouses.");
                LueWhsCode2.Focus();
                return true;
            }
            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 item.");
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning, "Item data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            string Msg = string.Empty;

            //ReComputeStock();

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, Row, 2, false, "Item is empty.")) return true;
                
                Msg =
                    "Item's Code : " + Sm.GetGrdStr(Grd1, Row, 2) + Environment.NewLine +
                    "Item's Name : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine +
                    (mIsShowForeignName?("Foreign Name : " + Sm.GetGrdStr(Grd1, Row, 5) + Environment.NewLine):string.Empty) + 
                    Environment.NewLine;

                if (Sm.GetGrdDec(Grd1, Row, 13) <= 0m)
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Quantity should be greater than 0.");
                    return true;
                }

                if (Sm.GetGrdDec(Grd1, Row, 13) > Sm.GetGrdDec(Grd1, Row, 12))
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Requested quantity should not be bigger than available stock.");
                    return true;
                }

                if (Grd1.Cols[16].Visible)
                {
                    if (Sm.GetGrdDec(Grd1, Row, 16) <= 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Quantity (2) should be greater than 0.");
                        return true;
                    }

                    if (Sm.GetGrdDec(Grd1, Row, 16) > Sm.GetGrdDec(Grd1, Row, 15))
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Requested quantity (2) should not be bigger than available stock (2).");
                        return true;
                    }
                }

                if (Grd1.Cols[19].Visible)
                {
                    if (Sm.GetGrdDec(Grd1, Row, 19) <= 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Quantity (3) should be greater than 0.");
                        return true;
                    }

                    if (Sm.GetGrdDec(Grd1, Row, 19) > Sm.GetGrdDec(Grd1, Row, 18))
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Requested quantity (3) should not be bigger than available stock (3).");
                        return true;
                    }
                }
            }

            return false;
        }

        private MySqlCommand SaveTransferRequestWhs2Hdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblTransferRequestWhs2Hdr(DocNo, DocDt, WhsCode, WhsCode2, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @WhsCode, @WhsCode2, @Remark, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            Sm.CmParam<String>(ref cm, "@WhsCode2", Sm.GetLue(LueWhsCode2));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveTransferRequestWhs2Dtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblTransferRequestWhs2Dtl");
            SQL.AppendLine("(DocNo, DNo, ItCode, PropCode, BatchNo, Source, Lot, Bin, ");
            SQL.AppendLine("Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @ItCode, @PropCode, @BatchNo, @Source, @Lot, @Bin, ");
            SQL.AppendLine("@Qty, @Qty2, @Qty3, @Remark, @CreateBy, CurrentDateTime()) ");
            
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<String>(ref cm, "@PropCode", Sm.GetGrdStr(Grd1, Row, 6));
            Sm.CmParam<String>(ref cm, "@BatchNo", Sm.GetGrdStr(Grd1, Row, 8));
            Sm.CmParam<String>(ref cm, "@Source", Sm.GetGrdStr(Grd1, Row, 9));
            Sm.CmParam<String>(ref cm, "@Lot", Sm.GetGrdStr(Grd1, Row, 10));
            Sm.CmParam<String>(ref cm, "@Bin", Sm.GetGrdStr(Grd1, Row, 11));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 13));
            Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd1, Row, 16));
            Sm.CmParam<Decimal>(ref cm, "@Qty3", Sm.GetGrdDec(Grd1, Row, 19));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 21));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsCancelledDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditTransferRequestWhs2Hdr());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsCancelledDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDataNotCancelled() ||
                IsDataAlreadyCancelled() ||
                IsDataAlreadyDO();
        }

        private bool IsDataNotCancelled()
        {
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this document.");
                return true;
            }
            return false;
        }

        private bool IsDataAlreadyCancelled()
        {
            return
                Sm.IsDataExist(
                    "Select DocNo From TblTransferRequestWhs2Hdr " +
                    "Where CancelInd='Y' And DocNo=@Param;",
                    TxtDocNo.Text,
                    "This document already cancelled."
                );
        }

        private bool IsDataAlreadyDO()
        {
            return
                Sm.IsDataExist(
                    "Select DocNo From TblTransferRequestWhs2Hdr " +
                    "Where DOWhs3DocNo Is Not Null " +
                    "And DocNo=@Param;",
                    TxtDocNo.Text,
                    "This document already processed to DO."
                );
        }

        private MySqlCommand EditTransferRequestWhs2Hdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblTransferRequestWhs2Hdr Set ");
            SQL.AppendLine("    CancelInd='Y', CancelReason=@CancelReason, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowTransferRequestWhs2Hdr(DocNo);
                ShowTransferRequestWhs2Dtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowTransferRequestWhs2Hdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select DocNo, DocDt, CancelInd, WhsCode, WhsCode2, CancelReason, DOWhs3DocNo, Remark " +
                    "From TblTransferRequestWhs2Hdr Where DocNo=@DocNo;",
                    new string[] 
                    { 
                        //0
                        "DocNo",
 
                        //1-5
                        "DocDt", "CancelReason", "CancelInd", "WhsCode", "WhsCode2", 
                        
                        //6-7
                        "DOWhs3DocNo", "Remark" 
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        MeeCancelReason.EditValue = Sm.DrStr(dr, c[2]);
                        ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[3]), "Y");
                        Sm.SetLue(LueWhsCode, Sm.DrStr(dr, c[4]));
                        Sl.SetLueWhsCode(ref LueWhsCode2, Sm.DrStr(dr, c[5]));
                        TxtDOWhs3DocNo.EditValue = Sm.DrStr(dr, c[6]);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[7]);
                    }, true
                );
        }

        private void ShowTransferRequestWhs2Dtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.ItCode, B.ItName, B.ForeignName, ");
            SQL.AppendLine("A.PropCode, C.PropName, A.BatchNo, A.Source, A.Lot, A.Bin, ");
            SQL.AppendLine("A.Qty, B.InventoryUomCode, A.Qty2, B.InventoryUomCode2, A.Qty3, B.InventoryUomCode3, A.Remark ");
            SQL.AppendLine("From TblTransferRequestWhs2Dtl A ");
            SQL.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
            SQL.AppendLine("Left Join TblProperty C On A.PropCode=C.PropCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 

                    //1-5
                    "ItCode", "ItName", "ForeignName", "PropCode", "PropName",   
                    
                    //6-10
                    "BatchNo", "Source", "Lot", "Bin", "Qty",   
                    
                    //11-15
                    "InventoryUomCode", "Qty2", "InventoryUomCode2", "Qty3", "InventoryUomCode3", 
                    
                    //16
                    "Remark" 
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 10);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 12);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 13);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 14);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 15);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 16);
                    Sm.SetGrdNumValueZero(ref Grd1, Row, new int[] { 12, 15, 18 });
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 12, 13, 15, 16, 18, 19 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            var NumberOfInventoryUomCode = Sm.GetParameter("NumberOfInventoryUomCode");
            if (NumberOfInventoryUomCode.Length > 0) mNumberOfInventoryUomCode = int.Parse(NumberOfInventoryUomCode);
            mIsShowForeignName = Sm.GetParameter("IsShowForeignName") == "Y";
        }

        //private void ReComputeStock()
        //{
        //    if (Grd1.Rows.Count <= 1) return;
        //    string Filter = string.Empty, Source = string.Empty, Lot = string.Empty, Bin = string.Empty;
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Select Source, Lot, Bin, Qty, Qty2, Qty3 ");
        //    SQL.AppendLine("From TblStockSummary ");
        //    SQL.AppendLine("Where WhsCode=@WhsCode ");

        //    using (var cn = new MySqlConnection(Gv.ConnectionString))
        //    {
        //        cn.Open();
        //        var cm = new MySqlCommand();
        //        cm.Connection = cn;
        //        Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
        //        if (Grd1.Rows.Count != 1)
        //        {
        //            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
        //                if (Sm.GetGrdStr(Grd1, Row, 9).Length != 0)
        //                    Sm.GenerateSQLConditionForInventory(ref cm, ref Filter, Row+1, ref Grd1, Row, 9, 10, 11);
        //        }
        //        cm.CommandText =
        //            SQL.ToString() + (Filter.Length != 0 ? " And (" + Filter + ") " : string.Empty);
        //        var dr = cm.ExecuteReader();
        //        var c = Sm.GetOrdinal(dr, new string[] 
        //            { 
        //                //0
        //                "Source", 
                        
        //                //1-5
        //                "Lot", "Bin", "Qty", "Qty2", "Qty3" 
        //            });

        //        if (dr.HasRows)
        //        {
        //            Grd1.BeginUpdate();
        //            while (dr.Read())
        //            {
        //                Source = Sm.DrStr(dr, 0);
        //                Lot = Sm.DrStr(dr, 1);
        //                Bin = Sm.DrStr(dr, 2);
        //                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
        //                {
        //                    if (
        //                        Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 9), Source) &&
        //                        Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 10), Lot) &&
        //                        Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 11), Bin)
        //                        )
        //                    {
        //                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 12, 3);
        //                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 15, 4);
        //                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 18, 5);
        //                        break;
        //                    }
        //                }
        //            }
        //            Grd1.EndUpdate();
        //        }
        //        dr.Close();
        //    }
        //}

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue1(Sl.SetLueWhsCode));
        }

        private void LueWhsCode2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode2, new Sm.RefreshLue2(Sl.SetLueWhsCode), string.Empty);
        }

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        #endregion

        #endregion
    }
}
