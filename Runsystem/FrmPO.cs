﻿#region Update
/*
    03/04/2017 Outstanding quantity berdasarkan PO request dan pemutihan MR
    16/01/2018 tambahan parameter buat set bill to, jika Y ambil dari entity paling awal dibikin 
    19/02/2018 [TKG] berdasarkan IsShowPOWithTaxWarning, pada saat menmbah PO baru akan keluar warning (yg masih bisa disimpan) ketika customer yg sama pernah mengeluarkan PO dengan pajak sedangkan PO baru yg akan disimpan tidak menggunakan pajak. 
    09/03/2018 [TKG] hilangkan customs tax.
    10/03/2018 [TKG] tambah import PO.
    06/04/2018 [ARI] feedback printout PO AWG
    18/05/2018 [ARI] detail printout PO tidak sesuai dg di ditransaksi (saat ada proses PO cancel)
    26/05/2018 [TKG] Data DP yg dicancel tidak dimunculkan
    29/05/2018 [TKG] tambah status PO.
    26/06/2018 [TKG] Bug PO (Apabila semua item di PO dicancel semua, status PO diubah menjadi cancel).
    05/07/2018 [TKG] Jika menggunakan PO approval, pada saat insert new PO tidak otomatis print PO
    05/07/2018 [TKG] Jika menggunakan PO approval, Tanda tangan PO berdasarkan data PO
    13/08/2018 [WED] tambah informasi Project Name dari LOP, berdasarkan parameter IsPOShowLOPData
    15/08/2018 [HAR] tambah button download di detail berdasarkan file yang diupload di vendor quotation
    02/09/2018 [TKG] Menampilkan nomor urut berdasarkan parameter
    23/11/2018 [HAR] jadi 5 file + format ftp
    07/02/2019 [DITA] Tambah parameter PORequestPropCodeEnabled
    19/03/2019 [DITA] Mengunci saat PO (save) melebihi budget melihat dari reporting budget VS PO
    04/04/2019 [DITA] Print PO yg indikator import berbhasa inggris 
    11/04/2019 [WED] validasi budget v PO ganti rumus, berdasarkan reporting budget cashflow
    09/05/2019 [TKG] ubah setting tgl approve signature di PO
    14/05/2019 [WED] validasi Budget v PO, melihat Grand Total PO (minus TaxAmt & CustomsTaxAmt)
    29/05/2019 [WED] ttd approval PO AWG, dimunculkan semuanya. urutan : Created By, Approval POR, Approval PO
    11/07/2019 [DITA] Format Baru Print Out PO ada Term Condition nya --TWC
    14/08/2019 [DITA] Notes PO yang Import ubah ke bahasa inggris, dan tambah info ketika remark berubah
    02/09/2019 [WED] PO yang sudah kebikin APDP nya, gaboleh di cancel.
    22/09/2019 [TKG/IMS] layar dialog untuk split estimated received date
    18/10/2019 [DITA/IMS] Attachment File untuk PO
    07/11/2019 [DITA/IMS] tambah informasi Specifications
    04/12/2019 [HAR/MSI] BUG print out ketika user create by dan user approval sama, munuclnya cuma 1 (without approval PO)
    04/12/2019 [DITA+VIN/IMS] printout purchase order
    17/02/2020 [TKG/IMS] tambah project code, project name
    11/05/2020 [IBL/KSM] Manambah extension rar/zip di upload file
    27/05/2020 [TKG/IMS] tambah karakter menjadi 2000 di remark
    28/05/2020 [WED/IMS] print out Project Name belom ada
    02/06/2020 [WED/IMS] upload file bisa berkali kali (edit)
    18/09/2020 [WED/IMS] printout PO di rubah
    18/09/2020 [DITA/IMS] perubahan design printout PO
    20/09/2020 [TKG/IMS] Berdasarkan parameter IsDocNoFormatUseFullYear, dokumen# menampilkan tahun secara lengkap (Misal : 2020).
    21/09/2020 [WED/IMS] parameter tanda tangan POSignatureSource
    22/09/2020 [DITA/IMS] feedback printout PO (kode barang : local code)
    29/09/2020 [TKG/IMS] GenerateDocNo reset nomor urut per tahun
    06/10/2020 [VIN/IMS] feedback printout PO IMS
    23/10/2020 [TKG/IMS] saat save (insert) otomatis menampilkan data yg disimpan
    03/12/2020 [ICA/SIER] Menambah keterangan category pada SetLueVdCode berdasarkan parameter IsVendorComboShowCategory
    13/01/2021 [VIN/KSM] SetLueVdCode --> IsVendorComboShowCategory tambah ifnull vdctname 
    16/01/2021 [TKG/KSM] bug saat SetLueVdCode
    19/01/2021 [VIN/IMS] menampilkan informasi item berdasarkan parameter IsBOMShowSpecifications
    11/02/2021 [IBL/SIER] menambah field Contract berdasarkan IsPOUseContract
    12/03/2021 [TKG/IMS] Berdasarkan parameter IsPOSignatureWithRangeEnabled, print out po signature diubah querynya
    15/03/2021 [VIN/SIER] generate local docno berdasarkan parameter IsPOUseContract
    30/04/2021 [TRI/KIM] Printout PO divalidasi berdasarkan nilai yang ada di parameter (POAmtForSignature)
    25/05/2021 [VIN/IMS] ganti source printout - createby - usercode 
    31/05/2021 [VIN/IMS] feedback printout sign parameter amt * rate 
    03/06/2021 [HAR/IMS] bug saat prinout ketika MR banyak dan di load di tiap page
    08/06/2021 [VIN/IMS] bug saat prinout create by tidak muncul
    10/06/2021 [VIN/IMS] penyesuaian source local document 
    14/06/2021 [RDH/IMS] Menambah ceklist di header Find untuk meng excludekan dokumen PO hasil dr PO For Service 
    23/06/2021 [RDA/IMS] Menambah pemanggilan parameter IsItCtFilteredByGroup untuk tampil item sesuai group login pada dialog
    09/08/2021 [WED/PADI] tambah parameter IsUseECatalog
    14/09/2021 [VIN/IMS] tambah parameter IsRecvExpeditionEnabled --> tidak bisa cancel jika item sudah di expedition kan 
    28/10/2021 [TYO/PHT] Menambahkan kolom ETA (Estimated Time of Arrival)
    08/11/2021 [TYO/PHT] Menyesuaikan kolom ETA (Estimated Time of Arrival)
    12/11/2021 [NJP/RM] Menampilan Nilai Quotation Awal
    19/11/2021 [ICA/PHT] Bug ketik klik lup Cancellation di PO
    24/11/2021 [ICA/PHT] menambah lup di tab Revision untuk download file yg diupload di PO Revision
    25/11/2021 [RIS/PHT] Menambahkan kolom cancelation unit dan total dengan param IsPOShowCancelationUnitPrice
    06/01/2022 [ISD/PHT] menambah parameter IsFilterByDept dan IsFilterByItCt untuk PODlg
    20/01/2022 [TKG/PHT] merubah GetParameter() dan proses save
    14/03/2022 [TRI/IMS] bug ketika printout ada qty cancel maka kolom terbilang ambilnya dari detail+pajak-discount 
    07/10/2022 [RDA/SIER] vendor yang muncul terfilter sesuai grup berdasarkan parameter IsFilterByVendorCategory
    16/12/2022 [DITA/MNET] memandatorykan remark based on param : IsPurchaseOrderRemarkMandatory
    26/12/2022 [ICA/MNET] membuat dua tab attachment untuk upload multifile berdasarkan parameter POUploadFileFormat
    10/01/2023 [RDA/MNET] printout baru untuk PO MNET 
    19/01/2023 [SET/SIER] printout ketika outstanding berdasar parameter IsOutstandingPOPrintEnabled
    31/01/2023 [BRI/SIER] bug pada chkfile
    20/02/2023 [DITA/SIER] based on param IsUploadFileRenamed, upload file dapat di rename file nya --> ngerubah ambil method upload file dari stdmtd
    14/03/2023 [RDA/MNET] tambah field POSignBy + penyesuaian ttd printout MNET based on param mIsPOUsePOSignedBy
    25/03/2023 [RDA/MNET] fix ttd kepotong ketika sudah save pdf
    28/03/2023 [VIN/TWC] BUG printout blm di false
    03/04/2023 [MAU/MNET] memandatorykan Contact Person berdasarkan parameter IsContactPersonPurchaseOrderMandatory
    11/04/2023 [ISN/SIER] menambahkan parameter IsFilterVendorInListofPORequestHide untuk filter vendor 
 */
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

using FastReport;
using FastReport.Data;
using System.IO;
using System.Net;
using System.Threading;

#endregion

namespace RunSystem
{
    public partial class FrmPO : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string
            mMenuCode = string.Empty, mAccessInd = string.Empty, IsProcFormat = string.Empty,
            mDocNo = string.Empty, //if this application is called from other application;
            mSiteCode = string.Empty,
            mHeaderDate = string.Empty,
            mDetailDate = string.Empty,
            compare1 = string.Empty,
            compare2 = string.Empty;
        private string 
             InsertInd = "0",
             mPOPrintOutCompanyLogo = "1",
             mEmpCodePO = string.Empty,
             mBudgetBasedOn = string.Empty,
             mMainCurCode = string.Empty,
             mPOAutoApprovedByEmpCode = string.Empty,
             mInitFile = string.Empty,
             mPOSignatureSource = string.Empty,
             mPOUploadFileFormat = string.Empty,
             mDocTitle = string.Empty
             ;
        private bool
            mIsShowPOWithTaxWarning = false,
            mIsPOShowLOPData = false,
            mIsShowSeqNoInPO = false,
            mIsPOEstRecvDtSplitEnabled = false,
            mIsPOAllowToUploadFile = false,
            mIsVendorComboShowCategory = false,
            mIsPOSignatureWithRangeEnabled = false,
            mIsPOShowCancelationUnitPrice = false,
            mIsPurchaseOrderRemarkMandatory = false,
            mIsOutstandingPOPrintEnabled = false,
            mIsUploadFileRenamed = false,
            mIsPOUsePOSignedBy = false,
            mIsContactPersonPurchaseOrderMandatory = false
            ;
        internal FrmPOFind FrmFind;
        internal bool
            mIsAutoGeneratePurchaseLocalDocNo = false,
            mIsShowForeignName = false,
            mIsSiteMandatory = false,
            mIsFilterBySite = false,
            mIsComparedToDetailDate = false,
            mIsShipToDefaultByCompany = false,
            mIsBillToDefaultByEntity = false,
            mIsGroupPaymentTermActived = false,
            mIsPOApprovalActived = false,
            mIsQTAllowToUploadFile = false,
            mIsPOGrandTotalComparedToBudget = false,
            mIsPOPrintOutInEnglish = false,
            mPORequestPropCodeEnabled = false,
            mIsPODisplayNotificationRemarkChanges = false,
            mIsBOMShowSpecifications = false,
            mIsPurchaseRequestForProductionEnabled = false,
            mIsPOUseContract = false,
            mIsPOFindUseExlucedPOService = false,
            mIsItCtFilteredByGroup = false,
            mIsUseECatalog = false,
            mIsRecvExpeditionEnabled = false,
            mIsPORevisionUseETA = false,
            mIsFilterByDept = false,
            mIsFilterByItCt = false,
            mIsFilterByVendorCategory = false,
            mIsFilterVendorInListofPORequestHide = false
            ;
        private List<LocalDocument> mlLocalDocument = null;
        private string
           mPortForFTPClient = string.Empty,
           mHostAddrForFTPClient = string.Empty,
           mSharedFolderForFTPClient = string.Empty,
           mUsernameForFTPClient = string.Empty,
           mPasswordForFTPClient = string.Empty,
           mFileSizeMaxUploadFTPClient = string.Empty,
           mFormatFTPClient = string.Empty;
        private byte[] downloadedData;
        internal List<EstRecvDt> mlEstRecvDt;

        private decimal mPOAmtForSignature = 0m;

        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmPO(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Purchase Order";

                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint, ref BtnExcel);
                GetParameter();
                GetValue();
                SetGrd();
                SetFormControl(mState.View);
                Sl.SetLueTaxCode(ref LueTaxCode1);
                Sl.SetLueTaxCode(ref LueTaxCode2);
                Sl.SetLueTaxCode(ref LueTaxCode3);
                SetLueContract(ref LueContract, string.Empty);
                SetLueOption(ref LueSignBy, "POSignBy");

                mlLocalDocument = new List<LocalDocument>();
                if (mIsSiteMandatory)
                    LblSiteCode.ForeColor = Color.Red;

                if (!mIsPOUseContract)
                {
                    LblContract.Visible = false;
                    LueContract.Visible = false;
                }
                if (mIsPurchaseOrderRemarkMandatory) LblRemark.ForeColor = Color.Red;
                mlEstRecvDt = new List<EstRecvDt>();

                if(mPOUploadFileFormat == "2")
                {
                    tabControl1.TabPages.Remove(tabPage6);
                }
                else
                {
                    tabControl1.TabPages.Remove(tabPage7);
                    tabControl1.TabPages.Remove(tabPage8);
                }

                if (!mIsPOUsePOSignedBy)
                {
                    LblSignBy.Visible = false;
                    LueSignBy.Visible = false;
                }

                if (mIsContactPersonPurchaseOrderMandatory)
                {
                    label12.ForeColor = Color.Red;
                }

                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
             }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grid 1

            Grd1.Cols.Count = 46;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                         //0
                        "DNo",

                        //1-5
                        "Cancel",
                        "Old Cancel",
                        "Cancel Reason",
                        "",
                        "Site Code",
                        
                        //6-10
                        "Site",
                        "PO Request#",
                        "PO Request"+Environment.NewLine+"DNo",
                        "Local#",
                        "PO Request"+Environment.NewLine+"Date",
                        
                        //11-15
                        "Material Request"+Environment.NewLine+"Date",
                        "Item's"+Environment.NewLine+"Code",
                        "Local Code",
                        "",
                        "Item's Name",    
                        
                        //16-20
                        "Foreign Name",
                        "ItScCode",
                        "Sub-Category",
                        "Quantity",
                        "UoM",
                        
                        //21-25
                        "Quotation#",
                        "Vendor Code",
                        "Vendor Name",
                        "Term of Payment",
                        "Currency",
                        
                        //26-30
                        "Unit Price",
                        "Discount"+Environment.NewLine+"%",
                        "Discount"+Environment.NewLine+"Amount",
                        "Rounding"+Environment.NewLine+"Value",
                        "Total",

                        //31-35
                        "Estimated"+Environment.NewLine+"Received Date",
                        "Delivery Type",
                        "Remark",
                        "",
                        "LOP#",

                        //36-40
                        "",
                        "Project's Code",
                        "File",
                        "No",
                        "",

                        //41-45
                        "Specification",
                        "Project's Name",
                        "Production",
                        "Estimated Received"+Environment.NewLine+"Date Revision",
                        "Quotation Price"
                    },
                     new int[] 
                    {
                        //0
                        20,
                        
                        //1-5
                        50, 80, 200, 20, 80, 
                        
                        //6-10
                        150, 180, 20, 180, 100, 
                        
                        //11-15
                        100, 150, 150, 20, 250, 
                        
                        //16-20
                        200, 80, 150, 100, 80, 
                        
                        //21-25
                        150, 80, 200, 150, 80, 
                        
                        //26-30
                        130, 120, 120, 100, 150, 
                        
                        //31-35
                        130, 100, 250, 20, 180, 

                        //36-40
                        20, 150, 28, 50, 20,

                        //41-45
                        300, 200, 120, 130, 120
                    }
                );


            Sm.GrdFormatDec(Grd1, new int[] { 19, 26, 27, 28, 29, 30, 45 }, 0);
            Sm.GrdColCheck(Grd1, new int[] { 1, 2, 43 });
            Sm.GrdColButton(Grd1, new int[] { 4, 14, 34, 36, 38, 40 });
            Sm.GrdFormatDate(Grd1, new int[] { 31, 11, 10, 44 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 37, 39, 41, 42, 43, 44, 45 });
            Sm.GrdColInvisible(Grd1, new int[] { 39 }, mIsShowSeqNoInPO);
            
            
            if (mIsShowForeignName)
            {
                if (IsProcFormat == "1")
                {
                    Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 7, 8, 12, 14, 21, 22, 23, 24, 11, 17, 5, 6, 13, 10 }, false);
                }
                else
                {
                    Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 7, 8, 12, 14, 21, 22, 23, 24, 11, 17, 18, 5, 6, 13, 10 }, false);
                }
            }
            else
            {
                if (IsProcFormat == "1")
                {
                    Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 7, 8, 12, 14, 21, 22, 23, 24, 11, 17, 6, 7, 17, 14, 11 }, false);
                }
                else
                {
                    Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 7, 8, 12, 14, 21, 22, 23, 24, 11, 17, 5, 6, 16, 13, 10 }, false);
                }
            }

            if (!mIsBOMShowSpecifications) Sm.GrdColInvisible(Grd1, new int[] { 41 });
            else
                Sm.GrdColInvisible(Grd1, new int[] { 13, 41 }, true);
            Grd1.Cols[41].Move(17);

            if (!mIsAutoGeneratePurchaseLocalDocNo||!mIsPOUseContract)
                Sm.GrdColInvisible(Grd1, new int[] { 9 }, false);
            if (mIsSiteMandatory)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 6 }, true);
            }

            if (!mIsPOShowLOPData) Sm.GrdColInvisible(Grd1, new int[] { 34, 35, 36, 37 });
            if(!mIsQTAllowToUploadFile) Sm.GrdColInvisible(Grd1, new int[] { 38 });
            Grd1.Cols[39].Move(0);
            if (mIsPOEstRecvDtSplitEnabled) 
                Grd1.Cols[40].Move(33);
            else
                Sm.GrdColInvisible(Grd1, new int[] { 40 }, false);
            if (mIsPurchaseRequestForProductionEnabled)
                Sm.GrdColInvisible(Grd1, new int[] { 43 }, true);
            else
                Sm.GrdColInvisible(Grd1, new int[] { 43 }, false);

            if (!mIsPORevisionUseETA)
                Sm.GrdColInvisible(Grd1, new int[] { 44 }, false);
            Grd1.Cols[44].Move(35);

            if (!mIsUseECatalog) Sm.GrdColInvisible(Grd1, new int[] { 45 });
            else Grd1.Cols[45].Move(27);

            #endregion

            #region Grid 2

            Grd2.Cols.Count = 9;

            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[] 
                    {
                        //0
                        "Document#", 
                        
                        //1-5
                        "Date",
                        "Cancel",
                        "Currency",
                        "Amount",
                        "Person In Charge",
                        
                        //6-8
                        "Voucher Request#",
                        "Voucher#",
                        "Remark"
                    },
                    new int[] 
                    {
                        //0
                        150,
 
                        //1-5
                        80, 50, 100, 100, 200, 
                        
                        //6-8
                        130, 130, 250 
                    }
                );
            Sm.GrdFormatDate(Grd2, new int[] { 1 });
            Sm.GrdColCheck(Grd2, new int[] { 2 });
            Sm.GrdFormatDec(Grd2, new int[] { 4 }, 0);
            Sm.GrdColInvisible(Grd2, new int[] { 2, 5, 8 }, false);

            #endregion

            #region Grid 3

            Grd3.Cols.Count = 15;
            Grd3.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd3,
                    new string[] 
                    {
                        //0
                        "Item's Code",
                        
                        //1-5
                        "Item's Name",
                        "Foreign Name",
                        "PO Revision#", 
                        "",
                        "FileName",
                        
                        //6-10
                        "",
                        "Date",
                        "Cancel",
                        "Status",
                        "Quotation#",

                        //11-14
                        "",
                        "Unit Price",
                        "Discount %",
                        "Discount Amount"
                    },
                    new int[] 
                    {
                        //0
                        100,
 
                        //1-5
                        200, 200, 150, 20, 0, 
                        
                        //6-10
                        20, 80, 60, 80, 150,

                        //11-13
                        20, 150, 100, 130
                    }
                );
            Sm.GrdColButton(Grd3, new int[] { 4, 6, 11 });
            Sm.GrdColCheck(Grd3, new int[] { 8 });
            Sm.GrdFormatDate(Grd3, new int[] { 7 });
            Sm.GrdFormatDec(Grd3, new int[] { 12, 13, 14 }, 0);
            Sm.GrdColInvisible(Grd3, new int[] { 0, 4, 11 }, false);
            if(!mIsShowForeignName)
                Sm.GrdColInvisible(Grd3, new int[] { 2 }, false);

            #endregion

            #region Grid 4

            Grd4.Cols.Count = 10;
            Grd4.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd4,
                new string[] 
                {
                    //0
                    "Item's"+Environment.NewLine+"Code",
                    
                    //1-5
                    "Item's Name",
                    "Foreign Name",
                    "Document#", 
                    "",
                    "Date",
                    
                    //6-9
                    "Quantity",
                    "UoM",
                    "Cancellation Unit Price",
                    "Cancellation Total Price"
                },
                new int[] 
                {
                    //0
                    80,

                    //1-5
                    200, 150, 140, 20, 80,
                    
                    //6-9
                    80, 80, 120, 120
                }
            );
            Sm.GrdColButton(Grd4, new int[] { 4 });
            Sm.GrdFormatDate(Grd4, new int[] { 5 });
            Sm.GrdFormatDec(Grd4, new int[] { 6, 8, 9 }, 0);
            Sm.GrdColInvisible(Grd4, new int[] { 0, 4 }, false);
            if (!mIsPOShowCancelationUnitPrice)
            {
                Sm.GrdColInvisible(Grd4, new int[] { 8, 9 }, false);
            }
            Sm.GrdColReadOnly(true, true, Grd4, new int[] { 0, 1, 2, 3, 5, 6, 7, 8, 9 });

            #endregion

            #region Grid 5

            Grd5.Cols.Count = 5;
            Sm.GrdHdrWithColWidth(
                    Grd5,
                    new string[] 
                    {
                        //0
                        "No",

                        //1-4
                        "User", 
                        "Status",
                        "Date",
                        "Remark"
                    },
                    new int[] { 50, 150, 100, 100, 200 }
                );
            Sm.GrdFormatDate(Grd5, new int[] { 3 });
            Sm.GrdColReadOnly(Grd5, new int[] { 0, 1, 2, 3, 4 });

            #endregion

            #region Grid 6

            Grd6.Cols.Count = 8;
            Sm.GrdHdrWithColWidth(
                    Grd6,
                    new string[]
                    {
                        //0
                        "D No",
                        //1-5
                        "U",
                        "File Name",
                        "D",
                        "Upload By",
                        "Date",
                        //6
                        "Time",
                        "File Name 2"
                    },
                        new int[]
                    {
                        0,
                        20, 250, 20, 150, 100, 
                        100, 100
                    }
                );
            Sm.GrdColInvisible(Grd6, new int[] { 0, 7 }, false);
            Sm.GrdColButton(Grd6, new int[] { 1 }, "1");
            Sm.GrdColButton(Grd6, new int[] { 3 }, "2");
            Sm.GrdFormatDate(Grd6, new int[] { 5 });
            Sm.GrdFormatTime(Grd6, new int[] { 6 });
            Sm.GrdColReadOnly(Grd6, new int[] { 0, 2, 4, 5, 6, 7 });


            #endregion

            #region Grid 7

            Grd7.Cols.Count = 9;
            Sm.GrdHdrWithColWidth(
                Grd7,
                new string[]
                {
                    //0
                    "D No",
                    //1-5
                    "U",
                    "",
                    "File Name",
                    "D",
                    "Upload By",
                    //6-8
                    "Date",
                    "Time",
                    "File Name 2"
                },
                    new int[]
                {
                    0,
                    20, 150, 250, 20, 150,
                    100, 100, 100
                }
            );
            Sm.GrdColInvisible(Grd7, new int[] { 0, 8 }, false);
            Sm.GrdColButton(Grd7, new int[] { 1 }, "1");
            Sm.GrdColButton(Grd7, new int[] { 4 }, "2");
            Sm.GrdFormatDate(Grd7, new int[] { 6 });
            Sm.GrdFormatTime(Grd7, new int[] { 7 });
            Sm.GrdColReadOnly(Grd7, new int[] { 0, 2, 3, 5, 6, 7, 8 });


            #endregion
        }

        override protected void HideInfoInGrd()
        {
            if(!mIsBOMShowSpecifications)
                Sm.GrdColInvisible(Grd1, new int[] { 7, 12, 14, 21, 24, 11, 13, 10 }, !ChkHideInfoInGrd.Checked); 
            else
                Sm.GrdColInvisible(Grd1, new int[] { 7, 12, 14, 21, 24, 11, 10 }, !ChkHideInfoInGrd.Checked);

            if (!mIsAutoGeneratePurchaseLocalDocNo || !mIsPOUseContract)
                Sm.GrdColInvisible(Grd1, new int[] { 9 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd2, new int[] { 2, 5, 8 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd3, new int[] { 0, 4, 11 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd4, new int[] { 0, 4 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        ChkImportInd, DteDocDt, TxtLocalDocNo, LueVdCode, LueVdContactPersonName, 
                        MeeShipTo, MeeBillTo, MeeRemark, LueTaxCode1, LueTaxCode2, 
                        LueTaxCode3, TxtTaxAmt, TxtCustomsTaxAmt, TxtDiscountAmt, TxtAmt, TxtFile, ChkFile,
                        LueContract, LueSignBy
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 5, 6, 7, 8, 9, 10, 11, 12, 13, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36 });
                    Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8 });
                    Sm.GrdColReadOnly(true, true, Grd3, new int[] { 0, 1, 2, 3, 5, 7, 8, 9, 10, 12, 13, 14 });
                    Sm.GrdColReadOnly(true, true, Grd6, new int[] { 0, 1, 2, 4, 5, 6, 7 });
                    Sm.GrdColReadOnly(true, true, Grd7, new int[] { 0, 1, 2, 3, 5, 6, 7, 8 });
                    BtnVdContactPersonName.Enabled = false;
                    BtnFile.Enabled = false;
                    BtnDownload.Enabled = false;
                    if (TxtDocNo.Text.Length > 0 && mIsPOAllowToUploadFile)
                        BtnDownload.Enabled = true;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        ChkImportInd, DteDocDt, LueVdCode, MeeShipTo, MeeBillTo, 
                        MeeRemark, LueTaxCode1, LueTaxCode2, LueTaxCode3, LueContract, LueSignBy
                    }, false);
                    if (!mIsAutoGeneratePurchaseLocalDocNo || !mIsPOUseContract)
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ TxtLocalDocNo}, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 19, 27, 28, 29, 31, 33, 34, 36 });
                    Sm.GrdColInvisible(Grd1, new int[] { 3 }, false);
                    BtnVdContactPersonName.Enabled = true;
                    if (mIsPOAllowToUploadFile)
                    {
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtFile }, true);
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> { ChkFile }, false);
                        BtnFile.Enabled = true;
                        BtnDownload.Enabled = true;
                    }

                    if (mIsPOUseContract)
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtLocalDocNo }, true);
                    Sm.GrdColReadOnly(false, true, Grd6, new int[] { 1, 3 });
                    Sm.GrdColReadOnly(true, true, Grd7, new int[] { 0, 1, 2, 3, 5, 6, 7, 8 });
                    ChkImportInd.Focus();
                    break;
                case mState.Edit:
                    if (mIsPOAllowToUploadFile)
                    {
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtFile, ChkFile }, true);
                        BtnFile.Enabled = true;
                        BtnDownload.Enabled = true;
                    }
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1,3 });
                    Sm.GrdColReadOnly(true, true, Grd6, new int[] { 0, 1, 2, 4, 5, 6, 7 });
                    Sm.GrdColReadOnly(false, true, Grd7, new int[] { 1, 4 });
                    Sm.GrdColInvisible(Grd1, new int[] { 3 }, true);
                    SetGrdRowReadOnly(Grd7);
                    TxtDocNo.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            mInitFile = string.Empty;
            mlEstRecvDt.Clear();
            mSiteCode = string.Empty;
            mDetailDate = string.Empty;
            mHeaderDate = string.Empty;
            compare1 = string.Empty;
            compare2 = string.Empty;
            InsertInd = "0";
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, TxtStatus, TxtLocalDocNo, LueVdCode, 
                LueVdContactPersonName, TxtSiteCode, MeeShipTo, MeeBillTo, MeeRemark,  
                LueTaxCode1, LueTaxCode2, LueTaxCode3, TxtFile, LueContract, LueSignBy
            });
            ChkImportInd.Checked = false;
            ChkFile.Checked = false;
            BtnVdContactPersonName.Enabled = false;
            PbUpload.Value = 0;
           
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
            {
                TxtTaxAmt, TxtCustomsTaxAmt, TxtDiscountAmt, TxtAmt
            }, 0);
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ClearData2()
        {
            if (mIsBillToDefaultByEntity)
            {
                Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
                {
                    LueVdContactPersonName, LueTaxCode1, LueTaxCode2, LueTaxCode3
                });
            }
            else
            {
                Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
                {
                    LueVdContactPersonName, MeeBillTo, LueTaxCode1, LueTaxCode2, LueTaxCode3
                });
            }
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
            {
                TxtTaxAmt, TxtCustomsTaxAmt, TxtDiscountAmt, TxtAmt 
            }, 0);
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ClearGrd()
        {
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
            Sm.SetGrdBoolValueFalse(ref Grd1, 0, new int[] { 1, 2, 43 });
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 19, 26, 27, 28, 29, 30 });
            SetSeqNo();

            Grd2.Rows.Clear();
            Grd2.Rows.Count = 1;
            Sm.SetGrdBoolValueFalse(ref Grd2, 0, new int[] { 2 });
            Sm.SetGrdNumValueZero(ref Grd2, 0, new int[] { 4 });
            
            Grd3.Rows.Clear();
            Grd3.Rows.Count = 1;
            Sm.SetGrdBoolValueFalse(ref Grd3, 0, new int[] { 8 });
            Sm.SetGrdNumValueZero(ref Grd3, 0, new int[] { 12, 13, 14 });

            Grd4.Rows.Clear();
            Grd4.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd4, 0, new int[] { 6 });

            Grd5.Rows.Clear();

            Grd6.Rows.Clear();
            Grd6.Rows.Count = 1;

            Grd7.Rows.Clear();
            Grd7.Rows.Count = 2;
            Grd7.Cells[0, 2].Value = "PKS";
            Grd7.Cells[1, 2].Value = "PO Signed";
            Grd7.Cells[0, 0].Value = "001";
            Grd7.Cells[1, 0].Value = "002";
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmPOFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
               
                ClearData();
                InsertInd = "1";
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                SetLueVdCode(ref LueVdCode, "");

               string ShipTo = Sm.GetValue(
                    "Select Concat ((Select ParValue From TblParameter Where Parcode='ReportTitle1'),'\n'," +
                    "(Select ParValue From TblParameter Where Parcode='ReportTitle2'),'\n'," +
                    "(Select ParValue From TblParameter Where Parcode='ReportTitle3'),'\n'," +
                    "(Select ParValue From TblParameter Where Parcode='ReportTitle4'))"
                    );
               string PoNote = Sm.GetParameter("PONotes");
               string PoNoteEng = Sm.GetParameter("PONotesEng");
                if (mIsShipToDefaultByCompany)
                {
                     MeeShipTo.Text = "" + ShipTo + "";
                     MeeRemark.Text = "" +PoNote+ "";
                }

                string BillTo = Sm.GetValue(
                    "Select EntName From TblEntity "+
                    "Where CreateDt = (Select min(createDt) From tblEntity limit 1) "+
                    "Limit 1"
                    );
                if (mIsBillToDefaultByEntity)
                {
                    MeeBillTo.Text = "" + BillTo + "";
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e); 
                else
                    CancelData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            try
            {
                if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false) || 
                    Sm.StdMsgYN("Print", "") == DialogResult.No ||
                    IsPOStatusOutstanding() ||
                    IsPOStatusCancelled() 
                    ) return;

                ParPrint(TxtDocNo.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnFile_Click(object sender, EventArgs e)
        {

            try
            {
                ChkFile.Checked = true;
                OD.InitialDirectory = "c:";
                OD.Filter = "Image Files(*.PNG;*.JPG;*.JPEG)|*.PNG;*.JPG;*.JPEG|PDF files (*.pdf)|*.pdf|Compressed files (*.rar;*.zip)|*.rar;*.zip";
                OD.FilterIndex = 2;
                OD.ShowDialog();

                TxtFile.Text = OD.FileName;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }

        }

        private void BtnDownload_Click(object sender, EventArgs e)
        {
            DownloadFile2(mHostAddrForFTPClient, mPortForFTPClient, TxtFile.Text, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
            SFD.FileName = TxtFile.Text;
            SFD.DefaultExt = "";
            SFD.AddExtension = true;

            if (!Sm.IsTxtEmpty(TxtFile, "File", false) && downloadedData != null && downloadedData.Length != 0)
            {

                if (SFD.ShowDialog() == DialogResult.OK)
                {
                    Application.DoEvents();

                    //Write the bytes to a file
                    FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                    newFile.Write(downloadedData, 0, downloadedData.Length);
                    newFile.Close();
                    MessageBox.Show("Saved Successfully");
                }
            }
            else
                MessageBox.Show("No File was Downloaded Yet!");
        }


        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 4 && TxtDocNo.Text.Length == 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ") 
                    && !Sm.IsLueEmpty(LueVdCode, "Vendor")) 
                    Sm.FormShowDialog(new FrmPODlg(this, Sm.GetLue(LueVdCode)));
            }

            if (e.ColIndex == 14 && Sm.GetGrdStr(Grd1, e.RowIndex, 12).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 12);
                f.ShowDialog();
            }

            if (e.ColIndex == 34 && TxtDocNo.Text.Length == 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")
                    && !Sm.IsGrdValueEmpty(Grd1, e.RowIndex, 15, false, "Item is empty."))
                    Sm.FormShowDialog(new FrmPODlg3(this, e.RowIndex));
            }

            if (e.ColIndex == 37 && Sm.GetGrdStr(Grd1, e.RowIndex, 36).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmLOP(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 36);
                f.ShowDialog();
            }

            if (e.ColIndex == 40 && Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length != 0 && Sm.GetGrdDec(Grd1, e.RowIndex, 19) > 0m)
                Sm.FormShowDialog(new FrmPODlg4(this, e.RowIndex, TxtDocNo.Text.Length == 0 && BtnSave.Enabled));

            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    if (TxtDocNo.Text.Length == 0 &&
                        !IsPORequestEmpty(e) &&
                        Sm.IsGrdColSelected(new int[] { 19, 25, 28, 31, 32 }, e.ColIndex))
                    {
                        if (e.ColIndex == 31) Sm.DteRequestEdit(Grd1, DteEstRecvDt, ref fCell, ref fAccept, e);
                        Sm.GrdRequestEdit(Grd1, e.RowIndex);
                    }
                }
                else
                {
                    if (e.ColIndex == 1 && (Sm.GetGrdBool(Grd1, e.RowIndex, 2) || Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length == 0))
                        e.DoDefault = false;
                }
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0)
            {
                if (mIsPOEstRecvDtSplitEnabled && Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 7).Length>0)
                { 
                    var PORequestDocNo = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 7);
                    var PORequestDNo = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 8);

                    mlEstRecvDt.RemoveAll(x => 
                        Sm.CompareStr(x.PORequestDocNo, PORequestDocNo) &&
                        Sm.CompareStr(x.PORequestDNo, PORequestDNo)
                        );
                }

                Sm.GrdRemoveRow(Grd1, e, BtnSave);
                ComputeTaxAmt();
                SetSeqNo();
            }
            Sm.GrdEnter(Grd1, e);
            Sm.GrdTabInLastCell(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 4 
                && TxtDocNo.Text.Length == 0 
                && !Sm.IsLueEmpty(LueVdCode, "Vendor"))
                Sm.FormShowDialog(new FrmPODlg(this, Sm.GetLue(LueVdCode)));

            if (e.ColIndex == 14 && Sm.GetGrdStr(Grd1, e.RowIndex, 12).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 12);
                f.ShowDialog();
            }

            if (e.ColIndex == 34 && TxtDocNo.Text.Length == 0)
            {
                if (!Sm.IsGrdValueEmpty(Grd1, e.RowIndex, 15, false, "Item is empty."))
                    Sm.FormShowDialog(new FrmPODlg3(this, e.RowIndex));
            }

            if (e.ColIndex == 37 && Sm.GetGrdStr(Grd1, e.RowIndex, 36).Length != 0)
            {
                var f = new FrmLOP(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 36);
                f.ShowDialog();
            }

            if (e.ColIndex == 38 && Sm.GetGrdStr(Grd1, e.RowIndex, 21).Length != 0)
            {
                string Filename = Sm.GetValue("Select Filename1 From TblQtHdr Where DocNo = '" + Sm.GetGrdStr(Grd1, e.RowIndex, 21) + "'");
                if (Filename.Length > 0)
                    DownloadFileKu(Filename, "First File ");

                string Filename2 = Sm.GetValue("Select Filename2 From TblQtHdr Where DocNo = '" + Sm.GetGrdStr(Grd1, e.RowIndex, 21) + "'");
                if (Filename2.Length > 0)
                    DownloadFileKu(Filename2, "Second File ");

                string Filename3 = Sm.GetValue("Select Filename3 From TblQtHdr Where DocNo = '" + Sm.GetGrdStr(Grd1, e.RowIndex, 21) + "'");
                if (Filename3.Length > 0)
                    DownloadFileKu(Filename3, "Third File ");

                string Filename4 = Sm.GetValue("Select Filename4 From TblQtHdr Where DocNo = '" + Sm.GetGrdStr(Grd1, e.RowIndex, 21) + "'");
                if (Filename4.Length > 0)
                    DownloadFileKu(Filename4, "Fourth File ");

                string Filename5 = Sm.GetValue("Select Filename5 From TblQtHdr Where DocNo = '" + Sm.GetGrdStr(Grd1, e.RowIndex, 21) + "'");
                if (Filename5.Length > 0)
                    DownloadFileKu(Filename5, "Fifth File ");
            }

            if (e.ColIndex == 40 && Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length != 0 && Sm.GetGrdDec(Grd1, e.RowIndex, 19) > 0m)
                Sm.FormShowDialog(new FrmPODlg4(this, e.RowIndex, TxtDocNo.Text.Length==0 && BtnSave.Enabled));
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 19, 27, 28, 29 }, e);
            Sm.GrdAfterCommitEditTrimString(Grd1, new int[] { 33 }, e);
            if (e.ColIndex == 1 || e.ColIndex == 19 || e.ColIndex == 27 || e.ColIndex == 28 || e.ColIndex == 29)
            {
                ComputeTotal(e.RowIndex);
                ComputeTaxAmt();
            }
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Sm.GetGrdDate(Grd1, 0, 31).Length != 0)
            {
                var EstRecvDt = Sm.ConvertDate(Sm.GetGrdDate(Grd1, 0, 31));
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 12).Length != 0) Grd1.Cells[Row, 31].Value = EstRecvDt;
            }
        }

        override protected void GrdRequestColHdrToolTipText(object sender, iGRequestColHdrToolTipTextEventArgs e)
        {
            if (e.ColIndex == 31)
                e.Text = "Double click title to copy data based on the first row's value.";
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || 
                IsInsertedDataNotValid()) return;
  
            string SubCategory = Sm.GetGrdStr(Grd1, 0, 17);
            string DocNo = string.Empty;

            if (mIsPurchaseRequestForProductionEnabled)
            {
                //0001/IMS/PO-UMUM/04/20 (Untuk Umum)
                //0001/IMS/PO-LOG/04/20 (Untuk Logistik)
                DocNo = GenerateDocNoIMS();
            }
            else
            {
                DocNo = GenerateDocNo(IsProcFormat, Sm.GetDte(DteDocDt), "PO", "TblPOHdr", ref LueTaxCode1, SubCategory);
            }

            string LocalDocNo = (mIsAutoGeneratePurchaseLocalDocNo || mIsPOUseContract) ? string.Empty : TxtLocalDocNo.Text;
            string
                SeqNo = string.Empty,
                DeptCode = string.Empty,
                ItSCCode = string.Empty,
                Mth = string.Empty,
                Yr = string.Empty,
                Revision = string.Empty;

            if (mIsPOUseContract)
            {
                SetLocalDocNoSIER(Sm.GetDte(DteDocDt), ref LocalDocNo);
            }
            else
            {
                if (mIsAutoGeneratePurchaseLocalDocNo)
                {

                    SetLocalDocument(
                        ref SeqNo,
                        ref DeptCode,
                        ref ItSCCode,
                        ref Mth,
                        ref Yr
                    );
                    if (mlLocalDocument.Count == 0) return;
                    if (IsLocalDocumentNotValid(
                            ref SeqNo,
                            ref DeptCode,
                            ref ItSCCode,
                            ref Mth,
                            ref Yr
                        )) return;

                    SetRevision(
                        ref SeqNo,
                        ref DeptCode,
                        ref ItSCCode,
                        ref Mth,
                        ref Yr,
                        ref Revision
                        );

                    if (SeqNo.Length > 0)
                    {
                        SetLocalDocNo(
                            "PO",
                            ref LocalDocNo,
                            ref SeqNo,
                            ref DeptCode,
                            ref ItSCCode,
                            ref Mth,
                            ref Yr,
                            ref Revision
                            );
                    }
                    mlLocalDocument.Clear();
                }
                
            }

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(SavePOHdr(
                DocNo, 
                LocalDocNo,
                SeqNo,
                DeptCode,
                ItSCCode,
                Mth,
                Yr,
                Revision
                ));

            cml.Add(SavePODtl(DocNo));

            //for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            //    if (Sm.GetGrdStr(Grd1, Row, 7).Length > 0) cml.Add(SavePODtl(DocNo, Row));

            if (mIsPOEstRecvDtSplitEnabled && mlEstRecvDt.Count>0)
                cml.Add(SavePODtl2(DocNo));

            cml.Add(UpdateMaterialRequestProcessInd(DocNo));

            if (mIsPOAllowToUploadFile && mPOUploadFileFormat == "2")
            {
                cml.Add(SavePOFile(DocNo));
            }

            Sm.ExecCommands(cml);

            if (mIsPOAllowToUploadFile && TxtFile.Text.Length > 0 && TxtFile.Text != "openFileDialog1")
                ProcessUploadFile(DocNo);

            if (mIsPOAllowToUploadFile && mPOUploadFileFormat == "2")
            {
                for (int Row = 0; Row < Grd6.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd6, Row, 2).Length > 0)
                    {
                        if (mIsPOAllowToUploadFile && Sm.GetGrdStr(Grd6, Row, 2).Length > 0 && Sm.GetGrdStr(Grd6, Row, 2) != "openFileDialog1")
                        {
                            if (Sm.GetGrdStr(Grd6, Row, 2) != Sm.GetGrdStr(Grd6, Row, 7))
                            {
                                UploadFile(DocNo, Row, "1", Sm.GetGrdStr(Grd6, Row, 2));
                            }
                        }
                    }
                }
            }

            if (!mIsPOApprovalActived)
            {
                if (Sm.StdMsgYN("Print", "") == DialogResult.No)
                    ShowData(DocNo);
                else
                {
                    ShowData(DocNo);
                    ParPrint(DocNo);
                }
            }
            else
                ShowData(DocNo);
        }

        #region Generate Local Document

        //set local doc no SIER
        private void SetLocalDocNoSIER(string DocDt, ref string LocalDocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            string Yr = Sm.Left(DocDt, 4),
                   Mth = DocDt.Substring(4, 2),
                   Contract = Sm.GetValue("Select OptDesc From TblOption Where OptCat = 'POContract' And OptCode = @Param ", Sm.GetLue(LueContract)),
                   ContractAbbr = Sm.GetValue("Select OptDesc From TblOption Where OptCat = 'POContractAbbr' And OptCode = @Param ", Contract);

            SQL.AppendLine("Select Concat( ");
	        SQL.AppendLine("    IfNull((  ");
	        SQL.AppendLine("       Select Right(Concat('0000', Convert(LocalDocNo+1, Char)), 4) From (  ");
	        SQL.AppendLine("           Select Convert(Left(LocalDocNo, 4), Decimal) As LocalDocNo From TblPOHdr ");
            SQL.AppendLine("           Where Substring(LocalDocNo, 6, Length(Concat(@POContract, '/',@POContractAbbr)))=Concat(@POContract,'/',@POContractAbbr) ");
            SQL.AppendLine("           And Right(LocalDocNo,4)=@Yr ");
	        SQL.AppendLine("           Order By Left(LocalDocNo, 4) Desc Limit 1 ");
	        SQL.AppendLine("           ) As Temp ");
	        SQL.AppendLine("       ), '0001'), '/', @POContract, '/', @POContractAbbr, '/', @Mth, '/', @Yr ");
            SQL.AppendLine(") As LocalDocNo ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@POContract", Contract);
                Sm.CmParam<String>(ref cm, "@POCOntractAbbr", ContractAbbr);
                Sm.CmParam<String>(ref cm, "@Mth", Sm.MonthRoman(Mth));
                Sm.CmParam<String>(ref cm, "@Yr", Yr);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "LocalDocNo" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        LocalDocNo = Sm.DrStr(dr, c[0]);
                    }
                }
                dr.Close();
            }

        }

        private void SetLocalDocNo(
            string DocType,
            ref string LocalDocNo,
            ref string SeqNo,
            ref string DeptCode,
            ref string ItSCCode,
            ref string Mth,
            ref string Yr,
            ref string Revision
        )
        {
            var DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='" + DocType + "'");
            var ShortCode = Sm.GetValue("Select IfNull(ShortCode, DeptCode) From TblDepartment Where DeptCode='" + DeptCode + "'");
            LocalDocNo = SeqNo + "/" + DocAbbr + "/" + ShortCode + "/" + ItSCCode + "/" + Mth + "/" + Yr;
            if (Revision.Length > 0 && Revision != "0")
                LocalDocNo = LocalDocNo + "/R" + Revision;
        }

        private void SetRevision(
            ref string SeqNo,
            ref string DeptCode,
            ref string ItSCCode,
            ref string Mth,
            ref string Yr,
            ref string Revision
        )
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select IfNull(Revision, '0') ");
            SQL.AppendLine("From TblPOHdr ");
            SQL.AppendLine("Where SeqNo Is Not Null ");
            SQL.AppendLine("And SeqNo=@SeqNo ");
            SQL.AppendLine("And DeptCode=@DeptCode ");
            SQL.AppendLine("And ItSCCode=@ItSCCode ");
            SQL.AppendLine("And Mth=@Mth ");
            SQL.AppendLine("And Yr=@Yr ");
            SQL.AppendLine("Order By Revision Desc Limit 1;");

            var cm = new MySqlCommand(){ CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@SeqNo", SeqNo);
            Sm.CmParam<String>(ref cm, "@DeptCode", DeptCode);
            Sm.CmParam<String>(ref cm, "@ItSCCode", ItSCCode);
            Sm.CmParam<String>(ref cm, "@Mth", Mth);
            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            
            Revision = Sm.GetValue(cm);
            if (Revision.Length == 0)
                Revision = "0";
            else
                Revision = (int.Parse(Revision) + 1).ToString();
        }

        private bool IsLocalDocumentNotValid(
            ref string SeqNo,
            ref string DeptCode,
            ref string ItSCCode,
            ref string Mth,
            ref string Yr
            )
        {
            if (SeqNo.Length==0) return false;

            foreach (var x in mlLocalDocument.Where(x => x.SeqNo.Length>0))
            {
                if (!(
                  Sm.CompareStr(SeqNo, x.SeqNo) &&
                  Sm.CompareStr(DeptCode, x.DeptCode) &&
                  Sm.CompareStr(ItSCCode, x.ItSCCode) &&
                  Sm.CompareStr(Mth, x.Mth) &&
                  Sm.CompareStr(Yr, x.Yr) 
                ))
               {
                   Sm.StdMsg(mMsgType.Warning, 
                       "Document# : " + x.DocNo + Environment.NewLine +
                       "Local# : " + x.LocalDocNo + Environment.NewLine + Environment.NewLine +
                       "Invalid data.");
                    return true;
               }
            }
            return false;
        }

        private void SetLocalDocument(
            ref string SeqNo,
            ref string DeptCode,
            ref string ItSCCode,
            ref string Mth,
            ref string Yr)
        {
            mlLocalDocument.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty;
            bool IsFirst = true;

            if (Grd1.Rows.Count >= 1)
            {
                int No = 1;
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 7).Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(DocNo=@DocNo" + No.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@DocNo" + No.ToString(), Sm.GetGrdStr(Grd1, Row, 7));
                        No += 1;
                    }
                }
            }
            Filter = " Where (" + Filter + ")";

            SQL.AppendLine("Select DocNo, LocalDocNo, SeqNo, DeptCode, ItSCCode, Mth, Yr, Revision ");
            SQL.AppendLine("From TblPORequestHdr " + Filter);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "DocNo", 

                    //1-5
                    "LocalDocNo", "SeqNo", "DeptCode", "ItSCCode", "Mth", 
                    
                    //6-7
                    "Yr", "Revision" 
                });
                if (dr.HasRows)
                {
                    string 
                        DocNoTemp = string.Empty,
                        LocalDocNoTemp = string.Empty,
                        SeqNoTemp = string.Empty,
                        DeptCodeTemp = string.Empty,
                        ItSCCodeTemp = string.Empty,
                        MthTemp = string.Empty,
                        YrTemp = string.Empty,
                        RevisionTemp = string.Empty;

                    while (dr.Read())
                    {
                        DocNoTemp = Sm.DrStr(dr, c[0]);
                        LocalDocNoTemp = Sm.DrStr(dr, c[1]);
                        SeqNoTemp = Sm.DrStr(dr, c[2]);
                        DeptCodeTemp = Sm.DrStr(dr, c[3]);
                        ItSCCodeTemp = Sm.DrStr(dr, c[4]);
                        MthTemp = Sm.DrStr(dr, c[5]);
                        YrTemp = Sm.DrStr(dr, c[6]);
                        RevisionTemp = Sm.DrStr(dr, c[7]);   
                        
                        mlLocalDocument.Add(new LocalDocument()
                        {
                            DocNo = DocNoTemp,
                            LocalDocNo = LocalDocNoTemp,
                            SeqNo = SeqNoTemp,
                            DeptCode = DeptCodeTemp,
                            ItSCCode = ItSCCodeTemp,
                            Mth = MthTemp,
                            Yr = YrTemp,
                            Revision = RevisionTemp
                        });

                        if (IsFirst && SeqNoTemp.Length > 0)
                        {
                            SeqNo = SeqNoTemp;
                            DeptCode = DeptCodeTemp;
                            ItSCCode = ItSCCodeTemp;
                            Mth = MthTemp;
                            Yr = YrTemp;
                            IsFirst = false;
                        }
                    }
                }
                dr.Close();
            }
        }

        #endregion

        private bool IsInsertedDataNotValid()
        {
            IsSubCategoryNull();
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                (mIsPurchaseOrderRemarkMandatory && Sm.IsMeeEmpty(MeeRemark, "Remark"))||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                IsQtyNotValid() ||
                IsSubcategoryDifferent() ||
                IsSiteNotValid() ||
                IsDateNotValid() ||
                (mIsPOGrandTotalComparedToBudget && IsGrandTotalNotValid())||
                (mIsPOAllowToUploadFile && IsUploadFileNotValid()) ||
                IsPOWithTaxValidationInvalid() ||
                (mIsPOEstRecvDtSplitEnabled && IsQtyEstRecvDtNotValid()) ||
                (mIsPurchaseRequestForProductionEnabled && IsPurchaseRequestInvalid())||
                (mIsPOUseContract && Sm.IsLueEmpty(LueContract, "Contract")) ||
                (mIsContactPersonPurchaseOrderMandatory && Sm.IsLueEmpty(LueVdContactPersonName, "Contact Person"))
                ;
        }

        private bool IsPurchaseRequestInvalid()
        {
            //0001/IMS/PO-UMUM/04/20 (Untuk Umum)
            //0001/IMS/PO-LOG/04/20 (Untuk Logistik)
            var IsProduction = false;
            for (int r = 0; r<Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 7).Length > 0)
                { 
                    if (r==0) 
                    {
                        IsProduction = Sm.GetGrdBool(Grd1, r, 43);
                    }
                    else
                    {
                        if (IsProduction != Sm.GetGrdBool(Grd1, r, 43))
                        {
                            Sm.StdMsg(mMsgType.Warning,
                              "PO Request# : " + Sm.GetGrdStr(Grd1, r, 7) + Environment.NewLine +
                              "Item's Code : " + Sm.GetGrdStr(Grd1, r, 12) + Environment.NewLine +
                              "Item's Name : " + Sm.GetGrdStr(Grd1, r, 15) + Environment.NewLine +
                              "Invalid purchase request's type. 1 Document shoud have 1 type (Umum/Logistik).");
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        private bool IsQtyEstRecvDtNotValid()
        {
            if (mlEstRecvDt.Count == 0) return false;

            string PORequestDocNo = string.Empty, PORequestDNo = string.Empty;
            decimal Qty = 0m, QtyTemp = 0m;
            for (int r = 0; r< Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 7).Length > 0)
                { 
                    PORequestDocNo = Sm.GetGrdStr(Grd1, r, 7);
                    PORequestDNo = Sm.GetGrdStr(Grd1, r, 8);
                    Qty = Sm.GetGrdDec(Grd1, r, 19);
                    QtyTemp = mlEstRecvDt.Where(w =>
                        Sm.CompareStr(w.PORequestDocNo, PORequestDocNo) &&
                        Sm.CompareStr(w.PORequestDNo, PORequestDNo)).Sum(s => s.Qty);

                    if (Qty != QtyTemp)
                    {
                        Sm.StdMsg(mMsgType.Warning,
                          "PO Request# : " + PORequestDocNo + Environment.NewLine +
                          "Item's Code : " + Sm.GetGrdStr(Grd1, r, 12) + Environment.NewLine +
                          "Item's Name : " + Sm.GetGrdStr(Grd1, r, 15) + Environment.NewLine +
                          "PO's Quantity : " + Sm.FormatNum(Qty, 0) + Environment.NewLine +
                          "PO's Total Quantity For Estimated Received Date: " + Sm.FormatNum(QtyTemp, 0) + Environment.NewLine + Environment.NewLine +
                          "Invalid quantity.");
                        return true;
                    }
                }
            }
            return false;
        }

        private bool IsPOWithTaxValidationInvalid()
        {
            if (!mIsShowPOWithTaxWarning || Sm.GetLue(LueTaxCode1).Length>0) return false;

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            //string ItCode = string.Empty, Filter = string.Empty;

            //for (int r = 0; r < Grd1.Rows.Count; r++)
            //{
            //    ItCode = Sm.GetGrdStr(Grd1, r, 12);
            //    if (ItCode.Length > 0)
            //    {
            //        if (Filter.Length > 0) Filter += " Or ";
            //        Filter += "(D.ItCode0=@ItCode0" + r.ToString() + ") ";
            //        Sm.CmParam<String>(ref cm, "@ItCode0" + r.ToString(), ItCode);  
            //    }
            //}

            //if (Filter.Length > 0)
            //    Filter = " And (" + Filter + ") ";
            //else
            //    Filter = " And 1=0 ";

            //SQL.AppendLine("Select 1 ");
            //SQL.AppendLine("From TblPOHdr A ");
            //SQL.AppendLine("Inner Join TblPODtl B On A.DocNo=B.DocNo And B.CancelInd='N' ");
            //SQL.AppendLine("Inner Join TblPORequestDtl C On B.PORequestDocNo=C.DocNo And B.PORequestDNo=C.DNo ");
            //SQL.AppendLine("Inner Join TblMaterialRequestDtl D On C.MaterialRequestDocNo=D.DocNo And C.MaterialRequestDNo=D.DNo ");
            //SQL.AppendLine(Filter);
            //SQL.AppendLine("Where A.TaxCode Is Not Null ");
            //SQL.AppendLine("And A.VdCode=@VdCode ");
            //SQL.AppendLine("Limit 1;");
          
            SQL.AppendLine("Select 1 ");
            SQL.AppendLine("From TblPOHdr A ");
            SQL.AppendLine("Inner Join TblPODtl B On A.DocNo=B.DocNo And B.CancelInd='N' ");
            SQL.AppendLine("Where A.TaxCode1 Is Not Null ");
            SQL.AppendLine("And A.VdCode=@VdCode ");
            SQL.AppendLine("Limit 1;");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@VdCode", Sm.GetLue(LueVdCode));  

            if (Sm.IsDataExist(cm))
            {
                if (Sm.StdMsgYN("Question",
                    "This customer ever issue PO with tax." + Environment.NewLine +
                    "This PO does not have any tax." + Environment.NewLine +
                    "Do you want to continue to save this data ?"
                    ) == DialogResult.No)
                    return true;
            }
            return false;
        }

        private bool IsDateNotValid()
        {
            mHeaderDate = Sm.GetDte(DteDocDt);

            if (mIsComparedToDetailDate)
            {
                if (Grd1.Rows.Count != 1)
                {
                    //loop grid untuk mendapatkan tanggal terbaru
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    {
                        if (Sm.CompareDtTm(compare1, Sm.GetGrdDate(Grd1, Row, 10)) < 0)
                        {
                            compare1 = Sm.GetGrdDate(Grd1, Row, 10);
                            compare2 = compare1;
                        }
                        else
                            compare2 = compare1;
                    }
                    mDetailDate = compare2;

                    if (Sm.CompareDtTm(mHeaderDate, mDetailDate) < 0)
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "Date should be the same or greater than PO Request Date.");
                        DteDocDt.Focus();
                        return true;
                    }
                }
            }
            return false;
        }

        private bool IsSiteNotValid()
        {
            mSiteCode = Sm.GetGrdStr(Grd1, 0, 5);
            TxtSiteCode.EditValue = Sm.GetGrdStr(Grd1, 0, 6);

            if (!mIsSiteMandatory) return false;

            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (!Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 5), mSiteCode))
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "PO Request# : " + Sm.GetGrdStr(Grd1, Row, 7) + Environment.NewLine +
                            "Item Code : " + Sm.GetGrdStr(Grd1, Row, 12) + Environment.NewLine +
                            "Item Name : " + Sm.GetGrdStr(Grd1, Row, 15) + Environment.NewLine +
                            "Site Name : " + Sm.GetGrdStr(Grd1, Row, 6) + Environment.NewLine + Environment.NewLine +
                            "Invalid site."
                            );
                        return true;
                    }
                }
            }

            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 item.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, Row, 7, false, "PO request# is empty.") ||
                    Sm.IsGrdValueEmpty(Grd1, Row, 19, true, "Quantity is empty.") ||
                    Sm.IsGrdValueEmpty(Grd1, Row, 31, false, "Estimated received date is empty."))
                    return true;
            }
            return false;
        }

        private bool IsQtyNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 12).Length > 0 && IsQtyNotValid2(Row)) 
                    return true;
            return false;
        }

        private bool IsQtyNotValid2(int Row)
        {
            StringBuilder SQL = new StringBuilder();

            SQL.AppendLine("Select Concat('PO Request# : ', A.DocNo, '\n', 'Item : ', D.ItName, '\n', 'Outstanding Quantity : ', Convert(Format(A.OutstandingQty, 2) Using utf8), '\n', 'PO Quantity : ', Convert(Format(A.Qty, 2) Using utf8)) As GetValue ");
            SQL.AppendLine("From (");
            SQL.AppendLine("    Select @DocNo As DocNo, @DNo As DNo, @Qty As Qty, ");
            SQL.AppendLine("    IfNull((Select Qty From TblPORequestDtl Where DocNo=@DocNo And DNo=@DNo), 0) - ");
            SQL.AppendLine("    IfNull(( ");
            SQL.AppendLine("        Select Sum(Qty) From TblPODtl ");
            SQL.AppendLine("        Where CancelInd='N' And PORequestDocNo=@DocNo And PORequestDNo=@DNo ");
            SQL.AppendLine("    ), 0) - ");
            SQL.AppendLine("    IfNull(( ");
            SQL.AppendLine("        Select Sum(T1.Qty) ");
            SQL.AppendLine("        From TblPOQtyCancel T1 ");
            SQL.AppendLine("        Inner Join TblPODtl T2 ");
            SQL.AppendLine("            On T1.PODocNo=T2.DocNo And T1.PODNo=T2.DNo ");
            SQL.AppendLine("            And T2.PORequestDocNo=@DocNo And T2.PORequestDNo=@DNo ");
            SQL.AppendLine("        Where T1.CancelInd='N' ");
            SQL.AppendLine("    ), 0) ");
            SQL.AppendLine("    As OutstandingQty ");
            SQL.AppendLine(") A ");
            SQL.AppendLine("Inner Join TblPORequestDtl B On A.DocNo=B.DocNo And A.DNo=B.DNo ");
            SQL.AppendLine("Inner Join TblMaterialRequestDtl C On B.MaterialRequestDocNo=C.DocNo And B.MaterialRequestDNo=C.DNo ");
            SQL.AppendLine("Inner Join TblItem D On C.ItCode=D.ItCode ");
            SQL.AppendLine("Where A.Qty>A.OutstandingQty ");

            var cm = new MySqlCommand(){ CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", Sm.GetGrdStr(Grd1, Row, 7));
            Sm.CmParam<String>(ref cm, "@DNo", Sm.GetGrdStr(Grd1, Row, 8));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 19));

            string Msg = Sm.GetValue(cm);

            if (Msg.Length != 0)
            {
                Sm.StdMsg(mMsgType.Warning, 
                    Msg + Environment.NewLine + Environment.NewLine + 
                    "PO quantity is bigger than outstanding quantity.");
                return true;
            }
            return false;
        }

        private bool IsGrandTotalNotValid()
        {
            decimal GrandTotal = Decimal.Parse(TxtAmt.Text);

            string Budget = string.Empty;
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select convert(Format(Ifnull(Z1.Amt, 0)- (IfNull(Z5.Amt, 0.00)), 2) using utf8) As BudgetvsPO ");
            SQL.AppendLine("From TblBudget Z1 ");
            SQL.AppendLine("Inner Join TblDepartment Z2 On Z1.DeptCode = Z2.DeptCode ");
            SQL.AppendLine("And Z2.DeptCode In ( ");
            SQL.AppendLine("    Select Distinct T2.DeptCode ");
            SQL.AppendLine("    From TblPoRequestDtl T1 ");
            SQL.AppendLine("    Inner Join TblMaterialRequestHdr T2 ");
            SQL.AppendLine("        On T1.MaterialRequestDocNo = T2.DocNo ");
            SQL.AppendLine("    Where Find_In_Set(T1.DocNo, @PORequestDocNo) ");
            SQL.AppendLine(") ");

            //MR yg sudah dibuat PO dengan harga PO

            #region Old Code
            //SQL.AppendLine("Left Join (");
            //SQL.AppendLine("    Select A.DeptCode, Sum( ");
            //SQL.AppendLine("    (D.Qty*G.UPrice) ");
            //SQL.AppendLine("    * Case When F.CurCode=@MainCurCode Then 1.00 Else ");
            //SQL.AppendLine("    IfNull(( ");
            //SQL.AppendLine("        Select Amt From TblCurrencyRate ");
            //SQL.AppendLine("        Where RateDt<=E.DocDt And CurCode1=F.CurCode And CurCode2=@MainCurCode ");
            //SQL.AppendLine("        Order By RateDt Desc Limit 1 ");
            //SQL.AppendLine("    ), 0.00) End ");
            //SQL.AppendLine("    ) As Amt ");
            //SQL.AppendLine("    From TblMaterialRequestHdr A ");
            //SQL.AppendLine("    Inner Join TblMaterialRequestDtl B ");
            //SQL.AppendLine("        On A.DocNo=B.DocNo ");
            //SQL.AppendLine("        And B.CancelInd='N' ");
            //SQL.AppendLine("        And B.Status='A' ");
            //SQL.AppendLine("    Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.CancelInd='N' And C.Status='A' ");
            //SQL.AppendLine("    Inner Join TblPODtl D On C.DocNo=D.PORequestDocNo And C.DNo=D.PORequestDNo And D.CancelInd='N' ");
            //SQL.AppendLine("    Inner Join TblPOHdr E On D.DocNo=E.DocNo And E.Status='A' ");
            //SQL.AppendLine("    Inner Join TblQtHdr F On C.QtDocNo=F.DocNo ");
            //SQL.AppendLine("    Inner Join TblQtDtl G On C.QtDocNo=G.DocNo And C.QtDNo=G.DNo ");
            //SQL.AppendLine("    Where A.Status='A' ");
            //SQL.AppendLine("    And A.Reqtype='1' ");
            //SQL.AppendLine("    And Left(A.DocDt, 6)=Concat(@Yr, @Mth) ");
            //SQL.AppendLine("    Group By A.DeptCode ");
            //SQL.AppendLine(") Z5 On Z1.DeptCode=Z5.DeptCode ");
            #endregion

            SQL.AppendLine("Left Join (");
            SQL.AppendLine("    Select X2.DeptCode, Sum( ");
            SQL.AppendLine("    X2.Amt -  ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("      IfNull( X1.DiscountAmt *  ");
            SQL.AppendLine("          Case When X1.CurCode = @MainCurCode Then 1.00 Else ");
            SQL.AppendLine("          IfNull((  ");
            SQL.AppendLine("              Select Amt From TblCurrencyRate  ");
            SQL.AppendLine("              Where RateDt<=X1.DocDt And CurCode1=X1.CurCode And CurCode2=@MainCurCode  ");
            SQL.AppendLine("              Order By RateDt Desc Limit 1  ");
	        SQL.AppendLine("        ), 0.00) End ");
            SQL.AppendLine("      , 0.00)   ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("    ) As Amt ");
            SQL.AppendLine("    From TblPOHdr X1 ");
            SQL.AppendLine("    Inner Join ( ");
	        SQL.AppendLine("        Select A.DeptCode, E.DocNo, Sum(  ");
            SQL.AppendLine("        ((((100.00-D.Discount)*0.01)*(D.Qty*G.UPrice))-D.DiscountAmt+D.RoundingValue)  ");
	        SQL.AppendLine("        * Case When F.CurCode=@MainCurCode Then 1.00 Else  ");
	        SQL.AppendLine("        IfNull((  ");
	        SQL.AppendLine("            Select Amt From TblCurrencyRate  ");
	        SQL.AppendLine("            Where RateDt<=E.DocDt And CurCode1=F.CurCode And CurCode2=@MainCurCode  ");
	        SQL.AppendLine("            Order By RateDt Desc Limit 1  ");
	        SQL.AppendLine("        ), 0.00) End  ");
	        SQL.AppendLine("        ) As Amt  ");
	        SQL.AppendLine("        From TblMaterialRequestHdr A  ");
	        SQL.AppendLine("        Inner Join TblMaterialRequestDtl B  ");
	        SQL.AppendLine("            On A.DocNo=B.DocNo ");
		    SQL.AppendLine("             And Left(A.DocDt, 6)=Concat(@Yr, @Mth)  ");
	        SQL.AppendLine("            And B.CancelInd='N'  ");
	        SQL.AppendLine("            And B.Status='A'  ");
	        SQL.AppendLine("            And A.Status='A'  ");
	        SQL.AppendLine("            And A.Reqtype='1'   ");
	        SQL.AppendLine("        Inner Join TblPORequestDtl C On B.DocNo=C.MaterialRequestDocNo And B.DNo=C.MaterialRequestDNo And C.CancelInd='N' And C.Status='A'  ");
	        SQL.AppendLine("        Inner Join TblPODtl D On C.DocNo=D.PORequestDocNo And C.DNo=D.PORequestDNo And D.CancelInd='N'  ");
	        SQL.AppendLine("        Inner Join TblPOHdr E On D.DocNo=E.DocNo And E.Status='A'  ");
	        SQL.AppendLine("        Inner Join TblQtHdr F On C.QtDocNo=F.DocNo  ");
	        SQL.AppendLine("        Inner Join TblQtDtl G On C.QtDocNo=G.DocNo And C.QtDNo=G.DNo  ");
	        SQL.AppendLine("        Group By A.DeptCode, E.DocNo ");
            SQL.AppendLine("    ) X2 On X1.DocNo = X2.DocNo ");
            SQL.AppendLine("    Group By X2.DeptCode ");
            SQL.AppendLine(") Z5 On Z1.DeptCode=Z5.DeptCode ");

            SQL.AppendLine("Where Z1.ApprovalDt Is Not Null ");
            SQL.AppendLine("And Z1.Yr = @Yr ");
            SQL.AppendLine("And Z1.Mth = @Mth ");
            if (mBudgetBasedOn == "1" && mIsFilterByDept)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupDepartment ");
                SQL.AppendLine("    Where DeptCode=Z1.DeptCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode = @UserCode");
                SQL.AppendLine("		)");
                SQL.AppendLine(" )  ");
            }

            SQL.AppendLine("; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@Mth", Sm.GetDte(DteDocDt).Substring(4, 2));
                Sm.CmParam<String>(ref cm, "@Yr", Sm.GetDte(DteDocDt).Substring(0, 4));
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@PORequestDocNo", Sm.GetGrdStr(Grd1, 0, 7));
                Sm.CmParam<String>(ref cm, "@MainCurCode", mMainCurCode);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "BudgetvsPO" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        Budget = Sm.DrStr(dr, c[0]);
                    }
                }
                dr.Close();
            }

            decimal Budget2 = Budget.Length <= 0 ? 0m : Decimal.Parse(Budget);

            if (GrandTotal > Budget2)
            {
                Sm.StdMsg(mMsgType.Warning,
                            "Grand Total : " + TxtAmt.Text + Environment.NewLine +
                            "Budget VS PO : " + Budget + Environment.NewLine + 
                            "PO Grand Total is bigger than Budget VS PO .");
                return true;
            }
           
            return false;
        }

        private void IsSubCategoryNull()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 25).Length == 0)
                {
                    Grd1.Cells[Row, 17].Value = Grd1.Cells[Row, 18].Value = "XXX";
                }
            }
        }

        private bool IsSubcategoryDifferent()
        {
            if (IsProcFormat == "1")
            {
                string SubCat = Sm.GetGrdStr(Grd1, 0, 17);
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (SubCat != Sm.GetGrdStr(Grd1, Row, 17))
                    {
                        Sm.StdMsg(mMsgType.Warning, "Item have different subcategory ");
                        return true;
                    }
                }
            }
            else
            {
                return false;
            }
            return false;
        }

        private bool IsUploadFileNotValid()
        {
            if (mPOUploadFileFormat == "2")
            {
                for (int row = 0; row < Grd6.Rows.Count - 1; row++)
                {
                    if (Sm.GetGrdStr(Grd6, row, 2).Length > 0)
                    {
                        if (IsUploadFileNotValid(Sm.GetGrdStr(Grd6, row, 2)))
                            return true;
                    }
                }
            }
            else
                if(TxtFile.Text.Length != 0 ) return IsUploadFileNotValid(TxtFile.Text);

            return false;
        }

        private MySqlCommand SavePOHdr(
            string DocNo, 
            string LocalDocNo,
            string SeqNo,
            string DeptCode,
            string ItSCCode,
            string Mth,
            string Yr,
            string Revision
            )
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("/* PO Hdr */ ");
            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            SQL.AppendLine("Insert Into TblPOHdr ");
            SQL.AppendLine("(DocNo, DocDt, Status, ImportInd, LocalDocNo, SeqNo, DeptCode, ItSCCode, Mth, Yr, Revision, ");
            if (mIsPOUsePOSignedBy)
                SQL.AppendLine("SignBy, ");
            SQL.AppendLine("DocNoSource, RevNo, Contract, VdCode, VdContactPerson, SiteCode, ShipTo, BillTo, CurCode, TaxCode1, TaxCode2, TaxCode3, TaxAmt, CustomsTaxAmt, DiscountAmt, Amt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@DocNo, @DocDt, 'O', @ImportInd, @LocalDocNo, @SeqNo, @DeptCode, @ItSCCode, @Mth, @Yr, @Revision, ");
            if (mIsPOUsePOSignedBy)
                SQL.AppendLine("@SignBy, ");
            SQL.AppendLine("@DocNo, 0, @Contract, @VdCode, @VdContactPerson, @SiteCode, @ShipTo, @BillTo, @CurCode, @TaxCode1, @TaxCode2, @TaxCode3, @TaxAmt, @CustomsTaxAmt, @DiscountAmt, @Amt, @Remark, @UserCode, @Dt);");

            if (mIsPOApprovalActived)
            {
                SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
                SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @UserCode, @Dt ");
                SQL.AppendLine("From TblDocApprovalSetting T ");
                SQL.AppendLine("Where T.DocType='PO' ");
                SQL.AppendLine("And (T.StartAmt=0.00 Or T.StartAmt<=@Amt); ");
            }

            SQL.AppendLine("Update TblPOHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select 1 From TblDocApproval ");
            SQL.AppendLine("    Where DocType='PO' ");
            SQL.AppendLine("    And DocNo=@DocNo ");
            SQL.AppendLine("    ); ");

            var cm = new MySqlCommand(){CommandText = SQL.ToString()};
            
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@ImportInd", ChkImportInd.Checked?"Y":"N");
            Sm.CmParam<String>(ref cm, "@LocalDocNo", LocalDocNo);
            Sm.CmParam<String>(ref cm, "@SeqNo", SeqNo);
            Sm.CmParam<String>(ref cm, "@DeptCode", DeptCode);
            Sm.CmParam<String>(ref cm, "@ItSCCode", ItSCCode);
            Sm.CmParam<String>(ref cm, "@Mth", Mth);
            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            Sm.CmParam<String>(ref cm, "@Revision", Revision);
            Sm.CmParam<String>(ref cm, "@Contract", Sm.GetLue(LueContract));
            Sm.CmParam<String>(ref cm, "@VdCode", Sm.GetGrdStr(Grd1, 0, 22));
            Sm.CmParam<String>(ref cm, "@VdContactPerson", Sm.GetLue(LueVdContactPersonName));
            Sm.CmParam<String>(ref cm, "@SiteCode", mSiteCode);
            Sm.CmParam<String>(ref cm, "@ShipTo", MeeShipTo.Text);
            Sm.CmParam<String>(ref cm, "@BillTo", MeeBillTo.Text);
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetGrdStr(Grd1, 0, 25));
            Sm.CmParam<String>(ref cm, "@TaxCode1", Sm.GetLue(LueTaxCode1));
            Sm.CmParam<String>(ref cm, "@TaxCode2", Sm.GetLue(LueTaxCode2));
            Sm.CmParam<String>(ref cm, "@TaxCode3", Sm.GetLue(LueTaxCode3));
            Sm.CmParam<Decimal>(ref cm, "@TaxAmt", Decimal.Parse(TxtTaxAmt.Text));
            Sm.CmParam<Decimal>(ref cm, "@CustomsTaxAmt", Decimal.Parse(TxtCustomsTaxAmt.Text));
            Sm.CmParam<Decimal>(ref cm, "@DiscountAmt", Decimal.Parse(TxtDiscountAmt.Text));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtAmt.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            if (mIsPOUsePOSignedBy)
                Sm.CmParam<String>(ref cm, "@SignBy", Sm.GetLue(LueSignBy));
            return cm;
        }

        //private MySqlCommand SavePODtl(string DocNo, int Row)
        //{
        //    var SQLDtl = new StringBuilder();

        //    SQLDtl.AppendLine("Insert Into TblPODtl(DocNo, DNo, CancelInd, CancelReason, PORequestDocNo, PORequestDNo, ");
        //    SQLDtl.AppendLine("Qty, Discount, DiscountAmt, RoundingValue, EstRecvDt, EstTimeArrival, LOPDocNo, Remark, CreateBy, CreateDt) ");
        //    SQLDtl.AppendLine("Values(@DocNo, @DNo, 'N', @CancelReason, @PORequestDocNo, @PORequestDNo, ");
        //    SQLDtl.AppendLine("@Qty, @Discount, @DiscountAmt, @RoundingValue, @EstRecvDt, @EstTimeArrival, @LOPDocNo, @Remark, @CreateBy, CurrentDateTime()); ");

        //    var cm = new MySqlCommand() { CommandText = SQLDtl.ToString() };
        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
        //    Sm.CmParam<String>(ref cm, "@CancelReason", Sm.GetGrdStr(Grd1, Row, 3));
        //    Sm.CmParam<String>(ref cm, "@PORequestDocNo", Sm.GetGrdStr(Grd1, Row, 7));
        //    Sm.CmParam<String>(ref cm, "@PORequestDNo", Sm.GetGrdStr(Grd1, Row, 8));
        //    Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 19));
        //    Sm.CmParam<Decimal>(ref cm, "@Discount", Sm.GetGrdDec(Grd1, Row, 27)); 
        //    Sm.CmParam<Decimal>(ref cm, "@DiscountAmt", Sm.GetGrdDec(Grd1, Row, 28)); 
        //    Sm.CmParam<Decimal>(ref cm, "@RoundingValue", Sm.GetGrdDec(Grd1, Row, 29));
        //    Sm.CmParam<String>(ref cm, "@EstRecvDt", Sm.GetGrdDate(Grd1, Row, 31).Substring(0, 8));
        //    Sm.CmParam<String>(ref cm, "@LOPDocNo", Sm.GetGrdStr(Grd1, Row, 35));
        //    Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 33));
        //    Sm.CmParam<String>(ref cm, "@EstTimeArrival", Sm.GetGrdStr(Grd1, Row, 44));
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    return cm;
        //}

        private MySqlCommand SavePODtl(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* PO Dtl */ ");
            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 7).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblPODtl(DocNo, DNo, CancelInd, CancelReason, PORequestDocNo, PORequestDNo, ");
                        SQL.AppendLine("Qty, Discount, DiscountAmt, RoundingValue, EstRecvDt, EstTimeArrival, LOPDocNo, Remark, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");

                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");

                    SQL.AppendLine(
                        " (@DocNo, @DNo_" + r.ToString() +
                        ", 'N', @CancelReason_" + r.ToString() +
                        ", @PORequestDocNo_" + r.ToString() +
                        ", @PORequestDNo_" + r.ToString() +
                        ", @Qty_" + r.ToString() +
                        ", @Discount_" + r.ToString() +
                        ", @DiscountAmt_" + r.ToString() +
                        ", @RoundingValue_" + r.ToString() +
                        ", @EstRecvDt_" + r.ToString() +
                        ", @EstTimeArrival_" + r.ToString() +
                        ", @LOPDocNo_" + r.ToString() +
                        ", @Remark_" + r.ToString() +
                        ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@CancelReason_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 3));
                    Sm.CmParam<String>(ref cm, "@PORequestDocNo_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 7));
                    Sm.CmParam<String>(ref cm, "@PORequestDNo_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 8));
                    Sm.CmParam<Decimal>(ref cm, "@Qty_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 19));
                    Sm.CmParam<Decimal>(ref cm, "@Discount_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 27));
                    Sm.CmParam<Decimal>(ref cm, "@DiscountAmt_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 28));
                    Sm.CmParam<Decimal>(ref cm, "@RoundingValue_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 29));
                    Sm.CmParam<String>(ref cm, "@EstRecvDt_" + r.ToString(), Sm.GetGrdDate(Grd1, r, 31).Substring(0, 8));
                    Sm.CmParam<String>(ref cm, "@LOPDocNo_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 35));
                    Sm.CmParam<String>(ref cm, "@Remark_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 33));
                    Sm.CmParam<String>(ref cm, "@EstTimeArrival_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 44));
                }
            }

            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SavePODtl2(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;
            int i = 0;

            SQL.AppendLine("/* PO Dtl 2 */ ");
            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            foreach (var x in mlEstRecvDt)
            {
                if (IsFirstOrExisted)
                {
                    SQL.AppendLine("Insert Into TblPODtl2(DocNo, DNo, PORequestDocNo, PORequestDNo, Qty, EstRecvDt, CreateBy, CreateDt) ");
                    SQL.AppendLine("Values ");

                    IsFirstOrExisted = false;
                }
                else
                    SQL.AppendLine(", ");
                SQL.AppendLine(
                    "(@DocNo, @DNo_" + i.ToString() + 
                    ", @PORequestDocNo_" + i.ToString() + 
                    ", @PORequestDNo_" + i.ToString() + 
                    ", @Qty_" + i.ToString() + 
                    ", @EstRecvDt_" + i.ToString() + 
                    ", @UserCode, @Dt) ");

                Sm.CmParam<String>(ref cm, "@DNo_" + i.ToString(), (i + 1).ToString());
                Sm.CmParam<String>(ref cm, "@PORequestDocNo_" + i.ToString(), x.PORequestDocNo);
                Sm.CmParam<String>(ref cm, "@PORequestDNo_" + i.ToString(), x.PORequestDNo);
                Sm.CmParam<Decimal>(ref cm, "@Qty_" + i.ToString(), x.Qty);
                Sm.CmParam<String>(ref cm, "@EstRecvDt_" + i.ToString(), x.Dt);
                i++;
            }
            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            cm.CommandText = SQL.ToString();
            return cm;
        }

        private MySqlCommand UpdateMaterialRequestProcessInd(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblMaterialRequestDtl T1 ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select A.DocNo, A.DNo, A.Qty, IfNull(B.Qty, 0)+IfNull(C.Qty, 0) As Qty2 ");   
            SQL.AppendLine("    From TblMaterialRequestDtl A ");
            SQL.AppendLine("    Left Join ( ");
	        SQL.AppendLine("        Select X2.MaterialRequestDocNo As DocNo, X2.MaterialRequestDNo As DNo, Sum(X1.Qty) As Qty ");
	        SQL.AppendLine("        From TblPODtl X1 ");
	        SQL.AppendLine("        Inner Join TblPORequestDtl X2 On X1.PORequestDocNo=X2.DocNo And X1.PORequestDNo=X2.DNo ");
	        SQL.AppendLine("        Where X1.CancelInd='N' ");
            SQL.AppendLine("        And Exists( ");
            SQL.AppendLine("            Select 1 ");
            SQL.AppendLine("            From TblPORequestDtl Tbl1 ");
            SQL.AppendLine("            Inner Join TblPODtl Tbl2 ");
            SQL.AppendLine("                On Tbl1.DocNo=Tbl2.PORequestDocNo ");
            SQL.AppendLine("                And Tbl1.DNo=Tbl2.PORequestDNo ");
            SQL.AppendLine("                And Tbl2.DocNo=@DocNo ");
            SQL.AppendLine("            Where Tbl1.MaterialRequestDocNo=X2.MaterialRequestDocNo ");
            SQL.AppendLine("            And Tbl1.MaterialRequestDNo=X2.MaterialRequestDNo ");
            SQL.AppendLine("        )");
            SQL.AppendLine("        Group By X2.MaterialRequestDocNo, X2.MaterialRequestDNo ");
            SQL.AppendLine("    ) B On A.DocNo=B.DocNo And A.DNo=B.DNo ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select X.MRDocNo As DocNo, X.MRDNo As DNo, Sum(X.Qty) As Qty ");
            SQL.AppendLine("        From TblMRQtyCancel X ");
            SQL.AppendLine("        Where X.CancelInd='N' ");
            SQL.AppendLine("        And Exists( ");
            SQL.AppendLine("            Select 1 ");
            SQL.AppendLine("            From TblPORequestDtl Tbl1 ");
            SQL.AppendLine("            Inner Join TblPODtl Tbl2 ");
            SQL.AppendLine("                On Tbl1.DocNo=Tbl2.PORequestDocNo ");
            SQL.AppendLine("                And Tbl1.DNo=Tbl2.PORequestDNo ");
            SQL.AppendLine("                And Tbl2.DocNo=@DocNo ");
            SQL.AppendLine("            Where Tbl1.MaterialRequestDocNo=X.MRDocNo ");
            SQL.AppendLine("            And Tbl1.MaterialRequestDNo=X.MRDNo ");
            SQL.AppendLine("        )");
            SQL.AppendLine("        Group By X.MRDocNo, X.MRDNo ");
            SQL.AppendLine("    ) C On A.DocNo=C.DocNo And A.DNo=C.DNo ");
            SQL.AppendLine("    Where A.CancelInd='N' ");
            SQL.AppendLine("    And A.Status In ('O', 'A') ");
            SQL.AppendLine("    And Exists( ");
            SQL.AppendLine("        Select 1 ");
            SQL.AppendLine("        From TblPORequestDtl Tbl1 ");
            SQL.AppendLine("        Inner Join TblPODtl Tbl2 ");
            SQL.AppendLine("            On Tbl1.DocNo=Tbl2.PORequestDocNo ");
            SQL.AppendLine("            And Tbl1.DNo=Tbl2.PORequestDNo ");
            SQL.AppendLine("            And Tbl2.DocNo=@DocNo ");
            SQL.AppendLine("        Where Tbl1.MaterialRequestDocNo=A.DocNo ");
            SQL.AppendLine("        And Tbl1.MaterialRequestDNo=A.DNo ");
            SQL.AppendLine("    )");
            SQL.AppendLine(") T2 On T1.DocNo=T2.DocNo And T1.DNo=T2.DNo ");
            SQL.AppendLine("Set T1.ProcessInd=");
	        SQL.AppendLine("    Case When T2.Qty2=0 Then 'O' ");
	        SQL.AppendLine("    Else ");
		    SQL.AppendLine("        If((T2.Qty-T2.Qty2)<=0, 'F', 'P') ");
	        SQL.AppendLine("    End ");
            SQL.AppendLine("Where Exists( ");
            SQL.AppendLine("    Select 1 ");
            SQL.AppendLine("    From TblPORequestDtl Tbl1 ");
            SQL.AppendLine("    Inner Join TblPODtl Tbl2 ");
            SQL.AppendLine("        On Tbl1.DocNo=Tbl2.PORequestDocNo ");
            SQL.AppendLine("        And Tbl1.DNo=Tbl2.PORequestDNo ");
            SQL.AppendLine("        And Tbl2.DocNo=@DocNo ");
            SQL.AppendLine("    Where Tbl1.MaterialRequestDocNo=T1.DocNo ");
            SQL.AppendLine("    And Tbl1.MaterialRequestDNo=T1.DNo ");
            SQL.AppendLine(");");

            var cm = new MySqlCommand(){ CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            return cm;
        }

        private MySqlCommand UpdatePOFile(string DocNo, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblPOhdr Set ");
            SQL.AppendLine("    FileName=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo  ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);

            return cm;
        }

        private MySqlCommand UpdatePOFile(string DocNo, int Row, string Type, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblPOFile Set ");
            SQL.AppendLine("    FileName=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo And DNo=@DNo And Type=@Type; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);
            Sm.CmParam<String>(ref cm, "@Type", Type);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));

            return cm;
        }

        private MySqlCommand SavePOFile(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            #region Attachment
            for (int r = 0; r < Grd6.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd6, r, 2).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblPOFile(DocNo, DNo, FileName, Type, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");

                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");

                    SQL.AppendLine("(@DocNo, @DNo_" + r.ToString() +
                        ", @FileName_" + r.ToString() +
                        ", @Type, @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@FileName_" + r.ToString(), Sm.GetGrdStr(Grd6, r, 2));
                }
            }

            if (!IsFirstOrExisted)
            {
                SQL.AppendLine(" On Duplicate Key Update ");
                SQL.AppendLine("    FileName = Values(FileName); ");
            }
            #endregion

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@Type", "1");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit data

        private void CancelData()
        {
            int gridcount = 0;
            int rowcount = Grd1.Rows.Count - 1;
            UpdateCancelledPO();

            string DNo = "'XXX'";

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdBool(Grd1, Row, 1) && !Sm.GetGrdBool(Grd1, Row, 2) && Sm.GetGrdStr(Grd1, Row, 7).Length > 0)
                    DNo = DNo + ",'" + Sm.GetGrdStr(Grd1, Row, 0) + "'";

            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || 
                IsCancelledDataNotValid(DNo)) return;
           
            Cursor.Current = Cursors.WaitCursor;


            var cml = new List<MySqlCommand>();

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (Sm.GetGrdBool(Grd1, Row, 1) == true)
                    gridcount = gridcount + 1;
            }

            if (gridcount == rowcount)
            {
                cml.Add(DeleteVoucherFromPOHdr(TxtDocNo.Text));                
            }

            cml.Add(UpdatePOHdr(TxtDocNo.Text));
            //cml.Add(CancelPODtl(TxtDocNo.Text, DNo));

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (Sm.GetGrdBool(Grd1, Row, 1) && 
                    !Sm.GetGrdBool(Grd1, Row, 2) && 
                    Sm.GetGrdStr(Grd1, Row, 7).Length > 0)
                    
                    cml.Add(CancelPODtl(TxtDocNo.Text, Row));
            }

            if (mIsPOApprovalActived) cml.Add(UpdatePOStatus(TxtDocNo.Text));
            
            cml.Add(UpdateMaterialRequestProcessInd(TxtDocNo.Text));

            if (mIsPOAllowToUploadFile && TxtFile.Text.Length > 0 && TxtFile.Text != "openFileDialog1" && TxtFile.Text != mInitFile)
                ProcessUploadFile(TxtDocNo.Text);

            if (mIsPOAllowToUploadFile && mPOUploadFileFormat == "2")
            {
                cml.Add(SavePOFile2(TxtDocNo.Text));
            }

            Sm.ExecCommands(cml);

            if (mIsPOAllowToUploadFile && mPOUploadFileFormat == "2")
            {
                for (int Row = 0; Row < Grd7.Rows.Count; Row++)
                {
                    if (Sm.GetGrdStr(Grd7, Row, 3).Length > 0)
                    {
                        if (mIsPOAllowToUploadFile && Sm.GetGrdStr(Grd7, Row, 3).Length > 0 && Sm.GetGrdStr(Grd7, Row, 3) != "openFileDialog1")
                        {
                            if (Sm.GetGrdStr(Grd7, Row, 3) != Sm.GetGrdStr(Grd7, Row, 8))
                            {
                                UploadFile(TxtDocNo.Text, Row, "2", Sm.GetGrdStr(Grd7, Row, 3));
                            }
                        }
                    }
                }
            }

            ShowData(TxtDocNo.Text);
        }

        private void UpdateCancelledPO()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DNo, CancelInd ");
            SQL.AppendLine("From TblPODtl  ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("Order By DNo");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = SQL.ToString()
                };
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DNo", "CancelInd" });

                if (dr.HasRows)
                {
                    Grd1.ProcessTab = true;
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        for (int Index = 0; Index < Grd1.Rows.Count - 1; Index++)
                        {
                            if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Index, 0), Sm.DrStr(dr, 0)))
                            {
                                if (Sm.CompareStr(Sm.DrStr(dr, 1), "Y"))
                                    Sm.SetGrdValue("B", Grd1, dr, c, Index, 1, 1);
                                Sm.SetGrdValue("B", Grd1, dr, c, Index, 2, 1);
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        private MySqlCommand SavePOFile2(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            #region Attachment 2
            for (int r = 0; r < Grd7.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd7, r, 3).Length > 0 && Sm.GetGrdStr(Grd7, r, 8).Length == 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblPOFile(DocNo, DNo, FileName, Type, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");

                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");

                    SQL.AppendLine("(@DocNo, @DNo_" + r.ToString() +
                        ", @FileName_" + r.ToString() +
                        ", @Type, @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.GetGrdStr(Grd7, r, 0));
                    Sm.CmParam<String>(ref cm, "@FileName_" + r.ToString(), Sm.GetGrdStr(Grd7, r, 3));
                }
            }

            if (!IsFirstOrExisted)
            {
                SQL.AppendLine(" On Duplicate Key Update ");
                SQL.AppendLine("    FileName = Values(FileName); ");
            }
            #endregion

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@Type", "2");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private bool IsCancelReasonEmpty(string DNo)
        {
            if (!mIsPOAllowToUploadFile)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && Sm.GetGrdStr(Grd1, Row, 3).Length <= 0)
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "Item Name : " + Sm.GetGrdStr(Grd1, Row, 15) + Environment.NewLine +
                            "Cancel reason still empty.");
                        Sm.FocusGrd(Grd1, Row, 3);
                        return true;
                    }
                }
            }
            else
            {
                if (DNo != "XXX")
                {
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    {
                        if (Sm.GetGrdBool(Grd1, Row, 1) && Sm.GetGrdStr(Grd1, Row, 3).Length <= 0)
                        {
                            Sm.StdMsg(mMsgType.Warning,
                                "Item Name : " + Sm.GetGrdStr(Grd1, Row, 15) + Environment.NewLine +
                                "Cancel reason still empty.");
                            Sm.FocusGrd(Grd1, Row, 3);
                            return true;
                        }
                    }
                }
            }

            return false;
        }

        private bool IsCancelledDataNotValid(string DNo)
        { 
            return
                IsPOStatusCancelled() ||
                IsCancelledPONotExisted(DNo) ||
                IsCancelledPOProcessedAlready(DNo) ||
                (mIsRecvExpeditionEnabled && IsCancelledPOProcessedToExpeditionAlready(DNo)) ||
                IsPOUsedForAPDownpayment(DNo)||
                IsCancelReasonEmpty(DNo); ;
        }

        private bool IsPOUsedForAPDownpayment(string DNo)
        {
            if (!mIsPOAllowToUploadFile)
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select 1 ");
                SQL.AppendLine("From TblAPDownpayment A ");
                SQL.AppendLine("Inner Join TblVoucherRequestHdr B On A.VoucherRequestDocNo=B.DocNo ");
                SQL.AppendLine("Where A.CancelInd='N' ");
                SQL.AppendLine("And A.Status In ('O', 'A') ");
                SQL.AppendLine("And A.PODocNo Is Not Null ");
                SQL.AppendLine("And A.PODocNo=@Param; ");

                return Sm.IsDataExist(
                    SQL.ToString(), TxtDocNo.Text,
                    "This PO has been processed in AP downpayment.");
            }
            else
            {
                if (DNo != "XXX")
                {
                    var SQL = new StringBuilder();

                    SQL.AppendLine("Select 1 ");
                    SQL.AppendLine("From TblAPDownpayment A ");
                    SQL.AppendLine("Inner Join TblVoucherRequestHdr B On A.VoucherRequestDocNo=B.DocNo ");
                    SQL.AppendLine("Where A.CancelInd='N' ");
                    SQL.AppendLine("And A.Status In ('O', 'A') ");
                    SQL.AppendLine("And A.PODocNo Is Not Null ");
                    SQL.AppendLine("And A.PODocNo=@Param; ");

                    return Sm.IsDataExist(
                        SQL.ToString(), TxtDocNo.Text,
                        "This PO has been processed in AP downpayment.");
                }
            }

            return false;
        }

        private bool IsCancelledPONotExisted(string DNo)
        {
            if (Sm.CompareStr(DNo, "'XXX'") && !mIsPOAllowToUploadFile)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel at least 1 po request.");
                return true;
            }
            return false;
        }

        private void IsPODtlCancelAll()
        {
            int gridcount = 0;
            int rowcount = Grd1.Rows.Count - 1;
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (Sm.GetGrdBool(Grd1, Row, 1) == true)
                    gridcount = gridcount + 1;
            }
        }

        private bool IsCancelledPOProcessedAlready(string DNo)
        {
            return Sm.IsDataExist(
                "Select 1 From TblRecvVdDtl Where PODocNo=@Param And CancelInd='N' And PODNo In (" + DNo + ") Limit 1;",
                TxtDocNo.Text,
                "This document is already processed to receiving.");       
        }

        private bool IsCancelledPOProcessedToExpeditionAlready(string DNo)
        {
            return Sm.IsDataExist(
                "Select 1 From tblrecvexpeditiondtl Where PODocNo=@Param And CancelInd='N' And PODNo In (" + DNo + ") Limit 1;",
                TxtDocNo.Text,
                "This document is already processed to expedition.");
        }
        private MySqlCommand UpdatePOHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblPOHdr Set ");
            SQL.AppendLine("    TaxAmt=@TaxAmt, Amt=@Amt, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And (TaxAmt<>@TaxAmt Or Amt<>@Amt); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<Decimal>(ref cm, "@TaxAmt", Decimal.Parse(TxtTaxAmt.Text));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtAmt.Text));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        //private MySqlCommand CancelPODtl(string DocNo, string DNo)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Update TblPODtl Set ");
        //    SQL.AppendLine("    CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
        //    SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N' And DNo In (" + DNo + "); ");

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };
        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

        //    return cm;
        //}

        private MySqlCommand CancelPODtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblPODtl Set ");
            SQL.AppendLine("    CancelInd='Y', ");
            SQL.AppendLine("    CancelReason=@CancelReason, ");
            SQL.AppendLine("    LastUpBy=@UserCode, ");
            SQL.AppendLine("    LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And DNo=@DNo And CancelInd='N'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.GetGrdStr(Grd1, Row, 0));
            Sm.CmParam<String>(ref cm, "@CancelReason", Sm.GetGrdStr(Grd1, Row, 3));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand UpdatePOStatus(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblPOHdr Set ");
            SQL.AppendLine("    Status='C', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select 1 From TblPODtl Where CancelInd='N' And DocNo=@DocNo ");
            SQL.AppendLine("    ); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }


        private MySqlCommand DeleteVoucherFromPOHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblPOHdr  ");
            SQL.AppendLine("    Set VoucherDocNo=null, DownPayment=0.00 ");
            SQL.AppendLine("Where DocNo=@DocNo ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowPOHdr(DocNo);
                ShowPODtl(DocNo);
                if (mIsPOEstRecvDtSplitEnabled) ShowPODtl2(DocNo);
                ShowAPDownPayment(DocNo);
                ShowPOQtyCancel(DocNo);
                ComputeTaxAmt();
                ComputeAmt();
                ShowPORevision(DocNo);
                Sm.ShowDocApproval(DocNo, "PO", ref Grd5);
                SetSeqNo();
                if(mPOUploadFileFormat == "2")
                {
                    var l = new List<Attachment2> ();
                    ShowPOFile(DocNo);
                    ShowPOFile2(ref l, DocNo);
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowPOHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQLHdr = new StringBuilder();

            SQLHdr.AppendLine("Select A.DocNo, A.DocDt, A.LocalDocNo, A.VdCode, A.VdContactPerson, ");
            SQLHdr.AppendLine("A.SiteCode, B.SiteName, A.ShipTo, A.BillTo, ");
            if (mIsPOUsePOSignedBy)
                SQLHdr.AppendLine("A.SignBy, ");
            else
                SQLHdr.AppendLine("Null as SignBy, ");
            SQLHdr.AppendLine("A.TaxCode1, A.TaxCode2, A.TaxCode3, A.TaxAmt, A.CustomsTaxAmt, A.DiscountAmt, A.Amt, A.Remark, A.ImportInd, A.FileName, A.Contract, ");
            SQLHdr.AppendLine("Case A.Status When 'O' Then 'Outstanding' When 'A' Then 'Approved' When 'C' Then 'Cancel' End As StatusDesc ");
            SQLHdr.AppendLine("From TblPOHdr A ");
            SQLHdr.AppendLine("Left Join TblSite B On A.SiteCode=B.SiteCode ");
            SQLHdr.AppendLine("Where A.DocNo=@DocNo; ");

            Sm.ShowDataInCtrl(
                    ref cm, SQLHdr.ToString(),
                    new string[] { 
                        //0
                        "DocNo",

                        //1-5
                        "DocDt",  "LocalDocNo", "VdCode", "VdContactPerson", "SiteCode", 
                        
                        //6-10
                        "SiteName", "ShipTo", "BillTo", "TaxCode1", "TaxCode2", 
                        
                        //11-15
                        "TaxCode3", "TaxAmt", "CustomsTaxAmt", "DiscountAmt", "Amt",  
                        
                        //16-20
                        "Remark", "ImportInd", "StatusDesc" , "FileName", "Contract",

                        //21
                        "SignBy"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        TxtLocalDocNo.EditValue = Sm.DrStr(dr, c[2]);
                        SetLueVdCode(ref LueVdCode, Sm.DrStr(dr, c[3]));
                        Sm.SetLue(LueVdCode, Sm.DrStr(dr, c[3]));
                        SetLueVdPersonCode(ref LueVdContactPersonName, Sm.GetLue(LueVdCode));
                        Sm.SetLue(LueVdContactPersonName, Sm.DrStr(dr, c[4]));
                        mSiteCode = Sm.DrStr(dr, c[5]);
                        TxtSiteCode.EditValue = Sm.DrStr(dr, c[6]);
                        MeeShipTo.EditValue = Sm.DrStr(dr, c[7]);
                        MeeBillTo.EditValue = Sm.DrStr(dr, c[8]);
                        Sm.SetLue(LueTaxCode1, Sm.DrStr(dr, c[9]));
                        Sm.SetLue(LueTaxCode2, Sm.DrStr(dr, c[10]));
                        Sm.SetLue(LueTaxCode3, Sm.DrStr(dr, c[11]));
                        TxtTaxAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[12]),0);
                        TxtCustomsTaxAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[13]), 0);
                        TxtDiscountAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[14]), 0);
                        TxtAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[15]), 0);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[16]);
                        ChkImportInd.Checked = Sm.DrStr(dr, c[17])=="Y";
                        TxtStatus.EditValue = Sm.DrStr(dr, c[18]);
                        TxtFile.EditValue = Sm.DrStr(dr, c[19]);
                        mInitFile = Sm.DrStr(dr, c[19]);
                        Sm.SetLue(LueContract, Sm.DrStr(dr, c[20]));
                        Sm.SetLue(LueSignBy, Sm.DrStr(dr, c[21]));
                    }, true
                );
        }

        private void ShowPODtl(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.CancelInd, A.CancelReason, A.PORequestDocNo, A.PORequestDNo, B.DocDt AS PORequestDocDt, ");
            SQL.AppendLine("E.ItCode, H.ItName, H.ForeignName, A.Qty, H.PurchaseUomCode, ");
            SQL.AppendLine("C.QtDocNo, F.VdCode, I.VdName, J.PtName, F.CurCode, G.UPrice, G.UPriceInit, A.Discount, A.DiscountAmt, A.RoundingValue, ");
            SQL.AppendLine("A.EstRecvDt, A.EstTimeArrival, K.DTName, A.Remark, D.DocDt As MaterialRequestDocDt, H.ItScCode, L.ItScName, B.LocalDOcNo, ");
            SQL.AppendLine("H.ItCodeInternal, A.LOPDocNo, H.Specification, M.ProjectCode, M.ProjectName ");
            if (mIsPurchaseRequestForProductionEnabled)
                SQL.AppendLine(", D.IsProduction ");
            else
                SQL.AppendLine(", 'N' As IsProduction ");
            SQL.AppendLine("From TblPODtl A ");
            SQL.AppendLine("Inner Join TblPORequestHdr B On A.PORequestDocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblPORequestDtl C On A.PORequestDocNo=C.DocNo And A.PORequestDNo=C.DNo ");
            SQL.AppendLine("Inner Join TblMaterialRequestHdr D On C.MaterialRequestDocNo=D.DocNo ");
            SQL.AppendLine("Inner Join TblMaterialRequestDtl E On C.MaterialRequestDocNo=E.DocNo And C.MaterialRequestDNo=E.DNo ");
            SQL.AppendLine("Inner Join TblQtHdr F On C.QtDocNo=F.DocNo ");
            SQL.AppendLine("Inner Join TblQtDtl G On C.QtDocNo=G.DocNo And C.QtDNo=G.DNo ");
            SQL.AppendLine("Inner Join TblItem H On E.ItCode=H.ItCode ");
            SQL.AppendLine("Inner Join TblVendor I On F.VdCode=I.VdCode ");
            SQL.AppendLine("Inner Join TblPaymentTerm J On F.PtCode=J.PtCode ");
            SQL.AppendLine("Left Join TblDeliveryType K On F.DTCode=K.DTCode ");
            SQL.AppendLine("Left Join TblItemSubcategory L On H.ItScCode = L.ItScCode ");
            SQL.AppendLine("Left Join TblProjectGroup M On D.PGCode=M.PGCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo;");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 
                    //1-5
                    "CancelInd", "cancelReason", "PORequestDocNo", "PORequestDNo", "LocalDocNo",   
                    //6-10
                    "PORequestDocDt", "MaterialRequestDocDt",  "ItCode", "ItCodeInternal", "ItName",   
                    //11-15
                    "ForeignName", "ItScCode", "ItScName", "Qty", "PurchaseUomCode",   
                    //16-20
                    "QtDocNo", "VdCode", "VdName", "PtName", "CurCode",  
                    //21-25
                    "UPrice", "Discount", "DiscountAmt", "RoundingValue",  "EstRecvDt",    
                    //26-30
                    "DTName", "Remark", "LOPDocNo", "Specification", "ProjectCode", 
                    //31-34
                    "ProjectName", "IsProduction", "EstTimeArrival", "UPriceInit"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Grd.Cells[Row, 5].Value = mSiteCode;
                    Grd.Cells[Row, 6].Value = TxtSiteCode.EditValue;
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 5);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 10, 6);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 11, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 10);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 11);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 12);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 13);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 14);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 15);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 16);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 17);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 18);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 19);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 20);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 26, 21);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 27, 22);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 28, 23);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 29, 24);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 31, 25);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 32, 26);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 33, 27);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 35, 28);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 41, 29);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 37, 30);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 42, 31);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 43, 32);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 44, 33);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 45, 34);
                    ComputeTotal(Row);
                }, false, false, true, false
            );
            Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 1, 2, 43 });
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 19, 26, 27, 28, 29 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ShowPODtl2(string DocNo)
        {
            var cm = new MySqlCommand();
            cm.CommandTimeout = 600;
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            cm.CommandText =
                    "Select PORequestDocNo, PORequestDNo, Qty, EstRecvDt " +
                    "From TblPODtl2 Where DocNo=@DocNo Order By PORequestDocNo, PORequestDNo;";

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "PORequestDocNo", "PORequestDNo", "Qty", "EstRecvDt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        mlEstRecvDt.Add(new EstRecvDt()
                        {
                            PORequestDocNo = Sm.DrStr(dr, c[0]),
                            PORequestDNo = Sm.DrStr(dr, c[1]),
                            Qty = Sm.DrDec(dr, c[2]),
                            Dt = Sm.DrStr(dr, c[3])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void ShowAPDownPayment(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelInd, A.CurCode, A.Amt, A.PIC, A.VoucherRequestDocNo, B.VoucherDocNo, A.Remark ");
            SQL.AppendLine("From TblAPDownPayment A ");
            SQL.AppendLine("Left Join TblVoucherRequestHdr B On A.VoucherRequestDocNo=B.DocNo ");
            SQL.AppendLine("Where A.PODocNo=@DocNo ");
            SQL.AppendLine("And A.CancelInd='N' And A.Status In ('O', 'A') ");
            SQL.AppendLine("Order By A.DocDt, A.DocNo;");

            Sm.ShowDataInGrid(
                ref Grd2, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DocNo", 

                    //1-5
                    "DocDt", "CancelInd", "CurCode", "Amt", "PIC", 
                    
                    //6-8
                    "VoucherRequestDocNo", "VoucherDocNo", "Remark"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 8);
                }, false, false, true, false
            );
            Sm.SetGrdBoolValueFalse(ref Grd2, Grd2.Rows.Count - 1, new int[] { 2 });
            Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 4 });
            Sm.FocusGrd(Grd2, 0, 0);
        }

        private void ShowPORevision(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select G.ItCode, K.ItName, K.ForeignName, A.DocNo, A.DocDt, A.CancelInd, A.QtDocNo, A.DiscountAmt, A.Discount, H.UPrice, ");
            SQL.AppendLine("Case A.Status ");
            SQL.AppendLine("    When 'O' Then 'Outstanding' ");
            SQL.AppendLine("    When 'C' Then 'Cancelled' ");
            SQL.AppendLine("    When 'A' Then 'Approved' ");
            SQL.AppendLine("End As Status, A.FileName ");
            SQL.AppendLine("From TblPORevision A ");
            SQL.AppendLine("Inner Join TblPOHdr B On A.PODocNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblPODtl C On A.PODocNo = C.DocNo And A.PODNo = C.DNo ");
            SQL.AppendLine("Inner Join TblPORequestHdr D On C.PORequestDocNo=D.DocNo ");
            SQL.AppendLine("Inner Join TblPORequestDtl E On C.PORequestDocNo=E.DocNo And C.PORequestDNo=E.DNo ");
            SQL.AppendLine("Inner Join TblMaterialRequestHdr F On E.MaterialRequestDocNo=F.DocNo ");
            SQL.AppendLine("Inner Join TblMaterialRequestDtl G On E.MaterialRequestDocNo=G.DocNo And E.MaterialRequestDNo=G.DNo ");
            SQL.AppendLine("Inner Join TblQtDtl H On A.QtDocNo = H.DocNo And A.QtDNo= H.DNo ");
            SQL.AppendLine("Inner Join TblQtHdr I On A.QtDocNo = I.DocNo ");
            SQL.AppendLine("Inner Join TblVendor J On I.VdCode = J.VdCode ");
            SQL.AppendLine("Inner Join TblItem K On H.ItCode = K.ItCode ");
            //SQL.AppendLine("Where Not (A.CancelInd = 'Y' Or A.Status = 'C') ");
            SQL.AppendLine("Where A.PODocNo=@DocNo ");
            SQL.AppendLine("Order By A.PODocNo, A.PODNo, A.DocNo; ");

            Sm.ShowDataInGrid(
                ref Grd3, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "ItCode",

                    //1-5
                    "ItName", "ForeignName", "DocNo", "DocDt", "CancelInd",
                    
                    //6-10
                    "Status", "QtDocNo", "UPrice", "Discount", "DiscountAmt",

                    //11
                    "FileName"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 11);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 7, 4);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 8, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 10);
                }, false, false, true, false
            );
            Sm.SetGrdBoolValueFalse(ref Grd3, Grd3.Rows.Count - 1, new int[] { 8 });
            Sm.SetGrdNumValueZero(ref Grd3, Grd3.Rows.Count - 1, new int[] { 12, 13, 14 });
            Sm.FocusGrd(Grd3, 0, 0);
        }

        private void ShowPOQtyCancel(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select E.ItCode, E.ItName, E.ForeignName, ");
            SQL.AppendLine("A.DocNo, A.DocDt, A.Qty, E.PurchaseUomCode, F.UPrice ");
            SQL.AppendLine("From TblPOQtyCancel A ");
            SQL.AppendLine("Inner Join TblPODtl B On A.PODocNo=B.DocNo And A.PODNo=B.DNo ");
            SQL.AppendLine("Inner Join TblPORequestDtl C On B.PORequestDocNo=C.DocNo And B.PORequestDNo=C.DNo ");
            SQL.AppendLine("Inner Join TblMaterialRequestDtl D On C.MaterialRequestDocNo=D.DocNo And C.MaterialRequestDNo=D.DNo ");
            SQL.AppendLine("Inner Join TblItem E On D.ItCode=E.ItCode ");
            SQL.AppendLine("Inner join TblQtDtl F On C.QtDocNo = F.DocNo And C.QtDNo = F.DNo ");
            SQL.AppendLine("Where A.CancelInd= 'N' ");
            SQL.AppendLine("And A.PODocNo=@DocNo ");
            SQL.AppendLine("Order By A.PODNo, A.DocDt, A.DocNo; ");

            Sm.ShowDataInGrid(
                ref Grd4, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "ItCode",

                    //1-5
                    "ItName", "ForeignName", "DocNo", "DocDt", "Qty",
                    
                    //6
                    "PurchaseUomCode", "Uprice"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                    Grd4.Cells[Row, 9].Value = Sm.GetGrdDec(Grd4, Row, 6) * Sm.GetGrdDec(Grd4, Row, 8);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd4, Grd4.Rows.Count - 1, new int[] { 6 });
            Sm.FocusGrd(Grd4, 0, 0);
        }

        private void ShowPOFile(string DocNo)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                    ref Grd6, ref cm,
                   "select DNo, FileName, CreateBy, CreateDt from  TblPOFile Where DocNo=@DocNo And Type = '1' Order By Dno",

                    new string[]
                    {
                        "Dno",
                        "FileName",
                        "CreateBy",
                        "CreateDt"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd6, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd6, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd6, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("D", Grd6, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("T", Grd6, dr, c, Row, 6, 3);
                        Sm.SetGrdValue("S", Grd6, dr, c, Row, 7, 1);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd6, 0, 0);
        }

        private void ShowPOFile2(ref List<Attachment2> l, string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DNo, FileName, CreateBy, CreateDt ");
            SQL.AppendLine("From TblPOFile ");
            SQL.AppendLine("Where DocNo = @DocNo ");
            SQL.AppendLine("    And Type = '2' ");
            SQL.AppendLine("Order By DNo;");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DNo", "FileName", "CreateBy", "CreateDt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                        l.Add(new Attachment2()
                        {
                            DNo = Sm.DrStr(dr, c[0]),
                            FileName = Sm.DrStr(dr, c[1]),
                            CreateBy = Sm.DrStr(dr, c[2]),
                            CreateDt = Sm.DrStr(dr, c[3])
                        });
                }
                dr.Close();
            }


            //Set in Grid 
            if(l.Count > 0)
            {
                foreach (var x in l)
                {
                    for (int row = 0; row < Grd7.Rows.Count; row++)
                    {
                        if (Sm.GetGrdStr(Grd7, row, 0) == x.DNo)
                        {
                            Grd7.Cells[row, 3].Value = x.FileName;
                            Grd7.Cells[row, 5].Value = x.CreateBy;
                            Grd7.Cells[row, 6].Value = Sm.ConvertDate(x.CreateDt);
                            Grd7.Cells[row, 7].Value = Sm.ConvertDateTime(x.CreateDt);
                            Grd7.Cells[row, 8].Value = x.FileName;
                        }
                    }
                }
            }
        }

        #endregion

        #region Additional Method

        public void SetLueOption(ref LookUpEdit Lue, string OptCat)
        {
            var cm = new MySqlCommand()
            {
                CommandText = "Select A.OptCode As Col1, B.UserName As Col2 From TblOption A " +
            "Inner Join TblUser B On A.OptCode=B.UserCode " +
            "Where A.OptCat=@Param Order By B.UserName; "
            };
            if (OptCat.Length > 0) Sm.CmParam<String>(ref cm, "@Param", OptCat);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        internal void SetSeqNo()
        {
            for (int r = 0; r < Grd1.Rows.Count; r++)
                Grd1.Cells[r, 39].Value = r + 1;
        }

        private void GetValue()
        {
            mIsPOApprovalActived = Sm.IsDataExist("Select 1 from TblDocApprovalSetting Where DocType='PO' Limit 1;");
        }

        private void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'IsUseECatalog', 'IsRecvExpeditionEnabled', 'IsPORevisionUseETA', 'IsPOShowCancelationUnitPrice', 'IsFilterByItCt', ");
            SQL.AppendLine("'IsItCtFilteredByGroup', 'IsAutoGeneratePurchaseLocalDocNo', 'MainCurCode', 'POAutoApprovedByEmpCode', 'POAmtForSignature', ");
            SQL.AppendLine("'BudgetBasedOn', 'POPrintOutCompanyLogo', 'IsSiteMandatory', 'EmpCodePO', 'IsPOFindUseExlucedPOService', ");
            SQL.AppendLine("'IsPOSignatureWithRangeEnabled', 'POSignatureSource', 'IsVendorComboShowCategory', 'IsShowForeignName', 'IsFilterBySite', ");
            SQL.AppendLine("'IsPOEstRecvDtSplitEnabled', 'IsBOMShowSpecifications', 'ProcFormatDocNo', 'DocTitle', 'IsPOUseContract', ");
            SQL.AppendLine("'PORequestPropCodeEnabled', 'IsPOGrandTotalComparedToBudget', 'IsFilterByDept', 'IsPODisplayNotificationRemarkChanges', 'FormatFTPClient', ");
            SQL.AppendLine("'FileSizeMaxUploadFTPClient', 'IsQTAllowToUploadFile', 'IsPOAllowToUploadFile', 'IsShowSeqNoInPO', 'IsPOPrintOutInEnglish', ");
            SQL.AppendLine("'IsPOShowLOPData', 'SharedFolderForFTPClient', 'UsernameForFTPClient', 'PasswordForFTPClient', 'PortForFTPClient', ");
            SQL.AppendLine("'IsBillToDefaultByEntity', 'IsGroupPaymentTermActived', 'IsShowPOWithTaxWarning', 'SharedFolderForFTPClient', 'HostAddrForFTPClient', ");
            SQL.AppendLine("'IsShipToDefaultByCompany', 'IsComparedToDetailDate', 'IsFilterByVendorCategory', 'IsPurchaseOrderRemarkMandatory', ");
            SQL.AppendLine("'POUploadFileFormat', 'IsOutstandingPOPrintEnabled', 'IsUploadFileRenamed', 'IsPOUsePOSignedBy', 'IsContactPersonPurchaseOrderMandatory', ");
            SQL.AppendLine("'IsFilterVendorInListofPORequestHide' );");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();

                        switch (ParCode)
                        {
                            //boolean
                            case "IsComparedToDetailDate": mIsComparedToDetailDate = ParValue == "Y"; break;
                            case "IsShipToDefaultByCompany": mIsShipToDefaultByCompany = ParValue == "Y"; break;
                            case "IsBillToDefaultByEntity": mIsBillToDefaultByEntity = ParValue == "Y"; break;
                            case "IsGroupPaymentTermActived": mIsGroupPaymentTermActived = ParValue == "Y"; break;
                            case "IsShowPOWithTaxWarning": mIsShowPOWithTaxWarning = ParValue == "Y"; break;
                            case "IsPOShowLOPData": mIsPOShowLOPData = ParValue == "Y"; break;
                            case "IsQTAllowToUploadFile": mIsQTAllowToUploadFile = ParValue == "Y"; break;
                            case "IsPOAllowToUploadFile": mIsPOAllowToUploadFile = ParValue == "Y"; break;
                            case "IsShowSeqNoInPO": mIsShowSeqNoInPO = ParValue == "Y"; break;
                            case "IsPOPrintOutInEnglish": mIsPOPrintOutInEnglish = ParValue == "Y"; break;
                            case "PORequestPropCodeEnabled": mPORequestPropCodeEnabled = ParValue == "Y"; break;
                            case "IsPOGrandTotalComparedToBudget": mIsPOGrandTotalComparedToBudget = ParValue == "Y"; break;
                            case "IsFilterByDept": mIsFilterByDept = ParValue == "Y"; break;
                            case "IsPODisplayNotificationRemarkChanges": mIsPODisplayNotificationRemarkChanges = ParValue == "Y"; break;
                            case "IsPOEstRecvDtSplitEnabled": mIsPOEstRecvDtSplitEnabled = ParValue == "Y"; break;
                            case "IsBOMShowSpecifications": mIsBOMShowSpecifications = ParValue == "Y"; break;
                            case "IsPOUseContract": mIsPOUseContract = ParValue == "Y"; break;
                            case "IsPOSignatureWithRangeEnabled": mIsPOSignatureWithRangeEnabled = ParValue == "Y"; break;
                            case "IsVendorComboShowCategory": mIsVendorComboShowCategory = ParValue == "Y"; break;
                            case "IsShowForeignName": mIsShowForeignName = ParValue == "Y"; break;
                            case "IsFilterBySite": mIsFilterBySite = ParValue == "Y"; break;
                            case "IsSiteMandatory": mIsSiteMandatory = ParValue == "Y"; break;
                            case "IsPOFindUseExlucedPOService": mIsPOFindUseExlucedPOService = ParValue == "Y"; break;
                            case "IsItCtFilteredByGroup": mIsItCtFilteredByGroup = ParValue == "Y"; break;
                            case "IsAutoGeneratePurchaseLocalDocNo": mIsAutoGeneratePurchaseLocalDocNo = ParValue == "Y"; break;
                            case "IsUseECatalog": mIsUseECatalog = ParValue == "Y"; break;
                            case "IsRecvExpeditionEnabled": mIsRecvExpeditionEnabled = ParValue == "Y"; break;
                            case "IsPORevisionUseETA": mIsPORevisionUseETA = ParValue == "Y"; break;
                            case "IsPOShowCancelationUnitPrice": mIsPOShowCancelationUnitPrice = ParValue == "Y"; break;
                            case "IsFilterByItCt": mIsFilterByItCt = ParValue == "Y"; break;
                            case "IsFilterByVendorCategory": mIsFilterByVendorCategory = ParValue == "Y"; break;
                            case "IsPurchaseOrderRemarkMandatory": mIsPurchaseOrderRemarkMandatory = ParValue == "Y"; break;
                            case "IsOutstandingPOPrintEnabled": mIsOutstandingPOPrintEnabled = ParValue == "Y"; break;
                            case "IsUploadFileRenamed": mIsUploadFileRenamed = ParValue == "Y"; break;
                            case "IsPOUsePOSignedBy": mIsPOUsePOSignedBy = ParValue == "Y"; break;
                            case "IsContactPersonPurchaseOrderMandatory": mIsContactPersonPurchaseOrderMandatory = ParValue == "Y"; break;
                            case "IsFilterVendorInListofPORequestHide": mIsFilterVendorInListofPORequestHide = ParValue == "Y"; break;

                            //string
                            case "SharedFolderForFTPClient": mSharedFolderForFTPClient = ParValue; break;
                            case "HostAddrForFTPClient": mHostAddrForFTPClient = ParValue; break;
                            case "POAutoApprovedByEmpCode": mPOAutoApprovedByEmpCode = ParValue; break;
                            case "MainCurCode": mMainCurCode = ParValue; break;
                            case "EmpCodePO": mEmpCodePO = ParValue; break;
                            case "POPrintOutCompanyLogo": mPOPrintOutCompanyLogo = ParValue; break;
                            case "BudgetBasedOn": mBudgetBasedOn = ParValue; break;
                            case "POSignatureSource": mPOSignatureSource = ParValue; break;
                            case "DocTitle": mIsPurchaseRequestForProductionEnabled = ParValue == "IMS"; break;
                            case "ProcFormatDocNo": IsProcFormat = ParValue; break;
                            case "FormatFTPClient": mFormatFTPClient = ParValue; break;
                            case "FileSizeMaxUploadFTPClient": mFileSizeMaxUploadFTPClient = ParValue; break;
                            case "PortForFTPClient": mPortForFTPClient = ParValue; break;
                            case "PasswordForFTPClient": mPasswordForFTPClient = ParValue; break;
                            case "UsernameForFTPClient": mUsernameForFTPClient = ParValue; break;
                            case "POUploadFileFormat": mPOUploadFileFormat = ParValue; break;
                            //case "DocTitle": mDocTitle = ParValue; break;

                            //decimal
                            case "POAmtForSignature": if (ParValue.Length > 0) mPOAmtForSignature = decimal.Parse(ParValue); break;
                        }
                    }
                }
                dr.Close();
            }
            if (mPOSignatureSource.Length == 0) mPOSignatureSource = "1";
            if (mPOUploadFileFormat.Length == 0) mPOUploadFileFormat = "1";
        }

        private void ParPrint(string DocNo)
        {
            string mDocTitle = Sm.GetParameter("DocTitle");

            var l = new List<PO>();
            var ldtl = new List<PODtl>();
            var ldtl2 = new List<PODtl2>();
            var ldtl3 = new List<PODtl3>();
            var ldtl4 = new List<PODtl4>();
            var ldtl5 = new List<PODtl5>();
            var l2 = new List<Employee>();
            var ldtl6 = new List<Item>();
            var lDtlS = new List<SignIMS>();
            var ldtlMR = new List<MR>();
            var ldtlQT = new List<QT>();
            var ldtlLOV = new List<LOV>();
            var lPOHDRIMS = new List<POHDRIMS>();
            
            string[] TableName = { "PO", "PODtl", "PODtl2", "PODtl3", "PODtl4", "Employee", "PODtl5", "Item", "SignIMS", "MR", "QT", "LOV", "POHDRIMS" };
            List<IList> myLists = new List<IList>();
            var cm = new MySqlCommand();

            #region Header
            var SQL = new StringBuilder();

            if (mIsFilterBySite)
            {
                if (mDocTitle == "MNET")
                {
                    SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, I.CompanyName, " +
                        "(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', " +
                        "(Select ParValue From TblParameter Where Parcode='ReportTitle3') As CompanyAddressCity, " +
                        "(Select ParValue From TblParameter Where Parcode = 'ReportTitle4') As 'CompanyPhone', " +
                        "(Select ParValue From TblParameter Where Parcode='ReportTitle5') As 'CompanyFax', ");
                }
                else
                    SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, I.CompanyName, I.CompanyPhone, I.CompanyFax, I.CompanyAddress, '' As CompanyAddressCity, ");
            }
            else
            {
                SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyAddressCity', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyPhone', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle5') As 'CompanyFax', "); 
            }
            SQL.AppendLine("If((Select parvalue From TblParameter Where ParCode = 'IsFilterLocalDocNo')='Y',A.LocalDocNo,A.Docno) As DocNo, DATE_FORMAT(A.DocDt,'%d %M %Y') As DocDt, "); 
            SQL.AppendLine("Concat(RIGHT(A.DocDt, 2), '/', RIGHT(LEFT(A.DocDt, 6), 2), '/', RIGHT(LEFT(A.DocDt, 4), 2))As DocDt2, A.VdContactPerson, A.ShipTo, A.BillTo, ");
            SQL.AppendLine("IfNull(A.TaxCode1, null) As TaxCode1,IfNull(A.TaxCode2, null) As TaxCode2, IfNull(A.TaxCode3, null) As TaxCode3, A.TaxAmt, A.CustomsTaxAmt, ");
            SQL.AppendLine("A.DiscountAmt, A.Amt, A.DownPayment, A.Remark As ARemark, F.VdName, ");
            SQL.AppendLine("(Select Z.TaxName From TblTax Z ");
            SQL.AppendLine("Inner Join TblPOHdr Y on Z.TaxCode = Y.TaxCode1 Where Y.DocNo= @DocNo) TaxName1, ");
            SQL.AppendLine("(Select Z.TaxName From TblTax Z ");
            SQL.AppendLine("Inner Join TblPOHdr Y on Z.TaxCode = Y.TaxCode2 Where Y.DocNo= @DocNo) TaxName2, ");
            SQL.AppendLine("(Select  Z.TaxName From TblTax Z ");
            SQL.AppendLine("Inner Join TblPOHdr Y on Z.TaxCode = Y.TaxCode3 Where Y.DocNo= @DocNo) TaxName3, ");
            if(mDocTitle == "IMS")
                SQL.AppendLine("Concat(Upper(left(G.UserCode,1)),Substring(Lower(G.UserCode), 2, Length(G.UserCode)))As CreateBy, ");
            else
                SQL.AppendLine("Concat(Upper(left(G.UserName,1)),Substring(Lower(G.UserName), 2, Length(G.UserName)))As CreateBy, ");
            SQL.AppendLine("(Select Z.TaxRate From TblTax Z ");
            SQL.AppendLine("Inner Join TblPOHdr Y on Z.TaxCode = Y.TaxCode1 Where Y.DocNo= @DocNo) TaxRate1, ");
            SQL.AppendLine("(Select Z.TaxRate From TblTax Z ");
            SQL.AppendLine("Inner Join TblPOHdr Y on Z.TaxCode = Y.TaxCode2 Where Y.DocNo= @DocNo) TaxRate2, ");
            SQL.AppendLine("(Select  Z.TaxRate From TblTax Z ");
            SQL.AppendLine("Inner Join TblPOHdr Y on Z.TaxCode = Y.TaxCode3 Where Y.DocNo= @DocNo) TaxRate3, ");
            SQL.AppendLine("F.Address, F.Phone, F.Fax, F.Email, ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyPhone', H.ContactNumber, E.CurCode, ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='IsPOSplitBasedOnTax') As SplitPO, J.SiteName, A.LocalDocNo, K.PtDay, ");
            SQL.AppendLine("Group_Concat(Distinct D.QtDocNo Separator '\r\n') QtDocNo, Date_Format(E.DocDt,'%d %M %Y')As QtDocDt , M.PosName, H.Position 'VdPOSName',  ");
            SQL.AppendLine("Concat(RIGHT(N.DocDt, 2), '/', RIGHT(LEFT(N.DocDt, 6), 2), '/', RIGHT(LEFT(N.DocDt, 4), 2))As MRDocDt, O.BankAcNo, P.BankName, ");
            SQL.AppendLine("Q.VdCtName, GROUP_CONCAT(DISTINCT N.DocNo SEPARATOR '\r\n') MRDocNo, R.ProjectName, ");
            SQL.AppendLine("Group_COncat(Distinct IfNull(U.DocNo, '') Separator '\r\n') LOVDocNo, ");
            SQL.AppendLine("Group_Concat(Distinct ifNull(K.PtName, '') Separator '\r\n') PtName, ");
            SQL.AppendLine("Group_Concat(Distinct IfNull(V.DTName, '') Separator '\r\n') DTName ");

            SQL.AppendLine(", SUM(E1.uprice * (B.Qty - ifnull(S.Qty, 0)))");
            SQL.AppendLine("+ SUM(E1.uprice * (B.Qty - ifnull(S.Qty, 0))) * A1.taxrate1");
            SQL.AppendLine("+ SUM(E1.uprice * (B.Qty - ifnull(S.Qty, 0))) * A1.taxrate2");
            SQL.AppendLine("+ SUM(E1.uprice * (B.Qty - ifnull(S.Qty, 0))) * A1.taxrate3");
            SQL.AppendLine("+ a.CustomsTaxAmt - a.discountAmt amt2");

            SQL.AppendLine("From TblPOHdr A ");

            SQL.AppendLine("INNER JOIN(");
            SQL.AppendLine("SELECT a.DocNo, ifnull(t1.TaxRate,0)/ 100 taxrate1, ifnull(t2.TaxRate, 0) / 100 taxrate2, ifnull(t3.TaxRate, 0) / 100 taxrate3, a.CustomsTaxAmt, a.DiscountAmt");
            SQL.AppendLine("from tblpohdr a");
            SQL.AppendLine("left JOIN tbltax t1 ON t1.TaxCode = a.TaxCode1");
            SQL.AppendLine("left JOIN tbltax t2 ON t2.TaxCode = a.TaxCode2");
            SQL.AppendLine("left JOIN tbltax t3 ON t3.TaxCode = a.TaxCode3");
            SQL.AppendLine("WHERE docno = @docno");
            SQL.AppendLine(") A1 ON A1.docno = A.docno");

            SQL.AppendLine("Inner Join TblPODtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblPORequestHdr C On B.PORequestDocNo=C.DocNo ");
            SQL.AppendLine("Inner Join TblPORequestDtl D On B.PORequestDocNo=D.DocNo And B.PORequestDNo=D.DNo ");
            SQL.AppendLine("Inner Join TblQtHdr E On D.QtDocNo=E.DocNo ");

            SQL.AppendLine("Inner Join tblqtdtl E1 On D.QtDocNo=E1.DocNo And D.QtDNo=E1.DNo");

            SQL.AppendLine("Inner Join TblVendor F On E.VdCode=F.VdCode ");
            SQL.AppendLine("Inner Join TblUser G On A.CreateBy=G.UserCode ");
            SQL.AppendLine("Left Join TblVendorContactPerson H On A.VdContactPerson=H.ContactPersonName And A.VdCode = H.VdCode ");
            SQL.AppendLine("Left Join TblSite J On A.SiteCode = J.SiteCode ");
            SQL.AppendLine("Left Join TblPaymentTerm K On E.PtCode=K.PtCode ");
            if (mIsFilterBySite)
            {
                SQL.AppendLine("Left Join (");
                SQL.AppendLine("    Select distinct A.DocNo, D.EntName As CompanyName, D.EntPhone As CompanyPhone, D.EntFax As CompanyFax, D.EntAddress As CompanyAddress ");
                SQL.AppendLine("    From TblPOhdr A  ");
                SQL.AppendLine("    Inner Join TblSite B On A.SiteCode = B.SiteCode  ");
                SQL.AppendLine("    Inner Join TblProfitCenter C On  B.ProfitCenterCode  = C.ProfitCenterCode  ");
                SQL.AppendLine("    Inner Join TblEntity D On C.EntCode = D.EntCode  ");
                SQL.AppendLine("    Where A.DocNo=@DocNo And A.SiteCode is not null ");
                SQL.AppendLine(") I On A.DocNo = I.DocNo ");
            }

            SQL.AppendLine("Left Join TblEmployee L On G.UserCode = L.UserCode ");
            SQL.AppendLine("Left Join TblPosition M On L.PosCode = M.PosCode ");
            SQL.AppendLine("Inner Join TblMaterialRequestHdr N On D.MaterialRequestDocNo = N.DocNo");
            SQL.AppendLine("Left Join TblVendorBankAccount O On F.VdCode = O.VdCode ");
            SQL.AppendLine("Left Join TblBank P On O.BankCode = P.BankCode ");
            SQL.AppendLine("Left Join TblVendorCategory Q On Q.VdCtCode = F.VdCtCode ");
            SQL.AppendLine("Left JOIN (  ");
            SQL.AppendLine("    SELECT T1.DocNo, GROUP_CONCAT(IFNULL(T2.ProjectName, '') Separator '\r\n') ProjectName ");
            SQL.AppendLine("    From TblMaterialRequestHdr T1  ");
            SQL.AppendLine("    Left Join TblProjectGroup T2 ON T1.PGCode = T2.PGCode  ");
            SQL.AppendLine("    Group By T1.DocNo ");
            SQL.AppendLine(") R ON R.DocNo = N.DocNo ");
            SQL.AppendLine("Left Join TblItemVendorCollectionDtl S On S.MRDocNo = D.MaterialRequestDocNo And S.MRDNo = D.MaterialRequestDNo ");
            SQL.AppendLine("    And S.CancelInd = 'N' ");
            SQL.AppendLine("Left Join TblVendorItCtGroupDtl T On T.ItemVendorCollectionDocNo = S.DocNo And T.ItemVendorCollectionDNo = S.DNo ");
            SQL.AppendLine("Left JOin TblVendorItCtGroupHDr U On U.DocNo = T.DocNO And U.CancelInd = 'N' ");
            SQL.AppendLine("Left Join TblDeliveryType V On E.DTCode = V.DTCode ");

            SQL.AppendLine("Left join(");
            SQL.AppendLine("Select PODocNo, PODNo, Sum(Qty) Qty");
            SQL.AppendLine(" From TblPOQtyCancel where cancelind = 'N'");
            SQL.AppendLine(" Group by PODocNo, PODNo");
            SQL.AppendLine(")S On B.DocNo = S.PODocNo And B.DNo = S.PODNo");

            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Group By A.DocNo ");
            SQL.AppendLine("; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
                if (mIsFilterBySite)
                {
                    string CompanyLogo = Sm.GetValue(
                       "Select D.EntLogoName " +
                       "From TblPOhdr A  " +
                       "Inner Join TblSite B On A.SiteCode = B.SiteCode " +
                       "Inner Join TblProfitCenter C On  B.ProfitCenterCode  = C.ProfitCenterCode " +
                       "Inner Join TblEntity D On C.EntCode = D.EntCode  " +
                       "Where A.Docno='" + TxtDocNo.Text + "' "
                   );
                    Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo2(CompanyLogo));
                }
                else
                {
                    Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo(mPOPrintOutCompanyLogo));
                }
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                         //0
                         "CompanyName",
                         //1-5
                         "CompanyAddress",
                         "CompanyAddressCity",
                         "CompanyPhone",
                         "CompanyFax",
                         "DocNo",                        
                         //6-10
                         "DocDt",
                         "VdContactPerson",
                         "ShipTo" ,
                         "BillTo" ,
                         "TaxCode1",
                         //11-15
                         "TaxCode2",
                         "TaxCode3" ,
                         "TaxAmt",
                         "CustomsTaxAmt",
                         "DiscountAmt" ,
                         //16-20
                         "Amt" ,
                         "DownPayment" ,
                         "VdName",
                         "CompanyLogo",
                         "ARemark",
                         //21-25
                         "TaxName1",
                         "TaxName2",
                         "Taxname3" ,
                         "CreateBy",
                         "TaxRate1",
                         //26-30
                         "TaxRate2",
                         "TaxRate3",
                         "Address",
                         "Phone",
                         "Fax",
                         //31-35
                         "Email",
                         "ContactNumber",
                         "SplitPO",
                         "SiteName",
                         "LocalDocNo",

                         //36-39
                         "CurCode",
                         "PtDay",
                         "QtDocNo",
                         "QtDocDt",
                         "PosName",

                         //40-45
                         "VdPosName",
                         "MRDocDt",
                         "BankAcNo",
                         "BankName",
                         "VdCtName",

                         //46-50
                         "DocDt2",
                         "MRDocNo",
                         "ProjectName",
                         "LOVDocNo",
                         "PTName",

                         //51-52
                         "DTName",
                         "Amt2" //Amt di detail dikurang qty cancel
                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new PO()
                        {
                            CompanyName = Sm.DrStr(dr, c[0]),

                            CompanyAddress = Sm.DrStr(dr, c[1]),
                            CompanyLongAddress = Sm.DrStr(dr, c[2]),
                            CompanyPhone = Sm.DrStr(dr, c[3]),
                            CompanyFax = Sm.DrStr(dr, c[4]),
                            DocNo = Sm.DrStr(dr, c[5]),

                            DocDt = Sm.DrStr(dr, c[6]),
                            VdContactPerson = Sm.DrStr(dr, c[7]),
                            ShipTo = Sm.DrStr(dr, c[8]),
                            BillTo = Sm.DrStr(dr, c[9]),
                            TaxCode1 = Sm.DrStr(dr, c[10]),

                            TaxCode2 = Sm.DrStr(dr, c[11]),
                            TaxCode3 = Sm.DrStr(dr, c[12]),
                            TaxAmt = Sm.DrDec(dr, c[13]),
                            CustomsTaxAmt = Sm.DrDec(dr, c[14]),
                            DiscountAmt = Sm.DrDec(dr, c[15]),

                            Amt = Sm.DrDec(dr, c[16]),
                            DownPayment = Sm.DrDec(dr, c[17]),
                            VdName = Sm.DrStr(dr, c[18]),
                            CompanyLogo = Sm.DrStr(dr, c[19]),
                            ARemark = Sm.DrStr(dr, c[20]),

                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime())),
                            TaxName1 = Sm.DrStr(dr, c[21]),
                            TaxName2 = Sm.DrStr(dr, c[22]),
                            TaxName3 = Sm.DrStr(dr, c[23]),
                            CreateBy = Sm.DrStr(dr, c[24]),
                            TaxRate1 = Sm.DrDec(dr, c[25]),

                            TaxRate2 = Sm.DrDec(dr, c[26]),
                            TaxRate3 = Sm.DrDec(dr, c[27]),
                            Address = Sm.DrStr(dr, c[28]),
                            Phone = Sm.DrStr(dr, c[29]),
                            Fax = Sm.DrStr(dr, c[30]),

                            Email = Sm.DrStr(dr, c[31]),
                            ContactNumber = Sm.DrStr(dr, c[32]),
                            SplitPO = Sm.DrStr(dr, c[33]),
                            SiteName = Sm.DrStr(dr, c[34]),
                            LocalDocNo = Sm.DrStr(dr, c[35]),

                            CurCode = Sm.DrStr(dr, c[36]),
                            PtDay = Sm.DrDec(dr, c[37]),
                            QtDocNo = Sm.DrStr(dr, c[38]),
                            QtDocDt = Sm.DrStr(dr, c[39]),

                            Terbilang = (mDocTitle == "IMS") ? Sm.Terbilang(Sm.DrDec(dr, c[52])) : Sm.Terbilang(Sm.DrDec(dr, c[16])),
                            Terbilang2 = (mDocTitle == "IMS") ? Sm.Terbilang2(Sm.DrDec(dr, c[52])) : Sm.Terbilang2(Sm.DrDec(dr, c[16])),

                            PosName = Sm.DrStr(dr, c[40]),
                            VdPosName = Sm.DrStr(dr, c[41]),
                            MRDocDt = Sm.DrStr(dr, c[42]),
                            BankAcNo = Sm.DrStr(dr, c[43]),

                            BankName = Sm.DrStr(dr, c[44]),
                            VdCtName = Sm.DrStr(dr, c[45]),
                            DocDt2 = Sm.DrStr(dr, c[46]),
                            MRDocNo = Sm.DrStr(dr, c[47]),
                            ProjectName = Sm.DrStr(dr, c[48]),
                            LOVDocNo = Sm.DrStr(dr, c[49]),
                            PtName = Sm.DrStr(dr, c[50]),

                            DTName = Sm.DrStr(dr, c[51]),
                        });
                    }
                }
                dr.Close();
            }
            myLists.Add(l);
            #endregion

            #region Detail data
            var cmDtl = new MySqlCommand();

            var SQLDtl = new StringBuilder();
            using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl.Open();
                cmDtl.Connection = cnDtl;
                SQLDtl.AppendLine("Select F.ItCode, I.ItName, ");
                SQLDtl.AppendLine("(B.Qty-ifnull(M.Qty, 0)) As Qty, I.PurchaseUomCode, K.PtName, G.CurCode, H.UPrice, ");
                SQLDtl.AppendLine("B.Discount, B.DiscountAmt, B.Roundingvalue, Concat(RIGHT(B.EstRecvDt, 2), '/', RIGHT(LEFT(B.EstRecvDt, 6), 2), '/', RIGHT(LEFT(B.EstRecvDt, 4), 2)) As EstRecvDt, IfNull(Date_Format(B.EstRecvDt, '%d/%m/%Y'), '') EstRecvDt2, B.Remark As DRemark, ");
                SQLDtl.AppendLine("(H.Uprice * (B.Qty-ifnull(M.Qty, 0))) - (H.Uprice*(B.Qty-ifnull(M.Qty, 0))*B.Discount/100) - B.DiscountAmt + B.Roundingvalue  As Total, L.DtName, I.ForeignName, I.ItCodeInternal, I.Specification, N.ProjectName ");
                SQLDtl.AppendLine("From TblPODtl B ");
                SQLDtl.AppendLine("Inner Join TblPORequestHdr C On B.PORequestDocNo=C.DocNo ");
                SQLDtl.AppendLine("Inner Join TblPORequestDtl D On B.PORequestDocNo=D.DocNo And B.PORequestDNo=D.DNo ");
                SQLDtl.AppendLine("Inner Join TblMaterialRequestHdr E On D.MaterialRequestDocNo=E.DocNo ");
                SQLDtl.AppendLine("Inner Join TblMaterialRequestDtl F On D.MaterialRequestDocNo=F.DocNo And D.MaterialRequestDNo=F.DNo ");
                SQLDtl.AppendLine("Inner Join TblQtHdr G On D.QtDocNo=G.DocNo ");
                SQLDtl.AppendLine("Inner Join TblQtDtl H On D.QtDocNo=H.DocNo And D.QtDNo=H.DNo ");
                SQLDtl.AppendLine("Inner Join TblItem I On F.ItCode=I.ItCode ");
                SQLDtl.AppendLine("Inner Join TblPaymentTerm K On G.PtCode=K.PtCode ");
                SQLDtl.AppendLine("Left Join TblDeliveryType L On G.DtCode = L.DtCode ");
                SQLDtl.AppendLine("Left join( ");
                SQLDtl.AppendLine("     Select PODocNo, PODNo, Sum(Qty)Qty ");
                SQLDtl.AppendLine("     From TblPOQtyCancel where cancelind='N' ");
                SQLDtl.AppendLine("     Group by PODocNo, PODNo ");
                SQLDtl.AppendLine(")M On B.DocNo=M.PODocNo And B.DNo=M.PODNo ");
                SQLDtl.AppendLine("Left Join TblProjectGroup N On E.PGCode=N.PGCode ");
                SQLDtl.AppendLine("Where B.CancelInd='N' AND B.DocNo=@DocNo; ");

                cmDtl.CommandText = SQLDtl.ToString();
                Sm.CmParam<String>(ref cmDtl, "@DocNo", DocNo);
                var drDtl = cmDtl.ExecuteReader();
                var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                        {
                         //0
                         "ItCode" ,

                         //1-5
                         "ItName" ,
                         "Qty",
                         "PurchaseUomCode" ,
                         "PtName",
                         "CurCode" ,

                         //6-10
                         "UPrice" ,
                         "Discount" ,
                         "DiscountAmt",
                         "RoundingValue",
                         "EstRecvDt" ,

                         //11-15
                         "DRemark" ,
                         "Total",
                         "DtName",
                         "ForeignName",
                         "ItCodeInternal",

                         //16-18
                         "Specification",
                         "ProjectName",
                         "EstRecvDt2"
                        });
                if (drDtl.HasRows)
                {
                    int nomor = 0;
                    while (drDtl.Read())
                    {
                        nomor = nomor + 1;
                        ldtl.Add(new PODtl()
                        {
                            nomor=nomor,
                            ItCode = Sm.DrStr(drDtl, cDtl[0]),
                            ItName = Sm.DrStr(drDtl, cDtl[1]),

                            Qty = Sm.DrDec(drDtl, cDtl[2]),
                            PurchaseUomCode = Sm.DrStr(drDtl, cDtl[3]),
                            PtName = Sm.DrStr(drDtl, cDtl[4]),
                            CurCode = Sm.DrStr(drDtl, cDtl[5]),

                            UPrice = Sm.DrDec(drDtl, cDtl[6]),
                            Discount = Sm.DrDec(drDtl, cDtl[7]),
                            DiscountAmt = Sm.DrDec(drDtl, cDtl[8]),
                            RoundingValue = Sm.DrDec(drDtl, cDtl[9]),
                            EstRecvDt = Sm.DrStr(drDtl, cDtl[10]),

                            DRemark = Sm.DrStr(drDtl, cDtl[11]),
                            DTotal = Sm.DrDec(drDtl, cDtl[12]),
                            DtName = Sm.DrStr(drDtl, cDtl[13]),
                            ForeignName = Sm.DrStr(drDtl, cDtl[14]),
                            ItCodeInternal = Sm.DrStr(drDtl, cDtl[15]),

                            Specification = Sm.DrStr(drDtl, cDtl[16]),
                            ProjectName = Sm.DrStr(drDtl, cDtl[17]),
                            EstRecvDt2 = Sm.DrStr(drDtl, cDtl[18]),
                        });
                    }
                }
                drDtl.Close();
            }
            myLists.Add(ldtl);
            #endregion

            #region Detail Signature

            var cmDtl2 = new MySqlCommand();

            var SQLDtl2 = new StringBuilder();
            using (var cnDtl2 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl2.Open();
                cmDtl2.Connection = cnDtl2;

                if (mIsPOApprovalActived)
                {
                    SQLDtl2.AppendLine("Select Distinct Concat(IfNull(T4.ParValue, ''), T1.UserCode, '.JPG') As Signature, ");
                    SQLDtl2.AppendLine("T1.UserCode, T1.UserName, T3.PosName, ");
                    SQLDtl2.AppendLine("T1.DNo, T1.Seq As Level, @Space As Space, ");
                    SQLDtl2.AppendLine("T1.Title, DATE_FORMAT(Max(T1.LastUpDt),'%d %M %Y') As LastUpDt, NULL as CompanyName ");
                    SQLDtl2.AppendLine("From ( ");

                    SQLDtl2.AppendLine("    Select UserCode, UserName, DNo, Seq, Title, LastUpDt From ( ");
                    SQLDtl2.AppendLine("        Select Distinct B.UserCode, C.UserName, ");
                    SQLDtl2.AppendLine("        B.ApprovalDNo As DNo, 200+D.Level As Seq, ");
                    SQLDtl2.AppendLine("        'Approved By' As Title, ");
                    SQLDtl2.AppendLine("        Left(B.LastUpDt, 8) As LastUpDt ");
                    SQLDtl2.AppendLine("        From TblPOHdr A ");
                    SQLDtl2.AppendLine("        Inner Join TblDocApproval B On B.DocType='PO' And A.DocNo=B.DocNo ");
                    SQLDtl2.AppendLine("        Inner Join TblUser C On B.UserCode=C.UserCode ");
                    SQLDtl2.AppendLine("        Inner Join TblDocApprovalSetting D On B.ApprovalDNo=D.DNo And D.DocType='PO' ");
                    SQLDtl2.AppendLine("        Where A.Status='A' ");
                    SQLDtl2.AppendLine("        And A.DocNo=@DocNo ");
                    SQLDtl2.AppendLine("        And B.UserCode Not In (Select CreateBy From TblPOHdr Where DocNo = @DocNo) ");
                    SQLDtl2.AppendLine("        Order By D.Level Desc ");
                    //SQLDtl2.AppendLine("        Limit 1 ");
                    SQLDtl2.AppendLine("    ) Tbl ");

                    SQLDtl2.AppendLine("    Union All ");
                    SQLDtl2.AppendLine("    Select Distinct ");
                    SQLDtl2.AppendLine("    A.CreateBy As UserCode, B.UserName, ");
                    SQLDtl2.AppendLine("    '1' As DNo, 0 As Seq, 'Created By' As Title, Left(A.CreateDt, 8) As LastUpDt ");
                    SQLDtl2.AppendLine("    From TblPOHdr A ");
                    SQLDtl2.AppendLine("    Inner Join TblUser B On A.CreateBy=B.UserCode ");
                    if ((!mIsOutstandingPOPrintEnabled || mIsOutstandingPOPrintEnabled) && TxtStatus.Text == "Approved")
                    {
                        SQLDtl2.AppendLine("    Where A.Status='A' ");
                        SQLDtl2.AppendLine("    And A.DocNo=@DocNo ");
                    }
                    else
                        SQLDtl2.AppendLine("    Where A.DocNo=@DocNo ");

                    SQLDtl2.AppendLine("    Union All ");
                    SQLDtl2.AppendLine("    Select UserCode, UserName, DNo, Seq, Title, LastUpDt From ( ");
                    SQLDtl2.AppendLine("        Select Distinct D.UserCode, E.UserName, ");
                    SQLDtl2.AppendLine("        D.ApprovalDNo As DNo, 100+F.Level As Seq, 'Approved By' As Title, Left(D.LastUpDt, 8) As LastUpDt ");
                    SQLDtl2.AppendLine("        From TblPOHdr A ");
                    SQLDtl2.AppendLine("        Inner Join TblPODtl B On A.DocNo=B.DocNo And B.CancelInd='N' ");
                    SQLDtl2.AppendLine("        Inner Join TblPORequestDtl C On B.PORequestDocNo=C.DocNo And B.PORequestDNo=C.DNo And C.Status='A' And C.CancelInd='N' ");
                    SQLDtl2.AppendLine("        Inner Join TblDocApproval D On D.DocType='PORequest' And C.DocNo=D.DocNo And C.DNo=D.DNo ");
                    SQLDtl2.AppendLine("        Inner Join TblUser E On D.UserCode=E.UserCode ");
                    SQLDtl2.AppendLine("        Inner Join TblDocApprovalSetting F On D.ApprovalDNo=F.DNo And F.DocType='PORequest' ");
                    SQLDtl2.AppendLine("        Where A.Status='A' ");
                    SQLDtl2.AppendLine("        And A.DocNo=@DocNo ");
                    SQLDtl2.AppendLine("        And D.UserCode Not In (Select CreateBy From TblPOHdr Where DocNo = @DocNo) ");
                    SQLDtl2.AppendLine("        Order By F.Level ");
                    //SQLDtl2.AppendLine("        Limit 1 ");
                    SQLDtl2.AppendLine("    ) Tbl ");
                    SQLDtl2.AppendLine(") T1 ");
                    SQLDtl2.AppendLine("Left Join TblEmployee T2 On T1.UserCode=T2.UserCode ");
                    SQLDtl2.AppendLine("Left Join TblPosition T3 On T2.PosCode=T3.PosCode ");
                    SQLDtl2.AppendLine("Left Join TblParameter T4 On T4.ParCode = 'ImgFileSignature' ");
                    SQLDtl2.AppendLine("Group  By T4.ParValue, T1.UserCode, T1.UserName, T3.PosName ");
                    SQLDtl2.AppendLine("Order By T1.Seq; ");
                }
                else
                {
                    if (mDocTitle != "MNET")
                    {
                        SQLDtl2.AppendLine("Select Distinct Concat(IfNull(T4.ParValue, ''), T1.UserCode, '.JPG') As Signature, ");
                        SQLDtl2.AppendLine("T1.UserCode, T1.UserName, T3.PosName, T1.DNo, Max(T1.Level) Level, @Space As Space, T1.Title, DATE_FORMAT(Max(T1.LastUpDt),'%d %M %Y') As LastUpDt, NULL as CompanyName ");
                        SQLDtl2.AppendLine("From ( ");
                        SQLDtl2.AppendLine("    Select Distinct ");
                        SQLDtl2.AppendLine("    B.UserCode, C.UserName, "); // Concat(Upper(left(C.UserName,1)),Substring(Lower(C.UserName), 2, Length(C.UserName))) As UserName,
                        SQLDtl2.AppendLine("    B.ApprovalDNo As DNo, D.Level, 'Approved By' As Title, Left(B.LastUpDt, 8) As LastUpDt ");
                        SQLDtl2.AppendLine("    From TblPODtl A ");
                        SQLDtl2.AppendLine("    Inner Join TblDocApproval B On B.DocType='PORequest' And A.PORequestDocNo=B.DocNo And A.PORequestDNo=B.DNo ");
                        SQLDtl2.AppendLine("    Inner Join TblUser C On B.UserCode=C.UserCode ");
                        SQLDtl2.AppendLine("    Inner Join TblDocApprovalSetting D On B.ApprovalDNo=D.DNo And D.DocType = 'POrequest' ");
                        SQLDtl2.AppendLine("    Where A.CancelInd='N' And A.DocNo=@DocNo ");
                        SQLDtl2.AppendLine("    Union All ");
                        SQLDtl2.AppendLine("    Select Distinct ");
                        SQLDtl2.AppendLine("    A.CreateBy As UserCode, B.UserName, "); //Concat(Upper(left(B.UserName,1)),Substring(Lower(B.UserName), 2, Length(B.UserName)))
                        SQLDtl2.AppendLine("    '1' As DNo, 0 As Level, 'Created By' As Title, Left(A.CreateDt, 8) As LastUpDt ");
                        SQLDtl2.AppendLine("    From TblPODtl A ");
                        SQLDtl2.AppendLine("    Inner Join TblUser B On A.CreateBy=B.UserCode ");
                        SQLDtl2.AppendLine("    Where A.CancelInd='N' And A.DocNo=@DocNo ");
                        SQLDtl2.AppendLine(") T1 ");
                        SQLDtl2.AppendLine("Left Join TblEmployee T2 On T1.UserCode=T2.UserCode ");
                        SQLDtl2.AppendLine("Left Join TblPosition T3 On T2.PosCode=T3.PosCode ");
                        SQLDtl2.AppendLine("Left Join TblParameter T4 On T4.ParCode = 'ImgFileSignature' ");
                        SQLDtl2.AppendLine("Group  By T4.ParValue, T1.UserCode, T1.UserName, T3.PosName, T1.DNo, T1.Title ");
                        SQLDtl2.AppendLine("Order By T1.Level; ");
                    }
                    else if(mDocTitle == "MNET")
                    {
                        #region old code
                        //SQLDtl2.AppendLine("SELECT *  ");
                        //SQLDtl2.AppendLine("FROM( ");
                        //SQLDtl2.AppendLine("	SELECT  ");
                        //SQLDtl2.AppendLine("		NULL AS Signature, ");
                        //SQLDtl2.AppendLine("		NULL AS UserCode, ");
                        //if(decimal.Parse(TxtAmt.Text) <= 250000000)
                        //{
                        //    SQLDtl2.AppendLine("		(SELECT ParValue FROM tblparameter WHERE Parcode='reportTitlePurchaseOrder1') AS UserName,  ");
                        //    SQLDtl2.AppendLine("		(SELECT ParValue FROM tblparameter WHERE Parcode='reportTitlePurchaseOrder2') AS PosName, ");
                        //}
                        //else if (decimal.Parse(TxtAmt.Text) > 250000000)
                        //{
                        //    SQLDtl2.AppendLine("		(SELECT ParValue FROM tblparameter WHERE Parcode='reportTitlePurchaseOrder3') AS UserName,  ");
                        //    SQLDtl2.AppendLine("		(SELECT ParValue FROM tblparameter WHERE Parcode='reportTitlePurchaseOrder4') AS PosName, ");
                        //}
                        //SQLDtl2.AppendLine("		NULL AS DNo,  ");
                        //SQLDtl2.AppendLine("		0 AS Level,  ");
                        //SQLDtl2.AppendLine("		@SPACE AS Space, ");
                        //SQLDtl2.AppendLine("		'Signed By' AS Title,  ");
                        //SQLDtl2.AppendLine("		NULL AS LastUpDt ");
                        //SQLDtl2.AppendLine("	UNION ALL ");
                        //SQLDtl2.AppendLine("	SELECT  ");
                        //SQLDtl2.AppendLine("		NULL AS Signature, ");
                        //SQLDtl2.AppendLine("		NULL AS UserCode, ");
                        //SQLDtl2.AppendLine("		B.ContactPersonName AS UserName,  ");
                        //SQLDtl2.AppendLine("		B.Position AS PosName,  ");
                        //SQLDtl2.AppendLine("		NULL AS DNo,  ");
                        //SQLDtl2.AppendLine("		1 AS Level,  ");
                        //SQLDtl2.AppendLine("		@SPACE AS Space, ");
                        //SQLDtl2.AppendLine("		'Job Executor' AS Title,  ");
                        //SQLDtl2.AppendLine("		NULL AS LastUpDt ");
                        //SQLDtl2.AppendLine("		FROM tblpohdr A ");
                        //SQLDtl2.AppendLine("	INNER JOIN tblvendorcontactperson B ON A.VdCode=B.VdCode AND A.VdContactPerson=B.ContactPersonName ");
                        //SQLDtl2.AppendLine("	WHERE A.DocNo = @DocNo ");
                        //SQLDtl2.AppendLine(")X ORDER BY X.Level ");
                        //SQLDtl2.AppendLine("; ");
                        #endregion

                        SQLDtl2.AppendLine("SELECT T.Signature, T.UserName, T.PosName, T.Space, T.Level, T.Title, T.LastUpDt, T.CompanyName ");
                        SQLDtl2.AppendLine("FROM( ");
                        SQLDtl2.AppendLine("	SELECT 0 AS Level, 'Signed By' AS Title, 'PT Metranet' AS CompanyName, C.UserName AS UserName, B.OptDesc As PosName, D.UserSign AS Signature, NULL AS LastUpDt, @Space as Space ");
                        SQLDtl2.AppendLine("	FROM tblpohdr A ");
                        SQLDtl2.AppendLine("	Left Join TblOption B On A.SignBy = B.optCode And B.OptCat = 'POSignBy' ");
                        SQLDtl2.AppendLine("	LEFT JOIN tbluser C ON B.OptCode=C.UserCode ");
                        SQLDtl2.AppendLine("	LEFT JOIN(   ");
                        SQLDtl2.AppendLine("	  SELECT X1.UserCode, Concat(IfNull(X2.ParValue, ''), X1.UserCode, '.JPG') AS UserSign   ");
                        SQLDtl2.AppendLine("	  from tbluser X1   ");
                        SQLDtl2.AppendLine("	  left JOIN tblparameter X2 ON X2.ParCode = 'ImgFileSignature'   ");
                        SQLDtl2.AppendLine("	)D ON C.UserCode = D.UserCode  ");
                        SQLDtl2.AppendLine("	WHERE A.DocNo=@DocNo "); //AND (SELECT ParValue FROM tblparameter WHERE parcode = 'IsPOUsePOSignedBy') = 'Y'
                        SQLDtl2.AppendLine("	UNION ALL  ");
                        SQLDtl2.AppendLine("	SELECT 1 AS Level, 'Job Executor' AS Title, B.VdName AS CompanyName, A.VdContactPerson AS UserName, C.Position AS PosName, NULL AS Signature, NULL AS LastUpDt, @Space as Space ");
                        SQLDtl2.AppendLine("	FROM tblpohdr A ");
                        SQLDtl2.AppendLine("	LEFT JOIN tblvendor B ON A.VdCode=B.VdCode ");
                        SQLDtl2.AppendLine("	LEFT JOIN tblvendorcontactperson C ON B.VdCode=C.VdCode AND A.VdContactPerson=C.ContactPersonName ");
                        SQLDtl2.AppendLine("	WHERE A.DocNo=@DocNo ");
                        SQLDtl2.AppendLine(")T ");
                    }
                }
                cmDtl2.CommandText = SQLDtl2.ToString();
                Sm.CmParam<String>(ref cmDtl2, "@Space", "                   ");
                Sm.CmParam<String>(ref cmDtl2, "@DocNo", DocNo);
                var drDtl2 = cmDtl2.ExecuteReader();
                var cDtl2 = Sm.GetOrdinal(drDtl2, new string[] 
                        {
                         //0
                         "Signature" ,

                         //1-5
                         "Username" ,
                         "PosName",
                         "Space",
                         "Level",
                         "Title",

                         //6-7
                         "LastupDt", 
                         "CompanyName"
                        });
                if (drDtl2.HasRows)
                {
                    while (drDtl2.Read())
                    {

                        ldtl2.Add(new PODtl2()
                        {
                            Signature = Sm.DrStr(drDtl2, cDtl2[0]),
                            UserName = Sm.DrStr(drDtl2, cDtl2[1]),
                            PosName = Sm.DrStr(drDtl2, cDtl2[2]),
                            Space = Sm.DrStr(drDtl2, cDtl2[3]),
                            DNo = Sm.DrStr(drDtl2, cDtl2[4]),
                            Title = Sm.DrStr(drDtl2, cDtl2[5]),
                            LastUpDt = Sm.DrStr(drDtl2, cDtl2[6]),
                            CompanyName = Sm.DrStr(drDtl2, cDtl2[7])
                        });
                    }
                }
                drDtl2.Close();
            }
            myLists.Add(ldtl2);
            #endregion

            #region Detail date
            var cmDtl3 = new MySqlCommand();

            var SQLDtl3 = new StringBuilder();
            using (var cnDtl3 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl3.Open();
                cmDtl3.Connection = cnDtl3;

                SQLDtl3.AppendLine("Select * From ( ");
                SQLDtl3.AppendLine("Select DATE_FORMAT(SUBSTRING(LastUpDt, 1, 8),'%d %M %Y') As LastUpDt, ApprovalDNo As DNo ");
                SQLDtl3.AppendLine("From TblDocApproval ");
                SQLDtl3.AppendLine("Where DocType = 'PORequest' ");
                SQLDtl3.AppendLine("And DocNo In (Select PORequestDocNo From TblPODTl Where DocNo=@DocNo) ");
                SQLDtl3.AppendLine("Group By DocNo, ApprovalDNo ");
                SQLDtl3.AppendLine("UNION ");
                SQLDtl3.AppendLine("Select DATE_FORMAT(SUBSTRING(CreateDt, 1, 8),'%d %M %Y') As LastUpDt, '1' As DNo ");
                SQLDtl3.AppendLine("From TblPOHdr ");
                SQLDtl3.AppendLine("Where DocNo = @DocNo ");
                SQLDtl3.AppendLine(") G1 ");
                SQLDtl3.AppendLine("Order By G1.DNo ");


                cmDtl3.CommandText = SQLDtl3.ToString();
                Sm.CmParam<String>(ref cmDtl3, "@DocNo", DocNo);
                var drDtl3 = cmDtl3.ExecuteReader();
                var cDtl3 = Sm.GetOrdinal(drDtl3, new string[] 
                        {
                         //0
                         "LastUpdt", 
                         "DNo"
                        });
                if (drDtl3.HasRows)
                {
                    while (drDtl3.Read())
                    {
                        ldtl3.Add(new PODtl3()
                        {
                            LastUpDt = Sm.DrStr(drDtl3, cDtl3[0]),
                            DNo = Sm.DrStr(drDtl3, cDtl3[1])
                        });
                    }
                }
                drDtl3.Close();
            }
            myLists.Add(ldtl3);
            #endregion

            #region Detail note
            var cmDtl4 = new MySqlCommand();

            var SQLDtl4 = new StringBuilder();
            using (var cnDtl4 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl4.Open();
                cmDtl4.Connection = cnDtl4;

                SQLDtl4.AppendLine("Select ParValue From TblParameter Where ParCode='PONotes';");

                cmDtl4.CommandText = SQLDtl4.ToString();
                Sm.CmParam<String>(ref cmDtl4, "@DocNo", DocNo);
                var drDtl4 = cmDtl4.ExecuteReader();
                var cDtl4 = Sm.GetOrdinal(drDtl4, new string[] 
                        {
                         //0
                         "ParValue"
                        });
                if (drDtl4.HasRows)
                {
                    while (drDtl4.Read())
                    {
                        ldtl4.Add(new PODtl4()
                        {
                            Note = Sm.DrStr(drDtl4, cDtl4[0])
                        });
                    }
                }
                drDtl4.Close();
            }
            myLists.Add(ldtl4);
            #endregion

            #region Signature KIM
            var cm2 = new MySqlCommand();
            var SQL2 = new StringBuilder();


            SQL2.AppendLine("Select A.EmpCode, A.EmpName, B.PosName From TblEmployee A ");
            SQL2.AppendLine("Inner Join TblPosition B On A.PosCode=B.PosCode ");
            if (Decimal.Parse(TxtAmt.Text) < mPOAmtForSignature)
            {
                SQL2.AppendLine("Where A.EmpCode= Substring_index(@EmpCode, '#', 1) ");
            }
            else {
                SQL2.AppendLine("Where A.EmpCode= Substring_index(@EmpCode, '#', -1) ");
            }

            SQL2.AppendLine(";");
            

            using (var cn2 = new MySqlConnection(Gv.ConnectionString))
            {
                cn2.Open();
                cm2.Connection = cn2;
                cm2.CommandText = SQL2.ToString();
                Sm.CmParam<String>(ref cm2, "@EmpCode", mEmpCodePO);
                var dr2 = cm2.ExecuteReader();
                var c2 = Sm.GetOrdinal(dr2, new string[] 
                        {
                         //0-2
                         "EmpCode",
                         "EmpName",
                         "PosName"
                        });
                if (dr2.HasRows)
                {
                    while (dr2.Read())
                    {
                        l2.Add(new Employee()
                        {
                            EmpCode = Sm.DrStr(dr2, c2[0]),
                            EmpName = Sm.DrStr(dr2, c2[1]),
                            Position = Sm.DrStr(dr2, c2[2])
                        });
                    }
                }
                dr2.Close();
            }
            myLists.Add(l2);

            #endregion

            #region Detail5 
            // TOP, EstimatedDt AWG
            var cmDtl5 = new MySqlCommand();

            var SQLDtl5 = new StringBuilder();
            using (var cnDtl5 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl5.Open();
                cmDtl5.Connection = cnDtl5;

                SQLDtl5.AppendLine("Select A.DocNo, ");
                SQLDtl5.AppendLine("Group_concat(Distinct E.PtName separator ', ' )As PtName, ");
                SQLDtl5.AppendLine("Group_Concat(Distinct Date_Format(A.EstRecvDt, '%d/%m/%Y')separator ',')As EstRecvDt ");
                SQLDtl5.AppendLine("From TblPODtl A ");
                SQLDtl5.AppendLine("Inner Join TblPORequestHdr B On A.PORequestDocNo=B.DocNo ");
                SQLDtl5.AppendLine("Inner Join TblPORequestDtl C On A.PORequestDocNo=C.DocNo And A.PORequestDNo=C.DNo ");
                SQLDtl5.AppendLine("Inner Join TblQtHdr D On C.QtDocNo=D.DocNo ");
                SQLDtl5.AppendLine("Inner Join TblPaymentTerm E On D.PtCode=E.PtCode ");
                SQLDtl5.AppendLine("Where A.CancelInd='N' AND A.DocNo=@DocNo ");
                SQLDtl5.AppendLine("Group by A.DocNo ");


                cmDtl5.CommandText = SQLDtl5.ToString();
                Sm.CmParam<String>(ref cmDtl5, "@DocNo", DocNo);
                var drDtl5 = cmDtl5.ExecuteReader();
                var cDtl5 = Sm.GetOrdinal(drDtl5, new string[] 
                        {
                         //0-2
                         "DocNo",
                         "PtName", 
                         "EstRecvDt"
                        });
                if (drDtl5.HasRows)
                {
                    while (drDtl5.Read())
                    {
                        ldtl5.Add(new PODtl5()
                        {
                            DocNo = Sm.DrStr(drDtl5, cDtl5[0]),
                            PtName = Sm.DrStr(drDtl5, cDtl5[1]),
                            EstRecvDt = Sm.DrStr(drDtl5, cDtl5[2])
                        });
                    }
                }
                drDtl5.Close();
            }
            myLists.Add(ldtl5);
            #endregion

            #region Item
            if (Grd1.Rows.Count > 1)
            {
                string mItName = string.Empty;
                for (int i = 0; i < Grd1.Rows.Count; i++)
                {
                    if (Sm.GetGrdStr(Grd1, i, 15).Length > 0)
                    {
                        if (mItName.Length > 0) mItName += ", ";
                        mItName += Sm.GetGrdStr(Grd1, i, 15);
                    }
                }

                ldtl6.Add(new Item() { 
                    ItemName = mItName
                });

            }
            myLists.Add(ldtl6);
            #endregion

            #region Detail Signature IMS

            //Disetujui Oleh
            var cmDtlS = new MySqlCommand();
            var CurCode = Sm.GetValue("Select CurCode From TblPOHdr Where DocNo='" + TxtDocNo.Text + "' ");
            var DocDt = Sm.GetValue("Select DocDt From TblPOHdr Where DocNo='" + TxtDocNo.Text + "' ");
            var SQLDtlS = new StringBuilder();
            using (var cnDtlS = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtlS.Open();
                cmDtlS.Connection = cnDtlS;

                if (mIsPOSignatureWithRangeEnabled)
                {
                    if (CurCode == "IDR")
                    {
                        SQLDtlS.AppendLine("Select EmpName, PosName From ( ");
                        SQLDtlS.AppendLine("Select 1 As Level, E.EmpName, F.PosName  ");
                        SQLDtlS.AppendLine("From TblPOHdr A ");
                        SQLDtlS.AppendLine("Inner Join (Select Cast(ParValue As Decimal(18,4)) As ParValue From TblParameter Where ParValue Is Not Null And ParCode='POPrintOutApprovalRangeMin1') B On 1=1 ");
                        SQLDtlS.AppendLine("Inner Join (Select Cast(ParValue As Decimal(18,4)) As ParValue From TblParameter Where ParValue Is Not Null And ParCode='POPrintOutApprovalRangeMin2') C On 1=1 ");
                        SQLDtlS.AppendLine("Inner Join TblParameter D On D.ParValue Is Not Null And D.ParCode='POPrintOutApprovalBy1' ");
                        SQLDtlS.AppendLine("Inner Join TblEmployee E On D.ParValue=E.EmpCode ");
                        SQLDtlS.AppendLine("Left Join TblPosition F On E.PosCode=F.PosCode ");
                        SQLDtlS.AppendLine("Where A.DocNo=@DocNo ");
                        SQLDtlS.AppendLine("And A.Amt>=IfNull(B.ParValue, 0.00) And A.Amt<=IfNull(C.ParValue, 99999999999999) ");
                        SQLDtlS.AppendLine("Union All ");
                        SQLDtlS.AppendLine("Select 2 As Level, E.EmpName, F.PosName  ");
                        SQLDtlS.AppendLine("From TblPOHdr A ");
                        SQLDtlS.AppendLine("Inner Join (Select Cast(ParValue As Decimal(18,4)) As ParValue From TblParameter Where ParValue Is Not Null And ParCode='POPrintOutApprovalRangeMin2') B On 1=1 ");
                        SQLDtlS.AppendLine("Inner Join (Select Cast(ParValue As Decimal(18,4)) As ParValue From TblParameter Where ParValue Is Not Null And ParCode='POPrintOutApprovalRangeMin3') C On 1=1 ");
                        SQLDtlS.AppendLine("Inner Join TblParameter D On D.ParValue Is Not Null And D.ParCode='POPrintOutApprovalBy2' ");
                        SQLDtlS.AppendLine("Inner Join TblEmployee E On D.ParValue=E.EmpCode ");
                        SQLDtlS.AppendLine("Left Join TblPosition F On E.PosCode=F.PosCode ");
                        SQLDtlS.AppendLine("Where A.DocNo=@DocNo ");
                        SQLDtlS.AppendLine("And A.Amt>IfNull(B.ParValue, 0.00) And A.Amt<=IfNull(C.ParValue, 99999999999999) ");
                        SQLDtlS.AppendLine("Union All ");
                        SQLDtlS.AppendLine("Select 3 As Level, E.EmpName, F.PosName  ");
                        SQLDtlS.AppendLine("From TblPOHdr A ");
                        SQLDtlS.AppendLine("Inner Join (Select Cast(ParValue As Decimal(18,4)) As ParValue From TblParameter Where ParValue Is Not Null And ParCode='POPrintOutApprovalRangeMin3') B On 1=1 ");
                        SQLDtlS.AppendLine("Left Join (Select Cast(ParValue As Decimal(18,4)) As ParValue From TblParameter Where ParValue Is Not Null And ParCode='POPrintOutApprovalRangeMin4') C On 1=1 ");
                        SQLDtlS.AppendLine("Inner Join TblParameter D On D.ParValue Is Not Null And D.ParCode='POPrintOutApprovalBy3' ");
                        SQLDtlS.AppendLine("Inner Join TblEmployee E On D.ParValue=E.EmpCode ");
                        SQLDtlS.AppendLine("Left Join TblPosition F On E.PosCode=F.PosCode ");
                        SQLDtlS.AppendLine("Where A.DocNo=@DocNo ");
                        SQLDtlS.AppendLine("And A.Amt>IfNull(B.ParValue, 0.00) And A.Amt<=IfNull(C.ParValue, 99999999999999) ");
                        SQLDtlS.AppendLine("Union All ");
                        SQLDtlS.AppendLine("Select 4 As Level, D.EmpName, E.PosName  ");
                        SQLDtlS.AppendLine("From TblPOHdr A ");
                        SQLDtlS.AppendLine("Inner Join (Select Cast(ParValue As Decimal(18,4)) As ParValue From TblParameter Where ParValue Is Not Null And ParCode='POPrintOutApprovalRangeMin4') B On 1=1 ");
                        SQLDtlS.AppendLine("Inner Join TblParameter C On C.ParValue Is Not Null And C.ParCode='POPrintOutApprovalBy4' ");
                        SQLDtlS.AppendLine("Inner Join TblEmployee D On C.ParValue=D.EmpCode ");
                        SQLDtlS.AppendLine("Left Join TblPosition E On D.PosCode=E.PosCode ");
                        SQLDtlS.AppendLine("Where A.DocNo=@DocNo ");
                        SQLDtlS.AppendLine("And A.Amt>IfNull(B.ParValue, 0.00) ");
                        SQLDtlS.AppendLine(") T Order By Level Desc Limit 1;");
                    }
                    else
                    {
                        
                        SQLDtlS.AppendLine("Select EmpName, PosName From (  ");
                        SQLDtlS.AppendLine("Select 1 As Level, E.EmpName, F.PosName ");  
                        SQLDtlS.AppendLine("From TblPOHdr A  ");
                        SQLDtlS.AppendLine("Inner Join (Select Cast(ParValue As Decimal(18,4)) As ParValue From TblParameter Where ParValue Is Not Null And ParCode='POPrintOutApprovalRangeMin1') B On 1=1 "); 
                        SQLDtlS.AppendLine("Inner Join (Select Cast(ParValue As Decimal(18,4)) As ParValue From TblParameter Where ParValue Is Not Null And ParCode='POPrintOutApprovalRangeMin2') C On 1=1  ");
                        SQLDtlS.AppendLine("Inner Join TblParameter D On D.ParValue Is Not Null And D.ParCode='POPrintOutApprovalBy1'  ");
                        SQLDtlS.AppendLine("Inner Join TblEmployee E On D.ParValue=E.EmpCode  ");
                        SQLDtlS.AppendLine("Left Join TblPosition F On E.PosCode=F.PosCode  ");
                        SQLDtlS.AppendLine("INNER  JOIN  ");
                        SQLDtlS.AppendLine("( ");
                        SQLDtlS.AppendLine("Select Amt From tblcurrencyrate  ");
                        SQLDtlS.AppendLine("Where RateDt<=@DocDt And CurCode1=@CurCode And CurCode2='IDR' ");
                        SQLDtlS.AppendLine("Order By RateDt Desc LIMIT 1 ");
                        SQLDtlS.AppendLine(")G ON 0=0 ");
                        SQLDtlS.AppendLine("Where A.DocNo=@DocNo  ");
                        SQLDtlS.AppendLine("and (A.Amt*G.Amt)>=IfNull(B.ParValue, 0.00) and (A.Amt*G.Amt)<=IfNull(C.ParValue, 99999999999999) ");

                        SQLDtlS.AppendLine("Union All  ");
                        SQLDtlS.AppendLine("Select 2 As Level, E.EmpName, F.PosName  "); 
                        SQLDtlS.AppendLine("From TblPOHdr A  ");
                        SQLDtlS.AppendLine("Inner Join (Select Cast(ParValue As Decimal(18,4)) As ParValue From TblParameter Where ParValue Is Not Null And ParCode='POPrintOutApprovalRangeMin2') B On 1=1  ");
                        SQLDtlS.AppendLine("Inner Join (Select Cast(ParValue As Decimal(18,4)) As ParValue From TblParameter Where ParValue Is Not Null And ParCode='POPrintOutApprovalRangeMin3') C On 1=1  ");
                        SQLDtlS.AppendLine("Inner Join TblParameter D On D.ParValue Is Not Null And D.ParCode='POPrintOutApprovalBy2'  ");
                        SQLDtlS.AppendLine("Inner Join TblEmployee E On D.ParValue=E.EmpCode  ");
                        SQLDtlS.AppendLine("Left Join TblPosition F On E.PosCode=F.PosCode  ");
                        SQLDtlS.AppendLine("INNER  JOIN  ");
                        SQLDtlS.AppendLine("( ");
                        SQLDtlS.AppendLine("SELECT Amt From tblcurrencyrate  ");
                        SQLDtlS.AppendLine("Where RateDt<=@DocDt And CurCode1=@CurCode And CurCode2='IDR' ");
                        SQLDtlS.AppendLine("Order By RateDt Desc LIMIT 1 ");
                        SQLDtlS.AppendLine(")G ON 0=0 ");
                        SQLDtlS.AppendLine("Where A.DocNo=@DocNo  ");
                        SQLDtlS.AppendLine("and (A.Amt*G.Amt)>IfNull(B.ParValue, 0.00) and (A.Amt*G.Amt)<=IfNull(C.ParValue, 99999999999999) ");


                        SQLDtlS.AppendLine("Union All  ");
                        SQLDtlS.AppendLine("Select 3 As Level, E.EmpName, F.PosName  "); 
                        SQLDtlS.AppendLine("From TblPOHdr A  ");
                        SQLDtlS.AppendLine("Inner Join (Select Cast(ParValue As Decimal(18,4)) As ParValue From TblParameter Where ParValue Is Not Null And ParCode='POPrintOutApprovalRangeMin3') B On 1=1  ");
                        SQLDtlS.AppendLine("Left Join (Select Cast(ParValue As Decimal(18,4)) As ParValue From TblParameter Where ParValue Is Not Null And ParCode='POPrintOutApprovalRangeMin4') C On 1=1 "); 
                        SQLDtlS.AppendLine("Inner Join TblParameter D On D.ParValue Is Not Null And D.ParCode='POPrintOutApprovalBy3' "); 
                        SQLDtlS.AppendLine("Inner Join TblEmployee E On D.ParValue=E.EmpCode  ");
                        SQLDtlS.AppendLine("Left Join TblPosition F On E.PosCode=F.PosCode  ");
                        SQLDtlS.AppendLine("INNER  JOIN  ");
                        SQLDtlS.AppendLine("( ");
                        SQLDtlS.AppendLine("SELECT Amt From tblcurrencyrate  ");
                        SQLDtlS.AppendLine("Where RateDt<=@DocDt And CurCode1=@CurCode And CurCode2='IDR' ");
                        SQLDtlS.AppendLine("Order By RateDt Desc LIMIT 1 ");
                        SQLDtlS.AppendLine(")G ON 0=0 ");
                        SQLDtlS.AppendLine("Where A.DocNo=@DocNo  ");
                        SQLDtlS.AppendLine("and (A.Amt*G.Amt)>IfNull(B.ParValue, 0.00) and (A.Amt*G.Amt)<=IfNull(C.ParValue, 99999999999999) ");


                        SQLDtlS.AppendLine("Union All  ");
                        SQLDtlS.AppendLine("Select 4 As Level, D.EmpName, E.PosName  "); 
                        SQLDtlS.AppendLine("From TblPOHdr A  ");
                        SQLDtlS.AppendLine("Inner Join (Select Cast(ParValue As Decimal(18,4)) As ParValue From TblParameter Where ParValue Is Not Null And ParCode='POPrintOutApprovalRangeMin4') B On 1=1  ");
                        SQLDtlS.AppendLine("Inner Join TblParameter C On C.ParValue Is Not Null And C.ParCode='POPrintOutApprovalBy4'  ");
                        SQLDtlS.AppendLine("Inner Join TblEmployee D On C.ParValue=D.EmpCode  ");
                        SQLDtlS.AppendLine("Left Join TblPosition E On D.PosCode=E.PosCode  ");
                        SQLDtlS.AppendLine("INNER  JOIN  ");
                        SQLDtlS.AppendLine("( ");
                        SQLDtlS.AppendLine("SELECT Amt From tblcurrencyrate  ");
                        SQLDtlS.AppendLine("Where RateDt<=@DocDt And CurCode1=@CurCode And CurCode2='IDR' ");
                        SQLDtlS.AppendLine("Order By RateDt Desc LIMIT 1 ");
                        SQLDtlS.AppendLine(")G ON 0=0 ");
                        SQLDtlS.AppendLine("Where A.DocNo=@DocNo  ");
                        SQLDtlS.AppendLine("AND (A.Amt*G.Amt)>IfNull(B.ParValue, 0.00)  ");
                        SQLDtlS.AppendLine(") T Order By Level Desc Limit 1; ");
                    }
                }
                else
                {
                    if (mPOSignatureSource == "1")
                    {
                        SQLDtlS.AppendLine("Select A.EmpName, B.PosName  ");
                        SQLDtlS.AppendLine("From TblEmployee A  ");
                        SQLDtlS.AppendLine("Left Join TblPosition B On A.PosCode=B.PosCode  ");
                        SQLDtlS.AppendLine("Where A.EmpCode=@EmpCode;");
                    }
                    else
                    {
                        SQLDtlS.AppendLine("Select Distinct ");
                        SQLDtlS.AppendLine("IfNull(E.EmpName, C.UserName) EmpName, F.PosName ");
                        SQLDtlS.AppendLine("From TblPODtl A ");
                        SQLDtlS.AppendLine("Inner Join TblDocApproval B On B.DocType='PORequest' And A.PORequestDocNo=B.DocNo And A.PORequestDNo=B.DNo ");
                        SQLDtlS.AppendLine("Inner Join TblUser C On B.UserCode=C.UserCode ");
                        SQLDtlS.AppendLine("Inner Join (Select Doctype, DeptCode, Max(Level) MaxLvl From TblDocApprovalSetting Where Doctype = 'PORequest' Group By DocType, DeptCode) C1 ");
                        SQLDtlS.AppendLine("Inner Join TblDocApprovalSetting D On B.ApprovalDNo=D.DNo And D.DocType = 'POrequest' ");
                        SQLDtlS.AppendLine("    And C1.DocType = D.Doctype And C1.DeptCode = D.DeptCode And C1.MaxLvl = D.Level ");
                        SQLDtlS.AppendLine("Left Join TblEmployee E On C.UserCode = E.UserCode ");
                        SQLDtlS.AppendLine("Left Join TblPosition F On E.PosCode = F.PosCode ");
                        SQLDtlS.AppendLine("Where A.CancelInd='N' And A.DocNo=@DocNo ");
                        SQLDtlS.AppendLine("Limit 1 ");
                        SQLDtlS.AppendLine("; ");
                    }
                    Sm.CmParam<String>(ref cmDtlS, "@EmpCode", mPOAutoApprovedByEmpCode);
                }

                cmDtlS.CommandText = SQLDtlS.ToString();
                Sm.CmParam<String>(ref cmDtlS, "@Space", "-------------------------");
                Sm.CmParam<String>(ref cmDtlS, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cmDtlS, "@CurCode", CurCode);
                Sm.CmParam<String>(ref cmDtlS, "@DocDt", DocDt);
                
                
                var drDtlS = cmDtlS.ExecuteReader();
                var cDtlS = Sm.GetOrdinal(drDtlS, new string[]{ "EmpName", "PosName" });
                if (drDtlS.HasRows)
                {
                    while (drDtlS.Read())
                    {
                        lDtlS.Add(new SignIMS()
                        {
                            EmpName = Sm.DrStr(drDtlS, cDtlS[0]),
                            PosName = Sm.DrStr(drDtlS, cDtlS[1]),
                        });
                    }
                }
                drDtlS.Close();
            }
            myLists.Add(lDtlS);


            #endregion

            #region DetailMR
            // IMS
            var cmDtlMR = new MySqlCommand();

            var SQLDtlMR = new StringBuilder();
            using (var cnDtlMR = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtlMR.Open();
                cmDtlMR.Connection = cnDtlMR;

                SQLDtlMR.AppendLine("Select DISTINCT A.DocNo, ");
                SQLDtlMR.AppendLine("N.DocNo As MRDocNo ");
                SQLDtlMR.AppendLine("From TblPOHdr A  ");
                SQLDtlMR.AppendLine("Inner Join TblPODtl B On A.DocNo=B.DocNo  ");
                SQLDtlMR.AppendLine("Inner Join TblPORequestHdr C On B.PORequestDocNo=C.DocNo ");
                SQLDtlMR.AppendLine("Inner Join TblPORequestDtl D On B.PORequestDocNo=D.DocNo And B.PORequestDNo=D.DNo ");
                SQLDtlMR.AppendLine("Inner Join TblQtHdr E On D.QtDocNo=E.DocNo ");
                SQLDtlMR.AppendLine("Inner Join TblMaterialRequestHdr N On D.MaterialRequestDocNo = N.DocNo ");
                SQLDtlMR.AppendLine("Left Join TblItemVendorCollectionDtl S On S.MRDocNo = D.MaterialRequestDocNo And S.MRDNo = D.MaterialRequestDNo  ");
                SQLDtlMR.AppendLine("And S.CancelInd = 'N' ");
                SQLDtlMR.AppendLine("LEFT Join ( ");
                SQLDtlMR.AppendLine("    select A.DocNo, B.ItemVendorCollectionDOcnO, B.ItemVendorCollectionDno   ");
                SQLDtlMR.AppendLine("    from TblVendorItCtGroupHdr A ");
                SQLDtlMR.AppendLine("    Inner join TblVendorItCtGroupDtl B On A.DocNo = b.Docno ");
                SQLDtlMR.AppendLine("    Where CancelInd = 'N' ");
                SQLDtlMR.AppendLine(")T On T.ItemVendorCollectionDOcnO = S.DocNo And T.ItemVendorCollectionDNO = S.DNo ");
                SQLDtlMR.AppendLine("Where A.DocNo=@DocNo  ");
                SQLDtlMR.AppendLine(" ;  ");


                cmDtlMR.CommandText = SQLDtlMR.ToString();
                Sm.CmParam<String>(ref cmDtlMR, "@DocNo", DocNo);
                var drDtlMR = cmDtlMR.ExecuteReader();
                var cDtlMR = Sm.GetOrdinal(drDtlMR, new string[] 
                        {
                         //0-2
                         "DocNo", "MRDocNo",
                        });
                if (drDtlMR.HasRows)
                {
                    while (drDtlMR.Read())
                    {
                        ldtlMR.Add(new MR()
                        {
                            PODocNo = Sm.DrStr(drDtlMR, cDtlMR[0]),
                            MRDocNo = Sm.DrStr(drDtlMR, cDtlMR[1])
                        });
                    }
                }
                drDtlMR.Close();
            }
            myLists.Add(ldtlMR);
            #endregion

            #region DetailQT
            // IMS
            var cmDtlQT = new MySqlCommand();

            var SQLDtlQT = new StringBuilder();
            using (var cnDtlQT = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtlQT.Open();
                cmDtlQT.Connection = cnDtlQT;

                SQLDtlQT.AppendLine("Select Distinct A.Docno, ");
                SQLDtlQT.AppendLine("D.QtDocNo As QTDocNo ");
                SQLDtlQT.AppendLine("From TblPOHdr A  ");
                SQLDtlQT.AppendLine("Inner Join TblPODtl B On A.DocNo=B.DocNo  ");
                SQLDtlQT.AppendLine("Inner Join TblPORequestHdr C On B.PORequestDocNo=C.DocNo ");
                SQLDtlQT.AppendLine("Inner Join TblPORequestDtl D On B.PORequestDocNo=D.DocNo And B.PORequestDNo=D.DNo ");
                SQLDtlQT.AppendLine("Where A.DocNo=@DocNo  ");
                SQLDtlQT.AppendLine(" ;  ");


                cmDtlQT.CommandText = SQLDtlQT.ToString();
                Sm.CmParam<String>(ref cmDtlQT, "@DocNo", DocNo);
                var drDtlQT = cmDtlQT.ExecuteReader();
                var cDtlQT = Sm.GetOrdinal(drDtlQT, new string[] 
                        {
                         //0-2
                         "DocNo", "QTDocNo",
                        });
                if (drDtlQT.HasRows)
                {
                    while (drDtlQT.Read())
                    {
                        ldtlQT.Add(new QT()
                        {
                            PODocNo = Sm.DrStr(drDtlQT, cDtlQT[0]),
                            QTDocNo = Sm.DrStr(drDtlQT, cDtlQT[1])
                        });
                    }
                }
                drDtlQT.Close();
            }
            myLists.Add(ldtlQT);
            #endregion

            #region DetailLOV
            // IMS
            var cmDtlLOV = new MySqlCommand();

            var SQLDtlLOV = new StringBuilder();
            using (var cnDtlLOV = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtlLOV.Open();
                cmDtlLOV.Connection = cnDtlLOV;

                SQLDtlLOV.AppendLine("Select distinct A.Docno, ");
                SQLDtlLOV.AppendLine("U.DocNo As LOVDocNo ");
                SQLDtlLOV.AppendLine("From TblPOHdr A  ");
                SQLDtlLOV.AppendLine("Inner Join TblPODtl B On A.DocNo=B.DocNo  ");
                SQLDtlLOV.AppendLine("Inner Join TblPORequestHdr C On B.PORequestDocNo=C.DocNo ");
                SQLDtlLOV.AppendLine("Inner Join TblPORequestDtl D On B.PORequestDocNo=D.DocNo And B.PORequestDNo=D.DNo ");
                SQLDtlLOV.AppendLine("Inner Join TblQtHdr E On D.QtDocNo=E.DocNo ");
                SQLDtlLOV.AppendLine("Inner Join TblMaterialRequestHdr N On D.MaterialRequestDocNo = N.DocNo ");
                SQLDtlLOV.AppendLine("Left Join TblItemVendorCollectionDtl S On S.MRDocNo = D.MaterialRequestDocNo And S.MRDNo = D.MaterialRequestDNo  ");
                SQLDtlLOV.AppendLine("LEFT Join ( ");
                SQLDtlLOV.AppendLine("    select A.DocNo, B.ItemVendorCollectionDOcnO, B.ItemVendorCollectionDno   ");
                SQLDtlLOV.AppendLine("    from TblVendorItCtGroupHdr A ");
                SQLDtlLOV.AppendLine("    Inner join TblVendorItCtGroupDtl B On A.DocNo = b.Docno ");
                SQLDtlLOV.AppendLine("    Where CancelInd = 'N' ");
                SQLDtlLOV.AppendLine(")T On T.ItemVendorCollectionDOcnO = S.DocNo And T.ItemVendorCollectionDNO = S.DNo ");
                SQLDtlLOV.AppendLine("Left JOin TblVendorItCtGroupHDr U On U.DocNo = T.DocNO And U.CancelInd = 'N' ");
                SQLDtlLOV.AppendLine("Where A.DocNo=@DocNo  ");
                SQLDtlLOV.AppendLine(" ;  ");


                cmDtlLOV.CommandText = SQLDtlLOV.ToString();
                Sm.CmParam<String>(ref cmDtlLOV, "@DocNo", DocNo);
                var drDtlLOV = cmDtlLOV.ExecuteReader();
                var cDtlLOV = Sm.GetOrdinal(drDtlLOV, new string[] 
                        {
                         //0-2
                         "DocNo", "LOVDocNo",
                        });
                if (drDtlLOV.HasRows)
                {
                    while (drDtlLOV.Read())
                    {
                        ldtlLOV.Add(new LOV()
                        {
                            PODocNo = Sm.DrStr(drDtlLOV, cDtlLOV[0]),
                            LOVDocNo = Sm.DrStr(drDtlLOV, cDtlLOV[1])
                        });
                    }
                }
                drDtlLOV.Close();
            }
            myLists.Add(ldtlLOV);
            #endregion

            #region Header PO IMS
            var SQLPOHDR = new StringBuilder();
            var cmPOHDR = new MySqlCommand();

            SQLPOHDR.AppendLine("Select LocalDocNo, Remark, CreateBy From tblpohdr Where DocNo =@DocNo ");
            using (var cnPOHDR = new MySqlConnection(Gv.ConnectionString))
            {
                cnPOHDR.Open();
                cmPOHDR.Connection = cnPOHDR;
                cmPOHDR.CommandText = SQLPOHDR.ToString();
                Sm.CmParam<String>(ref cmPOHDR, "@DocNo", DocNo);

                var drPOHDR = cmPOHDR.ExecuteReader();
                var cPOHDR = Sm.GetOrdinal(drPOHDR, new string[] 
                        {
                         //0
                         "Remark",
                         //1-5
                         "CreateBy",
                         "LocalDocNo",
                         
                        });
                if (drPOHDR.HasRows)
                {
                    while (drPOHDR.Read())
                    {
                        lPOHDRIMS.Add(new POHDRIMS()
                        {
                            Remark = Sm.DrStr(drPOHDR, cPOHDR[0]),
                            CreateBY = Sm.DrStr(drPOHDR, cPOHDR[1]),
                            LocalDocNo = Sm.DrStr(drPOHDR, cPOHDR[2]),
                        });
                    }
                }
                drPOHDR.Close();
            }
            myLists.Add(lPOHDRIMS);
            #endregion


            string RptName = Sm.GetParameter("FormPrintOutPO");
            if(RptName.Length>0)
            {
                if (mIsPOPrintOutInEnglish && ChkImportInd.Checked)
                    Sm.PrintReport(RptName + "Eng", myLists, TableName, false);
                else
                {
                    if ((!mIsOutstandingPOPrintEnabled || mIsOutstandingPOPrintEnabled) && TxtStatus.Text == "Approved")
                        Sm.PrintReport(RptName, myLists, TableName, false);
                    else
                        Sm.PrintReport("POOutstandingSIER", myLists, TableName, false);
                }
            }
            
        }

        private void SetLueVdCode(ref DXE.LookUpEdit Lue, string VdCode)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            if (VdCode.Length == 0)
            {
                SQL.AppendLine("Select Distinct C.VdCode As Col1, ");
                if (mIsVendorComboShowCategory)
                    SQL.AppendLine("Concat(D.VdName, ' [', IFNULL(G.VdCtName, ' '), ']') As Col2 ");
                else
                    SQL.AppendLine("D.VdName As Col2 ");
                SQL.AppendLine("From TblPORequestHdr A ");
                SQL.AppendLine("Inner Join TblPORequestDtl B On A.DocNo=B.DocNo And B.Status='A' And B.CancelInd='N' ");
                SQL.AppendLine("Inner Join TblQtHdr C On B.QtDocNo=C.DocNo ");
                if (mIsGroupPaymentTermActived)
                {
                    SQL.AppendLine("And C.PtCode Is Not Null ");
                    SQL.AppendLine("And C.PtCode In (");
                    SQL.AppendLine("    Select PtCode From TblGroupPaymentTerm ");
                    SQL.AppendLine("    Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ) ");
                }
                SQL.AppendLine("Left Join TblVendor D On C.VdCode=D.VdCode ");
                SQL.AppendLine("Inner Join TblMaterialRequestDtl E ");
                SQL.AppendLine("    On B.MaterialRequestDocNo=E.DocNo ");
                SQL.AppendLine("    And B.MaterialRequestDNo=E.DNo ");
                SQL.AppendLine("    And IfNull(E.ProcessInd, '')<>'F' ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select PORequestDocNo As DocNo, T1.PORequestDNo As DNo, Sum(T1.Qty) As Qty ");
                SQL.AppendLine("    From TblPODtl T1 ");
                SQL.AppendLine("    Inner Join TblPORequestDtl T2 On T1.PORequestDocNo=T2.DocNo And T1.PORequestDNo=T2.DNo ");
                SQL.AppendLine("    Inner Join TblMaterialRequestDtl T3 ");
                SQL.AppendLine("        On T2.MaterialRequestDocNo=T3.DocNo ");
                SQL.AppendLine("        And T2.MaterialRequestDNo=T3.DNo ");
                SQL.AppendLine("        And IfNull(T3.ProcessInd, '')<>'F' ");
                SQL.AppendLine("        And T3.CancelInd='N' ");
                SQL.AppendLine("    Where T1.CancelInd='N' ");
                SQL.AppendLine("    Group By T1.PORequestDocNo, T1.PORequestDNo ");
                SQL.AppendLine(") F On A.DocNo=F.DocNo And B.DNo=F.DNo ");
                SQL.AppendLine("Left Join TblVendorCategory G On D.VdCtCode = G.VdCtCode ");
                SQL.AppendLine("Where B.Qty-IfNull(F.Qty, 0)<>0 ");
                SQL.AppendLine("Order By D.VdName;");
            }
            else
            {
                SQL.AppendLine("Select VdCode As Col1, ");
                if(mIsVendorComboShowCategory)
                    SQL.AppendLine("Concat(A.VdName, ' [', B.VdCtName, ']') As Col2 ");
                else
                    SQL.AppendLine("VdName As Col2 ");
                SQL.AppendLine("From TblVendor A ");
                SQL.AppendLine("Left Join TblVendorCategory B On A.VdCtCode = B.VdCtCode ");
                SQL.AppendLine("Where VdCode='" + VdCode + "' ");
            }

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            Sm.SetLue2(
                ref Lue, ref cm,
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        internal string GetSelectedPORequest()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 7).Length != 0 &&
                        Sm.GetGrdStr(Grd1, Row, 8).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL +=
                            "'" +
                            Sm.GetGrdStr(Grd1, Row, 7) +
                            Sm.GetGrdStr(Grd1, Row, 8) +
                            "'";
                    }
                }
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }

        internal void ComputeTotal(int Row)
        {
            decimal Qty = 0m, UPrice = 0m, Discount = 0m,
                DiscountAmt = 0m, RoundValue = 0m;

            if (Sm.GetGrdStr(Grd1, Row, 19).Length != 0) Qty = Sm.GetGrdDec(Grd1, Row, 19);
            if (Sm.GetGrdStr(Grd1, Row, 26).Length != 0) UPrice = Sm.GetGrdDec(Grd1, Row, 26);
            if (Sm.GetGrdStr(Grd1, Row, 27).Length != 0) Discount = Sm.GetGrdDec(Grd1, Row, 27);
            if (Sm.GetGrdStr(Grd1, Row, 28).Length != 0) DiscountAmt = Sm.GetGrdDec(Grd1, Row, 28);
            if (Sm.GetGrdStr(Grd1, Row, 29).Length != 0) RoundValue = Sm.GetGrdDec(Grd1, Row, 29);

            Grd1.Cells[Row, 30].Value = (((100-Discount)/100)*(Qty * UPrice)) - DiscountAmt + RoundValue ;

            ComputeTaxAmt();
            ComputeAmt();
        }

        private bool IsPORequestEmpty(iGRequestEditEventArgs e)
        {
            if (Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length == 0)
            {
                e.DoDefault = false;
                return true;
            }
            return false;
        }

        private decimal ComputeTotalBeforeTax()
        {
           decimal TotalBeforeTax = 0m;
           for (int Row = 0; Row <= Grd1.Rows.Count - 1; Row++)
               if (!Sm.GetGrdBool(Grd1, Row, 1) && Sm.GetGrdStr(Grd1, Row, 30).Length!=0) TotalBeforeTax += Sm.GetGrdDec(Grd1, Row, 30);
           return TotalBeforeTax;
        }

        internal void ComputeTaxAmt()
        {
            decimal 
                TaxAmt=0m, 
                TotalBeforeTax = ComputeTotalBeforeTax();

            string TaxAmt1 = "0", TaxAmt2 = "0", TaxAmt3 = "0";

            if (Sm.GetLue(LueTaxCode1).Length != 0)
            {
                TaxAmt1 = Sm.GetValue("Select TaxRate From TblTax Where TaxCode='" + Sm.GetLue(LueTaxCode1) + "'");
                if (TaxAmt1.Length != 0) 
                {
                    TaxAmt += (TotalBeforeTax*(decimal.Parse(TaxAmt1)/100m));
                }  
            }

            if (Sm.GetLue(LueTaxCode2).Length != 0)
            {
                TaxAmt2 = Sm.GetValue("Select TaxRate From TblTax Where TaxCode='" + Sm.GetLue(LueTaxCode2) + "'");
                if (TaxAmt2.Length != 0)
                {
                    TaxAmt += (TotalBeforeTax * (decimal.Parse(TaxAmt2) / 100m));
                }  
            }

            if (Sm.GetLue(LueTaxCode3).Length != 0)
            {
                TaxAmt3 = Sm.GetValue("Select TaxRate From TblTax Where TaxCode='" + Sm.GetLue(LueTaxCode3) + "'");
                if (TaxAmt3.Length != 0)
                {
                    TaxAmt += (TotalBeforeTax * (decimal.Parse(TaxAmt3) / 100m));
                }  
            }

            TxtTaxAmt.Text = Sm.FormatNum(TaxAmt, 0);

            ComputeAmt();
        }

        internal void ComputeAmt()
        {
            decimal 
                TaxAmt = 0m, CustomsTaxAmt = 0m, DiscountAmt = 0m,
                TotalBeforeTax = ComputeTotalBeforeTax();

            if (TxtTaxAmt.Text.Length != 0) TaxAmt = decimal.Parse(TxtTaxAmt.Text);
            if (TxtCustomsTaxAmt.Text.Length != 0) CustomsTaxAmt = decimal.Parse(TxtCustomsTaxAmt.Text);
            if (TxtDiscountAmt.Text.Length != 0) DiscountAmt = decimal.Parse(TxtDiscountAmt.Text);

            TxtAmt.Text = Sm.FormatNum(TotalBeforeTax+TaxAmt+CustomsTaxAmt-DiscountAmt, 0);
        }

        private void SetLueVdPersonCode(ref LookUpEdit Lue, string VdCode)
        {
            Sm.SetLue1(
                ref Lue,
                "Select ContactPersonName As Col1 From TblVendorContactPerson Where VdCode= '" + VdCode + "' Order By ContactPersonName",
                "Contact Person Name");
        }

        private void SetLueContract(ref LookUpEdit Lue, string ContractCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select OptCode As Col1, OptDesc As Col2 ");
            SQL.AppendLine("From TblOption Where OptCat = 'POContract' ");
            if (ContractCode.Length > 0)
                SQL.AppendLine("Where OptCode = @ContractCode ");
            SQL.AppendLine("Order By OptDesc;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<string>(ref cm, "@ContractCode", ContractCode);

            Sm.SetLue2(
                ref Lue, ref cm,
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private string GenerateDocNo(string IsProcFormat, string DocDt, string DocType, string Tbl, ref LookUpEdit LueTax, string SubCategory)
        {
            string
                Yr = DocDt.Substring(2, 2),
                Mth = DocDt.Substring(4, 2),
                DocTitle = Sm.GetValue("Select ParValue From TblParameter Where ParCode='DocTitle'"),
                DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='" + DocType + "'"),
                IsPOSplit = Sm.GetParameter("IsPOSplitBasedOnTax");
            int LengthOfDocNoWithTitle = 4 + DocTitle.Length + DocAbbr.Length + 4 + 4; //4=nomor urut(0001), 4=bln+tahun(1115), 4 = jmlh "/"
            int LengthOfDocNoWithoutTitle = 4 + DocAbbr.Length + 4 + 3;
            bool IsDocNoFormatUseFullYear = Sm.GetParameter("IsDocNoFormatUseFullYear") == "Y";

            var SQL = new StringBuilder();

            if (IsDocNoFormatUseFullYear)
            {
                Yr = Sm.Left(DocDt, 4);

                SQL.Append("Select Concat( ");
                SQL.Append("IfNull(( ");
                SQL.Append("   Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) From ( ");
                SQL.Append("       Select Convert(left(right(DocNo, '" + LengthOfDocNoWithTitle + "'), 4), Decimal) As DocNo From " + Tbl);
                SQL.Append("       Where Left(DocDt, 4)='" + Yr + "' ");
                SQL.Append("       And DocNo Like '%" + DocTitle + "%' ");
                SQL.Append("       Order By left(right(DocNo, '" + LengthOfDocNoWithTitle + "'), 4) Desc Limit 1 ");
                SQL.Append("       ) As Temp ");
                SQL.Append("   ), '0001') ");
                SQL.Append(", '/', '" + ((DocTitle.Length == 0) ? "XXX" : DocTitle));
                SQL.Append("', '/', '" + DocAbbr + "', '/', '" + Mth + "','/', '" + Yr + "'");
                SQL.Append(") As DocNo");
            }
            else
            {
                if (IsPOSplit == "Y" && Sm.GetLue(LueTax).Length == 0)
                {
                    if (IsProcFormat == "1")
                    {
                        SQL.Append("Select Concat('" + SubCategory + "', '/', ");
                        SQL.Append("IfNull(( ");
                        SQL.Append("   Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) From ( ");
                        SQL.Append("        Select Convert(left(right(DocNo, '" + LengthOfDocNoWithoutTitle + "'), 4), Decimal) As DocNo From " + Tbl);
                        SQL.Append("        Where Right(DocNo, 5)=Concat('" + Mth + "','/', '" + Yr + "') ");
                        SQL.Append("        And DocNo Not Like '%" + DocTitle + "%' ");
                        SQL.Append("        Order By left(right(DocNo, '" + LengthOfDocNoWithoutTitle + "'), 4) Desc Limit 1 ");
                        SQL.Append("       ) As Temp ");
                        SQL.Append("   ), '0001') ");
                        SQL.Append(", '/', '" + DocAbbr + "', '/', '" + Mth + "','/', '" + Yr + "' ");
                        SQL.Append(") As DocNo");
                    }
                    else
                    {
                        SQL.Append("Select Concat( ");
                        SQL.Append("IfNull(( ");
                        SQL.Append("   Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) From ( ");
                        SQL.Append("    Select Convert(left(right(DocNo, '" + LengthOfDocNoWithoutTitle + "'), 4), Decimal) As DocNo  From " + Tbl);
                        SQL.Append("    Where Right(DocNo, 5)=Concat('" + Mth + "','/', '" + Yr + "') ");
                        SQL.Append("    And DocNo Not Like '%" + DocTitle + "%' ");
                        SQL.Append("    Order By left(right(DocNo, '" + LengthOfDocNoWithoutTitle + "'), 4) Desc Limit 1 ");
                        SQL.Append("       ) As Temp ");
                        SQL.Append("   ), '0001') ");
                        SQL.Append(", '/', '" + DocAbbr + "', '/', '" + Mth + "','/', '" + Yr + "' ");
                        SQL.Append(") As DocNo");
                    }
                }
                else
                {
                    if (IsProcFormat == "1")
                    {
                        SQL.Append("Select Concat('" + SubCategory + "', '/', ");
                        SQL.Append("IfNull(( ");
                        SQL.Append("   Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) From ( ");
                        SQL.Append("         Select Convert(left(right(DocNo, '" + LengthOfDocNoWithTitle + "'), 4), Decimal) As DocNo From " + Tbl);
                        SQL.Append("        Where Right(DocNo, 5)=Concat('" + Mth + "','/', '" + Yr + "') ");
                        SQL.Append("        And DocNo Like '%" + DocTitle + "%' ");
                        SQL.Append("        Order By left(right(DocNo, '" + LengthOfDocNoWithTitle + "'), 4) Desc Limit 1 ");
                        SQL.Append("       ) As Temp ");
                        SQL.Append("   ), '0001') ");
                        SQL.Append(", '/', '" + ((DocTitle.Length == 0) ? "XXX" : DocTitle));
                        SQL.Append("', '/', '" + DocAbbr + "', '/', '" + Mth + "','/', '" + Yr + "'");
                        SQL.Append(") As DocNo");
                    }
                    else
                    {
                        SQL.Append("Select Concat( ");
                        SQL.Append("IfNull(( ");
                        SQL.Append("   Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) From ( ");
                        SQL.Append("       Select Convert(left(right(DocNo, '" + LengthOfDocNoWithTitle + "'), 4), Decimal) As DocNo From " + Tbl);
                        SQL.Append("       Where Right(DocNo, 5)=Concat('" + Mth + "','/', '" + Yr + "') ");
                        SQL.Append("       And DocNo Like '%" + DocTitle + "%' ");
                        SQL.Append("       Order By left(right(DocNo, '" + LengthOfDocNoWithTitle + "'), 4) Desc Limit 1 ");
                        SQL.Append("       ) As Temp ");
                        SQL.Append("   ), '0001') ");
                        SQL.Append(", '/', '" + ((DocTitle.Length == 0) ? "XXX" : DocTitle));
                        SQL.Append("', '/', '" + DocAbbr + "', '/', '" + Mth + "','/', '" + Yr + "'");
                        SQL.Append(") As DocNo");
                    }
                }
            }
            return Sm.GetValue(SQL.ToString());
        }

        private string GenerateDocNoIMS()
        {
            //0001/IMS/PO-UMUM/04/20 (Untuk Umum)
            //0001/IMS/PO-LOG/04/20 (Untuk Logistik)
            
            string DocDt = Sm.GetDte(DteDocDt);
            string
                Yr = DocDt.Substring(2, 2),
                Mth = DocDt.Substring(4, 2);
            var SQL = new StringBuilder();
            var Abbr = Sm.GetGrdBool(Grd1, 0, 43) ? "PO-LOG" : "PO-UMUM";
            bool IsDocNoFormatUseFullYear = Sm.GetParameter("IsDocNoFormatUseFullYear") == "Y";

            if (IsDocNoFormatUseFullYear)
            {
                Yr = Sm.Left(DocDt, 4);

                SQL.Append("Select Concat( ");
                SQL.Append("IfNull(( ");
                SQL.Append("   Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) From ( ");
                SQL.Append("       Select Convert(Left(DocNo, 4), Decimal) As DocNo From TblPOHdr ");
                SQL.Append("       Where Left(DocDt, 4)='" + Yr + "' ");
                SQL.Append("       And DocNo Like '%" + Abbr + "%' ");
                SQL.Append("       Order By Left(DocNo, 4) Desc Limit 1 ");
                SQL.Append("       ) As Temp ");
                SQL.Append("   ), '0001') ");
                SQL.Append(", '/IMS/', '" + Abbr + "', '/', '" + Mth + "','/', '" + Yr + "'");
                SQL.Append(") As DocNo");
            }
            else
            {
                SQL.Append("Select Concat( ");
                SQL.Append("IfNull(( ");
                SQL.Append("   Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) From ( ");
                SQL.Append("       Select Convert(Left(DocNo, 4), Decimal) As DocNo From TblPOHdr ");
                SQL.Append("       Where Right(DocNo, 5)=Concat('" + Mth + "','/', '" + Yr + "') ");
                SQL.Append("       And DocNo Like '%" + Abbr + "%' ");
                SQL.Append("       Order By Left(DocNo, 4) Desc Limit 1 ");
                SQL.Append("       ) As Temp ");
                SQL.Append("   ), '0001') ");
                SQL.Append(", '/IMS/', '" + Abbr + "', '/', '" + Mth + "','/', '" + Yr + "'");
                SQL.Append(") As DocNo");
            }
            return Sm.GetValue(SQL.ToString());
        }

        private bool IsPOStatusOutstanding()
        {
            if (!mIsOutstandingPOPrintEnabled)
            {
                return Sm.IsDataExist(
                    "Select 1 from TblPOHdr Where DocNo=@Param And Status='O';",
                    TxtDocNo.Text,
                    "PO's status still outstanding.");
            }
            else
                return false;
        }

        private bool IsPOStatusCancelled()
        {
            return Sm.IsDataExist(
                "Select 1 from TblPOHdr Where DocNo=@Param And Status='C';",
                TxtDocNo.Text,
                "PO is cancelled.");
        }

        private void SetGrdRowReadOnly(iGrid Grd)
        {
            for (int row = 0; row < Grd.Rows.Count; row++)
            {
                if(Sm.GetGrdStr(Grd, row, 3).Length > 0 && Sm.GetGrdStr(Grd, row, 3) == Sm.GetGrdStr(Grd, row, 8))
                {
                    Grd.Cells[row, 1].ReadOnly = iGBool.True;
                }
            }
        }

        private void ProcessUploadFile(string DocNo)
        {
            if (mIsPOAllowToUploadFile)
            {
                if (TxtFile.Text.Length == 0) return;

                var l = new List<Sm.UploadFileClass>();
                string RenameTo = string.Empty;
                bool isSuccessFile1 = true;
                FileInfo toUpload = null;
                #region init data FileName1
                if (TxtFile.Text.Length > 0)
                {
                    string FileName = TxtFile.Text;
                    toUpload = new FileInfo(string.Format(@"{0}", FileName));
                    string fExt = Path.GetExtension(toUpload.Name);
                    string fName = Path.GetFileNameWithoutExtension(toUpload.Name);
                    RenameTo = string.Concat(fName, "_", DocNo.Replace('/', '-'), fExt);
                    isSuccessFile1 = Sm.UploadFile(mIsPOAllowToUploadFile, FileName, mFormatFTPClient, RenameTo, mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, mUsernameForFTPClient, mPasswordForFTPClient, mIsUploadFileRenamed);
                }
                #endregion

                if (isSuccessFile1)
                {
                    l.Add(new Sm.UploadFileClass()
                    {
                        DocNo = DocNo,
                        FileName = mIsUploadFileRenamed ? RenameTo : toUpload != null ? toUpload.Name : string.Empty
                    });
                    Sm.UpdateUploadedFile(mIsPOAllowToUploadFile, ref l, "TblPOhdr");
                }

                l.Clear();
            }
        }

        private void DownloadFileKu(string TxtFile, string Type)
        {
            DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, TxtFile, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
            SFD1.FileName = TxtFile;
            SFD1.DefaultExt = "pdf";
            SFD1.AddExtension = true;

            if (downloadedData != null && downloadedData.Length != 0)
            {

                if (SFD1.ShowDialog() == DialogResult.OK)
                {
                    Application.DoEvents();

                    //Write the bytes to a file
                    FileStream newFile = new FileStream(SFD1.FileName, FileMode.Create);
                    newFile.Write(downloadedData, 0, downloadedData.Length);
                    newFile.Close();
                    MessageBox.Show(string.Concat(Type," Saved Successfully"));
                }
            }
            else
                MessageBox.Show("No File was Downloaded Yet!");
        }

        private void DownloadFile(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                Application.DoEvents();

                if (mFormatFTPClient == "1")
                {
                    #region type 1
                    FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                    //this.Text = string.Concat(this.Text, "         ", "Retrieving Information...");
                    Application.DoEvents();

                    //Get the file size first (for progress bar)
                    request.Method = WebRequestMethods.Ftp.GetFileSize;
                    request.Credentials = new NetworkCredential(username, password);
                    request.UsePassive = true;
                    request.UseBinary = true;
                    request.KeepAlive = true;

                    int dataLength = (int)request.GetResponse().ContentLength;

                    //this.Text = string.Concat(this.Text, "         ", "Downloading File...");
                    Application.DoEvents();

                    //Now get the actual data
                    request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                    request.Method = WebRequestMethods.Ftp.DownloadFile;
                    request.Credentials = new NetworkCredential(username, password);
                    request.UsePassive = true;
                    request.UseBinary = true;
                    request.KeepAlive = false;

                    //Streams
                    FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                    Stream reader = response.GetResponseStream();

                    //Download to memory
                    MemoryStream memStream = new MemoryStream();
                    byte[] buffer = new byte[1024];

                    while (true)
                    {
                        Application.DoEvents();

                        int bytesRead = reader.Read(buffer, 0, buffer.Length);

                        if (bytesRead == 0)
                        {
                            Application.DoEvents();
                            break;
                        }
                        else
                        {
                            memStream.Write(buffer, 0, bytesRead);
                        }
                    }

                    downloadedData = memStream.ToArray();

                    reader.Close();
                    memStream.Close();
                    response.Close();
                    #endregion
                }
                else
                {
                    #region  type 2
                    FtpWebRequest request = FtpWebRequest.Create("ftp://" + FTPAddress + "/" + filename) as FtpWebRequest;
                    //this.Text = string.Concat(this.Text, "         ", "Retrieving Information...");
                    Application.DoEvents();

                    //Get the file size first (for progress bar)
                    request.Method = WebRequestMethods.Ftp.GetFileSize;
                    request.Credentials = new NetworkCredential(username, password);
                    request.UsePassive = true;
                    request.UseBinary = true;
                    request.KeepAlive = true;

                    int dataLength = (int)request.GetResponse().ContentLength;

                    //this.Text = string.Concat(this.Text, "         ", "Downloading File...");
                    Application.DoEvents();

                    //Now get the actual data
                    request = FtpWebRequest.Create("ftp://" + FTPAddress + "/" + filename) as FtpWebRequest;
                    request.Method = WebRequestMethods.Ftp.DownloadFile;
                    request.Credentials = new NetworkCredential(username, password);
                    request.UsePassive = true;
                    request.UseBinary = true;
                    request.KeepAlive = false;

                    //Streams
                    FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                    Stream reader = response.GetResponseStream();

                    //Download to memory
                    MemoryStream memStream = new MemoryStream();
                    byte[] buffer = new byte[1024];

                    while (true)
                    {
                        Application.DoEvents();

                        int bytesRead = reader.Read(buffer, 0, buffer.Length);

                        if (bytesRead == 0)
                        {
                            Application.DoEvents();
                            break;
                        }
                        else
                        {
                            memStream.Write(buffer, 0, bytesRead);
                        }
                    }

                    downloadedData = memStream.ToArray();

                    reader.Close();
                    memStream.Close();
                    response.Close();
                    #endregion
                }
            }
            catch (Exception)
            {
                MessageBox.Show("There was an error connecting to the FTP Server.");
            }
        }

        private void DownloadFile2(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                this.Text = "Connecting...";
                Application.DoEvents();

                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                this.Text = "Retrieving Information...";
                Application.DoEvents();

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                this.Text = "Downloading File...";
                Application.DoEvents();

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Set up progress bar
                PbUpload.Value = 0;
                PbUpload.Maximum = dataLength;

                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        PbUpload.Value = PbUpload.Maximum;

                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);

                        if (PbUpload.Value + bytesRead <= PbUpload.Maximum)
                        {
                            PbUpload.Value += bytesRead;

                            PbUpload.Refresh();
                            Application.DoEvents();
                        }
                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                MessageBox.Show("There was an error connecting to the FTP Server.");
            }
        }

        private void DownloadFileRevision(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                this.Text = "Connecting...";
                Application.DoEvents();

                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                this.Text = "Retrieving Information...";
                Application.DoEvents();

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                this.Text = "Downloading File...";
                Application.DoEvents();

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Set up progress bar
                PbUpload.Value = 0;
                PbUpload.Maximum = dataLength;

                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);
                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                MessageBox.Show("There was an error connecting to the FTP Server.");
            }
        }

        private void UploadFile(string DocNo)
        {
            if (IsUploadFileNotValid(TxtFile.Text)) return;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile.Text));
            long mFileSize = toUpload.Length; 
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", TxtFile.Text));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);

                PbUpload.Invoke(
                    (MethodInvoker)delegate { PbUpload.Maximum = (int)file.Length; });

                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);
                    PbUpload.Invoke(
                        (MethodInvoker)delegate
                        {
                            PbUpload.Value = (int)file.Position;
                        });
                }
            }
            while (bytesRead != 0);

            Sm.StdMsg(mMsgType.Info, "File uploaded successfully");

            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdatePOFile(DocNo, toUpload.Name));
            Sm.ExecCommands(cml);
        }

        private void UploadFile(string DocNo, int Row, string Type, string FileName)
        {
            if (IsUploadFileNotValid(FileName)) return;

            FtpWebRequest request;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", FileName));
            long mFileSize = toUpload.Length;
            if (mFormatFTPClient == "1")
            {
                request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            }
            else
            {
                request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}", mHostAddrForFTPClient, mPortForFTPClient, toUpload.Name));
            }

            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);
            request.KeepAlive = false;
            request.UseBinary = true;


            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", FileName));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);


                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);

                }
            }
            while (bytesRead != 0);



            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdatePOFile(DocNo, Row, Type, toUpload.Name));
            Sm.ExecCommands(cml);

        }

        private bool IsUploadFileNotValid(string FileName)
        {

            FileInfo f = new FileInfo(FileName);

            if (Sm.IsFTPClientDataNotValid(true, FileName, mHostAddrForFTPClient, mSharedFolderForFTPClient, mUsernameForFTPClient, mPortForFTPClient) ||
                Sm.IsFileSizeNotValid(true, FileName, false, 0, ref f, mFileSizeMaxUploadFTPClient) ||
                mPOUploadFileFormat == "2" ? Sm.IsFileNameAlreadyExisted(true, FileName, "TblPOFile", "FileName") : Sm.IsFileNameAlreadyExisted(true, FileName, "TblPOHdr", "FileName"))
            {
                return true;
            }

            return false;
        }

        private bool IsFTPClientDataNotValid(string FileName)
        {

            if (mIsPOAllowToUploadFile && FileName.Length > 0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }

            if (mIsPOAllowToUploadFile && FileName.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }

            if (mIsPOAllowToUploadFile && FileName.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }

            if (mIsPOAllowToUploadFile && FileName.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }
            return false;
        }

        private bool IsFileSizeNotvalid(string FileName)
        {
            if (mIsPOAllowToUploadFile && FileName.Length > 0)
            {
                FileInfo f = new FileInfo(FileName);
                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }

                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File too large to Upload.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted(string FileName)
        {
            if (mIsPOAllowToUploadFile && FileName.Length > 0)
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", FileName));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select DocNo From TblPOHdr ");
                SQL.AppendLine("Where FileName=@FileName ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }


        #endregion

        #endregion

        #region Event

        #region Grid Event

        #region Grid 3
        private void Grd3_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd3, e.RowIndex, 3).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmPORevision(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd3, e.RowIndex, 3);
                f.ShowDialog();
            }

            if (e.ColIndex == 11 && Sm.GetGrdStr(Grd3, e.RowIndex, 10).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmQt(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd3, e.RowIndex, 10);
                f.ShowDialog();
            }

            if (e.ColIndex == 6 && !Sm.IsGrdValueEmpty(Grd3, e.RowIndex, 5, false, "File Name"))
            {
                DownloadFileRevision(mHostAddrForFTPClient, mPortForFTPClient, Sm.GetGrdStr(Grd3, e.RowIndex, 5), mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
                SFD.FileName = Sm.GetGrdStr(Grd3, e.RowIndex, 5);
                SFD.DefaultExt = "";
                SFD.AddExtension = true;

                if (!Sm.IsTxtEmpty(TxtFile, "File", false) && downloadedData != null && downloadedData.Length != 0)
                {

                    if (SFD.ShowDialog() == DialogResult.OK)
                    {
                        Application.DoEvents();

                        //Write the bytes to a file
                        FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                        newFile.Write(downloadedData, 0, downloadedData.Length);
                        newFile.Close();
                        MessageBox.Show("Saved Successfully");
                    }
                }
                else
                    MessageBox.Show("No File was Downloaded Yet!");
            }
        }

        private void Grd3_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd3, e.RowIndex, 3).Length != 0)
            {
                var f = new FrmPORevision(mMenuCode);
                f.Tag = "X";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd3, e.RowIndex, 3);
                f.ShowDialog();
            }

            if (e.ColIndex == 11 && Sm.GetGrdStr(Grd3, e.RowIndex, 10).Length != 0)
            {
                var f = new FrmQt(mMenuCode);
                f.Tag = "X";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd3, e.RowIndex, 10);
                f.ShowDialog();
            }

            if (e.ColIndex == 6 && !Sm.IsGrdValueEmpty(Grd3, e.RowIndex, 5, false, "No File was Downloaded Yet!"))
            {
                string FileName = Sm.GetGrdStr(Grd3, e.RowIndex, 5);
                DownloadFileRevision(mHostAddrForFTPClient, mPortForFTPClient, FileName, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
                SFD.FileName = FileName;
                SFD.DefaultExt = "";
                SFD.AddExtension = true;

                if (!Sm.IsGrdValueEmpty(Grd3, e.RowIndex, 5, false, "File Name") && downloadedData != null && downloadedData.Length != 0)
                {

                    if (SFD.ShowDialog() == DialogResult.OK)
                    {
                        Application.DoEvents();

                        //Write the bytes to a file
                        FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                        newFile.Write(downloadedData, 0, downloadedData.Length);
                        newFile.Close();
                        MessageBox.Show("Saved Successfully");
                    }
                }
                else
                    MessageBox.Show("No File was Downloaded Yet!");
            }
        }

        private void Grd3RequestCellToolTipText(object sender, iGRequestCellToolTipTextEventArgs e)
        {
            if (e.ColIndex == 6)
            {
                e.Text = "Download";
            }
        }
        #endregion

        #region Grid 4
        private void Grd4_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd4, e.RowIndex, 3).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmPOQtyCancel(mMenuCode);
                f.Tag = "X";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd4, e.RowIndex, 3);
                f.ShowDialog();
            }
        }

        private void Grd4_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd4, e.RowIndex, 3).Length != 0)
            {
                var f = new FrmPOQtyCancel(mMenuCode);
                f.Tag = "X";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd4, e.RowIndex, 3);
                f.ShowDialog();
            }
        }
        #endregion

        #region Grid 6

        private void Grd6_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3)
            {
                if (Sm.GetGrdStr(Grd6, e.RowIndex, 2).Length > 0)
                {
                    DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, Sm.GetGrdStr(Grd6, e.RowIndex, 2), mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
                    SFD.FileName = Sm.GetGrdStr(Grd6, e.RowIndex, 1);
                    SFD.DefaultExt = "pdf";
                    SFD.AddExtension = true;

                    if (!Sm.IsGrdValueEmpty(Grd6, e.RowIndex, 2, false, "File name is empty") && downloadedData != null && downloadedData.Length != 0)
                    {
                        if (SFD.ShowDialog() == DialogResult.OK)
                        {
                            Application.DoEvents();

                            //Write the bytes to a file
                            FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                            newFile.Write(downloadedData, 0, downloadedData.Length);
                            newFile.Close();
                            Sm.StdMsg(mMsgType.Info, "File Downloaded");
                        }
                    }
                    else
                        Sm.StdMsg(mMsgType.Warning, "No File Downloaded");
                }
            }

            if (BtnSave.Enabled)
            {

                if (e.ColIndex == 1)
                {
                    Sm.GrdRequestEdit(Grd6, e.RowIndex);
                    OD.InitialDirectory = "c:";
                    OD.Filter = "Image files (*.PNG;*.JPG;*.JPEG)|*.PNG;*.JPG;*.JPEG" +
                                "|PDF files (*.pdf)|*.pdf" +
                                "|Word files (*.doc;*docx)|*.doc;*docx" +
                                "|Excel files (*.xls;*xlsx;*.csv)|*.xls;*xlsx;*.csv" +
                                "|Text files (*.txt)|*.txt";
                    OD.FilterIndex = 2;
                    OD.ShowDialog();
                    Grd6.Cells[e.RowIndex, 2].Value = OD.FileName;
                }
            }
        }

        private void Grd6_RequestEdit(object sender, iGRequestEditEventArgs e)
        {

            if (Sm.IsGrdColSelected(new int[] { 0, 1, 2, 3, 4, 5, 6 }, e.ColIndex) && BtnSave.Enabled)
            {
                Sm.GrdRequestEdit(Grd6, e.RowIndex);
            }

            if (e.ColIndex == 3)
            {
                Sm.GrdRequestEdit(Grd6, e.RowIndex);
                if (Sm.GetGrdStr(Grd6, e.RowIndex, 1).Length > 0)
                {
                    DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, Sm.GetGrdStr(Grd6, e.RowIndex, 2), mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
                    SFD.FileName = Sm.GetGrdStr(Grd6, e.RowIndex, 2);
                    SFD.DefaultExt = "pdf";
                    SFD.AddExtension = true;

                    if (!Sm.IsGrdValueEmpty(Grd6, e.RowIndex, 2, false, "File name is empty") && downloadedData != null && downloadedData.Length != 0)
                    {
                        if (SFD.ShowDialog() == DialogResult.OK)
                        {
                            Application.DoEvents();

                            //Write the bytes to a file
                            FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                            newFile.Write(downloadedData, 0, downloadedData.Length);
                            newFile.Close();
                            Sm.StdMsg(mMsgType.Info, "File Downloaded");
                        }
                    }
                    else
                        Sm.StdMsg(mMsgType.Warning, "No File Downloaded");
                }
            }
        }

        private void Grd6_RequestCellToolTipText(object sender, iGRequestCellToolTipTextEventArgs e)
        {
            if (e.ColIndex == 1)
            {
                e.Text = "Upload";
            }
            else if (e.ColIndex == 3)
            {
                e.Text = "Download";
                e.GetType();
            }
        }

        private void Grd6_KeyDown(object sender, KeyEventArgs e)
        {
            if(InsertInd == "1")
            {
                Sm.GrdRemoveRow(Grd6, e, BtnSave);
                Sm.GrdEnter(Grd6, e);
                Sm.GrdTabInLastCell(Grd6, e, BtnFind, BtnSave);
            }
        }

        #endregion

        #region Grid 7

        private void Grd7_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 4)
            {
                if (Sm.GetGrdStr(Grd7, e.RowIndex, 3).Length > 0)
                {
                    DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, Sm.GetGrdStr(Grd7, e.RowIndex, 3), mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
                    SFD.FileName = Sm.GetGrdStr(Grd7, e.RowIndex, 3);
                    SFD.DefaultExt = "pdf";
                    SFD.AddExtension = true;

                    if (!Sm.IsGrdValueEmpty(Grd7, e.RowIndex, 3, false, "File name is empty") && downloadedData != null && downloadedData.Length != 0)
                    {
                        if (SFD.ShowDialog() == DialogResult.OK)
                        {
                            Application.DoEvents();

                            //Write the bytes to a file
                            FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                            newFile.Write(downloadedData, 0, downloadedData.Length);
                            newFile.Close();
                            Sm.StdMsg(mMsgType.Info, "File Downloaded");
                        }
                    }
                    else
                        Sm.StdMsg(mMsgType.Warning, "No File Downloaded");
                }
            }

            if (BtnSave.Enabled)
            {

                if (e.ColIndex == 1)
                {
                    Sm.GrdRequestEdit(Grd6, e.RowIndex);
                    OD.InitialDirectory = "c:";
                    OD.Filter = "Image files (*.PNG;*.JPG;*.JPEG)|*.PNG;*.JPG;*.JPEG" +
                                "|PDF files (*.pdf)|*.pdf" +
                                "|Word files (*.doc;*docx)|*.doc;*docx" +
                                "|Excel files (*.xls;*xlsx;*.csv)|*.xls;*xlsx;*.csv" +
                                "|Text files (*.txt)|*.txt";
                    OD.FilterIndex = 2;
                    OD.ShowDialog();
                    Grd7.Cells[e.RowIndex, 3].Value = OD.FileName;
                }
            }
        }

        private void Grd7_RequestEdit(object sender, iGRequestEditEventArgs e)
        {

            if (Sm.IsGrdColSelected(new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8 }, e.ColIndex) && BtnSave.Enabled)
            {
                Sm.GrdRequestEdit(Grd7, e.RowIndex);
            }

            if (e.ColIndex == 4)
            {
                Sm.GrdRequestEdit(Grd7, e.RowIndex);
                if (Sm.GetGrdStr(Grd7, e.RowIndex, 3).Length > 0)
                {
                    DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, Sm.GetGrdStr(Grd7, e.RowIndex, 3), mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
                    SFD.FileName = Sm.GetGrdStr(Grd7, e.RowIndex, 3);
                    SFD.DefaultExt = "pdf";
                    SFD.AddExtension = true;

                    if (!Sm.IsGrdValueEmpty(Grd7, e.RowIndex, 3, false, "File name is empty") && downloadedData != null && downloadedData.Length != 0)
                    {
                        if (SFD.ShowDialog() == DialogResult.OK)
                        {
                            Application.DoEvents();

                            //Write the bytes to a file
                            FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                            newFile.Write(downloadedData, 0, downloadedData.Length);
                            newFile.Close();
                            Sm.StdMsg(mMsgType.Info, "File Downloaded");
                        }
                    }
                    else
                        Sm.StdMsg(mMsgType.Warning, "No File Downloaded");
                }
            }
        }

        private void Grd7_RequestCellToolTipText(object sender, iGRequestCellToolTipTextEventArgs e)
        {
            if (e.ColIndex == 1)
            {
                e.Text = "Upload";
            }
            else if (e.ColIndex == 4)
            {
                e.Text = "Download";
                e.GetType();
            }
        }

        #endregion

        #endregion

        #region Misc Control Event

        private void LueSignBy_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSignBy, new Sm.RefreshLue2(SetLueOption), "POSignBy");
        }

        private void ChkFile_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile.Checked == false)
            {
                TxtFile.EditValue = string.Empty;
            }
        }


        private void ChkImportInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                string PoNote = Sm.GetParameter("PONotes");
                string PoNoteEng = Sm.GetParameter("PONotesEng");
                if (mIsShipToDefaultByCompany)
                {
                    if (ChkImportInd.Checked && InsertInd == "1")
                    {
                        MeeRemark.Text = "" + PoNoteEng + "";
                    }
                    else
                    {
                        if (InsertInd == "1") MeeRemark.Text = "" + PoNote + "";
                        else MeeRemark.Text = "";
                    }

                    if (mIsPODisplayNotificationRemarkChanges && InsertInd == "1") Sm.StdMsg(mMsgType.Info, "Your Remark has changed.");
                }
            }
        }

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue2(SetLueVdCode), "");
        }

        private void LueVdCode_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (Sm.GetLue(LueVdCode).Length == 0)
                {
                    LueVdContactPersonName.EditValue = null;
                    Sm.SetControlReadOnly(LueVdContactPersonName, true);
                }
                else
                {
                    ClearData2();
                    SetLueVdPersonCode(ref LueVdContactPersonName, Sm.GetLue(LueVdCode));
                    Sm.SetControlReadOnly(LueVdContactPersonName, false);
                }

                Sm.ClearGrd(Grd1, true);
                ComputeAmt();
            }
        }

        private void LueTaxCode1_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueTaxCode1, new Sm.RefreshLue1(Sl.SetLueTaxCode));
                ComputeTaxAmt();
            }
        }

        private void LueTaxCode2_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueTaxCode2, new Sm.RefreshLue1(Sl.SetLueTaxCode));
                ComputeTaxAmt();
            }
        }

        private void LueTaxCode3_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueTaxCode3, new Sm.RefreshLue1(Sl.SetLueTaxCode));
                ComputeTaxAmt();
            }
        }

        private void TxtCustomsTaxAmt_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.FormatNumTxt(TxtCustomsTaxAmt, 0);
                ComputeAmt();
            }
        }

        private void TxtDiscountAmt_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.FormatNumTxt(TxtDiscountAmt, 0);
                ComputeAmt();
            }
        }

        private void DteEstRecvDt_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DteEstRecvDt, ref fCell, ref fAccept);
        }

        private void DteEstRecvDt_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd1, ref fAccept, e);
        }

        private void LueVdContactPersonName_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueVdContactPersonName, new Sm.RefreshLue2(SetLueVdPersonCode), Sm.GetLue(LueVdCode));
            }
        }

        private void BtnVdContactPersonName_Click(object sender, EventArgs e)
        {
            if (!Sm.IsLueEmpty(LueVdCode, "Vendor"))
            {
                var f = new FrmVendor(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mMenuCode = "RSYYNNN";
                f.mVdCode = Sm.GetLue(LueVdCode);
                f.ShowDialog();
                SetLueVdPersonCode(ref LueVdContactPersonName, Sm.GetLue(LueVdCode));
            }
        }

        private void TxtLocalDocNo_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtLocalDocNo);
        }

        #endregion

        #endregion

        #region Class

        private class LocalDocument
        {
            public string DocNo { set; get; }
            public string LocalDocNo { set; get; }
            public string SeqNo { get; set; }
            public string DeptCode { get; set; }
            public string ItSCCode { get; set; }
            public string Mth { get; set; }
            public string Yr { get; set; }
            public string Revision { get; set; }
        }

        internal class EstRecvDt
        {
            public string PORequestDocNo { get; set; }
            public string PORequestDNo { get; set; }
            public decimal Qty { get; set; }
            public string Dt { get; set; }
        }

        private class Attachment2
        {
            public string DNo { get; set; }
            public string FileName { get; set; }
            public string CreateBy { get; set; }
            public string CreateDt { get; set; }
        }

        #region Report Class

        private class PO
        {
            public string CompanyLogo { set; get; }
            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyLongAddress { get; set; }
            public string CompanyPhone { get; set; }
            public string CompanyFax { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string VdName { get; set; }
            public string VdContactPerson { get; set; }
            public string ShipTo { get; set; }
            public string BillTo { get; set; }
            public string TaxCode1 { get; set; }
            public string TaxCode2 { get; set; }
            public string TaxCode3 { get; set; }
            public decimal TaxAmt { get; set; }
            public decimal CustomsTaxAmt { get; set; }
            public decimal DiscountAmt { get; set; }
            public decimal Amt { get; set; }
            public decimal DownPayment { get; set; }
            public string ARemark { get; set; }
            public string PrintBy { get; set; }
            public string TaxName1 { get; set; }
            public string TaxName2 { get; set; }
            public string TaxName3 { get; set; }
            public string CreateBy { get; set; }
            public decimal TaxRate1 { get; set; }
            public decimal TaxRate2 { get; set; }
            public decimal TaxRate3 { get; set; }
            public string Address { get; set; }
            public string Phone { get; set; }
            public string Fax { get; set; }
            public string Email { get; set; }
            public string ContactNumber { get; set; }
            public string SplitPO { get; set; }
            public string SiteName { get; set; }
            public string LocalDocNo { get; set; }
            public string CurCode { get; set; }
            public decimal PtDay { get; set; }
            public string QtDocNo { get; set; }
            public string QtDocDt { get; set; }
            public string Terbilang { get; set; }
            public string Terbilang2 { get; set; }
            public string PosName { get; set; }
            public string VdPosName { get; set; }
            public string MRDocDt { get; set; }
            public string BankAcNo { get; set; }
            public string BankName { get; set; }
            public string VdCtName { get; set; }
            public string DocDt2 { get; set; }
            public string ProjectName { get; set; }
            public string MRDocNo { get; set; }

            public string LOVDocNo { get; set; }
            public string PtName { get; set; }
            public string DTName { get; set; }
        }

        private class PODtl
        {
            public int nomor { get; set; }
            public string ItCode { get; set; }
            public string ItName { get; set; }
            public decimal Qty { get; set; }
            public string PurchaseUomCode { get; set; }
            public string PtName { get; set; }
            public string CurCode { get; set; }
            public decimal UPrice { get; set; }
            public decimal Discount { get; set; }
            public decimal DiscountAmt { get; set; }
            public decimal RoundingValue { get; set; }
            public string EstRecvDt { get; set; }
            public string EstRecvDt2 { get; set; }
            public string DRemark { get; set; }
            public decimal DTotal { get; set; }
            public string DtName { get; set; }
            public string ForeignName { get; set; }
            public string ItCodeInternal { get; set; }
            public string Specification { get; set; }
            public string ProjectName { get; set; }
        }

        private class PODtl2
        {
            public string Signature { get; set; }
            public string UserName { get; set; }
            public string PosName { get; set; }
            public string Space { get; set; }
            public string DNo { get; set; }
            public string Title { get; set; }
            public string LastUpDt { get; set; }
            public string CompanyName { get; set; }
        }

        private class PODtl3
        {
            public string LastUpDt { get; set; }
            public string DNo { get; set; }
        }

        private class PODtl4
        {
            public string Note { get; set; }
        }

        private class Employee
        {
            public string EmpCode { set; get; }
            public string EmpName { get; set; }
            public string Position { get; set; }
        }

        private class PODtl5
        {
            public string DocNo { get; set; }
            public string PtName { get; set; }
            public string EstRecvDt { get; set; }
        }

        private class Item
        {
            public string ItemName { get; set; }
        }

        private class SignIMS
        {
            public string EmpName { get; set; }
            public string PosName { get; set; }
        }

        private class MR
        {
            public string PODocNo { get; set; }
            public string MRDocNo { get; set; }
        }

        private class QT
        {
            public string PODocNo { get; set; }
            public string QTDocNo { get; set; }
        }

        private class LOV
        {
            public string PODocNo { get; set; }
            public string LOVDocNo { get; set; }
        }
        private class POHDRIMS
        {
            public string CreateBY { get; set; }
            public string Remark { get; set; }
            public string LocalDocNo { get; set; }
        }
        #endregion


        #endregion
    }
}
