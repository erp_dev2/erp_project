﻿
#region Update
/*
 * 25/07/2022 [HPH/SIER] New apps
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmAdvanceChargeFind : RunSystem.FrmBase2
    {
        #region Field
        private FrmAdvanceCharge mFrmParent;
        private string mSQL = string.Empty;
        //private Frm mFrmParent;

        #endregion

        #region Constructor

        public FrmAdvanceChargeFind(FrmAdvanceCharge FrmParent) //(FrmTemplate FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Standar Method
        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                BtnPrint.Visible = false;
                BtnExcel.Visible = false;
                SetSQL();
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }
    
        override protected void SetSQL()
        {
            var SQL = new StringBuilder();
            SQL.AppendLine("Select AdvanceChargeCode, AdvanceChargeName, ActInd, LocalName, PercentageInd, Value, AcType, AcNo, CreateBy, CreateDt, LastUpBy, LastUpDt, ");
            SQL.AppendLine("Case ");
            SQL.AppendLine("When AcType = 'C' Then 'Credit'");
            SQL.AppendLine("When AcType = 'D' Then 'Debit'");
            SQL.AppendLine("End Status ");
            SQL.AppendLine("from tbladvancecharge ");
            mSQL = SQL.ToString();
        }
        private void SetGrd()
        {
            Grd1.Cols.Count = 15;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[]
                    {
                        //0
                        "No.",

                        //1-5
                        "Code",
                        "Name",
                        "Active",
                        "Local Name",
                        "Percentage",
                        
                        //6-10
                        "Value",
                        "Type",
                        "COA",                       
                        "Created"+Environment.NewLine+"By",
                        "Created"+Environment.NewLine+"Date", 

                        //11- 14
                        "Created"+Environment.NewLine+"Time",
                        "Last"+Environment.NewLine+"Updated By",
                        "Last"+Environment.NewLine+"Updated Date",
                        "Last"+Environment.NewLine+"Updated Time"
                        
                      },
                    new int[]
                    {
                        //0
                        50,

                        //1-5
                        50, 200, 50, 200, 100, 
                        
                        //6-10
                        120, 80, 200, 100, 120,
                        
                        //11-14
                        150, 150, 130, 200,
                       
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 6 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 10, 13 });
            Sm.GrdFormatTime(Grd1, new int[] { 11, 14 });
            Sm.GrdColCheck(Grd1, new int[] { 3, 5 });
            Sm.GrdColInvisible(Grd1, new int[] { 9, 10, 11, 12, 13, 14 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }


        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 9, 10, 11, 12, 13, 14 }, !ChkHideInfoInGrd.Checked);
        }


        override protected void ShowData()
        {


            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;
                var cm = new MySqlCommand();
                Sm.FilterStr(ref Filter, ref cm, TxtName.Text, new string[] { "AdvanceChargeCode", "AdvanceChargeName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, 
                        mSQL + Filter + "Order By AdvanceChargeCode ",
                        new string[]
                        {
                            //0
                            "AdvanceChargeCode", 
                                
                            //1-5
                            "AdvanceChargeName", "ActInd", "LocalName", "PercentageInd", "Value",

                            //6-10
                            "Status", "AcNo",  "CreateBy", "CreateDt", "LastUpBy", 
                            
                           
                            
                            //11
                            "LastUpDt"
                          
                             },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 10, 9);                          
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 10);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 13, 11);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 14, 11);
                          
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
           
        }

        override protected void ChooseData()
        {
           
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));                        
                this.Hide();

        }
        #endregion

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Event

        #region Misc Control Method
        private void TxtName_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Name");
        }
   
        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }
        #endregion

        #endregion
    }
}
