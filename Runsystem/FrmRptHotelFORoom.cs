﻿#region Update
/*
    18/09/2017 [TKG] reporting Hotel's FO Room.
    25/09/2017 [TKG] Revisi tampilan kolom
    29/09/2017 [TKG] Total room harus selalu muncul.
    03/10/2017 [TKG] perubahan rumus
    11/10/2017 [TKG] perhitungan menggunakan data tgl 1 hari sebelumnya.
    18/10/2017 [ARI] tambah kolom month
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptHotelFORoom : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmRptHotelFORoom(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                DteDt.DateTime = Sm.ConvertDate(Sm.ServerCurrentDateTime());
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        private void SetGrd()
        {
            Grd1.Cols.Count = 16;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Date",
                        "Month",
                        "Description",
                        "Today",
                        "Budget"+Environment.NewLine+"Today",

                        //6-10
                        "Variance"+Environment.NewLine+"Today",
                        "%"+Environment.NewLine+"Today",
                        "Mtd",
                        "Budget"+Environment.NewLine+"MTD",
                        "Variance"+Environment.NewLine+"MTD",                        
                        
                        //11-15
                        "%"+Environment.NewLine+"MTD",
                        "YTD",
                        "Budget"+Environment.NewLine+"YTD",
                        "Variance"+Environment.NewLine+"YTD",
                        "%"+Environment.NewLine+"YTD"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        80, 100, 200, 120, 120, 
                        
                        //6-10
                        120, 100, 120, 120, 120,  
                        
                        //11-15
                        100, 120, 120, 120, 100  
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 1 });
            Sm.GrdFormatDec(Grd1, new int[] { 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 5, 6, 7, 9, 10, 11, 13, 14, 15 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 5, 6, 7, 9, 10, 11, 13, 14, 15 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (Sm.IsDteEmpty(DteDt, "Date")) return;

            var mlResult1 = new List<Result1>();
            List<Result2> mlResult2 = null;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                Process1(ref mlResult1);
                if (mlResult1.Count > 0)
                {
                    Process2(ref mlResult1, ref mlResult2);
                    Process3(ref mlResult2);
                }
                else
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                mlResult1.Clear();
                if (mlResult1.Count > 0)
                {
                    if (mlResult2.Count > 0) mlResult2.Clear();
                }
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Additional Method

        private void Process1(ref List<Result1> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Tbl1.*, ");

            SQL.AppendLine("Case When total_roomCurrent<>0 Then total_roomCurrent ");
            SQL.AppendLine("Else ");
            SQL.AppendLine("    Case When total_roomBefore<>0 Then total_roomBefore ");
            SQL.AppendLine("    Else total_roomAfter End ");
            SQL.AppendLine("End As total_room, ");

            SQL.AppendLine("Case When total_roomCurrent<>0 Then total_roomCurrent ");
            SQL.AppendLine("Else ");
            SQL.AppendLine("    Case When total_roomBefore<>0 Then total_roomBefore ");
            SQL.AppendLine("    Else total_roomAfter End ");
            SQL.AppendLine("End As room_available_1, ");

            SQL.AppendLine("Case When total_roomCurrent<>0 Then total_roomCurrent ");
            SQL.AppendLine("Else ");
            SQL.AppendLine("    Case When total_roomBefore<>0 Then total_roomBefore ");
            SQL.AppendLine("    Else total_roomAfter End ");
            SQL.AppendLine("End As room_available_2, ");

            SQL.AppendLine("Case When total_roomCurrent<>0 Then total_roomCurrent ");
            SQL.AppendLine("Else ");
            SQL.AppendLine("    Case When total_roomBefore<>0 Then total_roomBefore ");
            SQL.AppendLine("    Else total_roomAfter End ");
            SQL.AppendLine("End As room_available_3, ");

            SQL.AppendLine("Case When total_roomCurrent<>0 Then total_roomCurrent ");
            SQL.AppendLine("Else ");
            SQL.AppendLine("    Case When total_roomBefore<>0 Then total_roomBefore ");
            SQL.AppendLine("    Else total_roomAfter End ");
            SQL.AppendLine("End-IfNull(room_oo_1, 0.00) As room_saleable_1, ");

            SQL.AppendLine("Case When total_roomCurrent<>0 Then total_roomCurrent ");
            SQL.AppendLine("Else ");
            SQL.AppendLine("    Case When total_roomBefore<>0 Then total_roomBefore ");
            SQL.AppendLine("    Else total_roomAfter End ");
            SQL.AppendLine("End-IfNull(room_oo_2, 0.00) As room_saleable_2, ");

            SQL.AppendLine("Case When total_roomCurrent<>0 Then total_roomCurrent ");
            SQL.AppendLine("Else ");
            SQL.AppendLine("    Case When total_roomBefore<>0 Then total_roomBefore ");
            SQL.AppendLine("    Else total_roomAfter End ");
            SQL.AppendLine("End-IfNull(room_oo_3, 0.00) As room_saleable_3, ");

            SQL.AppendLine("( ");
            SQL.AppendLine("    Select Sum(IfNull(Amount, 0.00)*IfNull(Currency_Rate, 0.00)) ");
            SQL.AppendLine("    From TblTransactions ");
            SQL.AppendLine("    Where Date=@Dt1 ");
            SQL.AppendLine(") As trr_1, ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select Sum(IfNull(Amount, 0.00)*IfNull(Currency_Rate, 0.00)) ");
            SQL.AppendLine("    From TblTransactions ");
            SQL.AppendLine("    Where Date Between @Dt2 And @Dt1 ");
            SQL.AppendLine(") As trr_2, ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select Sum(IfNull(Amount, 0.00)*IfNull(Currency_Rate, 0.00)) ");
            SQL.AppendLine("    From TblTransactions ");
            SQL.AppendLine("    Where Date Between @Dt3 And @Dt1 ");
            SQL.AppendLine(") As trr_3 ");
            SQL.AppendLine("From (");

            SQL.AppendLine("    Select Sum(total_room) As total_roomCurrent, ");

            for (int i = 1; i <= 3; i++)
            {
                //SQL.AppendLine("    Sum(room_available_" + i + ") As room_available_" + i + ", ");
                //SQL.AppendLine("    Sum(IfNull(room_saleable_" + i + ", 0.00)) As room_saleable_" + i + ", ");
                SQL.AppendLine("    Sum(room_occupied_" + i + ") As room_occupied_" + i + ", ");
                SQL.AppendLine("    Sum(room_hu_" + i + ") As room_hu_" + i + ", ");
                SQL.AppendLine("    Sum(room_complimentary_" + i + ") As room_complimentary_" + i + ", ");
                SQL.AppendLine("    Sum(guest_" + i + ") As guest_" + i + ", ");
                SQL.AppendLine("    Sum(room_oo_" + i + ") As room_oo_" + i + " ");
                if (i != 3) SQL.AppendLine(", ");
            }

            SQL.AppendLine("    From ( ");

            //1
            SQL.AppendLine("        Select total_room, ");

            //SQL.AppendLine("        total_room As room_available_1, ");
            //SQL.AppendLine("        IfNull(total_room, 0.00)-IfNull(room_oo, 0.00) As room_saleable_1, ");
            SQL.AppendLine("        IfNull(room_occupied, 0.00) As room_occupied_1, ");
            SQL.AppendLine("        IfNull(room_hu, 0.00) As room_hu_1, ");
            SQL.AppendLine("        IfNull(room_complimentary, 0.00) As room_complimentary_1, ");
            SQL.AppendLine("        IfNull(guest, 0.00) As guest_1, ");
            SQL.AppendLine("        IfNull(room_oo, 0.00) As room_oo_1, ");

            GetProcess1SubQuery(ref SQL, 2, true);
            GetProcess1SubQuery(ref SQL, 3, false);

            SQL.AppendLine("        From TblReports Where Date=@Dt1 ");

            //2
            SQL.AppendLine("        Union All ");

            SQL.AppendLine("        Select 0.00 As total_room, ");

            GetProcess1SubQuery(ref SQL, 1, true);

            //SQL.AppendLine("        0.00 As room_available_2, ");
            //SQL.AppendLine("        Sum(IfNull(total_room, 0.00)-IfNull(room_oo, 0.00)) As room_saleable_2, ");
            SQL.AppendLine("        Sum(IfNull(room_occupied, 0.00)) As room_occupied_2, ");
            SQL.AppendLine("        Sum(IfNull(room_hu, 0.00)) As room_hu_2, ");
            SQL.AppendLine("        Sum(IfNull(room_complimentary, 0.00)) As room_complimentary_2, ");
            SQL.AppendLine("        Sum(IfNull(guest, 0.00)) As guest_2, ");
            SQL.AppendLine("        Sum(IfNull(room_oo, 0.00)) As room_00_2, ");

            GetProcess1SubQuery(ref SQL, 3, false);

            SQL.AppendLine("        From TblReports Where Date Between @Dt2 And @Dt1 ");

            //3
            SQL.AppendLine("        Union All ");

            SQL.AppendLine("        Select 0.00 As total_room, ");

            GetProcess1SubQuery(ref SQL, 1, true);
            GetProcess1SubQuery(ref SQL, 2, true);

            //SQL.AppendLine("        0.00 As room_available_3, ");
            //SQL.AppendLine("        Sum(IfNull(total_room, 0.00)-IfNull(room_oo, 0.00)) As room_saleable_3, ");
            SQL.AppendLine("        Sum(IfNull(room_occupied, 0.00)) As room_occupied_3, ");
            SQL.AppendLine("        Sum(IfNull(room_hu, 0.00)) As room_hu_3, ");
            SQL.AppendLine("        Sum(IfNull(room_complimentary, 0.00)) As room_complimentary_3, ");
            SQL.AppendLine("        Sum(IfNull(guest, 0.00)) As guest_3, ");
            SQL.AppendLine("        Sum(IfNull(room_oo, 0.00)) As room_00_3 ");

            SQL.AppendLine("        From TblReports ");
            SQL.AppendLine("        Where Date Between @Dt3 And @Dt1 ");

            SQL.AppendLine("    ) T ");
            SQL.AppendLine(") Tbl1 ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select IfNull(total_room, 0) As total_roomBefore From TblReports ");
            SQL.AppendLine("    Where Date<=@Dt1 And IfNull(total_room, 0)<>0 Limit 1 ");
            SQL.AppendLine(") Tbl2 On 1=1  ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select IfNull(total_room, 0) As total_roomAfter From TblReports ");
            SQL.AppendLine("    Where Date>@Dt1 And IfNull(total_room, 0)<>0 Limit 1 ");
            SQL.AppendLine(") Tbl3 On 1=1  ");

            var Dt = Sm.GetDte(DteDt);
            var Dt1 = Sm.FormatDate(Sm.ConvertDate(Sm.GetDte(DteDt)).AddDays(-1));
            var Dt2 = Sm.FormatDate(Sm.ConvertDate(String.Concat(Sm.Left(Dt, 6), "01")).AddDays(-1));
            var Dt3 = Sm.FormatDate(Sm.ConvertDate(String.Concat(Sm.Left(Dt, 4), "0101")).AddDays(-1));

            Sm.CmParamDt(ref cm, "@Dt1", Dt1);
            Sm.CmParamDt(ref cm, "@Dt2", Dt2);
            Sm.CmParamDt(ref cm, "@Dt3", Dt3);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "total_room", 

                    //1-5
                    "room_available_1",
                    "room_saleable_1",
                    "room_occupied_1",
                    "room_hu_1",
                    "room_complimentary_1",

                    //6-10
                    "guest_1",
                    "room_oo_1",
                    "room_available_2",
                    "room_saleable_2",
                    "room_occupied_2",
                    
                    //11-15
                    "room_hu_2",
                    "room_complimentary_2",
                    "guest_2",
                    "room_oo_2",
                    "room_available_3",
                    
                    //16-20
                    "room_saleable_3",
                    "room_occupied_3",
                    "room_hu_3",
                    "room_complimentary_3",
                    "guest_3",

                    //21-24
                    "room_oo_3",
                    "trr_1",
                    "trr_2",
                    "trr_3"
                });
                if (dr.HasRows)
                {
                    
                    while (dr.Read())
                    {
                        l.Add(new Result1()
                        {
                            total_room = Sm.DrDec(dr, c[0]),
                            room_available_1 = Sm.DrDec(dr, c[1]),
                            room_saleable_1 = Sm.DrDec(dr, c[2]),
                            room_occupied_1 = Sm.DrDec(dr, c[3]),
                            room_hu_1 = Sm.DrDec(dr, c[4]),
                            room_complimentary_1 = Sm.DrDec(dr, c[5]),
                            guest_1 = Sm.DrDec(dr, c[6]),
                            room_oo_1 = Sm.DrDec(dr, c[7]),
                            room_available_2 = Sm.DrDec(dr, c[8]),
                            room_saleable_2 = Sm.DrDec(dr, c[9]),
                            room_occupied_2 = Sm.DrDec(dr, c[10]),
                            room_hu_2 = Sm.DrDec(dr, c[11]),
                            room_complimentary_2 = Sm.DrDec(dr, c[12]),
                            guest_2 = Sm.DrDec(dr, c[13]),
                            room_oo_2 = Sm.DrDec(dr, c[14]),
                            room_available_3 = Sm.DrDec(dr, c[15]),
                            room_saleable_3 = Sm.DrDec(dr, c[16]),
                            room_occupied_3 = Sm.DrDec(dr, c[17]),
                            room_hu_3 = Sm.DrDec(dr, c[18]),
                            room_complimentary_3 = Sm.DrDec(dr, c[19]),
                            guest_3 = Sm.DrDec(dr, c[20]),
                            room_oo_3 = Sm.DrDec(dr, c[21]),
                            trr_1 = Sm.DrDec(dr, c[22]),
                            trr_2 = Sm.DrDec(dr, c[23]),
                            trr_3 = Sm.DrDec(dr, c[24]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process2(ref List<Result1> l1, ref List<Result2> l2)
        {
            var total_room = l1[0].total_room;

            var room_available_1 = l1[0].room_available_1;
            var room_saleable_1 = l1[0].room_saleable_1;
            var room_occupied_1 = l1[0].room_occupied_1;
            var room_hu_1 = l1[0].room_hu_1;
            var room_complimentary_1 = l1[0].room_complimentary_1;
            var guest_1 = l1[0].guest_1;
            var room_oo_1 = l1[0].room_oo_1;

            var room_available_2 = l1[0].room_available_2;
            var room_saleable_2 = l1[0].room_saleable_2;
            var room_occupied_2 = l1[0].room_occupied_2;
            var room_hu_2 = l1[0].room_hu_2;
            var room_complimentary_2 = l1[0].room_complimentary_2;
            var guest_2 = l1[0].guest_2;
            var room_oo_2 = l1[0].room_oo_2;

            var room_available_3 = l1[0].room_available_3;
            var room_saleable_3 = l1[0].room_saleable_3;
            var room_occupied_3 = l1[0].room_occupied_3;
            var room_hu_3 = l1[0].room_hu_3;
            var room_complimentary_3 = l1[0].room_complimentary_3;
            var guest_3 = l1[0].guest_3;
            var room_oo_3 = l1[0].room_oo_3;

            var trr_1 = l1[0].trr_1;
            var trr_2 = l1[0].trr_2;
            var trr_3 = l1[0].trr_3;

            //room sold/paying
            decimal rsp_1 = room_occupied_1-room_hu_1-room_complimentary_1;
            decimal rsp_2 = room_occupied_2-room_hu_2-room_complimentary_2;
            decimal rsp_3 = room_occupied_3-room_hu_3-room_complimentary_3;

            //vacant room
            decimal vr_1 = room_saleable_1 - room_occupied_1;
            decimal vr_2 = room_saleable_2 - room_occupied_2;
            decimal vr_3 = room_saleable_3 - room_occupied_3;

            //% Occupancy (Paid)
            decimal pop_1 = 0m;
            if (room_saleable_1!=0m) pop_1 = (rsp_1 / room_saleable_1) * 100m;
            decimal pop_2 = 0m;
            if (room_saleable_2 != 0m) pop_2 = (rsp_2 / room_saleable_2) * 100m;
            decimal pop_3 = 0m;
            if (room_saleable_3 != 0m) pop_3 = (rsp_3 / room_saleable_3) * 100m;

            //% Occupancy (Paid+House Use+Compliment)
            decimal pphc_1 = 0m;
            if (room_saleable_1 != 0m) pphc_1 = ((room_hu_1 + room_complimentary_1 + rsp_1) / room_saleable_1) * 100m;
            decimal pphc_2 = 0m;
            if (room_saleable_2 != 0m) pphc_2 = ((room_hu_2 + room_complimentary_2 + rsp_2) / room_saleable_2) * 100m;
            decimal pphc_3 = 0m;
            if (room_saleable_3 != 0m) pphc_3 = ((room_hu_3 + room_complimentary_3 + rsp_3) / room_saleable_3) * 100m;

            //Average Room Rate
            decimal avr_1 = 0m;
            if (rsp_1!=0m) avr_1 = (trr_1 / rsp_1) * 100m;
            decimal avr_2 = 0m;
            if (rsp_2 != 0m) avr_2 = (trr_2 / rsp_2) * 100m;
            decimal avr_3 = 0m;
            if (rsp_3 != 0m) avr_3 = (trr_3 / rsp_3) * 100m;

            //Average Room Rate+Compliment
            decimal avrc_1 = 0m;
            if ((rsp_1 + room_complimentary_1)!=0m) avrc_1= (trr_1 / (rsp_1 + room_complimentary_1)) * 100m;
            decimal avrc_2 = 0m;
            if ((rsp_2 + room_complimentary_2) != 0m) avrc_2 = (trr_2 / (rsp_2 + room_complimentary_2)) * 100m;
            decimal avrc_3 = 0m;
            if ((rsp_3 + room_complimentary_3) != 0m) avrc_3 = (trr_3 / (rsp_3 + room_complimentary_3)) * 100m;

            //Double Occupancy
            decimal do_1 = 0m;
            if (rsp_1 != 0m) do_1 = (guest_1 / rsp_1);
            decimal do_2 = 0m;
            if (rsp_2 != 0m) do_2 = (guest_2 / rsp_2);
            decimal do_3 = 0m;
            if (rsp_3 != 0m) do_3 = (guest_3 / rsp_3);

            //Average Rate Per Guest
            decimal arg_1 = 0m;
            if (guest_1!=0m) arg_1 = (trr_1 / guest_1);
            decimal arg_2 = 0m;
            if (guest_2 != 0m) arg_2 = (trr_2 / guest_2);
            decimal arg_3 = 0m;
            if (guest_3 != 0m) arg_3 = (trr_3 / guest_3);

            //Average Guest Per Room
            decimal agr_1 = 0m;
            if (rsp_1!=0m) agr_1 = (guest_1 / rsp_1);
            decimal agr_2 = 0m;
            if (rsp_2 != 0m) agr_2 = (guest_2 / rsp_2);
            decimal agr_3 = 0m;
            if (rsp_3 != 0m) agr_3 = (guest_3 / rsp_3);

            //RevPar
            decimal rp_1 = 0m;
            if (room_available_1!=0m) rp_1 = (trr_1 / room_available_1);
            decimal rp_2 = 0m;
            if (room_available_2 != 0m) rp_2 = (trr_2 / room_available_2);
            decimal rp_3 = 0m;
            if (room_available_3 != 0m) rp_3 = (trr_3 / room_available_3);

            l2 = new List<Result2>(){
                    new Result2{
                        Code = "01", 
                        Description = "Total Room",
                        Today = total_room,
                        MTD = total_room,
                        YTD = total_room
                    },
                    new Result2{
                        Code = "02", 
                        Description = "Room Available", 
                        Today = room_available_1,
                        MTD = room_available_2,
                        YTD = room_available_3
                    },
                    new Result2{
                        Code = "03", 
                        Description = "Room Salesable",
                        Today = room_saleable_1,
                        MTD = room_saleable_2,
                        YTD = room_saleable_3
                    },
                    new Result2{
                        Code = "04", 
                        Description = "Room Occupied",
                        Today = room_occupied_1,
                        MTD = room_occupied_2,
                        YTD = room_occupied_3
                    },
                    new Result2{
                        Code = "05", 
                        Description = "Room Occupied House Use", 
                        Today = room_hu_1,
                        MTD = room_hu_2,
                        YTD = room_hu_3
                    },
                    new Result2{
                        Code = "06", 
                        Description = "Room Occupied Compliment",
                        Today = room_complimentary_1,
                        MTD = room_complimentary_2,
                        YTD = room_complimentary_3
                    },
                    new Result2{
                        Code = "07", 
                        Description = "Room Sold/Paying",
                        Today = rsp_1,
                        MTD = rsp_2,
                        YTD = rsp_3
                    },
                    new Result2{
                        Code = "08", 
                        Description = "Vacant Room",
                        Today = vr_1,
                        MTD = vr_2,
                        YTD = vr_3
                    },
                    new Result2{
                        Code = "09", 
                        Description = "% Occupancy (Paid)",
                        Today = pop_1,
                        MTD = pop_2,
                        YTD = pop_3
                    },
                    new Result2{
                        Code = "10", 
                        Description = "% Occupancy (Paid+House Use+Compliment)", 
                        Today = pphc_1,
                        MTD = pphc_2,
                        YTD = pphc_3
                    },
                    new Result2{
                        Code = "11", 
                        Description = "Number of Guest",
                        Today = guest_1,
                        MTD = guest_2,
                        YTD = guest_3
                    },
                    new Result2{
                        Code = "12", 
                        Description = "Room Out Of Order", 
                        Today = room_oo_1,
                        MTD = room_oo_2,
                        YTD = room_oo_3
                    },
                    new Result2{
                        Code = "13", 
                        Description = "Total Room Revenue",
                        Today = trr_1,
                        MTD = trr_2,
                        YTD = trr_3
                    },
                    new Result2{
                        Code = "14", 
                        Description = "Average Room Rate", 
                        Today = avr_1,
                        MTD = avr_2,
                        YTD = avr_3
                    },
                    new Result2{
                        Code = "15", 
                        Description = "Average Room Rate+Compliment",
                        Today = avrc_1,
                        MTD = avrc_2,
                        YTD = avrc_3
                    },
                    new Result2{
                        Code = "16", 
                        Description = "Double Occupancy",
                        Today = do_1,
                        MTD = do_2,
                        YTD = do_3
                    },
                    new Result2{
                        Code = "17", 
                        Description = "Average Rate Per Guest",
                        Today = arg_1,
                        MTD = arg_2,
                        YTD = arg_3
                    },
                    new Result2{
                        Code = "18", 
                        Description = "Average Guest Per Room", 
                        Today = agr_1,
                        MTD = agr_2,
                        YTD = agr_3
                    },
                    new Result2{
                        Code = "19", 
                        Description = "RevPar", 
                        Today = rp_1,
                        MTD = rp_2,
                        YTD = rp_3
                    }
            };
        }

        private void Process3(ref List<Result2> l)
        {
            iGRow r;
            var Dt = Sm.Left(Sm.GetDte(DteDt), 8);
            int Yr = Int32.Parse(Sm.Left(Dt, 4));
            var Month = Dt.Substring(4, 2);
            int Mth = Int32.Parse(Month);
            int Day = Int32.Parse(Sm.Right(Dt, 2));
            DateTime dt = new DateTime(Yr, Mth, Day, 0, 0, 0, 0);
            Grd1.BeginUpdate();
            for (var i = 0; i < l.Count; i++)
            {
                r = Grd1.Rows.Add();
                r.Cells[0].Value = i + 1;
                r.Cells[1].Value = Sm.ConvertDate(Dt);
                r.Cells[2].Value = string.Format("{0:MMMM}", dt);
                r.Cells[3].Value = l[i].Description;
                r.Cells[4].Value = l[i].Today;
                r.Cells[5].Value = 0m;
                r.Cells[6].Value = 0m;
                r.Cells[7].Value = 0m;
                r.Cells[8].Value = l[i].MTD;
                r.Cells[9].Value = 0m;
                r.Cells[10].Value = 0m;
                r.Cells[11].Value = 0m;
                r.Cells[12].Value = l[i].YTD;
                r.Cells[13].Value = 0m;
                r.Cells[14].Value = 0m;
                r.Cells[15].Value = 0m;
            }
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 4, 5, 6, 8, 9, 10, 12, 13, 14 });
            Grd1.EndUpdate();
        }

        private void GetProcess1SubQuery(ref StringBuilder SQL, byte i, bool IsUseComma)
        {
            //SQL.AppendLine("0.00 As room_available_"+i+", ");
            //SQL.AppendLine("0.00 As room_saleable_" + i + ", ");
            SQL.AppendLine("0.00 As room_occupied_" + i + ", ");
            SQL.AppendLine("0.00 As room_hu_" + i + ", ");
            SQL.AppendLine("0.00 As room_complimentary_" + i + ", ");
            SQL.AppendLine("0.00 As guest_" + i + ", ");
            SQL.AppendLine("0.00 As room_oo_" + i);
            if (IsUseComma) SQL.AppendLine(", ");
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Class

        private class Result1
        {
            //1:Today
            //2:Month
            //3:Year

            public decimal total_room { get; set; }

            public decimal room_available_1 { get; set; }
            public decimal room_saleable_1 { get; set; }
            public decimal room_occupied_1 { get; set; }
            public decimal room_hu_1 { get; set; }
            public decimal room_complimentary_1 { get; set; }
            public decimal guest_1 { get; set; }
            public decimal room_oo_1 { get; set; }

            public decimal room_available_2 { get; set; }
            public decimal room_saleable_2 { get; set; }
            public decimal room_occupied_2 { get; set; }
            public decimal room_hu_2 { get; set; }
            public decimal room_complimentary_2 { get; set; }
            public decimal guest_2 { get; set; }
            public decimal room_oo_2 { get; set; }

            public decimal room_available_3 { get; set; }
            public decimal room_saleable_3 { get; set; }
            public decimal room_occupied_3 { get; set; }
            public decimal room_hu_3 { get; set; }
            public decimal room_complimentary_3 { get; set; }
            public decimal guest_3 { get; set; }
            public decimal room_oo_3 { get; set; }

            public decimal trr_1 { get; set; }
            public decimal trr_2 { get; set; }
            public decimal trr_3 { get; set; }
        }

        private class Result2
        {
            public string Code { get; set; }
            public string Description { get; set; }
            public decimal Today { get; set; }
            public decimal MTD { get; set; }
            public decimal YTD { get; set; }
        }

        #endregion
    }
}
