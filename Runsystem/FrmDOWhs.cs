﻿#region Update
/*
    14/03/2018 [ARI] tambah informasi approval [detail]
    26/07/2018 [WED] waktu cancel dokumen juga cek ke approval status nya udah cancel atau belum
    08/09/2018 [TKG] Bug saat do to other warehouse, apabila stok gudang sudah kosong masih bisa di-DO-kan.
    01/04/2019 [HAR] tambah informasi expedisi ambil dari msater vendor
    19/01/2020 [TKG/IMS] tambah informasi local code, specification
    04/02/2020 [WED/MMM] memunculkan informasi Production Order, berdasarkan parameter IsDOWhsDisplayProductionOrderInfo
    25/08/2020 [VIN/MGI] tambah ProductionWorkGrp
    01/11/2020 [TKG/IMS] memunculkan keterangan vendor
    16/06/2021 [VIN/IMS] IMS pakai DOWhsIMS
    24/06/2021 [IQA/IMS] Penyesuain Di Print Out
    19/01/2023 [RDA/MNET] tambah fitur upload file berdasarkan parameter IsDOWHSAllowToUploadFile dan IsDOWHSAllowToUploadFileMandatory
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using System.IO;
using System.Net;

#endregion

namespace RunSystem
{
    public partial class FrmDOWhs : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string 
            mMenuCode = string.Empty, mAccessInd = string.Empty, 
            mDOWHSDefaultQty = string.Empty,
            mDocNo = string.Empty; //if this application is called from other application
        internal FrmDOWhsFind FrmFind;
        internal int mNumberOfInventoryUomCode = 1;
        internal bool mIsItGrpCodeShow = false;
        internal bool
            mIsDOWhsNeedApproval = false,
            mIsDOWhsApprovalBasedOnWhs = false,
            mIsDOWhsDisplayProductionOrderInfo = false,
            mIsUseProductionWorkGroup = false,
            mIsDOWhsShowItemVendor = false;

        //for upload file
        private bool mIsDOWHSAllowToUploadFile = false;
        private bool mIsDOWHSAllowToUploadFileMandatory = false;
        private string
          mPortForFTPClient = string.Empty,
          mHostAddrForFTPClient = string.Empty,
          mSharedFolderForFTPClient = string.Empty,
          mUsernameForFTPClient = string.Empty,
          mPasswordForFTPClient = string.Empty,
          mFileSizeMaxUploadFTPClient = string.Empty,
          mFormatFTPClient = string.Empty;
        private byte[] downloadedData;
        //for upload file

        #endregion

        #region Constructor

        public FrmDOWhs(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "DO To Other Warehouse";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                Sl.SetLueWhsCode(ref LueWhsCode2);
                Sl.SetLueTTCode(ref LueTtCode);
                SetLueVdCode(ref LueVdCode);
                Sl.SetLueOption(ref LueProductionWorkGroup, "ProductionWorkGroup");

                base.FrmLoad(sender, e);

                if (!mIsUseProductionWorkGroup)
                    WorkGroup.Visible = LueProductionWorkGroup.Visible = false;
                else
                    WorkGroup.Visible = LueProductionWorkGroup.Visible = true;

                if (!mIsDOWHSAllowToUploadFile)
                {
                    TcDOWhs.TabPages.Remove(Tp2);
                }

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            SetNumberOfInventoryUomCode();
            mIsItGrpCodeShow = Sm.GetParameter("IsItGrpCodeShow") == "Y";
            mIsDOWhsNeedApproval = Sm.GetParameter("IsDOWhsNeedApproval") == "Y";
            mIsDOWhsApprovalBasedOnWhs = Sm.GetParameter("IsDOWhsApprovalBasedOnWhs") == "Y";
            mDOWHSDefaultQty = Sm.GetParameter("DOWHSDefaultQty");
            mIsDOWhsDisplayProductionOrderInfo = Sm.GetParameterBoo("IsDOWhsDisplayProductionOrderInfo");
            mIsUseProductionWorkGroup = Sm.GetParameterBoo("IsUseProductionWorkGroup");
            mIsDOWhsShowItemVendor = Sm.GetParameterBoo("IsDOWhsShowItemVendor");

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'IsDOWHSAllowToUploadFile', 'IsDOWHSAllowToUploadFileMandatory', 'FileSizeMaxUploadFTPClient','PortForFTPClient','PasswordForFTPClient','UsernameForFTPClient','SharedFolderForFTPClient','HostAddrForFTPClient' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]).Trim();
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsDOWHSAllowToUploadFile": mIsDOWHSAllowToUploadFile = ParValue == "Y"; break;
                            case "IsDOWHSAllowToUploadFileMandatory": mIsDOWHSAllowToUploadFileMandatory = ParValue == "Y"; break;

                            //string
                            case "HostAddrForFTPClient": mHostAddrForFTPClient = ParValue; break;
                            case "SharedFolderForFTPClient": mSharedFolderForFTPClient = ParValue; break;
                            case "UsernameForFTPClient": mUsernameForFTPClient = ParValue; break;
                            case "PasswordForFTPClient": mPasswordForFTPClient = ParValue; break;
                            case "PortForFTPClient": mPortForFTPClient = ParValue; break;
                            case "FileSizeMaxUploadFTPClient": mFileSizeMaxUploadFTPClient = ParValue; break;
                        }
                    }
                }
                dr.Close();
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grid 1
            Grd1.Cols.Count = 26;
            Grd1.FrozenArea.ColCount = 6;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",
                        
                        //1-5
                        "Cancel",
                        "Old Cancel",
                        "",
                        "Item's Code",
                        "",
                        
                        //6-10
                        "Item's Name",
                        "Foreign Name",
                        "Batch#",
                        "Source",
                        "Lot",

                        //11-15
                        "Bin",
                        "Available"+Environment.NewLine+"Stock",
                        "Quantity",
                        "UoM",
                        "Available"+Environment.NewLine+"Stock",

                        //16-20
                        "Quantity",
                        "UoM",
                        "Available"+Environment.NewLine+"Stock",
                        "Quantity",
                        "UoM",

                        //21-25
                        "Remark",
                        "Group",
                        "Local Code",
                        "Specification",
                        "Vendor"
                    },
                     new int[] 
                    {
                        //0
                        20,
 
                        //1-5
                        50, 50, 20, 100, 20, 
                        
                        //6-10
                        300, 150, 200, 200, 100,  
                        
                        //11-15
                        100, 100, 100, 100, 100,  
                        
                        //16-20
                        100, 100, 100, 100, 100, 

                        //21-25
                        400, 100, 120, 200, 200
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1, 2 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 4, 6, 7, 8, 9, 10, 11, 12, 14, 15, 17, 18, 20, 22, 23, 24, 25 });
            Sm.GrdFormatDec(Grd1, new int[] { 12, 13, 15, 16, 18, 19 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 3, 5 });
            Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 4, 5, 9, 15, 16, 17, 18, 19, 20, 22, 24, 25 }, false);
            if (mIsItGrpCodeShow)
            {
                Grd1.Cols[22].Visible = true;
                Grd1.Cols[22].Move(6);
            }
            Grd1.Cols[23].Move(8);
            Grd1.Cols[24].Move(9);
            ShowInventoryUomCode();
            if (mIsDOWhsShowItemVendor) Sm.GrdColInvisible(Grd1, new int[] { 25 }, true);

            #endregion

            #region Grid 2

            Grd2.Cols.Count = 4;
            Grd2.ReadOnly = true;

            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[] 
                    {
                        //0
                        "Checked By", 
                        
                        //1-3
                        "Status",
                        "Date",
                        "Remark"
                    },
                    new int[] 
                    {
                        150, 
                        100, 100, 400
                    }
                );
            Sm.GrdFormatDate(Grd2, new int[] { 2 });

            #endregion

            #region Grid 3 Upload File

            if (mIsDOWHSAllowToUploadFile)
            {
                Grd3.Cols.Count = 7;
                Grd3.FrozenArea.ColCount = 3;

                Sm.GrdHdrWithColWidth(
                        Grd3,
                        new string[]
                        {
				            //0
				            "D No",
				            //1-5
				            "File Name",
                            "",
                            "D",
                            "Upload By",
                            "Date",

				            //6
				            "Time"
                        },
                         new int[]
                        {
                            0,
                            250, 20, 20, 100, 100,
                            100
                        }
                    );

                Sm.GrdColInvisible(Grd3, new int[] { 0 }, false);
                Sm.GrdFormatDate(Grd3, new int[] { 5 });
                Sm.GrdFormatTime(Grd3, new int[] { 6 });
                Sm.GrdColButton(Grd3, new int[] { 2, 3 });
                Sm.GrdColReadOnly(true, true, Grd3, new int[] { 0, 1, 2, 3, 4, 5, 6 });
                Grd3.Cols[2].Move(1);
            }

            #endregion
    }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4, 5, 9, 24 }, !ChkHideInfoInGrd.Checked);
        }

        private void ShowInventoryUomCode()
        {
            if (mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 15, 16, 17 }, true);

            if (mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 15, 16, 17, 18, 19, 20 }, true);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, TxtStatus, LueWhsCode, LueWhsCode2, MeeRemark,
                        LueVdCode, LueDriver, LueLicenceNo, LueTtCode, 
                        MeeNote, MeeExpRemark, LueProductionWorkGroup
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21 });
                    if (mIsDOWHSAllowToUploadFile)
                    {
                        Sm.GrdColReadOnly(true, true, Grd3, new int[] { 0, 1, 2, 4, 5, 6 });

                        for (int Row = 0; Row < Grd3.Rows.Count; Row++)
                        {
                            if (Sm.GetGrdStr(Grd3, Row, 4).Length == 0)
                            {
                                Grd3.Cells[Row, 2].ReadOnly = iGBool.False;
                                Grd3.Cells[Row, 3].ReadOnly = iGBool.True;
                            }
                            else
                            {
                                Grd3.Cells[Row, 2].ReadOnly = iGBool.True;
                                Grd3.Cells[Row, 3].ReadOnly = iGBool.False;
                            }
                        }
                    }
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueWhsCode, LueWhsCode2, MeeRemark,
                         LueVdCode, LueDriver, LueLicenceNo, LueTtCode, 
                        MeeNote, MeeExpRemark, LueProductionWorkGroup
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 3, 13, 16, 19, 21 });
                    if (mIsDOWHSAllowToUploadFile) Sm.GrdColReadOnly(false, true, Grd3, new int[] { 2, 3 });
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1 });
                    TxtDocNo.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, TxtStatus, LueWhsCode, LueWhsCode2, MeeRemark,
                LueVdCode, LueDriver, LueLicenceNo, LueTtCode,  MeeNote, MeeExpRemark,
                LueProductionWorkGroup
            });
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdBoolValueFalse(ref Grd1, 0, new int[] { 1, 2 });
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 12, 13, 15, 16, 18, 19 });
            Grd2.Rows.Clear();
            Grd2.Rows.Count = 1;
            if (mIsDOWHSAllowToUploadFile) Sm.ClearGrd(Grd3, true);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmDOWhsFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                TxtStatus.Text = "Outstanding";
                Sl.SetLueWhsCode(ref LueWhsCode, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            if (mIsDOWHSAllowToUploadFile)
            {
                if (TxtStatus.Text != "Approved")
                {
                    for (int Row = 0; Row < Grd3.Rows.Count; Row++)
                    {
                        if (Sm.GetGrdStr(Grd3, Row, 4).Length == 0)
                        {
                            Grd3.Cells[Row, 2].ReadOnly = iGBool.False;
                            Grd3.Cells[Row, 3].ReadOnly = iGBool.True;
                        }
                        else
                        {
                            Grd3.Cells[Row, 2].ReadOnly = iGBool.True;
                            Grd3.Cells[Row, 3].ReadOnly = iGBool.False;
                        }
                    }
                }
                else
                {
                    for (int Row = 0; Row < Grd3.Rows.Count; Row++)
                    {
                        Grd3.Cells[Row, 2].ReadOnly = iGBool.True;
                        Grd3.Cells[Row, 3].ReadOnly = iGBool.False;
                    }
                }

            }

            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    CancelData();

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            string ParValue = Sm.GetValue("Select ParValue From TblParameter Where Parcode='NumberOfInventoryUomCode' ");
            string DocTitle = Sm.GetValue("Select ParValue From TblParameter Where Parcode='DocTitle' "); 
            if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;

            string[] TableName = { "DOWhsHdr", "DOWhsDtl" };

            var l = new List<DOWhsHdr>();
            var ldtl = new List<DOWhsDtl>();

            List<IList> myLists = new List<IList>();
            var cm = new MySqlCommand();

            #region Header
            var SQL = new StringBuilder();
            SQL.AppendLine("Select @CompanyLogo As CompanyLogo,(Select ParValue From TblParameter Where ParCode='ReportTitle1')As Company, ");
            SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle2')As Address, ");
            SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle3')As Phone, ");
            SQL.AppendLine("(Select ParValue From TblParameter Where ParCode = 'DoWhsNotes')As DoWhsNotes, ");
            SQL.AppendLine("A.DocNo, Date_Format(A.DocDt,'%d %M %Y') As DocDt, A.WhsCode, B.WhsName As WhsFrom, C.WhsName As WhsTo, C.WhsName, A.Remark, ");
            SQL.AppendLine("(Select parvalue from tblparameter where parcode='IsShowForeignName') As IsForeignName ");
            SQL.AppendLine("From TblDoWhsHdr A ");
            SQL.AppendLine("Inner Join TblWarehouse B On A.WhsCode=B.WhsCode ");
            SQL.AppendLine("Inner Join TblWarehouse C On A.WhsCode2=C.WhsCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                         //0
                         "Company",
                         //1-5
                         "Address",
                         "Phone",
                         "DoWhsNotes",
                         "DocNo",
                         "DocDt",
                         //6-10
                         "WhsFrom",
                         "WhsTo",
                         "Remark",
                         "CompanyLogo",
                         "IsForeignName"

                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new DOWhsHdr()
                        {
                            Company = Sm.DrStr(dr, c[0]),
                            Address = Sm.DrStr(dr, c[1]),
                            Phone = Sm.DrStr(dr, c[2]),
                            DoWhsNotes = Sm.DrStr(dr, c[3]),
                            DocNo = Sm.DrStr(dr, c[4]),
                            DocDt = Sm.DrStr(dr, c[5]),
                            WhsNameFrom = Sm.DrStr(dr, c[6]),
                            WhsNameTo = Sm.DrStr(dr, c[7]),
                            Remark = Sm.DrStr(dr, c[8]),
                            CompanyLogo = Sm.DrStr(dr, c[9]),
                            IsForeignName = Sm.DrStr(dr, c[10]),
                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime())),
                            TotalInd = ChkPrintTotal.Checked == true ? "Y" : "N"
                        });
                    }
                }

                dr.Close();
            }
            myLists.Add(l);
            #endregion

            #region Detail
            var cmDtl = new MySqlCommand();

            var SQLDtl = new StringBuilder();
            using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl.Open();
                cmDtl.Connection = cnDtl;

                SQLDtl.AppendLine("Select B.ItCode, C.ItName, B.BatchNo, B.Lot, B.Bin, ");
                SQLDtl.AppendLine("B.Qty, B.Qty2, B.Qty3, C.InventoryUomCode, C.InventoryUomCode2, C.InventoryUomCode3, B.Remark, C.Specification, ");

                SQLDtl.AppendLine("B.Qty+IfNull(( ");
                SQLDtl.AppendLine("    Select Sum(Qty) From TblStockSummary ");
                SQLDtl.AppendLine("    Where WhsCode=A.WhsCode And ItCode=B.ItCode And BatchNo=B.BatchNo And Source=B.Source And Lot=B.Lot And Bin=B.Bin ");
                SQLDtl.AppendLine("), 0) As AvailableStock, ");

                SQLDtl.AppendLine("B.Qty2+IfNull(( ");
                SQLDtl.AppendLine("    Select Sum(Qty2) From TblStockSummary ");
                SQLDtl.AppendLine("    Where WhsCode=A.WhsCode And ItCode=B.ItCode And BatchNo=B.BatchNo And Source=B.Source And Lot=B.Lot And Bin=B.Bin ");
                SQLDtl.AppendLine("), 0) As AvailableStock2, ");

                SQLDtl.AppendLine("B.Qty3+IfNull(( ");
                SQLDtl.AppendLine("    Select Sum(Qty3) From TblStockSummary ");
                SQLDtl.AppendLine("    Where WhsCode=A.WhsCode And ItCode=B.ItCode And BatchNo=B.BatchNo And Source=B.Source And Lot=B.Lot And Bin=B.Bin ");
                SQLDtl.AppendLine("), 0) As AvailableStock3, C.ItGrpCode, C.ForeignName ");

                SQLDtl.AppendLine("From TblDOWhsHdr A ");
                SQLDtl.AppendLine("Inner Join TblDOWhsDtl B On A.DocNo=B.DocNo And B.cancelInd = 'N' ");
                SQLDtl.AppendLine("Inner Join TblItem C On B.ItCode=C.ItCode ");
                SQLDtl.AppendLine("Where A.DocNo=@DocNo Order By B.DNo"); 

                cmDtl.CommandText = SQLDtl.ToString();

                Sm.CmParam<String>(ref cmDtl, "@DocNo", TxtDocNo.Text);
                var drDtl = cmDtl.ExecuteReader();
                var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                        {
                         //0
                         
                         "ItCode",
                         "ItName",
                         "BatchNo",
                         "Lot",

                         //6-10
                         "Bin",
                         "Qty",
                         "Qty2",
                         "Qty3",
                         "InventoryUOMCode",

                         //11-15
                         "InventoryUOMCode2",
                         "InventoryUOMCode3",
                         "Remark",
                         "ItGrpCode",
                         "ForeignName",
                         "Specification"
                        });
                if (drDtl.HasRows)
                {
                    int No = 0;
                    while (drDtl.Read())
                    {
                        No = No + 1;
                        ldtl.Add(new DOWhsDtl()
                        {
                            nomor = No,
                            ItCode = Sm.DrStr(drDtl, cDtl[0]),
                            ItName = Sm.DrStr(drDtl, cDtl[1]),
                            BatchNo = Sm.DrStr(drDtl, cDtl[2]),
                            Lot = Sm.DrStr(drDtl, cDtl[3]),
                            Bin = Sm.DrStr(drDtl, cDtl[4]),
                            Qty = Sm.DrDec(drDtl, cDtl[5]),
                            Qty2 = Sm.DrDec(drDtl, cDtl[6]),
                            Qty3 = Sm.DrDec(drDtl, cDtl[7]),
                            InventoryUOMCode = Sm.DrStr(drDtl, cDtl[8]),
                            InventoryUOMCode2 = Sm.DrStr(drDtl, cDtl[9]),
                            InventoryUOMCode3 = Sm.DrStr(drDtl, cDtl[10]),
                            Remark = Sm.DrStr(drDtl, cDtl[11]),
                            ItGrpCode = Sm.DrStr(drDtl, cDtl[12]),
                            ForeignName = Sm.DrStr(drDtl, cDtl[13]),
                            Specification = Sm.DrStr(drDtl, cDtl[14])
                        });
                    }
                }
                drDtl.Close();
            }
            myLists.Add(ldtl);
            #endregion

             int a = int.Parse(ParValue);
             if (DocTitle == "IMS")
             {
                 Sm.PrintReport("DOWhsIMS", myLists, TableName, false);
             }
             else
             {
                 if (a == 1)
                 {
                     Sm.PrintReport("DOWhs1", myLists, TableName, false);
                 }
                 else if (a == 2)
                 {
                     Sm.PrintReport("DOWhs2", myLists, TableName, false);
                 }
                 else
                 {
                     Sm.PrintReport("DOWhs3", myLists, TableName, false);
                 }
             }
        }


        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    if (e.ColIndex == 3 && !Sm.IsLueEmpty(LueWhsCode, "Warehouse"))
                    {
                        e.DoDefault = false;
                        if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmDOWhsDlg(this, Sm.GetLue(LueWhsCode)));
                    }

                    if (Sm.IsGrdColSelected(new int[] { 3, 13, 16, 19, 21 }, e.ColIndex))
                    {
                        Sm.GrdRequestEdit(Grd1, e.RowIndex);
                        Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 1, 2 });
                        Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 12, 13, 15, 16, 18, 19 });
                    }
                }
                else
                {
                    if (e.ColIndex == 1 && (Sm.GetGrdBool(Grd1, e.RowIndex, 2) || Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length == 0))
                        e.DoDefault = false;
                }
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && BtnSave.Enabled && !Sm.IsLueEmpty(LueWhsCode, "Warehouse") && TxtDocNo.Text.Length == 0)
                Sm.FormShowDialog(new FrmDOWhsDlg(this, Sm.GetLue(LueWhsCode)));

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 13, 16, 19 }, e);
            Sm.GrdAfterCommitEditTrimString(Grd1, new int[] { 21 }, e);

            if (e.ColIndex == 13)
            {
                Sm.ComputeQtyBasedOnConvertionFormula("12", Grd1, e.RowIndex, 4, 13, 16, 19, 14, 17, 20);
                Sm.ComputeQtyBasedOnConvertionFormula("13", Grd1, e.RowIndex, 4, 13, 19, 16, 14, 20, 17);
            }

            if (e.ColIndex == 16)
            {
                Sm.ComputeQtyBasedOnConvertionFormula("21", Grd1, e.RowIndex, 4, 16, 13, 19, 17, 14, 20);
                Sm.ComputeQtyBasedOnConvertionFormula("23", Grd1, e.RowIndex, 4, 16, 19, 13, 17, 20, 14);
            }

            if (e.ColIndex == 19)
            {
                Sm.ComputeQtyBasedOnConvertionFormula("31", Grd1, e.RowIndex, 4, 19, 13, 16, 20, 14, 17);
                Sm.ComputeQtyBasedOnConvertionFormula("32", Grd1, e.RowIndex, 4, 19, 16, 13, 20, 17, 14);
            }

            if (e.ColIndex == 13 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 14), Sm.GetGrdStr(Grd1, e.RowIndex, 17)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 16, Grd1, e.RowIndex, 13);

            if (e.ColIndex == 13 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 14), Sm.GetGrdStr(Grd1, e.RowIndex, 20)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 19, Grd1, e.RowIndex, 13);

            if (e.ColIndex == 16 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 17), Sm.GetGrdStr(Grd1, e.RowIndex, 20)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 19, Grd1, e.RowIndex, 16);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "DOWhs", "TblDOWhsHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveDOWhsHdr(DocNo));
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 4).Length > 0) cml.Add(SaveDOWhsDtl(DocNo, Row));

            for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd3, Row, 1).Length > 0)
                {
                    if (mIsDOWHSAllowToUploadFile && Sm.GetGrdStr(Grd3, Row, 1).Length > 0 && Sm.GetGrdStr(Grd3, Row, 1) != "openFileDialog1")
                    {
                        if (IsUploadFileNotValid(Row, Sm.GetGrdStr(Grd3, Row, 1))) return;
                    }
                }
            }

            cml.Add(SaveUploadFile(DocNo));

            Sm.ExecCommands(cml);

            for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd3, Row, 1).Length > 0)
                {
                    if (mIsDOWHSAllowToUploadFile && Sm.GetGrdStr(Grd3, Row, 1).Length > 0 && Sm.GetGrdStr(Grd3, Row, 1) != "openFileDialog1")
                    {
                        //if (Sm.GetGrdStr(Grd3, Row, 1) != Sm.GetGrdStr(Grd3, Row, 4))
                        //{
                        UploadFile(DocNo, Row, Sm.GetGrdStr(Grd3, Row, 1));
                        //}
                    }
                }
            }

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueWhsCode, "Warehouse (From)") ||
                Sm.IsLueEmpty(LueWhsCode2, "Warehouse (To)") ||
                IsGrdEmpty() ||
                IsGrdExceedMaxRecords() ||
                (mIsDOWHSAllowToUploadFile && mIsDOWHSAllowToUploadFileMandatory && IsUploadFileEmpty()) ||
                IsGrdValueNotValid();
        }

        private bool IsUploadFileEmpty()
        {
            if (Grd3.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "No files to upload, please upload at least one file.");
                return true;
            }
            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 item.");
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "DO data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            ReComputeStock();
            string Msg = string.Empty;

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, Row, 4, false, "Item is empty.")) return true;

                Msg =
                    "Item's Code : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine +
                    "Item's Name : " + Sm.GetGrdStr(Grd1, Row, 7) + Environment.NewLine +
                    "Batch# : " + Sm.GetGrdStr(Grd1, Row, 8) + Environment.NewLine +
                    "Source : " + Sm.GetGrdStr(Grd1, Row, 9) + Environment.NewLine +
                    "Lot : " + Sm.GetGrdStr(Grd1, Row, 10) + Environment.NewLine +
                    "Bin : " + Sm.GetGrdStr(Grd1, Row, 11) + Environment.NewLine + Environment.NewLine;

                if (Sm.GetGrdDec(Grd1, Row, 13) <= 0m)
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Quantity should be greater than 0.");
                    return true;
                }

                if (Sm.GetGrdDec(Grd1, Row, 13) > Sm.GetGrdDec(Grd1, Row, 12))
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Quantity should not be bigger than available stock.");
                    return true;
                }

                if (Grd1.Cols[16].Visible)
                {
                    if (Sm.GetGrdDec(Grd1, Row, 16) <= 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Quantity (2) should be greater than 0.");
                        return true;
                    }
                    if (Sm.GetGrdDec(Grd1, Row, 16) > Sm.GetGrdDec(Grd1, Row, 15))
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Quantity (2) should not be bigger than available stock (2).");
                        return true;
                    }
                }

                if (Grd1.Cols[19].Visible)
                {
                    if (Sm.GetGrdDec(Grd1, Row, 19) <= 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Quantity (3) should be greater than 0.");
                        return true;
                    }
                    if (Sm.GetGrdDec(Grd1, Row, 19) > Sm.GetGrdDec(Grd1, Row, 18))
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Quantity (3) should not be bigger than available stock (3).");
                        return true;
                    }
                }
            }
            return false;
        }

        private MySqlCommand SaveDOWhsHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblDOWhsHdr(DocNo, DocDt, CancelInd, Status, WhsCode, WhsCode2, Remark, ExpVdCode, ExpDriver, ExpLicenceNo, ExpTTCode, ExpNote,  ProductionWorkGroup, ExpRemark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'N', 'O', @WhsCode, @WhsCode2, @Remark, @ExpVdCode, @ExpDriver, @ExpLicenceNo, @ExpTTCode, @Note, @ProductionWorkGroup, @ExpRemark, @CreateBy, CurrentDateTime());");

            if (mIsDOWhsNeedApproval)
            {
                SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
                SQL.AppendLine("Select DocType, @DocNo, '001', DNo, @CreateBy, CurrentDateTime() ");
                SQL.AppendLine("From TblDocApprovalSetting ");
                SQL.AppendLine("Where DocType='DOWhs' ");

                if (mIsDOWhsApprovalBasedOnWhs)
                {
                    SQL.AppendLine("And WhsCode In ( ");
                    SQL.AppendLine("    Select WhsCode From TblDOWhsHdr Where DocNo=@DocNo ");
                    SQL.AppendLine(") ");
                }
                SQL.AppendLine("; ");
            }

            SQL.AppendLine("Update TblDOWhsHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select DocNo From TblDocApproval ");
            SQL.AppendLine("    Where DocType='DOWhs' And DocNo=@DocNo ");
            SQL.AppendLine("); ");

            var cm = new MySqlCommand()
            {
                CommandText = SQL.ToString()
                    //"Insert Into TblDOWhsHdr(DocNo, DocDt, CancelInd, Status, WhsCode, WhsCode2, Remark, CreateBy, CreateDt) " +
                    //"Values(@DocNo, @DocDt, 'N', @Status, @WhsCode, @WhsCode2, @Remark, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            //if (mIsDOWhsNeedApproval)
            //{
            //    Sm.CmParam<String>(ref cm, "@Status", "O");
            //}
            //else
            //{
            //    Sm.CmParam<String>(ref cm, "@Status", "A");
            //}
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            Sm.CmParam<String>(ref cm, "@WhsCode2", Sm.GetLue(LueWhsCode2));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@ExpVdCode", Sm.GetLue(LueVdCode));
            Sm.CmParam<String>(ref cm, "@ExpDriver", Sm.GetLue(LueDriver));
            Sm.CmParam<String>(ref cm, "@ExpLicenceNo", Sm.GetLue(LueLicenceNo));
            Sm.CmParam<String>(ref cm, "@ExpTTCode", Sm.GetLue(LueTtCode));
            Sm.CmParam<String>(ref cm, "@Note", MeeNote.Text);
            Sm.CmParam<String>(ref cm, "@ProductionWorkGroup", Sm.GetLue(LueProductionWorkGroup));
            Sm.CmParam<String>(ref cm, "@ExpRemark", MeeExpRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveDOWhsDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblDOWhsDtl(DocNo, DNo, CancelInd, ProcessInd, ItCode, BatchNo, Source, Lot, Bin, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, 'N', 'O', @ItCode, @BatchNo, @Source, @Lot, @Bin, @Qty, @Qty2, @Qty3, @Remark, @CreateBy, CurrentDateTime()); ");
           
            var cm = new MySqlCommand(){ CommandText =SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 4));
            Sm.CmParam<String>(ref cm, "@BatchNo", Sm.GetGrdStr(Grd1, Row, 8));
            Sm.CmParam<String>(ref cm, "@Source", Sm.GetGrdStr(Grd1, Row, 9));
            Sm.CmParam<String>(ref cm, "@Lot", Sm.GetGrdStr(Grd1, Row, 10));
            Sm.CmParam<String>(ref cm, "@Bin", Sm.GetGrdStr(Grd1, Row, 11));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 13));
            Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd1, Row, 16));
            Sm.CmParam<Decimal>(ref cm, "@Qty3", Sm.GetGrdDec(Grd1, Row, 19));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 21));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Cancel Data

        private void CancelData()
        {
            UpdateCancelledItem();

            string DNo = "'XXX'";

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdBool(Grd1, Row, 1) && !Sm.GetGrdBool(Grd1, Row, 2) && Sm.GetGrdStr(Grd1, Row, 4).Length > 0)
                    DNo = DNo + ",'" + Sm.GetGrdStr(Grd1, Row, 0) + "'";

            if (IsCancelledDataNotValid(DNo) || Sm.StdMsgYN("Save", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(CancelDOWhsDtl(DNo));

            //EDIT UPLOAD
            if (mIsDOWHSAllowToUploadFile)
            {
                for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd3, Row, 1).Length > 0)
                    {
                        if (mIsDOWHSAllowToUploadFile && Sm.GetGrdStr(Grd3, Row, 1).Length > 0 && Sm.GetGrdStr(Grd3, Row, 1) != "openFileDialog1" && Sm.GetGrdStr(Grd3, Row, 4).Length == 0)
                        {
                            if (IsUploadFileNotValid(Row, Sm.GetGrdStr(Grd3, Row, 1))) return;
                        }
                    }
                }

                cml.Add(SaveUploadFile(TxtDocNo.Text));
            }
            //EDIT UPLOAD

            Sm.ExecCommands(cml);

            if (mIsDOWHSAllowToUploadFile)
            {
                for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd3, Row, 1).Length > 0)
                    {
                        if (mIsDOWHSAllowToUploadFile && Sm.GetGrdStr(Grd3, Row, 1).Length > 0 && Sm.GetGrdStr(Grd3, Row, 1) != "openFileDialog1" && Sm.GetGrdStr(Grd3, Row, 4).Length == 0)
                        {
                            UploadFile(TxtDocNo.Text, Row, Sm.GetGrdStr(Grd3, Row, 1));
                        }
                    }
                }
            }

            ShowData(TxtDocNo.Text);
            if (mIsDOWHSAllowToUploadFile) ShowUploadFile(TxtDocNo.Text);
        }

        private void UpdateCancelledItem()
        {
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText =
                        "Select DNo, CancelInd From TblDOWhsDtl " +
                        "Where DocNo=@DocNo Order By DNo"
                };
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DNo", "CancelInd" });

                if (dr.HasRows)
                {
                    Grd1.ProcessTab = true;
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        {
                            if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 0), Sm.DrStr(dr, 0)))
                            {
                                if (Sm.CompareStr(Sm.DrStr(dr, 1), "Y"))
                                    Sm.SetGrdValue("B", Grd1, dr, c, Row, 1, 1);
                                Sm.SetGrdValue("B", Grd1, dr, c, Row, 2, 1);
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        private bool IsCancelledDataNotValid(string DNo)
        {
            return
                IsDataCancelledAlready() ||
                (!mIsDOWHSAllowToUploadFile && IsCancelledItemNotExisted(DNo)) ||
                IsCancelledItemAlreadyProcessed(DNo);
        }

        private bool IsDataCancelledAlready()
        {
            return
               Sm.IsDataExist(
                   "Select DocNo From TblDOWhsHdr " +
                   "Where (CancelInd='Y' Or Status='C') And DocNo=@Param;",
                   TxtDocNo.Text,
                   "This document already cancelled."
               );
        }

        private bool IsCancelledItemNotExisted(string DNo)
        {
            if (Sm.CompareStr(DNo, "'XXX'"))
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel minimum 1 item.");
                return true;
            }
            return false;
        }

        private bool IsCancelledItemAlreadyProcessed(string DNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.ItCode, B.ItName, A.BatchNo, A.Source, A.Lot, A.Bin ");
            SQL.AppendLine("From TblDOWhsDtl A ");
            SQL.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
            SQL.AppendLine("Where A.CancelInd='N' ");
            SQL.AppendLine("And A.ProcessInd='F' ");
            SQL.AppendLine("And A.DocNo=@DocNo ");
            SQL.AppendLine("And A.DNo In (" + DNo + ") ");
            SQL.AppendLine("Limit 1;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = SQL.ToString()
                };
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ItCode", "ItName", "BatchNo", "Source", "Lot", "Bin" });

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        Sm.StdMsg(mMsgType.Warning,
                             "Item Code : " + Sm.DrStr(dr, 0) + Environment.NewLine +
                             "Item Name : " + Sm.DrStr(dr, 1) + Environment.NewLine +
                             "Batch Number : " + Sm.DrStr(dr, 2) + Environment.NewLine +
                             "Source : " + Sm.DrStr(dr, 3) + Environment.NewLine +
                             "Lot : " + Sm.DrStr(dr, 4) + Environment.NewLine +
                             "Bin : " + Sm.DrStr(dr, 5) + Environment.NewLine + Environment.NewLine +
                             "This data already received.");
                        return true;
                    }
                }
                dr.Close();
            }
            return false;
        }

        private MySqlCommand CancelDOWhsDtl(string DNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblDOWhsDtl Set CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N' And DNo In (" + DNo + "); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data
        
        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                ShowDOWhsHdr(DocNo);
                ShowDOWhsDtl(DocNo);
                ShowDocApproval(DocNo);
                if (mIsDOWHSAllowToUploadFile) ShowUploadFile(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowDOWhsHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select DocNo, DocDt, Case When Status='O' then 'Outstanding' when Status = 'A' Then 'Approved' When Status = 'C' Then 'Cancelled' End as Status, "+
                    "WhsCode, WhsCode2, Remark, " +
                    "ExpVdCode, ExpDriver, ExpLicenceNo, ExpTtCode, ExpNote, ExpRemark, ProductionWorkGroup " +
                    "From TblDOWhsHdr A Where DocNo=@DocNo",
                    new string[] 
                    { 
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "Status", "WhsCode", "WhsCode2", "Remark",

                        //6-10
                        "ExpVdCode", "ExpDriver", "ExpLicenceNo", "ExpTtCode", "ExpNote", 

                        //11-12
                        "ExpRemark", "ProductionWorkGroup"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        TxtStatus.EditValue = Sm.DrStr(dr, c[2]);
                        Sl.SetLueWhsCode(ref LueWhsCode, Sm.DrStr(dr, c[3]));
                        Sm.SetLue(LueWhsCode2, Sm.DrStr(dr, c[4]));
                        MeeRemark.EditValue = Sm.DrStr(dr, c[5]);
                        Sm.SetLue(LueVdCode, Sm.DrStr(dr, c[6]));
                        Sm.SetLue(LueDriver, Sm.DrStr(dr, c[7]));
                        Sm.SetLue(LueLicenceNo, Sm.DrStr(dr, c[8]));
                        Sm.SetLue(LueTtCode, Sm.DrStr(dr, c[9]));
                        MeeNote.EditValue = Sm.DrStr(dr, c[10]);
                        MeeExpRemark.EditValue = Sm.DrStr(dr, c[11]);
                        Sm.SetLue(LueProductionWorkGroup, Sm.DrStr(dr, c[12]));

                    }, true
                );
        }

        private void ShowDOWhsDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.DNo, B.CancelInd, B.ItCode, C.ItName, C.ForeignName, B.BatchNo, B.Source, B.Lot, B.Bin, ");
            SQL.AppendLine("B.Qty, B.Qty2, B.Qty3, C.InventoryUomCode, C.InventoryUomCode2, C.InventoryUomCode3, B.Remark, C.ItGrpCode, C.ItCodeInternal, C.Specification, ");
            if (mIsDOWhsShowItemVendor)
                SQL.AppendLine("F.VdName ");
            else
                SQL.AppendLine("Null As VdName ");
            SQL.AppendLine("From TblDOWhsHdr A ");
            SQL.AppendLine("Inner Join TblDOWhsDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblItem C On B.ItCode=C.ItCode ");
            if (mIsDOWhsShowItemVendor)
            {
                SQL.AppendLine("Left Join TblRecvVdDtl D On B.Source=D.Source ");
                SQL.AppendLine("Left Join TblRecvVdHdr E On D.DocNo=E.DocNo ");
                SQL.AppendLine("Left Join TblVendor F On E.VdCode=F.VdCode ");
            }
            SQL.AppendLine("Where A.DocNo=@DocNo Order By B.DNo");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 

                    //1-5
                    "CancelInd", "ItCode", "ItName", "ForeignName", "BatchNo",  
                    
                    //6-10
                    "Source", "Lot", "Bin", "Qty", "InventoryUomCode",  
                    
                    //11-15
                    "Qty2", "InventoryUomCode2", "Qty3", "InventoryUomCode3", "Remark", 
                    
                    //16-19
                    "ItGrpCode", "ItCodeInternal", "Specification", "VdName" 
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 11);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 12);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 13);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 14);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 15);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 16);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 17);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 18);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 19);
                    Sm.SetGrdNumValueZero(ref Grd, Row, new int[] { 12, 15, 18 });
                }, false, false, true, false
            );
            Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 1, 2 });
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 12, 13, 15, 16, 18, 19 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ShowDocApproval(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.ApprovalDNo, B.UserName, A.LastUpDt, A.Remark, ");
            SQL.AppendLine("Case Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' When 'O' Then 'Outstanding' End As StatusDesc ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Left Join TblUser B On A.UserCode = B.UserCode ");
            SQL.AppendLine("Where A.DocType='DOWhs' ");
            SQL.AppendLine("And IfNull(Status, 'O')<>'O' ");
            SQL.AppendLine("And A.DocNo=@DocNo ");
            SQL.AppendLine("Order By A.ApprovalDNo;");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                    ref Grd2, ref cm,
                    SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "UserName",
                        
                        //1-3
                        "StatusDesc","LastUpDt", "Remark"

                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    }, false, false, false, false
            );
            Sm.FocusGrd(Grd2, 0, 0);
        }

        private void ShowUploadFile(string DocNo)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                    ref Grd3, ref cm,
                   "select DNo, FileName, CreateBy, CreateDt from  tbldowhsfile Where DocNo=@DocNo Order By Dno",

                    new string[]
                    {
                "Dno",
                "FileName", "CreateBy", "CreateDt"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd3, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd3, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("S", Grd3, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("D", Grd3, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("T", Grd3, dr, c, Row, 6, 3);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd3, 0, 0);
        }

        #endregion

        #region Additional Method

        private void SetNumberOfInventoryUomCode()
        {
            string NumberOfInventoryUomCode = Sm.GetValue("Select ParValue From TblParameter Where ParCode='NumberOfInventoryUomCode'");
            if (NumberOfInventoryUomCode.Length == 0)
                mNumberOfInventoryUomCode = 1;
            else
                mNumberOfInventoryUomCode = int.Parse(NumberOfInventoryUomCode);
        }

        private string GetSelectedItem()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 9).Length != 0)
                        SQL +=
                            "##" +
                            Sm.GetGrdStr(Grd1, Row, 9) +
                            Sm.GetGrdStr(Grd1, Row, 10) +
                            Sm.GetGrdStr(Grd1, Row, 11) +
                            "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        private void ReComputeStock()
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty, Source = string.Empty, Lot = string.Empty, Bin = string.Empty;

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
                if (Grd1.Rows.Count != 1)
                {
                    for (int r = 0; r < Grd1.Rows.Count; r++)
                    {
                        Source = Sm.GetGrdStr(Grd1, r, 9);
                        if (Source.Length != 0)
                        {
                            if (Filter.Length > 0) Filter += " Or ";
                            Filter += "(A.Source=@Source0" + r.ToString() + ") ";
                            Sm.CmParam<String>(ref cm, "@Source0" + r.ToString(), Source);
                        }
                    }
                }
                if (Filter.Length == 0)
                    Filter = " And 0=1 ";
                else
                    Filter = " And (" + Filter + ") ";

                SQL.AppendLine("Select A.Source, A.Lot, A.Bin, ");
                SQL.AppendLine("A.Qty-IfNull(B.Qty, 0) As Qty, ");
                SQL.AppendLine("A.Qty2-IfNull(B.Qty2, 0) As Qty2, ");
                SQL.AppendLine("A.Qty3-IfNull(B.Qty3, 0) As Qty3 ");
                SQL.AppendLine("From TblStockSummary A ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select T2.Source, T2.Lot, T2.Bin, ");
                SQL.AppendLine("    Sum(T2.Qty) As Qty, Sum(T2.Qty2) As Qty2, Sum(T2.Qty3) As Qty3  ");
                SQL.AppendLine("    From TblDOWhsHdr T1 ");
                SQL.AppendLine("    Inner Join TblDOWhsDtl T2 On T1.DocNo=T2.DocNo And T2.CancelInd='N' And T2.ProcessInd='O' ");
                SQL.AppendLine(Filter.Replace("A.","T2."));
                SQL.AppendLine("    Where T1.WhsCode=@WhsCode ");
                SQL.AppendLine("    And T1.CancelInd='N' ");
                SQL.AppendLine("    And T1.Status In ('O', 'A') ");
                SQL.AppendLine("    And T1.TransferRequestWhsDocNo Is Null ");
                SQL.AppendLine("    Group By T2.Source, T2.Lot, T2.Bin ");
                SQL.AppendLine(") B ");
                SQL.AppendLine("    On A.Source=B.Source ");
                SQL.AppendLine("    And A.Lot=B.Lot ");
                SQL.AppendLine("    And A.Bin=B.Bin ");
                SQL.AppendLine("Where A.WhsCode=@WhsCode ");
                SQL.AppendLine(Filter);
                SQL.AppendLine(";");
                
                cm.CommandText = SQL.ToString();
                cm.Connection = cn;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    "Source", 
                    "Lot", "Bin", "Qty", "Qty2", "Qty3" 
                });
                if (dr.HasRows)
                {
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        Source = Sm.DrStr(dr, 0);
                        Lot = Sm.DrStr(dr, 1);
                        Bin = Sm.DrStr(dr, 2);
                        for (int row = 0; row < Grd1.Rows.Count - 1; row++)
                        {
                            if (
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, row, 9), Source) &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, row, 10), Lot) &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, row, 11), Bin)
                                )
                            {
                                Sm.SetGrdValue("N", Grd1, dr, c, row, 12, 3);
                                Sm.SetGrdValue("N", Grd1, dr, c, row, 15, 4);
                                Sm.SetGrdValue("N", Grd1, dr, c, row, 18, 5);
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        private void SetLueDriver(ref LookUpEdit Lue, string VdCode)
        {
            Sm.SetLue3(
                ref Lue,
                "Select ContactPersonName As Col1, Position As Col2, ContactNumber As Col3 From TblVendorContactPerson Where VdCode = '" + VdCode + "'" +
                "Order By ContactPersonName",
                30, 35, 35, true, true, true, "Name", "Position", "Phone", "Col1", "Col1");
        }

        private void SetLueLicenceNo(ref LookUpEdit Lue, string VdCode)
        {
           
            Sm.SetLue1(
                ref Lue,
                "Select LicenceNo As Col1 From TblVendorLicence Where VdCode = '" + VdCode + "'" +
                "Order By Dno",
                "Licence No");
        }

        private void SetLueVdCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select VdCode As Col1, VdName As Col2 From TblVendor " +
                "Where ActInd='Y' " +
                "And VdCtCode In (Select ParValue From TblParameter Where ParCode='VdCtCodeExpedition') " +
                "Order By VdName;",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        //UPLOAD FILE - START
        private MySqlCommand SaveUploadFile(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* DO to Other Warehouse - Upload File */ ");
            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            for (int r = 0; r < Grd3.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd3, r, 1).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into tbldowhsfile(DocNo, DNo, FileName, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");

                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");

                    SQL.AppendLine("(@DocNo, @DNo_" + r.ToString() +
                        ", @FileName_" + r.ToString() +
                        ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@FileName_" + r.ToString(), Sm.GetGrdStr(Grd3, r, 1));
                }
            }

            if (!IsFirstOrExisted)
            {
                SQL.AppendLine(" On Duplicate Key Update ");
                SQL.AppendLine("    FileName = Values(FileName); ");
            }

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private void DownloadFile(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                FtpWebRequest request;

                if (mFormatFTPClient == "1")
                {
                    request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                }
                else
                {
                    request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + filename) as FtpWebRequest;
                }
                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.KeepAlive = false;
                request.UseBinary = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);
                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                //MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                Sm.StdMsg(mMsgType.Warning, "There was an error connecting to the FTP Server.");
            }
        }

        private void UploadFile(string EmpCode, int Row, string FileName)
        {
            if (IsUploadFileNotValid(Row, FileName)) return;

            FtpWebRequest request;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", FileName));
            long mFileSize = toUpload.Length;
            if (mFormatFTPClient == "1")
            {
                request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            }
            else
            {
                request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}", mHostAddrForFTPClient, mPortForFTPClient, toUpload.Name));
            }

            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);
            request.KeepAlive = false;
            request.UseBinary = true;


            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", FileName));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);


                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);

                }
            }
            while (bytesRead != 0);



            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdateUploadFile(EmpCode, Row, toUpload.Name));
            Sm.ExecCommands(cml);

        }

        private bool IsUploadFileNotValid(int Row, string FileName)
        {
            return
                IsFTPClientDataNotValid(Row, FileName) ||
                IsFileSizeNotvalid(Row, FileName) ||
                IsFileNameAlreadyExisted(Row, FileName)
             ;
        }

        private bool IsFTPClientDataNotValid(int Row, string FileName)
        {

            if (mIsDOWHSAllowToUploadFile && FileName.Length > 0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }

            if (mIsDOWHSAllowToUploadFile && FileName.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }

            if (mIsDOWHSAllowToUploadFile && FileName.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }

            if (mIsDOWHSAllowToUploadFile && FileName.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }
            return false;
        }

        private bool IsFileSizeNotvalid(int Row, string FileName)
        {
            if (mIsDOWHSAllowToUploadFile && FileName.Length > 0)
            {
                FileInfo f = new FileInfo(FileName);
                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }

                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File too large to Upload in row " + (Row + 1));
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted(int Row, string FileName)
        {
            if (mIsDOWHSAllowToUploadFile && FileName.Length > 0 && Sm.GetGrdStr(Grd3, Row, 1) != Sm.GetGrdStr(Grd3, Row, 4))
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", FileName));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select FileName From tbldowhsfile ");
                SQL.AppendLine("Where FileName=@FileName ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        private MySqlCommand UpdateUploadFile(string DocNo, int Row, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update tbldowhsfile Set ");
            SQL.AppendLine("    FileName=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo and DNo = @DNo ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));

            return cm;
        }

        private MySqlCommand DeleteDOWhsFile()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Delete From tbldowhsfile Where DocNo=@DocNo; "
            };
            Sm.CmParam<String>(ref cm, "@EmpCode", TxtDocNo.Text);

            return cm;
        }
        //UPLOAD FILE - END

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue2(Sl.SetLueWhsCode), string.Empty);
            ClearGrd();
        }

        private void BtnJournalDocNo2_Click(object sender, EventArgs e)
        {

        }

        private void BtnJournalDocNo_Click(object sender, EventArgs e)
        {

        }

        private void LueWhsCode2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode2, new Sm.RefreshLue1(Sl.SetLueWhsCode));
        }

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue1(SetLueVdCode));
            if (Sm.GetLue(LueVdCode) != null)
            {
                SetLueDriver(ref LueDriver, Sm.GetLue(LueVdCode));
                SetLueLicenceNo(ref LueLicenceNo, Sm.GetLue(LueVdCode));
            }
        }

        private void LueDriver_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDriver, new Sm.RefreshLue2(SetLueDriver), Sm.GetLue(LueVdCode));
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                //ShowExpeditionData(Sm.GetLue(LueVdCode), Sm.GetLue(LueDriver));
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void LueTtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueTtCode, new Sm.RefreshLue1(Sl.SetLueTTCode));
        }

        private void LueLicenceNo_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueLicenceNo, new Sm.RefreshLue2(SetLueLicenceNo), Sm.GetLue(LueVdCode));

        }

        #endregion

        #region Grid Event

        //UPLOAD FILE - START
        private void Grd3_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3)
            {
                if (Sm.GetGrdStr(Grd3, e.RowIndex, 1).Length > 0)
                {
                    DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, Sm.GetGrdStr(Grd3, e.RowIndex, 1), mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
                    SFD.FileName = Sm.GetGrdStr(Grd3, e.RowIndex, 1);
                    SFD.DefaultExt = "pdf";
                    SFD.AddExtension = true;

                    if (!Sm.IsGrdValueEmpty(Grd3, e.RowIndex, 1, false, "File name is empty") && downloadedData != null && downloadedData.Length != 0)
                    {
                        if (SFD.ShowDialog() == DialogResult.OK)
                        {
                            Application.DoEvents();

                            //Write the bytes to a file
                            FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                            newFile.Write(downloadedData, 0, downloadedData.Length);
                            newFile.Close();
                            Sm.StdMsg(mMsgType.Info, "File Downloaded");
                        }
                    }
                    else
                        Sm.StdMsg(mMsgType.Warning, "No File Downloaded");
                }
            }

            if (BtnSave.Enabled)
            {

                if (e.ColIndex == 2)
                {
                    Sm.GrdRequestEdit(Grd3, e.RowIndex);
                    OD.InitialDirectory = "c:";
                    OD.Filter = "Image files (*.PNG;*.JPG;*.JPEG)|*.PNG;*.JPG;*.JPEG" +
                                "|PDF files (*.pdf)|*.pdf" +
                                "|ZIP/RAR files (*.rar;*zip)|*.rar;*zip";
                    OD.FilterIndex = 2;
                    OD.ShowDialog();
                    Grd3.Cells[e.RowIndex, 1].Value = OD.FileName;
                }
            }
        }

        private void Grd3_RequestEdit(object sender, iGRequestEditEventArgs e)
        {

            if (Sm.IsGrdColSelected(new int[] { 0, 1, 2, 3 }, e.ColIndex) && BtnSave.Enabled)
            {
                Sm.GrdRequestEdit(Grd3, e.RowIndex);
            }

            if (e.ColIndex == 3)
            {
                Sm.GrdRequestEdit(Grd3, e.RowIndex);
                if (Sm.GetGrdStr(Grd3, e.RowIndex, 1).Length > 0)
                {
                    DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, Sm.GetGrdStr(Grd3, e.RowIndex, 1), mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
                    SFD.FileName = Sm.GetGrdStr(Grd3, e.RowIndex, 1);
                    SFD.DefaultExt = "pdf";
                    SFD.AddExtension = true;

                    if (!Sm.IsGrdValueEmpty(Grd3, e.RowIndex, 1, false, "File name is empty") && downloadedData != null && downloadedData.Length != 0)
                    {
                        if (SFD.ShowDialog() == DialogResult.OK)
                        {
                            Application.DoEvents();

                            //Write the bytes to a file
                            FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                            newFile.Write(downloadedData, 0, downloadedData.Length);
                            newFile.Close();
                            Sm.StdMsg(mMsgType.Info, "File Downloaded");
                        }
                    }
                    else
                        Sm.StdMsg(mMsgType.Warning, "No File Downloaded");
                }
            }
        }

        private void Grd3_RequestCellToolTipText(object sender, iGRequestCellToolTipTextEventArgs e)
        {
            if (e.ColIndex == 2)
            {
                e.Text = "Upload";
            }
            else if (e.ColIndex == 3)
            {
                e.Text = "Download";
                e.GetType();
            }
        }

        private void Grd3_KeyDown(object sender, KeyEventArgs e)
        {
            int SelectedIndex = 0;

            if (Grd3.SelectedRows.Count > 0)
            {
                for (int Index = Grd3.SelectedRows.Count - 1; Index >= 0; Index--)
                    SelectedIndex = Grd3.SelectedRows[Index].Index;
            }

            if (TxtDocNo.Text.Length > 0 && TxtStatus.Text != "Approved")
            {
                if (Sm.GetGrdStr(Grd3, SelectedIndex, 4).Length == 0)
                {
                    Sm.GrdRemoveRow(Grd3, e, BtnSave);
                    Sm.GrdEnter(Grd3, e);
                    Sm.GrdTabInLastCell(Grd3, e, BtnFind, BtnSave);
                }
            }
            else if (TxtDocNo.Text.Length == 0)
            {
                Sm.GrdRemoveRow(Grd3, e, BtnSave);
                Sm.GrdEnter(Grd3, e);
                Sm.GrdTabInLastCell(Grd3, e, BtnFind, BtnSave);
            }
        }
        //UPLOAD FILE - END

        #endregion

        #endregion

        #region Print Class

        class DOWhsHdr
        {
            public string Company { get; set; }
            public string Address { get; set; }
            public string Phone { get; set; }
            public string DoWhsNotes { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string WhsNameFrom { get; set; }
            public string WhsNameTo { get; set; }
            public string Remark { get; set; }
            public string CompanyLogo { get; set; }
            public string IsForeignName { get; set; }
            public string PrintBy { get; set; }
            public string TotalInd { get; set; }
        }

        class DOWhsDtl
        {
            public int nomor { get; set; }
            public string RecvVdDocNo { get; set; }
            public string DocDt { get; set; }
            public string ItCode { get; set; }
            public string ItName { get; set; }
            public string BatchNo { get; set; }
            public string Lot { get; set; }
            public string Bin { get; set; }
            public decimal Qty { get; set; }
            public decimal Qty2 { get; set; }
            public decimal Qty3 { get; set; }
            public string InventoryUOMCode { get; set; }
            public string InventoryUOMCode2 { get; set; }
            public string InventoryUOMCode3 { get; set; }
            public string Remark { get; set; }
            public string ItGrpCode { get; set; }
            public string ForeignName { get; set; }
            public string Specification { get; set; }
        }

       

        #endregion

    }
}
