﻿#region Update
/*
    06/01/2020 [TKG/IOK] bug saat menampilkan data tanpa filter 
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmDKODlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmDKO mFrmParent;
        string mSQL = string.Empty;
        byte mDocType = 1;

        #endregion

        #region Constructor

        public FrmDKODlg(FrmDKO FrmParent, byte DocType)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mDocType = DocType;
        }

        #endregion

        #region Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                this.Text += mDocType == 1 ? " Delivery Request" : " Packing List";
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -30);
                Sl.SetLueCtCode(ref LueCtCode);
                SetGrd();
                SetSQL();
                base.FrmLoad(sender, e);
                ShowData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetSQL()
        {
            var SQL = new StringBuilder();

            if (mDocType == 1)
            {
                SQL.AppendLine("Select A.DocNo, A.DocDt, A.CtCode, B.CtName, ");
                SQL.AppendLine("A.SAName, C.VdName, A.ExpDriver, A.ExpPlatNo, D.TTName ");
                SQL.AppendLine("From TblDRHdr A ");
                SQL.AppendLine("Inner Join TblCustomer B On A.CtCode=B.CtCode ");
                SQL.AppendLine("Left Join TblVendor C On A.ExpVdCode=C.VdCode ");
                SQL.AppendLine("Left Join TblTransportType D On A.ExpTTCode = D.TTCode "); 
                SQL.AppendLine("Where A.CancelInd='N' ");
                SQL.AppendLine("And A.ProcessInd Not In ('C', 'M', 'F') ");
                SQL.AppendLine("And A.DocNo not in (Select distinct DrDocno from tblDKO where cancelind = 'N' And DrDocNo is not null) ");
                SQL.AppendLine("And A.DocDt Between @DocDt1 And @DocDt2 ");
            }
            else
            {
                SQL.AppendLine("Select Distinct A.DocNo, A.DocDt, C.CtCode, D.CtName, F.PortName, ");
                SQL.AppendLine("E.SectionNo, ");
                SQL.AppendLine("Case E.SectionNo ");
                for (int i = 1; i <= 25; i++)
                    SQL.AppendLine("    When '" + i.ToString() + "' Then Cnt" + i.ToString());
                SQL.AppendLine(" End As Cnt, ");
                SQL.AppendLine("Case E.SectionNo ");
                for (int i = 1; i <= 25; i++)
                    SQL.AppendLine("    When '" + i.ToString() + "' Then Seal" + i.ToString());
                SQL.AppendLine(" End As Seal ");
                SQL.AppendLine("From TblPLHdr A ");
                SQL.AppendLine("Inner Join TblSIHdr B On A.SIDocNo=B.DocNo ");
                SQL.AppendLine("Inner Join TblSP C ");
                SQL.AppendLine("    On B.SPDocNo=C.DocNo ");
                SQL.AppendLine("    And C.Status <> ('C') ");
                SQL.AppendLine("Inner Join TblCustomer D On C.CtCode=D.CtCode ");
                SQL.AppendLine("Inner Join TblPLDtl E On A.DocNo=E.DocNo  ");
                SQL.AppendLine("Left Join TblPort F On C.PortCode1 = F.PortCode ");
                SQL.AppendLine("Where Not Exists( ");
                SQL.AppendLine("    Select 1 ");
                SQL.AppendLine("    From TblDko ");
                SQL.AppendLine("    Where PLDocNo Is Not Null ");
                SQL.AppendLine("    And SectionNo Is Not Null ");
                SQL.AppendLine("    And PLDocNo=A.DocNo ");
                SQL.AppendLine("    And SectionNo=E.SectionNo ");
                SQL.AppendLine("    And CancelInd ='N' ");
                SQL.AppendLine(") ");
                SQL.AppendLine("And A.DocDt Between @DocDt1 And @DocDt2 ");
            }

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            if (mDocType == 1)
            {
                Grd1.Cols.Count = 11;
                Grd1.FrozenArea.ColCount = 2;
                Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "DR#", 
                        "",
                        "Date",
                        "Customer Code",
                        "Customer",

                        //6-10
                        "Shipping",
                        "Expedition",
                        "Driver",
                        "Transport Type",
                        "Vehicle Plat#"
                    },
                    new int[] 
                    {
                        //0
                        50, 

                        //1-5
                        140, 20, 80, 0, 250,   
                    
                        //6-9
                        250, 250, 150, 150, 150
                    }
                );
                Sm.GrdColButton(Grd1, new int[] { 2 });
                Sm.GrdFormatDate(Grd1, new int[] { 3 });
                Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 3, 4, 5, 6, 7, 8, 9, 10 });
                Sm.GrdColInvisible(Grd1, new int[] { 2 }, false);
            }
            else
            {
                Grd1.Cols.Count = 10;
                Grd1.FrozenArea.ColCount = 2;
                Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "PL#", 
                        "",
                        "Date",
                        "Customer Code",
                        "Customer",

                        //6-8
                        "Container",
                        "Seal",
                        "Section#",
                        "Port Loading"
                    },
                    new int[] 
                    {
                        //0
                        50, 

                        //1-5
                        140, 20, 80, 0, 250,   
                    
                        //6-8
                        200, 200, 0, 100
                    }
                );
                Sm.GrdColButton(Grd1, new int[] { 2 });
                Sm.GrdFormatDate(Grd1, new int[] { 3 });
                Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 3, 4, 5, 6, 7, 8, 9 });
                Sm.GrdColInvisible(Grd1, new int[] { 2, 8 }, false);
            }
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                if (mDocType == 1)
                    Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCode), "A.CtCode", true);
                else
                    Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCode), "C.CtCode", true);
                
                if (mDocType == 1)
                {
                    Sm.ShowDataInGrid(
                            ref Grd1, ref cm, mSQL + Filter + " Order By A.DocDt Desc, A.DocNo;",
                            new string[]
                        {
                            //0
                            "DocNo", 

                            //1-5
                            "DocDt", "CtCode", "CtName", "SAName", "VdName", 
                            
                            //6-7
                            "ExpDriver", "TTName", "ExpPlatNo" 
                        },
                            (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                            {
                                Grd.Cells[Row, 0].Value = Row + 1;
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                                Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 1);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                            }, true, false, false, false
                        );
                }
                else
                {
                    Sm.ShowDataInGrid(
                            ref Grd1, ref cm,
                            mSQL + Filter + " Order By A.DocDt, A.DocNo;",
                            new string[]
                        {
                            //0
                            "DocNo", 

                            //1-5
                            "DocDt", "CtCode", "CtName", "Cnt", "Seal",

                            //6
                            "SectionNo", "PortName"
                        },
                            (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                            {
                                Grd.Cells[Row, 0].Value = Row + 1;
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                                Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 1);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                            }, true, false, false, false
                        );
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            try
            {
                if (Sm.IsFindGridValid(Grd1, 1))
                {
                    mFrmParent.ClearData2();
                    int Row = Grd1.CurRow.Index;
                    string DocNo = Sm.GetGrdStr(Grd1, Row, 1);

                    if (mDocType == 1)
                    {
                        mFrmParent.TxtDRDocNo.EditValue = DocNo;
                        mFrmParent.mCtCode = Sm.GetGrdStr(Grd1, Row, 4);
                        mFrmParent.TxtCtCode.EditValue = Sm.GetGrdStr(Grd1, Row, 5);
                        mFrmParent.TxtTT.EditValue = Sm.GetGrdStr(Grd1, Row, 9);
                        mFrmParent.TxtPlatNo.EditValue = Sm.GetGrdStr(Grd1, Row, 10);
                    }
                    else
                    {
                        mFrmParent.TxtPLDocNo.EditValue = DocNo;
                        mFrmParent.mCtCode = Sm.GetGrdStr(Grd1, Row, 4);
                        mFrmParent.TxtCtCode.EditValue = Sm.GetGrdStr(Grd1, Row, 5);
                        mFrmParent.TxtCnt.EditValue = Sm.GetGrdStr(Grd1, Row, 6);
                        mFrmParent.TxtSeal.EditValue = Sm.GetGrdStr(Grd1, Row, 7);
                        mFrmParent.mSectionNo = Sm.GetGrdStr(Grd1, Row, 8);
                        mFrmParent.TxtPort.EditValue = Sm.GetGrdStr(Grd1, Row, 9);
                    }
                    mFrmParent.ShowDataPLDR();
                    this.Close();
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                if (mDocType == 1)
                {
                    var f = new FrmDR(mFrmParent.mMenuCode);
                    f.Tag = mFrmParent.mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                    f.ShowDialog();
                }
                else
                {
                    var f = new FrmPL(mFrmParent.mMenuCode);
                    f.Tag = mFrmParent.mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                    f.ShowDialog();
                }
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                if (mDocType == 1)
                {
                    var f = new FrmDR(mFrmParent.mMenuCode);
                    f.Tag = mFrmParent.mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                    f.ShowDialog();
                }
                else
                {
                    var f = new FrmPL(mFrmParent.mMenuCode);
                    f.Tag = mFrmParent.mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                    f.ShowDialog();
                }
            }
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");
        }

        #endregion

        #endregion

    }
}
