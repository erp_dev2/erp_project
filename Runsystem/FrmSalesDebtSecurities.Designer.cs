﻿namespace RunSystem
{
    partial class FrmSalesDebtSecurities
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmSalesDebtSecurities));
            this.TxtInvestmentBankAcc = new DevExpress.XtraEditors.TextEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.BtnInvestmentBankAcc = new DevExpress.XtraEditors.SimpleButton();
            this.ChkCancelInd = new DevExpress.XtraEditors.CheckEdit();
            this.TxtStatus = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.TcSalesDebt1 = new System.Windows.Forms.TabControl();
            this.TpgInvestmentDetails = new System.Windows.Forms.TabPage();
            this.TxtGainSale = new DevExpress.XtraEditors.TextEdit();
            this.label33 = new System.Windows.Forms.Label();
            this.DteMaturityDt = new DevExpress.XtraEditors.DateEdit();
            this.LueCurCode = new DevExpress.XtraEditors.LookUpEdit();
            this.LueAnnualDaysAssumption = new DevExpress.XtraEditors.LookUpEdit();
            this.DteNextCouponDt = new DevExpress.XtraEditors.DateEdit();
            this.DteLastCouponDt = new DevExpress.XtraEditors.DateEdit();
            this.BtnInvestmentCode = new DevExpress.XtraEditors.SimpleButton();
            this.TxtCouponInterestRt = new DevExpress.XtraEditors.TextEdit();
            this.TxtAccruedInterest = new DevExpress.XtraEditors.TextEdit();
            this.DteSettlementDt = new DevExpress.XtraEditors.DateEdit();
            this.DteTradeDt = new DevExpress.XtraEditors.DateEdit();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.TxtTotalSalesAmt = new DevExpress.XtraEditors.TextEdit();
            this.TxtTotalExpenses = new DevExpress.XtraEditors.TextEdit();
            this.TxtCurRate = new DevExpress.XtraEditors.TextEdit();
            this.label32 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.TxtInterestFrequency = new DevExpress.XtraEditors.TextEdit();
            this.TxtSalesAmt = new DevExpress.XtraEditors.TextEdit();
            this.TxtMarketValue = new DevExpress.XtraEditors.TextEdit();
            this.TxtMarketPrice = new DevExpress.XtraEditors.TextEdit();
            this.TxtInvestmentCost = new DevExpress.XtraEditors.TextEdit();
            this.TxtSalesPrice = new DevExpress.XtraEditors.TextEdit();
            this.TxtInvestmentName = new DevExpress.XtraEditors.TextEdit();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.TcSalesDebt2 = new System.Windows.Forms.TabControl();
            this.TpgExpenses = new System.Windows.Forms.TabPage();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.DteUsageDt = new DevExpress.XtraEditors.DateEdit();
            this.Grd1 = new TenTec.Windows.iGridLib.iGrid();
            this.panel10 = new System.Windows.Forms.Panel();
            this.BtnRefreshData = new DevExpress.XtraEditors.SimpleButton();
            this.TpgApproval = new System.Windows.Forms.TabPage();
            this.Grd2 = new TenTec.Windows.iGridLib.iGrid();
            this.label11 = new System.Windows.Forms.Label();
            this.TxtNominalAmt = new DevExpress.XtraEditors.TextEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.TxtMovingAveragePrice = new DevExpress.XtraEditors.TextEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.TxtInvestmentCode = new DevExpress.XtraEditors.TextEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.LueInvestmentCtCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.LueInvestmentType = new DevExpress.XtraEditors.LookUpEdit();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.LueFinancialInstitutionCode = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtVoucher = new DevExpress.XtraEditors.TextEdit();
            this.MeeCancelReason = new DevExpress.XtraEditors.MemoExEdit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInvestmentBankAcc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            this.TcSalesDebt1.SuspendLayout();
            this.TpgInvestmentDetails.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGainSale.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteMaturityDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteMaturityDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueAnnualDaysAssumption.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteNextCouponDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteNextCouponDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteLastCouponDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteLastCouponDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCouponInterestRt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAccruedInterest.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteSettlementDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteSettlementDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTradeDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTradeDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalSalesAmt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalExpenses.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCurRate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInterestFrequency.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSalesAmt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMarketValue.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMarketPrice.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInvestmentCost.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSalesPrice.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInvestmentName.Properties)).BeginInit();
            this.TcSalesDebt2.SuspendLayout();
            this.TpgExpenses.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DteUsageDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteUsageDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            this.panel10.SuspendLayout();
            this.TpgApproval.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtNominalAmt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMovingAveragePrice.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInvestmentCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueInvestmentCtCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueInvestmentType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueFinancialInstitutionCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVoucher.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCancelReason.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(854, 0);
            this.panel1.Size = new System.Drawing.Size(70, 623);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnEdit.Click += new System.EventHandler(this.BtnEdit_Click);
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnInsert.Click += new System.EventHandler(this.BtnInsert_Click);
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnFind.Click += new System.EventHandler(this.BtnFind_Click);
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.MeeCancelReason);
            this.panel2.Controls.Add(this.TxtVoucher);
            this.panel2.Controls.Add(this.LueFinancialInstitutionCode);
            this.panel2.Controls.Add(this.label14);
            this.panel2.Controls.Add(this.label13);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.DteDocDt);
            this.panel2.Controls.Add(this.TcSalesDebt1);
            this.panel2.Controls.Add(this.TxtInvestmentBankAcc);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.BtnInvestmentBankAcc);
            this.panel2.Controls.Add(this.ChkCancelInd);
            this.panel2.Controls.Add(this.TxtStatus);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.TxtDocNo);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Size = new System.Drawing.Size(854, 623);
            // 
            // TxtInvestmentBankAcc
            // 
            this.TxtInvestmentBankAcc.EnterMoveNextControl = true;
            this.TxtInvestmentBankAcc.Location = new System.Drawing.Point(195, 86);
            this.TxtInvestmentBankAcc.Margin = new System.Windows.Forms.Padding(5);
            this.TxtInvestmentBankAcc.Name = "TxtInvestmentBankAcc";
            this.TxtInvestmentBankAcc.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtInvestmentBankAcc.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtInvestmentBankAcc.Properties.Appearance.Options.UseBackColor = true;
            this.TxtInvestmentBankAcc.Properties.Appearance.Options.UseFont = true;
            this.TxtInvestmentBankAcc.Properties.MaxLength = 16;
            this.TxtInvestmentBankAcc.Properties.ReadOnly = true;
            this.TxtInvestmentBankAcc.Size = new System.Drawing.Size(256, 20);
            this.TxtInvestmentBankAcc.TabIndex = 19;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(98, 26);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(94, 14);
            this.label5.TabIndex = 11;
            this.label5.Text = "Document Date";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnInvestmentBankAcc
            // 
            this.BtnInvestmentBankAcc.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnInvestmentBankAcc.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInvestmentBankAcc.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInvestmentBankAcc.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInvestmentBankAcc.Appearance.Options.UseBackColor = true;
            this.BtnInvestmentBankAcc.Appearance.Options.UseFont = true;
            this.BtnInvestmentBankAcc.Appearance.Options.UseForeColor = true;
            this.BtnInvestmentBankAcc.Appearance.Options.UseTextOptions = true;
            this.BtnInvestmentBankAcc.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnInvestmentBankAcc.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnInvestmentBankAcc.Image = ((System.Drawing.Image)(resources.GetObject("BtnInvestmentBankAcc.Image")));
            this.BtnInvestmentBankAcc.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnInvestmentBankAcc.Location = new System.Drawing.Point(454, 85);
            this.BtnInvestmentBankAcc.Name = "BtnInvestmentBankAcc";
            this.BtnInvestmentBankAcc.Size = new System.Drawing.Size(24, 21);
            this.BtnInvestmentBankAcc.TabIndex = 20;
            this.BtnInvestmentBankAcc.ToolTip = "Find Item";
            this.BtnInvestmentBankAcc.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnInvestmentBankAcc.ToolTipTitle = "Run System";
            this.BtnInvestmentBankAcc.Click += new System.EventHandler(this.BtnInvestmentBankAcc_Click);
            // 
            // ChkCancelInd
            // 
            this.ChkCancelInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkCancelInd.Location = new System.Drawing.Point(458, 44);
            this.ChkCancelInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkCancelInd.Name = "ChkCancelInd";
            this.ChkCancelInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkCancelInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkCancelInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkCancelInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseFont = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkCancelInd.Properties.Caption = "Cancel";
            this.ChkCancelInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkCancelInd.Size = new System.Drawing.Size(76, 22);
            this.ChkCancelInd.TabIndex = 15;
            this.ChkCancelInd.CheckedChanged += new System.EventHandler(this.ChkCancelInd_CheckedChanged);
            // 
            // TxtStatus
            // 
            this.TxtStatus.EnterMoveNextControl = true;
            this.TxtStatus.Location = new System.Drawing.Point(195, 107);
            this.TxtStatus.Margin = new System.Windows.Forms.Padding(5);
            this.TxtStatus.Name = "TxtStatus";
            this.TxtStatus.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtStatus.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtStatus.Properties.Appearance.Options.UseBackColor = true;
            this.TxtStatus.Properties.Appearance.Options.UseFont = true;
            this.TxtStatus.Properties.MaxLength = 250;
            this.TxtStatus.Size = new System.Drawing.Size(256, 20);
            this.TxtStatus.TabIndex = 22;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(81, 68);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(111, 14);
            this.label3.TabIndex = 16;
            this.label3.Text = "Financial Institution";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(57, 47);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(135, 14);
            this.label2.TabIndex = 13;
            this.label2.Text = "Reason For Cancellation";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(195, 2);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 16;
            this.TxtDocNo.Properties.ReadOnly = true;
            this.TxtDocNo.Size = new System.Drawing.Size(256, 20);
            this.TxtDocNo.TabIndex = 10;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(119, 5);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 14);
            this.label1.TabIndex = 9;
            this.label1.Text = "Document#";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TcSalesDebt1
            // 
            this.TcSalesDebt1.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.TcSalesDebt1.Controls.Add(this.TpgInvestmentDetails);
            this.TcSalesDebt1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.TcSalesDebt1.Location = new System.Drawing.Point(0, 151);
            this.TcSalesDebt1.Multiline = true;
            this.TcSalesDebt1.Name = "TcSalesDebt1";
            this.TcSalesDebt1.SelectedIndex = 0;
            this.TcSalesDebt1.ShowToolTips = true;
            this.TcSalesDebt1.Size = new System.Drawing.Size(854, 472);
            this.TcSalesDebt1.TabIndex = 32;
            // 
            // TpgInvestmentDetails
            // 
            this.TpgInvestmentDetails.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.TpgInvestmentDetails.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.TpgInvestmentDetails.Controls.Add(this.TxtGainSale);
            this.TpgInvestmentDetails.Controls.Add(this.label33);
            this.TpgInvestmentDetails.Controls.Add(this.DteMaturityDt);
            this.TpgInvestmentDetails.Controls.Add(this.LueCurCode);
            this.TpgInvestmentDetails.Controls.Add(this.LueAnnualDaysAssumption);
            this.TpgInvestmentDetails.Controls.Add(this.DteNextCouponDt);
            this.TpgInvestmentDetails.Controls.Add(this.DteLastCouponDt);
            this.TpgInvestmentDetails.Controls.Add(this.BtnInvestmentCode);
            this.TpgInvestmentDetails.Controls.Add(this.TxtCouponInterestRt);
            this.TpgInvestmentDetails.Controls.Add(this.TxtAccruedInterest);
            this.TpgInvestmentDetails.Controls.Add(this.DteSettlementDt);
            this.TpgInvestmentDetails.Controls.Add(this.DteTradeDt);
            this.TpgInvestmentDetails.Controls.Add(this.MeeRemark);
            this.TpgInvestmentDetails.Controls.Add(this.TxtTotalSalesAmt);
            this.TpgInvestmentDetails.Controls.Add(this.TxtTotalExpenses);
            this.TpgInvestmentDetails.Controls.Add(this.TxtCurRate);
            this.TpgInvestmentDetails.Controls.Add(this.label32);
            this.TpgInvestmentDetails.Controls.Add(this.label31);
            this.TpgInvestmentDetails.Controls.Add(this.label30);
            this.TpgInvestmentDetails.Controls.Add(this.label29);
            this.TpgInvestmentDetails.Controls.Add(this.label28);
            this.TpgInvestmentDetails.Controls.Add(this.label27);
            this.TpgInvestmentDetails.Controls.Add(this.label26);
            this.TpgInvestmentDetails.Controls.Add(this.label25);
            this.TpgInvestmentDetails.Controls.Add(this.label24);
            this.TpgInvestmentDetails.Controls.Add(this.label22);
            this.TpgInvestmentDetails.Controls.Add(this.label21);
            this.TpgInvestmentDetails.Controls.Add(this.label20);
            this.TpgInvestmentDetails.Controls.Add(this.label19);
            this.TpgInvestmentDetails.Controls.Add(this.TxtInterestFrequency);
            this.TpgInvestmentDetails.Controls.Add(this.TxtSalesAmt);
            this.TpgInvestmentDetails.Controls.Add(this.TxtMarketValue);
            this.TpgInvestmentDetails.Controls.Add(this.TxtMarketPrice);
            this.TpgInvestmentDetails.Controls.Add(this.TxtInvestmentCost);
            this.TpgInvestmentDetails.Controls.Add(this.TxtSalesPrice);
            this.TpgInvestmentDetails.Controls.Add(this.TxtInvestmentName);
            this.TpgInvestmentDetails.Controls.Add(this.label18);
            this.TpgInvestmentDetails.Controls.Add(this.label17);
            this.TpgInvestmentDetails.Controls.Add(this.label16);
            this.TpgInvestmentDetails.Controls.Add(this.label15);
            this.TpgInvestmentDetails.Controls.Add(this.TcSalesDebt2);
            this.TpgInvestmentDetails.Controls.Add(this.label11);
            this.TpgInvestmentDetails.Controls.Add(this.TxtNominalAmt);
            this.TpgInvestmentDetails.Controls.Add(this.label10);
            this.TpgInvestmentDetails.Controls.Add(this.TxtMovingAveragePrice);
            this.TpgInvestmentDetails.Controls.Add(this.label9);
            this.TpgInvestmentDetails.Controls.Add(this.TxtInvestmentCode);
            this.TpgInvestmentDetails.Controls.Add(this.label7);
            this.TpgInvestmentDetails.Controls.Add(this.label6);
            this.TpgInvestmentDetails.Controls.Add(this.label23);
            this.TpgInvestmentDetails.Controls.Add(this.LueInvestmentCtCode);
            this.TpgInvestmentDetails.Controls.Add(this.label8);
            this.TpgInvestmentDetails.Controls.Add(this.label4);
            this.TpgInvestmentDetails.Controls.Add(this.LueInvestmentType);
            this.TpgInvestmentDetails.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TpgInvestmentDetails.Location = new System.Drawing.Point(4, 26);
            this.TpgInvestmentDetails.Name = "TpgInvestmentDetails";
            this.TpgInvestmentDetails.Size = new System.Drawing.Size(846, 442);
            this.TpgInvestmentDetails.TabIndex = 0;
            this.TpgInvestmentDetails.Text = "Investment Details";
            this.TpgInvestmentDetails.UseVisualStyleBackColor = true;
            // 
            // TxtGainSale
            // 
            this.TxtGainSale.EnterMoveNextControl = true;
            this.TxtGainSale.Location = new System.Drawing.Point(140, 234);
            this.TxtGainSale.Margin = new System.Windows.Forms.Padding(5);
            this.TxtGainSale.Name = "TxtGainSale";
            this.TxtGainSale.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtGainSale.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtGainSale.Properties.Appearance.Options.UseBackColor = true;
            this.TxtGainSale.Properties.Appearance.Options.UseFont = true;
            this.TxtGainSale.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtGainSale.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtGainSale.Properties.MaxLength = 16;
            this.TxtGainSale.Properties.ReadOnly = true;
            this.TxtGainSale.Size = new System.Drawing.Size(230, 20);
            this.TxtGainSale.TabIndex = 49;
            this.TxtGainSale.Validated += new System.EventHandler(this.TxtGainSale_Validated);
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.Black;
            this.label33.Location = new System.Drawing.Point(63, 237);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(74, 14);
            this.label33.TabIndex = 48;
            this.label33.Text = "Gain on Sale";
            // 
            // DteMaturityDt
            // 
            this.DteMaturityDt.EditValue = null;
            this.DteMaturityDt.EnterMoveNextControl = true;
            this.DteMaturityDt.Location = new System.Drawing.Point(610, 87);
            this.DteMaturityDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteMaturityDt.Name = "DteMaturityDt";
            this.DteMaturityDt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.DteMaturityDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteMaturityDt.Properties.Appearance.Options.UseBackColor = true;
            this.DteMaturityDt.Properties.Appearance.Options.UseFont = true;
            this.DteMaturityDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteMaturityDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteMaturityDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteMaturityDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteMaturityDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteMaturityDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteMaturityDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteMaturityDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteMaturityDt.Properties.MaxLength = 8;
            this.DteMaturityDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteMaturityDt.Size = new System.Drawing.Size(116, 20);
            this.DteMaturityDt.TabIndex = 61;
            // 
            // LueCurCode
            // 
            this.LueCurCode.EnterMoveNextControl = true;
            this.LueCurCode.Location = new System.Drawing.Point(610, 150);
            this.LueCurCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueCurCode.Name = "LueCurCode";
            this.LueCurCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.LueCurCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.Appearance.Options.UseBackColor = true;
            this.LueCurCode.Properties.Appearance.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCurCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCurCode.Properties.DropDownRows = 30;
            this.LueCurCode.Properties.NullText = "[Empty]";
            this.LueCurCode.Properties.PopupWidth = 350;
            this.LueCurCode.Size = new System.Drawing.Size(67, 20);
            this.LueCurCode.TabIndex = 67;
            this.LueCurCode.ToolTip = "F4 : Show/hide list";
            this.LueCurCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // LueAnnualDaysAssumption
            // 
            this.LueAnnualDaysAssumption.EnterMoveNextControl = true;
            this.LueAnnualDaysAssumption.Location = new System.Drawing.Point(610, 66);
            this.LueAnnualDaysAssumption.Margin = new System.Windows.Forms.Padding(5);
            this.LueAnnualDaysAssumption.Name = "LueAnnualDaysAssumption";
            this.LueAnnualDaysAssumption.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.LueAnnualDaysAssumption.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAnnualDaysAssumption.Properties.Appearance.Options.UseBackColor = true;
            this.LueAnnualDaysAssumption.Properties.Appearance.Options.UseFont = true;
            this.LueAnnualDaysAssumption.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAnnualDaysAssumption.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueAnnualDaysAssumption.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAnnualDaysAssumption.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueAnnualDaysAssumption.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAnnualDaysAssumption.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueAnnualDaysAssumption.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAnnualDaysAssumption.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueAnnualDaysAssumption.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueAnnualDaysAssumption.Properties.DropDownRows = 30;
            this.LueAnnualDaysAssumption.Properties.NullText = "[Empty]";
            this.LueAnnualDaysAssumption.Properties.PopupWidth = 350;
            this.LueAnnualDaysAssumption.Size = new System.Drawing.Size(67, 20);
            this.LueAnnualDaysAssumption.TabIndex = 59;
            this.LueAnnualDaysAssumption.ToolTip = "F4 : Show/hide list";
            this.LueAnnualDaysAssumption.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueAnnualDaysAssumption.EditValueChanged += new System.EventHandler(this.LueAnnualDays_EditValueChanged);
            // 
            // DteNextCouponDt
            // 
            this.DteNextCouponDt.EditValue = null;
            this.DteNextCouponDt.EnterMoveNextControl = true;
            this.DteNextCouponDt.Location = new System.Drawing.Point(610, 129);
            this.DteNextCouponDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteNextCouponDt.Name = "DteNextCouponDt";
            this.DteNextCouponDt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.DteNextCouponDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteNextCouponDt.Properties.Appearance.Options.UseBackColor = true;
            this.DteNextCouponDt.Properties.Appearance.Options.UseFont = true;
            this.DteNextCouponDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteNextCouponDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteNextCouponDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteNextCouponDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteNextCouponDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteNextCouponDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteNextCouponDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteNextCouponDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteNextCouponDt.Properties.MaxLength = 8;
            this.DteNextCouponDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteNextCouponDt.Size = new System.Drawing.Size(116, 20);
            this.DteNextCouponDt.TabIndex = 65;
            // 
            // DteLastCouponDt
            // 
            this.DteLastCouponDt.EditValue = null;
            this.DteLastCouponDt.EnterMoveNextControl = true;
            this.DteLastCouponDt.Location = new System.Drawing.Point(610, 108);
            this.DteLastCouponDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteLastCouponDt.Name = "DteLastCouponDt";
            this.DteLastCouponDt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.DteLastCouponDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteLastCouponDt.Properties.Appearance.Options.UseBackColor = true;
            this.DteLastCouponDt.Properties.Appearance.Options.UseFont = true;
            this.DteLastCouponDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteLastCouponDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteLastCouponDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteLastCouponDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteLastCouponDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteLastCouponDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteLastCouponDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteLastCouponDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteLastCouponDt.Properties.MaxLength = 8;
            this.DteLastCouponDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteLastCouponDt.Size = new System.Drawing.Size(116, 20);
            this.DteLastCouponDt.TabIndex = 63;
            // 
            // BtnInvestmentCode
            // 
            this.BtnInvestmentCode.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnInvestmentCode.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInvestmentCode.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInvestmentCode.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInvestmentCode.Appearance.Options.UseBackColor = true;
            this.BtnInvestmentCode.Appearance.Options.UseFont = true;
            this.BtnInvestmentCode.Appearance.Options.UseForeColor = true;
            this.BtnInvestmentCode.Appearance.Options.UseTextOptions = true;
            this.BtnInvestmentCode.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnInvestmentCode.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnInvestmentCode.Image = ((System.Drawing.Image)(resources.GetObject("BtnInvestmentCode.Image")));
            this.BtnInvestmentCode.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnInvestmentCode.Location = new System.Drawing.Point(375, 3);
            this.BtnInvestmentCode.Name = "BtnInvestmentCode";
            this.BtnInvestmentCode.Size = new System.Drawing.Size(24, 21);
            this.BtnInvestmentCode.TabIndex = 27;
            this.BtnInvestmentCode.ToolTip = "Find Item";
            this.BtnInvestmentCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnInvestmentCode.ToolTipTitle = "Run System";
            this.BtnInvestmentCode.Click += new System.EventHandler(this.BtnInvestmentCode_Click);
            // 
            // TxtCouponInterestRt
            // 
            this.TxtCouponInterestRt.EnterMoveNextControl = true;
            this.TxtCouponInterestRt.Location = new System.Drawing.Point(610, 45);
            this.TxtCouponInterestRt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCouponInterestRt.Name = "TxtCouponInterestRt";
            this.TxtCouponInterestRt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtCouponInterestRt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCouponInterestRt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCouponInterestRt.Properties.Appearance.Options.UseFont = true;
            this.TxtCouponInterestRt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtCouponInterestRt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtCouponInterestRt.Properties.MaxLength = 16;
            this.TxtCouponInterestRt.Properties.ReadOnly = true;
            this.TxtCouponInterestRt.Size = new System.Drawing.Size(230, 20);
            this.TxtCouponInterestRt.TabIndex = 57;
            // 
            // TxtAccruedInterest
            // 
            this.TxtAccruedInterest.EnterMoveNextControl = true;
            this.TxtAccruedInterest.Location = new System.Drawing.Point(610, 192);
            this.TxtAccruedInterest.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAccruedInterest.Name = "TxtAccruedInterest";
            this.TxtAccruedInterest.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAccruedInterest.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAccruedInterest.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAccruedInterest.Properties.Appearance.Options.UseFont = true;
            this.TxtAccruedInterest.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtAccruedInterest.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtAccruedInterest.Properties.MaxLength = 16;
            this.TxtAccruedInterest.Properties.ReadOnly = true;
            this.TxtAccruedInterest.Size = new System.Drawing.Size(230, 20);
            this.TxtAccruedInterest.TabIndex = 71;
            // 
            // DteSettlementDt
            // 
            this.DteSettlementDt.EditValue = null;
            this.DteSettlementDt.EnterMoveNextControl = true;
            this.DteSettlementDt.Location = new System.Drawing.Point(610, 24);
            this.DteSettlementDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteSettlementDt.Name = "DteSettlementDt";
            this.DteSettlementDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteSettlementDt.Properties.Appearance.Options.UseFont = true;
            this.DteSettlementDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteSettlementDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteSettlementDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteSettlementDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteSettlementDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteSettlementDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteSettlementDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteSettlementDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteSettlementDt.Properties.MaxLength = 8;
            this.DteSettlementDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteSettlementDt.Size = new System.Drawing.Size(116, 20);
            this.DteSettlementDt.TabIndex = 55;
            this.DteSettlementDt.EditValueChanged += new System.EventHandler(this.DteSettlementDt_EditValueChanged);
            // 
            // DteTradeDt
            // 
            this.DteTradeDt.EditValue = null;
            this.DteTradeDt.EnterMoveNextControl = true;
            this.DteTradeDt.Location = new System.Drawing.Point(610, 3);
            this.DteTradeDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteTradeDt.Name = "DteTradeDt";
            this.DteTradeDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTradeDt.Properties.Appearance.Options.UseFont = true;
            this.DteTradeDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTradeDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteTradeDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteTradeDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteTradeDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteTradeDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteTradeDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteTradeDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteTradeDt.Properties.MaxLength = 8;
            this.DteTradeDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteTradeDt.Size = new System.Drawing.Size(116, 20);
            this.DteTradeDt.TabIndex = 53;
            this.DteTradeDt.EditValueChanged += new System.EventHandler(this.DteTradeDt_EditValueChanged);
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(610, 255);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 450;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(230, 20);
            this.MeeRemark.TabIndex = 77;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // TxtTotalSalesAmt
            // 
            this.TxtTotalSalesAmt.EnterMoveNextControl = true;
            this.TxtTotalSalesAmt.Location = new System.Drawing.Point(610, 234);
            this.TxtTotalSalesAmt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotalSalesAmt.Name = "TxtTotalSalesAmt";
            this.TxtTotalSalesAmt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtTotalSalesAmt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotalSalesAmt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotalSalesAmt.Properties.Appearance.Options.UseFont = true;
            this.TxtTotalSalesAmt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotalSalesAmt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotalSalesAmt.Properties.MaxLength = 16;
            this.TxtTotalSalesAmt.Properties.ReadOnly = true;
            this.TxtTotalSalesAmt.Size = new System.Drawing.Size(230, 20);
            this.TxtTotalSalesAmt.TabIndex = 75;
            // 
            // TxtTotalExpenses
            // 
            this.TxtTotalExpenses.EnterMoveNextControl = true;
            this.TxtTotalExpenses.Location = new System.Drawing.Point(610, 213);
            this.TxtTotalExpenses.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotalExpenses.Name = "TxtTotalExpenses";
            this.TxtTotalExpenses.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtTotalExpenses.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotalExpenses.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotalExpenses.Properties.Appearance.Options.UseFont = true;
            this.TxtTotalExpenses.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotalExpenses.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotalExpenses.Properties.MaxLength = 16;
            this.TxtTotalExpenses.Properties.ReadOnly = true;
            this.TxtTotalExpenses.Size = new System.Drawing.Size(230, 20);
            this.TxtTotalExpenses.TabIndex = 73;
            // 
            // TxtCurRate
            // 
            this.TxtCurRate.EnterMoveNextControl = true;
            this.TxtCurRate.Location = new System.Drawing.Point(610, 171);
            this.TxtCurRate.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCurRate.Name = "TxtCurRate";
            this.TxtCurRate.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtCurRate.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCurRate.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCurRate.Properties.Appearance.Options.UseFont = true;
            this.TxtCurRate.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtCurRate.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtCurRate.Properties.MaxLength = 16;
            this.TxtCurRate.Properties.ReadOnly = true;
            this.TxtCurRate.Size = new System.Drawing.Size(230, 20);
            this.TxtCurRate.TabIndex = 69;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.Black;
            this.label32.Location = new System.Drawing.Point(560, 258);
            this.label32.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(47, 14);
            this.label32.TabIndex = 76;
            this.label32.Text = "Remark";
            this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.Black;
            this.label31.Location = new System.Drawing.Point(493, 237);
            this.label31.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(114, 14);
            this.label31.TabIndex = 74;
            this.label31.Text = "Total Sales Amount";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.Black;
            this.label30.Location = new System.Drawing.Point(423, 216);
            this.label30.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(184, 14);
            this.label30.TabIndex = 72;
            this.label30.Text = "Total Expenses and Other Detail";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Black;
            this.label29.Location = new System.Drawing.Point(507, 195);
            this.label29.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(100, 14);
            this.label29.TabIndex = 70;
            this.label29.Text = "Accrued Interest";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Black;
            this.label28.Location = new System.Drawing.Point(523, 174);
            this.label28.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(84, 14);
            this.label28.TabIndex = 68;
            this.label28.Text = "Currency Rate";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Black;
            this.label27.Location = new System.Drawing.Point(552, 153);
            this.label27.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(55, 14);
            this.label27.TabIndex = 66;
            this.label27.Text = "Currency";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Black;
            this.label26.Location = new System.Drawing.Point(498, 132);
            this.label26.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(109, 14);
            this.label26.TabIndex = 64;
            this.label26.Text = "Next Coupon Date";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Black;
            this.label25.Location = new System.Drawing.Point(502, 111);
            this.label25.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(105, 14);
            this.label25.TabIndex = 62;
            this.label25.Text = "Last Coupon Date";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Black;
            this.label24.Location = new System.Drawing.Point(526, 90);
            this.label24.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(81, 14);
            this.label24.TabIndex = 60;
            this.label24.Text = "Maturity Date";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(467, 69);
            this.label22.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(140, 14);
            this.label22.TabIndex = 58;
            this.label22.Text = "Annual Days Assumption";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Black;
            this.label21.Location = new System.Drawing.Point(481, 48);
            this.label21.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(126, 14);
            this.label21.TabIndex = 56;
            this.label21.Text = "Coupon Interest Rate";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Red;
            this.label20.Location = new System.Drawing.Point(508, 27);
            this.label20.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(99, 14);
            this.label20.TabIndex = 54;
            this.label20.Text = "Settlement Date";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Red;
            this.label19.Location = new System.Drawing.Point(538, 6);
            this.label19.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(69, 14);
            this.label19.TabIndex = 52;
            this.label19.Text = "Trade Date";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtInterestFrequency
            // 
            this.TxtInterestFrequency.EnterMoveNextControl = true;
            this.TxtInterestFrequency.Location = new System.Drawing.Point(140, 255);
            this.TxtInterestFrequency.Margin = new System.Windows.Forms.Padding(5);
            this.TxtInterestFrequency.Name = "TxtInterestFrequency";
            this.TxtInterestFrequency.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtInterestFrequency.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtInterestFrequency.Properties.Appearance.Options.UseBackColor = true;
            this.TxtInterestFrequency.Properties.Appearance.Options.UseFont = true;
            this.TxtInterestFrequency.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtInterestFrequency.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtInterestFrequency.Properties.MaxLength = 16;
            this.TxtInterestFrequency.Properties.ReadOnly = true;
            this.TxtInterestFrequency.Size = new System.Drawing.Size(230, 20);
            this.TxtInterestFrequency.TabIndex = 51;
            // 
            // TxtSalesAmt
            // 
            this.TxtSalesAmt.EnterMoveNextControl = true;
            this.TxtSalesAmt.Location = new System.Drawing.Point(140, 213);
            this.TxtSalesAmt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSalesAmt.Name = "TxtSalesAmt";
            this.TxtSalesAmt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtSalesAmt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSalesAmt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSalesAmt.Properties.Appearance.Options.UseFont = true;
            this.TxtSalesAmt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtSalesAmt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtSalesAmt.Properties.MaxLength = 16;
            this.TxtSalesAmt.Properties.ReadOnly = true;
            this.TxtSalesAmt.Size = new System.Drawing.Size(230, 20);
            this.TxtSalesAmt.TabIndex = 47;
            // 
            // TxtMarketValue
            // 
            this.TxtMarketValue.EnterMoveNextControl = true;
            this.TxtMarketValue.Location = new System.Drawing.Point(140, 171);
            this.TxtMarketValue.Margin = new System.Windows.Forms.Padding(5);
            this.TxtMarketValue.Name = "TxtMarketValue";
            this.TxtMarketValue.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtMarketValue.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtMarketValue.Properties.Appearance.Options.UseBackColor = true;
            this.TxtMarketValue.Properties.Appearance.Options.UseFont = true;
            this.TxtMarketValue.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtMarketValue.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtMarketValue.Properties.MaxLength = 16;
            this.TxtMarketValue.Properties.ReadOnly = true;
            this.TxtMarketValue.Size = new System.Drawing.Size(230, 20);
            this.TxtMarketValue.TabIndex = 43;
            // 
            // TxtMarketPrice
            // 
            this.TxtMarketPrice.EnterMoveNextControl = true;
            this.TxtMarketPrice.Location = new System.Drawing.Point(140, 150);
            this.TxtMarketPrice.Margin = new System.Windows.Forms.Padding(5);
            this.TxtMarketPrice.Name = "TxtMarketPrice";
            this.TxtMarketPrice.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtMarketPrice.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtMarketPrice.Properties.Appearance.Options.UseBackColor = true;
            this.TxtMarketPrice.Properties.Appearance.Options.UseFont = true;
            this.TxtMarketPrice.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtMarketPrice.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtMarketPrice.Properties.MaxLength = 16;
            this.TxtMarketPrice.Properties.ReadOnly = true;
            this.TxtMarketPrice.Size = new System.Drawing.Size(230, 20);
            this.TxtMarketPrice.TabIndex = 41;
            // 
            // TxtInvestmentCost
            // 
            this.TxtInvestmentCost.EnterMoveNextControl = true;
            this.TxtInvestmentCost.Location = new System.Drawing.Point(140, 129);
            this.TxtInvestmentCost.Margin = new System.Windows.Forms.Padding(5);
            this.TxtInvestmentCost.Name = "TxtInvestmentCost";
            this.TxtInvestmentCost.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtInvestmentCost.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtInvestmentCost.Properties.Appearance.Options.UseBackColor = true;
            this.TxtInvestmentCost.Properties.Appearance.Options.UseFont = true;
            this.TxtInvestmentCost.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtInvestmentCost.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtInvestmentCost.Properties.MaxLength = 16;
            this.TxtInvestmentCost.Properties.ReadOnly = true;
            this.TxtInvestmentCost.Size = new System.Drawing.Size(230, 20);
            this.TxtInvestmentCost.TabIndex = 39;
            // 
            // TxtSalesPrice
            // 
            this.TxtSalesPrice.EnterMoveNextControl = true;
            this.TxtSalesPrice.Location = new System.Drawing.Point(140, 192);
            this.TxtSalesPrice.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSalesPrice.Name = "TxtSalesPrice";
            this.TxtSalesPrice.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSalesPrice.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSalesPrice.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSalesPrice.Properties.Appearance.Options.UseFont = true;
            this.TxtSalesPrice.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtSalesPrice.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtSalesPrice.Properties.MaxLength = 250;
            this.TxtSalesPrice.Size = new System.Drawing.Size(230, 20);
            this.TxtSalesPrice.TabIndex = 45;
            this.TxtSalesPrice.Validated += new System.EventHandler(this.TxtSalesPrice_Validated);
            // 
            // TxtInvestmentName
            // 
            this.TxtInvestmentName.EnterMoveNextControl = true;
            this.TxtInvestmentName.Location = new System.Drawing.Point(140, 24);
            this.TxtInvestmentName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtInvestmentName.Name = "TxtInvestmentName";
            this.TxtInvestmentName.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtInvestmentName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtInvestmentName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtInvestmentName.Properties.Appearance.Options.UseFont = true;
            this.TxtInvestmentName.Properties.MaxLength = 16;
            this.TxtInvestmentName.Properties.ReadOnly = true;
            this.TxtInvestmentName.Size = new System.Drawing.Size(230, 20);
            this.TxtInvestmentName.TabIndex = 29;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(24, 258);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(112, 14);
            this.label18.TabIndex = 50;
            this.label18.Text = "Interest Frequency";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(54, 216);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(82, 14);
            this.label17.TabIndex = 46;
            this.label17.Text = "Sales Amount";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Red;
            this.label16.Location = new System.Drawing.Point(72, 195);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(64, 14);
            this.label16.TabIndex = 44;
            this.label16.Text = "Sales Price";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(2, 174);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(134, 14);
            this.label15.TabIndex = 42;
            this.label15.Text = "Recorded Market Value";
            // 
            // TcSalesDebt2
            // 
            this.TcSalesDebt2.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.TcSalesDebt2.Controls.Add(this.TpgExpenses);
            this.TcSalesDebt2.Controls.Add(this.TpgApproval);
            this.TcSalesDebt2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.TcSalesDebt2.Location = new System.Drawing.Point(0, 278);
            this.TcSalesDebt2.Multiline = true;
            this.TcSalesDebt2.Name = "TcSalesDebt2";
            this.TcSalesDebt2.SelectedIndex = 0;
            this.TcSalesDebt2.ShowToolTips = true;
            this.TcSalesDebt2.Size = new System.Drawing.Size(842, 160);
            this.TcSalesDebt2.TabIndex = 47;
            // 
            // TpgExpenses
            // 
            this.TpgExpenses.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.TpgExpenses.Controls.Add(this.panel5);
            this.TpgExpenses.Location = new System.Drawing.Point(4, 26);
            this.TpgExpenses.Name = "TpgExpenses";
            this.TpgExpenses.Padding = new System.Windows.Forms.Padding(3);
            this.TpgExpenses.Size = new System.Drawing.Size(834, 130);
            this.TpgExpenses.TabIndex = 0;
            this.TpgExpenses.Text = "Expenses & Other Detail";
            this.TpgExpenses.UseVisualStyleBackColor = true;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel5.Controls.Add(this.panel8);
            this.panel5.Controls.Add(this.panel10);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(3, 3);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(824, 120);
            this.panel5.TabIndex = 49;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel8.Controls.Add(this.DteUsageDt);
            this.panel8.Controls.Add(this.Grd1);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel8.Location = new System.Drawing.Point(0, 25);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(824, 95);
            this.panel8.TabIndex = 52;
            // 
            // DteUsageDt
            // 
            this.DteUsageDt.EditValue = null;
            this.DteUsageDt.EnterMoveNextControl = true;
            this.DteUsageDt.Location = new System.Drawing.Point(66, 24);
            this.DteUsageDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteUsageDt.Name = "DteUsageDt";
            this.DteUsageDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteUsageDt.Properties.Appearance.Options.UseFont = true;
            this.DteUsageDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteUsageDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteUsageDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteUsageDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteUsageDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteUsageDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteUsageDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteUsageDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteUsageDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteUsageDt.Size = new System.Drawing.Size(125, 20);
            this.DteUsageDt.TabIndex = 54;
            this.DteUsageDt.Visible = false;
            this.DteUsageDt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DteUsageDt_KeyDown);
            this.DteUsageDt.Leave += new System.EventHandler(this.DteUsageDt_Leave);
            // 
            // Grd1
            // 
            this.Grd1.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd1.DefaultRow.Height = 20;
            this.Grd1.DefaultRow.Sortable = false;
            this.Grd1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 21;
            this.Grd1.Location = new System.Drawing.Point(0, 0);
            this.Grd1.Name = "Grd1";
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.RowModeHasCurCell = true;
            this.Grd1.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd1.SingleClickEdit = true;
            this.Grd1.Size = new System.Drawing.Size(824, 95);
            this.Grd1.TabIndex = 52;
            this.Grd1.TreeCol = null;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd1_EllipsisButtonClick);
            this.Grd1.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd1_RequestEdit);
            this.Grd1.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd1_AfterCommitEdit);
            this.Grd1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd1_KeyDown);
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel10.Controls.Add(this.BtnRefreshData);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel10.Location = new System.Drawing.Point(0, 0);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(824, 25);
            this.panel10.TabIndex = 47;
            // 
            // BtnRefreshData
            // 
            this.BtnRefreshData.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnRefreshData.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnRefreshData.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRefreshData.Appearance.ForeColor = System.Drawing.Color.Green;
            this.BtnRefreshData.Appearance.Options.UseBackColor = true;
            this.BtnRefreshData.Appearance.Options.UseFont = true;
            this.BtnRefreshData.Appearance.Options.UseForeColor = true;
            this.BtnRefreshData.Appearance.Options.UseTextOptions = true;
            this.BtnRefreshData.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnRefreshData.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnRefreshData.Dock = System.Windows.Forms.DockStyle.Left;
            this.BtnRefreshData.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnRefreshData.Location = new System.Drawing.Point(0, 0);
            this.BtnRefreshData.Name = "BtnRefreshData";
            this.BtnRefreshData.Size = new System.Drawing.Size(87, 25);
            this.BtnRefreshData.TabIndex = 77;
            this.BtnRefreshData.Text = "Refresh Data";
            this.BtnRefreshData.ToolTip = "Refresh Data";
            this.BtnRefreshData.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnRefreshData.ToolTipTitle = "Run System";
            this.BtnRefreshData.Click += new System.EventHandler(this.BtnRefreshData_Click);
            // 
            // TpgApproval
            // 
            this.TpgApproval.Controls.Add(this.Grd2);
            this.TpgApproval.Location = new System.Drawing.Point(4, 26);
            this.TpgApproval.Name = "TpgApproval";
            this.TpgApproval.Padding = new System.Windows.Forms.Padding(3);
            this.TpgApproval.Size = new System.Drawing.Size(752, 130);
            this.TpgApproval.TabIndex = 1;
            this.TpgApproval.Text = "Approval Information";
            this.TpgApproval.UseVisualStyleBackColor = true;
            // 
            // Grd2
            // 
            this.Grd2.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd2.DefaultRow.Height = 20;
            this.Grd2.DefaultRow.Sortable = false;
            this.Grd2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd2.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd2.Header.Height = 21;
            this.Grd2.Location = new System.Drawing.Point(3, 3);
            this.Grd2.Name = "Grd2";
            this.Grd2.RowHeader.Visible = true;
            this.Grd2.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd2.SingleClickEdit = true;
            this.Grd2.Size = new System.Drawing.Size(746, 124);
            this.Grd2.TabIndex = 115;
            this.Grd2.TreeCol = null;
            this.Grd2.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(6, 153);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(130, 14);
            this.label11.TabIndex = 40;
            this.label11.Text = "Recorded Market Price";
            // 
            // TxtNominalAmt
            // 
            this.TxtNominalAmt.EnterMoveNextControl = true;
            this.TxtNominalAmt.Location = new System.Drawing.Point(140, 87);
            this.TxtNominalAmt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtNominalAmt.Name = "TxtNominalAmt";
            this.TxtNominalAmt.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtNominalAmt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtNominalAmt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtNominalAmt.Properties.Appearance.Options.UseFont = true;
            this.TxtNominalAmt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtNominalAmt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtNominalAmt.Properties.MaxLength = 250;
            this.TxtNominalAmt.Size = new System.Drawing.Size(230, 20);
            this.TxtNominalAmt.TabIndex = 35;
            this.TxtNominalAmt.Validated += new System.EventHandler(this.TxtNominalAmt_Validated);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(12, 111);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(124, 14);
            this.label10.TabIndex = 36;
            this.label10.Text = "Moving Average Price";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtMovingAveragePrice
            // 
            this.TxtMovingAveragePrice.EnterMoveNextControl = true;
            this.TxtMovingAveragePrice.Location = new System.Drawing.Point(140, 108);
            this.TxtMovingAveragePrice.Margin = new System.Windows.Forms.Padding(5);
            this.TxtMovingAveragePrice.Name = "TxtMovingAveragePrice";
            this.TxtMovingAveragePrice.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtMovingAveragePrice.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtMovingAveragePrice.Properties.Appearance.Options.UseBackColor = true;
            this.TxtMovingAveragePrice.Properties.Appearance.Options.UseFont = true;
            this.TxtMovingAveragePrice.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtMovingAveragePrice.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtMovingAveragePrice.Properties.MaxLength = 16;
            this.TxtMovingAveragePrice.Properties.ReadOnly = true;
            this.TxtMovingAveragePrice.Size = new System.Drawing.Size(230, 20);
            this.TxtMovingAveragePrice.TabIndex = 37;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(13, 69);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(123, 14);
            this.label9.TabIndex = 32;
            this.label9.Text = "Investment Category";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtInvestmentCode
            // 
            this.TxtInvestmentCode.EnterMoveNextControl = true;
            this.TxtInvestmentCode.Location = new System.Drawing.Point(140, 3);
            this.TxtInvestmentCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtInvestmentCode.Name = "TxtInvestmentCode";
            this.TxtInvestmentCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtInvestmentCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtInvestmentCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtInvestmentCode.Properties.Appearance.Options.UseFont = true;
            this.TxtInvestmentCode.Properties.MaxLength = 16;
            this.TxtInvestmentCode.Properties.ReadOnly = true;
            this.TxtInvestmentCode.Size = new System.Drawing.Size(230, 20);
            this.TxtInvestmentCode.TabIndex = 26;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(34, 48);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(102, 14);
            this.label7.TabIndex = 30;
            this.label7.Text = "Investment Type";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(31, 27);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(105, 14);
            this.label6.TabIndex = 28;
            this.label6.Text = "Investment Name";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Black;
            this.label23.Location = new System.Drawing.Point(39, 90);
            this.label23.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(97, 14);
            this.label23.TabIndex = 34;
            this.label23.Text = "Nominal Amount";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueInvestmentCtCode
            // 
            this.LueInvestmentCtCode.EnterMoveNextControl = true;
            this.LueInvestmentCtCode.Location = new System.Drawing.Point(140, 66);
            this.LueInvestmentCtCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueInvestmentCtCode.Name = "LueInvestmentCtCode";
            this.LueInvestmentCtCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.LueInvestmentCtCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInvestmentCtCode.Properties.Appearance.Options.UseBackColor = true;
            this.LueInvestmentCtCode.Properties.Appearance.Options.UseFont = true;
            this.LueInvestmentCtCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInvestmentCtCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueInvestmentCtCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInvestmentCtCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueInvestmentCtCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInvestmentCtCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueInvestmentCtCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInvestmentCtCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueInvestmentCtCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueInvestmentCtCode.Properties.DropDownRows = 30;
            this.LueInvestmentCtCode.Properties.NullText = "[Empty]";
            this.LueInvestmentCtCode.Properties.PopupWidth = 200;
            this.LueInvestmentCtCode.Size = new System.Drawing.Size(149, 20);
            this.LueInvestmentCtCode.TabIndex = 33;
            this.LueInvestmentCtCode.ToolTip = "F4 : Show/hide list";
            this.LueInvestmentCtCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(38, 132);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(98, 14);
            this.label8.TabIndex = 38;
            this.label8.Text = "Investment Cost";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(34, 6);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(102, 14);
            this.label4.TabIndex = 25;
            this.label4.Text = "Investment Code";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueInvestmentType
            // 
            this.LueInvestmentType.EnterMoveNextControl = true;
            this.LueInvestmentType.Location = new System.Drawing.Point(140, 45);
            this.LueInvestmentType.Margin = new System.Windows.Forms.Padding(5);
            this.LueInvestmentType.Name = "LueInvestmentType";
            this.LueInvestmentType.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.LueInvestmentType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInvestmentType.Properties.Appearance.Options.UseBackColor = true;
            this.LueInvestmentType.Properties.Appearance.Options.UseFont = true;
            this.LueInvestmentType.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInvestmentType.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueInvestmentType.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInvestmentType.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueInvestmentType.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInvestmentType.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueInvestmentType.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInvestmentType.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueInvestmentType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueInvestmentType.Properties.DropDownRows = 30;
            this.LueInvestmentType.Properties.NullText = "[Empty]";
            this.LueInvestmentType.Properties.PopupWidth = 350;
            this.LueInvestmentType.Size = new System.Drawing.Size(149, 20);
            this.LueInvestmentType.TabIndex = 31;
            this.LueInvestmentType.ToolTip = "F4 : Show/hide list";
            this.LueInvestmentType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(195, 23);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.BackColor = System.Drawing.Color.Silver;
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseBackColor = true;
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(116, 20);
            this.DteDocDt.TabIndex = 12;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Red;
            this.label12.Location = new System.Drawing.Point(5, 89);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(187, 14);
            this.label12.TabIndex = 18;
            this.label12.Text = "Investment Bank Account (RDN)";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(150, 110);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(42, 14);
            this.label13.TabIndex = 21;
            this.label13.Text = "Status";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(139, 131);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(53, 14);
            this.label14.TabIndex = 23;
            this.label14.Text = "Voucher";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueFinancialInstitutionCode
            // 
            this.LueFinancialInstitutionCode.EnterMoveNextControl = true;
            this.LueFinancialInstitutionCode.Location = new System.Drawing.Point(195, 65);
            this.LueFinancialInstitutionCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueFinancialInstitutionCode.Name = "LueFinancialInstitutionCode";
            this.LueFinancialInstitutionCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFinancialInstitutionCode.Properties.Appearance.Options.UseFont = true;
            this.LueFinancialInstitutionCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFinancialInstitutionCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueFinancialInstitutionCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFinancialInstitutionCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueFinancialInstitutionCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFinancialInstitutionCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueFinancialInstitutionCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFinancialInstitutionCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueFinancialInstitutionCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueFinancialInstitutionCode.Properties.DropDownRows = 30;
            this.LueFinancialInstitutionCode.Properties.NullText = "[Empty]";
            this.LueFinancialInstitutionCode.Properties.PopupWidth = 350;
            this.LueFinancialInstitutionCode.Size = new System.Drawing.Size(256, 20);
            this.LueFinancialInstitutionCode.TabIndex = 17;
            this.LueFinancialInstitutionCode.ToolTip = "F4 : Show/hide list";
            this.LueFinancialInstitutionCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // TxtVoucher
            // 
            this.TxtVoucher.EnterMoveNextControl = true;
            this.TxtVoucher.Location = new System.Drawing.Point(195, 128);
            this.TxtVoucher.Margin = new System.Windows.Forms.Padding(5);
            this.TxtVoucher.Name = "TxtVoucher";
            this.TxtVoucher.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtVoucher.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtVoucher.Properties.Appearance.Options.UseBackColor = true;
            this.TxtVoucher.Properties.Appearance.Options.UseFont = true;
            this.TxtVoucher.Properties.MaxLength = 250;
            this.TxtVoucher.Size = new System.Drawing.Size(256, 20);
            this.TxtVoucher.TabIndex = 24;
            // 
            // MeeCancelReason
            // 
            this.MeeCancelReason.EnterMoveNextControl = true;
            this.MeeCancelReason.Location = new System.Drawing.Point(195, 44);
            this.MeeCancelReason.Margin = new System.Windows.Forms.Padding(5);
            this.MeeCancelReason.Name = "MeeCancelReason";
            this.MeeCancelReason.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.Appearance.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeCancelReason.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeCancelReason.Properties.MaxLength = 450;
            this.MeeCancelReason.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeCancelReason.Properties.ShowIcon = false;
            this.MeeCancelReason.Size = new System.Drawing.Size(256, 20);
            this.MeeCancelReason.TabIndex = 14;
            this.MeeCancelReason.ToolTip = "F4 : Show/hide text";
            this.MeeCancelReason.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeCancelReason.ToolTipTitle = "Run System";
            this.MeeCancelReason.Validated += new System.EventHandler(this.MeeCancelReason_Validated);
            // 
            // FrmSalesDebtSecurities
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(924, 623);
            this.Name = "FrmSalesDebtSecurities";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInvestmentBankAcc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            this.TcSalesDebt1.ResumeLayout(false);
            this.TpgInvestmentDetails.ResumeLayout(false);
            this.TpgInvestmentDetails.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGainSale.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteMaturityDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteMaturityDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueAnnualDaysAssumption.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteNextCouponDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteNextCouponDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteLastCouponDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteLastCouponDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCouponInterestRt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAccruedInterest.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteSettlementDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteSettlementDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTradeDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTradeDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalSalesAmt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalExpenses.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCurRate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInterestFrequency.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSalesAmt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMarketValue.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMarketPrice.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInvestmentCost.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSalesPrice.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInvestmentName.Properties)).EndInit();
            this.TcSalesDebt2.ResumeLayout(false);
            this.TpgExpenses.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DteUsageDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteUsageDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            this.panel10.ResumeLayout(false);
            this.TpgApproval.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtNominalAmt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMovingAveragePrice.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInvestmentCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueInvestmentCtCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueInvestmentType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueFinancialInstitutionCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVoucher.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCancelReason.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal DevExpress.XtraEditors.TextEdit TxtInvestmentBankAcc;
        private System.Windows.Forms.Label label5;
        public DevExpress.XtraEditors.SimpleButton BtnInvestmentBankAcc;
        private DevExpress.XtraEditors.CheckEdit ChkCancelInd;
        internal DevExpress.XtraEditors.TextEdit TxtStatus;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabControl TcSalesDebt1;
        private System.Windows.Forms.TabPage TpgInvestmentDetails;
        private System.Windows.Forms.TabControl TcSalesDebt2;
        private System.Windows.Forms.TabPage TpgExpenses;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel8;
        protected internal TenTec.Windows.iGridLib.iGrid Grd1;
        private System.Windows.Forms.Panel panel10;
        public DevExpress.XtraEditors.SimpleButton BtnRefreshData;
        private System.Windows.Forms.TabPage TpgApproval;
        protected internal TenTec.Windows.iGridLib.iGrid Grd2;
        private System.Windows.Forms.Label label11;
        internal DevExpress.XtraEditors.TextEdit TxtNominalAmt;
        private System.Windows.Forms.Label label10;
        internal DevExpress.XtraEditors.TextEdit TxtMovingAveragePrice;
        private System.Windows.Forms.Label label9;
        internal DevExpress.XtraEditors.TextEdit TxtInvestmentCode;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label23;
        public DevExpress.XtraEditors.LookUpEdit LueInvestmentCtCode;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label4;
        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        internal DevExpress.XtraEditors.TextEdit TxtVoucher;
        public DevExpress.XtraEditors.LookUpEdit LueFinancialInstitutionCode;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        internal DevExpress.XtraEditors.TextEdit TxtInterestFrequency;
        internal DevExpress.XtraEditors.TextEdit TxtSalesAmt;
        internal DevExpress.XtraEditors.TextEdit TxtMarketValue;
        internal DevExpress.XtraEditors.TextEdit TxtMarketPrice;
        internal DevExpress.XtraEditors.TextEdit TxtInvestmentCost;
        internal DevExpress.XtraEditors.TextEdit TxtSalesPrice;
        internal DevExpress.XtraEditors.TextEdit TxtInvestmentName;
        public DevExpress.XtraEditors.LookUpEdit LueInvestmentType;
        internal DevExpress.XtraEditors.TextEdit TxtTotalSalesAmt;
        internal DevExpress.XtraEditors.TextEdit TxtTotalExpenses;
        internal DevExpress.XtraEditors.TextEdit TxtCurRate;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        internal DevExpress.XtraEditors.TextEdit TxtCouponInterestRt;
        internal DevExpress.XtraEditors.TextEdit TxtAccruedInterest;
        internal DevExpress.XtraEditors.DateEdit DteSettlementDt;
        internal DevExpress.XtraEditors.DateEdit DteTradeDt;
        private DevExpress.XtraEditors.MemoExEdit MeeCancelReason;
        internal DevExpress.XtraEditors.DateEdit DteNextCouponDt;
        internal DevExpress.XtraEditors.DateEdit DteLastCouponDt;
        public DevExpress.XtraEditors.SimpleButton BtnInvestmentCode;
        public DevExpress.XtraEditors.LookUpEdit LueCurCode;
        public DevExpress.XtraEditors.LookUpEdit LueAnnualDaysAssumption;
        internal DevExpress.XtraEditors.DateEdit DteMaturityDt;
        internal DevExpress.XtraEditors.DateEdit DteUsageDt;
        internal DevExpress.XtraEditors.TextEdit TxtGainSale;
        private System.Windows.Forms.Label label33;
    }
}