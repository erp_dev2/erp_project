﻿#region Update
/*
 * 25/07/2022 [HPH/SIER] New apps
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmAdvanceChargeDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmAdvanceCharge mFrmParent;
        private string mSQL = string.Empty;
        #endregion

        #region Constructor

        public FrmAdvanceChargeDlg(FrmAdvanceCharge FrmParent) 
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                BtnPrint.Visible = false;
                BtnExcel.Visible = false;
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

       
        private void SetGrd()
        {
            Grd1.Cols.Count = 3;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[]
                    {
                        //0
                        "No.",

                        //1-3
                        "AcNo",
                    
                        "AcDesc",
                        
                    },
                    new int[]
                    {
                        //0
                        50,

                        //1-3
                        100,  250, 
                    }
                );

            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2,});
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();
            SQL.AppendLine("Select AcNo, AcDesc From TblCOA ");
            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = "";
                var cm = new MySqlCommand();
                Sm.FilterStr(ref Filter, ref cm, TxtAcNo.Text, new string[] { "AcNo", "AcDesc" });
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + "order By AcNo;",
                        new string[] { "AcNo", "AcDesc" },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                           
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                Sm.SetLue(mFrmParent.LueAcNo, Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));      
                this.Close();
            }
        }
        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Event


        private void TxtAcNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkAcNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "COA's Account");
        }


        #endregion
    }
}
