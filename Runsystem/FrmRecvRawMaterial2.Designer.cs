﻿namespace RunSystem
{
    partial class FrmRecvRawMaterial2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.LueEmpCode2 = new DevExpress.XtraEditors.LookUpEdit();
            this.LueEmpCode1 = new DevExpress.XtraEditors.LookUpEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtCreateBy = new DevExpress.XtraEditors.TextEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.ChkCancelInd = new DevExpress.XtraEditors.CheckEdit();
            this.label28 = new System.Windows.Forms.Label();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.TxtTotal = new DevExpress.XtraEditors.TextEdit();
            this.label17 = new System.Windows.Forms.Label();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label18 = new System.Windows.Forms.Label();
            this.LueQueueNo = new DevExpress.XtraEditors.LookUpEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.LueItCode1 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd11 = new TenTec.Windows.iGridLib.iGrid();
            this.panel8 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtLabelQty1 = new DevExpress.XtraEditors.TextEdit();
            this.label43 = new System.Windows.Forms.Label();
            this.LueWhsCode1 = new DevExpress.XtraEditors.LookUpEdit();
            this.LueShelf1 = new DevExpress.XtraEditors.LookUpEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.TxtTotal1 = new DevExpress.XtraEditors.TextEdit();
            this.label11 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.LueItCode2 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd12 = new TenTec.Windows.iGridLib.iGrid();
            this.panel9 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtLabelQty2 = new DevExpress.XtraEditors.TextEdit();
            this.label44 = new System.Windows.Forms.Label();
            this.LueWhsCode2 = new DevExpress.XtraEditors.LookUpEdit();
            this.LueShelf2 = new DevExpress.XtraEditors.LookUpEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.TxtTotal2 = new DevExpress.XtraEditors.TextEdit();
            this.label13 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.LueItCode3 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd13 = new TenTec.Windows.iGridLib.iGrid();
            this.panel10 = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtLabelQty3 = new DevExpress.XtraEditors.TextEdit();
            this.label45 = new System.Windows.Forms.Label();
            this.LueWhsCode3 = new DevExpress.XtraEditors.LookUpEdit();
            this.LueShelf3 = new DevExpress.XtraEditors.LookUpEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.TxtTotal3 = new DevExpress.XtraEditors.TextEdit();
            this.label15 = new System.Windows.Forms.Label();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.LueItCode4 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd14 = new TenTec.Windows.iGridLib.iGrid();
            this.panel11 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.TxtLabelQty4 = new DevExpress.XtraEditors.TextEdit();
            this.label46 = new System.Windows.Forms.Label();
            this.LueWhsCode4 = new DevExpress.XtraEditors.LookUpEdit();
            this.LueShelf4 = new DevExpress.XtraEditors.LookUpEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.TxtTotal4 = new DevExpress.XtraEditors.TextEdit();
            this.label19 = new System.Windows.Forms.Label();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.LueItCode5 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd15 = new TenTec.Windows.iGridLib.iGrid();
            this.panel12 = new System.Windows.Forms.Panel();
            this.label42 = new System.Windows.Forms.Label();
            this.TxtLabelQty5 = new DevExpress.XtraEditors.TextEdit();
            this.label47 = new System.Windows.Forms.Label();
            this.LueWhsCode5 = new DevExpress.XtraEditors.LookUpEdit();
            this.LueShelf5 = new DevExpress.XtraEditors.LookUpEdit();
            this.label14 = new System.Windows.Forms.Label();
            this.TxtTotal5 = new DevExpress.XtraEditors.TextEdit();
            this.label21 = new System.Windows.Forms.Label();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.LueItCode6 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd16 = new TenTec.Windows.iGridLib.iGrid();
            this.panel13 = new System.Windows.Forms.Panel();
            this.label58 = new System.Windows.Forms.Label();
            this.TxtLabelQty6 = new DevExpress.XtraEditors.TextEdit();
            this.label48 = new System.Windows.Forms.Label();
            this.LueWhsCode6 = new DevExpress.XtraEditors.LookUpEdit();
            this.LueShelf6 = new DevExpress.XtraEditors.LookUpEdit();
            this.label16 = new System.Windows.Forms.Label();
            this.TxtTotal6 = new DevExpress.XtraEditors.TextEdit();
            this.label23 = new System.Windows.Forms.Label();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.LueItCode7 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd17 = new TenTec.Windows.iGridLib.iGrid();
            this.panel14 = new System.Windows.Forms.Panel();
            this.label59 = new System.Windows.Forms.Label();
            this.TxtLabelQty7 = new DevExpress.XtraEditors.TextEdit();
            this.label49 = new System.Windows.Forms.Label();
            this.LueWhsCode7 = new DevExpress.XtraEditors.LookUpEdit();
            this.LueShelf7 = new DevExpress.XtraEditors.LookUpEdit();
            this.label20 = new System.Windows.Forms.Label();
            this.TxtTotal7 = new DevExpress.XtraEditors.TextEdit();
            this.label25 = new System.Windows.Forms.Label();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this.LueItCode8 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd18 = new TenTec.Windows.iGridLib.iGrid();
            this.panel15 = new System.Windows.Forms.Panel();
            this.label60 = new System.Windows.Forms.Label();
            this.TxtLabelQty8 = new DevExpress.XtraEditors.TextEdit();
            this.label50 = new System.Windows.Forms.Label();
            this.LueWhsCode8 = new DevExpress.XtraEditors.LookUpEdit();
            this.LueShelf8 = new DevExpress.XtraEditors.LookUpEdit();
            this.label22 = new System.Windows.Forms.Label();
            this.TxtTotal8 = new DevExpress.XtraEditors.TextEdit();
            this.label27 = new System.Windows.Forms.Label();
            this.tabPage9 = new System.Windows.Forms.TabPage();
            this.LueItCode9 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd19 = new TenTec.Windows.iGridLib.iGrid();
            this.panel16 = new System.Windows.Forms.Panel();
            this.label61 = new System.Windows.Forms.Label();
            this.TxtLabelQty9 = new DevExpress.XtraEditors.TextEdit();
            this.label51 = new System.Windows.Forms.Label();
            this.LueWhsCode9 = new DevExpress.XtraEditors.LookUpEdit();
            this.LueShelf9 = new DevExpress.XtraEditors.LookUpEdit();
            this.label24 = new System.Windows.Forms.Label();
            this.TxtTotal9 = new DevExpress.XtraEditors.TextEdit();
            this.label29 = new System.Windows.Forms.Label();
            this.tabPage10 = new System.Windows.Forms.TabPage();
            this.LueItCode10 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd20 = new TenTec.Windows.iGridLib.iGrid();
            this.panel17 = new System.Windows.Forms.Panel();
            this.label62 = new System.Windows.Forms.Label();
            this.TxtLabelQty10 = new DevExpress.XtraEditors.TextEdit();
            this.label52 = new System.Windows.Forms.Label();
            this.LueWhsCode10 = new DevExpress.XtraEditors.LookUpEdit();
            this.LueShelf10 = new DevExpress.XtraEditors.LookUpEdit();
            this.label26 = new System.Windows.Forms.Label();
            this.TxtTotal10 = new DevExpress.XtraEditors.TextEdit();
            this.label31 = new System.Windows.Forms.Label();
            this.tabPage11 = new System.Windows.Forms.TabPage();
            this.LueItCode11 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd21 = new TenTec.Windows.iGridLib.iGrid();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label63 = new System.Windows.Forms.Label();
            this.TxtLabelQty11 = new DevExpress.XtraEditors.TextEdit();
            this.label53 = new System.Windows.Forms.Label();
            this.LueWhsCode11 = new DevExpress.XtraEditors.LookUpEdit();
            this.LueShelf11 = new DevExpress.XtraEditors.LookUpEdit();
            this.label30 = new System.Windows.Forms.Label();
            this.TxtTotal11 = new DevExpress.XtraEditors.TextEdit();
            this.label32 = new System.Windows.Forms.Label();
            this.tabPage12 = new System.Windows.Forms.TabPage();
            this.LueItCode12 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd22 = new TenTec.Windows.iGridLib.iGrid();
            this.panel18 = new System.Windows.Forms.Panel();
            this.label64 = new System.Windows.Forms.Label();
            this.TxtLabelQty12 = new DevExpress.XtraEditors.TextEdit();
            this.label54 = new System.Windows.Forms.Label();
            this.LueWhsCode12 = new DevExpress.XtraEditors.LookUpEdit();
            this.LueShelf12 = new DevExpress.XtraEditors.LookUpEdit();
            this.label33 = new System.Windows.Forms.Label();
            this.TxtTotal12 = new DevExpress.XtraEditors.TextEdit();
            this.label34 = new System.Windows.Forms.Label();
            this.tabPage13 = new System.Windows.Forms.TabPage();
            this.LueItCode13 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd23 = new TenTec.Windows.iGridLib.iGrid();
            this.panel19 = new System.Windows.Forms.Panel();
            this.label65 = new System.Windows.Forms.Label();
            this.TxtLabelQty13 = new DevExpress.XtraEditors.TextEdit();
            this.label55 = new System.Windows.Forms.Label();
            this.LueWhsCode13 = new DevExpress.XtraEditors.LookUpEdit();
            this.LueShelf13 = new DevExpress.XtraEditors.LookUpEdit();
            this.label35 = new System.Windows.Forms.Label();
            this.TxtTotal13 = new DevExpress.XtraEditors.TextEdit();
            this.label36 = new System.Windows.Forms.Label();
            this.tabPage14 = new System.Windows.Forms.TabPage();
            this.LueItCode14 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd24 = new TenTec.Windows.iGridLib.iGrid();
            this.panel20 = new System.Windows.Forms.Panel();
            this.label66 = new System.Windows.Forms.Label();
            this.TxtLabelQty14 = new DevExpress.XtraEditors.TextEdit();
            this.label56 = new System.Windows.Forms.Label();
            this.LueWhsCode14 = new DevExpress.XtraEditors.LookUpEdit();
            this.LueShelf14 = new DevExpress.XtraEditors.LookUpEdit();
            this.label37 = new System.Windows.Forms.Label();
            this.TxtTotal14 = new DevExpress.XtraEditors.TextEdit();
            this.label38 = new System.Windows.Forms.Label();
            this.tabPage15 = new System.Windows.Forms.TabPage();
            this.LueItCode15 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd25 = new TenTec.Windows.iGridLib.iGrid();
            this.panel21 = new System.Windows.Forms.Panel();
            this.label67 = new System.Windows.Forms.Label();
            this.TxtLabelQty15 = new DevExpress.XtraEditors.TextEdit();
            this.label57 = new System.Windows.Forms.Label();
            this.LueWhsCode15 = new DevExpress.XtraEditors.LookUpEdit();
            this.LueShelf15 = new DevExpress.XtraEditors.LookUpEdit();
            this.label39 = new System.Windows.Forms.Label();
            this.TxtTotal15 = new DevExpress.XtraEditors.TextEdit();
            this.label40 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueEmpCode2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueEmpCode1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCreateBy.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueQueueNo.Properties)).BeginInit();
            this.panel7.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd11)).BeginInit();
            this.panel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLabelQty1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShelf1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal1.Properties)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd12)).BeginInit();
            this.panel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLabelQty2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShelf2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal2.Properties)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd13)).BeginInit();
            this.panel10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLabelQty3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShelf3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal3.Properties)).BeginInit();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd14)).BeginInit();
            this.panel11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLabelQty4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShelf4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal4.Properties)).BeginInit();
            this.tabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd15)).BeginInit();
            this.panel12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLabelQty5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShelf5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal5.Properties)).BeginInit();
            this.tabPage6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd16)).BeginInit();
            this.panel13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLabelQty6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShelf6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal6.Properties)).BeginInit();
            this.tabPage7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd17)).BeginInit();
            this.panel14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLabelQty7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShelf7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal7.Properties)).BeginInit();
            this.tabPage8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd18)).BeginInit();
            this.panel15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLabelQty8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShelf8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal8.Properties)).BeginInit();
            this.tabPage9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd19)).BeginInit();
            this.panel16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLabelQty9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShelf9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal9.Properties)).BeginInit();
            this.tabPage10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd20)).BeginInit();
            this.panel17.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLabelQty10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShelf10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal10.Properties)).BeginInit();
            this.tabPage11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd21)).BeginInit();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLabelQty11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShelf11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal11.Properties)).BeginInit();
            this.tabPage12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd22)).BeginInit();
            this.panel18.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLabelQty12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShelf12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal12.Properties)).BeginInit();
            this.tabPage13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode13.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd23)).BeginInit();
            this.panel19.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLabelQty13.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode13.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShelf13.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal13.Properties)).BeginInit();
            this.tabPage14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode14.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd24)).BeginInit();
            this.panel20.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLabelQty14.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode14.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShelf14.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal14.Properties)).BeginInit();
            this.tabPage15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode15.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd25)).BeginInit();
            this.panel21.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLabelQty15.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode15.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShelf15.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal15.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Size = new System.Drawing.Size(70, 473);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel7);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Size = new System.Drawing.Size(772, 473);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel3.Controls.Add(this.panel4);
            this.panel3.Controls.Add(this.ChkCancelInd);
            this.panel3.Controls.Add(this.label28);
            this.panel3.Controls.Add(this.DteDocDt);
            this.panel3.Controls.Add(this.TxtTotal);
            this.panel3.Controls.Add(this.label17);
            this.panel3.Controls.Add(this.TxtDocNo);
            this.panel3.Controls.Add(this.label18);
            this.panel3.Controls.Add(this.LueQueueNo);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(772, 93);
            this.panel3.TabIndex = 10;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.LueEmpCode2);
            this.panel4.Controls.Add(this.LueEmpCode1);
            this.panel4.Controls.Add(this.label1);
            this.panel4.Controls.Add(this.TxtCreateBy);
            this.panel4.Controls.Add(this.label5);
            this.panel4.Controls.Add(this.label41);
            this.panel4.Controls.Add(this.MeeRemark);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(338, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(434, 93);
            this.panel4.TabIndex = 20;
            // 
            // LueEmpCode2
            // 
            this.LueEmpCode2.EnterMoveNextControl = true;
            this.LueEmpCode2.Location = new System.Drawing.Point(83, 25);
            this.LueEmpCode2.Margin = new System.Windows.Forms.Padding(5);
            this.LueEmpCode2.Name = "LueEmpCode2";
            this.LueEmpCode2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEmpCode2.Properties.Appearance.Options.UseFont = true;
            this.LueEmpCode2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEmpCode2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueEmpCode2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEmpCode2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueEmpCode2.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEmpCode2.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueEmpCode2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEmpCode2.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueEmpCode2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueEmpCode2.Properties.DropDownRows = 25;
            this.LueEmpCode2.Properties.MaxLength = 16;
            this.LueEmpCode2.Properties.NullText = "[Empty]";
            this.LueEmpCode2.Properties.PopupWidth = 500;
            this.LueEmpCode2.Size = new System.Drawing.Size(343, 20);
            this.LueEmpCode2.TabIndex = 23;
            this.LueEmpCode2.ToolTip = "F4 : Show/hide list";
            this.LueEmpCode2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueEmpCode2.EditValueChanged += new System.EventHandler(this.LueEmpCode2_EditValueChanged);
            // 
            // LueEmpCode1
            // 
            this.LueEmpCode1.EnterMoveNextControl = true;
            this.LueEmpCode1.Location = new System.Drawing.Point(83, 4);
            this.LueEmpCode1.Margin = new System.Windows.Forms.Padding(5);
            this.LueEmpCode1.Name = "LueEmpCode1";
            this.LueEmpCode1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEmpCode1.Properties.Appearance.Options.UseFont = true;
            this.LueEmpCode1.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEmpCode1.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueEmpCode1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEmpCode1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueEmpCode1.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEmpCode1.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueEmpCode1.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEmpCode1.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueEmpCode1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueEmpCode1.Properties.DropDownRows = 25;
            this.LueEmpCode1.Properties.MaxLength = 16;
            this.LueEmpCode1.Properties.NullText = "[Empty]";
            this.LueEmpCode1.Properties.PopupWidth = 500;
            this.LueEmpCode1.Size = new System.Drawing.Size(343, 20);
            this.LueEmpCode1.TabIndex = 22;
            this.LueEmpCode1.ToolTip = "F4 : Show/hide list";
            this.LueEmpCode1.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueEmpCode1.EditValueChanged += new System.EventHandler(this.LueEmpCode1_EditValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(6, 6);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 14);
            this.label1.TabIndex = 21;
            this.label1.Text = "QC Personel";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCreateBy
            // 
            this.TxtCreateBy.EnterMoveNextControl = true;
            this.TxtCreateBy.Location = new System.Drawing.Point(83, 46);
            this.TxtCreateBy.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCreateBy.Name = "TxtCreateBy";
            this.TxtCreateBy.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCreateBy.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCreateBy.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCreateBy.Properties.Appearance.Options.UseFont = true;
            this.TxtCreateBy.Properties.MaxLength = 16;
            this.TxtCreateBy.Size = new System.Drawing.Size(343, 20);
            this.TxtCreateBy.TabIndex = 25;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(32, 70);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 14);
            this.label5.TabIndex = 26;
            this.label5.Text = "Remark";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.ForeColor = System.Drawing.Color.Black;
            this.label41.Location = new System.Drawing.Point(12, 49);
            this.label41.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(67, 14);
            this.label41.TabIndex = 24;
            this.label41.Text = "Created By";
            this.label41.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(83, 67);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 400;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(343, 20);
            this.MeeRemark.TabIndex = 27;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // ChkCancelInd
            // 
            this.ChkCancelInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkCancelInd.Location = new System.Drawing.Point(274, 3);
            this.ChkCancelInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkCancelInd.Name = "ChkCancelInd";
            this.ChkCancelInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkCancelInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkCancelInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkCancelInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseFont = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkCancelInd.Properties.Caption = "Cancel";
            this.ChkCancelInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkCancelInd.Size = new System.Drawing.Size(66, 22);
            this.ChkCancelInd.TabIndex = 13;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Black;
            this.label28.Location = new System.Drawing.Point(38, 70);
            this.label28.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(35, 14);
            this.label28.TabIndex = 18;
            this.label28.Text = "Total";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(76, 25);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(126, 20);
            this.DteDocDt.TabIndex = 15;
            // 
            // TxtTotal
            // 
            this.TxtTotal.EditValue = "";
            this.TxtTotal.EnterMoveNextControl = true;
            this.TxtTotal.Location = new System.Drawing.Point(76, 67);
            this.TxtTotal.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal.Name = "TxtTotal";
            this.TxtTotal.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotal.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotal.Size = new System.Drawing.Size(196, 20);
            this.TxtTotal.TabIndex = 19;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Red;
            this.label17.Location = new System.Drawing.Point(40, 28);
            this.label17.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(33, 14);
            this.label17.TabIndex = 14;
            this.label17.Text = "Date";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(76, 4);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 16;
            this.TxtDocNo.Properties.ReadOnly = true;
            this.TxtDocNo.Size = new System.Drawing.Size(196, 20);
            this.TxtDocNo.TabIndex = 12;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(5, 6);
            this.label18.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(68, 14);
            this.label18.TabIndex = 11;
            this.label18.Text = "Dokumen#";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueQueueNo
            // 
            this.LueQueueNo.EnterMoveNextControl = true;
            this.LueQueueNo.Location = new System.Drawing.Point(76, 46);
            this.LueQueueNo.Margin = new System.Windows.Forms.Padding(5);
            this.LueQueueNo.Name = "LueQueueNo";
            this.LueQueueNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueQueueNo.Properties.Appearance.Options.UseFont = true;
            this.LueQueueNo.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueQueueNo.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueQueueNo.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueQueueNo.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueQueueNo.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueQueueNo.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueQueueNo.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueQueueNo.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueQueueNo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueQueueNo.Properties.DropDownRows = 30;
            this.LueQueueNo.Properties.NullText = "[Empty]";
            this.LueQueueNo.Properties.PopupWidth = 300;
            this.LueQueueNo.Size = new System.Drawing.Size(196, 20);
            this.LueQueueNo.TabIndex = 17;
            this.LueQueueNo.ToolTip = "F4 : Show/hide list";
            this.LueQueueNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueQueueNo.EditValueChanged += new System.EventHandler(this.LueQueueNo_EditValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(20, 49);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 14);
            this.label4.TabIndex = 16;
            this.label4.Text = "Queue#";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel7.Controls.Add(this.tabControl1);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel7.Location = new System.Drawing.Point(0, 93);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(772, 380);
            this.panel7.TabIndex = 55;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Controls.Add(this.tabPage7);
            this.tabControl1.Controls.Add(this.tabPage8);
            this.tabControl1.Controls.Add(this.tabPage9);
            this.tabControl1.Controls.Add(this.tabPage10);
            this.tabControl1.Controls.Add(this.tabPage11);
            this.tabControl1.Controls.Add(this.tabPage12);
            this.tabControl1.Controls.Add(this.tabPage13);
            this.tabControl1.Controls.Add(this.tabPage14);
            this.tabControl1.Controls.Add(this.tabPage15);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(772, 380);
            this.tabControl1.TabIndex = 28;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.LueItCode1);
            this.tabPage1.Controls.Add(this.Grd11);
            this.tabPage1.Controls.Add(this.panel8);
            this.tabPage1.Location = new System.Drawing.Point(4, 23);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(764, 353);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "No.1";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // LueItCode1
            // 
            this.LueItCode1.EnterMoveNextControl = true;
            this.LueItCode1.Location = new System.Drawing.Point(22, 55);
            this.LueItCode1.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode1.Name = "LueItCode1";
            this.LueItCode1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode1.Properties.Appearance.Options.UseFont = true;
            this.LueItCode1.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode1.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode1.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode1.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode1.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode1.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode1.Properties.DropDownRows = 20;
            this.LueItCode1.Properties.NullText = "[Empty]";
            this.LueItCode1.Properties.PopupWidth = 500;
            this.LueItCode1.Size = new System.Drawing.Size(152, 20);
            this.LueItCode1.TabIndex = 38;
            this.LueItCode1.ToolTip = "F4 : Show/hide list";
            this.LueItCode1.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueItCode1.EditValueChanged += new System.EventHandler(this.LueItCode1_EditValueChanged);
            this.LueItCode1.Leave += new System.EventHandler(this.LueItCode1_Leave);
            this.LueItCode1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueItCode1_KeyDown);
            // 
            // Grd11
            // 
            this.Grd11.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd11.DefaultRow.Height = 20;
            this.Grd11.DefaultRow.Sortable = false;
            this.Grd11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd11.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd11.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd11.Header.Height = 20;
            this.Grd11.Location = new System.Drawing.Point(3, 33);
            this.Grd11.Name = "Grd11";
            this.Grd11.RowHeader.Visible = true;
            this.Grd11.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd11.SingleClickEdit = true;
            this.Grd11.Size = new System.Drawing.Size(758, 317);
            this.Grd11.TabIndex = 37;
            this.Grd11.TreeCol = null;
            this.Grd11.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd11.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd11_RequestEdit);
            this.Grd11.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd11_KeyDown);
            this.Grd11.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd11_ColHdrDoubleClick);
            this.Grd11.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd11_AfterCommitEdit);
            this.Grd11.CustomDrawCellForeground += new TenTec.Windows.iGridLib.iGCustomDrawCellEventHandler(this.Grd11_CustomDrawCellForeground);
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel8.Controls.Add(this.label2);
            this.panel8.Controls.Add(this.TxtLabelQty1);
            this.panel8.Controls.Add(this.label43);
            this.panel8.Controls.Add(this.LueWhsCode1);
            this.panel8.Controls.Add(this.LueShelf1);
            this.panel8.Controls.Add(this.label8);
            this.panel8.Controls.Add(this.TxtTotal1);
            this.panel8.Controls.Add(this.label11);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel8.Location = new System.Drawing.Point(3, 3);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(758, 30);
            this.panel8.TabIndex = 44;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(283, 8);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 14);
            this.label2.TabIndex = 33;
            this.label2.Text = "Label";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtLabelQty1
            // 
            this.TxtLabelQty1.EditValue = "";
            this.TxtLabelQty1.EnterMoveNextControl = true;
            this.TxtLabelQty1.Location = new System.Drawing.Point(321, 5);
            this.TxtLabelQty1.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLabelQty1.Name = "TxtLabelQty1";
            this.TxtLabelQty1.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLabelQty1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLabelQty1.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLabelQty1.Properties.Appearance.Options.UseFont = true;
            this.TxtLabelQty1.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtLabelQty1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtLabelQty1.Size = new System.Drawing.Size(77, 20);
            this.TxtLabelQty1.TabIndex = 34;
            this.TxtLabelQty1.Validated += new System.EventHandler(this.TxtLabelQty1_Validated);
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.ForeColor = System.Drawing.Color.Black;
            this.label43.Location = new System.Drawing.Point(403, 8);
            this.label43.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(69, 14);
            this.label43.TabIndex = 35;
            this.label43.Text = "Warehouse";
            this.label43.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueWhsCode1
            // 
            this.LueWhsCode1.EnterMoveNextControl = true;
            this.LueWhsCode1.Location = new System.Drawing.Point(474, 5);
            this.LueWhsCode1.Margin = new System.Windows.Forms.Padding(5);
            this.LueWhsCode1.Name = "LueWhsCode1";
            this.LueWhsCode1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode1.Properties.Appearance.Options.UseFont = true;
            this.LueWhsCode1.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode1.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueWhsCode1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueWhsCode1.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode1.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueWhsCode1.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode1.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueWhsCode1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueWhsCode1.Properties.DropDownRows = 20;
            this.LueWhsCode1.Properties.NullText = "[Empty]";
            this.LueWhsCode1.Properties.PopupWidth = 500;
            this.LueWhsCode1.Size = new System.Drawing.Size(273, 20);
            this.LueWhsCode1.TabIndex = 36;
            this.LueWhsCode1.ToolTip = "F4 : Show/hide list";
            this.LueWhsCode1.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueWhsCode1.EditValueChanged += new System.EventHandler(this.LueWhsCode1_EditValueChanged);
            // 
            // LueShelf1
            // 
            this.LueShelf1.EnterMoveNextControl = true;
            this.LueShelf1.Location = new System.Drawing.Point(41, 5);
            this.LueShelf1.Margin = new System.Windows.Forms.Padding(5);
            this.LueShelf1.Name = "LueShelf1";
            this.LueShelf1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf1.Properties.Appearance.Options.UseFont = true;
            this.LueShelf1.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf1.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueShelf1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueShelf1.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf1.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueShelf1.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf1.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueShelf1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueShelf1.Properties.DropDownRows = 20;
            this.LueShelf1.Properties.NullText = "[Empty]";
            this.LueShelf1.Properties.PopupWidth = 500;
            this.LueShelf1.Size = new System.Drawing.Size(120, 20);
            this.LueShelf1.TabIndex = 30;
            this.LueShelf1.ToolTip = "F4 : Show/hide list";
            this.LueShelf1.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueShelf1.EditValueChanged += new System.EventHandler(this.LueShelf1_EditValueChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(165, 8);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(35, 14);
            this.label8.TabIndex = 31;
            this.label8.Text = "Total";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal1
            // 
            this.TxtTotal1.EditValue = "";
            this.TxtTotal1.EnterMoveNextControl = true;
            this.TxtTotal1.Location = new System.Drawing.Point(202, 5);
            this.TxtTotal1.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal1.Name = "TxtTotal1";
            this.TxtTotal1.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtTotal1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal1.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal1.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal1.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotal1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotal1.Properties.ReadOnly = true;
            this.TxtTotal1.Size = new System.Drawing.Size(77, 20);
            this.TxtTotal1.TabIndex = 32;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Red;
            this.label11.Location = new System.Drawing.Point(11, 8);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(26, 14);
            this.label11.TabIndex = 29;
            this.label11.Text = "Rak";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.LueItCode2);
            this.tabPage2.Controls.Add(this.Grd12);
            this.tabPage2.Controls.Add(this.panel9);
            this.tabPage2.Location = new System.Drawing.Point(4, 23);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(764, 113);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "No.2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // LueItCode2
            // 
            this.LueItCode2.EnterMoveNextControl = true;
            this.LueItCode2.Location = new System.Drawing.Point(24, 56);
            this.LueItCode2.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode2.Name = "LueItCode2";
            this.LueItCode2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode2.Properties.Appearance.Options.UseFont = true;
            this.LueItCode2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode2.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode2.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode2.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode2.Properties.DropDownRows = 20;
            this.LueItCode2.Properties.NullText = "[Empty]";
            this.LueItCode2.Properties.PopupWidth = 500;
            this.LueItCode2.Size = new System.Drawing.Size(163, 20);
            this.LueItCode2.TabIndex = 39;
            this.LueItCode2.ToolTip = "F4 : Show/hide list";
            this.LueItCode2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueItCode2.EditValueChanged += new System.EventHandler(this.LueItCode2_EditValueChanged);
            this.LueItCode2.Leave += new System.EventHandler(this.LueItCode2_Leave);
            this.LueItCode2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueItCode2_KeyDown);
            // 
            // Grd12
            // 
            this.Grd12.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd12.DefaultRow.Height = 20;
            this.Grd12.DefaultRow.Sortable = false;
            this.Grd12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd12.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd12.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd12.Header.Height = 20;
            this.Grd12.Location = new System.Drawing.Point(3, 35);
            this.Grd12.Name = "Grd12";
            this.Grd12.RowHeader.Visible = true;
            this.Grd12.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd12.SingleClickEdit = true;
            this.Grd12.Size = new System.Drawing.Size(758, 75);
            this.Grd12.TabIndex = 38;
            this.Grd12.TreeCol = null;
            this.Grd12.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd12.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd12_RequestEdit);
            this.Grd12.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd12_KeyDown);
            this.Grd12.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd12_ColHdrDoubleClick);
            this.Grd12.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd12_AfterCommitEdit);
            this.Grd12.CustomDrawCellForeground += new TenTec.Windows.iGridLib.iGCustomDrawCellEventHandler(this.Grd12_CustomDrawCellForeground);
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel9.Controls.Add(this.label3);
            this.panel9.Controls.Add(this.TxtLabelQty2);
            this.panel9.Controls.Add(this.label44);
            this.panel9.Controls.Add(this.LueWhsCode2);
            this.panel9.Controls.Add(this.LueShelf2);
            this.panel9.Controls.Add(this.label9);
            this.panel9.Controls.Add(this.TxtTotal2);
            this.panel9.Controls.Add(this.label13);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel9.Location = new System.Drawing.Point(3, 3);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(758, 32);
            this.panel9.TabIndex = 29;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(301, 9);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 14);
            this.label3.TabIndex = 34;
            this.label3.Text = "Label";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtLabelQty2
            // 
            this.TxtLabelQty2.EditValue = "";
            this.TxtLabelQty2.EnterMoveNextControl = true;
            this.TxtLabelQty2.Location = new System.Drawing.Point(339, 6);
            this.TxtLabelQty2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLabelQty2.Name = "TxtLabelQty2";
            this.TxtLabelQty2.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLabelQty2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLabelQty2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLabelQty2.Properties.Appearance.Options.UseFont = true;
            this.TxtLabelQty2.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtLabelQty2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtLabelQty2.Size = new System.Drawing.Size(77, 20);
            this.TxtLabelQty2.TabIndex = 35;
            this.TxtLabelQty2.Validated += new System.EventHandler(this.TxtLabelQty2_Validated);
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.ForeColor = System.Drawing.Color.Black;
            this.label44.Location = new System.Drawing.Point(422, 9);
            this.label44.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(69, 14);
            this.label44.TabIndex = 36;
            this.label44.Text = "Warehouse";
            this.label44.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueWhsCode2
            // 
            this.LueWhsCode2.EnterMoveNextControl = true;
            this.LueWhsCode2.Location = new System.Drawing.Point(496, 6);
            this.LueWhsCode2.Margin = new System.Windows.Forms.Padding(5);
            this.LueWhsCode2.Name = "LueWhsCode2";
            this.LueWhsCode2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode2.Properties.Appearance.Options.UseFont = true;
            this.LueWhsCode2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueWhsCode2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueWhsCode2.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode2.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueWhsCode2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode2.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueWhsCode2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueWhsCode2.Properties.DropDownRows = 20;
            this.LueWhsCode2.Properties.NullText = "[Empty]";
            this.LueWhsCode2.Properties.PopupWidth = 500;
            this.LueWhsCode2.Size = new System.Drawing.Size(249, 20);
            this.LueWhsCode2.TabIndex = 37;
            this.LueWhsCode2.ToolTip = "F4 : Show/hide list";
            this.LueWhsCode2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueWhsCode2.EditValueChanged += new System.EventHandler(this.LueWhsCode2_EditValueChanged);
            // 
            // LueShelf2
            // 
            this.LueShelf2.EnterMoveNextControl = true;
            this.LueShelf2.Location = new System.Drawing.Point(39, 6);
            this.LueShelf2.Margin = new System.Windows.Forms.Padding(5);
            this.LueShelf2.Name = "LueShelf2";
            this.LueShelf2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf2.Properties.Appearance.Options.UseFont = true;
            this.LueShelf2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueShelf2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueShelf2.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf2.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueShelf2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf2.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueShelf2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueShelf2.Properties.DropDownRows = 20;
            this.LueShelf2.Properties.NullText = "[Empty]";
            this.LueShelf2.Properties.PopupWidth = 500;
            this.LueShelf2.Size = new System.Drawing.Size(125, 20);
            this.LueShelf2.TabIndex = 31;
            this.LueShelf2.ToolTip = "F4 : Show/hide list";
            this.LueShelf2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueShelf2.EditValueChanged += new System.EventHandler(this.LueShelf2_EditValueChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(168, 9);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(35, 14);
            this.label9.TabIndex = 32;
            this.label9.Text = "Total";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal2
            // 
            this.TxtTotal2.EnterMoveNextControl = true;
            this.TxtTotal2.Location = new System.Drawing.Point(205, 6);
            this.TxtTotal2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal2.Name = "TxtTotal2";
            this.TxtTotal2.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal2.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal2.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotal2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotal2.Size = new System.Drawing.Size(90, 20);
            this.TxtTotal2.TabIndex = 33;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(9, 9);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(26, 14);
            this.label13.TabIndex = 30;
            this.label13.Text = "Rak";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.LueItCode3);
            this.tabPage3.Controls.Add(this.Grd13);
            this.tabPage3.Controls.Add(this.panel10);
            this.tabPage3.Location = new System.Drawing.Point(4, 23);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(764, 113);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "No.3";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // LueItCode3
            // 
            this.LueItCode3.EnterMoveNextControl = true;
            this.LueItCode3.Location = new System.Drawing.Point(21, 55);
            this.LueItCode3.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode3.Name = "LueItCode3";
            this.LueItCode3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode3.Properties.Appearance.Options.UseFont = true;
            this.LueItCode3.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode3.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode3.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode3.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode3.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode3.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode3.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode3.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode3.Properties.DropDownRows = 20;
            this.LueItCode3.Properties.NullText = "[Empty]";
            this.LueItCode3.Properties.PopupWidth = 500;
            this.LueItCode3.Size = new System.Drawing.Size(163, 20);
            this.LueItCode3.TabIndex = 39;
            this.LueItCode3.ToolTip = "F4 : Show/hide list";
            this.LueItCode3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueItCode3.EditValueChanged += new System.EventHandler(this.LueItCode3_EditValueChanged);
            this.LueItCode3.Leave += new System.EventHandler(this.LueItCode3_Leave);
            this.LueItCode3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueItCode3_KeyDown);
            // 
            // Grd13
            // 
            this.Grd13.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd13.DefaultRow.Height = 20;
            this.Grd13.DefaultRow.Sortable = false;
            this.Grd13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd13.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd13.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd13.Header.Height = 20;
            this.Grd13.Location = new System.Drawing.Point(0, 34);
            this.Grd13.Name = "Grd13";
            this.Grd13.RowHeader.Visible = true;
            this.Grd13.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd13.SingleClickEdit = true;
            this.Grd13.Size = new System.Drawing.Size(764, 79);
            this.Grd13.TabIndex = 38;
            this.Grd13.TreeCol = null;
            this.Grd13.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd13.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd13_RequestEdit);
            this.Grd13.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd13_KeyDown);
            this.Grd13.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd13_ColHdrDoubleClick);
            this.Grd13.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd13_AfterCommitEdit);
            this.Grd13.CustomDrawCellForeground += new TenTec.Windows.iGridLib.iGCustomDrawCellEventHandler(this.Grd13_CustomDrawCellForeground);
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel10.Controls.Add(this.label6);
            this.panel10.Controls.Add(this.TxtLabelQty3);
            this.panel10.Controls.Add(this.label45);
            this.panel10.Controls.Add(this.LueWhsCode3);
            this.panel10.Controls.Add(this.LueShelf3);
            this.panel10.Controls.Add(this.label10);
            this.panel10.Controls.Add(this.TxtTotal3);
            this.panel10.Controls.Add(this.label15);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel10.Location = new System.Drawing.Point(0, 0);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(764, 34);
            this.panel10.TabIndex = 29;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(304, 9);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 14);
            this.label6.TabIndex = 34;
            this.label6.Text = "Label";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtLabelQty3
            // 
            this.TxtLabelQty3.EditValue = "";
            this.TxtLabelQty3.EnterMoveNextControl = true;
            this.TxtLabelQty3.Location = new System.Drawing.Point(342, 6);
            this.TxtLabelQty3.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLabelQty3.Name = "TxtLabelQty3";
            this.TxtLabelQty3.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLabelQty3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLabelQty3.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLabelQty3.Properties.Appearance.Options.UseFont = true;
            this.TxtLabelQty3.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtLabelQty3.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtLabelQty3.Size = new System.Drawing.Size(77, 20);
            this.TxtLabelQty3.TabIndex = 35;
            this.TxtLabelQty3.Validated += new System.EventHandler(this.TxtLabelQty3_Validated);
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.ForeColor = System.Drawing.Color.Black;
            this.label45.Location = new System.Drawing.Point(420, 9);
            this.label45.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(69, 14);
            this.label45.TabIndex = 36;
            this.label45.Text = "Warehouse";
            this.label45.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueWhsCode3
            // 
            this.LueWhsCode3.EnterMoveNextControl = true;
            this.LueWhsCode3.Location = new System.Drawing.Point(494, 6);
            this.LueWhsCode3.Margin = new System.Windows.Forms.Padding(5);
            this.LueWhsCode3.Name = "LueWhsCode3";
            this.LueWhsCode3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode3.Properties.Appearance.Options.UseFont = true;
            this.LueWhsCode3.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode3.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueWhsCode3.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode3.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueWhsCode3.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode3.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueWhsCode3.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode3.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueWhsCode3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueWhsCode3.Properties.DropDownRows = 20;
            this.LueWhsCode3.Properties.NullText = "[Empty]";
            this.LueWhsCode3.Properties.PopupWidth = 500;
            this.LueWhsCode3.Size = new System.Drawing.Size(260, 20);
            this.LueWhsCode3.TabIndex = 37;
            this.LueWhsCode3.ToolTip = "F4 : Show/hide list";
            this.LueWhsCode3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueWhsCode3.EditValueChanged += new System.EventHandler(this.LueWhsCode3_EditValueChanged);
            // 
            // LueShelf3
            // 
            this.LueShelf3.EnterMoveNextControl = true;
            this.LueShelf3.Location = new System.Drawing.Point(34, 6);
            this.LueShelf3.Margin = new System.Windows.Forms.Padding(5);
            this.LueShelf3.Name = "LueShelf3";
            this.LueShelf3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf3.Properties.Appearance.Options.UseFont = true;
            this.LueShelf3.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf3.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueShelf3.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf3.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueShelf3.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf3.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueShelf3.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf3.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueShelf3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueShelf3.Properties.DropDownRows = 20;
            this.LueShelf3.Properties.NullText = "[Empty]";
            this.LueShelf3.Properties.PopupWidth = 500;
            this.LueShelf3.Size = new System.Drawing.Size(134, 20);
            this.LueShelf3.TabIndex = 31;
            this.LueShelf3.ToolTip = "F4 : Show/hide list";
            this.LueShelf3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueShelf3.EditValueChanged += new System.EventHandler(this.LueShelf3_EditValueChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(172, 9);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(35, 14);
            this.label10.TabIndex = 32;
            this.label10.Text = "Total";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal3
            // 
            this.TxtTotal3.EnterMoveNextControl = true;
            this.TxtTotal3.Location = new System.Drawing.Point(209, 6);
            this.TxtTotal3.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal3.Name = "TxtTotal3";
            this.TxtTotal3.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal3.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal3.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal3.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotal3.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotal3.Size = new System.Drawing.Size(90, 20);
            this.TxtTotal3.TabIndex = 33;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(4, 9);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(26, 14);
            this.label15.TabIndex = 30;
            this.label15.Text = "Rak";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.LueItCode4);
            this.tabPage4.Controls.Add(this.Grd14);
            this.tabPage4.Controls.Add(this.panel11);
            this.tabPage4.Location = new System.Drawing.Point(4, 23);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(764, 353);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "No.4";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // LueItCode4
            // 
            this.LueItCode4.EnterMoveNextControl = true;
            this.LueItCode4.Location = new System.Drawing.Point(19, 54);
            this.LueItCode4.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode4.Name = "LueItCode4";
            this.LueItCode4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode4.Properties.Appearance.Options.UseFont = true;
            this.LueItCode4.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode4.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode4.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode4.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode4.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode4.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode4.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode4.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode4.Properties.DropDownRows = 20;
            this.LueItCode4.Properties.NullText = "[Empty]";
            this.LueItCode4.Properties.PopupWidth = 500;
            this.LueItCode4.Size = new System.Drawing.Size(163, 20);
            this.LueItCode4.TabIndex = 39;
            this.LueItCode4.ToolTip = "F4 : Show/hide list";
            this.LueItCode4.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueItCode4.EditValueChanged += new System.EventHandler(this.LueItCode4_EditValueChanged);
            this.LueItCode4.Leave += new System.EventHandler(this.LueItCode4_Leave);
            this.LueItCode4.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueItCode4_KeyDown);
            // 
            // Grd14
            // 
            this.Grd14.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd14.DefaultRow.Height = 20;
            this.Grd14.DefaultRow.Sortable = false;
            this.Grd14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd14.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd14.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd14.Header.Height = 20;
            this.Grd14.Location = new System.Drawing.Point(0, 32);
            this.Grd14.Name = "Grd14";
            this.Grd14.RowHeader.Visible = true;
            this.Grd14.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd14.SingleClickEdit = true;
            this.Grd14.Size = new System.Drawing.Size(764, 321);
            this.Grd14.TabIndex = 38;
            this.Grd14.TreeCol = null;
            this.Grd14.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd14.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd14_RequestEdit);
            this.Grd14.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd14_KeyDown);
            this.Grd14.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd14_ColHdrDoubleClick);
            this.Grd14.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd14_AfterCommitEdit);
            this.Grd14.CustomDrawCellForeground += new TenTec.Windows.iGridLib.iGCustomDrawCellEventHandler(this.Grd14_CustomDrawCellForeground);
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel11.Controls.Add(this.label7);
            this.panel11.Controls.Add(this.TxtLabelQty4);
            this.panel11.Controls.Add(this.label46);
            this.panel11.Controls.Add(this.LueWhsCode4);
            this.panel11.Controls.Add(this.LueShelf4);
            this.panel11.Controls.Add(this.label12);
            this.panel11.Controls.Add(this.TxtTotal4);
            this.panel11.Controls.Add(this.label19);
            this.panel11.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel11.Location = new System.Drawing.Point(0, 0);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(764, 32);
            this.panel11.TabIndex = 29;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(304, 9);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(35, 14);
            this.label7.TabIndex = 34;
            this.label7.Text = "Label";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtLabelQty4
            // 
            this.TxtLabelQty4.EditValue = "";
            this.TxtLabelQty4.EnterMoveNextControl = true;
            this.TxtLabelQty4.Location = new System.Drawing.Point(342, 6);
            this.TxtLabelQty4.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLabelQty4.Name = "TxtLabelQty4";
            this.TxtLabelQty4.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLabelQty4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLabelQty4.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLabelQty4.Properties.Appearance.Options.UseFont = true;
            this.TxtLabelQty4.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtLabelQty4.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtLabelQty4.Size = new System.Drawing.Size(77, 20);
            this.TxtLabelQty4.TabIndex = 35;
            this.TxtLabelQty4.Validated += new System.EventHandler(this.TxtLabelQty4_Validated);
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.ForeColor = System.Drawing.Color.Black;
            this.label46.Location = new System.Drawing.Point(420, 9);
            this.label46.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(69, 14);
            this.label46.TabIndex = 36;
            this.label46.Text = "Warehouse";
            this.label46.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueWhsCode4
            // 
            this.LueWhsCode4.EnterMoveNextControl = true;
            this.LueWhsCode4.Location = new System.Drawing.Point(492, 6);
            this.LueWhsCode4.Margin = new System.Windows.Forms.Padding(5);
            this.LueWhsCode4.Name = "LueWhsCode4";
            this.LueWhsCode4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode4.Properties.Appearance.Options.UseFont = true;
            this.LueWhsCode4.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode4.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueWhsCode4.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode4.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueWhsCode4.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode4.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueWhsCode4.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode4.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueWhsCode4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueWhsCode4.Properties.DropDownRows = 20;
            this.LueWhsCode4.Properties.NullText = "[Empty]";
            this.LueWhsCode4.Properties.PopupWidth = 500;
            this.LueWhsCode4.Size = new System.Drawing.Size(262, 20);
            this.LueWhsCode4.TabIndex = 37;
            this.LueWhsCode4.ToolTip = "F4 : Show/hide list";
            this.LueWhsCode4.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueWhsCode4.EditValueChanged += new System.EventHandler(this.LueWhsCode4_EditValueChanged);
            // 
            // LueShelf4
            // 
            this.LueShelf4.EnterMoveNextControl = true;
            this.LueShelf4.Location = new System.Drawing.Point(35, 6);
            this.LueShelf4.Margin = new System.Windows.Forms.Padding(5);
            this.LueShelf4.Name = "LueShelf4";
            this.LueShelf4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf4.Properties.Appearance.Options.UseFont = true;
            this.LueShelf4.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf4.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueShelf4.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf4.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueShelf4.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf4.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueShelf4.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf4.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueShelf4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueShelf4.Properties.DropDownRows = 20;
            this.LueShelf4.Properties.NullText = "[Empty]";
            this.LueShelf4.Properties.PopupWidth = 500;
            this.LueShelf4.Size = new System.Drawing.Size(135, 20);
            this.LueShelf4.TabIndex = 31;
            this.LueShelf4.ToolTip = "F4 : Show/hide list";
            this.LueShelf4.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueShelf4.EditValueChanged += new System.EventHandler(this.LueShelf4_EditValueChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(174, 9);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(35, 14);
            this.label12.TabIndex = 32;
            this.label12.Text = "Total";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal4
            // 
            this.TxtTotal4.EnterMoveNextControl = true;
            this.TxtTotal4.Location = new System.Drawing.Point(211, 6);
            this.TxtTotal4.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal4.Name = "TxtTotal4";
            this.TxtTotal4.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal4.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal4.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal4.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotal4.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotal4.Size = new System.Drawing.Size(90, 20);
            this.TxtTotal4.TabIndex = 33;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(6, 9);
            this.label19.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(26, 14);
            this.label19.TabIndex = 30;
            this.label19.Text = "Rak";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.LueItCode5);
            this.tabPage5.Controls.Add(this.Grd15);
            this.tabPage5.Controls.Add(this.panel12);
            this.tabPage5.Location = new System.Drawing.Point(4, 23);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(764, 353);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "No.5";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // LueItCode5
            // 
            this.LueItCode5.EnterMoveNextControl = true;
            this.LueItCode5.Location = new System.Drawing.Point(21, 54);
            this.LueItCode5.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode5.Name = "LueItCode5";
            this.LueItCode5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode5.Properties.Appearance.Options.UseFont = true;
            this.LueItCode5.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode5.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode5.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode5.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode5.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode5.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode5.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode5.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode5.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode5.Properties.DropDownRows = 20;
            this.LueItCode5.Properties.NullText = "[Empty]";
            this.LueItCode5.Properties.PopupWidth = 500;
            this.LueItCode5.Size = new System.Drawing.Size(163, 20);
            this.LueItCode5.TabIndex = 39;
            this.LueItCode5.ToolTip = "F4 : Show/hide list";
            this.LueItCode5.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueItCode5.EditValueChanged += new System.EventHandler(this.LueItCode5_EditValueChanged);
            this.LueItCode5.Leave += new System.EventHandler(this.LueItCode5_Leave);
            this.LueItCode5.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueItCode5_KeyDown);
            // 
            // Grd15
            // 
            this.Grd15.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd15.DefaultRow.Height = 20;
            this.Grd15.DefaultRow.Sortable = false;
            this.Grd15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd15.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd15.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd15.Header.Height = 20;
            this.Grd15.Location = new System.Drawing.Point(0, 32);
            this.Grd15.Name = "Grd15";
            this.Grd15.RowHeader.Visible = true;
            this.Grd15.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd15.SingleClickEdit = true;
            this.Grd15.Size = new System.Drawing.Size(764, 321);
            this.Grd15.TabIndex = 38;
            this.Grd15.TreeCol = null;
            this.Grd15.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd15.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd15_RequestEdit);
            this.Grd15.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd15_KeyDown);
            this.Grd15.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd15_ColHdrDoubleClick);
            this.Grd15.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd15_AfterCommitEdit);
            this.Grd15.CustomDrawCellForeground += new TenTec.Windows.iGridLib.iGCustomDrawCellEventHandler(this.Grd15_CustomDrawCellForeground);
            // 
            // panel12
            // 
            this.panel12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel12.Controls.Add(this.label42);
            this.panel12.Controls.Add(this.TxtLabelQty5);
            this.panel12.Controls.Add(this.label47);
            this.panel12.Controls.Add(this.LueWhsCode5);
            this.panel12.Controls.Add(this.LueShelf5);
            this.panel12.Controls.Add(this.label14);
            this.panel12.Controls.Add(this.TxtTotal5);
            this.panel12.Controls.Add(this.label21);
            this.panel12.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel12.Location = new System.Drawing.Point(0, 0);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(764, 32);
            this.panel12.TabIndex = 29;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.ForeColor = System.Drawing.Color.Black;
            this.label42.Location = new System.Drawing.Point(301, 9);
            this.label42.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(35, 14);
            this.label42.TabIndex = 34;
            this.label42.Text = "Label";
            this.label42.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtLabelQty5
            // 
            this.TxtLabelQty5.EditValue = "";
            this.TxtLabelQty5.EnterMoveNextControl = true;
            this.TxtLabelQty5.Location = new System.Drawing.Point(339, 6);
            this.TxtLabelQty5.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLabelQty5.Name = "TxtLabelQty5";
            this.TxtLabelQty5.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLabelQty5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLabelQty5.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLabelQty5.Properties.Appearance.Options.UseFont = true;
            this.TxtLabelQty5.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtLabelQty5.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtLabelQty5.Size = new System.Drawing.Size(77, 20);
            this.TxtLabelQty5.TabIndex = 35;
            this.TxtLabelQty5.Validated += new System.EventHandler(this.TxtLabelQty5_Validated);
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.ForeColor = System.Drawing.Color.Black;
            this.label47.Location = new System.Drawing.Point(418, 9);
            this.label47.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(69, 14);
            this.label47.TabIndex = 36;
            this.label47.Text = "Warehouse";
            this.label47.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueWhsCode5
            // 
            this.LueWhsCode5.EnterMoveNextControl = true;
            this.LueWhsCode5.Location = new System.Drawing.Point(492, 6);
            this.LueWhsCode5.Margin = new System.Windows.Forms.Padding(5);
            this.LueWhsCode5.Name = "LueWhsCode5";
            this.LueWhsCode5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode5.Properties.Appearance.Options.UseFont = true;
            this.LueWhsCode5.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode5.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueWhsCode5.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode5.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueWhsCode5.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode5.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueWhsCode5.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode5.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueWhsCode5.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueWhsCode5.Properties.DropDownRows = 20;
            this.LueWhsCode5.Properties.NullText = "[Empty]";
            this.LueWhsCode5.Properties.PopupWidth = 500;
            this.LueWhsCode5.Size = new System.Drawing.Size(263, 20);
            this.LueWhsCode5.TabIndex = 37;
            this.LueWhsCode5.ToolTip = "F4 : Show/hide list";
            this.LueWhsCode5.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueWhsCode5.EditValueChanged += new System.EventHandler(this.LueWhsCode5_EditValueChanged);
            // 
            // LueShelf5
            // 
            this.LueShelf5.EnterMoveNextControl = true;
            this.LueShelf5.Location = new System.Drawing.Point(34, 6);
            this.LueShelf5.Margin = new System.Windows.Forms.Padding(5);
            this.LueShelf5.Name = "LueShelf5";
            this.LueShelf5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf5.Properties.Appearance.Options.UseFont = true;
            this.LueShelf5.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf5.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueShelf5.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf5.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueShelf5.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf5.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueShelf5.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf5.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueShelf5.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueShelf5.Properties.DropDownRows = 20;
            this.LueShelf5.Properties.NullText = "[Empty]";
            this.LueShelf5.Properties.PopupWidth = 500;
            this.LueShelf5.Size = new System.Drawing.Size(132, 20);
            this.LueShelf5.TabIndex = 31;
            this.LueShelf5.ToolTip = "F4 : Show/hide list";
            this.LueShelf5.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueShelf5.EditValueChanged += new System.EventHandler(this.LueShelf5_EditValueChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(171, 9);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(35, 14);
            this.label14.TabIndex = 32;
            this.label14.Text = "Total";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal5
            // 
            this.TxtTotal5.EnterMoveNextControl = true;
            this.TxtTotal5.Location = new System.Drawing.Point(208, 6);
            this.TxtTotal5.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal5.Name = "TxtTotal5";
            this.TxtTotal5.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal5.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal5.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal5.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotal5.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotal5.Size = new System.Drawing.Size(90, 20);
            this.TxtTotal5.TabIndex = 33;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Black;
            this.label21.Location = new System.Drawing.Point(5, 9);
            this.label21.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(26, 14);
            this.label21.TabIndex = 30;
            this.label21.Text = "Rak";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.LueItCode6);
            this.tabPage6.Controls.Add(this.Grd16);
            this.tabPage6.Controls.Add(this.panel13);
            this.tabPage6.Location = new System.Drawing.Point(4, 23);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Size = new System.Drawing.Size(764, 353);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "No.6";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // LueItCode6
            // 
            this.LueItCode6.EnterMoveNextControl = true;
            this.LueItCode6.Location = new System.Drawing.Point(21, 53);
            this.LueItCode6.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode6.Name = "LueItCode6";
            this.LueItCode6.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode6.Properties.Appearance.Options.UseFont = true;
            this.LueItCode6.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode6.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode6.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode6.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode6.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode6.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode6.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode6.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode6.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode6.Properties.DropDownRows = 20;
            this.LueItCode6.Properties.NullText = "[Empty]";
            this.LueItCode6.Properties.PopupWidth = 500;
            this.LueItCode6.Size = new System.Drawing.Size(163, 20);
            this.LueItCode6.TabIndex = 39;
            this.LueItCode6.ToolTip = "F4 : Show/hide list";
            this.LueItCode6.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueItCode6.EditValueChanged += new System.EventHandler(this.LueItCode6_EditValueChanged);
            this.LueItCode6.Leave += new System.EventHandler(this.LueItCode6_Leave);
            this.LueItCode6.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueItCode6_KeyDown);
            // 
            // Grd16
            // 
            this.Grd16.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd16.DefaultRow.Height = 20;
            this.Grd16.DefaultRow.Sortable = false;
            this.Grd16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd16.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd16.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd16.Header.Height = 20;
            this.Grd16.Location = new System.Drawing.Point(0, 33);
            this.Grd16.Name = "Grd16";
            this.Grd16.RowHeader.Visible = true;
            this.Grd16.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd16.SingleClickEdit = true;
            this.Grd16.Size = new System.Drawing.Size(764, 320);
            this.Grd16.TabIndex = 38;
            this.Grd16.TreeCol = null;
            this.Grd16.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd16.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd16_RequestEdit);
            this.Grd16.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd16_KeyDown);
            this.Grd16.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd16_ColHdrDoubleClick);
            this.Grd16.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd16_AfterCommitEdit);
            this.Grd16.CustomDrawCellForeground += new TenTec.Windows.iGridLib.iGCustomDrawCellEventHandler(this.Grd16_CustomDrawCellForeground);
            // 
            // panel13
            // 
            this.panel13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel13.Controls.Add(this.label58);
            this.panel13.Controls.Add(this.TxtLabelQty6);
            this.panel13.Controls.Add(this.label48);
            this.panel13.Controls.Add(this.LueWhsCode6);
            this.panel13.Controls.Add(this.LueShelf6);
            this.panel13.Controls.Add(this.label16);
            this.panel13.Controls.Add(this.TxtTotal6);
            this.panel13.Controls.Add(this.label23);
            this.panel13.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel13.Location = new System.Drawing.Point(0, 0);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(764, 33);
            this.panel13.TabIndex = 29;
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label58.ForeColor = System.Drawing.Color.Black;
            this.label58.Location = new System.Drawing.Point(304, 9);
            this.label58.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(35, 14);
            this.label58.TabIndex = 34;
            this.label58.Text = "Label";
            this.label58.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtLabelQty6
            // 
            this.TxtLabelQty6.EditValue = "";
            this.TxtLabelQty6.EnterMoveNextControl = true;
            this.TxtLabelQty6.Location = new System.Drawing.Point(342, 6);
            this.TxtLabelQty6.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLabelQty6.Name = "TxtLabelQty6";
            this.TxtLabelQty6.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLabelQty6.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLabelQty6.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLabelQty6.Properties.Appearance.Options.UseFont = true;
            this.TxtLabelQty6.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtLabelQty6.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtLabelQty6.Size = new System.Drawing.Size(77, 20);
            this.TxtLabelQty6.TabIndex = 35;
            this.TxtLabelQty6.Validated += new System.EventHandler(this.TxtLabelQty6_Validated);
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.ForeColor = System.Drawing.Color.Black;
            this.label48.Location = new System.Drawing.Point(428, 9);
            this.label48.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(69, 14);
            this.label48.TabIndex = 36;
            this.label48.Text = "Warehouse";
            this.label48.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueWhsCode6
            // 
            this.LueWhsCode6.EnterMoveNextControl = true;
            this.LueWhsCode6.Location = new System.Drawing.Point(502, 6);
            this.LueWhsCode6.Margin = new System.Windows.Forms.Padding(5);
            this.LueWhsCode6.Name = "LueWhsCode6";
            this.LueWhsCode6.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode6.Properties.Appearance.Options.UseFont = true;
            this.LueWhsCode6.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode6.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueWhsCode6.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode6.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueWhsCode6.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode6.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueWhsCode6.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode6.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueWhsCode6.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueWhsCode6.Properties.DropDownRows = 20;
            this.LueWhsCode6.Properties.NullText = "[Empty]";
            this.LueWhsCode6.Properties.PopupWidth = 500;
            this.LueWhsCode6.Size = new System.Drawing.Size(249, 20);
            this.LueWhsCode6.TabIndex = 37;
            this.LueWhsCode6.ToolTip = "F4 : Show/hide list";
            this.LueWhsCode6.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueWhsCode6.EditValueChanged += new System.EventHandler(this.LueWhsCode6_EditValueChanged);
            // 
            // LueShelf6
            // 
            this.LueShelf6.EnterMoveNextControl = true;
            this.LueShelf6.Location = new System.Drawing.Point(35, 6);
            this.LueShelf6.Margin = new System.Windows.Forms.Padding(5);
            this.LueShelf6.Name = "LueShelf6";
            this.LueShelf6.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf6.Properties.Appearance.Options.UseFont = true;
            this.LueShelf6.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf6.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueShelf6.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf6.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueShelf6.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf6.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueShelf6.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf6.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueShelf6.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueShelf6.Properties.DropDownRows = 20;
            this.LueShelf6.Properties.NullText = "[Empty]";
            this.LueShelf6.Properties.PopupWidth = 500;
            this.LueShelf6.Size = new System.Drawing.Size(132, 20);
            this.LueShelf6.TabIndex = 31;
            this.LueShelf6.ToolTip = "F4 : Show/hide list";
            this.LueShelf6.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueShelf6.EditValueChanged += new System.EventHandler(this.LueShelf6_EditValueChanged);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(172, 9);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(35, 14);
            this.label16.TabIndex = 32;
            this.label16.Text = "Total";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal6
            // 
            this.TxtTotal6.EnterMoveNextControl = true;
            this.TxtTotal6.Location = new System.Drawing.Point(209, 6);
            this.TxtTotal6.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal6.Name = "TxtTotal6";
            this.TxtTotal6.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal6.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal6.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal6.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal6.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotal6.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotal6.Size = new System.Drawing.Size(90, 20);
            this.TxtTotal6.TabIndex = 33;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Black;
            this.label23.Location = new System.Drawing.Point(6, 9);
            this.label23.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(26, 14);
            this.label23.TabIndex = 30;
            this.label23.Text = "Rak";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage7
            // 
            this.tabPage7.Controls.Add(this.LueItCode7);
            this.tabPage7.Controls.Add(this.Grd17);
            this.tabPage7.Controls.Add(this.panel14);
            this.tabPage7.Location = new System.Drawing.Point(4, 23);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Size = new System.Drawing.Size(764, 353);
            this.tabPage7.TabIndex = 6;
            this.tabPage7.Text = "No.7";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // LueItCode7
            // 
            this.LueItCode7.EnterMoveNextControl = true;
            this.LueItCode7.Location = new System.Drawing.Point(19, 54);
            this.LueItCode7.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode7.Name = "LueItCode7";
            this.LueItCode7.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode7.Properties.Appearance.Options.UseFont = true;
            this.LueItCode7.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode7.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode7.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode7.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode7.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode7.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode7.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode7.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode7.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode7.Properties.DropDownRows = 20;
            this.LueItCode7.Properties.NullText = "[Empty]";
            this.LueItCode7.Properties.PopupWidth = 500;
            this.LueItCode7.Size = new System.Drawing.Size(163, 20);
            this.LueItCode7.TabIndex = 39;
            this.LueItCode7.ToolTip = "F4 : Show/hide list";
            this.LueItCode7.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueItCode7.EditValueChanged += new System.EventHandler(this.LueItCode7_EditValueChanged);
            this.LueItCode7.Leave += new System.EventHandler(this.LueItCode7_Leave);
            this.LueItCode7.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueItCode7_KeyDown);
            // 
            // Grd17
            // 
            this.Grd17.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd17.DefaultRow.Height = 20;
            this.Grd17.DefaultRow.Sortable = false;
            this.Grd17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd17.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd17.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd17.Header.Height = 20;
            this.Grd17.Location = new System.Drawing.Point(0, 32);
            this.Grd17.Name = "Grd17";
            this.Grd17.RowHeader.Visible = true;
            this.Grd17.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd17.SingleClickEdit = true;
            this.Grd17.Size = new System.Drawing.Size(764, 321);
            this.Grd17.TabIndex = 38;
            this.Grd17.TreeCol = null;
            this.Grd17.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd17.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd17_RequestEdit);
            this.Grd17.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd17_KeyDown);
            this.Grd17.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd17_ColHdrDoubleClick);
            this.Grd17.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd17_AfterCommitEdit);
            this.Grd17.CustomDrawCellForeground += new TenTec.Windows.iGridLib.iGCustomDrawCellEventHandler(this.Grd17_CustomDrawCellForeground);
            // 
            // panel14
            // 
            this.panel14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel14.Controls.Add(this.label59);
            this.panel14.Controls.Add(this.TxtLabelQty7);
            this.panel14.Controls.Add(this.label49);
            this.panel14.Controls.Add(this.LueWhsCode7);
            this.panel14.Controls.Add(this.LueShelf7);
            this.panel14.Controls.Add(this.label20);
            this.panel14.Controls.Add(this.TxtTotal7);
            this.panel14.Controls.Add(this.label25);
            this.panel14.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel14.Location = new System.Drawing.Point(0, 0);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(764, 32);
            this.panel14.TabIndex = 29;
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label59.ForeColor = System.Drawing.Color.Black;
            this.label59.Location = new System.Drawing.Point(304, 9);
            this.label59.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(35, 14);
            this.label59.TabIndex = 34;
            this.label59.Text = "Label";
            this.label59.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtLabelQty7
            // 
            this.TxtLabelQty7.EditValue = "";
            this.TxtLabelQty7.EnterMoveNextControl = true;
            this.TxtLabelQty7.Location = new System.Drawing.Point(342, 6);
            this.TxtLabelQty7.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLabelQty7.Name = "TxtLabelQty7";
            this.TxtLabelQty7.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLabelQty7.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLabelQty7.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLabelQty7.Properties.Appearance.Options.UseFont = true;
            this.TxtLabelQty7.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtLabelQty7.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtLabelQty7.Size = new System.Drawing.Size(77, 20);
            this.TxtLabelQty7.TabIndex = 35;
            this.TxtLabelQty7.Validated += new System.EventHandler(this.TxtLabelQty7_Validated);
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.ForeColor = System.Drawing.Color.Black;
            this.label49.Location = new System.Drawing.Point(420, 9);
            this.label49.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(69, 14);
            this.label49.TabIndex = 36;
            this.label49.Text = "Warehouse";
            this.label49.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueWhsCode7
            // 
            this.LueWhsCode7.EnterMoveNextControl = true;
            this.LueWhsCode7.Location = new System.Drawing.Point(494, 6);
            this.LueWhsCode7.Margin = new System.Windows.Forms.Padding(5);
            this.LueWhsCode7.Name = "LueWhsCode7";
            this.LueWhsCode7.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode7.Properties.Appearance.Options.UseFont = true;
            this.LueWhsCode7.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode7.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueWhsCode7.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode7.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueWhsCode7.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode7.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueWhsCode7.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode7.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueWhsCode7.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueWhsCode7.Properties.DropDownRows = 20;
            this.LueWhsCode7.Properties.NullText = "[Empty]";
            this.LueWhsCode7.Properties.PopupWidth = 500;
            this.LueWhsCode7.Size = new System.Drawing.Size(261, 20);
            this.LueWhsCode7.TabIndex = 37;
            this.LueWhsCode7.ToolTip = "F4 : Show/hide list";
            this.LueWhsCode7.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueWhsCode7.EditValueChanged += new System.EventHandler(this.LueWhsCode7_EditValueChanged);
            // 
            // LueShelf7
            // 
            this.LueShelf7.EnterMoveNextControl = true;
            this.LueShelf7.Location = new System.Drawing.Point(36, 6);
            this.LueShelf7.Margin = new System.Windows.Forms.Padding(5);
            this.LueShelf7.Name = "LueShelf7";
            this.LueShelf7.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf7.Properties.Appearance.Options.UseFont = true;
            this.LueShelf7.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf7.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueShelf7.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf7.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueShelf7.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf7.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueShelf7.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf7.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueShelf7.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueShelf7.Properties.DropDownRows = 20;
            this.LueShelf7.Properties.NullText = "[Empty]";
            this.LueShelf7.Properties.PopupWidth = 500;
            this.LueShelf7.Size = new System.Drawing.Size(130, 20);
            this.LueShelf7.TabIndex = 31;
            this.LueShelf7.ToolTip = "F4 : Show/hide list";
            this.LueShelf7.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueShelf7.EditValueChanged += new System.EventHandler(this.LueShelf7_EditValueChanged);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(172, 9);
            this.label20.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(35, 14);
            this.label20.TabIndex = 32;
            this.label20.Text = "Total";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal7
            // 
            this.TxtTotal7.EnterMoveNextControl = true;
            this.TxtTotal7.Location = new System.Drawing.Point(209, 6);
            this.TxtTotal7.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal7.Name = "TxtTotal7";
            this.TxtTotal7.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal7.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal7.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal7.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal7.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotal7.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotal7.Size = new System.Drawing.Size(90, 20);
            this.TxtTotal7.TabIndex = 33;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Black;
            this.label25.Location = new System.Drawing.Point(8, 9);
            this.label25.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(26, 14);
            this.label25.TabIndex = 30;
            this.label25.Text = "Rak";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage8
            // 
            this.tabPage8.Controls.Add(this.LueItCode8);
            this.tabPage8.Controls.Add(this.Grd18);
            this.tabPage8.Controls.Add(this.panel15);
            this.tabPage8.Location = new System.Drawing.Point(4, 23);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Size = new System.Drawing.Size(764, 353);
            this.tabPage8.TabIndex = 7;
            this.tabPage8.Text = "No.8";
            this.tabPage8.UseVisualStyleBackColor = true;
            // 
            // LueItCode8
            // 
            this.LueItCode8.EnterMoveNextControl = true;
            this.LueItCode8.Location = new System.Drawing.Point(20, 54);
            this.LueItCode8.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode8.Name = "LueItCode8";
            this.LueItCode8.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode8.Properties.Appearance.Options.UseFont = true;
            this.LueItCode8.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode8.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode8.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode8.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode8.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode8.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode8.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode8.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode8.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode8.Properties.DropDownRows = 20;
            this.LueItCode8.Properties.NullText = "[Empty]";
            this.LueItCode8.Properties.PopupWidth = 500;
            this.LueItCode8.Size = new System.Drawing.Size(163, 20);
            this.LueItCode8.TabIndex = 39;
            this.LueItCode8.ToolTip = "F4 : Show/hide list";
            this.LueItCode8.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueItCode8.EditValueChanged += new System.EventHandler(this.LueItCode8_EditValueChanged);
            this.LueItCode8.Leave += new System.EventHandler(this.LueItCode8_Leave);
            this.LueItCode8.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueItCode8_KeyDown);
            // 
            // Grd18
            // 
            this.Grd18.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd18.DefaultRow.Height = 20;
            this.Grd18.DefaultRow.Sortable = false;
            this.Grd18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd18.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd18.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd18.Header.Height = 20;
            this.Grd18.Location = new System.Drawing.Point(0, 32);
            this.Grd18.Name = "Grd18";
            this.Grd18.RowHeader.Visible = true;
            this.Grd18.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd18.SingleClickEdit = true;
            this.Grd18.Size = new System.Drawing.Size(764, 321);
            this.Grd18.TabIndex = 38;
            this.Grd18.TreeCol = null;
            this.Grd18.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd18.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd18_RequestEdit);
            this.Grd18.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd18_KeyDown);
            this.Grd18.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd18_ColHdrDoubleClick);
            this.Grd18.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd18_AfterCommitEdit);
            this.Grd18.CustomDrawCellForeground += new TenTec.Windows.iGridLib.iGCustomDrawCellEventHandler(this.Grd18_CustomDrawCellForeground);
            // 
            // panel15
            // 
            this.panel15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel15.Controls.Add(this.label60);
            this.panel15.Controls.Add(this.TxtLabelQty8);
            this.panel15.Controls.Add(this.label50);
            this.panel15.Controls.Add(this.LueWhsCode8);
            this.panel15.Controls.Add(this.LueShelf8);
            this.panel15.Controls.Add(this.label22);
            this.panel15.Controls.Add(this.TxtTotal8);
            this.panel15.Controls.Add(this.label27);
            this.panel15.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel15.Location = new System.Drawing.Point(0, 0);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(764, 32);
            this.panel15.TabIndex = 29;
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label60.ForeColor = System.Drawing.Color.Black;
            this.label60.Location = new System.Drawing.Point(305, 9);
            this.label60.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(35, 14);
            this.label60.TabIndex = 34;
            this.label60.Text = "Label";
            this.label60.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtLabelQty8
            // 
            this.TxtLabelQty8.EditValue = "";
            this.TxtLabelQty8.EnterMoveNextControl = true;
            this.TxtLabelQty8.Location = new System.Drawing.Point(343, 6);
            this.TxtLabelQty8.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLabelQty8.Name = "TxtLabelQty8";
            this.TxtLabelQty8.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLabelQty8.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLabelQty8.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLabelQty8.Properties.Appearance.Options.UseFont = true;
            this.TxtLabelQty8.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtLabelQty8.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtLabelQty8.Size = new System.Drawing.Size(77, 20);
            this.TxtLabelQty8.TabIndex = 35;
            this.TxtLabelQty8.Validated += new System.EventHandler(this.TxtLabelQty8_Validated);
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.ForeColor = System.Drawing.Color.Black;
            this.label50.Location = new System.Drawing.Point(424, 9);
            this.label50.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(69, 14);
            this.label50.TabIndex = 36;
            this.label50.Text = "Warehouse";
            this.label50.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueWhsCode8
            // 
            this.LueWhsCode8.EnterMoveNextControl = true;
            this.LueWhsCode8.Location = new System.Drawing.Point(498, 6);
            this.LueWhsCode8.Margin = new System.Windows.Forms.Padding(5);
            this.LueWhsCode8.Name = "LueWhsCode8";
            this.LueWhsCode8.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode8.Properties.Appearance.Options.UseFont = true;
            this.LueWhsCode8.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode8.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueWhsCode8.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode8.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueWhsCode8.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode8.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueWhsCode8.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode8.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueWhsCode8.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueWhsCode8.Properties.DropDownRows = 20;
            this.LueWhsCode8.Properties.NullText = "[Empty]";
            this.LueWhsCode8.Properties.PopupWidth = 500;
            this.LueWhsCode8.Size = new System.Drawing.Size(258, 20);
            this.LueWhsCode8.TabIndex = 37;
            this.LueWhsCode8.ToolTip = "F4 : Show/hide list";
            this.LueWhsCode8.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueWhsCode8.EditValueChanged += new System.EventHandler(this.LueWhsCode8_EditValueChanged);
            // 
            // LueShelf8
            // 
            this.LueShelf8.EnterMoveNextControl = true;
            this.LueShelf8.Location = new System.Drawing.Point(37, 6);
            this.LueShelf8.Margin = new System.Windows.Forms.Padding(5);
            this.LueShelf8.Name = "LueShelf8";
            this.LueShelf8.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf8.Properties.Appearance.Options.UseFont = true;
            this.LueShelf8.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf8.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueShelf8.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf8.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueShelf8.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf8.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueShelf8.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf8.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueShelf8.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueShelf8.Properties.DropDownRows = 20;
            this.LueShelf8.Properties.NullText = "[Empty]";
            this.LueShelf8.Properties.PopupWidth = 500;
            this.LueShelf8.Size = new System.Drawing.Size(129, 20);
            this.LueShelf8.TabIndex = 31;
            this.LueShelf8.ToolTip = "F4 : Show/hide list";
            this.LueShelf8.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueShelf8.EditValueChanged += new System.EventHandler(this.LueShelf8_EditValueChanged);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(171, 9);
            this.label22.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(35, 14);
            this.label22.TabIndex = 32;
            this.label22.Text = "Total";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal8
            // 
            this.TxtTotal8.EnterMoveNextControl = true;
            this.TxtTotal8.Location = new System.Drawing.Point(208, 6);
            this.TxtTotal8.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal8.Name = "TxtTotal8";
            this.TxtTotal8.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal8.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal8.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal8.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal8.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotal8.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotal8.Size = new System.Drawing.Size(90, 20);
            this.TxtTotal8.TabIndex = 33;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Black;
            this.label27.Location = new System.Drawing.Point(8, 9);
            this.label27.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(26, 14);
            this.label27.TabIndex = 30;
            this.label27.Text = "Rak";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage9
            // 
            this.tabPage9.Controls.Add(this.LueItCode9);
            this.tabPage9.Controls.Add(this.Grd19);
            this.tabPage9.Controls.Add(this.panel16);
            this.tabPage9.Location = new System.Drawing.Point(4, 23);
            this.tabPage9.Name = "tabPage9";
            this.tabPage9.Size = new System.Drawing.Size(764, 353);
            this.tabPage9.TabIndex = 8;
            this.tabPage9.Text = "No.9";
            this.tabPage9.UseVisualStyleBackColor = true;
            // 
            // LueItCode9
            // 
            this.LueItCode9.EnterMoveNextControl = true;
            this.LueItCode9.Location = new System.Drawing.Point(21, 54);
            this.LueItCode9.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode9.Name = "LueItCode9";
            this.LueItCode9.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode9.Properties.Appearance.Options.UseFont = true;
            this.LueItCode9.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode9.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode9.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode9.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode9.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode9.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode9.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode9.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode9.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode9.Properties.DropDownRows = 20;
            this.LueItCode9.Properties.NullText = "[Empty]";
            this.LueItCode9.Properties.PopupWidth = 500;
            this.LueItCode9.Size = new System.Drawing.Size(163, 20);
            this.LueItCode9.TabIndex = 39;
            this.LueItCode9.ToolTip = "F4 : Show/hide list";
            this.LueItCode9.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueItCode9.EditValueChanged += new System.EventHandler(this.LueItCode9_EditValueChanged);
            this.LueItCode9.Leave += new System.EventHandler(this.LueItCode9_Leave);
            this.LueItCode9.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueItCode9_KeyDown);
            // 
            // Grd19
            // 
            this.Grd19.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd19.DefaultRow.Height = 20;
            this.Grd19.DefaultRow.Sortable = false;
            this.Grd19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd19.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd19.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd19.Header.Height = 20;
            this.Grd19.Location = new System.Drawing.Point(0, 32);
            this.Grd19.Name = "Grd19";
            this.Grd19.RowHeader.Visible = true;
            this.Grd19.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd19.SingleClickEdit = true;
            this.Grd19.Size = new System.Drawing.Size(764, 321);
            this.Grd19.TabIndex = 38;
            this.Grd19.TreeCol = null;
            this.Grd19.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd19.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd19_RequestEdit);
            this.Grd19.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd19_KeyDown);
            this.Grd19.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd19_ColHdrDoubleClick);
            this.Grd19.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd19_AfterCommitEdit);
            this.Grd19.CustomDrawCellForeground += new TenTec.Windows.iGridLib.iGCustomDrawCellEventHandler(this.Grd19_CustomDrawCellForeground);
            // 
            // panel16
            // 
            this.panel16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel16.Controls.Add(this.label61);
            this.panel16.Controls.Add(this.TxtLabelQty9);
            this.panel16.Controls.Add(this.label51);
            this.panel16.Controls.Add(this.LueWhsCode9);
            this.panel16.Controls.Add(this.LueShelf9);
            this.panel16.Controls.Add(this.label24);
            this.panel16.Controls.Add(this.TxtTotal9);
            this.panel16.Controls.Add(this.label29);
            this.panel16.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel16.Location = new System.Drawing.Point(0, 0);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(764, 32);
            this.panel16.TabIndex = 29;
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label61.ForeColor = System.Drawing.Color.Black;
            this.label61.Location = new System.Drawing.Point(301, 9);
            this.label61.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(35, 14);
            this.label61.TabIndex = 34;
            this.label61.Text = "Label";
            this.label61.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtLabelQty9
            // 
            this.TxtLabelQty9.EditValue = "";
            this.TxtLabelQty9.EnterMoveNextControl = true;
            this.TxtLabelQty9.Location = new System.Drawing.Point(339, 6);
            this.TxtLabelQty9.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLabelQty9.Name = "TxtLabelQty9";
            this.TxtLabelQty9.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLabelQty9.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLabelQty9.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLabelQty9.Properties.Appearance.Options.UseFont = true;
            this.TxtLabelQty9.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtLabelQty9.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtLabelQty9.Size = new System.Drawing.Size(77, 20);
            this.TxtLabelQty9.TabIndex = 35;
            this.TxtLabelQty9.Validated += new System.EventHandler(this.TxtLabelQty9_Validated);
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.ForeColor = System.Drawing.Color.Black;
            this.label51.Location = new System.Drawing.Point(418, 9);
            this.label51.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(69, 14);
            this.label51.TabIndex = 36;
            this.label51.Text = "Warehouse";
            this.label51.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueWhsCode9
            // 
            this.LueWhsCode9.EnterMoveNextControl = true;
            this.LueWhsCode9.Location = new System.Drawing.Point(492, 6);
            this.LueWhsCode9.Margin = new System.Windows.Forms.Padding(5);
            this.LueWhsCode9.Name = "LueWhsCode9";
            this.LueWhsCode9.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode9.Properties.Appearance.Options.UseFont = true;
            this.LueWhsCode9.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode9.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueWhsCode9.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode9.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueWhsCode9.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode9.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueWhsCode9.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode9.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueWhsCode9.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueWhsCode9.Properties.DropDownRows = 20;
            this.LueWhsCode9.Properties.NullText = "[Empty]";
            this.LueWhsCode9.Properties.PopupWidth = 500;
            this.LueWhsCode9.Size = new System.Drawing.Size(262, 20);
            this.LueWhsCode9.TabIndex = 37;
            this.LueWhsCode9.ToolTip = "F4 : Show/hide list";
            this.LueWhsCode9.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueWhsCode9.EditValueChanged += new System.EventHandler(this.LueWhsCode9_EditValueChanged);
            // 
            // LueShelf9
            // 
            this.LueShelf9.EnterMoveNextControl = true;
            this.LueShelf9.Location = new System.Drawing.Point(35, 6);
            this.LueShelf9.Margin = new System.Windows.Forms.Padding(5);
            this.LueShelf9.Name = "LueShelf9";
            this.LueShelf9.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf9.Properties.Appearance.Options.UseFont = true;
            this.LueShelf9.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf9.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueShelf9.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf9.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueShelf9.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf9.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueShelf9.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf9.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueShelf9.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueShelf9.Properties.DropDownRows = 20;
            this.LueShelf9.Properties.NullText = "[Empty]";
            this.LueShelf9.Properties.PopupWidth = 500;
            this.LueShelf9.Size = new System.Drawing.Size(132, 20);
            this.LueShelf9.TabIndex = 31;
            this.LueShelf9.ToolTip = "F4 : Show/hide list";
            this.LueShelf9.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueShelf9.EditValueChanged += new System.EventHandler(this.LueShelf9_EditValueChanged);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Black;
            this.label24.Location = new System.Drawing.Point(172, 9);
            this.label24.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(35, 14);
            this.label24.TabIndex = 32;
            this.label24.Text = "Total";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal9
            // 
            this.TxtTotal9.EnterMoveNextControl = true;
            this.TxtTotal9.Location = new System.Drawing.Point(209, 6);
            this.TxtTotal9.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal9.Name = "TxtTotal9";
            this.TxtTotal9.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal9.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal9.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal9.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal9.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotal9.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotal9.Size = new System.Drawing.Size(90, 20);
            this.TxtTotal9.TabIndex = 33;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Black;
            this.label29.Location = new System.Drawing.Point(6, 9);
            this.label29.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(26, 14);
            this.label29.TabIndex = 30;
            this.label29.Text = "Rak";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage10
            // 
            this.tabPage10.Controls.Add(this.LueItCode10);
            this.tabPage10.Controls.Add(this.Grd20);
            this.tabPage10.Controls.Add(this.panel17);
            this.tabPage10.Location = new System.Drawing.Point(4, 23);
            this.tabPage10.Name = "tabPage10";
            this.tabPage10.Size = new System.Drawing.Size(764, 353);
            this.tabPage10.TabIndex = 9;
            this.tabPage10.Text = "No.10";
            this.tabPage10.UseVisualStyleBackColor = true;
            // 
            // LueItCode10
            // 
            this.LueItCode10.EnterMoveNextControl = true;
            this.LueItCode10.Location = new System.Drawing.Point(20, 54);
            this.LueItCode10.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode10.Name = "LueItCode10";
            this.LueItCode10.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode10.Properties.Appearance.Options.UseFont = true;
            this.LueItCode10.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode10.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode10.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode10.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode10.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode10.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode10.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode10.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode10.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode10.Properties.DropDownRows = 20;
            this.LueItCode10.Properties.NullText = "[Empty]";
            this.LueItCode10.Properties.PopupWidth = 500;
            this.LueItCode10.Size = new System.Drawing.Size(163, 20);
            this.LueItCode10.TabIndex = 39;
            this.LueItCode10.TabStop = false;
            this.LueItCode10.ToolTip = "F4 : Show/hide list";
            this.LueItCode10.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueItCode10.EditValueChanged += new System.EventHandler(this.LueItCode10_EditValueChanged);
            this.LueItCode10.Leave += new System.EventHandler(this.LueItCode10_Leave);
            this.LueItCode10.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueItCode10_KeyDown);
            // 
            // Grd20
            // 
            this.Grd20.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd20.DefaultRow.Height = 20;
            this.Grd20.DefaultRow.Sortable = false;
            this.Grd20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd20.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd20.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd20.Header.Height = 20;
            this.Grd20.Location = new System.Drawing.Point(0, 33);
            this.Grd20.Name = "Grd20";
            this.Grd20.RowHeader.Visible = true;
            this.Grd20.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd20.SingleClickEdit = true;
            this.Grd20.Size = new System.Drawing.Size(764, 320);
            this.Grd20.TabIndex = 38;
            this.Grd20.TreeCol = null;
            this.Grd20.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd20.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd20_RequestEdit);
            this.Grd20.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd20_KeyDown);
            this.Grd20.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd20_ColHdrDoubleClick);
            this.Grd20.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd20_AfterCommitEdit);
            this.Grd20.CustomDrawCellForeground += new TenTec.Windows.iGridLib.iGCustomDrawCellEventHandler(this.Grd20_CustomDrawCellForeground);
            // 
            // panel17
            // 
            this.panel17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel17.Controls.Add(this.label62);
            this.panel17.Controls.Add(this.TxtLabelQty10);
            this.panel17.Controls.Add(this.label52);
            this.panel17.Controls.Add(this.LueWhsCode10);
            this.panel17.Controls.Add(this.LueShelf10);
            this.panel17.Controls.Add(this.label26);
            this.panel17.Controls.Add(this.TxtTotal10);
            this.panel17.Controls.Add(this.label31);
            this.panel17.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel17.Location = new System.Drawing.Point(0, 0);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(764, 33);
            this.panel17.TabIndex = 29;
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label62.ForeColor = System.Drawing.Color.Black;
            this.label62.Location = new System.Drawing.Point(301, 9);
            this.label62.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(35, 14);
            this.label62.TabIndex = 34;
            this.label62.Text = "Label";
            this.label62.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtLabelQty10
            // 
            this.TxtLabelQty10.EditValue = "";
            this.TxtLabelQty10.EnterMoveNextControl = true;
            this.TxtLabelQty10.Location = new System.Drawing.Point(339, 6);
            this.TxtLabelQty10.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLabelQty10.Name = "TxtLabelQty10";
            this.TxtLabelQty10.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLabelQty10.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLabelQty10.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLabelQty10.Properties.Appearance.Options.UseFont = true;
            this.TxtLabelQty10.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtLabelQty10.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtLabelQty10.Size = new System.Drawing.Size(77, 20);
            this.TxtLabelQty10.TabIndex = 35;
            this.TxtLabelQty10.Validated += new System.EventHandler(this.TxtLabelQty10_Validated);
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.ForeColor = System.Drawing.Color.Black;
            this.label52.Location = new System.Drawing.Point(421, 9);
            this.label52.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(69, 14);
            this.label52.TabIndex = 36;
            this.label52.Text = "Warehouse";
            this.label52.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueWhsCode10
            // 
            this.LueWhsCode10.EnterMoveNextControl = true;
            this.LueWhsCode10.Location = new System.Drawing.Point(495, 6);
            this.LueWhsCode10.Margin = new System.Windows.Forms.Padding(5);
            this.LueWhsCode10.Name = "LueWhsCode10";
            this.LueWhsCode10.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode10.Properties.Appearance.Options.UseFont = true;
            this.LueWhsCode10.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode10.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueWhsCode10.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode10.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueWhsCode10.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode10.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueWhsCode10.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode10.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueWhsCode10.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueWhsCode10.Properties.DropDownRows = 20;
            this.LueWhsCode10.Properties.NullText = "[Empty]";
            this.LueWhsCode10.Properties.PopupWidth = 500;
            this.LueWhsCode10.Size = new System.Drawing.Size(259, 20);
            this.LueWhsCode10.TabIndex = 37;
            this.LueWhsCode10.ToolTip = "F4 : Show/hide list";
            this.LueWhsCode10.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueWhsCode10.EditValueChanged += new System.EventHandler(this.LueWhsCode10_EditValueChanged);
            // 
            // LueShelf10
            // 
            this.LueShelf10.EnterMoveNextControl = true;
            this.LueShelf10.Location = new System.Drawing.Point(38, 6);
            this.LueShelf10.Margin = new System.Windows.Forms.Padding(5);
            this.LueShelf10.Name = "LueShelf10";
            this.LueShelf10.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf10.Properties.Appearance.Options.UseFont = true;
            this.LueShelf10.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf10.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueShelf10.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf10.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueShelf10.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf10.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueShelf10.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf10.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueShelf10.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueShelf10.Properties.DropDownRows = 20;
            this.LueShelf10.Properties.NullText = "[Empty]";
            this.LueShelf10.Properties.PopupWidth = 500;
            this.LueShelf10.Size = new System.Drawing.Size(127, 20);
            this.LueShelf10.TabIndex = 31;
            this.LueShelf10.ToolTip = "F4 : Show/hide list";
            this.LueShelf10.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueShelf10.EditValueChanged += new System.EventHandler(this.LueShelf10_EditValueChanged);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Black;
            this.label26.Location = new System.Drawing.Point(169, 9);
            this.label26.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(35, 14);
            this.label26.TabIndex = 32;
            this.label26.Text = "Total";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal10
            // 
            this.TxtTotal10.EnterMoveNextControl = true;
            this.TxtTotal10.Location = new System.Drawing.Point(206, 6);
            this.TxtTotal10.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal10.Name = "TxtTotal10";
            this.TxtTotal10.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal10.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal10.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal10.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal10.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotal10.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotal10.Size = new System.Drawing.Size(90, 20);
            this.TxtTotal10.TabIndex = 33;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.Black;
            this.label31.Location = new System.Drawing.Point(8, 9);
            this.label31.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(26, 14);
            this.label31.TabIndex = 30;
            this.label31.Text = "Rak";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage11
            // 
            this.tabPage11.Controls.Add(this.LueItCode11);
            this.tabPage11.Controls.Add(this.Grd21);
            this.tabPage11.Controls.Add(this.panel5);
            this.tabPage11.Location = new System.Drawing.Point(4, 23);
            this.tabPage11.Name = "tabPage11";
            this.tabPage11.Size = new System.Drawing.Size(764, 353);
            this.tabPage11.TabIndex = 10;
            this.tabPage11.Text = "No.11";
            this.tabPage11.UseVisualStyleBackColor = true;
            // 
            // LueItCode11
            // 
            this.LueItCode11.EnterMoveNextControl = true;
            this.LueItCode11.Location = new System.Drawing.Point(21, 55);
            this.LueItCode11.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode11.Name = "LueItCode11";
            this.LueItCode11.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode11.Properties.Appearance.Options.UseFont = true;
            this.LueItCode11.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode11.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode11.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode11.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode11.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode11.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode11.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode11.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode11.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode11.Properties.DropDownRows = 20;
            this.LueItCode11.Properties.NullText = "[Empty]";
            this.LueItCode11.Properties.PopupWidth = 500;
            this.LueItCode11.Size = new System.Drawing.Size(163, 20);
            this.LueItCode11.TabIndex = 39;
            this.LueItCode11.TabStop = false;
            this.LueItCode11.ToolTip = "F4 : Show/hide list";
            this.LueItCode11.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueItCode11.EditValueChanged += new System.EventHandler(this.LueItCode11_EditValueChanged);
            this.LueItCode11.Leave += new System.EventHandler(this.LueItCode11_Leave);
            this.LueItCode11.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueItCode11_KeyDown);
            // 
            // Grd21
            // 
            this.Grd21.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd21.DefaultRow.Height = 20;
            this.Grd21.DefaultRow.Sortable = false;
            this.Grd21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd21.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd21.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd21.Header.Height = 20;
            this.Grd21.Location = new System.Drawing.Point(0, 33);
            this.Grd21.Name = "Grd21";
            this.Grd21.RowHeader.Visible = true;
            this.Grd21.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd21.SingleClickEdit = true;
            this.Grd21.Size = new System.Drawing.Size(764, 320);
            this.Grd21.TabIndex = 38;
            this.Grd21.TreeCol = null;
            this.Grd21.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd21.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd21_RequestEdit);
            this.Grd21.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd21_KeyDown);
            this.Grd21.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd21_ColHdrDoubleClick);
            this.Grd21.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd21_AfterCommitEdit);
            this.Grd21.CustomDrawCellForeground += new TenTec.Windows.iGridLib.iGCustomDrawCellEventHandler(this.Grd21_CustomDrawCellForeground);
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel5.Controls.Add(this.label63);
            this.panel5.Controls.Add(this.TxtLabelQty11);
            this.panel5.Controls.Add(this.label53);
            this.panel5.Controls.Add(this.LueWhsCode11);
            this.panel5.Controls.Add(this.LueShelf11);
            this.panel5.Controls.Add(this.label30);
            this.panel5.Controls.Add(this.TxtTotal11);
            this.panel5.Controls.Add(this.label32);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(764, 33);
            this.panel5.TabIndex = 29;
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label63.ForeColor = System.Drawing.Color.Black;
            this.label63.Location = new System.Drawing.Point(303, 9);
            this.label63.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(35, 14);
            this.label63.TabIndex = 34;
            this.label63.Text = "Label";
            this.label63.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtLabelQty11
            // 
            this.TxtLabelQty11.EditValue = "";
            this.TxtLabelQty11.EnterMoveNextControl = true;
            this.TxtLabelQty11.Location = new System.Drawing.Point(341, 6);
            this.TxtLabelQty11.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLabelQty11.Name = "TxtLabelQty11";
            this.TxtLabelQty11.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLabelQty11.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLabelQty11.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLabelQty11.Properties.Appearance.Options.UseFont = true;
            this.TxtLabelQty11.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtLabelQty11.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtLabelQty11.Size = new System.Drawing.Size(77, 20);
            this.TxtLabelQty11.TabIndex = 35;
            this.TxtLabelQty11.Validated += new System.EventHandler(this.TxtLabelQty11_Validated);
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.ForeColor = System.Drawing.Color.Black;
            this.label53.Location = new System.Drawing.Point(424, 9);
            this.label53.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(69, 14);
            this.label53.TabIndex = 36;
            this.label53.Text = "Warehouse";
            this.label53.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueWhsCode11
            // 
            this.LueWhsCode11.EnterMoveNextControl = true;
            this.LueWhsCode11.Location = new System.Drawing.Point(498, 6);
            this.LueWhsCode11.Margin = new System.Windows.Forms.Padding(5);
            this.LueWhsCode11.Name = "LueWhsCode11";
            this.LueWhsCode11.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode11.Properties.Appearance.Options.UseFont = true;
            this.LueWhsCode11.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode11.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueWhsCode11.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode11.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueWhsCode11.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode11.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueWhsCode11.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode11.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueWhsCode11.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueWhsCode11.Properties.DropDownRows = 20;
            this.LueWhsCode11.Properties.NullText = "[Empty]";
            this.LueWhsCode11.Properties.PopupWidth = 500;
            this.LueWhsCode11.Size = new System.Drawing.Size(255, 20);
            this.LueWhsCode11.TabIndex = 37;
            this.LueWhsCode11.ToolTip = "F4 : Show/hide list";
            this.LueWhsCode11.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueWhsCode11.EditValueChanged += new System.EventHandler(this.LueWhsCode11_EditValueChanged);
            // 
            // LueShelf11
            // 
            this.LueShelf11.EnterMoveNextControl = true;
            this.LueShelf11.Location = new System.Drawing.Point(38, 6);
            this.LueShelf11.Margin = new System.Windows.Forms.Padding(5);
            this.LueShelf11.Name = "LueShelf11";
            this.LueShelf11.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf11.Properties.Appearance.Options.UseFont = true;
            this.LueShelf11.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf11.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueShelf11.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf11.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueShelf11.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf11.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueShelf11.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf11.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueShelf11.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueShelf11.Properties.DropDownRows = 20;
            this.LueShelf11.Properties.NullText = "[Empty]";
            this.LueShelf11.Properties.PopupWidth = 500;
            this.LueShelf11.Size = new System.Drawing.Size(129, 20);
            this.LueShelf11.TabIndex = 31;
            this.LueShelf11.ToolTip = "F4 : Show/hide list";
            this.LueShelf11.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueShelf11.EditValueChanged += new System.EventHandler(this.LueShelf11_EditValueChanged);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.Black;
            this.label30.Location = new System.Drawing.Point(172, 9);
            this.label30.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(35, 14);
            this.label30.TabIndex = 32;
            this.label30.Text = "Total";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal11
            // 
            this.TxtTotal11.EnterMoveNextControl = true;
            this.TxtTotal11.Location = new System.Drawing.Point(209, 6);
            this.TxtTotal11.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal11.Name = "TxtTotal11";
            this.TxtTotal11.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal11.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal11.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal11.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal11.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotal11.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotal11.Size = new System.Drawing.Size(90, 20);
            this.TxtTotal11.TabIndex = 33;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.Black;
            this.label32.Location = new System.Drawing.Point(8, 9);
            this.label32.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(26, 14);
            this.label32.TabIndex = 30;
            this.label32.Text = "Rak";
            this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage12
            // 
            this.tabPage12.Controls.Add(this.LueItCode12);
            this.tabPage12.Controls.Add(this.Grd22);
            this.tabPage12.Controls.Add(this.panel18);
            this.tabPage12.Location = new System.Drawing.Point(4, 23);
            this.tabPage12.Name = "tabPage12";
            this.tabPage12.Size = new System.Drawing.Size(764, 353);
            this.tabPage12.TabIndex = 11;
            this.tabPage12.Text = "No.12";
            this.tabPage12.UseVisualStyleBackColor = true;
            // 
            // LueItCode12
            // 
            this.LueItCode12.EnterMoveNextControl = true;
            this.LueItCode12.Location = new System.Drawing.Point(64, 53);
            this.LueItCode12.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode12.Name = "LueItCode12";
            this.LueItCode12.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode12.Properties.Appearance.Options.UseFont = true;
            this.LueItCode12.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode12.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode12.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode12.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode12.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode12.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode12.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode12.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode12.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode12.Properties.DropDownRows = 20;
            this.LueItCode12.Properties.NullText = "[Empty]";
            this.LueItCode12.Properties.PopupWidth = 500;
            this.LueItCode12.Size = new System.Drawing.Size(163, 20);
            this.LueItCode12.TabIndex = 39;
            this.LueItCode12.TabStop = false;
            this.LueItCode12.ToolTip = "F4 : Show/hide list";
            this.LueItCode12.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueItCode12.EditValueChanged += new System.EventHandler(this.LueItCode12_EditValueChanged);
            this.LueItCode12.Leave += new System.EventHandler(this.LueItCode12_Leave);
            this.LueItCode12.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueItCode12_KeyDown);
            // 
            // Grd22
            // 
            this.Grd22.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd22.DefaultRow.Height = 20;
            this.Grd22.DefaultRow.Sortable = false;
            this.Grd22.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd22.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd22.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd22.Header.Height = 20;
            this.Grd22.Location = new System.Drawing.Point(0, 33);
            this.Grd22.Name = "Grd22";
            this.Grd22.RowHeader.Visible = true;
            this.Grd22.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd22.SingleClickEdit = true;
            this.Grd22.Size = new System.Drawing.Size(764, 320);
            this.Grd22.TabIndex = 38;
            this.Grd22.TreeCol = null;
            this.Grd22.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd22.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd22_RequestEdit);
            this.Grd22.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd22_KeyDown);
            this.Grd22.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd22_ColHdrDoubleClick);
            this.Grd22.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd22_AfterCommitEdit);
            this.Grd22.CustomDrawCellForeground += new TenTec.Windows.iGridLib.iGCustomDrawCellEventHandler(this.Grd22_CustomDrawCellForeground);
            // 
            // panel18
            // 
            this.panel18.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel18.Controls.Add(this.label64);
            this.panel18.Controls.Add(this.TxtLabelQty12);
            this.panel18.Controls.Add(this.label54);
            this.panel18.Controls.Add(this.LueWhsCode12);
            this.panel18.Controls.Add(this.LueShelf12);
            this.panel18.Controls.Add(this.label33);
            this.panel18.Controls.Add(this.TxtTotal12);
            this.panel18.Controls.Add(this.label34);
            this.panel18.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel18.Location = new System.Drawing.Point(0, 0);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(764, 33);
            this.panel18.TabIndex = 29;
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label64.ForeColor = System.Drawing.Color.Black;
            this.label64.Location = new System.Drawing.Point(306, 9);
            this.label64.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(35, 14);
            this.label64.TabIndex = 34;
            this.label64.Text = "Label";
            this.label64.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtLabelQty12
            // 
            this.TxtLabelQty12.EditValue = "";
            this.TxtLabelQty12.EnterMoveNextControl = true;
            this.TxtLabelQty12.Location = new System.Drawing.Point(344, 6);
            this.TxtLabelQty12.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLabelQty12.Name = "TxtLabelQty12";
            this.TxtLabelQty12.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLabelQty12.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLabelQty12.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLabelQty12.Properties.Appearance.Options.UseFont = true;
            this.TxtLabelQty12.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtLabelQty12.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtLabelQty12.Size = new System.Drawing.Size(77, 20);
            this.TxtLabelQty12.TabIndex = 35;
            this.TxtLabelQty12.Validated += new System.EventHandler(this.TxtLabelQty12_Validated);
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.ForeColor = System.Drawing.Color.Black;
            this.label54.Location = new System.Drawing.Point(424, 9);
            this.label54.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(69, 14);
            this.label54.TabIndex = 36;
            this.label54.Text = "Warehouse";
            this.label54.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueWhsCode12
            // 
            this.LueWhsCode12.EnterMoveNextControl = true;
            this.LueWhsCode12.Location = new System.Drawing.Point(498, 6);
            this.LueWhsCode12.Margin = new System.Windows.Forms.Padding(5);
            this.LueWhsCode12.Name = "LueWhsCode12";
            this.LueWhsCode12.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode12.Properties.Appearance.Options.UseFont = true;
            this.LueWhsCode12.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode12.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueWhsCode12.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode12.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueWhsCode12.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode12.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueWhsCode12.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode12.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueWhsCode12.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueWhsCode12.Properties.DropDownRows = 20;
            this.LueWhsCode12.Properties.NullText = "[Empty]";
            this.LueWhsCode12.Properties.PopupWidth = 500;
            this.LueWhsCode12.Size = new System.Drawing.Size(255, 20);
            this.LueWhsCode12.TabIndex = 37;
            this.LueWhsCode12.ToolTip = "F4 : Show/hide list";
            this.LueWhsCode12.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueWhsCode12.EditValueChanged += new System.EventHandler(this.LueWhsCode12_EditValueChanged);
            // 
            // LueShelf12
            // 
            this.LueShelf12.EnterMoveNextControl = true;
            this.LueShelf12.Location = new System.Drawing.Point(38, 6);
            this.LueShelf12.Margin = new System.Windows.Forms.Padding(5);
            this.LueShelf12.Name = "LueShelf12";
            this.LueShelf12.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf12.Properties.Appearance.Options.UseFont = true;
            this.LueShelf12.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf12.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueShelf12.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf12.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueShelf12.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf12.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueShelf12.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf12.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueShelf12.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueShelf12.Properties.DropDownRows = 20;
            this.LueShelf12.Properties.NullText = "[Empty]";
            this.LueShelf12.Properties.PopupWidth = 500;
            this.LueShelf12.Size = new System.Drawing.Size(128, 20);
            this.LueShelf12.TabIndex = 31;
            this.LueShelf12.ToolTip = "F4 : Show/hide list";
            this.LueShelf12.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueShelf12.EditValueChanged += new System.EventHandler(this.LueShelf12_EditValueChanged);
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.Black;
            this.label33.Location = new System.Drawing.Point(171, 9);
            this.label33.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(35, 14);
            this.label33.TabIndex = 32;
            this.label33.Text = "Total";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal12
            // 
            this.TxtTotal12.EnterMoveNextControl = true;
            this.TxtTotal12.Location = new System.Drawing.Point(208, 6);
            this.TxtTotal12.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal12.Name = "TxtTotal12";
            this.TxtTotal12.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal12.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal12.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal12.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal12.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotal12.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotal12.Size = new System.Drawing.Size(90, 20);
            this.TxtTotal12.TabIndex = 33;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.Black;
            this.label34.Location = new System.Drawing.Point(8, 9);
            this.label34.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(26, 14);
            this.label34.TabIndex = 30;
            this.label34.Text = "Rak";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage13
            // 
            this.tabPage13.Controls.Add(this.LueItCode13);
            this.tabPage13.Controls.Add(this.Grd23);
            this.tabPage13.Controls.Add(this.panel19);
            this.tabPage13.Location = new System.Drawing.Point(4, 23);
            this.tabPage13.Name = "tabPage13";
            this.tabPage13.Size = new System.Drawing.Size(764, 353);
            this.tabPage13.TabIndex = 12;
            this.tabPage13.Text = "No.13";
            this.tabPage13.UseVisualStyleBackColor = true;
            // 
            // LueItCode13
            // 
            this.LueItCode13.EnterMoveNextControl = true;
            this.LueItCode13.Location = new System.Drawing.Point(19, 54);
            this.LueItCode13.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode13.Name = "LueItCode13";
            this.LueItCode13.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode13.Properties.Appearance.Options.UseFont = true;
            this.LueItCode13.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode13.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode13.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode13.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode13.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode13.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode13.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode13.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode13.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode13.Properties.DropDownRows = 20;
            this.LueItCode13.Properties.NullText = "[Empty]";
            this.LueItCode13.Properties.PopupWidth = 500;
            this.LueItCode13.Size = new System.Drawing.Size(163, 20);
            this.LueItCode13.TabIndex = 39;
            this.LueItCode13.TabStop = false;
            this.LueItCode13.ToolTip = "F4 : Show/hide list";
            this.LueItCode13.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueItCode13.EditValueChanged += new System.EventHandler(this.LueItCode13_EditValueChanged);
            this.LueItCode13.Leave += new System.EventHandler(this.LueItCode13_Leave);
            this.LueItCode13.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueItCode13_KeyDown);
            // 
            // Grd23
            // 
            this.Grd23.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd23.DefaultRow.Height = 20;
            this.Grd23.DefaultRow.Sortable = false;
            this.Grd23.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd23.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd23.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd23.Header.Height = 20;
            this.Grd23.Location = new System.Drawing.Point(0, 33);
            this.Grd23.Name = "Grd23";
            this.Grd23.RowHeader.Visible = true;
            this.Grd23.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd23.SingleClickEdit = true;
            this.Grd23.Size = new System.Drawing.Size(764, 320);
            this.Grd23.TabIndex = 38;
            this.Grd23.TreeCol = null;
            this.Grd23.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd23.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd23_RequestEdit);
            this.Grd23.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd23_KeyDown);
            this.Grd23.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd23_ColHdrDoubleClick);
            this.Grd23.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd23_AfterCommitEdit);
            this.Grd23.CustomDrawCellForeground += new TenTec.Windows.iGridLib.iGCustomDrawCellEventHandler(this.Grd23_CustomDrawCellForeground);
            // 
            // panel19
            // 
            this.panel19.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel19.Controls.Add(this.label65);
            this.panel19.Controls.Add(this.TxtLabelQty13);
            this.panel19.Controls.Add(this.label55);
            this.panel19.Controls.Add(this.LueWhsCode13);
            this.panel19.Controls.Add(this.LueShelf13);
            this.panel19.Controls.Add(this.label35);
            this.panel19.Controls.Add(this.TxtTotal13);
            this.panel19.Controls.Add(this.label36);
            this.panel19.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel19.Location = new System.Drawing.Point(0, 0);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(764, 33);
            this.panel19.TabIndex = 29;
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label65.ForeColor = System.Drawing.Color.Black;
            this.label65.Location = new System.Drawing.Point(302, 9);
            this.label65.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(35, 14);
            this.label65.TabIndex = 34;
            this.label65.Text = "Label";
            this.label65.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtLabelQty13
            // 
            this.TxtLabelQty13.EditValue = "";
            this.TxtLabelQty13.EnterMoveNextControl = true;
            this.TxtLabelQty13.Location = new System.Drawing.Point(340, 6);
            this.TxtLabelQty13.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLabelQty13.Name = "TxtLabelQty13";
            this.TxtLabelQty13.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLabelQty13.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLabelQty13.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLabelQty13.Properties.Appearance.Options.UseFont = true;
            this.TxtLabelQty13.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtLabelQty13.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtLabelQty13.Size = new System.Drawing.Size(77, 20);
            this.TxtLabelQty13.TabIndex = 35;
            this.TxtLabelQty13.Validated += new System.EventHandler(this.TxtLabelQty13_Validated);
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.ForeColor = System.Drawing.Color.Black;
            this.label55.Location = new System.Drawing.Point(423, 9);
            this.label55.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(69, 14);
            this.label55.TabIndex = 36;
            this.label55.Text = "Warehouse";
            this.label55.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueWhsCode13
            // 
            this.LueWhsCode13.EnterMoveNextControl = true;
            this.LueWhsCode13.Location = new System.Drawing.Point(497, 6);
            this.LueWhsCode13.Margin = new System.Windows.Forms.Padding(5);
            this.LueWhsCode13.Name = "LueWhsCode13";
            this.LueWhsCode13.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode13.Properties.Appearance.Options.UseFont = true;
            this.LueWhsCode13.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode13.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueWhsCode13.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode13.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueWhsCode13.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode13.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueWhsCode13.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode13.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueWhsCode13.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueWhsCode13.Properties.DropDownRows = 20;
            this.LueWhsCode13.Properties.NullText = "[Empty]";
            this.LueWhsCode13.Properties.PopupWidth = 500;
            this.LueWhsCode13.Size = new System.Drawing.Size(258, 20);
            this.LueWhsCode13.TabIndex = 37;
            this.LueWhsCode13.ToolTip = "F4 : Show/hide list";
            this.LueWhsCode13.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueWhsCode13.EditValueChanged += new System.EventHandler(this.LueWhsCode13_EditValueChanged);
            // 
            // LueShelf13
            // 
            this.LueShelf13.EnterMoveNextControl = true;
            this.LueShelf13.Location = new System.Drawing.Point(38, 6);
            this.LueShelf13.Margin = new System.Windows.Forms.Padding(5);
            this.LueShelf13.Name = "LueShelf13";
            this.LueShelf13.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf13.Properties.Appearance.Options.UseFont = true;
            this.LueShelf13.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf13.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueShelf13.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf13.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueShelf13.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf13.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueShelf13.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf13.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueShelf13.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueShelf13.Properties.DropDownRows = 20;
            this.LueShelf13.Properties.NullText = "[Empty]";
            this.LueShelf13.Properties.PopupWidth = 500;
            this.LueShelf13.Size = new System.Drawing.Size(128, 20);
            this.LueShelf13.TabIndex = 31;
            this.LueShelf13.ToolTip = "F4 : Show/hide list";
            this.LueShelf13.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueShelf13.EditValueChanged += new System.EventHandler(this.LueShelf13_EditValueChanged);
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.Color.Black;
            this.label35.Location = new System.Drawing.Point(171, 9);
            this.label35.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(35, 14);
            this.label35.TabIndex = 32;
            this.label35.Text = "Total";
            this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal13
            // 
            this.TxtTotal13.EnterMoveNextControl = true;
            this.TxtTotal13.Location = new System.Drawing.Point(208, 6);
            this.TxtTotal13.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal13.Name = "TxtTotal13";
            this.TxtTotal13.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal13.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal13.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal13.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal13.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotal13.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotal13.Size = new System.Drawing.Size(90, 20);
            this.TxtTotal13.TabIndex = 33;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.ForeColor = System.Drawing.Color.Black;
            this.label36.Location = new System.Drawing.Point(8, 9);
            this.label36.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(26, 14);
            this.label36.TabIndex = 30;
            this.label36.Text = "Rak";
            this.label36.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage14
            // 
            this.tabPage14.Controls.Add(this.LueItCode14);
            this.tabPage14.Controls.Add(this.Grd24);
            this.tabPage14.Controls.Add(this.panel20);
            this.tabPage14.Location = new System.Drawing.Point(4, 23);
            this.tabPage14.Name = "tabPage14";
            this.tabPage14.Size = new System.Drawing.Size(764, 353);
            this.tabPage14.TabIndex = 13;
            this.tabPage14.Text = "No.14";
            this.tabPage14.UseVisualStyleBackColor = true;
            // 
            // LueItCode14
            // 
            this.LueItCode14.EnterMoveNextControl = true;
            this.LueItCode14.Location = new System.Drawing.Point(64, 53);
            this.LueItCode14.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode14.Name = "LueItCode14";
            this.LueItCode14.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode14.Properties.Appearance.Options.UseFont = true;
            this.LueItCode14.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode14.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode14.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode14.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode14.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode14.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode14.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode14.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode14.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode14.Properties.DropDownRows = 20;
            this.LueItCode14.Properties.NullText = "[Empty]";
            this.LueItCode14.Properties.PopupWidth = 500;
            this.LueItCode14.Size = new System.Drawing.Size(163, 20);
            this.LueItCode14.TabIndex = 39;
            this.LueItCode14.TabStop = false;
            this.LueItCode14.ToolTip = "F4 : Show/hide list";
            this.LueItCode14.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueItCode14.EditValueChanged += new System.EventHandler(this.LueItCode14_EditValueChanged);
            this.LueItCode14.Leave += new System.EventHandler(this.LueItCode14_Leave);
            this.LueItCode14.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueItCode14_KeyDown);
            // 
            // Grd24
            // 
            this.Grd24.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd24.DefaultRow.Height = 20;
            this.Grd24.DefaultRow.Sortable = false;
            this.Grd24.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd24.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd24.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd24.Header.Height = 20;
            this.Grd24.Location = new System.Drawing.Point(0, 33);
            this.Grd24.Name = "Grd24";
            this.Grd24.RowHeader.Visible = true;
            this.Grd24.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd24.SingleClickEdit = true;
            this.Grd24.Size = new System.Drawing.Size(764, 320);
            this.Grd24.TabIndex = 38;
            this.Grd24.TreeCol = null;
            this.Grd24.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd24.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd24_RequestEdit);
            this.Grd24.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd24_KeyDown);
            this.Grd24.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd24_ColHdrDoubleClick);
            this.Grd24.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd24_AfterCommitEdit);
            this.Grd24.CustomDrawCellForeground += new TenTec.Windows.iGridLib.iGCustomDrawCellEventHandler(this.Grd24_CustomDrawCellForeground);
            // 
            // panel20
            // 
            this.panel20.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel20.Controls.Add(this.label66);
            this.panel20.Controls.Add(this.TxtLabelQty14);
            this.panel20.Controls.Add(this.label56);
            this.panel20.Controls.Add(this.LueWhsCode14);
            this.panel20.Controls.Add(this.LueShelf14);
            this.panel20.Controls.Add(this.label37);
            this.panel20.Controls.Add(this.TxtTotal14);
            this.panel20.Controls.Add(this.label38);
            this.panel20.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel20.Location = new System.Drawing.Point(0, 0);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(764, 33);
            this.panel20.TabIndex = 29;
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label66.ForeColor = System.Drawing.Color.Black;
            this.label66.Location = new System.Drawing.Point(305, 9);
            this.label66.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(35, 14);
            this.label66.TabIndex = 34;
            this.label66.Text = "Label";
            this.label66.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtLabelQty14
            // 
            this.TxtLabelQty14.EditValue = "";
            this.TxtLabelQty14.EnterMoveNextControl = true;
            this.TxtLabelQty14.Location = new System.Drawing.Point(343, 6);
            this.TxtLabelQty14.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLabelQty14.Name = "TxtLabelQty14";
            this.TxtLabelQty14.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLabelQty14.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLabelQty14.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLabelQty14.Properties.Appearance.Options.UseFont = true;
            this.TxtLabelQty14.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtLabelQty14.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtLabelQty14.Size = new System.Drawing.Size(77, 20);
            this.TxtLabelQty14.TabIndex = 35;
            this.TxtLabelQty14.Validated += new System.EventHandler(this.TxtLabelQty14_Validated);
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label56.ForeColor = System.Drawing.Color.Black;
            this.label56.Location = new System.Drawing.Point(427, 9);
            this.label56.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(69, 14);
            this.label56.TabIndex = 36;
            this.label56.Text = "Warehouse";
            this.label56.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueWhsCode14
            // 
            this.LueWhsCode14.EnterMoveNextControl = true;
            this.LueWhsCode14.Location = new System.Drawing.Point(501, 6);
            this.LueWhsCode14.Margin = new System.Windows.Forms.Padding(5);
            this.LueWhsCode14.Name = "LueWhsCode14";
            this.LueWhsCode14.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode14.Properties.Appearance.Options.UseFont = true;
            this.LueWhsCode14.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode14.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueWhsCode14.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode14.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueWhsCode14.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode14.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueWhsCode14.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode14.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueWhsCode14.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueWhsCode14.Properties.DropDownRows = 20;
            this.LueWhsCode14.Properties.NullText = "[Empty]";
            this.LueWhsCode14.Properties.PopupWidth = 500;
            this.LueWhsCode14.Size = new System.Drawing.Size(249, 20);
            this.LueWhsCode14.TabIndex = 37;
            this.LueWhsCode14.ToolTip = "F4 : Show/hide list";
            this.LueWhsCode14.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueWhsCode14.EditValueChanged += new System.EventHandler(this.LueWhsCode14_EditValueChanged);
            // 
            // LueShelf14
            // 
            this.LueShelf14.EnterMoveNextControl = true;
            this.LueShelf14.Location = new System.Drawing.Point(38, 6);
            this.LueShelf14.Margin = new System.Windows.Forms.Padding(5);
            this.LueShelf14.Name = "LueShelf14";
            this.LueShelf14.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf14.Properties.Appearance.Options.UseFont = true;
            this.LueShelf14.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf14.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueShelf14.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf14.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueShelf14.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf14.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueShelf14.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf14.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueShelf14.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueShelf14.Properties.DropDownRows = 20;
            this.LueShelf14.Properties.NullText = "[Empty]";
            this.LueShelf14.Properties.PopupWidth = 500;
            this.LueShelf14.Size = new System.Drawing.Size(129, 20);
            this.LueShelf14.TabIndex = 31;
            this.LueShelf14.ToolTip = "F4 : Show/hide list";
            this.LueShelf14.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueShelf14.EditValueChanged += new System.EventHandler(this.LueShelf14_EditValueChanged);
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.ForeColor = System.Drawing.Color.Black;
            this.label37.Location = new System.Drawing.Point(172, 9);
            this.label37.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(35, 14);
            this.label37.TabIndex = 32;
            this.label37.Text = "Total";
            this.label37.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal14
            // 
            this.TxtTotal14.EnterMoveNextControl = true;
            this.TxtTotal14.Location = new System.Drawing.Point(209, 6);
            this.TxtTotal14.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal14.Name = "TxtTotal14";
            this.TxtTotal14.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal14.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal14.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal14.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal14.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotal14.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotal14.Size = new System.Drawing.Size(90, 20);
            this.TxtTotal14.TabIndex = 33;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.ForeColor = System.Drawing.Color.Black;
            this.label38.Location = new System.Drawing.Point(8, 9);
            this.label38.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(26, 14);
            this.label38.TabIndex = 30;
            this.label38.Text = "Rak";
            this.label38.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage15
            // 
            this.tabPage15.Controls.Add(this.LueItCode15);
            this.tabPage15.Controls.Add(this.Grd25);
            this.tabPage15.Controls.Add(this.panel21);
            this.tabPage15.Location = new System.Drawing.Point(4, 23);
            this.tabPage15.Name = "tabPage15";
            this.tabPage15.Size = new System.Drawing.Size(764, 353);
            this.tabPage15.TabIndex = 14;
            this.tabPage15.Text = "No.15";
            this.tabPage15.UseVisualStyleBackColor = true;
            // 
            // LueItCode15
            // 
            this.LueItCode15.EnterMoveNextControl = true;
            this.LueItCode15.Location = new System.Drawing.Point(19, 53);
            this.LueItCode15.Margin = new System.Windows.Forms.Padding(5);
            this.LueItCode15.Name = "LueItCode15";
            this.LueItCode15.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode15.Properties.Appearance.Options.UseFont = true;
            this.LueItCode15.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode15.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueItCode15.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode15.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueItCode15.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode15.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueItCode15.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueItCode15.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueItCode15.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueItCode15.Properties.DropDownRows = 20;
            this.LueItCode15.Properties.NullText = "[Empty]";
            this.LueItCode15.Properties.PopupWidth = 500;
            this.LueItCode15.Size = new System.Drawing.Size(163, 20);
            this.LueItCode15.TabIndex = 39;
            this.LueItCode15.TabStop = false;
            this.LueItCode15.ToolTip = "F4 : Show/hide list";
            this.LueItCode15.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueItCode15.EditValueChanged += new System.EventHandler(this.LueItCode15_EditValueChanged);
            this.LueItCode15.Leave += new System.EventHandler(this.LueItCode15_Leave);
            this.LueItCode15.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueItCode15_KeyDown);
            // 
            // Grd25
            // 
            this.Grd25.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd25.DefaultRow.Height = 20;
            this.Grd25.DefaultRow.Sortable = false;
            this.Grd25.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd25.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd25.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd25.Header.Height = 20;
            this.Grd25.Location = new System.Drawing.Point(0, 33);
            this.Grd25.Name = "Grd25";
            this.Grd25.RowHeader.Visible = true;
            this.Grd25.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd25.SingleClickEdit = true;
            this.Grd25.Size = new System.Drawing.Size(764, 320);
            this.Grd25.TabIndex = 38;
            this.Grd25.TreeCol = null;
            this.Grd25.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd25.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd25_RequestEdit);
            this.Grd25.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd25_KeyDown);
            this.Grd25.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd25_ColHdrDoubleClick);
            this.Grd25.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd25_AfterCommitEdit);
            this.Grd25.CustomDrawCellForeground += new TenTec.Windows.iGridLib.iGCustomDrawCellEventHandler(this.Grd25_CustomDrawCellForeground);
            // 
            // panel21
            // 
            this.panel21.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel21.Controls.Add(this.label67);
            this.panel21.Controls.Add(this.TxtLabelQty15);
            this.panel21.Controls.Add(this.label57);
            this.panel21.Controls.Add(this.LueWhsCode15);
            this.panel21.Controls.Add(this.LueShelf15);
            this.panel21.Controls.Add(this.label39);
            this.panel21.Controls.Add(this.TxtTotal15);
            this.panel21.Controls.Add(this.label40);
            this.panel21.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel21.Location = new System.Drawing.Point(0, 0);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(764, 33);
            this.panel21.TabIndex = 29;
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label67.ForeColor = System.Drawing.Color.Black;
            this.label67.Location = new System.Drawing.Point(307, 9);
            this.label67.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(35, 14);
            this.label67.TabIndex = 34;
            this.label67.Text = "Label";
            this.label67.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtLabelQty15
            // 
            this.TxtLabelQty15.EditValue = "";
            this.TxtLabelQty15.EnterMoveNextControl = true;
            this.TxtLabelQty15.Location = new System.Drawing.Point(347, 6);
            this.TxtLabelQty15.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLabelQty15.Name = "TxtLabelQty15";
            this.TxtLabelQty15.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLabelQty15.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLabelQty15.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLabelQty15.Properties.Appearance.Options.UseFont = true;
            this.TxtLabelQty15.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtLabelQty15.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtLabelQty15.Size = new System.Drawing.Size(77, 20);
            this.TxtLabelQty15.TabIndex = 35;
            this.TxtLabelQty15.Validated += new System.EventHandler(this.TxtLabelQty15_Validated);
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label57.ForeColor = System.Drawing.Color.Black;
            this.label57.Location = new System.Drawing.Point(430, 9);
            this.label57.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(69, 14);
            this.label57.TabIndex = 36;
            this.label57.Text = "Warehouse";
            this.label57.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueWhsCode15
            // 
            this.LueWhsCode15.EnterMoveNextControl = true;
            this.LueWhsCode15.Location = new System.Drawing.Point(504, 6);
            this.LueWhsCode15.Margin = new System.Windows.Forms.Padding(5);
            this.LueWhsCode15.Name = "LueWhsCode15";
            this.LueWhsCode15.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode15.Properties.Appearance.Options.UseFont = true;
            this.LueWhsCode15.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode15.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueWhsCode15.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode15.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueWhsCode15.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode15.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueWhsCode15.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode15.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueWhsCode15.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueWhsCode15.Properties.DropDownRows = 20;
            this.LueWhsCode15.Properties.NullText = "[Empty]";
            this.LueWhsCode15.Properties.PopupWidth = 500;
            this.LueWhsCode15.Size = new System.Drawing.Size(249, 20);
            this.LueWhsCode15.TabIndex = 37;
            this.LueWhsCode15.ToolTip = "F4 : Show/hide list";
            this.LueWhsCode15.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueWhsCode15.EditValueChanged += new System.EventHandler(this.LueWhsCode15_EditValueChanged);
            // 
            // LueShelf15
            // 
            this.LueShelf15.EnterMoveNextControl = true;
            this.LueShelf15.Location = new System.Drawing.Point(38, 6);
            this.LueShelf15.Margin = new System.Windows.Forms.Padding(5);
            this.LueShelf15.Name = "LueShelf15";
            this.LueShelf15.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf15.Properties.Appearance.Options.UseFont = true;
            this.LueShelf15.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf15.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueShelf15.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf15.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueShelf15.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf15.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueShelf15.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShelf15.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueShelf15.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueShelf15.Properties.DropDownRows = 20;
            this.LueShelf15.Properties.NullText = "[Empty]";
            this.LueShelf15.Properties.PopupWidth = 500;
            this.LueShelf15.Size = new System.Drawing.Size(129, 20);
            this.LueShelf15.TabIndex = 31;
            this.LueShelf15.ToolTip = "F4 : Show/hide list";
            this.LueShelf15.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueShelf15.EditValueChanged += new System.EventHandler(this.LueShelf15_EditValueChanged);
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.ForeColor = System.Drawing.Color.Black;
            this.label39.Location = new System.Drawing.Point(172, 9);
            this.label39.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(35, 14);
            this.label39.TabIndex = 32;
            this.label39.Text = "Total";
            this.label39.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotal15
            // 
            this.TxtTotal15.EnterMoveNextControl = true;
            this.TxtTotal15.Location = new System.Drawing.Point(209, 6);
            this.TxtTotal15.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal15.Name = "TxtTotal15";
            this.TxtTotal15.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal15.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal15.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal15.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal15.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotal15.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotal15.Size = new System.Drawing.Size(90, 20);
            this.TxtTotal15.TabIndex = 33;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.ForeColor = System.Drawing.Color.Black;
            this.label40.Location = new System.Drawing.Point(8, 9);
            this.label40.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(26, 14);
            this.label40.TabIndex = 30;
            this.label40.Text = "Rak";
            this.label40.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // FrmRecvRawMaterial2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 473);
            this.Name = "FrmRecvRawMaterial2";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueEmpCode2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueEmpCode1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCreateBy.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueQueueNo.Properties)).EndInit();
            this.panel7.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd11)).EndInit();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLabelQty1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShelf1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal1.Properties)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd12)).EndInit();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLabelQty2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShelf2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal2.Properties)).EndInit();
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd13)).EndInit();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLabelQty3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShelf3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal3.Properties)).EndInit();
            this.tabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd14)).EndInit();
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLabelQty4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShelf4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal4.Properties)).EndInit();
            this.tabPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd15)).EndInit();
            this.panel12.ResumeLayout(false);
            this.panel12.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLabelQty5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShelf5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal5.Properties)).EndInit();
            this.tabPage6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd16)).EndInit();
            this.panel13.ResumeLayout(false);
            this.panel13.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLabelQty6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShelf6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal6.Properties)).EndInit();
            this.tabPage7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd17)).EndInit();
            this.panel14.ResumeLayout(false);
            this.panel14.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLabelQty7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShelf7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal7.Properties)).EndInit();
            this.tabPage8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd18)).EndInit();
            this.panel15.ResumeLayout(false);
            this.panel15.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLabelQty8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShelf8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal8.Properties)).EndInit();
            this.tabPage9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd19)).EndInit();
            this.panel16.ResumeLayout(false);
            this.panel16.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLabelQty9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShelf9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal9.Properties)).EndInit();
            this.tabPage10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd20)).EndInit();
            this.panel17.ResumeLayout(false);
            this.panel17.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLabelQty10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShelf10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal10.Properties)).EndInit();
            this.tabPage11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd21)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLabelQty11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShelf11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal11.Properties)).EndInit();
            this.tabPage12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd22)).EndInit();
            this.panel18.ResumeLayout(false);
            this.panel18.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLabelQty12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShelf12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal12.Properties)).EndInit();
            this.tabPage13.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode13.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd23)).EndInit();
            this.panel19.ResumeLayout(false);
            this.panel19.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLabelQty13.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode13.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShelf13.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal13.Properties)).EndInit();
            this.tabPage14.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode14.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd24)).EndInit();
            this.panel20.ResumeLayout(false);
            this.panel20.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLabelQty14.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode14.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShelf14.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal14.Properties)).EndInit();
            this.tabPage15.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueItCode15.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd25)).EndInit();
            this.panel21.ResumeLayout(false);
            this.panel21.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLabelQty15.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode15.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShelf15.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal15.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label5;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.LookUpEdit LueQueueNo;
        private DevExpress.XtraEditors.CheckEdit ChkCancelInd;
        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        private System.Windows.Forms.Label label17;
        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Label label18;
        protected System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.TabPage tabPage8;
        private System.Windows.Forms.TabPage tabPage9;
        private System.Windows.Forms.TabPage tabPage10;
        protected System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label label11;
        protected internal TenTec.Windows.iGridLib.iGrid Grd11;
        protected internal TenTec.Windows.iGridLib.iGrid Grd12;
        protected System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Label label13;
        protected internal TenTec.Windows.iGridLib.iGrid Grd13;
        protected System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Label label15;
        protected internal TenTec.Windows.iGridLib.iGrid Grd14;
        protected System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Label label19;
        protected internal TenTec.Windows.iGridLib.iGrid Grd15;
        protected System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Label label21;
        protected internal TenTec.Windows.iGridLib.iGrid Grd16;
        protected System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Label label23;
        protected internal TenTec.Windows.iGridLib.iGrid Grd17;
        protected System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Label label25;
        protected internal TenTec.Windows.iGridLib.iGrid Grd18;
        protected System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.Label label27;
        protected internal TenTec.Windows.iGridLib.iGrid Grd19;
        protected System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.Label label29;
        protected internal TenTec.Windows.iGridLib.iGrid Grd20;
        protected System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.Label label31;
        private DevExpress.XtraEditors.LookUpEdit LueItCode3;
        private DevExpress.XtraEditors.LookUpEdit LueItCode4;
        private DevExpress.XtraEditors.LookUpEdit LueItCode5;
        private DevExpress.XtraEditors.LookUpEdit LueItCode7;
        private DevExpress.XtraEditors.LookUpEdit LueItCode8;
        private DevExpress.XtraEditors.LookUpEdit LueItCode9;
        private DevExpress.XtraEditors.LookUpEdit LueItCode10;
        private DevExpress.XtraEditors.LookUpEdit LueItCode2;
        private DevExpress.XtraEditors.LookUpEdit LueItCode6;
        private DevExpress.XtraEditors.LookUpEdit LueItCode1;
        private System.Windows.Forms.Label label8;
        internal DevExpress.XtraEditors.TextEdit TxtTotal1;
        private System.Windows.Forms.Label label9;
        internal DevExpress.XtraEditors.TextEdit TxtTotal2;
        private System.Windows.Forms.Label label10;
        internal DevExpress.XtraEditors.TextEdit TxtTotal3;
        private System.Windows.Forms.Label label12;
        internal DevExpress.XtraEditors.TextEdit TxtTotal4;
        private System.Windows.Forms.Label label14;
        internal DevExpress.XtraEditors.TextEdit TxtTotal5;
        private System.Windows.Forms.Label label16;
        internal DevExpress.XtraEditors.TextEdit TxtTotal6;
        private System.Windows.Forms.Label label20;
        internal DevExpress.XtraEditors.TextEdit TxtTotal7;
        private System.Windows.Forms.Label label22;
        internal DevExpress.XtraEditors.TextEdit TxtTotal8;
        private System.Windows.Forms.Label label24;
        internal DevExpress.XtraEditors.TextEdit TxtTotal9;
        private System.Windows.Forms.Label label26;
        internal DevExpress.XtraEditors.TextEdit TxtTotal10;
        private System.Windows.Forms.Label label28;
        internal DevExpress.XtraEditors.TextEdit TxtTotal;
        private DevExpress.XtraEditors.LookUpEdit LueShelf1;
        private DevExpress.XtraEditors.LookUpEdit LueShelf2;
        private DevExpress.XtraEditors.LookUpEdit LueShelf3;
        private DevExpress.XtraEditors.LookUpEdit LueShelf4;
        private DevExpress.XtraEditors.LookUpEdit LueShelf5;
        private DevExpress.XtraEditors.LookUpEdit LueShelf6;
        private DevExpress.XtraEditors.LookUpEdit LueShelf7;
        private DevExpress.XtraEditors.LookUpEdit LueShelf8;
        private DevExpress.XtraEditors.LookUpEdit LueShelf9;
        private DevExpress.XtraEditors.LookUpEdit LueShelf10;
        private System.Windows.Forms.TabPage tabPage11;
        protected internal TenTec.Windows.iGridLib.iGrid Grd21;
        protected System.Windows.Forms.Panel panel5;
        private DevExpress.XtraEditors.LookUpEdit LueShelf11;
        private System.Windows.Forms.Label label30;
        internal DevExpress.XtraEditors.TextEdit TxtTotal11;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TabPage tabPage12;
        private System.Windows.Forms.TabPage tabPage13;
        private System.Windows.Forms.TabPage tabPage14;
        private System.Windows.Forms.TabPage tabPage15;
        private DevExpress.XtraEditors.LookUpEdit LueItCode11;
        private DevExpress.XtraEditors.LookUpEdit LueItCode12;
        protected internal TenTec.Windows.iGridLib.iGrid Grd22;
        protected System.Windows.Forms.Panel panel18;
        private DevExpress.XtraEditors.LookUpEdit LueShelf12;
        private System.Windows.Forms.Label label33;
        internal DevExpress.XtraEditors.TextEdit TxtTotal12;
        private System.Windows.Forms.Label label34;
        private DevExpress.XtraEditors.LookUpEdit LueItCode13;
        protected internal TenTec.Windows.iGridLib.iGrid Grd23;
        protected System.Windows.Forms.Panel panel19;
        private DevExpress.XtraEditors.LookUpEdit LueShelf13;
        private System.Windows.Forms.Label label35;
        internal DevExpress.XtraEditors.TextEdit TxtTotal13;
        private System.Windows.Forms.Label label36;
        private DevExpress.XtraEditors.LookUpEdit LueItCode14;
        protected internal TenTec.Windows.iGridLib.iGrid Grd24;
        protected System.Windows.Forms.Panel panel20;
        private DevExpress.XtraEditors.LookUpEdit LueShelf14;
        private System.Windows.Forms.Label label37;
        internal DevExpress.XtraEditors.TextEdit TxtTotal14;
        private System.Windows.Forms.Label label38;
        private DevExpress.XtraEditors.LookUpEdit LueItCode15;
        protected internal TenTec.Windows.iGridLib.iGrid Grd25;
        protected System.Windows.Forms.Panel panel21;
        private DevExpress.XtraEditors.LookUpEdit LueShelf15;
        private System.Windows.Forms.Label label39;
        internal DevExpress.XtraEditors.TextEdit TxtTotal15;
        private System.Windows.Forms.Label label40;
        internal DevExpress.XtraEditors.TextEdit TxtCreateBy;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label43;
        internal DevExpress.XtraEditors.LookUpEdit LueWhsCode1;
        private System.Windows.Forms.Label label44;
        internal DevExpress.XtraEditors.LookUpEdit LueWhsCode2;
        private System.Windows.Forms.Label label45;
        internal DevExpress.XtraEditors.LookUpEdit LueWhsCode3;
        private System.Windows.Forms.Label label46;
        internal DevExpress.XtraEditors.LookUpEdit LueWhsCode4;
        private System.Windows.Forms.Label label47;
        internal DevExpress.XtraEditors.LookUpEdit LueWhsCode5;
        private System.Windows.Forms.Label label48;
        internal DevExpress.XtraEditors.LookUpEdit LueWhsCode6;
        private System.Windows.Forms.Label label49;
        internal DevExpress.XtraEditors.LookUpEdit LueWhsCode7;
        private System.Windows.Forms.Label label50;
        internal DevExpress.XtraEditors.LookUpEdit LueWhsCode8;
        private System.Windows.Forms.Label label51;
        internal DevExpress.XtraEditors.LookUpEdit LueWhsCode9;
        private System.Windows.Forms.Label label52;
        internal DevExpress.XtraEditors.LookUpEdit LueWhsCode10;
        private System.Windows.Forms.Label label53;
        internal DevExpress.XtraEditors.LookUpEdit LueWhsCode11;
        private System.Windows.Forms.Label label54;
        internal DevExpress.XtraEditors.LookUpEdit LueWhsCode12;
        private System.Windows.Forms.Label label55;
        internal DevExpress.XtraEditors.LookUpEdit LueWhsCode13;
        private System.Windows.Forms.Label label56;
        internal DevExpress.XtraEditors.LookUpEdit LueWhsCode14;
        private System.Windows.Forms.Label label57;
        internal DevExpress.XtraEditors.LookUpEdit LueWhsCode15;
        private System.Windows.Forms.Panel panel4;
        private DevExpress.XtraEditors.LookUpEdit LueEmpCode1;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.LookUpEdit LueEmpCode2;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtLabelQty1;
        private System.Windows.Forms.Label label3;
        internal DevExpress.XtraEditors.TextEdit TxtLabelQty2;
        private System.Windows.Forms.Label label6;
        internal DevExpress.XtraEditors.TextEdit TxtLabelQty3;
        private System.Windows.Forms.Label label7;
        internal DevExpress.XtraEditors.TextEdit TxtLabelQty4;
        private System.Windows.Forms.Label label42;
        internal DevExpress.XtraEditors.TextEdit TxtLabelQty5;
        private System.Windows.Forms.Label label58;
        internal DevExpress.XtraEditors.TextEdit TxtLabelQty6;
        private System.Windows.Forms.Label label59;
        internal DevExpress.XtraEditors.TextEdit TxtLabelQty7;
        private System.Windows.Forms.Label label60;
        internal DevExpress.XtraEditors.TextEdit TxtLabelQty8;
        private System.Windows.Forms.Label label61;
        internal DevExpress.XtraEditors.TextEdit TxtLabelQty9;
        private System.Windows.Forms.Label label62;
        internal DevExpress.XtraEditors.TextEdit TxtLabelQty10;
        private System.Windows.Forms.Label label63;
        internal DevExpress.XtraEditors.TextEdit TxtLabelQty11;
        private System.Windows.Forms.Label label64;
        internal DevExpress.XtraEditors.TextEdit TxtLabelQty12;
        private System.Windows.Forms.Label label65;
        internal DevExpress.XtraEditors.TextEdit TxtLabelQty13;
        private System.Windows.Forms.Label label66;
        internal DevExpress.XtraEditors.TextEdit TxtLabelQty14;
        private System.Windows.Forms.Label label67;
        internal DevExpress.XtraEditors.TextEdit TxtLabelQty15;
    }
}