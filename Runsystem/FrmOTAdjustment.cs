﻿#region Update
/*
    07/03/2022 [ISD/SIER] Menyambungkan Doc Approval Group
    26/07/2022 [IBL/SIER] memunculkan tombol print ketika dipanggil dari docapproval & menu lain
    11/08/2022 [SET/SIER] penyesuaian tampilan approval berdasar parameter IsTransactionUseDetailApprovalInformation
    15/09/2022 [SET/SIER] penyesuaian save docapproval
    10/04/2023 [ISN/SIER] penyesuaian tampilan kolom date dan remark berdasar parameter IsOTAdjustmentShowDateandRemark
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

using FastReport;
using FastReport.Data;

#endregion

namespace RunSystem
{
    public partial class FrmOTAdjustment : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string 
            mMenuCode = string.Empty, mAccessInd = string.Empty,
            mDocNo = string.Empty; //if this application is called from other application;
        internal string mOTRequestDNo = string.Empty, mDeptCode = string.Empty;
        internal FrmOTAdjustmentFind FrmFind;
        private string mDocTitle = string.Empty;
        internal bool
            mIsTransactionUseDetailApprovalInformation = false,
            mIsOTAdjustmentShowDateandRemark = false;

        #endregion

        #region Constructor

        public FrmOTAdjustment(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "OT Adjustment";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = false;
                    if (mDocTitle != "SIER") BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grid 1

            Grd1.Cols.Count = 6;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "No.",
                        
                        //1-5
                        "Checked By", 
                        "Position",
                        "Status",
                        "Date",
                        "Remark"
                    },
                    new int[] 
                    {
                        100, 
                        150, 300, 100, 100, 400
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 4 });
            Sm.GrdColInvisible(Grd1, new int[] { 4, 5 }, mIsOTAdjustmentShowDateandRemark ? true : false);
            if (!mIsTransactionUseDetailApprovalInformation)
                Sm.GrdColInvisible(Grd1, new int[] { 0, 2 }, false);

            #endregion

            #region Grid 2

            Grd2.Cols.Count = 1;
            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[]{ "Time" },
                    new int[]{ 100 }
                );
            Sm.GrdFormatTime(Grd2, new int[] { 0 });

            #endregion
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4, 5 }, mIsOTAdjustmentShowDateandRemark ? true : !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { DteDocDt, ChkCancelInd, TmeStartTm, TmeEndTm, MeeRemark }, true);
                    BtnOTRequestDocNo.Enabled = false;
                    ChkCancelInd.Properties.ReadOnly = true;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { DteDocDt, TmeStartTm, TmeEndTm, MeeRemark }, false);
                    BtnOTRequestDocNo.Enabled = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    ChkCancelInd.Properties.ReadOnly = false;
                    TxtDocNo.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            mOTRequestDNo = string.Empty;
            mDeptCode = string.Empty;
            ChkCancelInd.Checked = false;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, TxtStatus, DteDocDt, TxtOTRequestDocNo, TxtEmpCode, 
                TxtEmpName, TxtEmpCodeOld, TxtPosCode, TxtDeptCode, DteOTDt, 
                TmeRequestedStartTm, TmeRequestedEndTm, TmeStartTm, TmeEndTm, MeeRemark
            });
            ClearGrd();
        }

        internal void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.ClearGrd(Grd2, true);
            Sm.FocusGrd(Grd1, 0, 0);
            Sm.FocusGrd(Grd2, 0, 0);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmOTAdjustmentFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Save Data

        private void InsertData(object sender, EventArgs e)
        {
            if (
                Sm.StdMsgYN("Question", "Do you want to save this data ?") == DialogResult.No ||
                IsInsertedDataNotValid()
                ) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "OTAdjustment", "TblOTAdjustment");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveOTAdjustment(DocNo));
            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsTxtEmpty(TxtOTRequestDocNo, "OT document#", false) ||
                Sm.IsTmeEmpty(TmeStartTm, "Start time") ||
                Sm.IsTmeEmpty(TmeEndTm, "End time") ||
                IsAdjustedTimeNotValid() ||
                IsOTRequestAlreadyAdjusted() ||
                IsOTRequestAlreadyCancelled();
        }

        private bool IsAdjustedTimeNotValid()
        {
            int StartTm = 0, EndTm = 0;
            StartTm = int.Parse(Sm.GetTme(TmeStartTm));
            EndTm = int.Parse(Sm.GetTme(TmeEndTm));
            if (EndTm != 0 && StartTm > EndTm)
            {
                Sm.StdMsg(mMsgType.Warning, "Invalid Over Time.");
                return true;
            }
            return false;
        }

        private bool IsOTRequestAlreadyAdjusted()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select DocNo From TblOTRequestDtl " +
                    "Where AdjustedInd='Y' And DocNo=@DocNo And DNo=@DNo;"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtOTRequestDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DNo", mOTRequestDNo);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This OT already adjusted.");
                return true;
            }
            else
                return false;
        }

        private bool IsOTRequestAlreadyCancelled()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select DocNo From TblOTRequestHdr " +
                    "Where (CancelInd='Y' Or Status='C') And DocNo=@DocNo;"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtOTRequestDocNo.Text);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This OT already cancelled.");
                return true;
            }
            else
                return false;
        }

        private MySqlCommand SaveOTAdjustment(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblOTRequestDtl Set ");
            SQL.AppendLine("    AdjustedInd='Y' ");
            SQL.AppendLine("Where DocNo=@OTRequestDocNo And DNo=@OTRequestDNo; ");

            SQL.AppendLine("Insert Into TblOTAdjustment(DocNo, DocDt, CancelInd, Status, OTRequestDocNo, OTRequestDNo, StartTm, EndTm, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'N', 'O', @OTRequestDocNo, @OTRequestDNo, @StartTm, @EndTm, @Remark, @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblDocApprovalSetting T ");
            SQL.AppendLine("Where T.DocType='OTAdjustment' And T.DeptCode=@DeptCode ");
            SQL.AppendLine("And (T.DAGCode Is Null Or ");
            SQL.AppendLine("(T.DAGCode Is Not Null ");
            SQL.AppendLine("And T.DAGCode In ( ");
            SQL.AppendLine("    Select A.DAGCode ");
            SQL.AppendLine("    From TblDocApprovalGroupHdr A, TblDocApprovalGroupDtl B ");
            SQL.AppendLine("    Where A.DAGCode=B.DAGCode ");
            SQL.AppendLine("    And A.ActInd='Y' ");
            SQL.AppendLine("    And B.EmpCode = (Select EmpCode From TblEmployee Where UserCode = @CreateBy Limit 1) ");
            SQL.AppendLine("))) ");
            SQL.AppendLine("; ");

            SQL.AppendLine("Update TblOTAdjustment Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select DocNo From TblDocApproval ");
            SQL.AppendLine("    Where DocType='OTAdjustment' ");
            SQL.AppendLine("    And DocNo=@DocNo ");
            SQL.AppendLine("    ); ");



            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@OTRequestDocNo", TxtOTRequestDocNo.Text);
            Sm.CmParam<String>(ref cm, "@OTRequestDNo", mOTRequestDNo);
            Sm.CmParam<String>(ref cm, "@StartTm", Sm.GetTme(TmeStartTm));
            Sm.CmParam<String>(ref cm, "@EndTm", Sm.GetTme(TmeEndTm));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@DeptCode", mDeptCode);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditOTAdjustment());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsEditedDataInvalid() ||
                IsDataCancelledAlready() ;
        }

        private bool IsEditedDataInvalid()
        {
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this document.");
                return true;
            }
            return false;
        }

        private bool IsDataCancelledAlready()
        {
            return Sm.IsDataExist(
                "Select DocNo From TblOTAdjustment Where (CancelInd='Y' Or Status='C') And DocNo=@Param;",
                TxtDocNo.Text,
                "This data already cancelled.");
        }

        private MySqlCommand EditOTAdjustment()
        {
            var SQL = new StringBuilder();


            SQL.AppendLine("Update TblOTRequestDtl Set ");
            SQL.AppendLine("    AdjustedInd='N' ");
            SQL.AppendLine("Where DocNo=@OTRequestDocNo And DNo=@OTRequestDNo; ");

            SQL.AppendLine("Update TblOTAdjustment Set ");
            SQL.AppendLine("    CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@OTRequestDocNo", TxtOTRequestDocNo.Text);
            Sm.CmParam<String>(ref cm, "@OTRequestDNo", mOTRequestDNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowOTAdjustment(DocNo);
                ShowAttendanceLog();
                //ShowDocApproval();
                Sm.ShowDocApproval(DocNo, "OTAdjustment", ref Grd1, mIsTransactionUseDetailApprovalInformation);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }

        }

        private void ShowOTAdjustment(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelInd, ");
            SQL.AppendLine("Case A.Status When 'O' Then 'Outstanding' When 'A' Then 'Approved' When 'C' Then 'Cancelled' End As StatusDesc, ");
            SQL.AppendLine("A.OTRequestDocNo, A.OTRequestDNo, ");
            SQL.AppendLine("C.EmpCode, D.EmpName, D.EmpCodeOld, E.PosName, D.DeptCode, F.DeptName, ");
            SQL.AppendLine("B.OTDt, C.OTStartTm, C.OTEndTm, A.StartTm, A.EndTm, A.Remark ");
            SQL.AppendLine("From TblOTAdjustment A ");
            SQL.AppendLine("Inner Join TblOTRequestHdr B On A.OTRequestDocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblOTRequestDtl C On A.OTRequestDocNo=C.DocNo And A.OTRequestDNo=C.DNo ");
            SQL.AppendLine("Inner Join TblEmployee D On C.EmpCode=D.EmpCode ");
            SQL.AppendLine("Left Join TblPosition E On D.PosCode=E.PosCode ");
            SQL.AppendLine("Inner Join TblDepartment F On D.DeptCode=F.DeptCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                 ref cm, SQL.ToString(),
                 new string[] 
                 {
                    //0
                    "DocNo",
                    
                    //1-5
                    "DocDt", "CancelInd", "StatusDesc", "OTRequestDocNo", "OTRequestDNo", 
                    
                    //6-10
                    "EmpCode", "EmpName", "EmpCodeOld", "PosName", "DeptCode", 
                    
                    //11-15
                    "DeptName", "OTDt", "OTStartTm", "OTEndTm", "StartTm", 
                    
                    //16-17
                    "EndTm", "Remark"
                },
                 (MySqlDataReader dr, int[] c) =>
                 {
                     TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                     Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                     ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y");
                     TxtStatus.EditValue = Sm.DrStr(dr, c[3]);
                     TxtOTRequestDocNo.EditValue = Sm.DrStr(dr, c[4]);
                     mOTRequestDNo = Sm.DrStr(dr, c[5]);
                     TxtEmpCode.EditValue = Sm.DrStr(dr, c[6]);
                     TxtEmpName.EditValue = Sm.DrStr(dr, c[7]);
                     TxtEmpCodeOld.EditValue = Sm.DrStr(dr, c[8]);
                     TxtPosCode.EditValue = Sm.DrStr(dr, c[9]);
                     mDeptCode = Sm.DrStr(dr, c[10]);
                     TxtDeptCode.EditValue = Sm.DrStr(dr, c[11]);
                     Sm.SetDte(DteOTDt, Sm.DrStr(dr, c[12]));
                     Sm.SetTme(TmeRequestedStartTm, Sm.DrStr(dr, c[13]));
                     Sm.SetTme(TmeRequestedEndTm, Sm.DrStr(dr, c[14]));
                     Sm.SetTme(TmeStartTm, Sm.DrStr(dr, c[15]));
                     Sm.SetTme(TmeEndTm, Sm.DrStr(dr, c[16]));
                     MeeRemark.EditValue = Sm.DrStr(dr, c[17]);
                 }, true
             );
        }


        #endregion

        #region Additional Method

        private void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'DocTitle', 'IsTransactionUseDetailApprovalInformation', 'IsOTAdjustmentShowDateandRemark');");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsTransactionUseDetailApprovalInformation": mIsTransactionUseDetailApprovalInformation = ParValue == "Y"; break;
                            case "IsOTAdjustmentShowDateandRemark": mIsOTAdjustmentShowDateandRemark = ParValue == "Y"; break;

                            //string
                            case "DocTitle": mDocTitle = ParValue; break;
                        }
                    }
                }
                dr.Close();
            }
        }
        private void ShowDocApproval()
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.UserName, ");
            SQL.AppendLine("Case Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' End As StatusDesc, ");
            SQL.AppendLine("Case When A.LastUpDt Is Not Null Then  A.LastUpDt ");
            SQL.AppendLine("Else Null End As LastUpDt, A.Remark ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Left Join TblUser B On A.UserCode=B.UserCode ");
            SQL.AppendLine("Where A.DocType='OTAdjustment' ");
            SQL.AppendLine("And Status In ('A', 'C') ");
            SQL.AppendLine("And A.DocNo=@DocNo ");
            SQL.AppendLine("Order By A.ApprovalDNo;");

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.ShowDataInGrid(
                    ref Grd1, ref cm, SQL.ToString(),
                    new string[] { "UserName", "StatusDesc", "LastUpDt", "Remark" },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 0);
        }

        internal void ShowAttendanceLog()
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Tm From TblAttendanceLog ");
            SQL.AppendLine("Where EmpCode=@EmpCode And Dt=@Dt ");
            SQL.AppendLine("Order By Tm;");

            Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
            Sm.CmParamDt(ref cm, "@Dt", Sm.GetDte(DteOTDt));
            Sm.ShowDataInGrid(
                    ref Grd2, ref cm, SQL.ToString(),
                    new string[]{ "Tm" },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("T2", Grd, dr, c, Row, 0, 0);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd2, 0, 0);
        }

        #endregion

        #endregion

        #region Event

        #region Button Event

        private void BtnOTRequestDocNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmOTAdjustmentDlg(this));
        }

        #endregion

        #endregion
    }
}
