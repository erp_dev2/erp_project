﻿namespace RunSystem
{
    partial class FrmBinUpdateStatus
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.TxtBin = new DevExpress.XtraEditors.TextEdit();
            this.ChkBin = new DevExpress.XtraEditors.CheckEdit();
            this.LueStatus = new DevExpress.XtraEditors.LookUpEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtLot = new DevExpress.XtraEditors.TextEdit();
            this.ChkLot = new DevExpress.XtraEditors.CheckEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.LueLot = new DevExpress.XtraEditors.LookUpEdit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueFontSize.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBin.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkBin.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLot.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkLot.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueLot.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Location = new System.Drawing.Point(0, 487);
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnRefresh
            // 
            this.BtnRefresh.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRefresh.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnRefresh.Appearance.Options.UseBackColor = true;
            this.BtnRefresh.Appearance.Options.UseFont = true;
            this.BtnRefresh.Appearance.Options.UseForeColor = true;
            this.BtnRefresh.Appearance.Options.UseTextOptions = true;
            this.BtnRefresh.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.TxtLot);
            this.panel2.Controls.Add(this.ChkLot);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.LueStatus);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.TxtBin);
            this.panel2.Controls.Add(this.ChkBin);
            this.panel2.Size = new System.Drawing.Size(796, 78);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.LueLot);
            this.panel3.Location = new System.Drawing.Point(0, 78);
            this.panel3.Size = new System.Drawing.Size(796, 431);
            this.panel3.Controls.SetChildIndex(this.Grd1, 0);
            this.panel3.Controls.SetChildIndex(this.LueLot, 0);
            // 
            // Grd1
            // 
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 20;
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.Size = new System.Drawing.Size(796, 431);
            this.Grd1.TabIndex = 18;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // LueFontSize
            // 
            this.LueFontSize.EditValue = "9";
            this.LueFontSize.Location = new System.Drawing.Point(0, 467);
            this.LueFontSize.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFontSize.Properties.Appearance.Options.UseFont = true;
            this.LueFontSize.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFontSize.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueFontSize.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFontSize.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(88, 7);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(23, 14);
            this.label2.TabIndex = 10;
            this.label2.Text = "Bin";
            // 
            // TxtBin
            // 
            this.TxtBin.EnterMoveNextControl = true;
            this.TxtBin.Location = new System.Drawing.Point(115, 4);
            this.TxtBin.Name = "TxtBin";
            this.TxtBin.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBin.Properties.Appearance.Options.UseFont = true;
            this.TxtBin.Properties.MaxLength = 30;
            this.TxtBin.Size = new System.Drawing.Size(224, 20);
            this.TxtBin.TabIndex = 11;
            this.TxtBin.Validated += new System.EventHandler(this.TxtBin_Validated);
            // 
            // ChkBin
            // 
            this.ChkBin.Location = new System.Drawing.Point(341, 3);
            this.ChkBin.Name = "ChkBin";
            this.ChkBin.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkBin.Properties.Appearance.Options.UseFont = true;
            this.ChkBin.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkBin.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkBin.Properties.Caption = " ";
            this.ChkBin.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkBin.Size = new System.Drawing.Size(20, 22);
            this.ChkBin.TabIndex = 12;
            this.ChkBin.ToolTip = "Remove filter";
            this.ChkBin.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkBin.ToolTipTitle = "Run System";
            this.ChkBin.CheckedChanged += new System.EventHandler(this.ChkBin_CheckedChanged);
            // 
            // LueStatus
            // 
            this.LueStatus.EnterMoveNextControl = true;
            this.LueStatus.Location = new System.Drawing.Point(115, 51);
            this.LueStatus.Margin = new System.Windows.Forms.Padding(5);
            this.LueStatus.Name = "LueStatus";
            this.LueStatus.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStatus.Properties.Appearance.Options.UseFont = true;
            this.LueStatus.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStatus.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueStatus.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStatus.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueStatus.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStatus.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueStatus.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStatus.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueStatus.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueStatus.Properties.DropDownRows = 5;
            this.LueStatus.Properties.NullText = "[Empty]";
            this.LueStatus.Properties.PopupWidth = 250;
            this.LueStatus.Size = new System.Drawing.Size(224, 20);
            this.LueStatus.TabIndex = 17;
            this.LueStatus.ToolTip = "F4 : Show/hide list";
            this.LueStatus.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueStatus.EditValueChanged += new System.EventHandler(this.LueStatus_EditValueChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(5, 54);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(106, 14);
            this.label5.TabIndex = 16;
            this.label5.Text = "Change Status To";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtLot
            // 
            this.TxtLot.EnterMoveNextControl = true;
            this.TxtLot.Location = new System.Drawing.Point(115, 26);
            this.TxtLot.Name = "TxtLot";
            this.TxtLot.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLot.Properties.Appearance.Options.UseFont = true;
            this.TxtLot.Properties.MaxLength = 40;
            this.TxtLot.Size = new System.Drawing.Size(224, 20);
            this.TxtLot.TabIndex = 14;
            this.TxtLot.Validated += new System.EventHandler(this.TxtLot_Validated);
            // 
            // ChkLot
            // 
            this.ChkLot.Location = new System.Drawing.Point(341, 25);
            this.ChkLot.Name = "ChkLot";
            this.ChkLot.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkLot.Properties.Appearance.Options.UseFont = true;
            this.ChkLot.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkLot.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkLot.Properties.Caption = " ";
            this.ChkLot.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkLot.Size = new System.Drawing.Size(20, 22);
            this.ChkLot.TabIndex = 15;
            this.ChkLot.ToolTip = "Remove filter";
            this.ChkLot.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkLot.ToolTipTitle = "Run System";
            this.ChkLot.CheckedChanged += new System.EventHandler(this.ChkLot_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(86, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(25, 14);
            this.label1.TabIndex = 13;
            this.label1.Text = "Lot";
            // 
            // LueLot
            // 
            this.LueLot.EnterMoveNextControl = true;
            this.LueLot.Location = new System.Drawing.Point(214, 22);
            this.LueLot.Margin = new System.Windows.Forms.Padding(5);
            this.LueLot.Name = "LueLot";
            this.LueLot.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLot.Properties.Appearance.Options.UseFont = true;
            this.LueLot.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLot.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueLot.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLot.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueLot.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLot.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueLot.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLot.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueLot.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueLot.Properties.DropDownRows = 25;
            this.LueLot.Properties.NullText = "[Empty]";
            this.LueLot.Properties.PopupWidth = 200;
            this.LueLot.Size = new System.Drawing.Size(130, 20);
            this.LueLot.TabIndex = 32;
            this.LueLot.ToolTip = "F4 : Show/hide list";
            this.LueLot.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueLot.EditValueChanged += new System.EventHandler(this.LueLot_EditValueChanged);
            this.LueLot.Validated += new System.EventHandler(this.LueLot_Validated);
            this.LueLot.Leave += new System.EventHandler(this.LueLot_Leave);
            this.LueLot.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueLot_KeyDown);
            // 
            // FrmBinUpdateStatus
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(866, 509);
            this.KeyPreview = true;
            this.Name = "FrmBinUpdateStatus";
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueFontSize.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBin.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkBin.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLot.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkLot.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueLot.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.TextEdit TxtBin;
        private DevExpress.XtraEditors.CheckEdit ChkBin;
        private DevExpress.XtraEditors.LookUpEdit LueStatus;
        private System.Windows.Forms.Label label5;
        private DevExpress.XtraEditors.TextEdit TxtLot;
        private DevExpress.XtraEditors.CheckEdit ChkLot;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.LookUpEdit LueLot;
    }
}