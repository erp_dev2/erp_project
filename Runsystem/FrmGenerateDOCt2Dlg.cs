﻿#region update
/*
    31/01/2020 [WED/SIER] new apps
    18/06/2020 [VIN/SIER] customer yang ditampilkan berdasarkan CtCtCode di frmparent 
    08/03/2023 [WED/KBN] tambah kolom 18 di parent grid yang di 0-kan ketika choose data
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmGenerateDOCt2Dlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmGenerateDOCt2 mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmGenerateDOCt2Dlg(FrmGenerateDOCt2 FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        #region Form Load
        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            SetGrd();
            SetSQL();
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT A.CtCode, B.DNo, A.CtName SAName, IFNULL(B.Address, A.Address) SAAddress, ");
            SQL.AppendLine("IFNULL(C2.CityName, C1.CityName) SACityName, IFNULL(D2.CntName, D1.CntName) SACntName, ");
            SQL.AppendLine("IFNULL(B.PostalCd, A.PostalCd) SAPostalCd, IFNULL(B.Phone, A.Phone) SAPhone, ");
            SQL.AppendLine("IFNULL(B.Fax, A.Fax) SAFax, IFNULL(B.Email, A.Email) SAEmail, IFNULL(B.Mobile, A.Mobile) SAMobile ");
            SQL.AppendLine("FROM TblCustomer A ");
            SQL.AppendLine("INNER JOIN TblCustomerShipAddress B ON A.CtCode = B.CtCode ");
            SQL.AppendLine("    AND A.ActInd = 'Y' ");
            SQL.AppendLine("Left join TblCity C1 On A.CityCode = C1.CityCode ");
            SQL.AppendLine("Left join TblCity C2 On B.CityCode = C2.CityCode ");
            SQL.AppendLine("Left join TblCountry D1 On A.CntCode = D1.CntCode ");
            SQL.AppendLine("Left join TblCountry D2 On B.CntCode = D2.CntCode ");
            SQL.AppendLine("Where A.CtCtCode = @CtCtCode ");


            mSQL = SQL.ToString();
        }
        #endregion

        #region Set Grid

        private void SetGrd()
        {
            Grd1.Cols.Count = 14;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "", 
                    "Customer's Code",
                    "",
                    "Customer's Name",
                    "DNo",

                    //6-10
                    "Shipping's Address",
                    "Shipping's City",
                    "Shipping's Country",
                    "Shipping's Postal",
                    "Shipping's Phone",

                    //11-13
                    "Shipping's Fax",
                    "Shipping's Email",
                    "Shipping's Mobile"
                }, new int[]
                {
                    //0
                    50,

                    //1-5
                    20, 120, 20, 280, 0,
                    
                    //6-10
                    200, 180, 180, 100, 100, 

                    //11-13
                    100, 100, 100
                }
            );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColButton(Grd1, new int[] { 3 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 5 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3 }, !ChkHideInfoInGrd.Checked);
        }
        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmCustomer(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mCtCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmCustomer(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mCtCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    Grd1.Cells[Row, 1].Value = !IsSelected;
            }
        }

        #endregion

        #region Show Data

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " And 0 = 0 ";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtCtName.Text, new string[] { "A.CtCode", "A.CtName" });
                Sm.CmParam<String>(ref cm, "@CtCtCode", Sm.GetLue(mFrmParent.LueCtCtCode));

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By A.CtName, B.DNo; ",
                    new string[]
                    {
                        //0
                        "CtCode", 

                        //1-5
                        "SAName", "DNo", "SAAddress", "SACityName", "SACntName", 

                        //6-10
                        "SAPostalCd", "SAPhone", "SAFax", "SAEmail", "SAMobile", 
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd1.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 0);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 1);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 2);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 3);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 4);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 8, 5);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 9, 6);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 10, 7);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 11, 8);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 12, 9);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 13, 10);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 2);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 1, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 2, Grd1, Row2, 5);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 3, Grd1, Row2, 4);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 4, Grd1, Row2, 6);
                        mFrmParent.Grd1.Cells[Row1, 8].Value = Sm.FormatNum(mFrmParent.mGenerateDOCt2UPrice, 0);
                        mFrmParent.Grd1.Cells[Row1, 9].Value = Sm.FormatNum(mFrmParent.mGenerateDOCt2RentPrice, 0);
                        mFrmParent.Grd1.Cells[Row1, 10].Value = Sm.FormatNum(mFrmParent.mGenerateDOCt2RetributionPrice, 0);
                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, Row1, new int[] { 5, 6, 7, 11, 12, 13, 16, 18 });

                        mFrmParent.Grd1.Rows.Add();
                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 5, 6, 7, 8, 9, 10, 11, 12, 13, 16, 17, 18 });
                    }
                }
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 customer.");
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtCtName_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkCtName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Customer");
        }

        #endregion

        #endregion

    }
}
