﻿#region Update
/*
    29/03/2020 [TKG/KSM] New Apps
    12/05/2020 [DITA/KSM] tambah lebar grid di kolom docno
    19/05/2020 [VIN/KSM] tambah Quantity, Uom, IDR, Price, Total, Delivery, dan Term of Payment
    28/05/2020 [HAR/KSM] filter  belum jalan
    17/06/2020 [IBL/KSM] filter tanggal belum jalan
    15/12/2020 [VIN/KSM] local DocNo kurang lebar
    22/02/2021 [BRI/KSM] Menambahkan grid Customer Category berdasarkan parameter=mIsCustomerComboShowCategory
 *  19/04/2021 [BRI/KSM] tambah kolom price after tax dan total after tax
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmSalesContractFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmSalesContract mFrmParent;
        
        #endregion

        #region Constructor

        public FrmSalesContractFind(FrmSalesContract FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -30);
                Sl.SetLueSiteCode(ref LueSiteCode);
                Sl.SetLueCtCode(ref LueCtCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private string GetSQL(string Filter, string Filter2)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.* From ( ");
            SQL.AppendLine("    Select A.DocNo, A.DocDt, A.CancelInd, A.LocalDocNo, A.SalesMemoDocNo, B.LocalDocNo As SalesMemoLocalDocNo,   "); 
            SQL.AppendLine("    C.CtName, E.CtCtName, D.SiteName, A.Remark, A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt, F.Qty, G.SalesUomCode UOM,  ");
            SQL.AppendLine("    F.UPrice, F.DeliveryDt, F.UPrice*F.Qty As Total, F.UpriceAfterTax, F.UPriceAfterTax*F.Qty As TotalAfterTax, B.CurCode, H.PtName, B.CtCode, B.SiteCode  ");
            SQL.AppendLine("    From TblSalesContract A   ");
            SQL.AppendLine("    Inner Join TblSalesMemoHdr B ON A.SalesMemoDocNo=B.DocNo  ");
            SQL.AppendLine("    Left Join TblCustomer C ON B.CtCode=C.CtCode  ");
            SQL.AppendLine("    Left Join TblSite D ON B.SiteCode=D.SiteCode  ");
            SQL.AppendLine("    Left Join TblCustomerCategory E ON C.CtCtCode=E.CtCtCode  ");
            SQL.AppendLine("    INNER Join TblSalesMemoDtl F ON F.DocNo=B.Docno ");
            SQL.AppendLine("    left Join TblItem G On F.ItCode=G.ItCode ");
            SQL.AppendLine("    left JOIN tblpaymentterm H ON H.PtCode=A.PtCode  ");
            SQL.AppendLine("    Where A.DocDt Between @DocDt1 And @DocDt2  ");
            SQL.AppendLine(Filter);
            SQL.AppendLine(") T ");
            SQL.AppendLine(Filter2);
            SQL.AppendLine(" Order By T.DocDt Desc, T.DocNo;");

            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 26;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Document#", 
                    "Date",
                    "Cancel",
                    "Local#",
                    "Sales Memo#",

                    //6-10
                    "Memo Local#",
                    "Customer", 
                    "Site",
                    "Remark",
                    "Created By",

                    //11-15
                    "Created Date",
                    "Created Time", 
                    "Last Updated By", 
                    "Last Updated Date", 
                    "Last Updated Time",

                    //16-20
                    "Quantity",
                    "UoM",
                    "Currency",
                    "Delivery",
                    "Price",

                    //21-25
                    "Total",
                    "Term of Payment",
                    "Customer Category",
                    "Total After Tax",
                    "Price After Tax"




                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    180, 80, 80, 180, 150, 
                    
                    //6-10
                    150, 200, 200, 200, 130, 
                    
                    //11-15
                    130, 130, 130, 130, 130,

                    //16-20
                    100, 100, 130, 130, 130,

                    //21-25
                    130, 130, 200, 130, 130
                }
            );
            Sm.GrdColCheck(Grd1, new int[] { 3 });
            Sm.GrdFormatDate(Grd1, new int[] { 2, 11, 14, 19 });
            Sm.GrdFormatTime(Grd1, new int[] { 12, 15 });
            Sm.GrdColInvisible(Grd1, new int[] { 10, 11, 12, 13, 14, 15 }, false);
            Sm.SetGrdProperty(Grd1, false);
            Sm.GrdFormatDec(Grd1, new int[] { 16, 20, 21, 24, 25 }, 0);
            if (!mFrmParent.mIsCustomerComboShowCategory)
                Sm.GrdColInvisible(Grd1, new int[] { 23 }, false);
            Grd1.Cols[23].Move(8);
            Grd1.Cols[16].Move(9);
            Grd1.Cols[17].Move(10);
            Grd1.Cols[18].Move(11);
            Grd1.Cols[19].Move(12);
            Grd1.Cols[20].Move(13);
            Grd1.Cols[21].Move(14);
            Grd1.Cols[25].Move(15);
            Grd1.Cols[24].Move(16);
            Grd1.Cols[22].Move(17);

   

        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 10, 11, 12, 13, 14, 15 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = "And 0=0 ";
                string Filter2 = "Where 0=0 ";
                int Row1 = 0, Row2 = 0;
                var cm = new MySqlCommand();


                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, new string[] { "A.DocNo", "A.LocalDocNo" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCode), "B.CtCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "B.SiteCode", true);
                Sm.FilterStr(ref Filter2, ref cm, TxtSalesMemoDocNo.Text, new string[] { "SalesMemoDocNo", "SalesMemoLocalDocNo" });

                Sm.ShowDataInGrid(
                    
                        ref Grd1, ref cm, GetSQL(Filter, Filter2),
                        new string[]
                        {
                            //0
                            "DocNo", 

                            //1-5
                            "DocDt", "CancelInd", "LocalDocNo", "SalesMemoDocNo", "SalesMemoLocalDocNo", 
                            
                            //6-10
                            "CtName", "SiteName", "Remark", "CreateBy", "CreateDt", 
                            
                            //11-15
                            "LastUpBy", "LastUpDt", "Qty", "UOM", "DeliveryDt",

                            //16-20
                            "UPrice", "Total", "UPriceAfterTax", "TotalAfterTax", "CurCode", 

                            //21-22
                            "PtName", "CtCtName"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Row1 = mFrmParent.Grd1.Rows.Count - 1;
                            Row2 = Row;
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 12, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 11);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 14, 12);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 15, 12);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 14);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 19, 15);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 16);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 17);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 25, 18);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 24, 19);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 20);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 21);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 22);

                            //Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 5, Grd1, Row2, 16);

                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue1(Sl.SetLueSiteCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }
        private void TxtSalesMemoDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkSalesMemoDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Sales memo#");
        }

        #endregion

        #endregion
    }
}