﻿#region Update
/*
    16/06/2020 [WED/IMS] new apps
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptProjectBudgetControl2Dlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmRptProjectBudgetControl2 mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmRptProjectBudgetControl2Dlg (FrmRptProjectBudgetControl2 FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -30);
                ShowData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, ");
            SQL.AppendLine("A.LocalDocNo, ");
            SQL.AppendLine("Case A.Status When 'O' Then 'Outstanding' When 'A' Then 'Approve' End As StatusDesc, A.CancelInd, ");
            SQL.AppendLine("A.SAAddress, B.CtName, A.BOQDocNo, (A.Amt + A.AmtBOM) Amt, ");
            SQL.AppendLine("C.ItCode, D.ItCodeInternal, D.ItName, D.Specification, E.CtItCode, E.CtItName, C.DeliveryDt, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt, ");
            SQL.AppendLine("IfNull(H.ProjectCode, A.ProjectCode2) ProjectCode, IfNull(H.ProjectName, G.ProjectName) ProjectName, I.DocNo As NTPDocNo, A.PONo ");
            SQL.AppendLine("From TblSOContractHdr A ");
            SQL.AppendLine("Inner Join tblCustomer B on A.CtCode = B.CtCode And A.DocType = '2' ");
            SQL.AppendLine("Inner Join TblSOContractDtl C On A.DocNo=C.DocNo  ");
            SQL.AppendLine("Inner Join TblItem D On C.ItCode=D.ItCode ");
            SQL.AppendLine("Left Join TblCustomerItem E On C.ItCode=E.ItCode And A.CtCode=E.CtCode ");
            SQL.AppendLine("Inner Join TblBOQHdr F On A.BOQDocNo=F.DocNo ");
            SQL.AppendLine("Inner Join TblLOphdr G On F.LOPDocNo=G.DocNO ");
            if (mFrmParent.mIsFilterBySite)
            {
                SQL.AppendLine("And (G.SiteCode Is Null Or ( ");
                SQL.AppendLine("    G.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(G.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("            ) ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine(")) ");
            }
            SQL.AppendLine("Left Join TblProjectGroup H On H.PGCode=G.PGCode ");
            SQL.AppendLine("Left Join TblNoticeToProceed I On G.DocNo = I.LOPDocNo ");
            SQL.AppendLine("Where (A.DocDt Between @DocDt1 And @DocDt2) ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 27;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Document#", 
                    "Date",
                    "Status",
                    "Cancel",
                    "Local#",
                    
                    //6-10
                    "Customer",
                    "Bill Of Quantity",
                    "Shipping"+Environment.NewLine+"Address",
                    "Amount",
                    "Item's Code",

                    //11-15
                    "Local Code",
                    "Item's Name",
                    "Customer's"+Environment.NewLine+"Item Code",
                    "Customer's"+Environment.NewLine+"Item Name",
                    "Specification",

                    //16-20
                    "Delivery Date",
                    "Created"+Environment.NewLine+"By",
                    "Created"+Environment.NewLine+"Date", 
                    "Created"+Environment.NewLine+"Time", 
                    "Last"+Environment.NewLine+"Updated By",

                    //21-25
                    "Last"+Environment.NewLine+"Updated Date", 
                    "Last"+Environment.NewLine+"Updated Time",
                    "Project Code",
                    "Project Name",
                    "Notice To Proceed#",

                    //26
                    "Customer's PO#"
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    130, 80, 80, 60, 130, 
                    
                    //6-10
                    200, 130, 200, 120, 100,  

                    //11-15
                    100, 250, 100, 200, 250, 

                    //16-20
                    100, 100, 100, 100, 100, 

                    //21-25
                    100, 100, 120, 200, 120,

                    //26
                    100
                }
            );
            Sm.GrdColCheck(Grd1, new int[] { 4 });
            Sm.GrdFormatDec(Grd1, new int[] { 9 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2, 16, 18, 21 });
            Sm.GrdFormatTime(Grd1, new int[] { 19, 22 });
            Sm.GrdColInvisible(Grd1, new int[] { 10, 11, 17, 18, 19, 20, 21, 22 }, false);
            Grd1.Cols[23].Move(17);
            Grd1.Cols[24].Move(18);
            Grd1.Cols[25].Move(19);
            Grd1.Cols[26].Move(20);
            if (!mFrmParent.mIsCustomerItemNameMandatory)
                Sm.GrdColInvisible(Grd1, new int[] { 13, 14 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 10, 11 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " And 0 = 0 ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, mSQL + Filter + " Order By A.DocDt, A.DocNo; ",
                    new string[] 
                    { 
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "StatusDesc", "CancelInd", "LocalDocNo", "CtName",   
                        
                        //6-10
                        "BOQDocNo", "SAAddress", "Amt", "ItCode", "ItCodeInternal", 
                        
                        //11-15
                        "ItName", "CtItCode", "CtItName", "Specification","DeliveryDt", 

                        //16-20
                        "CreateBy", "CreateDt", "LastUpBy", "LastUpDt", "ProjectCode",

                        //21-23
                        "ProjectName", "NTPDocNo","PONo"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 14);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 16, 15);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 16);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 18, 17);
                        Sm.SetGrdValue("T", Grd, dr, c, Row, 19, 17);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 18);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 21, 19);
                        Sm.SetGrdValue("T", Grd, dr, c, Row, 22, 19);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 20);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 21);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 22);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 23);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            string ContactPerson = string.Empty;
            if (Sm.IsFindGridValid(Grd1, 0))
            {
                mFrmParent.TxtSOContractDocNo.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
                mFrmParent.ChkSOContractDocNo.Checked = true;
                this.Close();
            }
        }

        #region Grid Method

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            Sm.GrdExpand(Grd1);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt1).Length == 0) DteDocDt1.EditValue = DteDocDt2.EditValue;
        }

        #endregion

        #endregion

    }
}
