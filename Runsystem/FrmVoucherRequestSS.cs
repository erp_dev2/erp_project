﻿#region Update
/*
    08/08/2017 [TKG] ditambah entity untuk journal
    10/03/2018 [TKG] filter site
    14/04/2018 [TKG] menggunakan format khusus voucher atau format standard dokumen run system berdasarkan parameter VoucherCodeFormatType
    08/05/2018 [ARI] printout (TWC dan IOK)
    19/02/2019 [TKG] validasi monthly closing untuk cancel menggunakan tanggal hari ini.
    25/09/2019 [TKG] kalau parameter AcNoForPremiumDebtSS kosong, maka journal untuk premium debt tidak akan diproses
    09/06/2020 [TKG/TWC+IMS+MMM] Berdasarkan parameter IsVoucherRequestSSJournalDisabled, journal voucher request SS dinonatifkan. 
    03/08/2020 [VIN/SRN] Menyamakan printout dengan TWC
    01/09/2020 [WED/SRN] ada input Adjustment yg mempengaruhi nilai Paid Amount, berdasarkan parameter IsVoucherRequestSSUseAdjustment dan AllowedVoucherRequestSSAdjustmentAmt
    15/09/2020 [ICA/SRN] Menambah VoucherRequestDocNo di printout
    01/11/2020 [TKG/IMS] Journal menggunakan setting jenis social security
    05/11/2020 [DITA/IMS+ALL] Feedback validasi untuk untuk user yg tidak punya akses ke menu list emp ss BtnEmpSSListDocNo2 di disabled (kasus : waktu dipanggil di voucherrequestforbudget)
    16/11/2020 [DITA/IMS+ALL] SetFormForGroupUser ambil dari stabdard method
    17/01/2021 [TKG/PHT] ubah GenerateVoucherRequestDocNo
    10/03/2021 [WED/IMS] Journal yang dibikin bisa multiple berdasarkan cost center, berdasarkan parameter IsVoucherRequestSSJournalBasedOnCostCenter
    20/05/2021 [ICA/IMS] Saat save journal bisa memilih AcNo yg sama dari Department(employer) dan SS (employee) 
    23/06/2021 [TKG/IMS] ubah proses journal
    28/07/2021 [WED/PHT] tambah input Budget berdasarkan parameter IsVoucherRequestSSUseBudget
    29/07/2021 [TKG/IMS] ubah journal cancel vr ss ims dan ditambah validasi ActInd ketika join dengan TblCostCenter
    24/09/2021 [TKG/ALL] tambah validasi setting journal, ubah GetParameter
    25/10/2021 [TKG/TWC] menggunakan parameter IsVoucherDocSeqNoEnabled saat membuat Voucher Request#
    01/12/2021 [DITA/PHT] untuk parameter SSPCodeForPension value nya bisa lebih dari 1, diubah menggunakan find_in_set bukan comparestr lagi
    12/05/2022 [VIN/PHT] BUG VR SS IsVoucherRequestSSJournalBasedOnCostCenter : 1. array , 2. Credit Amount 
    18/10/2022 [RDA/PHT] validasi monthly journal closing
    25/10/2022 [RDA/PHT] validasi monthly journal closing melihat profit center code dari employee code yang dipilih
    01/02/2023 [BRI/PHT] merubah validasi clossing journal berdasarkan param IsVoucherRequestSSTransactionValidatedbyClosingJournal
    15/02/2023 [MAU/PHT] BUG : validasi Closing journal Entries VR SS
 */
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using System.Text.RegularExpressions;


#endregion

namespace RunSystem
{
    public partial class FrmVoucherRequestSS : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mDocNo = string.Empty, mGrpCode = string.Empty, mMenuCodeForEmpSSList = string.Empty;
        internal FrmVoucherRequestSSFind FrmFind;
        private bool 
            mIsAutoJournalActived = false,
            mIsVoucherRequestSSJournalDisabled = false,
            mIsVoucherRequestSSUseAdjustment = false,
            mIsVoucherRequestSSUseSSCOASetting = false,
            mIsVoucherRequestSSJournalBasedOnCostCenter = false,
            mIsCOACouldBeChosenMoreThanOnce = false,
            mIsJournalValidationVoucherRequestSSEnabled = false,
            mIsFilterByDeptHR = false,
            mIsCheckCOAJournalNotExists = false,
            mIsVoucherRequestSSTransactionValidatedbyClosingJournal = false;
        internal bool mIsFilterBySiteHR = false, mIsVoucherRequestSSUseBudget = false;
        private string
            mVoucherCodeFormatType = "1",
            mAcNoForPremiumDebtSSHealth = string.Empty,
            mAcNoForAllowanceSSHealth = string.Empty,
            mAcNoForPremiumDebtSSEmployment = string.Empty,
            mAcNoForAllowanceSSEmployment = string.Empty,
            mAcNoForPremiumDebtSSPension = string.Empty,
            mAcNoForAllowanceSSPension = string.Empty,
            mSSPCodeForEmployment = string.Empty,
            mSSPCodeForHealth = string.Empty,
            mSSPCodeForPension = string.Empty,
            mDocTitle = string.Empty;

        private decimal mAllowedVoucherRequestSSAdjustmentAmt = 0m;

        #endregion

        #region Constructor

        public FrmVoucherRequestSS(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Voucher Request For Social Security";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                TcVoucherRequestSS.SelectedTab = TpBudget;
                if (!mIsVoucherRequestSSUseBudget) TcVoucherRequestSS.TabPages.Remove(TpBudget);
                else Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mIsFilterByDeptHR ? "Y" : "N");

                TcVoucherRequestSS.SelectedTab = TpGeneral;

                SetFormControl(mState.View);

                if (!mIsVoucherRequestSSUseAdjustment)
                {
                    LblAdjustmentAmt.Visible = TxtAdjustmentAmt.Visible = false;
                }

                Sl.SetLueAcType(ref LueAcType);
                Sl.SetLueOption(ref LuePaymentType, "VoucherPaymentType");
                Sl.SetLueBankAcCode(ref LueBankAcCode);
                Sl.SetLueBankCode(ref LueBankCode);
                Sl.SetLueCurCode(ref LueCurCode);

                Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                { 
                    TxtDocNo, TxtStatus, TxtEmpSSListDocNo, TxtYr, TxtMth, 
                    TxtSSPCode, TxtEmployer, TxtEmployee, TxtTotal, TxtVoucherRequestDocNo,
                    TxtVoucherDocNo, TxtAmt
                }, true);
                mMenuCodeForEmpSSList = Sm.GetValue("Select Menucode from TblMenu where Param = 'FrmEmpSSList'");
                mGrpCode = Sm.SetFormForGroupUser(mMenuCodeForEmpSSList);
                if (mGrpCode.Length == 0) BtnEmpSSListDocNo2.Enabled = false;

                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 4;
            Sm.GrdHdrWithColWidth(
                Grd1,
                new string[] 
                {
                    //0
                    "DNo",

                    //1-3
                    "Description",
                    "Amount",
                    "Remark"
                },
                new int[]{ 0, 300, 120, 300 }
            );
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2 });
            Sm.GrdFormatDec(Grd1, new int[] { 2 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 0 }, false);

            Grd2.Cols.Count = 5;
            Grd2.ReadOnly = true;
            Grd2.FrozenArea.ColCount = 1;
            Sm.GrdHdrWithColWidth(
                Grd2,
                new string[] 
                {
                    //0
                    "No",

                    //1-4
                    "User", 
                    "Status",
                    "Date",
                    "Remark"
                },
                new int[]{ 40, 150, 100, 100, 300 }
            );
            Sm.GrdFormatDate(Grd2, new int[] { 3 });
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, ChkCancelInd, LueAcType, LuePaymentType, LueBankAcCode, 
                        LueBankCode, TxtGiroNo, DteDueDt, TxtPaymentUser, TxtPaidToBankCode, 
                        TxtPaidToBankBranch, TxtPaidToBankAcName, TxtPaidToBankAcNo, LueCurCode, MeeRemark,
                        TxtAdjustmentAmt, LueDeptCode, LueBCCode
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 3 });
                    BtnEmpSSListDocNo.Enabled = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, ChkCancelInd, LueAcType, LuePaymentType, LueBankAcCode, 
                        TxtPaymentUser, TxtPaidToBankCode, TxtPaidToBankBranch, TxtPaidToBankAcName, TxtPaidToBankAcNo, 
                        LueCurCode, MeeRemark, LueDeptCode, LueBCCode
                    }, false);
                    if (mIsVoucherRequestSSUseAdjustment)
                    {
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                        { 
                            TxtAdjustmentAmt
                        }, false);
                    }
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 3 });
                    BtnEmpSSListDocNo.Enabled = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    ChkCancelInd.Properties.ReadOnly = false;
                    TxtDocNo.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, TxtStatus, TxtEmpSSListDocNo, TxtYr, TxtMth, 
                TxtSSPCode, TxtVoucherRequestDocNo, TxtVoucherDocNo, DteDocDt, LueAcType, 
                LuePaymentType, LueBankAcCode, LueBankCode, TxtGiroNo, DteDueDt, 
                TxtPaymentUser, TxtPaidToBankCode, TxtPaidToBankBranch, TxtPaidToBankAcName, TxtPaidToBankAcNo, 
                LueCurCode, MeeRemark, LueDeptCode, LueBCCode
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtEmployer, TxtEmployee, TxtTotal, TxtAmt, TxtAdjustmentAmt, TxtRemainingBudget }, 0);
            ChkCancelInd.Checked = false;
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ClearGrd()
        {
            ClearGrd1();
            Grd2.Rows.Clear();
            Grd2.Rows.Count = 1;
        }

        internal void ClearGrd1()
        {
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 2 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmVoucherRequestSSFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sm.SetLue(LueCurCode, Sm.GetParameter("MainCurCode"));
                Sm.SetLue(LueAcType, "C");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            try
            {
                if(ChkCancelInd.Checked == false)
                ParPrint(TxtDocNo.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditTrimString(Grd1, new int[] { 3 }, e);
        }

        #endregion

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (
                Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No ||
                IsInsertedDataNotValid()
                ) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "VoucherRequestSS", "TblVoucherRequestSSHdr");
            string VoucherRequestDocNo = string.Empty;

            if (mVoucherCodeFormatType == "2")
                VoucherRequestDocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "VoucherRequest", "TblVoucherRequestHdr");
            else
                VoucherRequestDocNo = GenerateVoucherRequestDocNo();

            var cml = new List<MySqlCommand>();

            cml.Add(SaveVoucherRequestSSHdr(DocNo, VoucherRequestDocNo));
            cml.Add(SaveVoucherRequestSSDtl(DocNo));
            cml.Add(SaveVoucherRequestHdr(VoucherRequestDocNo, DocNo));
            cml.Add(SaveVoucherRequestDtl(VoucherRequestDocNo));
            cml.Add(UpdateEmpSSListHdr());
            if (mIsAutoJournalActived && !IsNeedApproval() && !mIsVoucherRequestSSJournalDisabled)
            {
                if (mIsVoucherRequestSSJournalBasedOnCostCenter)
                {
                    var l = new List<JournalHdr>();
                    var l2 = new List<JournalDtl>();
                    var l21 = new List<JournalDtlTemp>();
                    var l3 = new List<DtlCostCenter>();
                    var l4 = new List<DtlDept>();

                    PrepDetailData(ref l4);
                    if (l4.Count > 0)
                    {
                        SumByCostCenter(ref l4, ref l3);
                        GetJournalHdr(ref l3, ref l);
                        GetJournalDtl(ref l4, ref l21);
                        SumJournalDtl(ref l21, ref l2);
                        GetDocNoJournalDtl(ref l, ref l2);

                        foreach (var x in l) cml.Add(SaveJournalHdr(x, DocNo));
                        foreach (var x in l2) cml.Add(SaveJournalDtl(x, DocNo));
                    }
                }
                else
                {
                    if (mIsVoucherRequestSSUseSSCOASetting)
                        cml.Add(SaveJournal2(DocNo));
                    else
                        cml.Add(SaveJournal(DocNo));
                }
            }

            Sm.ExecCommands(cml);

            BtnInsertClick(sender, e);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsTxtEmpty(TxtEmpSSListDocNo, "Social security#", false) ||
                //mIsVoucherRequestSSTransactionValidatedbyClosingJournal ?
                //Sm.IsClosingJournalInvalid(mIsAutoJournalActived, false, Sm.GetDte(DteDocDt), GetProfitCenterCode()) :
                //(!mIsVoucherRequestSSJournalDisabled && Sm.IsClosingJournalInvalid(mIsAutoJournalActived, false, Sm.GetDte(DteDocDt), GetProfitCenterCode())) ||
                (mIsVoucherRequestSSTransactionValidatedbyClosingJournal && Sm.IsClosingJournalInvalid(mIsAutoJournalActived, false, Sm.GetDte(DteDocDt), GetProfitCenterCode())) ||  

                Sm.IsLueEmpty(LueAcType, "Account Type") ||
                Sm.IsLueEmpty(LuePaymentType, "Payment type") ||
                Sm.IsLueEmpty(LueBankAcCode, "Account") ||
                Sm.IsLueEmpty(LueCurCode, "Currency") ||
                IsAdjustmentAmountInvalid() ||
                //Sm.IsClosingJournalInvalid(Sm.GetDte(DteDocDt)) ||
                IsPaymentTypeNotValid() ||
                IsBudgetCategoryInvalid() ||
                IsAmountNotValid() ||
                IsEmpSSListAlreadyCancelled() ||
                IsEmpSSListAlreadyProcessed() ||
                IsJournalSettingInvalid();
        }

        private bool IsBudgetCategoryInvalid()
        {
            if (!mIsVoucherRequestSSUseBudget) return false;

            if (Sm.GetLue(LueBCCode).Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Budget Category is empty.");
                TcVoucherRequestSS.SelectedTab = TpBudget;
                LueBCCode.Focus();
                return true;
            }

            return false;
        }

        private bool IsAmountNotValid()
        {
            if (!mIsVoucherRequestSSUseBudget) return false;

            ComputeRemainingBudget();
            if (Decimal.Parse(TxtRemainingBudget.Text) < 0m)
            {
                Sm.StdMsg(mMsgType.Warning, "Amount should not be greater than available budget.");
                TxtAmt.Focus();
                return true;
            }

            return false;
        }

        private bool IsJournalSettingInvalid()
        {
            if (!mIsAutoJournalActived || 
                mIsVoucherRequestSSJournalDisabled ||
                !mIsCheckCOAJournalNotExists
                ) return false;

            var SQL = new StringBuilder();
            var Msg =
                "Journal's setting is invalid." + Environment.NewLine +
                "Please contact Finance/Accounting department." + Environment.NewLine;
            string SSPCode = string.Empty;

            if (mIsVoucherRequestSSJournalBasedOnCostCenter)
            {
                //Table
                //if (IsJournalSettingInvalid_EmployeeCostGroup(Msg)) return true;

                SSPCode = Sm.GetValue("Select SSPCode From TblEmpSSListHdr Where DocNo=@Param;", TxtEmpSSListDocNo.Text);

                if (!mIsVoucherRequestSSUseSSCOASetting)
                {
                    if (Sm.CompareStr(SSPCode, mSSPCodeForHealth))
                    {
                        if (mAcNoForAllowanceSSHealth.Length == 0)
                        {
                            Sm.StdMsg(mMsgType.Warning, Msg + "Parameter AcNoForAllowanceSSHealth is empty.");
                            return true;
                        }
                        if (mAcNoForPremiumDebtSSHealth.Length == 0)
                        {
                            Sm.StdMsg(mMsgType.Warning, Msg + "Parameter AcNoForPremiumDebtSSHealth is empty.");
                            return true;
                        }
                    }

                    if (Sm.CompareStr(SSPCode, mSSPCodeForEmployment))
                    {
                        if (mAcNoForAllowanceSSEmployment.Length == 0)
                        {
                            Sm.StdMsg(mMsgType.Warning, Msg + "Parameter AcNoForAllowanceSSEmployment is empty.");
                            return true;
                        }
                        if (mAcNoForPremiumDebtSSEmployment.Length == 0)
                        {
                            Sm.StdMsg(mMsgType.Warning, Msg + "Parameter AcNoForPremiumDebtSSEmployment is empty.");
                            return true;
                        }
                    }
                    if (Sm.Find_In_Set(SSPCode, mSSPCodeForPension))
                    {
                        if (mAcNoForAllowanceSSPension.Length == 0)
                        {
                            Sm.StdMsg(mMsgType.Warning, Msg + "Parameter AcNoForAllowanceSSPension is empty.");
                            return true;
                        }
                        if (mAcNoForPremiumDebtSSPension.Length == 0)
                        {
                            Sm.StdMsg(mMsgType.Warning, Msg + "Parameter AcNoForPremiumDebtSSPension is empty.");
                            return true;
                        }
                    }
                }
            }
            else
            {
                if (mIsVoucherRequestSSUseSSCOASetting)
                {
                    //Table
                    if (IsJournalSettingInvalid_Department(Msg)) return true;
                    if (IsJournalSettingInvalid_SS(Msg)) return true;
                }
                else
                {
                    //Parameter
                    SSPCode = Sm.GetValue("Select SSPCode From TblEmpSSListHdr Where DocNo=@Param;", TxtEmpSSListDocNo.Text);
                    if (Sm.CompareStr(SSPCode, mSSPCodeForHealth))
                    {
                        if (mAcNoForAllowanceSSHealth.Length == 0)
                        {
                            Sm.StdMsg(mMsgType.Warning, Msg + "Parameter AcNoForAllowanceSSHealth is empty.");
                            return true;
                        }
                        if (mAcNoForPremiumDebtSSHealth.Length == 0)
                        {
                            Sm.StdMsg(mMsgType.Warning, Msg + "Parameter AcNoForPremiumDebtSSHealth is empty.");
                            return true;
                        }
                    }
                    if (Sm.CompareStr(SSPCode, mSSPCodeForEmployment))
                    {
                        if (mAcNoForAllowanceSSEmployment.Length == 0)
                        {
                            Sm.StdMsg(mMsgType.Warning, Msg + "Parameter AcNoForAllowanceSSEmployment is empty.");
                            return true;
                        }
                        if (mAcNoForPremiumDebtSSEmployment.Length == 0)
                        {
                            Sm.StdMsg(mMsgType.Warning, Msg + "Parameter AcNoForPremiumDebtSSEmployment is empty.");
                            return true;
                        }
                    }
                    if (Sm.Find_In_Set(SSPCode, mSSPCodeForPension))
                    {
                        if (mAcNoForAllowanceSSPension.Length == 0)
                        {
                            Sm.StdMsg(mMsgType.Warning, Msg + "Parameter AcNoForAllowanceSSPension is empty.");
                            return true;
                        }
                        if (mAcNoForPremiumDebtSSPension.Length == 0)
                        {
                            Sm.StdMsg(mMsgType.Warning, Msg + "Parameter AcNoForPremiumDebtSSPension is empty.");
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        private bool IsJournalSettingInvalid_SS(string Msg)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string v = string.Empty;

            SQL.AppendLine("Select B.SSName ");
            SQL.AppendLine("From TblEmpSSListDtl A ");
            SQL.AppendLine("Inner Join TblSS B On A.SSCode=B.SSCode And B.AcNo1 Is Null ");
            SQL.AppendLine("Where A.DocNo=@DocNo Limit 1; ");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@DocNo", TxtEmpSSListDocNo.Text);
            v = Sm.GetValue(cm);
            if (v.Length > 0)
            {
                Sm.StdMsg(mMsgType.Warning, Msg + "Social security " + v + "'s account# (Debt) is empty.");
                return true;
            }
            return false;
        }

        private bool IsJournalSettingInvalid_Department(string Msg)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string v = string.Empty;

            SQL.AppendLine("Select B.DeptName ");
            SQL.AppendLine("From TblEmpSSListDtl A ");
            SQL.AppendLine("Inner Join TblDepartment B On A.DeptCode=B.DeptCode And B.AcNo10 Is Null ");
            SQL.AppendLine("Where A.DocNo=@DocNo Limit 1; ");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@DocNo", TxtEmpSSListDocNo.Text);
            v = Sm.GetValue(cm);
            if (v.Length > 0)
            {
                Sm.StdMsg(mMsgType.Warning, Msg + "Department " + v + "'s account# (social security) is empty.");
                return true;
            }
            return false;
        }

        private bool IsJournalSettingInvalid_EmployeeCostGroup(string Msg)
        {
            // Tidak dipakai ? TKG
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string v = string.Empty;

            SQL.AppendLine("Select Concat(B.EmpCode, ' : ', B.EmpName) As Employee ");
            SQL.AppendLine("From TblEmpSSListDtl A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode=B.EmpCode And B.CostGroup Is Null ");
            SQL.AppendLine("Where A.DocNo=@DocNo And A.TotalAmt>0.00 Limit 1; ");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@DocNo", TxtEmpSSListDocNo.Text);
            v = Sm.GetValue(cm);
            if (v.Length > 0)
            {
                Sm.StdMsg(mMsgType.Warning, Msg + "Employee's cost group (" + v + ") is empty.");
                return true;
            }
            return false;
        }

        private bool IsAdjustmentAmountInvalid()
        {
            if (!mIsVoucherRequestSSUseAdjustment) return false;

            decimal AdjustmentAmt = 0m;

            if (TxtAdjustmentAmt.Text.Length > 0) AdjustmentAmt = Math.Abs(Decimal.Parse(TxtAdjustmentAmt.Text));

            if (AdjustmentAmt > mAllowedVoucherRequestSSAdjustmentAmt)
            {
                if (mAllowedVoucherRequestSSAdjustmentAmt == 0m)
                    Sm.StdMsg(mMsgType.Warning, "You should only insert 0 for adjustment.");
                else
                    Sm.StdMsg(mMsgType.Warning, "You should only insert adjustment amount within range " + Sm.FormatNum(mAllowedVoucherRequestSSAdjustmentAmt, 0) + " to " + Sm.FormatNum(mAllowedVoucherRequestSSAdjustmentAmt * -1, 0));

                return true;
            }

            return false;
        }

        private bool IsPaymentTypeNotValid()
        {
            if (Sm.CompareStr(Sm.GetLue(LuePaymentType), "B"))
            {
                if (Sm.IsLueEmpty(LueBankCode, "Bank")) return true;
            }

            if (Sm.CompareStr(Sm.GetLue(LuePaymentType), "G") || Sm.CompareStr(Sm.GetLue(LuePaymentType), "K"))
            {
                if (Sm.IsLueEmpty(LueBankCode, "Bank")) return true;
                if (Sm.IsTxtEmpty(TxtGiroNo, "Giro Bilyet/Cheque Number ", false)) return true;
                if (Sm.IsDteEmpty(DteDueDt, "Due Date ")) return true;
            }

            return false;
        }

        private bool IsEmpSSListAlreadyCancelled()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select DocNo From TblEmpSSListHdr " +
                    "Where CancelInd='Y' And DocNo=@DocNo;"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtEmpSSListDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Social security# : " + TxtEmpSSListDocNo.Text + Environment.NewLine + Environment.NewLine +
                    "This document already cancelled.");
                return true;
            }
            return false;
        }

        private bool IsEmpSSListAlreadyProcessed()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select DocNo From TblEmpSSListHdr " +
                    "Where ProcessInd='F' And DocNo=@DocNo;"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtEmpSSListDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Social security# : " + TxtEmpSSListDocNo.Text + Environment.NewLine + Environment.NewLine +
                    "This document already processed.");
                return true;
            }
            return false;
        }

        private MySqlCommand SaveVoucherRequestSSHdr(string DocNo, string VoucherRequestDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblVoucherRequestSSHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, Status, EmpSSListDocNo, VoucherRequestDocNo, ");
            SQL.AppendLine("AcType, PaymentType, BankAcCode, BankCode, GiroNo, DueDt, ");
            if (mIsVoucherRequestSSUseBudget)
                SQL.AppendLine("DeptCode, BCCode, ");
            SQL.AppendLine("PaymentUser, PaidToBankCode, PaidToBankBranch, PaidToBankAcName, PaidToBankAcNo,");
            SQL.AppendLine("CurCode, Amt, AdjustmentAmt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'N', 'O', @EmpSSListDocNo, @VoucherRequestDocNo, ");
            SQL.AppendLine("@AcType, @PaymentType, @BankAcCode, @BankCode, @GiroNo, @DueDt, ");
            if (mIsVoucherRequestSSUseBudget)
                SQL.AppendLine("@DeptCode, @BCCode, ");
            SQL.AppendLine("@PaymentUser, @PaidToBankCode, @PaidToBankBranch, @PaidToBankAcName, @PaidToBankAcNo,");
            SQL.AppendLine("@CurCode, @Amt, @AdjustmentAmt, ");
            SQL.AppendLine("@Remark, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@EmpSSListDocNo", TxtEmpSSListDocNo.Text);
            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo", VoucherRequestDocNo);
            Sm.CmParam<String>(ref cm, "@AcType", Sm.GetLue(LueAcType));
            Sm.CmParam<String>(ref cm, "@PaymentType", Sm.GetLue(LuePaymentType));
            Sm.CmParam<String>(ref cm, "@BankAcCode", Sm.GetLue(LueBankAcCode));
            Sm.CmParam<String>(ref cm, "@BankCode", Sm.GetLue(LueBankCode));
            Sm.CmParam<String>(ref cm, "@GiroNo", TxtGiroNo.Text);
            Sm.CmParamDt(ref cm, "@DueDt", Sm.GetDte(DteDueDt));
            if (mIsVoucherRequestSSUseBudget)
            {
                Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
                Sm.CmParam<String>(ref cm, "@BCCode", Sm.GetLue(LueBCCode));
            }
            Sm.CmParam<String>(ref cm, "@PaymentUser", TxtPaymentUser.Text);
            Sm.CmParam<String>(ref cm, "@PaidToBankCode", TxtPaidToBankCode.Text);
            Sm.CmParam<String>(ref cm, "@PaidToBankBranch", TxtPaidToBankBranch.Text);
            Sm.CmParam<String>(ref cm, "@PaidToBankAcName", TxtPaidToBankAcName.Text);
            Sm.CmParam<String>(ref cm, "@PaidToBankAcNo", TxtPaidToBankAcNo.Text);
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtAmt.Text));
            Sm.CmParam<Decimal>(ref cm, "@AdjustmentAmt", Decimal.Parse(TxtAdjustmentAmt.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveVoucherRequestSSDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblVoucherRequestSSDtl(DocNo, DNo, Description, Amt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@DocNo, @DNo, @Description, @Amt, @Remark, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", "001");
            Sm.CmParam<String>(ref cm, "@Description", Sm.GetGrdStr(Grd1, 0, 1));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd1, 0, 2));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, 0, 3));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private string GenerateVoucherRequestDocNo()
        {
            var SQL = new StringBuilder();
            var IsDocSeqNoEnabled = Sm.GetParameterBoo("IsDocSeqNoEnabled");
            var IsVoucherDocSeqNoEnabled = Sm.GetParameterBoo("IsVoucherDocSeqNoEnabled");
            string
                Yr = Sm.GetDte(DteDocDt).Substring(2, 2),
                Mth = Sm.GetDte(DteDocDt).Substring(4, 2),
                DocTitle = Sm.GetParameter("DocTitle"),
                DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='VoucherRequest'"),
                type = string.Empty,
                DocSeqNo = "4";

            if (IsDocSeqNoEnabled || IsVoucherDocSeqNoEnabled) DocSeqNo = Sm.GetParameter("DocSeqNo");

            if (Sm.GetLue(LueAcType) == "C")
                type = Sm.GetValue("Select AutoNoCredit From TblBankAccount Where BankAcCode = '" + Sm.GetLue(LueBankAcCode) + "' ");
            else
                type = Sm.GetValue("Select AutoNoDebit From TblBankAccount Where BankAcCode = '" + Sm.GetLue(LueBankAcCode) + "' ");

            if (type == string.Empty)
            {
                SQL.Append("Select Concat('" + Yr + "','/', '" + Mth + "', '/', (select ifnull ( ");
                SQL.Append("    (Select Right(Concat(Repeat('0', " + DocSeqNo + "), Convert(DocNoTemp+1, Char)), " + DocSeqNo + ") As Numb From ( ");
                SQL.Append("    Select Convert(SUBSTRING(DocNo,7," + DocSeqNo + "), Decimal) As DocNoTemp ");
                SQL.Append("    From TblVoucherRequestHdr ");
                //SQL.Append("(Select Right(Concat('0000', Convert(DocNoTemp+1, Char)), 4) As Numb From ( ");
                //SQL.Append("Select Convert(SUBSTRING(DocNo,7,4), Decimal) As DocNoTemp From TblVoucherRequestHdr ");
                SQL.Append("Where Left(DocNo, 5)= Concat('" + Yr + "','/', '" + Mth + "') ");
                SQL.Append("Order By SUBSTRING(DocNo,7,"+DocSeqNo+") Desc Limit 1) As temp ");
                SQL.Append("), Right(Concat(Repeat('0', " + DocSeqNo + "), '1'), " + DocSeqNo + ") ");
                //SQL.Append("), '0001' ");
                SQL.Append(") As Number), '/', '" + DocAbbr + "' ) As DocNo ");
            }
            else
            {
                SQL.Append("Select Concat('" + Yr + "','/', '" + Mth + "', '/', (select ifnull ( ");
                SQL.Append("    (Select Right(Concat(Repeat('0', " + DocSeqNo + "), Convert(DocNoTemp+1, Char)), " + DocSeqNo + ") As Numb From ( ");
                SQL.Append("    Select Convert(SUBSTRING(DocNo,7," + DocSeqNo + "), Decimal) As DocNoTemp ");
                SQL.Append("    From TblVoucherRequestHdr ");
                //SQL.Append("(Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) As Numb From ( ");
                //SQL.Append("Select Convert(SUBSTRING(DocNo,7,4), Decimal) As DocNo From TblVoucherRequestHdr ");
                SQL.Append("Where Left(DocNo, 5)= Concat('" + Yr + "','/', '" + Mth + "') ");
                SQL.Append("And Right(DocNo, '" + type.Length + "') = '" + type + "' ");
                SQL.Append("Order By SUBSTRING(DocNo,7," + DocSeqNo + ") Desc Limit 1) As temp ");
                SQL.Append("), Right(Concat(Repeat('0', " + DocSeqNo + "), '1'), " + DocSeqNo + ") ");
                //SQL.Append("), '0001' ");
                SQL.Append(") As Number), '/', '" + DocAbbr + "', '/', '" + type + "' ) As DocNo ");
            }
            return Sm.GetValue(SQL.ToString());
        }

        private MySqlCommand SaveVoucherRequestHdr(string VoucherRequestDocNo, string VoucherRequestSSDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblVoucherRequestHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, Status, DeptCode, ");
            SQL.AppendLine("DocType, AcType, PaymentType, BankAcCode, BankCode, GiroNo, DueDt, ");
            SQL.AppendLine("PIC, DocEnclosure, CurCode, Amt, ");
            SQL.AppendLine("PaymentUser, PaidToBankCode, PaidToBankBranch, PaidToBankAcNo, PaidToBankAcName, ");
            SQL.AppendLine("EntCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@DocNo, @DocDt, 'N', 'O', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where ParCode='VoucherRequestSSDeptCode'), ");
            SQL.AppendLine("'07', @AcType, @PaymentType, @BankAcCode, @BankCode, @GiroNo, @DueDt, ");
            SQL.AppendLine("(Select UserCode From TblUser Where UserCode=@CreateBy), ");
            SQL.AppendLine("0, @CurCode, @Amt, ");
            SQL.AppendLine("@PaymentUser, @PaidToBankCode, @PaidToBankBranch, @PaidToBankAcNo, @PaidToBankAcName, ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select D.EntCode ");
            SQL.AppendLine("    From TblVoucherRequestSSHdr A ");
            SQL.AppendLine("    Inner Join TblEmpSSListHdr B On A.EmpSSListDocNo=B.DocNo ");
            SQL.AppendLine("    Inner Join TblSite C On B.SiteCode=C.SiteCode ");
            SQL.AppendLine("    Inner Join TblProfitCenter D On C.ProfitCenterCode=D.ProfitCenterCode ");
            SQL.AppendLine("    Where A.DocNo=@VoucherRequestSSDocNo ");
            SQL.AppendLine("), ");
            SQL.AppendLine("@Remark, @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select T.DocType, @VoucherRequestSSDocNo, '001', T.DNo, @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblDocApprovalSetting T ");
            SQL.AppendLine("Where T.DocType='VoucherRequestSS' ");
            SQL.AppendLine("And (T.StartAmt=0 ");
            SQL.AppendLine("Or T.StartAmt<=IfNull(( ");
            SQL.AppendLine("    Select A.Amt*IfNull(B.Amt, 1) ");
            SQL.AppendLine("    From TblVoucherRequestHdr A ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select B1.CurCode1, B1.Amt ");
            SQL.AppendLine("        From TblCurrencyRate B1 ");
            SQL.AppendLine("        Inner Join ( ");
            SQL.AppendLine("            Select CurCode1, Max(RateDt) RateDt ");
            SQL.AppendLine("            From TblCurrencyRate ");
            SQL.AppendLine("            Where CurCode2=(Select ParValue From TblParameter Where ParCode='MainCurCode') ");
            SQL.AppendLine("            Group By CurCode1 ");
            SQL.AppendLine("        ) B2 On B1.CurCode1=B2.CurCode1 And B1.RateDt=B2.RateDt ");
            SQL.AppendLine("    ) B On A.CurCode=B.CurCode1 ");
            SQL.AppendLine("    Where A.DocNo=@DocNo ");
            SQL.AppendLine("), 0)); ");

            SQL.AppendLine("Update TblVoucherRequestHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select DocNo From TblDocApproval ");
            SQL.AppendLine("    Where DocType='VoucherRequestSS' ");
            SQL.AppendLine("    And DocNo=@VoucherRequestSSDocNo ");
            SQL.AppendLine("    ); ");

            SQL.AppendLine("Update TblVoucherRequestSSHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@VoucherRequestSSDocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select DocNo From TblDocApproval ");
            SQL.AppendLine("    Where DocType='VoucherRequestSS' ");
            SQL.AppendLine("    And DocNo=@VoucherRequestSSDocNo ");
            SQL.AppendLine("    ); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", VoucherRequestDocNo);
            Sm.CmParam<String>(ref cm, "@VoucherRequestSSDocNo", VoucherRequestSSDocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@PaymentType", Sm.GetLue(LuePaymentType));
            Sm.CmParam<String>(ref cm, "@AcType", Sm.GetLue(LueAcType));
            Sm.CmParam<String>(ref cm, "@BankAcCode", Sm.GetLue(LueBankAcCode));
            Sm.CmParam<String>(ref cm, "@BankCode", Sm.GetLue(LueBankCode));
            Sm.CmParam<String>(ref cm, "@GiroNo", TxtGiroNo.Text);
            Sm.CmParamDt(ref cm, "@DueDt", Sm.GetDte(DteDueDt));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<Decimal>(ref cm, "@Amt", decimal.Parse(TxtAmt.Text));
            Sm.CmParam<String>(ref cm, "@PaymentUser", TxtPaymentUser.Text);
            Sm.CmParam<String>(ref cm, "@PaidToBankCode", TxtPaidToBankCode.Text);
            Sm.CmParam<String>(ref cm, "@PaidToBankBranch", TxtPaidToBankBranch.Text);
            Sm.CmParam<String>(ref cm, "@PaidToBankAcNo", TxtPaidToBankAcNo.Text);
            Sm.CmParam<String>(ref cm, "@PaidToBankAcName", TxtPaidToBankAcName.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveVoucherRequestDtl(string DocNo)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblVoucherRequestDtl(DocNo, DNo, Description, Amt, Remark, CreateBy, CreateDt) " +
                    "Values (@DocNo, @DNo, @Description, @Amt, @Remark, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", "001");
            Sm.CmParam<String>(ref cm, "@Description", Sm.GetGrdStr(Grd1, 0, 1));
            if (mIsVoucherRequestSSUseAdjustment)
                Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtAmt.Text));
            else
                Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd1, 0, 2));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, 0, 3));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand UpdateEmpSSListHdr()
        {
            var cm = new MySqlCommand() 
            { 
                CommandText = 
                    "Update TblEmpSSListHdr Set ProcessInd=@ProcessInd " +
                    "Where DocNo=@DocNo;"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtEmpSSListDocNo.Text);
            Sm.CmParam<String>(ref cm, "@ProcessInd", ChkCancelInd.Checked?"O":"F");
            return cm;
        }

        private MySqlCommand SaveJournal(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            var SSPCode = Sm.GetValue("Select SSPCode From TblEmpSSListHdr Where DocNo=@Param;", TxtEmpSSListDocNo.Text);

            //if (
            //    SSPCode.Length == 0 ||
            //    (
            //    !Sm.CompareStr(SSPCode, mSSPCodeForHealth) &&
            //    !Sm.CompareStr(SSPCode, mSSPCodeForEmployment) && 
            //    !Sm.CompareStr(SSPCode, mSSPCodeForPension)
            //    )
            //    )
            //    return cm;

            SQL.AppendLine("Update TblVoucherRequestSSHdr Set ");
            SQL.AppendLine("    JournalDocNo=@JournalDocNo ");
            SQL.AppendLine("Where DocNo=@DocNo;");

            SQL.AppendLine("Insert Into TblJournalHdr ");
            SQL.AppendLine("(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select JournalDocNo, ");
            SQL.AppendLine("DocDt, ");
            SQL.AppendLine("Concat('Voucher Request For Social Security '");
            if (Sm.CompareStr(SSPCode, mSSPCodeForHealth))
                SQL.AppendLine(",'Health'");
            if (Sm.CompareStr(SSPCode, mSSPCodeForEmployment))
                SQL.AppendLine(",'Employment'");
            if (Sm.Find_In_Set(SSPCode, mSSPCodeForPension))
                SQL.AppendLine(",'Pension'");
            SQL.AppendLine(", ' : ', DocNo) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, ");
            SQL.AppendLine("(Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, Remark, ");
            SQL.AppendLine("CreateBy, CreateDt ");
            SQL.AppendLine("From TblVoucherRequestSSHdr Where DocNo=@DocNo;");

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, F.EntCode, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("        Select @AcNoForAllowanceSS As AcNo, ");
            SQL.AppendLine("        @Amt As DAmt, 0 As CAmt ");
            if (mAcNoForPremiumDebtSSHealth.Length > 0 || 
                mAcNoForAllowanceSSEmployment.Length>0 ||
                mAcNoForAllowanceSSPension.Length>0)
            {
                SQL.AppendLine("        Union All ");
                SQL.AppendLine("        Select @AcNoForPremiumDebtSS As AcNo, ");
                SQL.AppendLine("        0.00 As DAmt, @Amt As CAmt ");
            }
            SQL.AppendLine(") B On 1=1 ");
            SQL.AppendLine("Left Join TblVoucherRequestSSHdr C On C.DocNo=@DocNo ");
            SQL.AppendLine("Left Join TblEmpSSListHdr D On C.EmpSSListDocNo=D.DocNo ");
            SQL.AppendLine("Left Join TblSite E On D.SiteCode=E.SiteCode ");
            SQL.AppendLine("Left Join TblProfitCenter F On E.ProfitCenterCode=F.ProfitCenterCode ");
            SQL.AppendLine("Where A.DocNo=@JournalDocNo;");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(Sm.GetDte(DteDocDt), 1));
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            if (Sm.CompareStr(SSPCode, mSSPCodeForHealth))
            {
                Sm.CmParam<String>(ref cm, "@AcNoForAllowanceSS", mAcNoForAllowanceSSHealth);
                Sm.CmParam<String>(ref cm, "@AcNoForPremiumDebtSS", mAcNoForPremiumDebtSSHealth);
            }
            if (Sm.CompareStr(SSPCode, mSSPCodeForEmployment))
            {
                Sm.CmParam<String>(ref cm, "@AcNoForAllowanceSS", mAcNoForAllowanceSSEmployment);
                Sm.CmParam<String>(ref cm, "@AcNoForPremiumDebtSS", mAcNoForPremiumDebtSSEmployment);
            }
            if (Sm.Find_In_Set(SSPCode, mSSPCodeForPension))
            {
                Sm.CmParam<String>(ref cm, "@AcNoForAllowanceSS", mAcNoForAllowanceSSPension);
                Sm.CmParam<String>(ref cm, "@AcNoForPremiumDebtSS", mAcNoForPremiumDebtSSPension);
            }
            Sm.CmParam<Decimal>(ref cm, "@Amt", decimal.Parse(TxtAmt.Text));
            return cm;
        }

        private MySqlCommand SaveJournal2(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            
            SQL.AppendLine("Update TblVoucherRequestSSHdr Set ");
            SQL.AppendLine("    JournalDocNo=@JournalDocNo ");
            SQL.AppendLine("Where DocNo=@DocNo;");

            SQL.AppendLine("Insert Into TblJournalHdr ");
            SQL.AppendLine("(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.JournalDocNo, A.DocDt, ");
            SQL.AppendLine("Concat('Voucher Request For Social Security ', C.SSPName, ' : ', A.DocNo) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, ");
            SQL.AppendLine("(Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, A.Remark, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblVoucherRequestSSHdr A ");
            SQL.AppendLine("Inner Join TblEmpSSListHdr B On A.EmpSSListDocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblSSProgram C On B.SSPCode=C.SSPCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, F.EntCode, B.Remark, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");

            SQL.AppendLine("    Select AcNo, ");
            SQL.AppendLine("    Sum(DAmt) As DAmt, Sum(CAmt) As CAmt ");
            SQL.AppendLine("    From ( ");

            //SS Downpayment
            SQL.AppendLine("    Select T3.AcNo2 As AcNo, T2.EmployeeAmt As DAmt, 0.00 As CAmt ");
            SQL.AppendLine("    From TblVoucherRequestSSHdr T1 ");
            SQL.AppendLine("    Inner Join TblEmpSSListDtl T2 On T1.EmpSSListDocNo=T2.DocNo ");
            SQL.AppendLine("    Inner Join TblSS T3 On T2.SSCode=T3.SSCode And T3.AcNo2 Is Not Null ");
            SQL.AppendLine("    Where T1.DocNo=@DocNo ");

            //Department's SS
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select T3.AcNo10 As AcNo, T2.EmployerAmt As DAmt, 0.00 As CAmt ");
            SQL.AppendLine("    From TblVoucherRequestSSHdr T1 ");
            SQL.AppendLine("    Inner Join TblEmpSSListDtl T2 On T1.EmpSSListDocNo=T2.DocNo ");
            SQL.AppendLine("    Inner Join TblDepartment T3 On T2.DeptCode=T3.DeptCode And T3.AcNo10 Is Not Null ");
            SQL.AppendLine("    Where T1.DocNo=@DocNo ");

            //SS Debt
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select T3.AcNo1 As AcNo, 0.00 As DAmt, T2.EmployerAmt+T2.EmployeeAmt As CAmt ");
            SQL.AppendLine("    From TblVoucherRequestSSHdr T1 ");
            SQL.AppendLine("    Inner Join TblEmpSSListDtl T2 On T1.EmpSSListDocNo=T2.DocNo ");
            SQL.AppendLine("    Inner Join TblSS T3 On T2.SSCode=T3.SSCode And T3.AcNo1 Is Not Null ");
            SQL.AppendLine("    Where T1.DocNo=@DocNo ");

            SQL.AppendLine("    ) T ");
            SQL.AppendLine("    Group By AcNo ");

            SQL.AppendLine(") B On 1=1 ");
            SQL.AppendLine("Left Join TblVoucherRequestSSHdr C On C.DocNo=@DocNo ");
            SQL.AppendLine("Left Join TblEmpSSListHdr D On C.EmpSSListDocNo=D.DocNo ");
            SQL.AppendLine("Left Join TblSite E On D.SiteCode=E.SiteCode ");
            SQL.AppendLine("Left Join TblProfitCenter F On E.ProfitCenterCode=F.ProfitCenterCode ");
            SQL.AppendLine("Where A.DocNo=@JournalDocNo;");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(Sm.GetDte(DteDocDt), 1));
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
           
            return cm;
        }

        private MySqlCommand SaveJournalHdr(JournalHdr x, string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblVoucherRequestSSJournal(VRSSDocNo, JournalDocNo, JournalDocNo2, CCCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @JournalDocNo, Null, @CCCode, @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, DocDt, Concat('Voucher Request Social Security ', @SSPName, ' : ', DocNo), ");
            SQL.AppendLine("@MenuCode As MenuCode, (Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, @CCCode, ");
            SQL.AppendLine("Remark, CreateBy, CreateDt ");
            SQL.AppendLine("From TblVoucherRequestSSHdr Where DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@SSPName", x.SSPName);
            Sm.CmParam<String>(ref cm, "@JournalDocNo", x.DocNo);
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@CCCode", x.CCCode);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournalDtl(JournalDtl x, string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@JournalDocNo, @DNo, ");
            SQL.AppendLine("@AcNo, @DAmt, @CAmt, @EntCode, @Remark, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@JournalDocNo", x.DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", x.DNo);
            Sm.CmParam<String>(ref cm, "@AcNo", x.AcNo);
            Sm.CmParam<Decimal>(ref cm, "@DAmt", x.DAmt);
            Sm.CmParam<Decimal>(ref cm, "@CAmt", x.CAmt);
            Sm.CmParam<String>(ref cm, "@EntCode", x.EntCode);
            if (mIsCOACouldBeChosenMoreThanOnce)
                Sm.CmParam<String>(ref cm, "@Remark", x.Remark);
            else
                Sm.CmParam<String>(ref cm, "@Remark", string.Empty);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(UpdateVoucherRequestSSHdr());
            cml.Add(UpdateEmpSSListHdr());
            if (mIsAutoJournalActived && IsJournalDataExisted() && !mIsVoucherRequestSSJournalDisabled)
            {
                if (mIsVoucherRequestSSJournalBasedOnCostCenter)
                {
                    cml.Add(SaveJournal2());
                    //var l = new List<JournalHdrCancel>();
                    //string JournalDocNo = string.Empty;
                    //var CurrentDt = Sm.ServerCurrentDate();
                    //var DocDt = Sm.GetDte(DteDocDt);
                    //var IsClosingJournalUseCurrentDt = Sm.IsClosingJournalUseCurrentDt(DocDt);
                    //PrepDataCancel(ref l);
                    //if (l.Count > 0)
                    //{
                    //    int index = 1;

                    //    foreach (var x in l)
                    //    {
                    //        if (IsClosingJournalUseCurrentDt)
                    //            JournalDocNo = Sm.GenerateDocNoJournal(CurrentDt, index);
                    //        else
                    //            JournalDocNo = Sm.GenerateDocNoJournal(DocDt, index);

                    //        cml.Add(SaveJournal2(x, JournalDocNo, IsClosingJournalUseCurrentDt));

                    //        index += 1;
                    //    }
                    //}
                }
                else
                    cml.Add(SaveJournal());
            }

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                mIsVoucherRequestSSTransactionValidatedbyClosingJournal ?
                Sm.IsClosingJournalInvalid(mIsAutoJournalActived, true, Sm.GetDte(DteDocDt), GetProfitCenterCode()) :
                //(!mIsVoucherRequestSSJournalDisabled && Sm.IsClosingJournalInvalid(mIsAutoJournalActived, true, Sm.GetDte(DteDocDt), GetProfitCenterCode())) ||
                (mIsVoucherRequestSSTransactionValidatedbyClosingJournal && Sm.IsClosingJournalInvalid(mIsAutoJournalActived, true, Sm.GetDte(DteDocDt), GetProfitCenterCode())) ||
                IsDataNotCancelled() ||
                IsDataCancelledAlready() ||
                IsDataProcessedAlready();
        }

        private bool IsDataNotCancelled()
        {
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this data.");
                return true;
            }
            return false;
        }

        private bool IsDataCancelledAlready()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select DocNo From TblVoucherRequestSSHdr " +
                    "Where (CancelInd='Y' Or Status='C') And DocNo=@DocNo "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data already cancelled.");
                return true;
            }
            return false;
        }

        private bool IsDataProcessedAlready()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo ");
            SQL.AppendLine("From TblVoucherRequestHdr A, TblVoucherHdr B ");
            SQL.AppendLine("Where A.VoucherDocNo=B.DocNo And B.CancelInd='N' ");
            SQL.AppendLine("And A.DocNo=@DocNo ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtVoucherRequestDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "Data already processed into voucher.");
                return true;
            }

            return false;
        }

        private bool IsJournalDataExisted()
        {
            var SQL = new StringBuilder();

            if (mIsVoucherRequestSSJournalBasedOnCostCenter)
            {
                SQL.AppendLine("Select VRSSDocNo ");
                SQL.AppendLine("From TblVoucherRequestSSJournal ");
                SQL.AppendLine("Where JournalDocNo Is Not Null  ");
                SQL.AppendLine("And VRSSDocNo=@DocNo ");
                SQL.AppendLine("Limit 1; ");
            }
            else
            {
                SQL.AppendLine("Select DocNo  ");
                SQL.AppendLine("From TblVoucherRequestSSHdr ");
                SQL.AppendLine("Where JournalDocNo Is Not Null  ");
                SQL.AppendLine("And DocNo=@DocNo ");
                SQL.AppendLine("Limit 1; ");
            }

            var cm = new MySqlCommand()
            {
                CommandText = SQL.ToString()
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            return Sm.IsDataExist(cm);
        }

        private MySqlCommand UpdateVoucherRequestSSHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblVoucherRequestSSHdr Set ");
            SQL.AppendLine("    CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N' And Status<>'C'; ");

            SQL.AppendLine("Update TblVoucherRequestHdr Set ");
            SQL.AppendLine("    CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@VoucherRequestDocNo And CancelInd='N' And Status<>'C'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo", TxtVoucherRequestDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        ////private void PrepDataCancel(ref List<JournalHdrCancel> l)
        ////{
        ////    var SQL = new StringBuilder();
        ////    var cm = new MySqlCommand();

        ////    SQL.AppendLine("Select VRSSDocNo, JournalDocNo ");
        ////    SQL.AppendLine("From TblVoucherRequestSSJournal ");
        ////    SQL.AppendLine("Where VRSSDocNo = @DocNo ");
        ////    SQL.AppendLine("; ");

        ////    using (var cn = new MySqlConnection(Gv.ConnectionString))
        ////    {
        ////        cn.Open();
        ////        cm.Connection = cn;
        ////        cm.CommandText = SQL.ToString();
        ////        Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

        ////        var dr = cm.ExecuteReader();
        ////        var c = Sm.GetOrdinal(dr, new string[] { "VRSSDocNo", "JournalDocNo" });
        ////        if (dr.HasRows)
        ////        {
        ////            while (dr.Read())
        ////            {
        ////                l.Add(new JournalHdrCancel()
        ////                {
        ////                    DocNo = Sm.DrStr(dr, c[0]),
        ////                    JournalDocNo = Sm.DrStr(dr, c[1]),
        ////                    JournalDocNo2 = string.Empty
        ////                });
        ////            }
        ////        }
        ////        dr.Close();
        ////    }
        ////}

        ////private MySqlCommand SaveJournal2(JournalHdrCancel x, string JournalDocNo, bool IsClosingJournalUseCurrentDt)
        ////{
        ////    var SQL = new StringBuilder();

        ////    SQL.AppendLine("Update TblVoucherRequestSSJournal Set JournalDocNo2=@JournalDocNo2 ");
        ////    SQL.AppendLine("Where VRSSDocNo=@DocNo And JournalDocNo=@JournalDocNo; ");

        ////    SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
        ////    SQL.AppendLine("Select @JournalDocNo2, ");
        ////    if (IsClosingJournalUseCurrentDt)
        ////        SQL.AppendLine("Replace(CurDate(), '-', ''), ");
        ////    else
        ////        SQL.AppendLine("DocDt, ");
        ////    SQL.AppendLine("Concat('Cancelling ', JnDesc) As JnDesc, ");
        ////    SQL.AppendLine("MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt ");
        ////    SQL.AppendLine("From TblJournalHdr ");
        ////    SQL.AppendLine("Where DocNo In (Select JournalDocNo From TblVoucherRequestSSJournal Where VRSSDocNo=@DocNo And JournalDocNo = @JournalDocNo); ");

        ////    SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, Remark, CreateBy, CreateDt) ");
        ////    SQL.AppendLine("Select @JournalDocNo2, DNo, AcNo, CAMt As DAmt, DAmt As CAmt, EntCode, Remark, CreateBy, CreateDt ");
        ////    SQL.AppendLine("From TblJournalDtl ");
        ////    SQL.AppendLine("Where DocNo In (Select JournalDocNo From TblVoucherRequestSSJournal Where VRSSDocNo=@DocNo And JournalDocNo = @JournalDocNo); ");

        ////    var cm = new MySqlCommand() { CommandText = SQL.ToString() };

        ////    Sm.CmParam<String>(ref cm, "@DocNo", x.DocNo);
        ////    Sm.CmParam<String>(ref cm, "@JournalDocNo", x.JournalDocNo);
        ////    Sm.CmParam<String>(ref cm, "@JournalDocNo2", JournalDocNo);

        ////    return cm;
        ////}

        private MySqlCommand SaveJournal()
        {
            var SQL = new StringBuilder();
            var CurrentDt = Sm.ServerCurrentDate();
            var DocDt = Sm.GetDte(DteDocDt);
            var IsClosingJournalUseCurrentDt = Sm.IsClosingJournalUseCurrentDt(DocDt);

            SQL.AppendLine("Update TblVoucherRequestSSHdr Set JournalDocNo2=@JournalDocNo ");
            SQL.AppendLine("Where DocNo=@DocNo And JournalDocNo Is Not Null; ");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, ");
            if (IsClosingJournalUseCurrentDt)
                SQL.AppendLine("Replace(CurDate(), '-', ''), ");
            else
                SQL.AppendLine("DocDt, ");
            SQL.AppendLine("Concat('Cancelling ', JnDesc) As JnDesc, ");
            SQL.AppendLine("MenuCode, MenuDesc, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblJournalHdr ");
            SQL.AppendLine("Where DocNo In ( ");
            SQL.AppendLine("    Select JournalDocNo From TblVoucherRequestSSHdr ");
            SQL.AppendLine("    Where DocNo=@DocNo And JournalDocNo Is Not Null ");
            SQL.AppendLine("    ); ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, DNo, AcNo, CAMt As DAmt, DAmt As CAmt, EntCode, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblJournalDtl Where DocNo In (Select JournalDocNo From TblVoucherRequestSSHdr Where DocNo=@DocNo);");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (IsClosingJournalUseCurrentDt)
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(CurrentDt, 1));
            else
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(DocDt, 1));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal2()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            var DocDt = Sm.GetDte(DteDocDt);
            var IsClosingJournalUseCurrentDt = Sm.IsClosingJournalUseCurrentDt(DocDt);
            string
                DocTitle = Sm.GetParameter("DocTitle"),
                DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='Journal'"),
                JournalDocSeqNo = Sm.GetParameter("JournalDocSeqNo");

            if (JournalDocSeqNo.Length == 0) JournalDocSeqNo = "4";

            SQL.AppendLine("Set @Dt:=CurrentDateTime(); ");
            if (IsClosingJournalUseCurrentDt)
                SQL.AppendLine("Set @MthYr:=Concat(Substring(@Dt, 5, 2), '/', Substring(@Dt, 3, 2)); ");
            else
                SQL.AppendLine("Set @MthYr:=Concat(Substring(@DocDt, 5, 2), '/', Substring(@DocDt, 3, 2)); ");

            SQL.AppendLine("Update TblVoucherRequestSSJournal X1 ");
            SQL.AppendLine("Inner Join (");
            SQL.AppendLine("    Select DNo, CCCode ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("        Select CCCode, ");
            SQL.AppendLine("        (Case DocNo When @curType Then @curRow := @curRow+1 Else @curRow := 1 And @curType := DocNo End) As DNo ");
            SQL.AppendLine("        From ( ");
            SQL.AppendLine("            Select Distinct A.DocNo, D.CCCode ");
            SQL.AppendLine("            From TblVoucherRequestSSHdr A ");
            SQL.AppendLine("            Inner Join TblEmpSSListDtl B On A.EmpSSListDocNo=B.DocNo ");
            SQL.AppendLine("            Inner Join TblDepartment C On B.DeptCode=C.DeptCode ");
            SQL.AppendLine("            Inner Join TblCostCenter D On C.DeptCode=D.DeptCode And D.DeptCode Is Not Null And D.ActInd='Y' ");
            SQL.AppendLine("            Where A.DocNo=@DocNo ");
            SQL.AppendLine("        ) T1, (Select @curRow := 0, @curType := '') T2 ");
            SQL.AppendLine("    ) T ");
            SQL.AppendLine(") X2 On X1.CCCode=X2.CCCode ");
            SQL.AppendLine("Set X1.JournalDocNo2=X2.DNo, x1.seqno=x2.dno ");
            SQL.AppendLine("Where X1.VRSSDocNo=@DocNo; ");

            SQL.AppendLine("Set @LastNo:=(Select IfNull((");
            SQL.AppendLine("    Select Convert(Left(DocNo, Locate('/', DocNo)-1), Decimal) As DocNo ");
            SQL.AppendLine("    From TblJournalHdr ");
            SQL.AppendLine("    Where Right(DocNo, 5)=@MthYr ");
            SQL.AppendLine("    Order By DocNo Desc Limit 1 ");
            SQL.AppendLine("    ), 0));");

            SQL.AppendLine("Update TblVoucherRequestSSJournal Set ");
            SQL.AppendLine("    JournalDocNo2=Concat( ");
            SQL.AppendLine("        IfNull( ");
            SQL.AppendLine("        ( ");
            SQL.AppendLine("        Right(Concat(Repeat('0', @JournalDocSeqNo), Convert(Cast((@LastNo+Convert(JournalDocNo2, Decimal)) As Decimal(10, 0)), Char(8))), @JournalDocSeqNo) ");
            SQL.AppendLine("        ), ");
            SQL.AppendLine("        Right(Concat(Repeat('0', @JournalDocSeqNo), Convert(JournalDocNo2, Decimal)), @JournalDocSeqNo)),");
            SQL.AppendLine("        '/', @DocTitle, '/', @DocAbbr, '/', @MthYr ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine("Where VRSSDocNo=@DocNo; ");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.JournalDocNo2, ");
            if (IsClosingJournalUseCurrentDt)
                SQL.AppendLine("Replace(CurDate(), '-', ''), ");
            else
                SQL.AppendLine("B.DocDt, ");
            SQL.AppendLine("Concat('Cancelling ', B.JnDesc) As JnDesc, ");
            SQL.AppendLine("B.MenuCode, B.MenuDesc, B.CCCode, B.Remark, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblVoucherRequestSSJournal A, TblJournalHdr B ");
            SQL.AppendLine("Where A.VRSSDocNo=@DocNo And A.JournalDocNo=B.DocNo; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.JournalDocNo2, B.DNo, B.AcNo, B.CAMt As DAmt, B.DAmt As CAmt, B.EntCode, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblVoucherRequestSSJournal A, TblJournalDtl B  ");
            SQL.AppendLine("Where A.VRSSDocNo=@DocNo And A.JournalDocNo=B.DocNo;");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParamDt(ref cm, "@DocDt", DocDt);
            Sm.CmParam<String>(ref cm, "@DocTitle", DocTitle);
            Sm.CmParam<String>(ref cm, "@DocAbbr", DocAbbr);
            Sm.CmParam<decimal>(ref cm, "@JournalDocSeqNo", decimal.Parse(JournalDocSeqNo));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowVoucherRequestSSHdr(DocNo);
                ShowVoucherRequestSSDtl(DocNo);
                ShowDocApproval(DocNo);
                if (mIsVoucherRequestSSUseBudget) ComputeRemainingBudget();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowVoucherRequestSSHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelInd, ");
            SQL.AppendLine("Case A.Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' Else 'Outstanding' End As StatusDesc, ");
            SQL.AppendLine("A.AcType, A.EmpSSListDocNo, B.Yr, B.Mth, C.SSPName, B.Employer, B.Employee, B.Total, ");
            SQL.AppendLine("A.VoucherRequestDocNo, D.VoucherDocNo, ");
            SQL.AppendLine("A.PaymentType, A.BankAcCode, A.BankCode, A.GiroNo, A.DueDt, ");
            if (mIsVoucherRequestSSUseBudget)
                SQL.AppendLine("A.DeptCode, A.BCCode, ");
            else
                SQL.AppendLine("Null As DeptCode, Null As BCCode, ");
            SQL.AppendLine("A.PaymentUser, A.PaidToBankCode, A.PaidToBankBranch, A.PaidToBankAcNo, A.PaidToBankAcName, ");
            SQL.AppendLine("A.CurCode, A.Amt, A.AdjustmentAmt, A.Remark ");
            SQL.AppendLine("From TblVoucherRequestSSHdr A ");
            SQL.AppendLine("Inner Join TblEmpSSListHdr B On A.EmpSSListDocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblSSProgram C On B.SSPCode=C.SSPCode ");
            SQL.AppendLine("Left Join TblVoucherRequestHdr D On A.VoucherRequestDocNo=D.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInCtrl(
                 ref cm, SQL.ToString(),
                 new string[] 
                {
                    //0
                    "DocNo",
                    
                    //1-5
                    "DocDt", "CancelInd", "StatusDesc", "AcType", "EmpSSListDocNo", 
                    
                    //6-10
                    "Yr", "Mth", "SSPName", "Employer", "Employee", 
                    
                    //11-15
                    "Total", "VoucherRequestDocNo", "VoucherDocNo", "PaymentType", "BankAcCode", 
                    
                    //16-20
                    "BankCode", "GiroNo", "DueDt", "PaymentUser", "PaidToBankCode", 
                    
                    //21-25
                    "PaidToBankBranch", "PaidToBankAcNo", "PaidToBankAcName", "CurCode", "Amt", 
                    
                    //26-29
                    "Remark", "AdjustmentAmt", "DeptCode", "BCCode"
                },
                 (MySqlDataReader dr, int[] c) =>
                 {
                     TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                     Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                     ChkCancelInd.Checked = Sm.DrStr(dr, c[2])=="Y";
                     TxtStatus.EditValue = Sm.DrStr(dr, c[3]);
                     Sm.SetLue(LueAcType, Sm.DrStr(dr, c[4]));
                     TxtEmpSSListDocNo.EditValue = Sm.DrStr(dr, c[5]);
                     TxtYr.EditValue = Sm.DrStr(dr, c[6]);
                     TxtMth.EditValue = Sm.DrStr(dr, c[7]);
                     TxtSSPCode.EditValue = Sm.DrStr(dr, c[8]);
                     TxtEmployer.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[9]), 0);
                     TxtEmployee.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[10]), 0);
                     TxtTotal.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[11]), 0);
                     TxtVoucherRequestDocNo.EditValue = Sm.DrStr(dr, c[12]);
                     TxtVoucherDocNo.EditValue = Sm.DrStr(dr, c[13]);
                     Sm.SetLue(LuePaymentType, Sm.DrStr(dr, c[14]));
                     Sm.SetLue(LueBankAcCode, Sm.DrStr(dr, c[15]));
                     Sm.SetLue(LueBankCode, Sm.DrStr(dr, c[16]));
                     TxtGiroNo.EditValue = Sm.DrStr(dr, c[17]);
                     Sm.SetDte(DteDueDt, Sm.DrStr(dr, c[18]));
                     TxtPaymentUser.EditValue = Sm.DrStr(dr, c[19]);
                     TxtPaidToBankCode.EditValue = Sm.DrStr(dr, c[20]);
                     TxtPaidToBankBranch.EditValue = Sm.DrStr(dr, c[21]);
                     TxtPaidToBankAcNo.EditValue = Sm.DrStr(dr, c[22]);
                     TxtPaidToBankAcName.EditValue = Sm.DrStr(dr, c[23]);
                     Sm.SetLue(LueCurCode, Sm.DrStr(dr, c[24]));
                     TxtAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[25]), 0);
                     MeeRemark.EditValue = Sm.DrStr(dr, c[26]);
                     TxtAdjustmentAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[27]), 0);
                     if (mIsVoucherRequestSSUseBudget)
                     {
                         Sm.SetLue(LueDeptCode, Sm.DrStr(dr, c[28]));
                         SetLueBCCode(ref LueBCCode, Sm.DrStr(dr, c[29]), string.Empty);
                         Sm.SetLue(LueBCCode, Sm.DrStr(dr, c[29]));
                     }
                 }, true
             );
        }

        private void ShowVoucherRequestSSDtl(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    "Select DNo, Description, Amt, Remark From TblVoucherRequestSSDtl " +
                    "Where DocNo=@DocNo Order By DNo;",
                    new string[] 
                    { "DNo", "Description", "Amt", "Remark" },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 2 });
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ShowDocApproval(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.UserName, ");
            SQL.AppendLine("Case Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' Else 'Outstanding' End As StatusDesc, ");
            SQL.AppendLine("Case When A.LastUpDt Is Not Null Then A.LastUpDt Else Null End As LastUpDt, A.Remark ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Left Join TblUser B On A.UserCode = B.UserCode ");
            SQL.AppendLine("Where A.DocType='VoucherRequestSS' ");
            SQL.AppendLine("And A.DocNo=@DocNo ");
            SQL.AppendLine("And A.Status In ('A', 'C') "); 
            SQL.AppendLine("Order By A.ApprovalDNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                    ref Grd2, ref cm, SQL.ToString(),
                    new string[] 
                    { "UserName", "StatusDesc", "LastUpDt", "Remark" },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row+1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    }, false, false, true, false
            );
            Sm.FocusGrd(Grd2, 0, 0);
        }

        #endregion

        #region Additional Method

        #region generate journal request

        private void PrepDetailData(ref List<DtlDept> l4)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            GenerateSQL(ref SQL);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", TxtEmpSSListDocNo.Text);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    "DeptCode", 
                    "CCCode", "EmployerAmt", "EmployeeAmt", "TotalAmt", "AcNo10", 
                    "AcNo1", "AcNo2", "EntCode" 
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l4.Add(new DtlDept()
                        {
                            DeptCode = Sm.DrStr(dr, c[0]),
                            CCCode = Sm.DrStr(dr, c[1]),
                            EmployerAmt = Sm.DrDec(dr, c[2]),
                            EmployeeAmt = Sm.DrDec(dr, c[3]),
                            TotalAmt = Sm.DrDec(dr, c[4]),
                            AcNo10 = Sm.DrStr(dr, c[5]),
                            AcNo1 = Sm.DrStr(dr, c[6]),
                            AcNo2 = Sm.DrStr(dr, c[7]),
                            EntCode = Sm.DrStr(dr, c[8]),
                        });
                    }
                }
                dr.Close();
            }
        }

        private void GenerateSQL(ref StringBuilder SQL)
        {
            //SQL.AppendLine("Select B.DeptCode, C.CCCode, A.EmployerAmt, A.EmployeeAmt, A.TotalAmt, ");
            //SQL.AppendLine("B.AcNo10, D.AcNo1, D.AcNo2, G.EntCode ");
            //SQL.AppendLine("From TblEmpSSListDtl A ");
            //SQL.AppendLine("Inner Join TblDepartment B On A.DeptCode = B.DeptCode ");
            //SQL.AppendLine("Inner Join TblCostCenter C On A.DeptCode = C.DeptCode ");
            //SQL.AppendLine("Inner Join TblSS D On A.SSCode = D.SSCode ");
            //SQL.AppendLine("Inner Join TblEmpSSListHdr E On A.DocNo = E.DocNo ");
            //SQL.AppendLine("Left Join TblSite F On E.SiteCode = F.SiteCode ");
            //SQL.AppendLine("Left Join TblProfitCenter G On F.ProfitCenterCode = G.ProfitCenterCode ");
            //SQL.AppendLine("Where A.DocNo = @DocNo ");
            //SQL.AppendLine("; ");

            SQL.AppendLine("Select Null As DeptCode, C.CCCode, A.EmployerAmt, A.EmployeeAmt, A.TotalAmt, ");
            SQL.AppendLine("I.Property1 As AcNo10, D.AcNo1, I.Property1 As AcNo2, G.EntCode ");
            SQL.AppendLine("From TblEmpSSListDtl A ");
            SQL.AppendLine("Inner Join TblDepartment B On A.DeptCode = B.DeptCode ");
            SQL.AppendLine("Inner Join TblCostCenter C On B.DeptCode = C.DeptCode And C.DeptCode Is Not Null And C.ActInd='Y' ");
            SQL.AppendLine("Inner Join TblSS D On A.SSCode = D.SSCode ");
            SQL.AppendLine("Inner Join TblEmpSSListHdr E On A.DocNo = E.DocNo ");
            SQL.AppendLine("Left Join TblSite F On E.SiteCode = F.SiteCode ");
            SQL.AppendLine("Left Join TblProfitCenter G On F.ProfitCenterCode = G.ProfitCenterCode ");
            SQL.AppendLine("Inner Join TblEmployee H On A.EmpCode=H.EmpCode ");
            SQL.AppendLine("Left Join TblOption I On I.OptCat='EmpCostGroup' And H.CostGroup=I.OptCode And I.Property1 Is Not Null ");
            SQL.AppendLine("Where A.DocNo=@DocNo And A.TotalAmt>0.00; ");
        }        

        private void SumByCostCenter(ref List<DtlDept> l4, ref List<DtlCostCenter> l3)
        {
            l3 = l4.GroupBy(g => new { g.CCCode, g.DeptCode })
                        .Select
                        (
                            s => new DtlCostCenter()
                            {
                                CCCode = s.Key.CCCode,
                                DeptCode = s.Key.DeptCode
                                //AcNo1 = s.Key.AcNo1,
                                //AcNo10 = s.Key.AcNo10,
                                //AcNo2 = s.Key.AcNo2,
                                //EntCode = s.Key.EntCode,
                                //EmployeeAmt = s.Sum(p => p.EmployeeAmt),
                                //EmployerAmt = s.Sum(p => p.EmployerAmt),
                                //TotalAmt = s.Sum(p => p.TotalAmt)
                            }
                        ).ToList();
        }

        private void GetJournalHdr(ref List<DtlCostCenter> l3, ref List<JournalHdr> l)
        {
            string SSPName = string.Empty;

            if (mIsVoucherRequestSSUseSSCOASetting)
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select C.SSPName ");
                SQL.AppendLine("From TblEmpSSListHdr B ");
                SQL.AppendLine("Inner Join TblSSProgram C On B.SSPCode=C.SSPCode ");
                SQL.AppendLine("Where B.DocNo = @Param; ");

                SSPName = Sm.GetValue(SQL.ToString(), TxtEmpSSListDocNo.Text);
            }
            else
            {
                var SSPCode = Sm.GetValue("Select SSPCode From TblEmpSSListHdr Where DocNo=@Param;", TxtEmpSSListDocNo.Text);

                if (Sm.CompareStr(SSPCode, mSSPCodeForHealth))
                    SSPName = "Health";
                if (Sm.CompareStr(SSPCode, mSSPCodeForEmployment))
                    SSPName = "Employment";
                if (Sm.Find_In_Set(SSPCode, mSSPCodeForPension))
                    SSPName = "Pension";
            }

            int index = 1;
            foreach (var x in l3)
            {
                l.Add(new JournalHdr()
                {
                    DocNo = Sm.GenerateDocNoJournal(Sm.GetDte(DteDocDt), index),
                    DeptCode = x.DeptCode,
                    CCCode = x.CCCode,
                    SSPName = SSPName
                });

                index += 1;
            }
        }

        private void GetJournalDtl(ref List<DtlDept> l4, ref List<JournalDtlTemp> l21)
        {
            decimal DAmtTemp = 0m, CAmtTemp = 0m;
            var SSPCode = Sm.GetValue("Select SSPCode From TblEmpSSListHdr Where DocNo=@Param;", TxtEmpSSListDocNo.Text);
            string AcNo1 = string.Empty, AcNo2 = string.Empty;

            Array paramAcNo = Array.CreateInstance(typeof(String), 400);

            if (!mIsVoucherRequestSSUseSSCOASetting)
            {
                if (Sm.CompareStr(SSPCode, mSSPCodeForHealth))
                {
                    paramAcNo.SetValue(mAcNoForAllowanceSSHealth, 0);
                    paramAcNo.SetValue(mAcNoForPremiumDebtSSHealth, 1);
                    AcNo1 = mAcNoForAllowanceSSHealth;
                    AcNo2 = mAcNoForPremiumDebtSSHealth;
                }

                if (Sm.CompareStr(SSPCode, mSSPCodeForEmployment))
                {
                    paramAcNo.SetValue(mAcNoForAllowanceSSEmployment, 0);
                    paramAcNo.SetValue(mAcNoForPremiumDebtSSEmployment, 1);
                    AcNo1 = mAcNoForAllowanceSSEmployment;
                    AcNo2 = mAcNoForPremiumDebtSSEmployment;
                }
                if (Sm.Find_In_Set(SSPCode, mSSPCodeForPension))
                {
                    paramAcNo.SetValue(mAcNoForAllowanceSSPension, 0);
                    paramAcNo.SetValue(mAcNoForPremiumDebtSSPension, 1);
                    AcNo1 = mAcNoForAllowanceSSPension;
                    AcNo2 = mAcNoForPremiumDebtSSPension;
                }
            }

            foreach (var x in l4)
            {
                DAmtTemp = 0; CAmtTemp = 0;

                if (mIsVoucherRequestSSUseSSCOASetting)
                {
                    if (x.AcNo1.Length > 0)
                    {
                        DAmtTemp = 0m;
                        CAmtTemp = x.EmployerAmt + x.EmployeeAmt;

                        l21.Add(new JournalDtlTemp()
                        {
                            DocNo = string.Empty,
                            AcNo = x.AcNo1,
                            DNo = string.Empty,
                            CCCode = x.CCCode,
                            DeptCode = x.DeptCode,
                            EntCode = x.EntCode,
                            DAmt = DAmtTemp,
                            CAmt = CAmtTemp, 
                            Remark = string.Empty,
                        });
                    }

                    if (x.AcNo10.Length > 0)
                    {
                        DAmtTemp = x.EmployerAmt;
                        CAmtTemp = 0m;

                        l21.Add(new JournalDtlTemp()
                        {
                            DocNo = string.Empty,
                            AcNo = x.AcNo10,
                            DNo = string.Empty,
                            CCCode = x.CCCode,
                            DeptCode = x.DeptCode,
                            EntCode = x.EntCode,
                            DAmt = DAmtTemp,
                            CAmt = CAmtTemp,
                            Remark = (mIsCOACouldBeChosenMoreThanOnce ? "Employeer" : string.Empty)
                        });
                    }

                    if (x.AcNo2.Length > 0)
                    {
                        DAmtTemp = x.EmployeeAmt;
                        CAmtTemp = 0m;

                        l21.Add(new JournalDtlTemp()
                        {
                            DocNo = string.Empty,
                            AcNo = x.AcNo2,
                            DNo = string.Empty,
                            CCCode = x.CCCode,
                            DeptCode = x.DeptCode,
                            EntCode = x.EntCode,
                            DAmt = DAmtTemp,
                            CAmt = CAmtTemp,
                            Remark = (mIsCOACouldBeChosenMoreThanOnce ? "Employee" : string.Empty)
                        });
                    }
                }
                else
                {
                    foreach (var i in paramAcNo)
                    {
                        if (i != null)
                        {
                            if (i.ToString().Length > 0)
                            {
                                if (i.ToString() == AcNo1)
                                {
                                    DAmtTemp = x.TotalAmt;
                                    CAmtTemp = 0m;
                                }

                                if (i.ToString() == AcNo2)
                                {
                                    CAmtTemp = x.TotalAmt;
                                    DAmtTemp = 0m;
                                }

                                l21.Add(new JournalDtlTemp()
                                {
                                    DocNo = string.Empty,
                                    AcNo = i.ToString(),
                                    DNo = string.Empty,
                                    CCCode = x.CCCode,
                                    DeptCode = x.DeptCode,
                                    EntCode = x.EntCode,
                                    DAmt = DAmtTemp,
                                    CAmt = CAmtTemp,
                                    Remark = string.Empty
                                });
                            }
                        }
                    }
                }
            }

            if (l21.Count > 0)
            {
                l21.RemoveAll(x => x.DAmt == 0m && x.CAmt == 0m);
            }
        }

        private void SumJournalDtl(ref List<JournalDtlTemp> l21, ref List<JournalDtl> l2)
        {
            if (!mIsCOACouldBeChosenMoreThanOnce)
            {
                l2 = l21.GroupBy(g => new { g.CCCode, g.DeptCode, g.AcNo })
                .Select(s => new JournalDtl()
                {
                    AcNo = s.Key.AcNo,
                    CCCode = s.Key.CCCode,
                    DeptCode = s.Key.DeptCode,
                    EntCode = s.First().EntCode,
                    DocNo = string.Empty,
                    DNo = string.Empty,
                    DAmt = s.Sum(p => p.DAmt),
                    CAmt = s.Sum(p => p.CAmt)
                }
                ).ToList();
            }
            else
            {
                l2 = l21.GroupBy(g => new { g.CCCode, g.DeptCode, g.AcNo, g.Remark, g.EntCode  })
                .Select(s => new JournalDtl()
                {
                    //AcNo = s.Key.AcNo,
                    //CCCode = s.Key.CCCode,
                    //DeptCode = s.Key.DeptCode,
                    //EntCode = s.First().EntCode,
                    //DocNo = string.Empty,
                    //DNo = string.Empty,
                    //DAmt = s.Sum(p => p.DAmt),
                    //CAmt = s.Sum(p => p.CAmt),
                    //Remark = s.First().Remark,

                    AcNo = s.Key.AcNo,
                    CCCode = s.Key.CCCode,
                    DeptCode = s.Key.DeptCode,
                    EntCode = s.Key.EntCode,
                    DocNo = string.Empty,
                    DNo = string.Empty,
                    DAmt = s.Sum(p => p.DAmt),
                    CAmt = s.Sum(p => p.CAmt),
                    Remark = s.Key.Remark
                }
                ).ToList();
            }
        }

        private void GetDocNoJournalDtl(ref List<JournalHdr> l, ref List<JournalDtl> l2)
        {
            int DNo = 1;

            foreach (var x in l)
            {
                DNo = 1;
                foreach (var y in l2
                    .OrderBy(o => o.CCCode)
                    .ThenBy(o => o.DeptCode)
                    .Where(w => w.DocNo.Length == 0 &&
                    w.CCCode == x.CCCode &&
                    w.DeptCode == x.DeptCode))
                {                    
                    y.DocNo = x.DocNo;
                    y.DNo = Sm.Right(string.Concat("000", DNo.ToString()), 3);

                    DNo += 1;                    
                }
            }
        }

        #endregion

        //private string GetProfitCenterCode()
        //{
        //    var Value = TxtEmpSSListDocNo.Text;

        //    if (Value.Length == 0) return string.Empty;

        //    return
        //        Sm.GetValue(
        //            "SELECT B.ProfitCenterCode " +
        //            "FROM tblempsslisthdr A " +
        //            "INNER JOIN tblsite B ON A.SiteCode=B.SiteCode " +
        //            "INNER JOIN tblprofitcenter C ON B.ProfitCenterCode=C.ProfitCenterCode " +
        //            "WHERE C.ProfitCenterCode Is Not Null and A.DocNo=@Param; ",
        //            Value);
        //}

        private string GetProfitCenterCode()
        {
            //var Value = string.Empty ;
            var Value = Sm.GetLue(LueBankAcCode);
            if (Value.Length == 0) return string.Empty;

            return
                Sm.GetValue(
                    " SELECT B.ProfitCenterCode FROM tblbankaccount A " +
                    " INNER JOIN tblsite B ON A.SiteCode = B.SiteCode " +
                    " INNER JOIN tblprofitcenter C ON B.ProfitCenterCode = C.ProfitCenterCode " +
                    " WHERE A.Bankaccode = @Param ",
            Value);
        }

        private void SetLueBCCode(ref DXE.LookUpEdit Lue, string BCCode, string DeptCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select BCCode As Col1, BCName As Col2 From TblBudgetCategory ");
            if (BCCode.Length > 0)
                SQL.AppendLine("Where BCCode=@BCCode ");
            else
                SQL.AppendLine("Where DeptCode=@DeptCode And ActInd='Y' ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            if (BCCode.Length > 0) Sm.CmParam<String>(ref cm, "@BCCode", BCCode);
            if (DeptCode.Length > 0) Sm.CmParam<String>(ref cm, "@DeptCode", DeptCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (BCCode.Length > 0) Sm.SetLue(Lue, BCCode);
        }

        private void ComputeRemainingBudget()
        {
            decimal AvailableBudget = 0m, RequestedBudget = 0m;
            try
            {
                AvailableBudget = Sm.ComputeAvailableBudget(
                    (TxtDocNo.Text.Length > 0 ? TxtDocNo.Text : "XXX"),
                    Sm.GetDte(DteDocDt),
                    string.Empty,
                    Sm.GetLue(LueDeptCode),
                    Sm.GetLue(LueBCCode)
                    );
                RequestedBudget = Decimal.Parse(TxtAmt.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }

            TxtRemainingBudget.Text = Sm.FormatNum(AvailableBudget - RequestedBudget, 0);
        }
        
        internal void ComputeAmt()
        {
            decimal Total = 0m, Amt = 0m, AdjustmentAmt = 0m;

            if (TxtTotal.Text.Length > 0) Total = Decimal.Parse(TxtTotal.Text);
            if (mIsVoucherRequestSSUseAdjustment)
            {
                if (TxtAdjustmentAmt.Text.Length > 0) AdjustmentAmt = Decimal.Parse(TxtAdjustmentAmt.Text);
            }
            Amt = Total + AdjustmentAmt;

            TxtAmt.EditValue = Sm.FormatNum(Amt, 0);

            if (mIsVoucherRequestSSUseBudget) ComputeRemainingBudget();
        }

        private void FormatNumTxtNegative(DXE.TextEdit Txt, Byte FormatType)
        {
            Txt.EditValue = FormatNumNegative(Txt.Text, FormatType);
        }

        private string FormatNumNegative(string Value, byte FormatType)
        {
            try
            {
                decimal NumValue = 0m;
                Value = (Value.Length == 0) ? "0" : Value.Trim();
                if (!decimal.TryParse(Value, out NumValue))
                {
                    Sm.StdMsg(mMsgType.Warning, "Invalid numeric value.");
                    NumValue = 0m;
                }
                switch (FormatType)
                {
                    case 0:
                        return String.Format(
                            (Gv.FormatNum0.Length != 0) ?
                                Gv.FormatNum0 : "{0:#,##0.00##}",
                            NumValue);
                    //return String.Format("{0:#,##0.00##}", NumValue);
                    case 1: return String.Format("{0:#,##0}", NumValue);
                    case 2: return String.Format("{0:#,##0.00}", NumValue);
                    case 3: return String.Format("{0:#,##0.00#}", NumValue);
                    case 8: return String.Format("{0:#,##0.00######}", NumValue);
                    default: return String.Format("{0:###0}", NumValue);
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            return "0";
        }

        private void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'IsAutoJournalActived', 'IsCheckCOAJournalNotExists', 'IsFilterByDeptHR', 'IsVoucherRequestSSUseBudget', 'IsJournalValidationVoucherRequestSSEnabled', ");
            SQL.AppendLine("'IsCOACouldBeChosenMoreThanOnce', 'VoucherCodeFormatType', 'AcNoForPremiumDebtSSHealth', 'AcNoForAllowanceSSHealth', 'AcNoForPremiumDebtSSEmployment', ");
            SQL.AppendLine("'AllowedVoucherRequestSSAdjustmentAmt', 'IsFilterBySiteHR', 'IsVoucherRequestSSJournalDisabled', 'IsVoucherRequestSSUseAdjustment', 'IsVoucherRequestSSUseSSCOASetting', ");
            SQL.AppendLine("'IsVoucherRequestSSJournalBasedOnCostCenter', 'DocTitle', 'AcNoForAllowanceSSEmployment', 'AcNoForPremiumDebtSSPension', 'AcNoForAllowanceSSPension', ");
            SQL.AppendLine("'SSPCodeForEmployment', 'SSPCodeForHealth', 'SSPCodeForPension', 'IsVoucherRequestSSTransactionValidatedbyClosingJournal' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsAutoJournalActived": mIsAutoJournalActived = ParValue == "Y"; break;
                            case "IsCheckCOAJournalNotExists": mIsCheckCOAJournalNotExists = ParValue == "Y"; break;
                            case "IsFilterByDeptHR": mIsFilterByDeptHR = ParValue == "Y"; break;
                            case "IsVoucherRequestSSUseBudget": mIsVoucherRequestSSUseBudget = ParValue == "Y"; break;
                            case "IsJournalValidationVoucherRequestSSEnabled": mIsJournalValidationVoucherRequestSSEnabled = ParValue == "Y"; break;
                            case "IsCOACouldBeChosenMoreThanOnce": mIsCOACouldBeChosenMoreThanOnce = ParValue == "Y"; break;
                            case "IsFilterBySiteHR": mIsFilterBySiteHR = ParValue == "Y"; break;
                            case "IsVoucherRequestSSJournalDisabled": mIsVoucherRequestSSJournalDisabled = ParValue == "Y"; break;
                            case "IsVoucherRequestSSUseAdjustment": mIsVoucherRequestSSUseAdjustment = ParValue == "Y"; break;
                            case "IsVoucherRequestSSUseSSCOASetting": mIsVoucherRequestSSUseSSCOASetting = ParValue == "Y"; break;
                            case "IsVoucherRequestSSJournalBasedOnCostCenter": mIsVoucherRequestSSJournalBasedOnCostCenter = ParValue == "Y"; break;
                            case "IsVoucherRequestSSTransactionValidatedbyClosingJournal": mIsVoucherRequestSSTransactionValidatedbyClosingJournal = ParValue == "Y"; break;

                            //string
                            case "VoucherCodeFormatType": mVoucherCodeFormatType = ParValue; break;
                            case "AcNoForPremiumDebtSSHealth": mAcNoForPremiumDebtSSHealth = ParValue; break;
                            case "AcNoForAllowanceSSHealth": mAcNoForAllowanceSSHealth = ParValue; break;
                            case "AcNoForPremiumDebtSSEmployment": mAcNoForPremiumDebtSSEmployment = ParValue; break;
                            case "DocTitle": mDocTitle = ParValue; break;
                            case "AcNoForAllowanceSSEmployment": mAcNoForAllowanceSSEmployment = ParValue; break;
                            case "AcNoForPremiumDebtSSPension": mAcNoForPremiumDebtSSPension = ParValue; break;
                            case "AcNoForAllowanceSSPension": mAcNoForAllowanceSSPension = ParValue; break;
                            case "SSPCodeForEmployment": mSSPCodeForEmployment = ParValue; break;
                            case "SSPCodeForHealth": mSSPCodeForHealth = ParValue; break;
                            case "SSPCodeForPension": mSSPCodeForPension = ParValue; break;
                            
                            //decimal
                            case "AllowedVoucherRequestSSAdjustmentAmt":
                                if (ParValue.Length == 0)
                                    mAllowedVoucherRequestSSAdjustmentAmt = 1m;
                                else
                                    mAllowedVoucherRequestSSAdjustmentAmt = decimal.Parse(ParValue);
                                break;
                        }
                    }
                }
                dr.Close();
            }
        }

        private void ParPrint(string DocNo)
        {
            string Doctitle = Sm.GetParameter("Doctitle");
            if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;
            var l = new List<VoucherRequestSS>();
            var ldtl2 = new List<VoucherRequestSSDtl>();
            var ldtl = new List<VoucherRequestSSDtl2>();

            string[] TableName = { "VoucherRequestSS", "VoucherRequestSSDtl", "VoucherRequestSSDtl2" };
            List<IList> myLists = new List<IList>();
           
            #region Header
            var cm = new MySqlCommand();

            var SQL = new StringBuilder();
            SQL.AppendLine("Select @CompanyLogo As CompanyLogo,(Select ParValue From tblparameter Where ParCode='ReportTitle1')As CompanyName, ");
            SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle2')As CompanyAddress, ");
            SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle4')As CompanyPhone, ");
            SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle5')As CompanyFax, ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As CompanyAddressCity, ");

            SQL.AppendLine("B.Yr, Case When B.Mth ='01' then 'Januari' When B.Mth ='02' then 'Februari' When B.Mth ='03' then 'Maret' When B.Mth ='04' then 'April' ");
            SQL.AppendLine("When B.Mth ='05' then 'Mei' When B.Mth ='06' then 'Juni' When B.Mth ='07' then 'Juli' When B.Mth ='08' then 'Agusutus' When B.Mth ='09' then 'September' ");
            SQL.AppendLine("When B.Mth ='10' then 'Oktober' When B.Mth ='11' then 'November' When B.Mth ='12' then 'Desember' End As Mth, C.SSPName, A.PaidToBankCode, ");
            SQL.AppendLine("A.PaidtoBankAcNo, E.Pimp, (Select OptDesc From TblOption Where OptCat='VoucherPaymentType' AND OptCode=A.PaymentType Limit 1)As PaymentType, ");
            SQL.AppendLine("A.GiroNo, D.BankName As GiroBankName, DATE_FORMAT(A.DueDt,'%d %M %Y') As GiroDueDt, A.CurCode, A.VoucherRequestDocNo, A.Remark, A.Amt As AmtHdr, F.DocEnclosure, A.DocNo, DATE_FORMAT(A.DocDt,'%d %M %Y') As DocDt ");
            SQL.AppendLine("From TblVoucherRequestSSHdr A ");
            SQL.AppendLine("Inner Join TblEmpSSListHdr B On A.EmpSSListDocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblSSProgram C On B.SSPCode=C.SSPCode ");
            //SQL.AppendLine("Inner join tbluser D On A.CreateBy=D.UserCode ");
            SQL.AppendLine("Left Join TblBank D On A.BankCode=D.BankCode ");
            SQL.AppendLine("left Join ( ");
            SQL.AppendLine("    Select Empname As Pimp from TblEmployee A ");
            SQL.AppendLine("    Left Join tblUser B On A.userCode = B.UserCode ");
            SQL.AppendLine("    left Join TblGroup C On B.GrpCode = C.GrpCode ");
            SQL.AppendLine("    Where A.deptCode = 'HRD' And C.grpCode = 'Pimp. HRD' ");
            SQL.AppendLine("    limit 1");
            SQL.AppendLine(")E On 0=0");
            SQL.AppendLine("left join TblVoucherRequestHdr F On A.VoucherrequestDocNo=F.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo And A.CancelInd='N' ");

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]
                {
                    //0
                    "CompanyLogo",

                    //1-5
                    "CompanyName",
                    "CompanyAddress",
                    "CompanyPhone",
                    "CompanyFax",
                    "CompanyAddressCity",

                    //6-10
                    "Yr",
                    "Mth",
                    "SSPName",
                    "PaidToBankCode",
                    "PaidtoBankAcNo",

                    //11-15
                    "Pimp",
                    "PaymentType",
                    "GiroNo",
                    "GiroBankName",
                    "GiroDueDt",

                    //16-20
                    "CurCode",
                    "Remark",
                    "AmtHdr",
                    "DocEnclosure",
                    "DocNo",

                    //21-22
                    "DocDt",
                    "VoucherRequestDocNo"

                });

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new VoucherRequestSS()
                        {
                            CompanyLogo = Sm.DrStr(dr, c[0]),
                            CompanyName = Sm.DrStr(dr, c[1]),
                            CompanyAddress = Sm.DrStr(dr, c[2]),
                            CompanyPhone = Sm.DrStr(dr, c[3]),
                            CompanyFax = Sm.DrStr(dr, c[4]),
                            CompanyAddressCity = Sm.DrStr(dr, c[5]),

                            Yr = Sm.DrStr(dr, c[6]),
                            Mth = Sm.DrStr(dr, c[7]),
                            SSPName = Sm.DrStr(dr, c[8]),
                            BankName = Sm.DrStr(dr, c[9]),
                            Account = Sm.DrStr(dr, c[10]),

                            Pimp = Sm.DrStr(dr, c[11]),
                            PaymentType = Sm.DrStr(dr, c[12]),
                            GiroNo = Sm.DrStr(dr, c[13]),
                            GiroBankName = Sm.DrStr(dr, c[14]),
                            GiroDueDt = Sm.DrStr(dr, c[15]),

                            CurCode = Sm.DrStr(dr, c[16]),
                            Remark = Sm.DrStr(dr, c[17]),
                            AmtHdr = Sm.DrDec(dr, c[18]),
                            Terbilang = Sm.Terbilang(Sm.DrDec(dr, c[18])),
                            Terbilang2 = Sm.Terbilang2(Sm.DrDec(dr, c[18])),
                            DocEnclosure = Sm.DrStr(dr, c[19]),
                            DocNo = Sm.DrStr(dr, c[20]),

                            DocDt = Sm.DrStr(dr, c[21]),
                            VRDocno = Sm.DrStr(dr, c[22]),

                            Print = String.Format("{0:dd MM yyyy }", Sm.ConvertDateTime(Sm.ServerCurrentDateTime())),
                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime())),
                        });
                    }
                }
                dr.Close();
            }
            myLists.Add(l);
            #endregion

            #region Detail2

            var cmDtl2 = new MySqlCommand();
            var SQLDtl2 = new StringBuilder();
            using (var cnDtl2 = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl2.Open();
                cmDtl2.Connection = cnDtl2;

                SQLDtl2.AppendLine("Select X.SSCode, X.SSName, Round(((SUM(X.Totalperc) / Count(X.EmpCode)))/100,4) As Prosen,");
                SQLDtl2.AppendLine("X.SSCode, X.SSName, Round((((SUM(X.Totalperc) / Count(X.EmpCode))/100)*100),0) As Prosen2,");
                SQLDtl2.AppendLine("Round((SUM(X.Salary) / Count(X.EmpCode)),0) As Salary,");
                SQLDtl2.AppendLine("Round(((SUM(X.Salary) / Count(X.EmpCode)) * ((SUM(X.Totalperc)/100) / Count(X.EmpCode))),0) As Up,");
                SQLDtl2.AppendLine("Round((((SUM(X.Salary) / Count(X.EmpCode)) * ((SUM(X.Totalperc)/100) / Count(X.EmpCode))) * Count(X.EmpCode)),0) As Iuran,");
                SQLDtl2.AppendLine("Count(X.EmpCode) As Emp ");
                SQLDtl2.AppendLine("From ( ");
                SQLDtl2.AppendLine("Select A.EmpCode, A.PName, ");
                SQLDtl2.AppendLine("A.SSCode, E.SSName, ");
                SQLDtl2.AppendLine("A.Salary, A.EmployerPerc, A.EmployerAmt, A.EmployeePerc, A.EmployeeAmt, A.TotalPerc, A.TotalAmt ");
                SQLDtl2.AppendLine("From TblEmpSSListDtl A ");
                SQLDtl2.AppendLine("Inner Join TblEmployee B On A.EmpCode=B.EmpCode ");
                SQLDtl2.AppendLine("Left Join TblSS E On A.SSCode=E.SSCode ");
                SQLDtl2.AppendLine("Where A.DocNo=@DocNo And A.TotalAmt<>0 ");
                SQLDtl2.AppendLine("Order By B.EmpName, A.EmpCode, A.EmpInd Desc ");
                SQLDtl2.AppendLine(")X ");
                SQLDtl2.AppendLine("Group By X.SSCode, X.SSName ");

                cmDtl2.CommandText = SQLDtl2.ToString();

                string m = TxtEmpSSListDocNo.Text;
                Sm.CmParam<String>(ref cmDtl2, "@DocNo", m);

                var drDtl2 = cmDtl2.ExecuteReader();
                var cDtl2 = Sm.GetOrdinal(drDtl2, new string[]
                {
                        //0
                         "SSCode",
                        //1-5
                        "SSName",
                        "Prosen",
                        "Salary",
                        "Up",
                        "Iuran",
                        //6
                        "Emp",
                        "Prosen2"
                });

                if (drDtl2.HasRows)
                {
                    while (drDtl2.Read())
                    {
                        ldtl2.Add(new VoucherRequestSSDtl()
                        {
                            SSCode = Sm.DrStr(drDtl2, cDtl2[0]),
                            SSName = Sm.DrStr(drDtl2, cDtl2[1]),
                            Prosen = Sm.DrStr(drDtl2, cDtl2[2]),
                            SSPSalaryName = Sm.DrDec(drDtl2, cDtl2[3]),
                            Up = Sm.DrDec(drDtl2, cDtl2[4]),
                            Iuran = Sm.DrDec(drDtl2, cDtl2[5]),
                            Emp = Sm.DrDec(drDtl2, cDtl2[6]),
                            Prosen2 = Sm.DrStr(drDtl2, cDtl2[7]),
                        });
                    }
                }
                drDtl2.Close();
            }
            myLists.Add(ldtl2);

            #endregion       

            #region Detail

            var cmDtl = new MySqlCommand();
            var SQLDtl = new StringBuilder();
            using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl.Open();
                cmDtl.Connection = cnDtl;

                SQLDtl.AppendLine("Select Description, Amt, Remark From TblVoucherRequestSSDtl ");
                SQLDtl.AppendLine("Where DocNo=@DocNo ");

                cmDtl.CommandText = SQLDtl.ToString();

                Sm.CmParam<String>(ref cmDtl, "@DocNo", TxtDocNo.Text);

                var drDtl = cmDtl.ExecuteReader();
                var cDtl = Sm.GetOrdinal(drDtl, new string[]
                {
                        //0
                        "Description",

                        //1-2
                        "Amt",
                        "Remark",
                });

                if (drDtl.HasRows)
                {
                    while (drDtl.Read())
                    {
                        ldtl.Add(new VoucherRequestSSDtl2()
                        {
                            Description = Sm.DrStr(drDtl, cDtl[0]),
                            Amt = Sm.DrDec(drDtl, cDtl[1]),
                            Remark = Sm.DrStr(drDtl, cDtl[2]),
                        });
                    }
                }
                drDtl.Close();
            }
            myLists.Add(ldtl);

            #endregion       

            string SSpCode = Sm.GetValue("Select SSPCode From tblSSprogram Where SSPName = '" + TxtSSPCode.Text + "'");

            if (Doctitle == "TWC" || Doctitle == "KMI")
                Sm.PrintReport("VoucherRequestSS3", myLists, TableName, false);
            else
            {
                if (Doctitle == "SRN")
                {
                    Sm.PrintReport("VoucherRequestSS3", myLists, TableName, false);
                }
                else
                {
                    if (SSpCode != Sm.GetValue("Select SSPCode From tblSSprogram Where SSPName like '%kesehatan%' "))
                    {
                        Sm.PrintReport("VoucherRequestSS", myLists, TableName, false);
                    }
                    else
                    {
                        Sm.PrintReport("VoucherRequestSS2", myLists, TableName, false);
                    }
                }
            }
        }

        private bool IsNeedApproval()
        {
            return Sm.IsDataExist("Select 1 From TblDocApprovalSetting Where DocType='VoucherRequestSS' Limit 1;");
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueAcType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueAcType, new Sm.RefreshLue1(Sl.SetLueAcType));
        }

        private void LuePaymentType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePaymentType, new Sm.RefreshLue1(Sl.SetLueVoucherPaymentType));

            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> { LueBankCode, TxtGiroNo, DteDueDt });

            if (Sm.CompareStr(Sm.GetLue(LuePaymentType), "B"))
            {
                Sm.SetControlReadOnly(LueBankCode, false);
                Sm.SetControlReadOnly(TxtGiroNo, true);
                Sm.SetControlReadOnly(DteDueDt, true);
                return;
            }

            if (Sm.CompareStr(Sm.GetLue(LuePaymentType), "G") || Sm.CompareStr(Sm.GetLue(LuePaymentType), "K"))
            {
                Sm.SetControlReadOnly(LueBankCode, false);
                Sm.SetControlReadOnly(TxtGiroNo, false);
                Sm.SetControlReadOnly(DteDueDt, false);
                return;
            }

            Sm.SetControlReadOnly(LueBankCode, true);
            Sm.SetControlReadOnly(TxtGiroNo, true);
            Sm.SetControlReadOnly(DteDueDt, true);
        }

        private void LueBankCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueBankCode, new Sm.RefreshLue1(Sl.SetLueBankCode));
        }

        private void TxtGiroNo_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtGiroNo);
        }

        private void LueCurCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCurCode, new Sm.RefreshLue1(Sl.SetLueCurCode));
        }

        private void LueBankAcCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueBankAcCode, new Sm.RefreshLue1(Sl.SetLueBankAcCode));
        }

        private void TxtPaymentUser_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtPaymentUser);
        }

        private void TxtPaidToBankCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtPaidToBankCode);
        }

        private void TxtPaidToBankBranch_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtPaidToBankBranch);
        }

        private void TxtPaidToBankAcName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtPaidToBankAcName);
        }

        private void TxtPaidToBankAcNo_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtPaidToBankAcNo);
        }

        private void TxtAdjustmentAmt_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (mIsVoucherRequestSSUseAdjustment && TxtDocNo.Text.Length == 0)
                {
                    FormatNumTxtNegative(TxtAdjustmentAmt, 0);
                    ComputeAmt();
                }
            }
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled && mIsVoucherRequestSSUseBudget)
            {
                Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, mIsFilterByDeptHR ? "Y" : "N");
                SetLueBCCode(ref LueBCCode, string.Empty, Sm.GetLue(LueDeptCode));
            }
        }

        private void LueBCCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled && mIsVoucherRequestSSUseBudget)
            {
                Sm.RefreshLookUpEdit(LueBCCode, new Sm.RefreshLue3(SetLueBCCode), string.Empty, Sm.GetLue(LueDeptCode));
                ComputeRemainingBudget();
            }
        }

        private void DteDocDt_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled && mIsVoucherRequestSSUseBudget)
            {
                ComputeRemainingBudget();
            }
        }

        #endregion

        #region Button Event

        private void BtnEmpSSListDocNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmVoucherRequestSSDlg(this));
        }

        private void BtnEmpSSListDocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtEmpSSListDocNo, "Social Security#", false))
            {
                var f1 = new FrmEmpSSList(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = TxtEmpSSListDocNo.Text;
                f1.ShowDialog();
            }
        }

        #endregion

        #endregion

        #region Class

        private class JournalHdr
        {
            public string DocNo { get; set; }
            public string DeptCode { get; set; }
            public string CCCode { get; set; }
            public string SSPName { get; set; }
        }

        private class JournalDtlTemp
        {
            public string DocNo { get; set; }
            public string AcNo { get; set; }
            public string CCCode { get; set; }
            public string DeptCode { get; set; }
            public string EntCode { get; set; }
            public string DNo { get; set; }
            public string Notes { get; set; }
            public string Remark { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
        }

        private class JournalDtl
        {
            public string DocNo { get; set; }
            public string AcNo { get; set; }            
            public string CCCode { get; set; }
            public string DeptCode { get; set; }
            public string EntCode { get; set; }
            public string DNo { get; set; }
            public string Notes { get; set; }
            public string Remark { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
        }

        private class JournalHdrCancel
        {
            public string DocNo { get; set; }
            public string JournalDocNo { get; set; }
            public string JournalDocNo2 { get; set; }
        }

        private class DtlDept
        {
            public string DeptCode { get; set; }
            public string CCCode { get; set; }
            public string AcNo10 { get; set; }
            public string AcNo1 { get; set; }
            public string AcNo2 { get; set; }
            public string EntCode { get; set; }
            public decimal EmployerAmt { get; set; }
            public decimal EmployeeAmt { get; set; }
            public decimal TotalAmt { get; set; }
        }

        private class DtlCostCenter
        {
            public string CCCode { get; set; }
            public string DeptCode { get; set; }
            public string AcNo10 { get; set; }
            public string AcNo1 { get; set; }
            public string AcNo2 { get; set; }
            public string EntCode { get; set; }
            public decimal EmployerAmt { get; set; }
            public decimal EmployeeAmt { get; set; }
            public decimal TotalAmt { get; set; }
        }

        class VoucherRequestSS
        {
            public string CompanyLogo { get; set; }
            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyPhone { get; set; }
            public string CompanyFax { get; set; }
            public string CompanyAddressCity { get; set; }
            public string Yr { get; set; }
            public string Mth { get; set; }
            public string SSPName { get; set; }
            public string Print { get; set; }
            public string PrintBy { get; set; }
            public string BankName { get; set; }
            public string Account { get; set; }
            public string Pimp { get; set; }
            public string PaymentType { get; set; }
            public string GiroNo { get; set; }
            public string GiroBankName { get; set; }
            public string GiroDueDt { get; set; }
            public string CurCode { get; set; }
            public string VRDocno { get; set; }
            public string Remark { get; set; }
            public decimal AmtHdr { get; set; }
            public string Terbilang { get; set; }
            public string Terbilang2 { get; set; }
            public string DocEnclosure { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; } 
        }

        class VoucherRequestSSDtl
        {
            public string SSCode { get; set; }
            public string SSName { get; set; }
            public string Prosen { get; set; }
            public string Prosen2 { get; set; }
            public decimal SSPSalaryName { get; set; }
            public decimal Up { get; set; }
            public decimal Iuran { get; set; }
            public decimal Emp { get; set; }
        }

        class VoucherRequestSSDtl2
        {
            public string Description { get; set; }
            public decimal Amt { get; set; }
            public string Remark { get; set; }
        }

        #endregion
    }
}
