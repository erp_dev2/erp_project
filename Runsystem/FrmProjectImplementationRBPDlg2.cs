﻿#region Update
/*
    06/09/2019 [WED] ubah alur ke SO Contract Revision
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmProjectImplementationRBPDlg2 : RunSystem.FrmBase4
    {
        #region Field

        private FrmProjectImplementationRBP mFrmParent;
        private string mSQL = string.Empty, mPRJIDocNo = string.Empty;

        #endregion

        #region Constructor

        public FrmProjectImplementationRBPDlg2(FrmProjectImplementationRBP FrmParent, string PRJIDocNo)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mPRJIDocNo = PRJIDocNo;
        }

        #endregion

        #region Methods

        #region Form Load

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            SetGrd();
            SetSQL();
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.ResourceItCode, B.ItName, B.ItGrpCode, C.ItGrpName, A.Amt ");
            SQL.AppendLine("From TblProjectImplementationDtl2 A ");
            SQL.AppendLine("Inner Join TblItem B On A.ResourceItCode = B.ItCode ");
            SQL.AppendLine("Left Join TblItemGroup C On B.ItGrpCode = C.ItGrpCode ");

            if (mFrmParent.mIsFilterBySite)
            {
                SQL.AppendLine("Inner Join TblProjectImplementationHdr D On A.DocNo=D.DocNo ");
                SQL.AppendLine("Inner Join TblSOContractRevisionHdr D1 On D.SOContractDocNo = D1.DocNo ");
                SQL.AppendLine("Inner Join TblSOContractHdr E On D1.SOCDocNo=E.DocNo ");
                SQL.AppendLine("Inner Join TblBOQHdr F On E.BOQDocNo=F.DocNo ");
                SQL.AppendLine("Inner Join TblLOPHdr G On F.LOPDocNo=G.DocNo ");
                SQL.AppendLine("And (G.SiteCode Is Null Or ( ");
                SQL.AppendLine("    G.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(G.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("            ) ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine(")) ");
            }

            SQL.AppendLine("Where A.DocNo = @DocNo ");
            SQL.AppendLine("And Concat(A.DocNo, A.ResourceItCode) Not In ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select Concat(PRJIDocNo, ResourceItCode) ");
            SQL.AppendLine("    From TblProjectImplementationRBPHdr ");
            SQL.AppendLine("    Where PRJIDocNo = @DocNo ");
            SQL.AppendLine("    And CancelInd = 'N' ");
            SQL.AppendLine(") ");

            mSQL = SQL.ToString();
        }
        #endregion

        #region Set Grid

        private void SetGrd()
        {
            Grd1.Cols.Count = 6;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Resource Item Code",
                    "Resource Item Name",
                    "Item Group Code",
                    "Item Group",
                    "Total With Tax"
                }, new int[]
                {
                    //0
                    50,

                    //1-3
                    150, 220, 120, 200, 120
                }
            );
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4, 5 });
            Sm.GrdFormatDec(Grd1, new int[] { 5 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3 }, !ChkHideInfoInGrd.Checked);
        }
        #endregion

        #region Grid Method

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        #endregion

        #region Show Data

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " And 0 = 0 ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@DocNo", mPRJIDocNo);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtResourceItCode.Text, new string[] { "A.ResourceItCode", "B.ItName" });

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By A.DNo; ",
                    new string[]
                    {
                        //0
                        "ResourceItCode", 

                        //1-4
                        "ItName", "ItGrpCode", "ItGrpName", "Amt"

                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd1.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 5, 4);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 2);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.TxtResourceItCode.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
                mFrmParent.TxtResourceItName.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 2);
                mFrmParent.TxtTotalWithTax.EditValue = Sm.FormatNum(Sm.GetGrdDec(Grd1, Grd1.CurRow.Index, 5), 0);
                this.Hide();
            }
        }
        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtResourceItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkResourceItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Resource");
        }

        #endregion

        #endregion

    }
}
