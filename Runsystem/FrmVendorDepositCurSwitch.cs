﻿#region Update
/*
    06/11/2017 [WED] insert ke tabel summary2.
    07/06/2018 [TKG] bug saat insert ke tabel summary2 dari IDR to Non IDR.
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmVendorDepositCurSwitch : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string 
            mMenuCode = string.Empty, mAccessInd = string.Empty,
            mVdCode = string.Empty, mEntCode = string.Empty;
        internal FrmVendorDepositCurSwitchFind FrmFind;
        internal string mDocNo = string.Empty;
        internal bool mIsAPDownpaymentUseEntity = false;
        private bool mIsAutoJournalActived = false;
        private string 
            mMainCurCode = string.Empty,
            mAcNoForForeignExchange = string.Empty,
            mVendorAcNoDownPayment = string.Empty;

        #endregion

        #region Constructor

        public FrmVendorDepositCurSwitch(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetFormControl(mState.View);
                Sl.SetLueCurCode(ref LueCurCode2);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { DteDocDt, LueCurCode2, TxtAmt1, TxtRateAmt, MeeRemark }, true);
                    BtnVdCode.Enabled = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { DteDocDt, TxtAmt1, LueCurCode2, TxtRateAmt, MeeRemark }, false);
                    BtnVdCode.Enabled = true;
                    TxtRateAmt.EditValue = Sm.FormatNum(1, 0);
                    DteDocDt.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            mVdCode = string.Empty;
            mEntCode = string.Empty;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, TxtVdCode, TxtEntCode, TxtCurCode1, 
                LueCurCode2, MeeRemark
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit>  
            { TxtAmt1, TxtAmt2,  TxtExistingAmt1, TxtRateAmt }, 0);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmVendorDepositCurSwitchFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sm.FormShowDialog(new FrmVendorDepositCurSwitchDlg(this));
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                InsertData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Save Data

        private void GetVendorDepositSummary2(ref List<Rate> l)
        {
            if (Sm.CompareStr(TxtCurCode1.Text, mMainCurCode)) return;

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select VdCode, CurCode, ExcRate, Amt ");
            SQL.AppendLine("From TblVendorDepositSummary2 ");
            SQL.AppendLine("Where VdCode=@VdCode ");
            SQL.AppendLine("And CurCode=@CurCode1 ");
            SQL.AppendLine("And Amt>0 ");
            SQL.AppendLine("Order By CreateDt Asc; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@VdCode", mVdCode);
                Sm.CmParam<String>(ref cm, "@CurCode1", TxtCurCode1.Text);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                {
                    //0
                    "VdCode",

                    //1-3
                    "CurCode",
                    "ExcRate",
                    "Amt"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Rate()
                        {
                            VdCode = Sm.DrStr(dr, c[0]),
                            CurCode = Sm.DrStr(dr, c[1]),
                            ExcRate = Sm.DrDec(dr, c[2]),
                            Amt = Sm.DrDec(dr, c[3])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", "", mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;
            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "VendorDepositCurSwitch", "TblVendorDepositCurSwitch");

            var cml = new List<MySqlCommand>();

            var lR = new List<Rate>();
            decimal DP = 0m;

            GetVendorDepositSummary2(ref lR);

            if (lR.Count > 0)
            {
                DP = decimal.Parse(TxtAmt1.Text);
                for (int i = 0; i < lR.Count; i++)
                {
                    if (DP > 0)
                    {
                        cml.Add(ReduceAmtSummary2(ref lR, i, DP));
                        DP = DP - lR[i].Amt;
                    }
                }

                // save ke tabel summary2 untuk nilai switch nya

                if (Sm.GetLue(LueCurCode2) != mMainCurCode && TxtCurCode1.Text != mMainCurCode) // switch from non-MainCurCode to non-MainCurCode
                {
                    decimal rate = 0m;
                    for (int x = 0; x < lR.Count; x++)
                    {
                        rate += lR[x].ExcRate / decimal.Parse(TxtRateAmt.Text);
                    }

                    rate = rate / lR.Count;

                    cml.Add(SaveExcRateOtherMainCurCode(rate));
                }
                else // swicth from MainCurCode or to MainCurCode
                {
                    cml.Add(SaveExcRateMainCurCode());
                }
            }
            else
            { 
                // 07/06/2018 [TKG] untuk IDR to Non IDR
                if (!Sm.CompareStr(Sm.GetLue(LueCurCode2), mMainCurCode))
                    cml.Add(SaveVendorDepositSummary2());
            }
            
            cml.Add(SaveVendorDepositCurSwitch(DocNo));

            if (mIsAutoJournalActived)
            {
                decimal Amt1 = 0m, Amt2 = 0m;
                PrepareJournal(ref Amt1, ref Amt2);
                if (Amt1 != Amt2)
                {
                    if (Amt1 < Amt2)
                        cml.Add(SaveJournal(DocNo, 1, Amt2-Amt1));
                    else
                        cml.Add(SaveJournal(DocNo, 2, Amt1-Amt2));
                }
            }

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private MySqlCommand SaveVendorDepositSummary2()
        {
            var SQL = new StringBuilder();
            decimal RateAmt = Decimal.Parse(TxtRateAmt.Text), ExcRate = 0m;

            if (RateAmt!=0) ExcRate = 1m/RateAmt;
            
            SQL.AppendLine("Insert Into TblVendorDepositSummary2 ");
            SQL.AppendLine("(VdCode, CurCode, EntCode, ExcRate, Amt, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@VdCode, @CurCode, IfNull(@EntCode, ''), @ExcRate, @Amt, ");
            SQL.AppendLine("@UserCode, CurrentDateTime()) ");
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("    Update ");
            SQL.AppendLine("        Amt=Amt+@Amt, ");
            SQL.AppendLine("        LastUpBy=@UserCode, ");
            SQL.AppendLine("        LastUpDt=CurrentDateTime(); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            
            Sm.CmParam<String>(ref cm, "@VdCode", mVdCode);
            Sm.CmParam<String>(ref cm, "@EntCode", mEntCode);
            Sm.CmParam<Decimal>(ref cm, "@RateAmt", Decimal.Parse(TxtRateAmt.Text));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode2));
            Sm.CmParam<Decimal>(ref cm, "@ExcRate", ExcRate);
            Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtAmt2.Text));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal(string DocNo, byte Type, decimal Amt)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Update TblVendorDepositCurSwitch Set ");
            SQL.AppendLine("    JournalDocNo=@JournalDocNo ");
            SQL.AppendLine("Where DocNo=@DocNo;");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CreateBy, CreateDt) ");
            SQL.AppendLine("Select JournalDocNo, DocDt, Concat('Vendor Deposit (Currency Switch) : ', DocNo) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, (Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, ");
            SQL.AppendLine("CreateBy, CreateDt ");
            SQL.AppendLine("From TblVendorDepositCurSwitch Where DocNo=@DocNo;");

            SQL.AppendLine("Insert Into TblJournalDtl ");
            SQL.AppendLine("(DocNo, DNo, AcNo, DAmt, CAmt, CreateBy, CreateDt) ");

            if (Type == 1)
            {
                SQL.AppendLine("Select DocNo, '001' As DNo, ");
                SQL.AppendLine("@AcNoForForeignExchange As AcNo, @Amt As DAmt, 0 As CAmt, CreateBy, CreateDt ");
                SQL.AppendLine("From TblJournalHdr Where DocNo=@JournalDocNo ");
                SQL.AppendLine("Union All ");
                SQL.AppendLine("Select DocNo, '002' As DNo, ");
                SQL.AppendLine("Concat(@VendorAcNoDownPayment, @VdCode) As AcNo, 0 As DAmt, @Amt As CAmt, CreateBy, CreateDt ");
                SQL.AppendLine("From TblJournalHdr A ");
                SQL.AppendLine("Where DocNo=@JournalDocNo;");
            }
            else
            {
                SQL.AppendLine("Select DocNo, '001' As DNo, ");
                SQL.AppendLine("Concat(@VendorAcNoDownPayment, @VdCode) As AcNo, @Amt As DAmt, 0 As CAmt, CreateBy, CreateDt ");
                SQL.AppendLine("From TblJournalHdr A ");
                SQL.AppendLine("Where DocNo=@JournalDocNo ");
                SQL.AppendLine("Union All ");
                SQL.AppendLine("Select DocNo, '002' As DNo, ");
                SQL.AppendLine("@AcNoForForeignExchange As AcNo, 0 As DAmt, @Amt As CAmt, CreateBy, CreateDt ");
                SQL.AppendLine("From TblJournalHdr Where DocNo=@JournalDocNo;");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(Sm.GetDte(DteDocDt), 1));
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@AcNoForForeignExchange", mAcNoForForeignExchange);
            Sm.CmParam<String>(ref cm, "@VendorAcNoDownPayment", mVendorAcNoDownPayment);
            Sm.CmParam<String>(ref cm, "@VdCode", mVdCode);
            Sm.CmParam<Decimal>(ref cm, "@Amt", Amt);

            return cm;
        }

        private MySqlCommand SaveVendorDepositCurSwitch(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblVendorDepositCurSwitch (DocNo, DocDt, VdCode, EntCode, CurCode1, Amt1, RateAmt1, CurCode2, Amt2, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@DocNo, @DocDt, @VdCode, @EntCode, @CurCode1, @Amt1, @RateAmt1, @CurCode2, @Amt2, @Remark, @UserCode, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblVendorDepositMovement(DocNo, DocType, DocDt, VdCode, EntCode, CurCode, Amt, CreateBy, CreateDt)  ");
            SQL.AppendLine("Values (@DocNo, @DocType1, @DocDt, @VdCode, @EntCode, @CurCode1, @AmtMin, @UserCode, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblVendorDepositMovement(DocNo, DocType, DocDt, VdCode, EntCode, CurCode, Amt, CreateBy, CreateDt)  ");
            SQL.AppendLine("Values (@DocNo, @DocType2, @DocDt, @VdCode, @EntCode, @CurCode2, @AmtPlus, @UserCode, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblVendorDepositSummary(VdCode, EntCode, CurCode, Amt, CreateBy, CreateDt) Values (@VdCode, IfNull(@EntCode, ''), @CurCode1, @Amt1, @UserCode, CurrentDateTime()) ");
            SQL.AppendLine("On Duplicate Key Update Amt = Amt-@Amt1, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

            SQL.AppendLine("Insert Into TblVendorDepositSummary(VdCode, EntCode, CurCode, Amt, CreateBy, CreateDt) Values (@VdCode, IfNull(@EntCode, ''), @CurCode2, @Amt2, @UserCode, CurrentDateTime()) ");
            SQL.AppendLine("On Duplicate Key Update Amt = Amt+@Amt2, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

            var cm = new MySqlCommand(){ CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@MainCurCode", mMainCurCode);
            Sm.CmParam<String>(ref cm, "@VdCode", mVdCode);
            Sm.CmParam<String>(ref cm, "@EntCode", mEntCode);
            Sm.CmParam<String>(ref cm, "@CurCode1", TxtCurCode1.Text);
            Sm.CmParam<Decimal>(ref cm, "@Amt1", Decimal.Parse(TxtAmt1.Text));
            Sm.CmParam<Decimal>(ref cm, "@AmtMin", Decimal.Parse(TxtAmt1.Text)*-1);
            Sm.CmParam<Decimal>(ref cm, "@RateAmt1", Decimal.Parse(TxtRateAmt.Text));
            Sm.CmParam<String>(ref cm, "@CurCode2", Sm.GetLue(LueCurCode2));
            Sm.CmParam<Decimal>(ref cm, "@Amt2", Decimal.Parse(TxtAmt2.Text));
            Sm.CmParam<Decimal>(ref cm, "@AmtPlus", Decimal.Parse(TxtAmt2.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@DocType1", "05");
            Sm.CmParam<String>(ref cm, "@DocType2", "06");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsTxtEmpty(TxtVdCode, "Vendor", false) ||
                Sm.IsTxtEmpty(TxtAmt1, "Switched Amount", true) ||
                Sm.IsLueEmpty(LueCurCode2, "Switch To Currency") ||
                IsCurCodeNotDifferent() ||
                Sm.IsTxtEmpty(TxtRateAmt, "Rate Amount", true) ||
                Sm.IsTxtEmpty(TxtAmt2, "Amount", true) ||
                Sm.IsClosingJournalInvalid(Sm.GetDte(DteDocDt)) ||
                IsAmountNotValid();
        }

        private bool IsCurCodeNotDifferent()
        {
            if (TxtCurCode1.Text == Sm.GetLue(LueCurCode2))
            {
                Sm.StdMsg(mMsgType.Info, "Switched Currency should be different.");
                LueCurCode2.Focus();
                return true;
            }

            return false;
        }

        private bool IsAmountNotValid()
        {
            ComputeExistingAmt(false);
            decimal Exist = decimal.Parse(TxtExistingAmt1.Text);
            decimal Amt1 = decimal.Parse(TxtAmt1.Text);
            if (Amt1 > Exist)
            {
                Sm.StdMsg(mMsgType.Warning, "Switched Amount is bigger than Existing Amount.");
                return true;
            }
            return false;
        }

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var SQL = new StringBuilder();

                SQL.AppendLine("Select A.DocNo, A.DocDt, A.VdCode, B.VdName, A.EntCode, C.EntName, ");
                SQL.AppendLine("A.CurCode1, A.Amt1, A.RateAmt1, A.CurCode2, A.Amt2, A.Remark ");
                SQL.AppendLine("From TblVendorDepositCurSwitch A ");
                SQL.AppendLine("Inner Join TblVendor B On A.VdCode=B.VdCode ");
                SQL.AppendLine("Left Join TblEntity C On A.EntCode=C.EntCode And IfNull(C.EntCode, '')<>'' ");
                SQL.AppendLine("Where A.DocNo=@DocNo;");

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
                Sm.ShowDataInCtrl(
                        ref cm, SQL.ToString(),
                        new string[] 
                        {
                            //0
                            "DocNo", 
                            
                            //1-5
                            "DocDt", "VdCode", "VdName", "EntCode", "EntName", 
                            
                            //6-10
                            "CurCode1", "Amt1", "RateAmt1", "CurCode2", "Amt2", 
                            
                            //11
                            "Remark"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                            Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                            mVdCode = Sm.DrStr(dr, c[2]);
                            TxtVdCode.EditValue = Sm.DrStr(dr, c[3]);
                            mEntCode = Sm.DrStr(dr, c[4]);
                            TxtEntCode.EditValue = Sm.DrStr(dr, c[5]);
                            TxtCurCode1.EditValue = Sm.DrStr(dr, c[6]);
                            TxtAmt1.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[7]), 0);
                            TxtRateAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[8]), 8);
                            Sm.SetLue(LueCurCode2, Sm.DrStr(dr, c[9]));
                            TxtAmt2.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[10]), 0);
                            MeeRemark.EditValue = Sm.DrStr(dr, c[11]);
                        }, true
                    );
                    ComputeExistingAmt(true);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Additional Method

        private MySqlCommand ReduceAmtSummary2(ref List<Rate> lR, int i, decimal DP)
        {
            var SQL1 = new StringBuilder();

            decimal x = ((DP - lR[i].Amt) >= 0) ? lR[i].Amt : DP;

            SQL1.AppendLine("Select @DP:=" + x + "; ");

            SQL1.AppendLine("Update TblVendorDepositSummary2 ");
            SQL1.AppendLine("    Set Amt = Amt-@DP ");
            SQL1.AppendLine("Where VdCode = @VdCode And CurCode = @CurCode And ExcRate = @ExcRate; ");

            var cm1 = new MySqlCommand() { CommandText = SQL1.ToString() };
            Sm.CmParam<String>(ref cm1, "@VdCode", lR[i].VdCode);
            Sm.CmParam<String>(ref cm1, "@CurCode", lR[i].CurCode);
            Sm.CmParam<Decimal>(ref cm1, "@ExcRate", lR[i].ExcRate);
            Sm.CmParam<String>(ref cm1, "@CreateBy", Gv.CurrentUserCode);

            return cm1;
        }

        private MySqlCommand SaveExcRateMainCurCode()
        {
            var SQL1 = new StringBuilder();

            SQL1.AppendLine("Insert Into TblVendorDepositSummary2(VdCode, CurCode, ExcRate, Amt, CreateBy, CreateDt) ");
            SQL1.AppendLine("Select @VdCode, @CurCode2, @ExcRate2, @Amt2, @CreateBy, CurrentDateTime() ");
            SQL1.AppendLine("On Duplicate Key ");
            SQL1.AppendLine("   Update Amt=Amt+@Amt2, LastUpBy=@CreateBy, LastUpDt=CurrentDateTime(); ");

            var cm1 = new MySqlCommand() { CommandText = SQL1.ToString() };
            Sm.CmParam<String>(ref cm1, "@VdCode", mVdCode);
            Sm.CmParam<String>(ref cm1, "@CurCode2", Sm.GetLue(LueCurCode2));
            Sm.CmParam<Decimal>(ref cm1, "@Amt2", decimal.Parse(TxtAmt2.Text));
            Sm.CmParam<Decimal>(ref cm1, "@ExcRate2", (Sm.GetLue(LueCurCode2) == mMainCurCode) ? 1 : (1 / decimal.Parse(TxtRateAmt.Text)));
            Sm.CmParam<String>(ref cm1, "@CreateBy", Gv.CurrentUserCode);

            return cm1;
        }

        private MySqlCommand SaveExcRateOtherMainCurCode(decimal rate)
        {
            var SQL1 = new StringBuilder();

            SQL1.AppendLine("Insert Into TblVendorDepositSummary2(VdCode, CurCode, ExcRate, Amt, CreateBy, CreateDt) ");
            SQL1.AppendLine("Select @VdCode, @CurCode2, @ExcRate2, @Amt2, @CreateBy, CurrentDateTime() ");
            SQL1.AppendLine("On Duplicate Key ");
            SQL1.AppendLine("   Update Amt=Amt+@Amt2, LastUpBy=@CreateBy, LastUpDt=CurrentDateTime(); ");

            var cm1 = new MySqlCommand() { CommandText = SQL1.ToString() };
            Sm.CmParam<String>(ref cm1, "@VdCode", mVdCode);
            Sm.CmParam<String>(ref cm1, "@CurCode2", Sm.GetLue(LueCurCode2));
            Sm.CmParam<Decimal>(ref cm1, "@Amt2", decimal.Parse(TxtAmt2.Text));
            Sm.CmParam<Decimal>(ref cm1, "@ExcRate2", rate);
            Sm.CmParam<String>(ref cm1, "@CreateBy", Gv.CurrentUserCode);

            return cm1;
        }

        private void GetParameter()
        {
            mIsAPDownpaymentUseEntity = Sm.GetParameterBoo("IsAPDownpaymentUseEntity");
            mIsAutoJournalActived = Sm.GetParameterBoo("IsAutoJournalActived");
            mMainCurCode = Sm.GetParameter("MainCurCode");
            mAcNoForForeignExchange = Sm.GetParameter("AcNoForForeignExchange");
            mVendorAcNoDownPayment = Sm.GetParameter("VendorAcNoDownPayment");   
        }

        private void ComputeExistingAmt(bool IsInclSwitchedAmt)
        {
            var SQL = new StringBuilder();
            
            SQL.AppendLine("Select A.Amt-IfNull(B.Amt, 0) As Amt ");
            SQL.AppendLine("From TblVendorDepositSummary A ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T1.VdCode, IfNull(T1.EntCode, '') As EntCode, T1.CurCode, ");
            SQL.AppendLine("    Sum(T1.Amt) As Amt ");
            SQL.AppendLine("    From TblReturnAPDownpayment T1 ");
            SQL.AppendLine("    Inner Join TblVoucherRequestHdr T2 ");
            SQL.AppendLine("        On T1.VoucherRequestDocNo=T2.DocNo ");
            SQL.AppendLine("        And T2.CancelInd='N' ");
            SQL.AppendLine("        And T2.Status<>'C' ");
            SQL.AppendLine("        And T2.VoucherDocNo Is Null ");
            SQL.AppendLine("    Where T1.Status<>'C' ");
            SQL.AppendLine("    And T1.CancelInd='N' ");
            SQL.AppendLine("    And T1.VdCode=@VdCode ");
            SQL.AppendLine("    And IfNull(T1.EntCode, '')=IfNull(@EntCode, '') ");
            SQL.AppendLine("    And T1.CurCode=@CurCode ");
            SQL.AppendLine("    Group By T1.VdCode, IfNull(T1.EntCode, ''), T1.CurCode ");
            SQL.AppendLine(") B ");
            SQL.AppendLine("    On A.VdCode=B.VdCode ");
            SQL.AppendLine("    And IfNull(A.EntCode, '')=IfNull(B.EntCode, '') ");
            SQL.AppendLine("    And A.CurCode=B.CurCode ");
            SQL.AppendLine("Where A.VdCode=@VdCode ");
            SQL.AppendLine("And IfNull(A.EntCode, '')=IfNull(@EntCode, '') ");
            SQL.AppendLine("And A.CurCode=@CurCode;");
            
            var cm = new MySqlCommand(){CommandText = SQL.ToString()};

            Sm.CmParam<String>(ref cm, "@VdCode", mVdCode);
            Sm.CmParam<String>(ref cm, "@EntCode", mEntCode);
            Sm.CmParam<String>(ref cm, "@CurCode", TxtCurCode1.Text);

            decimal Amt = 0m;
            string Value = Sm.GetValue(cm);
            if(Value.Length!=0) Amt = decimal.Parse(Value);
            if (IsInclSwitchedAmt && TxtAmt1.Text.Length>0) Amt += decimal.Parse(TxtAmt1.Text);
            TxtExistingAmt1.EditValue = Sm.FormatNum(Amt, 0);
        }

        internal void ComputeAmt()
        {
            try
            {
                if (Sm.GetLue(LueCurCode2).Length != 0 && TxtRateAmt.Text.Length != 0 && TxtAmt1.Text.Length != 0 && TxtDocNo.Text.Length == 0)
                {
                    decimal Amt = Decimal.Parse(TxtAmt1.Text) * Decimal.Parse(TxtRateAmt.Text);
                    TxtAmt2.EditValue = Sm.FormatNum(Amt, 0);
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void PrepareJournal(ref decimal Amt1, ref decimal Amt2)
        { 
            string 
                CurCode1 = TxtCurCode1.Text,
                CurCode2 = Sm.GetLue(LueCurCode2);

            if (TxtAmt1.Text.Length > 0) 
                Amt1 = decimal.Parse(TxtAmt1.Text);
           
            if (!Sm.CompareStr(CurCode1, mMainCurCode))
                Amt1 *= GetCurRate(CurCode1, mMainCurCode);

            if (TxtAmt2.Text.Length > 0)
                Amt2 = decimal.Parse(TxtAmt2.Text);

            if (!Sm.CompareStr(CurCode2, mMainCurCode))
                Amt2 *= GetCurRate(CurCode2, mMainCurCode);  
        }

        private decimal GetCurRate(string CurCode1, string CurCode2)
        {
            var SQL = new StringBuilder();
            var Value = string.Empty;

            SQL.AppendLine("Select Amt From TblCurrencyRate ");
            SQL.AppendLine("Where CurCode1=@CurCode1 ");
            SQL.AppendLine("And CurCode2=@CurCode2 ");
            SQL.AppendLine("And RateDt<=@DocDt ");
            SQL.AppendLine("Order By RateDt Desc Limit 1;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@CurCode1", CurCode1);
            Sm.CmParam<String>(ref cm, "@CurCode2", CurCode2);
            Value = Sm.GetValue(cm);

            if (Value.Length==0)
                return 0m;
            else
                return decimal.Parse(Value);
        }

        private void ComputeAmt2()
        {
            string 
                CurCode1 = TxtCurCode1.Text, 
                CurCode2 = Sm.GetLue(LueCurCode2), 
                DocDt = Sm.GetDte(DteDocDt);
            decimal 
                Amt1 = 0m, 
                Amt2 = 0m, 
                RateAmt = 0m;

            if (CurCode1.Length == 0 || CurCode2.Length == 0)
            {
                RateAmt = 0m;
                Amt2 = 0m;
                return;
            }

            if (DocDt.Length == 0)
            {
                RateAmt = 0m;
                Amt2 = 0m;
                return;
            }
            else
            {
                try
                {
                    DocDt = Sm.Left(DocDt, 8);
                    if (Sm.CompareStr(CurCode1, CurCode2))
                        RateAmt = 1m;
                    else
                    {
                        if (CurCode2 == mMainCurCode)
                            RateAmt = GetCurRate(CurCode1, CurCode2);
                        else
                            RateAmt = GetCurRate(CurCode2, CurCode1);
                    }
                   if (RateAmt!=0) RateAmt = 1m / RateAmt;
                   if (TxtAmt1.Text.Length > 0) Amt1 = decimal.Parse(TxtAmt1.Text);
                    Amt2 = Amt1 * RateAmt;
                    TxtRateAmt.EditValue = String.Format("{0:#,##0.00######}", RateAmt);
                    TxtAmt2.EditValue = Sm.FormatNum(Amt2, 0);
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
            }
        }

        #endregion

        #endregion

        #region Event

        #region Button Event

        private void BtnVdCode_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmVendorDepositCurSwitchDlg(this));
        }

        #endregion

        #region Misc Control Event

        private void DteDocDt_EditValueChanged(object sender, EventArgs e)
        {
            ComputeAmt2();
        }

        private void TxtAmt1_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.FormatNumTxt(TxtAmt1, 0);
                ComputeAmt2();
            }
        }
      
        private void TxtRateAmt_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    ComputeAmt();
                    Sm.FormatNumTxt(TxtRateAmt, 0);
                }
            }
        }

        private void TxtAmt2_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtAmt2, 0);
        }

        private void LueCurCode2_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtAmt2, TxtRateAmt }, 0);
                    Sm.RefreshLookUpEdit(LueCurCode2, new Sm.RefreshLue1(Sl.SetLueCurCode));
                    ComputeAmt2();
                }
            }
        }

        #endregion

        #endregion

        #region Class

        #region Rate Class

        class Rate
        {
            public decimal Downpayment { get; set; }
            public decimal Amt { get; set; }
            public string CurCode { get; set; }
            public decimal ExcRate { get; set; }
            public string VdCode { get; set; }
        }

        #endregion

        #endregion
    }
}
