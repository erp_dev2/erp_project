﻿#region Update
/*
    08/03/2018 [TKG] tidak dihubungkan ke voucher request
    19/03/2018 [WED] salary ngambil dari yang startdt terakhir dan yang aktif
    13/12/2018 [HAR] perhitungan annual allowance berubah
    20/12/2018 [TKG] bug saat proses meal dan transport
 *  20/12/2018 [HAR] tambah prose variable 
 *  24/01/2019 [HAR] tambah prose allowance fix jadi variable 
 *  20/02/2019 [HAR] other allowance tidak masuk ke perhitungan annual 
*/
#endregion

#region Rules
/*
 3. tunjangan cuti tahunan : 
- 1 dokumen utk 1 employee atau banyak employee ya ? 
- untuk tunjangan cuti besar juga gak ya ? kalau iya aturannya gmana ?
- untuk amountnya itungannya kemaren gmana ya ?
sori lama belum dikerjakan banyak yg lupa.
1 dokumen bisa untuk banyak employee pak,
didapatkan untuk cuti tahunan, cuti besar 1, dan cuti besar 2

aturannya: jika karyawan sudah mengambil x hari cuti, maka dia berhak mendapatkan uang cuti tersebut. x= parameter, bisa 0.

Jika karyawan sudah mendapatkan uang saku cuti, dia tidak mendapatkannya lagi sampai dengan periode cuti berikutnya.

amount:
cuti tahunan: 1x monthly salary 
cuti besar 1: 2x monthly salary
cuti besar 2: 1x monthly salary
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmAnnualLeaveAllowance : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string 
            mMenuCode = string.Empty, 
            mAccessInd = string.Empty, 
            mSQL = string.Empty, 
            mDocNo = string.Empty,
            mAnnualLeaveAllowanceCode = string.Empty,
            mMinDayTakenForLeaveAllowance = string.Empty;
        internal FrmAnnualLeaveAllowanceFind FrmFind;
        private string
            mMultiplierValueForAnnualLeave = string.Empty,
            mMultiplierValueForLongService1 = string.Empty,
            mMultiplierValueForLongService2 = string.Empty,
            mLongServiceLeaveCode = string.Empty,
            mLongServiceLeaveCode2 = string.Empty,
            mAnnualLeaveCode = string.Empty,
            mADCodeMeal = string.Empty,
            mADCodeTransport = string.Empty,
            mADCodeVariable = string.Empty,
            mAnnualLeaveAllowanceType = string.Empty;

        #endregion

        #region Constructor

        public FrmAnnualLeaveAllowance(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Annual Leave Allowance";
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                Sl.SetLueYr(LueYr, "");
                
                SetGrd();
                SetFormControl(mState.View);

                base.FrmLoad(sender, e);
                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 8;
            Sm.GrdHdrWithColWidth(
                Grd1,
                new string[] 
                {
                    //0
                    "DNo",

                    //1-5
                    "", 
                    "Employee's"+Environment.NewLine+"Code",
                    "",
                    "Employee's Name",
                    "Amount",

                    //6-7
                    "Leave Code",
                    "Payrun Code"
                },
                new int[] 
                { 
                    0, 
                    20, 100, 20, 250, 150, 
                    0, 130 
                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 5 }, 0);
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 4, 5, 7 });
            Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 3, 6 });
            Sm.GrdColButton(Grd1, new int[] { 1, 3 });
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, MeeCancelReason, LueYr, MeeRemark
                    }, true);
                    Grd1.ReadOnly = true;
                    ChkCancelInd.Properties.ReadOnly = true;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueYr, MeeRemark
                    }, false);
                    Grd1.ReadOnly = false;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeCancelReason, ChkCancelInd }, false);
                    MeeCancelReason.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, MeeCancelReason, TxtStatus, LueYr, MeeRemark
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtAmt }, 0);
            ChkCancelInd.Checked = false;
            ClearGrd();
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 5 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmAnnualLeaveAllowanceFind(this);
                Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Save Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "AnnualLeaveAllowance", "TblAnnualLeaveAllowanceHdr");
            decimal TotalAmt = decimal.Parse(TxtAmt.Text);
            string MainCurCode = Sm.GetParameter("MainCurCode");

            bool NoNeedApproval = !IsDocApprovalSettingExisted();

            var cml = new List<MySqlCommand>();

            cml.Add(SaveAnnualLeaveAllowanceHdr(DocNo, NoNeedApproval, TotalAmt));

            for (int r = 0; r < Grd1.Rows.Count - 1; r++)
                if(Sm.GetGrdStr(Grd1, r, 2).Length > 0) cml.Add(SaveAnnualLeaveAllowanceDtl(DocNo, r));
                
            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueYr, "Year") ||
                IsGrdEmpty() ||
                IsGrdExceedMaxRecords() ||
                IsEmployeeWithTheSameYrExisted()
                ;
        }

        private bool IsEmployeeWithTheSameYrExisted()
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string EmpCode = string.Empty, Filter = string.Empty;
            if (Grd1.Rows.Count >= 1)
            {
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, r, 2);
                    if (EmpCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(EmpCode=@EmpCode0" + r.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode0" + r.ToString(), EmpCode);
                    }
                }
            }
            if (Filter.Length > 0)
                Filter = " And (" + Filter + ") ";
            else
                Filter = " And 1=0 ";

            SQL.AppendLine("Select Concat(EmpName, ' (', EmpCode, ')')  From TblEmployee ");
            SQL.AppendLine("Where EmpCode In (");
            SQL.AppendLine("    Select T2.EmpCode ");
            SQL.AppendLine("    From TblAnnualLeaveAllowanceHdr T1 ");
            SQL.AppendLine("    Inner Join TblAnnualLeaveAllowanceDtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    And T1.Status In ('O', 'A') ");
            SQL.AppendLine("    And T1.Yr=@Yr ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("Limit 1;");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));

            EmpCode = Sm.GetValue(cm);

            if (EmpCode.Length > 0)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Employee : " + EmpCode + Environment.NewLine +
                    "This employee already processed in another document."
                    );
                return true;
            }
            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 employee.");
                return true;
            }

            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Employee entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsDocApprovalSettingExisted()
        {
            return Sm.IsDataExist(
                "Select 1 From TblDocApprovalSetting " +
                "Where UserCode Is Not Null And DocType='AnnualLeaveAllowance' Limit 1;"
                );
        }

        private MySqlCommand SaveAnnualLeaveAllowanceHdr(string DocNo, bool NoNeedApproval, decimal TotalAmt)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblAnnualLeaveAllowanceHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, Status, Yr, Amt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@DocNo, @DocDt, 'N', 'O', @Yr, @Amt, @Remark, @CreateBy, CurrentDateTime()); ");

            if (!NoNeedApproval)
            {
                SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
                SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @CreateBy, CurrentDateTime() ");
                SQL.AppendLine("From TblDocApprovalSetting T ");
                SQL.AppendLine("Where T.DocType='AnnualLeaveAllowance'; ");
            }

            SQL.AppendLine("Update TblAnnualLeaveAllowanceHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select DocNo From TblDocApproval ");
            SQL.AppendLine("    Where DocType='AnnualLeaveAllowance' ");
            SQL.AppendLine("    And DocNo=@DocNo ");
            SQL.AppendLine("    ); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
            Sm.CmParam<Decimal>(ref cm, "@Amt", TotalAmt);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveAnnualLeaveAllowanceDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblAnnualLeaveAllowanceDtl ");
            SQL.AppendLine("(DocNo, DNo, EmpCode, LeaveCode, Amt, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@DocNo, @DNo, @EmpCode, @LeaveCode, @Amt, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd1, Row, 5));
            Sm.CmParam<String>(ref cm, "@LeaveCode", Sm.GetGrdStr(Grd1, Row, 6));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsCancelledDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(CancelAnnualLeaveAllowance());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsCancelledDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsAnnualLeaveAllowanceNotCancelled() ||
                IsDataCancelledAlready() ||
                IsDataProcessedAlready();
        }

        private bool IsDataProcessedAlready()
        {
            return Sm.IsDataExist(
                "Select 1 From TblAnnualLeaveAllowanceDtl Where DocNo=@Param And PayrunCode Is Not Null;",
                TxtDocNo.Text,
                "This data already processed in payroll processing.");
        }

        private bool IsAnnualLeaveAllowanceNotCancelled()
        {
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this document.");
                return true;
            }
            return false;
        }

        private bool IsDataCancelledAlready()
        {
            return Sm.IsDataExist(
                "Select DocNo From TblAnnualLeaveAllowanceHdr Where (CancelInd='Y' Or Status='C') And DocNo=@Param;",
                TxtDocNo.Text,
                "This data already cancelled.");
        }

        private MySqlCommand CancelAnnualLeaveAllowance()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblAnnualLeaveAllowanceHdr Set ");
            SQL.AppendLine("    CancelInd='Y', CancelReason=@CancelReason, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N' And Status In ('O', 'A'); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowAnnualLeaveAllowanceHdr(DocNo);
                ShowAnnualLeaveAllowanceDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }

        }

        private void ShowAnnualLeaveAllowanceHdr(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("SELECT DocNo, DocDt, CancelReason, CancelInd, Yr, Amt, Remark, ");
            SQL.AppendLine("Case Status When 'A' Then 'Approved' When 'O' Then 'Outstanding' Else 'Cancelled' End As StatusDesc ");
            SQL.AppendLine("FROM TblAnnualLeaveAllowanceHdr WHERE DocNo=@DocNo;");
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[] 
                {
                    //0
                    "DocNo",
                    
                    //1-5
                    "DocDt", "CancelReason", "CancelInd", "Yr", "Remark", 

                    //6-7
                    "StatusDesc", "Amt"
                },
                 (MySqlDataReader dr, int[] c) =>
                 {
                     TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                     Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                     MeeCancelReason.EditValue = Sm.DrStr(dr, c[2]);
                     ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[3]), "Y");
                     Sm.SetLue(LueYr, Sm.DrStr(dr, c[4]));
                     MeeRemark.EditValue = Sm.DrStr(dr, c[5]);
                     TxtStatus.EditValue = Sm.DrStr(dr, c[6]);
                     TxtAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[7]), 0);
                }, true
             );
        }

        private void ShowAnnualLeaveAllowanceDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.EmpCode, B.EmpName, A.Amt, A.LeaveCode, A.PayrunCode ");
            SQL.AppendLine("From TblAnnualLeaveAllowanceDtl A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode = B.EmpCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo;  ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 

                    //1-5
                    "EmpCode", "EmpName", "Amt", "LeaveCode", "PayrunCode"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 5 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Grid Methods

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 1 && !Sm.IsLueEmpty(LueYr, "Year"))
            {
                e.DoDefault = false;
                Sm.FormShowDialog(new FrmAnnualLeaveAllowanceDlg(this, Sm.GetLue(LueYr)));
            }

            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmEmployee(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mEmpCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0)
            {
                Sm.GrdRemoveRow(Grd1, e, BtnSave);
            }
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
            ComputeTotalAmt();
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1 && !Sm.IsLueEmpty(LueYr, "Year"))
                Sm.FormShowDialog( new FrmAnnualLeaveAllowanceDlg(this, Sm.GetLue(LueYr)));

            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmEmployee(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mEmpCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        #endregion

        #region Additional Methods

        internal void ComputeAmt()
        {
            if(Grd1.Rows.Count > 1)
            {
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    if (Sm.GetGrdStr(Grd1, r, 2).Length > 0)
                    {
                        string LeaveCode = Sm.GetGrdStr(Grd1, r, 6);
                        string EmpCode = Sm.GetGrdStr(Grd1, r, 2);

                        decimal Amt = 0m;

                        decimal Salary = 0m;
                        decimal Meal = 0m;
                        decimal Transport = 0m;
                        decimal Variable = 0m;
                        decimal OtherAllowance = 0m;
                        decimal VariableNonFixed = 0m;

                        string Value = string.Empty;

                        Value = Sm.GetValue("Select IfNull(Amt, 0) From TblEmployeeSalary Where EmpCode=@Param And ActInd='Y' Order By StartDt Desc Limit 1;", EmpCode);
                        if (Value.Length > 0) Salary = decimal.Parse(Value);

                        Value = Sm.GetValue("Select IfNull(Amt, 0) From tblemployeeallowancededuction Where EmpCode=@Param1 And ADCode=@Param2 Order By StartDt Desc Limit 1;", EmpCode, mADCodeMeal, string.Empty);
                        if (Value.Length > 0) Meal = decimal.Parse(Value);

                        Value = Sm.GetValue("Select IfNull(Amt, 0) From tblemployeeallowancededuction Where EmpCode=@Param1 And ADCode=@Param2 Order By StartDt Desc Limit 1;", EmpCode, mADCodeTransport, string.Empty);
                        if (Value.Length > 0) Transport = decimal.Parse(Value);

                        Value = Sm.GetValue("Select IfNull(Amt, 0) From tblemployeeallowancededuction Where EmpCode=@Param1 And ADCode=@Param2 Order By StartDt Desc Limit 1;", EmpCode, mADCodeVariable, string.Empty);
                        if (Value.Length > 0) Variable = decimal.Parse(Value);

                        Value = Sm.GetValue("Select IfNull(SUM(Amt), 0) From tblemployeeallowancededuction Where EmpCode=@Param1 And find_in_set(AdCode, (select parvalue from tblparameter Where parCode ='AllowanceCodeVariable'))>0 Order By StartDt Desc Limit 1;", EmpCode, string.Empty, string.Empty);
                        if (Value.Length > 0) VariableNonFixed = decimal.Parse(Value);


                        Value = Sm.GetValue("Select IfNull(SUM(Amt), 0) From tblemployeeallowancededuction A " +
                                            "Inner Join TblAllowanceDeduction B On A.AdCode = B.AdCode And B.AdType = 'A' " +
                                            "Where A.EmpCode=@Param1 And A.ADCode Not In (@Param2,@Param3, '" + mADCodeVariable + "') Order By StartDt Desc Limit 1; ",
                                            EmpCode, mADCodeMeal, mADCodeTransport);

                        if (Value.Length > 0) OtherAllowance = decimal.Parse(Value) - VariableNonFixed;

                        if(mAnnualLeaveCode.Length > 0 && mLongServiceLeaveCode.Length > 0 && mLongServiceLeaveCode2.Length > 0)
                        {
                            if(LeaveCode == mAnnualLeaveCode)
                            {
                                if (mAnnualLeaveAllowanceType == "0")
                                {
                                    Amt = (mMultiplierValueForAnnualLeave.Length > 0) ? decimal.Parse(mMultiplierValueForAnnualLeave) * Salary : 0m;
                                }
                                else
                                {
                                    Amt = (mMultiplierValueForAnnualLeave.Length > 0) ? decimal.Parse(mMultiplierValueForAnnualLeave) * Salary : Salary + (20 * Meal) + (20 * Transport) + (20* Variable) +  (20*VariableNonFixed);
                                }
                            }
                            else if(LeaveCode == mLongServiceLeaveCode)
                            {
                                Amt = (mMultiplierValueForLongService1.Length > 0) ? decimal.Parse(mMultiplierValueForLongService1) * Salary : 0m;
                            }
                            else if(LeaveCode == mLongServiceLeaveCode2)
                            {
                                Amt = (mMultiplierValueForLongService2.Length > 0) ? decimal.Parse(mMultiplierValueForLongService2) * Salary : 0m;
                            }
                        }

                        Grd1.Cells[r, 5].Value = Amt;
                    }
                }

                ComputeTotalAmt();
            }
        }

        private void ComputeTotalAmt()
        {
            decimal TotalAmt = 0m;

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 2).Length > 0)
                {
                    TotalAmt += Sm.GetGrdDec(Grd1, r, 5);
                }
            }

            TxtAmt.EditValue = Sm.FormatNum(TotalAmt, 0);
        }

        internal string GetSelectedEmployee()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length != 0)
                        SQL += "##" + Sm.GetGrdStr(Grd1, Row, 2) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        private void GetParameter()
        {
            mMinDayTakenForLeaveAllowance = Sm.GetParameter("MinDayTakenForLeaveAllowance");
            mAnnualLeaveAllowanceCode = Sm.GetParameter("AnnualLeaveAllowanceCode");
            mMultiplierValueForAnnualLeave = Sm.GetParameter("MultiplierValueForAnnualLeave");
            mMultiplierValueForLongService1 = Sm.GetParameter("MultiplierValueForLongService1");
            mMultiplierValueForLongService2 = Sm.GetParameter("MultiplierValueForLongService2");
            mAnnualLeaveCode = Sm.GetParameter("AnnualLeaveCode");
            mLongServiceLeaveCode = Sm.GetParameter("LongServiceLeaveCode");
            mLongServiceLeaveCode2 = Sm.GetParameter("LongServiceLeaveCode2");
            mADCodeMeal = Sm.GetParameter("ADCodeMeal");
            mADCodeTransport = Sm.GetParameter("ADCodeTransport");
            mADCodeVariable = Sm.GetParameter("ADCodeVariable");
            mAnnualLeaveAllowanceType = Sm.GetParameter("AnnualLeaveAllowanceType");

        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Event

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        private void LueYr_EditValueChanged(object sender, EventArgs e)
        {
            ClearGrd();
            ComputeTotalAmt();
        }

        #endregion

        #endregion

    }
}
