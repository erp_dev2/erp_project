﻿#region Update
/*
    13/10/2017 [TKG] bug saat filter dengan customer
    13/04/2018 [TKG] filter by site
    07/08/2018 [HAR] Bisa ambil data dari SO contract untuk VIR
    14/08/2018 [WED] tambah kolom dan filter Customer PO# dari SO
    19/11/2019 [WED/IMS] customer PO# dari SO Contract juga dimunculkan
    25/11/2021 [RIS/AMKA] Mennambah kolom departemen dan memfilter bedasarkan group departement dengan param IsFilterByDept
    09/12/2021 [VIN/ALL] Bug: tambah param IsARDownpaymentUseDepartment untuk menampilkan department
    11/02/2022 [ISD/PRODUCT] tambah filter VR#
    05/07/2022 [VIN/ALL] bug IsFilterByDept
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmARDownpaymentFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmARDownpayment mFrmParent;
        private string mSQL = string.Empty;
        private bool mIsARDownpaymentShowSalesInvoice = false;
        
        #endregion

        #region Constructor

        public FrmARDownpaymentFind(FrmARDownpayment FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                Sl.SetLueCtCode(ref LueCtCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mIsARDownpaymentShowSalesInvoice = Sm.GetParameterBoo("IsARDownpaymentShowSalesInvoice");
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelInd, A.SODocNo, IfNull(C.CtName, D.CtName) As CtName, A.CurCode, A.Amt, A.Remark, A.VoucherRequestDocNo, E.VoucherDocNo, ");
            SQL.AppendLine("Case A.Status When 'O' Then 'Outstanding' When 'A' Then 'Approved' When 'C' Then 'Cancelled' End As StatusDesc, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt, ");
            if (mIsARDownpaymentShowSalesInvoice)
                SQL.AppendLine("F.SalesInvoiceDocNo ");
            else
                SQL.AppendLine("Null As SalesInvoiceDocNo ");

            if (mFrmParent.mGenerateCustomerCOAFormat == "1") SQL.AppendLine(", B.CtPONo ");
            else SQL.AppendLine(", B.PONo As CtPONo ");
            SQL.AppendLine(", G.DeptName AS DeptName ");
            SQL.AppendLine("From TblARDownpayment A ");
            if (mFrmParent.mGenerateCustomerCOAFormat == "1")
            {
                SQL.AppendLine("Left Join TblSOHdr B On A.SODocNo=B.DocNo ");
            }
            else
            {
                SQL.AppendLine("Left Join TblSOContractHdr B On A.SODocNo=B.DocNo ");
            }
            SQL.AppendLine("Left Join TblCustomer C On B.CtCode=C.CtCode ");
            SQL.AppendLine("Left Join TblCustomer D On A.CtCode=D.CtCode ");
            SQL.AppendLine("Left Join TblVoucherRequestHdr E On A.VoucherRequestDocNo=E.DocNo ");

            if (mIsARDownpaymentShowSalesInvoice)
            {
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select T.SODocNo, ");
                SQL.AppendLine("    Group_Concat(Distinct T.DocNo Order By T.DocNo Separator ', ') As SalesInvoiceDocNo ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select T4.SODocNo, T1.DocNo ");
                SQL.AppendLine("        From TblSalesInvoiceHdr T1 ");
                SQL.AppendLine("        Inner Join TblSalesInvoiceDtl T2 On T1.DocNo=T2.DocNo ");
                SQL.AppendLine("        Inner Join TblDOCt2Hdr T3 On T2.DOCtDocNo=T3.DocNo And T3.DRDocNo is Not Null ");
                SQL.AppendLine("        Inner Join TblDRDtl T4 On T3.DRDocNo=T4.DocNo ");
                SQL.AppendLine("            And Exists( ");
                SQL.AppendLine("                Select 1 From TblARDownpayment ");
                SQL.AppendLine("                Where SODocNo Is Not Null ");
                SQL.AppendLine("                And SODocNo=T4.SODocNo ");
                SQL.AppendLine("                And DocDt Between @DocDt1 And @DocDt2 ");
                SQL.AppendLine("            ) ");
                SQL.AppendLine("        Where T1.CancelInd='N' ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("        Select T4.SODocNo, T1.DocNo ");
                SQL.AppendLine("        From TblSalesInvoiceHdr T1 ");
                SQL.AppendLine("        Inner Join TblSalesInvoiceDtl T2 On T1.DocNo=T2.DocNo ");
                SQL.AppendLine("        Inner Join TblDOCt2Hdr T3 On T2.DOCtDocNo=T3.DocNo And T3.PLDocNo is Not Null ");
                SQL.AppendLine("        Inner Join TblPLDtl T4 On T3.PLDocNo=T4.DocNo ");
                SQL.AppendLine("            And Exists( ");
                SQL.AppendLine("                Select 1 From TblARDownpayment ");
                SQL.AppendLine("                Where SODocNo Is Not Null ");
                SQL.AppendLine("                And SODocNo=T4.SODocNo ");
                SQL.AppendLine("                And DocDt Between @DocDt1 And @DocDt2 ");
                SQL.AppendLine("            ) ");
                SQL.AppendLine("        Where T1.CancelInd='N' ");
                SQL.AppendLine("    ) T Group By T.SODocNo ");
                SQL.AppendLine(") F On A.SODocNo=F.SODocNo ");
            }
            if(mFrmParent.mIsARDownpaymentUseDepartment)
                SQL.AppendLine("Inner Join TblDepartment G on A.DeptCode = G.DeptCode ");
            else
                SQL.AppendLine("Left Join TblDepartment G on A.DeptCode = G.DeptCode ");

            SQL.AppendLine("Where A.DocDt Between @DocDt1 And @DocDt2 ");
            if (mFrmParent.mIsFilterByDept)
            {
                SQL.AppendLine("And (A.DeptCode IS NULL OR Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupDepartment ");
                SQL.AppendLine("    Where DeptCode=G.DeptCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    )) ");
            }
            
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 22;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Document#", 
                    "Date",
                    "Cancel",
                    "Status",
                    "Customer",

                    //6-10
                    "SO#",
                    "",
                    "Customer PO#",
                    "Currency", 
                    "Amount",
                    
                    //11-15
                    "Remark",
                    "Voucher"+Environment.NewLine+"Request#",
                    "Voucher#",
                    "Department",
                    "Estimated"+Environment.NewLine+"Sales Invoice#",
                    
                    
                    //16-20
                    "Created"+Environment.NewLine+"By",
                    "Created"+Environment.NewLine+"Date", 
                    "Created"+Environment.NewLine+"Time", 
                    "Last"+Environment.NewLine+"Updated By", 
                    "Last"+Environment.NewLine+"Updated Date", 

                    //21
                    "Last"+Environment.NewLine+"Updated Time"
                },
                new int[] 
                {
                    //0
                    50,
                    //1-5
                    130, 100, 80, 80, 250, 
                    //6-10
                    130, 29, 200, 80, 100, 
                    //11-15
                    200,130, 130, 150, 500,  
                    //16-20
                    100, 100, 100, 100, 100, 
                    //21
                    100
                }
            );
            Sm.GrdColCheck(Grd1, new int[] { 3 });
            Sm.GrdColButton(Grd1, new int[] { 7 });
            Sm.GrdFormatDec(Grd1, new int[] { 10 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2, 17, 20 });
            Sm.GrdFormatTime(Grd1, new int[] { 18, 21 });
            Sm.GrdColInvisible(Grd1, new int[] { 16, 17, 18, 19, 20, 21 }, false);
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 16, 17, 18, 19, 20, 21 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCode), "B.CtCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtSODocNo.Text, "A.SODocNo", false);
                if (mFrmParent.mGenerateCustomerCOAFormat == "1") Sm.FilterStr(ref Filter, ref cm, TxtCtPONo.Text, "B.CtPONo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtVRDocNo.Text, new string[] { "A.VoucherRequestDocno" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.CreateDt Desc;",
                        new string[]
                        {

                            //0
                            "DocNo", 

                            //1-5
                            "DocDt", "CancelInd", "StatusDesc", "CtName", "SODocNo", 
                            
                            //6-10
                            "CtPONo", "CurCode", "Amt", "Remark", "VoucherRequestDocNo", 
                            
                            //11-15
                            "VoucherDocNo", "DeptName",  "SalesInvoiceDocNo", "CreateBy", "CreateDt", 

                            //16-17
                            "LastUpBy", "LastUpDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 14);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 17, 15);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 18, 15);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 16);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 20, 17);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 21, 17);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 7 && Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmSO2(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 7 && Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length != 0)
            {
                var f = new FrmSO2(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                f.ShowDialog();
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");
        }

        private void TxtSODocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkSODocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "SO#");
        }

        private void TxtCtPONo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkCtPONo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Customer PO#");
        }

        private void TxtVRDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkVRDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "VR#");
        }

        #endregion

        #endregion
    }
}
