﻿#region Update
/*
    13/01/2022 [RDA/PRODUCT] New apps
    07/02/2022 [MYA/PRODUCT] Penyesuaian rumus outstanding qty pada saat memilih DO to Department pada menu Master Asset
    16/02/2022 [IBL/PRODUCT] BUG: DODept yg cancel masih ketarik
    18/08/2022 [MYA/PRODUCT] Membuat validasi Departement, Cost Center, Item's Category dan COA berdasarkan apa yang telah terpasang di Group (system tools)
    10/11/2022 [MAU/BBT] Membuat validasi DO to Department yang dapat ditarik ke master asset (Asset Project) 
    02/02/2023 [WED/BBT] tambah informasi kolom Remark dari DODeptDtl
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmAsset2Dlg5 : RunSystem.FrmBase4
    {
        #region Field


        private FrmAsset2 mFrmParent;
        private string mSQL = string.Empty;
        internal List<FrmAsset2.DoDeptItem> ListDoDeptItem = new List<FrmAsset2.DoDeptItem>();

        #endregion

        #region Constructor

        public FrmAsset2Dlg5(FrmAsset2 FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                this.Text = "List of DO to Department";
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                ListDoDeptItem = mFrmParent.ListDoDeptItem;
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 14;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[]
                {
                    //0
                    "No.",

                    //1-5
                    "",
                    "Document#",
                    "DNo",
                    "DO Date",
                    "Item Code",
                    
                    //6-10
                    "Item's Name",
                    "UoM",
                    "DO Quantity",
                    "Outstanding Qty",
                    "Currency",                    

                    //11-13
                    "Unit Price",
                    "Amount", 
                    "Remark"
                },
                new int[]
                {
                    //0
                    50,
                    
                    //1-5
                    20, 180, 50, 100, 100, 

                    //6-10
                    180, 50, 100, 120, 80, 

                    //11-13
                    120, 120, 200
                }
            );
            Sm.GrdColInvisible(Grd1, new int[] { 0, 3, 5 }, false);
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdFormatDec(Grd1, new int[] { 8, 9, 11, 12 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 4 });
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 5 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT A.DocNo, B.DNo, A.DocDt, B.ItCode, C.ItName, D.UomName, B.Remark, ");
            SQL.AppendLine("B.Qty, E.CurCode, E.UPrice, IfNull(B.Qty, 0) - ifnull(F.QtyUsed, 0) OutstandingQty ");
            SQL.AppendLine("FROM TblDODeptHdr A ");
            SQL.AppendLine("INNER JOIN TblDODeptDtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("    And B.CancelInd = 'N' ");
            if (mFrmParent.mIsFilterByDept)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupDepartment ");
                SQL.AppendLine("    Where DeptCode=A.DeptCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode='" + Gv.CurrentUserCode + "' ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("INNER JOIN TblItem C ON B.ItCode = C.ItCode ");
            SQL.AppendLine("INNER JOIN TblUOM D ON C.InventoryUOMCode = D.UomCode ");
            SQL.AppendLine("INNER JOIN TblStockPrice E ON B.Source = E.Source ");
            SQL.AppendLine("Left Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select DODDocNo, DODDNo, Sum(IfNull(QtyUsed,0.00)) As QtyUsed ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("        Select DODDocNo, DODDNo, Qty As QtyUsed ");
            SQL.AppendLine("        From TblReconditionAssetHdr T1 ");
            SQL.AppendLine("        Inner Join TblReconditionAssetDtl T2 On T1.DocNo = T2.DocNo ");
            SQL.AppendLine("        And T1.CancelInd = 'N' ");
            SQL.AppendLine("        And T1.Status = 'A' ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select T2.RecvVdDocNo As DODDocNo, T2.RecvVdDNo As DODDNo, T2.Qty As QtyUsed ");
            SQL.AppendLine("        From TblAsset T1  ");
            SQL.AppendLine("        Inner Join TblAssetDtl T2 On T1.AssetCode = T2.AssetCode And T1.ActiveInd = 'Y' ");
            SQL.AppendLine("    )Tbl Group By Tbl.DODDocNo, Tbl.DODDNo ");
            SQL.AppendLine(")F On A.DocNo = F.DODDocNo And B.DNo = F.DODDNo ");

            SQL.AppendLine(" Left Join tblcostcenter G ON A.CCCode = G.CCCode   ");
            
            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            if (
                   Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                   Sm.IsDteEmpty(DteDocDt2, "End date") ||
                   Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                   ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " Where A.DocDt Between @DocDt1 and @DocDt2 And G.AssetProjectInd = 'Y' ";
                Filter += "And IfNull(B.Qty, 0) - ifnull(F.QtyUsed, 0) > 0.00 ";
                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "B.ItCode", "C.ItName", "C.ItCodeInternal", "C.ForeignName" });

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By A.DocNo;",
                    new string[]
                    { 
                        //0
                        "DocNo",

                        //1-5
                        "DNo", "DocDt", "ItCode", "ItName", "UomName", 
                        
                        //6-10
                        "Qty", "CurCode", "UPrice", "OutstandingQty", "Remark"

                     },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1; //no
                        Grd.Cells[Row, 1].Value = false; //checklist
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0); // docno
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1); //dno
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 4, 2);  //date
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3); //itcode
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4); //itname
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5); //uomname
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 6); //doqty
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 9); //outstandingQty
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7); //curcode
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 8); //uprice
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 10); //remark
                        Grd.Cells[Row, 12].Value = Sm.GetGrdDec(Grd1, Row, 9) * Sm.GetGrdDec(Grd1, Row, 11); //amt
                    }, true, false, false, false
                );
                mFrmParent.ListDoDeptItem.Clear();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && Sm.GetGrdStr(Grd1, Row, 2).Length > 0 && !IsRecvVdAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 0, Grd1, Row2, 3); //dno
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 2, Grd1, Row2, 2); //docno
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 5, Grd1, Row2, 4); //date
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 6, Grd1, Row2, 5); //itcode
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 7, Grd1, Row2, 6); //itname
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 8, Grd1, Row2, 8); //doqty
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 9, Grd1, Row2, 7); //uom
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 10, Grd1, Row2, 10); //currency
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 15, Grd1, Row2, 11); //uprice
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 18, Grd1, Row2, 9); //outstanding qty
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 19, Grd1, Row2, 13); //remark

                        mFrmParent.Grd1.Rows.Add();
                        mFrmParent.Grd1.Cells[Row1, 11].Value = Sm.GetGrdDec(mFrmParent.Grd1, Row1, 8) * Sm.GetGrdDec(mFrmParent.Grd1, Row1, 15);
                        mFrmParent.OutstandingQtyDlg = Sm.GetGrdDec(Grd1, Row2, 9);
                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 8, 11, 13, 14, 15, 16, 17, 18 });
                    }
                }
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 item.");

            mFrmParent.ComputeRate();
        }

        #region Additional Method

        private bool IsRecvVdAlreadyChosen(int Row)
        {
            for (int Index = 0; Index < mFrmParent.Grd1.Rows.Count - 1; Index++)
            {
                if (
                    Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd1, Index, 2), Sm.GetGrdStr(Grd1, Row, 2)) &&
                    Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd1, Index, 0), Sm.GetGrdStr(Grd1, Row, 3))
                    )
                    return true;
            }
            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }


        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt1).Length == 0) DteDocDt1.EditValue = DteDocDt2.EditValue;
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        #endregion

        #endregion

    }
}
