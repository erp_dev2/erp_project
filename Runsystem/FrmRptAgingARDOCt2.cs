﻿#region Update
/*
    18/07/2022 [SET/PRODUCT] Menu Reporting baru ref RptAgingARDOCt
    05/08/2022 [SET/PRODUCT] Feedback : belum terfilter berdasar DODt
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

#endregion

namespace RunSystem
{
    public partial class FrmRptAgingARDOCt2 : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mDocTitle = string.Empty;
        private bool
            mIsDOCtAmtRounded = false,
            mIsSalesContractEnabled = false,
            mIsRptAgingARDOCtUseDODtFilter = false,
            mIsRptAgingARDOCtShowFulfilledInvoice = false,
            mIsFilterByCtCt = false,
            mIsFilterByWarehouse = false;

        #endregion

        #region Constructor

        public FrmRptAgingARDOCt2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                //Sl.SetLueCtCode(ref LueCtCode);
                //Sl.SetLueCtCtCode(ref LueCtCtCode);
                Sl.SetLueItCtCode(ref LueCtItCode);
                Sl.SetLueCtCode(ref LueCtCode, string.Empty, mIsFilterByCtCt ? "Y" : "N");
                Sl.SetLueCtCtCode(ref LueCtCtCode, string.Empty, mIsFilterByCtCt ? "Y" : "N");

                base.FrmLoad(sender, e);
                //if (!mIsRptAgingARDOCtUseDODtFilter)
                //    label6.Visible = label5.Visible = DteDODt1.Visible = DteDODt2.Visible = ChkDODt.Visible = false;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            //mDocTitle = Sm.GetParameter("DocTitle");
            //mIsDOCtAmtRounded = Sm.GetParameterBoo("IsDOCtAmtRounded");
            //mIsSalesContractEnabled = Sm.GetParameterBoo("IsSalesContractEnabled");
            mIsRptAgingARDOCtUseDODtFilter = Sm.GetParameterBoo("IsRptAgingARDOCtUseDODtFilter");

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'DocTitle', 'IsDOCtAmtRounded', 'IsSalesContractEnabled', 'IsRptAgingARDOCtShowFulfilledInvoice', ");
            SQL.AppendLine("'IsFilterByCtCt', 'IsFilterByWarehouse' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsDOCtAmtRounded": mIsDOCtAmtRounded = ParValue == "Y"; break;
                            case "IsSalesContractEnabled": mIsSalesContractEnabled = ParValue == "Y"; break;
                            case "IsRptAgingARDOCtShowFulfilledInvoice": mIsRptAgingARDOCtShowFulfilledInvoice = ParValue == "Y"; break;
                            case "IsFilterByCtCt": mIsFilterByCtCt = ParValue == "Y"; break;
                            case "IsFilterByWarehouse": mIsFilterByWarehouse = ParValue == "Y"; break;

                            //string
                            case "DocTitle": mDocTitle = ParValue; break;
                        }
                    }
                }
                dr.Close();
            }
        }

        private string GetSQL(ref MySqlCommand cm)
        {
            var SQL = new StringBuilder();
            string
                Filter1 = " ",
                Filter2 = string.Empty,
                Filter3 = " ";
            var a = Sm.GetDte(DteBalAsOfDt);

            Sm.FilterStr(ref Filter2, ref cm, Sm.GetLue(LueCtCode), "T.CtCode", true);
            Sm.FilterDt(ref Filter2, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "T.DueDt");
            Sm.FilterDt(ref Filter2, ref cm, Sm.GetDte(DteDODt1), Sm.GetDte(DteDODt2), "T.DocDt");
            Sm.FilterStr(ref Filter3, ref cm, Sm.GetLue(LueCtItCode), "Q.ItCtCode", true);
            Sm.FilterStr(ref Filter1, ref cm, Sm.GetLue(LueCtCtCode), "T2.CtCtCode", true);
            Sm.CmParamDt(ref cm, "@BalAsOfDt", Sm.GetDte(DteBalAsOfDt));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            SQL.AppendLine("Select T1.Period, T2.CtName, T1.CurCode, T1.DocNo, T1.WhsCode, ");
            SQL.AppendLine("T1.AmtBeforeTax, T1.TaxAmt, T1.AmtAfterTax, T1.DueDt, T1.AgingDays, T1.WhsName, T1.EntName, T1.Remark, T1.DocType, T3.CtCtName, ");
            SQL.AppendLine("T1.InvoiceDocNo, T1.InvoiceAmt, T1.BalanceAmt, T1.ProjectCode ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select T.Period, T.DueDt, IfNull(T.AgingDays, 0.00) AgingDays, T.CtCode, T.DocNo, T.DocDt, T.CurCode, T.WhsCode, ");
            SQL.AppendLine("    Sum(T.Qty*T.UPriceBeforeTax) As AmtBeforeTax, ");
            SQL.AppendLine("    Sum(T.Qty*T.TaxAmt) As TaxAmt, ");
            SQL.AppendLine("    Sum((T.Qty*T.UPriceBeforeTax)+(T.Qty*T.TaxAmt)) As AmtAfterTax, ");
            SQL.AppendLine("    T.WhsName, T.EntName, T.Remark, T.DocType, ");
            SQL.AppendLine("    T.InvoiceDocNo, Sum(T.InvoiceAmt) InvoiceAmt, Sum(T.BalanceAmt) BalanceAmt, T.ProjectCode ");
            SQL.AppendLine("    From ( ");

            //Local
            SQL.AppendLine("        Select Concat(Left(A.DocDt, 4), '-',Substring(A.DocDt, 5, 2)) As Period, A.WhsCode, ");
            SQL.AppendLine("        Date_Format(Date_Add(A.DocDt, Interval K.PtDay Day), '%Y%m%d') As DueDt, ");
            SQL.AppendLine("        DateDiff(Left(currentdatetime(), 8), Date_Format(Date_Add(A.DocDt, Interval K.PtDay Day), '%Y%m%d')) As AgingDays, ");
            SQL.AppendLine("        A.CtCode, A.DocNo, H.CurCode, ");
            SQL.AppendLine("        If(C.QtyInventory=0.00, 0.00, (B.Qty/C.QtyInventory)*C.Qty) As Qty, ");
            SQL.AppendLine("        (G.UPrice-(G.UPrice*0.01*IfNull(J.DiscRate, 0.00))) As UPriceBeforeTax, ");
            SQL.AppendLine("        ((G.UPrice-(G.UPrice*0.01*IfNull(J.DiscRate, 0.00)))*0.01*E.TaxRate) As TaxAmt, ");
            SQL.AppendLine("        L.WhsName, O.entName, A.Remark, '2' As DocType, A.DocDt ");
            SQL.AppendLine("        , Null As InvoiceDocNo, 0.00 As InvoiceAmt, 0.00 As BalanceAmt, Null As ProjectCode ");
            SQL.AppendLine("        From TblDOCt2Hdr A ");
            SQL.AppendLine("        Inner Join TblDOCt2Dtl2 B On A.DocNo=B.DocNo And B.Qty>0 And B.ProcessInd='O' ");
            SQL.AppendLine("        Inner Join TblDRDtl C On A.DRDocNo=C.DocNo And B.DRDNo=C.DNo ");
            SQL.AppendLine("        Inner Join TblSOHdr D On C.SODocNo=D.DocNo ");
            SQL.AppendLine("        Inner Join TblSODtl E On C.SODocNo=E.DocNo And C.SODNo=E.DNo ");
            SQL.AppendLine("        Inner Join TblCtQtHdr F On D.CtQtDocNo=F.DocNo ");
            SQL.AppendLine("        Inner Join TblCtQtDtl G On D.CtQtDocNo=G.DocNo And E.CtQtDNo=G.DNo ");
            SQL.AppendLine("        Inner Join TblItemPriceHdr H On G.ItemPriceDocNo=H.DocNo ");
            SQL.AppendLine("        Inner Join TblItemPriceDtl I On G.ItemPriceDocNo=I.DocNo And G.ItemPriceDNo=I.DNo ");
            SQL.AppendLine("        Left Join TblSOQuotPromoItem J On D.SOQuotPromoDocNo=J.DocNo And I.ItCode=J.ItCode ");
            SQL.AppendLine("        Left Join TblPaymentTerm K On F.PtCode=K.PtCode ");
            SQL.AppendLine("        Inner Join TblWarehouse L On A.WhsCode = L.WhsCode ");
            SQL.AppendLine("        Left Join TblCostcenter M On L.CCCode = M.CCCode ");
            SQL.AppendLine("        left Join TblProfitcenter N On M.ProfitCenterCode = N.ProfitcenterCode ");
            SQL.AppendLine("        left Join TblEntity O On N.EntCode = O.EntCode ");
            SQL.AppendLine("        INNER JOIN tbldoct2dtl P ON A.DocNo = P.DocNo ");
            SQL.AppendLine("        INNER JOIN tblitem Q ON P.ItCode = Q.ItCode ");
            SQL.AppendLine("        INNER JOIN tbldoct2dtl R ON A.DocNo = R.DocNo ");
            SQL.AppendLine("        LEFT JOIN tblsalesinvoicedtl S ON R.DocNo = S.DOCtDocNo AND R.DNo = S.DOCtDNo ");
            SQL.AppendLine("        LEFT JOIN tblsalesinvoicehdr T ON S.DocNo = T.DocNo ");
            SQL.AppendLine("        Where A.DRDocNo IS Not Null ");
            SQL.AppendLine(Filter3);

            SQL.AppendLine("        Union All ");

            //Oversea
            SQL.AppendLine("        Select Concat(Left(A.DocDt, 4), '-',Substring(A.DocDt, 5, 2)) As Period, A.WhsCode, ");
            SQL.AppendLine("        Date_Format(Date_Add(A.DocDt, Interval K.PtDay Day), '%Y%m%d') As DueDt, ");
            SQL.AppendLine("        DateDiff(Left(currentdatetime(), 8), Date_Format(Date_Add(A.DocDt, Interval K.PtDay Day), '%Y%m%d')) As AgingDays, ");
            SQL.AppendLine("        A.CtCode, A.DocNo, H.CurCode, ");
            SQL.AppendLine("        If(C.QtyInventory=0.00, 0.00, (B.Qty/C.QtyInventory)*C.Qty) As Qty, ");
            SQL.AppendLine("        (G.UPrice-(G.UPrice*0.01*IfNull(J.DiscRate, 0.00))) As UPriceBeforeTax, ");
            SQL.AppendLine("        ((G.UPrice-(G.UPrice*0.01*IfNull(J.DiscRate, 0.00)))*0.01*E.TaxRate) As TaxAmt, ");
            SQL.AppendLine("        L.WhsName, O.entName, A.Remark, '2' As DocType, A.DocDt ");
            SQL.AppendLine("        , Null As InvoiceDocNo, 0.00 As InvoiceAmt, 0.00 As BalanceAmt, Null As ProjectCode ");
            SQL.AppendLine("        From TblDOCt2Hdr A ");
            SQL.AppendLine("        Inner Join TblDOCt2Dtl3 B On A.DocNo=B.DocNo And B.Qty>0 And B.ProcessInd='O' ");
            SQL.AppendLine("        Inner Join TblPLDtl C On A.PLDocNo=C.DocNo And B.PLDNo=C.DNo ");
            SQL.AppendLine("        Inner Join TblSOHdr D On C.SODocNo=D.DocNo ");
            SQL.AppendLine("        Inner Join TblSODtl E On C.SODocNo=E.DocNo And C.SODNo=E.DNo ");
            SQL.AppendLine("        Inner Join TblCtQtHdr F On D.CtQtDocNo=F.DocNo ");
            SQL.AppendLine("        Inner Join TblCtQtDtl G On D.CtQtDocNo=G.DocNo And E.CtQtDNo=G.DNo ");
            SQL.AppendLine("        Inner Join TblItemPriceHdr H On G.ItemPriceDocNo=H.DocNo ");
            SQL.AppendLine("        Inner Join TblItemPriceDtl I On G.ItemPriceDocNo=I.DocNo And G.ItemPriceDNo=I.DNo ");
            SQL.AppendLine("        Left Join TblSOQuotPromoItem J On D.SOQuotPromoDocNo=J.DocNo And I.ItCode=J.ItCode ");
            SQL.AppendLine("        Left Join TblPaymentTerm K On F.PtCode=K.PtCode ");
            SQL.AppendLine("        Inner Join TblWarehouse L On A.WhsCode = L.WhsCode ");
            SQL.AppendLine("        Left Join TblCostcenter M On L.CCCode = M.CCCode ");
            SQL.AppendLine("        left Join TblProfitcenter N On M.ProfitCenterCode = N.ProfitcenterCode ");
            SQL.AppendLine("        left Join TblEntity O On N.EntCode = O.EntCode  ");
            SQL.AppendLine("        INNER JOIN tbldoct2dtl P ON A.DocNo = P.DocNo ");
            SQL.AppendLine("        INNER JOIN tblitem Q ON P.ItCode = Q.ItCode ");
            SQL.AppendLine("        INNER JOIN tbldoct2dtl R ON A.DocNo = R.DocNo ");
            SQL.AppendLine("        LEFT JOIN tblsalesinvoicedtl S ON R.DocNo = S.DOCtDocNo AND R.DNo = S.DOCtDNo ");
            SQL.AppendLine("        LEFT JOIN tblsalesinvoicehdr T ON S.DocNo = T.DocNo ");
            SQL.AppendLine("        Where A.PLDocNo IS Not Null ");
            SQL.AppendLine(Filter3);

            // Sales Contract
            if (mIsSalesContractEnabled)
            {
                SQL.AppendLine("        Union All ");

                SQL.AppendLine("        Select Concat(Left(A.DocDt, 4), '-',Substring(A.DocDt, 5, 2)) As Period, A.WhsCode, ");
                SQL.AppendLine("        Date_Format(Date_Add(A.DocDt, Interval G.PtDay Day), '%Y%m%d') As DueDt, ");
                SQL.AppendLine("        DateDiff(Left(currentdatetime(), 8), Date_Format(Date_Add(A.DocDt, Interval G.PtDay Day), '%Y%m%d')) As AgingDays, ");
                SQL.AppendLine("        A.CtCode, A.DocNo, E.CurCode, ");
                SQL.AppendLine("        If(C.QtyInventory=0.00, 0.00, (B.Qty/C.QtyInventory)*C.Qty) As Qty, ");
                SQL.AppendLine("        If(E.TaxInd = 'N', F.UPrice, F.UPrice/1.1) UPriceBeforeTax, ");
                SQL.AppendLine("        If(E.TaxInd = 'N', 0.00, F.UPrice - (F.UPrice/1.1)) TaxAmt, ");
                SQL.AppendLine("        H.WhsName, K.EntName, A.Remark, '3' As DocType, A.DocDt ");
                SQL.AppendLine("        , Null As InvoiceDocNo, 0.00 As InvoiceAmt, 0.00 As BalanceAmt, Null As ProjectCode ");
                SQL.AppendLine("        From TblDOCt2Hdr A ");
                SQL.AppendLine("        Inner Join TblDOCt2Dtl2 B On A.DocNo=B.DocNo And B.Qty>0 And B.ProcessInd='O' ");
                SQL.AppendLine("        Inner Join TblDRDtl C On A.DRDocNo=C.DocNo And B.DRDNo=C.DNo And C.SCDocNo Is Not Null ");
                SQL.AppendLine("        Inner Join TblSalesContract D On C.SCDocNo = D.DocNo ");
                SQL.AppendLine("        Inner Join TblSalesMemoHdr E On D.SalesMemoDocNo = E.DocNo ");
                SQL.AppendLine("        Inner Join TblSalesMemoDtl F On E.DocNo = F.DocNo ");
                SQL.AppendLine("        Left Join TblPaymentTerm G On D.PtCode = G.PtCode ");
                SQL.AppendLine("        Inner Join TblWarehouse H On A.WhsCode = H.WhsCode ");
                SQL.AppendLine("        Left Join TblCostCenter I On H.CCCode = I.CCCode ");
                SQL.AppendLine("        Left Join TblProfitCenter J On I.ProfitCenterCode = J.ProfitCenterCode ");
                SQL.AppendLine("        Left Join TblEntity K On J.EntCode = K.EntCode ");
                SQL.AppendLine("        INNER JOIN tbldoct2dtl P ON A.DocNo = P.DocNo ");
                SQL.AppendLine("        INNER JOIN tblitem Q ON P.ItCode = Q.ItCode ");
                SQL.AppendLine("        INNER JOIN tbldoct2dtl R ON A.DocNo = R.DocNo ");
                SQL.AppendLine("        LEFT JOIN tblsalesinvoicedtl S ON R.DocNo = S.DOCtDocNo AND R.DNo = S.DOCtDNo ");
                SQL.AppendLine("        LEFT JOIN tblsalesinvoicehdr T ON S.DocNo = T.DocNo ");
                SQL.AppendLine("        Where A.DRDocNo IS Not Null ");
                SQL.AppendLine(Filter3);
            }

            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select Concat(Left(A.DocDt, 4), '-',Substring(A.DocDt, 5, 2)) As Period, A.WhsCode, ");
            SQL.AppendLine("        Date_Format(Date_Add(A.DocDt, Interval 0 Day), '%Y%m%d') As DueDt, ");
            SQL.AppendLine("        DateDiff(Left(currentdatetime(), 8), Date_Format(Date_Add(A.DocDt, Interval 0 Day), '%Y%m%d')) As AgingDays, ");
            SQL.AppendLine("        A.CtCode, A.Docno, A.CurCode, ");
            SQL.AppendLine("        B.Qty, B.UPrice, 0.00 As Tax, ");
            SQL.AppendLine("        D.WhsName, G.EntName, A.Remark, '1' As DocType, A.DocDt ");
            if (mIsRptAgingARDOCtShowFulfilledInvoice)
                SQL.AppendLine("        , K.DocNo As InvoiceDocNo, IfNull(H.InvoiceAmt, 0.00) InvoiceAmt, ((IfNull(B.Qty, 0.00) * IfNull(B.UPrice, 0.00)) - IfNull(H.InvoiceAmt, 0.00)) As BalanceAmt, I.ProjectCode ");
            else
                SQL.AppendLine("        , K.DocNo As InvoiceDocNo, 0.00 As InvoiceAmt, 0.00 As BalanceAmt, Null As ProjectCode ");
            SQL.AppendLine("        From TblDOCtHdr A  ");
            SQL.AppendLine("        Inner Join TblDOCtDtl B On A.DocNo=B.DocNo And B.Qty>0 And CancelInd = 'N' ");
            SQL.AppendLine("            And B.ProcessInd In ('O' ");
            if (mIsRptAgingARDOCtShowFulfilledInvoice)
                SQL.AppendLine("            , 'P', 'F' ");
            SQL.AppendLine("            ) ");
            SQL.AppendLine("        Inner Join TblWarehouse D On A.WhsCode = D.WhsCode ");
            SQL.AppendLine("        Left Join TblCostcenter E On D.CCCode = E.CCCode ");
            SQL.AppendLine("        Left Join TblProfitcenter F On E.ProfitCenterCode = F.ProfitcenterCode ");
            SQL.AppendLine("        Left Join TblEntity G On F.EntCode = G.EntCode ");
            if (mIsRptAgingARDOCtShowFulfilledInvoice)
            {
                SQL.AppendLine("    Left Join ( ");
                SQL.AppendLine("        Select X2.DocNo, X2.DOCtDocNo, X2.DOCtDNo, Sum(X2.UPriceAfterTax * X2.Qty) InvoiceAmt ");
                SQL.AppendLine("        From TblSalesInvoiceHdr X1 ");
                SQL.AppendLine("        Inner Join TblSalesInvoiceDtl X2 On X1.DocNo = X2.DocNo ");
                SQL.AppendLine("            And X1.CancelInd = 'N' ");
                SQL.AppendLine("            And X1.Status = 'A' ");
                SQL.AppendLine("            And X1.CBDInd = 'N' ");
                SQL.AppendLine("            And X2.DocType = '3' ");
                SQL.AppendLine("        Group By X2.DocNo, X2.DoCtDocNo, X2.DOCtDNo ");
                SQL.AppendLine("    ) H On B.DocNo = H.DOCtDocNo And B.DNo = H.DOCtDNo ");
                SQL.AppendLine("    Left Join ( ");
                SQL.AppendLine("        Select X1.DocNO, X4.ProjectCode ");
                SQL.AppendLine("        From TblSOContractHdr X1 ");
                SQL.AppendLine("        Inner Join TblBOQHdr X2 On X1.BOQDocNo = X2.DocNo ");
                SQL.AppendLine("            And X1.DocNo In (Select Distinct SOContractDocNo From TblDOCtHdr Where SOContractDocNo Is Not Null) ");
                SQL.AppendLine("        Inner Join TblLOPHdr X3 On X2.LOPDocNo = X3.DocNo ");
                SQL.AppendLine("        Inner Join TblProjectGroup X4 ON X3.PGCode = X4.PGCode ");
                SQL.AppendLine("    ) I On A.SOContractDocNo = I.DocNo ");
            }
            SQL.AppendLine("        LEFT JOIN tblsalesinvoicedtl J ON B.DocNo = J.DOCtDocNo AND B.DNo = J.DOCtDNo ");
            SQL.AppendLine("        LEFT JOIN tblsalesinvoicehdr K ON K.DocNo = J.DocNo ");
            SQL.AppendLine("        INNER JOIN tblitem Q ON B.ItCode = Q.ItCode ");
            SQL.AppendLine("        Where A.Status='A' ");
            SQL.AppendLine(Filter3);
            SQL.AppendLine("    ) T " + Filter2);
            SQL.AppendLine("    Group By T.Period, T.DueDt, T.AgingDays, T.CtCode, T.DocNo, T.CurCode ");
            if (mIsRptAgingARDOCtShowFulfilledInvoice)
                SQL.AppendLine("    , T.InvoiceDocNo, T.ProjectCode ");
            SQL.AppendLine(") T1 ");
            SQL.AppendLine("Inner Join TblCustomer T2 On T1.CtCode=T2.CtCode ");
            SQL.AppendLine(Filter1);
            SQL.AppendLine("Left Join TblCustomerCategory T3 On T2.CtCtCode=T3.CtCtCode ");
            SQL.AppendLine("WHERE 1 = 1 ");
            if (Sm.GetDte(DteBalAsOfDt).Length != 0)
            {
                SQL.AppendLine("And T1.DocDt <= @BalAsOfDt ");
                SQL.AppendLine("AND T1.DocNo NOT IN (SELECT DOCtDocNo FROM tblsalesinvoicedtl A ");
                SQL.AppendLine("    INNER JOIN tblsalesinvoicehdr B ON A.DocNo = B.DocNo WHERE B.DocDt <= @BalAsOfDt AND B.CancelInd = 'N') ");
            }
            if (mIsFilterByWarehouse)
            {
                SQL.AppendLine("AND Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupWarehouse ");
                SQL.AppendLine("    Where WhsCode=T1.WhsCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            if (mIsFilterByCtCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupCustomerCategory ");
                SQL.AppendLine("    Where CtCtCode=T3.CtCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Order By T1.Period, T1.CurCode, T2.CtName, T1.DocNo; ");

            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 26;
            Grd1.FrozenArea.ColCount = 5;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[]
                    {
                        //0
                        "No", 
                        
                        //1-5
                        "Period",
                        "Currency",
                        "Customer",
                        "DO#",
                        "",
                        
                        //6-10
                        "Entity",
                        "Warehouse",
                        "Remark",
                        "Amount"+Environment.NewLine+"Before Tax",
                        "Tax"+Environment.NewLine+"Amount",

                        //11-15
                        "Amount"+Environment.NewLine+"After Tax",
                        "Due"+Environment.NewLine+"Date",
                        "Aging"+Environment.NewLine+"(Days)",
                        "Aging"+Environment.NewLine+"Current AR",
                        "Over Due"+Environment.NewLine+"1-30 Days",

                        //16-20
                        "Over Due"+Environment.NewLine+"31-60 Days",
                        "Over Due"+Environment.NewLine+"61-90 Days",
                        "Over Due"+Environment.NewLine+"91-120 Days",
                        "Over 120 Days",
                        "DocType",

                        //21-25
                        "Customer Category",
                        "Invoice#",
                        "Invoice Amount",
                        "Balance",
                        "Project Code",
                    },
                    new int[]
                    {
                        //0
                        50,

                        //1-5
                        100, 60, 250, 145, 20,
                        
                        //6-10
                        180, 180, 200, 130, 130,  

                        //11-15
                        130, 80, 130, 130, 130, 

                        //16-20
                        130, 130, 130, 130, 0,

                        //21-25
                        200, 200, 180, 180, 200
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 5 });
            Sm.GrdFormatDec(Grd1, new int[] { 9, 10, 11, 13, 14, 15, 16, 17, 18, 19, 23, 24 }, 2);
            Sm.GrdFormatDate(Grd1, new int[] { 12 });
            Sm.GrdColInvisible(Grd1, new int[] { 6, 7, 8, 9, 10, 20 }, false);
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25 });
            if (!mIsRptAgingARDOCtShowFulfilledInvoice)
                Sm.GrdColInvisible(Grd1, new int[] { 22, 23, 24, 25 });
            Grd1.Cols[21].Move(4);
            Grd1.Cols[22].Move(14);
            Grd1.Cols[23].Move(15);
            Grd1.Cols[24].Move(16);

        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 6, 7, 8, 9, 10 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Cursor.Current = Cursors.WaitCursor;
            try
            {
                if (Sm.IsFilterByDateInvalid(ref DteDODt1, ref DteDODt2)
                    ||
                    IsFilterDtMandatory()
                    ) return;
                var cm = new MySqlCommand();
                decimal AgingDays = 0m, AmtAfterTax = 0m;
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        GetSQL(ref cm),
                        new string[]
                            { 
                                //0
                                "Period", 

                                //1-5
                                "CurCode", "CtName", "DocNo", "EntName", "WhsName",  
                                
                                //6-10
                                "Remark", "AmtBeforeTax", "TaxAmt", "AmtAfterTax", "DueDt", 
                                
                                //11-15
                                "AgingDays", "DocType", "CtCtName", "InvoiceDocNo", "InvoiceAmt",

                                //16-17
                                "BalanceAmt", "ProjectCode"
                            },

                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                            if (mIsDOCtAmtRounded)
                            {
                                Grd.Cells[Row, 9].Value = decimal.Truncate(dr.GetDecimal(c[7]));
                                Grd.Cells[Row, 10].Value = decimal.Truncate(dr.GetDecimal(c[8]));
                                AmtAfterTax = decimal.Truncate(dr.GetDecimal(c[9]));
                                Grd.Cells[Row, 11].Value = decimal.Truncate(AmtAfterTax);
                            }
                            else
                            {
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 7);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 8);
                                AmtAfterTax = decimal.Truncate(dr.GetDecimal(c[9]));
                                Grd.Cells[Row, 11].Value = AmtAfterTax;
                            }

                            Sm.SetGrdValue("D", Grd, dr, c, Row, 12, 10);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 11);

                            AgingDays = dr.GetDecimal(c[11]);
                            Grd.Cells[Row, 14].Value = AgingDays < 0m ? AmtAfterTax : 0m;
                            Grd.Cells[Row, 15].Value = AgingDays > 0m && AgingDays < 31 ? AmtAfterTax : 0m;
                            Grd.Cells[Row, 16].Value = AgingDays > 30m && AgingDays < 61 ? AmtAfterTax : 0m;
                            Grd.Cells[Row, 17].Value = AgingDays > 60m && AgingDays < 91 ? AmtAfterTax : 0m;
                            Grd.Cells[Row, 18].Value = AgingDays > 90m && AgingDays < 121 ? AmtAfterTax : 0m;
                            Grd.Cells[Row, 19].Value = AgingDays > 120m ? AmtAfterTax : 0m;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 13);

                            if (mIsRptAgingARDOCtShowFulfilledInvoice)
                            {
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 14);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 23, 15);
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 24, 16);
                                Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 17);
                            }
                        }, true, false, false, false
                    );
                Grd1.GroupObject.Add(1);
                Grd1.GroupObject.Add(2);
                Grd1.GroupObject.Add(3);
                Grd1.Group();
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 9, 10, 11, 13, 14, 15, 16, 17, 18, 19, 23, 24 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        internal void SetLueCtCode(ref DXE.LookUpEdit Lue)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select CtCode As Col1, CtName As Col2 ");
                SQL.AppendLine("From TblCustomer ");
                SQL.AppendLine("Order By CtName; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };


                Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                e.DoDefault = false;
                switch (mDocTitle)
                {
                    case "KMI":
                    case "KIM":
                        {
                            var f1 = new FrmDOCt(mMenuCode);
                            f1.Tag = mMenuCode;
                            f1.WindowState = FormWindowState.Normal;
                            f1.StartPosition = FormStartPosition.CenterScreen;
                            f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                            f1.ShowDialog();
                            break;
                        }
                    default:
                        {
                            string DocType = Sm.GetGrdStr(Grd1, e.RowIndex, 20);

                            if (DocType == "1")
                            {
                                var f1 = new FrmDOCt(mMenuCode);
                                f1.Tag = mMenuCode;
                                f1.WindowState = FormWindowState.Normal;
                                f1.StartPosition = FormStartPosition.CenterScreen;
                                f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                                f1.ShowDialog();
                            }
                            else if (DocType == "3")
                            {
                                var f1 = new FrmDOCt7(mMenuCode);
                                f1.Tag = mMenuCode;
                                f1.WindowState = FormWindowState.Normal;
                                f1.StartPosition = FormStartPosition.CenterScreen;
                                f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                                f1.ShowDialog();
                            }
                            else
                            {
                                var f1 = new FrmDOCt2(mMenuCode);
                                f1.Tag = mMenuCode;
                                f1.WindowState = FormWindowState.Normal;
                                f1.StartPosition = FormStartPosition.CenterScreen;
                                f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                                f1.ShowDialog();
                            }
                            break;
                        }
                }
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                switch (mDocTitle)
                {
                    case "KMI":
                    case "KIM":
                        {
                            var f1 = new FrmDOCt(mMenuCode);
                            f1.Tag = mMenuCode;
                            f1.WindowState = FormWindowState.Normal;
                            f1.StartPosition = FormStartPosition.CenterScreen;
                            f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                            f1.ShowDialog();
                            break;
                        }
                    default:
                        {
                            string DocType = Sm.GetGrdStr(Grd1, e.RowIndex, 20);

                            if (DocType == "1")
                            {
                                var f1 = new FrmDOCt(mMenuCode);
                                f1.Tag = mMenuCode;
                                f1.WindowState = FormWindowState.Normal;
                                f1.StartPosition = FormStartPosition.CenterScreen;
                                f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                                f1.ShowDialog();
                            }
                            else if (DocType == "3")
                            {
                                var f1 = new FrmDOCt7(mMenuCode);
                                f1.Tag = mMenuCode;
                                f1.WindowState = FormWindowState.Normal;
                                f1.StartPosition = FormStartPosition.CenterScreen;
                                f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                                f1.ShowDialog();
                            }
                            else
                            {
                                var f1 = new FrmDOCt2(mMenuCode);
                                f1.Tag = mMenuCode;
                                f1.WindowState = FormWindowState.Normal;
                                f1.StartPosition = FormStartPosition.CenterScreen;
                                f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                                f1.ShowDialog();
                            }
                            break;
                        }
                }
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        internal bool IsFilterDtMandatory()
        {
            if(Sm.GetDte(DteDODt1).Length == 0 && Sm.GetDte(DteDODt2).Length == 0 && Sm.GetDte(DteBalAsOfDt).Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, "You must enter at least a DO date or Balance As Of");
                return true;
            }
            //else if (DteBalAsOfDt.Enabled == false && ChkBalAsOfDt.Checked == false)
            //{
            //    return
            //    Sm.IsDteEmpty(DteDODt1, "DO Date") ||
            //    Sm.IsDteEmpty(DteDODt2, "DO Date");
            //}
            //else if (DteDODt1.Enabled == false && DteDODt2.Enabled == false & ChkDODt.Checked == false)
            //{
            //    return
            //        Sm.IsDteEmpty(DteBalAsOfDt, "Balance As Of Date");
            //}
            else
                return false;
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            //Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue3(Sl.SetLueCtCode), string.Empty, mIsFilterByCtCt ? "Y" : "N");
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(SetLueCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Due date");
        }

        private void DteDODt1_Validated(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDODt1).Length != 0 && Sm.GetDte(DteDODt2).Length != 0)
            {
                ChkBalAsOfDt.Checked = false;
                DteBalAsOfDt.Enabled = false;
                //LblBalAsOf.ForeColor = Color.Black;
                //LblDODt.ForeColor = Color.Red;
            }
        }

        private void DteDODt2_Validated(object sender, EventArgs e)
        {
            //ChkBalAsOfDt.Checked = false;
            if (Sm.GetDte(DteDODt1).Length != 0 && Sm.GetDte(DteDODt2).Length != 0)
            {
                DteBalAsOfDt.Enabled = false;
                ChkBalAsOfDt.Enabled = false;
                //LblBalAsOf.ForeColor = Color.Black;
                //LblDODt.ForeColor = Color.Red;
            }
        }

        private void DteBalAsOfDt_Validated(object sender, EventArgs e)
        {
            //ChkDODt.Checked = false;
            if (Sm.GetDte(DteBalAsOfDt).Length != 0)
            {
                DteDODt1.Enabled = false;
                DteDODt2.Enabled = false;
                ChkDODt.Enabled = false;
                //LblDODt.ForeColor = Color.Black;
                //LblBalAsOf.ForeColor = Color.Red;
            }
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void LueCtItem_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtItCode, new Sm.RefreshLue1(Sl.SetLueItCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCtItem_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Item's category");
        }

        private void LueCtCtCode_EditValueChanged(object sender, EventArgs e)
        {
            //Sm.RefreshLookUpEdit(LueCtCtCode, new Sm.RefreshLue3(Sl.SetLueCtCtCode), string.Empty, mIsFilterByCtCt ? "Y" : "N");
            Sm.RefreshLookUpEdit(LueCtCtCode, new Sm.RefreshLue1(Sl.SetLueCtCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCtCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer's category");
        }

        private void DteDODt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDODt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void ChkDODt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "DO Date");
            if (Sm.GetDte(DteDODt1).Length == 0 && Sm.GetDte(DteDODt2).Length == 0)
            {
                DteBalAsOfDt.Enabled = true;
                ChkBalAsOfDt.Enabled = true;
                //LblDODt.ForeColor = Color.Black;
            }
        }

        private void DteBalAsOfDt_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit2(this, sender);
        }

        private void ChkBalAsOfDt_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit2(this, sender, "Balance As Of Date");
            if (Sm.GetDte(DteBalAsOfDt).Length == 0)
            {
                DteDODt1.Enabled = true;
                DteDODt2.Enabled = true;
                ChkDODt.Enabled = true;
                //LblBalAsOf.ForeColor = Color.Black;
            }
        }

        #endregion

        #endregion
    }
}
