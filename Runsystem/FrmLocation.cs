﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmLocation : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;// mLocNo = string.Empty;
        internal FrmLocationFind FrmFind;
        private bool mIsSiteMandatory = false;

        #endregion

        #region Constructor

        public FrmLocation(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Location";
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                BtnPrint.Visible = false;
                GetParameter();
                SetFormControl(mState.View);

                SetLueSiteCode(ref LueSiteCode, string.Empty);
                SetLueParent(ref LueParent, string.Empty);

                //if (mLocNo.Length != 0)
                //{
                //    ShowData(mLocNo);
                //    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                //}
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void GetParameter()
        {
            mIsSiteMandatory = Sm.GetParameter("IsSiteMandatory") == "Y";
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtLocCode, TxtLocName, LueSiteCode, LueParent, MeeRemark, ChkActInd
                    }, true);
                    if (!mIsSiteMandatory)
                        label1.ForeColor = Color.Black;
                    LueSiteCode.Focus();
                    break;
                case mState.Insert:
                    //Sm.SetLue(LueSiteCode, Sm.GetValue("SELECT SiteCode FROM tblsite WHERE ActInd='Y'"));
                    //Sm.SetLue(LueParent, Sm.GetValue("SELECT IFNULL(LocationCode, '') FROM tbllocation"));
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtLocCode, TxtLocName, LueParent, LueSiteCode, MeeRemark
                    }, false);
                    Sm.SetControlReadOnly(ChkActInd, true);
                    LueSiteCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(ChkActInd, false);
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtLocCode, TxtLocName, LueParent, LueSiteCode, MeeRemark
                    }, true);
                    ChkActInd.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
               TxtLocCode, TxtLocName, LueParent, LueSiteCode, MeeRemark
            });
            ChkActInd.Checked = false;
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmLocationFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                SetLueSiteCode(ref LueSiteCode, string.Empty);
                SetLueParent(ref LueParent, string.Empty);
                ChkActInd.Checked = true;
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtLocCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtLocCode, "", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "DELETE FROM tbllocation WHERE LocCode=@LocCode" };
                Sm.CmParam<String>(ref cm, "@LocCode", TxtLocCode.Text);
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }     

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;
                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("INSERT INTO tbllocation(LocCode, LocName, Parent, SiteCode, ActInd, Remark, CreateBy, CreateDt) ");
                SQL.AppendLine("VALUES(@LocCode, @LocName, @Parent, @SiteCode, 'Y', @Remark, @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("ON DUPLICATE KEY ");
                SQL.AppendLine("   UPDATE LocName=@LocName, Parent=@Parent, SiteCode=@SiteCode, ActInd=@ActInd, Remark=@Remark, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };

                Sm.CmParam<String>(ref cm, "@LocCode", TxtLocCode.Text);
                Sm.CmParam<String>(ref cm, "@LocName", TxtLocName.Text);
                Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
                Sm.CmParam<String>(ref cm, "@Parent", Sm.GetLue(LueParent));
                Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked ? "Y" : "N");
                Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                Sm.ExecCommand(cm);

                ShowData(TxtLocCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private bool IsDataNotValid()
        {
            return
                IsSiteMandatory() ||
                Sm.IsTxtEmpty(TxtLocCode, "Location Code", false) ||
                Sm.IsTxtEmpty(TxtLocName, "Location Name", false) ||
                IsLocCodeExisted();
                
        }

        private bool IsSiteMandatory()
        {
            if (mIsSiteMandatory)
            {
                return
                    Sm.IsLueEmpty(LueSiteCode, "Site");
                    //IsLocCodeExisted();
                //return false;
            }
            return false;
        }

        private bool IsLocCodeExisted()
        {
            if (!TxtLocCode.Properties.ReadOnly)
            {
                var cm = new MySqlCommand();
                cm.CommandText = "SELECT LocCode FROM tbllocation WHERE LocCode=@LocCode;";
                Sm.CmParam<String>(ref cm, "@LocCode", TxtLocCode.Text);
                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "Location code ( " + TxtLocCode.Text + " ) already existed.");
                    return true;
                }
            }
            return false;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string LocCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@LocCode", LocCode);

                Sm.ShowDataInCtrl(
                        ref cm,
                        "SELECT * FROM tbllocation WHERE LocCode=@LocCode",
                        new string[] 
                        {
                            //0
                            "LocCode", 
                            
                            //1-5
                            "LocName", "Parent", "SiteCode", "ActInd", "Remark"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtLocCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtLocName.EditValue = Sm.DrStr(dr, c[1]);
                            SetLueParent(ref LueParent, Sm.DrStr(dr, c[2]));
                            Sm.SetLue(LueParent, Sm.DrStr(dr, c[2]));
                            SetLueSiteCode(ref LueSiteCode, Sm.DrStr(dr, c[3]));
                            Sm.SetLue(LueSiteCode, Sm.DrStr(dr, c[3]));
                            ChkActInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[4]), "Y");
                            MeeRemark.EditValue = Sm.DrStr(dr, c[5]);
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Additional Method

        internal void SetLueSiteCode(ref DXE.LookUpEdit Lue, string SiteCode)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("SELECT SiteCode As Col1, SiteName As Col2 ");
            SQL.AppendLine("FROM tblsite ");

            if (SiteCode.Length != 0)
                SQL.AppendLine("WHERE SiteCode=@SiteCode ");
            else
            {
                SQL.AppendLine("WHERE ActInd='Y' ");
            }
            SQL.AppendLine("ORDER BY SiteName;");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@SiteCode", SiteCode);

            Sm.SetLue2(
                ref Lue, ref cm,
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        internal void SetLueParent(ref DXE.LookUpEdit Lue, string LocCode)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("SELECT LocCode As Col1, LocName As Col2 ");
            SQL.AppendLine("FROM tbllocation ");

            if (LocCode.Length != 0)
                SQL.AppendLine("WHERE LocCode=@LocCode ");
            else
            {
                SQL.AppendLine("WHERE ActInd='Y' ");
            }
            SQL.AppendLine("ORDER BY LocName;");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@LocCode", LocCode);

            Sm.SetLue2(
                ref Lue, ref cm,
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
            {
                Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue2(SetLueSiteCode), string.Empty);
            }

            private void TxtLocCode_Validated(object sender, EventArgs e)
            {
                Sm.TxtTrim(TxtLocCode);
            }

            private void TxtLocName_Validated(object sender, EventArgs e)
            {
                Sm.TxtTrim(TxtLocName);
            }

            private void LueParent_EditValueChanged(object sender, EventArgs e)
            {
                Sm.RefreshLookUpEdit(LueParent, new Sm.RefreshLue2(SetLueParent), string.Empty);
            }

            private void MeeRemark_EditValueChanged(object sender, EventArgs e)
            {
                Sm.TxtTrim(MeeRemark);
            }

            #endregion

        #endregion
    }
}
