﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmJobHolder : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmJobHolderFind FrmFind;

        #endregion

        #region Constructor

        public FrmJobHolder(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            SetFormControl(mState.View);
            Sl.SetLueDeptCode(ref LueDeptCode);
            Sl.SetLuePosCode(ref LuePosCode);
            Sl.SetLueGrdLvlCode(ref LueGrdLvlCode);
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtJobHolderCode,LueDeptCode,LuePosCode,TxtEmpCode,DteStartDt,LueGrdLvlCode,MeeRemark
                    }, true);
                    TxtJobHolderCode.Focus(); BtnEmpCode.Enabled = false;
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(TxtEmpCode, true);
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                         TxtJobHolderCode,LueDeptCode,LuePosCode,DteStartDt,LueGrdLvlCode,MeeRemark
                    }, false);
                    TxtJobHolderCode.Focus(); BtnEmpCode.Enabled = true;
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(TxtJobHolderCode, true);
                    Sm.SetControlReadOnly(TxtEmpCode, true);
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        LueDeptCode,LuePosCode,DteStartDt,LueGrdLvlCode,MeeRemark
                    }, false);
                    LueDeptCode.Focus(); BtnEmpCode.Enabled = true;
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtJobHolderCode,LueDeptCode,LuePosCode,TxtEmpCode,DteStartDt,LueGrdLvlCode,MeeRemark
            });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmJobHolderFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
            DteStartDt.DateTime = Sm.ConvertDate(Sm.ServerCurrentDateTime());
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtJobHolderCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtJobHolderCode, "", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblJobHolder Where JobHolderCode=@JobHolderCode" };
                Sm.CmParam<String>(ref cm, "@JobHolderCode", TxtJobHolderCode.Text);
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into TblJobHolder(JobHolderCode, DeptCode, PosCode, EmpCode, StartDt, GrdLvlCode, Remark, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@JobHolderCode, @DeptCode, @PosCode, @EmpCode, @StartDt, @GrdLvlCode, @Remark, @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("   Update  DeptCode=@DeptCode, PosCode=@PosCode, EmpCode=@EmpCode, StartDt=@StartDt, GrdLvlCode=@GrdLvlCode, Remark=@Remark, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@JobHolderCode", TxtJobHolderCode.Text);
                Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
                Sm.CmParam<String>(ref cm, "@PosCode", Sm.GetLue(LuePosCode));
                Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
                Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
                Sm.CmParam<String>(ref cm, "@GrdLvlCode", Sm.GetLue(LueGrdLvlCode));
                Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.ExecCommand(cm);

                ShowData(TxtJobHolderCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string JobHolderCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@JobHolderCode", JobHolderCode);
                Sm.ShowDataInCtrl(
                        ref cm,
                        "Select JobHolderCode, DeptCode, PosCode, EmpCode, StartDt, GrdLvlCode, Remark From TblJobHolder Where JobHolderCode=@JobHolderCode ",
                        new string[] {
                        //0
                        "JobHolderCode",
                        //1-5
                        "DeptCode", 
                        "PosCode", 
                        "EmpCode", 
                        "StartDt", 
                        "GrdLvlCode",
                        //6
                        "Remark"

                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtJobHolderCode.EditValue = Sm.DrStr(dr, c[0]);
                            Sm.SetLue(LueDeptCode, Sm.DrStr(dr, c[1]));
                            Sm.SetLue(LuePosCode, Sm.DrStr(dr, c[2]));
                            TxtEmpCode.EditValue = Sm.DrStr(dr, c[3]);
                            Sm.SetDte(DteStartDt, Sm.DrStr(dr, c[4]));
                            Sm.SetLue(LueGrdLvlCode, Sm.DrStr(dr, c[5]));
                            MeeRemark.EditValue = Sm.DrStr(dr, c[6]);
                            
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtJobHolderCode, "Job holder code", false) ||
                Sm.IsLueEmpty(LueDeptCode, "Department") ||
                 Sm.IsLueEmpty(LuePosCode, "Position") ||
                IsCurCodeExisted();
        }

        private bool IsCurCodeExisted()
        {
            if (!TxtJobHolderCode.Properties.ReadOnly && Sm.IsDataExist("Select JobHolderCode From TblJobHolder Where JobHolderCode='" + TxtJobHolderCode.Text + "' Limit 1"))
            {
                Sm.StdMsg(mMsgType.Warning, "Job holder code ( " + TxtJobHolderCode.Text + " ) already existed.");
                return true;
            }
            return false;
        }

        #endregion

      

        #endregion
        

        #region Event

        #region Misc Control Event

        private void TxtJobHolderCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtJobHolderCode);
        }
        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtEmpCode
            });
        }

        private void LuePosCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePosCode, new Sm.RefreshLue1(Sl.SetLuePosCode));
        }

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtEmpCode);
        }


        private void LueGrdLvlCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueGrdLvlCode, new Sm.RefreshLue1(Sl.SetLueGrdLvlCode));
        }

        private void MeeRemark_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(MeeRemark);
        }
        private void BtnEmpCode_Click(object sender, EventArgs e)
        {
            if (!Sm.IsLueEmpty(LueDeptCode, "Department"))
            {
                Sm.FormShowDialog(new FrmJobHolderDlg(this));
            }
        }
        #endregion

       

        #endregion        
    }
}
