﻿#region Update

#region Old
/*
    20/04/2017 [HAR] perubahan di printout dan aturan additinal discount jika ada account kepala 4 yang normalnya di debet dia tidak berlaku rule debet di debet = +,  
    27/04/2017 [TKG] bug fixing validasi sales invoice sudah diproses menjadi incoming payment.
    24/05/2017 [TKG] Journal selisih kurs.
    30/05/2017 [TKG] Berdasarkan parameter IsRemarkForJournalMandatory, Remark harus disi atau tidak.
    30/05/2017 [TKG] Update remark di journal 
    11/06/2017 [TKG] tambah validasi vat settlement ketika data hendak dicancel. 
    01/07/2017 [TKG] tambah kolom CBDInd pada saat save.
    18/07/2017 [TKG] perubahan proses journal terkait dengan additional cost.
    25/09/2017 [HAR] bug fixing printout additional cost.
    03/11/2017 [WED] insert ke tabel baru, untuk menyimpan curcode dan excrate saat ada downpayment (hanya saat insert)
    03/11/2017 [WED] update ke tabel summary2 saat edit
    03/12/2017 [TKG] perubahan proses penyimpanan untuk deposit dengan mata uang non IDR, perubahan proses journal selisih kurs uang muka.
    17/03/2018 [WED] print out add cost & disc, di modif, IOK & default
    27/03/2018 [ari] feedback printout (samakan dg si based on do to cus)-->KIM
    03/04/2018 [ARI] feedback printout kwitansi (rubah layout)
    04/04/2018 [HAR] Local Document dipanjangin jadi 80
    11/04/2018 [ARI] Rounding tax ke bawah, Amount Detail rounding ke atas
    13/04/2018 [ARI] remark di detail ambil dari remark do customer header
    04/07/2018 [ARI] tambah printout sales invoice dmk
    05/07/2018 [WED] Terbilang dan Terbilang2 IOK direvisi source nya
    25/07/2018 [Ari] tambah printout default runsystem
    26/07/2018 [Ari] tambah printout kwitansi default runsystem
    01/08/2018 [WED] tambah TaxInvDt
    27/08/2018 [TKG] tambah informasi total qty packaging dan total quantity
    05/10/2018 [HAR] additinal discount tidak dimunculkan, nilai amount diambil dari amount+ tax dikurangi additional disc
    21/02/2019 [MEY] tambah parameter baru MIndForSales 
    12/04/2019 [DITA] tambah inputan signature name dan Penandatangan di bawah "PT INDOTAMA" diisi ketikan saja, jangan otomatis.
    01/11/2019 [WED/IMS] tambah DR+SO Contract di Show Data
    26/11/2019 [HAR/IOK] BUG saat show data detail, item name tidk muncul (ShowSalesInvoiceDtl)
    28/11/2019 [DITA/IMS] Printout sales invoice baru
    29/11/2019 [DITA/IMS] tambah parameter isbomshowspecification
    12/12/2019 [WED/IMS] tambah fasilitas upload file per DO nya, berdasarkan parameter IsSalesInvoiceAllowToUploadFile
    10/01/2020 [WED/ALL] pakai parameter untuk print out nya
    13/01/2020 [VIN/SIER] prinout Sales Invoice 
    14/01/2020 [DITA/SIER] Bug address prinout Sales Invoice 
    18/02/2020 [TKG/SRN] tambah tax 
    20/02/2020 [HAR/SRN] printout lokasi ttd didasarkan pada parameter companylocation1
    05/03/2020 [IBL/KBN] Penomoran dokumen 6 digit, reset tiap tahun, dan berdasarkan entity
    10/03/2020 [VIN/SIER] sesuaikan printout sier 
    15/04/2020 [TKG/GSS] tambah tax untuk daftar coa
    11/05/2020 [DITA/SIER] tambah parameter mIsSLIUseAutoFacturNumber untuk mengisi tax invoice number secara otomatis dari factur number
    20/05/2020 [VIN/SIER] Penyesuaian Printout
    11/06/2020 [HAR/IOK] bug management indicator : validasi Mind baiknya tidak ditambah2in kecuali dari IOK krn itu khuus IOK
    08/07/2020 [TKG/SRN] Berdasarkan parameter IsSalesInvoiceCOANonTaxable, additional cost masuk pajak atau tidak.
    10/07/2020 [IBL/KSM] Menyambungkan dengan DOCt Based On DR-Sales Contract
    23/07/2020 [ICA/SRN] Membuat Tax invoice# dan tax dapat di edit dengan parameter IsSalesInvoiceTaxInvDocEditable dan Validasi incoming payment
    23/07/2020 [ICA/SRN] Show data setelah save dengan parameter IsSLIShowDataAfterSave
    24/07/2020 [TKG/SRN] saat journal untuk rekening coa pajak menggunakan setting dari master tax.
    28/07/2020 [ICA/SIER] PrintOut bagian keterangan diisi dengan remark yg ada di DO to customer 
    29/07/2020 [VIN/SRN] Printout memunculkan semua tax
    07/08/2020 [WED/SRN] pilihan additional discount dan cost masuk hitungan atau tidak, berdasarkan parameter SLITaxCalculationFormat
    12/08/2020 [VIN/SRN] Penyesuaian Printout - Additional Cost ditampilkan semua 
    11/09/2020 [TKG/SIER] Bug berhubungan dengan pajak (save hdr dan journal)
    02/12/2020 [IBL/SIER] Lue customer menamplikan customer category, berdasarkan parameter IsCustomerComboShowCategory
 */
#endregion
/*
    11/01/2021 [VIN/SRN] Validasi tidak bisa SAVE ketika terdapat setting COA otomatis yang masih kosong
    17/01/2021 [TKG/PHT] ubah GenerateReceipt
    24/02/2021 [WED/SIER] tambah inputan Customer Category berdasarakan parameter IsCustomerComboBasedOnCategory
    09/04/2021 [ICA/PHT] Insert Cost Center saat save journal based on parameter IsJournalCostCenterEnabled
    09/04/2021 [ICA/SIER] menambah kondisi di setluebankaccode yg muncul hanya bank active
    20/04/2021 [WED/SRN] perhitungan tax per detail nya berdasarkan parameter SalesInvoiceTaxCalculationFormula (default : 1)
    22/04/2021 [WED/SRN] bug belom tambah di setformcontrol untuk checkbox barunya
    23/04/2021 [WED/ALL] bug saat save, param query @CtCode ter duplikasi
    03/05/2021 [RDH/SIER] menambah lup dialog customer category 
    18/05/2021 [RDH/SIER] FEEDBACK : penyesuaian customer category berdasarkan grop login
    27/05/2021 [RDA/SIER] memperbaiki tampilan insert LueSPCode berdasarkan parameter IsSalesInvoice3NotAutoChooseSalesPerson
    17/06/2021 [IQA/SIER] menambahkan parameter mIsItCtFilteredByGroup buat ngefilter data berdasarkan grup login
    23/06/2021 [WED/SIER] pembulatan kebawah berdasarkan parameter IsDOCtAmtRounded
    13/09/2021 [IBL/ALL] validasi tidak bisa save ketika terdapat setting coa yang masih kosong. Berdasarkan parameter IsCheckCOAJournalNotExists dan IsJournalValidationSalesInvoiceEnabled
    30/09/2021 [VIN/ALL] tambah Query hapus Journal selisih laba rugi =0
    18/02/2022 [TKG/GSS] merubah GetParameter() dan proses Insert Data
    12/10/2022 [TYO/SKI] add print out SKI
    21/10/2022 [HAR/GSS] save journal ppn keluaran berdasarkan param yang sudah ada  : IsSalesInvoiceJournalUseTaxAcNo2, dimana jika aktif nilai jurnal ngelihat taxrate jika dia minus maka masuk ke debit
    27/10/2022 [HAR/GSS] sales invoice ppn keluaran : tidak perlu di rounding
    03/11/2022 [HAR/GSS] sales invoice ppn keluaran : jika nilai tax minus maka dibuat menjadi plus di jurnalnya
    10/11/2022 [TYO/SKI] Penyesuaian printout SKI
    10/04/2023 [RDA/KBN] approval sales invoice
    28/04/2023 [RDA/KBN] printout sales invoice KBN
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;
using System.IO;
using System.Net;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

using FastReport;
using FastReport.Data;

#endregion

namespace RunSystem
{
    public partial class FrmSalesInvoice : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string
            mMenuCode = string.Empty, mAccessInd = string.Empty, 
            mDocNo = string.Empty, //if this application is called from other application;
            mIsFormPrintOutInvoice = string.Empty,
            mFormPrintOutInvoiceReceipt = string.Empty,
            mSLITaxCalculationFormat = string.Empty,
            mSalesInvoiceTaxCalculationFormula = string.Empty
            ;
        private string 
            mMainCurCode = string.Empty,
            mEmpCodeSI = string.Empty,
            mEmpCodeTaxCollector = string.Empty,
            mPortForFTPClient = string.Empty,
            mHostAddrForFTPClient = string.Empty,
            mSharedFolderForFTPClient = string.Empty,
            mUsernameForFTPClient = string.Empty,
            mPasswordForFTPClient = string.Empty,
            mFileSizeMaxUploadFTPClient = string.Empty,
            mDocNoFormat = string.Empty,
            mEntCode = string.Empty,
            mTaxInvDocument = string.Empty,
            mDOTaxInfo = string.Empty
            ;
        private decimal mAdditionalCostDiscAmt = 0m;
        private bool
            mIsAutoJournalActived = false,
            mIsRemarkForJournalMandatory = false,
            mMIndForSales = false,
            mIsSalesInvoiceAllowToUploadFile = false,
            mIsSalesInvoiceTaxEnabled = false,
            mIsSalesInvoiceTaxInvDocEditable = false,
            mIsSLIShowDataAfterSave = false,
            mIsSalesInvoiceCOANonTaxable = false,
            mIsSalesInvoiceJournalUseTaxAcNo2 = false,
            mIsCheckCOAJournalNotExists = false,
            mIsSLIPrintShowAllTax = false,
            IsInsert = false,
            mIsSLIJournalUseCCCode = false,
            mIsSalesInvoice3NotAutoChooseSalesPerson = false,
            mIsJournalValidationSalesInvoiceEnabled = false
            ;
        internal bool 
            mIsCustomerItemNameMandatory = false, 
            mIsBOMShowSpecifications = false,
            mIsSLIUseAutoFacturNumber = false, 
            mIsSalesContractEnabled = false,
            mIsCustomerComboShowCategory = false,
            mIsCustomerComboBasedOnCategory = false,
            mIsFilterByCtCt = false,
            mIsItCtFilteredByGroup = false,
            mIsShowCustomerCategory = false,
            mIsDOCtAmtRounded = false
            ;
        internal FrmSalesInvoiceFind FrmFind;
        iGCell fCell;
        bool fAccept;
        private byte[] downloadedData;
        
        #endregion

        #region Constructor

        public FrmSalesInvoice(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Sales Invoice";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                if (mSalesInvoiceTaxCalculationFormula != "1") mDOTaxInfo = "(DO)";
                LblCtCtCode.Visible = LueCtCtCode.Visible = mIsCustomerComboBasedOnCategory;
                Sm.SetLookUpEdit(LueFontSize, new string[] { "6", "7", "8", "9", "10", "11", "12" });
                Sm.SetLue(LueFontSize, "9");

                Tc1.SelectedTabPage = Tp4;
                SetLueOptionCode(ref LueOption);
                LueOption.Visible = false;

                if (mIsSalesInvoiceTaxEnabled)
                {
                    Tc1.SelectedTabPage = Tp5;
                    Sl.SetLueTaxCode(new List<DevExpress.XtraEditors.LookUpEdit> { LueTaxCode1, LueTaxCode2, LueTaxCode3 });
                }
                else
                    Tp5.PageVisible = false;
                Tc1.SelectedTabPage = Tp1;
                Sl.SetLueCurCode(ref LueCurCode);
                Sl.SetLueBankAcCode(ref LueBankAcCode, string.Empty);
                SetLueSPCode(ref LueSPCode);
                
                SetGrd();
                SetFormControl(mState.View);
                if (mIsRemarkForJournalMandatory) LblRemark.ForeColor = Color.Red;
                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grd1

            Grd1.Cols.Count = 46;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",

                        //1-5
                        "",
                        "DO#",
                        "DO D#",
                        "",
                        "Date",

                        //6-10
                        "DO Request#" + Environment.NewLine + "/ PL#",
                        "DO Request D#" + Environment.NewLine + "/ PL D#",
                        "",
                        "SO#",
                        "SO D#",
                        
                        //11-15
                        "",
                        "Quotation#",
                        "Quotation D#",
                        "",
                        "Item's Code",
                        
                        //16-20
                        "",
                        "Item's Name",
                        "Packaging" + Environment.NewLine + "Quantity",
                        "Packaging" + Environment.NewLine + "UoM",
                        "Quantity",

                        //21-25
                        "UoM",
                        "Currency",
                        "Price" + Environment.NewLine + "(Before Tax)",
                        "Tax"+ Environment.NewLine + "Rate " + mDOTaxInfo,
                        "Tax"+ Environment.NewLine + "Amount" + mDOTaxInfo,
                        
                        //26-30
                        "Price" + Environment.NewLine + "(After Tax)",
                        "Amount",
                        "TOP",
                        "Local" + Environment.NewLine + "Document",
                        "Type",

                        //31-35
                        "Customer"+Environment.NewLine+"Item's Name",
                        "FileName",
                        "",
                        "",
                        "FileName2",

                        //36-40
                        "Site Code",
                        "Tax 1",
                        "Tax 2",
                        "Tax 3",
                        "Tax Amount"+Environment.NewLine+"(Sales)",

                        //41-45
                        "Amount"+Environment.NewLine+"(Before Tax)",
                        "TaxAmt1",
                        "TaxAmt2",
                        "TaxAmt3",

                    },
                     new int[] 
                    {
                        //0
                        0, 

                        //1-5
                        20, 150, 0, 20, 100,
 
                        //6-10
                        130, 0, 20, 130, 0, 
                        
                        //11-15
                        20, 130, 0, 20, 80, 
                        
                        //16-20
                        20, 200, 100, 80, 100, 

                        //21-25
                        80, 80, 130, 100, 100, 

                        //26-30
                        130, 130, 150, 150, 0,

                        //31-35
                        250, 100, 20, 20, 100,

                        //36-40
                        0, 60, 60, 60, 150, 

                        //41-45
                        150, 0, 0, 0
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 37, 38, 39 });
            Sm.GrdColButton(Grd1, new int[] { 1, 4, 8, 11, 14, 16, 33, 34 });
            Sm.GrdFormatDec(Grd1, new int[] { 18, 20, 23, 24, 25, 26, 27, 28, 40, 41, 42, 43, 44 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 5 } );
            Grd1.Cols[31].Move(18);
            Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 24, 28, 30, 35, 36, 42, 43, 44 }, false);
            if (!mIsCustomerItemNameMandatory)
                Sm.GrdColInvisible(Grd1, new int[] { 31 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 3, 5, 6, 7, 9, 10, 12, 13, 15, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 35, 36, 40, 41, 45 });
            if (!mIsSalesInvoiceAllowToUploadFile) Sm.GrdColInvisible(Grd1, new int[] { 32, 33, 34 });

            if (mSalesInvoiceTaxCalculationFormula == "1") Sm.GrdColInvisible(Grd1, new int[] { 37, 38, 39, 40, 41 });

            #endregion

            #region Grd2

            Grd2.Cols.Count = 2;
            Sm.GrdHdrWithColWidth(Grd2, new string[] { "Currency", "Deposit Amount" }, new int[] { 100, 200 });
            Sm.GrdFormatDec(Grd2, new int[] { 1 }, 0);
            Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 1 });

            #endregion

            #region Grd3

            Grd3.Cols.Count = 11;
            Grd3.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd3,
                    new string[] 
                    {
                        //0
                        "",

                        //1-5
                        "Account#",
                        "Description",
                        "",
                        "Debit"+Environment.NewLine+"Amount",
                        "",
                        //6-10
                        "Credit"+Environment.NewLine+"Amount",
                        "OptCode",
                        "Option",
                        "Remark",
                        ""
                    },
                     new int[] 
                    {
                        //0
                        20,

                        //1-5
                        150, 300, 20, 100, 20, 
                        //6-10
                        100, 50, 150, 400, 20
                    }
                );
            Sm.GrdColButton(Grd3, new int[] { 0 });
            Sm.GrdColCheck(Grd3, new int[] { 3, 5, 10 });
            Sm.GrdColReadOnly(true, true, Grd3, new int[] { 0, 1, 2, 4, 6, 7, 8, 9, 10 });
            Sm.GrdFormatDec(Grd3, new int[] { 4, 6 }, 0);
            Sm.GrdColInvisible(Grd3, new int[] { 1, 7, 10 }, false);

            #endregion

            #region Grid 4 - Approval Information

            Grd4.Cols.Count = 6;
            Sm.GrdHdrWithColWidth(
                    Grd4,
                    new string[]
                    {
                        //0
                        "No",

                        //1-4
                        "User",
                        "Position",
                        "Status",
                        "Date",
                        "Remark"
                    },
                    new int[] { 50, 150, 150, 100, 80, 200 }
                );
            Sm.GrdFormatDate(Grd4, new int[] { 4 });

            #endregion
        }

        private void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 4, 6, 8, 9, 11, 12, 14, 15, 16, 24 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd3, new int[] { 1 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        ChkCancelInd, DteDocDt, LueCtCode, DteDueDt, TxtLocalDocNo, LueSPCode,
                        TxtTaxInvDocument, LueCurCode, TxtDownpayment, LueBankAcCode, MeeRemark, 
                        DteTaxInvDt, TxtSignName, TxtTaxInvoiceNo, TxtTaxInvoiceNo2, TxtTaxInvoiceNo3, 
                        LueTaxCode1, LueTaxCode2, LueTaxCode3, DteTaxInvoiceDt, DteTaxInvoiceDt2, 
                        DteTaxInvoiceDt3, TxtAlias1, TxtAlias2, TxtAlias3, LueCtCtCode
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 37, 38, 39 });
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 33 });
                    Sm.GrdColReadOnly(true, true, Grd3, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 });
                    TxtDocNo.Focus();
                    BtnCtCode.Enabled = false;
                    if (!mIsShowCustomerCategory) BtnCtCode.Visible = false;
                    else LblCtCtCode.ForeColor = System.Drawing.Color.Black;

                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueCtCode, DteDueDt, TxtLocalDocNo,
                        LueCurCode, TxtDownpayment, MeeRemark,  LueBankAcCode, LueSPCode, 
                        DteTaxInvDt, TxtSignName, TxtTaxInvoiceNo, TxtTaxInvoiceNo2, 
                        TxtTaxInvoiceNo3, LueTaxCode1, LueTaxCode2, LueTaxCode3, DteTaxInvoiceDt, 
                        DteTaxInvoiceDt2, DteTaxInvoiceDt3, TxtAlias1, TxtAlias2, TxtAlias3
                    }, false);
                    if (mIsCustomerComboBasedOnCategory)
                    {
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueCtCtCode }, false);
                    }
                    if (mIsSLIUseAutoFacturNumber) Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtTaxInvDocument }, true);
                    else Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtTaxInvDocument }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1, 37, 38, 39 });
                    Sm.GrdColReadOnly(false, true, Grd3, new int[] { 0, 3, 4, 5, 6, 8, 9 });
                    DteDocDt.Focus();
                    if (mIsShowCustomerCategory)
                    {
                        BtnCtCode.Enabled = true;
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueCtCtCode, LueCtCode }, true);
                    }
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { ChkCancelInd }, false);
                    if (mIsSalesInvoiceTaxInvDocEditable) Sm.SetControlReadOnly(new List<DXE.BaseEdit> {
                        TxtTaxInvDocument, TxtTaxInvoiceNo, TxtTaxInvoiceNo2, TxtTaxInvoiceNo3
                    }, false);
                    ChkCancelInd.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            mAdditionalCostDiscAmt = 0m;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                 TxtDocNo, DteDocDt, LueCtCode, DteDueDt, TxtLocalDocNo, 
                 TxtTaxInvDocument, LueCurCode, MeeRemark, LueSPCode, LueBankAcCode,
                 TxtJournalDocNo, TxtJournalDocNo2, TxtReceiptNo, DteTaxInvDt, TxtSignName,
                 TxtTaxInvoiceNo, TxtTaxInvoiceNo2, TxtTaxInvoiceNo3, LueTaxCode1, LueTaxCode2, 
                 LueTaxCode3, DteTaxInvoiceDt, DteTaxInvoiceDt2, DteTaxInvoiceDt3, TxtAlias1, 
                 TxtAlias2, TxtAlias3, LueCtCtCode, TxtStatus
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit>
            { 
                TxtTotalAmt, TxtTotalTax, TxtDownpayment, TxtAmt, TxtTotalQtyPackagingUnit, 
                TxtTotalQty, TxtTaxAmt1, TxtTaxAmt2, TxtTaxAmt3  
            }, 0);
            ChkCancelInd.Checked = false;
            ClearGrd();
        }

        #region Clear Grid

        private void ClearGrd()
        {
            ClearGrd1();
            ClearGrd2();
            ClearGrd3();
            Sm.ClearGrd(Grd4, false);
        }

        private void ClearGrd1()
        {
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 18, 20, 23, 24, 25, 26, 27, 28, 40, 41, 42, 43, 44 });
        }

        private void ClearGrd2()
        {
            Grd2.Rows.Clear();
            Grd2.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd2, 0, new int[] { 1 });
        }

        private void ClearGrd3()
        {
            Grd3.Rows.Clear();
            Grd3.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd3, 0, new int[] { 4, 6 });
        }

        #endregion

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmSalesInvoiceFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sm.SetLue(LueCurCode, Sm.GetParameter("MainCurCode"));
                Sl.SetLueBankAcCode(ref LueBankAcCode, string.Empty);
                IsInsert = true;
                if (mIsCustomerComboBasedOnCategory)
                {
                    SetLueCtCtCode(ref LueCtCtCode);
                    SetLueCtCodeBasedOnCategory(ref LueCtCode, string.Empty, mIsFilterByCtCt ? "Y" : "N", string.Empty);
                }
                else
                {
                    SetLueCtCode(ref LueCtCode, string.Empty);
                }

                if (!mIsSalesInvoice3NotAutoChooseSalesPerson)
                    Sm.SetLue(LueSPCode, Sm.GetValue("Select SpCode From TblSalesPerson Order by CreateDt limit 1;"));
                

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
            IsInsert = false;
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            var x = new StringBuilder();
            string mDocNo = string.Empty;

            if (Grd1.Rows.Count > 0)
            {
                for (int i = 0; i < Grd1.Rows.Count; ++i)
                {
                    if (Sm.GetGrdStr(Grd1, i, 2).Length > 0)
                    {
                        if (mDocNo.Length > 0) mDocNo += ",";
                        mDocNo += Sm.GetGrdStr(Grd1, i, 2);
                    }
                }
            }

            x.AppendLine(" Select Distinct D.EntCode ");
            x.AppendLine(" From TblDOCt2Hdr A ");
            x.AppendLine(" Inner Join TblWarehouse B On A.WhsCode = B.WhsCode ");
            x.AppendLine("     And Find_In_Set(A.DocNo, @Param) ");
            x.AppendLine(" Inner Join TblCostCenter C On B.CCCode = C.CCCode ");
            x.AppendLine(" Inner Join TblProfitCenter D On C.ProfitCenterCode = D.ProfitCenterCode ");
            x.AppendLine(" Limit 1; ");

            mEntCode = Sm.GetValue(x.ToString(), mDocNo);

            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            ParPrint();
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            mTaxInvDocument = Sm.GetValue("Select FacturNo From TblFacturNumberDtl Where UsedInd='N' And SLIDocNo is null Limit 1; ");
            
            if (
                Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No ||
                IsInsertedDataNotValid()
                ) return;

            Cursor.Current = Cursors.WaitCursor;

            var IsNeedApproval = IsDocNeedApproval(TxtAmt.Text);

            string DocNo = string.Empty;
            if(mDocNoFormat == "1")
                DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "SalesInvoice", "TblSalesInvoiceHdr");
            else
                DocNo = Sm.GenerateDocNo3(Sm.GetDte(DteDocDt), "SalesInvoice", "TblSalesInvoiceHdr", mEntCode, "1");

            string ReceiptNo = GenerateReceipt(Sm.GetDte(DteDocDt));
            var lDepositSummary = new List<DepositSummary>();

            ProcessDepositSummary(ref lDepositSummary);

            var cml = new List<MySqlCommand>();

            cml.Add(SaveSalesInvoiceHdr(DocNo, ReceiptNo, IsNeedApproval));

            if (IsNeedApproval)
                cml.Add(SaveDocApproval(DocNo));

            cml.Add(SaveSalesInvoiceDtl(DocNo));
            //for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            //    if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0) 
            //        cml.Add(SaveSalesInvoiceDtl(DocNo, Row));

            if (Grd3.Rows.Count > 1)
            {
                cml.Add(SaveSalesInvoiceDtl2(DocNo));
                //for (int Row = 0; Row < Grd3.Rows.Count; Row++)
                //    if (Sm.GetGrdStr(Grd3, Row, 1).Length > 0) 
                //        cml.Add(SaveSalesInvoiceDtl2(DocNo, Row));
            }

            if (!IsNeedApproval)
            {
                if (decimal.Parse(TxtDownpayment.Text) != 0m)
                {
                    cml.Add(SaveCustomerDeposit(DocNo, IsNeedApproval));
                    if (lDepositSummary.Count > 0)
                    {
                        for (int i = 0; i < lDepositSummary.Count; i++)
                            cml.Add(SaveSalesInvoiceDtl3(DocNo, lDepositSummary[i], IsNeedApproval));
                    }
                }

                cml.Add(UpdateDOCtProcessInd(DocNo, "N", IsNeedApproval));

                if (mIsAutoJournalActived) cml.Add(SaveJournal(DocNo, IsNeedApproval));
                if (mIsSLIUseAutoFacturNumber)
                    cml.Add(UpdateFacturNumber(mTaxInvDocument, DocNo));
            }
            Sm.ExecCommands(cml);
            

            if (mIsSalesInvoiceAllowToUploadFile)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0)
                    {
                        if (mIsSalesInvoiceAllowToUploadFile && Sm.GetGrdStr(Grd1, Row, 32).Length > 0 && Sm.GetGrdStr(Grd1, Row, 32) != "openFileDialog1")
                        {
                            UploadFile(DocNo, Row, Sm.GetGrdStr(Grd1, Row, 32));
                            // IsFile = true;
                        }
                    }
                }
            }

            if (mIsSLIShowDataAfterSave)
            {
                ShowData(DocNo);
                IsInsert = false;
            }
            else
            {
                BtnInsertClick(sender, e);
                IsInsert = true;
            }
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                (mIsCustomerComboBasedOnCategory && !mIsShowCustomerCategory && Sm.IsLueEmpty(LueCtCtCode, "Customer Category")) ||
                Sm.IsLueEmpty(LueCtCode, "Customer") ||
                Sm.IsDteEmpty(DteDueDt, "Due date") ||
                IsTaxInvoiceInvalid() ||
                Sm.IsLueEmpty(LueCurCode, "Currency") ||
                (mIsRemarkForJournalMandatory && Sm.IsMeeEmpty(MeeRemark, "Remark")) ||
                Sm.IsClosingJournalInvalid(Sm.GetDte(DteDocDt)) ||
                IsGrdEmpty() ||
                IsGrdExceedMaxRecords() ||
                IsGrdValueNotValid() ||
                (mIsSalesContractEnabled && IsGrdHasDifferentSite()) ||
                IsDOCtAlreadyCancelled() ||
                IsDOCtAlreadyFulfilled() ||
                IsCurrencyNotValid() ||
                IsDownpaymentNotValid() ||
                IsFacturNumberNotValid() ||
                IsTaxInvDocumentNotValid() ||
                IsJournalAmtNotBalanced() ||
                IsLocalDocNoNotValid() ||
                IsTickedAddCostDiscInvalid() ||
                //(mIsAutoJournalActived && mIsCheckCOAJournalNotExists && IsCOAJournalNotValid()) ||
                IsJournalSettingInvalid() ||
                (mIsSalesInvoiceAllowToUploadFile && IsUploadFileNotValid());
        }

        private bool IsJournalSettingInvalid()
        {
            if (!mIsAutoJournalActived || !mIsJournalValidationSalesInvoiceEnabled) return false;

            string
                CustomerAcNoAR = Sm.GetParameter("CustomerAcNoAR"),
                CustomerAcNoNonInvoice = Sm.GetParameter("CustomerAcNoNonInvoice"),
                AcNoForVATOut = Sm.GetParameter("AcNoForVATOut"),
                CustomerAcNoDownPayment = Sm.GetParameter("CustomerAcNoDownPayment"),
                AcNoForForeignCurrencyExchangeGains = Sm.GetParameter("AcNoForForeignCurrencyExchangeGains");
            var CurCode = Sm.GetLue(LueCurCode);
            var Msg =
                    "Journal's setting is invalid." + Environment.NewLine +
                    "Please contact Finance/Accounting department." + Environment.NewLine;

            //Parameter
            if (CustomerAcNoAR.Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, Msg + "Parameter CustomerAcNoAR is empty.");
                return true;
            }
            if (CustomerAcNoNonInvoice.Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, Msg + "Parameter CustomerAcNoNonInvoice is empty.");
                return true;
            }

            if (TxtDownpayment.Text.Length > 0 && decimal.Parse(TxtDownpayment.Text) != 0)
            {
                if (CustomerAcNoDownPayment.Length == 0)
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Parameter CustomerAcNoDownPayment is empty.");
                    return true;
                }
            }

            if (!Sm.CompareStr(mMainCurCode, CurCode))
            {
                if (AcNoForForeignCurrencyExchangeGains.Length == 0)
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Parameter AcNoForForeignCurrencyExchangeGains is empty.");
                    return true;
                }
            }

            if (mIsSalesInvoiceJournalUseTaxAcNo2)
            {
                if (IsJournalSettingInvalid_Tax(Msg, "AcNo2")) return true;
            }
            else
            {
                if (AcNoForVATOut.Length == 0)
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Parameter AcNoForVATOut is empty.");
                    return true;
                }
            }

            if (IsJournalSettingInvalid_DiscCost(Msg)) return true;

            return false;
        }

        private bool IsJournalSettingInvalid_Tax(string Msg, string AcNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string TaxName = string.Empty;

            SQL.AppendLine("Select TaxName From TblTax Where " + AcNo + " Is Null ");
            SQL.AppendLine("And TaxCode In (@TaxCode1,@TaxCode2,@TaxCode3) ");
            SQL.AppendLine("Limit 1; ");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<string>(ref cm, "@TaxCode1", Sm.GetLue(LueTaxCode1));
            Sm.CmParam<string>(ref cm, "@TaxCode2", Sm.GetLue(LueTaxCode2));
            Sm.CmParam<string>(ref cm, "@TaxCode3", Sm.GetLue(LueTaxCode3));

            TaxName = Sm.GetValue(cm);

            if (TaxName.Length > 0)
            {
                Sm.StdMsg(mMsgType.Warning, Msg + (AcNo == "AcNo" ? "Input" : "Output") + " Tax's COA Account  (" + TaxName + ") is empty.");
                return true;
            }

            return false;
        }

        private bool IsJournalSettingInvalid_DiscCost(string Msg)
        {
            for (int row = 0; row < Grd3.Rows.Count - 1; row++)
            {
                if (Sm.GetGrdStr(Grd3, row, 1).Length == 0)
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Disc, Cost, Etc COA Account "+Sm.GetGrdStr(Grd3, row, 2)+" is empty.");
                    return true;
                }
            }

            return false;
        }

        private bool IsCOAJournalNotValid()
        {

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@TaxCode1", Sm.GetLue(LueTaxCode1));
            Sm.CmParam<String>(ref cm, "@TaxCode2", Sm.GetLue(LueTaxCode2));
            Sm.CmParam<String>(ref cm, "@TaxCode3", Sm.GetLue(LueTaxCode3));
            Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));

            if (Grd3.Rows.Count > 1)
            {
                for (int row = 0; row < Grd3.Rows.Count - 1; row++)
                {
                    Sm.CmParam<String>(ref cm, "@AcNo_" + row, Sm.GetGrdStr(Grd3, row, 1));
                }
                if (mSLITaxCalculationFormat != "1")
                    Sm.CmParam<String>(ref cm, "@CtCode2", string.Concat("%.", Sm.GetLue(LueCtCode)));
                    
            }

            var SQL = new StringBuilder();

            SQL.AppendLine("Select AcNo From ");
            SQL.AppendLine("( ");

            //Piutang Usaha
            SQL.AppendLine("Select ParValue As AcNo ");
            SQL.AppendLine("From TblParameter Where ParCode = 'CustomerAcNoAR' ");

            //Piutang Uninvoice
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select ParValue As AcNo ");
            SQL.AppendLine("From TblParameter Where ParCode = 'CustomerAcNoNonInvoice' ");

            //PPN Keluaran
            if (mIsSalesInvoiceJournalUseTaxAcNo2)
            {
                SQL.AppendLine("Union All ");
                SQL.AppendLine("Select AcNo2 As AcNo ");
                SQL.AppendLine("From TblTax Where TaxCode = @TaxCode1 ");

                SQL.AppendLine("Union All ");
                SQL.AppendLine("Select AcNo2 As AcNo ");
                SQL.AppendLine("From TblTax Where TaxCode = @TaxCode2 ");

                SQL.AppendLine("Union All ");
                SQL.AppendLine("Select AcNo2 As AcNo ");
                SQL.AppendLine("From TblTax Where TaxCode = @TaxCode3 ");
            }
            else
            {
                SQL.AppendLine("Union All ");
                SQL.AppendLine("Select ParValue As AcNo ");
                SQL.AppendLine("Inner Join TblParameter Where ParCode='AcNoForVATOut' ");
            }

            //Downpayment
            if (TxtDownpayment.Text.Length > 0 && decimal.Parse(TxtDownpayment.Text) != 0)
            {
                SQL.AppendLine("Union All ");
                SQL.AppendLine("Select ParValue As AcNo ");
                SQL.AppendLine("From TblParameter Where ParCode = 'CustomerAcNoDownPayment' ");
            }

            //List Of COA
            if (Grd3.Rows.Count > 1)
            {
                SQL.AppendLine("Union All ");
                SQL.AppendLine("Select AcNo From ( ");
                for (int row = 0; row < Grd3.Rows.Count - 1; row++)
                {
                    SQL.AppendLine("Select @AcNo_" + row + " As AcNo ");
                    if (row < Grd3.Rows.Count - 2)
                        SQL.AppendLine("Union All ");
                }
                SQL.AppendLine(") T1 ");
                if (mSLITaxCalculationFormat != "1")
                    SQL.AppendLine("Where T1.AcNo Not Like @CtCode2 ");
            }

            //Laba Rugi Selisih Kurs
            if (!Sm.CompareStr(mMainCurCode, Sm.GetLue(LueCurCode)))
            {
                SQL.AppendLine("Union All ");
                SQL.AppendLine("Select ParValue As AcNo ");
                SQL.AppendLine("From TblParameter Where ParCode = 'AcNoForForeignCurrencyExchangeGains' ");
            }
            SQL.AppendLine(") Tbl ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                {
                    "AcNo",
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        if (Sm.DrStr(dr, c[0]).Length == 0)
                        {
                            Sm.StdMsg(mMsgType.Warning, "There is/are one or more COA Account that not exists for crating journal transaction.");
                            return true;
                        }
                    }
                }
                dr.Close();
            }

            return false;
        }
        

        private bool IsTickedAddCostDiscInvalid()
        {
            if (mSLITaxCalculationFormat == "1") return false;

            decimal mDAmt = 0m, mCAmt = 0m;

            if (Grd3.Rows.Count > 1)
            {
                for (int i = 0; i < Grd3.Rows.Count; ++i)
                {
                    if(Sm.GetGrdStr(Grd3, i, 1).Length > 0)
                    {
                        if (Sm.GetGrdBool(Grd3, i, 3)) // Debit
                            mDAmt += Sm.GetGrdDec(Grd3, i, 4);

                        if (Sm.GetGrdBool(Grd3, i, 5)) // Credit
                            mCAmt += Sm.GetGrdDec(Grd3, i, 6);
                    }
                }

                if (mDAmt != mCAmt && mDAmt != 0m && mCAmt != 0m)
                {
                    Tc1.SelectedTabPage = Tp4;
                    Sm.StdMsg(mMsgType.Warning, "Your chosen Debit and Credit amount should be balanced.");
                    return true;
                }
            }

            return false;
        }

        private bool IsUploadFileNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0)
                {
                    if (mIsSalesInvoiceAllowToUploadFile && Sm.GetGrdStr(Grd1, Row, 32).Length > 0 && Sm.GetGrdStr(Grd1, Row, 32) != "openFileDialog1")
                    {
                        if (IsUploadFileNotValid(Row, Sm.GetGrdStr(Grd1, Row, 32))) return true;
                    }
                }
            }

            return false;
        }

        private bool IsTaxInvoiceInvalid()
        {
            if ((TxtTaxInvDocument.Text.Length > 0 || mTaxInvDocument.Length > 0) && DteTaxInvDt.Text.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Tax Invoice Date should not be empty.");
                DteTaxInvDt.Focus();
                return true;
            }

            return false;
        }

        private bool IsFacturNumberNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count-1; Row++)
            {
                if (Sm.GetGrdDec(Grd1, Row, 25) > 0)
                    if (!mIsSLIUseAutoFacturNumber && Sm.IsTxtEmpty(TxtTaxInvDocument, "Factur number", false)) return true;       
            }
            return false;
        }

        private bool IsLocalDocNoNotValid()
        {
            var LocalDocNo = TxtLocalDocNo.Text;
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (!Sm.CompareStr(LocalDocNo, Sm.GetGrdStr(Grd1, Row, 29)))
                {
                    return (Sm.StdMsgYN("Question", 
                        "Sales invoice's local document should be the same as all existing PL/DR local document." + 
                        Environment.NewLine +
                        "Do you want to continue ?"
                        )== DialogResult.No);
                }
            }
            return false;
        }

        private bool IsTaxInvDocumentNotValid()
        {
            bool IsTaxable = TxtTaxInvDocument.Text.Length > 0;
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0)
                {
                    if (!mIsSLIUseAutoFacturNumber)
                    {
                        if (IsTaxable && Sm.GetGrdDec(Grd1, Row, 24) <= 0)
                        {
                            Sm.StdMsg(mMsgType.Warning,
                                "DO# : " + Sm.GetGrdStr(Grd1, Row, 2) + Environment.NewLine +
                                "Item Code : " + Sm.GetGrdStr(Grd1, Row, 15) + Environment.NewLine +
                                "Item Name : " + Sm.GetGrdStr(Grd1, Row, 17) + Environment.NewLine +
                                "Tax Rate : " + Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 24), 0) + Environment.NewLine + Environment.NewLine +
                                "You can't choose nontaxable DO#."
                                );
                            return true;
                        }

                        if (!IsTaxable && Sm.GetGrdDec(Grd1, Row, 24) > 0)
                        {
                            Sm.StdMsg(mMsgType.Warning,
                                "DO# : " + Sm.GetGrdStr(Grd1, Row, 2) + Environment.NewLine +
                                "Item Code : " + Sm.GetGrdStr(Grd1, Row, 15) + Environment.NewLine +
                                "Item Name : " + Sm.GetGrdStr(Grd1, Row, 17) + Environment.NewLine +
                                "Tax Rate : " + Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 24), 0) + Environment.NewLine + Environment.NewLine +
                                "You can't choose taxable DO#."
                                );
                            return true;
                        }
                    }
                    else
                    {
                        if (mTaxInvDocument.Length > 0 && Sm.GetGrdDec(Grd1, Row, 24) <= 0)
                        {
                            Sm.StdMsg(mMsgType.Warning,
                                "DO# : " + Sm.GetGrdStr(Grd1, Row, 2) + Environment.NewLine +
                                "Item Code : " + Sm.GetGrdStr(Grd1, Row, 15) + Environment.NewLine +
                                "Item Name : " + Sm.GetGrdStr(Grd1, Row, 17) + Environment.NewLine +
                                "Tax Rate : " + Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 24), 0) + Environment.NewLine + Environment.NewLine +
                                "You can't choose nontaxable DO#."
                                );
                            return true;
                        }

                        if (mTaxInvDocument.Length <= 0 && Sm.GetGrdDec(Grd1, Row, 24) > 0)
                        {
                            Sm.StdMsg(mMsgType.Warning,
                                "DO# : " + Sm.GetGrdStr(Grd1, Row, 2) + Environment.NewLine +
                                "Item Code : " + Sm.GetGrdStr(Grd1, Row, 15) + Environment.NewLine +
                                "Item Name : " + Sm.GetGrdStr(Grd1, Row, 17) + Environment.NewLine +
                                "Tax Rate : " + Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 24), 0) + Environment.NewLine + Environment.NewLine +
                                "You can't choose taxable DO#."
                                );
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        private bool IsDocumentTypeNotValid()
        {
            string DocType = Sm.GetGrdStr(Grd1, 0, 29);
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (
                    Sm.GetGrdStr(Grd1, Row, 2).Length > 0 &&
                    !Sm.CompareStr(DocType, Sm.GetGrdStr(Grd1, Row, 29))
                    )
                {
                    Sm.StdMsg(mMsgType.Warning,
                            "DO# : " + Sm.GetGrdStr(Grd1, Row, 2) + Environment.NewLine +
                            "Item Code : " + Sm.GetGrdStr(Grd1, Row, 15) + Environment.NewLine +
                            "Item Name : " + Sm.GetGrdStr(Grd1, Row, 17) + Environment.NewLine +
                            "Type : " + DocType=="1"?"Oversea DO":"Local DO" + Environment.NewLine + Environment.NewLine +
                            "You can't choose nontaxable DO#."
                            );

                    return true;
                }
            }
            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 DO.");
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning, "DO request/Packing list entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            if (Grd3.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning, "Additional cost, discount information entered (" + (Grd3.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsGrdHasDifferentSite()
        {
            string SiteCode = Sm.GetValue("SELECT SiteCode FROM TblSalesMemoHdr WHERE DocNo = @Param Limit 1; ", Sm.GetGrdStr(Grd1, 0, 12));
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 36) != SiteCode)
                {
                    Sm.StdMsg(mMsgType.Warning,
                    "DO# :" + Sm.GetGrdStr(Grd1, Row, 2) + Environment.NewLine +
                    "Item's Code :" + Sm.GetGrdStr(Grd1, Row, 15) + Environment.NewLine +
                    "Item's Name :" + Sm.GetGrdStr(Grd1, Row, 17) + Environment.NewLine + Environment.NewLine +
                    "This document has different site.");
                    return true;
                }
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            if (Grd3.Rows.Count > 1)
            {
                var AcType = string.Empty;

                for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                {
                    if (Sm.IsGrdValueEmpty(Grd3, Row, 1, false, "COA's account is empty.")) return true;
                    if (Sm.GetGrdDec(Grd3, Row, 4) == 0m && Sm.GetGrdDec(Grd3, Row, 6) == 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "Acount No : " + Sm.GetGrdStr(Grd3, Row, 1) + Environment.NewLine +
                            "Account Description : " + Sm.GetGrdStr(Grd3, Row, 2) + Environment.NewLine + Environment.NewLine +
                            "Both debit and credit amount can't be 0.");
                        return true;
                    }
                    if (Sm.GetGrdDec(Grd3, Row, 4) != 0m && Sm.GetGrdDec(Grd3, Row, 6) != 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "Acount No : " + Sm.GetGrdStr(Grd3, Row, 1) + Environment.NewLine +
                            "Account Description : " + Sm.GetGrdStr(Grd3, Row, 2) + Environment.NewLine + Environment.NewLine +
                            "Both debit and credit amount both can't be bigger than 0.");
                        return true;
                    }
                }
            }

            var ldtl = new List<SLI>();
            var cmDtl = new MySqlCommand();
            var SQLDtl = new StringBuilder();
            using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl.Open();
                cmDtl.Connection = cnDtl;

                SQLDtl.AppendLine("Select Distinct DOCtDocNo, DOCtDNo " +
                    "From TblSalesInvoiceDtl X1  " +
                    "Inner Join TblSalesInvoiceHdr X2 On X1.Docno = X2.DocNo  " +
                    "And X2.Status In('A', 'O')  " +
                    "And X2.CancelInd = 'N'; ");

                cmDtl.CommandText = SQLDtl.ToString();

                var drDtl = cmDtl.ExecuteReader();
                var cDtl = Sm.GetOrdinal(drDtl, new string[]
                { 
                    "DOCtDocNo" , "DOCtDNo" 
                });
                if (drDtl.HasRows)
                {
                    while (drDtl.Read())
                    {
                        ldtl.Add(new SLI()
                        {
                            DocNo = Sm.DrStr(drDtl, cDtl[0]),
                            DNo = Sm.DrStr(drDtl, cDtl[1]),
                        });
                    }
                }
                drDtl.Close();
            }

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                string DOCtDocNo = Sm.GetGrdStr(Grd1, Row, 2);
                string DOCtDNo = Sm.GetGrdStr(Grd1, Row, 3);

                if (ldtl.Exists(x => x.DocNo == DOCtDocNo && x.DNo == DOCtDNo))
                {
                    Sm.StdMsg(mMsgType.Warning,
                        "DO# : " + Sm.GetGrdStr(Grd1, Row, 2) + Environment.NewLine +
                        "DO D# : " + Sm.GetGrdStr(Grd1, Row, 3) + Environment.NewLine + Environment.NewLine +
                        "Already used in another active document.");
                    return true;
                }
            }
            ldtl.Clear();

            return false;
        }

        private bool IsDOCtAlreadyCancelled()
        {
            string Msg = string.Empty;
            if (Grd1.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length != 0)
                    {
                        Msg =
                            "DO# : " + Sm.GetGrdStr(Grd1, Row, 2) + Environment.NewLine +
                            "Item's Code : " + Sm.GetGrdStr(Grd1, Row, 15) + Environment.NewLine + 
                            "Item's Name : " + Sm.GetGrdStr(Grd1, Row, 17) + Environment.NewLine + Environment.NewLine;

                        if (IsDOCtAlreadyCancelled(Row))
                        {
                            Sm.StdMsg(mMsgType.Warning, Msg + "Some items already cancelled.");
                            Sm.FocusGrd(Grd1, Row, 2);
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        private bool IsDOCtAlreadyCancelled(int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo From (Select A.DocNo, Cast(((B.Qty/C.QtyInventory)*C.Qty) As Decimal(12, 4)) As Qty ");
            SQL.AppendLine("From TblDOCt2Hdr A ");
            SQL.AppendLine("Inner Join TblDOCt2Dtl2 B On A.DocNo=B.DocNo And B.DNo=@DNo ");
            SQL.AppendLine("Inner Join TblDRDtl C On A.DRDocNo=C.DocNo And B.DRDNo=C.DNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo) T Where Qty<Cast(@Qty As Decimal(12, 4))");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select DocNo From (Select A.DocNo, Cast(((B.Qty/C.QtyInventory)*C.Qty) As Decimal(12, 4)) As Qty ");
            SQL.AppendLine("From TblDOCt2Hdr A ");
            SQL.AppendLine("Inner Join TblDOCt2Dtl3 B On A.DocNo=B.DocNo And B.DNo=@DNo ");
            SQL.AppendLine("Inner Join TblPLDtl C On A.PLDocNo=C.DocNo And B.PLDNo=C.DNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo) T Where Qty<Cast(@Qty As Decimal(12, 4));");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<String>(ref cm, "@DNo", Sm.GetGrdStr(Grd1, Row, 3));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 20));
            return Sm.IsDataExist(cm);
        }

        private bool IsDOCtAlreadyFulfilled()
        {
            string Msg = string.Empty;
            if (Grd1.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length != 0)
                    {
                        Msg =
                            "DO# : " + Sm.GetGrdStr(Grd1, Row, 2) + Environment.NewLine +
                            "Item's Code : " + Sm.GetGrdStr(Grd1, Row, 15) + Environment.NewLine +
                            "Item's Name : " + Sm.GetGrdStr(Grd1, Row, 17) + Environment.NewLine + Environment.NewLine;

                        if (IsDOCtAlreadyFulfilled(Row))
                        {
                            Sm.StdMsg(mMsgType.Warning, Msg + "This document already fulfilled.");
                            Sm.FocusGrd(Grd1, Row, 2);
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        private bool IsDOCtAlreadyFulfilled(int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo From TblDOCt2Dtl2 ");
            SQL.AppendLine("Where ProcessInd='F' And DocNo=@DocNo And DNo=@DNo ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select DocNo From TblDOCt2Dtl3 ");
            SQL.AppendLine("Where ProcessInd='F' And DocNo=@DocNo And DNo=@DNo;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<String>(ref cm, "@DNo", Sm.GetGrdStr(Grd1, Row, 3));
            return Sm.IsDataExist(cm);
        }

        private bool IsCurrencyNotValid()
        {
            string CurCode = Sm.GetLue(LueCurCode);
            if (Grd1.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (!Sm.CompareStr(CurCode, Sm.GetGrdStr(Grd1, Row, 22)))
                    {
                        Sm.StdMsg(mMsgType.Warning, "One sales invoice# only allowed 1 currency type.");
                        return true;
                    }
                }
            }
            return false;
        }

        private bool IsDownpaymentNotValid()
        {
            decimal Downpayment = decimal.Parse(TxtDownpayment.Text);

            if (Downpayment == 0m) return false;

            //Recompute Deposit
            ShowCustomerDepositSummary(Sm.GetLue(LueCtCode));

            //Get Currency
            decimal Deposit = 0m;
            string CurCode = Sm.GetLue(LueCurCode);

            //Get Deposit Amount Based on currency
            if (Grd2.Rows.Count > 0)
            {
                for (int row = 0; row < Grd2.Rows.Count - 1; row++)
                {
                    if (Sm.CompareStr(CurCode, Sm.GetGrdStr(Grd2, row, 0)))
                    {
                        Deposit = Sm.GetGrdDec(Grd2, row, 1);
                        break;
                    }
                }
            }

            if (Downpayment > Deposit)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Currency : " + CurCode + Environment.NewLine +
                    "Deposit Amount: " + Sm.FormatNum(Deposit, 0) + Environment.NewLine +
                    "Downpayment Amount: " + Sm.FormatNum(Downpayment, 0) + Environment.NewLine + Environment.NewLine +
                    "Downpayment is bigger than existing deposit."
                    );
                return true;
            }
            return false;
        }

        private bool IsJournalAmtNotBalanced()
        {
            decimal Debit = 0m, Credit = 0m;

            for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd3, Row, 4).Length > 0) Debit += Sm.GetGrdDec(Grd3, Row, 4);
                if (Sm.GetGrdStr(Grd3, Row, 6).Length > 0) Credit += Sm.GetGrdDec(Grd3, Row, 6);
            }

            if (Debit != Credit)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Total Debit : " + Sm.FormatNum(Debit, 0) + Environment.NewLine +
                    "Total Credit : " + Sm.FormatNum(Credit, 0) + Environment.NewLine + Environment.NewLine +
                    "Total debit and credit is not balanced."
                    );
                return true;
            }
            return false;
        }

        public MySqlCommand SaveCustomerDeposit(string DocNo, bool IsNeedApproval)
        {
            var SQL = new StringBuilder();

            #region Old Code
            //SQL.AppendLine("Insert Into TblCustomerDepositMovement(DocNo, DocType, DocDt, CtCode, CurCode, Amt, CreateBy, CreateDt) ");
            //SQL.AppendLine("Select DocNo, ");
            //SQL.AppendLine("(Case @CancelInd When 'Y' Then '04' Else '03' End) As DocType, ");
            //SQL.AppendLine("DocDt, CtCode, CurCode, (Case @CancelInd When 'Y' Then 1 Else -1 End)*@Amt, @UserCode, CurrentDateTime() ");
            //SQL.AppendLine("From TblSalesInvoiceHdr");
            //SQL.AppendLine("Where DocNo=@DocNo ");
            //if (IsNeedApproval) SQL.AppendLine("And status='A' ");
            //SQL.AppendLine("; ");

            //SQL.AppendLine("Insert Into TblCustomerDepositSummary(CtCode, CurCode, Amt, CreateBy, CreateDt) ");
            //SQL.AppendLine("Select @CtCode, @CurCode, @Amt, @UserCode, CurrentDateTime() ");
            //if (IsNeedApproval) SQL.AppendLine("From TblSalesInvoiceHdr T Where T.DocNo = @DocNo And T.Status = 'A' ");
            //SQL.AppendLine("On Duplicate Key ");
            //SQL.AppendLine("   Update TblCustomerDepositSummary.Amt=TblCustomerDepositSummary.Amt+((Case @CancelInd When 'Y' Then 1 Else -1 End)*@Amt), TblCustomerDepositSummary.LastUpBy=@UserCode, TblCustomerDepositSummary.LastUpDt=CurrentDateTime(); ");
            #endregion

            SQL.AppendLine("Insert Into TblCustomerDepositMovement(DocNo, DocType, DocDt, CtCode, CurCode, Amt, CreateBy, CreateDt) ");
            SQL.AppendLine("Select DocNo, ");
            SQL.AppendLine("(Case CancelInd When 'Y' Then '04' Else '03' End) As DocType, ");
            SQL.AppendLine("DocDt, CtCode, CurCode, (Case CancelInd When 'Y' Then 1 Else -1 End)*DownPayment as Amt, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblSalesInvoiceHdr ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            if (IsNeedApproval) SQL.AppendLine("And status='A' ");
            SQL.AppendLine("; ");

            SQL.AppendLine("Insert Into TblCustomerDepositSummary(CtCode, CurCode, Amt, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.CtCode, A.CurCode, A.DownPayment as Amt, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblSalesInvoiceHdr A Where DocNo=@DocNo ");
            if (IsNeedApproval) SQL.AppendLine("And status='A' ");
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("   Update tblcustomerdepositsummary.Amt=tblcustomerdepositsummary.Amt+((Case A.CancelInd When 'Y' Then 1 Else -1 End)*A.DownPayment), tblcustomerdepositsummary.LastUpBy=@UserCode, tblcustomerdepositsummary.LastUpDt=CurrentDateTime(); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            //Sm.CmParam<String>(ref cm, "@CancelInd", (ChkCancelInd.Checked) ? "Y" : "N");
            //Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
            //Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            //Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtDownpayment.Text));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveSalesInvoiceHdr(string DocNo, string ReceiptNo, bool IsNeedApproval)
        {
            string Doctitle = Sm.GetParameter("Doctitle");
            var SQL = new StringBuilder();

            SQL.AppendLine("/* Sales Invoice (Hdr) */ ");
            SQL.AppendLine("Insert Into TblSalesInvoiceHdr(DocNo, ReceiptNo, DocDt, CancelInd, ProcessInd, CBDInd, CtCode, DueDt, LocalDocNo, TaxInvDocument, TaxInvDt, ");
            if (IsNeedApproval) SQL.AppendLine("Status, ");
            SQL.AppendLine("TaxInvoiceNo, TaxInvoiceNo2, TaxInvoiceNo3, ");
            SQL.AppendLine("TaxInvoiceDt, TaxInvoiceDt2, TaxInvoiceDt3, ");
            SQL.AppendLine("TaxCode1, TaxCode2, TaxCode3, ");
            SQL.AppendLine("TaxAlias1, TaxAlias2, TaxAlias3, ");
            SQL.AppendLine("TaxAmt1, TaxAmt2, TaxAmt3, ");
            SQL.AppendLine("CurCode, TotalAmt, TotalTax, Downpayment, Amt, AdditionalCostDiscAmt, MInd, BankAcCode, SalesName, SignName, JournalDocNo, JournalDocNo2, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @ReceiptNo, @DocDt, 'N', 'O', 'N', @CtCode, @DueDt, @LocalDocNo, @TaxInvDocument, @TaxInvDt, ");
            if (IsNeedApproval) SQL.AppendLine("@Status, ");
            SQL.AppendLine("@TaxInvoiceNo, @TaxInvoiceNo2, @TaxInvoiceNo3, ");
            SQL.AppendLine("@TaxInvoiceDt, @TaxInvoiceDt2, @TaxInvoiceDt3, ");
            SQL.AppendLine("@TaxCode1, @TaxCode2, @TaxCode3, ");
            SQL.AppendLine("@TaxAlias1, @TaxAlias2, @TaxAlias3, ");
            SQL.AppendLine("@TaxAmt1, @TaxAmt2, @TaxAmt3, ");
            SQL.AppendLine("@CurCode, @TotalAmt, @TotalTax, @Downpayment, @Amt, @AdditionalCostDiscAmt, @MInd, @BankAcCode, @SalesName, @SignName, Null, Null, @Remark, @UserCode, CurrentDateTime());");

            var cm = new MySqlCommand(){ CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            //if(Doctitle=="KIM")
            //Sm.CmParam<String>(ref cm, "@ReceiptNo", ReceiptNo);
            //else
            //    Sm.CmParam<String>(ref cm, "@ReceiptNo", string.Empty);
            Sm.CmParam<String>(ref cm, "@ReceiptNo", ReceiptNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@Status", IsNeedApproval ? "O" : "A");
            Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
            Sm.CmParamDt(ref cm, "@DueDt", Sm.GetDte(DteDueDt));
            Sm.CmParam<String>(ref cm, "@LocalDocNo", TxtLocalDocNo.Text);
            if(mIsSLIUseAutoFacturNumber)
                Sm.CmParam<String>(ref cm, "@TaxInvDocument", mTaxInvDocument);
            else
                Sm.CmParam<String>(ref cm, "@TaxInvDocument", TxtTaxInvDocument.Text);
            Sm.CmParamDt(ref cm, "@TaxInvDt", Sm.GetDte(DteTaxInvDt));
            Sm.CmParam<String>(ref cm, "@TaxInvoiceNo", TxtTaxInvoiceNo.Text);
            Sm.CmParam<String>(ref cm, "@TaxInvoiceNo2", TxtTaxInvoiceNo2.Text);
            Sm.CmParam<String>(ref cm, "@TaxInvoiceNo3", TxtTaxInvoiceNo3.Text);
            Sm.CmParamDt(ref cm, "@TaxInvoiceDt", Sm.GetDte(DteTaxInvoiceDt));
            Sm.CmParamDt(ref cm, "@TaxInvoiceDt2", Sm.GetDte(DteTaxInvoiceDt2));
            Sm.CmParamDt(ref cm, "@TaxInvoiceDt3", Sm.GetDte(DteTaxInvoiceDt3));
            Sm.CmParam<String>(ref cm, "@TaxCode1", Sm.GetLue(LueTaxCode1));
            Sm.CmParam<String>(ref cm, "@TaxCode2", Sm.GetLue(LueTaxCode2));
            Sm.CmParam<String>(ref cm, "@TaxCode3", Sm.GetLue(LueTaxCode3));
            Sm.CmParam<String>(ref cm, "@TaxAlias1", TxtAlias1.Text);
            Sm.CmParam<String>(ref cm, "@TaxAlias2", TxtAlias2.Text);
            Sm.CmParam<String>(ref cm, "@TaxAlias3", TxtAlias3.Text);
            Sm.CmParam<Decimal>(ref cm, "@TaxAmt1", decimal.Parse(TxtTaxAmt1.Text));
            Sm.CmParam<Decimal>(ref cm, "@TaxAmt2", decimal.Parse(TxtTaxAmt2.Text));
            Sm.CmParam<Decimal>(ref cm, "@TaxAmt3", decimal.Parse(TxtTaxAmt3.Text));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<Decimal>(ref cm, "@TotalAmt", decimal.Parse(TxtTotalAmt.Text));
            Sm.CmParam<Decimal>(ref cm, "@TotalTax", decimal.Parse(TxtTotalTax.Text));
            Sm.CmParam<Decimal>(ref cm, "@Downpayment", decimal.Parse(TxtDownpayment.Text));
            Sm.CmParam<Decimal>(ref cm, "@Amt", decimal.Parse(TxtAmt.Text));
            Sm.CmParam<Decimal>(ref cm, "@AdditionalCostDiscAmt", mSLITaxCalculationFormat == "1" ? 0m : mAdditionalCostDiscAmt);
            Sm.CmParam<String>(ref cm, "@SignName", TxtSignName.Text);
            if(mMIndForSales)
                Sm.CmParam<String>(ref cm, "@MInd",
                     TxtTaxInvDocument.Text.Length == 0 && Sm.GetGrdStr(Grd1, 0, 30) == "1" ?"Y" : "N");
            else
                Sm.CmParam<String>(ref cm, "@MInd", "N");
            
            Sm.CmParam<String>(ref cm, "@SalesName", Sm.GetValue("Select SpName From tblSalesPerson Where SpCode = '" + Sm.GetLue(LueSPCode) + "' "));
            Sm.CmParam<String>(ref cm, "@BankAcCode", Sm.GetLue(LueBankAcCode));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveSalesInvoiceDtl(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* Sales Invoice (Dtl) */ ");
            SQL.AppendLine("Set @Dt:=CurrentDateTime();");

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 2).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblSalesInvoiceDtl(DocNo, DNo, DocType, DOCtDocNo, DOCtDNo, ProcessInd, ItCode, QtyPackagingUnit, Qty, UPriceBeforeTax, TaxRate, TaxAmt, UPriceAfterTax, ");
                        if (mSalesInvoiceTaxCalculationFormula != "1")
                            SQL.AppendLine("TaxInd1, TaxInd2, TaxInd3, TaxAmt1, TaxAmt2, TaxAmt3, TaxAmtTotal, ");
                        SQL.AppendLine("CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");
                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");
                    SQL.AppendLine(
                        " (@DocNo, @DNo_" + r.ToString() +
                        ", @DocType_" + r.ToString() +
                        ", @DOCtDocNo_" + r.ToString() +
                        ", @DOCtDNo_" + r.ToString() +
                        ", 'O', @ItCode_" + r.ToString() +
                        ", @QtyPackagingUnit_" + r.ToString() +
                        ", @Qty_" + r.ToString() +
                        ", @UPriceBeforeTax_" + r.ToString() +
                        ", @TaxRate_" + r.ToString() +
                        ", @TaxAmt_" + r.ToString() +
                        ", @UPriceAfterTax_" + r.ToString());
                    if (mSalesInvoiceTaxCalculationFormula != "1")
                        SQL.AppendLine(
                            ", @TaxInd1_" + r.ToString() +
                            ", @TaxInd2_" + r.ToString() +
                            ", @TaxInd3_" + r.ToString() +
                            ", @TaxAmt1_" + r.ToString() +
                            ", @TaxAmt2_" + r.ToString() +
                            ", @TaxAmt3_" + r.ToString() +
                            ", @TaxAmtTotal_" + r.ToString());

                    SQL.AppendLine(", @UserCode, @Dt) ");
                    
                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("000" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@DocType_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 30));
                    Sm.CmParam<String>(ref cm, "@DOCtDocNo_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 2));
                    Sm.CmParam<String>(ref cm, "@DOCtDNo_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 3));
                    Sm.CmParam<String>(ref cm, "@ItCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 15));
                    Sm.CmParam<Decimal>(ref cm, "@QtyPackagingUnit_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 18));
                    Sm.CmParam<Decimal>(ref cm, "@Qty_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 20));
                    Sm.CmParam<Decimal>(ref cm, "@UPriceBeforeTax_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 23));
                    Sm.CmParam<Decimal>(ref cm, "@TaxRate_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 24));
                    Sm.CmParam<Decimal>(ref cm, "@TaxAmt_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 25));
                    Sm.CmParam<Decimal>(ref cm, "@UPriceAfterTax_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 26));
                    if (mSalesInvoiceTaxCalculationFormula != "1")
                    {
                        Sm.CmParam<String>(ref cm, "@TaxInd1_" + r.ToString(), Sm.GetGrdBool(Grd1, r, 37) ? "Y" : "N");
                        Sm.CmParam<String>(ref cm, "@TaxInd2_" + r.ToString(), Sm.GetGrdBool(Grd1, r, 38) ? "Y" : "N");
                        Sm.CmParam<String>(ref cm, "@TaxInd3_" + r.ToString(), Sm.GetGrdBool(Grd1, r, 39) ? "Y" : "N");
                        Sm.CmParam<Decimal>(ref cm, "@TaxAmt1_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 42));
                        Sm.CmParam<Decimal>(ref cm, "@TaxAmt2_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 43));
                        Sm.CmParam<Decimal>(ref cm, "@TaxAmt3_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 44));
                        Sm.CmParam<Decimal>(ref cm, "@TaxAmtTotal_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 40));
                    }
                }
            }
            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        //private MySqlCommand SaveSalesInvoiceDtl(string DocNo, int Row)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Insert Into TblSalesInvoiceDtl(DocNo, DNo, DocType, DOCtDocNo, DOCtDNo, ProcessInd, ItCode, QtyPackagingUnit, Qty, UPriceBeforeTax, TaxRate, TaxAmt, UPriceAfterTax, ");

        //    if (mSalesInvoiceTaxCalculationFormula != "1")
        //        SQL.AppendLine("TaxInd1, TaxInd2, TaxInd3, TaxAmt1, TaxAmt2, TaxAmt3, TaxAmtTotal, ");

        //    SQL.AppendLine("CreateBy, CreateDt) ");
        //    SQL.AppendLine("Values(@DocNo, @DNo, @DocType, @DOCtDocNo, @DOCtDNo, 'O', @ItCode, @QtyPackagingUnit, @Qty, @UPriceBeforeTax, @TaxRate, @TaxAmt, @UPriceAfterTax, ");

        //    if (mSalesInvoiceTaxCalculationFormula != "1")
        //        SQL.AppendLine("@TaxInd1, @TaxInd2, @TaxInd3, @TaxAmt1, @TaxAmt2, @TaxAmt3, @TaxAmtTotal, ");

        //    SQL.AppendLine("@CreateBy, CurrentDateTime()); ");

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };
        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
        //    Sm.CmParam<String>(ref cm, "@DocType", Sm.GetGrdStr(Grd1, Row, 30));
        //    Sm.CmParam<String>(ref cm, "@DOCtDocNo", Sm.GetGrdStr(Grd1, Row, 2));
        //    Sm.CmParam<String>(ref cm, "@DOCtDNo", Sm.GetGrdStr(Grd1, Row, 3));
        //    Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 15));
        //    Sm.CmParam<Decimal>(ref cm, "@QtyPackagingUnit", Sm.GetGrdDec(Grd1, Row, 18));
        //    Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 20));
        //    Sm.CmParam<Decimal>(ref cm, "@UPriceBeforeTax", Sm.GetGrdDec(Grd1, Row, 23));
        //    Sm.CmParam<Decimal>(ref cm, "@TaxRate", Sm.GetGrdDec(Grd1, Row, 24));
        //    Sm.CmParam<Decimal>(ref cm, "@TaxAmt", Sm.GetGrdDec(Grd1, Row, 25));
        //    Sm.CmParam<Decimal>(ref cm, "@UPriceAfterTax", Sm.GetGrdDec(Grd1, Row, 26));
        //    if (mSalesInvoiceTaxCalculationFormula != "1")
        //    {
        //        Sm.CmParam<String>(ref cm, "@TaxInd1", Sm.GetGrdBool(Grd1, Row, 37) ? "Y" : "N");
        //        Sm.CmParam<String>(ref cm, "@TaxInd2", Sm.GetGrdBool(Grd1, Row, 38) ? "Y" : "N");
        //        Sm.CmParam<String>(ref cm, "@TaxInd3", Sm.GetGrdBool(Grd1, Row, 39) ? "Y" : "N");
        //        Sm.CmParam<Decimal>(ref cm, "@TaxAmt1", Sm.GetGrdDec(Grd1, Row, 42));
        //        Sm.CmParam<Decimal>(ref cm, "@TaxAmt2", Sm.GetGrdDec(Grd1, Row, 43));
        //        Sm.CmParam<Decimal>(ref cm, "@TaxAmt3", Sm.GetGrdDec(Grd1, Row, 44));
        //        Sm.CmParam<Decimal>(ref cm, "@TaxAmtTotal", Sm.GetGrdDec(Grd1, Row, 40));
        //    }
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    return cm;
        //}

        private MySqlCommand SaveSalesInvoiceDtl2(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* Sales Invoice (Dtl2) */ ");
            SQL.AppendLine("Set @Dt:=CurrentDateTime();");

            for (int r = 0; r < Grd3.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd3, r, 1).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblSalesInvoiceDtl2(DocNo, DNo, AcNo, DAmt, CAmt, OptAcDesc, AcInd, Remark, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");
                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");
                    SQL.AppendLine(
                        " (@DocNo, @DNo_" + r.ToString() +
                        ", @AcNo_" + r.ToString() +
                        ", @DAmt_" + r.ToString() +
                        ", @CAmt_" + r.ToString() +
                        ", @OptAcDesc_" + r.ToString() +
                        ", @AcInd_" + r.ToString() +
                        ", @Remark_" + r.ToString() +
                        ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("000" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@AcNo_" + r.ToString(), Sm.GetGrdStr(Grd3, r, 1));
                    Sm.CmParam<Decimal>(ref cm, "@DAmt_" + r.ToString(), Sm.GetGrdDec(Grd3, r, 4));
                    Sm.CmParam<Decimal>(ref cm, "@CAmt_" + r.ToString(), Sm.GetGrdDec(Grd3, r, 6));
                    Sm.CmParam<String>(ref cm, "@OptAcDesc_" + r.ToString(), Sm.GetGrdStr(Grd3, r, 7));
                    Sm.CmParam<String>(ref cm, "@Remark_" + r.ToString(), Sm.GetGrdStr(Grd3, r, 9));
                    Sm.CmParam<String>(ref cm, "@AcInd_" + r.ToString(), Sm.GetGrdBool(Grd3, r, 10) ? "Y" : "N");
                }
            }
            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;            
        }

        //private MySqlCommand SaveSalesInvoiceDtl2(string DocNo, int Row)
        //{
        //    var cm = new MySqlCommand()
        //    {
        //        CommandText =
        //            "Insert Into TblSalesInvoiceDtl2(DocNo, DNo, AcNo, DAmt, CAmt, OptAcDesc,  AcInd,  Remark, CreateBy, CreateDt) " +
        //            "Values(@DocNo, @DNo, @AcNo, @DAmt, @CAmt, @OptAcDesc,  @AcInd, @Remark, @CreateBy, CurrentDateTime()); "
        //    };
        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
        //    Sm.CmParam<String>(ref cm, "@AcNo", Sm.GetGrdStr(Grd3, Row, 1));
        //    Sm.CmParam<Decimal>(ref cm, "@DAmt", Sm.GetGrdDec(Grd3, Row, 4));
        //    Sm.CmParam<Decimal>(ref cm, "@CAmt", Sm.GetGrdDec(Grd3, Row, 6));
        //    Sm.CmParam<String>(ref cm, "@OptAcDesc", Sm.GetGrdStr(Grd3, Row, 7));
        //    Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd3, Row, 9));
        //    Sm.CmParam<String>(ref cm, "@AcInd", Sm.GetGrdBool(Grd3, Row, 10) ? "Y" : "N");
        //    //if (Sm.GetGrdStr(Grd3, Row, 10) == "1")
        //    //{
        //    //    Sm.CmParam<String>(ref cm, "@AcInd", "Y");
        //    //}
        //    //else
        //    //{
        //    //    Sm.CmParam<String>(ref cm, "@AcInd", "N");
        //    //}
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    return cm;
        //}

        private MySqlCommand SaveSalesInvoiceDtl3(string DocNo, DepositSummary i, bool IsNeedApproval)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("/* Sales Invoice (Dtl3) */ ");
            SQL.AppendLine("Set @Dt:=CurrentDateTime();");

            SQL.AppendLine("Insert Into TblSalesInvoiceDtl3 ");
            SQL.AppendLine("(DocNo, CurCode, ExcRate, Amt, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocNo, @CurCode, @ExcRate, @Amt, @UserCode, @Dt ");
            SQL.AppendLine("From TblSalesInvoiceHdr Where DocNo=@DocNo ");
            if (IsNeedApproval) SQL.AppendLine("And Status='A' ");
            SQL.AppendLine("; ");

            SQL.AppendLine("Update TblCustomerDepositSummary2 A ");
            if (IsNeedApproval) SQL.AppendLine("Inner Join TblSalesInvoiceHdr B On B.DocNo = @DocNo And B.Status = 'A' ");
            SQL.AppendLine("Set ");
            SQL.AppendLine("    A.Amt=A.Amt-@Amt, A.LastUpBy=@UserCode, A.LastUpDt=@Dt ");
            SQL.AppendLine("Where A.CtCode=@CtCode ");
            SQL.AppendLine("And A.CurCode=@CurCode ");
            SQL.AppendLine("And A.ExcRate=@ExcRate; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<Decimal>(ref cm, "@ExcRate", i.ExcRate);
            Sm.CmParam<Decimal>(ref cm, "@Amt", i.UsedAmt);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        public MySqlCommand UpdateDOCtProcessInd(string DocNo, string CancelInd, bool IsNeedApproval)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblDOCt2Dtl2 A ");
            SQL.AppendLine("Inner Join TblSalesInvoiceDtl B ");
            SQL.AppendLine("    On A.DocNo=B.DOCtDocNo ");
            SQL.AppendLine("    And A.DNo=B.DOCtDNo ");
            SQL.AppendLine("    And B.DocType='1' ");
            SQL.AppendLine("    And B.DocNo=@DocNo ");
            if (IsNeedApproval) SQL.AppendLine("Inner Join TblSalesInvoiceHdr C On B.DocNo = C.DocNo And C.DocNo = @DocNo And C.Status = 'A' ");
            SQL.AppendLine("Set A.ProcessInd=@ProcessIndNew ");
            SQL.AppendLine("Where A.ProcessInd=@ProcessIndOld; ");

            SQL.AppendLine("Update TblDOCt2Dtl3 A ");
            SQL.AppendLine("Inner Join TblSalesInvoiceDtl B ");
            SQL.AppendLine("    On A.DocNo=B.DOCtDocNo ");
            SQL.AppendLine("    And A.DNo=B.DOCtDNo ");
            SQL.AppendLine("    And B.DocType='2' ");
            SQL.AppendLine("    And B.DocNo=@DocNo ");
            if (IsNeedApproval) SQL.AppendLine("Inner Join TblSalesInvoiceHdr C On B.DocNo = C.DocNo And C.DocNo = @DocNo And C.Status = 'A' ");
            SQL.AppendLine("Set A.ProcessInd=@ProcessIndNew ");
            SQL.AppendLine("Where A.ProcessInd=@ProcessIndOld; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@ProcessIndOld", CancelInd=="N"?"O":"F");
            Sm.CmParam<String>(ref cm, "@ProcessIndNew", CancelInd=="N"?"F":"O");
            return cm;
        }

        private MySqlCommand SaveJournal(string DocNo, bool IsNeedApproval)
        {
            var SQL = new StringBuilder();
            var CurCode = Sm.GetLue(LueCurCode);

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Update TblSalesInvoiceHdr Set JournalDocNo=@JournalDocNo Where DocNo=@DocNo ");
            if (IsNeedApproval) SQL.AppendLine("And Status='A' ");
            SQL.AppendLine("; ");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, ");
            if (mIsSLIJournalUseCCCode)
                SQL.AppendLine("CCCode, ");
            SQL.AppendLine("Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, ");
            SQL.AppendLine("@DocDt, ");
            SQL.AppendLine("Concat('Sales Invoice : ', @DocNo), ");
            SQL.AppendLine("@MenuCode, (Select MenuDesc From TblMenu Where MenuCode=@MenuCode), ");
            if (mIsSLIJournalUseCCCode)
                SQL.AppendLine("@CCCode, ");
            SQL.AppendLine("@Remark, @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblSalesInvoiceHdr ");
            SQL.AppendLine("Where DocNo = @DocNo ");
            SQL.AppendLine("And JournalDocNo Is Not Null; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("T.AcNo, T.DAmt, T.CAMt, Null As Remark, @CreateBy As CreateBy, CurrentDateTime() As CreateDt ");
            SQL.AppendLine("From TblJournalHdr T0 ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From (");


            // Piutang Usaha

            if (mSLITaxCalculationFormat == "1")
            {
                #region Default
                if (Sm.CompareStr(mMainCurCode, CurCode))
                {
                    SQL.AppendLine("        Select Concat(B.ParValue, A.CtCode) As AcNo, ");
                    SQL.AppendLine("        (A.TotalAmt+A.TotalTax-A.Downpayment+IfNull(C.Amt, 0.00)) As DAmt, ");
                    SQL.AppendLine("        0 As CAmt ");
                    SQL.AppendLine("        From TblSalesInvoiceHdr A ");
                    SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='CustomerAcNoAR' ");
                    SQL.AppendLine("        Left Join ( ");
                    SQL.AppendLine("            Select Sum(Amt) Amt From (");
                    SQL.AppendLine("                Select Case When T1.DAmt<>0.00 Then ");
                    SQL.AppendLine("                    Case T2.AcType ");
                    SQL.AppendLine("                        When 'D' Then -1.00*T1.DAmt ");
                    SQL.AppendLine("                        When 'C' Then T1.DAmt ");
                    SQL.AppendLine("                    End ");
                    SQL.AppendLine("                Else ");
                    SQL.AppendLine("                    Case T2.AcType ");
                    SQL.AppendLine("                        When 'D' Then T1.CAmt ");
                    SQL.AppendLine("                        When 'C' Then -1*T1.CAmt ");
                    SQL.AppendLine("                    End ");
                    SQL.AppendLine("                End As Amt ");
                    SQL.AppendLine("                From TblSalesInvoiceDtl2 T1, TblCoa T2 ");
                    SQL.AppendLine("                Where T1.DocNo=@DocNo ");
                    SQL.AppendLine("                And T1.AcNo=T2.AcNo ");
                    SQL.AppendLine("                And T1.AcInd='Y' ");
                    SQL.AppendLine("            ) T ");
                    SQL.AppendLine("        ) C On 0=0 ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo ");
                }
                else
                {
                    SQL.AppendLine("        Select Concat(B.ParValue, A.CtCode) As AcNo, ");
                    SQL.AppendLine("        IfNull(( ");
                    SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                    SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                    SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                    SQL.AppendLine("        ), 0.00) * ");
                    SQL.AppendLine("        (A.TotalAmt+A.TotalTax-A.Downpayment+IfNull(C.Amt, 0.00)) As DAmt, ");
                    SQL.AppendLine("        0.00 As CAmt ");
                    SQL.AppendLine("        From TblSalesInvoiceHdr A ");
                    SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='CustomerAcNoAR' ");
                    SQL.AppendLine("        Left Join ( ");
                    SQL.AppendLine("            Select Sum(Amt) Amt From (");
                    SQL.AppendLine("                Select Case When T1.DAmt<>0.00 Then ");
                    SQL.AppendLine("                    Case T2.AcType ");
                    SQL.AppendLine("                        When 'D' Then -1.00*T1.DAmt ");
                    SQL.AppendLine("                        When 'C' Then T1.DAmt ");
                    SQL.AppendLine("                    End ");
                    SQL.AppendLine("                Else ");
                    SQL.AppendLine("                    Case T2.AcType ");
                    SQL.AppendLine("                        When 'D' Then T1.CAmt ");
                    SQL.AppendLine("                        When 'C' Then -1*T1.CAmt ");
                    SQL.AppendLine("                    End ");
                    SQL.AppendLine("                End As Amt ");
                    SQL.AppendLine("                From TblSalesInvoiceDtl2 T1, TblCoa T2 ");
                    SQL.AppendLine("                Where T1.DocNo=@DocNo ");
                    SQL.AppendLine("                And T1.AcNo=T2.AcNo ");
                    SQL.AppendLine("                And T1.AcInd='Y' ");
                    SQL.AppendLine("            ) T ");
                    SQL.AppendLine("        ) C On 0=0 ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo ");
                }
                #endregion
            }
            else
            {
                #region SRN
                if (Sm.CompareStr(mMainCurCode, CurCode))
                {
                    SQL.AppendLine("        Select Concat(B.ParValue, A.CtCode) As AcNo, ");
                    SQL.AppendLine("        A.Amt As DAmt, ");
                    SQL.AppendLine("        0 As CAmt ");
                    SQL.AppendLine("        From TblSalesInvoiceHdr A ");
                    SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='CustomerAcNoAR' ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo ");
                }
                else
                {
                    SQL.AppendLine("        Select Concat(B.ParValue, A.CtCode) As AcNo, ");
                    SQL.AppendLine("        IfNull(( ");
                    SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                    SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                    SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                    SQL.AppendLine("        ), 0.00) * ");
                    SQL.AppendLine("        A.Amt As DAmt, ");
                    SQL.AppendLine("        0.00 As CAmt ");
                    SQL.AppendLine("        From TblSalesInvoiceHdr A ");
                    SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='CustomerAcNoAR' ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo ");
                }
                #endregion
            }

            //Piutang Uninvoice

            if (mSLITaxCalculationFormat == "1")
            {
                #region Default
                if (Sm.CompareStr(mMainCurCode, CurCode))
                {
                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("        Select Concat(B.ParValue, A.CtCode) As AcNo, ");
                    SQL.AppendLine("        0 As DAmt, ");
                    SQL.AppendLine("        A.TotalAmt+IfNull(C.Amt, 0.00) As CAmt ");
                    SQL.AppendLine("        From TblSalesInvoiceHdr A ");
                    SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='CustomerAcNoNonInvoice' ");
                    SQL.AppendLine("        Left Join ( ");
                    SQL.AppendLine("            Select Sum(Amt) Amt From (");
                    SQL.AppendLine("                Select Case When T1.DAmt<>0.00 Then ");
                    SQL.AppendLine("                    Case T2.AcType ");
                    SQL.AppendLine("                        When 'D' Then -1.00*T1.DAmt ");
                    SQL.AppendLine("                        When 'C' Then T1.DAmt ");
                    SQL.AppendLine("                    End ");
                    SQL.AppendLine("                Else ");
                    SQL.AppendLine("                    Case T2.AcType ");
                    SQL.AppendLine("                        When 'D' Then T1.CAmt ");
                    SQL.AppendLine("                        When 'C' Then -1*T1.CAmt ");
                    SQL.AppendLine("                    End ");
                    SQL.AppendLine("                End As Amt ");
                    SQL.AppendLine("                From TblSalesInvoiceDtl2 T1, TblCoa T2 ");
                    SQL.AppendLine("                Where T1.DocNo=@DocNo ");
                    SQL.AppendLine("                And T1.AcNo=T2.AcNo ");
                    SQL.AppendLine("                And T1.AcInd='Y' ");
                    SQL.AppendLine("            ) T ");
                    SQL.AppendLine("        ) C On 0=0 ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo ");
                }
                else
                {
                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("        Select Concat(B.ParValue, A.CtCode) As AcNo, ");
                    SQL.AppendLine("        0.00 As DAmt, ");
                    SQL.AppendLine("        IfNull(( ");
                    SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                    SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                    SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                    SQL.AppendLine("        ), 0.00) * ");
                    SQL.AppendLine("        A.TotalAmt+IfNull(C.Amt, 0.00) As CAmt ");
                    SQL.AppendLine("        From TblSalesInvoiceHdr A ");
                    SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='CustomerAcNoNonInvoice' ");
                    SQL.AppendLine("        Left Join ( ");
                    SQL.AppendLine("            Select Sum(Amt) Amt From (");
                    SQL.AppendLine("                Select Case When T1.DAmt<>0.00 Then ");
                    SQL.AppendLine("                    Case T2.AcType ");
                    SQL.AppendLine("                        When 'D' Then -1.00*T1.DAmt ");
                    SQL.AppendLine("                        When 'C' Then T1.DAmt ");
                    SQL.AppendLine("                    End ");
                    SQL.AppendLine("                Else ");
                    SQL.AppendLine("                    Case T2.AcType ");
                    SQL.AppendLine("                        When 'D' Then T1.CAmt ");
                    SQL.AppendLine("                        When 'C' Then -1*T1.CAmt ");
                    SQL.AppendLine("                    End ");
                    SQL.AppendLine("                End As Amt ");
                    SQL.AppendLine("                From TblSalesInvoiceDtl2 T1, TblCoa T2 ");
                    SQL.AppendLine("                Where T1.DocNo=@DocNo ");
                    SQL.AppendLine("                And T1.AcNo=T2.AcNo ");
                    SQL.AppendLine("                And T1.AcInd='Y' ");
                    SQL.AppendLine("            ) T ");
                    SQL.AppendLine("        ) C On 0=0 ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo ");
                }
                #endregion
            }
            else
            {
                #region SRN
                if (Sm.CompareStr(mMainCurCode, CurCode))
                {
                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("        Select Concat(B.ParValue, A.CtCode) As AcNo, ");
                    SQL.AppendLine("        0 As DAmt, ");
                    SQL.AppendLine("        C.Amt As CAmt ");
                    SQL.AppendLine("        From TblSalesInvoiceHdr A ");
                    SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='CustomerAcNoNonInvoice' ");
                    SQL.AppendLine("        Inner Join ");
                    SQL.AppendLine("        ( ");
                    SQL.AppendLine("            Select DocNo, Sum(Qty * UPriceBeforeTax) Amt ");
                    SQL.AppendLine("            From TblSalesInvoiceDtl ");
                    SQL.AppendLine("            Where DocNo = @DocNo ");
                    SQL.AppendLine("            Group By DocNo ");
                    SQL.AppendLine("        ) C On A.DocNo = C.DocNo ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo ");
                }
                else
                {
                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("        Select Concat(B.ParValue, A.CtCode) As AcNo, ");
                    SQL.AppendLine("        0.00 As DAmt, ");
                    SQL.AppendLine("        IfNull(( ");
                    SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                    SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                    SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                    SQL.AppendLine("        ), 0.00) * ");
                    SQL.AppendLine("        C.Amt As CAmt ");
                    SQL.AppendLine("        From TblSalesInvoiceHdr A ");
                    SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='CustomerAcNoNonInvoice' ");
                    SQL.AppendLine("        Inner Join ");
                    SQL.AppendLine("        ( ");
                    SQL.AppendLine("            Select DocNo, Sum(Qty * UPriceBeforeTax) Amt ");
                    SQL.AppendLine("            From TblSalesInvoiceDtl ");
                    SQL.AppendLine("            Where DocNo = @DocNo ");
                    SQL.AppendLine("            Group By DocNo ");
                    SQL.AppendLine("        ) C On A.DocNo = C.DocNo ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo ");
                }
                #endregion
            }
            
            //PPN Keluaran

            if (mIsSalesInvoiceJournalUseTaxAcNo2)
            {
                if (Sm.CompareStr(mMainCurCode, CurCode))
                {
                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("        Select B.AcNo2 As AcNo, ");
                    //SQL.AppendLine("        0.00 As DAmt, ");
                    //SQL.AppendLine("        A.TaxAmt1 As CAmt ");
                    SQL.AppendLine("        Case When A.TotalAmt* B.TaxRate * 0.01 >= 0.00 Then 0.00 Else if(A.TaxAmt1<0, -1*A.TaxAmt1, A.TaxAmt1) End As DAmt,  ");
                    SQL.AppendLine("        Case When A.TotalAmt* B.TaxRate * 0.01 >= 0.00 Then if(A.TaxAmt1<0, -1*A.TaxAmt1, A.TaxAmt1) Else 0.00 End As CAmt  ");
                    SQL.AppendLine("        From TblSalesInvoiceHdr A ");
                    SQL.AppendLine("        Inner Join TblTax B On A.TaxCode1=B.TaxCode And B.AcNo2 Is Not Null ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo And A.TaxCode1 Is Not Null And A.TaxAmt1<>0.00 ");
                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("        Select B.AcNo2 As AcNo, ");
                    //SQL.AppendLine("        0.00 As DAmt, ");
                    //SQL.AppendLine("        A.TaxAmt2 As CAmt ");
                    SQL.AppendLine("        Case When A.TotalAmt* B.TaxRate * 0.01 >= 0.00 Then 0.00 Else if(A.TaxAmt2<0, -1*A.TaxAmt2, A.TaxAmt2) End As DAmt,  ");
                    SQL.AppendLine("        Case When A.TotalAmt* B.TaxRate * 0.01 >= 0.00 Then if(A.TaxAmt2<0, -1*A.TaxAmt2, A.TaxAmt2) Else 0.00 End As CAmt  ");
                    SQL.AppendLine("        From TblSalesInvoiceHdr A ");
                    SQL.AppendLine("        Inner Join TblTax B On A.TaxCode2=B.TaxCode And B.AcNo2 Is Not Null ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo And A.TaxCode2 Is Not Null And A.TaxAmt2<>0.00 ");
                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("        Select B.AcNo2 As AcNo, ");
                    //SQL.AppendLine("        0.00 As DAmt, ");
                    //SQL.AppendLine("        A.TaxAmt3 As CAmt ");
                    SQL.AppendLine("        Case When A.TotalAmt* B.TaxRate * 0.01 >= 0.00 Then 0.00 Else if(A.TaxAmt3<0, -1*A.TaxAmt3, A.TaxAmt2) End As DAmt,  ");
                    SQL.AppendLine("        Case When A.TotalAmt* B.TaxRate * 0.01 >= 0.00 Then if(A.TaxAmt3<0, -1*A.TaxAmt3, A.TaxAmt3) Else 0.00 End As CAmt  ");
                    SQL.AppendLine("        From TblSalesInvoiceHdr A ");
                    SQL.AppendLine("        Inner Join TblTax B On A.TaxCode3=B.TaxCode And B.AcNo2 Is Not Null ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo And A.TaxCode3 Is Not Null And A.TaxAmt3<>0.00 ");
                }
                else
                {
                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("        Select B.AcNo2 As AcNo, ");
                    SQL.AppendLine("        0.00 As DAmt, ");
                    SQL.AppendLine("        IfNull(( ");
                    SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                    SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                    SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                    SQL.AppendLine("        ), 0.00) * ");
                    SQL.AppendLine("        A.TaxAmt1 As CAmt ");
                    SQL.AppendLine("        From TblSalesInvoiceHdr A ");
                    SQL.AppendLine("        Inner Join TblTax B On A.TaxCode1=B.TaxCode And B.AcNo2 Is Not Null ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo And A.TaxCode1 Is Not Null And A.TaxAmt1<>0.00 ");
                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("        Select B.AcNo2 As AcNo, ");
                    SQL.AppendLine("        0.00 As DAmt, ");
                    SQL.AppendLine("        IfNull(( ");
                    SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                    SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                    SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                    SQL.AppendLine("        ), 0.00) * ");
                    SQL.AppendLine("        A.TaxAmt2 As CAmt ");
                    SQL.AppendLine("        From TblSalesInvoiceHdr A ");
                    SQL.AppendLine("        Inner Join TblTax B On A.TaxCode2=B.TaxCode And B.AcNo2 Is Not Null ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo And A.TaxCode2 Is Not Null And A.TaxAmt2<>0.00 ");
                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("        Select B.AcNo2 As AcNo, ");
                    SQL.AppendLine("        0.00 As DAmt, ");
                    SQL.AppendLine("        IfNull(( ");
                    SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                    SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                    SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                    SQL.AppendLine("        ), 0.00) * ");
                    SQL.AppendLine("        A.TaxAmt3 As CAmt ");
                    SQL.AppendLine("        From TblSalesInvoiceHdr A ");
                    SQL.AppendLine("        Inner Join TblTax B On A.TaxCode3=B.TaxCode And B.AcNo2 Is Not Null ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo And A.TaxCode3 Is Not Null And A.TaxAmt3<>0.00 ");
                }
            }
            else
            {
                if (Sm.CompareStr(mMainCurCode, CurCode))
                {
                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("        Select B.ParValue As AcNo, ");
                    SQL.AppendLine("        0 As DAmt, ");
                    SQL.AppendLine("        A.TotalTax As CAmt ");
                    SQL.AppendLine("        From TblSalesInvoiceHdr A ");
                    SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='AcNoForVATOut' ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo ");
                }
                else
                {
                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("        Select B.ParValue As AcNo, ");
                    SQL.AppendLine("        0 As DAmt, ");
                    SQL.AppendLine("        IfNull(( ");
                    SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                    SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                    SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                    SQL.AppendLine("        ), 0) * ");
                    SQL.AppendLine("        A.TotalTax As CAmt ");
                    SQL.AppendLine("        From TblSalesInvoiceHdr A ");
                    SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='AcNoForVATOut' ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo ");
                }
            }


            //Downpayment
            if (TxtDownpayment.Text.Length > 0 && decimal.Parse(TxtDownpayment.Text) != 0)
            {
                if (Sm.CompareStr(mMainCurCode, CurCode))
                {
                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("        Select Concat(B.ParValue, A.CtCode) As AcNo, ");
                    SQL.AppendLine("        A.Downpayment As DAmt, ");
                    SQL.AppendLine("        0 As CAmt ");
                    SQL.AppendLine("        From TblSalesInvoiceHdr A ");
                    SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='CustomerAcNoDownPayment' ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo ");
                }
                else
                {
                    //SQL.AppendLine("    Union All ");
                    //SQL.AppendLine("        Select Concat(B.ParValue, A.CtCode) As AcNo, ");
                    //SQL.AppendLine("        IfNull(( ");
                    //SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                    //SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                    //SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                    //SQL.AppendLine("        ), 0) * ");
                    //SQL.AppendLine("        A.Downpayment As DAmt, ");
                    //SQL.AppendLine("        0 As CAmt ");
                    //SQL.AppendLine("        From TblSalesInvoiceHdr A ");
                    //SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='CustomerAcNoDownPayment' ");
                    //SQL.AppendLine("        Where A.DocNo=@DocNo ");

                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("        Select Concat(C.ParValue, A.CtCode) As AcNo, ");
                    SQL.AppendLine("        B.ExcRate*B.Amt As DAmt, 0.00 As CAmt ");
                    SQL.AppendLine("        From TblSalesInvoiceHdr A ");
                    SQL.AppendLine("        Inner Join TblSalesInvoiceDtl3 B On A.DocNo=B.DocNo ");
                    SQL.AppendLine("        Inner Join TblParameter C On C.ParCode='CustomerAcNoDownPayment' And C.ParValue is Not Null ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo ");
                }
            }


            //List of COA

            SQL.AppendLine("    Union All ");
            SQL.AppendLine("        Select B.AcNo, ");
            if (!Sm.CompareStr(mMainCurCode, CurCode))
            {
                SQL.AppendLine("        IfNull(( ");
                SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("        ), 0) * ");
            }
            SQL.AppendLine("        B.DAmt, ");
            if (!Sm.CompareStr(mMainCurCode, CurCode))
            {
                SQL.AppendLine("        IfNull(( ");
                SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("        ), 0) * ");
            }
            SQL.AppendLine("        B.CAmt ");
            SQL.AppendLine("        From TblSalesInvoiceHdr A ");
            SQL.AppendLine("        Inner Join TblSalesInvoiceDtl2 B On A.DocNo=B.DocNo ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");
            if (mSLITaxCalculationFormat != "1")
            {
                SQL.AppendLine("        And B.AcNo Not Like @CtCode ");
            }


            //Laba rugi selisih kurs
            if (!Sm.CompareStr(mMainCurCode, CurCode))
            {
                SQL.AppendLine("        Union All ");
                SQL.AppendLine("        Select ParValue As AcNo, ");
                SQL.AppendLine("        0.00 As DAmt, ");
                SQL.AppendLine("        0.00 As CAmt ");
                SQL.AppendLine("        From TblParameter Where ParCode='AcNoForForeignCurrencyExchangeGains' And ParValue Is Not Null ");
            }


            SQL.AppendLine("    ) Tbl ");
            SQL.AppendLine("    Where AcNo Is Not Null ");
            SQL.AppendLine("    Group By AcNo  ");
            SQL.AppendLine(") T On 0 = 0  ");
            SQL.AppendLine("Where T0.DocNo = @JournalDocNo; ");

            if (!Sm.CompareStr(mMainCurCode, CurCode))
            {
                SQL.AppendLine("Update TblJournalDtl A ");
                SQL.AppendLine("Inner Join ( ");
                SQL.AppendLine("    Select DAmt, CAmt From (");
                SQL.AppendLine("        Select Sum(DAmt) As DAmt, Sum(CAmt) As CAmt ");
                SQL.AppendLine("        From TblJournalDtl Where DocNo=@JournalDocNo ");
                SQL.AppendLine("    ) Tbl ");
                SQL.AppendLine(") B On 0=0 ");
                SQL.AppendLine("Set ");
                SQL.AppendLine("    A.DAmt=Case When B.DAmt<B.CAmt Then Abs(B.CAmt-B.DAmt) Else 0 End, ");
                SQL.AppendLine("    A.CAmt=Case When B.DAmt>B.CAmt Then Abs(B.DAmt-B.CAmt) Else 0 End ");
                SQL.AppendLine("Where A.DocNo=@JournalDocNo ");
                SQL.AppendLine("And A.AcNo In ( ");
                SQL.AppendLine("    Select ParValue From TblParameter ");
                SQL.AppendLine("    Where ParCode='AcNoForForeignCurrencyExchangeGains' ");
                SQL.AppendLine("    And ParValue Is Not Null ");
                SQL.AppendLine("    );");
            }
            SQL.AppendLine("Delete From TblJournalDtl ");
            SQL.AppendLine("Where DocNo=@JournalDocNo ");
            SQL.AppendLine("And (DAmt=0 And CAmt=0) ");
            SQL.AppendLine("And AcNo In ( ");
            SQL.AppendLine("    Select ParValue From TblParameter ");
            SQL.AppendLine("    Where ParCode='AcNoForForeignCurrencyExchangeGains' ");
            SQL.AppendLine("    And ParValue Is Not Null ");
            SQL.AppendLine("    );");


            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            if (mSLITaxCalculationFormat != "1")
                Sm.CmParam<String>(ref cm, "@CtCode", string.Concat("%.", Sm.GetLue(LueCtCode)));

            if(mDocNoFormat == "1")
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(Sm.GetDte(DteDocDt), 1));
            else
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNo3(Sm.GetDte(DteDocDt), "Journal", "TblJournalHdr", mEntCode, "1"));

            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@MainCurCode", mMainCurCode);
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<String>(ref cm, "@CCCode", Sm.GetValue("Select B.CCCode From TblDOCt2Hdr A Inner Join TblWarehouse B On A.WhsCode=B.WhsCode Where A.DocNo=@Param", Sm.GetGrdStr(Grd1, 0, 2)));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand UpdateSalesInvoiceFile(string DocNo, int Row, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblSalesInvoiceDtl Set ");
            SQL.AppendLine("    FileName=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo and DNo = @DNo ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));

            return cm;
        }

        public MySqlCommand UpdateFacturNumber(string FacturNo, string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("UPDATE TblFacturNumberDtl ");
            SQL.AppendLine("SET UsedInd = 'Y', SLIDocNo =@DocNo ,LastUpBy = @UserCode, LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("WHERE FacturNo = @FacturNo And UsedInd= 'N'");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FacturNo", FacturNo);

            return cm;

        }

        //private MySqlCommand SaveJournal(string DocNo)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Set @Row:=0; ");

        //    SQL.AppendLine("Update TblSalesInvoiceHdr Set JournalDocNo=@JournalDocNo Where DocNo=@DocNo;");

        //    SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Values(@JournalDocNo, @DocDt, Concat('Sales Invoice : ', @DocNo), ");
        //    SQL.AppendLine("@MenuCode, (Select MenuDesc From TblMenu Where MenuCode=@MenuCode), ");
        //    SQL.AppendLine("@CreateBy, CurrentDateTime()); ");

        //    SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, Remark, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Select @JournalDocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
        //    SQL.AppendLine("T.AcNo, ");

        //    if (Sm.CompareStr(mMainCurCode, Sm.GetLue(LueCurCode)))
        //    {
        //        SQL.AppendLine("T.DAmt, T.CAMt, ");
        //    }
        //    else
        //    {
        //        SQL.AppendLine("T.DAmt*( ");
        //        SQL.AppendLine("IfNull(( ");
        //        SQL.AppendLine("    Select Amt From TblCurrencyRate ");
        //        SQL.AppendLine("    Where RateDt<=@DocDt And CurCode1=@CurCode And CurCode2=@MainCurCode ");
        //        SQL.AppendLine("    Order By RateDt Desc Limit 1 ");
        //        SQL.AppendLine("), 0) ");
        //        SQL.AppendLine(") As DAmt, ");
        //        SQL.AppendLine("T.CAMt*( ");
        //        SQL.AppendLine("IfNull(( ");
        //        SQL.AppendLine("    Select Amt From TblCurrencyRate ");
        //        SQL.AppendLine("    Where RateDt<=@DocDt And CurCode1=@CurCode And CurCode2=@MainCurCode ");
        //        SQL.AppendLine("    Order By RateDt Desc Limit 1 ");
        //        SQL.AppendLine("), 0) ");
        //        SQL.AppendLine(") As CAmt, ");
        //    }
        //    SQL.AppendLine("Null As Remark, @CreateBy As CreateBy, CurrentDateTime() As CreateDt ");
        //    SQL.AppendLine("From ( ");
        //    SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From (");


        //    // Piutang Usaha

        //    SQL.AppendLine("        Select Concat(B.ParValue, A.CtCode) As AcNo, ");
        //    SQL.AppendLine("        (A.TotalAmt+A.TotalTax) As DAmt, ");
        //    SQL.AppendLine("        0 As CAmt ");
        //    SQL.AppendLine("        From TblSalesInvoiceHdr A ");
        //    SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='CustomerAcNoAR' ");
        //    SQL.AppendLine("        Where A.DocNo=@DocNo ");

        //    //Piutang Uninvoice

        //    SQL.AppendLine("    Union All ");
        //    SQL.AppendLine("        Select Concat(B.ParValue, A.CtCode) As AcNo, ");
        //    SQL.AppendLine("        0 As DAmt, ");
        //    SQL.AppendLine("        A.TotalAmt As CAmt ");
        //    SQL.AppendLine("        From TblSalesInvoiceHdr A ");
        //    SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='CustomerAcNoNonInvoice' ");
        //    SQL.AppendLine("        Where A.DocNo=@DocNo ");

        //    //PPN Keluaran
        //    SQL.AppendLine("    Union All ");
        //    SQL.AppendLine("        Select B.ParValue As AcNo, ");
        //    SQL.AppendLine("        0 As DAmt, ");
        //    SQL.AppendLine("        A.TotalTax As CAmt ");
        //    SQL.AppendLine("        From TblSalesInvoiceHdr A ");
        //    SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='AcNoForVATOut' ");
        //    SQL.AppendLine("        Where A.DocNo=@DocNo ");

        //    //List of COA

        //    SQL.AppendLine("    Union All ");
        //    SQL.AppendLine("        Select AcNo, DAmt, CAmt ");
        //    SQL.AppendLine("        From TblSalesInvoiceDtl2 Where DocNo=@DocNo ");




        //    SQL.AppendLine("    ) Tbl ");
        //    SQL.AppendLine("    Where AcNo Is Not Null ");
        //    SQL.AppendLine("    Group By AcNo  ");
        //    SQL.AppendLine(") T;  ");

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };
        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(Sm.GetDte(DteDocDt), 1));
        //    Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
        //    Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
        //    Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
        //    return cm;
        //}

        private MySqlCommand SaveDocApproval(string DocNo)
        {
            var SQL = new StringBuilder();
            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblDocApprovalSetting T ");
            SQL.AppendLine("Where T.DocType='SalesInvoice' ");
            SQL.AppendLine("And (T.StartAmt=0 ");
            SQL.AppendLine("Or T.StartAmt<=IfNull(( ");
            SQL.AppendLine("    Select IfNull(Amt, 0) ");
            SQL.AppendLine("    From TblSalesInvoiceHdr ");
            SQL.AppendLine("    Where DocNo=@DocNo ");
            SQL.AppendLine("), 0)) ");
            SQL.AppendLine("And (T.EndAmt=0 ");
            SQL.AppendLine("Or T.EndAmt>=IfNull(( ");
            SQL.AppendLine("    Select IfNull(Amt, 0) ");
            SQL.AppendLine("    From TblSalesInvoiceHdr ");
            SQL.AppendLine("    Where DocNo=@DocNo ");
            SQL.AppendLine("), 0)); ");

            SQL.AppendLine("Update TblSalesInvoiceHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select 1 From TblDocApproval ");
            SQL.AppendLine("    Where DocType='SalesInvoice' ");
            SQL.AppendLine("    And DocNo=@DocNo ");
            SQL.AppendLine("); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }



        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;
            string JournalDocNo = string.Empty;
            if(mDocNoFormat == "1")
                JournalDocNo = Sm.GenerateDocNoJournal(Sm.GetDte(DteDocDt), 1);
            else
                JournalDocNo = Sm.GenerateDocNo3(Sm.GetDte(DteDocDt), "Journal", "TblJournalHdr", mEntCode, "1");

            var lDepositSummary = new List<DepositSummary>();

            if (ChkCancelInd.Checked &&
              decimal.Parse(TxtDownpayment.Text) != 0 &&
              !Sm.CompareStr(Sm.GetLue(LueCurCode), mMainCurCode)
              )
                GetDepositSummary2(ref lDepositSummary);

            var cml = new List<MySqlCommand>();

            bool IsNeedApproval = IsDocNeedApproval(TxtAmt.Text);

            cml.Add(EditSalesInvoiceHdr());

            if (ChkCancelInd.Checked &&
                decimal.Parse(TxtDownpayment.Text) != 0)
            {
                cml.Add(SaveCustomerDeposit(TxtDocNo.Text, IsNeedApproval));
                if (lDepositSummary.Count > 0)
                {
                    for (int i = 0; i < lDepositSummary.Count; i++)
                        cml.Add(SaveCustomerDepositSummary2(lDepositSummary[i], TxtDocNo.Text, IsNeedApproval));
                }
            }

            if (ChkCancelInd.Checked)
            {
                cml.Add(UpdateDOCtProcessInd(TxtDocNo.Text, "Y", IsNeedApproval));
                if (mIsAutoJournalActived) cml.Add(SaveJournal2());
            }

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private MySqlCommand SaveCustomerDepositSummary2(DepositSummary i, string DocNo, bool IsNeedApproval)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblCustomerDepositSummary2 A ");
            if (IsNeedApproval)
            {
                SQL.AppendLine("Inner Join TblSalesInvoiceHdr B On B.DocNo = @DocNo And B.Status = 'A' ");
                SQL.AppendLine("    And B.CtCode = A.CtCode ");
                SQL.AppendLine("    And B.CurCode = A.CurCode ");
            }
            SQL.AppendLine("Set ");
            SQL.AppendLine("    A.Amt=A.Amt+@Amt, A.LastUpBy=@UserCode, A.LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where A.CtCode=@CtCode ");
            SQL.AppendLine("And A.CurCode=@CurCode ");
            SQL.AppendLine("And A.ExcRate=@ExcRate; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<Decimal>(ref cm, "@ExcRate", i.ExcRate);
            Sm.CmParam<Decimal>(ref cm, "@Amt", i.UsedAmt);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                Sm.IsClosingJournalInvalid(mIsAutoJournalActived, true, Sm.GetDte(DteDocDt)) ||
                (!mIsSalesInvoiceTaxInvDocEditable && IsDocumentNotCancelled()) ||
                IsDataCancelledAlready() ||
                IsDataAlreadyProcessedToIncomingPayment() ||
                (ChkCancelInd.Checked && IsVoucherRequestPPNExisted()) ||
                (ChkCancelInd.Checked && IsVATSettlementExisted());
        }

        private bool IsDocumentNotCancelled()
        {
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this document.");
                return true;
            }
            return false;
        }

        private bool IsDataCancelledAlready()
        {
            if (Sm.IsDataExist("Select 1 From TblSalesInvoiceHdr Where CancelInd='Y' And DocNo=@DocNo;", TxtDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This document already cancelled.");
                return true;
            }
            return false;
        }

        private bool IsDataAlreadyProcessedToIncomingPayment()
        {
            if (Sm.IsDataExist("Select 1 From TblSalesInvoiceHdr Where ProcessInd<>'O' And DocNo=@Param Limit 1;", TxtDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This document already processed to incoming payment.");
                return true;
            }
            return false;
        }

        private bool IsVoucherRequestPPNExisted()
        {
            if (Sm.IsDataExist(
                "SELECT 1 FROM TblSalesInvoiceHdr " +
                "WHERE DocNo=@Param AND VoucherRequestPPNDocNo IS NOT NULL;",
                TxtDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This data is already processed to Voucher Request VAT.");
                return true;
            }
            return false;
        }

        private bool IsVATSettlementExisted()
        {
            if (Sm.IsDataExist(
                "Select 1 From TblSalesInvoiceHdr " +
                "Where DocNo=@Param And VATSettlementDocNo Is Not Null;",
                TxtDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Warning, "This document already processed to VAT settlement.");
                return true;
            }
            return false;
        }

        private MySqlCommand EditSalesInvoiceHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblSalesInvoiceHdr Set ");
            if (mIsSalesInvoiceTaxInvDocEditable)
            {
                SQL.AppendLine("TaxInvDocument=@TaxInvDocument, ");
                SQL.AppendLine("TaxInvoiceNo=@TaxInvoiceNo, TaxInvoiceNo2=@TaxInvoiceNo2, TaxInvoiceNo3=@TaxInvoiceNo3, ");
            }
            SQL.AppendLine("    CancelInd=@CancelInd, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@TaxInvDocument", TxtTaxInvDocument.Text);
            Sm.CmParam<String>(ref cm, "@TaxInvoiceNo", TxtTaxInvoiceNo.Text);
            Sm.CmParam<String>(ref cm, "@TaxInvoiceNo2", TxtTaxInvoiceNo2.Text);
            Sm.CmParam<String>(ref cm, "@TaxInvoiceNo3", TxtTaxInvoiceNo3.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");

            return cm;
        }

        private MySqlCommand SaveJournal2()
        {
            var SQL = new StringBuilder();
            var CurrentDt = Sm.ServerCurrentDate();
            var DocDt = Sm.GetDte(DteDocDt);
            var IsClosingJournalUseCurrentDt = Sm.IsClosingJournalUseCurrentDt(DocDt);

            SQL.AppendLine("Update TblSalesInvoiceHdr Set JournalDocNo2=@JournalDocNo Where DocNo=@DocNo And CancelInd='Y' And JournalDocNo Is Not Null;");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, ");
            if (IsClosingJournalUseCurrentDt)
                SQL.AppendLine("Replace(CurDate(), '-', ''), ");
            else
                SQL.AppendLine("DocDt, ");
            SQL.AppendLine("Concat('Cancelling ', JnDesc) As JnDesc, ");
            SQL.AppendLine("MenuCode, MenuDesc, CCCode, Remark, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblJournalHdr Where DocNo In (Select JournalDocNo From TblSalesInvoiceHdr Where DocNo=@DocNo And CancelInd='Y');");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, DNo, AcNo, CAMt As DAmt, DAmt As CAmt, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblJournalDtl Where DocNo In (Select JournalDocNo From TblSalesInvoiceHdr Where DocNo=@DocNo And CancelInd='Y');");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (IsClosingJournalUseCurrentDt)
                if(mDocNoFormat == "1")
                    Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(CurrentDt, 1));
                else
                    Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNo3(CurrentDt, "Journal", "TblJournalHdr", mEntCode, "1"));
            else
                if(mDocNoFormat == "1")
                    Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(DocDt, 1));
                else
                    Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNo3(DocDt, "Journal", "TblJournalHdr", mEntCode, "1"));

            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowSalesInvoiceHdr(DocNo);
                ShowSalesInvoiceDtl(DocNo);
                ShowSalesInvoiceDtl2(DocNo);
                Sm.ShowDocApproval(DocNo, "SalesInvoice", ref Grd4, false);
                ComputeTotalQtyPackagingUnit();
                ComputeTotalQty();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowSalesInvoiceHdr(string DocNo)
        {
            if (mIsCustomerComboBasedOnCategory)
            {
                Sl.SetLueCtCtCode(ref LueCtCtCode);
                Sl.SetLueCtCode(ref LueCtCode, string.Empty);
            }

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            SQL.AppendLine("Select A.DocNo, A.ReceiptNo, A.DocDt, A.CancelInd, A.CtCode, A.DueDt, ");
            SQL.AppendLine("A.LocalDocNo, A.TaxInvDocument, A.CurCode, A.TotalAmt, A.TotalTax, ");
            SQL.AppendLine("A.DownPayment, A.Amt, A.BankAcCode, A.SalesName, A.SignName, A.Remark, ");
            SQL.AppendLine("A.JournalDocNo, A.JournalDocNo2, A.TaxInvDt, ");
            SQL.AppendLine("A.TaxInvoiceNo, A.TaxInvoiceNo2, A.TaxInvoiceNo3, ");
            SQL.AppendLine("A.TaxInvoiceDt, A.TaxInvoiceDt2, A.TaxInvoiceDt3, ");
            SQL.AppendLine("A.TaxCode1, A.TaxCode2, A.TaxCode3, ");
            SQL.AppendLine("A.TaxAmt1, A.TaxAmt2, A.TaxAmt3, ");
            SQL.AppendLine("A.TaxAlias1, A.TaxAlias2, A.TaxAlias3, A.AdditionalCostDiscAmt, ");
            SQL.AppendLine("Case ");
            SQL.AppendLine("When A.Status = 'A' Then 'Approved' ");
            SQL.AppendLine("When A.Status = 'O' Then 'Outstanding' ");
            SQL.AppendLine("When A.Status = 'C' Then 'Cancelled' ");
            SQL.AppendLine("End Status, ");
            if (mIsCustomerComboBasedOnCategory)
                SQL.AppendLine("B.CtCtCode ");
            else
                SQL.AppendLine("Null As CtCtCode ");
            SQL.AppendLine("From TblSalesInvoiceHdr A ");
            if (mIsCustomerComboBasedOnCategory)
                SQL.AppendLine("Inner Join TblCustomer B On A.CtCode = B.CtCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo",

                        //1-5
                        "ReceiptNo", "DocDt",  "CancelInd", "CtCode", "DueDt",
                        
                        //6-10
                         "LocalDocNo", "TaxInvDocument", "CurCode", "TotalAmt", "TotalTax", 
                        
                        //11-15
                        "DownPayment", "Amt", "BankAcCode", "SalesName", "Remark", 
                        
                        //16-20
                        "JournalDocNo", "JournalDocNo2", "TaxInvDt", "SignName", "TaxInvoiceNo", 
                        
                        //21-25
                        "TaxInvoiceNo2", "TaxInvoiceNo3", "TaxInvoiceDt", "TaxInvoiceDt2", "TaxInvoiceDt3", 
                        
                        //26-30
                        "TaxCode1", "TaxCode2", "TaxCode3", "TaxAmt1", "TaxAmt2", 
                        
                        //31-35
                        "TaxAmt3", "TaxAlias1", "TaxAlias2", "TaxAlias3", "AdditionalCostDiscAmt",

                        //36-37
                        "CtCtCode", "Status"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        TxtReceiptNo.EditValue = Sm.DrStr(dr, c[1]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[2]));
                        ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[3]), "Y") ? true : false;
                        if (!mIsCustomerComboBasedOnCategory)
                            SetLueCtCode(ref LueCtCode, Sm.DrStr(dr, c[4]));
                        Sm.SetLue(LueCtCode, Sm.DrStr(dr, c[4]));
                        Sm.SetDte(DteDueDt, Sm.DrStr(dr, c[5]));
                        TxtLocalDocNo.EditValue = Sm.DrStr(dr, c[6]);
                        TxtTaxInvDocument.EditValue = Sm.DrStr(dr, c[7]);
                        Sm.SetLue(LueCurCode, Sm.DrStr(dr, c[8]));
                        TxtTotalAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[9]), 0);
                        TxtTotalTax.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[10]), 0);
                        TxtDownpayment.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[11]), 0);
                        TxtAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[12]), 0);
                        Sl.SetLueBankAcCode(ref LueBankAcCode, Sm.DrStr(dr, c[13]));
                        Sm.SetLue(LueSPCode, Sm.GetValue("Select SpCode From TblSalesPerson Where SPname=@Param Limit 1;", Sm.DrStr(dr, c[14])));
                        MeeRemark.EditValue = Sm.DrStr(dr, c[15]);
                        TxtJournalDocNo.EditValue = Sm.DrStr(dr, c[16]);
                        TxtJournalDocNo2.EditValue = Sm.DrStr(dr, c[17]);
                        Sm.SetDte(DteTaxInvDt, Sm.DrStr(dr, c[18]));
                        TxtSignName.EditValue = Sm.DrStr(dr, c[19]);
                        TxtTaxInvoiceNo.EditValue = Sm.DrStr(dr, c[20]);
                        TxtTaxInvoiceNo2.EditValue = Sm.DrStr(dr, c[21]);
                        TxtTaxInvoiceNo3.EditValue = Sm.DrStr(dr, c[22]);
                        Sm.SetDte(DteTaxInvoiceDt, Sm.DrStr(dr, c[23]));
                        Sm.SetDte(DteTaxInvoiceDt2, Sm.DrStr(dr, c[24]));
                        Sm.SetDte(DteTaxInvoiceDt3, Sm.DrStr(dr, c[25]));
                        Sm.SetLue(LueTaxCode1, Sm.DrStr(dr, c[26]));
                        Sm.SetLue(LueTaxCode2, Sm.DrStr(dr, c[27]));
                        Sm.SetLue(LueTaxCode3, Sm.DrStr(dr, c[28]));
                        TxtTaxAmt1.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[29]), 0);
                        TxtTaxAmt2.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[30]), 0);
                        TxtTaxAmt3.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[31]), 0);
                        TxtAlias1.EditValue = Sm.DrStr(dr, c[32]);
                        TxtAlias2.EditValue = Sm.DrStr(dr, c[33]);
                        TxtAlias3.EditValue = Sm.DrStr(dr, c[34]);
                        mAdditionalCostDiscAmt = mSLITaxCalculationFormat == "1" ? 0m : Sm.DrDec(dr, c[35]);
                        if (mIsCustomerComboBasedOnCategory) Sm.SetLue(LueCtCtCode, Sm.DrStr(dr, c[36]));
                        TxtStatus.EditValue = Sm.DrStr(dr, c[37]);
                    }, true
                );
        }

        private void ShowSalesInvoiceDtl(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.DocType, ");
            SQL.AppendLine("A.DOCtDocNo, A.DOCtDNo, B.DocDt, ");
            SQL.AppendLine("If(A.DocType='1', B.DRDocNo, B.PLDocNo) As DRPLDocNo,  ");
            SQL.AppendLine("If(A.DocType='1', C1.DRDNo, C2.PLDNo) As DRPLDNo, ");
            SQL.AppendLine("If(A.DocType='1', C1.SODocNo, D2.SODocNo) As SODocNo, ");
            SQL.AppendLine("If(A.DocType='1', C1.SODNo, D2.SODNo) As SODNo, ");
            SQL.AppendLine("If(A.DocType='1', C1.CtQtDocNo, E2.CtQtDocNo) As CtQtDocNo,");
            SQL.AppendLine("If(A.DocType='1', C1.CtQtDNo, F2.CtQtDNo) As CtQtDNo, ");
            SQL.AppendLine("A.ItCode, ");
            SQL.AppendLine("If(A.DocType='1', C1.ItName, K2.ItName) As ItName, ");
            SQL.AppendLine("A.QtyPackagingUnit, ");
            SQL.AppendLine("If(A.DocType='1', C1.PackagingUnitUomCode, F2.PackagingUnitUomCode) As PackagingUnitUomCode, ");
            SQL.AppendLine("A.Qty, ");
            SQL.AppendLine("If(A.DocType='1', C1.PriceUomCode, I2.PriceUomCode) As PriceUomCode, ");
            SQL.AppendLine("A.UPriceBeforeTax, ");
            SQL.AppendLine("A.TaxRate, ");
            SQL.AppendLine("A.TaxAmt, ");
            SQL.AppendLine("A.UPriceAfterTax, ");
            SQL.AppendLine("If(A.DocType='1', C1.PtDay, L2.PtDay) As PtDay, ");
            SQL.AppendLine("If(A.DocType='1', C1.LocalDocNo, M2.LocalDocNo) As LocalDocNo, ");
            SQL.AppendLine("If(A.DocType='1', N1.CtItName, N2.CtItName) As CtItName, ");
            if (mSalesInvoiceTaxCalculationFormula != "1")
                SQL.AppendLine("A.TaxInd1, A.TaxInd2, A.TaxInd3, A.TaxAmt1, A.TaxAmt2, A.TaxAmt3, A.TaxAmtTotal, ");
            else
                SQL.AppendLine("'N' As TaxInd1, 'N' As TaxInd2, 'N' As TaxInd3, 0.00 As TaxAmt1, 0.00 As TaxAmt2, 0.00 As TaxAmt3, 0.00 As TaxAmtTotal, ");
            SQL.AppendLine("A.FileName ");
            SQL.AppendLine("From TblSalesInvoiceDtl A ");
            SQL.AppendLine("Inner Join TblDOCt2Hdr B On A.DOCtDocNo=B.DocNo ");
            
            //DR

            SQL.AppendLine("Left Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select X1.DocNo, X1.DNo, C1.DRDNo, D1.SODocNo, D1.SODNo, E1.CtQtDocNo, F1.CtQtDNo, ");
            SQL.AppendLine("    K1.ItName, F1.PackagingUnitUomCode, I1.PriceUomCode, L1.PtDay, ");
            SQL.AppendLine("    M1.LocalDocNo, G1.CtCode, J1.ItCode ");
            SQL.AppendLine("    From TblSalesInvoiceDtl X1 ");
            SQL.AppendLine("    Inner join TblDOCt2Hdr X2 On X1.DOCtDocNo = X2.DocNo And X1.DocNo = @DocNo ");
            SQL.AppendLine("    Inner Join TblDOCt2Dtl2 C1 On X1.DOCtDocNo=C1.DocNo And X1.DOCtDNo=C1.DNo ");
            SQL.AppendLine("    Inner Join TblDRDtl D1 On X2.DRDocNo=D1.DocNo And C1.DRDNo=D1.DNo ");
            SQL.AppendLine("    Inner Join TblSOHdr E1 On D1.SODocNo=E1.DocNo ");
            SQL.AppendLine("    Inner Join TblSODtl F1 On D1.SODocNo=F1.DocNo And D1.SODNo=F1.DNo ");
            SQL.AppendLine("    Inner Join TblCtQtHdr G1 On E1.CtQtDocNo=G1.DocNo ");
            SQL.AppendLine("    Inner Join TblCtQtDtl H1 On E1.CtQtDocNo=H1.DocNo And F1.CtQtDNo=H1.DNo ");
            SQL.AppendLine("    Inner Join TblItemPriceHdr I1 On H1.ItemPriceDocNo=I1.DocNo ");
            SQL.AppendLine("    Inner Join TblItemPriceDtl J1 On H1.ItemPriceDocNo=J1.DocNo And H1.ItemPriceDNo=J1.DNo ");
            SQL.AppendLine("    Inner Join TblItem K1 On J1.ItCode=K1.ItCode ");
            SQL.AppendLine("    Inner Join TblPaymentTerm L1 On G1.PtCode=L1.PtCode ");
            SQL.AppendLine("    Inner Join TblDRHdr M1 On X2.DRDocNo=M1.DocNo ");
            
            SQL.AppendLine("    Union All ");

            SQL.AppendLine("    Select X1.DocNo, X1.DNo, C1.DRDNo, D1.SODocNo, D1.SODNo, E1.BOQDocNo As CtQtDocNo, '001' As CtQtDNo, ");
            SQL.AppendLine("    K1.ItName, F1.PackagingUnitUomCode, F1.PackagingUnitUomCode As PriceUomCode, L1.PtDay, ");
            SQL.AppendLine("    M1.LocalDocNo, H1.CtCode, F1.ItCode ");
            SQL.AppendLine("    From TblSalesInvoiceDtl X1 ");
            SQL.AppendLine("    Inner join TblDOCt2Hdr X2 On X1.DOCtDocNo = X2.DocNo And X1.DocNo = @DocNo ");
            SQL.AppendLine("    Inner Join TblDOCt2Dtl2 C1 On X1.DOCtDocNo=C1.DocNo And X1.DOCtDNo=C1.DNo ");
            SQL.AppendLine("    Inner Join TblDRDtl D1 On X2.DRDocNo=D1.DocNo And C1.DRDNo=D1.DNo ");
            SQL.AppendLine("    Inner Join TblSOContractHdr E1 On D1.SODocNo=E1.DocNo ");
            SQL.AppendLine("    Inner Join TblSOContractDtl F1 On D1.SODocNo=F1.DocNo And D1.SODNo=F1.DNo ");
            SQL.AppendLine("    Inner Join TblBOQHDr G1 On E1.BOQDocNo = G1.Docno ");
            SQL.AppendLine("    Inner Join TblLOPHdr H1 On G1.LOPDocNo = H1.DocNo ");
            SQL.AppendLine("    Inner Join TblItem K1 On F1.ItCode=K1.ItCode ");
            SQL.AppendLine("    Inner Join TblPaymentTerm L1 On G1.PtCode=L1.PtCode ");
            SQL.AppendLine("    Inner Join TblDRHdr M1 On X2.DRDocNo=M1.DocNo ");

            SQL.AppendLine("Union All ");

            SQL.AppendLine("    SELECT X1.DocNo, X1.DNo, C1.DRDNo, D1.SODocNo, D1.SODNo, ");
            SQL.AppendLine("    F1.DocNo AS CtQtDocNo, G1.DNo AS CtQtDNo, ");
            SQL.AppendLine("    H1.ItName, H1.SalesUomCode AS PackagingUomCode, H1.SalesUoMCode AS PriceUomCode, ");
            SQL.AppendLine("    I1.PtDay, K1.LocalDocNo, J1.CtCode, H1.ItCode ");
            SQL.AppendLine("    FROM TblSalesInvoiceDtl X1 ");
            SQL.AppendLine("    INNER JOIN TblDOCt2Hdr X2 ON X1.DOCtDocNo = X2.DocNo AND X1.DocNo = @DocNo ");
            SQL.AppendLine("    INNER JOIN TblDOCt2Dtl2 C1 ON X1.DOCtDocNo = C1.DocNo AND X1.DOCtDNo = C1.DNo ");
            SQL.AppendLine("    INNER JOIN TblDRDtl D1 ON X2.DRDocNo = D1.DocNo AND C1.DRDNo = D1.DNo ");
            SQL.AppendLine("    INNER JOIN TblSalesContract E1 ON D1.SCDocNo = E1.DocNo ");
            SQL.AppendLine("    INNER JOIN TblSalesMemoHdr F1 ON D1.SODocNo = F1.DocNo ");
            SQL.AppendLine("    INNER JOIN TblSalesMemoDtl G1 ON F1.DocNo = G1.DocNo AND D1.SODNo = G1.DNo ");
            SQL.AppendLine("    INNER JOIN TblItem H1 ON G1.ItCode = H1.ItCode ");
            SQL.AppendLine("    LEFT JOIN TblPaymentTerm I1 ON E1.PtCode = I1.PtCode ");
            SQL.AppendLine("    LEFT JOIN TblCustomerItem J1 ON F1.CtCode = J1.CtCode ");
            SQL.AppendLine("    INNER JOIN TblDRHdr K1 ON X2.DRDocNo = K1.DocNo ");
            SQL.AppendLine(") C1 On A.DocNo=C1.DocNo And A.DNo=C1.DNo ");

            //PL
            SQL.AppendLine("Left Join TblDOCt2Dtl3 C2 On A.DOCtDocNo=C2.DocNo And A.DOCtDNo=C2.DNo ");
            SQL.AppendLine("Left Join TblPLDtl D2 On B.PLDocNo=D2.DocNo And C2.PLDNo=D2.DNo ");
            SQL.AppendLine("Left Join TblSOHdr E2 On D2.SODocNo=E2.DocNo ");
            SQL.AppendLine("Left Join TblSODtl F2 On D2.SODocNo=F2.DocNo And D2.SODNo=F2.DNo ");
            SQL.AppendLine("Left Join TblCtQtHdr G2 On E2.CtQtDocNo=G2.DocNo ");
            SQL.AppendLine("Left Join TblCtQtDtl H2 On E2.CtQtDocNo=H2.DocNo And F2.CtQtDNo=H2.DNo ");
            SQL.AppendLine("Left Join TblItemPriceHdr I2 On H2.ItemPriceDocNo=I2.DocNo ");
            SQL.AppendLine("Left Join TblItemPriceDtl J2 On H2.ItemPriceDocNo=J2.DocNo And H2.ItemPriceDNo=J2.DNo ");
            SQL.AppendLine("Left Join TblItem K2 On J2.ItCode=K2.ItCode ");
            SQL.AppendLine("Left Join TblPaymentTerm L2 On G2.PtCode=L2.PtCode ");
            SQL.AppendLine("Left Join TblPLHdr M2 On B.PLDocNo=M2.DocNo ");

            //Customer Item Name
            SQL.AppendLine("Left Join TblCustomerItem N1 On C1.CtCode = N1.CtCode And C1.ItCode = N1.ItCode ");
            SQL.AppendLine("Left Join TblCustomerItem N2 On G2.CtCode = N2.CtCode And K2.ItCode = N2.ItCode ");

            SQL.AppendLine("Where A.DocNo=@DocNo; ");
            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo",

                    //1-5
                    "DOCtDocNo", "DOCtDNo", "DocDt", "DRPLDocNo", "DRPLDNo", 
                    
                    //6-10
                    "SODocNo", "SODNo", "CtQtDocNo", "CtQtDNo", "ItCode", 
                    
                    //11-15
                    "ItName", "QtyPackagingUnit", "PackagingUnitUomCode", "Qty", "PriceUomCode", 
                    
                    //16-20
                    "UPriceBeforeTax", "TaxRate", "TaxAmt", "UPriceAfterTax", "PtDay",

                    //21-25
                    "LocalDocNo", "DocType", "CtItName", "FileName", "TaxInd1", 
                    
                    //26-30
                    "TaxInd2", "TaxInd3", "TaxAmtTotal", "TaxAmt1", "TaxAmt2", 
                    
                    //31
                    "TaxAmt3"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 10);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 12);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 13);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 14);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 15);
                    Grd.Cells[Row, 22].Value = Sm.GetLue(LueCurCode);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 23, 16);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 24, 17);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 25, 18);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 26, 19);
                    Grd.Cells[Row, 27].Value = dr.GetDecimal(c[14]) * dr.GetDecimal(c[19]);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 28, 20);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 29, 21);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 30, 22);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 31, 23);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 32, 24);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 35, 24);
                    if (mSalesInvoiceTaxCalculationFormula != "1")
                    {
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 37, 25);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 38, 26);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 39, 27);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 40, 28);
                        Grd.Cells[Row, 41].Value = dr.GetDecimal(c[14]) * dr.GetDecimal(c[16]);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 42, 29);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 43, 30);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 44, 31);
                    }
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 18, 20, 23, 24, 25, 26, 27, 28, 40, 41, 42, 43, 44 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ShowSalesInvoiceDtl2(string DocNo)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();
            SQL.AppendLine("Select A.AcNo, B.AcDesc, A.AcInd, A.DAmt, A.CAmt, A.OptAcDesc, C.OptDesc, A.Remark ");
            SQL.AppendLine("From TblSalesInvoiceDtl2 A "); 
            SQL.AppendLine("Inner Join TblCOA B ");
            SQL.AppendLine("Left Join TblOption C On A.OptAcDesc = C.OptCode And OptCat='AccountDescriptionOnSalesInvoice'  ");
            SQL.AppendLine("Where A.DocNo=@DocNo And A.AcNo=B.AcNo Order By A.DNo;");

            Sm.ShowDataInGrid(
                ref Grd3, ref cm, SQL.ToString(),
                new string[] 
                { 
                    "AcNo", 
                    "AcDesc", "AcInd", "DAmt", "CAmt", "OptAcDesc",
                    "OptDesc", "Remark"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 10, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);

                    if (Sm.GetGrdBool(Grd3, Row, 10).ToString() == "True")
                    {
                        if (Sm.GetGrdDec(Grd3, Row, 4) > 0)
                        {
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                        }
                        else
                        {
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 5, 2);
                        }
                    }
                }, false, false, true, false
            );
            Sm.SetGrdBoolValueFalse(Grd3, Grd3.Rows.Count - 1, new int[] { 3, 5 });
            Sm.SetGrdNumValueZero(ref Grd3, Grd3.Rows.Count - 1, new int[] { 4, 6 });
            Sm.FocusGrd(Grd3, 0, 1);
        }

        private void ShowCustomerDepositSummary(string CtCode)
        {
            ClearGrd2();

            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@CtCode", CtCode);

            var SQL = new StringBuilder();
            SQL.AppendLine("Select CurCode, Amt ");
            SQL.AppendLine("From TblCustomerDepositSummary ");
            SQL.AppendLine("Where CtCode=@CtCode Order By CurCode;");

            Sm.ShowDataInGrid(
                ref Grd2, ref cm, SQL.ToString(),
                new string[] { "CurCode", "Amt" },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 1, 1);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 1 });
            Sm.FocusGrd(Grd2, 0, 0);
        }

        #endregion

        #region Additional Method

        private void ComputeTaxPerDetail(bool IsFromDetail, int Row)
        {
            if (IsFromDetail) // perubahan dari detail yg di tick/untick
            {
                ComputeTaxPerDetail2(Row);
            }
            else
            {
                for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
                {
                    ComputeTaxPerDetail2(i);
                }
            }

            ComputeAmt();
        }

        private void ComputeTaxPerDetail2(int Row)
        {
            decimal TaxAmt = 0m;
            decimal Qty = Sm.GetGrdDec(Grd1, Row, 20);
            decimal UPriceBeftax = Sm.GetGrdDec(Grd1, Row, 23);

            Grd1.Cells[Row, 42].Value = 0m;
            Grd1.Cells[Row, 43].Value = 0m;
            Grd1.Cells[Row, 44].Value = 0m;

            if (Sm.GetGrdBool(Grd1, Row, 37) && Sm.GetLue(LueTaxCode1).Length > 0)
            {
                decimal Amt = (Qty * UPriceBeftax) * GetTaxRate(Sm.GetLue(LueTaxCode1)) * 0.01m;
                if (mIsDOCtAmtRounded) Amt = decimal.Truncate(Amt);
                TaxAmt += Amt;
                Grd1.Cells[Row, 42].Value = Amt;
            }
            if (Sm.GetGrdBool(Grd1, Row, 38) && Sm.GetLue(LueTaxCode2).Length > 0)
            {
                decimal Amt = (Qty * UPriceBeftax) * GetTaxRate(Sm.GetLue(LueTaxCode2)) * 0.01m;
                if (mIsDOCtAmtRounded) Amt = decimal.Truncate(Amt);
                TaxAmt += Amt;
                Grd1.Cells[Row, 43].Value = Amt;
            }
            if (Sm.GetGrdBool(Grd1, Row, 39) && Sm.GetLue(LueTaxCode3).Length > 0)
            {
                decimal Amt = (Qty * UPriceBeftax) * GetTaxRate(Sm.GetLue(LueTaxCode3)) * 0.01m;
                if (mIsDOCtAmtRounded) Amt = decimal.Truncate(Amt);
                TaxAmt += Amt;
                Grd1.Cells[Row, 44].Value = Amt;
            }

            Grd1.Cells[Row, 40].Value = Sm.FormatNum(TaxAmt, 0);
        }

        private void ReloadCustomer()
        {
            DteDueDt.EditValue = null;
            ClearGrd();
            ComputeTotalQtyPackagingUnit();
            ComputeTotalQty();
            ComputeAmt();
            if (Sm.GetLue(LueCtCode).Length > 0)
                ShowCustomerDepositSummary(Sm.GetLue(LueCtCode));
        }

        private void DownloadFile(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);
                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                //MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                Sm.StdMsg(mMsgType.Warning, "There was an error connecting to the FTP Server.");
            }
        }

        private void UploadFile(string DocNo, int Row, string FileName)
        {
            if (IsUploadFileNotValid(Row, FileName)) return;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", FileName));
            long mFileSize = toUpload.Length;
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", FileName));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);

                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);
                }
            }
            while (bytesRead != 0);

            file.Close();
            ftpStream.Close();
            
            var cml = new List<MySqlCommand>();
            cml.Add(UpdateSalesInvoiceFile(DocNo, Row, toUpload.Name));
            Sm.ExecCommands(cml);            
        }

        private bool IsUploadFileNotValid(int Row, string FileName)
        {
            return
                IsFTPClientDataNotValid(Row, FileName) ||
                IsFileSizeNotvalid(Row, FileName)
             ;
        }

        private bool IsFTPClientDataNotValid(int Row, string FileName)
        {
            if (mIsSalesInvoiceAllowToUploadFile && FileName.Length > 0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }

            if (mIsSalesInvoiceAllowToUploadFile && FileName.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }

            if (mIsSalesInvoiceAllowToUploadFile && FileName.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }

            if (mIsSalesInvoiceAllowToUploadFile && FileName.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }
            return false;
        }

        private bool IsFileSizeNotvalid(int Row, string FileName)
        {
            if (mIsSalesInvoiceAllowToUploadFile && FileName.Length > 0)
            {
                FileInfo f = new FileInfo(FileName);
                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }

                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File too large to Upload in row " + (Row + 1));
                    return true;
                }
            }
            return false;
        }

        internal void ComputeTotalQtyPackagingUnit()
        {
            var TotalQtyPackagingUnit = 0m;

            for(int r =0;r<Grd1.Rows.Count;r++)
                if (Sm.GetGrdStr(Grd1, r, 18).Length > 0) TotalQtyPackagingUnit += Sm.GetGrdDec(Grd1, r, 18);

            TxtTotalQtyPackagingUnit.EditValue = Sm.FormatNum(TotalQtyPackagingUnit, 0);
        }
        
        internal void ComputeTotalQty()
        {
            var TotalQty = 0m;
            
            for (int r = 0; r < Grd1.Rows.Count; r++)
                if (Sm.GetGrdStr(Grd1, r, 20).Length > 0) TotalQty += Sm.GetGrdDec(Grd1, r, 20);

            TxtTotalQty.EditValue = Sm.FormatNum(TotalQty, 0);
        }

        private void ProcessDepositSummary(ref List<DepositSummary> l)
        {
            if (Sm.CompareStr(Sm.GetLue(LueCurCode), mMainCurCode)) return;

            decimal Downpayment = 0m;

            if (TxtDownpayment.Text.Length > 0)
                Downpayment = decimal.Parse(TxtDownpayment.Text);

            if (Downpayment <= 0m) return;

            GetDepositSummary1(ref l);

            if (l.Count > 0m)
            {
                for (int i = 0; i < l.Count; i++)
                {
                    if (Downpayment > 0m)
                    {
                        if (Downpayment >= l[i].Amt)
                        {
                            l[i].UsedAmt = l[i].Amt;
                            Downpayment -= l[i].Amt;
                        }
                        else
                        {
                            l[i].UsedAmt = Downpayment;
                            Downpayment = 0m;
                            break;
                        }
                    }
                    else
                        break;
                }

                for (int i = l.Count - 1; i >= 0; i--)
                    if (l[i].UsedAmt == 0m) l.RemoveAt(i);
            }
        }

        private void GetDepositSummary1(ref List<DepositSummary> l)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select ExcRate, Amt ");
            SQL.AppendLine("From TblCustomerDepositSummary2 ");
            SQL.AppendLine("Where CtCode=@CtCode ");
            SQL.AppendLine("And CurCode=@CurCode ");
            SQL.AppendLine("And Amt>0.00 ");
            SQL.AppendLine("Order By CreateDt; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = SQL.ToString()
                };
                Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
                Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ExcRate", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new DepositSummary()
                        {
                            ExcRate = Sm.DrDec(dr, c[0]),
                            Amt = Sm.DrDec(dr, c[1]),
                            UsedAmt = 0m
                        });
                    }
                }
                dr.Close();
            }
        }

        private void GetDepositSummary2(ref List<DepositSummary> l)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select ExcRate, Amt ");
            SQL.AppendLine("From TblSalesInvoiceDtl3 ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("Order By CreateDt; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = SQL.ToString()
                };
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ExcRate", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new DepositSummary()
                        {
                            ExcRate = Sm.DrDec(dr, c[0]),
                            UsedAmt = Sm.DrDec(dr, c[1]),
                            Amt = 0m
                        });
                    }
                }
                dr.Close();
            }
        }

        //private MySqlCommand SaveExcRate(ref List<Rate> lR, int i, string DocNo, decimal DP)
        //{
        //    var SQL1 = new StringBuilder();

        //    decimal x = ((DP - lR[i].Amt) >= 0) ? lR[i].Amt : DP;

        //    SQL1.AppendLine("Select @DP:=(Case @CancelInd When 'Y' Then -1 Else 1 End * " + x + "); ");

        //    SQL1.AppendLine("Update TblCustomerDepositSummary2 ");
        //    SQL1.AppendLine("    Set Amt = Amt-@DP ");
        //    SQL1.AppendLine("Where CtCode = @CtCode And CurCode = @CurCode And ExcRate = @ExcRate; ");

        //    SQL1.AppendLine("Insert Into TblSalesInvoiceDtl3(DocNo, CurCode, ExcRate, Amt, CreateBy, CreateDt) ");
        //    SQL1.AppendLine("Select @DocNo, @CurCode, @ExcRate, @DP, @CreateBy, CurrentDateTime() ");
        //    SQL1.AppendLine("On Duplicate Key ");
        //    SQL1.AppendLine("   Update Amt=Amt-@DP, LastUpBy=@CreateBy, LastUpDt=CurrentDateTime(); ");

        //    var cm1 = new MySqlCommand() { CommandText = SQL1.ToString() };
        //    Sm.CmParam<String>(ref cm1, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm1, "@CtCode", lR[i].CtCode);
        //    Sm.CmParam<String>(ref cm1, "@CurCode", lR[i].CurCode);
        //    Sm.CmParam<Decimal>(ref cm1, "@ExcRate", lR[i].ExcRate);
        //    Sm.CmParam<String>(ref cm1, "@CancelInd", (ChkCancelInd.Checked) ? "Y" : "N");
        //    Sm.CmParam<String>(ref cm1, "@CreateBy", Gv.CurrentUserCode);

        //    return cm1;
        //}

        //private MySqlCommand UpdateSummary2(ref List<Rate> lR, int i)
        //{
        //    var SQL1 = new StringBuilder();

        //    SQL1.AppendLine("Update TblCustomerDepositSummary2 ");
        //    SQL1.AppendLine("    Set Amt = Amt + @Amt, LastUpBy = @CreateBy, LastUpDt = CurrentDateTime() ");
        //    SQL1.AppendLine("Where CtCode = @CtCode And CurCode = @CurCode And ExcRate = @ExcRate; ");

        //    var cm1 = new MySqlCommand() { CommandText = SQL1.ToString() };
        //    Sm.CmParam<String>(ref cm1, "@CtCode", lR[i].CtCode);
        //    Sm.CmParam<String>(ref cm1, "@CurCode", lR[i].CurCode);
        //    Sm.CmParam<Decimal>(ref cm1, "@ExcRate", lR[i].ExcRate);
        //    Sm.CmParam<Decimal>(ref cm1, "@Amt", lR[i].Amt);
        //    Sm.CmParam<String>(ref cm1, "@CreateBy", Gv.CurrentUserCode);

        //    return cm1;
        //}

        private void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'HostAddrForFTPClient', 'SharedFolderForFTPClient', 'UsernameForFTPClient', 'PasswordForFTPClient', 'PortForFTPClient', ");
            SQL.AppendLine("'FileSizeMaxUploadFTPClient', 'MainCurCode', 'FormPrintOutInvoice', 'FormPrintOutInvoiceReceipt', 'EmpCodeSI', ");
            SQL.AppendLine("'IsJournalValidationSalesInvoiceEnabled', 'SalesInvoiceTaxCalculationFormula', 'SLITaxCalculationFormat', 'DocNoFormat', 'EmpCodeTaxCollector', ");
            SQL.AppendLine("'IsSLIJournalUseCCCode', 'IsFilterByCtCt', 'IsCustomerComboBasedOnCategory', 'IsCustomerComboShowCategory', 'IsCheckCOAJournalNotExists', ");
            SQL.AppendLine("'IsSLIPrintShowAllTax', 'IsSalesInvoiceJournalUseTaxAcNo2', 'IsSLIShowDataAfterSave', 'IsSalesInvoiceJournalUseTaxAcNo2', 'IsShowCustomerCategory', ");
            SQL.AppendLine("'IsBOMShowSpecifications', 'IsSalesInvoiceAllowToUploadFile', 'IsSalesInvoiceTaxEnabled', 'IsSLIUseAutoFacturNumber', 'IsSalesInvoiceCOANonTaxable', ");
            SQL.AppendLine("'IsAutoJournalActived', 'IsItCtFilteredByGroup', 'IsCustomerItemNameMandatory', 'IsRemarkForJournalMandatory', 'MIndForSales', ");
            SQL.AppendLine("'IsDOCtAmtRounded', 'IsSalesInvoice3NotAutoChooseSalesPerson' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "MIndForSales": mMIndForSales = ParValue == "Y"; break;
                            case "IsRemarkForJournalMandatory": mIsRemarkForJournalMandatory = ParValue == "Y"; break;
                            case "IsCustomerItemNameMandatory": mIsCustomerItemNameMandatory = ParValue == "Y"; break;
                            case "IsItCtFilteredByGroup": mIsItCtFilteredByGroup = ParValue == "Y"; break;
                            case "IsAutoJournalActived": mIsAutoJournalActived = ParValue == "Y"; break;
                            case "IsSalesInvoiceCOANonTaxable": mIsSalesInvoiceCOANonTaxable = ParValue == "Y"; break;
                            case "IsSLIUseAutoFacturNumber": mIsSLIUseAutoFacturNumber = ParValue == "Y"; break;
                            case "IsSalesInvoiceTaxEnabled": mIsSalesInvoiceTaxEnabled = ParValue == "Y"; break;
                            case "IsSalesInvoiceAllowToUploadFile": mIsSalesInvoiceAllowToUploadFile = ParValue == "Y"; break;
                            case "IsBOMShowSpecifications": mIsBOMShowSpecifications = ParValue == "Y"; break;
                            case "IsSalesInvoiceJournalUseTaxAcNo2": mIsSalesInvoiceJournalUseTaxAcNo2 = ParValue == "Y"; break;
                            case "IsSLIShowDataAfterSave": mIsSLIShowDataAfterSave = ParValue == "Y"; break;
                            case "IsSLIPrintShowAllTax": mIsSLIPrintShowAllTax = ParValue == "Y"; break;
                            case "IsCheckCOAJournalNotExists": mIsCheckCOAJournalNotExists = ParValue == "Y"; break;
                            case "IsCustomerComboShowCategory": mIsCustomerComboShowCategory = ParValue == "Y"; break;
                            case "IsCustomerComboBasedOnCategory": mIsCustomerComboBasedOnCategory = ParValue == "Y"; break;
                            case "IsSLIJournalUseCCCode": mIsSLIJournalUseCCCode = ParValue == "Y"; break;
                            case "IsShowCustomerCategory": mIsShowCustomerCategory = ParValue == "Y"; break;
                            case "IsFilterByCtCt": mIsFilterByCtCt = ParValue == "Y"; break;
                            case "IsSalesInvoice3NotAutoChooseSalesPerson": mIsSalesInvoice3NotAutoChooseSalesPerson = ParValue == "Y"; break;
                            case "IsDOCtAmtRounded": mIsDOCtAmtRounded = ParValue == "Y"; break;
                            case "IsJournalValidationSalesInvoiceEnabled": mIsJournalValidationSalesInvoiceEnabled = ParValue == "Y"; break;

                            //string
                            case "SalesInvoiceTaxCalculationFormula": mSalesInvoiceTaxCalculationFormula = ParValue; break;
                            case "SLITaxCalculationFormat": mSLITaxCalculationFormat = ParValue; break;
                            case "DocNoFormat": mDocNoFormat = ParValue; break;
                            case "EmpCodeTaxCollector": mEmpCodeTaxCollector = ParValue; break;
                            case "FormPrintOutInvoiceReceipt": mFormPrintOutInvoiceReceipt = ParValue; break;
                            case "FormPrintOutInvoice": mIsFormPrintOutInvoice = ParValue; break;
                            case "MainCurCode": mMainCurCode = ParValue; break;
                            case "FileSizeMaxUploadFTPClient": mFileSizeMaxUploadFTPClient = ParValue; break;
                            case "EmpCodeSI": mEmpCodeSI = ParValue; break;
                            case "PortForFTPClient": mPortForFTPClient = ParValue; break;
                            case "PasswordForFTPClient": mPortForFTPClient = ParValue; break;
                            case "UsernameForFTPClient": mUsernameForFTPClient = ParValue; break;
                            case "SharedFolderForFTPClient": mSharedFolderForFTPClient = ParValue; break;
                            case "HostAddrForFTPClient": mHostAddrForFTPClient = ParValue; break;
                        }
                    }
                }
                dr.Close();
            }

            if (mSLITaxCalculationFormat.Length == 0) mSLITaxCalculationFormat = "1";
            if (mSalesInvoiceTaxCalculationFormula.Length == 0) mSalesInvoiceTaxCalculationFormula = "1";
        }

        private void SetLueSPCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select SPCode As Col1, SPName As Col2 From TblSalesPerson " +
                "Union ALL Select 'All' As Col1, 'ALL' As Col2 Order By Col2",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");

        }

        private void LueRequestEdit(
         iGrid Grd,
         DevExpress.XtraEditors.LookUpEdit Lue,
         ref iGCell fCell,
         ref bool fAccept,
         TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, 0).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, 0));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        private void SetLueCtCtCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct T1.CtCtCode As Col1, T1.CtCtName As Col2 ");
            SQL.AppendLine("From TblCustomerCategory T1 ");
            SQL.AppendLine("Inner Join TblCustomer T2 On T1.CtCtCode = T2.CtCtCode ");
            SQL.AppendLine("    And T2.CtCode In ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select A.CtCode ");
            SQL.AppendLine("        From TblDOCt2Hdr A ");
            SQL.AppendLine("        Inner Join TblDOCt2Dtl2 B On A.DocNo=B.DocNo And B.Qty>0 And B.ProcessInd='O' ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select A.CtCode ");
            SQL.AppendLine("        From TblDOCt2Hdr A ");
            SQL.AppendLine("        Inner Join TblDOCt2Dtl3 B On A.DocNo=B.DocNo And B.Qty>0 And B.ProcessInd='O' ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("Order By T1.CtCtName; ");

            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void SetLueCtCodeBasedOnCategory(ref LookUpEdit Lue, string Code, string IsFilterByCtCt, string CtCtCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct T.CtCode As Col1, ");
            
            if (mIsCustomerComboShowCategory)
                SQL.AppendLine("Concat(T.CtName, ' [', T1.CtCtName, ']') As Col2 ");
            else
                SQL.AppendLine("T.CtName As Col2 ");

            SQL.AppendLine("From TblCustomer T ");
            SQL.AppendLine("Left Join TblCustomerCategory T1 On T.CtCtCode = T1.CtCtCode ");
            if (Code.Length > 0)
                SQL.AppendLine("Where T.CtCode=@Code ");
            else
            {
                SQL.AppendLine("Where 1=1 ");
                SQL.AppendLine("And T.CtCtCode = @CtCtCode ");
                if (IsFilterByCtCt == "Y")
                {
                    SQL.AppendLine("And (T.CtCtCode Is Null Or (T.CtCtCode Is Not Null ");
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupCustomerCategory ");
                    SQL.AppendLine("    Where CtCtCode=T.CtCtCode ");
                    SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("    ))) ");
                }
                SQL.AppendLine("And T.CtCode In ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select A.CtCode ");
                SQL.AppendLine("    From TblDOCt2Hdr A ");
                SQL.AppendLine("    Inner Join TblDOCt2Dtl2 B On A.DocNo=B.DocNo And B.Qty>0 And B.ProcessInd='O' ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select A.CtCode ");
                SQL.AppendLine("    From TblDOCt2Hdr A ");
                SQL.AppendLine("    Inner Join TblDOCt2Dtl3 B On A.DocNo=B.DocNo And B.Qty>0 And B.ProcessInd='O' ");
                SQL.AppendLine(") ");
                SQL.AppendLine("Order By T.CtName ");
            }

            SQL.AppendLine("; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            if (Code.Length > 0)
                Sm.CmParam<String>(ref cm, "@Code", Code);
            else
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            Sm.CmParam<String>(ref cm, "@CtCtCode", CtCtCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (Code.Length > 0) Sm.SetLue(Lue, Code);
        }

        private void SetLueCtCode(ref DXE.LookUpEdit Lue, string CtCode)
        {
            try
            {
                var SQL = new StringBuilder();

                if (CtCode.Length == 0)
                {
                    SQL.AppendLine("Select Distinct T1.CtCode As Col1, ");
                    if (mIsCustomerComboShowCategory)
                        SQL.AppendLine("Concat(T2.CtName, ' [',T3.CtCtName,']') As Col2");
                    else
                        SQL.AppendLine("T2.CtName As Col2");
                    SQL.AppendLine("From ( ");
                    SQL.AppendLine("    Select A.CtCode ");
                    SQL.AppendLine("    From TblDOCt2Hdr A ");
                    SQL.AppendLine("    Inner Join TblDOCt2Dtl2 B On A.DocNo=B.DocNo And B.Qty>0 And B.ProcessInd='O' ");
                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("    Select A.CtCode ");
                    SQL.AppendLine("    From TblDOCt2Hdr A ");
                    SQL.AppendLine("    Inner Join TblDOCt2Dtl3 B On A.DocNo=B.DocNo And B.Qty>0 And B.ProcessInd='O' ");
                    SQL.AppendLine("    ) T1 ");
                    SQL.AppendLine("Inner Join TblCustomer T2 On T1.CtCode=T2.CtCode ");
                    SQL.AppendLine("Inner Join TblCustomerCategory T3 On T2.CtCtCode = T3.CtCtCode ");
                    SQL.AppendLine("Order By T2.CtName; ");
                }
                else
                {
                    SQL.AppendLine("Select Distinct T1.CtCode As Col1, ");
                    if (mIsCustomerComboShowCategory)
                        SQL.AppendLine("Concat(T1.CtName, ' [',T2.CtCtName,']') As Col2");
                    else
                        SQL.AppendLine("T1.CtName As Col2");
                    SQL.AppendLine("From TblCustomer T1 ");
                    SQL.AppendLine("Inner Join TblCustomerCategory T2 On T1.CtCtCode = T2.CtCtCode ");
                    SQL.AppendLine("Where T1.CtCode='" + CtCode + "'");
                }
                
                Sm.SetLue2(
                    ref Lue, SQL.ToString(),
                    0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetLueOptionCode(ref DXE.LookUpEdit Lue)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select OptCode As Col1, OptDesc As Col2 From TblOption Where OptCat='AccountDescriptionOnSalesInvoice' ");

                Sm.SetLue2(
                    ref Lue, SQL.ToString(),
                    0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        internal void ComputeAmt()
        { 
            decimal COAAmt = 0m, Amt = 0m, TotalTax = 0m, Downpayment = 0m;

            if (TxtDownpayment.Text.Length > 0) Downpayment = decimal.Parse(TxtDownpayment.Text);

            //var CustomerAcNoAR = 
            //    Sm.GetValue(
            //            "Select (Select Concat(ParValue, A.CtCode) From TblParameter Where ParCode='CustomerAcNoAR' Limit 1) As AcNo " +
            //            "From TblCustomer A, TblCustomerCategory B " +
            //            "Where A.CtCtCode is Not Null " +
            //            "And A.CtCtCode=B.CtCtCode " +
            //            "And A.CtCode='"+Sm.GetLue(LueCtCode)+"' Limit 1; "
            //            );

            if (mSLITaxCalculationFormat == "1") // default
            {
                #region Default
                for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                {
                    string AcType = Sm.GetValue("Select AcType From TblCoa Where AcNo=@Param;", Sm.GetGrdStr(Grd3, Row, 1));
                    if (Sm.GetGrdBool(Grd3, Row, 3))
                    {
                        if (AcType == "D")
                            COAAmt += Sm.GetGrdDec(Grd3, Row, 4);
                        else
                            COAAmt -= Sm.GetGrdDec(Grd3, Row, 4);
                    }
                    if (Sm.GetGrdBool(Grd3, Row, 5))
                    {
                        if (AcType == "C")
                            COAAmt += Sm.GetGrdDec(Grd3, Row, 6);
                        else
                            COAAmt -= Sm.GetGrdDec(Grd3, Row, 6);
                    }
                }

                Amt = COAAmt;
                if (mIsSalesInvoiceTaxEnabled)
                {
                    for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                        Amt += (Sm.GetGrdDec(Grd1, Row, 20) * Sm.GetGrdDec(Grd1, Row, 23));

                    TxtTotalAmt.EditValue = Sm.FormatNum(Amt, 0);
                    if (mIsSalesInvoiceCOANonTaxable)
                        ComputeTax(Amt - COAAmt);
                    else
                        ComputeTax(Amt);
                    TotalTax = Decimal.Parse(TxtTaxAmt1.Text) + Decimal.Parse(TxtTaxAmt2.Text) + Decimal.Parse(TxtTaxAmt3.Text);
                    TxtTotalTax.EditValue = Sm.FormatNum(TotalTax, 0);
                    TxtAmt.EditValue = Sm.FormatNum(Amt + TotalTax - Downpayment, 0);
                }
                else
                {
                    for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                        if (Sm.GetGrdStr(Grd1, Row, 27).Length > 0) Amt += Sm.GetGrdDec(Grd1, Row, 27);

                    for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                        if (Sm.GetGrdStr(Grd1, Row, 25).Length > 0)
                            TotalTax += (Sm.GetGrdDec(Grd1, Row, 20) * Sm.GetGrdDec(Grd1, Row, 25));

                    TxtTotalAmt.EditValue = Sm.FormatNum(Amt - TotalTax, 0);
                    TxtTotalTax.EditValue = Sm.FormatNum(TotalTax, 0);
                    TxtAmt.EditValue = Sm.FormatNum(Amt - Downpayment, 0);
                }
                #endregion
            }
            else // SRN, pilihan additional cost & discount nya ada yg masuk hitungan atau enggak
            {
                #region SRN
                string CtCode = Sm.GetLue(LueCtCode);
                mAdditionalCostDiscAmt = 0m;

                for (int Row = 0; Row < Grd3.Rows.Count - 1; ++Row)
                {
                    string AcNo = Sm.GetGrdStr(Grd3, Row, 1);
                    if (!AcNo.Contains(string.Concat(".", CtCode))) // kalau bukan COA customer
                    {
                        //kalau dicentang di debit, (-)
                        if (Sm.GetGrdBool(Grd3, Row, 3))
                        {
                            COAAmt -= Sm.GetGrdDec(Grd3, Row, 4);
                        }
                        //kalau dicentang di kredit, (+)
                        if (Sm.GetGrdBool(Grd3, Row, 5))
                        {
                            COAAmt += Sm.GetGrdDec(Grd3, Row, 6);
                        }
                        
                        //kalau ga di centang baik debit dan kredit, masuk ke AdditionalCostDiscAmt
                        if (!Sm.GetGrdBool(Grd3, Row, 3) && !Sm.GetGrdBool(Grd3, Row, 5))
                        {
                            mAdditionalCostDiscAmt -= Sm.GetGrdDec(Grd3, Row, 4);
                            mAdditionalCostDiscAmt += Sm.GetGrdDec(Grd3, Row, 6);
                        }
                    }
                }

                Amt = COAAmt;

                if (mIsSalesInvoiceTaxEnabled)
                {
                    for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    {
                        decimal Qty = Sm.GetGrdDec(Grd1, Row, 20);
                        decimal UPriceBefTax = Sm.GetGrdDec(Grd1, Row, 23);
                        
                        Amt += (Qty * UPriceBefTax);
                    }

                    if (mIsDOCtAmtRounded) Amt = decimal.Truncate(Amt);

                    TxtTotalAmt.EditValue = Sm.FormatNum(Amt, 0);
                    //if (mIsSalesInvoiceCOANonTaxable)
                    //    ComputeTax(Amt - COAAmt);
                    //else
                        ComputeTax(Amt);
                    TotalTax = Decimal.Parse(TxtTaxAmt1.Text) + Decimal.Parse(TxtTaxAmt2.Text) + Decimal.Parse(TxtTaxAmt3.Text);
                    TxtTotalTax.EditValue = Sm.FormatNum(TotalTax, 0);
                    TxtAmt.EditValue = Sm.FormatNum(Amt + TotalTax - Downpayment, 0);
                }
                else
                {
                    for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    {
                        if (Sm.GetGrdStr(Grd1, Row, 27).Length > 0)
                        {
                            decimal Amount = Sm.GetGrdDec(Grd1, Row, 27);
                            
                            Amt += Amount;
                        }
                    }

                    for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    {
                        if (Sm.GetGrdStr(Grd1, Row, 25).Length > 0)
                        {
                            decimal Qty = Sm.GetGrdDec(Grd1, Row, 20);
                            decimal TaxAmt = 0m;
                            if (mSalesInvoiceTaxCalculationFormula == "1") TaxAmt = Sm.GetGrdDec(Grd1, Row, 25);
                            else TaxAmt = Sm.GetGrdDec(Grd1, Row, 40);
                            
                            TotalTax += (Qty * TaxAmt);
                        }
                    }

                    if (mIsDOCtAmtRounded)
                    {
                        Amt = decimal.Truncate(Amt);
                        TotalTax = decimal.Truncate(TotalTax);
                        Downpayment = decimal.Truncate(Downpayment);
                        if (mAdditionalCostDiscAmt != 0m) mAdditionalCostDiscAmt = decimal.Truncate(mAdditionalCostDiscAmt);
                    }

                    TxtTotalAmt.EditValue = Sm.FormatNum(Amt - TotalTax, 0);
                    TxtTotalTax.EditValue = Sm.FormatNum(TotalTax, 0);
                    TxtAmt.EditValue = Sm.FormatNum(Amt - Downpayment, 0);
                }

                // ditambahkan nilai additional cost discount yg ga dicentang
                TxtAmt.EditValue = Sm.FormatNum((Decimal.Parse(TxtAmt.Text) + mAdditionalCostDiscAmt), 0);
                #endregion
            }
        }


        private void ComputeTax(decimal Amt)
        {
            string
                TaxCode1 = Sm.GetLue(LueTaxCode1),
                TaxCode2 = Sm.GetLue(LueTaxCode2),
                TaxCode3 = Sm.GetLue(LueTaxCode3);

            Sm.SetControlNumValueZero(new List<DXE.TextEdit>
            { TxtTaxAmt1, TxtTaxAmt2, TxtTaxAmt3 }, 0);

            if (mSalesInvoiceTaxCalculationFormula == "1")
            {
                if (mIsDOCtAmtRounded)
                {
                    if (TaxCode1.Length != 0) TxtTaxAmt1.Text = Sm.FormatNum(decimal.Truncate(GetTaxRate(TaxCode1) * 0.01m * Amt), 0);
                    if (TaxCode2.Length != 0) TxtTaxAmt2.Text = Sm.FormatNum(decimal.Truncate(GetTaxRate(TaxCode2) * 0.01m * Amt), 0);
                    if (TaxCode3.Length != 0) TxtTaxAmt3.Text = Sm.FormatNum(decimal.Truncate(GetTaxRate(TaxCode3) * 0.01m * Amt), 0);
                }
                else
                {
                    if (TaxCode1.Length != 0) TxtTaxAmt1.Text = Sm.FormatNum(GetTaxRate(TaxCode1) * 0.01m * Amt, 0);
                    if (TaxCode2.Length != 0) TxtTaxAmt2.Text = Sm.FormatNum(GetTaxRate(TaxCode2) * 0.01m * Amt, 0);
                    if (TaxCode3.Length != 0) TxtTaxAmt3.Text = Sm.FormatNum(GetTaxRate(TaxCode3) * 0.01m * Amt, 0);
                }
            }
            else
            {
                if (TaxCode1.Length != 0)
                {
                    decimal TaxAmt = 0m;
                    TaxAmt = GetTaxAmtPerDetail(1);
                    TxtTaxAmt1.Text = Sm.FormatNum(TaxAmt, 0);
                }

                if (TaxCode2.Length != 0)
                {
                    decimal TaxAmt = 0m;
                    TaxAmt = GetTaxAmtPerDetail(2);
                    TxtTaxAmt2.Text = Sm.FormatNum(TaxAmt, 0);
                }

                if (TaxCode3.Length != 0)
                {
                    decimal TaxAmt = 0m;
                    TaxAmt = GetTaxAmtPerDetail(3);
                    TxtTaxAmt3.Text = Sm.FormatNum(TaxAmt, 0);
                }
            }
        }

        private decimal GetTaxAmtPerDetail(byte TaxNo)
        {
            decimal TaxAmt = 0m;
            int TaxCol = 0;

            if (TaxNo == 1) TaxCol = 37;
            if (TaxNo == 2) TaxCol = 38;
            if (TaxNo == 3) TaxCol = 39;

            for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
            {
                //decimal UPriceBefTax = Sm.GetGrdDec(Grd1, i, 23);
                //decimal Qty = Sm.GetGrdDec(Grd1, i, 20);

                //if (Sm.GetGrdBool(Grd1, i, TaxCol))
                //    TaxAmt += (Qty * UPriceBefTax) * TaxRate * 0.01m;

                if (Sm.GetGrdBool(Grd1, i, TaxCol))
                    TaxAmt += Sm.GetGrdDec(Grd1, i, (TaxCol + 5)); // hitungnya ini ada di ComputeTaxPerDetail2
            }

            if (mIsDOCtAmtRounded) TaxAmt = decimal.Truncate(TaxAmt);

            return TaxAmt;
        }

        private decimal GetTaxRate(string TaxCode)
        {
            return Sm.GetValueDec("Select TaxRate from TblTax Where TaxCode=@Param;", TaxCode);
        }

        internal string GetSelectedAcNo()
        {
            var SQL = string.Empty;
            if (Grd3.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd3, Row, 1).Length != 0)
                        SQL += "##" + Sm.GetGrdStr(Grd3, Row, 1) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        #region Convert To Words

        private static string[] _ones =
            {
                "Zero",
                "One",
                "Two",
                "Three",
                "Four",
                "Five",
                "Six",
                "Seven",
                "Eight",
                "Nine"
            };

        private static string[] _teens =
            {
                "Ten",
                "Eleven",
                "Twelve",
                "Thirteen",
                "Fourteen",
                "Fifteen",
                "Sixteen",
                "Seventeen",
                "Eighteen",
                "Nineteen"
            };

        private static string[] _tens =
            {
                "",
                "Ten",
                "Twenty",
                "Thirty",
                "Forty",
                "Fifty",
                "Sixty",
                "Seventy",
                "Eighty",
                "Ninety"
            };

        // US Nnumbering:
        private static string[] _thousands =
            {
                "",
                "Thousand",
                "Million",
                "Billion",
                "Trillion",
                "Quadrillion"
            };



        private static string Convert(decimal value)
        {
            string digits, temp;
            bool showThousands = false;
            bool allZeros = true;

            StringBuilder builder = new StringBuilder();
            digits = ((long)value).ToString();
            for (int i = digits.Length - 1; i >= 0; i--)
            {
                int ndigit = (int)(digits[i] - '0');
                int column = (digits.Length - (i + 1));

                switch (column % 3)
                {
                    case 0:        // Ones position
                        showThousands = true;
                        if (i == 0)
                        {
                            temp = String.Format("{0} ", _ones[ndigit]);
                        }
                        else if (digits[i - 1] == '1')
                        {
                            temp = String.Format("{0} ", _teens[ndigit]);
                            i--;
                        }
                        else if (ndigit != 0)
                        {
                            temp = String.Format("{0} ", _ones[ndigit]);
                        }
                        else
                        {
                            temp = String.Empty;
                            if (digits[i - 1] != '0' || (i > 1 && digits[i - 2] != '0'))
                                showThousands = true;
                            else
                                showThousands = false;
                        }

                        if (showThousands)
                        {
                            if (column > 0)
                            {
                                temp = String.Format("{0}{1}{2}",
                                    temp,
                                    _thousands[column / 3],
                                    allZeros ? " " : " ");
                            }
                            allZeros = false;
                        }
                        builder.Insert(0, temp);
                        break;

                    case 1:        // Tens column
                        if (ndigit > 0)
                        {
                            temp = String.Format("{0}{1}",
                                _tens[ndigit],
                                (digits[i + 1] != '0') ? " " : " ");
                            builder.Insert(0, temp);
                        }
                        break;

                    case 2:        // Hundreds column
                        if (ndigit > 0)
                        {
                            temp = String.Format("{0} Hundred ", _ones[ndigit]);
                            builder.Insert(0, temp);
                        }
                        break;
                }
            }

            string cents = value.ToString();
            decimal cettt = Decimal.Parse(cents.Substring(cents.Length - 2, 2));
            string cent = Sm.Terbilang2(cettt);
            builder.AppendFormat("Dollars And " + cent + " Cents # ", (value - (long)value) * 100);

            return String.Format("{0}{1} ",
                Char.ToUpper(builder[0]),
                builder.ToString(1, builder.Length - 1));
        }
        #endregion

        private void ParPrint()
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;

            #region IOK
            if (Sm.GetParameter("DocTitle") == "IOK")
            {
                var l = new List<InvoiceHdr>();
                var ldtl = new List<InvoiceDtl>();
                var ldtl2 = new List<InvoiceDtl2>();
                var lC = new List<AddCost>();
                var lCD = new List<AddCostDisc>();

                decimal mGrandTotal = 0m; decimal mAdDisc = 0m;

                for (int Row = 0; Row < Grd3.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd3, Row, 3) == true)
                    {
                        mAdDisc += Sm.GetGrdDec(Grd3, Row, 4);
                    }
                    if (Sm.GetGrdBool(Grd3, Row, 5) == true)
                    {
                        mAdDisc += Sm.GetGrdDec(Grd3, Row, 6);
                    }
                }

                if (decimal.Parse(TxtDownpayment.Text) == 0)
                    mGrandTotal = decimal.Parse(TxtAmt.Text) - mAdDisc;
                else
                    mGrandTotal = decimal.Parse(TxtTotalAmt.Text) + decimal.Parse(TxtTotalTax.Text) - mAdDisc;


                string[] TableName = { "InvoiceHdr", "InvoiceDtl", "InvoiceDtl2", "AddCost", "AddDisc" };

                List<IList> myLists = new List<IList>();
                var cm = new MySqlCommand();

                #region Header
                var SQL = new StringBuilder();
                SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyAddressFull', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyPhone', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle5') As 'CompanyFax', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='Shipper1') As 'Shipper1', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='Shipper2') As 'Shipper2', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='Shipper3') As 'Shipper3', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='Shipper4') As 'Shipper4', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='CompanyLocation2') As 'CompLocation2', ");
                SQL.AppendLine("A.LocalDocNo, DATE_FORMAT(A.DocDt,'%d %M %Y') As DocDt, ");
                SQL.AppendLine("A.CtCode, if(C.Doctype = '1',  Concat(F.SAName, '\n', F.SAAddress), '') As DesAddress, B.CtName, B.Address, DATE_FORMAT(A.DueDt,'%d %M %Y') As DueDt, ");
                SQL.AppendLine("A.CurCode, Round(A.TotalAmt, 2) As totalAmt, A.TotalTax, A.DownPayment, if(A.DownPayment = 0, round(A.Amt, 2), (Round(A.TotalAmt, 2)+A.TotalTax)) As Amt, A.SalesName, A.SignName, if(C.Doctype = '1', G.VdName, '') As ExpVd, ");
                SQL.AppendLine("if(C.Doctype = '1', D.ExpPlatNo, '') As ExpPlatNo, A.TaxInvDocument, H.bankAcNo, I.BankName As BankAcNm, A.Remark, A.cancelInd, ifnull((J.DAmt+J.CAmt), 0) As AddCost ");
                SQL.AppendLine("From TblSalesInvoiceHdr A ");
                SQL.AppendLine("Inner Join TblCustomer B On A.CtCode = B.CtCode ");
                SQL.AppendLine("Inner Join TblSalesInvoiceDtl C On A.DocNo = C.DocNo ");
                SQL.AppendLine("Inner Join TblDoct2Hdr D On C.DOCtDocNo = D.DocNo ");
                SQL.AppendLine("Left Join TblPlHdr E On D.PLDOcNo = E.DocNo ");
                SQL.AppendLine("Left Join TblDRHdr F On D.DrDOcNo = F.DocNo ");
                SQL.AppendLine("Left Join tblVendor G On F.ExpVdCode = G.VdCode ");
                SQL.AppendLine("Left Join TblBankAccount H On A.BankAcCode = H.BankAcCode ");
                SQL.AppendLine("Left Join TblBank I On H.BankCode = I.BankCode ");
                SQL.AppendLine("left jOin tblsalesInvoiceDtl2 J On A.DocNo = J.DocNo And J.ACInd = 'Y' ");
                SQL.AppendLine("Where A.DocNo=@DocNo ");

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = SQL.ToString();
                    Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                    Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[]
                        {
                        //0
                         "CompanyLogo",

                         //1-5
                         "CompanyName",
                         "CompanyAddress",
                         "CompanyAddressFull",
                         "CompanyPhone",
                         "CompanyFax",

                         //6-10
                         "LocalDocNo",
                         "DocDt",
                         "CtName",
                         "Address",
                         "DesAddress",

                         //11-15 
                         "CurCode",
                         "TotalAmt",
                         "TotalTax",
                         "DownPayment",
                         "Amt",

                         //16-20
                         "SalesName",
                         "ExpVd",
                         "ExpPlatNo",
                         "TaxInvDocument",
                         "BankAcNm",

                         //21-25
                         "BAnkAcNo",
                         "Remark",
                         "CancelInd",
                         "CompLocation2",
                         "AddCost",

                         //26
                         "SignName"
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            l.Add(new InvoiceHdr()
                            {
                                CompanyLogo = Sm.DrStr(dr, c[0]),

                                CompanyName = Sm.DrStr(dr, c[1]),
                                CompanyAddress = Sm.DrStr(dr, c[2]),
                                CompanyAddressFull = Sm.DrStr(dr, c[3]),
                                CompanyPhone = Sm.DrStr(dr, c[4]),
                                CompanyFax = Sm.DrStr(dr, c[5]),

                                LocalDocNo = Sm.DrStr(dr, c[6]),
                                DocDt = Sm.DrStr(dr, c[7]),
                                CtName = Sm.DrStr(dr, c[8]),
                                Address = Sm.DrStr(dr, c[9]),
                                DesAddress = Sm.DrStr(dr, c[10]),

                                CurCode = Sm.DrStr(dr, c[11]),
                                TotalAmt = decimal.Parse(TxtTotalAmt.Text),
                                TotalTax = decimal.Parse(TxtTotalTax.Text),
                                DownPayment = decimal.Parse(TxtDownpayment.Text),
                                Amt = decimal.Parse(TxtAmt.Text),
                                SalesName = (TxtSignName.Text.Length > 0) ? Sm.DrStr(dr, c[26]) : Sm.DrStr(dr, c[16]),
                                ExpVd = Sm.DrStr(dr, c[17]),
                                ExpPlatNo = Sm.DrStr(dr, c[18]),
                                TaxInvNo = Sm.DrStr(dr, c[19]),
                                BankAcNm = Sm.DrStr(dr, c[20]),
                                BankAcNo = Sm.DrStr(dr, c[21]),
                                Remark = Sm.DrStr(dr, c[22]),
                                Terbilang = Sm.Terbilang(mGrandTotal),
                                Terbilang2 = Convert(mGrandTotal),
                                CancelInd = Sm.DrStr(dr, c[23]),
                                CompLocation2 = Sm.DrStr(dr, c[24]),
                                AddCost = Sm.DrDec(dr, c[25]),

                                PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
                            });
                        }
                    }
                    dr.Close();
                }
                myLists.Add(l);
                #endregion

                #region Detail 1
                var cmDtl = new MySqlCommand();
                var SQLDtl = new StringBuilder();
                using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl.Open();
                    cmDtl.Connection = cnDtl;

                    SQLDtl.AppendLine("Select  ");
                    SQLDtl.AppendLine("A.DOCtDocNo, A.DOCtDNo, ");
                    SQLDtl.AppendLine("A.ItCode, ");
                    SQLDtl.AppendLine("If(A.DocType='1', K1.ForeignName, K2.ForeignName) As ItName,  ");
                    SQLDtl.AppendLine("A.QtyPackagingUnit, ");
                    SQLDtl.AppendLine("If(A.DocType='1', F1.PackagingUnitUomCode, F2.PackagingUnitUomCode) As PackagingUnitUomCode, ");
                    SQLDtl.AppendLine("A.Qty, ");
                    SQLDtl.AppendLine("If(A.DocType='1', I1.PriceUomCode, I2.PriceUomCode) As PriceUomCode, ");
                    SQLDtl.AppendLine("A.UPriceBeforeTax, ");
                    SQLDtl.AppendLine("A.TaxRate, ");
                    SQLDtl.AppendLine("A.TaxAmt, ");
                    SQLDtl.AppendLine("A.UPriceAfterTax, ");
                    SQLDtl.AppendLine("If(A.DocType='1', L1.PtName, L2.PtName) As PtName, ");
                    SQLDtl.AppendLine("Round((A.Qty * A.UPriceBeforeTax), 4) As Amt, ");
                    SQLDtl.AppendLine("if(A.Qty =0, 0, if(A.Doctype='1', ((N1.Qty2*A.Qty)/N1.Qty), ((N2.Qty2*A.Qty)/N2.Qty))) As Qty2,  ");
                    SQLDtl.AppendLine("0 As AddCost, 0 As AddDisc ");
                    SQLDtl.AppendLine("From TblSalesInvoiceDtl A ");
                    SQLDtl.AppendLine("Inner Join TblDOCt2Hdr B On A.DOCtDocNo=B.DocNo ");

                    SQLDtl.AppendLine("Left Join TblDOCt2Dtl2 C1 On A.DOCtDocNo=C1.DocNo And A.DOCtDNo=C1.DNo ");
                    SQLDtl.AppendLine("Left Join TblDRDtl D1 On B.DRDocNo=D1.DocNo And C1.DRDNo=D1.DNo ");
                    SQLDtl.AppendLine("Left Join TblSOHdr E1 On D1.SODocNo=E1.DocNo ");
                    SQLDtl.AppendLine("Left Join TblSODtl F1 On D1.SODocNo=F1.DocNo And D1.SODNo=F1.DNo ");
                    SQLDtl.AppendLine("Left Join TblCtQtHdr G1 On E1.CtQtDocNo=G1.DocNo  ");
                    SQLDtl.AppendLine("Left Join TblCtQtDtl H1 On E1.CtQtDocNo=H1.DocNo And F1.CtQtDNo=H1.DNo ");
                    SQLDtl.AppendLine("Left Join TblItemPriceHdr I1 On H1.ItemPriceDocNo=I1.DocNo  ");
                    SQLDtl.AppendLine("Left Join TblItemPriceDtl J1 On H1.ItemPriceDocNo=J1.DocNo And H1.ItemPriceDNo=J1.DNo ");
                    SQLDtl.AppendLine("Left Join TblItem K1 On J1.ItCode=K1.ItCode ");
                    SQLDtl.AppendLine("Left Join TblPaymentTerm L1 On G1.PtCode=L1.PtCode ");
                    SQLDtl.AppendLine("Left Join TblDRHdr M1 On B.DRDocNo=M1.DocNo ");
                    SQLDtl.AppendLine("Left Join TblItemPackagingUnit N1 On F1.PackagingUnitUomCode = N1.UomCOde And A.ItCode = N1.ItCode ");

                    SQLDtl.AppendLine("Left Join TblDOCt2Dtl3 C2 On A.DOCtDocNo=C2.DocNo And A.DOCtDNo=C2.DNo ");
                    SQLDtl.AppendLine("Left Join TblPLDtl D2 On B.PLDocNo=D2.DocNo And C2.PLDNo=D2.DNo ");
                    SQLDtl.AppendLine("Left Join TblSOHdr E2 On D2.SODocNo=E2.DocNo ");
                    SQLDtl.AppendLine("Left Join TblSODtl F2 On D2.SODocNo=F2.DocNo And D2.SODNo=F2.DNo ");
                    SQLDtl.AppendLine("Left Join TblCtQtHdr G2 On E2.CtQtDocNo=G2.DocNo ");
                    SQLDtl.AppendLine("Left Join TblCtQtDtl H2 On E2.CtQtDocNo=H2.DocNo And F2.CtQtDNo=H2.DNo ");
                    SQLDtl.AppendLine("Left Join TblItemPriceHdr I2 On H2.ItemPriceDocNo=I2.DocNo ");
                    SQLDtl.AppendLine("Left Join TblItemPriceDtl J2 On H2.ItemPriceDocNo=J2.DocNo And H2.ItemPriceDNo=J2.DNo ");
                    SQLDtl.AppendLine("Left Join TblItem K2 On J2.ItCode=K2.ItCode ");
                    SQLDtl.AppendLine("Left Join TblPaymentTerm L2 On G2.PtCode=L2.PtCode ");
                    SQLDtl.AppendLine("Left Join TblPLHdr M2 On B.PLDocNo=M2.DocNo ");
                    SQLDtl.AppendLine("Left Join TblItemPackagingUnit N2 On F2.PackagingUnitUomCode = N2.UomCOde And A.ItCode = N2.ItCode ");
                    SQLDtl.AppendLine("Where A.DocNo = @DocNo ");
                    SQLDtl.AppendLine("Group By A.DOCtDocNo, A.DOCtDNo ");

                    cmDtl.CommandText = SQLDtl.ToString();

                    Sm.CmParam<String>(ref cmDtl, "@DocNo", TxtDocNo.Text);
                    //Sm.CmParam<String>(ref cmDtl, "@CtCode", Sm.GetLue(LueCtCode));

                    var drDtl = cmDtl.ExecuteReader();
                    var cDtl = Sm.GetOrdinal(drDtl, new string[]
                    {
                     //0
                     "ItCode" ,

                     //1-5
                     "ItName" ,
                     "QtyPackagingUnit",
                     "PackagingUnitUomCode",
                     "Qty",
                     "PriceUomCode",

                     //6-10
                     "UPriceBeforeTax",
                     "TaxRate",
                     "TaxAmt",
                     "UPriceAfterTax",
                     "PtName",

                     //11-15
                     "Amt",
                     "Qty2",
                     "AddCost",
                     "AddDisc",
                    });
                    if (drDtl.HasRows)
                    {
                        while (drDtl.Read())
                        {
                            ldtl.Add(new InvoiceDtl()
                            {
                                ItCode = Sm.DrStr(drDtl, cDtl[0]),
                                ItName = Sm.DrStr(drDtl, cDtl[1]),
                                QtyPackagingUnit = Sm.DrDec(drDtl, cDtl[2]),
                                PackagingUnitUomCode = Sm.DrStr(drDtl, cDtl[3]),
                                Qty = Sm.DrDec(drDtl, cDtl[4]),
                                PriceUomCode = Sm.DrStr(drDtl, cDtl[5]),
                                UPriceBeforeTax = Sm.DrDec(drDtl, cDtl[6]),
                                TaxRate = Sm.DrDec(drDtl, cDtl[7]),
                                TaxAmt = Sm.DrDec(drDtl, cDtl[8]),
                                UPriceAfterTax = Sm.DrDec(drDtl, cDtl[9]),
                                PtName = Sm.DrStr(drDtl, cDtl[10]),
                                Amt = Sm.DrDec(drDtl, cDtl[11]),
                                CurCode = Sm.GetLue(LueCurCode),

                                Qty2 = Sm.DrDec(drDtl, cDtl[12]),
                                AddCost = Sm.DrDec(drDtl, cDtl[13]),
                                AddDisc = Sm.DrDec(drDtl, cDtl[14]),
                            });
                        }
                    }
                    drDtl.Close();
                }
                myLists.Add(ldtl);
                #endregion

                #region Detail 2
                var cmDtl2 = new MySqlCommand();
                var SQLDtl2 = new StringBuilder();
                using (var cnDtl2 = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl2.Open();
                    cmDtl2.Connection = cnDtl2;

                    SQLDtl2.AppendLine("Select ");
                    SQLDtl2.AppendLine("If(A.DocType='1', Group_Concat(Distinct E1.CtPONo SEPARATOR ','), Group_Concat(Distinct E2.CtPONo SEPARATOR ',')) As PONumber, ");
                    SQLDtl2.AppendLine("If(A.DocType='1', Group_concat(Distinct E1.LocalDocNo separator ', '), Group_concat(Distinct E2.LocalDocNo separator ', ')) As SODocNo ");
                    SQLDtl2.AppendLine("From TblSalesInvoiceDtl A ");
                    SQLDtl2.AppendLine("Inner Join TblDOCt2Hdr B On A.DOCtDocNo=B.DocNo ");
                    SQLDtl2.AppendLine("Left Join TblDOCt2Dtl2 C1 On A.DOCtDocNo=C1.DocNo And A.DOCtDNo=C1.DNo ");
                    SQLDtl2.AppendLine("Left Join TblDRDtl D1 On B.DRDocNo=D1.DocNo And C1.DRDNo=D1.DNo ");
                    SQLDtl2.AppendLine("Left Join TblSOHdr E1 On D1.SODocNo=E1.DocNo ");
                    SQLDtl2.AppendLine("Left Join TblSODtl F1 On D1.SODocNo=F1.DocNo And D1.SODNo=F1.DNo ");
                    SQLDtl2.AppendLine("Left Join TblDOCt2Dtl3 C2 On A.DOCtDocNo=C2.DocNo And A.DOCtDNo=C2.DNo ");
                    SQLDtl2.AppendLine("Left Join TblPLDtl D2 On B.PLDocNo=D2.DocNo And C2.PLDNo=D2.DNo ");
                    SQLDtl2.AppendLine("Left Join TblSOHdr E2 On D2.SODocNo=E2.DocNo ");
                    SQLDtl2.AppendLine("Left Join TblSODtl F2 On D2.SODocNo=F2.DocNo And D2.SODNo=F2.DNo ");
                    SQLDtl2.AppendLine("Where A.DocNo = @DocNo ");
                    SQLDtl2.AppendLine("Group BY A.DocNo ");

                    cmDtl2.CommandText = SQLDtl2.ToString();

                    Sm.CmParam<String>(ref cmDtl2, "@DocNo", TxtDocNo.Text);

                    var drDtl2 = cmDtl2.ExecuteReader();
                    var cDtl2 = Sm.GetOrdinal(drDtl2, new string[]
                    {
                     //0
                     "PONumber" ,

                     //1-5
                     "SODocNo" ,
                    });
                    if (drDtl2.HasRows)
                    {
                        while (drDtl2.Read())
                        {
                            ldtl2.Add(new InvoiceDtl2()
                            {
                                PONumber = Sm.DrStr(drDtl2, cDtl2[0]),
                                SODocNo = Sm.DrStr(drDtl2, cDtl2[1]),
                            });
                        }
                    }
                    drDtl2.Close();
                }
                myLists.Add(ldtl2);
                #endregion

                #region Additional Cost

                var cmC = new MySqlCommand();
                var SQLC = new StringBuilder();
                using (var cnC = new MySqlConnection(Gv.ConnectionString))
                {
                    cnC.Open();
                    cmC.Connection = cnC;

                    SQLC.AppendLine("    select DocNo, AcNo, DAmt, CAmt from tblsalesinvoicedtl2 ");
                    SQLC.AppendLine("    where acind = 'Y' ");
                    SQLC.AppendLine("    And DocNo = @DocNo ");

                    cmC.CommandText = SQLC.ToString();

                    Sm.CmParam<String>(ref cmC, "@DocNo", TxtDocNo.Text);

                    var drC = cmC.ExecuteReader();
                    var cDtC = Sm.GetOrdinal(drC, new string[]
                    {
                     //0
                     "DocNo" ,

                     //1-3
                     "AcNo", "DAmt", "CAmt"
                    });

                    decimal mAddCost = 0m;
                    decimal mAddDisc = 0m;

                    if (drC.HasRows)
                    {
                        while (drC.Read())
                        {
                            lC.Add(new AddCost()
                            {
                                DocNo = Sm.DrStr(drC, cDtC[0]),
                                AcNo = Sm.DrStr(drC, cDtC[1]),
                                DAmt = Sm.DrDec(drC, cDtC[2]),
                                CAmt = Sm.DrDec(drC, cDtC[3]),
                            });
                        }
                    }
                    drC.Close();

                    if (lC.Count > 0)
                    {
                        for (int Row = 0; Row < lC.Count; Row++)
                        {
                            string AcType = Sm.GetValue("Select AcType From TblCoa Where AcNo=@Param;", lC[Row].AcNo);
                            if (lC[Row].DAmt > 0)
                            {
                                if (AcType == "D")
                                    mAddCost += lC[Row].DAmt;
                                else
                                    mAddDisc -= lC[Row].DAmt;
                            }
                            if (lC[Row].CAmt > 0)
                            {
                                if (AcType == "C")
                                    mAddCost += lC[Row].CAmt;
                                else
                                    mAddDisc -= lC[Row].CAmt;
                            }
                        }

                        lCD.Add(new AddCostDisc()
                        {
                            AddCost = mAddCost,
                            AddDisc = mAddDisc
                        });
                    }

                }
                myLists.Add(lCD);

                #endregion

                if (Decimal.Parse(TxtTotalTax.Text) > 0)
                {
                    Sm.PrintReport(mIsFormPrintOutInvoice, myLists, TableName, false);
                    //Sm.PrintReport("Invoice", myLists, TableName, false);
                    //Sm.PrintReport("FakturPenjualan", myLists, TableName, false);
                }
                else
                {
                    Sm.PrintReport(string.Concat(mIsFormPrintOutInvoice + '2'), myLists, TableName, false);
                    //Sm.PrintReport("InvoiceNoPPN", myLists, TableName, false);
                    //Sm.PrintReport("FakturPenjualan", myLists, TableName, false);
                }

            }

            #endregion

            #region MSI
            else if (Sm.GetParameter("DocTitle") == "MSI")
            {
                var ListH = new List<InvoiceHdr2>();
                var ListD = new List<InvoiceDtl3>();
                var ListD2 = new List<InvoiceDtl4>();
                var ListD3 = new List<InvoiceDtl5>();

                string[] TableName = { "InvoiceHdr2", "InvoiceDtl3", "InvoiceDtl4", "InvoiceDtl5" };

                List<IList> myLists = new List<IList>();
                var cm = new MySqlCommand();

                #region MSI

                #region Header2 MSI

                var cm2 = new MySqlCommand();
                var SQL2 = new StringBuilder();
                SQL2.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
                SQL2.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
                SQL2.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyAddressFull', ");
                SQL2.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyPhone', ");
                SQL2.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle5') As 'CompanyEmail', ");
                SQL2.AppendLine("Concat(Left(A.DocDt,4),'.',substring(A.DocDt,5,2),'.', Left(A.DocNo,4))As Doc,");
                SQL2.AppendLine("A.DocNo, DATE_FORMAT(A.DocDt,'%d %M %Y') As DocDt, B.CtName, B.Address, B.Phone, A.SalesName,");
                SQL2.AppendLine("F.SAName, F.SAAddress, A.DownPayment, F.SaPhone, A.Curcode, if(C.Doctype = '1', G.VdName, '') As ExpVd,");
                SQL2.AppendLine("DATE_FORMAT(A.DueDt,'%d %M %Y') As DueDt, Round(A.TotalAmt, 2) As totalAmt, A.TotalTax,  ");
                SQL2.AppendLine("if(A.DownPayment = 0, round(A.Amt, 2), (Round(A.TotalAmt, 2)+A.TotalTax)) As Amt,  A.Remark, A.cancelInd, H.CityName As CtCityName  ");
                SQL2.AppendLine("From TblSalesInvoiceHdr A ");
                SQL2.AppendLine("Inner Join TblCustomer B On A.CtCode = B.CtCode ");
                SQL2.AppendLine("Inner Join TblSalesInvoiceDtl C On A.DocNo = C.DocNo ");
                SQL2.AppendLine("Inner Join TblDoct2Hdr D On C.DOCtDocNo = D.DocNo ");
                SQL2.AppendLine("Left Join TblPlHdr E On D.PLDOcNo = E.DocNo ");
                SQL2.AppendLine("Left Join TblDRHdr F On D.DrDOcNo = F.DocNo ");
                SQL2.AppendLine("Left Join tblVendor G On F.ExpVdCode = G.VdCode ");
                SQL2.AppendLine("Left Join TblCity H On B.cityCode = H.CityCode ");
                SQL2.AppendLine("Where A.DocNo=@DocNo ");

                using (var cn2 = new MySqlConnection(Gv.ConnectionString))
                {
                    cn2.Open();
                    cm2.Connection = cn2;
                    cm2.CommandText = SQL2.ToString();
                    Sm.CmParam<String>(ref cm2, "@DocNo", TxtDocNo.Text);
                    Sm.CmParam<String>(ref cm2, "@CompanyLogo", @Sm.CompanyLogo());
                    var dr2 = cm2.ExecuteReader();
                    var c2 = Sm.GetOrdinal(dr2, new string[]
                        {
                        //0
                         "CompanyLogo",

                         //1-5
                         "CompanyName",
                         "CompanyAddress",
                         "CompanyAddressFull",
                         "CompanyPhone",
                         "CompanyEmail",

                         //6-10
                         "Doc",
                         "DocNo",
                         "DocDt",
                         "CtName",
                         "Address",
                        

                         //11-15
                         "Phone",
                         "SalesName",
                         "SAName",
                         "SAAddress",
                         "DownPayment",

                         //16-20
                         "SaPhone",
                         "Curcode",
                         "DueDt",
                         "TotalAmt",
                         "TotalTax",

                         //21-24
                         "Amt",
                         "Remark",
                         "Cancelind",
                         "ExpVd",
                         "CtCityName"
                        });
                    if (dr2.HasRows)
                    {
                        while (dr2.Read())
                        {
                            ListH.Add(new InvoiceHdr2()
                            {
                                CompanyLogo = Sm.DrStr(dr2, c2[0]),

                                CompanyName = Sm.DrStr(dr2, c2[1]),
                                CompanyAddress = Sm.DrStr(dr2, c2[2]),
                                CompanyAddressFull = Sm.DrStr(dr2, c2[3]),
                                CompanyPhone = Sm.DrStr(dr2, c2[4]),
                                CompanyEmail = Sm.DrStr(dr2, c2[5]),

                                Doc = Sm.DrStr(dr2, c2[6]),
                                DocNo = Sm.DrStr(dr2, c2[7]),
                                DocDt = Sm.DrStr(dr2, c2[8]),
                                CtName = Sm.DrStr(dr2, c2[9]),
                                Address = Sm.DrStr(dr2, c2[10]),
                                Phone = Sm.DrStr(dr2, c2[11]),

                                SalesName = Sm.DrStr(dr2, c2[12]),
                                SAName = Sm.DrStr(dr2, c2[13]),
                                SAAddress = Sm.DrStr(dr2, c2[14]),
                                Downpayment = Sm.DrDec(dr2, c2[15]),

                                SaPhone = Sm.DrStr(dr2, c2[16]),
                                Curcode = Sm.DrStr(dr2, c2[17]),
                                DueDt = Sm.DrStr(dr2, c2[18]),
                                TotalAmt = Sm.DrDec(dr2, c2[19]),
                                TotalTax = Sm.DrDec(dr2, c2[20]),

                                //21-23
                                Amt = Sm.DrDec(dr2, c2[21]),
                                Remark = Sm.DrStr(dr2, c2[22]),
                                Cancelind = Sm.DrStr(dr2, c2[23]),
                                ExpVd = Sm.DrStr(dr2, c2[24]),
                                CtCityName = Sm.DrStr(dr2, c2[25]),

                                PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime())),
                                UserCode = Sm.GetValue("Select UserName From tblUser Where UserCode='" + Gv.CurrentUserCode + "'")
                            });
                        }
                    }
                    dr2.Close();
                }
                myLists.Add(ListH);
                #endregion

                #region Detail 3 MSI
                var cmDtl3 = new MySqlCommand();

                var SQLDtl3 = new StringBuilder();
                using (var cnDtl3 = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl3.Open();
                    cmDtl3.Connection = cnDtl3;

                    SQLDtl3.AppendLine("Select A.DocNo, A.ItCode, K.ItName,A.Qty, I.PriceUomCode, M.UPrice, A.TaxAmt, ");
                    SQLDtl3.AppendLine("A.UPriceAfterTax, ifnull(H.Discount,0) As Discount, Round((A.Qty * (ifnull(M.UPrice,0)-(ifnull(M.UPrice,0)*ifnull(H.discount, 0)/100))), 2) As SubTotal, ");
                    SQLDtl3.AppendLine("L.PTName, F.DocNo As DocNoSO, Round((A.Qty*ifnull(M.UPrice,0)*ifnull(H.discount, 0)/100),2) As DiscAmt, Round ((A.Qty*ifnull(M.UPrice,0)),2) As Amt, N.CityName As SACityName ");
                    SQLDtl3.AppendLine("From TblSalesInvoiceDtl A ");
                    SQLDtl3.AppendLine("Inner Join TblDOCt2Hdr B On A.DOCtDocNo=B.DocNo ");
                    SQLDtl3.AppendLine("Left Join TblDOCt2Dtl2 C On A.DOCtDocNo=C.DocNo And A.DOCtDNo=C.DNo ");
                    SQLDtl3.AppendLine("Left Join TblDRDtl D On B.DRDocNo=D.DocNo And C.DRDNo=D.DNo ");
                    SQLDtl3.AppendLine("Left Join TblSOHdr E On D.SODocNo=E.DocNo ");
                    SQLDtl3.AppendLine("Left Join TblSODtl F On D.SODocNo=F.DocNo And D.SODNo=F.DNo ");
                    SQLDtl3.AppendLine("Left Join TblCtQtHdr G On E.CtQtDocNo=G.DocNo ");
                    SQLDtl3.AppendLine("left Join TblCtQtDtl H On E.CtQtDocNo=H.DocNo And F.CtQtDNo=H.DNo ");
                    SQLDtl3.AppendLine("Left Join TblItemPriceHdr I On H.ItemPriceDocNo=I.DocNo ");
                    SQLDtl3.AppendLine("Left Join TblItem K On A.ItCode=K.ItCode ");
                    SQLDtl3.AppendLine("Left Join TblPaymentTerm L On G.PtCode=L.PtCode ");
                    SQLDtl3.AppendLine("Left Join tblitempricedtl M On H.ItemPriceDocno=M.DocNo And H.ItemPriceDNo=M.DNo ");
                    SQLDtl3.AppendLine("Left Join TblCity N On E.SaCityCode = N.CityCode ");
                    SQLDtl3.AppendLine("Where A.DocNo=@DocNo ");

                    cmDtl3.CommandText = SQLDtl3.ToString();

                    Sm.CmParam<String>(ref cmDtl3, "@DocNo", TxtDocNo.Text);
                    // Sm.CmParam<String>(ref cmDtl, "@CtCode", Sm.GetLue(LueCtCode));

                    var drDtl3 = cmDtl3.ExecuteReader();
                    var cDtl3 = Sm.GetOrdinal(drDtl3, new string[]
                    {
                     //0
                     "DocNo" ,

                     //1-5
                     "ItCode" ,
                     "ItName",
                     "Qty",
                     "PriceUomCode",
                     "Uprice",
                     
                     //6-10
                     "TaxAmt",
                     "UPriceAfterTax",
                     "Discount",
                     "SubTotal",
                     "PTName",

                     //11-12
                     "DocNoSO",
                     "DiscAmt",
                     "Amt",
                     "SACityName"
                    });
                    if (drDtl3.HasRows)
                    {
                        while (drDtl3.Read())
                        {
                            ListD.Add(new InvoiceDtl3()
                            {
                                DocNo = Sm.DrStr(drDtl3, cDtl3[0]),
                                ItCode = Sm.DrStr(drDtl3, cDtl3[1]),
                                ItName = Sm.DrStr(drDtl3, cDtl3[2]),
                                Qty = Sm.DrDec(drDtl3, cDtl3[3]),
                                PriceUomCode = Sm.DrStr(drDtl3, cDtl3[4]),
                                Uprice = Sm.DrDec(drDtl3, cDtl3[5]),
                                TaxAmt = Sm.DrDec(drDtl3, cDtl3[6]),
                                UPriceAfterTax = Sm.DrDec(drDtl3, cDtl3[7]),
                                Discount = Sm.DrDec(drDtl3, cDtl3[8]),
                                SubTotal = Sm.DrDec(drDtl3, cDtl3[9]),
                                PTName = Sm.DrStr(drDtl3, cDtl3[10]),
                                DocNoSO = Sm.DrStr(drDtl3, cDtl3[11]),
                                DiscAmt = Sm.DrDec(drDtl3, cDtl3[12]),
                                Amt = Sm.DrDec(drDtl3, cDtl3[13]),
                                SACityName = Sm.DrStr(drDtl3, cDtl3[14]),
                            });
                        }
                    }
                    drDtl3.Close();
                }
                myLists.Add(ListD);
                #endregion

                #region Detail 4 MSI
                var cmDtl4 = new MySqlCommand();

                var SQLDtl4 = new StringBuilder();
                using (var cnDtl4 = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl4.Open();
                    cmDtl4.Connection = cnDtl4;

                    if (TxtDownpayment == null)
                    {
                        SQLDtl4.AppendLine("Select A.Docno, A.TotalAmt, A.TotalTax, B.DAmt, B.CAmt, ");
                        SQLDtl4.AppendLine("(A.TotalAmt+ A.TotalTax+ ifnull(if(B.Damt!=0,B.DAmt,B.CAmt),0))As Total ");
                        SQLDtl4.AppendLine("from  tblsalesinvoicehdr A ");
                        SQLDtl4.AppendLine("Left  join tblsalesinvoicedtl2 B On A.DocNo=B.DocNo And Acno=(Select Parvalue From Tblparameter Where parCode = 'TOPFreightAcNo') ");
                        SQLDtl4.AppendLine("where A.DocNo=@DocNo");
                    }
                    else
                    {
                        SQLDtl4.AppendLine("Select A.Docno, A.TotalAmt, A.TotalTax, B.DAmt, B.CAmt, ");
                        SQLDtl4.AppendLine("(A.TotalAmt+ A.TotalTax+ ifnull(if(B.Damt!=0,B.DAmt,B.CAmt),0)- A.DownPayment) As Total ");
                        SQLDtl4.AppendLine("from  tblsalesinvoicehdr A ");
                        SQLDtl4.AppendLine("Left  join tblsalesinvoicedtl2 B On A.DocNo=B.DocNo And Acno=(Select Parvalue From Tblparameter Where parCode = 'TOPFreightAcNo') ");
                        SQLDtl4.AppendLine("where A.DocNo=@DocNo");
                    }
                    cmDtl4.CommandText = SQLDtl4.ToString();

                    Sm.CmParam<String>(ref cmDtl4, "@DocNo", TxtDocNo.Text);

                    var drDtl4 = cmDtl4.ExecuteReader();
                    var cDtl4 = Sm.GetOrdinal(drDtl4, new string[]
                    {
                     //0
                     "DocNo",

                     //1-5
                     "TotalAmt",
                     "TotalTax",
                     "DAmt",
                     "CAmt",
                     "Total",

                    });
                    if (drDtl4.HasRows)
                    {
                        while (drDtl4.Read())
                        {
                            ListD2.Add(new InvoiceDtl4()
                            {
                                DocNo = Sm.DrStr(drDtl4, cDtl4[0]),

                                TotalAmt = Sm.DrDec(drDtl4, cDtl4[1]),
                                TotalTax = Sm.DrDec(drDtl4, cDtl4[2]),
                                DAmt = Sm.DrDec(drDtl4, cDtl4[3]),
                                CAmt = Sm.DrDec(drDtl4, cDtl4[4]),
                                Total = Sm.DrDec(drDtl4, cDtl4[5]),

                                Terbilang = Sm.Terbilang(Sm.DrDec(drDtl4, cDtl4[1]) + Sm.DrDec(drDtl4, cDtl4[2])), // Sm.Terbilang(Sm.DrDec(drDtl4, cDtl4[5])),
                                Terbilang2 = Convert(Sm.DrDec(drDtl4, cDtl4[1]) + Sm.DrDec(drDtl4, cDtl4[2])) // Convert(Sm.DrDec(drDtl4, cDtl4[5])),

                            });
                        }
                    }
                    drDtl4.Close();
                }
                myLists.Add(ListD2);
                #endregion

                #region Detail 5 MSI
                var cmDtl5 = new MySqlCommand();

                var SQLDtl5 = new StringBuilder();
                using (var cnDtl5 = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl5.Open();
                    cmDtl5.Connection = cnDtl5;


                    SQLDtl5.AppendLine("Select X.Acno, X.AcDesc, X.Actype, X.Damt, X.Camt,");
                    SQLDtl5.AppendLine("case  ");
                    SQLDtl5.AppendLine("When (X.Actype ='C' && X.CAmt>0) Then X.CAmt  ");
                    SQLDtl5.AppendLine("When (X.Actype ='C' && X.CAmt=0) Then (X.DAmt*-1) ");
                    SQLDtl5.AppendLine("When (X.Actype ='D' && X.DAmt>0) Then X.DAmt  ");
                    SQLDtl5.AppendLine("When (X.Actype ='D' && X.DAmt=0) Then (X.CAmt*-1) ");
                    SQLDtl5.AppendLine("End As AmtAcNo, ");
                    SQLDtl5.AppendLine("case  ");
                    SQLDtl5.AppendLine("When (X.Actype ='C' && CAmt>0) Then '1'  ");
                    SQLDtl5.AppendLine("When (X.Actype ='C' && CAmt=0) Then '0' ");
                    SQLDtl5.AppendLine("When (X.Actype ='D' && DAmt>0) Then '1'  ");
                    SQLDtl5.AppendLine("When (X.Actype ='D' && DAmt=0) Then '0' ");
                    SQLDtl5.AppendLine("End As TypeInd, ifnull(X.OptDesc, '-') As OptDesc, ");
                    SQLDtl5.AppendLine("if((X.Actype = 'D' && Left(X.AcNo, 1) = '4' && DAmt>0), -1, 1) As 'UniqInd', X.Remark ");
                    SQLDtl5.AppendLine("From ");
                    SQLDtl5.AppendLine("( ");
                    SQLDtl5.AppendLine("    Select A.DoCNo, A.Acno, b.AcDesc, B.Actype, A.Damt, A.Camt, C.OptDesc, A.Remark   ");
                    SQLDtl5.AppendLine("    From TblSalesInvoiceDtl2 A ");
                    SQLDtl5.AppendLine("    Inner Join TblCOA B On A.AcNo = B.Acno ");
                    SQLDtl5.AppendLine("    Left Join TblOption C On A.OptAcDesc = C.OptCode And C.OptCat = 'AccountDescriptionOnSalesInvoice' ");
                    SQLDtl5.AppendLine("    Where A.AcInd = 'N' ");
                    SQLDtl5.AppendLine(")X Where X.DocNo=@DocNo ");
                    SQLDtl5.AppendLine("union All ");
                    SQLDtl5.AppendLine("Select ' ', ' ', ' ', 0, 0, 0, ' ', ' ', 0, ' ' ");

                    cmDtl5.CommandText = SQLDtl5.ToString();

                    Sm.CmParam<String>(ref cmDtl5, "@DocNo", TxtDocNo.Text);

                    var drDtl5 = cmDtl5.ExecuteReader();
                    var cDtl5 = Sm.GetOrdinal(drDtl5, new string[]
                    {
                      //0
                     "AcNo",

                     //1-5
                     "AcDesc",
                     "AcType",
                     "DAmt",
                     "CAmt",
                     "AmtAcNo",
                     //6-8
                     "OptDesc",
                     "Remark",
                     "UniqInd"
                    });
                    if (drDtl5.HasRows)
                    {
                        while (drDtl5.Read())
                        {
                            ListD3.Add(new InvoiceDtl5()
                            {
                                AcNo = Sm.DrStr(drDtl5, cDtl5[0]),

                                AcDesc = Sm.DrStr(drDtl5, cDtl5[1]),
                                AcType = Sm.DrStr(drDtl5, cDtl5[2]),
                                DAmt = Sm.DrDec(drDtl5, cDtl5[3]),
                                CAmt = Sm.DrDec(drDtl5, cDtl5[4]),
                                AmtAcNo = Sm.DrDec(drDtl5, cDtl5[5]),
                                OptDesc = Sm.DrStr(drDtl5, cDtl5[6]),
                                Remark = Sm.DrStr(drDtl5, cDtl5[7]),
                                UniqInd = Sm.DrDec(drDtl5, cDtl5[8]),
                            });
                        }
                    }
                    drDtl5.Close();
                }
                myLists.Add(ListD3);
                #endregion


                #endregion


                if (Decimal.Parse(TxtTotalTax.Text) > 0)
                {
                    Sm.PrintReport(mIsFormPrintOutInvoice, myLists, TableName, false);
                    //Sm.PrintReport("Invoice", myLists, TableName, false);
                    //Sm.PrintReport("FakturPenjualan", myLists, TableName, false);
                }
                else
                {
                    Sm.PrintReport(string.Concat(mIsFormPrintOutInvoice + '2'), myLists, TableName, false);
                    //Sm.PrintReport("InvoiceNoPPN", myLists, TableName, false);
                    //Sm.PrintReport("FakturPenjualan", myLists, TableName, false);
                }
            }

            #endregion

            #region MAI
            else if (Sm.GetParameter("DocTitle") == "MAI")
            {
                var MAIH = new List<InvoiceHdr>();
                var MAID = new List<InvoiceDtl>();
                var MAID2 = new List<InvoiceDtl2>();
                var MAID6 = new List<InvoiceDtl6>();
                var MAID7 = new List<InvoiceDtl7>();

                string[] TableName = { "InvoiceHdr", "InvoiceDtl", "InvoiceDtl2", "InvoiceDtl6", "InvoiceDtl7" };

                List<IList> myLists = new List<IList>();

                #region MAI

                #region Header MAI

                var cm3 = new MySqlCommand();
                var SQL3 = new StringBuilder();
                SQL3.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
                SQL3.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
                SQL3.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyAddressFull', ");
                SQL3.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyPhone', ");
                SQL3.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle5') As 'CompanyFax', ");
                SQL3.AppendLine("(Select ParValue From TblParameter Where Parcode='Shipper1') As 'Shipper1', ");
                SQL3.AppendLine("(Select ParValue From TblParameter Where Parcode='Shipper2') As 'Shipper2', ");
                SQL3.AppendLine("(Select ParValue From TblParameter Where Parcode='Shipper3') As 'Shipper3', ");
                SQL3.AppendLine("(Select ParValue From TblParameter Where Parcode='Shipper4') As 'Shipper4', ");
                SQL3.AppendLine("(Select ParValue From TblParameter Where Parcode='CompanyLocation2') As 'CompLocation2', ");
                SQL3.AppendLine("A.LocalDocNo, DATE_FORMAT(A.DocDt,'%d %M %Y') As DocDt, ");
                SQL3.AppendLine("A.CtCode, if(C.Doctype = '1',  Concat(F.SAName, '\n', F.SAAddress), '') As DesAddress, B.CtName, B.Address, DATE_FORMAT(A.DueDt,'%d %M %Y') As DueDt, ");
                SQL3.AppendLine("A.CurCode, Round(A.TotalAmt, 2) As totalAmt, A.TotalTax, A.DownPayment, if(A.DownPayment = 0, round(A.Amt, 2), (Round(A.TotalAmt, 2)+A.TotalTax)) As Amt, A.SalesName, if(C.Doctype = '1', G.VdName, '') As ExpVd, ");
                SQL3.AppendLine("if(C.Doctype = '1', D.ExpPlatNo, '') As ExpPlatNo, A.TaxInvDocument, H.bankAcNo, I.BankName As BankAcNm, A.Remark, A.cancelInd, ");
                SQL3.AppendLine("E.DocNo As PLDocNo, E.LocalDocNo As PLLocal, E3.SOLocal, E3.CtPONo, E.Account, J.PortName, E2.SPPlaceDelivery, K.SpName ");
                SQL3.AppendLine("From TblSalesInvoiceHdr A ");
                SQL3.AppendLine("Inner Join TblCustomer B On A.CtCode = B.CtCode ");
                SQL3.AppendLine("Inner Join TblSalesInvoiceDtl C On A.DocNo = C.DocNo ");
                SQL3.AppendLine("Inner Join TblDoct2Hdr D On C.DOCtDocNo = D.DocNo ");
                SQL3.AppendLine("Left Join TblPlHdr E On D.PLDOcNo = E.DocNo ");
                SQL3.AppendLine("Left Join TblSIhdr E2 On E.SIDocNo = E2.DocNo ");
                SQL3.AppendLine("Left Join (  ");
                SQL3.AppendLine("       Select  A.DocNo As SIDOCno, Group_concat(Distinct C.LocalDocNo separator ', ' )As SoLocal, ");
                SQL3.AppendLine("       Group_concat(Distinct C.CtPONo separator ', ' )As CtPONo ");
                SQL3.AppendLine("       From tblsidtl A ");
                SQL3.AppendLine("       Inner Join TblSodtl B On A.SODocNo=B.DocNo And A.SODNo=B.Dno ");
                SQL3.AppendLine("       Inner JOin TblSoHdr C On B.DocNo=C.DocNo ");
                SQL3.AppendLine("       Group By A.DocNo ");
                SQL3.AppendLine("       )E3 On E.DocNo=E3.SiDocNo ");
                SQL3.AppendLine("Left Join TblDRHdr F On D.DrDOcNo = F.DocNo ");
                SQL3.AppendLine("Left Join tblVendor G On F.ExpVdCode = G.VdCode ");
                SQL3.AppendLine("Left Join TblBankAccount H On A.BankAcCode = H.BankAcCode ");
                SQL3.AppendLine("Left Join TblBank I On H.BankCode = I.BankCode ");
                SQL3.AppendLine("Left Join TblPort J On E2.SpPortCode1 = J.PortCode");
                SQL3.AppendLine("Left Join TblSP K On E2.SpDocNo = K.DocNo ");
                SQL3.AppendLine("Where A.DocNo=@DocNo ");

                using (var cn3 = new MySqlConnection(Gv.ConnectionString))
                {
                    cn3.Open();
                    cm3.Connection = cn3;
                    cm3.CommandText = SQL3.ToString();
                    Sm.CmParam<String>(ref cm3, "@DocNo", TxtDocNo.Text);
                    Sm.CmParam<String>(ref cm3, "@CompanyLogo", @Sm.CompanyLogo());
                    var dr3 = cm3.ExecuteReader();
                    var c3 = Sm.GetOrdinal(dr3, new string[]
                        {
                        //0
                         "CompanyLogo",

                         //1-5
                         "CompanyName",
                         "CompanyAddress",
                         "CompanyAddressFull",
                         "CompanyPhone",
                         "CompanyFax",

                         //6-10
                         "LocalDocNo",
                         "DocDt",
                         "CtName",
                         "Address",
                         "DesAddress",

                         //11-15 
                         "CurCode",
                         "TotalAmt",
                         "TotalTax",
                         "DownPayment",
                         "Amt",

                         //16-20
                         "SalesName",
                         "ExpVd",
                         "ExpPlatNo",
                         "TaxInvDocument",
                         "BankAcNm",

                         //21-25
                         "BAnkAcNo",
                         "Remark",
                         "CancelInd",
                         "CompLocation2",
                         "PLDocNo",

                         //26-30
                         "PLLocal",
                         "SOLocal",
                         "CtPONo",
                         "Account",
                         "PortName",

                         //31-32
                         "SPPlaceDelivery",
                         "SpName"
                        });
                    if (dr3.HasRows)
                    {
                        while (dr3.Read())
                        {
                            MAIH.Add(new InvoiceHdr()
                            {
                                CompanyLogo = Sm.DrStr(dr3, c3[0]),

                                CompanyName = Sm.DrStr(dr3, c3[1]),
                                CompanyAddress = Sm.DrStr(dr3, c3[2]),
                                CompanyAddressFull = Sm.DrStr(dr3, c3[3]),
                                CompanyPhone = Sm.DrStr(dr3, c3[4]),
                                CompanyFax = Sm.DrStr(dr3, c3[5]),

                                LocalDocNo = Sm.DrStr(dr3, c3[6]),
                                DocDt = Sm.DrStr(dr3, c3[7]),
                                CtName = Sm.DrStr(dr3, c3[8]),
                                Address = Sm.DrStr(dr3, c3[9]),
                                DesAddress = Sm.DrStr(dr3, c3[10]),

                                CurCode = Sm.DrStr(dr3, c3[11]),
                                TotalAmt = Sm.DrDec(dr3, c3[12]),
                                TotalTax = Sm.DrDec(dr3, c3[13]),
                                DownPayment = Sm.DrDec(dr3, c3[14]),
                                Amt = Sm.DrDec(dr3, c3[15]),

                                SalesName = Sm.DrStr(dr3, c3[16]),
                                ExpVd = Sm.DrStr(dr3, c3[17]),
                                ExpPlatNo = Sm.DrStr(dr3, c3[18]),
                                TaxInvNo = Sm.DrStr(dr3, c3[19]),
                                BankAcNm = Sm.DrStr(dr3, c3[20]),
                                BankAcNo = Sm.DrStr(dr3, c3[21]),
                                Remark = Sm.DrStr(dr3, c3[22]),
                                Terbilang = Sm.Terbilang(Sm.DrDec(dr3, c3[15])),
                                Terbilang2 = Convert(Sm.DrDec(dr3, c3[15])),
                                CancelInd = Sm.DrStr(dr3, c3[23]),
                                CompLocation2 = Sm.DrStr(dr3, c3[24]),
                                PLDocNo = Sm.DrStr(dr3, c3[25]),
                                PLLocal = Sm.DrStr(dr3, c3[26]),
                                SOLocal = Sm.DrStr(dr3, c3[27]),
                                CtPONo = Sm.DrStr(dr3, c3[28]),
                                Account = Sm.DrStr(dr3, c3[29]),
                                PortName = Sm.DrStr(dr3, c3[30]),
                                SPPlaceDelivery = Sm.DrStr(dr3, c3[31]),
                                SpName = Sm.DrStr(dr3, c3[32]),

                                PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
                            });
                        }
                    }
                    dr3.Close();
                }
                myLists.Add(MAIH);
                #endregion

                #region Detail MAI
                var cmDtl6 = new MySqlCommand();

                var SQLDtl6 = new StringBuilder();
                using (var cnDtl6 = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl6.Open();
                    cmDtl6.Connection = cnDtl6;

                    SQLDtl6.AppendLine("Select  ");
                    SQLDtl6.AppendLine("A.DOCtDocNo, A.DOCtDNo, ");
                    SQLDtl6.AppendLine("A.ItCode, ");
                    SQLDtl6.AppendLine("IfNull(If(A.DocType='1', R.CtItName, R2.CtItName),If(A.DocType='1', K1.ItName, K2.ItName)) As ItName, ");
                    SQLDtl6.AppendLine("A.QtyPackagingUnit, ");
                    SQLDtl6.AppendLine("If(A.DocType='1', F1.PackagingUnitUomCode, F2.PackagingUnitUomCode) As PackagingUnitUomCode, ");
                    SQLDtl6.AppendLine("A.Qty, ");
                    SQLDtl6.AppendLine("If(A.DocType='1', I1.PriceUomCode, I2.PriceUomCode) As PriceUomCode, ");
                    SQLDtl6.AppendLine("A.UPriceBeforeTax, ");
                    SQLDtl6.AppendLine("A.TaxRate, ");
                    SQLDtl6.AppendLine("A.TaxAmt, ");
                    SQLDtl6.AppendLine("A.UPriceAfterTax, ");
                    SQLDtl6.AppendLine("If(A.DocType='1', L1.PtName, L2.PtName) As PtName, ");
                    SQLDtl6.AppendLine("Round((A.Qty * A.UPriceBeforeTax), 2) As Amt, ");
                    SQLDtl6.AppendLine("if(A.Qty =0, 0, if(A.Doctype='1', ((N1.Qty2*A.Qty)/N1.Qty), ((N2.Qty2*A.Qty)/N2.Qty))) As Qty2,  ");
                    SQLDtl6.AppendLine("T1.AddCost, T2.AddDisc,");
                    SQLDtl6.AppendLine("If(A.DocType='1', K1.HSCode,K2.HSCode) As HSCode,");
                    SQLDtl6.AppendLine("If(A.DocType='1', O.OptDesc, O1.OptDesc) As Material,");
                    SQLDtl6.AppendLine("If(A.DocType='1', P.OptDesc, P1.OptDesc) As Color,");
                    SQLDtl6.AppendLine("If(A.DocType='1', K1.ItCodeInternal, K2.ItCodeInternal) As ItCodeInternal, ");
                    SQLDtl6.AppendLine("If(A.DocType='1', Q.DTNAme, Q2.DTNAme) As DTNAme, ");
                    SQLDtl6.AppendLine("If(A.DocType='1', R.CtItCode, R2.CtItCode) As CtItCode, ");
                    SQLDtl6.AppendLine("If(A.DocType='1', L1.PtName,L2.PtName) As Top, ");
                    SQLDtl6.AppendLine("If(A.DocType='1', (D1.Qty/D1.QtyPackagingUnit), (D2.Qty/D2.QtyPackagingUnit)) As QtyPcs");
                    SQLDtl6.AppendLine("From TblSalesInvoiceDtl A ");
                    SQLDtl6.AppendLine("Inner Join TblDOCt2Hdr B On A.DOCtDocNo=B.DocNo ");

                    SQLDtl6.AppendLine("Left Join TblDOCt2Dtl2 C1 On A.DOCtDocNo=C1.DocNo And A.DOCtDNo=C1.DNo ");
                    SQLDtl6.AppendLine("Left Join TblDRDtl D1 On B.DRDocNo=D1.DocNo And C1.DRDNo=D1.DNo ");
                    SQLDtl6.AppendLine("Left Join TblSOHdr E1 On D1.SODocNo=E1.DocNo ");
                    SQLDtl6.AppendLine("Left Join TblSODtl F1 On D1.SODocNo=F1.DocNo And D1.SODNo=F1.DNo ");
                    SQLDtl6.AppendLine("Left Join TblCtQtHdr G1 On E1.CtQtDocNo=G1.DocNo  ");
                    SQLDtl6.AppendLine("Left Join TblCtQtDtl H1 On E1.CtQtDocNo=H1.DocNo And F1.CtQtDNo=H1.DNo ");
                    SQLDtl6.AppendLine("Left Join TblItemPriceHdr I1 On H1.ItemPriceDocNo=I1.DocNo  ");
                    SQLDtl6.AppendLine("Left Join TblItemPriceDtl J1 On H1.ItemPriceDocNo=J1.DocNo And H1.ItemPriceDNo=J1.DNo ");
                    SQLDtl6.AppendLine("Left Join TblItem K1 On J1.ItCode=K1.ItCode ");
                    SQLDtl6.AppendLine("Left Join TblPaymentTerm L1 On G1.PtCode=L1.PtCode ");
                    SQLDtl6.AppendLine("Left Join TblDRHdr M1 On B.DRDocNo=M1.DocNo ");
                    SQLDtl6.AppendLine("Left Join TblItemPackagingUnit N1 On F1.PackagingUnitUomCode = N1.UomCOde And A.ItCode = N1.ItCode ");
                    SQLDtl6.AppendLine("Left Join TblOption O On O.OptCat='ItemInformation5' And K1.Information5=O.OptCode");
                    SQLDtl6.AppendLine("Left Join TblOption P On P.OptCat='ItemInformation1' And K1.Information1=P.OptCode");
                    SQLDtl6.AppendLine("Left Join TblDeliveryType Q On G1.ShpMCode=Q.DTCode ");
                    SQLDtl6.AppendLine("Left Join TblCustomerItem R On K1.ItCode=R.ItCode And E1.CtCode=R.CtCode ");

                    SQLDtl6.AppendLine("Left Join TblDOCt2Dtl3 C2 On A.DOCtDocNo=C2.DocNo And A.DOCtDNo=C2.DNo ");
                    SQLDtl6.AppendLine("Left Join TblPLDtl D2 On B.PLDocNo=D2.DocNo And C2.PLDNo=D2.DNo ");
                    SQLDtl6.AppendLine("Left Join TblSOHdr E2 On D2.SODocNo=E2.DocNo ");
                    SQLDtl6.AppendLine("Left Join TblSODtl F2 On D2.SODocNo=F2.DocNo And D2.SODNo=F2.DNo ");
                    SQLDtl6.AppendLine("Left Join TblCtQtHdr G2 On E2.CtQtDocNo=G2.DocNo ");
                    SQLDtl6.AppendLine("Left Join TblCtQtDtl H2 On E2.CtQtDocNo=H2.DocNo And F2.CtQtDNo=H2.DNo ");
                    SQLDtl6.AppendLine("Left Join TblItemPriceHdr I2 On H2.ItemPriceDocNo=I2.DocNo ");
                    SQLDtl6.AppendLine("Left Join TblItemPriceDtl J2 On H2.ItemPriceDocNo=J2.DocNo And H2.ItemPriceDNo=J2.DNo ");
                    SQLDtl6.AppendLine("Left Join TblItem K2 On J2.ItCode=K2.ItCode ");
                    SQLDtl6.AppendLine("Left Join TblPaymentTerm L2 On G2.PtCode=L2.PtCode ");
                    SQLDtl6.AppendLine("Left Join TblPLHdr M2 On B.PLDocNo=M2.DocNo ");
                    SQLDtl6.AppendLine("Left Join TblItemPackagingUnit N2 On F2.PackagingUnitUomCode = N2.UomCOde And A.ItCode = N2.ItCode ");
                    SQLDtl6.AppendLine("Left Join TblOption O1 On O1.OptCat='ItemInformation5' And K2.Information5=O1.OptCode ");
                    SQLDtl6.AppendLine("Left Join TblOption P1 On P1.OptCat='ItemInformation1' And K2.Information1=P1.OptCode ");
                    SQLDtl6.AppendLine("Left Join TblDeliveryType Q2 On G2.ShpMCode=Q2.DTCode ");
                    SQLDtl6.AppendLine("Left Join TblCustomerItem R2 On K2.ItCode=R2.ItCode And E2.CtCode=R2.CtCode ");

                    SQLDtl6.AppendLine("Left Join ( ");
                    SQLDtl6.AppendLine("     Select if(X.Actype='D'&& X.Damt=0, X.Camt, X.Damt) As AddCost, 0 As AddCost1, X.DocNo ");
                    SQLDtl6.AppendLine("     From ");
                    SQLDtl6.AppendLine("     ( ");
                    SQLDtl6.AppendLine("     Select A.Damt, A.Camt, A.DocNo, A.AcNo, B.AcType, B.AcDesc  ");
                    SQLDtl6.AppendLine("     From TblsalesInvoiceDtl2 A ");
                    SQLDtl6.AppendLine("     Inner Join TblCoa B On A.AcNo = B.AcNo ");
                    SQLDtl6.AppendLine("     Where A.AcNo = '1.14.11' ");
                    SQLDtl6.AppendLine("     Or A.AcNo = '1.14.12' ");
                    SQLDtl6.AppendLine("     And DocNo=@DocNo  ");
                    SQLDtl6.AppendLine("     )X ");
                    SQLDtl6.AppendLine(") T1 On A.DocNo = T1.DocNo  ");
                    SQLDtl6.AppendLine("Left Join ( ");
                    SQLDtl6.AppendLine("         Select if(X.AcType = 'C' && X.Camt !=0, X.Camt, X.Damt) As AddDisc, X.DocNo ");
                    SQLDtl6.AppendLine("         From (  ");
                    SQLDtl6.AppendLine("             Select A.DocNo, A.AcNo, A.Damt, A.Camt, B.AcType  ");
                    SQLDtl6.AppendLine("             From TblsalesInvoiceDtl2 A  ");
                    SQLDtl6.AppendLine("             Inner Join TblCoa B On A.AcNo = B.AcNo  ");
                    SQLDtl6.AppendLine("             Where left(A.Acno, 5)='4.2.1' ");
                    SQLDtl6.AppendLine("         )X  ");
                    SQLDtl6.AppendLine("     ) T2 On A.DocNo = T2.DocNo ");
                    SQLDtl6.AppendLine("Where A.DocNo = @DocNo ");
                    SQLDtl6.AppendLine("Group By A.DOCtDocNo, A.DOCtDNo ");

                    cmDtl6.CommandText = SQLDtl6.ToString();

                    Sm.CmParam<String>(ref cmDtl6, "@DocNo", TxtDocNo.Text);

                    var drDtl6 = cmDtl6.ExecuteReader();
                    var cDtl6 = Sm.GetOrdinal(drDtl6, new string[]
                    {
                     //0
                     "ItCode" ,

                     //1-5
                     "ItName" ,
                     "QtyPackagingUnit",
                     "PackagingUnitUomCode",
                     "Qty",
                     "PriceUomCode",

                     //6-10
                     "UPriceBeforeTax",
                     "TaxRate",
                     "TaxAmt",
                     "UPriceAfterTax",
                     "PtName",

                     //11-15
                     "Amt",
                     "Qty2",
                     "AddCost",
                     "AddDisc",
                     "HSCode",

                     //16-20
                     "Material",
                     "Color",
                     "ItCodeInternal",
                     "DTNAme",
                     "CtItCode",

                     //21-22
                     "Top",
                     "QtyPcs"
                    });
                    if (drDtl6.HasRows)
                    {
                        int nomor = 0;
                        while (drDtl6.Read())
                        {
                            nomor = nomor + 1;
                            MAID.Add(new InvoiceDtl()
                            {
                                nomor = nomor,
                                ItCode = Sm.DrStr(drDtl6, cDtl6[0]),
                                ItName = Sm.DrStr(drDtl6, cDtl6[1]),
                                QtyPackagingUnit = Sm.DrDec(drDtl6, cDtl6[2]),
                                PackagingUnitUomCode = Sm.DrStr(drDtl6, cDtl6[3]),
                                Qty = Sm.DrDec(drDtl6, cDtl6[4]),
                                PriceUomCode = Sm.DrStr(drDtl6, cDtl6[5]),
                                UPriceBeforeTax = Sm.DrDec(drDtl6, cDtl6[6]),
                                TaxRate = Sm.DrDec(drDtl6, cDtl6[7]),
                                TaxAmt = Sm.DrDec(drDtl6, cDtl6[8]),
                                UPriceAfterTax = Sm.DrDec(drDtl6, cDtl6[9]),
                                PtName = Sm.DrStr(drDtl6, cDtl6[10]),
                                Amt = Sm.DrDec(drDtl6, cDtl6[11]),
                                CurCode = Sm.GetLue(LueCurCode),

                                Qty2 = Sm.DrDec(drDtl6, cDtl6[12]),
                                AddCost = Sm.DrDec(drDtl6, cDtl6[13]),
                                AddDisc = Sm.DrDec(drDtl6, cDtl6[14]),
                                HSCode = Sm.DrStr(drDtl6, cDtl6[15]),
                                Material = Sm.DrStr(drDtl6, cDtl6[16]),
                                Color = Sm.DrStr(drDtl6, cDtl6[17]),
                                ItCodeInternal = Sm.DrStr(drDtl6, cDtl6[18]),
                                DTNAme = Sm.DrStr(drDtl6, cDtl6[19]),
                                CtItCode = Sm.DrStr(drDtl6, cDtl6[20]),
                                Top = Sm.DrStr(drDtl6, cDtl6[21]),
                                QtyPcs = Sm.DrDec(drDtl6, cDtl6[22]),
                            });
                        }
                    }
                    drDtl6.Close();
                }
                myLists.Add(MAID);
                #endregion

                #region Detail 2 MAI
                var cmDtl8 = new MySqlCommand();
                var SQLDtl8 = new StringBuilder();
                using (var cnDtl8 = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl8.Open();
                    cmDtl8.Connection = cnDtl8;

                    SQLDtl8.AppendLine("Select ");
                    SQLDtl8.AppendLine("If(A.DocType='1', Group_Concat(Distinct E1.CtPONo SEPARATOR ','), Group_Concat(Distinct E2.CtPONo SEPARATOR ',')) As PONumber, ");
                    SQLDtl8.AppendLine("If(A.DocType='1', Group_concat(Distinct E1.DocNo separator ', '), Group_concat(Distinct E2.DocNo separator ', ')) As SODocNo ");
                    SQLDtl8.AppendLine("From TblSalesInvoiceDtl A ");
                    SQLDtl8.AppendLine("Inner Join TblDOCt2Hdr B On A.DOCtDocNo=B.DocNo ");
                    SQLDtl8.AppendLine("Left Join TblDOCt2Dtl2 C1 On A.DOCtDocNo=C1.DocNo And A.DOCtDNo=C1.DNo ");
                    SQLDtl8.AppendLine("Left Join TblDRDtl D1 On B.DRDocNo=D1.DocNo And C1.DRDNo=D1.DNo ");
                    SQLDtl8.AppendLine("Left Join TblSOHdr E1 On D1.SODocNo=E1.DocNo ");
                    SQLDtl8.AppendLine("Left Join TblSODtl F1 On D1.SODocNo=F1.DocNo And D1.SODNo=F1.DNo ");
                    SQLDtl8.AppendLine("Left Join TblDOCt2Dtl3 C2 On A.DOCtDocNo=C2.DocNo And A.DOCtDNo=C2.DNo ");
                    SQLDtl8.AppendLine("Left Join TblPLDtl D2 On B.PLDocNo=D2.DocNo And C2.PLDNo=D2.DNo ");
                    SQLDtl8.AppendLine("Left Join TblSOHdr E2 On D2.SODocNo=E2.DocNo ");
                    SQLDtl8.AppendLine("Left Join TblSODtl F2 On D2.SODocNo=F2.DocNo And D2.SODNo=F2.DNo ");
                    SQLDtl8.AppendLine("Where A.DocNo = @DocNo ");
                    SQLDtl8.AppendLine("Group BY A.DocNo ");

                    cmDtl8.CommandText = SQLDtl8.ToString();

                    Sm.CmParam<String>(ref cmDtl8, "@DocNo", TxtDocNo.Text);

                    var drDtl8 = cmDtl8.ExecuteReader();
                    var cDtl8 = Sm.GetOrdinal(drDtl8, new string[]
                    {
                     //0
                     "PONumber" ,

                     //1-5
                     "SODocNo" ,
                    });
                    if (drDtl8.HasRows)
                    {
                        while (drDtl8.Read())
                        {
                            MAID2.Add(new InvoiceDtl2()
                            {
                                PONumber = Sm.DrStr(drDtl8, cDtl8[0]),
                                SODocNo = Sm.DrStr(drDtl8, cDtl8[1]),
                            });
                        }
                    }
                    drDtl8.Close();
                }
                myLists.Add(MAID2);
                #endregion

                #region Detail 6 MAI
                var cmDtl7 = new MySqlCommand();

                var SQLDtl7 = new StringBuilder();
                using (var cnDtl7 = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl7.Open();
                    cmDtl7.Connection = cnDtl7;

                    SQLDtl7.AppendLine("Select ");
                    SQLDtl7.AppendLine("If(A.DocType='1', N1.CtItName, N2.CtItName) As CtItName, ");
                    SQLDtl7.AppendLine("(Select ParValue From TblParameter Where ParCode = 'IsCustomerItemNameMandatory') As IsCustomerItemNameMandatory ");
                    SQLDtl7.AppendLine("From TblSalesInvoiceDtl A ");
                    SQLDtl7.AppendLine("Inner Join TblDOCt2Hdr B On A.DOCtDocNo=B.DocNo ");

                    //DR
                    SQLDtl7.AppendLine("Left Join TblDOCt2Dtl2 C1 On A.DOCtDocNo=C1.DocNo And A.DOCtDNo=C1.DNo ");
                    SQLDtl7.AppendLine("Left Join TblDRDtl D1 On B.DRDocNo=D1.DocNo And C1.DRDNo=D1.DNo ");
                    SQLDtl7.AppendLine("Left Join TblSOHdr E1 On D1.SODocNo=E1.DocNo ");
                    SQLDtl7.AppendLine("Left Join TblSODtl F1 On D1.SODocNo=F1.DocNo And D1.SODNo=F1.DNo ");
                    SQLDtl7.AppendLine("Left Join TblCtQtHdr G1 On E1.CtQtDocNo=G1.DocNo ");
                    SQLDtl7.AppendLine("Left Join TblCtQtDtl H1 On E1.CtQtDocNo=H1.DocNo And F1.CtQtDNo=H1.DNo ");
                    SQLDtl7.AppendLine("Left Join TblItemPriceHdr I1 On H1.ItemPriceDocNo=I1.DocNo ");
                    SQLDtl7.AppendLine("Left Join TblItemPriceDtl J1 On H1.ItemPriceDocNo=J1.DocNo And H1.ItemPriceDNo=J1.DNo ");
                    SQLDtl7.AppendLine("Left Join TblItem K1 On J1.ItCode=K1.ItCode ");
                    SQLDtl7.AppendLine("Left Join TblPaymentTerm L1 On G1.PtCode=L1.PtCode ");
                    SQLDtl7.AppendLine("Left Join TblDRHdr M1 On B.DRDocNo=M1.DocNo ");

                    //PL
                    SQLDtl7.AppendLine("Left Join TblDOCt2Dtl3 C2 On A.DOCtDocNo=C2.DocNo And A.DOCtDNo=C2.DNo ");
                    SQLDtl7.AppendLine("Left Join TblPLDtl D2 On B.PLDocNo=D2.DocNo And C2.PLDNo=D2.DNo ");
                    SQLDtl7.AppendLine("Left Join TblSOHdr E2 On D2.SODocNo=E2.DocNo ");
                    SQLDtl7.AppendLine("Left Join TblSODtl F2 On D2.SODocNo=F2.DocNo And D2.SODNo=F2.DNo ");
                    SQLDtl7.AppendLine("Left Join TblCtQtHdr G2 On E2.CtQtDocNo=G2.DocNo ");
                    SQLDtl7.AppendLine("Left Join TblCtQtDtl H2 On E2.CtQtDocNo=H2.DocNo And F2.CtQtDNo=H2.DNo ");
                    SQLDtl7.AppendLine("Left Join TblItemPriceHdr I2 On H2.ItemPriceDocNo=I2.DocNo ");
                    SQLDtl7.AppendLine("Left Join TblItemPriceDtl J2 On H2.ItemPriceDocNo=J2.DocNo And H2.ItemPriceDNo=J2.DNo ");
                    SQLDtl7.AppendLine("Left Join TblItem K2 On J2.ItCode=K2.ItCode ");
                    SQLDtl7.AppendLine("Left Join TblPaymentTerm L2 On G2.PtCode=L2.PtCode ");
                    SQLDtl7.AppendLine("Left Join TblPLHdr M2 On B.PLDocNo=M2.DocNo ");

                    //Customer Item Name
                    SQLDtl7.AppendLine("Left Join TblCustomerItem N1 On G1.CtCode = N1.CtCode And K1.ItCode = N1.ItCode ");
                    SQLDtl7.AppendLine("Left Join TblCustomerItem N2 On G2.CtCode = N2.CtCode And K2.ItCode = N2.ItCode ");

                    SQLDtl7.AppendLine("Where A.DocNo=@DocNo ");

                    cmDtl7.CommandText = SQLDtl7.ToString();

                    Sm.CmParam<String>(ref cmDtl7, "@DocNo", TxtDocNo.Text);

                    var drDtl7 = cmDtl7.ExecuteReader();
                    var cDtl7 = Sm.GetOrdinal(drDtl7, new string[]
                    {
                     //0
                     "IsCustomerItemNameMandatory" ,

                     //1
                     "CtItName"

                    });
                    if (drDtl7.HasRows)
                    {
                        while (drDtl7.Read())
                        {
                            MAID6.Add(new InvoiceDtl6()
                            {
                                IsCustomerItemNameMandatory = Sm.DrStr(drDtl7, cDtl7[0]),
                                CtItName = Sm.DrStr(drDtl7, cDtl7[1]),

                            });
                        }
                    }
                    drDtl7.Close();
                }
                myLists.Add(MAID6);
                #endregion

                #region Total MAI
                var cmDtl9 = new MySqlCommand();

                var SQLDtl9 = new StringBuilder();
                using (var cnDtl9 = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl9.Open();
                    cmDtl9.Connection = cnDtl9;

                    SQLDtl9.AppendLine("Select X.DocNo, SUM(X.Amt)As Amt, Round(SUM(X.Qty2),2)As Qty2, IfNull(X.AddCost,0)As AddCost, IfNull(X.AddDisc,0)As AddDisc,");
                    SQLDtl9.AppendLine("Round(SUM(X.Amt)+IfNull(X.AddCost,0)-IfNull(X.AddDisc,0),2)As GrandTot ");
                    SQLDtl9.AppendLine("From (");
                    SQLDtl9.AppendLine("Select A.DocNo, Round((A.Qty * A.UPriceBeforeTax), 2) As Amt, ");
                    SQLDtl9.AppendLine("if(A.Qty =0, 0, if(A.Doctype='1', ((N1.Qty2*A.Qty)/N1.Qty), ((N2.Qty2*A.Qty)/N2.Qty))) As Qty2, ");
                    SQLDtl9.AppendLine("T1.AddCost, T2.AddDisc ");
                    SQLDtl9.AppendLine("From TblSalesInvoiceDtl A ");
                    SQLDtl9.AppendLine("Inner Join TblDOCt2Hdr B On A.DOCtDocNo=B.DocNo ");

                    //DR
                    SQLDtl9.AppendLine("Left Join TblDOCt2Dtl2 C1 On A.DOCtDocNo=C1.DocNo And A.DOCtDNo=C1.DNo ");
                    SQLDtl9.AppendLine("Left Join TblDRDtl D1 On B.DRDocNo=D1.DocNo And C1.DRDNo=D1.DNo ");
                    SQLDtl9.AppendLine("Left Join TblSOHdr E1 On D1.SODocNo=E1.DocNo ");
                    SQLDtl9.AppendLine("Left Join TblSODtl F1 On D1.SODocNo=F1.DocNo And D1.SODNo=F1.DNo ");
                    SQLDtl9.AppendLine("Left Join TblCtQtHdr G1 On E1.CtQtDocNo=G1.DocNo ");
                    SQLDtl9.AppendLine("Left Join TblCtQtDtl H1 On E1.CtQtDocNo=H1.DocNo And F1.CtQtDNo=H1.DNo ");
                    SQLDtl9.AppendLine("Left Join TblItemPackagingUnit N1 On F1.PackagingUnitUomCode = N1.UomCOde And A.ItCode = N1.ItCode ");

                    //PL
                    SQLDtl9.AppendLine("Left Join TblDOCt2Dtl3 C2 On A.DOCtDocNo=C2.DocNo And A.DOCtDNo=C2.DNo ");
                    SQLDtl9.AppendLine("Left Join TblPLDtl D2 On B.PLDocNo=D2.DocNo And C2.PLDNo=D2.DNo ");
                    SQLDtl9.AppendLine("Left Join TblSOHdr E2 On D2.SODocNo=E2.DocNo ");
                    SQLDtl9.AppendLine("Left Join TblSODtl F2 On D2.SODocNo=F2.DocNo And D2.SODNo=F2.DNo ");
                    SQLDtl9.AppendLine("Left Join TblCtQtHdr G2 On E2.CtQtDocNo=G2.DocNo ");
                    SQLDtl9.AppendLine("Left Join TblCtQtDtl H2 On E2.CtQtDocNo=H2.DocNo And F2.CtQtDNo=H2.DNo ");
                    SQLDtl9.AppendLine("Left Join TblItemPackagingUnit N2 On F2.PackagingUnitUomCode = N2.UomCOde And A.ItCode = N2.ItCode ");

                    //Customer Item Name
                    SQLDtl9.AppendLine("Left Join ( ");
                    SQLDtl9.AppendLine("     Select if(X.Actype='D'&& X.Damt=0, X.Camt, X.Damt) As AddCost, 0 As AddCost1, X.DocNo ");
                    SQLDtl9.AppendLine("     From ");
                    SQLDtl9.AppendLine("     ( ");
                    SQLDtl9.AppendLine("     Select A.Damt, A.Camt, A.DocNo, A.AcNo, B.AcType, B.AcDesc ");
                    SQLDtl9.AppendLine("     From TblsalesInvoiceDtl2 A ");
                    SQLDtl9.AppendLine("     Inner Join TblCoa B On A.AcNo = B.AcNo ");
                    SQLDtl9.AppendLine("     Where A.AcNo = '1.14.11' ");
                    SQLDtl9.AppendLine("     Or A.AcNo = '1.14.12' ");
                    SQLDtl9.AppendLine("     And DocNo=@DocNo ");
                    SQLDtl9.AppendLine("     )X ");
                    SQLDtl9.AppendLine(") T1 On A.DocNo = T1.DocNo ");
                    SQLDtl9.AppendLine("Left Join ( ");
                    SQLDtl9.AppendLine("         Select if(X.AcType = 'C' && X.Camt !=0, X.Camt, X.Damt) As AddDisc, X.DocNo ");
                    SQLDtl9.AppendLine("         From ( ");
                    SQLDtl9.AppendLine("             Select A.DocNo, A.AcNo, A.Damt, A.Camt, B.AcType ");
                    SQLDtl9.AppendLine("             From TblsalesInvoiceDtl2 A ");
                    SQLDtl9.AppendLine("             Inner Join TblCoa B On A.AcNo = B.AcNo ");
                    SQLDtl9.AppendLine("             Where left(A.Acno, 5)='4.2.1' ");
                    SQLDtl9.AppendLine("         )X ");
                    SQLDtl9.AppendLine("     ) T2 On A.DocNo = T2.DocNo ");
                    SQLDtl9.AppendLine(" Where A.DocNo = @DocNo ");
                    SQLDtl9.AppendLine("Group By A.DOCtDocNo, A.DOCtDNo ");
                    SQLDtl9.AppendLine(")X");
                    SQLDtl9.AppendLine("Group by X.DocNo ");

                    cmDtl9.CommandText = SQLDtl9.ToString();

                    Sm.CmParam<String>(ref cmDtl9, "@DocNo", TxtDocNo.Text);

                    var drDtl9 = cmDtl9.ExecuteReader();
                    var cDtl9 = Sm.GetOrdinal(drDtl9, new string[]
                    {
                     //0
                     "DocNo",

                     //1-5
                     "Amt",
                     "Qty2",
                     "AddCost",
                     "AddDisc",
                     "GrandTot"

                    });
                    if (drDtl9.HasRows)
                    {
                        while (drDtl9.Read())
                        {
                            MAID7.Add(new InvoiceDtl7()
                            {
                                DocNo = Sm.DrStr(drDtl9, cDtl9[0]),
                                Amt = Sm.DrDec(drDtl9, cDtl9[1]),
                                Qty2 = Sm.DrDec(drDtl9, cDtl9[2]),
                                AddCost = Sm.DrDec(drDtl9, cDtl9[3]),
                                AddDisc = Sm.DrDec(drDtl9, cDtl9[4]),
                                GrandTot = Sm.DrDec(drDtl9, cDtl9[5]),
                                Terbilang = Convert(Sm.DrDec(drDtl9, cDtl9[5])),
                            });
                        }
                    }
                    drDtl9.Close();
                }
                myLists.Add(MAID7);
                #endregion

                #endregion

                if (Decimal.Parse(TxtTotalTax.Text) > 0)
                {
                    Sm.PrintReport(mIsFormPrintOutInvoice, myLists, TableName, false);

                }
                else
                {
                    Sm.PrintReport(string.Concat(mIsFormPrintOutInvoice + '2'), myLists, TableName, false);
                }
            }

            #endregion

            #region KIM

            else if (Sm.GetParameter("Doctitle") == "KIM")
            {
                var l2 = new List<InvoiceHdrKIM>();
                var ldtl = new List<InvoiceDtlKIM>();
                var ldtl2 = new List<InvoiceDtl2KIM>();
                var l3 = new List<Employee>();
                var l4 = new List<EmployeeTaxCollector>();

                string[] TableName = { "InvoiceHdrKIM", "InvoiceDtlKIM", "InvoiceDtl2KIM", "Employee", "EmployeeTaxCollector" };

                List<IList> myLists = new List<IList>();
                var cm = new MySqlCommand();

                #region Header KIM
                var cm2 = new MySqlCommand();
                var SQL2 = new StringBuilder();
                SQL2.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
                SQL2.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
                SQL2.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyAddressFull', ");
                SQL2.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyPhone', ");
                SQL2.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle5') As 'CompanyFax', ");
                SQL2.AppendLine("(Select ParValue From TblParameter Where Parcode='CompanyLocation2') As 'CompLocation2', ");
                SQL2.AppendLine("A.DocNo, Date_Format(A.DocDt,'%d %M %Y')As DocDt, Date_Format(A.DueDt,'%d %M %Y')As DueDt, ");
                SQL2.AppendLine("A.CurCode, Floor(A.TotalTax)As TotalTax, CEIL(A.TotalAmt)As TotalAmt, CEIL(A.DownPayment)As DownPayment, B.CtName, B.Address, E.CityName, D.Remark, ");
                SQL2.AppendLine("A.Amt, F.SAName, F.SAAddress, G.CityName As SACityName, A.Remark As RemarkSI, A.ReceiptNo ");
                SQL2.AppendLine("From TblSalesInvoiceHdr A ");
                SQL2.AppendLine("Inner Join TblCustomer B On A.CtCode = B.CtCode ");
                SQL2.AppendLine("Inner join ( ");
                SQL2.AppendLine("		Select A.DocNo, B.DOCtDocNo ");
                SQL2.AppendLine("		From TblSalesInvoiceHdr A ");
                SQL2.AppendLine("		Inner join TblSalesInvoicedtl B On A.DocNo=B.DocNo ");
                SQL2.AppendLine("		group by A.DocNo ");
                SQL2.AppendLine("	) C On A.DocNo=C.DocNo ");
                SQL2.AppendLine("Inner Join TblDOCt2Hdr D On C.DOCtDocNo= D.DocNo ");
                SQL2.AppendLine("Inner Join TblCity E On B.CityCode= E.CityCode ");
                SQL2.AppendLine("Left Join TblDRHdr F On D.DrDOcNo = F.DocNo ");
                SQL2.AppendLine("Left Join TblCity G On F.SACityCode=G.CityCode ");
                SQL2.AppendLine("Where A.DocNo=@DocNo ");

                using (var cn2 = new MySqlConnection(Gv.ConnectionString))
                {
                    cn2.Open();
                    cm2.Connection = cn2;
                    cm2.CommandText = SQL2.ToString();
                    Sm.CmParam<String>(ref cm2, "@DocNo", TxtDocNo.Text);
                    Sm.CmParam<String>(ref cm2, "@CompanyLogo", @Sm.CompanyLogo());
                    var dr2 = cm2.ExecuteReader();
                    var c2 = Sm.GetOrdinal(dr2, new string[]
                        {
                        //0
                         "CompanyLogo",

                         //1-5
                         "CompanyName",
                         "CompanyAddress",
                         "CompanyAddressFull",
                         "CompanyPhone",
                         "CompanyFax",

                         //6-10
                         "DocNo",
                         "DocDt",
                         "DueDt",
                         "CurCode",
                         "TotalTax",

                         //11-15
                         "TotalAmt",
                         "DownPayment",
                         "CtName",
                         "Address",
                         "CityName",

                         //16-20
                         "Remark",
                         "Amt",
                         "SAName",
                         "SAAddress",
                         "SACityName",

                         //21-22
                         "RemarkSI",
                         "ReceiptNo"

                        });
                    if (dr2.HasRows)
                    {
                        while (dr2.Read())
                        {
                            l2.Add(new InvoiceHdrKIM()
                            {
                                CompanyLogo = Sm.DrStr(dr2, c2[0]),

                                CompanyName = Sm.DrStr(dr2, c2[1]),
                                CompanyAddress = Sm.DrStr(dr2, c2[2]),
                                CompanyAddressFull = Sm.DrStr(dr2, c2[3]),
                                CompanyPhone = Sm.DrStr(dr2, c2[4]),
                                CompanyFax = Sm.DrStr(dr2, c2[5]),

                                DocNo = Sm.DrStr(dr2, c2[6]),
                                DocDt = Sm.DrStr(dr2, c2[7]),
                                DueDt = Sm.DrStr(dr2, c2[8]),
                                CurCode = Sm.DrStr(dr2, c2[9]),
                                TotalTax = Sm.DrDec(dr2, c2[10]),

                                TotalAmt = Sm.DrDec(dr2, c2[11]),
                                DownPayment = Sm.DrDec(dr2, c2[12]),
                                CtName = Sm.DrStr(dr2, c2[13]),
                                Address = Sm.DrStr(dr2, c2[14]),
                                CityName = Sm.DrStr(dr2, c2[15]),

                                Remark = Sm.DrStr(dr2, c2[16]),
                                Amt = Sm.DrDec(dr2, c2[17]),
                                SAName = Sm.DrStr(dr2, c2[18]),
                                SAAddress = Sm.DrStr(dr2, c2[19]),
                                SACityName = Sm.DrStr(dr2, c2[20]),

                                Terbilang = Sm.Terbilang(Sm.DrDec(dr2, c2[17])),
                                RemarkSI = Sm.DrStr(dr2, c2[21]),
                                ReceiptNo = Sm.DrStr(dr2, c2[22]),

                            });
                        }
                    }
                    dr2.Close();
                }
                myLists.Add(l2);
                #endregion

                #region Detail

                var cmDtl = new MySqlCommand();
                var SQLDtl = new StringBuilder();
                using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl.Open();
                    cmDtl.Connection = cnDtl;
                    SQLDtl.AppendLine("Select A.ItCode, B.ItName, A.Qty, A.UPriceBeforeTax,CEIL(A.Qty*A.UPriceBeforeTax)As Amt, ");
                    SQLDtl.AppendLine("B.SalesuomCode As PriceUomCode, B1.Remark, FLOOR(A.Qty*A.UPriceBeforeTax*ifnull(F1.TaxRate, 0)/100)As Tax ");
                    SQLDtl.AppendLine("From TblSalesInvoiceDtl A ");
                    SQLDtl.AppendLine("Inner join tblitem B On A.ItCode=B.ItCode ");
                    SQLDtl.AppendLine("Inner join TblDOCt2Dtl C On A.DoctDocno=C.DocNo And A.DOCtDNo=C.DNo ");
                    SQLDtl.AppendLine("Inner Join TblDOCt2Hdr B1 On A.DOCtDocNo=B1.DocNo ");
                    SQLDtl.AppendLine("Left Join TblDOCt2Dtl2 C1 On A.DOCtDocNo=C1.DocNo And A.DOCtDNo=C1.DNo ");
                    SQLDtl.AppendLine("Left Join TblDRDtl D1 On B1.DRDocNo=D1.DocNo And C1.DRDNo=D1.DNo ");
                    SQLDtl.AppendLine("Left Join TblSOHdr E1 On D1.SODocNo=E1.DocNo ");
                    SQLDtl.AppendLine("Left Join TblSODtl F1 On D1.SODocNo=F1.DocNo And D1.SODNo=F1.DNo ");
                    SQLDtl.AppendLine("Where A.DocNo=@DocNo ");

                    cmDtl.CommandText = SQLDtl.ToString();
                    Sm.CmParam<String>(ref cmDtl, "@DocNo", TxtDocNo.Text);
                    var drDtl = cmDtl.ExecuteReader();
                    var cDtl = Sm.GetOrdinal(drDtl, new string[]
                {
                    //0
                    "ItCode" ,

                    //1-5
                    "ItName" ,
                    "Qty",
                    "UPriceBeforeTax",
                    "Amt",
                    "PriceUomCode",
                    //6-7
                    "Remark",
                    "Tax"

                });

                    if (drDtl.HasRows)
                    {
                        while (drDtl.Read())
                        {
                            ldtl.Add(new InvoiceDtlKIM()
                            {
                                ItCode = Sm.DrStr(drDtl, cDtl[0]),
                                ItName = Sm.DrStr(drDtl, cDtl[1]),
                                Qty = Sm.DrDec(drDtl, cDtl[2]),
                                UPriceBeforeTax = Sm.DrDec(drDtl, cDtl[3]),
                                Amt = Sm.DrDec(drDtl, cDtl[4]),
                                PriceUomCode = Sm.DrStr(drDtl, cDtl[5]),

                                Remark = Sm.DrStr(drDtl, cDtl[6]),
                                Tax = Sm.DrDec(drDtl, cDtl[7]),
                            });
                        }
                    }

                    drDtl.Close();
                }

                myLists.Add(ldtl);

                #endregion

                #region Detail

                var cmDtl2 = new MySqlCommand();
                var SQLDtl2 = new StringBuilder();
                using (var cnDtl2 = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl2.Open();
                    cmDtl2.Connection = cnDtl2;
                    SQLDtl2.AppendLine("Select A.DocNo, ifnull(sum(B.DAmt),0)As DAmt, ifnull(sum(B.CAmt),0) As CAmt, ifnull(sum(B.DAmt),0)+ ifnull(sum(B.CAmt),0) As TAmt ");
                    SQLDtl2.AppendLine("From tblsalesinvoicehdr A ");
                    SQLDtl2.AppendLine("Left join tblsalesinvoicedtl2 B On A.DocNo=B.DocNo");
                    SQLDtl2.AppendLine("Where A.DocNo=@DocNo And B.AcInd='Y' ");
                    SQLDtl2.AppendLine("Group by B.Docno ");

                    cmDtl2.CommandText = SQLDtl2.ToString();
                    Sm.CmParam<String>(ref cmDtl2, "@DocNo", TxtDocNo.Text);
                    var drDtl2 = cmDtl2.ExecuteReader();
                    var cDtl2 = Sm.GetOrdinal(drDtl2, new string[]
                {
                    //0
                    "DocNo" ,

                    //1-2
                    "DAmt" ,
                    "CAmt",
                    "TAmt",

                });

                    if (drDtl2.HasRows)
                    {
                        while (drDtl2.Read())
                        {
                            ldtl2.Add(new InvoiceDtl2KIM()
                            {
                                DocNo = Sm.DrStr(drDtl2, cDtl2[0]),
                                DAmt = Sm.DrDec(drDtl2, cDtl2[1]),
                                CAmt = Sm.DrDec(drDtl2, cDtl2[2]),
                                TAmt = Sm.DrDec(drDtl2, cDtl2[3]),
                            });
                        }
                    }

                    drDtl2.Close();
                }

                myLists.Add(ldtl2);

                #endregion

                #region Signature KIM
                var cm3 = new MySqlCommand();
                var SQL3 = new StringBuilder();

                SQL3.AppendLine("Select A.EmpCode, A.EmpName, B.PosName, Concat(IfNull(C.ParValue, ''), A.UserCode, '.JPG') As EmpPict ");
                SQL3.AppendLine("From TblEmployee A ");
                SQL3.AppendLine("Inner Join TblPosition B On A.PosCode=B.PosCode ");
                SQL3.AppendLine("Left Join TblParameter C On C.ParCode = 'ImgFileSignature' ");
                SQL3.AppendLine("Where A.EmpCode=@EmpCode ");

                using (var cn3 = new MySqlConnection(Gv.ConnectionString))
                {
                    cn3.Open();
                    cm3.Connection = cn3;
                    cm3.CommandText = SQL3.ToString();
                    Sm.CmParam<String>(ref cm3, "@EmpCode", mEmpCodeSI);
                    var dr3 = cm3.ExecuteReader();
                    var c3 = Sm.GetOrdinal(dr3, new string[]
                        {
                         //0-3
                         "EmpCode",
                         "EmpName",
                         "PosName",
                         "EmpPict"

                        });
                    if (dr3.HasRows)
                    {
                        while (dr3.Read())
                        {
                            l3.Add(new Employee()
                            {
                                EmpCode = Sm.DrStr(dr3, c3[0]),

                                EmpName = Sm.DrStr(dr3, c3[1]),
                                Position = Sm.DrStr(dr3, c3[2]),
                                EmpPict = Sm.DrStr(dr3, c3[3]),
                            });
                        }
                    }
                    dr3.Close();
                }
                myLists.Add(l3);

                #endregion

                #region Signature2 KIM
                var cm4 = new MySqlCommand();
                var SQL4 = new StringBuilder();

                SQL4.AppendLine("Select A.EmpCode, A.EmpName, B.PosName, A.Mobile ");
                SQL4.AppendLine("From TblEmployee A ");
                SQL4.AppendLine("Inner Join TblPosition B On A.PosCode=B.PosCode ");
                SQL4.AppendLine("Where A.EmpCode=@EmpCode ");

                using (var cn4 = new MySqlConnection(Gv.ConnectionString))
                {
                    cn4.Open();
                    cm4.Connection = cn4;
                    cm4.CommandText = SQL4.ToString();
                    Sm.CmParam<String>(ref cm4, "@EmpCode", mEmpCodeTaxCollector);
                    var dr4 = cm4.ExecuteReader();
                    var c4 = Sm.GetOrdinal(dr4, new string[]
                        {
                         //0-3
                         "EmpCode",
                         "EmpName",
                         "PosName",
                         "Mobile"

                        });
                    if (dr4.HasRows)
                    {
                        while (dr4.Read())
                        {
                            l4.Add(new EmployeeTaxCollector()
                            {
                                EmpCode = Sm.DrStr(dr4, c4[0]),

                                EmpName = Sm.DrStr(dr4, c4[1]),
                                Position = Sm.DrStr(dr4, c4[2]),
                                Mobile = Sm.DrStr(dr4, c4[3]),
                            });
                        }
                    }
                    dr4.Close();
                }
                myLists.Add(l4);

                #endregion

                Sm.PrintReport(mIsFormPrintOutInvoice, myLists, TableName, false);
                Sm.PrintReport(mFormPrintOutInvoiceReceipt, myLists, TableName, false);
            }

            #endregion

            #region DMK

            else if (Sm.GetParameter("DocTitle") == "DMK")
            {
                var l = new List<InvoiceHdr>();
                var ldtl = new List<InvoiceDtl>();
                var ldtl2 = new List<InvoiceDtl2>();
                var lC = new List<AddCost>();
                var lCD = new List<AddCostDisc>();
                var ldtl3 = new List<SISignature>();

                string[] TableName = { "InvoiceHdr", "InvoiceDtl", "AddCostDisc", "InvoiceDtl2", "SISignature" };

                List<IList> myLists = new List<IList>();
                var cm = new MySqlCommand();

                #region Header
                var SQL = new StringBuilder();
                SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyAddressFull', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyPhone', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle5') As 'CompanyFax', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle6') As 'NPWP', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle7') As 'Email',");
                SQL.AppendLine("A.DocNo, A.LocalDocNo, DATE_FORMAT(A.DocDt,'%d %M %Y') As DocDt, A.CtCode, ");
                SQL.AppendLine("B.CtName, if(C.Doctype = '1',  Concat(F.SAName, '\n', F.SAAddress), '') As DesAddress, ");
                SQL.AppendLine("B.Address, DATE_FORMAT(A.DueDt,'%d %M %Y') As DueDt, If(A.CurCode='IDR', 'Rp', A.CurCode)As CurCode, ");
                SQL.AppendLine("A.TotalAmt, A.TotalTax, A.DownPayment, ");
                //SQL.AppendLine("if(A.DownPayment = 0, round(A.Amt, 2), (A.TotalAmt)+A.TotalTax) As Amt, ");
                SQL.AppendLine("A.Amt,");
                SQL.AppendLine("A.TaxInvDocument, H.bankAcNo, I.BankName As BankAcNm, A.Remark, A.cancelInd, B.NPWP As CtNPWP ");
                SQL.AppendLine("From TblSalesInvoiceHdr A ");
                SQL.AppendLine("Inner Join TblCustomer B On A.CtCode = B.CtCode ");
                SQL.AppendLine("Inner Join TblSalesInvoiceDtl C On A.DocNo = C.DocNo ");
                SQL.AppendLine("Inner Join TblDoct2Hdr D On C.DOCtDocNo = D.DocNo ");
                SQL.AppendLine("Left Join TblPlHdr E On D.PLDOcNo = E.DocNo ");
                SQL.AppendLine("Left Join TblDRHdr F On D.DrDOcNo = F.DocNo ");
                SQL.AppendLine("Left Join tblVendor G On F.ExpVdCode = G.VdCode ");
                SQL.AppendLine("Left Join TblBankAccount H On A.BankAcCode = H.BankAcCode ");
                SQL.AppendLine("Left Join TblBank I On H.BankCode = I.BankCode ");
                SQL.AppendLine("Where A.DocNo=@DocNo ");

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = SQL.ToString();
                    Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                    Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[]
                        {
                        //0
                         "CompanyLogo",

                         //1-5
                         "CompanyName",
                         "CompanyAddress",
                         "CompanyAddressFull",
                         "CompanyPhone",
                         "CompanyFax",

                         //6-10
                         "NPWP",
                         "Email",
                         "DocNo",
                         "LocalDocNo",
                         "DocDt",

                         //11-15
                         "CtCode",
                         "CtName",
                         "Address",
                         "DesAddress",
                         "DueDt",

                         //16-20
                         "CurCode",
                         "TotalAmt",
                         "TotalTax",
                         "DownPayment",
                         "Amt",

                         //21-25
                         "TaxInvDocument",
                         "bankAcNo",
                         "BankAcNm",
                         "Remark",
                         "CancelInd",

                         //26
                         "CtNPWP",
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            l.Add(new InvoiceHdr()
                            {
                                CompanyLogo = Sm.DrStr(dr, c[0]),

                                CompanyName = Sm.DrStr(dr, c[1]),
                                CompanyAddress = Sm.DrStr(dr, c[2]),
                                CompanyAddressFull = Sm.DrStr(dr, c[3]),
                                CompanyPhone = Sm.DrStr(dr, c[4]),
                                CompanyFax = Sm.DrStr(dr, c[5]),

                                NPWP = Sm.DrStr(dr, c[6]),
                                Email = Sm.DrStr(dr, c[7]),
                                DocNo = Sm.DrStr(dr, c[8]),
                                LocalDocNo = Sm.DrStr(dr, c[9]),
                                DocDt = Sm.DrStr(dr, c[10]),

                                CtCode = Sm.DrStr(dr, c[11]),
                                CtName = Sm.DrStr(dr, c[12]),
                                Address = Sm.DrStr(dr, c[13]),
                                DesAddress = Sm.DrStr(dr, c[14]),
                                DueDt = Sm.DrStr(dr, c[15]),

                                CurCode = Sm.DrStr(dr, c[16]),
                                TotalAmt = Sm.DrDec(dr, c[17]),
                                TotalTax = Sm.DrDec(dr, c[18]),
                                DownPayment = Sm.DrDec(dr, c[19]),
                                Amt = Sm.DrDec(dr, c[20]),

                                TaxInvNo = Sm.DrStr(dr, c[21]),
                                BankAcNo = Sm.DrStr(dr, c[22]),
                                BankAcNm = Sm.DrStr(dr, c[23]),
                                Remark = Sm.DrStr(dr, c[24]),
                                CancelInd = Sm.DrStr(dr, c[25]),

                                CtNPWP = Sm.DrStr(dr, c[26]),
                                Terbilang = Sm.Terbilang(Sm.DrDec(dr, c[20])),
                                Terbilang2 = Convert(Sm.DrDec(dr, c[20])),

                                PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
                            });
                        }
                    }
                    dr.Close();
                }
                myLists.Add(l);
                #endregion

                #region Detail 1
                var cmDtl = new MySqlCommand();
                var SQLDtl = new StringBuilder();
                using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl.Open();
                    cmDtl.Connection = cnDtl;

                    SQLDtl.AppendLine("Select  ");
                    SQLDtl.AppendLine("A.DOCtDocNo, A.DOCtDNo, ");
                    SQLDtl.AppendLine("A.ItCode, Date_Format(B.DocDt,'%d %M %Y')As DocDt, ");
                    SQLDtl.AppendLine("If(A.DocType='1', K1.ItName, K2.ItName) As ItName, ");
                    SQLDtl.AppendLine("A.QtyPackagingUnit, ");
                    SQLDtl.AppendLine("If(A.DocType='1', F1.PackagingUnitUomCode, F2.PackagingUnitUomCode) As PackagingUnitUomCode, ");
                    SQLDtl.AppendLine("A.Qty, ");
                    SQLDtl.AppendLine("If(A.DocType='1', I1.PriceUomCode, I2.PriceUomCode) As PriceUomCode, ");
                    SQLDtl.AppendLine("A.UPriceBeforeTax, ");
                    SQLDtl.AppendLine("A.TaxRate, ");
                    SQLDtl.AppendLine("A.TaxAmt, ");
                    SQLDtl.AppendLine("A.UPriceAfterTax, ");
                    SQLDtl.AppendLine("If(A.DocType='1', L1.PtName, L2.PtName) As PtName, ");
                    SQLDtl.AppendLine("Round((A.Qty * A.UPriceBeforeTax), 2) As Amt, ");
                    SQLDtl.AppendLine("if(A.Qty =0, 0, if(A.Doctype='1', ((N1.Qty2*A.Qty)/N1.Qty), ((N2.Qty2*A.Qty)/N2.Qty))) As Qty2,  ");
                    SQLDtl.AppendLine("0 As AddCost, 0 As AddDisc ");
                    SQLDtl.AppendLine("From TblSalesInvoiceDtl A ");
                    SQLDtl.AppendLine("Inner Join TblDOCt2Hdr B On A.DOCtDocNo=B.DocNo ");

                    SQLDtl.AppendLine("Left Join TblDOCt2Dtl2 C1 On A.DOCtDocNo=C1.DocNo And A.DOCtDNo=C1.DNo ");
                    SQLDtl.AppendLine("Left Join TblDRDtl D1 On B.DRDocNo=D1.DocNo And C1.DRDNo=D1.DNo ");
                    SQLDtl.AppendLine("Left Join TblSOHdr E1 On D1.SODocNo=E1.DocNo ");
                    SQLDtl.AppendLine("Left Join TblSODtl F1 On D1.SODocNo=F1.DocNo And D1.SODNo=F1.DNo ");
                    SQLDtl.AppendLine("Left Join TblCtQtHdr G1 On E1.CtQtDocNo=G1.DocNo  ");
                    SQLDtl.AppendLine("Left Join TblCtQtDtl H1 On E1.CtQtDocNo=H1.DocNo And F1.CtQtDNo=H1.DNo ");
                    SQLDtl.AppendLine("Left Join TblItemPriceHdr I1 On H1.ItemPriceDocNo=I1.DocNo  ");
                    SQLDtl.AppendLine("Left Join TblItemPriceDtl J1 On H1.ItemPriceDocNo=J1.DocNo And H1.ItemPriceDNo=J1.DNo ");
                    SQLDtl.AppendLine("Left Join TblItem K1 On J1.ItCode=K1.ItCode ");
                    SQLDtl.AppendLine("Left Join TblPaymentTerm L1 On G1.PtCode=L1.PtCode ");
                    SQLDtl.AppendLine("Left Join TblDRHdr M1 On B.DRDocNo=M1.DocNo ");
                    SQLDtl.AppendLine("Left Join TblItemPackagingUnit N1 On F1.PackagingUnitUomCode = N1.UomCOde And A.ItCode = N1.ItCode ");

                    SQLDtl.AppendLine("Left Join TblDOCt2Dtl3 C2 On A.DOCtDocNo=C2.DocNo And A.DOCtDNo=C2.DNo ");
                    SQLDtl.AppendLine("Left Join TblPLDtl D2 On B.PLDocNo=D2.DocNo And C2.PLDNo=D2.DNo ");
                    SQLDtl.AppendLine("Left Join TblSOHdr E2 On D2.SODocNo=E2.DocNo ");
                    SQLDtl.AppendLine("Left Join TblSODtl F2 On D2.SODocNo=F2.DocNo And D2.SODNo=F2.DNo ");
                    SQLDtl.AppendLine("Left Join TblCtQtHdr G2 On E2.CtQtDocNo=G2.DocNo ");
                    SQLDtl.AppendLine("Left Join TblCtQtDtl H2 On E2.CtQtDocNo=H2.DocNo And F2.CtQtDNo=H2.DNo ");
                    SQLDtl.AppendLine("Left Join TblItemPriceHdr I2 On H2.ItemPriceDocNo=I2.DocNo ");
                    SQLDtl.AppendLine("Left Join TblItemPriceDtl J2 On H2.ItemPriceDocNo=J2.DocNo And H2.ItemPriceDNo=J2.DNo ");
                    SQLDtl.AppendLine("Left Join TblItem K2 On J2.ItCode=K2.ItCode ");
                    SQLDtl.AppendLine("Left Join TblPaymentTerm L2 On G2.PtCode=L2.PtCode ");
                    SQLDtl.AppendLine("Left Join TblPLHdr M2 On B.PLDocNo=M2.DocNo ");
                    SQLDtl.AppendLine("Left Join TblItemPackagingUnit N2 On F2.PackagingUnitUomCode = N2.UomCOde And A.ItCode = N2.ItCode ");
                    SQLDtl.AppendLine("Where A.DocNo = @DocNo ");
                    SQLDtl.AppendLine("Group By A.DOCtDocNo, A.DOCtDNo ");

                    cmDtl.CommandText = SQLDtl.ToString();

                    Sm.CmParam<String>(ref cmDtl, "@DocNo", TxtDocNo.Text);
                    //Sm.CmParam<String>(ref cmDtl, "@CtCode", Sm.GetLue(LueCtCode));

                    var drDtl = cmDtl.ExecuteReader();
                    var cDtl = Sm.GetOrdinal(drDtl, new string[]
                    {
                     //0
                     "ItCode" ,

                     //1-5
                     "DocDt",
                     "ItName" ,
                     "QtyPackagingUnit",
                     "PackagingUnitUomCode",
                     "Qty",
                     "PriceUomCode",

                     //6-10
                     "UPriceBeforeTax",
                     "TaxRate",
                     "TaxAmt",
                     "UPriceAfterTax",
                     "PtName",

                     //11-15
                     "Amt",
                     "Qty2",
                     "AddCost",
                     "AddDisc",
                    });
                    if (drDtl.HasRows)
                    {
                        while (drDtl.Read())
                        {
                            ldtl.Add(new InvoiceDtl()
                            {
                                ItCode = Sm.DrStr(drDtl, cDtl[0]),
                                DocDt = Sm.DrStr(drDtl, cDtl[1]),
                                ItName = Sm.DrStr(drDtl, cDtl[2]),
                                QtyPackagingUnit = Sm.DrDec(drDtl, cDtl[3]),
                                PackagingUnitUomCode = Sm.DrStr(drDtl, cDtl[4]),
                                Qty = Sm.DrDec(drDtl, cDtl[5]),

                                PriceUomCode = Sm.DrStr(drDtl, cDtl[6]),
                                UPriceBeforeTax = Sm.DrDec(drDtl, cDtl[7]),
                                TaxRate = Sm.DrDec(drDtl, cDtl[8]),
                                TaxAmt = Sm.DrDec(drDtl, cDtl[9]),
                                UPriceAfterTax = Sm.DrDec(drDtl, cDtl[10]),

                                PtName = Sm.DrStr(drDtl, cDtl[11]),
                                Amt = Sm.DrDec(drDtl, cDtl[12]),
                                CurCode = Sm.GetLue(LueCurCode),
                                Qty2 = Sm.DrDec(drDtl, cDtl[13]),
                                AddCost = Sm.DrDec(drDtl, cDtl[14]),
                                AddDisc = Sm.DrDec(drDtl, cDtl[15]),
                            });
                        }
                    }
                    drDtl.Close();
                }
                myLists.Add(ldtl);
                #endregion

                #region Additional Cost

                var cmC = new MySqlCommand();
                var SQLC = new StringBuilder();
                using (var cnC = new MySqlConnection(Gv.ConnectionString))
                {
                    cnC.Open();
                    cmC.Connection = cnC;

                    SQLC.AppendLine("    select DocNo, AcNo, DAmt, CAmt from tblsalesinvoicedtl2 ");
                    SQLC.AppendLine("    where acind = 'Y' ");
                    SQLC.AppendLine("    And DocNo = @DocNo ");

                    cmC.CommandText = SQLC.ToString();

                    Sm.CmParam<String>(ref cmC, "@DocNo", TxtDocNo.Text);

                    var drC = cmC.ExecuteReader();
                    var cDtC = Sm.GetOrdinal(drC, new string[]
                    {
                     //0
                     "DocNo" ,

                     //1-3
                     "AcNo", "DAmt", "CAmt"
                    });

                    decimal mAddCost = 0m;
                    decimal mAddDisc = 0m;

                    if (drC.HasRows)
                    {
                        while (drC.Read())
                        {
                            lC.Add(new AddCost()
                            {
                                DocNo = Sm.DrStr(drC, cDtC[0]),
                                AcNo = Sm.DrStr(drC, cDtC[1]),
                                DAmt = Sm.DrDec(drC, cDtC[2]),
                                CAmt = Sm.DrDec(drC, cDtC[3]),
                            });
                        }
                    }
                    drC.Close();

                    if (lC.Count > 0)
                    {
                        for (int Row = 0; Row < lC.Count; Row++)
                        {
                            string AcType = Sm.GetValue("Select AcType From TblCoa Where AcNo=@Param;", lC[Row].AcNo);
                            if (lC[Row].DAmt > 0)
                            {
                                if (AcType == "D")
                                    mAddCost += lC[Row].DAmt;
                                else
                                    mAddDisc -= lC[Row].DAmt;
                            }
                            if (lC[Row].CAmt > 0)
                            {
                                if (AcType == "C")
                                    mAddCost += lC[Row].CAmt;
                                else
                                    mAddDisc -= lC[Row].CAmt;
                            }
                        }

                        lCD.Add(new AddCostDisc()
                        {
                            AddCost = mAddCost,
                            AddDisc = mAddDisc
                        });
                    }

                }
                myLists.Add(lCD);

                #endregion

                #region Detail 2
                var cmDtl2 = new MySqlCommand();
                var SQLDtl2 = new StringBuilder();
                using (var cnDtl2 = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl2.Open();
                    cmDtl2.Connection = cnDtl2;

                    SQLDtl2.AppendLine("Select T.DocDt, T.Period, T.InvoiceAmt, T.CurCode From ");
                    SQLDtl2.AppendLine("( ");
                    SQLDtl2.AppendLine("Select Date_Format(DocDt,'%d %M %Y')As DocDt, Date_Format(Period, '%M %Y')As Period, '1' As Type, 'Sales Invoice' As TypeDesc, CurCode, ");
                    SQLDtl2.AppendLine("CtCode, CtName, DocNo, TaxInvDocument, InvoiceAmt, DownPayment, PaidAmt, Balance, DueDt, EarliestPaidDt, ");
                    SQLDtl2.AppendLine("Case When Balance<=0 Then 'F' Else ");
                    SQLDtl2.AppendLine("   Case When Balance>InvoiceAmt Then 'P' Else 'O' End  ");
                    SQLDtl2.AppendLine("End As Status  ");
                    SQLDtl2.AppendLine("From ( ");
                    SQLDtl2.AppendLine("   Select A.DocDt, Concat(Left(A.DocDt, 4),Substring(A.DocDt, 5, 2), '00') As Period, ");
                    SQLDtl2.AppendLine("   If(A.CurCode='IDR', 'Rp', A.CurCode)As CurCode, A.CtCode, C.CtName, A.DocNo, A.TaxInvDocument, ");
                    SQLDtl2.AppendLine("   (A.TotalAmt+A.TotalTax) As InvoiceAmt, ");
                    SQLDtl2.AppendLine("   A.DownPayment As DownPayment, ");
                    SQLDtl2.AppendLine("   IfNull(B.PaidAmt, 0) As PaidAmt, ");
                    SQLDtl2.AppendLine("   A.TotalAmt+A.TotalTax-A.DownPayment-IfNull(B.PaidAmt, 0) As Balance, ");
                    SQLDtl2.AppendLine("   A.DueDt, B.EarliestPaidDt ");
                    SQLDtl2.AppendLine("   From (Select * From TblSalesInvoiceHdr Where CtCode = @CtCode And Left(DocDt, 6) < @DocDt ) A ");
                    SQLDtl2.AppendLine("   Left Join ( ");
                    SQLDtl2.AppendLine("       Select DocNo, Min(DocDt) As EarliestPaidDt, Sum(Amt) As PaidAMt ");
                    SQLDtl2.AppendLine("       From (  ");
                    SQLDtl2.AppendLine("           Select T2.InvoiceDocNo As DocNo, T3.DocDt, T2.Amt ");
                    SQLDtl2.AppendLine("           From TblIncomingPaymentHdr T1 ");
                    SQLDtl2.AppendLine("           Inner Join TblIncomingPaymentDtl T2 ");
                    SQLDtl2.AppendLine("               On T1.DocNo=T2.DocNo ");
                    SQLDtl2.AppendLine("               And T2.InvoiceType='1' ");
                    SQLDtl2.AppendLine("               And T2.InvoiceDocNo In ( ");
                    SQLDtl2.AppendLine("                   Select DocNo ");
                    SQLDtl2.AppendLine("                   From TblSalesInvoiceHdr ");
                    SQLDtl2.AppendLine("                   Where CancelInd='N' ");
                    SQLDtl2.AppendLine("                   And CtCode = @CtCode ");
                    SQLDtl2.AppendLine("                   ) ");
                    SQLDtl2.AppendLine("           Inner Join TblVoucherHdr T3 On T1.VoucherRequestDocNo=T3.VoucherRequestDocNo And T3.CancelInd='N' ");
                    SQLDtl2.AppendLine("           Where T1.CancelInd='N' ");
                    SQLDtl2.AppendLine("           And IfNull(T1.Status, 'O') In ('O', 'A') ");
                    SQLDtl2.AppendLine("       ) Tbl Group By DocNo ");
                    SQLDtl2.AppendLine("   ) B On A.DocNo=B.DocNo ");
                    SQLDtl2.AppendLine("   Inner Join (Select Left(CurrentDateTime(), 8) As  CurrentDateTime) C On 1=1 ");
                    SQLDtl2.AppendLine("   Inner Join TblCustomer C On A.CtCode = C.CtCode ");
                    SQLDtl2.AppendLine("   Where A.CancelInd='N' ");
                    SQLDtl2.AppendLine(") Tbl ");
                    SQLDtl2.AppendLine(") T ");
                    SQLDtl2.AppendLine("Where T.Status = 'O' ");
                    SQLDtl2.AppendLine("Order By DocDt Desc ");
                    SQLDtl2.AppendLine("Limit 1;");

                    cmDtl2.CommandText = SQLDtl2.ToString();

                    Sm.CmParam<String>(ref cmDtl2, "@DocDt", Sm.Left(Sm.GetDte(DteDocDt), 6));
                    Sm.CmParam<String>(ref cmDtl2, "@CtCode", Sm.GetLue(LueCtCode));

                    var drDtl2 = cmDtl2.ExecuteReader();
                    var cDtl2 = Sm.GetOrdinal(drDtl2, new string[]
                    {
                     //0
                     "DocDt" ,

                     //1-3
                     "Period",
                     "InvoiceAmt",
                     "CurCode",
                    });
                    if (drDtl2.HasRows)
                    {
                        while (drDtl2.Read())
                        {
                            ldtl2.Add(new InvoiceDtl2()
                            {
                                DocDt = Sm.DrStr(drDtl2, cDtl2[0]),
                                Period = Sm.DrStr(drDtl2, cDtl2[1]),
                                InvoiceAmt = Sm.DrDec(drDtl2, cDtl2[2]),
                                CurCode = Sm.DrStr(drDtl2, cDtl2[3]),
                            });
                        }
                    }
                    drDtl2.Close();
                }
                myLists.Add(ldtl2);
                #endregion

                #region Detail Signature
                var cmDtl3 = new MySqlCommand();

                var SQLDtl3 = new StringBuilder();
                using (var cnDtl3 = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl3.Open();
                    cmDtl3.Connection = cnDtl3;
                    SQLDtl3.AppendLine("Select A.CreateBy As UserCode, B.UserName, ");
                    SQLDtl3.AppendLine("Concat(IfNull(G.ParValue, ''), B.UserCode, '.JPG') As EmpPict, E.Posname ");
                    SQLDtl3.AppendLine("From TblSalesInvoiceHdr A ");
                    SQLDtl3.AppendLine("Inner Join TblUser B On A.CreateBy = B.UserCode ");
                    SQLDtl3.AppendLine("Left Join TblParameter G On G.ParCode = 'ImgFileSignature' ");
                    SQLDtl3.AppendLine("Left Join tblemployee D On A.CreateBy=D.UserCode ");
                    SQLDtl3.AppendLine("Left Join TblPosition E On D.PosCode=E.PosCode ");
                    SQLDtl3.AppendLine("Where DocNo=@DocNo ");
                    cmDtl3.CommandText = SQLDtl3.ToString();
                    Sm.CmParam<String>(ref cmDtl3, "@DocNo", TxtDocNo.Text);
                    var drDtl3 = cmDtl3.ExecuteReader();
                    var cDtl3 = Sm.GetOrdinal(drDtl3, new string[]
                        {
                         //0-1
                         "UserCode" ,
                         "UserName",
                         "EmpPict",
                         "Posname"
                        });
                    if (drDtl3.HasRows)
                    {
                        while (drDtl3.Read())
                        {
                            ldtl3.Add(new SISignature()
                            {
                                UserCode = Sm.DrStr(drDtl3, cDtl3[0]),
                                UserName = Sm.DrStr(drDtl3, cDtl3[1]),
                                EmpPict = Sm.DrStr(drDtl3, cDtl3[2]),
                                PosName = Sm.DrStr(drDtl3, cDtl3[3]),

                            });
                        }
                    }
                    drDtl3.Close();
                }
                myLists.Add(ldtl3);
                #endregion

                Sm.PrintReport(mIsFormPrintOutInvoice, myLists, TableName, false);

            }


            #endregion

            #region SKI
            else if (Sm.GetParameter("DocTitle") == "SKI")
            {
                var SODocNo = Sm.GetGrdStr(Grd1, 0, 9);
                var l = new List<InvoiceHdrSKI>();
                var ldtl = new List<InvoiceDtlSKI>();
                var ldtl2 = new List<InvoiceDtl2SKI>();

                string[] TableName = { "InvoiceHdr", "InvoiceDtl", "InvoiceDtl2" };

                List<IList> myLists = new List<IList>();
                var cm = new MySqlCommand();

                #region Header
                var SQL = new StringBuilder();
                SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='CompanyLocation2') As 'CompLocation2', ");
                SQL.AppendLine("A.DocNo, Date_format(A.DocDt, '%d %M %Y') DocDt, B.CtName, B.Address CustomerAddress, B.Phone CustomerPhone, B.Fax CustomerFax, D.SODocNo, Date_format(D.SOContractDt, '%d %M %Y') SOContractDt, ");
                SQL.AppendLine("A.ReceiptNo, A.Remark SLIRemark, D.Remark SORemark, A.Amt InvoiceAmount, Date_format(CURDATE(), '%d %M %Y') Today, Date_format(A.DueDt, '%d %M %Y') DueDt, D.SOLocalDocNo, D.Comodity  ");
                SQL.AppendLine("FROM tblsalesinvoicehdr A ");
                SQL.AppendLine("INNER JOIN tblcustomer B ON A.CtCode = B.CtCode ");
                SQL.AppendLine("INNER JOIN tblsalesinvoicedtl C ON A.DocNo = C.DocNo ");
                SQL.AppendLine("LEFT JOIN  ");
                SQL.AppendLine("( ");
                SQL.AppendLine("	SELECT T1.DocNo DOCtDocNo, T2.DocNo DRDocNo, T3.DocNo SODocNo, T3.DteContract SOContractDt, T3.Remark, T3.LocalDocNo SOLocalDocNo, T3.Comodity ");
                SQL.AppendLine("	FROM tbldoct2hdr T1 ");
                SQL.AppendLine("	INNER JOIN tbldrdtl T2 ON T1.DRDocNo = T2.DocNo ");
                SQL.AppendLine("	INNER JOIN tblsohdr T3 ON T2.SODocNo = T3.DocNo ");
                SQL.AppendLine(") D ON C.DOCtDocNo = D.DOCtDocNo ");
                SQL.AppendLine("Where A.DocNo=@DocNo ");

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = SQL.ToString();
                    Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                    Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[]
                        {
                        //0
                         "CompanyLogo",

                         //1-5
                         "CompanyName",
                         "CompanyAddress",
                         "CompLocation2",
                         "DocNo",
                         "DocDt",

                         //6-10
                         "CtName",
                         "CustomerAddress",
                         "CustomerPhone",
                         "CustomerFax",
                         "SODocNo",

                         //11-15
                         "SOContractDt",
                         "ReceiptNo",
                         "SLIRemark",
                         "SORemark",
                         "InvoiceAmount",

                         //16-20
                         "Today",
                         "DueDt",
                         "SOLocalDocNo",
                         "Comodity"

                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            l.Add(new InvoiceHdrSKI()
                            {
                                CompanyLogo = Sm.DrStr(dr, c[0]),

                                CompanyName = Sm.DrStr(dr, c[1]),
                                CompanyAddress = Sm.DrStr(dr, c[2]),
                                CompanyAddress2 = Sm.DrStr(dr, c[3]),
                                DocNo = Sm.DrStr(dr, c[4]),
                                DocDt = Sm.DrStr(dr, c[5]),

                                CustomerName = Sm.DrStr(dr, c[6]),
                                CustomerAddress = Sm.DrStr(dr, c[7]),
                                CustomerPhone = Sm.DrStr(dr, c[8]),
                                CustomerFax = Sm.DrStr(dr, c[9]),
                                SODocNo = Sm.DrStr(dr, c[10]),

                                SODocDt = Sm.DrStr(dr, c[11]),
                                ReceiptNo = Sm.DrStr(dr, c[12]),
                                SLIRemark = Sm.DrStr(dr, c[13]),
                                SORemark = Sm.DrStr(dr, c[14]),
                                Terbilang = Sm.Terbilang(Sm.DrDec(dr, c[15])),
                                InvoiceAmount = decimal.Parse(TxtAmt.Text), //Sm.DrDec(dr, c[15]),

                                CurrentDate = Sm.DrStr(dr, c[16]),
                                DueDate = Sm.DrStr(dr, c[17]),
                                LocalDocNo = Sm.DrStr(dr, c[18]),
                                Comodity = Sm.DrStr(dr, c[19])

                            });
                        }
                    }
                    dr.Close();
                }
                myLists.Add(l);
                #endregion

                #region Detail 1
                var cmDtl = new MySqlCommand();
                var SQLDtl = new StringBuilder();
                using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl.Open();
                    cmDtl.Connection = cnDtl;

                    SQLDtl.AppendLine("SELECT A.DocNo, C.Remark SORemark, ");
                    if (mSalesInvoiceTaxCalculationFormula == "1")
                    {
                        SQLDtl.AppendLine("A.TotalAmt PriceBeforeTax, A.TotalTax TaxAmt, A.Amt PriceAfterTax, ");
                        SQLDtl.AppendLine("D.TaxName, D.TaxRate, A.TotalAmt+A.TotalTax TerbilangTotal  ");
                    }
                    else
                        SQLDtl.AppendLine("D.TaxName, B.UPriceBeforeTax PriceBeforeTax, B.TaxRate, B.TaxAmt, B.UPriceAfterTax PriceAfterTax, SUM(B.UPriceBeforeTax)+SUM(B.TaxAmt) TerbilangTotal  ");
                    SQLDtl.AppendLine("FROM tblsalesinvoicehdr A ");
                    SQLDtl.AppendLine("INNER JOIN tblsalesinvoicedtl B ON A.DocNo = B.DocNo ");
                    SQLDtl.AppendLine("LEFT JOIN  ");
                    SQLDtl.AppendLine("( ");
                    SQLDtl.AppendLine("	    SELECT T1.DocNo DOCtDocNo, T1.DNo, T3.DocNo DRDocNo, T5.DocNo SODocNo, T4.DocDt SODocDt, GROUP_CONCAT(distinct(T5.Remark)) Remark, ");
                    SQLDtl.AppendLine("		T5.DNo SODNo, T1.ItCode ");
                    SQLDtl.AppendLine("		FROM tbldoct2dtl T1 ");
                    SQLDtl.AppendLine("		inner JOIN tbldoct2hdr T2 ON T1.DocNo = T2.DocNo   ");
                    SQLDtl.AppendLine("		inner JOIN tbldrdtl T3 ON T2.DRDocNo = T3.DocNo  AND T1.DNo = T3.DNo ");
                    SQLDtl.AppendLine("		inner JOIN tblsohdr T4 ON T3.SODocNo = T4.DocNo   ");
                    SQLDtl.AppendLine("		inner JOIN TblSoDtl T5 ON T3.SODocNo = T5.DoCno AND T3.SODNo = T5.DNo ");
                    SQLDtl.AppendLine("		WHERE T4.DocNo = @SODocNo ");
                    SQLDtl.AppendLine("		GROUP BY T1.DocNo, T1.DNo ");
                    SQLDtl.AppendLine("	) C ON B.DOCtDocNo = C.DOCtDocNo AND B.DOCtDNo = C.DNo ");
                    //SQLDtl.AppendLine("	SELECT T1.DocNo DOCtDocNo, T2.DocNo DRDocNo, T3.DocNo SODocNo, T3.DocDt SODocDt, T4.Remark  ");
                    //SQLDtl.AppendLine("	FROM tbldoct2hdr T1  ");
                    //SQLDtl.AppendLine("	INNER JOIN tbldrdtl T2 ON T1.DRDocNo = T2.DocNo  ");
                    //SQLDtl.AppendLine("	INNER JOIN tblsohdr T3 ON T2.SODocNo = T3.DocNo  ");
                    //SQLDtl.AppendLine(" INNER JOIN TblSoDtl T4 ON T3.DocNo = T4.DoCno    ");
                    //SQLDtl.AppendLine(" GROUP BY T4.DocNo, T4.DNo ");
                    //SQLDtl.AppendLine(") C ON B.DOCtDocNo = C.DOCtDocNo ");
                    SQLDtl.AppendLine("LEFT JOIN  ");
                    SQLDtl.AppendLine("( ");
                    SQLDtl.AppendLine("	SELECT '1' Taxtype, T1.DocNo, T2.TaxName, T2.TaxRate ");
                    SQLDtl.AppendLine("	FROM tblsalesinvoicehdr T1 ");
                    SQLDtl.AppendLine("	INNER JOIN tbltax T2 ON T1.TaxCode1 = T2.TaxCode ");

                    SQLDtl.AppendLine("	UNION ALL  ");
                    SQLDtl.AppendLine("	SELECT '2' Taxtype, T1.DocNo, T2.TaxName, T2.TaxRate ");
                    SQLDtl.AppendLine("	FROM tblsalesinvoicehdr T1 ");
                    SQLDtl.AppendLine("	INNER JOIN tbltax T2 ON T1.TaxCode2 = T2.TaxCode ");

                    SQLDtl.AppendLine("	UNION ALL ");
                    SQLDtl.AppendLine("	SELECT '3' Taxtype, T1.DocNo, T2.TaxName, T2.TaxRate ");
                    SQLDtl.AppendLine("	FROM tblsalesinvoicehdr T1 ");
                    SQLDtl.AppendLine("	INNER JOIN tbltax T2 ON T1.TaxCode3 = T2.TaxCode ");
                    SQLDtl.AppendLine(") D ON A.DocNo = D.DocNo ");
                    SQLDtl.AppendLine("WHERE A.DocNo = @DocNo ");
                    if (mSalesInvoiceTaxCalculationFormula == "2")
                        SQLDtl.AppendLine("Group By B.DNo ");
                    cmDtl.CommandText = SQLDtl.ToString();

                    Sm.CmParam<String>(ref cmDtl, "@DocNo", TxtDocNo.Text);
                    Sm.CmParam<String>(ref cmDtl, "@SODocNo", SODocNo);


                    var drDtl = cmDtl.ExecuteReader();
                    var cDtl = Sm.GetOrdinal(drDtl, new string[]
                    {
                     //0
                     "DocNo" ,

                     //1-5
                     "SORemark",
                     "PriceBeforeTax",
                     "TaxAmt",
                     "PriceAfterTax",
                     "TaxName",

                     //6-7
                     "TaxRate",
                     "TerbilangTotal"
                    });
                    if (drDtl.HasRows)
                    {
                        int nomor = 0;
                        while (drDtl.Read())
                        {
                            nomor = nomor + 1;
                            ldtl.Add(new InvoiceDtlSKI()
                            {
                                Nomor = nomor,
                                DocNo = Sm.DrStr(drDtl, cDtl[0]),
                                SORemark = Sm.DrStr(drDtl, cDtl[1]),
                                PriceBeforeTax = Sm.DrDec(drDtl, cDtl[2]),
                                TaxAmt = Sm.DrDec(drDtl, cDtl[3]),
                                PriceAfterTax = Sm.DrDec(drDtl, cDtl[4]),
                                TaxName = Sm.DrStr(drDtl, cDtl[5]),

                                TaxRate = Sm.DrDec(drDtl, cDtl[6]),
                                TerbilangTotal = Sm.Terbilang(Sm.DrDec(drDtl, cDtl[7]))
                            });
                        }
                    }
                    drDtl.Close();
                }
                myLists.Add(ldtl);
                #endregion

                #region Detail 2
                var cmDtl2 = new MySqlCommand();
                var SQLDtl2 = new StringBuilder();
                using (var cnDtl2 = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl2.Open();
                    cmDtl2.Connection = cnDtl2;

                    SQLDtl2.AppendLine("SELECT A.DocNo, B.BankAcNo, B.BranchAcNm, C.BankName, A.SignName ");
                    SQLDtl2.AppendLine("FROM tblsalesinvoicehdr A ");
                    SQLDtl2.AppendLine("LEFT JOIN tblbankaccount B ON A.BankAcCode = B.BankAcCode ");
                    SQLDtl2.AppendLine("LEFT JOIN tblbank C ON B.BankCode = C.BankCode ");
                    SQLDtl2.AppendLine("WHERE A.DocNo = @DocNo ");


                    cmDtl2.CommandText = SQLDtl2.ToString();

                    Sm.CmParam<String>(ref cmDtl2, "@DocNo", TxtDocNo.Text);

                    var drDtl2 = cmDtl2.ExecuteReader();
                    var cDtl2 = Sm.GetOrdinal(drDtl2, new string[]
                    {
                     //0
                     "DocNo" ,

                     //1-4
                     "BankAcNo",
                     "BranchAcNm",
                     "BankName",
                     "SignName"
                    });
                    if (drDtl2.HasRows)
                    {
                        while (drDtl2.Read())
                        {
                            ldtl2.Add(new InvoiceDtl2SKI()
                            {
                                DocNo = Sm.DrStr(drDtl2, cDtl2[0]),
                                BankAcNo = Sm.DrStr(drDtl2, cDtl2[1]),
                                BranchName = Sm.DrStr(drDtl2, cDtl2[2]),
                                BankName = Sm.DrStr(drDtl2, cDtl2[3]),
                                SignName = Sm.DrStr(drDtl2, cDtl2[4])
                                //DocDt = Sm.DrStr(drDtl2, cDtl2[0]),
                                //Period = Sm.DrStr(drDtl2, cDtl2[1]),
                                //InvoiceAmt = Sm.DrDec(drDtl2, cDtl2[2]),
                                //CurCode = Sm.DrStr(drDtl2, cDtl2[3]),
                            });
                        }
                    }
                    drDtl2.Close();
                }
                myLists.Add(ldtl2);
                #endregion


                Sm.PrintReport(mIsFormPrintOutInvoice, myLists, TableName, false);
                //Sm.PrintReport(mFormPrintOutInvoiceReceipt, myLists, TableName, false);

            }
            #endregion

            #region
            else if (Sm.GetParameter("DocTitle") == "KBN")
            {
                var l = new List<InvoiceHdrKBN>();
                var ldtl = new List<InvoiceDtlKBN>();
                var ldtl2 = new List<SignatureKBN>();

                string[] TableName = { "InvoiceHdrKBN", "InvoiceDtlKBN", "SignatureKBN" };

                List<IList> myLists = new List<IList>();
                var cm = new MySqlCommand();

                #region Header
                var SQL = new StringBuilder();

                SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo,  ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName',  ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress',  ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyAddressFull',  ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyPhone',  ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle5') As 'CompanyFax',  ");
                SQL.AppendLine("E.WhsName, A.DocNo, B.CtName, B.Address, A.Remark, ");
                SQL.AppendLine("B.NPWP, DATE_FORMAT(A.DueDt, '%d %M %Y') AS Period, ");
                SQL.AppendLine("A.TotalAmt, A.TotalTax, A.Amt, F.BankAcNm, F.BankAcNo, A.CurCode ");
                SQL.AppendLine("From TblSalesInvoiceHdr A  ");
                SQL.AppendLine("Inner Join TblCustomer B On A.CtCode = B.CtCode  ");
                SQL.AppendLine("Inner Join TblSalesInvoiceDtl C On A.DocNo = C.DocNo  ");
                SQL.AppendLine("Inner Join TblDoct2Hdr D On C.DOCtDocNo = D.DocNo  ");
                SQL.AppendLine("INNER JOIN tblwarehouse E ON D.WhsCode=E.WhsCode ");
                SQL.AppendLine("LEFT JOIN tblbankaccount F ON A.BankAcCode=F.BankAcCode ");
                SQL.AppendLine("Where A.DocNo=@DocNo  ");

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = SQL.ToString();
                    Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                    Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[]
                        {
                        //0
                         "CompanyLogo",

                         //1-5
                         "CompanyName",
                         "CompanyAddress",
                         "CompanyAddressFull",
                         "CompanyPhone",
                         "CompanyFax",

                         //6-10
                         "WhsName",
                         "DocNo",
                         "CtName",
                         "Address",
                         "Remark",

                         //11-15
                         "NPWP",
                         "Period",
                         "TotalAmt",
                         "TotalTax",
                         "Amt",

                         //16-18
                         "BankAcNm",
                         "BankAcNo",
                         "CurCode"

                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            l.Add(new InvoiceHdrKBN()
                            {
                                CompanyLogo = Sm.DrStr(dr, c[0]),
                                CompanyName = Sm.DrStr(dr, c[1]),
                                CompanyAddress = Sm.DrStr(dr, c[2]),
                                CompanyAddressFull = Sm.DrStr(dr, c[3]),
                                CompanyPhone = Sm.DrStr(dr, c[4]),
                                CompanyFax = Sm.DrStr(dr, c[5]),
                                WhsName = Sm.DrStr(dr, c[6]),
                                DocNo = Sm.DrStr(dr, c[7]),
                                CtName = Sm.DrStr(dr, c[8]),
                                Address = Sm.DrStr(dr, c[9]),
                                Remark = Sm.DrStr(dr, c[10]),
                                NPWP = Sm.DrStr(dr, c[11]),
                                DueDt = Sm.DrStr(dr, c[12]),
                                TotalAmt = Sm.DrDec(dr, c[13]),
                                TotalTax = Sm.DrDec(dr, c[14]),
                                InvoiceAmt = Sm.DrDec(dr, c[15]),
                                Terbilang = Sm.Terbilang(Sm.DrDec(dr, c[15])),
                                BankAcNm = Sm.DrStr(dr, c[16]),
                                BankAcNo = Sm.DrStr(dr, c[17]),
                                CurCode = Sm.DrStr(dr, c[18]),
                            });
                        }
                    }
                    dr.Close();
                }
                myLists.Add(l);
                #endregion

                #region Detail
                var cmDtl = new MySqlCommand();
                var SQLDtl = new StringBuilder();
                using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl.Open();
                    cmDtl.Connection = cnDtl;

                    SQLDtl.AppendLine("SELECT A2.DNo, ");
                    SQLDtl.AppendLine("If(A2.DocType='1', C1.SODocNo, D2.SODocNo) As SODocNo, ");
                    SQLDtl.AppendLine("If(A2.DocType='1', C1.SODNo, D2.SODNo) As SODNo, ");
                    SQLDtl.AppendLine("If(A2.DocType='1', C1.ItName, K2.ItName) As ItName,  ");
                    SQLDtl.AppendLine("A2.Qty, If(A2.DocType='1', C1.PriceUomCode, I2.PriceUomCode) As PriceUomCode,  ");
                    SQLDtl.AppendLine("A1.CurCode, A2.UPriceBeforeTax, A2.TaxAmt, A2.UPriceAfterTax ");
                    SQLDtl.AppendLine("FROM tblsalesinvoicehdr A1  ");
                    SQLDtl.AppendLine("inner join tblsalesinvoicedtl A2 ON A1.DocNo=A2.DocNo ");
                    SQLDtl.AppendLine("Inner Join TblDOCt2Hdr B On A2.DOCtDocNo=B.DocNo  ");
                    SQLDtl.AppendLine("Left Join  ");
                    SQLDtl.AppendLine("(  ");
                    SQLDtl.AppendLine("Select X1.DocNo, X1.DNo, C1.DRDNo, D1.SODocNo, D1.SODNo, E1.CtQtDocNo, F1.CtQtDNo,  ");
                    SQLDtl.AppendLine("K1.ItName, F1.PackagingUnitUomCode, I1.PriceUomCode, L1.PtDay,  ");
                    SQLDtl.AppendLine("M1.LocalDocNo, G1.CtCode, J1.ItCode  ");
                    SQLDtl.AppendLine("From TblSalesInvoiceDtl X1  ");
                    SQLDtl.AppendLine("Inner join TblDOCt2Hdr X2 On X1.DOCtDocNo = X2.DocNo And X1.DocNo = @DocNo  ");
                    SQLDtl.AppendLine("Inner Join TblDOCt2Dtl2 C1 On X1.DOCtDocNo=C1.DocNo And X1.DOCtDNo=C1.DNo  ");
                    SQLDtl.AppendLine("Inner Join TblDRDtl D1 On X2.DRDocNo=D1.DocNo And C1.DRDNo=D1.DNo  ");
                    SQLDtl.AppendLine("Inner Join TblSOHdr E1 On D1.SODocNo=E1.DocNo  ");
                    SQLDtl.AppendLine("Inner Join TblSODtl F1 On D1.SODocNo=F1.DocNo And D1.SODNo=F1.DNo  ");
                    SQLDtl.AppendLine("Inner Join TblCtQtHdr G1 On E1.CtQtDocNo=G1.DocNo  ");
                    SQLDtl.AppendLine("Inner Join TblCtQtDtl H1 On E1.CtQtDocNo=H1.DocNo And F1.CtQtDNo=H1.DNo  ");
                    SQLDtl.AppendLine("Inner Join TblItemPriceHdr I1 On H1.ItemPriceDocNo=I1.DocNo  ");
                    SQLDtl.AppendLine("Inner Join TblItemPriceDtl J1 On H1.ItemPriceDocNo=J1.DocNo And H1.ItemPriceDNo=J1.DNo  ");
                    SQLDtl.AppendLine("Inner Join TblItem K1 On J1.ItCode=K1.ItCode  ");
                    SQLDtl.AppendLine("Inner Join TblPaymentTerm L1 On G1.PtCode=L1.PtCode  ");
                    SQLDtl.AppendLine("Inner Join TblDRHdr M1 On X2.DRDocNo=M1.DocNo  ");
                    SQLDtl.AppendLine("Union All  ");
                    SQLDtl.AppendLine("Select X1.DocNo, X1.DNo, C1.DRDNo, D1.SODocNo, D1.SODNo, E1.BOQDocNo As CtQtDocNo, '001' As CtQtDNo,  ");
                    SQLDtl.AppendLine("K1.ItName, F1.PackagingUnitUomCode, F1.PackagingUnitUomCode As PriceUomCode, L1.PtDay,  ");
                    SQLDtl.AppendLine("M1.LocalDocNo, H1.CtCode, F1.ItCode  ");
                    SQLDtl.AppendLine("From TblSalesInvoiceDtl X1  ");
                    SQLDtl.AppendLine("Inner join TblDOCt2Hdr X2 On X1.DOCtDocNo = X2.DocNo And X1.DocNo = @DocNo  ");
                    SQLDtl.AppendLine("Inner Join TblDOCt2Dtl2 C1 On X1.DOCtDocNo=C1.DocNo And X1.DOCtDNo=C1.DNo  ");
                    SQLDtl.AppendLine("Inner Join TblDRDtl D1 On X2.DRDocNo=D1.DocNo And C1.DRDNo=D1.DNo  ");
                    SQLDtl.AppendLine("Inner Join TblSOContractHdr E1 On D1.SODocNo=E1.DocNo  ");
                    SQLDtl.AppendLine("Inner Join TblSOContractDtl F1 On D1.SODocNo=F1.DocNo And D1.SODNo=F1.DNo  ");
                    SQLDtl.AppendLine("Inner Join TblBOQHDr G1 On E1.BOQDocNo = G1.Docno  ");
                    SQLDtl.AppendLine("Inner Join TblLOPHdr H1 On G1.LOPDocNo = H1.DocNo  ");
                    SQLDtl.AppendLine("Inner Join TblItem K1 On F1.ItCode=K1.ItCode  ");
                    SQLDtl.AppendLine("Inner Join TblPaymentTerm L1 On G1.PtCode=L1.PtCode  ");
                    SQLDtl.AppendLine("Inner Join TblDRHdr M1 On X2.DRDocNo=M1.DocNo  ");
                    SQLDtl.AppendLine("Union All  ");
                    SQLDtl.AppendLine("SELECT X1.DocNo, X1.DNo, C1.DRDNo, D1.SODocNo, D1.SODNo,  ");
                    SQLDtl.AppendLine("F1.DocNo AS CtQtDocNo, G1.DNo AS CtQtDNo,  ");
                    SQLDtl.AppendLine("H1.ItName, H1.SalesUomCode AS PackagingUomCode, H1.SalesUoMCode AS PriceUomCode,  ");
                    SQLDtl.AppendLine("I1.PtDay, K1.LocalDocNo, J1.CtCode, H1.ItCode  ");
                    SQLDtl.AppendLine("FROM TblSalesInvoiceDtl X1  ");
                    SQLDtl.AppendLine("INNER JOIN TblDOCt2Hdr X2 ON X1.DOCtDocNo = X2.DocNo AND X1.DocNo = @DocNo  ");
                    SQLDtl.AppendLine("INNER JOIN TblDOCt2Dtl2 C1 ON X1.DOCtDocNo = C1.DocNo AND X1.DOCtDNo = C1.DNo  ");
                    SQLDtl.AppendLine("INNER JOIN TblDRDtl D1 ON X2.DRDocNo = D1.DocNo AND C1.DRDNo = D1.DNo  ");
                    SQLDtl.AppendLine("INNER JOIN TblSalesContract E1 ON D1.SCDocNo = E1.DocNo  ");
                    SQLDtl.AppendLine("INNER JOIN TblSalesMemoHdr F1 ON D1.SODocNo = F1.DocNo  ");
                    SQLDtl.AppendLine("INNER JOIN TblSalesMemoDtl G1 ON F1.DocNo = G1.DocNo AND D1.SODNo = G1.DNo  ");
                    SQLDtl.AppendLine("INNER JOIN TblItem H1 ON G1.ItCode = H1.ItCode  ");
                    SQLDtl.AppendLine("LEFT JOIN TblPaymentTerm I1 ON E1.PtCode = I1.PtCode  ");
                    SQLDtl.AppendLine("LEFT JOIN TblCustomerItem J1 ON F1.CtCode = J1.CtCode  ");
                    SQLDtl.AppendLine("INNER JOIN TblDRHdr K1 ON X2.DRDocNo = K1.DocNo  ");
                    SQLDtl.AppendLine(") C1 On A2.DocNo=C1.DocNo And A2.DNo=C1.DNo  ");
                    SQLDtl.AppendLine("Left Join TblDOCt2Dtl3 C2 On A2.DOCtDocNo=C2.DocNo And A2.DOCtDNo=C2.DNo  ");
                    SQLDtl.AppendLine("Left Join TblPLDtl D2 On B.PLDocNo=D2.DocNo And C2.PLDNo=D2.DNo  ");
                    SQLDtl.AppendLine("Left Join TblSOHdr E2 On D2.SODocNo=E2.DocNo  ");
                    SQLDtl.AppendLine("Left Join TblSODtl F2 On D2.SODocNo=F2.DocNo And D2.SODNo=F2.DNo  ");
                    SQLDtl.AppendLine("Left Join TblCtQtHdr G2 On E2.CtQtDocNo=G2.DocNo  ");
                    SQLDtl.AppendLine("Left Join TblCtQtDtl H2 On E2.CtQtDocNo=H2.DocNo And F2.CtQtDNo=H2.DNo  ");
                    SQLDtl.AppendLine("Left Join TblItemPriceHdr I2 On H2.ItemPriceDocNo=I2.DocNo  ");
                    SQLDtl.AppendLine("Left Join TblItemPriceDtl J2 On H2.ItemPriceDocNo=J2.DocNo And H2.ItemPriceDNo=J2.DNo  ");
                    SQLDtl.AppendLine("Left Join TblItem K2 On J2.ItCode=K2.ItCode  ");
                    SQLDtl.AppendLine("WHERE A1.DocNo=@DocNo;  ");

                    cmDtl.CommandText = SQLDtl.ToString();

                    Sm.CmParam<String>(ref cmDtl, "@DocNo", TxtDocNo.Text);

                    var drDtl = cmDtl.ExecuteReader();
                    var cDtl = Sm.GetOrdinal(drDtl, new string[]
                    {
                         //0
                         "SODocNo",

                         //1-5
                         "SODNo",
                         "ItName",
                         "Qty",
                         "PriceUomCode",
                         "CurCode",

                         //6-8
                         "UPriceBeforeTax",
                         "TaxAmt",
                         "UPriceAfterTax",
                    });
                    if (drDtl.HasRows)
                    {
                        int Nomor = 0;
                        while (drDtl.Read())
                        {
                            Nomor = Nomor + 1;
                            ldtl.Add(new InvoiceDtlKBN()
                            {
                                Nomor = Nomor,
                                SODocNo = Sm.DrStr(drDtl, cDtl[0]),
                                SODNo = Sm.DrStr(drDtl, cDtl[1]),
                                ItName = Sm.DrStr(drDtl, cDtl[2]),
                                Qty = Sm.DrDec(drDtl, cDtl[3]),
                                PriceUomCode = Sm.DrStr(drDtl, cDtl[4]),
                                CurCode = Sm.DrStr(drDtl, cDtl[5]),
                                UPriceBeforeTax = Sm.DrDec(drDtl, cDtl[6]),
                                TaxAmt = Sm.DrDec(drDtl, cDtl[7]),
                                UPriceAfterTax = Sm.DrDec(drDtl, cDtl[8]),
                            });
                        }
                    }
                    drDtl.Close();
                }
                myLists.Add(ldtl);
                #endregion

                #region Signature

                bool IsDOUsedDiffDoc = false;
                bool IsFirstOrExisted = true;

                string DODocNo = string.Empty;
                string SiteName = string.Empty;

                if (Grd1.Rows.Count != 1)
                {
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    {
                        if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0)
                        {
                            if (IsFirstOrExisted)
                            {
                                DODocNo = Sm.GetGrdStr(Grd1, Row, 2);
                                IsFirstOrExisted = false;
                            }
                            else
                            {
                                if (Sm.GetGrdStr(Grd1, Row, 2) != DODocNo)
                                {
                                    IsDOUsedDiffDoc = true;
                                }
                            }
                        }
                    }

                    if (!IsDOUsedDiffDoc)
                    {
                        SiteName = Sm.GetValue("SELECT G.SiteName " +
                            "FROM tblsalesinvoicedtl A " +
                            "INNER Join TblDOCt2Hdr B On A.DOCtDocNo=B.DocNo " +
                            "INNER JOIN tblwarehouse C ON B.WhsCode=C.WhsCode " +
                            "INNER JOIN tblcostcenter D ON C.CCCode=D.CCCode " +
                            "INNER JOIN tbldepartment E ON D.DeptCode=E.DeptCode " +
                            "LEFT JOIN tbldepartmentbudgetsite F ON E.DeptCode=F.DeptCode " +
                            "LEFT JOIN tblsite G ON F.SiteCode=G.SiteCode " +
                            "WHERE A.DocNo=@Param LIMIT 1;", Sm.GetGrdStr(Grd1, 0, 2));
                    }
                }

                var cmDtl2 = new MySqlCommand();
                var SQLDtl2 = new StringBuilder();
                using (var cnDtl2 = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl2.Open();
                    cmDtl2.Connection = cnDtl2;

                    SQLDtl2.AppendLine("Select Distinct Concat(IfNull(T4.ParValue, ''), T1.UserCode, '.JPG') As Signature, ");
                    SQLDtl2.AppendLine("T1.UserCode, T1.UserName, T3.PosName, T1.DNo, Max(T1.Level) Level, T1.Title, DATE_FORMAT(Max(T1.DocDt),'%d-%M-%Y') As DocDt ");
                    SQLDtl2.AppendLine("FROM ( ");
                    SQLDtl2.AppendLine("  Select Distinct ");
                    SQLDtl2.AppendLine("  B.UserCode, C.UserName, B.ApprovalDNo As DNo, D.Level, 'Approved By' As Title, LEFT(A.DocDt, 8) As DocDt ");
                    SQLDtl2.AppendLine("  From TblSalesInvoiceHdr A ");
                    SQLDtl2.AppendLine("  Inner Join TblDocApproval B On B.DocType='SalesInvoice' And A.DocNo=B.DocNo ");
                    SQLDtl2.AppendLine("  Inner Join TblUser C On B.UserCode=C.UserCode ");
                    SQLDtl2.AppendLine("  Inner Join TblDocApprovalSetting D On B.ApprovalDNo=D.DNo And D.DocType = 'SalesInvoice' ");
                    SQLDtl2.AppendLine("  Where A.CancelInd='N' And A.DocNo=@DocNo ");
                    SQLDtl2.AppendLine("  AND D.Level='1'");
                    SQLDtl2.AppendLine(") T1 ");
                    SQLDtl2.AppendLine("Left Join TblEmployee T2 On T1.UserCode=T2.UserCode ");
                    SQLDtl2.AppendLine("Left Join TblPosition T3 On T2.PosCode=T3.PosCode ");
                    SQLDtl2.AppendLine("Left Join TblParameter T4 On T4.ParCode = 'ImgFileSignature' ");
                    SQLDtl2.AppendLine("Group By T4.ParValue, T1.UserCode, T1.UserName, T3.PosName, T1.DNo, T1.Title ");
                    SQLDtl2.AppendLine("Order By T1.Level; ");

                    cmDtl2.CommandText = SQLDtl2.ToString();
                    Sm.CmParam<String>(ref cmDtl2, "@DocNo", TxtDocNo.Text);
                    var drDtl2 = cmDtl2.ExecuteReader();
                    var cDtl2 = Sm.GetOrdinal(drDtl2, new string[]
                    {
                         //0
                         "Signature",

                         //1-3
                         "Username",
                         "PosName",
                         "DocDt",
                    });
                    if (drDtl2.HasRows)
                    {
                        while (drDtl2.Read())
                        {
                            ldtl2.Add(new SignatureKBN()
                            {
                                Signature = Sm.DrStr(drDtl2, cDtl2[0]),
                                UserName = Sm.DrStr(drDtl2, cDtl2[1]),
                                PosName = Sm.DrStr(drDtl2, cDtl2[2]),
                                DocDt = Sm.DrStr(drDtl2, cDtl2[3]),
                                SiteName = IsDOUsedDiffDoc ? string.Empty : SiteName
                            });
                        }
                    }
                    drDtl2.Close();
                }
                myLists.Add(ldtl2);

                #endregion

                Sm.PrintReport(mIsFormPrintOutInvoice, myLists, TableName, false);
            }
            #endregion

            else
            {
                var l = new List<InvoiceHdr>();
                var ldtl = new List<InvoiceDtl>();
                var ldtl2 = new List<InvoiceDtl2>();
                var lC = new List<AddCost>();
                var lCD = new List<AddCostDisc>();
                var lCD2 = new List<AddCostDiscSRN>();


                string[] TableName = { "InvoiceHdr", "InvoiceDtl", "InvoiceDtl2", "AddCostDisc", "AddCostDiscSRN" };

                List<IList> myLists = new List<IList>();
                var cm = new MySqlCommand();

                #region Header
                var SQL = new StringBuilder();
                SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyAddressFull', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyPhone', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle5') As 'CompanyFax', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle6') As 'CompanyNPWP', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='Shipper1') As 'Shipper1', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='Shipper2') As 'Shipper2', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='Shipper3') As 'Shipper3', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='Shipper4') As 'Shipper4', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='SLINotes') As 'SLINotes', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='SLINotes2') As 'SLINotes2', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='CompanyLocation2') As 'CompLocation2', ");
                SQL.AppendLine("A.DocNo, A.LocalDocNo, DATE_FORMAT(A.DocDt,'%d %M %Y') As DocDt, DATE_FORMAT(A.DocDt, '%M %Y') As DocDtMthYr, ");
                SQL.AppendLine("A.CtCode, if(C.Doctype = '1',  Concat(F.SAName, '\n', F.SAAddress), '') As DesAddress, B.CtName, B.Address, J.CityName As CityNameCT, DATE_FORMAT(A.DueDt,'%d %M %Y') As DueDt, ");
                SQL.AppendLine("A.CurCode, Round(A.TotalAmt, 2) As totalAmt, A.TotalTax, A.DownPayment, if(A.DownPayment = 0, round(A.Amt, 2), (Round(A.TotalAmt, 2)+A.TotalTax)) As Amt, A.SalesName, if(C.Doctype = '1', G.VdName, '') As ExpVd, ");
                SQL.AppendLine("if(C.Doctype = '1', D.ExpPlatNo, '') As ExpPlatNo, A.TaxInvDocument, H.bankAcNo, I.BankName, H.BankAcNm, A.Remark, A.cancelInd, A.ReceiptNo, A.CtCode, DATE_FORMAT(A.DocDt,'%d-%m-%Y') As ReceiptDt, ");
                if (Sm.GetParameter("DocTitle") == "SIER")
                    SQL.AppendLine(" 'Surabaya' As AddressTTD ");
                else
                    if (Sm.GetParameter("CompanyLocation1").Length > 0)
                    SQL.AppendLine(" '" + Sm.GetParameter("CompanyLocation1") + "' As AddressTTD ");
                else
                    SQL.AppendLine(" 'Yogyakarta' As AddressTTD ");
                SQL.AppendLine("From TblSalesInvoiceHdr A ");
                SQL.AppendLine("Inner Join TblCustomer B On A.CtCode = B.CtCode ");
                SQL.AppendLine("Inner Join TblSalesInvoiceDtl C On A.DocNo = C.DocNo ");
                SQL.AppendLine("Inner Join TblDoct2Hdr D On C.DOCtDocNo = D.DocNo ");
                SQL.AppendLine("Left Join TblPlHdr E On D.PLDOcNo = E.DocNo ");
                SQL.AppendLine("Left Join TblDRHdr F On D.DrDOcNo = F.DocNo ");
                SQL.AppendLine("Left Join tblVendor G On F.ExpVdCode = G.VdCode ");
                SQL.AppendLine("Left Join TblBankAccount H On A.BankAcCode = H.BankAcCode ");
                SQL.AppendLine("Left Join TblBank I On H.BankCode = I.BankCode ");
                SQL.AppendLine("Left Join TblCity J On B.CityCode = J.CityCode ");
                SQL.AppendLine("Where A.DocNo=@DocNo ");

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = SQL.ToString();
                    Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                    Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[]
                        {
                        //0
                         "CompanyLogo",

                         //1-5
                         "CompanyName",
                         "CompanyAddress",
                         "CompanyAddressFull",
                         "CompanyPhone",
                         "CompanyFax",

                         //6-10
                         "LocalDocNo",
                         "DocDt",
                         "CtName",
                         "Address",
                         "DesAddress",

                         //11-15 
                         "CurCode",
                         "TotalAmt",
                         "TotalTax",
                         "DownPayment",
                         "Amt",

                         //16-20
                         "SalesName",
                         "ExpVd",
                         "ExpPlatNo",
                         "TaxInvDocument",
                         "BankAcNm",

                         //21-25
                         "BAnkAcNo",
                         "Remark",
                         "CancelInd",
                         "CompLocation2",
                         "CompanyNPWP",

                         //26-30
                         "DocNo",
                         "CityNameCT",
                         "BankName",
                         "ReceiptNo",
                         "CtCode",
                         
                         //32
                         "ReceiptDt",
                         "AddressTTD",
                         "SLINotes",
                         "SLINotes2",
                         "DocDtMthYr"

                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            l.Add(new InvoiceHdr()
                            {
                                CompanyLogo = Sm.DrStr(dr, c[0]),

                                CompanyName = Sm.DrStr(dr, c[1]),
                                CompanyAddress = Sm.DrStr(dr, c[2]),
                                CompanyAddressFull = Sm.DrStr(dr, c[3]),
                                CompanyPhone = Sm.DrStr(dr, c[4]),
                                CompanyFax = Sm.DrStr(dr, c[5]),

                                LocalDocNo = Sm.DrStr(dr, c[6]),
                                DocDt = Sm.DrStr(dr, c[7]),
                                CtName = Sm.DrStr(dr, c[8]),
                                Address = Sm.DrStr(dr, c[9]),
                                DesAddress = Sm.DrStr(dr, c[10]),

                                CurCode = Sm.DrStr(dr, c[11]),
                                TotalAmt = Sm.DrDec(dr, c[12]),
                                TotalTax = Sm.DrDec(dr, c[13]),
                                DownPayment = Sm.DrDec(dr, c[14]),
                                Amt = Sm.DrDec(dr, c[15]),

                                SalesName = Sm.DrStr(dr, c[16]),
                                ExpVd = Sm.DrStr(dr, c[17]),
                                ExpPlatNo = Sm.DrStr(dr, c[18]),
                                TaxInvNo = Sm.DrStr(dr, c[19]),
                                BankAcNm = Sm.DrStr(dr, c[20]),

                                BankAcNo = Sm.DrStr(dr, c[21]),
                                Remark = Sm.DrStr(dr, c[22]),
                                Terbilang = Sm.Terbilang(Sm.DrDec(dr, c[15])),
                                Terbilang2 = Convert(Sm.DrDec(dr, c[15])),
                                CancelInd = Sm.DrStr(dr, c[23]),
                                CompLocation2 = Sm.DrStr(dr, c[24]),
                                CompanyNPWP = Sm.DrStr(dr, c[25]),
                                DocNo = Sm.DrStr(dr, c[26]),
                                CityNameCT = Sm.DrStr(dr, c[27]),
                                BankName = Sm.DrStr(dr, c[28]),
                                ReceiptNo = Sm.DrStr(dr, c[29]),
                                CtCode = Sm.DrStr(dr, c[30]),
                                ReceiptDt = Sm.DrStr(dr, c[31]),
                                PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime())),
                                AddressTTD = Sm.DrStr(dr, c[32]),
                                SLINotes = Sm.DrStr(dr, c[33]),
                                SLINotes2 = Sm.DrStr(dr, c[34]),
                                DocDtMthYr = Sm.DrStr(dr, c[35]),
                                TaxCode1 = LueTaxCode1.Text,
                                TaxCode2 = LueTaxCode2.Text,
                                TaxCode3 = LueTaxCode3.Text,
                                TaxAmt1 = decimal.Parse(TxtTaxAmt1.Text),
                                TaxAmt2 = decimal.Parse(TxtTaxAmt2.Text),
                                TaxAmt3 = decimal.Parse(TxtTaxAmt3.Text),
                                InvoiceAmt = decimal.Parse(TxtAmt.Text),

                            });
                        }
                    }
                    dr.Close();
                }
                myLists.Add(l);
                #endregion

                #region Detail 1
                var cmDtl = new MySqlCommand();
                var SQLDtl = new StringBuilder();
                using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl.Open();
                    cmDtl.Connection = cnDtl;

                    SQLDtl.AppendLine("Select  ");
                    SQLDtl.AppendLine("A.DOCtDocNo, A.DOCtDNo, ");
                    SQLDtl.AppendLine("A.ItCode, ");
                    if (Sm.GetParameter("DocTitle") == "SIER")
                        SQLDtl.AppendLine(" IfNull(X1.ItGrpName, O2.ItGrpName) As ItName,  ");
                    else
                        SQLDtl.AppendLine("If(A.DocType='1', X1.ItName, K2.ItName) As ItName,  ");
                    SQLDtl.AppendLine("If(A.DocType='1', X1.ItCodeInternal, K2.ItCodeInternal) As ItCodeInternal,  ");
                    SQLDtl.AppendLine("A.QtyPackagingUnit, ");
                    SQLDtl.AppendLine("If(A.DocType='1', X1.PackagingUnitUomCode, F2.PackagingUnitUomCode) As PackagingUnitUomCode, ");
                    SQLDtl.AppendLine("A.Qty, ");
                    SQLDtl.AppendLine("If(A.DocType='1', X1.PriceUomCode, I2.PriceUomCode) As PriceUomCode, ");
                    SQLDtl.AppendLine("A.UPriceBeforeTax, ");
                    SQLDtl.AppendLine("A.TaxRate, ");
                    SQLDtl.AppendLine("A.TaxAmt, ");
                    SQLDtl.AppendLine(" Case monthname(Z.DocDt)  ");
                    SQLDtl.AppendLine(" When 'January' Then 'Januari' When 'February' Then 'Februari' When 'March' Then 'Maret'  ");
                    SQLDtl.AppendLine(" When 'April' Then 'April' When 'May' Then 'Mei' When 'June' Then 'Juni' When 'July' Then 'Juli' When 'August' Then 'Agustus'   ");
                    SQLDtl.AppendLine(" When 'September' Then 'September' When 'October' Then 'Oktober' When 'November' Then 'Nopember' When 'December' Then 'Desember' End As Month ");
                    SQLDtl.AppendLine(" ,DATE_FORMAT(Z.DocDt, '%Y') Year, ");
                    SQLDtl.AppendLine("A.UPriceAfterTax, ");
                    SQLDtl.AppendLine("If(A.DocType='1', X1.PtName, L2.PtName) As PtName, ");
                    SQLDtl.AppendLine("Round((A.Qty * A.UPriceBeforeTax), 2) As Amt, ");
                    SQLDtl.AppendLine("if(A.Qty =0, 0, if(A.Doctype='1', ((X1.Qty2*A.Qty)/X1.Qty), ((N2.Qty2*A.Qty)/N2.Qty))) As Qty2,  ");
                    SQLDtl.AppendLine("0 As AddCost, 0 As AddDisc, C3.Remark, B.Remark As DOCtRemark  ");
                    SQLDtl.AppendLine("From TblSalesInvoiceDtl A ");
                    SQLDtl.AppendLine("INNER JOIN tblsalesinvoicehdr Z ON A.DocNo=Z.DocNo ");
                    SQLDtl.AppendLine("Inner Join TblDOCt2Hdr B On A.DOCtDocNo=B.DocNo ");

                    SQLDtl.AppendLine("Left Join ");
                    SQLDtl.AppendLine("( ");
                    SQLDtl.AppendLine("    Select A.DocNo, A.DNo, ");
                    SQLDtl.AppendLine("    O1.ItGrpName, K1.ItName, K1.ItCodeInternal, F1.PackagingUnitUomCode, I1.PriceUomCode, L1.PtName, N1.Qty2, N1.Qty ");
                    SQLDtl.AppendLine("    From TblSalesInvoiceDtl A ");
                    SQLDtl.AppendLine("    Inner Join TblSalesInvoiceHdr Z ON A.DocNo=Z.DocNo AND A.DocNo = @DocNo ");
                    SQLDtl.AppendLine("    Inner Join TblDOCt2Hdr B On A.DOCtDocNo=B.DocNo ");
                    SQLDtl.AppendLine("    Inner Join TblDOCt2Dtl2 C1 On A.DOCtDocNo=C1.DocNo And A.DOCtDNo=C1.DNo ");
                    SQLDtl.AppendLine("    Inner Join TblDRDtl D1 On B.DRDocNo=D1.DocNo And C1.DRDNo=D1.DNo ");
                    SQLDtl.AppendLine("    Inner Join TblSOHdr E1 On D1.SODocNo=E1.DocNo ");
                    SQLDtl.AppendLine("    Inner Join TblSODtl F1 On D1.SODocNo=F1.DocNo And D1.SODNo=F1.DNo ");
                    SQLDtl.AppendLine("    Inner Join TblCtQtHdr G1 On E1.CtQtDocNo=G1.DocNo ");
                    SQLDtl.AppendLine("    Inner Join TblCtQtDtl H1 On E1.CtQtDocNo=H1.DocNo And F1.CtQtDNo=H1.DNo ");
                    SQLDtl.AppendLine("    Inner Join TblItemPriceHdr I1 On H1.ItemPriceDocNo=I1.DocNo ");
                    SQLDtl.AppendLine("    Inner Join TblItemPriceDtl J1 On H1.ItemPriceDocNo=J1.DocNo And H1.ItemPriceDNo=J1.DNo ");
                    SQLDtl.AppendLine("    Inner Join TblItem K1 On J1.ItCode=K1.ItCode ");
                    SQLDtl.AppendLine("    Left Join TblPaymentTerm L1 On G1.PtCode=L1.PtCode ");
                    SQLDtl.AppendLine("    Left Join TblDRHdr M1 On B.DRDocNo=M1.DocNo ");
                    SQLDtl.AppendLine("    Left Join TblItemPackagingUnit N1 On F1.PackagingUnitUomCode = N1.UomCOde And A.ItCode = N1.ItCode ");
                    SQLDtl.AppendLine("    Left Join TblItemGroup O1 On O1.ItGrpCode=K1.ItGrpCode ");
                    SQLDtl.AppendLine("    Union All ");
                    SQLDtl.AppendLine("    Select A.DocNo, A.DNo, ");
                    SQLDtl.AppendLine("    K1.ItGrpName, H1.ItName, H1.ItCodeInternal, H1.SalesUomCode PackagingUnitUomCode, H1.SalesUomCode PriceUomCode, I1.PtName, L1.Qty2, L1.Qty ");
                    SQLDtl.AppendLine("    From TblSalesInvoiceDtl A ");
                    SQLDtl.AppendLine("    Inner Join TblSalesInvoiceHdr Z ON A.DocNo=Z.DocNo AND A.DocNo = @DocNo ");
                    SQLDtl.AppendLine("    Inner Join TblDOCt2Hdr B On A.DOCtDocNo=B.DocNo ");
                    SQLDtl.AppendLine("    Inner Join TblDOCt2Dtl2 C1 On A.DOCtDocNo=C1.DocNo And A.DOCtDNo=C1.DNo ");
                    SQLDtl.AppendLine("    Inner Join TblDRDtl D1 On B.DRDocNo=D1.DocNo And C1.DRDNo=D1.DNo ");
                    SQLDtl.AppendLine("    Inner Join TblSalesContract E1 ON D1.SCDocNo = E1.DocNo ");
                    SQLDtl.AppendLine("    Inner Join TblSalesMemoHdr F1 ON D1.SODocNo = F1.DocNo ");
                    SQLDtl.AppendLine("    Inner Join TblSalesMemoDtl G1 ON F1.DocNo = G1.DocNo AND D1.SODNo = G1.DNo ");
                    SQLDtl.AppendLine("    Inner Join TblItem H1 ON G1.ItCode = H1.ItCode ");
                    SQLDtl.AppendLine("    left Join TblPaymentTerm I1 ON E1.PtCode = I1.PtCode ");
                    SQLDtl.AppendLine("    Left Join TblCustomerItem J1 ON F1.CtCode = J1.CtCode ");
                    SQLDtl.AppendLine("    Left Join TblItemGroup K1 ON H1.ItGrpCode = K1.ItGrpCode ");
                    SQLDtl.AppendLine("    Left Join TblItemPackagingUnit L1 ON H1.SalesUomCode = L1.UomCOde And A.ItCode = L1.ItCode ");
                    SQLDtl.AppendLine(")X1 On A.DocNo = X1.DocNo And A.DNo = X1.DNo ");

                    SQLDtl.AppendLine("Left Join TblDOCt2Dtl3 C2 On A.DOCtDocNo=C2.DocNo And A.DOCtDNo=C2.DNo ");
                    SQLDtl.AppendLine("Left Join TblPLDtl D2 On B.PLDocNo=D2.DocNo And C2.PLDNo=D2.DNo ");
                    SQLDtl.AppendLine("Left Join TblSOHdr E2 On D2.SODocNo=E2.DocNo ");
                    SQLDtl.AppendLine("Left Join TblSODtl F2 On D2.SODocNo=F2.DocNo And D2.SODNo=F2.DNo ");
                    SQLDtl.AppendLine("Left Join TblCtQtHdr G2 On E2.CtQtDocNo=G2.DocNo ");
                    SQLDtl.AppendLine("Left Join TblCtQtDtl H2 On E2.CtQtDocNo=H2.DocNo And F2.CtQtDNo=H2.DNo ");
                    SQLDtl.AppendLine("Left Join TblItemPriceHdr I2 On H2.ItemPriceDocNo=I2.DocNo ");
                    SQLDtl.AppendLine("Left Join TblItemPriceDtl J2 On H2.ItemPriceDocNo=J2.DocNo And H2.ItemPriceDNo=J2.DNo ");
                    SQLDtl.AppendLine("Left Join TblItem K2 On J2.ItCode=K2.ItCode ");
                    SQLDtl.AppendLine("Left Join TblPaymentTerm L2 On G2.PtCode=L2.PtCode ");
                    SQLDtl.AppendLine("Left Join TblPLHdr M2 On B.PLDocNo=M2.DocNo ");
                    SQLDtl.AppendLine("Left Join TblItemPackagingUnit N2 On F2.PackagingUnitUomCode = N2.UomCOde And A.ItCode = N2.ItCode ");
                    SQLDtl.AppendLine("Left Join TblItemGroup O2 On O2.ItGrpCode=K2.ItGrpCode ");

                    SQLDtl.AppendLine("Left Join TblDOCt2Dtl C3 On A.DOCtDocNo=C3.DocNo And A.DOCtDNo=C3.DNo ");

                    SQLDtl.AppendLine("Where A.DocNo = @DocNo ");
                    SQLDtl.AppendLine("Group By A.DOCtDocNo, A.DOCtDNo ");

                    cmDtl.CommandText = SQLDtl.ToString();

                    Sm.CmParam<String>(ref cmDtl, "@DocNo", TxtDocNo.Text);
                    //Sm.CmParam<String>(ref cmDtl, "@CtCode", Sm.GetLue(LueCtCode));

                    var drDtl = cmDtl.ExecuteReader();
                    var cDtl = Sm.GetOrdinal(drDtl, new string[]
                    {
                     //0
                     "ItCode" ,

                     //1-5
                     "ItName" ,
                     "QtyPackagingUnit",
                     "PackagingUnitUomCode",
                     "Qty",
                     "PriceUomCode",

                     //6-10
                     "UPriceBeforeTax",
                     "TaxRate",
                     "TaxAmt",
                     "UPriceAfterTax",
                     "PtName",

                     //11-15
                     "Amt",
                     "Qty2",
                     "AddCost",
                     "AddDisc",
                     "ItCodeInternal",

                     //16-18
                     "Remark",
                     "Month",
                     "Year",
                     "DOCtRemark"
                    });
                    if (drDtl.HasRows)
                    {
                        int No = 0;
                        while (drDtl.Read())
                        {
                            No = No + 1;
                            ldtl.Add(new InvoiceDtl()
                            {
                                nomor = No,
                                ItCode = Sm.DrStr(drDtl, cDtl[0]),
                                ItName = Sm.DrStr(drDtl, cDtl[1]),
                                QtyPackagingUnit = Sm.DrDec(drDtl, cDtl[2]),
                                PackagingUnitUomCode = Sm.DrStr(drDtl, cDtl[3]),
                                Qty = Sm.DrDec(drDtl, cDtl[4]),

                                PriceUomCode = Sm.DrStr(drDtl, cDtl[5]),
                                UPriceBeforeTax = Sm.DrDec(drDtl, cDtl[6]),
                                TaxRate = Sm.DrDec(drDtl, cDtl[7]),
                                TaxAmt = Sm.DrDec(drDtl, cDtl[8]),
                                UPriceAfterTax = Sm.DrDec(drDtl, cDtl[9]),

                                PtName = Sm.DrStr(drDtl, cDtl[10]),
                                Amt = Sm.DrDec(drDtl, cDtl[11]),
                                CurCode = Sm.GetLue(LueCurCode),
                                Qty2 = Sm.DrDec(drDtl, cDtl[12]),
                                AddCost = Sm.DrDec(drDtl, cDtl[13]),

                                AddDisc = Sm.DrDec(drDtl, cDtl[14]),
                                ItCodeInternal = Sm.DrStr(drDtl, cDtl[15]),
                                Remark = Sm.DrStr(drDtl, cDtl[16]),
                                Month = Sm.DrStr(drDtl, cDtl[17]),
                                Year = Sm.DrStr(drDtl, cDtl[18]),
                                DOCtRemark = Sm.DrStr(drDtl, cDtl[19]),
                            });
                        }
                    }
                    drDtl.Close();
                }
                myLists.Add(ldtl);
                #endregion

                #region Detail 2
                var cmDtl2 = new MySqlCommand();
                var SQLDtl2 = new StringBuilder();
                using (var cnDtl2 = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl2.Open();
                    cmDtl2.Connection = cnDtl2;

                    SQLDtl2.AppendLine("Select ");
                    SQLDtl2.AppendLine("If(A.DocType='1', Group_Concat(Distinct X1.CtPONo SEPARATOR ','), Group_Concat(Distinct E2.CtPONo SEPARATOR ',')) As PONumber, ");
                    SQLDtl2.AppendLine("If(A.DocType='1', Group_concat(Distinct X1.LocalDocno separator ', '), Group_concat(Distinct E2.LocalDocno separator ', ')) As SODocNo ");
                    SQLDtl2.AppendLine("From TblSalesInvoiceDtl A ");
                    SQLDtl2.AppendLine("Inner Join TblDOCt2Hdr B On A.DOCtDocNo=B.DocNo ");

                    SQLDtl2.AppendLine("Left Join ");
                    SQLDtl2.AppendLine("( ");
                    SQLDtl2.AppendLine("    Select A.DocNo, A.DNo, ");
                    SQLDtl2.AppendLine("    E1.CtPONo, E1.LocalDocNo ");
                    SQLDtl2.AppendLine("    From TblSalesInvoiceDtl A ");
                    SQLDtl2.AppendLine("    Inner Join TblDOCt2Hdr B On A.DOCtDocNo=B.DocNo AND A.DocNo = @DocNo ");
                    SQLDtl2.AppendLine("    Inner Join TblDOCt2Dtl2 C1 On A.DOCtDocNo=C1.DocNo And A.DOCtDNo=C1.DNo ");
                    SQLDtl2.AppendLine("    Inner Join TblDRDtl D1 On B.DRDocNo=D1.DocNo And C1.DRDNo=D1.DNo ");
                    SQLDtl2.AppendLine("    Inner Join TblSOHdr E1 On D1.SODocNo=E1.DocNo ");
                    SQLDtl2.AppendLine("    Inner Join TblSODtl F1 On D1.SODocNo=F1.DocNo And D1.SODNo=F1.DNo ");
                    SQLDtl2.AppendLine("    Union All ");
                    SQLDtl2.AppendLine("    Select A.DocNo, A.DNo, ");
                    SQLDtl2.AppendLine("    Null AS CtPONo, E1.LocalDocNo ");
                    SQLDtl2.AppendLine("    From TblSalesInvoiceDtl A ");
                    SQLDtl2.AppendLine("    Inner Join TblDOCt2Hdr B On A.DOCtDocNo=B.DocNo AND A.DocNo = @DocNo ");
                    SQLDtl2.AppendLine("    Inner Join TblDOCt2Dtl2 C1 On A.DOCtDocNo=C1.DocNo And A.DOCtDNo=C1.DNo ");
                    SQLDtl2.AppendLine("    Inner Join TblDRDtl D1 On B.DRDocNo=D1.DocNo And C1.DRDNo=D1.DNo ");
                    SQLDtl2.AppendLine("    Inner Join TblSalesContract E1 ON D1.SCDocNo = E1.DocNo ");
                    SQLDtl2.AppendLine(")X1 On A.DocNo = X1.DocNo And A.DNo = X1.DNo ");

                    SQLDtl2.AppendLine("Left Join TblDOCt2Dtl3 C2 On A.DOCtDocNo=C2.DocNo And A.DOCtDNo=C2.DNo ");
                    SQLDtl2.AppendLine("Left Join TblPLDtl D2 On B.PLDocNo=D2.DocNo And C2.PLDNo=D2.DNo ");
                    SQLDtl2.AppendLine("Left Join TblSOHdr E2 On D2.SODocNo=E2.DocNo ");
                    SQLDtl2.AppendLine("Left Join TblSODtl F2 On D2.SODocNo=F2.DocNo And D2.SODNo=F2.DNo ");
                    SQLDtl2.AppendLine("Where A.DocNo = @DocNo ");
                    SQLDtl2.AppendLine("Group BY A.DocNo ");

                    cmDtl2.CommandText = SQLDtl2.ToString();

                    Sm.CmParam<String>(ref cmDtl2, "@DocNo", TxtDocNo.Text);

                    var drDtl2 = cmDtl2.ExecuteReader();
                    var cDtl2 = Sm.GetOrdinal(drDtl2, new string[]
                    {
                     //0
                     "PONumber" ,

                     //1-5
                     "SODocNo" ,
                    });
                    if (drDtl2.HasRows)
                    {
                        while (drDtl2.Read())
                        {
                            ldtl2.Add(new InvoiceDtl2()
                            {
                                PONumber = Sm.DrStr(drDtl2, cDtl2[0]),
                                SODocNo = Sm.DrStr(drDtl2, cDtl2[1]),
                            });
                        }
                    }
                    drDtl2.Close();
                }
                myLists.Add(ldtl2);
                #endregion

                #region Additional Cost

                var cmC = new MySqlCommand();
                var SQLC = new StringBuilder();
                using (var cnC = new MySqlConnection(Gv.ConnectionString))
                {
                    cnC.Open();
                    cmC.Connection = cnC;

                    SQLC.AppendLine("    select DocNo, AcNo, DAmt, CAmt from tblsalesinvoicedtl2 ");
                    SQLC.AppendLine("    where acind = 'Y' ");
                    SQLC.AppendLine("    And DocNo = @DocNo ");

                    cmC.CommandText = SQLC.ToString();

                    Sm.CmParam<String>(ref cmC, "@DocNo", TxtDocNo.Text);

                    var drC = cmC.ExecuteReader();
                    var cDtC = Sm.GetOrdinal(drC, new string[]
                    {
                     //0
                     "DocNo" ,

                     //1-3
                     "AcNo", "DAmt", "CAmt"
                    });

                    decimal mAddCost = 0m;
                    decimal mAddDisc = 0m;

                    if (drC.HasRows)
                    {
                        while (drC.Read())
                        {
                            lC.Add(new AddCost()
                            {
                                DocNo = Sm.DrStr(drC, cDtC[0]),
                                AcNo = Sm.DrStr(drC, cDtC[1]),
                                DAmt = Sm.DrDec(drC, cDtC[2]),
                                CAmt = Sm.DrDec(drC, cDtC[3]),
                            });
                        }
                    }
                    drC.Close();

                    if (lC.Count > 0)
                    {
                        for (int Row = 0; Row < lC.Count; Row++)
                        {
                            string AcType = Sm.GetValue("Select AcType From TblCoa Where AcNo=@Param;", lC[Row].AcNo);
                            if (lC[Row].DAmt > 0)
                            {
                                if (AcType == "D")
                                    mAddCost += lC[Row].DAmt;
                                else
                                    mAddDisc -= lC[Row].DAmt;
                            }
                            if (lC[Row].CAmt > 0)
                            {
                                if (AcType == "C")
                                    mAddCost += lC[Row].CAmt;
                                else
                                    mAddDisc -= lC[Row].CAmt;
                            }
                        }

                        lCD.Add(new AddCostDisc()
                        {
                            AddCost = mAddCost,
                            AddDisc = mAddDisc
                        });
                    }

                }
                myLists.Add(lCD);

                #endregion

                #region Additional Cost SRN

                if (mSLITaxCalculationFormat == "1")
                {
                    var cmC2 = new MySqlCommand();
                    var SQLC2 = new StringBuilder();
                    using (var cnC2 = new MySqlConnection(Gv.ConnectionString))
                    {
                        cnC2.Open();
                        cmC2.Connection = cnC2;

                        SQLC2.AppendLine(" Select A.DocNo, A.AcNo, A.DAmt, A.CAmt, ifnull(B.OptDesc, 'Additional Cost') OptDesc from TblSalesInvoiceDtl2 A ");
                        SQLC2.AppendLine(" Left Join TblOption B ON A.OptAcDesc=B.OptCode AND B.OptCat='AccountDescriptionOnSalesInvoice' ");
                        SQLC2.AppendLine(" where acind = 'Y' ");
                        SQLC2.AppendLine(" And DocNo = @DocNo ");

                        cmC2.CommandText = SQLC2.ToString();

                        Sm.CmParam<String>(ref cmC2, "@DocNo", TxtDocNo.Text);

                        var drC2 = cmC2.ExecuteReader();
                        var cDtC2 = Sm.GetOrdinal(drC2, new string[]
                    {
                     //0
                     "OptDesc" ,

                     //1-3
                     "DAmt", "CAmt"
                    });

                        //decimal mAddCost = 0m;
                        //decimal mAddDisc = 0m;

                        if (drC2.HasRows)
                        {
                            while (drC2.Read())
                            {

                                lCD2.Add(new AddCostDiscSRN()
                                {
                                    OptDesc = Sm.DrStr(drC2, cDtC2[0]),
                                    DAmt = Sm.DrDec(drC2, cDtC2[1]),
                                    CAmt = Sm.DrDec(drC2, cDtC2[2]),
                                });

                            }
                        }
                        drC2.Close();
                    }
                }
                else
                {
                    string CtCode = Sm.GetLue(LueCtCode);
                    for (int i = 0; i < Grd3.Rows.Count - 1; ++i)
                    {
                        if (Sm.GetGrdStr(Grd3, i, 1).Length > 0)
                        {
                            string AcNo = Sm.GetGrdStr(Grd3, i, 1);
                            if (!AcNo.Contains(string.Concat(".", CtCode)))
                            {
                                lCD2.Add(new AddCostDiscSRN()
                                {
                                    OptDesc = Sm.GetGrdStr(Grd3, i, 8),
                                    DAmt = Sm.GetGrdDec(Grd3, i, 4),
                                    CAmt = Sm.GetGrdDec(Grd3, i, 6),
                                });
                            }
                        }

                    }
                    if (lCD2.Count > 0)
                    {
                        string mBlankOptionAddCostDiscSalesInvoice = Sm.GetParameter("BlankOptionAddCostDiscSalesInvoice");
                        if (mBlankOptionAddCostDiscSalesInvoice.Length == 0) mBlankOptionAddCostDiscSalesInvoice = "Additional Cost";
                        foreach (var y in lCD2.Where(w => w.OptDesc.Length <= 0))
                        {
                            y.OptDesc = mBlankOptionAddCostDiscSalesInvoice;
                        }
                    }

                }

                myLists.Add(lCD2);

                #endregion


                if (Sm.GetParameter("DocTitle") == "IMS")
                {
                    Sm.PrintReport(mIsFormPrintOutInvoice, myLists, TableName, false);
                }
                else
                {
                    if (Sm.GetParameter("DocTitle") == "SIER")
                    {
                        Sm.PrintReport(mIsFormPrintOutInvoice, myLists, TableName, false);
                    }
                    else
                    {
                        if (mIsSLIPrintShowAllTax && (Sm.GetLue(LueTaxCode1).Length != 0 || Sm.GetLue(LueTaxCode2).Length != 0 || Sm.GetLue(LueTaxCode3).Length != 0))
                            Sm.PrintReport(mIsFormPrintOutInvoice, myLists, TableName, false);
                        else
                        {
                            if (Decimal.Parse(TxtTotalTax.Text) > 0)
                            {
                                Sm.PrintReport(mIsFormPrintOutInvoice, myLists, TableName, false);
                            }
                            else
                            {
                                Sm.PrintReport(mIsFormPrintOutInvoice + "2", myLists, TableName, false);
                            }
                        }
                    }

                }
                Sm.PrintReport(mFormPrintOutInvoiceReceipt, myLists, TableName, false);

            }

        }

        internal string GenerateReceipt(string DocDt)
        {
            var SQL = new StringBuilder();
            bool IsDocSeqNoEnabled = Sm.GetParameterBoo("IsDocSeqNoEnabled");
            string
                Yr = DocDt.Substring(2, 2),
                Mth = DocDt.Substring(4, 2),
                DocTitle = Sm.GetParameter("DocTitle"),
                DocSeqNo = "4";
            if (IsDocSeqNoEnabled) DocSeqNo = Sm.GetParameter("DocSeqNo");
            
            SQL.Append("Select Concat( ");
            SQL.Append("IfNull(( ");
            SQL.Append("   Select Right(Concat(Repeat('0', " + DocSeqNo + "), Convert(DocNo+1, Char)), " + DocSeqNo + ") From ( ");
            SQL.Append("       Select Convert(Left(DocNo, Locate('/', DocNo)-1), Decimal) As DocNo ");
            SQL.Append("       From TblSalesInvoiceHdr ");
            //SQL.Append("   Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) From ( ");
            //SQL.Append("       Select Convert(Left(DocNo, 4), Decimal) As DocNo From TblSalesInvoiceHdr ");
            SQL.Append("       Where Right(DocNo, 5)=Concat('" + Mth + "','/', '" + Yr + "') ");
            SQL.Append("       Order By Left(DocNo, "+DocSeqNo+") Desc Limit 1 ");
            SQL.Append("       ) As Temp ");
            SQL.Append("   ), Right(Concat(Repeat('0', " + DocSeqNo + "), '1'), " + DocSeqNo + ")) ");
            //SQL.Append("   ), '0001') ");
            SQL.Append(", '/', '" + ((DocTitle.Length == 0) ? "XXX" : DocTitle));
            SQL.Append("', '/', '" + "KWT" + "', '/', '" + Mth + "','/', '" + Yr + "'");
            SQL.Append(") As ReceiptNo");

            return Sm.GetValue(SQL.ToString());
        }

        public bool IsDocNeedApproval(string Amt)
        {
            return Sm.IsDataExist("Select 1 From TblDocApprovalSetting T Where T.DocType='SalesInvoice' " +
                "And (T.StartAmt=0 " +
                "Or T.StartAmt<=IfNull(( " +
                "    Select IfNull(@Param, 0) " +
                "), 0)) " +
                "And (T.EndAmt=0 " +
                "Or T.EndAmt>=IfNull(( " +
                "    Select IfNull(@Param, 0) " +
                "), 0)) " +
                "Limit 1;", Amt);
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void DteDocDt_EditValueChanged(object sender, EventArgs e)
        {
            DteDueDt.EditValue = null;
        }

        private void ChkHideInfoInGrd_CheckedChanged(object sender, EventArgs e)
        {
            HideInfoInGrd();
        }

        private void LueFontSize_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                var TheFont = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
                Grd1.Font = TheFont;
                Grd2.Font = TheFont;
            }
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            if (mIsCustomerComboBasedOnCategory)
                Sm.RefreshLookUpEdit(LueCtCtCode, new Sm.RefreshLue4(SetLueCtCodeBasedOnCategory), string.Empty, mIsFilterByCtCt ? "Y" : "N", Sm.GetLue(LueCtCtCode));
            else
                Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue2(SetLueCtCode), string.Empty);

            ReloadCustomer();
        }

        private void LueCurCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCurCode, new Sm.RefreshLue1(Sl.SetLueCurCode));
        }

        private void TxtDownpayment_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtDownpayment, 0);
            ComputeAmt();
        }

        private void TxtLocalDocNo_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtLocalDocNo);
        }

        private void TxtTaxInvDocument_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtTaxInvDocument);
        }

        private void LueBankAcCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueBankAcCode, new Sm.RefreshLue1(Sl.SetLueBankAcCode));
        }

        private void LueOption_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueOption, new Sm.RefreshLue1(SetLueOptionCode));
        }

        private void LueOption_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd3, ref fAccept, e);
        }

        private void LueOption_Leave(object sender, EventArgs e)
        {
            if (LueOption.Visible && fAccept && fCell.ColIndex == 8)
            {
                if (Sm.GetLue(LueOption).Length == 0)
                {
                    Grd3.Cells[fCell.RowIndex, 7].Value =
                    Grd3.Cells[fCell.RowIndex, 8].Value = null;
                }
                else
                {
                    Grd3.Cells[fCell.RowIndex, 7].Value = Sm.GetLue(LueOption);
                    Grd3.Cells[fCell.RowIndex, 8].Value = LueOption.GetColumnValue("Col2");
                }
                LueOption.Visible = false;
            }
        }

        private void LueSPCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueSPCode, new Sm.RefreshLue1(SetLueSPCode));
        }

        private void TxtTaxInvoiceNo_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtTaxInvoiceNo);
        }

        private void LueTaxCode1_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueTaxCode1, new Sm.RefreshLue1(Sl.SetLueTaxCode));
                if (mSalesInvoiceTaxCalculationFormula == "2") ComputeTaxPerDetail(false, 0);
                else ComputeAmt();
            }
        }

        private void TxtAlias1_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtAlias1);
        }

        private void TxtTaxInvoiceNo2_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtTaxInvoiceNo2);
        }

        private void LueTaxCode2_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueTaxCode2, new Sm.RefreshLue1(Sl.SetLueTaxCode));
                if (mSalesInvoiceTaxCalculationFormula == "2") ComputeTaxPerDetail(false, 0);
                else ComputeAmt();
            }
        }

        private void TxtAlias2_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtAlias2);
        }

        private void TxtTaxInvoiceNo3_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtTaxInvoiceNo3);
        }

        private void LueTaxCode3_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueTaxCode3, new Sm.RefreshLue1(Sl.SetLueTaxCode));
                if (mSalesInvoiceTaxCalculationFormula == "2") ComputeTaxPerDetail(false, 0);
                else ComputeAmt();
            }
        }

        private void TxtAlias3_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtAlias3);
        }

        private void LueCtCtCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled && mIsCustomerComboBasedOnCategory && IsInsert)
            {
                Sm.RefreshLookUpEdit(LueCtCtCode, new Sm.RefreshLue1(SetLueCtCtCode));
                SetLueCtCodeBasedOnCategory(ref LueCtCode, string.Empty, mIsFilterByCtCt ? "Y" : "N", Sm.GetLue(LueCtCtCode));
                ReloadCustomer();
            }
        }

        #endregion

        #region Button Event

        private void BtnDueDt_Click(object sender, EventArgs e)
        {
            DteDueDt.EditValue = null;
            var DocDt = Sm.GetDte(DteDocDt);
            if (DocDt.Length > 0 && Grd1.Rows.Count > 1)
            {
                decimal TOP = Sm.GetGrdDec(Grd1, 0, 28);
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0)
                    {
                        if (TOP > Sm.GetGrdDec(Grd1, Row, 28))
                            TOP = Sm.GetGrdDec(Grd1, Row, 28);
                    }
                }
                
                DteDueDt.EditValue = new DateTime(
                   Int32.Parse(DocDt.Substring(0, 4)),
                   Int32.Parse(DocDt.Substring(4, 2)),
                   Int32.Parse(DocDt.Substring(6, 2)),
                   0, 0, 0).AddDays((double)TOP);
            
            }

        }

        private void BtnLocalDocNo_Click(object sender, EventArgs e)
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 29).Length > 0)
                {
                    TxtLocalDocNo.EditValue = Sm.GetGrdStr(Grd1, Row, 29);
                    break;
                }
            }
        }

        private void BtnJournalDocNo_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtJournalDocNo, "Journal#", false))
            {
                var f = new FrmJournal(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtJournalDocNo.Text;
                f.ShowDialog();
            }
        }

        private void BtnJournalDocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtJournalDocNo2, "Journal#", false))
            {
                var f = new FrmJournal(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtJournalDocNo2.Text;
                f.ShowDialog();
            }
        }

        #endregion

        #region Grid

        #region Grd1

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                if (mIsSalesContractEnabled)
                {
                    var f = new FrmDOCt7(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                    f.ShowDialog();
                }
                else
                {
                    var f = new FrmDOCt2(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                    f.ShowDialog();
                }
            }

            if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length != 0)
            {
                e.DoDefault = false;
                if (mIsSalesContractEnabled)
                {
                    var f = new FrmDR3(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                    f.ShowDialog();
                }
                else
                {
                    var f = new FrmDR(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                    f.ShowDialog();
                }
            }

            if (e.ColIndex == 11 && Sm.GetGrdStr(Grd1, e.RowIndex, 9).Length != 0)
            {
                e.DoDefault = false;

                if (mIsSalesContractEnabled)
                {
                    var f = new FrmSalesContract(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 9);
                    f.ShowDialog();
                }
                else
                {
                    var f = new FrmSO2(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 9);
                    f.ShowDialog();
                }
            }

            if (e.ColIndex == 14 && Sm.GetGrdStr(Grd1, e.RowIndex, 12).Length != 0)
            {
                e.DoDefault = false;
                if (mIsSalesContractEnabled)
                {
                    var f = new FrmSalesMemo(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 12);
                    f.ShowDialog();
                }
                else
                {
                    var f = new FrmCtQt(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 12);
                    f.ShowDialog();
                }
            }

            if (e.ColIndex == 16 && Sm.GetGrdStr(Grd1, e.RowIndex, 15).Length != 0)
            {
                e.DoDefault = false;
                Sm.ShowItemInfo(mMenuCode, Sm.GetGrdStr(Grd1, e.RowIndex, 15));
            }

            if (e.ColIndex == 1 && TxtDocNo.Text.Length == 0 && !Sm.IsLueEmpty(LueCtCode, "Customer"))
            {
                e.DoDefault = false;
                Sm.FormShowDialog(new FrmSalesInvoiceDlg(this, Sm.GetLue(LueCtCode)));
            }
        }

        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (mSalesInvoiceTaxCalculationFormula != "1")
            {
                if (e.ColIndex == 37 || e.ColIndex == 38 || e.ColIndex == 39)
                {
                    ComputeTaxPerDetail(true, e.RowIndex);
                }
            }
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            ComputeTotalQtyPackagingUnit();
            ComputeTotalQty();
            ComputeAmt();
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0 && e.ColIndex == 1 && !Sm.IsLueEmpty(LueCtCode, "Customer"))
                Sm.FormShowDialog(new FrmSalesInvoiceDlg(this, Sm.GetLue(LueCtCode)));

            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                if (mIsSalesContractEnabled)
                {
                    var f = new FrmDOCt7(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                    f.ShowDialog();
                }
                else
                {
                    var f = new FrmDOCt2(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                    f.ShowDialog();
                }
            }

            if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length != 0)
            {
                if (mIsSalesContractEnabled)
                {
                    var f = new FrmDR3(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                    f.ShowDialog();
                }
                else
                {
                    var f = new FrmDR(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                    f.ShowDialog();
                }
            }

            if (e.ColIndex == 11 && Sm.GetGrdStr(Grd1, e.RowIndex, 9).Length != 0)
            {
                if (mIsSalesContractEnabled)
                {
                    var f = new FrmSalesContract(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 9);
                    f.ShowDialog();
                }
                else
                {
                    var f = new FrmSO2(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 9);
                    f.ShowDialog();
                }
            }

            if (e.ColIndex == 14 && Sm.GetGrdStr(Grd1, e.RowIndex, 12).Length != 0)
            {
                if (mIsSalesContractEnabled)
                {
                    var f = new FrmSalesMemo(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 12);
                    f.ShowDialog();
                }
                else
                {
                    var f = new FrmCtQt(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 12);
                    f.ShowDialog();
                }
            }

            if (e.ColIndex == 16 && Sm.GetGrdStr(Grd1, e.RowIndex, 15).Length != 0)
                Sm.ShowItemInfo(mMenuCode, Sm.GetGrdStr(Grd1, e.RowIndex, 15));

            //download file
            if (e.ColIndex == 34)
            {
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 32).Length > 0)
                {
                    DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, Sm.GetGrdStr(Grd1, e.RowIndex, 32), mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
                    SFD.FileName = Sm.GetGrdStr(Grd1, e.RowIndex, 32);
                    SFD.DefaultExt = "pdf";
                    SFD.AddExtension = true;

                    if (!Sm.IsGrdValueEmpty(Grd1, e.RowIndex, 32, false, "File name is empty") && downloadedData != null && downloadedData.Length != 0)
                    {
                        if (SFD.ShowDialog() == DialogResult.OK)
                        {
                            Application.DoEvents();

                            //Write the bytes to a file
                            FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                            newFile.Write(downloadedData, 0, downloadedData.Length);
                            newFile.Close();
                            Sm.StdMsg(mMsgType.Info, "File Downloaded");
                        }
                    }
                    else
                        Sm.StdMsg(mMsgType.Warning, "No File Downloaded");
                }
            }

            //upload file
            if (BtnSave.Enabled && e.ColIndex == 33)
            {
                OD.InitialDirectory = "c:";
                OD.Filter = "PDF files (*.pdf)|*.pdf";
                OD.FilterIndex = 2;
                OD.ShowDialog();
                Grd1.Cells[e.RowIndex, 32].Value = OD.FileName;
            }
        }

        #endregion

        #region Grd3

        private void Grd3_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (e.ColIndex == 4 && Sm.GetGrdDec(Grd3, e.RowIndex, 4)>0)
            {
                Grd3.Cells[e.RowIndex, 6].Value = 0;
            }

            if (e.ColIndex == 6 && Sm.GetGrdDec(Grd3, e.RowIndex, 6)>0)
            {
                Grd3.Cells[e.RowIndex, 4].Value = 0;
            }

            if (e.ColIndex == 3 || e.ColIndex == 5)
                Grd3.Cells[e.RowIndex, 10].Value = Sm.GetGrdBool(Grd3, e.RowIndex, e.ColIndex);

            if (Sm.IsGrdColSelected(new int[] { 3, 4, 5, 6 }, e.ColIndex))
                ComputeAmt();
        }

        private void Grd3_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0 && TxtDocNo.Text.Length == 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmSalesInvoiceDlg2(this));
            }

            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 8 }, e.ColIndex))
            {
                LueRequestEdit(Grd3, LueOption, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd3, e.RowIndex);
                Sm.SetGrdNumValueZero(ref Grd3, Grd3.Rows.Count - 1, new int[] { 4, 6 });
                SetLueOptionCode(ref LueOption);
            }
        }

        private void Grd3_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && TxtDocNo.Text.Length == 0) Sm.FormShowDialog(new FrmSalesInvoiceDlg2(this));
        }

        private void Grd3_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd3, e, BtnSave);
            ComputeAmt();
            Sm.GrdEnter(Grd3, e);
            Sm.GrdTabInLastCell(Grd3, e, BtnFind, BtnSave);
        }

        #endregion

        #endregion

        #endregion

        #region Class

        #region Report Class

        private class InvoiceHdr
        {
            public string CompanyLogo { set; get; }
            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyAddressFull { get; set; }
            public string CompanyPhone { get; set; }
            public string CompanyFax { get; set; }
            public string LocalDocNo { get; set; }
            public string DocDt { get; set; }
            public string CtName { get; set; }
            public string DesAddress { get; set; }
            public string Address { get; set; }
            public string CurCode { get; set; }
            public decimal TotalAmt { get; set; }
            public decimal TotalTax { get; set; }
            public decimal DownPayment { get; set; }
            public decimal Amt { get; set; }
            public string SalesName { get; set; }
            public string ExpVd { get; set; }
            public string ExpPlatNo { get; set; }
            public string Remark { get; set; }
            public string PrintBy { get; set; }
            public string Terbilang { get; set; }
            public string Terbilang2 { get; set; }
            public string TaxInvNo { get; set; }
            public string BankAcNm { get; set; }
            public string BankAcNo { get; set; }
            public string CancelInd { get; set; }
            public string CompLocation2 { get; set; }
            public string PLDocNo { get; set; }
            public string PLLocal { get; set; }
            public string SOLocal { get; set; }
            public string CtPONo { get; set; }
            public string Account { get; set; }
            public string PortName { get; set; }
            public string SPPlaceDelivery { get; set; }
            public string SpName { get; set; }
            public string NPWP { get; set; }
            public string CtCode { get; set; }
            public string Email { get; set; }
            public string CtNPWP { get; set; }
            public string DocNo { get; set; }
            public string DueDt { get; set; }
            public string CompanyNPWP { get; set; }
            public string CityNameCT { get; set; }
            public string BankName { get; set; }
            public string ReceiptNo { get; set; }
            public string ReceiptDt { get; set; }
            public decimal AddCost { get; set; }
            public string AddressTTD { get; set; }
            public string SLINotes { get; set; }
            public string SLINotes2 { get; set; }
            public string DocDtMthYr { get; set; }
            public string TaxCode1 { get; set; }
            public string TaxCode2 { get; set; }
            public string TaxCode3 { get; set; }
            public decimal TaxAmt1 { get; set; }
            public decimal TaxAmt2 { get; set; }
            public decimal TaxAmt3 { get; set; }
            public decimal InvoiceAmt { get; set; }
            }

        private class InvoiceHdr2
        {
            public string CompanyLogo { set; get; }

            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyAddressFull { get; set; }
            public string CompanyPhone { get; set; }
            public string CompanyEmail { get; set; }

            public string Doc { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string CtName { get; set; }
            public string Address { get; set; }
            public string Phone { get; set; }

            public string SalesName { get; set; }
            public string SAName { get; set; }
            public string SAAddress { get; set; }
            public string PrintBy { get; set; }
            public string CompLocation2 { get; set; }
            public decimal Downpayment { get; set; }

            public string SaPhone { get; set; }
            public string Curcode { get; set; }
            public string UserCode { get; set; }
            public string DueDt  { get; set; }
            public decimal TotalAmt  { get; set; }
            public decimal TotalTax  { get; set; }
            public decimal Amt  { get; set; }
            public string Remark  { get; set; }
            public string Cancelind  { get; set; }
            public string ExpVd { get; set; }
            public string CtCityName { get; set; }
            
        }

        private class InvoiceHdrSKI
        {
            public string CompanyLogo { set; get; }
            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyAddress2 { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string CustomerName { get; set; }
            public string CustomerAddress { get; set; }
            public string CustomerPhone { get; set; }
            public string CustomerFax { get; set; }
            public string SODocNo { get; set; }
            public string SODocDt { get; set; }
            public string ReceiptNo { get; set; }
            public string SLIRemark { get; set; }
            public decimal InvoiceAmount { get; set; }
            public string SORemark { get; set; }
            public string Terbilang { get; set; }
            public string CurrentDate { get; set; }
            public string DueDate { get; set; }
            public string LocalDocNo { get; set; }
            public string Comodity { get; set; } 
            
        }

        private class InvoiceDtlSKI
        {
            public int Nomor { get; set; }
            public string DocNo { get; set; }
            public string SORemark { get; set; }
            public decimal PriceBeforeTax { get; set; }
            public decimal TaxAmt { get; set; }
            public decimal PriceAfterTax { get; set; }
            public string TaxName { get; set; }
            public decimal TaxRate { get; set; }
            public string TerbilangTotal { get; set; }
        }
            private class InvoiceDtl2SKI
        {
            public string DocNo { get; set; }
            public string BankAcNo { get; set; }
            public string BranchName { get; set; }
            public string BankName { get; set; }
            public string SignName { get; set; }
        }

        private class InvoiceDtl
        {
            public int nomor { get; set; }
            public decimal UPriceBeforeTax { get; set; }
            public decimal UPriceAfterTax { get; set; }
            public string ItCode { get; set; }
            public string ItName { get; set; }
            public decimal QtyPackagingUnit { get; set; }
            public string PackagingUnitUomCode { get; set; }
            public decimal Qty { get; set; }
            public decimal UPrice { get; set; }
            public string CurCode { get; set; }
            public string CTPoNo { get; set; }
            public string PriceUomCode { get; set; }
            public decimal Discount { get; set; }
            public decimal DiscAmt { get; set; }
            public decimal TaxRate { get; set; }
            public decimal TaxAmt { get; set; }
            public decimal Amt { get; set; }
            public decimal AmtDisc { get; set; }
            public decimal AmtTax { get; set; }
            public decimal AmtCost { get; set; }
            public string PtName { get; set; }
            public decimal Qty2 { get; set; }
            public string SODocNo { get; set; }
            public decimal AddCost { get; set; }
            public decimal AddDisc { get; set; }
            public string SACityName { get; set; }
            public string HSCode { get; set; }
            public string Color { get; set; } 
            public string Material { get; set; }
            public string ItCodeInternal { get; set; } 
            public string DTNAme { get; set; }
            public string CtItCode { get; set; }
            public string Top { get; set; }
            public decimal QtyPcs { get; set; }
            public string DocDt { get; set; }
            public string Remark { get; set; }
            public string Month { get; set; }
            public string Year { get; set; }
            public string DOCtRemark { get; set; }
        }

        private class InvoiceDtl2
        {
            public string PONumber { get; set; }
            public string SODocNo { get; set; }
            public string DocDt { get; set; }
            public string Period { get; set; }
            public decimal InvoiceAmt { get; set; }
            public string CurCode { get; set; }
        }

        private class InvoiceDtl3
        {
            public string DocNo { get; set; }

            public string ItCode { get; set; }
            public string ItName { get; set; }
            public decimal Qty { get; set; }
            public string PriceUomCode { get; set; }
            public decimal TaxAmt { get; set; }
            public decimal Uprice { get; set; }

            public decimal UPriceAfterTax { get; set; }
            public decimal Discount { get; set; }
            public decimal SubTotal { get; set; }
            public string PTName { get; set; }
            public string DocNoSO { get; set; }
            public decimal DiscAmt { get; set; }
            public decimal Amt { get; set; }
            public string SACityName { get; set; }

        }

        private class InvoiceDtl4
        {
            public string DocNo { get; set; }

            public decimal TotalAmt { get; set; }
            public decimal TotalTax { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
            public decimal Total { get; set; }
            public string Terbilang { get; set; }
            public string Terbilang2 { get; set; }

        }

        private class InvoiceDtl5
        {
            public string AcNo { get; set; }
            public string AcDesc { get; set; }
            public string AcType { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
            public decimal AmtAcNo { get; set; }
            public string OptDesc { get; set; }
            public string Remark { get; set; }
            public decimal UniqInd { get; set; }
        }

        private class InvoiceDtl6
        {
            public string IsCustomerItemNameMandatory { get; set; }
            public string CtItName { get; set; }
        }

        private class InvoiceDtl7
        {
            public string DocNo { get; set; }
            public decimal Amt { get; set; }
            public decimal Qty2 { get; set; }
            public decimal AddCost { get; set; }
            public decimal AddDisc { get; set; }
            public decimal GrandTot { get; set; }
            public string Terbilang { get; set; }
        }

        private class AddCost
        {
            public string DocNo { get; set; }
            public string AcNo { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
        }

        private class AddCostDisc
        {
            public decimal AddCost { get; set; }
            public decimal AddDisc { get; set; }
        }

        private class AddCostDiscSRN
        {
            public string OptDesc { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
            public bool CheckDAmt { get; set; }
            public bool CheckCAmt { get; set; }

        }

        private class InvoiceHdrKIM
        {
            public string CompanyLogo { set; get; }
            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyAddressFull { get; set; }
            public string CompanyPhone { get; set; }
            public string CompanyFax { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string DueDt { get; set; }
            public string CurCode { get; set; }
            public decimal TotalTax { get; set; }
            public decimal TotalAmt { get; set; }
            public decimal DownPayment { get; set; }
            public string CtName { get; set; }
            public string Address { get; set; }
            public string SAName { get; set; }
            public string SAAddress { get; set; }
            public string Remark { get; set; }
            public decimal Amt { get; set; }
            public string Terbilang { get; set; }
            public string CityName { get; set; }
            public string SACityName { get; set; }
            public string RemarkSI { get; set; }
            public string ReceiptNo { get; set; }
        }

        private class InvoiceDtlKIM
        {
            public string ItCode { get; set; }
            public string ItName { get; set; }
            public decimal Qty { get; set; }
            public decimal UPriceBeforeTax { get; set; }
            public decimal Amt { get; set; }
            public string PriceUomCode { get; set; }
            public string Remark { get; set; }
            public decimal Tax { get; set; }
        }

        private class InvoiceDtl2KIM
        {
            public string DocNo { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
            public decimal TAmt { get; set; }
        }

        private class Employee
        {
            public string EmpCode { set; get; }
            public string EmpName { get; set; }
            public string Position { get; set; }
            public string EmpPict { get; set; }
        }

        #endregion       

        public class DepositSummary
        {
            public decimal ExcRate { get; set; }
            public decimal Amt { get; set; }
            public decimal UsedAmt { get; set; }
        }

        private class EmployeeTaxCollector
        {
            public string EmpCode { set; get; }
            public string EmpName { get; set; }
            public string Position { get; set; }
            public string Mobile { get; set; }
        }

        //class Rate
        //{
        //    public decimal Downpayment { get; set; }
        //    public decimal Amt { get; set; }
        //    public string CurCode { get; set; }
        //    public decimal ExcRate { get; set; }
        //    public string CtCode { get; set; }
        //}

        private class SISignature
        {
            public string UserCode { get; set; }
            public string UserName { get; set; }
            public string EmpPict { get; set; }
            public string PosName { get; set; }

        }

        private class SLI
        {
            public string DocNo { get; set; }
            public string DNo { get; set; }

        }

        private class InvoiceHdrKBN
        {
            public string CompanyLogo { set; get; }
            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyAddressFull { get; set; }
            public string CompanyPhone { get; set; }
            public string CompanyFax { get; set; }
            public string WhsName { get; set; }
            public string DocNo { get; set; }
            public string CtName { get; set; }
            public string Address { get; set; }
            public string Remark { get; set; }
            public string NPWP { get; set; }
            public string DueDt { get; set; }
            public decimal TotalAmt { get; set; }
            public decimal TotalTax { get; set; }
            public decimal InvoiceAmt { get; set; }
            public string Terbilang { get; set; }
            public string BankAcNm { get; set; }
            public string BankAcNo { get; set; }
            public string CurCode { get; set; }

        }

        private class InvoiceDtlKBN
        {
            public int Nomor { get; set; }
            public string SODocNo { get; set; }
            public string SODNo { get; set; }
            public string ItName { get; set; }
            public decimal Qty { get; set; }
            public string PriceUomCode { get; set; }
            public string CurCode { get; set; }
            public decimal UPriceBeforeTax { get; set; }
            public decimal TaxAmt { get; set; }
            public decimal UPriceAfterTax { get; set; }
        }

        private class SignatureKBN
        {
            public string Signature { set; get; }
            public string UserName { get; set; }
            public string PosName { get; set; }
            public string DocDt { get; set; }
            public string SiteName { get; set; }
        }

        #endregion

        private void BtnCtCode_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmSalesInvoiceDlg3(this));
        }
    }
}
