﻿namespace RunSystem
{
    partial class FrmQt3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmQt3));
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.LuePtCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.LueVdCode = new DevExpress.XtraEditors.LookUpEdit();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.LueCurCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.LueDTCode = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtStatus = new DevExpress.XtraEditors.TextEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.DteExpDt = new DevExpress.XtraEditors.DateEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.PnlUpload = new System.Windows.Forms.Panel();
            this.BtnDownload5 = new DevExpress.XtraEditors.SimpleButton();
            this.BtnUpload5 = new DevExpress.XtraEditors.SimpleButton();
            this.ChkFile5 = new DevExpress.XtraEditors.CheckEdit();
            this.PbUpload5 = new System.Windows.Forms.ProgressBar();
            this.TxtFile5 = new DevExpress.XtraEditors.TextEdit();
            this.label14 = new System.Windows.Forms.Label();
            this.BtnDownload4 = new DevExpress.XtraEditors.SimpleButton();
            this.BtnUpload4 = new DevExpress.XtraEditors.SimpleButton();
            this.BtnDownload3 = new DevExpress.XtraEditors.SimpleButton();
            this.BtnUpload3 = new DevExpress.XtraEditors.SimpleButton();
            this.ChkFile4 = new DevExpress.XtraEditors.CheckEdit();
            this.PbUpload4 = new System.Windows.Forms.ProgressBar();
            this.TxtFile4 = new DevExpress.XtraEditors.TextEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.ChkFile3 = new DevExpress.XtraEditors.CheckEdit();
            this.PbUpload3 = new System.Windows.Forms.ProgressBar();
            this.TxtFile3 = new DevExpress.XtraEditors.TextEdit();
            this.label13 = new System.Windows.Forms.Label();
            this.BtnDownload2 = new DevExpress.XtraEditors.SimpleButton();
            this.BtnUpload2 = new DevExpress.XtraEditors.SimpleButton();
            this.BtnDownload1 = new DevExpress.XtraEditors.SimpleButton();
            this.BtnUpload1 = new DevExpress.XtraEditors.SimpleButton();
            this.ChkFile2 = new DevExpress.XtraEditors.CheckEdit();
            this.PbUpload2 = new System.Windows.Forms.ProgressBar();
            this.TxtFile2 = new DevExpress.XtraEditors.TextEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.ChkFile1 = new DevExpress.XtraEditors.CheckEdit();
            this.PbUpload1 = new System.Windows.Forms.ProgressBar();
            this.TxtFile1 = new DevExpress.XtraEditors.TextEdit();
            this.label11 = new System.Windows.Forms.Label();
            this.OD1 = new System.Windows.Forms.OpenFileDialog();
            this.SFD1 = new System.Windows.Forms.SaveFileDialog();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePtCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueVdCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDTCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteExpDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteExpDt.Properties)).BeginInit();
            this.PnlUpload.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(833, 0);
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.PnlUpload);
            this.panel2.Controls.Add(this.DteExpDt);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.TxtStatus);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.LueDTCode);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.LueCurCode);
            this.panel2.Controls.Add(this.MeeRemark);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.LuePtCode);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.LueVdCode);
            this.panel2.Controls.Add(this.DteDocDt);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.TxtDocNo);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Size = new System.Drawing.Size(833, 199);
            // 
            // panel3
            // 
            this.panel3.Location = new System.Drawing.Point(0, 199);
            this.panel3.Size = new System.Drawing.Size(833, 274);
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // Grd1
            // 
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 20;
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.Size = new System.Drawing.Size(833, 274);
            this.Grd1.TabIndex = 59;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(119, 174);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 400;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(320, 20);
            this.MeeRemark.TabIndex = 28;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(67, 177);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 14);
            this.label5.TabIndex = 27;
            this.label5.Text = "Remark";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(11, 93);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(103, 14);
            this.label4.TabIndex = 19;
            this.label4.Text = "Term of Payment";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LuePtCode
            // 
            this.LuePtCode.EnterMoveNextControl = true;
            this.LuePtCode.Location = new System.Drawing.Point(119, 90);
            this.LuePtCode.Margin = new System.Windows.Forms.Padding(5);
            this.LuePtCode.Name = "LuePtCode";
            this.LuePtCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePtCode.Properties.Appearance.Options.UseFont = true;
            this.LuePtCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePtCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePtCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePtCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePtCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePtCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePtCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePtCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePtCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePtCode.Properties.DropDownRows = 30;
            this.LuePtCode.Properties.NullText = "[Empty]";
            this.LuePtCode.Properties.PopupWidth = 350;
            this.LuePtCode.Size = new System.Drawing.Size(319, 20);
            this.LuePtCode.TabIndex = 20;
            this.LuePtCode.ToolTip = "F4 : Show/hide list";
            this.LuePtCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LuePtCode.EditValueChanged += new System.EventHandler(this.LuePtCode_EditValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(67, 72);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 14);
            this.label3.TabIndex = 17;
            this.label3.Text = "Vendor";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueVdCode
            // 
            this.LueVdCode.EnterMoveNextControl = true;
            this.LueVdCode.Location = new System.Drawing.Point(119, 69);
            this.LueVdCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueVdCode.Name = "LueVdCode";
            this.LueVdCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVdCode.Properties.Appearance.Options.UseFont = true;
            this.LueVdCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVdCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueVdCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVdCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueVdCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVdCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueVdCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueVdCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueVdCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueVdCode.Properties.DropDownRows = 30;
            this.LueVdCode.Properties.NullText = "[Empty]";
            this.LueVdCode.Properties.PopupWidth = 350;
            this.LueVdCode.Size = new System.Drawing.Size(319, 20);
            this.LueVdCode.TabIndex = 18;
            this.LueVdCode.ToolTip = "F4 : Show/hide list";
            this.LueVdCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueVdCode.EditValueChanged += new System.EventHandler(this.LueVdCode_EditValueChanged);
            this.LueVdCode.Validated += new System.EventHandler(this.LueVdCode_Validated);
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(119, 27);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(119, 20);
            this.DteDocDt.TabIndex = 14;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(81, 30);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 14);
            this.label2.TabIndex = 13;
            this.label2.Text = "Date";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(119, 6);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 30;
            this.TxtDocNo.Properties.ReadOnly = true;
            this.TxtDocNo.Size = new System.Drawing.Size(215, 20);
            this.TxtDocNo.TabIndex = 12;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(41, 9);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 14);
            this.label1.TabIndex = 11;
            this.label1.Text = "Document#";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Red;
            this.label6.Location = new System.Drawing.Point(59, 114);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(55, 14);
            this.label6.TabIndex = 21;
            this.label6.Text = "Currency";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueCurCode
            // 
            this.LueCurCode.EnterMoveNextControl = true;
            this.LueCurCode.Location = new System.Drawing.Point(119, 111);
            this.LueCurCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueCurCode.Name = "LueCurCode";
            this.LueCurCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.Appearance.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCurCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCurCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCurCode.Properties.DropDownRows = 30;
            this.LueCurCode.Properties.NullText = "[Empty]";
            this.LueCurCode.Properties.PopupWidth = 500;
            this.LueCurCode.Size = new System.Drawing.Size(119, 20);
            this.LueCurCode.TabIndex = 22;
            this.LueCurCode.ToolTip = "F4 : Show/hide list";
            this.LueCurCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCurCode.EditValueChanged += new System.EventHandler(this.LueCurCode_EditValueChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(33, 135);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(81, 14);
            this.label7.TabIndex = 23;
            this.label7.Text = "Delivery Type";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueDTCode
            // 
            this.LueDTCode.EnterMoveNextControl = true;
            this.LueDTCode.Location = new System.Drawing.Point(119, 132);
            this.LueDTCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueDTCode.Name = "LueDTCode";
            this.LueDTCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDTCode.Properties.Appearance.Options.UseFont = true;
            this.LueDTCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDTCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueDTCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDTCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueDTCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDTCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueDTCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDTCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueDTCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueDTCode.Properties.DropDownRows = 30;
            this.LueDTCode.Properties.NullText = "[Empty]";
            this.LueDTCode.Properties.PopupWidth = 350;
            this.LueDTCode.Size = new System.Drawing.Size(319, 20);
            this.LueDTCode.TabIndex = 24;
            this.LueDTCode.ToolTip = "F4 : Show/hide list";
            this.LueDTCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueDTCode.EditValueChanged += new System.EventHandler(this.LueDTCode_EditValueChanged);
            // 
            // TxtStatus
            // 
            this.TxtStatus.EnterMoveNextControl = true;
            this.TxtStatus.Location = new System.Drawing.Point(119, 48);
            this.TxtStatus.Margin = new System.Windows.Forms.Padding(5);
            this.TxtStatus.Name = "TxtStatus";
            this.TxtStatus.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtStatus.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtStatus.Properties.Appearance.Options.UseBackColor = true;
            this.TxtStatus.Properties.Appearance.Options.UseFont = true;
            this.TxtStatus.Properties.MaxLength = 16;
            this.TxtStatus.Properties.ReadOnly = true;
            this.TxtStatus.Size = new System.Drawing.Size(119, 20);
            this.TxtStatus.TabIndex = 16;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(72, 51);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(42, 14);
            this.label8.TabIndex = 15;
            this.label8.Text = "Status";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteExpDt
            // 
            this.DteExpDt.EditValue = null;
            this.DteExpDt.EnterMoveNextControl = true;
            this.DteExpDt.Location = new System.Drawing.Point(119, 153);
            this.DteExpDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteExpDt.Name = "DteExpDt";
            this.DteExpDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteExpDt.Properties.Appearance.Options.UseFont = true;
            this.DteExpDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteExpDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteExpDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteExpDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteExpDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteExpDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteExpDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteExpDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteExpDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteExpDt.Size = new System.Drawing.Size(119, 20);
            this.DteExpDt.TabIndex = 26;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(37, 156);
            this.label9.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(77, 14);
            this.label9.TabIndex = 25;
            this.label9.Text = "Expired Date";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // PnlUpload
            // 
            this.PnlUpload.Controls.Add(this.BtnDownload5);
            this.PnlUpload.Controls.Add(this.BtnUpload5);
            this.PnlUpload.Controls.Add(this.ChkFile5);
            this.PnlUpload.Controls.Add(this.PbUpload5);
            this.PnlUpload.Controls.Add(this.TxtFile5);
            this.PnlUpload.Controls.Add(this.label14);
            this.PnlUpload.Controls.Add(this.BtnDownload4);
            this.PnlUpload.Controls.Add(this.BtnUpload4);
            this.PnlUpload.Controls.Add(this.BtnDownload3);
            this.PnlUpload.Controls.Add(this.BtnUpload3);
            this.PnlUpload.Controls.Add(this.ChkFile4);
            this.PnlUpload.Controls.Add(this.PbUpload4);
            this.PnlUpload.Controls.Add(this.TxtFile4);
            this.PnlUpload.Controls.Add(this.label12);
            this.PnlUpload.Controls.Add(this.ChkFile3);
            this.PnlUpload.Controls.Add(this.PbUpload3);
            this.PnlUpload.Controls.Add(this.TxtFile3);
            this.PnlUpload.Controls.Add(this.label13);
            this.PnlUpload.Controls.Add(this.BtnDownload2);
            this.PnlUpload.Controls.Add(this.BtnUpload2);
            this.PnlUpload.Controls.Add(this.BtnDownload1);
            this.PnlUpload.Controls.Add(this.BtnUpload1);
            this.PnlUpload.Controls.Add(this.ChkFile2);
            this.PnlUpload.Controls.Add(this.PbUpload2);
            this.PnlUpload.Controls.Add(this.TxtFile2);
            this.PnlUpload.Controls.Add(this.label10);
            this.PnlUpload.Controls.Add(this.ChkFile1);
            this.PnlUpload.Controls.Add(this.PbUpload1);
            this.PnlUpload.Controls.Add(this.TxtFile1);
            this.PnlUpload.Controls.Add(this.label11);
            this.PnlUpload.Dock = System.Windows.Forms.DockStyle.Right;
            this.PnlUpload.Location = new System.Drawing.Point(457, 0);
            this.PnlUpload.Name = "PnlUpload";
            this.PnlUpload.Size = new System.Drawing.Size(376, 199);
            this.PnlUpload.TabIndex = 28;
            // 
            // BtnDownload5
            // 
            this.BtnDownload5.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDownload5.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDownload5.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDownload5.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDownload5.Appearance.Options.UseBackColor = true;
            this.BtnDownload5.Appearance.Options.UseFont = true;
            this.BtnDownload5.Appearance.Options.UseForeColor = true;
            this.BtnDownload5.Appearance.Options.UseTextOptions = true;
            this.BtnDownload5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDownload5.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDownload5.Image = ((System.Drawing.Image)(resources.GetObject("BtnDownload5.Image")));
            this.BtnDownload5.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDownload5.Location = new System.Drawing.Point(337, 144);
            this.BtnDownload5.Name = "BtnDownload5";
            this.BtnDownload5.Size = new System.Drawing.Size(24, 21);
            this.BtnDownload5.TabIndex = 57;
            this.BtnDownload5.ToolTip = "Download File";
            this.BtnDownload5.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDownload5.ToolTipTitle = "Run System";
            this.BtnDownload5.Click += new System.EventHandler(this.BtnDownload5_Click);
            // 
            // BtnUpload5
            // 
            this.BtnUpload5.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnUpload5.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnUpload5.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnUpload5.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnUpload5.Appearance.Options.UseBackColor = true;
            this.BtnUpload5.Appearance.Options.UseFont = true;
            this.BtnUpload5.Appearance.Options.UseForeColor = true;
            this.BtnUpload5.Appearance.Options.UseTextOptions = true;
            this.BtnUpload5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnUpload5.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnUpload5.Image = ((System.Drawing.Image)(resources.GetObject("BtnUpload5.Image")));
            this.BtnUpload5.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnUpload5.Location = new System.Drawing.Point(310, 143);
            this.BtnUpload5.Name = "BtnUpload5";
            this.BtnUpload5.Size = new System.Drawing.Size(24, 21);
            this.BtnUpload5.TabIndex = 56;
            this.BtnUpload5.ToolTip = "Browse File";
            this.BtnUpload5.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnUpload5.ToolTipTitle = "Run System";
            this.BtnUpload5.Click += new System.EventHandler(this.BtnUpload5_Click);
            // 
            // ChkFile5
            // 
            this.ChkFile5.Location = new System.Drawing.Point(282, 144);
            this.ChkFile5.Name = "ChkFile5";
            this.ChkFile5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkFile5.Properties.Appearance.Options.UseFont = true;
            this.ChkFile5.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkFile5.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkFile5.Properties.Caption = " ";
            this.ChkFile5.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkFile5.Size = new System.Drawing.Size(20, 22);
            this.ChkFile5.TabIndex = 55;
            this.ChkFile5.ToolTip = "Remove filter";
            this.ChkFile5.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkFile5.ToolTipTitle = "Run System";
            this.ChkFile5.CheckedChanged += new System.EventHandler(this.ChkFile5_CheckedChanged);
            // 
            // PbUpload5
            // 
            this.PbUpload5.Location = new System.Drawing.Point(46, 167);
            this.PbUpload5.Name = "PbUpload5";
            this.PbUpload5.Size = new System.Drawing.Size(320, 10);
            this.PbUpload5.TabIndex = 58;
            // 
            // TxtFile5
            // 
            this.TxtFile5.EnterMoveNextControl = true;
            this.TxtFile5.Location = new System.Drawing.Point(46, 145);
            this.TxtFile5.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFile5.Name = "TxtFile5";
            this.TxtFile5.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFile5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFile5.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFile5.Properties.Appearance.Options.UseFont = true;
            this.TxtFile5.Properties.MaxLength = 16;
            this.TxtFile5.Size = new System.Drawing.Size(234, 20);
            this.TxtFile5.TabIndex = 54;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(6, 149);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(35, 14);
            this.label14.TabIndex = 53;
            this.label14.Text = "File 5";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnDownload4
            // 
            this.BtnDownload4.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDownload4.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDownload4.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDownload4.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDownload4.Appearance.Options.UseBackColor = true;
            this.BtnDownload4.Appearance.Options.UseFont = true;
            this.BtnDownload4.Appearance.Options.UseForeColor = true;
            this.BtnDownload4.Appearance.Options.UseTextOptions = true;
            this.BtnDownload4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDownload4.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDownload4.Image = ((System.Drawing.Image)(resources.GetObject("BtnDownload4.Image")));
            this.BtnDownload4.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDownload4.Location = new System.Drawing.Point(337, 109);
            this.BtnDownload4.Name = "BtnDownload4";
            this.BtnDownload4.Size = new System.Drawing.Size(24, 21);
            this.BtnDownload4.TabIndex = 51;
            this.BtnDownload4.ToolTip = "Download File";
            this.BtnDownload4.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDownload4.ToolTipTitle = "Run System";
            this.BtnDownload4.Click += new System.EventHandler(this.BtnDownload4_Click);
            // 
            // BtnUpload4
            // 
            this.BtnUpload4.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnUpload4.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnUpload4.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnUpload4.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnUpload4.Appearance.Options.UseBackColor = true;
            this.BtnUpload4.Appearance.Options.UseFont = true;
            this.BtnUpload4.Appearance.Options.UseForeColor = true;
            this.BtnUpload4.Appearance.Options.UseTextOptions = true;
            this.BtnUpload4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnUpload4.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnUpload4.Image = ((System.Drawing.Image)(resources.GetObject("BtnUpload4.Image")));
            this.BtnUpload4.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnUpload4.Location = new System.Drawing.Point(310, 108);
            this.BtnUpload4.Name = "BtnUpload4";
            this.BtnUpload4.Size = new System.Drawing.Size(24, 21);
            this.BtnUpload4.TabIndex = 50;
            this.BtnUpload4.ToolTip = "Browse File";
            this.BtnUpload4.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnUpload4.ToolTipTitle = "Run System";
            this.BtnUpload4.Click += new System.EventHandler(this.BtnUpload4_Click);
            // 
            // BtnDownload3
            // 
            this.BtnDownload3.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDownload3.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDownload3.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDownload3.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDownload3.Appearance.Options.UseBackColor = true;
            this.BtnDownload3.Appearance.Options.UseFont = true;
            this.BtnDownload3.Appearance.Options.UseForeColor = true;
            this.BtnDownload3.Appearance.Options.UseTextOptions = true;
            this.BtnDownload3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDownload3.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDownload3.Image = ((System.Drawing.Image)(resources.GetObject("BtnDownload3.Image")));
            this.BtnDownload3.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDownload3.Location = new System.Drawing.Point(337, 73);
            this.BtnDownload3.Name = "BtnDownload3";
            this.BtnDownload3.Size = new System.Drawing.Size(24, 21);
            this.BtnDownload3.TabIndex = 45;
            this.BtnDownload3.ToolTip = "Download File";
            this.BtnDownload3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDownload3.ToolTipTitle = "Run System";
            this.BtnDownload3.Click += new System.EventHandler(this.BtnDownload3_Click);
            // 
            // BtnUpload3
            // 
            this.BtnUpload3.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnUpload3.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnUpload3.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnUpload3.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnUpload3.Appearance.Options.UseBackColor = true;
            this.BtnUpload3.Appearance.Options.UseFont = true;
            this.BtnUpload3.Appearance.Options.UseForeColor = true;
            this.BtnUpload3.Appearance.Options.UseTextOptions = true;
            this.BtnUpload3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnUpload3.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnUpload3.Image = ((System.Drawing.Image)(resources.GetObject("BtnUpload3.Image")));
            this.BtnUpload3.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnUpload3.Location = new System.Drawing.Point(310, 74);
            this.BtnUpload3.Name = "BtnUpload3";
            this.BtnUpload3.Size = new System.Drawing.Size(24, 21);
            this.BtnUpload3.TabIndex = 44;
            this.BtnUpload3.ToolTip = "Browse File";
            this.BtnUpload3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnUpload3.ToolTipTitle = "Run System";
            this.BtnUpload3.Click += new System.EventHandler(this.BtnUpload3_Click);
            // 
            // ChkFile4
            // 
            this.ChkFile4.Location = new System.Drawing.Point(282, 109);
            this.ChkFile4.Name = "ChkFile4";
            this.ChkFile4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkFile4.Properties.Appearance.Options.UseFont = true;
            this.ChkFile4.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkFile4.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkFile4.Properties.Caption = " ";
            this.ChkFile4.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkFile4.Size = new System.Drawing.Size(20, 22);
            this.ChkFile4.TabIndex = 49;
            this.ChkFile4.ToolTip = "Remove filter";
            this.ChkFile4.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkFile4.ToolTipTitle = "Run System";
            this.ChkFile4.CheckedChanged += new System.EventHandler(this.ChkFile4_CheckedChanged);
            // 
            // PbUpload4
            // 
            this.PbUpload4.Location = new System.Drawing.Point(46, 132);
            this.PbUpload4.Name = "PbUpload4";
            this.PbUpload4.Size = new System.Drawing.Size(320, 10);
            this.PbUpload4.TabIndex = 52;
            // 
            // TxtFile4
            // 
            this.TxtFile4.EnterMoveNextControl = true;
            this.TxtFile4.Location = new System.Drawing.Point(46, 110);
            this.TxtFile4.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFile4.Name = "TxtFile4";
            this.TxtFile4.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFile4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFile4.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFile4.Properties.Appearance.Options.UseFont = true;
            this.TxtFile4.Properties.MaxLength = 16;
            this.TxtFile4.Size = new System.Drawing.Size(234, 20);
            this.TxtFile4.TabIndex = 48;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(6, 114);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(35, 14);
            this.label12.TabIndex = 47;
            this.label12.Text = "File 4";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkFile3
            // 
            this.ChkFile3.Location = new System.Drawing.Point(283, 74);
            this.ChkFile3.Name = "ChkFile3";
            this.ChkFile3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkFile3.Properties.Appearance.Options.UseFont = true;
            this.ChkFile3.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkFile3.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkFile3.Properties.Caption = " ";
            this.ChkFile3.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkFile3.Size = new System.Drawing.Size(20, 22);
            this.ChkFile3.TabIndex = 43;
            this.ChkFile3.ToolTip = "Remove filter";
            this.ChkFile3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkFile3.ToolTipTitle = "Run System";
            this.ChkFile3.CheckedChanged += new System.EventHandler(this.ChkFile3_CheckedChanged);
            // 
            // PbUpload3
            // 
            this.PbUpload3.Location = new System.Drawing.Point(46, 97);
            this.PbUpload3.Name = "PbUpload3";
            this.PbUpload3.Size = new System.Drawing.Size(320, 10);
            this.PbUpload3.TabIndex = 46;
            // 
            // TxtFile3
            // 
            this.TxtFile3.EnterMoveNextControl = true;
            this.TxtFile3.Location = new System.Drawing.Point(46, 75);
            this.TxtFile3.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFile3.Name = "TxtFile3";
            this.TxtFile3.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFile3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFile3.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFile3.Properties.Appearance.Options.UseFont = true;
            this.TxtFile3.Properties.MaxLength = 16;
            this.TxtFile3.Size = new System.Drawing.Size(234, 20);
            this.TxtFile3.TabIndex = 42;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(6, 79);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(35, 14);
            this.label13.TabIndex = 41;
            this.label13.Text = "File 3";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnDownload2
            // 
            this.BtnDownload2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDownload2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDownload2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDownload2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDownload2.Appearance.Options.UseBackColor = true;
            this.BtnDownload2.Appearance.Options.UseFont = true;
            this.BtnDownload2.Appearance.Options.UseForeColor = true;
            this.BtnDownload2.Appearance.Options.UseTextOptions = true;
            this.BtnDownload2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDownload2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDownload2.Image = ((System.Drawing.Image)(resources.GetObject("BtnDownload2.Image")));
            this.BtnDownload2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDownload2.Location = new System.Drawing.Point(337, 40);
            this.BtnDownload2.Name = "BtnDownload2";
            this.BtnDownload2.Size = new System.Drawing.Size(24, 21);
            this.BtnDownload2.TabIndex = 39;
            this.BtnDownload2.ToolTip = "Download File";
            this.BtnDownload2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDownload2.ToolTipTitle = "Run System";
            this.BtnDownload2.Click += new System.EventHandler(this.BtnDownload2_Click);
            // 
            // BtnUpload2
            // 
            this.BtnUpload2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnUpload2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnUpload2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnUpload2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnUpload2.Appearance.Options.UseBackColor = true;
            this.BtnUpload2.Appearance.Options.UseFont = true;
            this.BtnUpload2.Appearance.Options.UseForeColor = true;
            this.BtnUpload2.Appearance.Options.UseTextOptions = true;
            this.BtnUpload2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnUpload2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnUpload2.Image = ((System.Drawing.Image)(resources.GetObject("BtnUpload2.Image")));
            this.BtnUpload2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnUpload2.Location = new System.Drawing.Point(310, 39);
            this.BtnUpload2.Name = "BtnUpload2";
            this.BtnUpload2.Size = new System.Drawing.Size(24, 21);
            this.BtnUpload2.TabIndex = 38;
            this.BtnUpload2.ToolTip = "Browse File";
            this.BtnUpload2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnUpload2.ToolTipTitle = "Run System";
            this.BtnUpload2.Click += new System.EventHandler(this.BtnUpload2_Click);
            // 
            // BtnDownload1
            // 
            this.BtnDownload1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDownload1.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDownload1.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDownload1.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDownload1.Appearance.Options.UseBackColor = true;
            this.BtnDownload1.Appearance.Options.UseFont = true;
            this.BtnDownload1.Appearance.Options.UseForeColor = true;
            this.BtnDownload1.Appearance.Options.UseTextOptions = true;
            this.BtnDownload1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDownload1.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDownload1.Image = ((System.Drawing.Image)(resources.GetObject("BtnDownload1.Image")));
            this.BtnDownload1.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDownload1.Location = new System.Drawing.Point(337, 4);
            this.BtnDownload1.Name = "BtnDownload1";
            this.BtnDownload1.Size = new System.Drawing.Size(24, 21);
            this.BtnDownload1.TabIndex = 33;
            this.BtnDownload1.ToolTip = "Download File";
            this.BtnDownload1.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDownload1.ToolTipTitle = "Run System";
            this.BtnDownload1.Click += new System.EventHandler(this.BtnDownload1_Click);
            // 
            // BtnUpload1
            // 
            this.BtnUpload1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnUpload1.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnUpload1.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnUpload1.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnUpload1.Appearance.Options.UseBackColor = true;
            this.BtnUpload1.Appearance.Options.UseFont = true;
            this.BtnUpload1.Appearance.Options.UseForeColor = true;
            this.BtnUpload1.Appearance.Options.UseTextOptions = true;
            this.BtnUpload1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnUpload1.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnUpload1.Image = ((System.Drawing.Image)(resources.GetObject("BtnUpload1.Image")));
            this.BtnUpload1.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnUpload1.Location = new System.Drawing.Point(310, 5);
            this.BtnUpload1.Name = "BtnUpload1";
            this.BtnUpload1.Size = new System.Drawing.Size(24, 21);
            this.BtnUpload1.TabIndex = 32;
            this.BtnUpload1.ToolTip = "Browse File";
            this.BtnUpload1.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnUpload1.ToolTipTitle = "Run System";
            this.BtnUpload1.Click += new System.EventHandler(this.BtnUpload1_Click);
            // 
            // ChkFile2
            // 
            this.ChkFile2.Location = new System.Drawing.Point(282, 40);
            this.ChkFile2.Name = "ChkFile2";
            this.ChkFile2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkFile2.Properties.Appearance.Options.UseFont = true;
            this.ChkFile2.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkFile2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkFile2.Properties.Caption = " ";
            this.ChkFile2.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkFile2.Size = new System.Drawing.Size(20, 22);
            this.ChkFile2.TabIndex = 37;
            this.ChkFile2.ToolTip = "Remove filter";
            this.ChkFile2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkFile2.ToolTipTitle = "Run System";
            this.ChkFile2.CheckedChanged += new System.EventHandler(this.ChkFile2_CheckedChanged);
            // 
            // PbUpload2
            // 
            this.PbUpload2.Location = new System.Drawing.Point(46, 63);
            this.PbUpload2.Name = "PbUpload2";
            this.PbUpload2.Size = new System.Drawing.Size(320, 10);
            this.PbUpload2.TabIndex = 40;
            // 
            // TxtFile2
            // 
            this.TxtFile2.EnterMoveNextControl = true;
            this.TxtFile2.Location = new System.Drawing.Point(46, 41);
            this.TxtFile2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFile2.Name = "TxtFile2";
            this.TxtFile2.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFile2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFile2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFile2.Properties.Appearance.Options.UseFont = true;
            this.TxtFile2.Properties.MaxLength = 16;
            this.TxtFile2.Size = new System.Drawing.Size(234, 20);
            this.TxtFile2.TabIndex = 36;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(6, 45);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(35, 14);
            this.label10.TabIndex = 35;
            this.label10.Text = "File 2";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkFile1
            // 
            this.ChkFile1.Location = new System.Drawing.Point(283, 5);
            this.ChkFile1.Name = "ChkFile1";
            this.ChkFile1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkFile1.Properties.Appearance.Options.UseFont = true;
            this.ChkFile1.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkFile1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkFile1.Properties.Caption = " ";
            this.ChkFile1.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkFile1.Size = new System.Drawing.Size(20, 22);
            this.ChkFile1.TabIndex = 31;
            this.ChkFile1.ToolTip = "Remove filter";
            this.ChkFile1.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkFile1.ToolTipTitle = "Run System";
            this.ChkFile1.CheckedChanged += new System.EventHandler(this.ChkFile1_CheckedChanged);
            // 
            // PbUpload1
            // 
            this.PbUpload1.Location = new System.Drawing.Point(46, 28);
            this.PbUpload1.Name = "PbUpload1";
            this.PbUpload1.Size = new System.Drawing.Size(320, 10);
            this.PbUpload1.TabIndex = 34;
            // 
            // TxtFile1
            // 
            this.TxtFile1.EnterMoveNextControl = true;
            this.TxtFile1.Location = new System.Drawing.Point(46, 6);
            this.TxtFile1.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFile1.Name = "TxtFile1";
            this.TxtFile1.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFile1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFile1.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFile1.Properties.Appearance.Options.UseFont = true;
            this.TxtFile1.Properties.MaxLength = 16;
            this.TxtFile1.Size = new System.Drawing.Size(234, 20);
            this.TxtFile1.TabIndex = 30;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(6, 10);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(35, 14);
            this.label11.TabIndex = 29;
            this.label11.Text = "File 1";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // OD1
            // 
            this.OD1.FileName = "NamaFile";
            // 
            // FrmQt
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(903, 473);
            this.Name = "FrmQt";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePtCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueVdCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDTCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteExpDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteExpDt.Properties)).EndInit();
            this.PnlUpload.ResumeLayout(false);
            this.PnlUpload.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile1.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label6;
        private DevExpress.XtraEditors.LookUpEdit LueCurCode;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Label label1;
        internal DevExpress.XtraEditors.LookUpEdit LuePtCode;
        internal DevExpress.XtraEditors.LookUpEdit LueVdCode;
        private System.Windows.Forms.Label label7;
        private DevExpress.XtraEditors.LookUpEdit LueDTCode;
        internal DevExpress.XtraEditors.TextEdit TxtStatus;
        private System.Windows.Forms.Label label8;
        internal DevExpress.XtraEditors.DateEdit DteExpDt;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel PnlUpload;
        private DevExpress.XtraEditors.CheckEdit ChkFile2;
        private System.Windows.Forms.ProgressBar PbUpload2;
        internal DevExpress.XtraEditors.TextEdit TxtFile2;
        private System.Windows.Forms.Label label10;
        private DevExpress.XtraEditors.CheckEdit ChkFile1;
        private System.Windows.Forms.ProgressBar PbUpload1;
        internal DevExpress.XtraEditors.TextEdit TxtFile1;
        private System.Windows.Forms.Label label11;
        public DevExpress.XtraEditors.SimpleButton BtnDownload2;
        public DevExpress.XtraEditors.SimpleButton BtnUpload2;
        public DevExpress.XtraEditors.SimpleButton BtnDownload1;
        public DevExpress.XtraEditors.SimpleButton BtnUpload1;
        private System.Windows.Forms.OpenFileDialog OD1;
        private System.Windows.Forms.SaveFileDialog SFD1;
        public DevExpress.XtraEditors.SimpleButton BtnDownload5;
        public DevExpress.XtraEditors.SimpleButton BtnUpload5;
        private DevExpress.XtraEditors.CheckEdit ChkFile5;
        private System.Windows.Forms.ProgressBar PbUpload5;
        internal DevExpress.XtraEditors.TextEdit TxtFile5;
        private System.Windows.Forms.Label label14;
        public DevExpress.XtraEditors.SimpleButton BtnDownload4;
        public DevExpress.XtraEditors.SimpleButton BtnUpload4;
        public DevExpress.XtraEditors.SimpleButton BtnDownload3;
        public DevExpress.XtraEditors.SimpleButton BtnUpload3;
        private DevExpress.XtraEditors.CheckEdit ChkFile4;
        private System.Windows.Forms.ProgressBar PbUpload4;
        internal DevExpress.XtraEditors.TextEdit TxtFile4;
        private System.Windows.Forms.Label label12;
        private DevExpress.XtraEditors.CheckEdit ChkFile3;
        private System.Windows.Forms.ProgressBar PbUpload3;
        internal DevExpress.XtraEditors.TextEdit TxtFile3;
        private System.Windows.Forms.Label label13;
    }
}