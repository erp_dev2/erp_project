﻿#region Update
/*
    24/02/2020 [WED/SIER] new apps
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmDODept2Dlg4 : RunSystem.FrmBase4
    {
        #region Field

        private FrmDODept2 mFrmParent;
        private int mRow = 0;
        private string mCCCode = string.Empty;

        #endregion

        #region Constructor

        public FrmDODept2Dlg4(FrmDODept2 FrmParent, int Row, string CCCode)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mRow = Row;
            mCCCode = CCCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 8;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Cost Category's"+Environment.NewLine+"Code", 
                    "Cost Category's"+Environment.NewLine+"Name",
                    "Cost Center",
                    "COA Account#",
                    "COA Account"+Environment.NewLine+"Description",

                    //6-7
                    "Group",
                    "Entity",
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    100, 250, 200, 150, 250, 
                    
                    //6-7
                    150, 180
                }
            );
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7 });
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();
                var Filter = " And 0 = 0 ";
                var SQL = new StringBuilder();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@CCCode", mCCCode);
                Sm.FilterStr(ref Filter, ref cm, TxtCCtCode.Text, new string[] { "A.CCtCode", "A.CCtName" });

                SQL.AppendLine("Select A.CCtCode, A.CCtName, C.CCName, A.AcNo, B.AcDesc, ");
                SQL.AppendLine("D.OptDesc, E.EntName, ");
                SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
                SQL.AppendLine("From TblCostCategory A  ");
                SQL.AppendLine("Left Join TblCOA B on A.AcNo=B.AcNo ");
                if (mFrmParent.mIsCostCategoryFilterByEntity)
                {
                    SQL.AppendLine("And B.EntCode is Not Null ");
                    SQL.AppendLine("And B.EntCode In ( ");
                    SQL.AppendLine("    Select T1.EntCode From TblProfitCenter T1, TblSite T2 ");
                    SQL.AppendLine("    Where T1.ProfitCenterCode=T2.ProfitCenterCode ");
                    SQL.AppendLine("    And T1.EntCode Is Not Null ");
                    SQL.AppendLine("    And T2.ProfitCenterCode Is Not Null ");
                    SQL.AppendLine("    And T2.SiteCode In (");
                    SQL.AppendLine("        Select SiteCode From TblGroupSite ");
                    SQL.AppendLine("        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                    SQL.AppendLine("        ) ");
                    SQL.AppendLine("    ) ");
                }
                SQL.AppendLine("Left Join TblCostCenter C On A.CCCode = C.CCCode ");
                SQL.AppendLine("Left Join TblOption D On A.CCGrpCode = D.OptCode And D.OptCat='CostCenterGroup' ");
                SQL.AppendLine("Left Join TblEntity E On B.EntCode=E.EntCode ");
                SQL.AppendLine("Where A.CCCode = @CCCode ");
                SQL.AppendLine(Filter);
                SQL.AppendLine(" Order By C.CCName, A.CCtName;");

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, SQL.ToString(),
                    new string[]
                    {
                        //0
                        "CCtCode",

                        //1-5
                        "CCtName",
                        "CCName",
                        "AcNo",
                        "AcDesc",
                        "OptDesc",

                        //6
                        "EntName",
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                //mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                mFrmParent.Grd1.Cells[mRow, 35].Value = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
                mFrmParent.Grd1.Cells[mRow, 28].Value = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 2);
                mFrmParent.Grd1.Cells[mRow, 34].Value = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 4);
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            Grd1.Rows.AutoHeight();
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        protected override void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            ChooseData();
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtCCtCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkCCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Cost Category");
        }

        #endregion

        #endregion

    }
}
