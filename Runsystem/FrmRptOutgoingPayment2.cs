﻿#region Update
/*
    07/11/2017 [TKG] tambah vendor category, vendor's invoice
    21/05/2018 [HAR] Bug double bank
    19/07/2018 [HAR] tambah informasi giro, openingdt ambil dr voucher, bank account, PI docno, PI docdt
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptOutgoingPayment2 : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmRptOutgoingPayment2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -30);
                SetGrd();
                SetSQL();
                Sl.SetLueVdCode(ref LueVdCode);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelInd, ");
            SQL.AppendLine("Case A.Status When 'O' Then 'Outstanding' When 'C' Then 'Cancelled' When 'A' Then 'Approved' End As StatusDesc, ");
            SQL.AppendLine("B.VdName, G.VdCtName, H.VdInv, C.OptDesc As PaymentTypeDesc, D.BankName, Concat(I.bankAcnm, ' ', I.BankAcNo) AcNm, J.GiroNo, J.OpeningDt, J.DueDt, ");
            SQL.AppendLine("E.BankName As PaidToBankName, A.PaidToBankBranch, A.PaidToBankAcName, A.PaidToBankAcNo, ");
            SQL.AppendLine("A.CurCode, A.Amt, A.Remark, K.PIdocnO, K.PIDocDt,  A.VoucherRequestDocNo, F.VoucherDocNo, J.DocDt VoucherDt ");
            SQL.AppendLine("From TblOutgoingPaymentHdr A ");
            SQL.AppendLine("Inner Join TblVendor B On A.VdCode=B.VdCode ");
            SQL.AppendLine("Left Join TblOption C On A.PaymentType=C.OptCode And C.OptCat='VoucherPaymentType' ");
            SQL.AppendLine("Left Join TblBank D On A.BankCode=D.BankCode ");
            SQL.AppendLine("Left Join TblBank E On A.PaidToBankCode=E.BankCode ");
            SQL.AppendLine("Left Join TblVoucherRequestHdr F On A.VoucherRequestDocNo=F.DocNo ");
            SQL.AppendLine("Left Join TblVendorCategory G On B.VdCtCode=G.VdCtCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T1.DocNo, ");
            SQL.AppendLine("    Group_Concat(Distinct Concat(T3.VdInvNo, ");
            SQL.AppendLine("    Case When T3.VdInvDt Is Not Null Then Concat(' (', Date_Format(T3.VdInvDt, '%d/%m/%Y'), ') ') Else '' End) Order By T3.VdInvDt Asc Separator ', ') As VdInv ");
            SQL.AppendLine("    From TblOutgoingPaymentHdr T1 ");
            SQL.AppendLine("    Inner Join TblOutgoingPaymentDtl T2 On T1.DocNo=T2.DocNo And T2.InvoiceType='1' ");
            SQL.AppendLine("    Inner Join TblPurchaseInvoiceHdr T3 On T2.InvoiceDocNo=T3.DocNo And T3.VdInvNo Is Not Null ");
            SQL.AppendLine("    Where T1.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    Group By T1.DocNo ");
            SQL.AppendLine(") H On A.DocNo=H.DocNo ");
            SQL.AppendLine("Left Join TblbankAccount I On A.bankAcCode = I.BankAcCode ");
            SQL.AppendLine("left Join TblVoucherhdr J On F.VoucherDocNo = J.DocNo ");
            SQL.AppendLine("Inner Join  ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select A.DocNo, Group_Concat(A.invoiceDocNo) PIDocNo, Group_concat(Date_Format(B.DocDt,'%d/%b/%Y')) As PIDocDt  ");
	        SQL.AppendLine("    From TblOutgoingPaymentDtl A ");
	        SQL.AppendLine("    Inner Join TblPurchaseInvoiceHdr B On A.InvoiceDocNo = B.DocNo ");
            SQL.AppendLine("    Where B.DocDt Between @DocDt1 And @DocDt2 ");
	        SQL.AppendLine("    Group By A.DocNo  ");
            SQL.AppendLine(")K On A.DocNo = K.DocNo  ");
            SQL.AppendLine("Where (A.DocDt Between @DocDt1 And @DocDt2) ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 26;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#",
                        "Date",   
                        "Cancel", 
                        "Status",
                        "Vendor", 

                        //6-10
                        "Vendor's Category",
                        "Vendor's Invoice",
                        "Payment Type",
                        "Bank",
                        "Bank Account Name",
                        //11-15
                        "Cheque/Giro",
                        "Opening"+Environment.NewLine+"Date",
                        "Due Date", 
                        "Paid To"+Environment.NewLine+"(Bank's Name)",                     
                        "Paid To"+Environment.NewLine+"(Bank's Branch)", 
                        //16-20
                        "Paid To"+Environment.NewLine+"(Account's Name)",
                        "Paid To"+Environment.NewLine+"(Account#)", 
                        "Currency",
                        "Amount",
                        "Remark",
                        //21-25
                        "Purchase Invoice#",
                        "Purchase Invoice "+Environment.NewLine+"Date",
                        "Voucher Request#",
                        "Voucher#",
                        "Voucher Date"
                    },
                    new int[] 
                    {
                        //0
                        50,
                        //1-5
                        140, 90, 80, 120, 200,
                        //6-10
                        200, 200, 150, 180, 180, 
                        //11-15
                        120, 90, 90, 150, 150,  
                        //16-20
                        150, 150, 100, 120, 250,  
                        //21
                        150, 120, 120, 120, 120
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 2, 12, 13, 25 });
            Sm.GrdColCheck(Grd1, new int[] { 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 19 }, 0);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueVdCode), "B.VdCode", true);

                Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                mSQL + Filter + " Order By A.DocNo;",
                new string[]
                    {
                        //0
                        "DocNo", 
                        //1-5
                        "DocDt", "CancelInd", "StatusDesc", "VdName", "VdCtName", 
                        //6-10
                        "VdInv", "PaymentTypeDesc", "BankName", "Acnm", "GiroNo",  
                        //11-15
                        "OpeningDt", "DueDt", "PaidToBankName", "PaidToBankBranch", "PaidToBankAcName",  
                        //16-20
                        "PaidToBankAcNo", "CurCode", "Amt", "Remark", "PIDOcnO", 
                        //21-24
                        "PIDocDt", "VoucherRequestDocNo", "VoucherDocNo", "VoucherDt"
                    },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Grd.Cells[Row, 0].Value = Row + 1;
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 12, 11);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 13, 12);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 14);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 15);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 16);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 17);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 18);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 19);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 20);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 21);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 22);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 23); 
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 25, 24);
                }, true, false, false, false
                );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 19 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #endregion

        #region Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue1(Sl.SetLueVdCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkVdCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Vendor");
        }
        #endregion
    }
}
