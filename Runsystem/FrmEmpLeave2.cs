﻿#region Update
/*
    31/07/2017 [HAR] function ComputeLeaveRemainingDay tambah validasi untuk yang long services
    01/08/2017 [HAR] function IsLeavePeriodNotValid tambah validasi untuk yang long services
    03/08/2017 [TKG] tambah resign date, Karyawan yg tgl resignnya lebih lama dari tgl skrg tetap bisa minta leave
    16/08/2017 [TKG] bug fixing saat issue annual leave
    23/08/2017 [HAR] bug fixing jumlah cuti besar tidak reload
    05/09/2017 [TKG] menggunakan validasi Employee's Leave Start Date untuk cuti tahunan/besar.
    28/10/2017 [TKG] perhitungan cuti besar utk HIN
    15/11/2017 [TKG] bug fixing perhitungan cuti tahunan
    19/12/2017 [TKG] tambah filter dept+site berdasarkan group
    09/07/2018 [WED] ellips button di grid, diganti ke dialog
    20/07/2017 [TKG] Tambah validasi hari libur (ambil Working Schedule) di duration (day)
    17/09/2020 [WED/SRN] approval berdasarkan leave code yang ada di DocApprovalSetting, berdasarkan parameter IsDocApprovalSettingUseLeaveCode
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmEmpLeave2 : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mDocNo = string.Empty;
        internal FrmEmpLeave2Find FrmFind;
        private string
            mLongServiceLeaveCode = string.Empty,
            mLongServiceLeaveCode2 = string.Empty,
            mNeglectLeaveCode = string.Empty;
        internal string
            mAnnualLeaveCode = string.Empty,
            mTrainingPosCode = string.Empty;
        internal bool 
            mIsSiteMandatory = false, 
            mIsApprovalBySiteMandatory = false, 
            mIsFilterByDeptHR = false,
            mIsFilterBySiteHR = false;
        private bool mIsDocApprovalSettingUseLeaveCode = false;

        #endregion

        #region Constructor

        public FrmEmpLeave2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Group of Employee's Leave Request";
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                if (mIsSiteMandatory)
                    LblSiteCode.ForeColor = Color.Red;
                Sl.SetLueOption(ref LueLeaveType, "LeaveType");
                Sl.SetLueAGCode(ref LueAGCode);
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mIsFilterByDeptHR ? "Y" : "N");
                Sl.SetLueSiteCode(ref LueSiteCode, string.Empty, mIsFilterBySiteHR ? "Y" : "N");
                base.FrmLoad(sender, e);
                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grid 1

            Grd1.Cols.Count = 13;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "No.", 
                        
                        //1-5
                        "",
                        "Employee's"+Environment.NewLine+"Code",
                        "Employee's Name",
                        "Old Code",
                        "Position", 

                        //6-10
                        "Permanent"+Environment.NewLine+"Date", 
                        "Number of"+Environment.NewLine+"leave (day)",
                        "Duration" + Environment.NewLine + "(Hour)",
                        "Include"+Environment.NewLine+"Break Time",
                        "Break Schedule",

                        //11-12
                        "Resign Date",
                        "Number of"+Environment.NewLine+"Holiday"
                    },
                     new int[] 
                    {
                        //0
                        40, 

                        //1-5
                        20, 80, 200, 100, 200, 
                        
                        //6-10
                        100, 100, 70, 80, 150,

                        //11-12
                        80, 100
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 7, 8, 12 }, 0);
            Sm.GrdColCheck(Grd1, new int[] { 9 });
            Sm.GrdColButton(Grd1, new int[] { 1 });
            Sm.GrdFormatDate(Grd1, new int[] { 6, 11 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 3, 4, 5, 6, 7, 10, 11, 12 });
            Grd1.Cols[11].Move(7);

            #endregion

            #region Grid 2

            Grd2.Cols.Count = 4;
            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[] { "Checked By", "Status", "Date", "Remark" },
                    new int[] { 200, 80, 80, 300 }
                );
            Sm.GrdFormatDate(Grd2, new int[] { 2 });
            Sm.GrdColInvisible(Grd2, new int[] { 3 }, false);

            #endregion
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd2, new int[] { 3 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, ChkCancelInd, LueDeptCode, LueAGCode, LueSiteCode, 
                        LueLeaveCode, ChkCompulsoryLeaveInd, LueLeaveType, DteStartDt, DteEndDt, 
                        TmeStartTm, TmeEndTm, MeeRemark
                    }, true);
                    BtnDurationHrDefault.Enabled = false;
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 8, 9 });
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, ChkCancelInd, LueDeptCode, LueAGCode, LueSiteCode,
                        LueLeaveCode, ChkCompulsoryLeaveInd, LueLeaveType, DteStartDt, MeeRemark
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 8, 9 });
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    ChkCancelInd.Properties.ReadOnly = false;
                    ChkCancelInd.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, TxtStatus, LueLeaveCode, LueDeptCode,
                LueAGCode, LueSiteCode, LueLeaveType, DteStartDt, DteEndDt, 
                TmeStartTm, TmeEndTm, MeeRemark
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtDurationDay }, 0);
            ChkCancelInd.Checked = false;
            ChkCompulsoryLeaveInd.Checked = false;
            ClearGrd();
        }

        private void ClearGrd()
        {
            ClearGrd1();
            Sm.ClearGrd(Grd2, true);
            Sm.FocusGrd(Grd2, 0, 0);
        }

        private void ClearGrd1()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdBoolValueFalse(ref Grd1, 0, new int[] { 9 });
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 7, 8, 12 });
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmEmpLeave2Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                SetLueLeaveCode(ref LueLeaveCode, string.Empty);
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mIsFilterByDeptHR ? "Y" : "N");
                Sl.SetLueSiteCode(ref LueSiteCode, string.Empty, mIsFilterBySiteHR ? "Y" : "N");
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> { DteEndDt, TmeStartTm, TmeEndTm }, true);
                Sm.GrdColReadOnly(true, true, Grd1, new int[] { 8, 9 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, string.Empty, false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            //try
            //{
            if (TxtDocNo.Text.Length == 0)
                InsertData(sender, e);
            else
                EditData();
            //}
            //catch (Exception Exc)
            //{
            //    Sm.ShowErrorMsg(Exc);
            //}
            //finally
            //{
            //    Cursor.Current = Cursors.Default;
            //}
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            #region Old Code
            //if (BtnSave.Enabled && TxtDocNo.Text.Length == 0)
            //{

            //}
            //if (e.ColIndex == 1 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            //{
            //    var f1 = new FrmEmployee(mMenuCode);
            //    f1.Tag = mMenuCode;
            //    f1.WindowState = FormWindowState.Normal;
            //    f1.StartPosition = FormStartPosition.CenterScreen;
            //    f1.mEmpCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
            //    f1.ShowDialog();
            //}
            #endregion

            if(e.ColIndex == 1 && 
                !Sm.IsLueEmpty(LueDeptCode, "Department") &&
                !Sm.IsLueEmpty(LueLeaveCode, "Leave") &&
                !Sm.IsLueEmpty(LueLeaveType, "Type") &&
                !Sm.IsDteEmpty(DteStartDt, "Start Date") &&
                !Sm.IsDteEmpty(DteEndDt, "End Date")
               )
            {
                Sm.FormShowDialog(new FrmEmpLeave2Dlg(this, 
                    Sm.GetLue(LueDeptCode), 
                    Sm.GetLue(LueSiteCode),
                    Sm.GetLue(LueLeaveCode), 
                    Sm.GetLue(LueLeaveType), 
                    Sm.GetDte(DteStartDt), 
                    Sm.GetDte(DteEndDt))
                    );
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0)
            {
                Sm.GrdRemoveRow(Grd1, e, BtnSave);
                for(int r=0;r<Grd1.Rows.Count-1;r++)
                    Grd1.Cells[r, 0].Value = r + 1;
            }
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            #region Old Code
            //if (e.ColIndex == 1 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            //{
            //    var f1 = new FrmEmployee(mMenuCode);
            //    f1.Tag = mMenuCode;
            //    f1.WindowState = FormWindowState.Normal;
            //    f1.StartPosition = FormStartPosition.CenterScreen;
            //    f1.mEmpCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
            //    f1.ShowDialog();
            //}
            #endregion

            if (e.ColIndex == 1 &&
                !Sm.IsLueEmpty(LueDeptCode, "Department") &&
                !Sm.IsLueEmpty(LueLeaveCode, "Leave") &&
                !Sm.IsLueEmpty(LueLeaveType, "Type") &&
                !Sm.IsDteEmpty(DteStartDt, "Start Date") &&
                !Sm.IsDteEmpty(DteEndDt, "End Date")
               )
            {
                Sm.FormShowDialog(new FrmEmpLeave2Dlg(this,
                    Sm.GetLue(LueDeptCode),
                    Sm.GetLue(LueSiteCode),
                    Sm.GetLue(LueLeaveCode),
                    Sm.GetLue(LueLeaveType),
                    Sm.GetDte(DteStartDt),
                    Sm.GetDte(DteEndDt))
                    );
            }
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            try
            {
                Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 8 }, e);
                if (e.ColIndex == 9)
                {
                    Grd1.Cells[e.RowIndex, 10].Value = null;
                    if (Sm.GetGrdBool(Grd1, e.RowIndex, 9))
                    {
                        Grd1.Cells[e.RowIndex, 8].Value = Sm.GetGrdDec(Grd1, e.RowIndex, 8) - 1;
                        Grd1.Cells[e.RowIndex, 10].Value = GetBreakSchedule(e.RowIndex);
                    }
                    else
                    {
                        Grd1.Cells[e.RowIndex, 8].Value = Sm.GetGrdDec(Grd1, e.RowIndex, 8) + 1;
                    }
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || 
                IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "EmpLeave2", "TblEmpLeave2Hdr");

            var cml = new List<MySqlCommand>();
            var l = new List<EmpLeaveDtl>();

            SetEmpLeaveDtl(ref l);

            cml.Add(SaveEmpLeave2Hdr(DocNo));

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0) cml.Add(SaveEmpLeave2Dtl(DocNo, Row));

            l.ForEach(i => { cml.Add(SaveEmpLeave2Dtl2(DocNo, i)); });

            Sm.ExecCommands(cml);

            BtnInsertClick(sender, e);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueLeaveCode, "Leave name") ||
                Sm.IsLueEmpty(LueDeptCode, "Department") ||
                (mIsSiteMandatory && Sm.IsLueEmpty(LueSiteCode, "Site")) ||
                Sm.IsLueEmpty(LueLeaveType, "Leave type") ||
                Sm.IsDteEmpty(DteStartDt, "Leave's started date") ||
                (!DteEndDt.Properties.ReadOnly && Sm.IsDteEmpty(DteEndDt, "Leave's end date")) ||
                (!TmeStartTm.Properties.ReadOnly && Sm.IsTmeEmpty(TmeStartTm, "Leave's start time")) ||
                (!TmeEndTm.Properties.ReadOnly && Sm.IsTmeEmpty(TmeEndTm, "Leave's end time")) ||
                IsGrdEmpty() ||
                IsDurationHourEmpty() ||
                IsLeaveDateNotValid() ||
                IsLeavePeriodNotValid() ||
                IsAnnualAmtDifferent() ||
                IsCompulsoryLeaveNotValid() ||
                IsNeglectLeaveTypeNotValid() ||
                IsLeaveDateAlreadyUsed1() ||
                IsDtForResingeeInvalid() ||
                IsEmpHaveHoliday() ||
                IsNoOfHolidayInvalid();
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to process at least 1 employee.");
                return true;
            }
            return false;
        }

        private bool IsEmpHaveHoliday()
        {
            var EmpCode = string.Empty;
            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                EmpCode = Sm.GetGrdStr(Grd1, r, 2);
                if (EmpCode.Length > 0)
                {
                    if (IsEmpHaveHoliday(EmpCode, Sm.GetGrdStr(Grd1, r, 3), Sm.GetDte(DteStartDt), Sm.GetDte(DteEndDt)))
                    return true;
                }
            }
            return false;
        }

        private bool IsEmpHaveHoliday(string EmpCode, string EmpName, string Dt1, string Dt2)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.Dt ");
            SQL.AppendLine("From TblEmpWorkSchedule A ");
            SQL.AppendLine("Inner Join TblWorkSchedule B On A.WSCode=B.WSCode And B.HolidayInd='Y' ");
            SQL.AppendLine("Inner Join TblEmployee C On A.EmpCode=C.EmpCode ");
            SQL.AppendLine("Where A.Dt Between @Dt1 And @Dt2 ");
            SQL.AppendLine("And A.EmpCode=@EmpCode ");
            SQL.AppendLine("Limit 1;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);
            Sm.CmParamDt(ref cm, "@Dt1", Sm.GetDte(DteStartDt));
            Sm.CmParamDt(ref cm, "@Dt2", Sm.GetDte(DteEndDt));

            var Dt = Sm.GetValue(cm);

            if (Dt.Length>0)
            {
                if (Sm.StdMsgYN(
                    "Question",
                    "Employee's Code : " + EmpCode + Environment.NewLine +
                    "Employee's Name : " + EmpName + Environment.NewLine +
                    "has holiday schedule on " + DteStartDt.Text + " until " + DteEndDt.Text + "." + Environment.NewLine + Environment.NewLine +
                    "Do you want to continue saving this data ?"
                    )==DialogResult.No)
                return true;
            }
            return false;
        }

        private bool IsNoOfHolidayInvalid()
        {
            decimal NoOfHoliday = Sm.GetGrdDec(Grd1, 0, 12);
            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 2).Length > 0) 
                {
                    if (NoOfHoliday != Sm.GetGrdDec(Grd1, r, 12))
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "Employee's Code : " + Sm.GetGrdStr(Grd1, r, 2) + Environment.NewLine +
                            "Employee's Name : " + Sm.GetGrdStr(Grd1, r, 3) + Environment.NewLine +
                            "Invalid number of holiday.");
                        return true;
                    }
                    NoOfHoliday = Sm.GetGrdDec(Grd1, r, 12);
                }
            }
            return false;
        }

        private bool IsDtForResingeeInvalid()
        {
            string EmpCode = string.Empty;
            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                EmpCode = Sm.GetGrdStr(Grd1, r, 2);
                if (EmpCode.Length > 0)
                {
                    if (IsDocDtForResingeeInvalid(EmpCode, r)) return true;
                    if (IsLeaveStartDtForResingeeInvalid(EmpCode, r)) return true;
                    if (IsLeaveEndDtForResingeeInvalid(EmpCode, r)) return true;
                }
            }
            return false;
        }

        private bool IsDocDtForResingeeInvalid(string EmpCode, int r)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select 1 from TblEmployee " +
                    "Where EmpCode=@EmpCode And ResignDt Is Not Null And ResignDt<=@DocDt;"
            };

            Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Employee's Code : " + EmpCode + Environment.NewLine +
                    "Employee's Name : " + Sm.GetGrdStr(Grd1, r, 3) + Environment.NewLine +
                    "Resign Date : " + Sm.FormatDate2("dd/MMM/yyyy", Sm.GetGrdDate(Grd1, r, 11)) + Environment.NewLine +
                    "Document Date :" + DteDocDt.Text + Environment.NewLine + Environment.NewLine +
                    "Document date should be earlier than resign date.");
                return true;
            }
            return false;
        }

        private bool IsLeaveStartDtForResingeeInvalid(string EmpCode, int r)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select 1 from TblEmployee " +
                    "Where EmpCode=@EmpCode And ResignDt Is Not Null And ResignDt<=@StartDt;"
            };

            Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);
            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Employee's Code : " + EmpCode + Environment.NewLine +
                    "Employee's Name : " + Sm.GetGrdStr(Grd1, r, 3) + Environment.NewLine +
                    "Resign Date : " + Sm.FormatDate2("dd/MMM/yyyy", Sm.GetGrdDate(Grd1, r, 11)) + Environment.NewLine +
                    "Leave's Start Date :" + DteStartDt.Text + Environment.NewLine + Environment.NewLine +
                    "Leave's start date should be earlier than resign date.");
                return true;
            }
            return false;
        }

        private bool IsLeaveEndDtForResingeeInvalid(string EmpCode, int r)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select 1 from TblEmployee " +
                    "Where EmpCode=@EmpCode And ResignDt Is Not Null And ResignDt<=@EndDt;"
            };

            Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Employee's Code : " + EmpCode + Environment.NewLine +
                    "Employee's Name : " + Sm.GetGrdStr(Grd1, r, 3) + Environment.NewLine +
                    "Resign Date : " + Sm.FormatDate2("dd/MMM/yyyy", Sm.GetGrdDate(Grd1, r, 11)) + Environment.NewLine +
                    "Leave's End Date :" + DteEndDt.Text + Environment.NewLine + Environment.NewLine +
                    "Leave's end date should be earlier than resign date.");
                return true;
            }
            return false;
        }

        private bool IsNeglectLeaveTypeNotValid()
        {
            if (mNeglectLeaveCode.Length != 0 &&
                Sm.GetLue(LueLeaveCode) == mNeglectLeaveCode &&
                Sm.GetLue(LueLeaveType) != "F")
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Leave : " + LueLeaveCode.GetColumnValue("Col2") + Environment.NewLine +
                    "Invalid leave type." + Environment.NewLine +
                    "It should be full day leave."
                    );
                return true;
            }
            return false;
        }

        private bool IsLeaveDateAlreadyUsed1()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0)
                {
                    if (IsLeaveDateAlreadyUsed2(Sm.GetGrdStr(Grd1, Row, 2), Sm.GetGrdStr(Grd1, Row, 3)))
                        return true;
                }
            }
            return false;
        }

        private bool IsLeaveDateAlreadyUsed2(string EmpCode, string EmpName)
        {
            string
                StartDt = Sm.GetDte(DteStartDt).Substring(0, 8),
                EndDt = Sm.GetDte(DteEndDt).Substring(0, 8);

            if (StartDt.Length != 0 && EndDt.Length != 0)
            {
                //if (!DteEndDt.Properties.ReadOnly)
                //{
                string Dt = string.Empty;
                DateTime Dt1 = new DateTime(
                    Int32.Parse(StartDt.Substring(0, 4)),
                    Int32.Parse(StartDt.Substring(4, 2)),
                    Int32.Parse(StartDt.Substring(6, 2)),
                    0, 0, 0
                    );
                DateTime Dt2 = new DateTime(
                    Int32.Parse(EndDt.Substring(0, 4)),
                    Int32.Parse(EndDt.Substring(4, 2)),
                    Int32.Parse(EndDt.Substring(6, 2)),
                    0, 0, 0
                    );

                if (IsLeaveDateAlreadyUsed3(
                        Dt1.Year.ToString() +
                        ("00" + Dt1.Month.ToString()).Substring(("00" + Dt1.Month.ToString()).Length - 2, 2) +
                        ("00" + Dt1.Day.ToString()).Substring(("00" + Dt1.Day.ToString()).Length - 2, 2),
                        EmpCode, EmpName
                    ))
                    return true;

                var TotalDays = (Dt2 - Dt1).Days;
                for (int i = 1; i <= TotalDays; i++)
                {
                    Dt1 = Dt1.AddDays(1);

                    if (IsLeaveDateAlreadyUsed3(
                            Dt1.Year.ToString() +
                            ("00" + Dt1.Month.ToString()).Substring(("00" + Dt1.Month.ToString()).Length - 2, 2) +
                            ("00" + Dt1.Day.ToString()).Substring(("00" + Dt1.Day.ToString()).Length - 2, 2),
                            EmpCode, EmpName
                        ))
                        return true;
                }
                //}
            }
            return false;
        }

        private bool IsLeaveDateAlreadyUsed3(string Dt, string EmpCode, string EmpName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select EmpCode From (");
            SQL.AppendLine("    Select A.EmpCode ");
            SQL.AppendLine("    From TblEmpLeaveHdr A ");
            SQL.AppendLine("    Inner Join TblEmpLeaveDtl B On A.DocNo=B.DocNo And B.LeaveDt=@Dt ");
            SQL.AppendLine("    Where A.CancelInd='N' ");
            SQL.AppendLine("    And IfNull(A.Status, 'O')<>'C' ");
            SQL.AppendLine("    And A.EmpCode=@EmpCode ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select B.EmpCode ");
            SQL.AppendLine("    From TblEmpLeave2Hdr A ");
            SQL.AppendLine("    Inner Join TblEmpLeave2Dtl B On A.DocNo=B.DocNo And B.EmpCode=@EmpCode ");
            SQL.AppendLine("    Inner Join TblEmpLeave2Dtl2 C On A.DocNo=C.DocNo And C.LeaveDt=@Dt ");
            SQL.AppendLine("    Where A.CancelInd='N' ");
            SQL.AppendLine("    And IfNull(A.Status, 'O')<>'C' ");
            SQL.AppendLine(") T Limit 1;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);
            Sm.CmParamDt(ref cm, "@Dt", Dt);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Employee's Code : " + EmpCode + Environment.NewLine +
                    "Employee's Name : " + EmpName + Environment.NewLine +
                    "Date : " + Sm.Right(Dt, 2) + "/" + Dt.Substring(4, 2) + "/" + Sm.Left(Dt, 4) + Environment.NewLine +
                    "Invalid leave date.");
                return true;
            }
            return false;
        }

        private bool IsCompulsoryLeaveNotValid()
        {
            if (
                ChkCompulsoryLeaveInd.Checked &&
                mAnnualLeaveCode.Length > 0 &&
                !Sm.CompareStr(mAnnualLeaveCode, Sm.GetLue(LueLeaveCode))
                )
            {
                Sm.StdMsg(mMsgType.Warning, "Invalid compulsory leave.");
                return true;
            }
            return false;
        }

        private bool IsLeaveDateNotValid()
        {
            string
                StartDt = Sm.GetDte(DteStartDt).Substring(0, 8),
                EndDt = Sm.GetDte(DteEndDt).Substring(0, 8);

            if (StartDt.Length != 0 && EndDt.Length != 0 && decimal.Parse(StartDt) > decimal.Parse(EndDt))
            {
                Sm.StdMsg(mMsgType.Warning, "Invalid leave date.");
                return true;
            }
            return false;
        }

        private bool IsLeaveTimeNotValid()
        {
            string
                StartTm = Sm.GetDte(DteStartDt) + Sm.GetTme(TmeStartTm),
                EndTm = Sm.GetDte(DteStartDt) + Sm.GetTme(TmeEndTm);

            if (StartTm.Length != 0 && EndTm.Length != 0 && decimal.Parse(StartTm) > decimal.Parse(EndTm))
            {
                Sm.StdMsg(mMsgType.Warning, "Invalid leave time.");
                return true;
            }
            return false;
        }

        private bool IsAnnualAmtDifferent()
        {
            if (Sm.GetParameter("AnnualLeaveCode") == Sm.GetLue(LueLeaveCode))
            {
                var AmtLeave = decimal.Parse(TxtDurationDay.Text);
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (AmtLeave > Sm.GetGrdDec(Grd1, Row, 7))
                    {
                        Sm.StdMsg(mMsgType.Warning, "Employee(s) have different number of annual leave.");
                        return true;
                    }
                }
            }
            return false;
        }

        private bool IsDurationHourEmpty()
        {
            if (!TmeStartTm.Properties.ReadOnly)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.IsGrdValueEmpty(Grd1, Row, 8, true, "Duration (hour) should not be 0.")) return true;
            }
            return false;
        }

        private bool IsLeavePeriodNotValid()
        {
            var LeaveCode = Sm.GetLue(LueLeaveCode);

            if (mAnnualLeaveCode.Length == 0 && mLongServiceLeaveCode.Length == 0) return false;
            if (!(Sm.CompareStr(mAnnualLeaveCode, LeaveCode) || Sm.CompareStr(mLongServiceLeaveCode, LeaveCode)))
                return false;

            var l = new List<EmployeeLeave>();
            string
                Dt1 = Sm.GetDte(DteStartDt),
                Dt2 = Sm.GetDte(DteEndDt);

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 2).Length > 0)
                {
                    l.Add(new EmployeeLeave
                    {
                        EmpCode = Sm.GetGrdStr(Grd1, r, 2),
                        EmpName = Sm.GetGrdStr(Grd1, r, 3),
                        LeaveStartDt = Sm.Left(Dt1, 8),
                        LeaveEndDt = Sm.Left(Dt2, 8),
                        JoinDt = Sm.Left(Sm.GetGrdDate(Grd1, r, 6), 8),
                        Period = string.Empty,
                        IsValid = false
                    });
                }
            }

            if (l.Count <= 0) return false;

            var RLPType = Sm.GetParameter("RLPType");

            if (RLPType == "1")
            {
                if (Dt2.Length > 0 && !Sm.CompareStr(Sm.Left(Dt1, 4), Sm.Left(Dt2, 4)))
                {
                    Sm.StdMsg(mMsgType.Warning, "Invalid period.");
                    return true;
                }
                for (int i = 0; i < l.Count; i++)
                    l[i].Period = Sm.Left(l[i].LeaveStartDt, 4);
            }

            if (RLPType == "2")
            {
                if (Dt2.Length > 0)
                {
                    for (int i = 0; i < l.Count; i++)
                    {
                        if (Sm.CompareDtTm(Sm.Right(l[i].LeaveStartDt, 4), Sm.Right(l[i].JoinDt, 4)) < 0)
                            l[i].Period = (int.Parse(Sm.Left(l[i].LeaveStartDt, 4)) - 1).ToString();
                        else
                            l[i].Period = Sm.Left(l[i].LeaveStartDt, 4);
                        var v = (int.Parse(l[i].Period) + 1).ToString() + Sm.Right(l[i].JoinDt, 4);
                        if (Sm.CompareDtTm(l[i].LeaveEndDt, v) < 0)
                            l[i].IsValid = true;
                        else
                        {
                            Sm.StdMsg(mMsgType.Warning,
                                "Employee's Code : " + l[i].EmpCode + Environment.NewLine +
                                "Employee's Name : " + l[i].EmpName + Environment.NewLine + Environment.NewLine +
                                "Invalid period."
                                );
                            return true;
                        }
                    }
                }
            }

            string Period2 = Sm.Left(Dt1, 4);
            string Period1 = (int.Parse(Period2) - 1).ToString();

            var l2 = new List<EmployeeResidualLeave>();
            for (int i = 0; i < l.Count; i++)
            {
                if (l[i].Period == Period2)
                {
                    l2.Add(new EmployeeResidualLeave
                    {
                        EmpCode = l[i].EmpCode,
                        EmpName = l[i].EmpName,
                    });
                }
            }
            if (Sm.CompareStr(mAnnualLeaveCode, LeaveCode))
            {
                Sm.GetEmpAnnualLeave(ref l2, Period2);
            }

            if (Sm.CompareStr(mLongServiceLeaveCode, LeaveCode))
            {
                Sm.GetEmpLongServiceLeave(ref l2, Period2);
            }

            var l3 = new List<EmployeeResidualLeave>();
            for (int i = 0; i < l.Count; i++)
            {
                if (l[i].Period == Period1)
                {
                    l3.Add(new EmployeeResidualLeave
                    {
                        EmpCode = l[i].EmpCode,
                        EmpName = l[i].EmpName,
                    });
                }
            }
            if (Sm.CompareStr(mAnnualLeaveCode, LeaveCode))
            {
                Sm.GetEmpAnnualLeave(ref l3, Period1);
            }

            if (Sm.CompareStr(mLongServiceLeaveCode, LeaveCode))
            {
                Sm.GetEmpLongServiceLeave(ref l3, Period1);
            }

            var LeaveDuration = decimal.Parse(TxtDurationDay.Text);

            if (l2.Count > 0)
            {
                for (int i = 0; i < l2.Count; i++)
                {
                    if (l2[i].RemainingDay < LeaveDuration)
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "Employee's Code : " + l2[i].EmpCode + Environment.NewLine +
                            "Employee's Name : " + l2[i].EmpName + Environment.NewLine +
                            "Leave Duration : " + TxtDurationDay.Text + Environment.NewLine +
                            "Number of Remaining Day : " + Sm.FormatNum(l2[i].RemainingDay, 0) + Environment.NewLine + Environment.NewLine +
                            "Invalid leave's duration."
                            );
                        return true;
                    }
                }
            }

            if (l3.Count > 0)
            {
                for (int i = 0; i < l3.Count; i++)
                {
                    if (l3[i].RemainingDay < LeaveDuration)
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "Employee's Code : " + l3[i].EmpCode + Environment.NewLine +
                            "Employee's Name : " + l3[i].EmpName + Environment.NewLine +
                            "Leave Duration : " + TxtDurationDay.Text + Environment.NewLine +
                            "Number of Remaining Day : " + Sm.FormatNum(l3[i].RemainingDay, 0) + Environment.NewLine + Environment.NewLine +
                            "Invalid leave's duration."
                            );
                        return true;
                    }
                }
            }
            return false;
        }

        private MySqlCommand SaveEmpLeave2Hdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblEmpLeave2Hdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, Status, DeptCode, AGCode, SiteCode, LeaveCode, CompulsoryLeaveInd, ");
            SQL.AppendLine("LeaveType, StartDt, EndDt, DurationDay, StartTm, EndTm, Remark, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'N', 'O', @DeptCode, @AGCode, @SiteCode, @LeaveCode, @CompulsoryLeaveInd, ");
            SQL.AppendLine("@LeaveType, @StartDt, @EndDt, @DurationDay, @StartTm, @EndTm, @Remark, ");
            SQL.AppendLine("@CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select DocType, @DocNo, '001', DNo, @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblDocApprovalSetting ");
            SQL.AppendLine("Where DocType='EmpLeave2' ");
            if (mIsApprovalBySiteMandatory)
                SQL.AppendLine("And IfNull(SiteCode, '')=@SiteCode ");
            SQL.AppendLine("And DeptCode=@DeptCode ");
            if (mIsDocApprovalSettingUseLeaveCode)
            {
                SQL.AppendLine("And LeaveCode Is Not Null And Find_In_set(@LeaveCode, LeaveCode) ");
            }
            SQL.AppendLine("; ");

            SQL.AppendLine("Update TblEmpLeave2Hdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select DocNo From TblDocApproval ");
            SQL.AppendLine("    Where DocType='EmpLeave2' And DocNo=@DocNo ");
            SQL.AppendLine("); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@AGCode", Sm.GetLue(LueAGCode));
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
            Sm.CmParam<String>(ref cm, "@LeaveCode", Sm.GetLue(LueLeaveCode));
            Sm.CmParam<String>(ref cm, "@CompulsoryLeaveInd", ChkCompulsoryLeaveInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@LeaveType", Sm.GetLue(LueLeaveType));
            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));
            Sm.CmParam<Decimal>(ref cm, "@DurationDay", Decimal.Parse(TxtDurationDay.Text));
            Sm.CmParam<String>(ref cm, "@StartTm", Sm.GetTme(TmeStartTm));
            Sm.CmParam<String>(ref cm, "@EndTm", Sm.GetTme(TmeEndTm));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveEmpLeave2Dtl(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblEmpLeave2Dtl(DocNo, EmpCode, LeaveStartDt, DurationHour, BreakInd, NoOfHoliday, CreateBy, CreateDt) " +
                    "Values(@DocNo, @EmpCode, @LeaveStartDt, @DurationHour, @BreakInd, @NoOfHoliday, @CreateBy, CurrentDateTime());"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParamDt(ref cm, "@LeaveStartDt", Sm.GetGrdDate(Grd1, Row, 6));
            Sm.CmParam<Decimal>(ref cm, "@DurationHour", Sm.GetGrdDec(Grd1, Row, 8));
            Sm.CmParam<String>(ref cm, "@BreakInd", Sm.GetGrdBool(Grd1, Row, 9) ? "Y" : "N");
            Sm.CmParam<Decimal>(ref cm, "@NoOfHoliday", Sm.GetGrdDec(Grd1, Row, 12));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveEmpLeave2Dtl2(string DocNo, EmpLeaveDtl i)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblEmpLeave2Dtl2(DocNo, DNo, LeaveDt, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @LeaveDt, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", i.DNo);
            Sm.CmParam<String>(ref cm, "@LeaveDt", i.LeaveDt);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private void SetEmpLeaveDtl(ref List<EmpLeaveDtl> l)
        {
            string StartDt = Sm.GetDte(DteStartDt).Substring(0, 8);

            l.Add(new EmpLeaveDtl() { DNo = "001", LeaveDt = StartDt });

            if (!DteEndDt.Properties.ReadOnly)
            {
                string Dt = string.Empty;
                string EndDt = Sm.GetDte(DteEndDt).Substring(0, 8);
                DateTime Dt1 = new DateTime(
                    Int32.Parse(StartDt.Substring(0, 4)),
                    Int32.Parse(StartDt.Substring(4, 2)),
                    Int32.Parse(StartDt.Substring(6, 2)),
                    0, 0, 0
                    );
                DateTime Dt2 = new DateTime(
                    Int32.Parse(EndDt.Substring(0, 4)),
                    Int32.Parse(EndDt.Substring(4, 2)),
                    Int32.Parse(EndDt.Substring(6, 2)),
                    0, 0, 0
                    );

                var TotalDays = (Dt2 - Dt1).Days;
                for (int i = 1; i <= TotalDays; i++)
                {
                    Dt1 = Dt1.AddDays(1);
                    Dt =
                        Dt1.Year.ToString() +
                        ("00" + Dt1.Month.ToString()).Substring(("00" + Dt1.Month.ToString()).Length - 2, 2) +
                        ("00" + Dt1.Day.ToString()).Substring(("00" + Dt1.Day.ToString()).Length - 2, 2);

                    l.Add(new EmpLeaveDtl()
                    {
                        DNo = Sm.Right("00" + (i + 1).ToString(), 3),
                        LeaveDt = Dt
                    });
                }
            }
        }

        #endregion

        #region Edit data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsCancelledDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(CancelEmpLeave2Hdr());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsCancelledDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDataCancelledAlready() ||
                IsDocumentNotCancelled();
        }

        private bool IsDocumentNotCancelled()
        {
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this document.");
                return true;
            }
            return false;
        }

        private bool IsDataCancelledAlready()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select DocNo From TblEmpLeave2Hdr " +
                    "Where (CancelInd='Y' Or Status='C') And DocNo=@DocNo "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data already cancelled.");
                return true;
            }
            return false;
        }

        private MySqlCommand CancelEmpLeave2Hdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblEmpLeave2Hdr Set ");
            SQL.AppendLine("    CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N' And Status<>'C'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowEmpLeave2Hdr(DocNo);
                ShowEmpLeave2Dtl(DocNo);
                ShowDocApproval(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowEmpLeave2Hdr(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            SQL.AppendLine("Select DocNo, DocDt, CancelInd, ");
            SQL.AppendLine("Case Status When 'A' Then 'Approved' When 'C' Then 'Cancel' When 'O' Then 'Outstanding' Else 'Unknown' End As StatusDesc, ");
            SQL.AppendLine("LeaveCode, CompulsoryLeaveInd, DeptCode, AGCode, SiteCode, ");
            SQL.AppendLine("LeaveType, StartDt, EndDt, DurationDay, StartTm, EndTm, Remark ");
            SQL.AppendLine("From TblEmpLeave2Hdr Where DocNo=@DocNo;");

            Sm.ShowDataInCtrl(
                    ref cm,
                    SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo", 
                        
                        //1-5
                        "DocDt", "CancelInd", "StatusDesc", "LeaveCode", "CompulsoryLeaveInd",   
                        
                        //6-10
                        "DeptCode", "AGCode", "SiteCode", "LeaveType", "StartDt", 
                        
                        //11-15
                        "EndDt", "DurationDay", "StartTm", "EndTm", "Remark"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y");
                        TxtStatus.EditValue = Sm.DrStr(dr, c[3]);
                        SetLueLeaveCode(ref LueLeaveCode, Sm.DrStr(dr, c[4]));
                        Sm.SetLue(LueLeaveCode, Sm.DrStr(dr, c[4]));
                        ChkCompulsoryLeaveInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[5]), "Y");
                        Sl.SetLueDeptCode(ref LueDeptCode, Sm.DrStr(dr, c[6]), string.Empty);
                        Sl.SetLueAGCode(ref LueAGCode);
                        Sm.SetLue(LueAGCode, Sm.DrStr(dr, c[7]));
                        Sl.SetLueSiteCode(ref LueSiteCode, Sm.DrStr(dr, c[8]), string.Empty);
                        Sm.SetLue(LueLeaveType, Sm.DrStr(dr, c[9]));
                        Sm.SetDte(DteStartDt, Sm.DrStr(dr, c[10]));
                        Sm.SetDte(DteEndDt, Sm.DrStr(dr, c[11]));
                        TxtDurationDay.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[12]), 0);
                        Sm.SetTme(TmeStartTm, Sm.DrStr(dr, c[13]));
                        Sm.SetTme(TmeEndTm, Sm.DrStr(dr, c[14]));
                        MeeRemark.EditValue = Sm.DrStr(dr, c[15]);
                    }, true
                );
        }

        private void ShowEmpLeave2Dtl(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select B.EmpCode, C.EmpName, C.EmpCodeOld, D.PosName, ");
            SQL.AppendLine("Case When IfNull(F.ParValue, '')='Y' Then ");
            SQL.AppendLine("    IfNull(B.LeaveStartDt, C.JoinDt) ");
            SQL.AppendLine("Else C.JoinDt ");
            SQL.AppendLine("End As JoinDt, ");
            SQL.AppendLine("C.ResignDt, E.NoOfDay, B.DurationHour, B.BreakInd, B.NoOfHoliday ");
            SQL.AppendLine("From TblEmpLeave2Hdr A  ");
            SQL.AppendLine("Inner Join TblEmpleave2Dtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblEmployee C On B.EmpCode=C.EmpCode ");
            SQL.AppendLine("Left Join TblPosition D On C.PosCode=D.PosCode ");
            SQL.AppendLine("Inner Join TblLeave E On A.LeaveCode=E.LeaveCode ");
            SQL.AppendLine("Left Join TblParameter F On F.ParCode='IsAnnualLeaveUseStartDt' ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "EmpCode", 

                    //1-5
                    "EmpName", "EmpCodeOld", "PosName", "JoinDt", "ResignDt", 

                    //6-9
                    "NoOfDay", "DurationHour", "BreakInd", "NoOfHoliday"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Grd.Cells[Row, 0].Value = Row + 1;
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 11, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 9, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 9);
                }, false, false, true, false
            );
            Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 9 });
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 7, 8, 12 });
            ComputeLeaveRemainingDay();
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ShowDocApproval(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.ApprovalDNo, B.UserName, A.LastUpDt, A.Remark, ");
            SQL.AppendLine("Case Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' When 'O' Then 'Outstanding' End As StatusDesc ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Left Join TblUser B On A.UserCode = B.UserCode ");
            SQL.AppendLine("Where A.DocType='EmpLeave2' And IfNull(Status, 'O')<>'O' And A.DocNo=@DocNo ");
            SQL.AppendLine("Order By A.ApprovalDNo;");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                    ref Grd2, ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "UserName",
                        
                        //1-3
                        "StatusDesc","LastUpDt", "Remark"

                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    }, false, false, false, false
            );
            Sm.FocusGrd(Grd2, 0, 0);
        }

        #endregion

        #region Additional Method

        internal string GetSelectedEmpCode()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length != 0)
                        SQL += "##" + Sm.GetGrdStr(Grd1, Row, 2) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        private void GetParameter()
        {
            mAnnualLeaveCode = Sm.GetParameter("AnnualLeaveCode");
            mLongServiceLeaveCode = Sm.GetParameter("LongServiceLeaveCode");
            mLongServiceLeaveCode2 = Sm.GetParameter("LongServiceLeaveCode2");
            mIsSiteMandatory = Sm.GetParameterBoo("IsSiteMandatory");
            mTrainingPosCode = Sm.GetParameter("TrainingPosCode");
            mIsApprovalBySiteMandatory = Sm.GetParameterBoo("IsApprovalBySiteMandatory");
            mNeglectLeaveCode = Sm.GetParameter("NeglectLeaveCode");
            mIsFilterByDeptHR = Sm.GetParameterBoo("IsFilterByDeptHR");
            mIsFilterBySiteHR = Sm.GetParameterBoo("IsFilterBySiteHR");
            mIsDocApprovalSettingUseLeaveCode = Sm.GetParameterBoo("IsDocApprovalSettingUseLeaveCode");
        }

        private void ComputeDurationDay()
        {
            TxtDurationDay.EditValue = 0;

            if (Sm.GetDte(DteStartDt).ToString().Length > 0 && Sm.GetDte(DteEndDt).ToString().Length > 0)
            {
                string
                    StartDt = Sm.GetDte(DteStartDt).Substring(0, 8),
                    EndDt = Sm.GetDte(DteEndDt).Substring(0, 8);

                DateTime Dt1 = new DateTime(
                    Int32.Parse(StartDt.Substring(0, 4)),
                    Int32.Parse(StartDt.Substring(4, 2)),
                    Int32.Parse(StartDt.Substring(6, 2)),
                    0, 0, 0
                    );
                DateTime Dt2 = new DateTime(
                    Int32.Parse(EndDt.Substring(0, 4)),
                    Int32.Parse(EndDt.Substring(4, 2)),
                    Int32.Parse(EndDt.Substring(6, 2)),
                    0, 0, 0
                    );

                decimal holiday = 0m;
                if (Grd1.Rows.Count >= 1 && Sm.GetGrdStr(Grd1, 0, 12).Length > 0)
                    holiday = Sm.GetGrdDec(Grd1, 0, 12);
                TxtDurationDay.EditValue = (((Dt2 - Dt1).Days + 1) - holiday);
            }
        }

        private void ComputeDurationHour()
        {
            var DurationHour = 0m;

            if (Sm.GetDte(DteStartDt).ToString().Length > 0 &&
                Sm.GetTme(TmeStartTm).ToString().Length > 0 &&
                Sm.GetTme(TmeEndTm).ToString().Length > 0)
            {
                string
                StartDt = Sm.GetDte(DteStartDt).Substring(0, 8),
                StartTm = Sm.GetTme(TmeStartTm),
                EndTm = Sm.GetTme(TmeEndTm);

                DateTime Dt1 = new DateTime(
                    Int32.Parse(StartDt.Substring(0, 4)),
                    Int32.Parse(StartDt.Substring(4, 2)),
                    Int32.Parse(StartDt.Substring(6, 2)),
                    Int32.Parse(StartTm.Substring(0, 2)),
                    Int32.Parse(StartTm.Substring(2, 2)),
                    0
                    );
                DateTime Dt2 = new DateTime(
                    Int32.Parse(StartDt.Substring(0, 4)),
                    Int32.Parse(StartDt.Substring(4, 2)),
                    Int32.Parse(StartDt.Substring(6, 2)),
                    Int32.Parse(EndTm.Substring(0, 2)),
                    Int32.Parse(EndTm.Substring(2, 2)),
                    0
                    );

                if (Dt2 < Dt1) Dt2 = Dt2.AddDays(1);
                double TotalHours = (Dt2 - Dt1).TotalHours;

                if (TotalHours - (int)(TotalHours) > 0.5)
                    DurationHour = (int)(TotalHours) + 1;
                else
                {
                    if (TotalHours - (int)(TotalHours) == 0)
                        DurationHour = (decimal)TotalHours;
                    else
                    {
                        if (TotalHours - (int)(TotalHours) > 0 &&
                            TotalHours - (int)(TotalHours) <= 0.5)
                            DurationHour = (decimal)((int)(TotalHours) + 0.5);
                    }
                }
            }
            if (Grd1.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0)
                        Grd1.Cells[Row, 8].Value = DurationHour - (Sm.GetGrdBool(Grd1, Row, 9) ? 1 : 0);
            }
        }

        internal void ComputeDurationHour2(int Row)
        {
            var DurationHour = 0m;

            if (Sm.GetDte(DteStartDt).ToString().Length > 0 &&
                Sm.GetTme(TmeStartTm).ToString().Length > 0 &&
                Sm.GetTme(TmeEndTm).ToString().Length > 0)
            {
                string
                StartDt = Sm.GetDte(DteStartDt).Substring(0, 8),
                StartTm = Sm.GetTme(TmeStartTm),
                EndTm = Sm.GetTme(TmeEndTm);

                DateTime Dt1 = new DateTime(
                    Int32.Parse(StartDt.Substring(0, 4)),
                    Int32.Parse(StartDt.Substring(4, 2)),
                    Int32.Parse(StartDt.Substring(6, 2)),
                    Int32.Parse(StartTm.Substring(0, 2)),
                    Int32.Parse(StartTm.Substring(2, 2)),
                    0
                    );
                DateTime Dt2 = new DateTime(
                    Int32.Parse(StartDt.Substring(0, 4)),
                    Int32.Parse(StartDt.Substring(4, 2)),
                    Int32.Parse(StartDt.Substring(6, 2)),
                    Int32.Parse(EndTm.Substring(0, 2)),
                    Int32.Parse(EndTm.Substring(2, 2)),
                    0
                    );

                if (Dt2 < Dt1) Dt2 = Dt2.AddDays(1);
                double TotalHours = (Dt2 - Dt1).TotalHours;

                if (TotalHours - (int)(TotalHours) > 0.5)
                    DurationHour = (int)(TotalHours) + 1;
                else
                {
                    if (TotalHours - (int)(TotalHours) == 0)
                        DurationHour = (decimal)TotalHours;
                    else
                    {
                        if (TotalHours - (int)(TotalHours) > 0 &&
                            TotalHours - (int)(TotalHours) <= 0.5)
                            DurationHour = (decimal)((int)(TotalHours) + 0.5);
                    }
                }
            }
            if (Grd1.Rows.Count > 0)
            {
                if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0)
                    Grd1.Cells[Row, 8].Value = DurationHour - (Sm.GetGrdBool(Grd1, Row, 9) ? 1 : 0);
            }
        }

        private void SetLueLeaveCode(ref DXE.LookUpEdit Lue, string LeaveCode)
        {
            try
            {
                var SQL = new StringBuilder();
                if (LeaveCode.Length == 0)
                {
                    SQL.AppendLine("Select LeaveCode As Col1, LeaveName As Col2 ");
                    SQL.AppendLine("From TblLeave Where ActInd='Y' Order By LeaveName;");
                }
                else
                {
                    SQL.AppendLine("Select LeaveCode As Col1, LeaveName As Col2 ");
                    SQL.AppendLine("From TblLeave Where LeaveCode='" + LeaveCode + "';");
                }

                Sm.SetLue2(
                    ref Lue, SQL.ToString(),
                    0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void SetLueAGCode(ref DXE.LookUpEdit Lue, string DeptCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select AGCode As Col1, AGName As Col2 From TblAttendanceGrpHdr ");
            SQL.AppendLine("Where ActInd='Y' ");
            if (DeptCode.Length != 0)
            {
                SQL.AppendLine("And IfNull(DeptCode, 'XXX')='" + DeptCode + "' ");
            }
            SQL.AppendLine("Order By AGName;");

            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private string GetBreakSchedule(int Row)
        {
            var EmpCode = Sm.GetGrdStr(Grd1, Row, 2);

            if (EmpCode.Length == 0 || Sm.GetDte(DteStartDt).Length == 0) return string.Empty;

            var SQL = new StringBuilder();

            SQL.AppendLine("Select Concat(In2, Out2) As Break ");
            SQL.AppendLine("From TblEmpWorkSchedule A ");
            SQL.AppendLine("Inner Join TblWorkschedule B On A.WSCode=B.WSCode ");
            SQL.AppendLine("Where A.Dt=@Dt And A.EmpCode=@EmpCode ");
            SQL.AppendLine("And In2 Is Not Null And Out2 Is Not Null ");
            SQL.AppendLine("Limit 1;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);
            Sm.CmParamDt(ref cm, "@Dt", Sm.GetDte(DteStartDt));

            var b = Sm.GetValue(cm);
            if (b.Length == 0)
                return string.Empty;
            else
                return
                    Sm.Left(b, 2) + ":" + b.Substring(2, 2) + " - " +
                    b.Substring(4, 2) + ":" + Sm.Right(b, 2);
        }

        private void ShowEmployee()
        {
            try
            {
                Sm.ClearGrd(Grd1, false);

                if (Sm.IsLueEmpty(LueDeptCode, "Department")) return;
                if (mIsSiteMandatory && Sm.IsLueEmpty(LueSiteCode, "Site")) return;
                if (Sm.IsLueEmpty(LueLeaveCode, "Leave")) return;
                if (Sm.IsLueEmpty(LueLeaveType, "Type")) return;
                if (Sm.IsDteEmpty(DteStartDt, "Start Date")) return;
                if (Sm.IsDteEmpty(DteEndDt, "End Date")) return;

                var SQL = new StringBuilder();

                bool
                    IsFilterByAGCode = Sm.GetLue(LueAGCode).Length > 0,
                    IsFilterBySiteCode = Sm.GetLue(LueSiteCode).Length > 0;

                if (Sm.GetLue(LueLeaveCode) == Sm.GetParameter("AnnualLeaveCode"))
                {
                    SQL.AppendLine("Select T1.EmpCode, T1.EmpCodeOld, T1.EmpName, T1.PosName, T1.JoinDt, T1.ResignDt, 0 As NoOfDay ");
                    SQL.AppendLine("From ( ");
                    SQL.AppendLine("    Select A.EmpCode, A.EmpName, A.EmpCodeOld, A.DeptCode, B.PosName, ");
                    SQL.AppendLine("    Case When IfNull(G.ParValue, '')='Y' Then ");
                    SQL.AppendLine("        IfNull(A.LeaveStartDt, A.JoinDt) ");
                    SQL.AppendLine("    Else A.JoinDt ");
                    SQL.AppendLine("    End As JoinDt, ");
                    SQL.AppendLine("    A.ResignDt ");
                    SQL.AppendLine("    From TblEmployee A  ");
                    SQL.AppendLine("    Left Join TblPosition B On A.PosCode=B.PosCode ");
                    SQL.AppendLine("    Inner Join TblDepartment C On A.DeptCode=C.DeptCode And C.DeptCode=@DeptCode ");
                    if (IsFilterBySiteCode)
                        SQL.AppendLine("    Inner Join TblSite D On A.SiteCode=D.SiteCode And D.SiteCode=@SiteCode ");
                    if (IsFilterByAGCode)
                    {
                        SQL.AppendLine("    Inner Join TblAttendanceGrpHdr E On E.AGCode=@AGCode And E.ActInd='Y' ");
                        SQL.AppendLine("    Inner Join TblAttendanceGrpDtl F On A.EmpCode=F.EmpCode And E.AGCode=F.AGCode ");
                    }
                    SQL.AppendLine("Left Join TblParameter G On G.ParCode='IsAnnualLeaveUseStartDt' ");
                    SQL.AppendLine("Where (A.ResignDt Is Null Or (A.ResignDt Is Not Null And A.ResignDt>@CurrentDate)) ");
                    SQL.AppendLine("    And A.EmpCode Not In ( ");
                    SQL.AppendLine("        Select EmpCode From TblEmpPd Where JobTransfer = 'P' And PosCode=@TrainingPosCode  ");
                    SQL.AppendLine("    )  ");
                    if (mIsFilterBySiteHR)
                    {
                        SQL.AppendLine("And A.SiteCode Is Not Null ");
                        SQL.AppendLine("And Exists( ");
                        SQL.AppendLine("    Select 1 From TblGroupSite ");
                        SQL.AppendLine("    Where SiteCode=IfNull(A.SiteCode, '') ");
                        SQL.AppendLine("    And GrpCode In ( ");
                        SQL.AppendLine("        Select GrpCode From TblUser ");
                        SQL.AppendLine("        Where UserCode=@UserCode ");
                        SQL.AppendLine("    ) ");
                        SQL.AppendLine(") ");
                    }
                    if (mIsFilterByDeptHR)
                    {
                        SQL.AppendLine("And A.DeptCode Is Not Null ");
                        SQL.AppendLine("And Exists( ");
                        SQL.AppendLine("    Select 1 From TblGroupDepartment ");
                        SQL.AppendLine("    Where DeptCode=A.DeptCode ");
                        SQL.AppendLine("    And GrpCode In ( ");
                        SQL.AppendLine("        Select GrpCode From TblUser ");
                        SQL.AppendLine("        Where UserCode=@UserCode ");
                        SQL.AppendLine("    ) ");
                        SQL.AppendLine(") ");
                    }
                    SQL.AppendLine(") T1  ");
                    SQL.AppendLine("Where T1.DeptCode=@DeptCode ");
                    SQL.AppendLine("Order By T1.EmpName; ");
                }
                else
                {
                    SQL.AppendLine("Select A.EmpCode, A.EmpName, A.EmpCodeOld, B.PosName, A.JoinDt, A.ResignDt, If(@leaveCode = (Select ParValue From Tblparameter Where ParCode = 'LongServiceLeaveCode'),  0, G.NoOfDay) As NoOfDay, A.DeptCode, C.Deptname ");
                    SQL.AppendLine("From TblEmployee A ");
                    SQL.AppendLine("Left Join TblPosition B On A.PosCode=B.PosCode ");
                    SQL.AppendLine("Inner Join TblDepartment C On A.DeptCode=C.DeptCode And C.DeptCode=@DeptCode ");
                    if (IsFilterBySiteCode)
                        SQL.AppendLine("    Inner Join TblSite D On A.SiteCode=D.SiteCode And D.SiteCode=@SiteCode ");
                    if (IsFilterByAGCode)
                    {
                        SQL.AppendLine("    Inner Join TblAttendanceGrpHdr E On E.AGCode=@AGCode And E.ActInd='Y' ");
                        SQL.AppendLine("    Inner Join TblAttendanceGrpDtl F On A.EmpCode=F.EmpCode And E.AGCode=F.AGCode ");
                    }
                    SQL.AppendLine("Left Join TblLeave G On LeaveCode=@LeaveCode ");
                    SQL.AppendLine("Where (A.ResignDt Is Null Or (A.ResignDt Is Not Null And A.ResignDt>@CurrentDate)) ");
                    if (mIsFilterBySiteHR)
                    {
                        SQL.AppendLine("And A.SiteCode Is Not Null ");
                        SQL.AppendLine("And Exists( ");
                        SQL.AppendLine("    Select 1 From TblGroupSite ");
                        SQL.AppendLine("    Where SiteCode=IfNull(A.SiteCode, '') ");
                        SQL.AppendLine("    And GrpCode In ( ");
                        SQL.AppendLine("        Select GrpCode From TblUser ");
                        SQL.AppendLine("        Where UserCode=@UserCode ");
                        SQL.AppendLine("    ) ");
                        SQL.AppendLine(") ");
                    }
                    if (mIsFilterByDeptHR)
                    {
                        SQL.AppendLine("And A.DeptCode Is Not Null ");
                        SQL.AppendLine("And Exists( ");
                        SQL.AppendLine("    Select 1 From TblGroupDepartment ");
                        SQL.AppendLine("    Where DeptCode=A.DeptCode ");
                        SQL.AppendLine("    And GrpCode In ( ");
                        SQL.AppendLine("        Select GrpCode From TblUser ");
                        SQL.AppendLine("        Where UserCode=@UserCode ");
                        SQL.AppendLine("    ) ");
                        SQL.AppendLine(") ");
                    }
                    SQL.AppendLine("Order By A.EmpName;");
                }
                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
                Sm.CmParam<String>(ref cm, "@AGCode", Sm.GetLue(LueAGCode));
                Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
                Sm.CmParam<String>(ref cm, "@LeaveCode", Sm.GetLue(LueLeaveCode));
                Sm.CmParam<String>(ref cm, "@TrainingPosCode", mTrainingPosCode);
                Sm.CmParam<String>(ref cm, "@AnnualLeaveCode", mAnnualLeaveCode);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@CurrentDate", Sm.ServerCurrentDateTime());

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "EmpCode",
                        
                        //1-5
                        "EmpName", "EmpCodeOld", "PosName", "JoinDt", "ResignDt",

                        //6
                        "NoOfDay"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 6, 4);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 11, 5);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                        Grd.Cells[Row, 8].Value = false;
                        Grd.Cells[Row, 9].Value = false;
                        Grd.Cells[Row, 10].Value = string.Empty;
                        Grd.Cells[Row, 12].Value = 0m;
                    }, true, false, true, false
                );
                ComputeDurationHour();
                ComputeLeaveRemainingDay();
                SetEmployeeNoOfHoliday();
                Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 9 });
                Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 7, 8, 12 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ComputeLeaveRemainingDay()
        {
            var LeaveCode = Sm.GetLue(LueLeaveCode);

            if (LeaveCode.Length == 0 ||
                !(
                Sm.CompareStr(mAnnualLeaveCode, LeaveCode) ||
                Sm.CompareStr(mLongServiceLeaveCode, LeaveCode) ||
                Sm.CompareStr(mLongServiceLeaveCode2, LeaveCode)
                ))
                return;


            var l = new List<EmployeeLeave>();
            string
                Dt1 = Sm.GetDte(DteStartDt),
                Dt2 = Sm.GetDte(DteEndDt);

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 2).Length > 0)
                {
                    l.Add(new EmployeeLeave
                    {
                        EmpCode = Sm.GetGrdStr(Grd1, r, 2),
                        EmpName = Sm.GetGrdStr(Grd1, r, 3),
                        LeaveStartDt = Sm.Left(Dt1, 8),
                        LeaveEndDt = Sm.Left(Dt2, 8),
                        JoinDt = Sm.Left(Sm.GetGrdDate(Grd1, r, 6), 8),
                        Period = string.Empty,
                        IsValid = false
                    });
                }
            }

            if (l.Count <= 0) return;

            var RLPType = Sm.GetParameter("RLPType");

            if (RLPType == "1" || RLPType == "3")
            {
                if (Dt2.Length > 0 && !Sm.CompareStr(Sm.Left(Dt1, 4), Sm.Left(Dt2, 4)))
                {
                    return;
                }
                for (int i = 0; i < l.Count; i++)
                    l[i].Period = Sm.Left(l[i].LeaveStartDt, 4);
            }

            if (RLPType == "2")
            {
                if (Dt2.Length > 0)
                {
                    for (int i = 0; i < l.Count; i++)
                    {
                        if (Sm.CompareDtTm(Sm.Right(l[i].LeaveStartDt, 4), Sm.Right(l[i].JoinDt, 4)) < 0)
                            l[i].Period = (int.Parse(Sm.Left(l[i].LeaveStartDt, 4)) - 1).ToString();
                        else
                            l[i].Period = Sm.Left(l[i].LeaveStartDt, 4);
                        var v = (int.Parse(l[i].Period) + 1).ToString() + Sm.Right(l[i].JoinDt, 4);
                        if (Sm.CompareDtTm(l[i].LeaveEndDt, v) < 0)
                            l[i].IsValid = true;
                        else
                            return;
                    }
                }
            }

            string Period2 = Sm.Left(Dt1, 4);
            string Period1 = (int.Parse(Period2) - 1).ToString();

            var l2 = new List<EmployeeResidualLeave>();
            for (int i = 0; i < l.Count; i++)
            {
                if (l[i].Period == Period2)
                {
                    l2.Add(new EmployeeResidualLeave
                    {
                        EmpCode = l[i].EmpCode,
                        EmpName = l[i].EmpName,
                        JoinDt = string.Empty,
                        ActiveDt = string.Empty,
                        StartDt = string.Empty,
                        EndDt = string.Empty,
                        LeaveDay = 0m,
                        UsedDay = 0m,
                        RemainingDay = 0m
                    });
                }
            }
            
            if (LeaveCode == mAnnualLeaveCode)
                Sm.GetEmpAnnualLeave(ref l2, Period2);

            if (LeaveCode == mLongServiceLeaveCode)
                Sm.GetEmpLongServiceLeave(ref l2, Period2);

            if (LeaveCode == mLongServiceLeaveCode2)
                Sm.GetEmpLongServiceLeave2(ref l2, Period2);
            

            var l3 = new List<EmployeeResidualLeave>();
            for (int i = 0; i < l.Count; i++)
            {
                if (l[i].Period == Period1)
                {
                    l3.Add(new EmployeeResidualLeave
                    {
                        EmpCode = l[i].EmpCode,
                        EmpName = l[i].EmpName,
                        JoinDt = string.Empty,
                        ActiveDt = string.Empty,
                        StartDt = string.Empty,
                        EndDt = string.Empty,
                        LeaveDay = 0m,
                        UsedDay = 0m,
                        RemainingDay = 0m
                    });
                }
            }
            if (LeaveCode == mAnnualLeaveCode)
                Sm.GetEmpAnnualLeave(ref l3, Period1);

            if (LeaveCode == mLongServiceLeaveCode)
                Sm.GetEmpLongServiceLeave(ref l3, Period1);

            if (LeaveCode == mLongServiceLeaveCode2)
                Sm.GetEmpLongServiceLeave2(ref l3, Period1);
            
            var LeaveDuration = decimal.Parse(TxtDurationDay.Text);

            if (l2.Count > 0)
            {
                for (int i = 0; i < l2.Count; i++)
                {
                    for (int r = 0; r < Grd1.Rows.Count; r++)
                    {
                        if (Sm.CompareStr(l2[i].EmpCode, Sm.GetGrdStr(Grd1, r, 2)))
                        {
                            //ini untuk ngecek tanggal end leave request dengan tanggal end leave periode 
                            string EndDate = "0";
                            if (l2[i].EndDt.Length > 0)
                                EndDate = l2[i].EndDt;
                            if (Decimal.Parse(EndDate) >= Decimal.Parse(Dt2.Substring(0, 8)))
                            {
                                Grd1.Cells[r, 7].Value = l2[i].RemainingDay;
                            }
                            else
                            {
                                Grd1.Cells[r, 7].Value = Sm.FormatNum(0, 0);
                            }
                            break;
                        }
                    }
                }
            }

            if (l3.Count > 0)
            {
                for (int i = 0; i < l3.Count; i++)
                {
                    for (int r = 0; r < Grd1.Rows.Count; r++)
                    {
                        if (Sm.CompareStr(l3[i].EmpCode, Sm.GetGrdStr(Grd1, r, 2)))
                        {
                            //ini untuk ngecek tanggal end leave request dengan tanggal end leave periode 
                            string EndDate = "0";
                            if (l3[i].EndDt.Length > 0)
                                EndDate = l3[i].EndDt;
                            if (Decimal.Parse(EndDate) >= Decimal.Parse(Dt2.Substring(0, 8)))
                            {
                                Grd1.Cells[r, 7].Value = l3[i].RemainingDay;
                            }
                            else
                            {
                                Grd1.Cells[r, 7].Value = Sm.FormatNum(0, 0);
                            }
                            break;
                        }
                    }
                }
            }
        }

        internal void ComputeLeaveRemainingDay2(int Row)
        {
            var LeaveCode = Sm.GetLue(LueLeaveCode);

            if (LeaveCode.Length == 0 ||
                !(
                Sm.CompareStr(mAnnualLeaveCode, LeaveCode) ||
                Sm.CompareStr(mLongServiceLeaveCode, LeaveCode) ||
                Sm.CompareStr(mLongServiceLeaveCode2, LeaveCode)
                ))
                return;


            var l = new List<EmployeeLeave>();
            string
                Dt1 = Sm.GetDte(DteStartDt),
                Dt2 = Sm.GetDte(DteEndDt);

            
            if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0)
            {
                l.Add(new EmployeeLeave
                {
                    EmpCode = Sm.GetGrdStr(Grd1, Row, 2),
                    EmpName = Sm.GetGrdStr(Grd1, Row, 3),
                    LeaveStartDt = Sm.Left(Dt1, 8),
                    LeaveEndDt = Sm.Left(Dt2, 8),
                    JoinDt = Sm.Left(Sm.GetGrdDate(Grd1, Row, 6), 8),
                    Period = string.Empty,
                    IsValid = false
                });
            }

            if (l.Count <= 0) return;

            var RLPType = Sm.GetParameter("RLPType");

            if (RLPType == "1" || RLPType == "3")
            {
                if (Dt2.Length > 0 && !Sm.CompareStr(Sm.Left(Dt1, 4), Sm.Left(Dt2, 4)))
                {
                    return;
                }
                for (int i = 0; i < l.Count; i++)
                    if(l[i].EmpCode == Sm.GetGrdStr(Grd1, Row, 2))
                        l[i].Period = Sm.Left(l[i].LeaveStartDt, 4);
            }

            if (RLPType == "2")
            {
                if (Dt2.Length > 0)
                {
                    for (int i = 0; i < l.Count; i++)
                    {
                        if (l[i].EmpCode == Sm.GetGrdStr(Grd1, Row, 2))
                        {
                            if (Sm.CompareDtTm(Sm.Right(l[i].LeaveStartDt, 4), Sm.Right(l[i].JoinDt, 4)) < 0)
                                l[i].Period = (int.Parse(Sm.Left(l[i].LeaveStartDt, 4)) - 1).ToString();
                            else
                                l[i].Period = Sm.Left(l[i].LeaveStartDt, 4);
                            var v = (int.Parse(l[i].Period) + 1).ToString() + Sm.Right(l[i].JoinDt, 4);
                            if (Sm.CompareDtTm(l[i].LeaveEndDt, v) < 0)
                                l[i].IsValid = true;
                            else
                                return;
                        }
                    }
                }
            }

            string Period2 = Sm.Left(Dt1, 4);
            string Period1 = (int.Parse(Period2) - 1).ToString();

            var l2 = new List<EmployeeResidualLeave>();
            for (int i = 0; i < l.Count; i++)
            {
                if (l[i].EmpCode == Sm.GetGrdStr(Grd1, Row, 2))
                {
                    if (l[i].Period == Period2)
                    {
                        l2.Add(new EmployeeResidualLeave
                        {
                            EmpCode = l[i].EmpCode,
                            EmpName = l[i].EmpName,
                            JoinDt = string.Empty,
                            ActiveDt = string.Empty,
                            StartDt = string.Empty,
                            EndDt = string.Empty,
                            LeaveDay = 0m,
                            UsedDay = 0m,
                            RemainingDay = 0m
                        });
                    }
                }
            }

            if (LeaveCode == mAnnualLeaveCode)
                Sm.GetEmpAnnualLeave(ref l2, Period2);

            if (LeaveCode == mLongServiceLeaveCode)
                Sm.GetEmpLongServiceLeave(ref l2, Period2);

            if (LeaveCode == mLongServiceLeaveCode2)
                Sm.GetEmpLongServiceLeave2(ref l2, Period2);


            var l3 = new List<EmployeeResidualLeave>();
            for (int i = 0; i < l.Count; i++)
            {
                if (l[i].EmpCode == Sm.GetGrdStr(Grd1, Row, 2))
                {
                    if (l[i].Period == Period1)
                    {
                        l3.Add(new EmployeeResidualLeave
                        {
                            EmpCode = l[i].EmpCode,
                            EmpName = l[i].EmpName,
                            JoinDt = string.Empty,
                            ActiveDt = string.Empty,
                            StartDt = string.Empty,
                            EndDt = string.Empty,
                            LeaveDay = 0m,
                            UsedDay = 0m,
                            RemainingDay = 0m
                        });
                    }
                }
            }
            if (LeaveCode == mAnnualLeaveCode)
                Sm.GetEmpAnnualLeave(ref l3, Period1);

            if (LeaveCode == mLongServiceLeaveCode)
                Sm.GetEmpLongServiceLeave(ref l3, Period1);

            if (LeaveCode == mLongServiceLeaveCode2)
                Sm.GetEmpLongServiceLeave2(ref l3, Period1);

            var LeaveDuration = decimal.Parse(TxtDurationDay.Text);

            if (l2.Count > 0)
            {
                for (int i = 0; i < l2.Count; i++)
                {
                    if (Sm.CompareStr(l2[i].EmpCode, Sm.GetGrdStr(Grd1, Row, 2)))
                    {
                        //ini untuk ngecek tanggal end leave request dengan tanggal end leave periode 
                        string EndDate = "0";
                        if (l2[i].EndDt.Length > 0)
                            EndDate = l2[i].EndDt;
                        if (Decimal.Parse(EndDate) >= Decimal.Parse(Dt2.Substring(0, 8)))
                        {
                            Grd1.Cells[Row, 7].Value = l2[i].RemainingDay;
                        }
                        else
                        {
                            Grd1.Cells[Row, 7].Value = Sm.FormatNum(0, 0);
                        }
                        break;
                    }
                }
            }

            if (l3.Count > 0)
            {
                for (int i = 0; i < l3.Count; i++)
                {
                    if (Sm.CompareStr(l3[i].EmpCode, Sm.GetGrdStr(Grd1, Row, 2)))
                    {
                        //ini untuk ngecek tanggal end leave request dengan tanggal end leave periode 
                        string EndDate = "0";
                        if (l3[i].EndDt.Length > 0)
                            EndDate = l3[i].EndDt;
                        if (Decimal.Parse(EndDate) >= Decimal.Parse(Dt2.Substring(0, 8)))
                        {
                            Grd1.Cells[Row, 7].Value = l3[i].RemainingDay;
                        }
                        else
                        {
                            Grd1.Cells[Row, 7].Value = Sm.FormatNum(0, 0);
                        }
                        break;
                    }
                }
            }
        }

        internal void SetEmployeeNoOfHoliday()
        {
            var cm = new MySqlCommand();
            string Filter = string.Empty;
            string EmpCode = string.Empty;

            if (Grd1.Rows.Count >= 1)
            {
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    EmpCode = Sm.GetGrdStr(Grd1, r, 2);
                    if (EmpCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(A.EmpCode=@EmpCode" + r.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode" + r.ToString(), EmpCode);
                    }
                }
            }

            if (Filter.Length > 0)
                Filter = " And (" + Filter + ") ";
            else
                return;

            var SQL = new StringBuilder();
            var l = new List<EmployeeNoOfHoliday>();

            SQL.AppendLine("Select A.EmpCode, Count(1) As NoOfHoliday ");
            SQL.AppendLine("From TblEmpWorkSchedule A ");
            SQL.AppendLine("Inner Join TblWorkSchedule B On A.WSCode = B.WSCode ");
            SQL.AppendLine("Where B.HolidayInd='Y' ");
            SQL.AppendLine("And A.Dt Between @Dt1 And @Dt2 ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("Group By A.EmpCode; ");
           
            Sm.CmParam<String>(ref cm, "@EmpCode", EmpCode);
            Sm.CmParamDt(ref cm, "@Dt1", Sm.GetDte(DteStartDt));
            Sm.CmParamDt(ref cm, "@Dt2", Sm.GetDte(DteEndDt));

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { "EmpCode", "NoOfHoliday" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new EmployeeNoOfHoliday()
                        {
                            EmpCode = Sm.DrStr(dr, c[0]),
                            NoOfHoliday = Sm.DrDec(dr, c[1])
                        });
                    }
                }
                dr.Close();
            }
            foreach(var i in l)
            {
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                     EmpCode = Sm.GetGrdStr(Grd1, r, 2);
                     if (EmpCode.Length != 0 && Sm.CompareStr(i.EmpCode, EmpCode))
                     {
                         Grd1.Cells[r, 12].Value = i.NoOfHoliday;
                         break;
                     }
                }
            }
            ComputeDurationDay();
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, mIsFilterByDeptHR ? "Y" : "N");
                Sm.ClearGrd(Grd1, true);
                LueAGCode.EditValue = null;
                string DeptCode = Sm.GetLue(LueDeptCode);
                SetLueAGCode(ref LueAGCode, (DeptCode.Length == 0) ? string.Empty : DeptCode);
            }
        }

        private void LueLeaveCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                ClearGrd1();
                Sm.RefreshLookUpEdit(LueLeaveCode, new Sm.RefreshLue2(SetLueLeaveCode), string.Empty);
            }
        }

        private void DteStartDt_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                ClearGrd1();
                DteEndDt.EditValue = DteStartDt.EditValue;
                ComputeDurationDay();
            }
        }

        private void DteEndDt_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                ClearGrd1();
                ComputeDurationDay();
            }
        }

        private void TmeStartTm_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                TmeEndTm.EditValue = TmeStartTm.EditValue;
                ComputeDurationHour();
            }
        }

        private void TmeEndTm_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                ComputeDurationHour();
            }
        }

        private void LueLeaveType_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                ClearGrd1();
                Sm.RefreshLookUpEdit(LueLeaveType, new Sm.RefreshLue2(Sl.SetLueOption), "LeaveType");
                Sm.SetControlEditValueNull(new List<DXE.BaseEdit> { DteStartDt, DteEndDt, TmeStartTm, TmeEndTm });
                Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtDurationDay }, 0);

                for (int Row = 0; Row <= Grd1.Rows.Count - 1; Row++)
                    Grd1.Cells[Row, 8].Value = 0m;

                Sm.SetControlReadOnly(new List<DXE.BaseEdit> { DteEndDt, TmeStartTm, TmeEndTm }, true);
                Sm.GrdColReadOnly(true, true, Grd1, new int[] { 8, 9 });
                BtnDurationHrDefault.Enabled = false;

                if (Sm.GetLue(LueLeaveType) == "F")
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { DteEndDt }, false);
                else
                {
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TmeStartTm, TmeEndTm }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 8, 9 });
                    BtnDurationHrDefault.Enabled = true;
                }
            }
        }

        private void LueAGCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                string DeptCode = Sm.GetLue(LueDeptCode);
                Sm.RefreshLookUpEdit(LueAGCode, new Sm.RefreshLue2(SetLueAGCode), DeptCode);
                ClearGrd1();
            }
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue3(Sl.SetLueSiteCode), string.Empty, mIsFilterBySiteHR ? "Y" : "N");
                ClearGrd1();
            }
        }

        #endregion

        #region Button Event

        private void BtnRefreshData_Click(object sender, EventArgs e)
        {
            ShowEmployee();
        }

        private void BtnDurationHrDefault_Click(object sender, EventArgs e)
        {
            ComputeDurationHour();
        }

        #endregion

        #endregion

        #region Class

        private class EmpLeaveDtl
        {
            public string DNo { get; set; }
            public string LeaveDt { get; set; }
        }

        private class Holiday
        {
            public string HolidayDt { get; set; }
        }

        private class EmployeeLeave
        {
            public string EmpCode { get; set; }
            public string EmpName { get; set; }
            public string LeaveStartDt { get; set; }
            public string LeaveEndDt { get; set; }
            public string JoinDt { get; set; }
            public string Period { get; set; }
            public bool IsValid { get; set; }
        }

        private class EmployeeNoOfHoliday
        {
            public string EmpCode { get; set; }
            public decimal NoOfHoliday { get; set; }
        }

        private class EmployeeHoliday
        {
            public string EmpCode { get; set; }
            public string Holiday { get; set; }
        }

        #endregion
    }
}
