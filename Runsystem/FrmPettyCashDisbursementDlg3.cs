﻿#region Update
/*
    03/02/2023 [TYO/MNET] New Apps
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPettyCashDisbursementDlg3 : RunSystem.FrmBase4
    {
        #region Field

        private FrmPettyCashDisbursement mFrmParent;

        #endregion

        #region Constructor

        public FrmPettyCashDisbursementDlg3(FrmPettyCashDisbursement FrmParent) //(FrmTemplate FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method
        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                SetGrd();
                SetSQL();                
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }
        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 9;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[]
                    {
                        //0
                        "No",

                        //1-5
                        "Document#",
                        "Date",
                        "Dropping Request Type",
                        "Project Implementation#",
                        "Budget's Category",

                        //6-8
                        "Dropping's Request"+Environment.NewLine+"Amount",
                        "Project",
                        "Customer"
                    },
                    new int[]
                    {
                        //0
                        50,

                        //1-5
                        150, 120, 170, 150, 150,

                        //6-8
                        150, 200, 150
                    }
                );
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8 });
            Sm.GrdFormatDec(Grd1, new int[] { 6 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2 });
        }

        private string SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, J.OptDesc DroppingType, D.DocNo PRJIDocNo, C.BCName, A.Amt, H.ProjectName, I.CtName ");
            SQL.AppendLine("From TblDroppingRequestHdr A ");
            SQL.AppendLine("Left Join TblDroppingRequestDtl2 B On A.DocNo = B.DocNo ");
            SQL.AppendLine("Left Join TblBudgetCAtegory C On B.BCCode = C.BCCode ");
            SQL.AppendLine("Inner Join TblProjectImplementationHdr D On A.PRJIDocNo = D.DocNo ");
            SQL.AppendLine("Inner Join TblSOContractRevisionHdr E On D.SOContractDocNo = E.DocNo ");
            SQL.AppendLine("Inner Join TblSOContractHdr F On E.SOCDocNo = F.DocNo ");
            SQL.AppendLine("Inner Join TblBOQHdr G On F.BOQDocNo = G.DocNo ");
            SQL.AppendLine("Inner Join TblLOPHdr H On G.LOPDocNo = H.DocNo ");
            SQL.AppendLine("Inner Join TblCustomer I On G.CtCode = I.CtCode ");
            SQL.AppendLine("Inner Join TblOption J On A.DocType = J.OptCode And J.OptCat = 'DroppingRequestDocType' ");
            SQL.AppendLine("Where A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("And A.DocType = '4' And A.PRJIDocNo Is Not NULL And A.Status ='A' ");
            SQL.AppendLine("And A.DocNo Not IN  ");
            SQL.AppendLine("( ");
            SQL.AppendLine("	Select T2.DocNo  ");
            SQL.AppendLine("	From TblPettyCashDisbursementHdr T1 ");
            SQL.AppendLine("	Inner Join tbldroppingrequesthdr T2 ON T1.DroppingRequestDocNo = T2.DocNo ");
            SQL.AppendLine("	Where T1.Status = 'A' AND T1.CancelInd = 'N' ");
            SQL.AppendLine(")  ");
            SQL.AppendLine("And A.Amt <> 0 ");
            SQL.AppendLine("And A.Status = 'A' AND A.CancelInd ='N' ");


            return SQL.ToString();
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.CmParam<string>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, new string[] { "A.DocNo" });
                Sm.FilterStr(ref Filter, ref cm, TxtPRJIDocNo.Text, new string[] { "D.DocNo", "H.ProjectName" });
                Sm.FilterStr(ref Filter, ref cm, TxtCustomerCode.Text, new string[] { "I.CtCode", "I.CtName" });


                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, SetSQL() + Filter,
                    new string[]
                    { 
                    
                        //0
                        "DocNo",
                        
                        //1-5
                        "DocDt", "DroppingType", "PRJIDocNo", "BCName", "Amt",

                        //6-7
                        "ProjectName", "CtName"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);

                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.GetDroppingRequest(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                mFrmParent.ComputeBalance();
                this.Hide();
            }
        }

        #endregion

        #endregion

        #region Event

        //Chk
        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }
        private void ChkPRJIDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Project Implementation");
        }
        private void ChkCustomerCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Customer");
        }

        //Txt
        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }
        private void TxtPRJIDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void TxtCustomerCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }
        #endregion


    }
}
