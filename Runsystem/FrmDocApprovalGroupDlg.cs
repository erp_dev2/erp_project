﻿#region Update
/* 
    11/02/2019 [TKG] New Application
    27/01/2021 [RDH/PHT] menambah filter dan grid level dan menambah parameter IsDocApprovalGroupUseLevel
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmDocApprovalGroupDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmDocApprovalGroup mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmDocApprovalGroupDlg(FrmDocApprovalGroup FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sl.SetLueDeptCode(ref LueDeptCode);
                Sl.SetLueSiteCode2(ref LueSiteCode, string.Empty);
                Sl.SetLueLevelCode(ref LueLvlCode);

                if (!mFrmParent.mIsDocApprovalGroupUseLevel)
                    label1.Visible = LueLvlCode.Visible = ChkLvlCode.Visible = false;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 9;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "",
                        "Employee's Code",
                        "Employee's Name",
                        "Old Code",
                        "Position",
                        
                        //6-8
                        "Department",
                        "Site",
                        "Level"
                    },
                     new int[] 
                    {
                        //0
                        50,
 
                        //1-5
                        20, 100, 300, 80, 150, 150,
                        
                        //6-8
                        150, 100, 80
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 3, 4, 5, 6, 7, 8 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 4 });
            Sm.SetGrdProperty(Grd1, false);

            if (!mFrmParent.mIsDocApprovalGroupUseLevel)
            {
                Grd1.Cols[8].Visible = false;
            }
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 4 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.EmpCode, A.EmpName, A.EmpCodeOld, B.PosName, C.DeptName, D.SiteName, E.LevelName ");
            SQL.AppendLine("From TblEmployee A ");
            SQL.AppendLine("Left Join TblPosition B On A.PosCode=B.PosCode ");
            SQL.AppendLine("Inner Join TblDepartment C On A.DeptCode=C.DeptCode ");
            SQL.AppendLine("Left Join TblSite D On A.SiteCode=D.SiteCode ");
            SQL.AppendLine("LEFT JOIN tbllevelhdr E ON A.LevelCode = E.LevelCode ");
            SQL.AppendLine("Where (A.ResignDt Is Null Or (ResignDt Is Not Null And ResignDt>Left(CurrentDateTime(), 8))) ");
            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = string.Empty, EmpCode = string.Empty;
                var cm = new MySqlCommand();
                if (mFrmParent.Grd1.Rows.Count >= 1)
                {
                    for (int r = 0; r < mFrmParent.Grd1.Rows.Count; r++)
                    {
                        EmpCode = Sm.GetGrdStr(mFrmParent.Grd1, r, 1);
                        if (EmpCode.Length != 0)
                        {
                            if (Filter.Length > 0) Filter += " And ";
                            Filter += " (A.EmpCode<>@EmpCode0" + r.ToString() + ") ";
                            Sm.CmParam<String>(ref cm, "@EmpCode0" + r.ToString(), EmpCode);
                        }
                    }
                }

                if (Filter.Length != 0)
                    Filter = " And (" + Filter + ") ";
                else
                    Filter = " ";

                Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "A.EmpCode", "A.EmpName", "A.EmpCodeOld" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "A.DeptCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "A.SiteCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueLvlCode), "A.LevelCode", true);


                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By C.DeptName, A.EmpName;",
                        new string[] 
                        { 
                             //0
                             "EmpCode", 
                             
                             //1-5
                             "EmpName", 
                             "EmpCodeOld", 
                             "PosName",
                             "DeptName",
                             "SiteName",

                             //6
                             "LevelName"
                         },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Grd.Cells[Row, 1].Value = false;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);

                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                mFrmParent.Grd1.BeginUpdate();
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsEmpCodeAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 1, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 2, Grd1, Row2, 3);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 3, Grd1, Row2, 4);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 4, Grd1, Row2, 5);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 5, Grd1, Row2, 6);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 6, Grd1, Row2, 7); 
                        mFrmParent.Grd1.Rows.Add();
                    }
                }
                mFrmParent.Grd1.EndUpdate();
            }
            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 employee.");
        }

        private bool IsEmpCodeAlreadyChosen(int Row)
        {
            string EmpCode = Sm.GetGrdStr(Grd1, Row, 2);
            for (int Index = 0; Index < mFrmParent.Grd1.Rows.Count - 1; Index++)
            {
                if (Sm.CompareStr(EmpCode, Sm.GetGrdStr(mFrmParent.Grd1, Index, 1)))
                    return true;
            }
            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    Grd1.Cells[Row, 1].Value = !IsSelected;
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue2(Sl.SetLueSiteCode2), string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }

        private void LueLevelCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueLvlCode, new Sm.RefreshLue1(Sl.SetLueLevelCode));
            Sm.FilterLueSetCheckEdit(this, sender);

        }

        private void ChkLevelCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Level");
        }

        #endregion

        #endregion
    }
}
