﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;
using TenTec.Windows.iGridLib;

#endregion

namespace RunSystem
{
    public partial class FrmRptAdvancePayment : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = "", mAccessInd = "", mSQL = "";

        #endregion

        #region Constructor

        public FrmRptAdvancePayment(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                SetLueSiteCode(ref LueSiteCode);
                Sl.SetLueDeptCode(ref LueDeptCode);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void SetSQL()
        {

            var SQL = new StringBuilder();

            SQL.AppendLine(" Select A.DocNo, A.EmpCode, C.EmpName, Round((A.Amt),2)As Amt, D.DeptName, E.PosName,F.SiteName, A.Top, Case A.StartMth ");
            SQL.AppendLine(" When '01' Then 'January' When '02' Then 'February'  When '03' Then 'March'  When '04' Then 'April' ");
            SQL.AppendLine(" When '05' Then 'May' When '06' Then 'June' When '07' Then 'July'  When '08' Then 'August' ");
            SQL.AppendLine(" When '09' Then 'September' When '10' Then 'October' When '11' Then 'November'  When '12' Then 'December' End As StartMth, ");
            SQL.AppendLine(" ROUND(IfNull(B.Amount,0),2)As Amount, ROUND(IfNull(A.Amt-B.Amount,0),2) As Balance ");
            SQL.AppendLine(" from TblAdvancePaymenthdr A ");
            SQL.AppendLine(" Left Join ");
            SQL.AppendLine(" ( ");
            SQL.AppendLine(" 	Select DocNo, EmpCode, sum(Amt) As Amount ");
            SQL.AppendLine(" 	From( ");
            SQL.AppendLine(" 	Select DocNo, EmpCode,Amt from tbladvancepaymentprocess ");
            SQL.AppendLine(" 	)x ");
            SQL.AppendLine(" 	Group By DocNo, EmpCode ");
            SQL.AppendLine("	)B On A.DocNo=B.DocNo ");
            SQL.AppendLine(" Inner join TblEmployee C On A.EmpCode=C.EmpCode ");
            SQL.AppendLine(" Left Join TblDepartment D On C.DeptCode =D.DeptCode ");
            SQL.AppendLine(" Left Join TblPosition E On C.PosCode=E.PosCode ");
            SQL.AppendLine(" Left Join TblSite F On C.SiteCode=F.SiteCode ");
            SQL.AppendLine(" where A.status='A' And A.CancelInd='N' ");
            
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 11;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5      
                        "Employee's Code",
                        "Employee's Name",
                        "Department",
                        "Position",
                        "Site",

                        //6-10
                        "Amount",
                        "Term Of "+Environment.NewLine+" Payment",
                        "Start Month",
                        "Paid Amount",  
                        "Balance",
                   },
                     new int[] 
                    {
                        //0
                        40,

                        //1-5
                        100, 180, 160, 180, 150,  
                        
                        //6-10
                        100, 60, 90, 100, 100
                   }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 6, 7, 9, 10 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();

                string Filter = " And 0=0 ";

                Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "A.EmpCode", "C.EmpName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "D.DeptCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "F.SiteCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter,
                        new string[]
                        {
                            //0
                            "EmpCode", 
                            //1-5
                            "EmpName","DeptName", "PosName", "SiteName", "Amt",
                            
                            //6-9
                            "Top", "StartMth", "Amount", "Balance"
                        
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 10, 9);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Grid Nethod

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            Grd1.Rows.AutoHeight();
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional

        private static void SetLueSiteCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select SiteCode As Col1, SiteName As Col2 From TblSite Where ActInd='Y' Order By SiteName;",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #endregion

        #endregion

        #region Event

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }
      
        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue1(SetLueSiteCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode,new Sm.RefreshLue1(Sl.SetLueDeptCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }

        #endregion

    }
}
