﻿#region Update
/*
    11/12/2020 [DITA/PHT] new apps 
*/

#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmAllowanceWorkPeriod : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mDocNo = string.Empty;
        iGCell fCell;
        bool fAccept;
        internal FrmAllowanceWorkPeriodFind FrmFind;

        #endregion

        #region Constructor

        public FrmAllowanceWorkPeriod(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetGrd();
                SetFormControl(mState.View);
                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 4;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",

                        //1-3
                        "Work Period",
                        "Amount",
                        "Remark",
                    },
                     new int[] 
                    {
                        //0
                        0,

                        //1-5
                        100, 150, 200
                    }
                );
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 1, 2 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 0 }, false);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtDocNo, MeeRemark, MeeCancelReason, TxtDocName, DteStartDt, DteEndDt, DteDocDt }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3});
                    ChkCancelInd.Properties.ReadOnly = true;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtDocName, DteStartDt, DteEndDt, DteDocDt, MeeRemark }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1, 2, 3 });
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeCancelReason }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1, 2, 3 });
                    ChkCancelInd.Properties.ReadOnly = false;
                    ChkCancelInd.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, MeeRemark, MeeCancelReason, TxtDocName, DteStartDt, DteEndDt, DteDocDt
            });
            ChkCancelInd.Checked = false;
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 1, 2 });
        }


        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmAllowanceWorkPeriodFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = string.Empty;

            DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "AllowanceWorkPeriod", "TblAllowanceWorkPeriodHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveAllowanceWorkPeriodHdr(DocNo));

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0) cml.Add(SaveAllowanceWorkPeriodDtl(DocNo, Row));

           
            Sm.ExecCommands(cml);
            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsTxtEmpty(TxtDocName, "Name", false) ||
                Sm.IsDteEmpty(DteStartDt, "Start Date") ||
                Sm.IsDteEmpty(DteEndDt, "End Date") ||
                IsDateNotValid() ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                IsGrdExceedMaxRecords()
                ;
        }

        private bool IsDateNotValid()
        {
            decimal dt1 = Decimal.Parse(Sm.GetDte(DteEndDt).Substring(0, 8));
            decimal dt2 = Decimal.Parse(Sm.GetDte(DteStartDt).Substring(0, 8));

            if (dt2 > dt1)
            {
                Sm.StdMsg(mMsgType.Warning, "date not valid.");
                return true;
            }
            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 Work Period.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.IsGrdValueEmpty(Grd1, Row, 1, true, "Work Period could not be empty or zero.")) return true;
                    if (Sm.IsGrdValueEmpty(Grd1, Row, 2, true, "Amount could not be empty or zero.")) return true; 
                }

            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {

            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Work period data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private MySqlCommand SaveAllowanceWorkPeriodHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblAllowanceWorkPeriodHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, ");
            SQL.AppendLine("CancelReason, DocName, StartDt, EndDt, Remark, CreateBy,CreateDt) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@DocNo, @DocDt, 'N', ");
            SQL.AppendLine("@CancelReason, @DocName, @StartDt, @EndDt, @Remark, @UserCode, CurrentDateTime()) ");


            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@DocName", TxtDocName.Text);
            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveAllowanceWorkPeriodDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblAllowanceWorkPeriodDtl ");
            SQL.AppendLine("(DocNo, DNo, WorkPeriod, ");
            SQL.AppendLine("Amt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@DocNo, @DNo, @WorkPeriod, ");
            SQL.AppendLine("@Amt, @Remark, @UserCode, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<Decimal>(ref cm, "@WorkPeriod", Sm.GetGrdDec(Grd1, Row, 1));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd1, Row, 2));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 3));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditAllowanceWorkPeriodHdr(TxtDocNo.Text));

            if (!ChkCancelInd.Checked)
            {
                cml.Add(DeleteAllowanceWorkPeriodDtl());
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0) cml.Add(SaveAllowanceWorkPeriodDtl(TxtDocNo.Text, Row));
            }

           
            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDateNotValid() ||
                IsDataCancelledAlready() ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() ||
                IsGrdExceedMaxRecords()||
                (ChkCancelInd.Checked && Sm.IsMeeEmpty(MeeCancelReason, "Reason for cancellation"))
                ;
        }

        private bool IsDataCancelledAlready()
        {
            return Sm.IsDataExist(
                "Select 1 From TblAllowanceWorkPeriodHdr Where (CancelInd='Y') And DocNo=@Param;",
                TxtDocNo.Text,
                "This document already cancelled."
                );

        }

        private MySqlCommand EditAllowanceWorkPeriodHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblAllowanceWorkPeriodHdr Set ");
            SQL.AppendLine("    CancelInd=@CancelInd, CancelReason=@CancelReason, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", (ChkCancelInd.Checked) ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand DeleteAllowanceWorkPeriodDtl()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Delete From TblAllowanceWorkPeriodDtl Where DocNo = @DocNo ");
            SQL.AppendLine("And Exists ( Select DocNo From TblAllowanceWorkPeriodHdr Where DocNo = @DocNo And CancelInd = 'N' ); ");
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                ShowAllowanceWorkPeriodHdr(DocNo);
                ShowAllowanceWorkPeriodDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowAllowanceWorkPeriodHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo, DocDt, CancelReason, CancelInd, DocName, StartDt, EndDt, Remark ");
            SQL.AppendLine("From TblAllowanceWorkPeriodHdr Where DocNo = @DocNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DocNo",

                    //1-5
                    "DocDt", "CancelReason", "CancelInd", "DocName", "StartDt",  
                    
                    //6-7
                    "EndDt", "Remark"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                    MeeCancelReason.EditValue = Sm.DrStr(dr, c[2]);
                    ChkCancelInd.Checked = Sm.DrStr(dr, c[3]) == "Y";
                    TxtDocName.EditValue = Sm.DrStr(dr, c[4]);
                    Sm.SetDte(DteStartDt, Sm.DrStr(dr, c[5]));
                    Sm.SetDte(DteEndDt, Sm.DrStr(dr, c[6]));
                    MeeRemark.EditValue = Sm.DrStr(dr, c[7]);
                   
                }, true
            );
        }

        private void ShowAllowanceWorkPeriodDtl(string DocNo)
        {
            var SQLDtl = new StringBuilder();

            SQLDtl.AppendLine("Select DNo, WorkPeriod, Amt, Remark ");;
            SQLDtl.AppendLine("From TblAllowanceWorkPeriodDtl Where DocNo = @DocNo  ");
            SQLDtl.AppendLine("Order By DNo; ");

            var cm1 = new MySqlCommand();
            Sm.CmParam<String>(ref cm1, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
                ref Grd1, ref cm1, SQLDtl.ToString(),
                new string[] 
                { 
                    "DNo", 
                    "WorkPeriod", "Amt", "Remark"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 1);
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 1, 2 });

        }

        #endregion

        #endregion

        #region Event

        #region Grid Event 

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        #endregion

        #region Misc Control Event

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 1, 2, 3 }, e.ColIndex) && TxtDocNo.Text.Length == 0)
            {
                Sm.GrdRequestEdit(Grd1, e.RowIndex);
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);;
        }

        #endregion

        #endregion
    }
}
