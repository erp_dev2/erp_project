﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptStockSummary4 : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty;
        private int mNumberOfInventoryUomCode = 1;
        private bool 
            IsReCompute = false,
            mIsItGrpCode = false,
            mIsShowForeignName = false,
            mIsFilterByItCt = false; 

        #endregion

        #region Constructor

        public FrmRptStockSummary4(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                SetSQL();
                Sm.SetDteCurrentDate(DteDocDt);
                Sl.SetLueWhsCode(ref LueWhsCode);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            string NumberOfInventoryUomCode = Sm.GetParameter("NumberOfInventoryUomCode");
            if (NumberOfInventoryUomCode.Length == 0)
                mNumberOfInventoryUomCode = 1;
            else
                mNumberOfInventoryUomCode = int.Parse(NumberOfInventoryUomCode);

            mIsItGrpCode = Sm.GetParameter("IsItGrpCodeShow") == "N";
            mIsShowForeignName = Sm.GetParameterBoo("IsShowForeignName");
            mIsFilterByItCt = Sm.GetParameterBoo("IsFilterByItCt");
        }

        private string SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.WhsCode, D.WhsName, A.ItCode, E.ItCodeInternal, E.ItName, E.ItCtCode, F.ItCtName, ");
            SQL.AppendLine("E.InventoryUomCode, E.InventoryUomCode2, E.InventoryUomCode3, ");
            SQL.AppendLine("A.Qty, A.Qty2, A.Qty3, B.CountBatch, CountBin, E.ItGrpCode, G.ItGrpName, E.ForeignName, H.ItScName ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select WhsCode, ItCode, ");
            SQL.AppendLine("    Sum(Qty) As Qty, Sum(Qty2) As Qty2, Sum(Qty3) As Qty3 ");
            SQL.AppendLine("    From TblStockSummary ");
            SQL.AppendLine("    Where Qty<>0 ");
            SQL.AppendLine("    Group By WhsCode, ItCode ");
            SQL.AppendLine(") A ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select WhsCode, ItCode, Count(BatchNo) As CountBatch From ( ");
            SQL.AppendLine("        Select Distinct WhsCode, ItCode, BatchNo ");
            SQL.AppendLine("        From TblStockSummary ");
            SQL.AppendLine("        Where Qty<>0 ");
            SQL.AppendLine("    ) T Group By WhsCode, ItCode ");
            SQL.AppendLine(") B On A.WhsCode=B.WhsCode And A.ItCode=B.ItCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select WhsCode, ItCode, Count(Bin) As CountBin From ( ");
            SQL.AppendLine("        Select Distinct WhsCode, ItCode, Bin ");
            SQL.AppendLine("        From TblStockSummary ");
            SQL.AppendLine("        Where Qty<>0 ");
            SQL.AppendLine("    ) T Group By WhsCode, ItCode ");
            SQL.AppendLine(") C On A.WhsCode=C.WhsCode And A.ItCode=C.ItCode ");
            SQL.AppendLine("Inner Join TblWarehouse D On A.WhsCode=D.WhsCode ");
            SQL.AppendLine("Inner Join TblItem E On A.ItCode=E.ItCode ");
            if (mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=E.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode ");
            SQL.AppendLine("Left Join TblItemGroup G On E.ItGrpCode = G.ItGrpCode ");
            SQL.AppendLine("Left Join TblItemSubCategory H On E.ItScCode = E.ItScCode ");

            return SQL.ToString();
        }

        private string SetSQL2(string DocDt)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.WhsCode, D.WhsName, A.ItCode, E.ItCodeInternal, E.ItName, E.ItCtCode, F.ItCtName, ");
            SQL.AppendLine("E.InventoryUomCode, E.InventoryUomCode2, E.InventoryUomCode3, ");
            SQL.AppendLine("A.Qty, A.Qty2, A.Qty3, B.CountBatch, CountBin,  E.ItGrpCode, G.ItGrpName, E.ForeignName, H.ItScName  ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select WhsCode, ItCode, ");
            SQL.AppendLine("    Sum(Qty) As Qty, Sum(Qty2) As Qty2, Sum(Qty3) As Qty3 ");
            SQL.AppendLine("    From TblStockMovement ");
            SQL.AppendLine("    Where DocDt<=" + DocDt);
            SQL.AppendLine("    Group By WhsCode, ItCode ");
            SQL.AppendLine("    Having (Sum(Qty)<>0 Or Sum(Qty2)<>0 Or Sum(Qty3)<>0) ");
            SQL.AppendLine(") A ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select WhsCode, ItCode, Count(BatchNo) As CountBatch From ( ");
            SQL.AppendLine("        Select WhsCode, ItCode, BatchNo ");
            SQL.AppendLine("        From TblStockMovement ");
            SQL.AppendLine("        Where DocDt<=" + DocDt);
            SQL.AppendLine("        Group By WhsCode, ItCode, BatchNo ");
            SQL.AppendLine("        Having (Sum(Qty)<>0 Or Sum(Qty2)<>0 Or Sum(Qty3)<>0) ");
            SQL.AppendLine("    ) T Group By WhsCode, ItCode ");
            SQL.AppendLine(") B On A.WhsCode=B.WhsCode And A.ItCode=B.ItCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select WhsCode, ItCode, Count(Bin) As CountBin From ( ");
            SQL.AppendLine("        Select WhsCode, ItCode, Bin ");
            SQL.AppendLine("        From TblStockMovement ");
            SQL.AppendLine("        Where DocDt<=" + DocDt);
            SQL.AppendLine("        Group By WhsCode, ItCode, Bin ");
            SQL.AppendLine("        Having (Sum(Qty)<>0 Or Sum(Qty2)<>0 Or Sum(Qty3)<>0) ");
            SQL.AppendLine("    ) T Group By WhsCode, ItCode ");
            SQL.AppendLine(") C On A.WhsCode=C.WhsCode And A.ItCode=C.ItCode ");
            SQL.AppendLine("Inner Join TblWarehouse D On A.WhsCode=D.WhsCode ");
            SQL.AppendLine("Inner Join TblItem E On A.ItCode=E.ItCode ");
            if (mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=E.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode ");
            SQL.AppendLine("Left Join TblItemGroup G On E.ItGrpCode = G.ItGrpCode ");
            SQL.AppendLine("Left Join TblItemSubCategory H On E.ItScCode = H.ItScCode ");

            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 22;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Warehouse", 
                        "Item's Code", 
                        "",
                        "Local Code",
                        "Item's Name", 

                        //6-10
                        "Total"+ Environment.NewLine + "Bin",
                        "",
                        "Category", 
                        "Total"+ Environment.NewLine + "Batch#",
                        "",

                        //11-15
                        "Quantity", 
                        "UoM",
                        "Quantity", 
                        "UoM",
                        "Quantity", 
                        
                        //16-20
                        "UoM",
                        "WhsCode",
                        "Item Group"+Environment.NewLine+"Code",
                        "Item Group"+Environment.NewLine+"Name",
                        "Foreign Name",

                        //21
                        "Sub-Category"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        200, 80, 20, 100, 200, 
                        
                        //6-10
                        80, 20, 150, 80, 20, 

                        //11-15
                        100, 70, 100, 70, 100,

                        //16-20
                        70, 80, 100, 150, 170,

                        //21
                        150
                    }
                );

            Sm.GrdColButton(Grd1, new int[] { 3, 7, 10 });
            Sm.GrdFormatDec(Grd1, new int[] { 6, 9, 11, 13, 15 }, 0);
            if (mIsShowForeignName)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 2, 4, 7, 13, 14, 15, 16, 17, 21 }, false);
                Grd1.Cols[20].Move(6);
            }
            else
            {
                Sm.GrdColInvisible(Grd1, new int[] { 2, 4, 7, 13, 14, 15, 16, 17, 20, 21 }, false);
            }
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 4, 6, 8, 9, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21 });
            ShowInventoryUomCode();
            if (mIsItGrpCode)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 18, 19 }, false);
            }
            Grd1.Cols[18].Move(6);
            Grd1.Cols[19].Move(7);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 4, 21 }, !ChkHideInfoInGrd.Checked);
        }

        private void ShowInventoryUomCode()
        {
            if (mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 13, 14 }, true);

            if (mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 13, 14, 15, 16 }, true);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string
                    Filter = string.Empty,
                    DocDt = (ChkDocDt.Checked) ? Sm.GetDte(DteDocDt).Substring(0, 8) : "";
                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueWhsCode), "A.WhsCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "A.ItCode", "E.ItCodeInternal", "E.ItName", "ForeignName" });
               
                IsReCompute = false;

                Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                ((ChkDocDt.Checked && decimal.Parse(DocDt) < decimal.Parse(Sm.ServerCurrentDateTime().Substring(0, 8))) ?
                SetSQL2(DocDt) : SetSQL()) +
                Filter + " Order By D.WhsName, E.ItName;",
                new string[]
                    {
                        //0
                        "WhsName", 

                        //1-5
                        "ItCode", "ItCodeInternal", "ItName", "CountBin", "ItCtName", 
                        
                        //6-10
                        "CountBatch", "Qty", "InventoryUomCode", "Qty2", "InventoryUOMCode2", 
                        
                        //11-15
                        "Qty3", "InventoryUOMCode3", "WhsCode", "ItGrpCode", "ItGrpName",

                        //16
                        "ForeignName", "ItScName"
                    },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Grd.Cells[Row, 0].Value = Row + 1;
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 11);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 12);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 13);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 14);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 15);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 16);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 17);
                }, true, false, false, false
                );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 11, 13, 15 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                IsReCompute = true;
                if (Grd1.Rows.Count > 1)
                    Sm.FocusGrd(Grd1, 1, ChkHideInfoInGrd.Checked ? 5 : 2);
                else
                    Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            if (IsReCompute) Sm.GrdExpand(Grd1);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            e.DoDefault = false;
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }

            if (e.ColIndex == 10 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                string DocDtXX = string.Empty, Exclude = string.Empty;
                if (ChkDocDt.Checked == true)
                {
                    DocDtXX = Sm.GetDte(DteDocDt);
                }
                var f = new FrmRptStockSummary4Dlg(this, Sm.GetGrdStr(Grd1, e.RowIndex, 17), Sm.GetGrdStr(Grd1, e.RowIndex, 2), DocDtXX);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.TxtWhs.EditValue = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.TxtItCode.EditValue = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.TxtItName.EditValue = Sm.GetGrdStr(Grd1, e.RowIndex, 5);
                f.ShowDialog();
            }
            
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }

            if (e.ColIndex == 10 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                string DocDtXX = string.Empty, Exclude = string.Empty;
                if (ChkDocDt.Checked == true)
                {
                    DocDtXX = Sm.GetDte(DteDocDt);
                }
                var f = new FrmRptStockSummary4Dlg(this, Sm.GetGrdStr(Grd1, e.RowIndex, 17), Sm.GetGrdStr(Grd1, e.RowIndex, 2), DocDtXX);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.TxtWhs.EditValue = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.TxtItCode.EditValue = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.TxtItName.EditValue = Sm.GetGrdStr(Grd1, e.RowIndex, 5);
                f.ShowDialog();
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        private void ChkWhsCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Warehouse");
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit2(this, sender, "Date");
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item"); 
        }

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue1(Sl.SetLueWhsCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void DteDocDt_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit2(this, sender);
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        #endregion
    }
}
