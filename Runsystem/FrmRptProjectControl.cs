﻿#region Update
/*
    11/12/2019 [WED/IMS] new apps, untuk menampilkan WBS dari Project Implementation
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptProjectControl : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;
        private decimal mNoOfDayProjectControlNotification = 0m;

        #endregion

        #region Constructor

        public FrmRptProjectControl(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
            GetParameter();
            Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -30);
            Sl.SetLueCtCode(ref LueCtCode);
            SetGrd();
            SetSQL();
            base.FrmLoad(sender, e);
        }
        
        private void SetGrd()
        {
            Grd1.Cols.Count = 22;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No",
                    //1-5
                    "Document#", "Date", "Status", "Process", "Customer Code",
                    //6-10
                    "Customer", "Customer PO#", "Project Code", "Project Name", "Achievement",
                    //11-15
                    "Stage", "Task", "Plan"+Environment.NewLine+"Start Date", "Plan"+Environment.NewLine+"End Date", "Bobot",
                    //16-20
                    "Percentage", "Price", "Estimated Price", "Settled", "Settled Price",
                    //21
                    "Settled Date"
                },
                new int[] 
                {
                    50,
                    180, 80, 100, 100, 100, 
                    200, 120, 100, 200, 100,
                    200, 200, 100, 100, 100,
                    100, 120, 120, 100, 120,
                    100
                }
            );
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21 });
            Sm.GrdFormatDec(Grd1, new int[] { 10, 15, 16, 17, 18, 20 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2, 13, 14, 21 });
            Sm.GrdColCheck(Grd1, new int[] { 19 });
            Sm.GrdColInvisible(Grd1, new int[] { 5 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdProperty(Grd1, false);
        }

        private void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.* From ( ");
            SQL.AppendLine("SELECT A.DocNo, A.DocDt,  ");
            SQL.AppendLine("Case A.Status When 'A' Then 'Approved' When 'O' Then 'Oustanding' ELSE 'Cancelled' END AS StatusDesc, ");
            SQL.AppendLine("Case A.ProcessInd When 'D' Then 'Draft' When 'F' Then 'Final' END AS ProcessInd, ");
            SQL.AppendLine("H.CtCode, I.CtName, F.PONo, IFNULL(J.ProjectCode, F.ProjectCode2) ProjectCode, IFNULL(J.ProjectName, H.ProjectName) ProjectName, ");
            SQL.AppendLine("A.Achievement, C.StageName, D.TaskName, B.PlanStartDt, B.PlanEndDt, B.Bobot, B.BobotPercentage, B.Amt, ");
            SQL.AppendLine("B.EstimatedAmt, B.SettledInd, B.SettledAmt, B.SettleDt ");
            SQL.AppendLine("FROM TblProjectImplementationHdr A ");
            SQL.AppendLine("INNER JOIN TblProjectImplementationDtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("    AND A.DocType = '2' ");
            SQL.AppendLine("    AND A.CancelInd = 'N' ");
            SQL.AppendLine("    AND A.Status IN ('O', 'A') ");
            SQL.AppendLine("    AND (A.DocDt BETWEEN @DocDt1 And @DocDt2) ");
            SQL.AppendLine("INNER JOIN TblProjectStage C ON B.StageCode = C.StageCode ");
            SQL.AppendLine("INNER JOIN TblProjectTask D ON B.TaskCode = D.TaskCode ");
            SQL.AppendLine("INNER JOIN TblSOContractRevisionHdr E ON A.SOContractDocNo = E.DocNo ");
            SQL.AppendLine("INNER JOIN TblSOContractHdr F ON E.SOCDocNo = F.DocNo ");
            SQL.AppendLine("INNER JOIN TblBOQHdr G ON F.BOQDocNo = G.DocNo ");
            SQL.AppendLine("INNER JOIN TblLOPHdr H ON G.LOPDocNo = H.DocNo ");
            SQL.AppendLine("INNER JOIN TblCustomer I ON H.CtCode = I.CtCode ");
            SQL.AppendLine("LEFT JOIN TblProjectGroup J ON H.PGCode = J.PGCode ");
            SQL.AppendLine(") T ");

            mSQL = SQL.ToString();
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 5 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " Where 0 = 0 ";

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "T.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "T.PONo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtProjectCode.Text, new string[] { "T.ProjectName", "T.ProjectCode" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCode), "T.CtCode", true);
                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By T.DocNo; ",
                    new string[]
                    {
                        //0
                        "DocNo",  
                        //1-5
                        "DocDt", "StatusDesc", "ProcessInd", "CtCode", "CtName", 
                        //6-10
                        "PONo", "ProjectCode", "ProjectName", "Achievement", "StageName", 
                        //11-15
                        "TaskName", "PlanStartDt", "PlanEndDt", "Bobot", "BobotPercentage", 
                        //16-20
                        "Amt", "EstimatedAmt", "SettledInd", "SettledAmt", "SettleDt"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 13, 12);
                        string mPlanEndDt = Sm.DrStr(dr, c[13]);
                        decimal a = 0m, b = 0m;
                        if (mPlanEndDt.Length > 0) a = Decimal.Parse(mPlanEndDt);
                        b = Decimal.Parse(Sm.Left(Sm.ServerCurrentDate(), 8));
                        if ((((a - b) <= mNoOfDayProjectControlNotification) ||
                            a <= b) &&
                            Sm.DrStr(dr, c[18]) == "N")
                        {
                            Grd.Rows[Row].BackColor = Color.Khaki;
                        }
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 14, 13);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 14);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 15);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 16);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 17);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 19, 18);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 19);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 21, 20);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Methods

        private void GetParameter()
        {
            mNoOfDayProjectControlNotification = Sm.GetParameterDec("NoOfDayProjectControlNotification");
            if (mNoOfDayProjectControlNotification == 0m) mNoOfDayProjectControlNotification = 7m;
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt1).Length == 0) DteDocDt1.EditValue = DteDocDt2.EditValue;
        }

        private void TxtProjectCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkProjectCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Project");
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue2(Sl.SetLueCtCode), string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");
        }

        private void TxtPONo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkPONo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Customer PO#");
        }

        #endregion

        #endregion
    }
}
