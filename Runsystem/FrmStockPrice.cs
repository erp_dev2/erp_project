﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmStockPrice : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmStockPriceFind FrmFind;

        #endregion

        #region Constructor

        public FrmStockPrice(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            SetFormControl(mState.View);
            Sl.SetLueCurCode(ref LueCurCode);
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtItCode, TxtItName,TxtBatchNo,TxtSource,LueCurCode,TxtUprice,TxtExcRate,MeeRemark
                    }, true);
                    TxtItCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(TxtItCode, true);
                    Sm.SetControlReadOnly(TxtItName, true);
                    Sm.SetControlReadOnly(TxtBatchNo, true);
                    Sm.SetControlReadOnly(TxtSource, true);
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        LueCurCode,TxtUprice,TxtExcRate,MeeRemark
                    }, false);
                    LueCurCode.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtItCode, TxtItName,TxtBatchNo,TxtSource,LueCurCode,TxtUprice,TxtExcRate,MeeRemark
            });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmStockPriceFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }


        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtItCode, "", false)) return;
            SetFormControl(mState.Edit);
        }


        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {


                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Update TblStockPrice Set CurCode=@CurCode,UPrice=@Uprice,ExcRate=@ExcRate,Remark=@Remark, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() Where ItCode=@ItCode And BatchNo=@BatchNo And Source=@Source ; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@ItCode", TxtItCode.Text);
                Sm.CmParam<String>(ref cm, "@BatchNo", TxtBatchNo.Text);
                Sm.CmParam<String>(ref cm, "@Source", TxtSource.Text);
                Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
                Sm.CmParam<Decimal>(ref cm, "@UPrice", decimal.Parse(TxtUprice.Text));
                Sm.CmParam<Decimal>(ref cm, "@ExcRate", decimal.Parse(TxtExcRate.Text));
                Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.ExecCommand(cm);

                Sm.StdMsg(mMsgType.Info, "Stock Price is successfully changed");
                ShowData(TxtItCode.Text, TxtBatchNo.Text, TxtSource.Text);

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string ItCode,string BatchNo,string Source)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@ItCode", ItCode);
                Sm.CmParam<String>(ref cm, "@BatchNo", BatchNo);
                Sm.CmParam<String>(ref cm, "@Source", Source);

                Sm.ShowDataInCtrl(
                        ref cm,
                        "Select A.ItCode,B.ItName,A.BatchNo,A.Source,A.CurCode,A.UPrice,A.ExcRate,A.Remark "+
                        "From TblStockPrice A "+
                        "Inner Join TblItem B On A.ItCode=B.ItCode Where A.ItCode=@ItCode And A.BatchNo=@BatchNo And A.Source=@Source ",
                        new string[] 
                        {   //0
                            "ItCode",
                            //1-5
                            "ItName","BatchNo","Source","CurCode","UPrice",
                            //6-7
                            "ExcRate","Remark"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtItCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtItName.EditValue = Sm.DrStr(dr, c[1]);
                            TxtBatchNo.EditValue = Sm.DrStr(dr, c[2]);
                            TxtSource.EditValue = Sm.DrStr(dr, c[3]);
                            Sm.SetLue(LueCurCode, Sm.DrStr(dr, c[4]));
                            TxtUprice.EditValue = Sm.DrStr(dr, c[5]);
                            TxtExcRate.EditValue = Sm.DrStr(dr, c[6]);
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtUprice, "Unit price",true) ||
                Sm.IsTxtEmpty(TxtExcRate, "Exchange rate", true);
               
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event
        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtItCode);
        }
        private void TxtItName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtItName);
        }

        private void TxtBatchNo_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtBatchNo);
        }

        private void TxtSource_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtSource);
        }

        private void LueCurCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCurCode, new Sm.RefreshLue1(Sl.SetLueCurCode));
        }

        private void TxtUprice_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtUprice, 0);
        }

        private void TxtExcRate_Validated(object sender, EventArgs e)
        {
            Sm.FormatNumTxt(TxtExcRate, 0);
        }
        private void MeeRemark_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(MeeRemark);
        }
        #endregion

        #endregion


    }
}
