﻿#region Update
/*
    13/12/2019 [WED/YK] VdCode di pass ke form utama
    03/09/2020 [TKG/TWC] Berdasarkan parameter IsPORequestCreateNewQtDisabled,  feature untuk membuat vendor quotation yg baru diaktifkan atau tidak.
    12/07/2021 [WED/PADI] tambah tombol e-catalog berdasarkan parameter IsUseECatalog
    10/08/2021 [WED/PADI] menampilkan dan passing qty ke main form berdasarkan parameter IsUseECatalog
    12/11/2021 [VIN/PHT] Lup QT show data bukan insert
    18/11/2021 [ICA/IOK+KSM] Bug : Lup Qt bisa insert berdasarkan parameter IsPORQuotationOnlyShowData
    30/11/2021 [WED/RM] quotation yang bisa ditarik hanya quotation yang HiddenInd nya N, berdasarkan parameter IsUseECatalog.
                        jika quotation nya dibuat di runmarket web, umumnya HiddenInd nya Y. 
                        karna quotation baru bisa dibuka/diakses sesuai tanggal procurement di menu RFQ
    27/02/2022 [WED/SIER] tambah code designer untuk BtnClose, karena sebelumnya malah ter replace dengan BtnECatalog
    11/04/2023 [ISN/SIER] penyesuaian filter vendor berdasarkan parameter IsVendorQuotationValidatedByVendorActInd
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPORequestDlg2 : RunSystem.FrmBase4
    {
        #region Field

        private FrmPORequest mFrmParent;
        private string mSQL = string.Empty;
        private int mCurRow;
        private bool mIsPORequestCreateNewQtDisabled = false;

        #endregion

        #region Constructor

        public FrmPORequestDlg2(FrmPORequest FrmParent, int CurRow)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mCurRow = CurRow;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            GetParameter();

            LblQt.Visible = !mIsPORequestCreateNewQtDisabled;
            TxtItCode.Text = Sm.GetGrdStr(mFrmParent.Grd1, mCurRow, 11);
            TxtItName.Text = Sm.GetGrdStr(mFrmParent.Grd1, mCurRow, 13);
            BtnECatalog.Visible = mFrmParent.mIsUseECatalog && mFrmParent.mECatalogWebURI.Length > 0;

            Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -360);
            SetGrd();
            SetSQL();
            if (mFrmParent.mIsVendorQuotationValidatedByVendorActInd)
            {
                Sl.SetLueVdCode2(ref LueVdCode, string.Empty);
            }
            else
            {
                Sl.SetLueVdCode(ref LueVdCode);
            }
            Sl.SetLuePtCode(ref LuePtCode);
            ShowData();
        }

        #endregion

        #region Standard Method

        private void GetParameter()
        {
            mIsPORequestCreateNewQtDisabled = Sm.GetParameterBoo("IsPORequestCreateNewQtDisabled");
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 13;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "Document#",
                        "DNo",
                        "",
                        "Date",
                        "Vendor Code",

                        //6-10
                        "Vendor",
                        "Term of Payment",
                        "Currency",
                        "Unit Price",
                        "Delivery Type",
                        
                        //11-12
                        "Remark",
                        "Quantity"+Environment.NewLine+"Capability"
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 3 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12 });
            Sm.GrdFormatDec(Grd1, new int[] { 9, 12 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 4 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 4, 5 }, false);
            if (!mFrmParent.mIsUseECatalog) Sm.GrdColInvisible(Grd1, new int[] { 12 });
            else Grd1.Cols[12].Move(9);
            if (mIsPORequestCreateNewQtDisabled) Sm.GrdColInvisible(Grd1, new int[] { 3 }, false);
            Sm.SetGrdProperty(Grd1 , true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdAutoSize(Grd1);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, B.DNo, A.DocDt, A.VdCode, C.VdName, D.PtName, F.DTName, A.CurCode, B.UPrice, B.Remark ");
            if (mFrmParent.mIsUseECatalog) SQL.AppendLine(", B.Qty ");
            else SQL.AppendLine(", 0.00 As Qty ");
            SQL.AppendLine("From TblQtHdr A ");
            SQL.AppendLine("Inner Join TblQtDtl B On A.DocNo=B.DocNo And B.ItCode=@ItCode And B.ActInd='Y' ");
            if (mFrmParent.mIsUseECatalog) SQL.AppendLine("    And A.HiddenInd = 'N' ");
            SQL.AppendLine("Inner Join TblVendor C On A.VdCode=C.VdCode And C.ActInd='Y' ");
            SQL.AppendLine("Left Join TblPaymentTerm D On A.PtCode=D.PtCode ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select T1.VdCode, T2.ItCode, ");
            SQL.AppendLine("    Max(Concat(T1.DocDt, T1.DocNo, T2.DNo)) As Key1  ");
            SQL.AppendLine("    From TblQtHdr T1, TblQtDtl T2 ");
            SQL.AppendLine("    Where T1.DocNo=T2.DocNo And T2.ActInd='Y' And T1.Status='A' ");
            if (mFrmParent.mIsGroupPaymentTermActived)
            {
                SQL.AppendLine("And T1.PtCode Is Not Null ");
                SQL.AppendLine("And T1.PtCode In (");
                SQL.AppendLine("    Select PtCode From TblGroupPaymentTerm ");
                SQL.AppendLine("    Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            SQL.AppendLine("    Group By T1.VdCode, T2.ItCode ");
            SQL.AppendLine("    ) E On Concat(A.DocDt, B.DocNo, B.DNo)=E.Key1 And A.VdCode=E.VdCode And B.ItCode=E.ItCode ");
            SQL.AppendLine("Left Join TblDeliveryType F On A.DTCode=F.DTCode ");
            SQL.AppendLine("Where A.Status='A' ");
            if (mFrmParent.mIsGroupPaymentTermActived)
            {
                SQL.AppendLine("And A.PtCode Is Not Null ");
                SQL.AppendLine("And A.PtCode In (");
                SQL.AppendLine("    Select PtCode From TblGroupPaymentTerm ");
                SQL.AppendLine("    Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }
            
            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " ";
                    //" And Concat(A.DocNo, B.DNo) Not In (" + mFrmParent.GetSelectedQt() + ") ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(mFrmParent.Grd1, mCurRow, 11));
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDt");
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueVdCode), "A.VdCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LuePtCode), "A.PtCode", true);

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By A.DocNo, B.DNo",
                    new string[] 
                    { 
                        //0
                        "DocNo",
                        
                        //1-5
                        "DNo", "DocDt", "VdCode", "VdName", "PtName",  
                        
                        //6-10
                        "CurCode", "UPrice", "DTName", "Remark", "Qty"
                        
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd1.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("D", Grd1, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 4);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 5);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 8, 6);
                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 9, 7);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 10, 8);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 11, 9);
                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 12, 10);
                    }, true, false, false, true
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                int Row1 = mCurRow, Row2 = Grd1.CurRow.Index;
            
                Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 19, Grd1, Row2, 1);
                Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 20, Grd1, Row2, 2);
                Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 22, Grd1, Row2, 4);
                Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 23, Grd1, Row2, 6);
                Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 27, Grd1, Row2, 7);
                Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 24, Grd1, Row2, 8);
                Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 25, Grd1, Row2, 9);
                mFrmParent.ComputeTotal(Row1);
                Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 28, Grd1, Row2, 10);
                Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 46, Grd1, Row2, 5);
                if (mFrmParent.mIsUseECatalog)
                {
                    Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 14, Grd1, Row2, 12);
                    Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 52, Grd1, Row2, 12);
                }
                this.Close();
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length != 0)
            {
                e.DoDefault = false;
                var MenuCode = Sm.GetValue("Select MenuCode From TblMenu Where Param='FrmQt' Limit 1;");
                var f = new FrmQt(MenuCode);
                f.Tag = MenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mVdCode = Sm.GetGrdStr(Grd1, e.RowIndex, 5);
                f.mItCode = Sm.GetGrdStr(mFrmParent.Grd1, mCurRow, 11);
                f.ShowDialog();
                ShowData();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length != 0)
            {
                var MenuCode = Sm.GetValue("Select MenuCode From TblMenu Where Param='FrmQt' Limit 1;");
                var f = new FrmQt(MenuCode);
                f.Tag = MenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                if (!mFrmParent.mIsPORQuotationOnlyShowData)
                {
                    f.mDocNo = string.Empty;
                    f.mVdCode = Sm.GetGrdStr(Grd1, e.RowIndex, 5);
                    f.mItCode = Sm.GetGrdStr(mFrmParent.Grd1, mCurRow, 11);
                }
                else
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
                ShowData();
            }
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Date");
        }

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            if (mFrmParent.mIsVendorQuotationValidatedByVendorActInd)
            {
                Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue2(Sl.SetLueVdCode2), string.Empty);
            }
            else
            {
                Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue1(Sl.SetLueVdCode));
            }
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkVdCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Vendor");
        }

        private void LuePtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePtCode, new Sm.RefreshLue1(Sl.SetLuePtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkPtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Term of payment");
        }

        private void LblQt_Click(object sender, EventArgs e)
        {
            var MenuCode = Sm.GetValue("Select MenuCode From TblMenu Where Param='FrmQt' Limit 1;");
            var f = new FrmQt(MenuCode);
            f.Tag = MenuCode;
            f.WindowState = FormWindowState.Normal;
            f.StartPosition = FormStartPosition.CenterScreen;
            f.mItCode = Sm.GetGrdStr(mFrmParent.Grd1, mCurRow, 11);
            f.ShowDialog();
            ShowData();
        }


        #endregion

        #region Button Click

        private void BtnECatalog_Click(object sender, EventArgs e)
        {
            if (mFrmParent.mIsUseECatalog && mFrmParent.mECatalogWebURI.Length > 0)
            {
                string target = mFrmParent.mECatalogWebURI;

                try
                {
                    System.Diagnostics.Process.Start(target);
                }
                catch (System.ComponentModel.Win32Exception noBrowser)
                {
                    if (noBrowser.ErrorCode == -2147467259)
                        MessageBox.Show(noBrowser.Message);
                }
                catch (System.Exception other)
                {
                    MessageBox.Show(other.Message);
                }
            }
        }

        #endregion

        #endregion

    }
}
