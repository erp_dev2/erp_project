﻿#region Update
/*
    27/08/2018 [TKG] tambah otomatis menghitung total qty packaging dan total quantity
    01/11/2019 [WED/IMS] tambah DR+SO Contract
    29/11/2019 [DITA/IMS] tambah kolom project code, project name, ntp docno, customer po no
    06/01/2020 [WED/IMS] nilai tax SO Contract dibenahi
    10/07/2020 [IBL/KSM] Menyambungkan dengan DOCt Based On Sales Contract
    20/04/2021 [WED/SRN] ketika parameter SalesInvoiceTaxCalculationFormula nilainya 2, maka amount before tax akan terisi dari kalkulasi uprice bef tax * qty
    17/06/2021 [IQA/SIER] menambahkan parameter mIsItCtFilteredByGroup buat ngefilter data berdasarkan grup login
    23/06/2021 [WED/SIER] pembulatan kebawah berdasarkan parameter IsDOCtAmtRounded
    16/01/2022 [WED/GSS] union ke sales contract diberi parameter IsSalesContractEnabled
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmSalesInvoiceDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmSalesInvoice mFrmParent;
        string mSQL = string.Empty, mCtCode = string.Empty, mMenuCode="XXX";

        #endregion

        #region Constructor

        public FrmSalesInvoiceDlg(FrmSalesInvoice FrmParent, string CtCode)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mCtCode = CtCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -30);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select * From (");
            SQL.AppendLine("    Select '1' As DocType, ");
            SQL.AppendLine("    A.DocNo, B.DNo, A.DocDt, ");
            SQL.AppendLine("    A.DRDocNo As DRPLDocNo, B.DRDNo As DRPLDNo, ");
            SQL.AppendLine("    C.SODocNo, C.SODNo, ");
            SQL.AppendLine("    D.CtQtDocNo, E.CtQtDNo, ");
            SQL.AppendLine("    I.ItCode, K.ItName, ");
            SQL.AppendLine("    C.QtyPackagingUnit, E.PackagingUnitUomCode, ");
            SQL.AppendLine("    If(C.QtyInventory=0, 0, (B.Qty/C.QtyInventory)*C.Qty) As Qty, H.PriceUomCode, ");
            SQL.AppendLine("    H.CurCode, ");
            SQL.AppendLine("    (G.UPrice-(G.UPrice*0.01*IfNull(J.DiscRate, 0))) As UPriceBeforeTax, ");
            SQL.AppendLine("    E.TaxRate, ");
            SQL.AppendLine("    ((G.UPrice-(G.UPrice*0.01*IfNull(J.DiscRate, 0)))*0.01*E.TaxRate) As TaxAmt, ");
            SQL.AppendLine("    L.PtDay, M.LocalDocNo, N.CtItName, ");
            SQL.AppendLine("    NULL As ProjectCode, NULL As ProjectName, NULL As PONo, ");
            SQL.AppendLine("    Null As SiteCode ");
            SQL.AppendLine("    From TblDOCt2Hdr A ");
            SQL.AppendLine("    Inner Join TblDOCt2Dtl2 B On A.DocNo=B.DocNo And B.Qty>0 And B.ProcessInd='O' ");
            SQL.AppendLine("    Inner Join TblDRDtl C On A.DRDocNo=C.DocNo And B.DRDNo=C.DNo ");
            SQL.AppendLine("    Inner Join TblSOHdr D On C.SODocNo=D.DocNo ");
            SQL.AppendLine("    Inner Join TblSODtl E On C.SODocNo=E.DocNo And C.SODNo=E.DNo ");
            SQL.AppendLine("    Inner Join TblCtQtHdr F On D.CtQtDocNo=F.DocNo ");
            SQL.AppendLine("    Inner Join TblCtQtDtl G On D.CtQtDocNo=G.DocNo And E.CtQtDNo=G.DNo ");
            SQL.AppendLine("    Inner Join TblItemPriceHdr H On G.ItemPriceDocNo=H.DocNo ");
            SQL.AppendLine("    Inner Join TblItemPriceDtl I On G.ItemPriceDocNo=I.DocNo And G.ItemPriceDNo=I.DNo ");
            SQL.AppendLine("    Left Join TblSOQuotPromoItem J On D.SOQuotPromoDocNo=J.DocNo And I.ItCode=J.ItCode ");
            SQL.AppendLine("    Inner Join TblItem K On I.ItCode=K.ItCode ");
            SQL.AppendLine("    Left Join TblPaymentTerm L On F.PtCode=L.PtCode ");
            SQL.AppendLine("    Inner Join TblDRHdr M On A.DRDocNo=M.DocNo ");
            SQL.AppendLine("    Left Join TblCustomerItem N On F.CtCode = N.CtCode And K.ItCode = N.ItCode ");
            SQL.AppendLine("    Where A.CtCode=@CtCode ");
            if (mFrmParent.mIsItCtFilteredByGroup)
            {
                SQL.AppendLine("And A.DocNo Not In ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select Distinct T1.DocNo ");
                SQL.AppendLine("    From TblDOCt2Hdr T1 ");
                SQL.AppendLine("    Inner Join TblDOCt2Dtl2 T2 On T1.DocNo = T2.DocNo ");
                SQL.AppendLine("        And T1.CtCode = @CtCode And T2.Qty > 0 And T2.ProcessInd = 'O' ");
                SQL.AppendLine("    Inner Join TblDRDtl T3 On T1.DRDocNo = T3.DocNo And T2.DRDNo = T3.DNo ");
                SQL.AppendLine("    Inner Join TblSODtl T4 On T3.SODocNo = T4.DocNo And T3.SODNo = T4.DNo ");
                SQL.AppendLine("    Inner Join TblSOHdr T5 On T4.DocNo = T5.DocNo ");
                SQL.AppendLine("    Inner Join TblCtQtDtl T6 On T5.CtQtDocNo = T6.DocNo And T4.CtQtDNo = T6.DNo ");
                SQL.AppendLine("    Inner Join TblItemPriceDtl T7 On T6.ItemPriceDocNo = T7.DocNo And T6.ItemPriceDNo = T7.DNo ");
                SQL.AppendLine("    Inner Join TblItem T8 On T7.ItCode = T8.ItCode ");
                SQL.AppendLine("        And T8.ItCtCode Not In ");
                SQL.AppendLine("        ( ");
                SQL.AppendLine("            Select ItCtCode ");
                SQL.AppendLine("            From TblGroupItemCategory ");
                SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode = @UserCode) ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("Union All ");
            SQL.AppendLine("    Select '2' As DocType, ");
            SQL.AppendLine("    A.DocNo, B.DNo, A.DocDt, ");
            SQL.AppendLine("    A.PLDocNo As DRPLDocNo, B.PLDNo As DRPLDNo, ");
            SQL.AppendLine("    C.SODocNo, C.SODNo, ");
            SQL.AppendLine("    D.CtQtDocNo, E.CtQtDNo, ");
            SQL.AppendLine("    I.ItCode, K.ItName, ");
            SQL.AppendLine("    C.QtyPackagingUnit, E.PackagingUnitUomCode, ");
            SQL.AppendLine("    If(C.QtyInventory=0, 0, (B.Qty/C.QtyInventory)*C.Qty) As Qty, H.PriceUomCode, ");
            SQL.AppendLine("    H.CurCode, ");
            SQL.AppendLine("    (G.UPrice-(G.UPrice*0.01*IfNull(J.DiscRate, 0))) As UPriceBeforeTax, ");
            SQL.AppendLine("    E.TaxRate, ");
            SQL.AppendLine("    ((G.UPrice-(G.UPrice*0.01*IfNull(J.DiscRate, 0)))*0.01*E.TaxRate) As TaxAmt, ");
            SQL.AppendLine("    L.PtDay, M.LocalDocNo, N.CtItName, ");
            SQL.AppendLine("    NULL As ProjectCode, NULL As ProjectName, NULL As PONo, ");
            SQL.AppendLine("    Null As SiteCode ");
            SQL.AppendLine("    From TblDOCt2Hdr A ");
            SQL.AppendLine("    Inner Join TblDOCt2Dtl3 B On A.DocNo=B.DocNo And B.Qty>0 And B.ProcessInd='O' ");
            SQL.AppendLine("    Inner Join TblPLDtl C On A.PLDocNo=C.DocNo And B.PLDNo=C.DNo ");
            SQL.AppendLine("    Inner Join TblSOHdr D On C.SODocNo=D.DocNo ");
            SQL.AppendLine("    inner Join TblSODtl E On C.SODocNo=E.DocNo And C.SODNo=E.DNo ");
            SQL.AppendLine("    Inner Join TblCtQtHdr F On D.CtQtDocNo=F.DocNo ");
            SQL.AppendLine("    Inner Join TblCtQtDtl G On D.CtQtDocNo=G.DocNo And E.CtQtDNo=G.DNo ");
            SQL.AppendLine("    Inner Join TblItemPriceHdr H On G.ItemPriceDocNo=H.DocNo ");
            SQL.AppendLine("    Inner Join TblItemPriceDtl I On G.ItemPriceDocNo=I.DocNo And G.ItemPriceDNo=I.DNo ");
            SQL.AppendLine("    Left Join TblSOQuotPromoItem J On D.SOQuotPromoDocNo=J.DocNo And I.ItCode=J.ItCode ");
            SQL.AppendLine("    Inner Join TblItem K On C.ItCode=K.ItCode ");
            SQL.AppendLine("    Left Join TblPaymentTerm L On F.PtCode=L.PtCode ");
            SQL.AppendLine("    Inner Join TblPLHdr M On A.PLDocNo=M.DocNo ");
            SQL.AppendLine("    Left Join TblCustomerItem N On F.CtCode = N.CtCode And K.ItCode = N.ItCode ");
            SQL.AppendLine("    Where A.CtCode=@CtCode ");
            if (mFrmParent.mIsItCtFilteredByGroup)
            {
                SQL.AppendLine("And A.DocNo Not In ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select Distinct T1.DocNo ");
                SQL.AppendLine("    From TblDOCt2Hdr T1 ");
                SQL.AppendLine("    Inner Join TblDOCt2Dtl3 T2 On T1.DocNo = T2.DocNo ");
                SQL.AppendLine("        And T1.CtCode = @CtCode And T2.Qty > 0 And T2.ProcessInd = 'O' ");
                SQL.AppendLine("    Inner Join TblPLDtl T3 On T1.PLDocNo = T3.DocNo And T2.PLDNo = T3.DNo ");
                SQL.AppendLine("    Inner Join TblSODtl T4 On T3.SODocNo = T4.DocNo And T3.SODNo = T4.DNo ");
                SQL.AppendLine("    Inner Join TblSOHdr T5 On T4.DocNo = T5.DocNo ");
                SQL.AppendLine("    Inner Join TblCtQtDtl T6 On T5.CtQtDocNo = T6.DocNo And T4.CtQtDNo = T6.DNo ");
                SQL.AppendLine("    Inner Join TblItemPriceDtl T7 On T6.ItemPriceDocNo = T7.DocNo And T6.ItemPriceDNo = T7.DNo ");
                SQL.AppendLine("    Inner Join TblItem T8 On T7.ItCode = T8.ItCode ");
                SQL.AppendLine("        And T8.ItCtCode Not In ");
                SQL.AppendLine("        ( ");
                SQL.AppendLine("            Select ItCtCode ");
                SQL.AppendLine("            From TblGroupItemCategory ");
                SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode = @UserCode) ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("Union All ");
            SQL.AppendLine("    Select '1' As DocType, ");
            SQL.AppendLine("    A.DocNo, B.DNo, A.DocDt, ");
            SQL.AppendLine("    A.DRDocNo As DRPLDocNo, B.DRDNo As DRPLDNo, ");
            SQL.AppendLine("    C.SODocNo, C.SODNo, ");
            SQL.AppendLine("    D.BOQDocNo CtQtDocNo, '001' As CtQtDNo, ");
            SQL.AppendLine("    E.ItCode, K.ItName, ");
            SQL.AppendLine("    C.QtyPackagingUnit, E.PackagingUnitUomCode, ");
            SQL.AppendLine("    If(C.QtyInventory=0, 0, (B.Qty/C.QtyInventory)*C.Qty) As Qty, E.PackagingUnitUomCode As PriceUomCode, ");
            SQL.AppendLine("    F.CurCode, ");
            SQL.AppendLine("    E.UPriceBefTax As UPriceBeforeTax, ");
            SQL.AppendLine("    E.TaxRate, ");
            SQL.AppendLine("    E.TaxAmt, ");
            SQL.AppendLine("    L.PtDay, M.LocalDocNo, N.CtItName, ");
            if (mFrmParent.mIsBOMShowSpecifications)
                SQL.AppendLine("   O.ProjectCode, O.ProjectName, O.PONo, ");
            else
                SQL.AppendLine("   NULL As ProjectCode, NULL As ProjectName, NULL As PONo, ");
            SQL.AppendLine("    Null As SiteCode ");
            SQL.AppendLine("    From TblDOCt2Hdr A ");
            SQL.AppendLine("    Inner Join TblDOCt2Dtl2 B On A.DocNo=B.DocNo And B.Qty>0 And B.ProcessInd='O' ");
            SQL.AppendLine("    Inner Join TblDRDtl C On A.DRDocNo=C.DocNo And B.DRDNo=C.DNo ");
            SQL.AppendLine("    Inner Join TblSOContractHdr D On C.SODocNo=D.DocNo ");
            SQL.AppendLine("    Inner Join TblSOContractDtl E On C.SODocNo=E.DocNo And C.SODNo=E.DNo ");
            SQL.AppendLine("    Inner Join TblBOQHdr F On D.BOQDocNo = F.DocNo ");
            SQL.AppendLine("    Inner Join TblItem K On E.ItCode=K.ItCode ");
            SQL.AppendLine("    Left Join TblPaymentTerm L On F.PtCode=L.PtCode ");
            SQL.AppendLine("    Inner Join TblDRHdr M On A.DRDocNo=M.DocNo ");
            SQL.AppendLine("    Left Join TblCustomerItem N On F.CtCode = N.CtCode And K.ItCode = N.ItCode ");
            if (mFrmParent.mIsBOMShowSpecifications)
            {
                SQL.AppendLine(" Left Join ");
                SQL.AppendLine(" ( ");
                SQL.AppendLine(" Select T0.DocNo, Group_Concat( Distinct IFNULL(T5.ProjectCode, T2.ProjectCode2)) ProjectCode,  ");
                SQL.AppendLine("	Group_Concat( Distinct IFNULL(T5.ProjectName, T4.ProjectName)) ProjectName, Group_Concat( Distinct T2.PONo) PONo ");
                SQL.AppendLine(" From TblDOCt2Hdr T0  ");
                SQL.AppendLine(" Inner Join TblDRDtl T1 ON T0.DRDocNo = T1.DocNo ");
                SQL.AppendLine(" Inner Join TblSOContractHdr T2 ON T1.SODocNo = T2.DocNo ");
                SQL.AppendLine(" Inner Join TblBOQHdr T3 ON T2.BOQDocNo = T3.DocNo ");
                SQL.AppendLine(" Inner Join TblLOPHdr T4 ON T3.LOPDocNo = T4.DocNo ");
                SQL.AppendLine(" Left Join TblProjectGroup T5 ON T4.PGCode = T5.PGCode ");
                SQL.AppendLine(" Group By T0.DocNo ");
                SQL.AppendLine("  ) O ON O.DocNo = A.DocNo ");
            }
            SQL.AppendLine("    Where A.CtCode=@CtCode ");
            if (mFrmParent.mIsItCtFilteredByGroup)
            {
                SQL.AppendLine("And A.DocNo Not In ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select Distinct T1.DocNo ");
                SQL.AppendLine("    From TblDOCt2Hdr T1 ");
                SQL.AppendLine("    Inner Join TblDOCt2Dtl2 T2 On T1.DocNo = T2.DocNo ");
                SQL.AppendLine("        And T1.CtCode = @CtCode And T2.Qty > 0 And T2.ProcessInd = 'O' ");
                SQL.AppendLine("    Inner Join TblDRDtl T3 On T1.DRDocNo = T3.DocNo And T2.DRDNo = T3.DNo ");
                SQL.AppendLine("    Inner Join TblSOContractDtl T4 On T3.SODocNo = T4.DocNo And T3.SODNo = T4.DNo ");
                SQL.AppendLine("    Inner Join TblItem T8 On T4.ItCode = T8.ItCode ");
                SQL.AppendLine("        And T8.ItCtCode Not In ");
                SQL.AppendLine("        ( ");
                SQL.AppendLine("            Select ItCtCode ");
                SQL.AppendLine("            From TblGroupItemCategory ");
                SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode = @UserCode) ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine(") ");
            }
            if (mFrmParent.mIsSalesContractEnabled)
            {
                SQL.AppendLine("Union All ");
                SQL.AppendLine("    SELECT '1' AS DocType, ");
                SQL.AppendLine("    A.DocNo, B.DNo, A.DocDt, ");
                SQL.AppendLine("    A.DRDocNo AS DRPLDocNo, B.DRDNo AS DRPLDNo, ");
                SQL.AppendLine("    C.SCDocNo AS SODocNo, '001' AS SODNo, ");
                SQL.AppendLine("    D.SalesMemoDocNo AS CtQtDocNo, F.DNo CtQtDNo, ");
                SQL.AppendLine("    G.ItCode, G.ItName, ");
                SQL.AppendLine("    C.QtyPackagingUnit, G.SalesUoMCode AS QtyPackagingUnitUoMCode, ");
                SQL.AppendLine("    C.Qty, G.SalesUoMCode AS PriceUomCode, ");
                SQL.AppendLine("    E.CurCode, ");
                SQL.AppendLine("    case ");
                SQL.AppendLine("        when E.TaxInd = 'Y' then F.UPrice/1.1 ");
                SQL.AppendLine("        when E.TaxInd = 'N' then F.UPrice ");
                SQL.AppendLine("    end ");
                SQL.AppendLine("    AS UPriceBeforeTax, ");
                SQL.AppendLine("    case ");
                SQL.AppendLine("        when E.TaxInd = 'Y' then 10 ");
                SQL.AppendLine("        when E.TaxInd = 'N' then 0 ");
                SQL.AppendLine("    end ");
                SQL.AppendLine("    AS TaxRate, ");
                SQL.AppendLine("    case ");
                SQL.AppendLine("        when E.TaxInd = 'Y' then F.UPrice - (F.UPrice/1.1) ");
                SQL.AppendLine("        when E.TaxInd = 'N' then 0 ");
                SQL.AppendLine("    end ");
                SQL.AppendLine("    AS TaxAmt, ");
                SQL.AppendLine("    H.PtDay, E.LocalDocNo, I.CtItName, ");
                SQL.AppendLine("    NULL As ProjectCode, NULL As ProjectName, NULL As PONo, ");
                SQL.AppendLine("    E.SiteCode ");
                SQL.AppendLine("    FROM TblDOCt2Hdr A ");
                SQL.AppendLine("    INNER JOIN TblDOCt2Dtl2 B ON A.DocNo = B.DocNo AND B.Qty > 0 AND B.ProcessInd = 'O' ");
                SQL.AppendLine("    INNER JOIN TblDRDtl C ON A.DRDocNo = C.DocNo AND B.DRDNo = C.DNo ");
                SQL.AppendLine("    INNER JOIN TblSalesContract D ON C.SCDocNo = D.DocNo ");
                SQL.AppendLine("    INNER JOIN TblSalesMemoHdr E ON C.SODocNo = E.DocNo ");
                SQL.AppendLine("    INNER JOIN TblSalesMemoDtl F ON E.DocNo = F.DocNo AND C.SODNo = F.DNo ");
                SQL.AppendLine("    INNER JOIN TblItem G ON G.ItCode = F.ItCode ");
                SQL.AppendLine("    LEFT JOIN TblPaymentTerm H ON D.PtCode = H.PtCode ");
                SQL.AppendLine("    LEFT JOIN TblCustomerItem I ON E.CtCode = I.CtCode ");
                SQL.AppendLine("    Where A.CtCode = @CtCode ");
            }
            if (mFrmParent.mIsItCtFilteredByGroup)
            {
                SQL.AppendLine("And A.DocNo Not In ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select Distinct T1.DocNo ");
                SQL.AppendLine("    From TblDOCt2Hdr T1 ");
                SQL.AppendLine("    Inner Join TblDOCt2Dtl2 T2 On T1.DocNo = T2.DocNo ");
                SQL.AppendLine("        And T1.CtCode = @CtCode And T2.Qty > 0 And T2.ProcessInd = 'O' ");
                SQL.AppendLine("    Inner Join TblDRDtl T3 On T1.DRDocNo = T3.DocNo And T2.DRDNo = T3.DNo ");
                SQL.AppendLine("    Inner Join TblSalesMemoDtl T4 On T3.SODocNo = T4.DocNo And T3.SODNo = T4.DNo ");
                SQL.AppendLine("    Inner Join TblItem T8 On T4.ItCode = T8.ItCode ");
                SQL.AppendLine("        And T8.ItCtCode Not In ");
                SQL.AppendLine("        ( ");
                SQL.AppendLine("            Select ItCtCode ");
                SQL.AppendLine("            From TblGroupItemCategory ");
                SQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode = @UserCode) ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine(") T ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 36;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                         //1-5
                        "",
                        "DO#",
                        "DO D#",
                        "",
                        "DO" + Environment.NewLine + "Date",

                        //6-10
                        "DO Request#" + Environment.NewLine + "/ PL#",
                        "DO Request D#" + Environment.NewLine + "/ PL D#",
                        "",
                        "SO#",
                        "SO D#",
                        
                        //11-15
                        "",
                        "Quotation#",
                        "Quotation D#",
                        "",
                        "Item Code",
                        
                        //16-20
                        "",
                        "Item Name",
                        "Packaging" + Environment.NewLine + "Quantity",
                        "Packaging" + Environment.NewLine + "UoM",
                        "Quantity",

                        //21-25
                        "Sales" + Environment.NewLine + "UoM",
                        "Currency",
                        "Unit Price" + Environment.NewLine + "(Before Tax)",
                        "Tax"+ Environment.NewLine + "Rate",
                        "Tax"+ Environment.NewLine + "Amount",
                        
                        //26-30
                        "Unit Price" + Environment.NewLine + "(After Tax)",
                        "Amount",
                        "TOP",
                        "Local" + Environment.NewLine + "Document",
                        "Type",

                        //31-35
                        "Customer"+Environment.NewLine+"Item's Name",
                        "Project Code",
                        "Project Name",
                        "Customer PO#",
                        "Site Code"
                    },
                     new int[] 
                    {
                        //0
                        50, 

                        //1-5
                        20, 145, 0, 20, 100,
 
                        //6-10
                        130, 0, 20, 130, 0, 
                        
                        //11-15
                        20, 130, 0, 20, 80, 
                        
                        //16-20
                        20, 200, 100, 80, 100, 

                        //21-25
                        80, 80, 130, 100, 100, 

                        //26-30
                        130, 130, 150, 150, 0,

                        //31-35
                        250, 120, 200, 120, 0
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColButton(Grd1, new int[] { 4, 8, 11, 14, 16 });
            Sm.GrdFormatDec(Grd1, new int[] { 18, 20, 23, 24, 25, 26, 27, 28 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 5 });
            Grd1.Cols[31].Move(18);
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 24, 28, 30, 35 }, false);
            if (!mFrmParent.mIsCustomerItemNameMandatory)
                Sm.GrdColInvisible(Grd1, new int[] { 31 });
            if (!mFrmParent.mIsBOMShowSpecifications) Sm.GrdColInvisible(Grd1, new int[] { 32, 33, 34 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 3, 5, 6, 7, 9, 10, 12, 13, 15, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 4, 5, 6, 8, 9, 11, 12, 14, 15, 16, 24 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@CtCode", mCtCode);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "DocNo", false);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "DocDt");
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "ItCode", "ItName" });
                
                Sm.ShowDataInGrid(
                    ref Grd1, ref cm, mSQL + Filter + " Order By DocDt, DocNo;",
                    new string[] 
                    { 
                        //0
                        "DocNo", 
                        
                        //1-5
                        "DNo", "DocDt", "DRPLDocNo", "DRPLDNo", "SODocNo", 
                        
                        //6-10
                        "SODNo", "CtQtDocNo", "CtQtDNo", "ItCode", "ItName",  

                        //11-15
                        "QtyPackagingUnit", "PackagingUnitUomCode", "Qty", "PriceUomCode", "CurCode",

                        //16-20
                        "UPriceBeforeTax", "TaxRate", "TaxAmt", "PtDay", "LocalDocNo", 
                        
                        //21-25
                        "DocType", "CtItName", "ProjectCode", "ProjectName", "PONo",

                        //26
                        "SiteCode"
                        
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd1.Cells[Row, 0].Value = Row + 1;
                        Grd1.Cells[Row, 1].Value = false;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 5, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 10);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 12);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 13);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 14);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 15);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 23, 16);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 24, 17);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 25, 18);
                        Grd1.Cells[Row, 26].Value = dr.GetDecimal(c[16]) + dr.GetDecimal(c[18]);
                        if (mFrmParent.mIsDOCtAmtRounded)
                            Grd1.Cells[Row, 27].Value = Decimal.Truncate((dr.GetDecimal(c[16]) + dr.GetDecimal(c[18])) * dr.GetDecimal(c[13]));
                        else
                            Grd1.Cells[Row, 27].Value = (dr.GetDecimal(c[16]) + dr.GetDecimal(c[18])) * dr.GetDecimal(c[13]);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 28, 19);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 29, 20);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 30, 21);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 31, 22);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 32, 23);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 33, 24);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 34, 25);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 35, 26);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsDataAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 2, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 3, Grd1, Row2, 3);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 5, Grd1, Row2, 5);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 6, Grd1, Row2, 6);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 7, Grd1, Row2, 7);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 9, Grd1, Row2, 9);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 10, Grd1, Row2, 10);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 12, Grd1, Row2, 12);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 13, Grd1, Row2, 13);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 15, Grd1, Row2, 15);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 17, Grd1, Row2, 17);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 18, Grd1, Row2, 18);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 19, Grd1, Row2, 19);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 20, Grd1, Row2, 20);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 21, Grd1, Row2, 21);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 22, Grd1, Row2, 22);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 23, Grd1, Row2, 23);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 24, Grd1, Row2, 24);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 25, Grd1, Row2, 25);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 26, Grd1, Row2, 26);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 27, Grd1, Row2, 27);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 28, Grd1, Row2, 28);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 29, Grd1, Row2, 29);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 30, Grd1, Row2, 30);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 31, Grd1, Row2, 31);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 36, Grd1, Row2, 35);
                        if (mFrmParent.mSalesInvoiceTaxCalculationFormula != "1")
                            mFrmParent.Grd1.Cells[Row1, 41].Value = Sm.GetGrdDec(Grd1, Row2, 20) * Sm.GetGrdDec(Grd1, Row2, 23);

                        mFrmParent.Grd1.Rows.Add();
                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 18, 20, 23, 24, 25, 26, 27, 28, 40, 41, 42, 43, 44 });
                    }
                }
            }
            mFrmParent.ComputeTotalQtyPackagingUnit();
            mFrmParent.ComputeTotalQty();
            mFrmParent.ComputeAmt();
            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 item.");
        }

        private bool IsDataAlreadyChosen(int Row)
        {
            string key = Sm.GetGrdStr(Grd1, Row, 29) + Sm.GetGrdStr(Grd1, Row, 2) + Sm.GetGrdStr(Grd1, Row, 3);
            for (int Index = 0; Index < mFrmParent.Grd1.Rows.Count - 1; Index++)
                if (Sm.CompareStr(key, 
                        Sm.GetGrdStr(mFrmParent.Grd1, Index, 29)+
                        Sm.GetGrdStr(mFrmParent.Grd1, Index, 2)+
                        Sm.GetGrdStr(mFrmParent.Grd1, Index, 3)
                    ))
                    return true;
            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                if (mFrmParent.mIsSalesContractEnabled)
                {
                    var f = new FrmDOCt7(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                    f.ShowDialog();
                }
                else
                {
                    var f = new FrmDOCt2(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                    f.ShowDialog();
                }
            }

            if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length != 0)
            {
                e.DoDefault = false;
                if (mFrmParent.mIsSalesContractEnabled)
                {
                    var f = new FrmDR3(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                    f.ShowDialog();
                }
                else
                {
                    var f = new FrmDR(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                    f.ShowDialog();
                }
            }

            if (e.ColIndex == 11 && Sm.GetGrdStr(Grd1, e.RowIndex, 9).Length != 0)
            {
                e.DoDefault = false;
                if (mFrmParent.mIsSalesContractEnabled)
                {
                    var f = new FrmSalesContract(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 9);
                    f.ShowDialog();
                }
                else
                {
                    var f = new FrmSO2(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 9);
                    f.ShowDialog();
                }
            }

            if (e.ColIndex == 14 && Sm.GetGrdStr(Grd1, e.RowIndex, 12).Length != 0)
            {
                e.DoDefault = false;
                if (mFrmParent.mIsSalesContractEnabled)
                {
                    var f = new FrmSalesMemo(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 12);
                    f.ShowDialog();
                }
                else
                {
                    var f = new FrmCtQt(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 12);
                    f.ShowDialog();
                }
            }

            if (e.ColIndex == 16 && Sm.GetGrdStr(Grd1, e.RowIndex, 15).Length != 0)
            {
                e.DoDefault = false;
                Sm.ShowItemInfo(mMenuCode, Sm.GetGrdStr(Grd1, e.RowIndex, 15));
            }

        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                if (mFrmParent.mIsSalesContractEnabled)
                {
                    var f = new FrmDOCt7(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                    f.ShowDialog();
                }
                else
                {
                    var f = new FrmDOCt2(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                    f.ShowDialog();
                }
            }

            if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 6).Length != 0)
            {
                if (mFrmParent.mIsSalesContractEnabled)
                {
                    var f = new FrmDR3(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                    f.ShowDialog();
                }
                else
                {
                    var f = new FrmDR(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 6);
                    f.ShowDialog();
                }
            }

            if (e.ColIndex == 11 && Sm.GetGrdStr(Grd1, e.RowIndex, 9).Length != 0)
            {
                if (mFrmParent.mIsSalesContractEnabled)
                {
                    var f = new FrmSalesContract(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 9);
                    f.ShowDialog();
                }
                else
                {
                    var f = new FrmSO2(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 9);
                    f.ShowDialog();
                }
            }

            if (e.ColIndex == 14 && Sm.GetGrdStr(Grd1, e.RowIndex, 12).Length != 0)
            {
                if (mFrmParent.mIsSalesContractEnabled)
                {
                    var f = new FrmSalesMemo(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 12);
                    f.ShowDialog();
                }
                else
                {
                    var f = new FrmCtQt(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 12);
                    f.ShowDialog();
                }
            }

            if (e.ColIndex == 16 && Sm.GetGrdStr(Grd1, e.RowIndex, 15).Length != 0)
                Sm.ShowItemInfo(mMenuCode, Sm.GetGrdStr(Grd1, e.RowIndex, 15));
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Grd1.Rows.Count > 0)
            {
                bool IsSelected = Sm.GetGrdBool(Grd1, 0, 1);
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    Grd1.Cells[Row, 1].Value = !IsSelected;
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Date");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        #endregion

        #endregion
    }
}
