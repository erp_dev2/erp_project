﻿#region Update
/*
    19/07/2018 [TKG] tambah method CloseForm
    22/11/2019 [WED/ALL] ganti logo icon ke RS.ico
    22/01/2022 [WED/ALL] ganti modifier BtnClose menjadi Protected (sebelumnya Private)
*/
#endregion

#region Namespace

using System;
using System.Windows.Forms;
using System.Drawing;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;

#endregion

namespace RunSystem
{
    public partial class FrmBase4 : Form
    {
        #region Disable close button

        private const int CP_NOCLOSE_BUTTON = 0x200;
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams myCp = base.CreateParams;
                myCp.ClassStyle = myCp.ClassStyle | CP_NOCLOSE_BUTTON;
                return myCp;
            }
        }

        #endregion

        #region Constructor

        public FrmBase4()
        {
            InitializeComponent();
        }

        #endregion

        #region Method

        #region From Method

        virtual protected void FrmLoad(object sender, EventArgs e)
        {
            Sm.SetLookUpEdit(LueFontSize, new string[] { "6", "7", "8", "9", "10", "11", "12" });
            Sm.SetLue(LueFontSize, "9");
        }

        #endregion

        #region Standard Method

        virtual protected void SetSQL()
        {
        }

        virtual protected string SetReportName()
        {
            return this.Text;
        }

        virtual protected void ShowData()
        {

        }

        virtual protected void ChooseData()
        {

        }

        virtual protected void CloseForm()
        {
            this.Close();
        }

        virtual protected void PrintData()
        {
            iGSubtotalManager.ForeColor = Color.Black;

            PM1.PageHeader.LeftSection.Text = Environment.NewLine + "Report : " + SetReportName();
            Sm.SetPrintManagerProperty(PM1, Grd1, ChkAutoWidth.Checked);
            iGSubtotalManager.ForeColor = Color.White;
        }

        virtual protected void HideInfoInGrd()
        {

        }

        virtual protected void ExportToExcel()
        {
            Sm.ExportToExcel(Grd1);
        }

        #endregion

        #region Grid Method

        virtual protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {

        }

        virtual protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {

        }

        virtual protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {

        }

        virtual protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            
        }

        virtual protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        virtual protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        virtual protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
                Sm.SetGrdAutoSize(Grd1);
            }
        }

        #endregion

        #endregion

        #region Event

        #region Form Event

        private void FrmBase4_Load(object sender, EventArgs e)
        {
            FrmLoad(sender, e);
        }

        #endregion

        #region Button Event

        private void BtnRefresh_Click(object sender, EventArgs e)
        {
            ShowData();
        }

        private void BtnChoose_Click(object sender, EventArgs e)
        {
            ChooseData();
        }

        private void BtnPrint_Click(object sender, EventArgs e)
        {
            PrintData();
        }

        private void BtnExcel_Click(object sender, EventArgs e)
        {
            //Sm.ExportToExcel(Grd1);
            ExportToExcel();
        }

        private void BtnClose_Click(object sender, EventArgs e)
        {
            CloseForm();
        }

        #endregion

        #region Misc Event

        private void ChkHideInfoInGrd_CheckedChanged(object sender, EventArgs e)
        {
            HideInfoInGrd();
        }

        private void LueFontSize_EditValueChanged(object sender, EventArgs e)
        {
            LueFontSizeEditValueChanged(sender, e);
        }

        #endregion

        #region Grid Event

        private void Grd1_AfterContentsSorted(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, false);
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdTabInLastCell(Grd1, e, BtnRefresh);
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            GrdEllipsisButtonClick(sender, e);
        }

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            GrdRequestEdit(sender, e);
        }


        private void Grd1_CellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            GrdCellDoubleClick(sender, e);
        }

        private void Grd1_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            GrdColHdrDoubleClick(sender, e);
        }

        private void Grd1_AfterContentsGrouped(object sender, EventArgs e)
        {
            GrdAfterContentsGrouped(sender, e);
        }

        private void Grd1_AfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
            GrdAfterRowStateChanged(sender, e);
        }

        #endregion

        #region Print Manager Event

        private void PM1_CustomDrawPageHeader(object sender, TenTec.Windows.iGridLib.Printing.iGCustomDrawPageBandEventArgs e)
        {
            try
            {
                e.Graphics.DrawImage(
                    Image.FromFile(@Sm.CompanyLogo()),
                    //new Rectangle() { Height = 50, Width = Gv.CompanyLogoWidth, X = 100, Y = 50 }
                     new Rectangle() { Height = 50, Width = Gv.CompanyLogoWidth, X = 78, Y = 78 }
                    );

                e.Graphics.DrawString(
                    Gv.CompanyName,
                    new Font("Times New Roman", 12, FontStyle.Regular),
                    SystemBrushes.WindowText,
                    //new Rectangle() { Height = 100, Width = 300, X = 350, Y = 50 });
                    new Rectangle() { Height = 100, Width = 300, X = Gv.CompanyLogoWidth + 85, Y = 90 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void PM1_CustomDrawPageHeaderGetBounds(object sender, TenTec.Windows.iGridLib.Printing.iGCustomDrawPageBandGetBoundsEventArgs e)
        {
            e.Bounds.Height = 20;
        }

        #endregion

        #endregion
    }
}
