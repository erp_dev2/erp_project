﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmProductionWages : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string 
            mMenuCode = "", mAccessInd = "",
            mDocNo = ""; //if this application is called from other application;
        internal int mNumberOfPlanningUomCode = 1;
        internal FrmProductionWagesFind FrmFind;

        #endregion

        #region Constructor

        public FrmProductionWages(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Production Wages";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

                Sl.SetLueProductionWagesSource(ref LueProductionWagesSource);
                Sl.SetLueWagesFormulationCode(ref LueWagesFormulationCode);
                Sl.SetLueCurCode(ref LueCurCode);
                SetNumberOfPlanningUomCode();
                SetGrd();
                SetFormControl(mState.View);
                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, EventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grd1

            Grd1.Cols.Count = 11;
            Grd1.FrozenArea.ColCount = 3;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",

                        //1-5
                        "SFC DNo",
                        "Item's Code",
                        "",
                        "Item's Name",
                        "Quantity",
                        
                        //6-10
                        "Uom",
                        "Quantity 2",
                        "Uom 2",
                        "Index",
                        "Value"
                    },
                     new int[] 
                    {
                        //0
                        0, 

                        //1-5
                        0, 100, 20, 200, 100,  
                        
                        //6-10
                        100, 100, 80, 80, 100 
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 5, 7, 9, 10 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 0, 1, 2, 3, 7, 8 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 4, 5, 6, 7, 8, 9, 10 });
            ShowPlanningUomCode();

            #endregion

            #region Grd2

            Grd2.Cols.Count = 4;

            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[] 
                    {
                        //0
                        "DNo",

                        //1-3
                        "Value",
                        "Wages/Unit",
                        "Amount"
                    },
                     new int[] 
                    {
                        //0
                        0, 

                        //1-3
                        120, 120, 150
                    }
                );
            Sm.GrdFormatDec(Grd2, new int[] { 1, 2, 3 }, 0);
            Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 1, 2, 3 });

            #endregion

            #region Grd3

            Grd3.Cols.Count = 9;

            Sm.GrdHdrWithColWidth(
                    Grd3,
                    new string[] 
                    {
                        //0
                        "DNo",

                        //1-5
                        "Employee Code",
                        "Employee Name",
                        "Grade Level",
                        "Index",
                        "Quantity",
                        
                        //6-8
                        "Wages",
                        "Position",
                        "Department"
                    },
                     new int[] 
                    {
                        //0
                        0, 

                        //1-5
                        0, 200, 120, 100, 80, 
                        
                        //6-8
                        120, 150, 150
                    }
                );
            Sm.GrdFormatDec(Grd3, new int[] { 4, 5, 6 }, 0);
            Sm.GrdColInvisible(Grd3, new int[] { 0, 1, 3 }, false);
            Sm.GrdColReadOnly(true, true, Grd3, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8 });

            #endregion

            #region Grd4

            Grd4.Cols.Count = 9;
            Grd4.ReadOnly = false;

            Sm.GrdHdrWithColWidth(
                    Grd4,
                    new string[] 
                    {
                        //0
                        "DNo",

                        //1-5
                        "",
                        "Employee Code",
                        "Employee Name",
                        "Position",
                        "Department",
                        
                        //6-8
                        "Grade Level",
                        "Index",
                        "Wages"
                    },
                     new int[] 
                    {
                        //0
                        0, 

                        //1-5
                        20, 0, 120, 120, 120, 
                        
                        //6-8
                        120, 80, 100
                    }
                );
            Sm.GrdColButton(Grd4, new int[] { 1 });
            Sm.GrdFormatDec(Grd4, new int[] { 7, 8 }, 0);
            Sm.GrdColInvisible(Grd4, new int[] { 0, 2, 6 }, false);
            Sm.GrdColReadOnly(true, true, Grd4, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8 });

            #endregion
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd3, new int[] { 1, 3 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd4, new int[] { 2, 6 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 

                        TxtDocNo, ChkCancelInd, DteDocDt, TxtWorkCenterDocNo, TxtShopFloorControlDocNo, 
                        DteShopFloorControlDocDt, TxtProductionShiftCode, LueProductionWagesSource, LueWagesFormulationCode, TxtUomCode, 
                        LueCurCode, MeeRemark
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd4, new int[] { 4 });
                    BtnShopFloorControlDocNo1.Enabled = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueWagesFormulationCode, LueCurCode, MeeRemark
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd4, new int[] { 1 });
                    BtnShopFloorControlDocNo1.Enabled = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        ChkCancelInd
                    }, false);
                    ChkCancelInd.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                 TxtDocNo, DteDocDt, TxtWorkCenterDocNo, TxtShopFloorControlDocNo, DteShopFloorControlDocDt, 
                 TxtProductionShiftCode, LueProductionWagesSource, LueWagesFormulationCode, TxtUomCode, LueCurCode, 
                 MeeRemark
            });
            ChkCancelInd.Checked = false;
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 2);
            Sm.FocusGrd(Grd2, 0, 1);
            Sm.FocusGrd(Grd3, 0, 1);
            Sm.FocusGrd(Grd4, 0, 1);
        }

        internal void ClearGrd()
        {
            iGSubtotalManager.HideSubtotals(Grd1);
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 5, 7, 9, 10 });

            iGSubtotalManager.HideSubtotals(Grd2);
            Sm.ClearGrd(Grd2, true);
            Sm.SetGrdNumValueZero(ref Grd2, 0, new int[] { 1, 2, 3 });

            iGSubtotalManager.HideSubtotals(Grd3);
            Sm.ClearGrd(Grd3, true);
            Sm.SetGrdNumValueZero(ref Grd3, 0, new int[] { 4, 5, 6 });

            Sm.ClearGrd(Grd4, true);
            Sm.SetGrdNumValueZero(ref Grd4, 0, new int[] { 7, 8 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmProductionWagesFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);

                DteDocDt.DateTime = Sm.ConvertDate(Sm.ServerCurrentDateTime());
                Sm.SetLue(LueProductionWagesSource, "1");
                Sm.SetLue(LueCurCode, Sm.GetParameter("MainCurCode"));
                BtnShopFloorControlDocNo1_Click(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    CancelData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (IsInsertedDataNotValid() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "ProductionWages", "TblProductionWagesHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveProductionWagesHdr(DocNo));
            
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0) cml.Add(SaveProductionWagesDtl(DocNo, Row));

            for (int Row = 0; Row < Grd2.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd2, Row, 2).Length > 0) cml.Add(SaveProductionWagesDtl2(DocNo, Row));

            for (int Row = 0; Row < Grd3.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd3, Row, 2).Length > 0) cml.Add(SaveProductionWagesDtl3(DocNo, Row));

            for (int Row = 0; Row < Grd4.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd4, Row, 2).Length > 0) cml.Add(SaveProductionWagesDtl4(DocNo, Row));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsTxtEmpty(TxtShopFloorControlDocNo, "Shop floor control#", false) ||
                Sm.IsLueEmpty(LueProductionWagesSource, "Source") ||
                Sm.IsLueEmpty(LueWagesFormulationCode, "Wages formulation") ||
                Sm.IsTxtEmpty(TxtUomCode, "Uom", false) ||
                Sm.IsLueEmpty(LueCurCode, "Currency") ||
                IsGrd4ValueNotValid() ||
                IsGrd4ExceedMaxRecords();
            ;
        }

        private bool IsGrd4ExceedMaxRecords()
        {
            if (Grd4.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Employee data entered (" + (Grd4.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsGrd4ValueNotValid()
        {
            for (int Row = 0; Row < Grd4.Rows.Count - 1; Row++)
                if (Sm.IsGrdValueEmpty(Grd4, Row, 2, false, "Employee is empty.")) return true;
            return false;
        }

        private MySqlCommand SaveProductionWagesHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblProductionWagesHdr(DocNo, DocDt, CancelInd, ShopFloorControlDocNo, ProductionWagesSource, WagesFormulationCode, UomCode, CurCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'N', @ShopFloorControlDocNo, @ProductionWagesSource, @WagesFormulationCode, @UomCode, @CurCode, @Remark, @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Update TblShopFloorControlHdr Set ProcessInd='F' Where ProcessInd='O' And CancelInd='N' And DocNo=@ShopFloorControlDocNo; ");

            var cm = new MySqlCommand(){ CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@ShopFloorControlDocNo", TxtShopFloorControlDocNo.Text);
            Sm.CmParam<String>(ref cm, "@ProductionWagesSource", Sm.GetLue(LueProductionWagesSource));
            Sm.CmParam<String>(ref cm, "@WagesFormulationCode", Sm.GetLue(LueWagesFormulationCode));
            Sm.CmParam<String>(ref cm, "@UomCode", TxtUomCode.Text);
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveProductionWagesDtl(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblProductionWagesDtl(DocNo, DNo, ShopFloorControlDNo, ItIndex, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DNo, @ShopFloorControlDNo, @ItIndex, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@ShopFloorControlDNo", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<Decimal>(ref cm, "@ItIndex", Sm.GetGrdDec(Grd1, Row, 9));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveProductionWagesDtl2(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblProductionWagesDtl2(DocNo, DNo, Value, Wages, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DNo, @Value, @Wages, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<Decimal>(ref cm, "@Value", Sm.GetGrdDec(Grd2, Row, 1));
            Sm.CmParam<Decimal>(ref cm, "@Wages", Sm.GetGrdDec(Grd2, Row, 2));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveProductionWagesDtl3(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblProductionWagesDtl3(DocNo, DNo, EmpCode, GrdLvlIndex, Qty, Wages, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DNo, @EmpCode, @GrdLvlIndex, @Qty, @Wages, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd3, Row, 1));
            Sm.CmParam<Decimal>(ref cm, "@GrdLvlIndex", Sm.GetGrdDec(Grd3, Row, 4));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd3, Row, 5));
            Sm.CmParam<Decimal>(ref cm, "@Wages", Sm.GetGrdDec(Grd3, Row, 6));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveProductionWagesDtl4(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblProductionWagesDtl4(DocNo, DNo, EmpCode, GrdLvlIndex, Wages, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DNo, @EmpCode, @GrdLvlIndex, @Wages, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd4, Row, 2));
            Sm.CmParam<Decimal>(ref cm, "@GrdLvlIndex", Sm.GetGrdDec(Grd4, Row, 7));
            Sm.CmParam<Decimal>(ref cm, "@Wages", Sm.GetGrdDec(Grd4, Row, 8));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Cancel data

        private void CancelData()
        {
            if (IsCancelledDataNotValid() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(CancelShopFloorControlHdr());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsCancelledDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDataCancelledAlready() ||
                IsSFCProcessIndAlreadyOutstanding() ||
                IsDataAlreadyProcessedToPPR();
        }

        private bool IsDataCancelledAlready()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo From TblProductionWagesHdr ");
            SQL.AppendLine("Where CancelInd='Y' And DocNo=@DocNo ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data is already cancelled .");
                return true;
            }

            return false;
        }

        private bool IsSFCProcessIndAlreadyOutstanding()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo From TblShopFloorControlHdr ");
            SQL.AppendLine("Where ProcessInd='O' And DocNo=@DocNo;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtShopFloorControlDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data already cancelled.");
                return true;
            }
            return false;
        }

        private bool IsDataAlreadyProcessedToPPR()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo From TblProductionWagesDtl3 ");
            SQL.AppendLine("Where ProcessInd='F' And DocNo=@DocNo ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select DocNo From TblProductionWagesDtl4 ");
            SQL.AppendLine("Where ProcessInd='F' And DocNo=@DocNo Limit 1;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data already processed to production payrun.");
                return true;
            }
            return false;
        }

        private MySqlCommand CancelShopFloorControlHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblProductionWagesHdr Set ");
            SQL.AppendLine("    CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N'; ");

            SQL.AppendLine("Update TblShopFloorControlHdr Set ");
            SQL.AppendLine("    ProcessInd='O' ");
            SQL.AppendLine("Where DocNo=@ShopFloorControlDocNo And CancelInd='N'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@ShopFloorControlDocNo", TxtShopFloorControlDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowProductionWagesHdr(DocNo);
                ShowProductionWagesDtl(DocNo);
                ShowProductionWagesDtl2(DocNo);
                ShowProductionWagesDtl3(DocNo);
                ShowProductionWagesDtl4(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowProductionWagesHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelInd, A.ShopFloorControlDocNo, C.DocName, B.DocDt As ShopFloorControlDocDt, ");
            SQL.AppendLine("D.ProductionShiftName, A.ProductionWagesSource, A.WagesFormulationCode, A.UomCode, A.CurCode, A.Remark ");
            SQL.AppendLine("From TblProductionWagesHdr A ");
            SQL.AppendLine("Inner Join TblShopFloorControlHdr B On A.ShopFloorControlDocNo=B.DocNo ");
            SQL.AppendLine("Left Join TblWorkCenterHdr C On B.WorkCenterDocNo=C.DocNo ");
            SQL.AppendLine("Inner Join TblProductionShift D On B.ProductionShiftCode=D.ProductionShiftCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            Sm.ShowDataInCtrl(
                    ref cm,
                    SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "CancelInd", "DocName", "ShopFloorControlDocNo", "ShopFloorControlDocDt",  
                        
                        //6-10
                        "ProductionShiftName", "ProductionWagesSource", "WagesFormulationCode", "UomCode", "CurCode", 
                        
                        //11
                        "Remark" 
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y");
                        TxtWorkCenterDocNo.EditValue = Sm.DrStr(dr, c[3]);
                        TxtShopFloorControlDocNo.EditValue = Sm.DrStr(dr, c[4]);
                        Sm.SetDte(DteShopFloorControlDocDt, Sm.DrStr(dr, c[5]));
                        TxtProductionShiftCode.EditValue = Sm.DrStr(dr, c[6]);
                        Sm.SetLue(LueProductionWagesSource, Sm.DrStr(dr, c[7]));
                        Sm.SetLue(LueWagesFormulationCode, Sm.DrStr(dr, c[8]));
                        TxtUomCode.EditValue = Sm.DrStr(dr, c[9]);
                        Sm.SetLue(LueCurCode, Sm.DrStr(dr, c[10]));
                        MeeRemark.EditValue = Sm.DrStr(dr, c[11]);
                    }, true
                );
        }

        private void ShowProductionWagesDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            //if (Sm.CompareStr(Sm.GetLue(LueProductionWagesSource), "2"))
            //{
            //    SQL.AppendLine("Select A.DNo, A.ShopFloorControlDNo, B.BomCode As ItCode, C.ItName, B.Qty, C.PlanningUomCode, IfNull(A.ItIndex, 0) As ItIndex, ");
            //    SQL.AppendLine("(B.Qty*IfNull(A.ItIndex, 0)) As Value ");
            //    SQL.AppendLine("From TblProductionWagesDtl A ");
            //    SQL.AppendLine("Inner Join TblShopFloorControl2Dtl B On A.ShopFloorControlDNo=B.DNo And B.DocNo=@ShopFloorControlDocNo ");
            //    SQL.AppendLine("Inner Join TblItem C On B.BomCode=C.ItCode ");
            //    SQL.AppendLine("Where A.DocNo=@DocNo ");
            //    SQL.AppendLine("Order By A.DNo");
            //}
            //else
            //{
            SQL.AppendLine("Select A.DNo, A.ShopFloorControlDNo, B.ItCode, C.ItName, ");
            SQL.AppendLine("B.Qty, C.PlanningUomCode, B.Qty2, C.PlanningUomCode2, ");
            SQL.AppendLine("IfNull(A.ItIndex, 0) As ItIndex, ");
            SQL.AppendLine("Case When @UomCode=C.PlanningUomCode2 Then (B.Qty2*IfNull(A.ItIndex, 0)) ");
            SQL.AppendLine("Else (B.Qty*IfNull(A.ItIndex, 0)) ");
            SQL.AppendLine("End As Value ");
            SQL.AppendLine("From TblProductionWagesDtl A ");
            SQL.AppendLine("Inner Join TblShopFloorControlDtl B On A.ShopFloorControlDNo=B.DNo And B.DocNo=@ShopFloorControlDocNo ");
            SQL.AppendLine("Inner Join TblItem C On B.ItCode=C.ItCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Order By A.DNo");
            //}

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@ShopFloorControlDocNo", TxtShopFloorControlDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UomCode", (TxtUomCode.Text.Length == 0 ? "XXX" : TxtUomCode.Text));
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 

                    //1-5
                    "ShopFloorControlDNo", "ItCode", "ItName", "Qty", "PlanningUomCode", 
                    
                    //6-9
                    "Qty2", "PlanningUomCode2", "ItIndex", "Value" 
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                }, false, false, true, false
            );
            if (Grd1.Rows.Count > 1)
            {
                Grd1.Rows.Count -= 1;
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 5, 7, 9, 10 });
            }
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ShowProductionWagesDtl2(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DNo, Value, Wages, (Value*Wages) As Amt ");
            SQL.AppendLine("From TblProductionWagesDtl2 ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("Order By DNo");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd2, ref cm, SQL.ToString(),
                new string[] 
                { 
                   //0
                   "DNo",

                   //1-3
                   "Value", "Wages", "Amt"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 3);
                    

                }, false, false, true, false
            );
            if (Grd2.Rows.Count > 1)
            {
                Grd2.Rows.Count -= 1;
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd2, new int[] { 1, 3 });
            }
            Sm.FocusGrd(Grd2, 0, 1);
        }

        private void ShowProductionWagesDtl3(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.EmpCode, B.EmpName, C.GrdLvlName, A.GrdLvlIndex, A.Qty, A.Wages, D.PosName, E.DeptName ");
            SQL.AppendLine("From TblProductionWagesDtl3 A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode=B.EmpCode ");
            SQL.AppendLine("Left Join TblGradeLevelHdr C On B.GrdLvlCode=C.GrdLvlCode ");
            SQL.AppendLine("Left Join TblPosition D On B.PosCode=D.PosCode ");
            SQL.AppendLine("Left Join TblDepartment E On B.DeptCode=E.DeptCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Order By A.DNo");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd3, ref cm, SQL.ToString(),
                new string[] 
                { 
                   //0
                   "DNo",

                   //1-5
                   "EmpCode", "EmpName", "GrdLvlName", "GrdLvlIndex", "Qty", 
                   
                   //6-8
                   "Wages", "PosName", "DeptName"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 8);
                }, false, false, true, false
            );
            if (Grd3.Rows.Count > 1)
            {
                Grd3.Rows.Count -= 1;
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd3, new int[] { 4, 5, 6 });
            }
            Sm.FocusGrd(Grd3, 0, 1);
        }

        private void ShowProductionWagesDtl4(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.EmpCode, B.EmpName, C.PosName, D.DeptName, E.GrdLvlName, A.GrdLvlIndex, A.Wages ");
            SQL.AppendLine("From TblProductionWagesDtl4 A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode=B.EmpCode ");
            SQL.AppendLine("Left Join TblPosition C On B.PosCode=C.PosCode ");
            SQL.AppendLine("Left Join TblDepartment D On B.DeptCode=D.DeptCode ");
            SQL.AppendLine("Left Join TblGradeLevelHdr E On B.GrdLvlCode=E.GrdLvlCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Order By A.DNo");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd4, ref cm, SQL.ToString(),
                new string[] 
                { 
                   //0
                   "DNo",

                   //1-5
                   "EmpCode", "EmpName", "PosName", "DeptName", "GrdLvlName",   
                   
                   //6-7
                   "GrdLvlIndex", "Wages"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd4, Grd4.Rows.Count - 1, new int[] { 7, 8 });
            Sm.FocusGrd(Grd4, 0, 1);
        }

        #endregion

        #region Additional Method

        private void SetNumberOfPlanningUomCode()
        {
            string Param = Sm.GetParameter("NumberOfPlanningUomCode");
            if (Param.Length != 0) mNumberOfPlanningUomCode = int.Parse(Param);
        }

        private void ShowPlanningUomCode()
        {
            if (mNumberOfPlanningUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 7, 8 }, true);
        }

        private void ShowUom()
        {
            TxtUomCode.EditValue = null;
            var WagesFormulationCode = Sm.GetLue(LueWagesFormulationCode);

            if (WagesFormulationCode.Length != 0)
            {
                var cm = new MySqlCommand()
                {
                    CommandText =
                        "Select UomCode From TblWagesFormulationHdr " +
                        "Where WagesFormulationCode=@Param;"
                };
                Sm.CmParam<String>(ref cm, "@Param", WagesFormulationCode);
                TxtUomCode.EditValue = Sm.GetValue(cm);
            }
        }

        internal string GetSelectedEmployee()
        {
            var SQL = "";
            if (Grd4.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd4.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd4, Row, 2).Length != 0)
                        SQL += "##" + Sm.GetGrdStr(Grd4, Row, 2) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        internal void ShowGrdInfo()
        {
            try
            {
                ClearGrd();
                if (
                    TxtShopFloorControlDocNo.Text.Length != 0 &&
                    Sm.GetLue(LueProductionWagesSource).Length != 0 &&
                    Sm.GetLue(LueWagesFormulationCode).Length != 0
                    )
                {
                    ShowGrdInfo1();
                    ShowGrdInfo2();
                    ShowGrdInfo3();
                    ShowGrdInfo4();
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void ShowGrdInfo1()
        {
            var SQL = new StringBuilder();
 
            //if (Sm.CompareStr(Sm.GetLue(LueProductionWagesSource), "2"))
            //{
            //    SQL.AppendLine("Select A.DNo, A.BomCode As ItCode, B.ItName, A.Qty, B.PlanningUomCode, IfNull(C.ItIndex, 0) As ItIndex, ");
            //    SQL.AppendLine("(A.Qty*IfNull(C.ItIndex, 0)) As Value ");
            //    SQL.AppendLine("From TblShopFloorControl2Dtl A ");
            //    SQL.AppendLine("Inner Join TblItem B On A.BomCode=B.ItCode ");
            //    SQL.AppendLine("Left Join TblItemIndex C On A.BomCode=C.ItCode ");
            //    SQL.AppendLine("Where A.DocNo=@DocNo And A.BomType='1' ");
            //    SQL.AppendLine("Order By A.DNo;");
            //}

            SQL.AppendLine("Select A.DNo, A.ItCode, B.ItName, ");
            SQL.AppendLine("A.Qty, B.PlanningUomCode, A.Qty2, B.PlanningUomCode2, ");
            SQL.AppendLine("IfNull(C.ItIndex, 0) As ItIndex, ");
            SQL.AppendLine("Case @UomCode ");
            SQL.AppendLine("When B.PlanningUomCode Then (A.Qty*IfNull(C.ItIndex, 0)) ");
            SQL.AppendLine("When B.PlanningUomCode2 Then (A.Qty2*IfNull(C.ItIndex, 0)) ");
            SQL.AppendLine("Else 0 End As Value ");
            SQL.AppendLine("From TblShopFloorControlDtl A ");
            SQL.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
            SQL.AppendLine("Left Join TblItemIndex C On A.ItCode=C.ItCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Order By A.DNo;");

            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", TxtShopFloorControlDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UomCode", (TxtUomCode.Text.Length == 0 ? "XXX" : TxtUomCode.Text));
            Sm.ShowDataInGrid(
                    ref Grd1, ref cm, SQL.ToString(),
                    new string[] 
                           { 
                               //0
                               "DNo",

                               //1-5
                               "ItCode", "ItName", "Qty", "PlanningUomCode", "Qty2", 
                               
                               //6-8
                               "PlanningUomCode2", "ItIndex", "Value"
                           },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 7);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 8);
                    }, false, false, true, false
            );
            if (Grd1.Rows.Count > 1)
            {
                Grd1.Rows.Count -= 1;
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 5, 7, 9, 10 });
            }
            Sm.FocusGrd(Grd1, 0, 2);
        }

        private void ShowGrdInfo2()
        {
            decimal Value = 0m, From = 0m, To = 0m, Wages = 0m, Temp = 0m;

            if (Sm.GetGrdStr(Grd1, Grd1.Rows.Count - 1, 10).Length != 0)
                Value = Sm.GetGrdDec(Grd1, Grd1.Rows.Count - 1, 10);

            if (Value <= 0) return;

            Temp = Value;

            var SQL = new StringBuilder();

            SQL.AppendLine("Select ValueFrom, ValueTo, Wages ");
            SQL.AppendLine("From TblWagesFormulationDtl ");
            SQL.AppendLine("Where WagesFormulationCode=@WagesFormulationCode ");
            SQL.AppendLine("Order By ValueFrom");
            
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@WagesFormulationCode", Sm.GetLue(LueWagesFormulationCode));
            
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                cm.CommandTimeout = 0;
                using (var dr = cm.ExecuteReader())
                {
                    var c = Sm.GetOrdinal(dr, new string[] { "ValueFrom", "ValueTo", "Wages" });
                    if (dr.HasRows)
                    {
                        int Row = 0;
                        Grd2.ProcessTab = true;
                        Grd2.BeginUpdate();
                        while (dr.Read())
                        {
                            // Kode lama

                            //From = Sm.DrDec(dr, 0);
                            //To = Sm.DrDec(dr, 1);
                            //Wages = Sm.DrDec(dr, 2);

                            //Grd2.Rows.Add();

                            //if (Row == 0)
                            //{
                            //    if (Value >= From)
                            //    {
                            //        if (Value <= To)
                            //        {
                            //            Grd2.Cells[Row, 1].Value = Value;
                            //            Grd2.Cells[Row, 2].Value = Wages;
                            //            Grd2.Cells[Row, 3].Value = Value * Wages;
                            //            Temp = 0m;
                            //        }
                            //        else
                            //        {
                            //            Grd2.Cells[Row, 1].Value = To;
                            //            Grd2.Cells[Row, 2].Value = Wages;
                            //            Grd2.Cells[Row, 3].Value = To * Wages;
                            //            Temp = To;
                            //        }
                            //    }
                            //}
                            //else
                            //{
                            //    if (To == 0)
                            //    {
                            //        Grd2.Cells[Row, 1].Value = Value - Temp;
                            //        Grd2.Cells[Row, 2].Value = Wages;
                            //        Grd2.Cells[Row, 3].Value = (Value - Temp) * Wages;
                            //        Temp = 0m;
                            //    }
                            //    else
                            //    {
                            //        if (Value <= To)
                            //        {
                            //            Grd2.Cells[Row, 1].Value = Value;
                            //            Grd2.Cells[Row, 2].Value = Wages;
                            //            Grd2.Cells[Row, 3].Value = Value * Wages;
                            //            Temp = 0m;
                            //        }
                            //        else
                            //        {
                            //            Grd2.Cells[Row, 1].Value = To - Temp;
                            //            Grd2.Cells[Row, 2].Value = Wages;
                            //            Grd2.Cells[Row, 3].Value = (To - Temp) * Wages;
                            //            Temp = Temp + (To - Temp);
                            //        }
                            //    }
                            //}
                            //if (Temp == 0) break;
                            //Row++;


                            // Kode lama

                            From = Sm.DrDec(dr, 0);
                            To = Sm.DrDec(dr, 1);
                            Wages = Sm.DrDec(dr, 2);
                            

                            Grd2.Rows.Add();

                            if (Value >= From)
                            {
                                if (Temp <= To)
                                {
                                    Grd2.Cells[Row, 1].Value = Temp;
                                    Grd2.Cells[Row, 2].Value = Wages;
                                    Grd2.Cells[Row, 3].Value = Temp * Wages;
                                    Temp = 0m;
                                }
                                else
                                {
                                    Grd2.Cells[Row, 1].Value = To;
                                    Grd2.Cells[Row, 2].Value = Wages;
                                    Grd2.Cells[Row, 3].Value = To * Wages;
                                    Temp -= To;
                                }
                            }
                            else
                                Temp = 0m;
                            if (Temp == 0m) break;
                            Row++;


                        }
                    }

                    if (Grd2.Rows.Count > 1)
                    {
                        Grd2.Rows.Count -= 1;
                        iGSubtotalManager.BackColor = Color.LightSalmon;
                        iGSubtotalManager.ShowSubtotalsInCells = true;
                        iGSubtotalManager.ShowSubtotals(Grd2, new int[] { 1, 3 });
                    }
                    //Grd2.Cols.AutoWidth();
                    Grd2.EndUpdate();
                    dr.Dispose();
                    cm.Dispose();
                }
            }
            Sm.FocusGrd(Grd2, 0, 1);
        }

        private void ShowGrdInfo3()
        {
            decimal Total = 0m;

            if (Sm.GetGrdStr(Grd2, Grd2.Rows.Count - 1, 3).Length != 0)
                Total = Sm.GetGrdDec(Grd2, Grd2.Rows.Count - 1, 3);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.BomCode As EmpCode, B.EmpName, E.GrdLvlName, IfNull(D.GrdLvlIndex, 1) As GrdLvlIndex, A.Qty, F.PosName, G.DeptName, ");
            SQL.AppendLine("Case When C.TotalQty=0 Then 0 Else (A.Qty/C.TotalQty)*@Total*IfNull(D.GrdLvlIndex, 1) End As Wages ");
            SQL.AppendLine("From TblShopFloorControl2Dtl A ");
            SQL.AppendLine("Inner Join TblEmployee B ");
            SQL.AppendLine("    On A.BomCode=B.EmpCode ");
            SQL.AppendLine("    And B.ResignDt Is Null ");
            SQL.AppendLine("    And B.PosCode<>IfNull((Select ParValue From TblParameter Where ParCode='SFCCoordinatorPosition'), 'XXX') ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select DocNo, Sum(Qty) As TotalQty ");
            SQL.AppendLine("    From TblShopFloorControl2Dtl ");
            SQL.AppendLine("    Where DocNo=@DocNo And BomType='3' ");
            SQL.AppendLine("    Group By DocNo ");
            SQL.AppendLine(") C On A.DocNo=C.DocNo ");
            SQL.AppendLine("Left Join TblWagesFormulationDtl2 D On B.GrdLvlCode=D.GrdLvlCode And D.WagesFormulationCode=@WagesFormulationCode ");
            SQL.AppendLine("Left Join TblGradeLevelHdr E On B.GrdLvlCode=E.GrdLvlCode ");
            SQL.AppendLine("Left Join TblPosition F On B.PosCode=F.PosCode ");
            SQL.AppendLine("Left Join TblDepartment G On B.DeptCode=G.DeptCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo And A.BomType='3' ");
            SQL.AppendLine("Order By A.DNo");
            
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@WagesFormulationCode", Sm.GetLue(LueWagesFormulationCode));
            Sm.CmParam<String>(ref cm, "@DocNo", TxtShopFloorControlDocNo.Text);
            Sm.CmParam<Decimal>(ref cm, "@Total", Total);

            Sm.ShowDataInGrid(
                    ref Grd3, ref cm, SQL.ToString(),
                    new string[] 
                           { 
                               //0
                               "EmpCode",

                               //1-5
                               "EmpName", "GrdLvlName", "GrdLvlIndex", "Qty", "Wages",

                               //6-7
                               "PosName", "DeptName"
                           },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                    }, false, false, true, false
            );
            if (Grd3.Rows.Count > 1)
            {
                Grd3.Rows.Count -= 1;
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd3, new int[] { 4, 5, 6 });
            }
            Sm.FocusGrd(Grd3, 0, 2);
        }

        private void ShowGrdInfo4()
        {
            decimal TotalEmployee = 0m, Total = 0m;

            TotalEmployee = (Grd3.Rows.Count < 2) ? 0 : Grd3.Rows.Count - 1;
            if (Sm.GetGrdStr(Grd2, Grd2.Rows.Count - 1, 3).Length != 0)
                Total = Sm.GetGrdDec(Grd2, Grd2.Rows.Count - 1, 3);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.EmpCode, A.EmpName, B.PosName, C.DeptName, D.GrdLvlName, E.GrdLvlIndex, ");
            SQL.AppendLine("Case When @TotalEmployee=0 Then 0 Else (1/@TotalEmployee)*@Total*IfNull(E.GrdLvlIndex, 1) End As Wages ");
            SQL.AppendLine("From TblEmployee A ");
            SQL.AppendLine("Left Join TblPosition B On A.PosCode=B.PosCode ");
            SQL.AppendLine("Left Join TblDepartment C On A.DeptCode=C.DeptCode ");
            SQL.AppendLine("Left Join TblGradeLevelHdr D On A.GrdLvlCode=D.GrdLvlCode ");
            SQL.AppendLine("Left Join TblWagesFormulationDtl2 E On A.GrdLvlCode=E.GrdLvlCode And E.WagesFormulationCode=@WagesFormulationCode ");
            SQL.AppendLine("Where A.ResignDt Is Null ");
            SQL.AppendLine("And A.PosCode=IfNull((Select ParValue From TblParameter Where ParCode='SFCCoordinatorPosition'), 'XXX') ");
            SQL.AppendLine("And A.EmpCode In (Select BomCode From TblShopFloorControl2Dtl Where DocNo=@ShopFloorControlDocNo And BomType='3') ");
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@ShopFloorControlDocNo", TxtShopFloorControlDocNo.Text);
            Sm.CmParam<String>(ref cm, "@WagesFormulationCode", Sm.GetLue(LueWagesFormulationCode));
            Sm.CmParam<decimal>(ref cm, "@TotalEmployee", TotalEmployee);
            Sm.CmParam<decimal>(ref cm, "@Total", Total);

            Sm.ShowDataInGrid(
                    ref Grd4, ref cm, SQL.ToString(),
                    new string[] 
                           { 
                            //0
                            "EmpCode",
 
                            //1-5
                            "EmpName", "PosName", "DeptName", "GrdLvlName", "GrdLvlIndex",   
                            
                            //6
                            "Wages"
                           },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 5);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 6);
                    }, false, false, true, false
            );
            //if (!(Grd4.Rows.Count == 1 && Sm.GetGrdStr(Grd4, Grd4.Rows.Count - 1, 2).Length == 0))
            //{
            //    Grd4.Rows.Add();
            //    Sm.SetGrdNumValueZero(ref Grd4, Grd4.Rows.Count - 1, new int[] { 7, 8 });
            //}
            Sm.SetGrdNumValueZero(ref Grd4, Grd4.Rows.Count - 1, new int[] { 7, 8 });
        }
        
        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void BtnShopFloorControlDocNo1_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmProductionWagesDlg(this));
        }

        private void BtnShopFloorControlDocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtShopFloorControlDocNo, "SFC#", false))
            {
                var f1 = new FrmShopFloorControl2(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = TxtShopFloorControlDocNo.Text;
                f1.ShowDialog();
            }
        }

        private void LueProductionWagesSource_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueProductionWagesSource, new Sm.RefreshLue1(Sl.SetLueProductionWagesSource));
            LueWagesFormulationCode.EditValue = null;
            TxtUomCode.EditValue = null;
            ClearGrd();
        }

        private void LueWagesFormulationCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWagesFormulationCode, new Sm.RefreshLue1(Sl.SetLueWagesFormulationCode));
            ShowUom();
            ShowGrdInfo();
        }

        private void LueCurCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCurCode, new Sm.RefreshLue1(Sl.SetLueCurCode));
        }

        #endregion

        #region Grid Event

        private void Grd4_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 1)
                {
                    e.DoDefault = false;

                    decimal TotalEmployee=0m, Total=0m;

                    TotalEmployee = (Grd3.Rows.Count < 2) ? 0 : Grd3.Rows.Count - 1;
                    if (Sm.GetGrdStr(Grd2, Grd2.Rows.Count - 1, 3).Length != 0)
                        Total = Sm.GetGrdDec(Grd2, Grd2.Rows.Count - 1, 3);

                    if (e.KeyChar == Char.Parse(" ")) 
                        Sm.FormShowDialog(new FrmProductionWagesDlg2(this, TotalEmployee, Total, Sm.GetLue(LueWagesFormulationCode)));
                }

                if (Sm.IsGrdColSelected(new int[] { 0 }, e.ColIndex))
                {
                    Sm.GrdRequestEdit(Grd4, e.RowIndex);
                    Sm.SetGrdNumValueZero(ref Grd4, Grd4.Rows.Count - 1, new int[] { 7, 8 });
                }
            }

        }

        private void Grd4_KeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0)
                Sm.GrdRemoveRow(Grd4, e, BtnSave);
            Sm.GrdKeyDown(Grd4, e, BtnFind, BtnSave);
        }

        private void Grd4_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1 && TxtDocNo.Text.Length == 0)
            {
                decimal TotalEmployee = 0m, Total = 0m;

                TotalEmployee = (Grd3.Rows.Count < 2) ? 0 : Grd3.Rows.Count - 1;
                if (Sm.GetGrdStr(Grd2, Grd2.Rows.Count - 1, 3).Length != 0)
                    Total = Sm.GetGrdDec(Grd2, Grd2.Rows.Count - 1, 3);
                
                Sm.FormShowDialog(new FrmProductionWagesDlg2(this, TotalEmployee, Total, Sm.GetLue(LueWagesFormulationCode)));
            }
        }

        #endregion

        #endregion
    }
}
