﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmTenderQuotation : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string 
            mMenuCode = string.Empty, 
            mAccessInd = string.Empty,
            mTQRDocNo = string.Empty,
            mMRDocNo = string.Empty,
            mMRDNo = string.Empty,
            mQtDocNo = string.Empty,
            mQtDNo = string.Empty,
            mItScCode = string.Empty;

        private string IsProcFormat = string.Empty;

        private bool mIsSiteMandatory = false, mIsAutoGeneratePurchaseLocalDocNo = false;
        private List<LocalDocument> mlLocalDocument = null;
        internal FrmTenderQuotationFind FrmFind;

        #endregion

        #region Constructor

        public FrmTenderQuotation(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Form Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = Sm.GetMenuDesc("FrmTenderQuotation");

                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetFormControl(mState.View);
                if (mIsSiteMandatory) LblSite.ForeColor = Color.Red;
                base.FrmLoad(sender, e);

                mlLocalDocument = new List<LocalDocument>();

                //if this application is called from other application
                if (mTQRDocNo.Length != 0)
                {
                    ShowData(mTQRDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Methods

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtTQRDocNo, TxtTenderDocNo, TxtQtDocNo, TxtPORequestDocNo, TxtLocalDocNo, LueSiteCode, 
                        TxtItCode, TxtItName, MeeRemark, TxtQty, TxtEstCurCode, TxtEstPrice, TxtQtCurCode, TxtUPrice
                    }, true);
                    BtnTQRDocNo.Enabled = false;
                    TxtTQRDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtLocalDocNo, MeeRemark }, false);
                    BtnTQRDocNo.Enabled = true;
                    BtnTQRDocNo.Focus();
                    break;
                case mState.Edit:
                    break;
            }
        }

        private void ClearData()
        {
            mTQRDocNo = string.Empty;
            mMRDocNo = string.Empty;
            mMRDNo = string.Empty;
            mQtDocNo = string.Empty;
            mQtDNo = string.Empty;
            mItScCode = string.Empty;

            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtTQRDocNo, TxtTenderDocNo, TxtQtDocNo, TxtPORequestDocNo, TxtLocalDocNo, LueSiteCode, TxtItCode, TxtItName, MeeRemark,
                TxtEstCurCode, TxtQtCurCode
            });

            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtQty, TxtEstPrice, TxtUPrice }, 0);
        }

        internal void ClearData2()
        {
            mTQRDocNo = string.Empty;
            mMRDocNo = string.Empty;
            mMRDNo = string.Empty;
            mQtDocNo = string.Empty;
            mQtDNo = string.Empty;
            mItScCode = string.Empty;

            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtTQRDocNo, TxtTenderDocNo, TxtQtDocNo, TxtPORequestDocNo, TxtLocalDocNo, LueSiteCode, TxtItCode, TxtItName,
                TxtEstCurCode, TxtQtCurCode
            });

            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtQty, TxtEstPrice, TxtUPrice }, 0);
        }

        internal void ClearData3()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtQtCurCode
            });

            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtUPrice }, 0);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmTenderQuotationFind(this);
                Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sl.SetLueSiteCode2(ref LueSiteCode, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                InsertData(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
        }

        #endregion

        #region Save Data

        #region Insert Data

        #region Generate Local Document

        private void SetLocalDocNo(
            string DocType,
            ref string LocalDocNo,
            ref string SeqNo,
            ref string DeptCode,
            ref string ItSCCode,
            ref string Mth,
            ref string Yr,
            ref string Revision
        )
        {
            var DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='" + DocType + "'");
            var ShortCode = Sm.GetValue("Select IfNull(ShortCode, DeptCode) From TblDepartment Where DeptCode='" + DeptCode + "'");
            LocalDocNo = SeqNo + "/" + DocAbbr + "/" + ShortCode + "/" + ItSCCode + "/" + Mth + "/" + Yr;
            if (Revision.Length > 0 && Revision != "0")
                LocalDocNo = LocalDocNo + "/R" + Revision;
        }

        private void SetRevision(
            ref string SeqNo,
            ref string DeptCode,
            ref string ItSCCode,
            ref string Mth,
            ref string Yr,
            ref string Revision
        )
        {
            var SQL = new StringBuilder();
            SQL.AppendLine("Select IfNull(Revision, '0') ");
            SQL.AppendLine("From TblPORequestHdr ");
            SQL.AppendLine("Where SeqNo Is Not Null ");
            SQL.AppendLine("And SeqNo=@SeqNo ");
            SQL.AppendLine("And DeptCode=@DeptCode ");
            SQL.AppendLine("And ItSCCode=@ItSCCode ");
            SQL.AppendLine("And Mth=@Mth ");
            SQL.AppendLine("And Yr=@Yr ");
            SQL.AppendLine("Order By Revision Desc Limit 1;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@SeqNo", SeqNo);
            Sm.CmParam<String>(ref cm, "@DeptCode", DeptCode);
            Sm.CmParam<String>(ref cm, "@ItSCCode", ItSCCode);
            Sm.CmParam<String>(ref cm, "@Mth", Mth);
            Sm.CmParam<String>(ref cm, "@Yr", Yr);

            Revision = Sm.GetValue(cm);
            if (Revision.Length == 0)
                Revision = "0";
            else
                Revision = (int.Parse(Revision) + 1).ToString();
        }

        private bool IsLocalDocumentNotValid(
            ref string SeqNo,
            ref string DeptCode,
            ref string ItSCCode,
            ref string Mth,
            ref string Yr
            )
        {
            if (SeqNo.Length == 0) return false;

            foreach (var x in mlLocalDocument.Where(x => x.SeqNo.Length > 0))
            {
                if (!(
                  Sm.CompareStr(SeqNo, x.SeqNo) &&
                  Sm.CompareStr(DeptCode, x.DeptCode) &&
                  Sm.CompareStr(ItSCCode, x.ItSCCode) &&
                  Sm.CompareStr(Mth, x.Mth) &&
                  Sm.CompareStr(Yr, x.Yr)
                ))
                {
                    Sm.StdMsg(mMsgType.Warning,
                        "Document# : " + x.DocNo + Environment.NewLine +
                        "Local# : " + x.LocalDocNo + Environment.NewLine + Environment.NewLine +
                        "Invalid data.");
                    return true;
                }
            }
            return false;
        }

        private void SetLocalDocument(
            ref string SeqNo,
            ref string DeptCode,
            ref string ItSCCode,
            ref string Mth,
            ref string Yr)
        {
            mlLocalDocument.Clear();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = string.Empty;
            bool IsFirst = true;

            SQL.AppendLine("Select DocNo, LocalDocNo, SeqNo, DeptCode, ItSCCode, Mth, Yr, Revision ");
            SQL.AppendLine("From TblMaterialRequestHdr Where DocNo = @DocNo; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", mMRDocNo);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "DocNo", 

                    //1-5
                    "LocalDocNo", "SeqNo", "DeptCode", "ItSCCode", "Mth", 
                    
                    //6-7
                    "Yr", "Revision" 
                });
                if (dr.HasRows)
                {
                    string
                        DocNoTemp = string.Empty,
                        LocalDocNoTemp = string.Empty,
                        SeqNoTemp = string.Empty,
                        DeptCodeTemp = string.Empty,
                        ItSCCodeTemp = string.Empty,
                        MthTemp = string.Empty,
                        YrTemp = string.Empty,
                        RevisionTemp = string.Empty;

                    while (dr.Read())
                    {
                        DocNoTemp = Sm.DrStr(dr, c[0]);
                        LocalDocNoTemp = Sm.DrStr(dr, c[1]);
                        SeqNoTemp = Sm.DrStr(dr, c[2]);
                        DeptCodeTemp = Sm.DrStr(dr, c[3]);
                        ItSCCodeTemp = Sm.DrStr(dr, c[4]);
                        MthTemp = Sm.DrStr(dr, c[5]);
                        YrTemp = Sm.DrStr(dr, c[6]);
                        RevisionTemp = Sm.DrStr(dr, c[7]);

                        mlLocalDocument.Add(new LocalDocument()
                        {
                            DocNo = DocNoTemp,
                            LocalDocNo = LocalDocNoTemp,
                            SeqNo = SeqNoTemp,
                            DeptCode = DeptCodeTemp,
                            ItSCCode = ItSCCodeTemp,
                            Mth = MthTemp,
                            Yr = YrTemp,
                            Revision = RevisionTemp
                        });

                        if (IsFirst && SeqNoTemp.Length > 0)
                        {
                            SeqNo = SeqNoTemp;
                            DeptCode = DeptCodeTemp;
                            ItSCCode = ItSCCodeTemp;
                            Mth = MthTemp;
                            Yr = YrTemp;
                            IsFirst = false;
                        }
                    }
                }
                dr.Close();
            }
        }

        #endregion

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            bool PORequestApprovalForAllDept = IsPORequestApprovalForAllDept();
            string PORequestDocNo = GenerateDocNo(IsProcFormat, string.Concat(Sm.ServerCurrentDateTime(), "00"), "PORequest", "TblPORequestHdr", mItScCode);

            string LocalDocNo = mIsAutoGeneratePurchaseLocalDocNo ? string.Empty : TxtLocalDocNo.Text;
            string
                SeqNo = string.Empty,
                DeptCode = string.Empty,
                ItSCCode = string.Empty,
                Mth = string.Empty,
                Yr = string.Empty,
                Revision = string.Empty;

            if (mIsAutoGeneratePurchaseLocalDocNo)
            {
                SetLocalDocument(
                    ref SeqNo,
                    ref DeptCode,
                    ref ItSCCode,
                    ref Mth,
                    ref Yr
                );
                if (mlLocalDocument.Count == 0) return;
                if (IsLocalDocumentNotValid(
                        ref SeqNo,
                        ref DeptCode,
                        ref ItSCCode,
                        ref Mth,
                        ref Yr
                    )) return;

                SetRevision(
                    ref SeqNo,
                    ref DeptCode,
                    ref ItSCCode,
                    ref Mth,
                    ref Yr,
                    ref Revision
                    );

                if (SeqNo.Length > 0)
                {
                    SetLocalDocNo(
                        "PORequest",
                        ref LocalDocNo,
                        ref SeqNo,
                        ref DeptCode,
                        ref ItSCCode,
                        ref Mth,
                        ref Yr,
                        ref Revision
                        );
                }

                mlLocalDocument.Clear();
            }

            Cursor.Current = Cursors.WaitCursor;
            var cml = new List<MySqlCommand>();

            cml.Add(SaveTenderQuotation(PORequestDocNo, LocalDocNo));
            cml.Add(SavePORequestHdr(PORequestDocNo, LocalDocNo, SeqNo, DeptCode, ItSCCode, Mth, Yr, Revision));
            cml.Add(SavePORequestDtl(PORequestDocNo, PORequestApprovalForAllDept));
            cml.Add(UpdateTender());

            Sm.ExecCommands(cml);

            ShowData(TxtTQRDocNo.Text);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtTQRDocNo, "Tender Quotation Request#", false) ||
                (mIsSiteMandatory && Sm.IsLueEmpty(LueSiteCode, "Site"))
                ;
        }

        private bool IsDocApprovalSettingNotExisted()
        {
            if (!Sm.IsDataExist("Select 1 From TblDocApprovalSetting Where DocType='PORequest' Limit 1;"))
            {
                Sm.StdMsg(mMsgType.Warning, "No approval setting for this PO request.");
                return true;
            }
            return false;
        }

        private MySqlCommand SaveTenderQuotation(string PORequestDocNo, string LocalDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblTenderQuotation(TQRDocNo, PORequestDocNo, LocalDocNo, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@TQRDocNo, @PORequestDocNo, @LocalDocNo, @Remark, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@TQRDocNo", TxtTQRDocNo.Text);
            Sm.CmParam<String>(ref cm, "@PORequestDocNo", PORequestDocNo);
            Sm.CmParam<String>(ref cm, "@LocalDocNo", LocalDocNo);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SavePORequestHdr(string DocNo, string LocalDocNo, string SeqNo, string DeptCode, string ItSCCode, string Mth, string Yr, string Revision)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblPORequestHdr " +
                    "(DocNo, DocDt, LocalDocNo, SiteCode, SeqNo, DeptCode, ItSCCode, Mth, Yr, Revision, Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DocDt, @LocalDocNo, @SiteCode, @SeqNo, @DeptCode, @ItSCCode, @Mth, @Yr, @Revision, @Remark, @CreateBy, CurrentDateTime());"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", string.Concat(Sm.ServerCurrentDateTime(), "00"));
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
            Sm.CmParam<String>(ref cm, "@LocalDocNo", LocalDocNo);
            Sm.CmParam<String>(ref cm, "@SeqNo", SeqNo);
            Sm.CmParam<String>(ref cm, "@DeptCode", DeptCode);
            Sm.CmParam<String>(ref cm, "@ItSCCode", ItSCCode);
            Sm.CmParam<String>(ref cm, "@Mth", Mth);
            Sm.CmParam<String>(ref cm, "@Yr", Yr);
            Sm.CmParam<String>(ref cm, "@Revision", Revision);
            var Remark = MeeRemark.Text;
            Sm.CmParam<String>(ref cm, "@Remark", Remark);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SavePORequestDtl(string DocNo, bool PORequestApprovalForAllDept)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblPORequestDtl");
            SQL.AppendLine("(DocNo, DNo, CancelInd, MaterialRequestDocNo, MaterialRequestDNo, Qty, QtDocNo, QtDNo, CreditLimit, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, 'N', @MaterialRequestDocNo, @MaterialRequestDNo, @Qty, @QtDocNo, @QtDNo, 0, @Remark, @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select T.DocType, @DocNo, @DNo, T.DNo, @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblDocApprovalSetting T ");
            SQL.AppendLine("Where T.DocType='PORequest' ");

            if (!PORequestApprovalForAllDept)
            {
                SQL.AppendLine("And T.DeptCode In ( ");
                SQL.AppendLine("    Select B.DeptCode ");
                SQL.AppendLine("    From TblPORequestDtl A ");
                SQL.AppendLine("    Inner Join TblMaterialRequestHdr B On A.MaterialRequestDocNo=B.DocNo ");
                SQL.AppendLine("    Where A.DocNo=@DocNo And DNo=@DNo ");
                SQL.AppendLine(") ");
            }

            SQL.AppendLine("And (T.StartAmt=0 ");
            SQL.AppendLine("Or T.StartAmt<=IfNull(( ");
            SQL.AppendLine("    Select A.Qty*B.UPrice*IfNull(D.Amt, 1) ");
            SQL.AppendLine("    From TblPORequestDtl A ");
            SQL.AppendLine("    Inner Join TblQtDtl B On A.QtDocNo=B.DocNo And A.QtDNo=B.DNo ");
            SQL.AppendLine("    Inner Join TblQtHdr C On A.QtDocNo=C.DocNo ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select D1.CurCode1, D1.Amt ");
            SQL.AppendLine("        From TblCurrencyRate D1 ");
            SQL.AppendLine("        Inner Join ( ");
            SQL.AppendLine("            Select CurCode1, Max(RateDt) RateDt ");
            SQL.AppendLine("            From TblCurrencyRate ");
            SQL.AppendLine("            Where CurCode2=(Select ParValue From TblParameter Where ParCode='MainCurCode') ");
            SQL.AppendLine("            Group By CurCode1 ");
            SQL.AppendLine("        ) D2 On D1.CurCode1=D2.CurCode1 And D1.RateDt=D2.RateDt ");
            SQL.AppendLine("    ) D On C.CurCode=D.CurCode1 ");
            SQL.AppendLine("    Where A.DocNo=@DocNo And A.DNo=@DNo ");
            SQL.AppendLine("), 0)); ");

            SQL.AppendLine("Update TblPORequestDtl Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo And DNo=@DNo ");
            SQL.AppendLine("And Not Exists(Select DocType From TblDocApproval Where DocType='PORequest' And DocNo=@DocNo And DNo=@DNo); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", "001");
            Sm.CmParam<String>(ref cm, "@MaterialRequestDocNo", mMRDocNo);
            Sm.CmParam<String>(ref cm, "@MaterialRequestDNo", mMRDNo);
            Sm.CmParam<Decimal>(ref cm, "@Qty", Decimal.Parse(TxtQty.Text));
            Sm.CmParam<String>(ref cm, "@QtDocNo", mQtDocNo);
            Sm.CmParam<String>(ref cm, "@QtDNo", mQtDNo);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand UpdateTender()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblTender ");
            SQL.AppendLine("   Set Status = 'C', LastUpBy = @CreateBy, LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("Where DocNo = @TenderDocNo And IfNull(Status, 'O') = 'O'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@TenderDocNo", TxtTenderDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string TQRDocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowTenderQuotation(TQRDocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowTenderQuotation(string TQRDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.TQRDocNo, T.TenderDocNo, T.QtDocNo, A.PORequestDocNo, A.LocalDocNo, C.SiteCode, D.ItCode, E.ItName, ");
            SQL.AppendLine("D.Qty, A.Remark, B.MaterialRequestDNo, F.DNo As QtDNo, E.ItScCode, D.CurCode As EstCurCode, D.EstPrice, ");
            SQL.AppendLine("F.CurCode As QtCurCode, F.UPrice, D.DocNo As MRDocNo ");
            SQL.AppendLine("From ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select * ");
            SQL.AppendLine("    From TblTenderQuotation ");
            SQL.AppendLine("    Where TQRDocNo = @TQRDocNo ");
            SQL.AppendLine(") A ");
            SQL.AppendLine("Inner Join TblTenderQuotationRequest T On A.TQRDocNo = T.DocNo ");
            SQL.AppendLine("Inner Join TblTender B On T.TenderDocNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblMaterialRequestHdr C On B.MaterialRequestDocNo = C.DocNo ");
            SQL.AppendLine("Inner Join TblMaterialRequestDtl D On B.MaterialRequestDocNo = D.DocNo And B.MaterialRequestDNo = D.DNo ");
            SQL.AppendLine("Inner Join TblItem E On D.ItCode = E.ItCode ");
            SQL.AppendLine("Inner Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select T1.DocNo, T2.DNo, T1.CurCode, T2.UPrice ");
            SQL.AppendLine("    From TblQtHdr T1 ");
            SQL.AppendLine("    Inner Join TblQtDtl T2 On T1.DocNo = T2.DocNo ");
            SQL.AppendLine("    Where T1.TenderDocNo Is Not Null ");
            SQL.AppendLine(") F On T.QtDocNo = F.DocNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@TQRDocNo", TQRDocNo);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[]
                    { 
                        //0
                        "TQRDocNo",
                        //1-5
                        "TenderDocNo", "QtDocNo", "PORequestDocNo", "LocalDocNo", "SiteCode", 
                        //6-10
                        "ItCode", "ItName", "Qty", "Remark", "MaterialRequestDNo", 
                        //11-15
                        "QtDNo", "ItScCode", "EstCurCode", "EstPrice", "QtCurCode", 
                        //16-17
                        "UPrice", "MRDocNo"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtTQRDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        TxtTenderDocNo.EditValue = Sm.DrStr(dr, c[1]);
                        TxtQtDocNo.EditValue = Sm.DrStr(dr, c[2]);
                        mQtDocNo = Sm.DrStr(dr, c[2]);
                        TxtPORequestDocNo.EditValue = Sm.DrStr(dr, c[3]);
                        TxtLocalDocNo.EditValue = Sm.DrStr(dr, c[4]);
                        Sl.SetLueSiteCode2(ref LueSiteCode, Sm.DrStr(dr, c[5]));
                        TxtItCode.EditValue = Sm.DrStr(dr, c[6]);
                        TxtItName.EditValue = Sm.DrStr(dr, c[7]);
                        TxtQty.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[8]), 0);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[9]);
                        mMRDNo = Sm.DrStr(dr, c[10]);
                        mQtDNo = Sm.DrStr(dr, c[11]);
                        mItScCode = Sm.DrStr(dr, c[12]);
                        TxtEstCurCode.EditValue = Sm.DrStr(dr, c[13]);
                        TxtEstPrice.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[14]), 0);
                        TxtQtCurCode.EditValue = Sm.DrStr(dr, c[15]);
                        TxtUPrice.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[16]), 0);
                        mMRDocNo = Sm.DrStr(dr, c[17]);
                    }, true
                );
        }

        internal void ShowTenderQuotationDetail(string TQRDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.TenderDocNo, C.DocNo As MRDocNo, C.DNo As MRDNo, F.DocNo As QtDocNo, ");
            SQL.AppendLine("F.DNo As QtDNo, D.ItScCode, H.SiteCode, C.ItCode, D.ItName, C.Qty, C.CurCode As EstCurCode, ");
            SQL.AppendLine("C.EstPrice, E.CurCode As QtCurCode, F.UPrice ");
            SQL.AppendLine("From ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select * ");
            SQL.AppendLine("    From TblTenderQuotationRequest ");
            SQL.AppendLine("    Where DocNo = @DocNo ");
            SQL.AppendLine(") A ");
            SQL.AppendLine("Inner Join TblTender B On A.TenderDocNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblMaterialRequestDtl C On B.MaterialRequestDocNo = C.DocNo And B.MaterialRequestDNo = C.DNo ");
            SQL.AppendLine("Inner Join TblItem D On C.ItCode = D.ItCode ");
            SQL.AppendLine("Inner Join TblQtHdr E On A.QtDocNo = E.DocNo And E.TenderDocNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblQtDtl F On A.QtDocNo = F.DocNo And F.DNo = '001' ");
            SQL.AppendLine("Inner Join TblMaterialRequestHdr H On B.MaterialRequestDocNo = H.DocNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", TQRDocNo);

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[]
                { 
                    //0
                    "DocNo",
                    
                    //1-5
                    "TenderDocNo", "MRDocNo", "MRDNo", "QtDocNo", "QtDNo", 
                    
                    //6-10
                    "ItScCode", "SiteCode", "ItCode", "ItName", "Qty", 

                    //11-14
                    "EstCurCode", "EstPrice", "QtCurCode", "UPrice"                    
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtTQRDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    TxtTenderDocNo.EditValue = Sm.DrStr(dr, c[1]);
                    mMRDocNo = Sm.DrStr(dr, c[2]);
                    mMRDNo = Sm.DrStr(dr, c[3]);
                    mQtDocNo = Sm.DrStr(dr, c[4]);
                    mQtDNo = Sm.DrStr(dr, c[5]);
                    mItScCode = Sm.DrStr(dr, c[6]);
                    TxtQtDocNo.EditValue = Sm.DrStr(dr, c[4]);
                    Sm.SetLue(LueSiteCode, Sm.DrStr(dr, c[7]));
                    TxtItCode.EditValue = Sm.DrStr(dr, c[8]);
                    TxtItName.EditValue = Sm.DrStr(dr, c[9]);
                    TxtQty.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[10]), 0);                   
                    TxtEstCurCode.EditValue = Sm.DrStr(dr, c[11]);
                    TxtEstPrice.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[12]), 0);
                    TxtQtCurCode.EditValue = Sm.DrStr(dr, c[13]);
                    TxtUPrice.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[14]), 0);
                }, true
            );
        }

        #endregion

        #region Additional Methods

        private void GetParameter()
        {
            IsProcFormat = Sm.GetParameter("ProcFormatDocNo");
            mIsSiteMandatory = Sm.GetParameterBoo("IsSiteMandatory");
            mIsAutoGeneratePurchaseLocalDocNo = Sm.GetParameterBoo("IsAutoGeneratePurchaseLocalDocNo");
        }

        private bool IsPORequestApprovalForAllDept()
        {
            return Sm.GetParameter("PORequestApprovalForAllDept") == "Y";
        }

        private static string GenerateDocNo(string IsProcFormat, string DocDt, string DocType, string Tbl, string SubCategory)
        {
            string
                Yr = DocDt.Substring(2, 2),
                Mth = DocDt.Substring(4, 2),
                DocTitle = Sm.GetValue("Select ParValue From TblParameter Where ParCode='DocTitle'"),
                DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='" + DocType + "'");


            var SQL = new StringBuilder();

            if (IsProcFormat == "1")
            {
                SQL.Append("Select Concat('" + SubCategory + "', '/', ");
                SQL.Append("IfNull(( ");
                SQL.Append("   Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) From ( ");
                SQL.Append("         Select Convert(Substring(DocNo, locate('" + DocTitle + "', DocNo)-5,4), Decimal) As DocNo From " + Tbl);
                SQL.Append("       Where Right(DocNo, 5)=Concat('" + Mth + "','/', '" + Yr + "') ");
                SQL.Append("      Order By Convert(Substring(DocNo, locate('" + DocTitle + "', DocNo)-5,4), Decimal) Desc Limit 1");
                SQL.Append("       ) As Temp ");
                SQL.Append("   ), '0001') ");
                SQL.Append(", '/', '" + ((DocTitle.Length == 0) ? "XXX" : DocTitle));
                SQL.Append("', '/', '" + DocAbbr + "', '/', '" + Mth + "','/', '" + Yr + "'");
                SQL.Append(") As DocNo");
            }
            else
            {
                SQL.Append("Select Concat( ");
                SQL.Append("IfNull(( ");
                SQL.Append("   Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) From ( ");
                SQL.Append("       Select Convert(Substring(DocNo, locate('" + DocTitle + "', DocNo)-5,4), Decimal) As DocNo From " + Tbl);
                SQL.Append("       Where Right(DocNo, 5)=Concat('" + Mth + "','/', '" + Yr + "') ");
                SQL.Append("       Order By Convert(Substring(DocNo, locate('" + DocTitle + "', DocNo)-5,4), Decimal) Desc Limit 1 ");
                SQL.Append("       ) As Temp ");
                SQL.Append("   ), '0001') ");
                SQL.Append(", '/', '" + ((DocTitle.Length == 0) ? "XXX" : DocTitle));
                SQL.Append("', '/', '" + DocAbbr + "', '/', '" + Mth + "','/', '" + Yr + "'");
                SQL.Append(") As DocNo");
            }

            return Sm.GetValue(SQL.ToString());
        }

        #endregion

        #endregion

        #region Events

        #region Button Events

        private void BtnTQRDocNo_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.FormShowDialog(new FrmTenderQuotationDlg(this));
            }
        }

        #endregion

        #region Misc Control Events

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue2(Sl.SetLueSiteCode2), string.Empty);
        }

        #endregion

        #endregion

        #region Class

        private class LocalDocument
        {
            public string DocNo { set; get; }
            public string LocalDocNo { set; get; }
            public string SeqNo { get; set; }
            public string DeptCode { get; set; }
            public string ItSCCode { get; set; }
            public string Mth { get; set; }
            public string Yr { get; set; }
            public string Revision { get; set; }
        }

        #endregion

    }
}
