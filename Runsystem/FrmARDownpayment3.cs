﻿#region Update
/*
    05/03/2017 [TKG] New Application
    27/07/2020 [WED/SRN] tambahin ifnull pas save VoucherRequestDtl nya ARDP
    29/07/2020 [ICA/SRN] Membuat printout ARDP
    22/09/2020 [WED/YK] parameter IsARDownpaymentSONotMandatory kebalik fungsi nya di SetLueCtCode dan BtnSODocNo nya
    04/11/2020 [TKG/SRN] bug saat save andaikata customer kosong
    17/01/2021 [TKG/PHT] ubah GenerateVoucherRequestDocNo
    10/02/2021 [IBL/YK] tambah auto save journal berdasarkan parameter IsAutoJournalActived dan IsARDownpayment3SaveJournal
    03/03/2021 [BRI/YK] Menambahkan field bank account di ARDP berdasarkan parameter IsARDownpaymentUseBankAccount
    12/03/2021 [WED/YK] BtnSO nya dibikin mengarah juga ke SO Contract
    22/03/2021 [VIN/SRN] jika IsARDownpaymentSONotMandatory = N , luectcode mandatory
    06/04/2021 [IBL/SRN] auto save journal seperti di YK berdasarkan parameter 
    08/04/2021 [VIN/SIER] berdasarkan parameter IsShowCustomerCategory, informasi category customer dimunculkan
    09/04/2021 [IBL/SRN] tidak ada jurnal Hutang Pajak-nya (COA Master Tax) -> dihilangkan
    13/04/2021 [IBL/SRN] salah COA di JN yg terbentuk untuk coa Debitnya, seharusnya piutang usaha
    30/04/2021 [BRI/SIER] tambah dialog untuk customer dan customer category berdasarkan param IsShowCustomerCategory
    05/05/2021 [BRI/SIER] feedback dialog customer dan customer category
    10/05/2021 [BRI/SIER] tambah parameter IsFilterByCtCt
    26/01/2022 [WED/GSS] BUG saat generate Voucher Request#, belum menambahkan melihat ke parameter IsVoucherDocSeqNoEnabled
    28/01/2022 [MYA/ALL] Penyesuaian jurnal AR Downpayment dengan tambahan additional COA amount
    18/02/2022 [TKG/GSS] merubah GetParameter() dan proses save
    24/03/2022 [WED/YK] TxtCOAAmt belum di clear 0.00 saat ClearData()
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;
using System.Data;


using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

#endregion

namespace RunSystem
{
    public partial class FrmARDownpayment3 : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string 
            mMenuCode = string.Empty, mAccessInd = string.Empty,
            mDocNo = string.Empty,
            mBankAccountFormat = "0";
            //if this application is called from other application;
        internal FrmARDownpayment3Find FrmFind;
        internal bool
            mIsVoucherBankAccountFilteredByGrp = false,
            mIsShowCustomerCategory = false,
            mIsFilterByCtCt = false
            ;
        private bool 
            mIsARDownpaymentSONotMandatory = false,
            mIsARDPUseCOA = false,
            mIsAutoJournalActived = false,
            mIsARDownpayment3SaveJournal = false,
            mIsARDownpaymentUseBankAccount = false,
            mIsCOAARDownpaymentBasedOnCustomerCode = false,
            mIsARDPProcessTo1Journal = false;
        private string 
            mVoucherCodeFormatType = "1",
            mGenerateCustomerCOAFormat = "",
            mMainCurCode = "",
            mProjectAcNoFormula = "";
        
        #endregion

        #region Constructor

        public FrmARDownpayment3(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Customer's AR Downpayment";

                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                if (mIsARDownpaymentSONotMandatory) LblCtCode.ForeColor = Color.Red;
                TcARDownpayment.SelectedTabPage = TpCOA;
                TpCOA.PageVisible = mIsARDPUseCOA;
                if (mIsShowCustomerCategory)
                    LblCtCtCode.ForeColor = Color.Black;
                if (!mIsShowCustomerCategory)
                    BtnCtCt.Visible = false;
                TcARDownpayment.SelectedTabPage = TpTax;
                Sl.SetLueTaxCode(ref LueTaxCode);
                Sl.SetLueTaxCode(ref LueTaxCode2);
                Sl.SetLueTaxCode(ref LueTaxCode3);

                TcARDownpayment.SelectedTabPage = TpGeneral;
                Sl.SetLueCurCode(ref LueCurCode);
                Sl.SetLueUserCode(ref LuePIC);
                Sl.SetLueOption(ref LuePaymentType, "VoucherPaymentType");
                Sl.SetLueBankCode(ref LueBankCode);
                SetLueBankAcCode(ref LueBankAcCode, string.Empty);
                SetFormControl(mState.View);
                if (!mIsShowCustomerCategory)
                {
                    LblCtCtCode.Visible = TxtCtCtName.Visible = false;
                }
                if (!mIsARDownpaymentUseBankAccount)
                {
                    label23.Visible = false;
                    LueBankAcCode.Visible = false;
                }
                
                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grid Approval Info
            Grd2.Cols.Count = 4;
            Grd2.ReadOnly = true;
            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[]{ "Checked By", "Status", "Date", "Remark" },
                    new int[]{ 150, 100, 100, 400 }
                );
            Sm.GrdFormatDate(Grd2, new int[] { 2 });
            #endregion

            #region Grid coa

            Grd3.Cols.Count = 9;
            Grd3.FrozenArea.ColCount = 3;

            Sm.GrdHdrWithColWidth(
                    Grd3,
                    new string[]
                    {
                        //0
                        "",

                        //1-5
                        "Account#",
                        "Description",
                        "Debit",
                        "Credit",
                        "Remark",

                        //6-8
                        "", //Checkbox debit ind
                        "", //Checkbox credit ind
                        "" //CheckBox Db Cr Indicator
                    },
                     new int[]
                    {
                        //0
                        20,

                        //1-5
                        150, 300, 100, 100, 400,

                         //6-8
                        20, 20, 0
                    }
                );
            Sm.GrdColButton(Grd3, new int[] { 0 });
            Sm.GrdColCheck(Grd3, new int[] { 6, 7, 8 });
            Sm.GrdColReadOnly(Grd3, new int[] { 1, 2 });
            Sm.GrdFormatDec(Grd3, new int[] { 3, 4 }, 0);
            Sm.GrdColInvisible(Grd3, new int[] { 1, 8 }, false);
            Sm.GrdColReadOnly(true, true, Grd3, new int[] { 1, 2 });
            if (!mIsARDPProcessTo1Journal)
                Sm.GrdColInvisible(Grd3, new int[] { 6, 7, 8 }, false);

            Grd3.Cols[6].Move(3);
            Grd3.Cols[7].Move(5);

            #endregion
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueCtCode, LueCurCode, TxtAmt, LuePIC, 
                        MeeRemark, LuePaymentType, LueBankCode, TxtGiroNo, DteDueDt, 
                        MeeCancelReason, LueTaxCode, TxtTaxInvoiceNo, DteTaxInvoiceDt, LueTaxCode2, 
                        TxtTaxInvoiceNo2, DteTaxInvoiceDt2, LueTaxCode3, TxtTaxInvoiceNo3, DteTaxInvoiceDt3,
                        LueBankAcCode
                    }, true);
                    BtnSODocNo.Enabled = false;
                    BtnCtCt.Enabled = false;
                    Sm.SetControlReadOnly(ChkCancelInd, true);
                    Sm.GrdColReadOnly(true, true, Grd3, new int[] { 0, 3, 4, 5, 6, 7, 8 });
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueCurCode, TxtAmt, LuePIC, LuePaymentType, 
                        MeeRemark, LueTaxCode, TxtTaxInvoiceNo, DteTaxInvoiceDt, LueTaxCode2, 
                        TxtTaxInvoiceNo2, DteTaxInvoiceDt2, LueTaxCode3, TxtTaxInvoiceNo3, DteTaxInvoiceDt3,
                        LueBankAcCode
                    }, false);
                    if (mIsARDownpaymentSONotMandatory) Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ LueCtCode }, false);
                    BtnSODocNo.Enabled = true;
                    BtnCtCt.Enabled = true;
                    if (mIsShowCustomerCategory)
                    {
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueCtCode }, true);
                    }
                    Sm.GrdColReadOnly(false, true, Grd3, new int[] { 0, 3, 4, 5, 6, 7 });
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeCancelReason, ChkCancelInd }, false);
                    ChkCancelInd.Focus();
                    Sm.SetControlReadOnly(ChkCancelInd, false);
                    Grd3.ReadOnly = true;
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, TxtStatus, TxtSODocNo, TxtNoOfDownpayment, 
                LueCtCode, TxtCtPONo, TxtSOCurCode, LueCurCode, LuePIC,  
                LuePaymentType, LueBankCode, TxtGiroNo, DteDueDt, MeeCancelReason,
                TxtVoucherRequestDocNo, TxtVoucherDocNo, MeeRemark, LueTaxCode, TxtTaxInvoiceNo, 
                DteTaxInvoiceDt, LueTaxCode2, TxtTaxInvoiceNo2, DteTaxInvoiceDt2, LueTaxCode3, 
                TxtTaxInvoiceNo3, DteTaxInvoiceDt3, LueBankAcCode
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> 
            { 
                TxtSOAmt, TxtOtherAmt, TxtAmt, TxtTaxAmt, TxtTaxAmt2, 
                TxtTaxAmt3, TxtAmtAftTax, TxtCOAAmt
            }, 0);
            ChkCancelInd.Checked = false;
            Grd2.Rows.Clear();
            Grd2.Rows.Count = 1;
            Grd3.Rows.Clear();
            Grd3.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd3, 0, new int[] { 3, 4 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmARDownpayment3Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                if (mIsARDownpaymentSONotMandatory)
                    SetLueCtCode(ref LueCtCode, string.Empty);
                else
                    BtnSODocNo_Click(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            if (ChkCancelInd.Checked == false)
            {
                ParPrint();
            }
        }

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var SQL = new StringBuilder();

                SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelInd, A.SODocNo, ");
                SQL.AppendLine("A.CurCode, A.Amt, A.PIC, A.VoucherRequestDocNo, A.CancelReason, A.Remark, ");
                SQL.AppendLine("B.CurCode As SOCurCode, ");
                SQL.AppendLine("B.Amt As SOAmt, ");
                SQL.AppendLine("IfNull(B.CtCode, A.CtCode) As CtCode, ");
                SQL.AppendLine("C.VoucherDocNo,  ");
                SQL.AppendLine("Case IfNull(A.Status, '') ");
                SQL.AppendLine("    When 'O' Then 'Outstanding' ");
                SQL.AppendLine("    When 'A' Then 'Approved' ");
                SQL.AppendLine("    When 'C' Then 'Cancel' End ");
                SQL.AppendLine("As StatusDesc,  A.PaymentType, A.BankAcCode, A.BankCode, A.GiroNo, A.DueDt,  ");
                SQL.AppendLine("IfNull((Select Count(DocNo) From TblARDownpayment Where SODocNo=A.SODocNo And CancelInd='N'), 0) As NoOfDownpayment, A.VoucherRequestDocNo2, D.VoucherDocNo As VoucherDocNo2, ");
                SQL.AppendLine("IfNull(( ");
                SQL.AppendLine("    Select Sum(Amt) ");
                SQL.AppendLine("    From TblARDownpayment ");
                SQL.AppendLine("    Where SODocNo=A.SODocNo ");
                SQL.AppendLine("    And DocNo<>@DocNo ");
                SQL.AppendLine("    And CancelInd='N' ");
                SQL.AppendLine("    And Status In ('O', 'A') ");
                SQL.AppendLine("), 0) As OtherAmt, ");
                SQL.AppendLine("B.CtPONo, ");
                SQL.AppendLine("A.TaxInvoiceNo, A.TaxInvoiceDt, A.TaxCode, A.TaxAmt, ");
                SQL.AppendLine("A.TaxInvoiceNo2, A.TaxInvoiceDt2, A.TaxCode2, A.TaxAmt2, ");
                SQL.AppendLine("A.TaxInvoiceNo3, A.TaxInvoiceDt3, A.TaxCode3, A.TaxAmt3 ");
                SQL.AppendLine("From TblARDownpayment A ");
                SQL.AppendLine("Left Join TblSOHdr B On A.SODocNo=B.DocNo ");
                SQL.AppendLine("Left Join TblVoucherRequestHdr C On A.VoucherRequestDocNo=C.DocNo ");
                SQL.AppendLine("Left Join TblVoucherRequestHdr D On A.VoucherRequestDocNo2=D.DocNo ");
                SQL.AppendLine("Where A.DocNo=@DocNo;");

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
                Sm.ShowDataInCtrl(
                        ref cm, SQL.ToString(),
                        new string[] 
                        {
                            //0
                            "DocNo", 

                            //1-5
                            "DocDt", "StatusDesc", "CancelInd", "SODocNo", "NoOfDownpayment",
                            
                            //6-10
                            "CtCode", "SOCurcode", "SOAmt", "CurCode", "Amt",  
                            
                            //11-15
                            "PIC", "VoucherRequestDocNo", "VoucherDocNo",  "paymentType", "bankCode", 
                            
                            //16-20
                            "GiroNo", "DueDt", "CancelReason", "VoucherRequestDocNo2", "VoucherDocNo2", 
                            
                            //21-25
                            "Remark", "OtherAmt", "CtPONo", "TaxInvoiceNo", "TaxInvoiceDt", 
                            
                            //26-30
                            "TaxCode", "TaxAmt", "TaxInvoiceNo2", "TaxInvoiceDt2", "TaxCode2", 
                            
                            //31-35
                            "TaxAmt2", "TaxInvoiceNo3", "TaxInvoiceDt3", "TaxCode3", "TaxAmt3",

                            //36
                            "BankAcCode"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                            Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                            TxtStatus.EditValue = Sm.DrStr(dr, c[2]);
                            MeeCancelReason.EditValue = Sm.DrStr(dr, c[18]);
                            ChkCancelInd.Checked = Sm.DrStr(dr, c[3]) == "Y";
                            TxtSODocNo.EditValue = Sm.DrStr(dr, c[4]);
                            TxtNoOfDownpayment.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[5]), 2);
                            SetLueCtCode(ref LueCtCode, Sm.DrStr(dr, c[6]));
                            TxtSOCurCode.EditValue = Sm.DrStr(dr, c[7]);
                            TxtSOAmt.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[8]), 0);
                            Sm.SetLue(LueCurCode, Sm.DrStr(dr, c[9]));
                            TxtAmt.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[10]), 0);
                            Sm.SetLue(LuePIC, Sm.DrStr(dr, c[11]));
                            TxtVoucherRequestDocNo.EditValue = Sm.DrStr(dr, c[12]);
                            TxtVoucherDocNo.EditValue = Sm.DrStr(dr, c[13]);
                            Sm.SetLue(LuePaymentType, Sm.DrStr(dr, c[14]));
                            Sm.SetLue(LueBankCode, Sm.DrStr(dr, c[15]));
                            TxtGiroNo.EditValue = Sm.DrStr(dr, c[16]);
                            Sm.SetDte(DteDueDt, Sm.DrStr(dr, c[17]));
                            TxtVoucherRequestDocNo2.EditValue = (mIsARDPProcessTo1Journal ? Sm.DrStr(dr, c[12]) : Sm.DrStr(dr, c[19]));
                            TxtVoucherDocNo2.EditValue = (mIsARDPProcessTo1Journal ? Sm.DrStr(dr, c[13]) : Sm.DrStr(dr, c[20]));
                            MeeRemark.EditValue = Sm.DrStr(dr, c[21]);
                            TxtOtherAmt.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[22]), 0);
                            TxtCtPONo.EditValue = Sm.DrStr(dr, c[23]);
                            TxtTaxInvoiceNo.EditValue = Sm.DrStr(dr, c[24]);
                            Sm.SetDte(DteTaxInvoiceDt, Sm.DrStr(dr, c[25]));
                            Sm.SetLue(LueTaxCode, Sm.DrStr(dr, c[26]));
                            TxtTaxAmt.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[27]), 0);
                            TxtTaxInvoiceNo2.EditValue = Sm.DrStr(dr, c[28]);
                            Sm.SetDte(DteTaxInvoiceDt2, Sm.DrStr(dr, c[29]));
                            Sm.SetLue(LueTaxCode2, Sm.DrStr(dr, c[30]));
                            TxtTaxAmt2.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[31]), 0);
                            TxtTaxInvoiceNo3.EditValue = Sm.DrStr(dr, c[32]);
                            Sm.SetDte(DteTaxInvoiceDt3, Sm.DrStr(dr, c[33]));
                            Sm.SetLue(LueTaxCode3, Sm.DrStr(dr, c[34]));
                            TxtTaxAmt3.EditValue = Sm.FormatNum(Sm.DrStr(dr, c[35]), 0);
                            SetLueBankAcCode(ref LueBankAcCode, Sm.DrStr(dr, c[36]));
                            TxtAmtAftTax.EditValue = Sm.FormatNum(
                                decimal.Parse(TxtAmt.Text) +
                                decimal.Parse(TxtTaxAmt.Text) +
                                decimal.Parse(TxtTaxAmt2.Text) +
                                decimal.Parse(TxtTaxAmt3.Text)
                                , 0);
                        }, true
                    );
                    ShowDocApproval(DocNo);
                    ShowARDownPaymentDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowARDownPaymentDtl(string DocNo)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();
            SQL.AppendLine("Select A.AcNo, B.AcDesc, A.DAmt, A.CAmt, A.Remark, A.ActInd ");
            SQL.AppendLine("From TblARDownPaymentDtl A, TblCOA B ");
            SQL.AppendLine("Where A.DocNo=@DocNo And A.AcNo=B.AcNo Order By A.DNo;");

            Sm.ShowDataInGrid(
                ref Grd3, ref cm, SQL.ToString(),
                new string[] { "AcNo", "AcDesc", "DAmt", "CAmt", "Remark", "ActInd" },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 8, 5);
                    if (Sm.DrStr(dr, c[5]) == "Y")
                    {
                        if (Sm.GetGrdDec(Grd3, Row, 3) > 0)
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 6, 5);
                        else
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 7, 5);
                    }
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd3, Grd3.Rows.Count - 1, new int[] { 3, 4 });
            Sm.FocusGrd(Grd3, 0, 1);
            ComputeCOAAmt();
        }


        private void ShowDocApproval(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.ApprovalDNo, B.UserName, A.LastUpDt, A.Remark, ");
            SQL.AppendLine("Case Status When 'A' Then 'Approved' When 'C' Then 'Cancelled' When 'O' Then 'Outstanding' End As StatusDesc ");
            SQL.AppendLine("From TblDocApproval A ");
            SQL.AppendLine("Left Join TblUser B On A.UserCode = B.UserCode ");
            SQL.AppendLine("Where A.DocType='ARDownpayment' ");
            SQL.AppendLine("And IfNull(Status, 'O')<>'O' ");
            SQL.AppendLine("And A.DocNo=@DocNo ");
            SQL.AppendLine("Order By A.ApprovalDNo;");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                    ref Grd2, ref cm,
                    SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "UserName",
                        
                        //1-3
                        "StatusDesc","LastUpDt", "Remark"

                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    }, false, false, false, false
            );
            Sm.FocusGrd(Grd2, 0, 0);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "ARDownpayment", "TblARDownpayment");
            string VoucherRequestDocNo = string.Empty;

            if (mVoucherCodeFormatType == "2")
                VoucherRequestDocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "VoucherRequest", "TblVoucherRequestHdr", "1");
            else
                VoucherRequestDocNo = GenerateVoucherRequestDocNo();

            var cml = new List<MySqlCommand>();
            cml.Add(SaveARDownPayment(DocNo, VoucherRequestDocNo));

            if (Grd3.Rows.Count > 1)
            {
                cml.Add(SaveARDownPaymentDtl2(DocNo));
                //for (int Row = 0; Row < Grd3.Rows.Count; Row++)
                //    if (Sm.GetGrdStr(Grd3, Row, 1).Length > 0) cml.Add(SaveARDownPaymentDtl2(DocNo, Row));
            }

            if (mIsAutoJournalActived && mIsARDownpayment3SaveJournal)
                cml.Add(SaveJournal(DocNo));

            Sm.ExecCommands(cml);

            if (!mIsARDPProcessTo1Journal) InsertDataVR(DocNo);

            ShowData(DocNo);
        }

        private void InsertDataVR(string ARDocNo)
        {
            var cml = new List<MySqlCommand>();
            if (mIsARDPUseCOA && Grd3.Rows.Count>1)
            {
                var VoucherRequestDocNo2 = string.Empty;

                if (mVoucherCodeFormatType == "2")
                    VoucherRequestDocNo2 = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "VoucherRequest", "TblVoucherRequestHdr", "1");
                else
                    VoucherRequestDocNo2 = GenerateVoucherRequestDocNo();

                cml.Add(SaveVoucherRequestHdr(VoucherRequestDocNo2, ARDocNo));
                cml.Add(SaveVoucherRequestDtl(VoucherRequestDocNo2));
            }
            Sm.ExecCommands(cml);
        }

        private string GenerateVoucherRequestDocNo()
        {
            var SQL = new StringBuilder();
            bool IsDocSeqNoEnabled = Sm.GetParameterBoo("IsDocSeqNoEnabled");
            bool IsVoucherDocSeqNoEnabled = Sm.GetParameterBoo("IsVoucherDocSeqNo");
            string DocSeqNo = "4";

            if (IsDocSeqNoEnabled || IsVoucherDocSeqNoEnabled) DocSeqNo = Sm.GetParameter("DocSeqNo");

            SQL.Append("Select Concat( ");
            SQL.Append("@Yr,'/', @Mth, '/', ");
            SQL.Append("( ");
            SQL.Append("    Select IfNull((");
            SQL.Append("    Select Right(Concat(Repeat('0', " + DocSeqNo + "), Convert(DocNo+1, Char)), " + DocSeqNo + ") As Numb From ( ");
            SQL.Append("Select Convert(SUBSTRING(DocNo,7," + DocSeqNo + "), Decimal) As DocNo ");
            //SQL.Append("        Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) As Numb ");
            //SQL.Append("        From ( ");
            //SQL.Append("            Select Convert(Substring(DocNo, 7, 4), Decimal) As DocNo ");
            SQL.Append("            From TblVoucherRequestHdr ");
            SQL.Append("            Where Left(DocNo, 5)= Concat(@Yr,'/', @Mth) ");
            SQL.Append("            Order By Substring(DocNo, 7, " + DocSeqNo + ") Desc Limit 1 ");
            SQL.Append("        ) As Temp ");
            SQL.Append("    ), Right(Concat(Repeat('0', " + DocSeqNo + "), '1'), " + DocSeqNo + ") ");
            //SQL.Append("    ), '0001' ");
            SQL.Append(") As Number ");
            SQL.Append("), '/', ");
            SQL.Append("(Select DocAbbr From TblDocAbbreviation Where DocType='VoucherRequest') ");
            SQL.Append(") As DocNo ");
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetDte(DteDocDt).Substring(2, 2));
            Sm.CmParam<String>(ref cm, "@Mth", Sm.GetDte(DteDocDt).Substring(4, 2));
            return Sm.GetValue(cm);
        }

        //private string GenerateVoucherRequestDocNo()
        //{
        //    var SQL = new StringBuilder();
        //    SQL.Append("Select Concat( ");
        //    SQL.Append("@Yr,'/', @Mth, '/', ");
        //    SQL.Append("( ");
        //    SQL.Append("    Select IfNull((");
        //    SQL.Append("        Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) As Numb ");
        //    SQL.Append("        From ( ");
        //    SQL.Append("            Select Convert(Substring(DocNo, 7, 4), Decimal) As DocNo ");
        //    SQL.Append("            From TblVoucherRequestHdr ");
        //    SQL.Append("            Where Left(DocNo, 5)= Concat(@Yr,'/', @Mth) ");
        //    SQL.Append("            Order By Substring(DocNo, 7, 5) Desc Limit 1 ");
        //    SQL.Append("        ) As Temp ");
        //    SQL.Append("    ), '0001') As Number ");
        //    SQL.Append("), '/', ");
        //    SQL.Append("(Select DocAbbr From TblDocAbbreviation Where DocType='VoucherRequest') ");
        //    SQL.Append(") As DocNo ");
        //    var cm = new MySqlCommand(){ CommandText = SQL.ToString() };
        //    Sm.CmParam<String>(ref cm, "@Yr", Sm.GetDte(DteDocDt).Substring(2, 2));
        //    Sm.CmParam<String>(ref cm, "@Mth", Sm.GetDte(DteDocDt).Substring(4, 2));
        //    return Sm.GetValue(cm);
        //}

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueCurCode, "CurCode") ||
                Sm.IsTxtEmpty(TxtAmt, "Amount", true) ||
                Sm.IsLueEmpty(LuePaymentType, "Payment Type") ||
                (mIsARDownpaymentSONotMandatory && Sm.IsLueEmpty(LueCtCode, "Customer")) ||
                IsPaymentTypeNotValid() ||
                (mIsARDownpaymentUseBankAccount && Sm.IsTxtEmpty(LueBankAcCode, "Bank Account", false)) ||
                Sm.IsLueEmpty(LuePIC, "Person in charge") ||
                Sm.IsMeeEmpty(MeeRemark, "Remark") ||
                IsGrdValueNotValid() ||
                IsAmtNotValid();
        }

        private bool IsAmtNotValid()
        {
            decimal SOAmt = decimal.Parse(TxtSOAmt.Text);
            decimal OtherAmt = decimal.Parse(TxtOtherAmt.Text);
            decimal Amt = decimal.Parse(TxtAmt.Text);

            if ((OtherAmt + Amt) > SOAmt)
            {
                if (Sm.StdMsgYN("Question",
                    "SO : " + Sm.FormatNum(SOAmt, 0) + Environment.NewLine +
                    "Other Downpayment : " + Sm.FormatNum(OtherAmt, 0) + Environment.NewLine +
                    "Downpayment : " + Sm.FormatNum(Amt, 0) + Environment.NewLine + Environment.NewLine +
                    "AR downpayment amount is bigger than SO's amount." + Environment.NewLine +
                    "Do you want to continue to save this data ?"
                    ) == DialogResult.No)
                    return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
            {
                if (Grd3.Rows.Count > 1)
                {
                    for (int RowX = 0; RowX < Grd3.Rows.Count - 1; RowX++)
                    {
                        if (Sm.IsGrdValueEmpty(Grd3, RowX, 1, false, "COA's account is empty.")) return true;
                        if (Sm.GetGrdDec(Grd3, RowX, 3) == 0m && Sm.GetGrdDec(Grd3, RowX, 4) == 0m)
                        {
                            Sm.StdMsg(mMsgType.Warning,
                                "Account# : " + Sm.GetGrdStr(Grd3, RowX, 1) + Environment.NewLine +
                                "Description : " + Sm.GetGrdStr(Grd3, RowX, 2) + Environment.NewLine + Environment.NewLine +
                                "Both debit and credit amount can't be 0.");
                            return true;
                        }
                    }
                }
            }

            return false;
        }

        private bool IsPaymentTypeNotValid()
        {
            if (Sm.CompareStr(Sm.GetLue(LuePaymentType), "B"))
            {
                if (Sm.IsLueEmpty(LueBankCode, "Bank")) return true;
            }

            if (Sm.CompareStr(Sm.GetLue(LuePaymentType), "G") || Sm.CompareStr(Sm.GetLue(LuePaymentType), "K"))
            {
                if (Sm.IsLueEmpty(LueBankCode, "Bank")) return true;
                if (Sm.IsTxtEmpty(TxtGiroNo, "Giro Bilyet/Cheque Number ", false)) return true;
                if (Sm.IsDteEmpty(DteDueDt, "Due Date ")) return true;
            }

            return false;
        }

        private MySqlCommand SaveARDownPayment(string DocNo, string VoucherRequestDocNo)
        {
            var SQL = new StringBuilder();

            #region save AR

            SQL.AppendLine("Insert Into TblARDownpayment ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, Status, SODocNo, CtCode, PaymentType, BankAcCode, BankCode, GiroNo, DueDt, VoucherRequestDocNo, CurCode, Amt, PIC, ");
            SQL.AppendLine("DownpaymentPercentage, AmtBefTax, ");
            SQL.AppendLine("TaxInvoiceNo, TaxInvoiceDt, TaxCode, TaxAmt, ");
            SQL.AppendLine("TaxInvoiceNo2, TaxInvoiceDt2, TaxCode2, TaxAmt2, ");
            SQL.AppendLine("TaxInvoiceNo3, TaxInvoiceDt3, TaxCode3, TaxAmt3, ");
            SQL.AppendLine("Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'N', 'O', @SODocNo, @CtCode, @PaymentType, @BankAcCode, @BankCode, @GiroNo, @DueDt, @VoucherRequestDocNo, @CurCode, @Amt, @PIC, ");
            SQL.AppendLine("0.00, 0.00, ");
            SQL.AppendLine("@TaxInvoiceNo, @TaxInvoiceDt, @TaxCode, @TaxAmt, ");
            SQL.AppendLine("@TaxInvoiceNo2, @TaxInvoiceDt2, @TaxCode2, @TaxAmt2, ");
            SQL.AppendLine("@TaxInvoiceNo3, @TaxInvoiceDt3, @TaxCode3, @TaxAmt3, ");
            SQL.AppendLine("@Remark, @CreateBy, CurrentDateTime()); ");
            
            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblDocApprovalSetting T ");
            SQL.AppendLine("Where T.DocType='ARDownpayment' ");
            SQL.AppendLine("And (T.StartAmt=0.00 ");
            SQL.AppendLine("Or T.StartAmt<=IfNull(( ");
            SQL.AppendLine("    Select A.Amt*IfNull(B.Amt, 1) ");
            SQL.AppendLine("    From TblARDownpayment A ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select D1.CurCode1, D1.Amt ");
            SQL.AppendLine("        From TblCurrencyRate D1 ");
            SQL.AppendLine("        Inner Join ( ");
            SQL.AppendLine("            Select CurCode1, Max(RateDt) RateDt ");
            SQL.AppendLine("            From TblCurrencyRate ");
            SQL.AppendLine("            Where CurCode2=(Select ParValue From TblParameter Where ParCode='MainCurCode') ");
            SQL.AppendLine("            Group By CurCode1 ");
            SQL.AppendLine("        ) D2 On D1.CurCode1=D2.CurCode1 And D1.RateDt=D2.RateDt ");
            SQL.AppendLine("    ) B On A.CurCode=B.CurCode1 ");
            SQL.AppendLine("    Where A.DocNo=@DocNo ");
            SQL.AppendLine("), 0.00)); ");

            SQL.AppendLine("Update TblARDownpayment Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo And Not Exists( ");
            SQL.AppendLine("    Select 1 From TblDocApproval ");
            SQL.AppendLine("    Where DocType='ARDownpayment' ");
            SQL.AppendLine("    And DocNo=@DocNo ");
            SQL.AppendLine("    ); ");
            
            #endregion

            #region Save VR

            SQL.AppendLine("Insert Into TblVoucherRequestHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, Status, DeptCode, DocType, VoucherDocNo, AcType, BankAcCode, PaymentType, GiroNo, BankCode, DueDt, PIC, DocEnclosure, CurCode, Amt, PaymentUser, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@VoucherRequestDocNo, @DocDt, 'N', 'O', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where ParCode='ARDownPaymentDeptCode'), ");
            SQL.AppendLine("'62', Null, 'D', Null, @PaymentType, @GiroNo, @BankCode, @DueDt, @PIC, 0, @CurCode, " + (mIsARDPProcessTo1Journal ? "@Amt2" : "@Amt") + "+@TaxAmt+@TaxAmt2+@TaxAmt3, Null, ");
            if (mIsARDownpaymentSONotMandatory)
                SQL.AppendLine("Concat(@CtName, '. ', @Remark), ");
            else
                SQL.AppendLine("Concat(@SODocNo, ' (', @CtName, '). ', @Remark), ");
            SQL.AppendLine("@CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblVoucherRequestDtl(DocNo, DNo, Description, Amt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@VoucherRequestDocNo, '001', ");
            if (mIsARDownpaymentSONotMandatory)
            {
                if (Sm.GetLue(LueCtCode).Length == 0)
                {
                    //SQL.AppendLine("Concat(@CtName, '. ', @Remark), ");
                    SQL.AppendLine("@Remark, ");
                }
                else
                {
                    SQL.AppendLine("Concat(@CtName, ' : ', IfNull(@Remark, '-')), ");
                }
            }
            else
            {
                //SQL.AppendLine("Concat(@SODocNo, ' (', @CtName, '). ', @Remark), ");
                SQL.AppendLine("Concat(Case When @SODocNo Is Null Then '-' Else @SODocNo End, ");
                SQL.AppendLine("Case When @CtName Is Null Then '' Else Concat(' (', @CtName, '). ') End, ");
                SQL.AppendLine("Case When @Remark is Null Then '' Else Concat(' ', @Remark) End ");
                SQL.AppendLine("), ");
            }
            SQL.AppendLine("@Amt+@TaxAmt+@TaxAmt2+@TaxAmt3, Null, @CreateBy, CurrentDateTime()); ");

            if (mIsARDPProcessTo1Journal && Decimal.Parse(TxtCOAAmt.Text) != 0)
            {
                SQL.AppendLine("Insert Into TblVoucherRequestDtl(DocNo, DNo, Description, Amt, Remark, CreateBy, CreateDt) ");
                SQL.AppendLine("Values (@VoucherRequestDocNo, '002', 'Customer Account Receivable Downpayment', @Amt3, Null, @CreateBy, CurrentDateTime()); ");
            }

            SQL.AppendLine("Update TblVoucherRequestHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@VoucherRequestDocNo And Not Exists( ");
            SQL.AppendLine("    Select DocNo From TblDocApproval ");
            SQL.AppendLine("    Where DocType='VoucherRequest' ");
            SQL.AppendLine("    And DocNo=@VoucherRequestDocNo ");
            SQL.AppendLine("    ); ");
            
            #endregion

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@SODocNo", TxtSODocNo.Text);
            if (Sm.GetLue(LueCtCode).Length == 0)
            {
                Sm.CmParam<String>(ref cm, "@CtCode", string.Empty);
                Sm.CmParam<String>(ref cm, "@CtName", string.Empty);
            }
            else
            {
                Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
                Sm.CmParam<String>(ref cm, "@CtName", LueCtCode.GetColumnValue("Col2").ToString());
            }
            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo", VoucherRequestDocNo);
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtAmt.Text));
            Sm.CmParam<Decimal>(ref cm, "@Amt2", Decimal.Parse(TxtAmt.Text) + Decimal.Parse(TxtCOAAmt.Text));
            Sm.CmParam<Decimal>(ref cm, "@Amt3", Decimal.Parse(TxtCOAAmt.Text));
            Sm.CmParam<String>(ref cm, "@PIC", Sm.GetLue(LuePIC));
            Sm.CmParam<String>(ref cm, "@PaymentType", Sm.GetLue(LuePaymentType));
            Sm.CmParam<String>(ref cm, "@BankAcCode", Sm.GetLue(LueBankAcCode));
            Sm.CmParam<String>(ref cm, "@BankCode", Sm.GetLue(LueBankCode));
            Sm.CmParam<String>(ref cm, "@GiroNo", TxtGiroNo.Text);
            Sm.CmParamDt(ref cm, "@DueDt", Sm.GetDte(DteDueDt));
            Sm.CmParam<String>(ref cm, "@TaxCode", Sm.GetLue(LueTaxCode));
            Sm.CmParam<String>(ref cm, "@TaxInvoiceNo", TxtTaxInvoiceNo.Text);
            Sm.CmParamDt(ref cm, "@TaxInvoiceDt", Sm.GetDte(DteTaxInvoiceDt));
            Sm.CmParam<Decimal>(ref cm, "@TaxAmt", decimal.Parse(TxtTaxAmt.Text));
            Sm.CmParam<String>(ref cm, "@TaxCode2", Sm.GetLue(LueTaxCode2));
            Sm.CmParam<String>(ref cm, "@TaxInvoiceNo2", TxtTaxInvoiceNo2.Text);
            Sm.CmParamDt(ref cm, "@TaxInvoiceDt2", Sm.GetDte(DteTaxInvoiceDt2));
            Sm.CmParam<Decimal>(ref cm, "@TaxAmt2", decimal.Parse(TxtTaxAmt2.Text));
            Sm.CmParam<String>(ref cm, "@TaxCode3", Sm.GetLue(LueTaxCode3));
            Sm.CmParam<String>(ref cm, "@TaxInvoiceNo3", TxtTaxInvoiceNo3.Text);
            Sm.CmParamDt(ref cm, "@TaxInvoiceDt3", Sm.GetDte(DteTaxInvoiceDt3));
            Sm.CmParam<Decimal>(ref cm, "@TaxAmt3", decimal.Parse(TxtTaxAmt3.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveARDownPaymentDtl2(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            bool IsFirstOrExisted = true;

            SQL.AppendLine("/* AR Downpayment - Dtl */ ");
            SQL.AppendLine("Set @Dt=CurrentDateTime(); ");

            for (int r = 0; r < Grd3.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd3, r, 1).Length > 0)
                {
                    if (IsFirstOrExisted)
                    {
                        SQL.AppendLine("Insert Into TblARDownpaymentDtl(DocNo, DNo, AcNo, DAmt, CAmt, ActInd, Remark, CreateBy, CreateDt) ");
                        SQL.AppendLine("Values ");

                        IsFirstOrExisted = false;
                    }
                    else
                        SQL.AppendLine(", ");

                    SQL.AppendLine(
                        " (@DocNo, @DNo_" + r.ToString() +
                        ", @AcNo_" + r.ToString() +
                        ", @DAmt_" + r.ToString() +
                        ", @CAmt_" + r.ToString() +
                        ", @ActInd_" + r.ToString() +
                        ", @Remark_" + r.ToString() +
                        ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@AcNo_" + r.ToString(), Sm.GetGrdStr(Grd3, r, 1));
                    Sm.CmParam<Decimal>(ref cm, "@DAmt_" + r.ToString(), Sm.GetGrdDec(Grd3, r, 3));
                    Sm.CmParam<Decimal>(ref cm, "@CAmt_" + r.ToString(), Sm.GetGrdDec(Grd3, r, 4));
                    Sm.CmParam<String>(ref cm, "@ActInd_" + r.ToString(), Sm.GetGrdBool(Grd3, r, 8) ? "Y" : "N");
                    Sm.CmParam<String>(ref cm, "@Remark_" + r.ToString(), Sm.GetGrdStr(Grd3, r, 5));
                }
            }

            if (!IsFirstOrExisted) SQL.AppendLine("; ");

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        //private MySqlCommand SaveARDownPaymentDtl2(string DocNo, int Row)
        //{
        //    var cm = new MySqlCommand()
        //    {
        //        CommandText =
        //            "Insert Into TblARDownpaymentDtl(DocNo, DNo, AcNo, DAmt, CAmt, ActInd, Remark, CreateBy, CreateDt) " +
        //            "Values(@DocNo, @DNo, @AcNo, @DAmt, @CAmt, @ActInd, @Remark, @CreateBy, CurrentDateTime()); "
        //    };
        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
        //    Sm.CmParam<String>(ref cm, "@AcNo", Sm.GetGrdStr(Grd3, Row, 1));
        //    Sm.CmParam<Decimal>(ref cm, "@DAmt", Sm.GetGrdDec(Grd3, Row, 3));
        //    Sm.CmParam<Decimal>(ref cm, "@CAmt", Sm.GetGrdDec(Grd3, Row, 4));
        //    Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd3, Row, 5));
        //    Sm.CmParam<String>(ref cm, "@ActInd", Sm.GetGrdBool(Grd3, Row, 8) ? "Y" : "N");
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    return cm;
        //}


        //save vr additional COA

        private MySqlCommand SaveVoucherRequestHdr(string VoucherRequestDocNo, string ARDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblVoucherRequestHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, Status, MInd, DeptCode, DocType, VoucherDocNo, ");
            SQL.AppendLine("AcType, PaymentType, BankAcCode, BankCode, GiroNo, DueDt, ");
            SQL.AppendLine("PIC, CurCode, Amt, PaymentUser, ");
            SQL.AppendLine("Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@DocNo, @DocDt, 'N', 'O', 'N', (Select ParValue From TblParameter Where ParCode='ARDownPaymentDeptCode'), '20', Null, ");
            SQL.AppendLine("'D', @PaymentType, Null, @BankCode, @GiroNo, @DueDt, ");
            SQL.AppendLine("@PIC, @CurCode, @Amt, Null, ");
            SQL.AppendLine("@Remark, @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblDocApprovalSetting T ");
            SQL.AppendLine("Where T.DocType='VoucherRequest' ");
            SQL.AppendLine("And T.DeptCode In ( ");
            SQL.AppendLine("    Select ParValue From TblParameter ");
            SQL.AppendLine("    Where ParCode='IncomingPaymentDeptCode' ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("And (T.StartAmt=0 ");
            SQL.AppendLine("Or T.StartAmt<=IfNull(( ");
            SQL.AppendLine("    Select A.Amt*IfNull(B.Amt, 1) ");
            SQL.AppendLine("    From TblVoucherRequestHdr A ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select B1.CurCode1, B1.Amt ");
            SQL.AppendLine("        From TblCurrencyRate B1 ");
            SQL.AppendLine("        Inner Join ( ");
            SQL.AppendLine("            Select CurCode1, Max(RateDt) RateDt ");
            SQL.AppendLine("            From TblCurrencyRate ");
            SQL.AppendLine("            Where CurCode2=(Select ParValue From TblParameter Where ParCode='MainCurCode') ");
            SQL.AppendLine("            Group By CurCode1 ");
            SQL.AppendLine("        ) B2 On B1.CurCode1=B2.CurCode1 And B1.RateDt=B2.RateDt ");
            SQL.AppendLine("    ) B On A.CurCode=B.CurCode1 ");
            SQL.AppendLine("    Where A.DocNo=@DocNo ");
            SQL.AppendLine("), 0)); ");

            SQL.AppendLine("Update TblVoucherRequestHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select DocNo From TblDocApproval ");
            SQL.AppendLine("    Where DocType='VoucherRequest' And DocNo=@DocNo ");
            SQL.AppendLine("); ");

            SQL.AppendLine("Update TblARDownPayment Set VoucherRequestDocNo2=@DocNo ");
            SQL.AppendLine("Where DocNo=@ARDocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", VoucherRequestDocNo);
            Sm.CmParam<String>(ref cm, "@ARDocNo", ARDocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetParameter("IncomingPaymentDeptCode"));
            Sm.CmParam<String>(ref cm, "@PaymentType", Sm.GetLue(LuePaymentType));
            Sm.CmParam<String>(ref cm, "@BankCode", Sm.GetLue(LueBankCode));
            Sm.CmParam<String>(ref cm, "@GiroNo", TxtGiroNo.Text);
            Sm.CmParamDt(ref cm, "@DueDt", Sm.GetDte(DteDueDt));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<String>(ref cm, "@PIC", Sm.GetLue(LuePIC));
            Sm.CmParam<Decimal>(ref cm, "@Amt", decimal.Parse(TxtCOAAmt.Text));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveVoucherRequestDtl(string VoucherRequestDocNo)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblVoucherRequestDtl(DocNo, DNo, Description, Amt, Remark, CreateBy, CreateDt) " +
                    "Values (@DocNo, @DNo, @Description, @Amt, Null, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", VoucherRequestDocNo);
            Sm.CmParam<String>(ref cm, "@DNo", "001");
            Sm.CmParam<String>(ref cm, "@Description", MeeRemark.Text);
            Sm.CmParam<Decimal>(ref cm, "@Amt", Decimal.Parse(TxtCOAAmt.Text));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal(string DocNo)
        {
            var SQL = new StringBuilder();
            var CurCode = Sm.GetLue(LueCurCode);

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Update TblARDownpayment Set JournalDocNo=@JournalDocNo Where DocNo=@DocNo;");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@JournalDocNo, ");
            SQL.AppendLine("@DocDt, ");
            SQL.AppendLine("Concat('Customer AR Downpayment With Tax : ', @DocNo), ");
            SQL.AppendLine("@MenuCode, (Select MenuDesc From TblMenu Where MenuCode=@MenuCode), ");
            SQL.AppendLine("@Remark, @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("T.AcNo, T.DAmt, T.CAMt, Null As Remark, @CreateBy As CreateBy, CurrentDateTime() As CreateDt ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From (");

            if (mIsCOAARDownpaymentBasedOnCustomerCode)
            {
                if (Sm.CompareStr(mMainCurCode, CurCode))
                {
                    SQL.AppendLine("        Select Concat(B.ParValue, C.CtCode) As AcNo, ");
                    SQL.AppendLine("        IfNull(A.Amt,0.00) As DAmt, ");
                    SQL.AppendLine("        0.00 As CAmt ");
                    SQL.AppendLine("        From TblARDownpayment A ");
                    SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='CustomerAcNoAR' And B.ParValue Is Not Null ");
                    SQL.AppendLine("        Inner Join TblCustomer C On A.CtCode = C.CtCode ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo ");
                }
                else
                {
                    SQL.AppendLine("       Select Concat(B.ParValue, C.CtCode) As AcNo, ");
                    SQL.AppendLine("       IfNull(( ");
                    SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                    SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                    SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                    SQL.AppendLine("       ), 0.00) * ");
                    SQL.AppendLine("       (IfNull(A.Amt,0.00) As DAmt, ");
                    SQL.AppendLine("       0.00 As CAmt ");
                    SQL.AppendLine("       From TblARDownpayment A ");
                    SQL.AppendLine("       Inner Join TblParameter B On B.ParCode='CustomerAcNoAR' And B.ParValue Is Not Null ");
                    SQL.AppendLine("       Inner Join TblCustomer C On A.CtCode = C.CtCode ");
                    SQL.AppendLine("       Where A.DocNo=@DocNo ");
                }

                if (Sm.CompareStr(mMainCurCode, CurCode))
                {
                    SQL.AppendLine("        Union All ");
                    SQL.AppendLine("        Select B.ParValue As AcNo, ");
                    SQL.AppendLine("        0.00 As DAmt, ");
                    SQL.AppendLine("        IfNull(A.Amt,0.00) As CAmt ");
                    SQL.AppendLine("        From TblARDownpayment A ");
                    SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='AcNoForShortTermUnearnedLeases' And B.ParValue Is Not Null ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo ");
                }
                else
                {
                    SQL.AppendLine("       Union All ");
                    SQL.AppendLine("       Select B.ParValue As AcNo, ");
                    SQL.AppendLine("       0.00 As DAmt, ");
                    SQL.AppendLine("       IfNull(( ");
                    SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                    SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                    SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                    SQL.AppendLine("       ), 0.00) * ");
                    SQL.AppendLine("       IfNull(A.Amt,0.00) As CAmt, ");
                    SQL.AppendLine("       From TblARDownpayment A ");
                    SQL.AppendLine("       Inner Join TblParameter B On B.ParCode='AcNoForShortTermUnearnedLeases' And B.ParValue Is Not Null ");
                    SQL.AppendLine("       Where A.DocNo=@DocNo ");
                }
            }
            else
            {
                // Downpayment after tax
                if (Sm.CompareStr(mMainCurCode, CurCode))
                {
                    SQL.AppendLine("        Select Concat(B.ParValue, C.ProjectCode2) As AcNo, ");
                    SQL.AppendLine("        IfNull(A.Amt,0.00)+IfNull(A.TaxAmt,0.00)+IfNull(A.TaxAmt2,0.00)+IfNull(A.TaxAmt3,0.00) As DAmt, ");
                    SQL.AppendLine("        0.00 As CAmt ");
                    SQL.AppendLine("        From TblARDownpayment A ");
                    SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='CustomerAcNoAR' And B.ParValue Is Not Null ");
                    SQL.AppendLine("        Inner Join ( ");
                    SQL.AppendLine("            Select Distinct T1.DocNo, T2.ProjectCode2 ");
                    SQL.AppendLine("            From TblARDownpayment T1 ");
                    SQL.AppendLine("            Inner Join TblSOContractHdr T2 On T1.SODocNo=T2.DocNo And T2.ProjectCode2 Is Not Null ");
                    SQL.AppendLine("            Where T1.DocNo=@DocNo ");
                    SQL.AppendLine("       ) C On A.DocNo=C.DocNo ");
                    SQL.AppendLine("       Where A.DocNo=@DocNo ");
                }
                else
                {
                    SQL.AppendLine("       Select Concat(B.ParValue, C.ProjectCode2) As AcNo, ");
                    SQL.AppendLine("       IfNull(( ");
                    SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                    SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                    SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                    SQL.AppendLine("       ), 0.00) * ");
                    SQL.AppendLine("       (IfNull(A.Amt,0.00)+IfNull(A.TaxAmt,0.00)+IfNull(A.TaxAmt2,0.00)+IfNull(A.TaxAmt3,0.00)) As DAmt, ");
                    SQL.AppendLine("       0.00 As CAmt ");
                    SQL.AppendLine("       From TblARDownpayment A ");
                    SQL.AppendLine("       Inner Join TblParameter B On B.ParCode='CustomerAcNoAR' And B.ParValue Is Not Null ");
                    SQL.AppendLine("       Inner Join ( ");
                    SQL.AppendLine("            Select Distinct T1.DocNo, T5.ProjectCode2 ");
                    SQL.AppendLine("            From TblARDownpayment T1 ");
                    SQL.AppendLine("            Inner Join TblSOContractHdr T2 On T1.SODocNo=T2.DocNo And T2.ProjectCode2 Is Not Null ");
                    SQL.AppendLine("            Where T1.DocNo=@DocNo ");
                    SQL.AppendLine("       ) C On A.DocNo=C.DocNo ");
                    SQL.AppendLine("       Where A.DocNo=@DocNo ");
                }

                //Downpayment
                if (Sm.CompareStr(mMainCurCode, CurCode))
                {
                    SQL.AppendLine("        Union All ");
                    SQL.AppendLine("        Select Concat(B.ParValue, C.ProjectCode2) As AcNo, ");
                    SQL.AppendLine("        0.00 As DAmt, ");
                    SQL.AppendLine("        IfNull(A.Amt,0.00) As CAmt ");
                    SQL.AppendLine("        From TblARDownpayment A ");
                    SQL.AppendLine("        Inner Join TblParameter B On B.ParCode='CustomerAcNoDownPayment' And B.ParValue Is Not Null ");
                    SQL.AppendLine("        Inner Join ( ");
                    SQL.AppendLine("            Select Distinct T1.DocNo, T2.ProjectCode2 ");
                    SQL.AppendLine("            From TblARDownpayment T1 ");
                    SQL.AppendLine("            Inner Join TblSOContractHdr T2 On T1.SODocNo=T2.DocNo And T2.ProjectCode2 Is Not Null ");
                    SQL.AppendLine("            Where T1.DocNo=@DocNo ");
                    SQL.AppendLine("       ) C On A.DocNo=C.DocNo ");
                    SQL.AppendLine("       Where A.DocNo=@DocNo ");
                }
                else
                {
                    SQL.AppendLine("       Union All ");
                    SQL.AppendLine("       Select Concat(B.ParValue, C.ProjectCode2) As AcNo, ");
                    SQL.AppendLine("       0.00 As DAmt, ");
                    SQL.AppendLine("       IfNull(( ");
                    SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                    SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                    SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                    SQL.AppendLine("       ), 0.00) * ");
                    SQL.AppendLine("        IfNull(A.Amt,0.00) As CAmt ");
                    SQL.AppendLine("       From TblARDownpayment A ");
                    SQL.AppendLine("       Inner Join TblParameter B On B.ParCode='CustomerAcNoDownPayment' And B.ParValue Is Not Null ");
                    SQL.AppendLine("       Inner Join ( ");
                    SQL.AppendLine("            Select Distinct T1.DocNo, T5.ProjectCode2 ");
                    SQL.AppendLine("            From TblARDownpayment T1 ");
                    SQL.AppendLine("            Inner Join TblSOContractHdr T2 On T1.SODocNo=T2.DocNo And T2.ProjectCode2 Is Not Null ");
                    SQL.AppendLine("            Where T1.DocNo=@DocNo ");
                    SQL.AppendLine("       ) C On A.DocNo=C.DocNo ");
                    SQL.AppendLine("       Where A.DocNo=@DocNo ");
                }

                //PPN Keluaran
                if (Sm.CompareStr(mMainCurCode, CurCode))
                {
                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("    Select B.AcNo2 As AcNo, ");
                    SQL.AppendLine("    0.00 As DAmt, ");
                    SQL.AppendLine("    A.TaxAmt As CAmt ");
                    SQL.AppendLine("    From TblARDownpayment A ");
                    SQL.AppendLine("    Inner Join TblTax B On A.TaxCode=B.TaxCode And B.AcNo2 Is Not Null ");
                    SQL.AppendLine("    Where A.TaxCode Is Not Null And A.DocNo=@DocNo ");
                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("    Select B.AcNo2 As AcNo, ");
                    SQL.AppendLine("    0.00 As DAmt, ");
                    SQL.AppendLine("    A.TaxAmt2 As CAmt ");
                    SQL.AppendLine("    From TblARDownpayment A ");
                    SQL.AppendLine("    Inner Join TblTax B On A.TaxCode2 = B.TaxCode And B.AcNo2 Is Not Null ");
                    SQL.AppendLine("    Where A.TaxCode2 Is Not Null And A.DocNo=@DocNo ");
                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("    Select B.AcNo2 As AcNo, ");
                    SQL.AppendLine("    0.00 As DAmt, ");
                    SQL.AppendLine("    A.TaxAmt3 As CAmt ");
                    SQL.AppendLine("    From TblARDownpayment A ");
                    SQL.AppendLine("    Inner Join TblTax B On A.TaxCode3=B.TaxCode And B.AcNo2 Is Not Null ");
                    SQL.AppendLine("    Where A.TaxCode3 Is Not Null And A.DocNo=@DocNo ");
                }
                else
                {
                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("    Select B.AcNo2 As AcNo, ");
                    SQL.AppendLine("    0.00 As DAmt, ");
                    SQL.AppendLine("       IfNull(( ");
                    SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                    SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                    SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                    SQL.AppendLine("       ), 0.00) * ");
                    SQL.AppendLine("    A.TaxAmt As CAmt ");
                    SQL.AppendLine("    From TblARDownpayment A ");
                    SQL.AppendLine("    Inner Join TblTax B On A.TaxCode=B.TaxCode And B.AcNo2 Is Not Null ");
                    SQL.AppendLine("    Where A.TaxCode Is Not Null And A.DocNo=@DocNo ");
                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("    Select B.AcNo2 As AcNo, ");
                    SQL.AppendLine("    0.00 As DAmt, ");
                    SQL.AppendLine("       IfNull(( ");
                    SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                    SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                    SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                    SQL.AppendLine("       ), 0.00) * ");
                    SQL.AppendLine("    A.TaxAmt2 As CAmt ");
                    SQL.AppendLine("    From TblARDownpayment A ");
                    SQL.AppendLine("    Inner Join TblTax B On A.TaxCode2 = B.TaxCode And B.AcNo2 Is Not Null ");
                    SQL.AppendLine("    Where A.TaxCode2 Is Not Null And A.DocNo=@DocNo ");
                    SQL.AppendLine("    Union All ");
                    SQL.AppendLine("    Select B.AcNo2 As AcNo, ");
                    SQL.AppendLine("    0.00 As DAmt, ");
                    SQL.AppendLine("       IfNull(( ");
                    SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                    SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=A.CurCode And CurCode2=@MainCurCode ");
                    SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                    SQL.AppendLine("       ), 0.00) * ");
                    SQL.AppendLine("    A.TaxAmt3 As CAmt ");
                    SQL.AppendLine("    From TblARDownpayment A ");
                    SQL.AppendLine("    Inner Join TblTax B On A.TaxCode3=B.TaxCode And B.AcNo2 Is Not Null ");
                    SQL.AppendLine("    Where A.TaxCode3 Is Not Null And A.DocNo=@DocNo ");
                }
            }
            SQL.AppendLine("    )Tbl ");
            SQL.AppendLine("    Where AcNo Is Not Null ");
            SQL.AppendLine("    Group By AcNo  ");
            SQL.AppendLine(")T; ");

            if (!Sm.CompareStr(mMainCurCode, Sm.GetLue(LueCurCode)))
            {
                SQL.AppendLine("Update TblJournalDtl A ");
                SQL.AppendLine("Inner Join ( ");
                SQL.AppendLine("    Select DAmt, CAmt From (");
                SQL.AppendLine("        Select Sum(DAmt) as DAmt, Sum(CAmt) as CAmt ");
                SQL.AppendLine("        From TblJournalDtl Where DocNo=@JournalDocNo ");
                SQL.AppendLine("    ) Tbl ");
                SQL.AppendLine(") B On 1=1 ");
                SQL.AppendLine("Set ");
                SQL.AppendLine("    A.DAmt=Case When B.DAmt<B.CAmt Then Abs(B.CAmt-B.DAmt) Else 0.00 End, ");
                SQL.AppendLine("    A.CAmt=Case When B.DAmt>B.CAmt Then Abs(B.DAmt-B.CAmt) Else 0.00 End ");
                SQL.AppendLine("Where A.DocNo=@JournalDocNo ");
                SQL.AppendLine("And A.AcNo In ( ");
                SQL.AppendLine("    Select ParValue From TblParameter ");
                SQL.AppendLine("    Where ParCode='AcNoForForeignExchange' ");
                SQL.AppendLine("    And ParValue Is Not Null ");
                SQL.AppendLine("    );");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(Sm.GetDte(DteDocDt), 1));
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@MainCurCode", mMainCurCode);
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (IsEditedDataNotValid() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditARDownpayment());

            if (mIsAutoJournalActived && mIsARDownpayment3SaveJournal)
                cml.Add(SaveJournal());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDataCancelledAlready() ||
                IsDataProcessedAlready() ||
                Sm.IsMeeEmpty(MeeCancelReason, "Reason for cancellation") ||
                IsCancelIndNotTrue();
        }

        private bool IsDataCancelledAlready()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select DocNo From TblARDownpayment " +
                    "Where (CancelInd='Y' Or Status='C') And DocNo=@DocNo "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data is already cancelled.");
                return true;
            }
            return false;
        }

        private bool IsCancelIndNotTrue()
        {
            if (ChkCancelInd.Checked == false)
            {
                Sm.StdMsg(mMsgType.Warning, "You must cancel this document.");
                return true;
            }
            return false;
        }

        private bool IsDataProcessedAlready()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo ");
            SQL.AppendLine("From TblVoucherRequestHdr A, TblVoucherHdr B ");
            SQL.AppendLine("Where A.VoucherDocNo=B.DocNo And B.CancelInd='N' ");
            SQL.AppendLine("And A.DocNo=@DocNo ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtVoucherRequestDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "Data already processed into voucher.");
                return true;
            }

            return false;
        }

        private MySqlCommand EditARDownpayment()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblARDownpayment Set ");
            SQL.AppendLine("    CancelInd='Y', CancelReason=@CancelReason, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N' And Status<>'C'; ");

            SQL.AppendLine("Update TblVoucherRequestHdr Set ");
            SQL.AppendLine("    CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where (DocNo=@VoucherRequestDocNo Or DocNo=@VoucherRequestDocNo2)  And CancelInd='N' And Status<>'C'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo", TxtVoucherRequestDocNo.Text);
            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo2", TxtVoucherRequestDocNo2.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal()
        {
            var SQL = new StringBuilder();

            var CurrentDt = Sm.ServerCurrentDate();
            var DocDt = Sm.GetDte(DteDocDt);
            var IsClosingJournalUseCurrentDt = Sm.IsClosingJournalUseCurrentDt(DocDt);

            SQL.AppendLine("Set @JournalDocNo:=");
            if (IsClosingJournalUseCurrentDt)
                SQL.AppendLine(Sm.GetNewJournalDocNo(CurrentDt, 1));
            else
                SQL.AppendLine(Sm.GetNewJournalDocNo(DocDt, 1));
            SQL.AppendLine(";");

            SQL.AppendLine("Update TblARDownpayment Set JournalDocNo2=@JournalDocNo Where DocNo=@DocNo And CancelInd='Y';");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, ");
            if (IsClosingJournalUseCurrentDt)
                SQL.AppendLine("Replace(CurDate(), '-', ''), ");
            else
                SQL.AppendLine("DocDt, ");
            SQL.AppendLine("Concat('Cancelling ', JnDesc) As JnDesc, ");
            SQL.AppendLine("MenuCode, MenuDesc, Remark, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblJournalHdr Where DocNo In (Select JournalDocNo From TblARDownpayment Where DocNo=@DocNo And CancelInd='Y');");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, DNo, AcNo, CAmt As DAmt, DAmt As CAmt, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblJournalDtl Where DocNo In (Select JournalDocNo From TblARDownpayment Where DocNo=@DocNo And CancelInd='Y');");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        #endregion

        #endregion

        #region Additional Method

        internal string GetSelectedAcNo()
        {
            var SQL = string.Empty;
            if (Grd3.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd3, Row, 1).Length != 0)
                        SQL += "##" + Sm.GetGrdStr(Grd3, Row, 1) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        private void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'VoucherCodeFormatType', 'GenerateCustomerCOAFormat', 'IsFilterByCtCt', 'IsARDownpaymentSONotMandatory', 'IsARDPProcessTo1Journal', ");
            SQL.AppendLine("'IsARDPUseCOA', 'BankAccountFormat', 'MainCurCode', 'ProjectAcNoFormula', 'IsVoucherBankAccountFilteredByGrp', ");
            SQL.AppendLine("'IsAutoJournalActived', 'IsARDownpayment3SaveJournal', 'IsARDownpaymentUseBankAccount', 'IsShowCustomerCategory', 'IsCOAARDownpaymentBasedOnCustomerCode' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsCOAARDownpaymentBasedOnCustomerCode": mIsCOAARDownpaymentBasedOnCustomerCode = ParValue == "Y"; break;
                            case "IsShowCustomerCategory": mIsShowCustomerCategory = ParValue == "Y"; break;
                            case "IsARDownpaymentUseBankAccount": mIsARDownpaymentUseBankAccount = ParValue == "Y"; break;
                            case "IsARDownpayment3SaveJournal": mIsARDownpayment3SaveJournal = ParValue == "Y"; break;
                            case "IsAutoJournalActived": mIsAutoJournalActived = ParValue == "Y"; break;
                            case "IsVoucherBankAccountFilteredByGrp": mIsVoucherBankAccountFilteredByGrp = ParValue == "Y"; break;
                            case "IsARDPUseCOA": mIsARDPUseCOA = ParValue == "Y"; break;
                            case "IsFilterByCtCt": mIsFilterByCtCt = ParValue == "Y"; break;
                            case "IsARDPProcessTo1Journal": mIsARDPProcessTo1Journal = ParValue == "Y"; break;
                            case "IsARDownpaymentSONotMandatory": mIsARDownpaymentSONotMandatory = ParValue == "Y"; break;

                            //string
                            case "ProjectAcNoFormula": mProjectAcNoFormula = ParValue; break;
                            case "MainCurCode": mMainCurCode = ParValue; break;
                            case "BankAccountFormat": mBankAccountFormat = ParValue; break;
                            case "VoucherCodeFormatType": mVoucherCodeFormatType = ParValue; break;
                            case "GenerateCustomerCOAFormat": mGenerateCustomerCOAFormat = ParValue; break;
                            
                        }
                    }
                }
                dr.Close();
            }
        }

        #region Convert To Words

        private static string[] _ones =
            {
                "Zero",
                "One",
                "Two",
                "Three",
                "Four",
                "Five",
                "Six",
                "Seven",
                "Eight",
                "Nine"
            };

        private static string[] _teens =
            {
                "Ten",
                "Eleven",
                "Twelve",
                "Thirteen",
                "Fourteen",
                "Fifteen",
                "Sixteen",
                "Seventeen",
                "Eighteen",
                "Nineteen"
            };

        private static string[] _tens =
            {
                "",
                "Ten",
                "Twenty",
                "Thirty",
                "Forty",
                "Fifty",
                "Sixty",
                "Seventy",
                "Eighty",
                "Ninety"
            };

        // US Nnumbering:
        private static string[] _thousands =
            {
                "",
                "Thousand",
                "Million",
                "Billion",
                "Trillion",
                "Quadrillion"
            };


        private static string Convert(decimal value)
        {
            string digits, temp;
            bool showThousands = false;
            bool allZeros = true;

            StringBuilder builder = new StringBuilder();
            digits = ((long)value).ToString();
            for (int i = digits.Length - 1; i >= 0; i--)
            {
                int ndigit = (int)(digits[i] - '0');
                int column = (digits.Length - (i + 1));

                switch (column % 3)
                {
                    case 0:        // Ones position
                        showThousands = true;
                        if (i == 0)
                        {
                            temp = String.Format("{0} ", _ones[ndigit]);
                        }
                        else if (digits[i - 1] == '1')
                        {
                            temp = String.Format("{0} ", _teens[ndigit]);
                            i--;
                        }
                        else if (ndigit != 0)
                        {
                            temp = String.Format("{0} ", _ones[ndigit]);
                        }
                        else
                        {
                            temp = String.Empty;
                            if (digits[i - 1] != '0' || (i > 1 && digits[i - 2] != '0'))
                                showThousands = true;
                            else
                                showThousands = false;
                        }

                        if (showThousands)
                        {
                            if (column > 0)
                            {
                                temp = String.Format("{0}{1}{2}",
                                    temp,
                                    _thousands[column / 3],
                                    allZeros ? " " : " ");
                            }
                            allZeros = false;
                        }
                        builder.Insert(0, temp);
                        break;

                    case 1:        // Tens column
                        if (ndigit > 0)
                        {
                            temp = String.Format("{0}{1}",
                                _tens[ndigit],
                                (digits[i + 1] != '0') ? " " : " ");
                            builder.Insert(0, temp);
                        }
                        break;

                    case 2:        // Hundreds column
                        if (ndigit > 0)
                        {
                            temp = String.Format("{0} Hundred ", _ones[ndigit]);
                            builder.Insert(0, temp);
                        }
                        break;
                }
            }

            string cents = value.ToString();
            decimal cettt = Decimal.Parse(cents.Substring(cents.Length - 2, 2));
            string cent = Sm.Terbilang2(cettt);
            builder.AppendFormat("Dollars And " + cent + " Cents # ", (value - (long)value) * 100);

            return String.Format("{0}{1} ",
                Char.ToUpper(builder[0]),
                builder.ToString(1, builder.Length - 1));
        }
        #endregion

        private void ParPrint()
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;
            var ld = new List<ARDep>();

            string[] TableName = { "ARDep" };
            List<IList> myLists = new List<IList>();

            var cm = new MySqlCommand();

            var SQL = new StringBuilder();
            SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyAddressCity', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyPhone', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle5') As 'CompanyFax', ");
            SQL.AppendLine("A.DocNo, DATE_FORMAT(A.DocDt,'%M %d, %Y') As DocDt,  B.CurCode, A.Amt, ");
            SQL.AppendLine("Case IfNull(A.Status, '') ");
            SQL.AppendLine("    When 'O' Then 'Outstanding' ");
            SQL.AppendLine("    When 'A' Then 'Approved' ");
            SQL.AppendLine("    When 'C' Then 'Cancel' End ");
            SQL.AppendLine("As StatusDesc, F.Username As CreateBy, Date_Format(left(A.CreateDt, 8),'%d %M %Y') As CreateDt, H.PosName As Position, ");
            SQL.AppendLine("C.CtName, A.SODocNo, E.DocNo As DocNoVC, DATE_FORMAT(E.DocDt,'%M %d, %Y') As DocDtVC, C.Address, A.Remark, ");
            if (mGenerateCustomerCOAFormat == "1")
                SQL.AppendLine("B.Amt As SoAmt ");
            else
                SQL.AppendLine("(B.Amt + B.AmtBOM) As SoAmt ");
            SQL.AppendLine("From TblARDownpayment A ");
            if (mGenerateCustomerCOAFormat == "1")
                SQL.AppendLine("Left Join TblSOHdr B On A.SODocNo=B.DocNo ");
            else
                SQL.AppendLine("Left Join TblSOContractHdr B On A.SODocNo=B.DocNo ");

            SQL.AppendLine("Inner Join TblCustomer C On A.CtCode=C.CtCode ");
            SQL.AppendLine("Left Join TblVoucherRequestHdr D On A.VoucherRequestDocNo=D.DocNo And D.CancelInd= 'N'");
            SQL.AppendLine("Left Join TblVoucherhdr E On D.DocNo=E.VoucherRequestDocNo And E.CancelInd= 'N'  ");
            SQL.AppendLine("Inner Join TblUser F On A.CreateBy = F.UserCode");
            SQL.AppendLine("Left Join TblEmployee G On A.CreateBy = G.UserCode");
            SQL.AppendLine("Left Join TblPosition H On G.PosCode = H.PosCode");
            SQL.AppendLine("Where A.DocNo=@DocNo;");


            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                    {
                     //0
                         "CompanyLogo",

                         //1-5
                         "CompanyName",
                         "CompanyAddress",
                         "CompanyAddressCity",
                         "CompanyPhone",
                         "CompanyFax",
                         //6-10
                         "DocNo",
                         "DocDt",
                         "CurCode",
                         "Amt",
                         "SoAmt",
                         //11-15
                         "CtName",
                         "SODocNo",
                         "DocNoVC",
                         "DocDtVC",
                         "StatusDesc",
                         //16
                         "Remark",
                         "CreateBy",
                         "CreateDt",
                         "Position"
                        
                    });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ld.Add(new ARDep()
                        {
                            CompanyLogo = Sm.DrStr(dr, c[0]),

                            CompanyName = Sm.DrStr(dr, c[1]),
                            CompanyAddress = Sm.DrStr(dr, c[2]),
                            CompanyAddressCity = Sm.DrStr(dr, c[3]),
                            CompanyPhone = Sm.DrStr(dr, c[4]),
                            CompanyFax = Sm.DrStr(dr, c[5]),

                            DocNo = TxtDocNo.Text,
                            DocDt = DteDocDt.Text,
                            CurCode = Sm.DrStr(dr, c[8]),
                            Amt = Sm.DrDec(dr, c[9]),
                            SoAmt = Sm.DrDec(dr, c[10]),

                            CtName = Sm.DrStr(dr, c[11]),
                            SODocNo = Sm.DrStr(dr, c[12]),
                            DocNoVC = Sm.DrStr(dr, c[13]),
                            DocDtVC = Sm.DrStr(dr, c[14]),
                            StatusDesc = Sm.DrStr(dr, c[15]),

                            Remark = Sm.DrStr(dr, c[16]),
                            CreateBy = Sm.DrStr(dr, c[17]),
                            CreateDt = Sm.DrStr(dr, c[18]),
                            Position = Sm.DrStr(dr, c[19]),
                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
                        });
                    }
                }
                dr.Close();
            }
            myLists.Add(ld);

            Sm.PrintReport("ARDownpayment2", myLists, TableName, false);
        }

        internal void SetLueCtCode(ref DXE.LookUpEdit Lue, string CtCode)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select CtCode As Col1, CtName As Col2 ");
                SQL.AppendLine("From TblCustomer ");
                if (CtCode.Length == 0)
                    SQL.AppendLine("Where ActInd='Y' ");
                else
                    SQL.AppendLine("Where CtCode=@CtCode ");
                SQL.AppendLine("Order By CtName; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };

                if (CtCode.Length != 0)
                    Sm.CmParam<String>(ref cm, "@CtCode", CtCode);

                Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
                if (CtCode.Length != 0) Sm.SetLue(LueCtCode, CtCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        internal void SetLueBankAcCode(ref LookUpEdit Lue, string BankAcCode)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.BankAcCode As Col1, ");
            if (mBankAccountFormat == "1")
            {
                SQL.AppendLine("Trim(Concat( ");
                SQL.AppendLine("Case When B.BankName Is Not Null Then Concat(B.BankName, ' ') Else '' End,  ");
                SQL.AppendLine("Case When A.BankAcNo Is Not Null  ");
                SQL.AppendLine("    Then Concat(A.BankAcNo) ");
                SQL.AppendLine("    Else IfNull(A.BankAcNm, '') End, ");
                SQL.AppendLine("Case When A.Remark Is Not Null Then Concat(' ', '(', A.Remark, ')') Else '' End ");
                SQL.AppendLine(")) As Col2 ");
            }
            else if (mBankAccountFormat == "2")
            {
                SQL.AppendLine("Trim(Concat( ");
                SQL.AppendLine("    Case When A.BankAcNo Is Not Null  ");
                SQL.AppendLine("    Then Concat(IfNull(A.BankAcNm, ''), ' : ', A.BankAcNo) ");
                SQL.AppendLine("    Else IfNull(A.BankAcNm, '') End, ");
                SQL.AppendLine("    Case When B.BankName Is Not Null Then Concat(' [', B.BankName, ']') Else '' End ");
                SQL.AppendLine("    )) As Col2 ");
            }
            else
            {
                SQL.AppendLine("Trim(Concat( ");
                SQL.AppendLine("Case When A.BankAcNo Is Not Null  ");
                SQL.AppendLine("    Then Concat(A.BankAcNo, ' [', IfNull(A.BankAcNm, ''), ']') ");
                SQL.AppendLine("    Else IfNull(A.BankAcNm, '') End, ");
                SQL.AppendLine("Case When B.BankName Is Not Null Then Concat(' ', B.BankName) Else '' End ");
                SQL.AppendLine(")) As Col2 ");
            }

            SQL.AppendLine("From TblBankAccount A ");
            SQL.AppendLine("Left Join TblBank B On A.BankCode=B.BankCode ");

            if (BankAcCode.Length != 0)
                SQL.AppendLine("Where A.BankAcCode=@BankAcCode ");
            else
            {
                if (mIsVoucherBankAccountFilteredByGrp)
                {
                    SQL.AppendLine("Where Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupBankAccount ");
                    SQL.AppendLine("    Where BankAcCode=A.BankAcCode ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode=@UserCode ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine(") ");
                }
            }
            SQL.AppendLine("Order By A.Sequence;");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@BankAcCode", BankAcCode);

            Sm.SetLue2(
                ref Lue, ref cm,
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (BankAcCode.Length != 0) Sm.SetLue(Lue, BankAcCode);
        }

        internal void ComputeCOAAmt()
        {
            decimal COAAmt = 0m;
            try
            {
                var SQL = new StringBuilder();

                if (mIsARDPProcessTo1Journal)
                {
                    for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                    {
                        if (Sm.GetGrdStr(Grd3, Row, 1).Length > 0 && Sm.GetGrdBool(Grd3, Row, 8))
                        {

                            if (Sm.GetGrdBool(Grd3, Row, 6) && Sm.GetGrdDec(Grd3, Row, 3) != 0)
                            {
                                //kalau dicentang di debit, (-)
                                COAAmt -= Sm.GetGrdDec(Grd3, Row, 3);
                            }
                            if (Sm.GetGrdBool(Grd3, Row, 7) && Sm.GetGrdDec(Grd3, Row, 4) != 0)
                            {
                                //kalau dicentang di credit, (+)
                                COAAmt += Sm.GetGrdDec(Grd3, Row, 4);
                            }
                        }
                    }
                }
                else
                {

                    SQL.AppendLine("Select Concat(C.ParValue, A.CtCode) As AcNo ");
                    SQL.AppendLine("From TblCustomer A ");
                    SQL.AppendLine("Inner Join TblCustomerCategory B On A.CtCtCode=B.CtCtCode ");
                    SQL.AppendLine("Left Join TblParameter C On C.ParCode='CustomerAcNoAR' ");
                    SQL.AppendLine("Where A.CtCtCode is Not Null ");
                    SQL.AppendLine("And A.CtCode=@CtCode Limit 1; ");

                    var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                    Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
                    var AcNo = Sm.GetValue(cm);

                    cm.CommandText = "Select AcType From TblCOA Where AcNo=@AcNo;";
                    Sm.CmParam<String>(ref cm, "@AcNo", AcNo);
                    string AcType = Sm.GetValue(cm);

                    for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
                    {
                        if (AcNo.Length > 0 && Sm.CompareStr(AcNo, Sm.GetGrdStr(Grd3, Row, 1)))
                        {
                            if (Sm.GetGrdDec(Grd3, Row, 3) != 0)
                            {
                                if (AcType == "D")
                                    COAAmt += Sm.GetGrdDec(Grd3, Row, 3);
                                else
                                    COAAmt -= Sm.GetGrdDec(Grd3, Row, 3);
                            }
                            if (Sm.GetGrdDec(Grd3, Row, 4) != 0)
                            {
                                if (AcType == "C")
                                    COAAmt += Sm.GetGrdDec(Grd3, Row, 4);
                                else
                                    COAAmt -= Sm.GetGrdDec(Grd3, Row, 4);
                            }
                        }
                    }
                }
                TxtCOAAmt.EditValue = Sm.FormatNum(COAAmt, 0);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        internal void ComputeOtherAmt()
        {
            var OtherAmt = Sm.GetValue(
                "Select Amt From TblARDownpayment " +
                "Where SODocNo=@Param And CancelInd='N' And Status In ('O', 'A');",
                TxtSODocNo.Text);
            if (OtherAmt.Length == 0) OtherAmt = "0";
            TxtOtherAmt.EditValue = Sm.FormatNum(decimal.Parse(OtherAmt), 0);
        }
        internal void GetCustomerCategory()
        {
            var CtCtCode = Sm.GetValue(
                "Select A.CtCtName From TblCustomerCategory A " +
                "Inner join TblCustomer B on A.CtCtCode=B.CtCtCode " +
                "Where B.CtCode='" + Sm.GetLue(LueCtCode) + "' ;");
            if (LueCtCode.Text.Length != 0)
                TxtCtCtName.Text = CtCtCode;
        }

        private void ComputeAmtAftTax()
        {
            decimal
                Amt = 0m,
                TaxAmt = 0m,
                TaxAmt2 = 0m,
                TaxAmt3 = 0m;

            string
                TaxCode = Sm.GetLue(LueTaxCode),
                TaxCode2 = Sm.GetLue(LueTaxCode2),
                TaxCode3 = Sm.GetLue(LueTaxCode3);

            if (TxtAmt.Text.Length > 0.00) Amt = decimal.Parse(TxtAmt.Text);
            
            if (TaxCode.Length != 0)
            {
                var TaxRate = Sm.GetValueDec("Select TaxRate From TblTax Where TaxCode=@Param;", TaxCode);
                if (TaxRate != 0) TaxAmt = TaxRate * 0.01m * Amt;
            }
            if (TaxCode2.Length != 0)
            {
                var TaxRate2 = Sm.GetValueDec("Select TaxRate From TblTax Where TaxCode=@Param;", TaxCode2);
                if (TaxRate2 != 0) TaxAmt2 = TaxRate2 * 0.01m * Amt;
            }
            if (TaxCode3.Length != 0)
            {
                var TaxRate3 = Sm.GetValueDec("Select TaxRate From TblTax Where TaxCode=@Param;", TaxCode3);
                if (TaxRate3 != 0) TaxAmt3 = TaxRate3 * 0.01m * Amt;
            }

            TxtTaxAmt.EditValue = Sm.FormatNum(TaxAmt, 0);
            TxtTaxAmt2.EditValue = Sm.FormatNum(TaxAmt2, 0);
            TxtTaxAmt3.EditValue = Sm.FormatNum(TaxAmt3, 0);
            TxtAmtAftTax.EditValue = Sm.FormatNum(Amt + TaxAmt + TaxAmt2 + TaxAmt3, 0);
        }

        #endregion    
 
        #endregion

        #region Event

        #region Button Event

        private void BtnCtCt_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmARDownpayment3Dlg3(this));
        }

        private void BtnSODocNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmARDownpayment3Dlg(this));
        }

        private void BtnSODocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtSODocNo, "SO#", false))
            {
                try
                {
                    bool mIsSOContract = TxtSODocNo.Text.Contains("/SOC/");

                    if (mIsSOContract)
                    {
                        if (Sm.GetParameter("DocTitle") == "IMS")
                        {
                            var f = new FrmSOContract2(mMenuCode);
                            f.Tag = "***";
                            f.WindowState = FormWindowState.Normal;
                            f.StartPosition = FormStartPosition.CenterScreen;
                            f.mDocNo = TxtSODocNo.Text;
                            f.ShowDialog();
                        }
                        else
                        {
                            var f = new FrmSOContract(mMenuCode);
                            f.Tag = "***";
                            f.WindowState = FormWindowState.Normal;
                            f.StartPosition = FormStartPosition.CenterScreen;
                            f.mDocNo = TxtSODocNo.Text;
                            f.ShowDialog();
                        }
                    }
                    else
                    {
                        var f = new FrmSO2(mMenuCode);
                        f.Tag = "***";
                        f.WindowState = FormWindowState.Normal;
                        f.StartPosition = FormStartPosition.CenterScreen;
                        f.mDocNo = TxtSODocNo.Text;
                        f.ShowDialog();
                    }
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
            }
        }

        private void BtnVoucherRequestDocNo_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtVoucherRequestDocNo, "Voucher request#", false))
            {
                try
                {
                    var f = new FrmVoucherRequest(mMenuCode);
                    f.Tag = "***";
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = TxtVoucherRequestDocNo.Text;
                    f.ShowDialog();
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
            }
        }

        private void BtnVoucherDocNo_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtVoucherDocNo, "Voucher#", false))
            {
                try
                {
                    var f = new FrmVoucher(mMenuCode);
                    f.Tag = "***";
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = TxtVoucherDocNo.Text;
                    f.ShowDialog();
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
            }
        }
        #endregion

        #region Grid Event

        private void Grd3_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0 && TxtDocNo.Text.Length == 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmARDownPayment3Dlg2(this));
            }
        }

        private void Grd3_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && TxtDocNo.Text.Length == 0) Sm.FormShowDialog(new FrmARDownPayment3Dlg2(this));
        }

        private void Grd3_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd3, e, BtnSave);
            ComputeCOAAmt();
            Sm.GrdEnter(Grd3, e);
            Sm.GrdTabInLastCell(Grd3, e, BtnFind, BtnSave);
        }

        private void Grd3_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd3, e.RowIndex, 1).Length != 0)
                Grd3.Cells[e.RowIndex, 4].Value = 0;

            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd3, e.RowIndex, 1).Length != 0)
                Grd3.Cells[e.RowIndex, 3].Value = 0;

            if (e.ColIndex == 6 && Sm.GetGrdBool(Grd3, e.RowIndex, 6))
                Grd3.Cells[e.RowIndex, 7].Value = false;

            if (e.ColIndex == 7 && Sm.GetGrdBool(Grd3, e.RowIndex, 7))
                Grd3.Cells[e.RowIndex, 6].Value = false;

            if (e.ColIndex == 6 || e.ColIndex == 7)
                Grd3.Cells[e.RowIndex, 8].Value = Sm.GetGrdBool(Grd3, e.RowIndex, e.ColIndex);

            if (Sm.IsGrdColSelected(new int[] { 3, 4, 6, 7 }, e.ColIndex))
                ComputeCOAAmt();
        }

        #endregion

        #region Misc Control Event

        private void LuePIC_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePIC, new Sm.RefreshLue1(Sl.SetLueUserCode));
        }

        private void LueCurCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCurCode, new Sm.RefreshLue1(Sl.SetLueCurCode));
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue2(SetLueCtCode), string.Empty);
            GetCustomerCategory();
        }

        private void TxtAmt_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.FormatNumTxt(TxtAmt, 0);
                ComputeAmtAftTax();
            }
        }

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        private void LuePaymentType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePaymentType, new Sm.RefreshLue1(Sl.SetLueVoucherPaymentType));

            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> { LueBankCode, TxtGiroNo, DteDueDt });

            if (Sm.CompareStr(Sm.GetLue(LuePaymentType), "B"))
            {
                Sm.SetControlReadOnly(LueBankCode, false);
                Sm.SetControlReadOnly(TxtGiroNo, true);
                Sm.SetControlReadOnly(DteDueDt, true);
                return;
            }

            if (Sm.CompareStr(Sm.GetLue(LuePaymentType), "G") || Sm.CompareStr(Sm.GetLue(LuePaymentType), "K"))
            {
                Sm.SetControlReadOnly(LueBankCode, false);
                Sm.SetControlReadOnly(TxtGiroNo, false);
                Sm.SetControlReadOnly(DteDueDt, false);
                return;
            }

            Sm.SetControlReadOnly(LueBankCode, true);
            Sm.SetControlReadOnly(TxtGiroNo, true);
            Sm.SetControlReadOnly(DteDueDt, true);
        }

        private void LueBankCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueBankCode, new Sm.RefreshLue1(Sl.SetLueBankCode));
        }

        private void LueTaxCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueTaxCode, new Sm.RefreshLue1(Sl.SetLueTaxCode));
                ComputeAmtAftTax();
            }
        }

        private void LueTaxCode2_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueTaxCode2, new Sm.RefreshLue1(Sl.SetLueTaxCode));
                ComputeAmtAftTax();
            }
        }

        private void LueTaxCode3_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueTaxCode3, new Sm.RefreshLue1(Sl.SetLueTaxCode));
                ComputeAmtAftTax();
            }
        }

        private void TxtTaxInvoiceNo_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtTaxInvoiceNo);
        }

        private void TxtTaxInvoiceNo2_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtTaxInvoiceNo2);
        }

        private void TxtTaxInvoiceNo3_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.TxtTrim(TxtTaxInvoiceNo3);
        }

        private void LueBankAcCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueBankAcCode, new Sm.RefreshLue2(SetLueBankAcCode), string.Empty);
        }

        #endregion

        #endregion

        #region Class

        private class ARDep
        {
            public string CompanyLogo { set; get; }
            public string CompanyName { get; set; }
            public string CompanyAddressCity { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyPhone { get; set; }

            public string CompanyFax { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string CurCode { get; set; }
            public decimal Amt { get; set; }

            public decimal SoAmt { get; set; }
            public string CtName { get; set; }
            public string SODocNo { get; set; }
            public string DocNoVC { get; set; }
            public string DocDtVC { get; set; }

            public string StatusDesc { get; set; }
            public string Remark { get; set; }
            public string PrintBy { get; set; }
            public string CreateBy { get; set; }
            public string CreateDt { get; set; }

            public string Position { get; set; }
        }

        #endregion
    }
}
