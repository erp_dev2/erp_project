﻿#region Update
/*
    05/09/2017 [ARI] tambah filter Periode Tanggal dan Site
    12/04/2018 [ari] tambah kolom HM, remark
    08/05/2018 [WED] tambah persentase Physical Available, Total Break Down, berdasarkan parameter IsPhysicalAvailableShowPA
            rumus Physical Available = ((MOHH - Total Breakdown)/MOHH) * 100
            MOHH = selisih filter date (end date - start date)
            Total Breakdown = sum(kolom Tm3 di TblWODtl, di group berdasarkan WODocNo) lalu dibuat berkoma. misal sum nya 06:30 maka total breakdown nya 6.5
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptPhysicalAvailable : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = "", mAccessInd = "", mSQL = "";
        private bool mIsPhysicalAvailableShowPA = false;
        private decimal mMOHH = 0m;

        #endregion

        #region Constructor

        public FrmRptPhysicalAvailable(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -7);
                Sl.SetLueSiteCode2(ref LueSiteCode, string.Empty);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.AssetCode, D.AssetName, D.DisplayName, C.DocNo As WODocNo, F.HoursMeter, C.Remark, ");
            SQL.AppendLine("Right(Concat('00', (G.STm1 + Truncate((G.STm2/60), 0))), 2) As STm1, Right(Concat('00', (G.STm2 % 60)), 2) As STm2 ");
            SQL.AppendLine("From TblTOhdr A ");
            SQL.AppendLine("Inner Join TblWOR B On A.AssetCode=B.TOCode And (B.DocDt Between @DocDt1 And @DocDt2) ");
            //SQL.AppendLine("Inner Join TblTOHdr B2 On B.ToCode = B2.AssetCode ");
            SQL.AppendLine("Inner Join TblWOHdr C On B.DocNo=C.WORDocNo ");
            SQL.AppendLine("Inner Join TblAsset D On A.AssetCode=D.AssetCode ");
            SQL.AppendLine("Left Join TblLocation E On A.LocCode = E.LocCode ");
            SQL.AppendLine("left Join TblHoursMeter F On A.HmDocNo = F.DocNo And A.AssetCode = F.TOCode ");
            SQL.AppendLine("Inner Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select T1.DocNo, Sum(Left(T2.Tm3, 2)) As STm1, Sum(Right(T2.Tm3, 2)) As STm2 ");
            SQL.AppendLine("    From TblWOHdr T1 ");
            SQL.AppendLine("    Inner Join TblWODtl T2 On T1.DocNo = T2.DocNo ");
            SQL.AppendLine("    Group By T1.DocNo ");
            SQL.AppendLine(") G On C.DocNo = G.DocNo ");
            SQL.AppendLine("where B.WOstatus='O' ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 10;
            Grd1.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Asset Code",
                    "Asset Name",
                    "Display Name",
                    "WO Document",
                    "Hours Meter",

                    //6-9
                    "Total"+Environment.NewLine+"Breakdown Hours",
                    "Remark",
                    "TotalBreakdownHours",
                    "PA (%)"
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    150, 250, 300, 210, 80,

                    //6-9
                    140, 200, 0, 100

                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 5, 8, 9 }, 0);
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 });
            Sm.GrdColInvisible(Grd1, new int[] { 8 });
            if (!mIsPhysicalAvailableShowPA)
                Sm.GrdColInvisible(Grd1, new int[] { 6, 9 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string
                    mStartDt = Sm.Left(Sm.GetDte(DteDocDt1), 8),
                    mEndDt = Sm.Left(Sm.GetDte(DteDocDt2), 8);

                DateTime mDt1 = new DateTime(
                    Int32.Parse(mStartDt.Substring(0, 4)),
                    Int32.Parse(mStartDt.Substring(4, 2)),
                    Int32.Parse(mStartDt.Substring(6, 2)),
                    0, 0, 0);

                DateTime mDt2 = new DateTime(
                    Int32.Parse(mEndDt.Substring(0, 4)),
                    Int32.Parse(mEndDt.Substring(4, 2)),
                    Int32.Parse(mEndDt.Substring(6, 2)),
                    0, 0, 0);

                mMOHH = Convert.ToDecimal((mDt2 - mDt1).TotalDays);

                string Filter = " And 0 = 0 ";
                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtAssetCode.Text, new string[] { "A.AssetCode", "D.AssetName", "D.DisplayName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "E.SiteCode", true);

                Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                mSQL + Filter + " Order By A.AssetCode; ",
                new string[]
                    {
                        //0
                        "AssetCode",

                        //1-5
                        "AssetName", "DisplayName", "WODocNo", "HoursMeter", "STm1", 
                        
                        //6-7
                        "STm2", "Remark"

                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                        Grd1.Cells[Row, 6].Value = dr.GetString(c[5]) + ":" + dr.GetString(c[6]);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 7);
                        Grd1.Cells[Row, 8].Value = Decimal.Parse(dr.GetString(c[5])) + Decimal.Parse(dr.GetString(c[6])) / 60;
                        Grd1.Cells[Row, 9].Value = 0m;
                        if(mMOHH != 0)
                            Grd1.Cells[Row, 9].Value = Sm.GetGrdDec(Grd1, Row, 9) + (((mMOHH - Sm.GetGrdDec(Grd1, Row, 8)) / mMOHH) * 100m);
                    }, true, false, false, false
                );              
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Additional Methods

        private void GetParameter()
        {
            mIsPhysicalAvailableShowPA = Sm.GetParameterBoo("IsPhysicalAvailableShowPA");
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        private void TxtAssetCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkAssetCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Asset");
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue2(Sl.SetLueSiteCode2), string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }
        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt1).Length == 0) DteDocDt1.EditValue = DteDocDt2.EditValue;
        }

        #endregion

    }
}
