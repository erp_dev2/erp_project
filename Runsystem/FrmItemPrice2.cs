﻿#region Update
/*
    13/02/2018 [TKG] New Application
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmItemPrice2 : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string 
            mMenuCode = string.Empty, mAccessInd = string.Empty, 
            mDocNo = string.Empty,
            mMainCurCode = string.Empty,
            mAssetPriceUomCode = string.Empty;
        internal FrmItemPrice2Find FrmFind;

        #endregion

        #region Constructor

        public FrmItemPrice2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Item/Asset Price";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                Sl.SetLueCurCode(ref LueCurCode);
                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 8;
            Grd1.FrozenArea.ColCount = 4;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",

                        //1-5
                        "",
                        "Item/Asset Code",
                        "Item/Asset Name",
                        "Asset",
                        "Price",

                        //6-7
                        "UoM",
                        "Remark"
                    },
                     new int[] 
                    {
                        0,
                        20, 130, 250, 80, 130,
                        100, 250
                    }
                );
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 3, 4, 6 });
            Sm.GrdColCheck(Grd1, new int[] { 4 });
            Sm.GrdColButton(Grd1, new int[] { 1 });
            Sm.GrdFormatDec(Grd1, new int[] { 5 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 4 }, false);
            
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 4 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { DteDocDt, LueCurCode, MeeRemark }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 5, 7 });
                    BtnSourcePrice.Enabled = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { DteDocDt, LueCurCode, MeeRemark }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1, 5, 7 });
                    BtnSourcePrice.Enabled = true;
                    DteDocDt.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            { TxtDocNo, DteDocDt, LueCurCode, TxtDocNoSource, MeeRemark });
            ChkActInd.Checked = false;
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ClearGrd()
        {
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 5 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmItemPrice2Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sm.SetLue(LueCurCode, mMainCurCode);
                ChkActInd.Checked = true;
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No ||
                    IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "ItemPrice2", "TblItemPrice2Hdr");
                int DNo = 0;
                var cml = new List<MySqlCommand>();

                cml.Add(SaveItemPrice2Hdr(DocNo));
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    if (Sm.GetGrdStr(Grd1, r, 2).Length > 0)
                    {
                        DNo += 1;
                        cml.Add(SaveItemPrice2Dtl(DocNo, r, Sm.Right(string.Concat("00000",DNo), 6)));
                    }
                }
                
                Sm.ExecCommands(cml);
                
                ShowData(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 5 }, e);
            Sm.GrdAfterCommitEditTrimString(Grd1, new int[] { 7 }, e);
            //if (e.ColIndex == 5)
            //{
            //    Grd1.Cells[e.RowIndex, 5].Value = Math.Truncate(Sm.GetGrdDec(Grd1, e.RowIndex, 5) * 10000) / 10000;
            //}
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) 
                    Sm.FormShowDialog(new FrmItemPrice2Dlg(this));
            }

            if (!IsItCodeEmpty(e) && Sm.IsGrdColSelected(new int[] { 1, 5, 7 }, e.ColIndex) && BtnSave.Enabled)
                Sm.GrdRequestEdit(Grd1, e.RowIndex);
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdEnter(Grd1, e);
            Sm.GrdTabInLastCell(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1 && TxtDocNo.Text.Length == 0)
                Sm.FormShowDialog(new FrmItemPrice2Dlg(this));
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (e.ColIndex == 5)
            {
                if (Sm.StdMsgYN("Question", "Do you want to copy price based on first record ?") == DialogResult.Yes)
                {
                    if (Sm.GetGrdStr(Grd1, 0, 5).Length != 0)
                    {
                        var UPrice = Sm.GetGrdDec(Grd1, 0, 5);
                        for (int r = 0;r< Grd1.Rows.Count - 1;r++)
                        {
                            if (Sm.GetGrdStr(Grd1, r, 2).Length != 0)
                                Grd1.Cells[r, 5].Value = UPrice;
                        }
                    }
                }
            }
        }

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowItemPrice2Hdr(DocNo);
                ShowItemPrice2Dtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }

        }

        private void ShowItemPrice2Hdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                ref cm,
                "Select DocNo, DocDt, ActInd, CurCode, Remark " +
                "From TblItemPrice2Hdr Where DocNo=@DocNo;",
                new string[] 
                {
                    //0
                    "DocNo",

                    //1-4
                    "DocDt", "ActInd", "CurCode", "Remark",
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                    ChkActInd.Checked = Sm.DrStr(dr, c[2]) == "Y";
                    Sm.SetLue(LueCurCode, Sm.DrStr(dr, c[3]));
                    MeeRemark.EditValue = Sm.DrStr(dr, c[4]);
                }, true
            );
        }

        private void ShowItemPrice2Dtl(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, B.ItCode, Case A.AssetInd When 'Y' Then C.AssetName Else B.ItName End As ItName, ");
            SQL.AppendLine("A.AssetInd, A.UPrice, A.PriceUomCode, A.Remark ");
            SQL.AppendLine("From TblItemPrice2Dtl A ");
            SQL.AppendLine("Left Join TblItem B On A.ItCode=B.ItCode ");
            SQL.AppendLine("Left Join TblAsset C On A.ItCode=C.AssetCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo;");

            Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    SQL.ToString(),
                    new string[] 
                       { 
                           //0
                           "DNo",
                           //1-5
                           "ItCode", "ItName", "AssetInd", "UPrice", "PriceUomCode", 
                           //6
                           "Remark"
                       },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                    }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 5 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueCurCode, "Currency") ||
                IsGrd1Empty() ||
                IsGrd1ValueNotValid();
        }

        private bool IsGrd1Empty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 item/asset.");
                return true;
            }
            return false;
        }

        private bool IsGrd1ValueNotValid()
        {
            if (Grd1.Rows.Count > 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.IsGrdValueEmpty(Grd1, Row, 3, false, "Item/asset name is empty.")) return true;
                    if (Sm.IsGrdValueEmpty(Grd1, Row, 5, true, "Price should be greater than 0.")) return true;
                }
            }
            return false;
        }

        private MySqlCommand SaveItemPrice2Hdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblItemPrice2Hdr Set ActInd='N' ");
            SQL.AppendLine("Where ActInd='Y' ");
            SQL.AppendLine("And CurCode=@CurCode; ");

            SQL.AppendLine("Insert Into TblItemPrice2Hdr ");
            SQL.AppendLine("(DocNo, DocDt, ActInd, CurCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@DocNo, @DocDt, @ActInd, @CurCode, @Remark, @UserCode, CurrentDateTime()); ");

            var cm = new MySqlCommand(){ CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked?"Y":"N");
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveItemPrice2Dtl(string DocNo, int Row, string DNo)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblItemPrice2Dtl(DocNo, DNo, ItCode, AssetInd, UPrice, PriceUomCode, Remark, CreateBy, CreateDt) " +
                    "Values (@DocNo, @DNo, @ItCode, @AssetInd, @UPrice, @PriceUomCode, @Remark, @UserCode, CurrentDateTime());"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", DNo);
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<String>(ref cm, "@AssetInd", Sm.GetGrdBool(Grd1, Row, 4)?"Y":"N");
            Sm.CmParam<Decimal>(ref cm, "@UPrice", Sm.GetGrdDec(Grd1, Row, 5));
            Sm.CmParam<String>(ref cm, "@PriceUomCode", Sm.GetGrdStr(Grd1, Row, 6));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 7));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mMainCurCode = Sm.GetParameter("MainCurCode");
            mAssetPriceUomCode = Sm.GetParameter("AssetPriceUomCode");
        }

        public void ShowItemPrice2(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.ItCode, ");
            SQL.AppendLine("Case B.AssetInd When 'Y' Then D.AssetName Else C.ItName End As ItName, ");
            SQL.AppendLine("B.AssetInd, B.UPrice, B.PriceUomCode, B.Remark ");
            SQL.AppendLine("From TblItemPrice2Hdr A ");
            SQL.AppendLine("Inner Join TblitemPrice2Dtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Left Join TblItem C On B.ItCode = C.ItCode ");
            SQL.AppendLine("Left Join TblAsset D On B.ItCode=D.AssetCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By B.DNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "ItCode", 
                    
                    //1-5
                    "ItName", "AssetInd", "UPrice", "PriceUomCode", "Remark"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 5 });
            Sm.FocusGrd(Grd1, 0, 0);
        }

        //private void SetLuePriceUomCode(ref DXE.LookUpEdit Lue)
        //{
        //    var SQL = new StringBuilder();

        //    SQL.AppendLine("Select Distinct T.Col1, T.Col2 ");
        //    SQL.AppendLine("From ( ");
        //    SQL.AppendLine("    Select A.SalesUomCode As Col1, B.UomName As Col2  ");
        //    SQL.AppendLine("    From Tblitem  A ");
        //    SQL.AppendLine("    Inner Join TblUom B On A.SalesUomCode = B.UomCode ");
        //    SQL.AppendLine("    Where A.SalesItemInd = 'Y'  ");
        //    SQL.AppendLine("    UNION ALL ");
        //    SQL.AppendLine("    Select  A.SalesUomCode2 As Col1, B.UomName As Col2  ");
        //    SQL.AppendLine("    From Tblitem  A ");
        //    SQL.AppendLine("    Inner Join TblUom B On A.SalesUomCode2 = B.UomCode ");
        //    SQL.AppendLine("    Where A.SalesItemInd = 'Y' ");
        //    SQL.AppendLine(")T  ");

        //    Sm.SetLue2(
        //        ref Lue, SQL.ToString(),
        //        0, 35, false, true, "Code", "Name", "Col2", "Col1");
        //}


        //internal string GetSelectedItem()
        //{
        //    var SQL = string.Empty;
        //    if (Grd1.Rows.Count != 1)
        //    {
        //        for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
        //        {
        //            if (Sm.GetGrdStr(Grd1, Row, 2).Length != 0)
        //            {
        //                if (SQL.Length != 0) SQL += ", ";
        //                SQL += "'" + Sm.GetGrdStr(Grd1, Row, 2) + "'";
        //            }
        //        }
        //    }
        //    return (SQL.Length == 0 ? "'XXX'" : SQL);
        //}

        //internal string GetSelectedItem2()
        //{
        //    var SQL = string.Empty;
        //    if (Grd1.Rows.Count != 1)
        //    {
        //        for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
        //        {
        //            if (Sm.GetGrdStr(Grd1, Row, 10).Length != 0)
        //            {
        //                if (SQL.Length != 0) SQL += ", ";
        //                SQL +=
        //                    "'" +
        //                    Sm.GetGrdStr(Grd1, Row, 10) +
        //                    "'";
        //            }
        //        }
        //    }
        //    return (SQL.Length == 0 ? "'XXX'" : SQL);
        //}

        private bool IsItCodeEmpty(iGRequestEditEventArgs e)
        {
            if (Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length == 0)
            {
                e.DoDefault = false;
                return true;
            }
            return false;
        }

        #endregion

        #endregion

        #region Event

        #region Button Event

        private void BtnSourcePrice_Click(object sender, EventArgs e)
        {
            if (!Sm.IsLueEmpty(LueCurCode, "Currency"))
            {
                TxtDocNoSource.Text = string.Empty;
                Sm.ClearGrd(Grd1, true);
                Sm.FormShowDialog(new FrmItemPrice2Dlg2(this, Sm.GetLue(LueCurCode)));
            }
        }

        #endregion

        #region Misc Control Event

        private void LueCurCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                TxtDocNoSource.Text = string.Empty;
                Sm.ClearGrd(Grd1, true);
                Sm.RefreshLookUpEdit(LueCurCode, new Sm.RefreshLue1(Sl.SetLueCurCode));
            }
        }

        #endregion

        #endregion        
    }
}
