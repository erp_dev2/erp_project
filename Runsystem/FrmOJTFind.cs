﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmOJTFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmOJT mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmOJTFind(FrmOJT FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetSQL();
                SetGrd();
                Sl.SetLueDeptCode(ref LueDeptCode);
                Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -7);
                
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt,A.CancelInd, B.EmpCode, B.EmpName, B.EmpCodeOld,D.PosName, ");
            SQL.AppendLine("C.DeptName, B.JoinDt, A.TrainingDesc, A.StartDt,A.EndDt,A.DurationDay, ");
            SQL.AppendLine("C.DeptName As PlaceOnTraining, D.PosName As NewPositionTrain,A.Remark, A.CreateBy,A.CreateDt,A.LastUpBy, A.LastUpDt, ");
            SQL.AppendLine("case when A.Result='P' Then 'Passed' when A.Result='F' Then 'Failed' when A.result='' Then 'Null' end As Result");
            SQL.AppendLine("From tblojt A ");
            SQL.AppendLine("Inner Join tblemployee B On A.EmpCode=B.EmpCode ");
            SQL.AppendLine("Left Join tbldepartment C On A.DeptCode=C.DeptCode ");
            SQL.AppendLine("Left Join tblposition D On A.PosCode=D.PosCode ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 24;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document"+Environment.NewLine+"Number", 
                        "Document"+Environment.NewLine+"Date",
                        "Cancel",
                        "Employee"+Environment.NewLine+"Code", 
                        "Employee"+Environment.NewLine+"Name", 
                        
 
                        //6-10
                        "Employee Old"+Environment.NewLine+"Code", 
                        "Position",
                        "Department",
                        "Join"+Environment.NewLine+"Date",
                        "Training"+Environment.NewLine+"Description",
                        

                        //11-15
                        "Start"+Environment.NewLine+"Date",
                        "End"+Environment.NewLine+"Date",
                        "Duration"+Environment.NewLine+"(Day)",
                        "Place"+Environment.NewLine+"On Training",
                        "New Position"+Environment.NewLine+"Training",
                       
  
                        //16-20
                        "Result",
                        "Remark",
                        "Created"+Environment.NewLine+"By",
                        "Created"+Environment.NewLine+"Date",
                        "Created"+Environment.NewLine+"Time", 
                         

                        //21-23
                        "Last"+Environment.NewLine+"Updated By",
                        "Last"+Environment.NewLine+"Updated Date", 
                        "Last"+Environment.NewLine+"Updated Time"
                    },
                    new int[]
                    {
                    //0
                    50,

                    //1-5
                    100, 100, 50, 100, 150,

                    //6-10
                    100, 100, 120, 80, 130,

                    //11-15
                    80, 80, 80, 150, 150,

                    //16-20
                    80, 100, 80, 90, 90,

                    //21-23
                    90, 90, 90
                    
                    }

                );
            Sm.GrdColCheck(Grd1, new int[] { 3 });
            Sm.GrdFormatDate(Grd1, new int[] { 2, 9, 11, 12, 19, 22 });
            Sm.GrdFormatTime(Grd1, new int[] { 20, 23 });
            Sm.GrdColInvisible(Grd1, new int[] { 4, 6, 17, 18, 19, 20, 21, 22, 23 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4, 6, 17, 18, 19, 20, 21, 22, 23 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdAutoSize(Grd1);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDt");
                Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "B.EmpCode", "B.EmpName", "B.EmpCodeOld" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "C.DeptCode", true);
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL +
                        Filter + "",
                        new string[]
                        {
                            //0
                            "DocNo", 
                            //1-5
                            "DocDt","CancelInd","EmpCode","EmpName","EmpCodeOld",
                            //6-10
                            "PosName","DeptName","JoinDt","TrainingDesc","StartDt",
                            //11-15
                            "EndDt","DurationDay","PlaceOnTraining","NewPositionTrain","Result",
                            //16-20
                            "Remark","CreateBy","CreateDt","LastUpBy","LastUpDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 15);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 16);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 17);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 19, 18);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 20, 18);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 19);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 22, 20);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 23, 20);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #endregion

        #region Event

        private void TxtDocNo_Validated_1(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void DteDocDt1_EditValueChanged_1(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged_1(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void TxtEmpCode_Validated_1(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged_1(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void ChkDocDt_CheckedChanged_1(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Date");
        }
       
        private void ChkEmpCode_CheckedChanged_1(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void ChkDeptCode_CheckedChanged_1(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }
        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }
        #endregion

        
    }
}
