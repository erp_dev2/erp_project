﻿#region Update
/*
    12/11/2018 [MEY] penambahan nama project saat find di menu project implementation
    08/08/2019 [WED] alur nya ambil dari SOContractRevision
    15/10/2019 [WED/IMS] doctype 1 untuk project
 *  06/05/2020 [HAR/YK] bug filter 
 *  09/05/2022 [TYO/PRODUCT] menambah kolom status  STATUS, PROCESS, Project Code, dan SITE, contract amount.
 *  20/05/2022 [TYO/PRODUCT] Bug : waktu show muncul 2 data double
 *  23/05/2022 [TYO/PRODUCT] Bug : Tampilan pada FIND ketika dibuka isian fieldnya berbeda
 */

#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmProjectImplementationFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmProjectImplementation mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmProjectImplementationFind(FrmProjectImplementation FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            this.Text = mFrmParent.Text;
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
            SetLueCtCode(ref LueCtCode);
            SetGrd();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 19;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#", 
                        "Date",
                        "Cancel",
                        "SO Contract#",
                        "Customer",
                        
                        //6-10
                        "Project Name",
                        "Remark",
                        "Created"+Environment.NewLine+"By", 
                        "Created"+Environment.NewLine+"Date", 
                        "Created"+Environment.NewLine+"Time",
                       
                        //11-15
                        "Last"+Environment.NewLine+"Updated By", 
                        "Last"+Environment.NewLine+"Updated Date", 
                        "Last"+Environment.NewLine+"Updated Time",
                        "Project Code",
                        "Site",

                        //16-18
                        "Status",
                        "Process",
                        "Contract Amount"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        150, 80, 60, 130, 180, 
                        
                        //6-10
                        150, 200, 100, 100, 100,
                        
                        //11-15
                         150, 150, 150, 100, 200,

                         //16-18
                         100, 100, 150
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 2, 9, 12 });
            Sm.GrdColCheck(Grd1, new int[] { 3 });
            Sm.GrdFormatTime(Grd1, new int[] { 10, 13 });
            Sm.GrdColInvisible(Grd1, new int[] { 8, 9, 10, 11, 12, 13 }, false);
            Sm.SetGrdProperty(Grd1, false);
            Sm.GrdFormatDec(Grd1, new int[] { 18 }, 2);
            Grd1.Cols[14].Move(2);
            Grd1.Cols[15].Move(5);
            Grd1.Cols[16].Move(6);
            Grd1.Cols[17].Move(7);
            Grd1.Cols[18].Move(11);

        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 8, 9, 10, 11, 12, 13 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " And 1=1 ";

                var cm = new MySqlCommand();
                var SQL = new StringBuilder();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, new string[] { "A.DocNo" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCode), "C.CtCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtSOContractDocNo.Text, "A.SOContractDocNo", false);

                SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelInd, A.SOContractDocNo, D.CtName, E.ProjectName, A.Remark,  ");
                SQL.AppendLine("B.ProjectCode2, G.SiteName, ");
                SQL.AppendLine("Case A.Status  ");
                SQL.AppendLine("	When 'A' Then 'Approved'  ");
                SQL.AppendLine("	When 'C' Then 'Cancelled'  ");
                SQL.AppendLine("	When 'O' Then 'Outstanding'  ");
                SQL.AppendLine("End As StatusDesc, ");
                SQL.AppendLine("F.OptDesc ProcessInd, ( A.PPNAmt + A.ExclPPNAmt) As ContractAmt, ");
                SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt  ");
                SQL.AppendLine("From TblProjectImplementationHdr A  ");
                SQL.AppendLine("Inner Join TblSOContractRevisionHdr A1 On A.SOContractDocNo = A1.DocNo And A.Doctype = '1'  ");
                SQL.AppendLine("Inner Join TblSOContractHdr B On A1.SOCDocNo = B.DocNo  ");
                SQL.AppendLine("Inner Join TblBOQHdr C On B.BOQDocNo = C.DocNo  ");
                SQL.AppendLine("Inner Join TblCustomer D On C.CtCode = D.CtCode  ");
                SQL.AppendLine("Inner Join TblLopHdr E On E.DocNo = C.LOPDocNo  ");
                SQL.AppendLine("Inner Join TblOption F ON F.OptCat ='ProjectImplementationProcessInd' AND F.OptCode = A.ProcessInd ");
                SQL.AppendLine("Left Join TblSite G ON E.SiteCode = G.SiteCode ");
                if (mFrmParent.mIsFilterBySite)
                {
                    SQL.AppendLine("And (E.SiteCode Is Null Or ( ");
                    SQL.AppendLine("    E.SiteCode Is Not Null ");
                    SQL.AppendLine("    And Exists( ");
                    SQL.AppendLine("        Select 1 From TblGroupSite ");
                    SQL.AppendLine("        Where SiteCode=IfNull(E.SiteCode, '') ");
                    SQL.AppendLine("        And GrpCode In ( ");
                    SQL.AppendLine("            Select GrpCode From TblUser ");
                    SQL.AppendLine("            Where UserCode=@UserCode ");
                    SQL.AppendLine("            ) ");
                    SQL.AppendLine("        ) ");
                    SQL.AppendLine(")) ");
                }
                SQL.AppendLine("Where A.DocDt Between @DocDt1 And @DocDt2 ");

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, SQL.ToString() + Filter + " Group By A.DocNo Order By A.DocDt Desc, A.DocNo; ",
                        new string[]
                        {
                            //0
                            "DocNo", 
                                
                            //1-5
                            "DocDt", "CancelInd", "SOContractDocNo", "CtName", "ProjectName", 

                            //6-10
                            "Remark", "CreateBy", "CreateDt", "LastUpBy", "LastUpDt",

                            //11-15
                            "ProjectCode2", "SiteName", "StatusDesc", "ProcessInd", "ContractAmt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 10, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 12, 10);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 13, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 14);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 15);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Methods

        private void SetLueCtCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.CtCode As Col1, B.CtName As Col2 ");
            SQL.AppendLine("From TblBOQHdr A ");
            SQL.AppendLine("Inner Join TblCustomer B On A.CtCode = B.CtCode ");
            SQL.AppendLine("Order By B.CtName; ");

            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");

        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt1).Length == 0) DteDocDt1.EditValue = DteDocDt2.EditValue;
        }

        private void TxtSOContractDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkSOContractDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "SO Contract#");
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(SetLueCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCtCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");
        }

        #endregion

        #endregion

    }
}
