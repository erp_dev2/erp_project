﻿#region Update
/* 
    22/01/2018 [TKG] otorisasi group thd department
    16/08/2019 [TKG] Filter site
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmAttendanceGrp : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mAGCode = string.Empty;
        internal FrmAttendanceGrpFind FrmFind;
        internal bool mIsFilterByDeptHR = false, mIsFilterBySiteHR = false;

        #endregion

        #region Constructor

        public FrmAttendanceGrp(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Attendance Group";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mAGCode.Length != 0)
                {
                    ShowData(mAGCode);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void GetParameter()
        {
            mIsFilterByDeptHR = Sm.GetParameterBoo("IsFilterByDeptHR");
            mIsFilterBySiteHR = Sm.GetParameterBoo("IsFilterBySiteHR");
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 7;
            Grd1.FrozenArea.ColCount = 1;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "",

                        //1-5
                        "Employee's"+Environment.NewLine+"Code",
                        "Employee's Name",
                        "Old Code",
                        "Position",
                        "Department",

                        //6
                        "Site"
                    },
                     new int[] 
                    {
                        //0
                        20,
 
                        //1-5
                        100, 200, 80, 150, 150,

                        //6
                        100
                    }
                );
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 2, 3, 4, 5, 6 });
            Sm.GrdColButton(Grd1, new int[] { 0 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3 }, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtAGCode, TxtAGName, LueDeptCode, MeeRemark }, true);
                    ChkActInd.Properties.ReadOnly = true;
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0 });
                    TxtAGCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtAGCode, TxtAGName, LueDeptCode, MeeRemark }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0 });
                    TxtAGCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtAGName, MeeRemark }, false);
                    ChkActInd.Properties.ReadOnly = false;
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0 });
                    TxtAGName.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtAGCode, TxtAGName, LueDeptCode, MeeRemark
            });
            ChkActInd.Checked = false;
            Sm.ClearGrd(Grd1, true);
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmAttendanceGrpFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                ChkActInd.Checked = true;
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty, mIsFilterByDeptHR ? "Y" : "N");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtAGCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (!TxtAGCode.Properties.ReadOnly)
                    InsertData(sender, e);
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(SaveAttendanceGrpHdr());

            if (Grd1.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0) cml.Add(SaveAttendanceGrpDtl(Row));
            }

            Sm.ExecCommands(cml);

            BtnInsertClick(sender, e);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtAGCode, "Attendance group code", false) ||
                Sm.IsTxtEmpty(TxtAGName, "Attendance group name", false) ||
                Sm.IsLueEmpty(LueDeptCode, "Department") ||
                IsAGCodeExisted() ||
                IsAGNameExisted() ||
                IsGrdValueNotValid() ||
                IsEmployeeNotValid();
        }

        private bool IsAGCodeExisted()
        {
            if (!TxtAGCode.Properties.ReadOnly)
            {
                var cm = new MySqlCommand()
                {
                    CommandText = "Select AGCode From TblAttendanceGrpHdr Where AGCode=@AGCode Limit 1;"
                };
                Sm.CmParam<String>(ref cm, "@AGCode", TxtAGCode.Text);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "Attendance group code ( " + TxtAGCode.Text + " ) already existed.");
                    return true;
                }
            }
            return false;
        }

        private bool IsAGNameExisted()
        {

            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select AGCode From TblAttendanceGrpHdr " +
                    "Where AGName=@AGName " +
                    (TxtAGCode.Properties.ReadOnly ? "And AGCode<>@AGCode " : string.Empty) +
                    "Limit 1;"
            };
            Sm.CmParam<String>(ref cm, "@AGName", TxtAGName.Text);
            if (TxtAGCode.Properties.ReadOnly)
                Sm.CmParam<String>(ref cm, "@AGCode", TxtAGCode.Text);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "Attendance group name ( " + TxtAGName.Text + " ) already existed.");
                return true;
            }
            return false;
        }

        private bool IsEmployeeNotValid()
        {
            if (ChkActInd.Checked)
            {
                var SQL = new StringBuilder();
                SQL.AppendLine("Select C.EmpName From TblAttendanceGrpHdr A, TblAttendanceGrpDtl B, TblEmployee C ");
                SQL.AppendLine("Where A.AGCode=B.AGCode And B.EmpCode=C.EmpCode And A.AGCode<>@AGCode And ActInd='Y' ");
                SQL.AppendLine("And Locate(Concat('##', B.EmpCode, '##'), @SelectedEmpCode)>0 ");
                SQL.AppendLine("Limit 1;");

                var cm = new MySqlCommand(){ CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@AGCode", TxtAGCode.Text);
                Sm.CmParam<String>(ref cm, "@SelectedEmpCode", GetSelectedEmpCode());

                var EmpName = Sm.GetValue(cm);
                if (EmpName.Length>0)
                {
                    Sm.StdMsg(mMsgType.Warning, 
                        "Employee : " + EmpName + Environment.NewLine +
                        "Invalid employee.");
                    return true;
                }
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            if (Grd1.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.IsGrdValueEmpty(Grd1, Row, 1, false, "Employee is empty.")) return true;       
            }
            return false;
        }

        private MySqlCommand SaveAttendanceGrpHdr()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblAttendanceGrpHdr(AGCode, AGName, ActInd, DeptCode, Remark, CreateBy, CreateDt) " +
                    "Values(@AGCode, @AGName, 'Y', @DeptCode, @Remark, @CreateBy, CurrentDateTime()); "
            };
            Sm.CmParam<String>(ref cm, "@AGCode", TxtAGCode.Text);
            Sm.CmParam<String>(ref cm, "@AGName", TxtAGName.Text);
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveAttendanceGrpDtl(int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblAttendanceGrpDtl(AGCode, EmpCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@AGCode, @EmpCode, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@AGCode", TxtAGCode.Text);
            Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || 
                IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(EditAttendanceGrpHdr());

            if (Grd1.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0) cml.Add(SaveAttendanceGrpDtl(Row));
            }

            Sm.ExecCommands(cml);

            ShowData(TxtAGCode.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return 
                Sm.IsTxtEmpty(TxtAGName, "Attendance group name", false) ||
                IsDataNotActived() ||
                IsEmployeeNotValid();
        }

        private bool IsDataNotActived()
        {
            return Sm.IsDataExist(
                "Select 1 From TblAttendanceGrpHdr Where AGCode=@Param And ActInd='N';",
                TxtAGCode.Text,
                "Attendance group code ( " + TxtAGCode.Text + " ) already not actived."
                );
        }

        private MySqlCommand EditAttendanceGrpHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblAttendanceGrpHdr Set ");
            SQL.AppendLine("    ActInd=@ActInd, AGName=@AGName, DeptCode=@DeptCode, Remark=@Remark, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where AGCode=@AGCode; ");

            SQL.AppendLine("Delete From TblAttendanceGrpDtl Where AGCode=@AGCode; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@AGCode", TxtAGCode.Text);
            Sm.CmParam<String>(ref cm, "@AGName", TxtAGName.Text);
            Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked?"Y":"N");
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string AGCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                ShowAttendanceGrpHdr(AGCode);
                ShowAttendanceGrpDtl(AGCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowAttendanceGrpHdr(string AGCode)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@AGCode", AGCode);
            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select AGCode, AGName, ActInd, DeptCode, Remark " +
                    "From TblAttendanceGrpHdr Where AGCode=@AGCode;",
                    new string[] 
                    { "AGCode", "AGName", "ActInd", "DeptCode", "Remark" },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtAGCode.EditValue = Sm.DrStr(dr, c[0]);
                        TxtAGName.EditValue = Sm.DrStr(dr, c[1]);
                        ChkActInd.Checked = Sm.DrStr(dr, c[2])=="Y";
                        Sl.SetLueDeptCode(ref LueDeptCode, Sm.DrStr(dr, c[3]), string.Empty);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[4]);
                    }, true
                );
        }

        private void ShowAttendanceGrpDtl(string AGCode)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@AGCode", AGCode);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.EmpCode, B.EmpName, B.EmpCodeOld, C.PosName, D.DeptName, E.SiteName ");
            SQL.AppendLine("From TblAttendanceGrpDtl A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode=B.EmpCode ");
            SQL.AppendLine("Left Join TblPosition C On B.PosCode=C.PosCode ");
            SQL.AppendLine("Left Join TblDepartment D On B.DeptCode=D.DeptCode ");
            SQL.AppendLine("Left Join TblSite E On B.SiteCode=E.SiteCode ");
            SQL.AppendLine("Where A.AGCode=@AGCode Order By B.ResignDt, D.DeptName, B.EmpName; ");
            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "EmpCode",

                    //1-5
                    "EmpName", "EmpCodeOld", "PosName", "DeptName", "SiteName"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && e.ColIndex == 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ") && !Sm.IsLueEmpty(LueDeptCode, "Department")) Sm.FormShowDialog(new FrmAttendanceGrpDlg(this, Sm.GetLue(LueDeptCode)));
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && BtnSave.Enabled && !Sm.IsLueEmpty(LueDeptCode, "Department")) Sm.FormShowDialog(new FrmAttendanceGrpDlg(this, Sm.GetLue(LueDeptCode)));
        }

        #endregion

        #region Additional Method

        internal string GetSelectedEmpCode()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length != 0)
                        SQL += "##" + Sm.GetGrdStr(Grd1, Row, 1) + "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtAGCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtAGCode);
        }

        private void TxtAGName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtAGCode);
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue3(Sl.SetLueDeptCode), string.Empty, mIsFilterByDeptHR?"Y":"N");
        }

        #endregion

        #endregion
    }
}
