﻿#region Update
/*
    04/12/2020 [WED/IMS] new apps
    07/12/2020 [WED/IMS] logic print masih salah
    24/03/2021 [VIN/IMS] tambah karakter resi
    02/04/2021 [WED/IMS] lueWhs event belom dipasang
 *  24/06/2021 [SET/IMS]  Menambahkan item dismantle di printout Do based DR
 *  02/07/2021 [SET/IMS] Menambahkan item dismantle pada printout Do based DR untuk printout kategori barang
    13/07/2021 [TRI/IMS] bug item double saat printout, dan nomor belum ambil dari TblSOContractDtl.no
    20/08/2021 [VIN/IMS] bug Stock Detail
    08/09/2021 [VIN/IMS] bug printout item dismantle service 
    13/12/2021 [VIN/IMS] bug printout
    16/02/2022 [VIN/IMS] feedback: tambah source printout service 
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

using FastReport;
using FastReport.Data;
using System.IO;
using System.Net;
using System.Threading;

#endregion

namespace RunSystem
{       
    public partial class FrmDOCt9 : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string
            mMenuCode = string.Empty, mAccessInd = string.Empty,
            mDocNo = string.Empty, //if this application is called from other application
            mCtCode = string.Empty, mSectionNo = string.Empty,
            QtyPallet = string.Empty,
            mEmpCode1 = string.Empty,
            mEmpCode2 = string.Empty,
            mEmpCode3 = string.Empty,
            mEmpCode4 = string.Empty,
            mCustomsDocCode = string.Empty,
            mPEBGrpCode = string.Empty,
            mPEBFilePath = string.Empty,
            mPEBPassword = string.Empty,
            mPEBDocType = string.Empty,
            mDocNoFormat = string.Empty;
        internal FrmDOCt9Find FrmFind;
        internal int
            mNumberOfInventoryUomCode = 1,
            mNumberOfSalesUomCode = 1;
        internal decimal pallet = 0m;
        private string 
            mDocType = "22", 
            mMainCurCode = string.Empty,
            mCustomerAcNoNonInvoice = string.Empty,
            mAcNoForCOGS = string.Empty,
            mAcNoForSaleOfFinishedGoods = string.Empty,
            mEntCode = string.Empty,
            mPortForFTPClient = string.Empty,
            mHostAddrForFTPClient = string.Empty,
            mSharedFolderForFTPClient = string.Empty,
            mUsernameForFTPClient = string.Empty,
            mPasswordForFTPClient = string.Empty,
            mFileSizeMaxUploadFTPClient = string.Empty
            ;
        private bool 
            mIsDOCtShowQueueNo = false,
            mIsAutoJournalActived = false,
            mIsRemarkForJournalMandatory = false,
            mIsAcNoForSaleUseItemCategory = false,
            mIsDOCtContainerSealAllowEditable = false,
            mIsBOMShowSpecifications = false,
            mIsBatchNoDOCt2NotShow = false,
            mIsBinDOCt2NotShow = false,
            mIsDOCt2JournalDisabled = false,
            mIsRemarkHdrEditableOvertime = false;
        internal bool mIsKawasanBerikatEnabled = false;
        internal bool
            mIsItGrpCodeShow = false,
            mIsShowLocalDocNo = false,
            mIsCreditLimitValidate = false,
            mIsDOCtCopyLocalDocNoFromDRPL = false,
            mIsDOCDRAllowToUploadFile = false,
            mIsInspectionSheetMandatory = false,
            mIsCustomerItemNameMandatory = false,
            mIsSalesTransactionShowSOContractRemark = false,
            mIsSalesTransactionUseItemNotes = false,
            mIsSO2NotUsePackagingLabel = false,
            mIsDetailShowColumnNumber = false;

        iGCell fCell;
        bool fAccept;

        private byte[] downloadedData;
        #endregion

        #region Constructor

        public FrmDOCt9(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "DO To Customer (Based On DR/PL)";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                if (mIsKawasanBerikatEnabled)
                {
                    Tp2.PageVisible = true;
                    if (!IsAuthorizedToAccessPEB())
                    {
                        BtnPEB.Visible = false;
                    }
                }
                SetGrd();
                if (mIsInspectionSheetMandatory) LblInspectionSheet.ForeColor = Color.Red;

                Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                { 
                    TxtDocNo, TxtDRDocNo, TxtPLDocNo, TxtCnt, TxtSeal, 
                    TxtCtCode, TxtSAName, TxtExpVdCode, TxtQueueNo, TxtResiNo 
                }, true);
                SetFormControl(mState.View);
                if (mIsRemarkForJournalMandatory) LblRemark.ForeColor = Color.Red;
                xtraTabPage3.PageVisible = mIsDOCtShowQueueNo;
                
                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = 
                    BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = 
                    BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grid 1

            Grd1.Cols.Count = 30;
            Grd1.FrozenArea.ColCount = 6;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",
                        
                        //1-5
                        "Cancel",
                        "Old Cancel",
                        "",
                        "Item's Code",
                        "",
                        
                        //6-10
                        "Local Code",
                        "Item's Name",
                        "Property Code",
                        "Property",
                        "Batch#",
                        
                        //11-15
                        "Source",
                        "Lot",
                        "Bin",
                        "Stock",
                        "Quantity",
                        
                        //16-20
                        "UoM",
                        "Stock",
                        "Quantity",
                        "UoM",
                        "Stock",

                        //21-25
                        "Quantity",
                        "UoM",
                        "Remark",
                        "Group",
                        "Notes",

                        //26-29
                        "ServiceItemInd",
                        "SOContractDocNo",
                        "SOContractDNo",
                        "No"
                    },
                     new int[] 
                    {
                        //0
                        20,
 
                        //1-5
                        50, 50, 20, 100, 20, 

                        //6-10
                        100, 300, 0, 0, 200,  
                        
                        //11-15
                        200, 60, 80, 80, 80,  
                        
                        //16-20
                        80, 80, 80, 80, 80,  
                        
                        //21-25
                        80, 80, 400, 100, 250,

                        //26-29
                        0, 0, 0, 60
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1, 2 });
            Sm.GrdFormatDec(Grd1, new int[] { 14, 15, 17, 18, 20, 21 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 3, 5 });
            Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 4, 5, 6, 8, 9, 11, 12, 17, 18, 19, 20, 21, 22, 23, 24, 26, 27, 28 }, false);
            if (mIsBatchNoDOCt2NotShow)
                Grd1.Cols[10].Visible = false;
            if (mIsBinDOCt2NotShow)
                Grd1.Cols[13].Visible = false;
            if (mIsItGrpCodeShow)
            {
                Grd1.Cols[24].Visible = true;
                Grd1.Cols[24].Move(7);
            }

            Grd1.Cols[29].Move(6);

            if (!mIsSalesTransactionUseItemNotes)
                Sm.GrdColInvisible(Grd1, new int[] { 25 });
            
            #endregion

            #region Grid 2

            Grd2.Cols.Count = 24;
            Sm.GrdHdrWithColWidth(
                Grd2,
                new string[] 
                {
                    //0
                    "DNo",
                    
                    //1-5
                    "SO#",
                    "SO DNo",
                    "",
                    "Agent",
                    "Item's Code",
                    
                    //6-10
                    "",
                    "Item's" + Environment.NewLine + "Local Code",
                    "Item's Name",
                    "Quantity" + Environment.NewLine + "(Packaging)",
                    "UoM" + Environment.NewLine + "(Packaging)",
                    
                    //11-15
                    "DR/PL" + Environment.NewLine + "Quantity",
                    "DO" + Environment.NewLine + "Quantity",
                    "Balance",
                    "UoM",
                    "Group",
                    //16-20
                    "Price After Tax",
                    "Project Code",
                    "Project Name",
                    "Customer PO#",
                    "Customer's"+Environment.NewLine+"Item Code",
                        
                    //21-23
                    "Customer's"+Environment.NewLine+"Item Name",
                    "SO COntract's"+Environment.NewLine+"Remark",
                    "SO Contract's"+Environment.NewLine+"No"
                },
                 new int[] 
                {
                    //0
                    50,

                    //1-5
                    130, 0, 20, 200, 100, 
                    
                    //6-10
                    20, 100, 250, 80, 80, 
                    
                    //11-15
                    80, 80, 80, 80, 100,

                    //16-20
                    150, 120, 120, 120, 100,

                    //21-23
                    150, 250, 100
                }
            );
            Sm.GrdFormatDec(Grd2, new int[] { 9, 11, 12, 13, 16 }, 0);
            Sm.GrdColButton(Grd2, new int[] { 3 ,6 });
            Sm.GrdColInvisible(Grd2, new int[] { 0, 2, 3, 4, 5, 6, 7, 15, 16 }, false);
            Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 1, 2, 4, 5, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23 });
            Grd2.Cols[21].Move(9);
            Grd2.Cols[20].Move(9);
            if (!mIsBOMShowSpecifications) Sm.GrdColInvisible(Grd2, new int[] { 17, 18, 19 });
            if (mIsItGrpCodeShow)
            {
                Grd2.Cols[15].Visible = true;
                Grd2.Cols[15].Move(8);
            }
            if (!mIsCustomerItemNameMandatory)
                Sm.GrdColInvisible(Grd2, new int[] { 21, 20 });

            if(!mIsSalesTransactionShowSOContractRemark)
                Sm.GrdColInvisible(Grd2, new int[] { 22 });

            if (mIsSO2NotUsePackagingLabel)
                Sm.GrdColInvisible(Grd2, new int[] { 9, 10 });

            if (!mIsDetailShowColumnNumber) Sm.GrdColInvisible(Grd2, new int[] { 23 });
            Grd2.Cols[23].Move(5);

            #endregion

            #region Grid 3

            Grd3.Cols.Count = 9;
            Sm.GrdHdrWithColWidth(
                    Grd3,
                    new string[] 
                    {
                        //0
                        "Item's Code",
                        
                        //1-5
                        "",
                        "Item's" + Environment.NewLine + "Local Code",
                        "Item's Name",
                        "DR/PL" + Environment.NewLine + "Quantity",
                        "DO" + Environment.NewLine + "Quantity",
                        
                        //6-8
                        "Balance",
                        "UoM",
                        "Group"
                    },
                     new int[] 
                    {
                        //0
                        80,

                        //1-5
                        20, 80, 250, 80, 80, 
                        
                        //6-8
                        80, 80, 100
                    }
                );
            Sm.GrdFormatDec(Grd3, new int[] { 4, 5, 6 }, 0);
            Sm.GrdColButton(Grd3, new int[] { 1 });
            Sm.GrdColInvisible(Grd3, new int[] { 0, 1, 2, 8 }, false);
            Sm.GrdColReadOnly(true, true, Grd3, new int[] { 0, 2, 3, 4, 5, 6, 7, 8 });

            if (mIsItGrpCodeShow)
            {
                Grd3.Cols[8].Visible = true;
                Grd3.Cols[8].Move(3);
            }

            #endregion

            ShowInventoryUomCode();
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4, 5, 6, 10, 11, 12, 13, 23 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd2, new int[] { 3, 4, 5, 6, 7 }, !ChkHideInfoInGrd.Checked);
            Sm.GrdColInvisible(Grd3, new int[] { 0, 1, 2 }, !ChkHideInfoInGrd.Checked);
        }

        private void ShowInventoryUomCode()
        {
            if (mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 17, 18, 19 }, true);
            
            if (mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 17, 18, 19, 20, 21, 22 }, true);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueWhsCode, TxtLocalDocNo, TxtExpDriver, TxtExpPlatNo, TxtSeal, TxtCnt,
                        TxtResiNo, MeeRemark, TxtInspectionSheet, TxtFile, TxtFile2, TxtFile3
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28 });
                    BtnDRDocNo.Enabled = false;
                    BtnPLDocNo.Enabled = false;
                    BtnQueueNo.Enabled = false;
                    BtnEmpCode1.Enabled = false;
                    BtnEmpCode2.Enabled = false;
                    BtnEmpCode3.Enabled = false;
                    BtnEmpCode4.Enabled = false;
                    BtnKBContractNo.Enabled = BtnPEB.Enabled = false;
                    BtnFile.Enabled = false;
                    BtnDownload.Enabled = false;
                    BtnFile2.Enabled = false;
                    BtnDownload2.Enabled = false;
                    BtnFile3.Enabled = false;
                    BtnDownload3.Enabled = false;
                    if (TxtDocNo.Text.Length > 0 && mIsDOCDRAllowToUploadFile)
                    {
                        BtnDownload.Enabled = true;
                        ChkFile.Enabled = false;
                    }
                    if (TxtDocNo.Text.Length > 0 && mIsDOCDRAllowToUploadFile)
                    {
                        BtnDownload2.Enabled = true;
                        ChkFile2.Enabled = false;
                    }
                    if (TxtDocNo.Text.Length > 0 && mIsDOCDRAllowToUploadFile)
                    {
                        BtnDownload3.Enabled = true;
                        ChkFile3.Enabled = false;
                    }
                    
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueWhsCode, TxtLocalDocNo, TxtExpDriver, TxtExpPlatNo, 
                        TxtResiNo, MeeRemark, TxtInspectionSheet
                    }, false);
                    if (mIsDOCtShowQueueNo) Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ TxtExpDriver, TxtExpPlatNo }, true);
                    if (mIsDOCtContainerSealAllowEditable) Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtSeal, TxtCnt }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 3, 15, 18, 21, 23, 25 });
                    BtnDRDocNo.Enabled = true;
                    BtnPLDocNo.Enabled = true;
                    BtnQueueNo.Enabled = mIsDOCtShowQueueNo;
                    BtnEmpCode1.Enabled = true;
                    BtnEmpCode2.Enabled = true;
                    BtnEmpCode3.Enabled = true;
                    BtnEmpCode4.Enabled = true;
                    BtnKBContractNo.Enabled = BtnPEB.Enabled = true;
                    if (mIsDOCDRAllowToUploadFile)
                    {
                        BtnFile.Enabled = true;
                        BtnDownload.Enabled = false;
                        BtnFile2.Enabled = true;
                        BtnDownload2.Enabled = false;
                        BtnFile3.Enabled = true;
                        BtnDownload3.Enabled = false;
                        ChkFile.Enabled = true;
                        ChkFile2.Enabled = true;
                        ChkFile3.Enabled = true;
                    }
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1 });
                    TxtDocNo.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            mCtCode = string.Empty;
            mSectionNo = string.Empty;
            mEntCode = string.Empty;
            mEmpCode1 = string.Empty;
            mEmpCode2 = string.Empty;
            mEmpCode3 = string.Empty;
            mEmpCode4 = string.Empty;
            mCustomsDocCode = string.Empty;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, TxtDRDocNo, TxtPLDocNo, TxtCnt, 
                TxtSeal, LueWhsCode, TxtCtCode, TxtLocalDocNo, TxtSAName, 
                TxtResiNo, TxtExpVdCode, TxtExpDriver, TxtExpPlatNo, TxtQueueNo, 
                TxtEmpCode1, TxtEmpCode2, TxtEmpCode3, TxtEmpCode4, MeeRemark,
                TxtKBContractNo, DteKBContractDt, TxtKBPLNo, DteKBPLDt, TxtKBRegistrationNo, 
                DteKBRegistrationDt, TxtKBSubmissionNo, TxtInspectionSheet, TxtFile, TxtFile2, TxtFile3
            });
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 1);
            ChkFile.Checked = false;
            PbUpload.Value = 0;
            ChkFile2.Checked = false;
            PbUpload2.Value = 0;
            ChkFile3.Checked = false;
            PbUpload3.Value = 0;
        }

        internal void ClearData2()
        {
            mCtCode = string.Empty;
            mSectionNo = string.Empty;
            mEntCode = string.Empty;
            mCustomsDocCode = string.Empty;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDRDocNo, TxtPLDocNo, TxtCnt, TxtSeal, TxtCtCode, TxtResiNo,
                TxtSAName, TxtExpVdCode, TxtExpDriver, TxtExpPlatNo, TxtQueueNo, 
                MeeRemark, TxtKBContractNo, DteKBContractDt, TxtKBPLNo, DteKBPLDt, 
                TxtKBRegistrationNo, DteKBRegistrationDt, TxtKBSubmissionNo, TxtInspectionSheet
            });
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 1);
        }

        internal void ClearGrd()
        {
            ClearGrd1();

            Sm.ClearGrd(Grd2, true);
            Sm.SetGrdNumValueZero(ref Grd2, 0, new int[] { 9, 11, 12, 13 });

            Sm.ClearGrd(Grd3, true);
            Sm.SetGrdNumValueZero(ref Grd3, 0, new int[] { 4, 5, 6 });
        }

        internal void ClearGrd1()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdBoolValueFalse(ref Grd1, 0, new int[] { 1, 2 });
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 14, 15, 17, 18, 20, 21 });
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    if (e.ColIndex == 3 && !Sm.IsLueEmpty(LueWhsCode, "Warehouse"))
                    {
                        e.DoDefault = false;
                        if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmDOCt9Dlg2(this, Sm.GetLue(LueWhsCode)));
                    }

                    if (Sm.IsGrdColSelected(new int[] { 3, 15, 18, 21, 23 }, e.ColIndex))
                    {
                        Sm.GrdRequestEdit(Grd1, e.RowIndex);
                        Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 1, 2 });
                        Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 14, 15, 17, 18, 20, 21 });
                    }
                }
                else
                {
                    if (e.ColIndex == 1 && (Sm.GetGrdBool(Grd1, e.RowIndex, 2) || Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length == 0))
                        e.DoDefault = false;
                }
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0 && BtnSave.Enabled && e.KeyCode == Keys.Delete)
            {
                if (Grd1.SelectedRows.Count > 0)
                {
                    if (Grd1.Rows[Grd1.Rows[Grd1.Rows.Count - 1].Index].Selected)
                        MessageBox.Show("You can't remove last row.", Gv.CompanyName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    else
                    {
                        if (MessageBox.Show("Remove the selected row(s)?", Gv.CompanyName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            for (int Index = Grd1.SelectedRows.Count - 1; Index >= 0; Index--)
                                Grd1.Rows.RemoveAt(Grd1.SelectedRows[Index].Index);
                            if (Grd1.Rows.Count <= 0) Grd1.Rows.Add();
                            ComputeItemDOQty();
                        }
                    }
                }
            }
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && BtnSave.Enabled && !Sm.IsLueEmpty(LueWhsCode, "Warehouse") && TxtDocNo.Text.Length == 0)
                Sm.FormShowDialog(new FrmDOCt9Dlg2(this, Sm.GetLue(LueWhsCode)));

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            try
            {
                Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 15, 18, 21 }, e);
                Sm.GrdAfterCommitEditTrimString(Grd1, new int[] { 23 }, e);

                if (e.ColIndex == 15)
                {
                    Sm.ComputeQtyBasedOnConvertionFormula("12", Grd1, e.RowIndex, 4, 15, 18, 21, 16, 19, 22);
                    Sm.ComputeQtyBasedOnConvertionFormula("13", Grd1, e.RowIndex, 4, 15, 21, 18, 16, 22, 19);
                }

                if (e.ColIndex == 18)
                {
                    Sm.ComputeQtyBasedOnConvertionFormula("21", Grd1, e.RowIndex, 4, 18, 15, 21, 19, 16, 22);
                    Sm.ComputeQtyBasedOnConvertionFormula("23", Grd1, e.RowIndex, 4, 18, 21, 15, 19, 22, 16);
                }

                if (e.ColIndex == 21)
                {
                    Sm.ComputeQtyBasedOnConvertionFormula("31", Grd1, e.RowIndex, 4, 21, 15, 18, 22, 16, 19);
                    Sm.ComputeQtyBasedOnConvertionFormula("32", Grd1, e.RowIndex, 4, 21, 18, 15, 22, 19, 16);
                }

                if (e.ColIndex == 15 && Sm.CompareGrdStr(Grd1, e.RowIndex, 16, Grd1, e.RowIndex, 19))
                    Sm.CopyGrdValue(Grd1, e.RowIndex, 18, Grd1, e.RowIndex, 15);

                if (e.ColIndex == 15 && Sm.CompareGrdStr(Grd1, e.RowIndex, 16, Grd1, e.RowIndex, 22))
                    Sm.CopyGrdValue(Grd1, e.RowIndex, 21, Grd1, e.RowIndex, 15);

                if (e.ColIndex == 18 && Sm.CompareGrdStr(Grd1, e.RowIndex, 19, Grd1, e.RowIndex, 22))
                    Sm.CopyGrdValue(Grd1, e.RowIndex, 21, Grd1, e.RowIndex, 18);


                if (Sm.IsGrdColSelected(new int[] { 1, 15, 18 }, e.ColIndex))
                    ComputeItemDOQty(Sm.GetGrdStr(Grd1, e.RowIndex, 4));
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmDOCt9Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sl.SetLueWhsCode(ref LueWhsCode, string.Empty);
                xtraTabControl1.SelectedTabPage = this.xtraTabPage1;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                mEntCode = Sm.GetValue("Select C.EntCode " +
                            "From TblWarehouse A " +
                            "Inner Join TblCostCenter B on A.CCCode = B.CCCode  " +
                            "INner Join TblProfitCenter C on B.ProfitCenterCode = C.ProfitCenterCode " +
                            "Where A.WhsCode = '" + Sm.GetLue(LueWhsCode) + "' limit 1;");
                
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    CancelData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            try
            {
                if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                    (Sm.StdMsgYN("Print", "") == DialogResult.No))
                    return;

                ParPrint(TxtDocNo.Text, TxtPLDocNo.Text, (int)mNumberOfInventoryUomCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnFile_Click(object sender, EventArgs e)
        {
            try
            {
                ChkFile.Checked = true;
                OD.InitialDirectory = "c:";
                OD.Filter = "PDF files (*.pdf)|*.pdf|rar/zip Files(*.rar;*.zip)|*.RAR;*.ZIP";
                OD.FilterIndex = 2;
                OD.ShowDialog();

                TxtFile.Text = OD.FileName;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnFile2_Click(object sender, EventArgs e)
        {
            try
            {
                ChkFile2.Checked = true;
                OD.InitialDirectory = "c:";
                OD.Filter = "PDF files (*.pdf)|*.pdf|rar/zip Files(*.rar;*.zip)|*.RAR;*.ZIP";
                OD.FilterIndex = 2;
                OD.ShowDialog();

                TxtFile2.Text = OD.FileName;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnFile3_Click(object sender, EventArgs e)
        {

            try
            {
                ChkFile3.Checked = true;
                OD.InitialDirectory = "c:";
                OD.Filter = "PDF files (*.pdf)|*.pdf|rar/zip Files(*.rar;*.zip)|*.RAR;*.ZIP";
                OD.FilterIndex = 2;
                OD.ShowDialog();

                TxtFile3.Text = OD.FileName;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnDownload_Click(object sender, EventArgs e)
        {
            DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, TxtFile.Text, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
            SFD.FileName = TxtFile.Text;
            SFD.DefaultExt = "pdf";
            SFD.AddExtension = true;

            if (!Sm.IsTxtEmpty(TxtFile, "File", false) && downloadedData != null && downloadedData.Length != 0)
            {

                if (SFD.ShowDialog() == DialogResult.OK)
                {
                    Application.DoEvents();

                    //Write the bytes to a file
                    FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                    newFile.Write(downloadedData, 0, downloadedData.Length);
                    newFile.Close();
                    MessageBox.Show("Saved Successfully");
                }
            }
            else
                MessageBox.Show("No File was Downloaded Yet!");
        }

        private void BtnDownload2_Click(object sender, EventArgs e)
        {

            DownloadFile2(mHostAddrForFTPClient, mPortForFTPClient, TxtFile2.Text, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
            SFD.FileName = TxtFile2.Text;
            SFD.DefaultExt = "pdf";
            SFD.AddExtension = true;

            if (!Sm.IsTxtEmpty(TxtFile2, "File", false) && downloadedData != null && downloadedData.Length != 0)
            {

                if (SFD.ShowDialog() == DialogResult.OK)
                {
                    Application.DoEvents();

                    //Write the bytes to a file
                    FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                    newFile.Write(downloadedData, 0, downloadedData.Length);
                    newFile.Close();
                    MessageBox.Show("Saved Successfully");
                }
            }
            else
                MessageBox.Show("No File was Downloaded Yet!");
        }

        private void BtnDownload3_Click(object sender, EventArgs e)
        {

            DownloadFile3(mHostAddrForFTPClient, mPortForFTPClient, TxtFile3.Text, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
            SFD.FileName = TxtFile3.Text;
            SFD.DefaultExt = "pdf";
            SFD.AddExtension = true;

            if (!Sm.IsTxtEmpty(TxtFile2, "File", false) && downloadedData != null && downloadedData.Length != 0)
            {

                if (SFD.ShowDialog() == DialogResult.OK)
                {
                    Application.DoEvents();

                    //Write the bytes to a file
                    FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                    newFile.Write(downloadedData, 0, downloadedData.Length);
                    newFile.Close();
                    MessageBox.Show("Saved Successfully");
                }
            }
            else
                MessageBox.Show("No File was Downloaded Yet!");
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            bool IsDOCtCBD = IsCBD();
            string DocNo = string.Empty;
            //string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "DOCt2", "TblDOCt2Hdr");
            bool IsService = IsItemService();

            if (IsService) DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "DOCt9Service", "TblDOCt2Hdr");
            else DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "DOCt9Inventory", "TblDOCt2Hdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveDOCt2Hdr(DocNo, IsDOCtCBD));

            for (int Row = 0; Row < Grd1.Rows.Count-1; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 4).Length > 0) cml.Add(SaveDOCt2Dtl(DocNo, Row));

            if (TxtDRDocNo.Text.Length > 0)
            {
                for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd2, Row, 0).Length > 0) cml.Add(SaveDOCt2Dtl2(DocNo, Row, IsDOCtCBD));

                cml.Add(UpdateDRProcessInd());
                cml.Add(UpdateSO(DocNo));
            }

            if (TxtPLDocNo.Text.Length > 0)
            {
                for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd2, Row, 0).Length > 0) cml.Add(SaveDOCt2Dtl3(DocNo, Row));

                cml.Add(UpdatePLProcessInd());
                cml.Add(UpdateSO2(DocNo));
            }

            if (!IsService)
            {
                cml.Add(SaveStockMovement(DocNo, "", "N"));

                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 4).Length > 0)
                        cml.Add(SaveStockSummary(1, Row));
            }


            if (mIsAutoJournalActived && !mIsDOCt2JournalDisabled)
            {
                if (IsDOCtCBD)
                    cml.Add(SaveJournal2(DocNo, IsService));
                else
                    cml.Add(SaveJournal(DocNo, IsService));
            }

            Sm.ExecCommands(cml);

            if (mIsDOCDRAllowToUploadFile && TxtFile.Text.Length > 0 && TxtFile.Text != "openFileDialog1")
                UploadFile(DocNo);
            if (mIsDOCDRAllowToUploadFile && TxtFile2.Text.Length > 0 && TxtFile2.Text != "openFileDialog1")
                UploadFile2(DocNo);
            if (mIsDOCDRAllowToUploadFile && TxtFile3.Text.Length > 0 && TxtFile3.Text != "openFileDialog1")
                UploadFile3(DocNo);

            if (Sm.StdMsgYN("Print", "") == DialogResult.No)
                BtnInsertClick(sender, e);
            else
            {
                ShowData(DocNo);
                ParPrint(TxtDocNo.Text, TxtPLDocNo.Text, (int)mNumberOfInventoryUomCode);
            }
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                (mIsRemarkForJournalMandatory && Sm.IsMeeEmpty(MeeRemark, "Remark")) ||
                (mIsInspectionSheetMandatory && Sm.IsTxtEmpty(TxtInspectionSheet, "Inspection Sheet#", false)) ||
                Sm.IsClosingJournalInvalid(Sm.GetDte(DteDocDt)) ||
                IsDocumentEmpty() ||
                IsGrdEmpty() ||
                IsGrdExceedMaxRecords() ||
                IsGrdValueNotValid() ||
                IsServiceInventoryNotValid() ||
                IsBalanceNotValid() ||
                IsDRNotValid() ||
                IsPLNotValid() ||
                (mIsDOCDRAllowToUploadFile && IsUploadFileNotValid()) ||
                IsQueueNoNotValid() 
                //credit limit pindah di dr dan pl
                //CheckCreditLimit()
                ;
        }

        private bool IsServiceInventoryNotValid() // tujuannya untuk penomoran dokumennya itu
        {
            string ServiceItemInd = string.Empty;
            for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
            {
                if (i == 0)
                {
                    ServiceItemInd = Sm.GetGrdStr(Grd1, i, 26);
                }

                if (ServiceItemInd != Sm.GetGrdStr(Grd1, i, 26))
                {
                    Sm.StdMsg(mMsgType.Warning, "Item couldn't be mixed between Inventory and Service.");
                    return true;
                }
            }

            return false;
        }

        private bool IsQueueNoNotValid()
        {
            if (TxtQueueNo.Text.Length==0) return false;

            return IsDataExists(
                "Select DocNo From TblLoadingQueue " +
                "Where ProcessInd='F' And DocNo=@Param;",
                TxtQueueNo.Text,
                "Queue# : " + TxtQueueNo.Text + Environment.NewLine +   
                "This queue# already used in another DO#."
                ); 
        }

        private bool IsDocumentEmpty()
        {
            if (TxtDRDocNo.Text.Length == 0 && TxtPLDocNo.Text.Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 document (Delivery request or packing list).");
                return true;
            }
            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 item.");
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Item data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            string Msg = string.Empty;

            ReComputeStock();
            CheckServiceItemInd();

            for (int Row = 0; Row < Grd1.Rows.Count-1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, Row, 4, false, "Item is empty.")) return true;
                Msg =
                    "Item's Code : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine +
                    "Local Code : " + Sm.GetGrdStr(Grd1, Row, 6) + Environment.NewLine +
                    "Item's Name : " + Sm.GetGrdStr(Grd1, Row, 7) + Environment.NewLine +
                    "Property : " + Sm.GetGrdStr(Grd1, Row, 9)+ Environment.NewLine +
                    "Batch# : " + Sm.GetGrdStr(Grd1, Row, 10) + Environment.NewLine +
                    "Source : " + Sm.GetGrdStr(Grd1, Row, 11) + Environment.NewLine +
                    "Lot : " + Sm.GetGrdStr(Grd1, Row, 12) + Environment.NewLine +
                    "Bin : " + Sm.GetGrdStr(Grd1, Row, 13) + Environment.NewLine + Environment.NewLine;

                if (Sm.GetGrdDec(Grd1, Row, 15) <= 0m)
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Quantity should be greater than 0.");
                    return true;
                }

                if (Sm.GetGrdDec(Grd1, Row, 15) > Sm.GetGrdDec(Grd1, Row, 14))
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "DO quantity should not be bigger than available stock.");
                    return true;
                }

                if (Grd1.Cols[18].Visible)
                {
                    if (Sm.GetGrdDec(Grd1, Row, 18) <= 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Quantity (2) should be greater than 0.");
                        return true;
                    }

                    if (Sm.GetGrdDec(Grd1, Row, 18) > Sm.GetGrdDec(Grd1, Row, 17))
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "DO quantity (2) should not be bigger than available stock (2).");
                        return true;
                    }
                }

                if (Grd1.Cols[21].Visible)
                {
                    if (Sm.GetGrdDec(Grd1, Row, 21) <= 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Quantity (3) should be greater than 0.");
                        return true;
                    }

                    if (Sm.GetGrdDec(Grd1, Row, 21) > Sm.GetGrdDec(Grd1, Row, 20))
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "DO quantity (3) should not be bigger than available stock (3).");
                        return true;
                    }
                }
            }
            return false;
        }

        private bool IsBalanceNotValid()
        {
            for (int Row = 0; Row < Grd3.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd3, Row, 0).Length > 0 && Sm.GetGrdDec(Grd3, Row, 6) < 0m)
                {
                    Sm.StdMsg(mMsgType.Warning,
                        "Item's Code : " + Sm.GetGrdStr(Grd3, Row, 0) + Environment.NewLine +
                        "Item's Local Code : " + Sm.GetGrdStr(Grd3, Row, 2) + Environment.NewLine +
                        "Item's Name : " + Sm.GetGrdStr(Grd3, Row, 3) + Environment.NewLine + Environment.NewLine +
                        "Balance should not be less than 0."
                        );
                    return true;
                }
            }
            return false;
        }

        private bool IsDRNotValid()
        {
            return (TxtDRDocNo.Text.Length==0)?
                false :
                IsDRAlreadyCancelled() ||
                IsDRAlreadyFulfilled();
        }

        private bool IsDRAlreadyCancelled()
        {
            return IsDataExists(
                "Select DocNo From TblDRHdr " +
                "Where CancelInd='Y' And DocNo=@Param;",
                TxtDRDocNo.Text,
                "This request delivery already cancelled."
                ); 
        }

        private bool IsDRAlreadyFulfilled()
        {
            return IsDataExists(
                "Select DocNo From TblDRHdr " +
                "Where IfNull(ProcessInd, 'O') In ('F', 'M') And DocNo=@Param; ",
                TxtDRDocNo.Text,
                "This request delivery already fulfilled."
                ); 
        }

        private bool IsPLNotValid()
        {
            return (TxtPLDocNo.Text.Length == 0) ?
                false :
                IsPLBalanceNotValid() ||
                IsSPStatusNotValid() ||
                IsPLAlreadyFulfilled()
                ;
        }

        private bool IsPLBalanceNotValid()
        {
            for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd2, Row, 1).Length > 0 && Sm.GetGrdDec(Grd2, Row, 13) != 0m)
                {
                    Sm.StdMsg(mMsgType.Warning,
                        "SO# : " + Sm.GetGrdStr(Grd2, Row, 1) + Environment.NewLine +
                        "Item's Code : " + Sm.GetGrdStr(Grd2, Row, 5) + Environment.NewLine +
                        "Item's Local Code : " + Sm.GetGrdStr(Grd2, Row, 7) + Environment.NewLine +
                        "Item's Name : " + Sm.GetGrdStr(Grd2, Row, 8) + Environment.NewLine + Environment.NewLine +
                        "Balance should be 0."
                        );
                    return true;
                }
            }
            return false;
        }

        private bool IsSPStatusNotValid()
        {
            return IsDataExists(
                "Select A.DocNo From TblPLHdr A " +
                "Inner Join TblSIHdr B On A.SIDocNo=B.DocNo " +
                "Inner Join TblSP C On B.SPDocNo=C.DocNo And IfNull(C.Status, '')<>'R' " +
                "Where A.Docno=@DocNo;",
                TxtPLDocNo.Text,
                "This packing list's planning status is not valid."
                ); 
        }

        private bool IsPLAlreadyFulfilled()
        {
            var cm = new MySqlCommand() 
            { 
                CommandText =
                    "Select DocNo From TblPLDtl " +
                    "Where DocNo=@DocNo And SectionNo=@SectionNo And ProcessInd='F';"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtPLDocNo.Text);
            Sm.CmParam<String>(ref cm, "@SectionNo", mSectionNo);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(
                    mMsgType.Warning, 
                    "Packing List : " + TxtPLDocNo.Text + Environment.NewLine +
                    "Container# : " + TxtCnt.Text + Environment.NewLine +
                    "Seal# : " + TxtSeal.Text + Environment.NewLine + Environment.NewLine +
                    "This document already fulfilled."
                    );
                return true;
            }
            return false;
        }

        private MySqlCommand SaveDOCt2Hdr(string DocNo, bool IsDOCtCBD)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblDOCt2Hdr(DocNo, DocDt, DRDocNo, PLDocNo, SectionNo, Cnt, Seal, WhsCode, CtCode, CBDInd, LocalDocNo, ExpDriver, ExpPlatNo, QueueNo, ResiNo, EmpCode1, EmpCode2, EmpCode3, EmpCode4, ");
            SQL.AppendLine("KBContractNo, KBContractDt, KBPLNo, KBPLDt, KBRegistrationNo, KBRegistrationDt, KBSubmissionNo, InspectionSheetNo, CustomsDocCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @DRDocNo, @PLDocNo, @SectionNo, @Cnt, @Seal, @WhsCode, @CtCode, @CBDInd, @LocalDocNo, @ExpDriver, @ExpPlatNo, @QueueNo, @ResiNo, @EmpCode1, @EmpCode2, @EmpCode3, @EmpCode4, ");
            SQL.AppendLine("@KBContractNo, @KBContractDt, @KBPLNo, @KBPLDt, @KBRegistrationNo, @KBRegistrationDt, @KBSubmissionNo, @InspectionSheetNo, @CustomsDocCode, @Remark, @CreateBy, CurrentDateTime()); ");

            if (TxtQueueNo.Text.Length > 0)
            {
                SQL.AppendLine("Update TblLoadingQueue Set ProcessInd='F' ");
                SQL.AppendLine("Where Docno=@QueueNo And ProcessInd='O';");
            }

            if (mIsDOCtContainerSealAllowEditable && TxtPLDocNo.Text.Length >0)
            {
                SQL.AppendLine("Update TblPLHdr  ");
                SQL.AppendLine("SET " + string.Concat("Cnt", mSectionNo) + "=@Cnt, " + string.Concat("Seal", mSectionNo) + "=@Seal");
                SQL.AppendLine("Where DocNo=@PLDocNo ;");
            }

            var cm = new MySqlCommand(){ CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@DRDocNo", TxtDRDocNo.Text);
            Sm.CmParam<String>(ref cm, "@PLDocNo", TxtPLDocNo.Text);
            Sm.CmParam<String>(ref cm, "@SectionNo", mSectionNo);
            Sm.CmParam<String>(ref cm, "@Cnt", TxtCnt.Text);
            Sm.CmParam<String>(ref cm, "@Seal", TxtSeal.Text);
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            Sm.CmParam<String>(ref cm, "@CtCode", mCtCode);
            Sm.CmParam<String>(ref cm, "@CBDInd", IsDOCtCBD ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@LocalDocNo", TxtLocalDocNo.Text);
            Sm.CmParam<String>(ref cm, "@ExpDriver", TxtExpDriver.Text);
            Sm.CmParam<String>(ref cm, "@ExpPlatNo", TxtExpPlatNo.Text);
            Sm.CmParam<String>(ref cm, "@QueueNo", TxtQueueNo.Text);
            Sm.CmParam<String>(ref cm, "@ResiNo", TxtResiNo.Text);
            Sm.CmParam<String>(ref cm, "@EmpCode1", mEmpCode1);
            Sm.CmParam<String>(ref cm, "@EmpCode2", mEmpCode2);
            Sm.CmParam<String>(ref cm, "@EmpCode3", mEmpCode3);
            Sm.CmParam<String>(ref cm, "@EmpCode4", mEmpCode4);
            Sm.CmParam<String>(ref cm, "@KBContractNo", TxtKBContractNo.Text);
            Sm.CmParamDt(ref cm, "@KBContractDt", Sm.GetDte(DteKBContractDt));
            Sm.CmParam<String>(ref cm, "@KBPLNo", TxtKBPLNo.Text);
            Sm.CmParamDt(ref cm, "@KBPLDt", Sm.GetDte(DteKBPLDt));
            Sm.CmParam<String>(ref cm, "@KBRegistrationNo", TxtKBRegistrationNo.Text);
            Sm.CmParamDt(ref cm, "@KBRegistrationDt", Sm.GetDte(DteKBRegistrationDt));
            Sm.CmParam<String>(ref cm, "@KBSubmissionNo", TxtKBSubmissionNo.Text);
            Sm.CmParam<String>(ref cm, "@InspectionSheetNo", TxtInspectionSheet.Text);
            Sm.CmParam<String>(ref cm, "@CustomsDocCode", mCustomsDocCode);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveDOCt2Dtl(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblDOCt2Dtl(DocNo, DNo, CancelInd, ItCode, PropCode, BatchNo, Source, Lot, Bin, Qty, Qty2, Qty3, SOContractDocNo, SOContractDNo, Remark, Notes, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DNo, 'N', @ItCode, IfNull(@PropCode, '-'), IfNull(@BatchNo, '-'), IfNull(@Source, '-'), IfNull(@Lot, '-'), IfNull(@Bin, '-'), @Qty, @Qty2, @Qty3, @SOContractDocNo, @SOContractDNo, @Remark, @Notes, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 4));
            Sm.CmParam<String>(ref cm, "@PropCode", Sm.GetGrdStr(Grd1, Row, 8));
            Sm.CmParam<String>(ref cm, "@BatchNo", Sm.GetGrdStr(Grd1, Row, 10));
            Sm.CmParam<String>(ref cm, "@Source", Sm.GetGrdStr(Grd1, Row, 11));
            Sm.CmParam<String>(ref cm, "@Lot", Sm.GetGrdStr(Grd1, Row, 12));
            Sm.CmParam<String>(ref cm, "@Bin", Sm.GetGrdStr(Grd1, Row, 13));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 15));
            Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd1, Row, 18));
            Sm.CmParam<Decimal>(ref cm, "@Qty3", Sm.GetGrdDec(Grd1, Row, 21));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 23));
            Sm.CmParam<String>(ref cm, "@SOContractDocNo", Sm.GetGrdStr(Grd1, Row, 27));
            Sm.CmParam<String>(ref cm, "@SOContractDNo", Sm.GetGrdStr(Grd1, Row, 28));
            Sm.CmParam<String>(ref cm, "@Notes", Sm.GetGrdStr(Grd1, Row, 25));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveDOCt2Dtl2(string DocNo, int Row, bool IsDOCtCBD)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblDOCt2Dtl2(DocNo, DNo, DRDNo, SOCRevDocNo, Qty, ProcessInd, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DNo, @DRDNo, @SOCRevDocNo, @Qty, @ProcessInd, @CreateBy, CurrentDateTime());"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@DRDNo", Sm.GetGrdStr(Grd2, Row, 0));
            Sm.CmParam<String>(ref cm, "@SOCRevDocNo", Sm.GetValue("Select Max(DocNo) From TblSOContractRevisionHdr Where SOCDocNo In (Select SODocNo From TblDRDtl Where DocNo = @Param1 And DNo = @Param2)", TxtDRDocNo.Text, Sm.GetGrdStr(Grd2, Row, 0), string.Empty));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd2, Row, 12));
            Sm.CmParam<String>(ref cm, "@ProcessInd", IsDOCtCBD?"F":"O");
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveDOCt2Dtl3(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblDOCt2Dtl3(DocNo, DNo, PLDNo, Qty, ProcessInd, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DNo, @PLDNo, @Qty, 'O', @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@PLDNo", Sm.GetGrdStr(Grd2, Row, 0));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd2, Row, 12));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand UpdateDRProcessInd()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Update TblDRHdr Set " +
                    "   ProcessInd='F', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() " +
                    "Where DocNo=@DocNo And CancelInd='N' And ProcessInd='O'; "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDRDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand UpdateSO(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblSODtl Tbl1 ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select T.* From ( ");
            SQL.AppendLine("        Select T1.DocNo, T1.DNo, T1.Qty As SOQty, ");
            SQL.AppendLine("        IfNull(T3.Qty, 0) As DRQty, IfNull(T3.QtyInventory, 0) As DRQtyInventory, IfNull(T4.Qty, 0) As DOQtyInventory, ");
            SQL.AppendLine("        Case When IfNull(T3.QtyInventory, 0)=0 Then 0 ");
            SQL.AppendLine("        Else (IfNull(T4.Qty, 0)/IfNull(T3.QtyInventory, 0))*IfNull(T3.Qty, 0) ");
            SQL.AppendLine("        End As DOQty ");
            SQL.AppendLine("        From TblSODtl T1 ");
            SQL.AppendLine("        Inner Join ( ");
            SQL.AppendLine("            Select Distinct C.SODocNo, C.SODNo ");
            SQL.AppendLine("            From TblDOCt2Hdr A ");
            SQL.AppendLine("            Inner Join TblDOCt2Dtl2 B On A.DocNo=B.DocNo ");
            SQL.AppendLine("            Inner Join TblDRDtl C On A.DRDocNo=C.DocNo And B.DRDNo=C.DNo ");
            SQL.AppendLine("            Where A.DocNo=@DocNo ");
            SQL.AppendLine("        ) T2 On T1.DocNo=T2.SODocNo And T1.DNo=T2.SODNo ");
            SQL.AppendLine("        Left Join ( ");
            SQL.AppendLine("            Select B.SODocNo, B.SODNo, ");
            SQL.AppendLine("            Sum(B.Qty) As Qty, Sum(B.QtyInventory) As QtyInventory ");
            SQL.AppendLine("            From TblDRHdr A ");
            SQL.AppendLine("            Inner Join TblDRDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("            Inner Join ( ");
            SQL.AppendLine("                Select Distinct C3.SODocNo, C3.SODNo ");
            SQL.AppendLine("                From TblDOCt2Hdr C1 ");
            SQL.AppendLine("                Inner Join TblDOCt2Dtl2 C2 On C1.DocNo=C2.DocNo ");
            SQL.AppendLine("                Inner Join TblDRDtl C3 On C1.DRDocNo=C3.DocNo And C2.DRDNo=C3.DNo ");
            SQL.AppendLine("                Where C1.DocNo=@DocNo ");
            SQL.AppendLine("            ) C On B.SODocNo=C.SODocNo And B.SODNo=C.SODNo ");
            SQL.AppendLine("            Where A.CancelInd='N' ");
            SQL.AppendLine("            Group By B.SODocNo, B.SODNo ");
            SQL.AppendLine("        ) T3 On T1.DocNo=T3.SODocNo And T1.DNo=T3.SODNo ");
            SQL.AppendLine("        Left Join ( ");
            SQL.AppendLine("            Select C.SODocNo, C.SODNo, Sum(B.Qty) As Qty ");
            SQL.AppendLine("            From TblDOCt2Hdr A ");
            SQL.AppendLine("            Inner Join TblDOCt2Dtl2 B On A.DocNo=B.DocNo ");
            SQL.AppendLine("            Inner Join TblDRDtl C On A.DRDocNo=C.DocNo And B.DRDNo=C.DNo ");
            SQL.AppendLine("            Inner Join ( ");
            SQL.AppendLine("                Select Distinct D3.SODocNo, D3.SODNo ");
            SQL.AppendLine("                From TblDOCt2Hdr D1 ");
            SQL.AppendLine("                Inner Join TblDOCt2Dtl2 D2 On D1.DocNo=D2.DocNo ");
            SQL.AppendLine("                Inner Join TblDRDtl D3 On D1.DRDocNo=D3.DocNo And D2.DRDNo=D3.DNo ");
            SQL.AppendLine("                Where D1.DocNo=@DocNo ");
            SQL.AppendLine("            ) D On C.SODocNo=D.SODocNo And C.SODNo=D.SODNo ");
            SQL.AppendLine("            Group By C.SODocNo, C.SODNo ");
            SQL.AppendLine("        ) T4 On T1.DocNo=T4.SODocNo And T1.DNo=T4.SODNo ");
            SQL.AppendLine("    ) T ");
            SQL.AppendLine(") Tbl2 On Tbl1.DocNo=Tbl2.DocNo And Tbl1.DNo=Tbl2.DNo ");
            SQL.AppendLine("Set ProcessInd2= ");
            SQL.AppendLine("    Case When Tbl2.DOQty=0 Then 'O' Else ");
            SQL.AppendLine("        Case When Tbl2.DOQty>=Tbl2.SOQty Then 'F' Else 'P' End ");
            SQL.AppendLine("    End; ");

            SQL.AppendLine("Update TblSOHdr T Set ");
            SQL.AppendLine("    T.Status= ");
            SQL.AppendLine("        Case When Not Exists( ");
            SQL.AppendLine("            Select DocNo From TblSODtl ");
            SQL.AppendLine("            Where ProcessInd2<>'O' And DocNo=T.DocNo Limit 1 ");
            SQL.AppendLine("        ) Then 'O' Else ");
            SQL.AppendLine("            Case When Not Exists( ");
            SQL.AppendLine("                Select DocNo From TblSODtl ");
            SQL.AppendLine("                Where ProcessInd2<>'F' And DocNo=T.DocNo Limit 1 ");
            SQL.AppendLine("            ) Then 'F' Else 'P' End ");
            SQL.AppendLine("        End ");
            SQL.AppendLine("Where T.Status<>'M' ");
            SQL.AppendLine("And T.CancelInd='N' ");
            SQL.AppendLine("And T.DocNo In ( ");
            SQL.AppendLine("    Select Distinct C.SODocNo ");
            SQL.AppendLine("    From TblDOCt2Hdr A ");
            SQL.AppendLine("    Inner Join TblDOCt2Dtl2 B On A.DocNo=B.DocNo ");
            SQL.AppendLine("    Inner Join TblDRDtl C On A.DRDocNo=C.DocNo And B.DRDNo=C.DNo ");
            SQL.AppendLine("    Where A.DocNo=@DocNo ");
            SQL.AppendLine("    ); ");

            SQL.AppendLine("Update TblSOContractDtl Tbl1 ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select T.* From ( ");
            SQL.AppendLine("        Select T1.DocNo, T1.DNo, T1.Qty As SOQty, ");
            SQL.AppendLine("        IfNull(T3.Qty, 0) As DRQty, IfNull(T3.QtyInventory, 0) As DRQtyInventory, IfNull(T4.Qty, 0) As DOQtyInventory, ");
            SQL.AppendLine("        Case When IfNull(T3.QtyInventory, 0)=0 Then 0 ");
            SQL.AppendLine("        Else (IfNull(T4.Qty, 0)/IfNull(T3.QtyInventory, 0))*IfNull(T3.Qty, 0) ");
            SQL.AppendLine("        End As DOQty ");
            SQL.AppendLine("        From TblSOContractDtl T1 ");
            SQL.AppendLine("        Inner Join ( ");
            SQL.AppendLine("            Select Distinct C.SODocNo, C.SODNo ");
            SQL.AppendLine("            From TblDOCt2Hdr A ");
            SQL.AppendLine("            Inner Join TblDOCt2Dtl2 B On A.DocNo=B.DocNo ");
            SQL.AppendLine("            Inner Join TblDRDtl C On A.DRDocNo=C.DocNo And B.DRDNo=C.DNo ");
            SQL.AppendLine("            Where A.DocNo=@DocNo ");
            SQL.AppendLine("        ) T2 On T1.DocNo=T2.SODocNo And T1.DNo=T2.SODNo ");
            SQL.AppendLine("        Left Join ( ");
            SQL.AppendLine("            Select B.SODocNo, B.SODNo, ");
            SQL.AppendLine("            Sum(B.Qty) As Qty, Sum(B.QtyInventory) As QtyInventory ");
            SQL.AppendLine("            From TblDRHdr A ");
            SQL.AppendLine("            Inner Join TblDRDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("            Inner Join ( ");
            SQL.AppendLine("                Select Distinct C3.SODocNo, C3.SODNo ");
            SQL.AppendLine("                From TblDOCt2Hdr C1 ");
            SQL.AppendLine("                Inner Join TblDOCt2Dtl2 C2 On C1.DocNo=C2.DocNo ");
            SQL.AppendLine("                Inner Join TblDRDtl C3 On C1.DRDocNo=C3.DocNo And C2.DRDNo=C3.DNo ");
            SQL.AppendLine("                Where C1.DocNo=@DocNo ");
            SQL.AppendLine("            ) C On B.SODocNo=C.SODocNo And B.SODNo=C.SODNo ");
            SQL.AppendLine("            Where A.CancelInd='N' ");
            SQL.AppendLine("            Group By B.SODocNo, B.SODNo ");
            SQL.AppendLine("        ) T3 On T1.DocNo=T3.SODocNo And T1.DNo=T3.SODNo ");
            SQL.AppendLine("        Left Join ( ");
            SQL.AppendLine("            Select C.SODocNo, C.SODNo, Sum(B.Qty) As Qty ");
            SQL.AppendLine("            From TblDOCt2Hdr A ");
            SQL.AppendLine("            Inner Join TblDOCt2Dtl2 B On A.DocNo=B.DocNo ");
            SQL.AppendLine("            Inner Join TblDRDtl C On A.DRDocNo=C.DocNo And B.DRDNo=C.DNo ");
            SQL.AppendLine("            Inner Join ( ");
            SQL.AppendLine("                Select Distinct D3.SODocNo, D3.SODNo ");
            SQL.AppendLine("                From TblDOCt2Hdr D1 ");
            SQL.AppendLine("                Inner Join TblDOCt2Dtl2 D2 On D1.DocNo=D2.DocNo ");
            SQL.AppendLine("                Inner Join TblDRDtl D3 On D1.DRDocNo=D3.DocNo And D2.DRDNo=D3.DNo ");
            SQL.AppendLine("                Where D1.DocNo=@DocNo ");
            SQL.AppendLine("            ) D On C.SODocNo=D.SODocNo And C.SODNo=D.SODNo ");
            SQL.AppendLine("            Group By C.SODocNo, C.SODNo ");
            SQL.AppendLine("        ) T4 On T1.DocNo=T4.SODocNo And T1.DNo=T4.SODNo ");
            SQL.AppendLine("    ) T ");
            SQL.AppendLine(") Tbl2 On Tbl1.DocNo=Tbl2.DocNo And Tbl1.DNo=Tbl2.DNo ");
            SQL.AppendLine("Set ProcessIndForDO= ");
            SQL.AppendLine("    Case When Tbl2.DOQty=0 Then 'O' Else ");
            SQL.AppendLine("        Case When Tbl2.DOQty >= Tbl2.SOQty Then 'F' Else 'P' End ");
            SQL.AppendLine("    End; ");

            SQL.AppendLine("Update TblSOContractHdr T Set ");
            SQL.AppendLine("    T.ProcessIndForDR = ");
            SQL.AppendLine("        Case When Not Exists( ");
            SQL.AppendLine("            Select DocNo From TblSOContractDtl ");
            SQL.AppendLine("            Where ProcessIndForDO <> 'O' And DocNo=T.DocNo Limit 1 ");
            SQL.AppendLine("        ) Then 'O' Else ");
            SQL.AppendLine("            Case When Not Exists( ");
            SQL.AppendLine("                Select DocNo From TblSOContractDtl ");
            SQL.AppendLine("                Where ProcessIndForDO <> 'F' And DocNo=T.DocNo Limit 1 ");
            SQL.AppendLine("            ) Then 'F' Else 'P' End ");
            SQL.AppendLine("        End ");
            SQL.AppendLine("Where T.ProcessIndForDR <> 'M' ");
            SQL.AppendLine("And T.CancelInd='N' ");
            SQL.AppendLine("And T.DocNo In ( ");
            SQL.AppendLine("    Select Distinct C.SODocNo ");
            SQL.AppendLine("    From TblDOCt2Hdr A ");
            SQL.AppendLine("    Inner Join TblDOCt2Dtl2 B On A.DocNo=B.DocNo ");
            SQL.AppendLine("    Inner Join TblDRDtl C On A.DRDocNo=C.DocNo And B.DRDNo=C.DNo ");
            SQL.AppendLine("    Where A.DocNo=@DocNo ");
            SQL.AppendLine("    ); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            return cm;
        }

        private MySqlCommand UpdatePLProcessInd()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Update TblPLDtl Set " +
                    "   ProcessInd='F', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() " +
                    "Where DocNo=@DocNo And SectionNo=@SectionNo And ProcessInd='O'; "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtPLDocNo.Text);
            Sm.CmParam<String>(ref cm, "@SectionNo", mSectionNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand UpdateSO2(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblSODtl Tbl1 ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select T.* From ( ");
            SQL.AppendLine("        Select T1.DocNo, T1.DNo, T1.Qty As SOQty, ");
            SQL.AppendLine("        IfNull(T3.Qty, 0) As PLQty, IfNull(T3.QtyInventory, 0) As PLQtyInventory, IfNull(T4.Qty, 0) As DOQtyInventory, ");
            SQL.AppendLine("        Case When IfNull(T3.QtyInventory, 0)=0 Then 0 ");
            SQL.AppendLine("        Else (IfNull(T4.Qty, 0)/IfNull(T3.QtyInventory, 0))*IfNull(T3.Qty, 0) ");
            SQL.AppendLine("        End As DOQty ");
            SQL.AppendLine("        From TblSODtl T1 ");
            SQL.AppendLine("        Inner Join ( ");
            SQL.AppendLine("            Select Distinct C.SODocNo, C.SODNo ");
            SQL.AppendLine("            From TblDOCt2Hdr A ");
            SQL.AppendLine("            Inner Join TblDOCt2Dtl3 B On A.DocNo=B.DocNo ");
            SQL.AppendLine("            Inner Join TblPLDtl C On A.PLDocNo=C.DocNo And B.PLDNo=C.DNo ");
            SQL.AppendLine("            Where A.DocNo=@DocNo ");
            SQL.AppendLine("        ) T2 On T1.DocNo=T2.SODocNo And T1.DNo=T2.SODNo ");
            SQL.AppendLine("        Left Join ( ");
            SQL.AppendLine("            Select B.SODocNo, B.SODNo, ");
            SQL.AppendLine("            Sum(B.Qty) As Qty, Sum(B.QtyInventory) As QtyInventory ");
            SQL.AppendLine("            From TblPLHdr A ");
            SQL.AppendLine("            Inner Join TblPLDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("            Inner Join ( ");
            SQL.AppendLine("                Select Distinct C3.SODocNo, C3.SODNo ");
            SQL.AppendLine("                From TblDOCt2Hdr C1 ");
            SQL.AppendLine("                Inner Join TblDOCt2Dtl3 C2 On C1.DocNo=C2.DocNo ");
            SQL.AppendLine("                Inner Join TblPLDtl C3 On C1.PLDocNo=C3.DocNo And C2.PLDNo=C3.DNo ");
            SQL.AppendLine("                Where C1.DocNo=@DocNo ");
            SQL.AppendLine("            ) C On B.SODocNo=C.SODocNo And B.SODNo=C.SODNo ");
            SQL.AppendLine("            Group By B.SODocNo, B.SODNo ");
            SQL.AppendLine("        ) T3 On T1.DocNo=T3.SODocNo And T1.DNo=T3.SODNo ");
            SQL.AppendLine("        Left Join ( ");
            SQL.AppendLine("            Select C.SODocNo, C.SODNo, Sum(B.Qty) As Qty ");
            SQL.AppendLine("            From TblDOCt2Hdr A ");
            SQL.AppendLine("            Inner Join TblDOCt2Dtl3 B On A.DocNo=B.DocNo ");
            SQL.AppendLine("            Inner Join TblPLDtl C On A.PLDocNo=C.DocNo And B.PLDNo=C.DNo ");
            SQL.AppendLine("            Inner Join ( ");
            SQL.AppendLine("                Select Distinct D3.SODocNo, D3.SODNo ");
            SQL.AppendLine("                From TblDOCt2Hdr D1 ");
            SQL.AppendLine("                Inner Join TblDOCt2Dtl3 D2 On D1.DocNo=D2.DocNo ");
            SQL.AppendLine("                Inner Join TblPLDtl D3 On D1.PLDocNo=D3.DocNo And D2.PLDNo=D3.DNo ");
            SQL.AppendLine("                Where D1.DocNo=@DocNo ");
            SQL.AppendLine("            ) D On C.SODocNo=D.SODocNo And C.SODNo=D.SODNo ");
            SQL.AppendLine("            Group By C.SODocNo, C.SODNo ");
            SQL.AppendLine("        ) T4 On T1.DocNo=T4.SODocNo And T1.DNo=T4.SODNo ");
            SQL.AppendLine("    ) T ");
            SQL.AppendLine(") Tbl2 On Tbl1.DocNo=Tbl2.DocNo And Tbl1.DNo=Tbl2.DNo ");
            SQL.AppendLine("Set ProcessInd2= ");
            SQL.AppendLine("    Case When Tbl2.DOQty=0 Then 'O' Else ");
            SQL.AppendLine("        Case When Tbl2.DOQty>=Tbl2.SOQty Then 'F' Else 'P' End ");
            SQL.AppendLine("    End; ");

            SQL.AppendLine("Update TblSOHdr T Set ");
            SQL.AppendLine("    T.Status= ");
            SQL.AppendLine("        Case When Not Exists( ");
            SQL.AppendLine("            Select DocNo From TblSODtl ");
            SQL.AppendLine("            Where ProcessInd2<>'O' And DocNo=T.DocNo Limit 1 ");
            SQL.AppendLine("        ) Then 'O' Else ");
            SQL.AppendLine("            Case When Not Exists( ");
            SQL.AppendLine("                Select DocNo From TblSODtl ");
            SQL.AppendLine("                Where ProcessInd2<>'F' And DocNo=T.DocNo Limit 1 ");
            SQL.AppendLine("            ) Then 'F' Else 'P' End ");
            SQL.AppendLine("        End ");
            SQL.AppendLine("Where T.Status<>'M' ");
            SQL.AppendLine("And T.CancelInd='N' ");
            SQL.AppendLine("And T.DocNo In ( ");
            SQL.AppendLine("    Select Distinct C.SODocNo ");
            SQL.AppendLine("    From TblDOCt2Hdr A ");
            SQL.AppendLine("    Inner Join TblDOCt2Dtl3 B On A.DocNo=B.DocNo ");
            SQL.AppendLine("    Inner Join TblPLDtl C On A.PLDocNo=C.DocNo And B.PLDNo=C.DNo ");
            SQL.AppendLine("    Where A.DocNo=@DocNo ");
            SQL.AppendLine("    ); ");
            
            SQL.AppendLine("Update TblSOContractDtl Tbl1 ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select T.* From ( ");
            SQL.AppendLine("        Select T1.DocNo, T1.DNo, T1.Qty As SOQty, ");
            SQL.AppendLine("        IfNull(T3.Qty, 0) As PLQty, IfNull(T3.QtyInventory, 0) As PLQtyInventory, IfNull(T4.Qty, 0) As DOQtyInventory, ");
            SQL.AppendLine("        Case When IfNull(T3.QtyInventory, 0)=0 Then 0 ");
            SQL.AppendLine("        Else (IfNull(T4.Qty, 0)/IfNull(T3.QtyInventory, 0))*IfNull(T3.Qty, 0) ");
            SQL.AppendLine("        End As DOQty ");
            SQL.AppendLine("        From TblSOContractDtl T1 ");
            SQL.AppendLine("        Inner Join ( ");
            SQL.AppendLine("            Select Distinct C.SODocNo, C.SODNo ");
            SQL.AppendLine("            From TblDOCt2Hdr A ");
            SQL.AppendLine("            Inner Join TblDOCt2Dtl3 B On A.DocNo=B.DocNo ");
            SQL.AppendLine("            Inner Join TblPLDtl C On A.PLDocNo=C.DocNo And B.PLDNo=C.DNo ");
            SQL.AppendLine("            Where A.DocNo=@DocNo ");
            SQL.AppendLine("        ) T2 On T1.DocNo=T2.SODocNo And T1.DNo=T2.SODNo ");
            SQL.AppendLine("        Left Join ( ");
            SQL.AppendLine("            Select B.SODocNo, B.SODNo, ");
            SQL.AppendLine("            Sum(B.Qty) As Qty, Sum(B.QtyInventory) As QtyInventory ");
            SQL.AppendLine("            From TblPLHdr A ");
            SQL.AppendLine("            Inner Join TblPLDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("            Inner Join ( ");
            SQL.AppendLine("                Select Distinct C3.SODocNo, C3.SODNo ");
            SQL.AppendLine("                From TblDOCt2Hdr C1 ");
            SQL.AppendLine("                Inner Join TblDOCt2Dtl3 C2 On C1.DocNo=C2.DocNo ");
            SQL.AppendLine("                Inner Join TblPLDtl C3 On C1.PLDocNo=C3.DocNo And C2.PLDNo=C3.DNo ");
            SQL.AppendLine("                Where C1.DocNo=@DocNo ");
            SQL.AppendLine("            ) C On B.SODocNo=C.SODocNo And B.SODNo=C.SODNo ");
            SQL.AppendLine("            Group By B.SODocNo, B.SODNo ");
            SQL.AppendLine("        ) T3 On T1.DocNo=T3.SODocNo And T1.DNo=T3.SODNo ");
            SQL.AppendLine("        Left Join ( ");
            SQL.AppendLine("            Select C.SODocNo, C.SODNo, Sum(B.Qty) As Qty ");
            SQL.AppendLine("            From TblDOCt2Hdr A ");
            SQL.AppendLine("            Inner Join TblDOCt2Dtl3 B On A.DocNo=B.DocNo ");
            SQL.AppendLine("            Inner Join TblPLDtl C On A.PLDocNo=C.DocNo And B.PLDNo=C.DNo ");
            SQL.AppendLine("            Inner Join ( ");
            SQL.AppendLine("                Select Distinct D3.SODocNo, D3.SODNo ");
            SQL.AppendLine("                From TblDOCt2Hdr D1 ");
            SQL.AppendLine("                Inner Join TblDOCt2Dtl3 D2 On D1.DocNo=D2.DocNo ");
            SQL.AppendLine("                Inner Join TblPLDtl D3 On D1.PLDocNo=D3.DocNo And D2.PLDNo=D3.DNo ");
            SQL.AppendLine("                Where D1.DocNo=@DocNo ");
            SQL.AppendLine("            ) D On C.SODocNo=D.SODocNo And C.SODNo=D.SODNo ");
            SQL.AppendLine("            Group By C.SODocNo, C.SODNo ");
            SQL.AppendLine("        ) T4 On T1.DocNo=T4.SODocNo And T1.DNo=T4.SODNo ");
            SQL.AppendLine("    ) T ");
            SQL.AppendLine(") Tbl2 On Tbl1.DocNo=Tbl2.DocNo And Tbl1.DNo=Tbl2.DNo ");
            SQL.AppendLine("Set ProcessIndForDO = ");
            SQL.AppendLine("    Case When Tbl2.DOQty=0 Then 'O' Else ");
            SQL.AppendLine("        Case When Tbl2.DOQty>=Tbl2.SOQty Then 'F' Else 'P' End ");
            SQL.AppendLine("    End; ");

            SQL.AppendLine("Update TblSOContractHdr T Set ");
            SQL.AppendLine("    T.Status= ");
            SQL.AppendLine("        Case When Not Exists( ");
            SQL.AppendLine("            Select DocNo From TblSOContractDtl ");
            SQL.AppendLine("            Where ProcessIndForDO <> 'O' And DocNo=T.DocNo Limit 1 ");
            SQL.AppendLine("        ) Then 'O' Else ");
            SQL.AppendLine("            Case When Not Exists( ");
            SQL.AppendLine("                Select DocNo From TblSOContractDtl ");
            SQL.AppendLine("                Where ProcessIndForDO <> 'F' And DocNo=T.DocNo Limit 1 ");
            SQL.AppendLine("            ) Then 'F' Else 'P' End ");
            SQL.AppendLine("        End ");
            SQL.AppendLine("Where T.ProcessIndForDR <> 'M' ");
            SQL.AppendLine("And T.CancelInd='N' ");
            SQL.AppendLine("And T.DocNo In ( ");
            SQL.AppendLine("    Select Distinct C.SODocNo ");
            SQL.AppendLine("    From TblDOCt2Hdr A ");
            SQL.AppendLine("    Inner Join TblDOCt2Dtl3 B On A.DocNo=B.DocNo ");
            SQL.AppendLine("    Inner Join TblPLDtl C On A.PLDocNo=C.DocNo And B.PLDNo=C.DNo ");
            SQL.AppendLine("    Where A.DocNo=@DocNo ");
            SQL.AppendLine("    ); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            return cm;
        }

        private MySqlCommand SaveStockMovement(string DocNo, string DNo, string CancelInd)
        {
            var SQL = new StringBuilder();

            if (CancelInd == "N")
            {
                SQL.AppendLine("Insert Into TblStockMovement(DocType, DocNo, DNo, CancelInd, DocDt, WhsCode, Lot, Bin, ItCode, PropCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, ");
                if (mIsKawasanBerikatEnabled)
                    SQL.AppendLine("CustomsDocCode, KBContractNo, KBSubmissionNo, KBRegistrationNo, ");
                SQL.AppendLine("CreateBy, CreateDt) ");
                SQL.AppendLine("Select @DocType, A.DocNo, B.DNo, @CancelInd, A.DocDt, A.WhsCode, B.Lot, B.Bin, B.ItCode, IfNull(B.PropCode, '-'), B.BatchNo, B.Source, -1*B.Qty, -1*B.Qty2, -1*B.Qty3, ");
                SQL.AppendLine("Case When A.Remark Is Null Then ");
                SQL.AppendLine("    Case When B.Remark Is Null Then Null Else B.Remark End ");
                SQL.AppendLine("Else ");
                SQL.AppendLine("    Case When B.Remark Is Null Then A.Remark Else Concat(A.Remark, ' ( ', B.Remark, ' )') End ");
                SQL.AppendLine("End As Remark, ");
                if (mIsKawasanBerikatEnabled)
                    SQL.AppendLine("A.CustomsDocCode, A.KBContractNo, A.KBSubmissionNo, A.KBRegistrationNo, ");
                SQL.AppendLine("@UserCode, CurrentDateTime() ");
                SQL.AppendLine("From TblDOCt2Hdr A ");
                SQL.AppendLine("Inner Join TblDOCt2Dtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("Where A.DocNo=@DocNo; ");
            }
            else
            {
                SQL.AppendLine("Insert Into TblStockMovement(DocType, DocNo, DNo, CancelInd, DocDt, WhsCode, Lot, Bin, ItCode, PropCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, ");
                if (mIsKawasanBerikatEnabled)
                    SQL.AppendLine("CustomsDocCode, KBContractNo, KBSubmissionNo, KBRegistrationNo, ");
                SQL.AppendLine("CreateBy, CreateDt) ");
                SQL.AppendLine("Select @DocType, A.DocNo, B.DNo, @CancelInd, A.DocDt, A.WhsCode, B.Lot, B.Bin, B.ItCode, IfNull(B.PropCode, '-'), B.BatchNo, B.Source, B.Qty, B.Qty2, B.Qty3, ");
                SQL.AppendLine("Case When A.Remark Is Null Then ");
                SQL.AppendLine("    Case When B.Remark Is Null Then Null Else B.Remark End ");
                SQL.AppendLine("Else ");
                SQL.AppendLine("    Case When B.Remark Is Null Then A.Remark Else Concat(A.Remark, ' ( ', B.Remark, ' )') End ");
                SQL.AppendLine("End As Remark, ");
                if (mIsKawasanBerikatEnabled)
                    SQL.AppendLine("A.CustomsDocCode, A.KBContractNo, A.KBSubmissionNo, A.KBRegistrationNo, ");
                SQL.AppendLine("@UserCode, CurrentDateTime() ");
                SQL.AppendLine("From TblDOCt2Hdr A ");
                SQL.AppendLine("Inner Join TblDOCt2Dtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("Where A.DocNo=@DocNo ");
                SQL.AppendLine("And Position(Concat('##', DNo, '##') In @DNo)>0; ");
            }
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            if (DNo.Length > 0) Sm.CmParam<String>(ref cm, "@DNo", DNo);
            Sm.CmParam<String>(ref cm, "@DocType", mDocType);
            Sm.CmParam<String>(ref cm, "@CancelInd", CancelInd);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveStockSummary(Byte Type, int Row)
        {
            //Type=1 -> Insert
            //Type=2 -> Edit
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblStockSummary Set ");
            if (Type == 1)
                SQL.AppendLine("    Qty=Qty-@Qty, Qty2=Qty2-@Qty2, Qty3=Qty3-@Qty3, ");
            else
                SQL.AppendLine("    Qty=Qty+@Qty, Qty2=Qty2+@Qty2, Qty3=Qty3+@Qty3, ");
            SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where WHsCode=@WhsCode And Lot=@Lot And Bin=@Bin ");
            SQL.AppendLine("And ItCode=@ItCode And PropCode=@PropCode And BatchNo=@BatchNo And Source=@Source; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            Sm.CmParam<String>(ref cm, "@Lot", Sm.GetGrdStr(Grd1, Row, 12));
            Sm.CmParam<String>(ref cm, "@Bin", Sm.GetGrdStr(Grd1, Row, 13));
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 4));
            Sm.CmParam<String>(ref cm, "@PropCode", Sm.GetGrdStr(Grd1, Row, 8));
            Sm.CmParam<String>(ref cm, "@BatchNo", Sm.GetGrdStr(Grd1, Row, 10));
            Sm.CmParam<String>(ref cm, "@Source", Sm.GetGrdStr(Grd1, Row, 11));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 15));
            Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd1, Row, 18));
            Sm.CmParam<Decimal>(ref cm, "@Qty3", Sm.GetGrdDec(Grd1, Row, 21));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal(string DocNo, bool IsService)
        {
            //Non CBD

            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblDOCt2Hdr Set JournalDocNo=@JournalDocNo Where DocNo=@DocNo;");

            SQL.AppendLine("Insert Into TblJournalHdr ");
            SQL.AppendLine("(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.JournalDocNo, ");
            SQL.AppendLine("A.DocDt, ");
            SQL.AppendLine("Concat('DO To Customer ', @JnDesc, ' : ', A.DocNo) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, ");
            SQL.AppendLine("(Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, ");
            SQL.AppendLine("B.CCCode, A.Remark, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblDOCt2Hdr A ");
            SQL.AppendLine("Left Join TblWarehouse B On A.WhsCode=B.WhsCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, @EntCode, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From (");

            //1
            SQL.AppendLine("        Select E.AcNo, ");
            SQL.AppendLine("        0.00 As DAmt, ");
            SQL.AppendLine("        IfNull(C.UPrice, 0) ");
            if (!IsService)
                SQL.AppendLine("        *IfNull(C.Excrate, 0) ");
            SQL.AppendLine("        *B.Qty As CAmt ");
            SQL.AppendLine("        From TblDOCt2Hdr A ");
            SQL.AppendLine("        Inner join TblDOCt2Dtl B On A.DocNo=B.DocNo ");
            if (IsService)
            {
                SQL.AppendLine("        Inner JOin TblSOContractDtl C On B.SOContractDocNo = C.DocNo And B.SOContractDNo = C.DNo ");
            }
            else
            {
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
            }
            SQL.AppendLine("        Inner Join TblItem D On B.ItCode=D.ItCode ");
            SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo Is Not Null ");
            SQL.AppendLine("        Where A.DocNo=@DocNo And A.DrDocNo Is Not Null ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select E.AcNo As AcNo, ");
            SQL.AppendLine("        0.00 As DAmt, ");
            SQL.AppendLine("        IfNull(C.UPrice, 0) ");
            if (!IsService)
                SQL.AppendLine("        *IfNull(C.Excrate, 0) ");
            SQL.AppendLine("        *B.Qty As CAmt ");
            SQL.AppendLine("        From TblDOCt2Hdr A ");
            SQL.AppendLine("        Inner join TblDOCt2Dtl B On A.DocNo=B.DocNo ");
            if (IsService)
            {
                SQL.AppendLine("        Inner JOin TblSOContractDtl C On B.SOContractDocNo = C.DocNo And B.SOContractDNo = C.DNo ");
            }
            else
            {
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
            }
            SQL.AppendLine("        Inner join TblItem D On B.ItCode=D.ItCode ");
            SQL.AppendLine("        Inner join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo Is Not Null ");
            SQL.AppendLine("        Where A.DocNo=@DocNo And A.PLDocNo Is Not Null ");
            SQL.AppendLine("        Union All ");

            //2
            if (mIsAcNoForSaleUseItemCategory)
            {
                SQL.AppendLine("        Select E.AcNo5 As AcNo, ");
                SQL.AppendLine("        IfNull(C.UPrice, 0) ");
                if (!IsService)
                    SQL.AppendLine("        *IfNull(C.ExcRate, 0) ");
                SQL.AppendLine("        *B.Qty As DAmt, ");
                SQL.AppendLine("        0.00 As CAmt ");
                SQL.AppendLine("        From TblDOCt2Hdr A ");
                SQL.AppendLine("        Inner join TblDOCt2Dtl B On A.DocNo=B.DocNo ");
                if (IsService)
                {
                    SQL.AppendLine("        Inner JOin TblSOContractDtl C On B.SOContractDocNo = C.DocNo And B.SOContractDNo = C.DNo ");
                }
                else
                {
                    SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                }
                SQL.AppendLine("        Inner join TblItem D On B.ItCode=D.ItCode ");
                SQL.AppendLine("        Inner join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo5 Is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo And A.DrDocNo Is Not Null ");
                SQL.AppendLine("        Union All ");
                
                SQL.AppendLine("        Select E.AcNo5 As AcNo, ");
                SQL.AppendLine("        IfNull(C.UPrice, 0) ");
                if (!IsService)
                    SQL.AppendLine("        *IfNull(C.ExcRate, 0) ");
                SQL.AppendLine("        *B.Qty As DAmt, ");
                SQL.AppendLine("        0.00 As CAmt ");
                SQL.AppendLine("        From TblDOCt2Hdr A ");
                SQL.AppendLine("        Inner join TblDOCt2Dtl B On A.DocNo=B.DocNo ");
                if (IsService)
                {
                    SQL.AppendLine("        Inner JOin TblSOContractDtl C On B.SOContractDocNo = C.DocNo And B.SOContractDNo = C.DNo ");
                }
                else
                {
                    SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                }
                SQL.AppendLine("        Inner join TblItem D On B.ItCode=D.ItCode ");
                SQL.AppendLine("        Inner join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo5 Is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo And A.PLDocNo Is Not Null ");
                SQL.AppendLine("        Union All ");
            }
            else
            {
                SQL.AppendLine("        Select @AcNoForCOGS As AcNo, ");
                SQL.AppendLine("        IfNull(C.UPrice, 0) ");
                if (!IsService)
                    SQL.AppendLine("        *IfNull(C.Excrate, 0) ");
                SQL.AppendLine("        *B.Qty As DAmt, ");
                SQL.AppendLine("        0.00 As CAmt ");
                SQL.AppendLine("        From TblDOCt2Hdr A ");
                SQL.AppendLine("        Inner join TblDOCt2Dtl B On A.DocNo=B.DocNo ");
                if (IsService)
                {
                    SQL.AppendLine("        Inner JOin TblSOContractDtl C On B.SOContractDocNo = C.DocNo And B.SOContractDNo = C.DNo ");
                }
                else
                {
                    SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                }
                SQL.AppendLine("        Where A.DocNo=@DocNo And A.DrDocNo Is Not Null ");
                SQL.AppendLine("        Union All ");
                SQL.AppendLine("        Select @AcNoForCOGS As AcNo, ");
                SQL.AppendLine("        IfNull(C.UPrice, 0) ");
                if (!IsService)
                    SQL.AppendLine("        *IfNull(C.Excrate, 0) ");
                SQL.AppendLine("        *B.Qty As DAmt, ");
                SQL.AppendLine("        0.00 As CAmt ");
                SQL.AppendLine("        From TblDOCt2Hdr A ");
                SQL.AppendLine("        Inner join TblDOCt2Dtl B On A.DocNo=B.DocNo ");
                if (IsService)
                {
                    SQL.AppendLine("        Inner JOin TblSOContractDtl C On B.SOContractDocNo = C.DocNo And B.SOContractDNo = C.DNo ");
                }
                else
                {
                    SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                }
                SQL.AppendLine("        Where A.DocNo=@DocNo And A.PLDocNo Is Not Null ");
                SQL.AppendLine("        Union All ");
            }

            //3
            SQL.AppendLine("        Select Concat(@CustomerAcNoNonInvoice, A.CtCode) As AcNo, ");
            SQL.AppendLine("        Case When C.CurCode=@MainCurCode Then 1.00 Else ");
            SQL.AppendLine("        IfNull(( ");
            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
            SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=C.CurCode And CurCode2=@MainCurCode ");
            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
            SQL.AppendLine("        ), 0) End*C.UPrice* ");
            SQL.AppendLine("        Case C.PriceUomCode ");
            SQL.AppendLine("            When D.InventoryUomCode Then B.Qty ");
            SQL.AppendLine("            When D.InventoryUomCode2 Then B.Qty2 ");
            SQL.AppendLine("            When D.InventoryUomCode3 Then B.Qty3 ");
            SQL.AppendLine("            Else 0 ");
            SQL.AppendLine("        End As DAmt, ");
            SQL.AppendLine("        0.00 As CAmt ");
            SQL.AppendLine("        From TblDOCt2Hdr A ");
            SQL.AppendLine("        Inner join TblDOCt2Dtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("        Inner Join  ");
            SQL.AppendLine("        ( ");
            SQL.AppendLine("            Select Distinct A.DocNo, G.ItCode, C.CurCode, F.PriceUomCode, ");
            SQL.AppendLine("            (E.UPrice-(E.UPrice*0.01*IfNull(H.DiscRate, 0))) As UPrice ");
            SQL.AppendLine("            From TblDrHdr A ");
            SQL.AppendLine("            Inner Join TblDrDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("            Inner Join TblSOHdr C On B.SODocNo = C.DocNo  ");
            SQL.AppendLine("            Inner Join TblSODtl D On C.DocNo = D.DocNo ");
            SQL.AppendLine("            Inner Join TblCtQtDtl E On C.CtQTDocno = E.DocNo And D.CtQTDNo = E.DNo ");
            SQL.AppendLine("            Inner Join TblItemPriceHdr F On E.ItemPriceDocNo=F.DocNo ");
            SQL.AppendLine("            Inner Join TblItemPriceDtl G On E.ItemPriceDocNo=G.DocNo And E.ItemPriceDNo=G.DNo ");
            SQL.AppendLine("            Left Join TblSOQuotPromoItem H On C.SOQuotPromoDocNo=H.DocNo And G.ItCode=H.ItCode  ");
            SQL.AppendLine("            Where Exists( ");
            SQL.AppendLine("                Select 1 ");
            SQL.AppendLine("                From TblDOCt2Hdr T1 ");
            SQL.AppendLine("                Inner Join TblDOCt2Dtl2 T2 ");
            SQL.AppendLine("                Where T1.DocNo=T2.DocNo ");
            SQL.AppendLine("                And T1.DRDocNo=B.DocNo ");
            SQL.AppendLine("                And T2.DRDNo=B.DNo ");
			SQL.AppendLine("                And T1.DocNo=@DocNo ");
            SQL.AppendLine("                ) ");
            SQL.AppendLine("        ) C On A.DrDocno=C.DocNo And B.ItCode=C.ItCode ");
            SQL.AppendLine("        Inner join TblItem D On B.ItCode=D.ItCode ");
            SQL.AppendLine("        Where A.DocNo=@DocNo And A.DrDocNo Is Not Null ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select Concat(@CustomerAcNoNonInvoice, A.CtCode) As AcNo, ");
            SQL.AppendLine("        Case When C.CurCode=@MainCurCode Then 1 Else ");
            SQL.AppendLine("        IfNull(( ");
            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
            SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=C.CurCode And CurCode2=@MainCurCode ");
            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
            SQL.AppendLine("        ), 0) End*C.UPrice* ");
            SQL.AppendLine("        Case C.PriceUomCode ");
            SQL.AppendLine("            When D.InventoryUomCode Then B.Qty ");
            SQL.AppendLine("            When D.InventoryUomCode2 Then B.Qty2 ");
            SQL.AppendLine("            When D.InventoryUomCode3 Then B.Qty3 ");
            SQL.AppendLine("            Else 0 ");
            SQL.AppendLine("        End As DAmt, ");
            SQL.AppendLine("        0.00 As CAmt ");
            SQL.AppendLine("        From TblDOCt2Hdr A ");
            SQL.AppendLine("        Inner join TblDOCt2Dtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("        Inner Join  ");
            SQL.AppendLine("        ( ");
            SQL.AppendLine("            Select Distinct A.DocNo, G.ItCode, C.CurCode, F.PriceUomCode, ");
            SQL.AppendLine("            (E.UPrice-(E.UPrice*0.01*IfNull(H.DiscRate, 0))) As UPrice ");
            SQL.AppendLine("            From TblPLHdr A ");
            SQL.AppendLine("            Inner Join TblPLDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("            Inner Join TblSOHdr C On B.SODocNo = C.DocNo  ");
            SQL.AppendLine("            Inner Join TblSODtl D On C.DocNo = D.DocNo ");
            SQL.AppendLine("            Inner Join TblCtQtDtl E On C.CtQTDocno = E.DocNo And D.CtQTDno = E.DNo ");
            SQL.AppendLine("            Inner Join TblItemPriceHdr F On E.ItemPriceDocNo=F.DocNo ");
            SQL.AppendLine("            Inner Join TblItemPriceDtl G On E.ItemPriceDocNo=G.DocNo And E.ItemPriceDNo=G.DNo ");
            SQL.AppendLine("            Left Join TblSOQuotPromoItem H On C.SOQuotPromoDocNo=H.DocNo And G.ItCode=H.ItCode  ");
            SQL.AppendLine("            Where Exists( ");
            SQL.AppendLine("                Select 1 ");
            SQL.AppendLine("                From TblDOCt2Hdr T1 ");
            SQL.AppendLine("                Inner Join TblDOCt2Dtl3 T2 ");
            SQL.AppendLine("                Where T1.DocNo=T2.DocNo ");
            SQL.AppendLine("                And T1.PLDocNo=B.DocNo ");
            SQL.AppendLine("                And T2.PLDNo=B.DNo ");
            SQL.AppendLine("                And T1.DocNo=@DocNo ");
            SQL.AppendLine("                ) ");
            SQL.AppendLine("        ) C On A.PLDocno=C.DocNo And B.ItCode=C.ItCode ");
            SQL.AppendLine("        Inner join TblItem D On B.ItCode=D.ItCode ");
            SQL.AppendLine("        Where A.DocNo=@DocNo And A.PLDocNo Is Not Null ");
            SQL.AppendLine("        Union All ");

            SQL.AppendLine("        Select Concat(@CustomerAcNoNonInvoice, A.CtCode) As AcNo, ");
            SQL.AppendLine("        Case When C.CurCode=@MainCurCode Then 1.00 Else ");
            SQL.AppendLine("        IfNull(( ");
            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
            SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=C.CurCode And CurCode2=@MainCurCode ");
            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
            SQL.AppendLine("        ), 0) End*C.UPrice* ");
            SQL.AppendLine("        Case C.PriceUomCode ");
            SQL.AppendLine("            When D.InventoryUomCode Then B.Qty ");
            SQL.AppendLine("            When D.InventoryUomCode2 Then B.Qty2 ");
            SQL.AppendLine("            When D.InventoryUomCode3 Then B.Qty3 ");
            SQL.AppendLine("            Else 0 ");
            SQL.AppendLine("        End As DAmt, ");
            SQL.AppendLine("        0.00 As CAmt ");
            SQL.AppendLine("        From TblDOCt2Hdr A ");
            SQL.AppendLine("        Inner join TblDOCt2Dtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("        Inner Join  ");
            SQL.AppendLine("        ( ");
            SQL.AppendLine("            Select Distinct A.DocNo, D.ItCode, C.CurCode, D.PackagingUnitUomCode AS PriceUomCode, ");
            SQL.AppendLine("            D.Amt As UPrice ");
            SQL.AppendLine("            From TblDrHdr A ");
            SQL.AppendLine("            Inner Join TblDrDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("            Inner Join TblSOContractHdr C On B.SODocNo = C.DocNo  ");
            SQL.AppendLine("            Inner Join TblSOContractDtl D On C.DocNo = D.DocNo ");
            SQL.AppendLine("            Where Exists( ");
            SQL.AppendLine("                Select 1 ");
            SQL.AppendLine("                From TblDOCt2Hdr T1 ");
            SQL.AppendLine("                Inner Join TblDOCt2Dtl2 T2 ");
            SQL.AppendLine("                Where T1.DocNo=T2.DocNo ");
            SQL.AppendLine("                And T1.DRDocNo=B.DocNo ");
            SQL.AppendLine("                And T2.DRDNo=B.DNo ");
            SQL.AppendLine("                And T1.DocNo=@DocNo ");
            SQL.AppendLine("                ) ");
            SQL.AppendLine("        ) C On A.DrDocno=C.DocNo And B.ItCode=C.ItCode ");
            SQL.AppendLine("        Inner join TblItem D On B.ItCode=D.ItCode ");
            SQL.AppendLine("        Where A.DocNo=@DocNo And A.DrDocNo Is Not Null ");
            SQL.AppendLine("        Union All ");
            
            //4
            if (mIsAcNoForSaleUseItemCategory)
            {
                SQL.AppendLine("        Select E.AcNo4 As AcNo, ");
                SQL.AppendLine("        0.00 As DAmt, ");
                SQL.AppendLine("        Case When C.CurCode=@MainCurCode Then 1.00 Else ");
                SQL.AppendLine("        IfNull(( ");
                SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=C.CurCode And CurCode2=@MainCurCode ");
                SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("        ), 0.00) End*C.UPrice*");
                SQL.AppendLine("        Case C.PriceUomCode ");
                SQL.AppendLine("            When D.InventoryUomCode Then B.Qty ");
                SQL.AppendLine("            When D.InventoryUomCode2 Then B.Qty2 ");
                SQL.AppendLine("            When D.InventoryUomCode3 Then B.Qty3 ");
                SQL.AppendLine("            Else 0 ");
                SQL.AppendLine("        End As CAmt ");
                SQL.AppendLine("        From TblDOCt2Hdr A ");
                SQL.AppendLine("        Inner join TblDOCt2Dtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Inner Join  ");
                SQL.AppendLine("        ( ");
                SQL.AppendLine("            Select Distinct A.DocNo, G.ItCode, C.CurCode, F.PriceUomCode, ");
                SQL.AppendLine("            (E.UPrice-(E.UPrice*0.01*IfNull(H.DiscRate, 0))) As UPrice ");
                SQL.AppendLine("            From TblDrHdr A ");
                SQL.AppendLine("            Inner Join TblDrDtl B On A.DocNo = B.DocNo ");
                SQL.AppendLine("            Inner Join TblSOHdr C On B.SODocNo = C.DocNo  ");
                SQL.AppendLine("            Inner Join TblSODtl D On C.DocNo = D.DocNo ");
                SQL.AppendLine("            Inner Join TblCtQtDtl E On C.CtQTDocno = E.DocNo And D.CtQTDNo = E.DNo ");
                SQL.AppendLine("            Inner Join TblItemPriceHdr F On E.ItemPriceDocNo=F.DocNo ");
                SQL.AppendLine("            Inner Join TblItemPriceDtl G On E.ItemPriceDocNo=G.DocNo And E.ItemPriceDNo=G.DNo ");
                SQL.AppendLine("            Left Join TblSOQuotPromoItem H On C.SOQuotPromoDocNo=H.DocNo And G.ItCode=H.ItCode  ");
                SQL.AppendLine("            Where Exists( ");
                SQL.AppendLine("                Select 1 ");
                SQL.AppendLine("                From TblDOCt2Hdr T1 ");
                SQL.AppendLine("                Inner Join TblDOCt2Dtl2 T2 ");
                SQL.AppendLine("                Where T1.DocNo=T2.DocNo ");
                SQL.AppendLine("                And T1.DRDocNo=B.DocNo ");
                SQL.AppendLine("                And T2.DRDNo=B.DNo ");
                SQL.AppendLine("                And T1.DocNo=@DocNo ");
                SQL.AppendLine("                ) ");
                SQL.AppendLine("        ) C On A.DrDocno=C.DocNo And B.ItCode=C.ItCode ");
                SQL.AppendLine("        Inner join TblItem D On B.ItCode=D.ItCode ");
                SQL.AppendLine("        Inner join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo4 Is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo And A.DrDocNo Is Not Null ");

                SQL.AppendLine("        Union All ");

                SQL.AppendLine("        Select E.AcNo4 As AcNo, 0.00 As DAmt, ");
                SQL.AppendLine("        Case When C.CurCode=@MainCurCode Then 1.00 Else ");
                SQL.AppendLine("        IfNull(( ");
                SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=C.CurCode And CurCode2=@MainCurCode ");
                SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("        ), 0.00) End*C.UPrice*");
                SQL.AppendLine("        Case C.PriceUomCode ");
                SQL.AppendLine("            When D.InventoryUomCode Then B.Qty ");
                SQL.AppendLine("            When D.InventoryUomCode2 Then B.Qty2 ");
                SQL.AppendLine("            When D.InventoryUomCode3 Then B.Qty3 ");
                SQL.AppendLine("            Else 0.00 ");
                SQL.AppendLine("        End As CAmt ");
                SQL.AppendLine("        From TblDOCt2Hdr A ");
                SQL.AppendLine("        Inner join TblDOCt2Dtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Inner Join  ");
                SQL.AppendLine("        ( ");
                SQL.AppendLine("            Select Distinct A.DocNo, G.ItCode, C.CurCode, F.PriceUomCode, ");
                SQL.AppendLine("            (E.UPrice-(E.UPrice*0.01*IfNull(H.DiscRate, 0))) As UPrice ");
                SQL.AppendLine("            From TblPLHdr A ");
                SQL.AppendLine("            Inner Join TblPLDtl B On A.DocNo = B.DocNo ");
                SQL.AppendLine("            Inner Join TblSOHdr C On B.SODocNo = C.DocNo  ");
                SQL.AppendLine("            Inner Join TblSODtl D On C.DocNo = D.DocNo ");
                SQL.AppendLine("            Inner Join TblCtQtDtl E On C.CtQTDocno = E.DocNo And D.CtQTDno = E.DNo ");
                SQL.AppendLine("            Inner Join TblItemPriceHdr F On E.ItemPriceDocNo=F.DocNo ");
                SQL.AppendLine("            Inner Join TblItemPriceDtl G On E.ItemPriceDocNo=G.DocNo And E.ItemPriceDNo=G.DNo ");
                SQL.AppendLine("            Left Join TblSOQuotPromoItem H On C.SOQuotPromoDocNo=H.DocNo And G.ItCode=H.ItCode  ");
                SQL.AppendLine("            Where Exists( ");
                SQL.AppendLine("                Select 1 ");
                SQL.AppendLine("                From TblDOCt2Hdr T1 ");
                SQL.AppendLine("                Inner Join TblDOCt2Dtl3 T2 ");
                SQL.AppendLine("                Where T1.DocNo=T2.DocNo ");
                SQL.AppendLine("                And T1.PLDocNo=B.DocNo ");
                SQL.AppendLine("                And T2.PLDNo=B.DNo ");
                SQL.AppendLine("                And T1.DocNo=@DocNo ");
                SQL.AppendLine("                ) ");
                SQL.AppendLine("        ) C On A.PLDocno=C.DocNo And B.ItCode=C.ItCode ");
                SQL.AppendLine("        Inner join TblItem D On B.ItCode=D.ItCode ");
                SQL.AppendLine("        Inner join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo4 Is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo And A.PLDocNo Is Not Null ");

                SQL.AppendLine("        Union All ");

                SQL.AppendLine("        Select E.AcNo4 As AcNo, ");
                SQL.AppendLine("        0.00 As DAmt, ");
                SQL.AppendLine("        Case When C.CurCode=@MainCurCode Then 1.00 Else ");
                SQL.AppendLine("        IfNull(( ");
                SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=C.CurCode And CurCode2=@MainCurCode ");
                SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("        ), 0.00) End*C.UPrice*");
                SQL.AppendLine("        Case C.PriceUomCode ");
                SQL.AppendLine("            When D.InventoryUomCode Then B.Qty ");
                SQL.AppendLine("            When D.InventoryUomCode2 Then B.Qty2 ");
                SQL.AppendLine("            When D.InventoryUomCode3 Then B.Qty3 ");
                SQL.AppendLine("            Else 0 ");
                SQL.AppendLine("        End As CAmt ");
                SQL.AppendLine("        From TblDOCt2Hdr A ");
                SQL.AppendLine("        Inner join TblDOCt2Dtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Inner Join  ");
                SQL.AppendLine("        ( ");
                SQL.AppendLine("            Select Distinct A.DocNo, D.ItCode, C.CurCode, D.PackagingUnitUomCode As PriceUomCode, ");
                SQL.AppendLine("            D.Amt As UPrice ");
                SQL.AppendLine("            From TblDrHdr A ");
                SQL.AppendLine("            Inner Join TblDrDtl B On A.DocNo = B.DocNo ");
                SQL.AppendLine("            Inner Join TblSOContractHdr C On B.SODocNo = C.DocNo  ");
                SQL.AppendLine("            Inner Join TblSOContractDtl D On C.DocNo = D.DocNo ");
                SQL.AppendLine("            Where Exists( ");
                SQL.AppendLine("                Select 1 ");
                SQL.AppendLine("                From TblDOCt2Hdr T1 ");
                SQL.AppendLine("                Inner Join TblDOCt2Dtl2 T2 ");
                SQL.AppendLine("                Where T1.DocNo=T2.DocNo ");
                SQL.AppendLine("                And T1.DRDocNo=B.DocNo ");
                SQL.AppendLine("                And T2.DRDNo=B.DNo ");
                SQL.AppendLine("                And T1.DocNo=@DocNo ");
                SQL.AppendLine("                ) ");
                SQL.AppendLine("        ) C On A.DrDocno=C.DocNo And B.ItCode=C.ItCode ");
                SQL.AppendLine("        Inner join TblItem D On B.ItCode=D.ItCode ");
                SQL.AppendLine("        Inner join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo4 Is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo And A.DrDocNo Is Not Null ");
            }
            else
            {
                SQL.AppendLine("        Select @AcNoForSaleOfFinishedGoods As AcNo, ");
                SQL.AppendLine("        0.00 As DAmt, ");
                SQL.AppendLine("        Case When C.CurCode=@MainCurCode Then 1.00 Else ");
                SQL.AppendLine("        IfNull(( ");
                SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=C.CurCode And CurCode2=@MainCurCode ");
                SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("        ), 0.00) End*C.UPrice*");
                SQL.AppendLine("        Case C.PriceUomCode ");
                SQL.AppendLine("            When D.InventoryUomCode Then B.Qty ");
                SQL.AppendLine("            When D.InventoryUomCode2 Then B.Qty2 ");
                SQL.AppendLine("            When D.InventoryUomCode3 Then B.Qty3 ");
                SQL.AppendLine("            Else 0 ");
                SQL.AppendLine("        End As CAmt ");
                SQL.AppendLine("        From TblDOCt2Hdr A ");
                SQL.AppendLine("        Inner join TblDOCt2Dtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Inner Join  ");
                SQL.AppendLine("        ( ");
                SQL.AppendLine("            Select Distinct A.DocNo, G.ItCode, C.CurCode, F.PriceUomCode, ");
                SQL.AppendLine("            (E.UPrice-(E.UPrice*0.01*IfNull(H.DiscRate, 0))) As UPrice ");
                SQL.AppendLine("            From TblDrHdr A ");
                SQL.AppendLine("            Inner Join TblDrDtl B On A.DocNo = B.DocNo ");
                SQL.AppendLine("            Inner Join TblSOHdr C On B.SODocNo = C.DocNo  ");
                SQL.AppendLine("            Inner Join TblSODtl D On C.DocNo = D.DocNo ");
                SQL.AppendLine("            Inner Join TblCtQtDtl E On C.CtQTDocno = E.DocNo And D.CtQTDNo = E.DNo ");
                SQL.AppendLine("            Inner Join TblItemPriceHdr F On E.ItemPriceDocNo=F.DocNo ");
                SQL.AppendLine("            Inner Join TblItemPriceDtl G On E.ItemPriceDocNo=G.DocNo And E.ItemPriceDNo=G.DNo ");
                SQL.AppendLine("            Left Join TblSOQuotPromoItem H On C.SOQuotPromoDocNo=H.DocNo And G.ItCode=H.ItCode  ");
                SQL.AppendLine("            Where Exists( ");
                SQL.AppendLine("                Select 1 ");
                SQL.AppendLine("                From TblDOCt2Hdr T1 ");
                SQL.AppendLine("                Inner Join TblDOCt2Dtl2 T2 ");
                SQL.AppendLine("                Where T1.DocNo=T2.DocNo ");
                SQL.AppendLine("                And T1.DRDocNo=B.DocNo ");
                SQL.AppendLine("                And T2.DRDNo=B.DNo ");
                SQL.AppendLine("                And T1.DocNo=@DocNo ");
                SQL.AppendLine("                ) ");
                SQL.AppendLine("        ) C On A.DrDocno=C.DocNo And B.ItCode=C.ItCode ");
                SQL.AppendLine("        Inner join TblItem D On B.ItCode=D.ItCode ");
                SQL.AppendLine("        Where A.DocNo=@DocNo And A.DrDocNo Is Not Null ");

                SQL.AppendLine("        Union All ");

                SQL.AppendLine("        Select @AcNoForSaleOfFinishedGoods As AcNo, 0.00 As DAmt, ");
                SQL.AppendLine("        Case When C.CurCode=@MainCurCode Then 1.00 Else ");
                SQL.AppendLine("        IfNull(( ");
                SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=C.CurCode And CurCode2=@MainCurCode ");
                SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("        ), 0.00) End*C.UPrice*");
                SQL.AppendLine("        Case C.PriceUomCode ");
                SQL.AppendLine("            When D.InventoryUomCode Then B.Qty ");
                SQL.AppendLine("            When D.InventoryUomCode2 Then B.Qty2 ");
                SQL.AppendLine("            When D.InventoryUomCode3 Then B.Qty3 ");
                SQL.AppendLine("            Else 0.00 ");
                SQL.AppendLine("        End As CAmt ");
                SQL.AppendLine("        From TblDOCt2Hdr A ");
                SQL.AppendLine("        Inner join TblDOCt2Dtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Inner Join  ");
                SQL.AppendLine("        ( ");
                SQL.AppendLine("            Select Distinct A.DocNo, G.ItCode, C.CurCode, F.PriceUomCode, ");
                SQL.AppendLine("            (E.UPrice-(E.UPrice*0.01*IfNull(H.DiscRate, 0))) As UPrice ");
                SQL.AppendLine("            From TblPLHdr A ");
                SQL.AppendLine("            Inner Join TblPLDtl B On A.DocNo = B.DocNo ");
                SQL.AppendLine("            Inner Join TblSOHdr C On B.SODocNo = C.DocNo  ");
                SQL.AppendLine("            Inner Join TblSODtl D On C.DocNo = D.DocNo ");
                SQL.AppendLine("            Inner Join TblCtQtDtl E On C.CtQTDocno = E.DocNo And D.CtQTDno = E.DNo ");
                SQL.AppendLine("            Inner Join TblItemPriceHdr F On E.ItemPriceDocNo=F.DocNo ");
                SQL.AppendLine("            Inner Join TblItemPriceDtl G On E.ItemPriceDocNo=G.DocNo And E.ItemPriceDNo=G.DNo ");
                SQL.AppendLine("            Left Join TblSOQuotPromoItem H On C.SOQuotPromoDocNo=H.DocNo And G.ItCode=H.ItCode  ");
                SQL.AppendLine("            Where Exists( ");
                SQL.AppendLine("                Select 1 ");
                SQL.AppendLine("                From TblDOCt2Hdr T1 ");
                SQL.AppendLine("                Inner Join TblDOCt2Dtl3 T2 ");
                SQL.AppendLine("                Where T1.DocNo=T2.DocNo ");
                SQL.AppendLine("                And T1.PLDocNo=B.DocNo ");
                SQL.AppendLine("                And T2.PLDNo=B.DNo ");
                SQL.AppendLine("                And T1.DocNo=@DocNo ");
                SQL.AppendLine("                ) ");
                SQL.AppendLine("        ) C On A.PLDocno=C.DocNo And B.ItCode=C.ItCode ");
                SQL.AppendLine("        Inner join TblItem D On B.ItCode=D.ItCode ");
                SQL.AppendLine("        Where A.DocNo=@DocNo And A.PLDocNo Is Not Null ");

                SQL.AppendLine("        Union All ");

                SQL.AppendLine("        Select @AcNoForSaleOfFinishedGoods As AcNo, ");
                SQL.AppendLine("        0.00 As DAmt, ");
                SQL.AppendLine("        Case When C.CurCode=@MainCurCode Then 1.00 Else ");
                SQL.AppendLine("        IfNull(( ");
                SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=C.CurCode And CurCode2=@MainCurCode ");
                SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("        ), 0.00) End*C.UPrice*");
                SQL.AppendLine("        Case C.PriceUomCode ");
                SQL.AppendLine("            When D.InventoryUomCode Then B.Qty ");
                SQL.AppendLine("            When D.InventoryUomCode2 Then B.Qty2 ");
                SQL.AppendLine("            When D.InventoryUomCode3 Then B.Qty3 ");
                SQL.AppendLine("            Else 0 ");
                SQL.AppendLine("        End As CAmt ");
                SQL.AppendLine("        From TblDOCt2Hdr A ");
                SQL.AppendLine("        Inner join TblDOCt2Dtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Inner Join  ");
                SQL.AppendLine("        ( ");
                SQL.AppendLine("            Select Distinct A.DocNo, D.ItCode, C.CurCode, D.PackagingUnitUomCode As PriceUomCode, ");
                SQL.AppendLine("            D.Amt As UPrice ");
                SQL.AppendLine("            From TblDrHdr A ");
                SQL.AppendLine("            Inner Join TblDrDtl B On A.DocNo = B.DocNo ");
                SQL.AppendLine("            Inner Join TblSOContractHdr C On B.SODocNo = C.DocNo  ");
                SQL.AppendLine("            Inner Join TblSOContractDtl D On C.DocNo = D.DocNo ");
                SQL.AppendLine("            Where Exists( ");
                SQL.AppendLine("                Select 1 ");
                SQL.AppendLine("                From TblDOCt2Hdr T1 ");
                SQL.AppendLine("                Inner Join TblDOCt2Dtl2 T2 ");
                SQL.AppendLine("                Where T1.DocNo=T2.DocNo ");
                SQL.AppendLine("                And T1.DRDocNo=B.DocNo ");
                SQL.AppendLine("                And T2.DRDNo=B.DNo ");
                SQL.AppendLine("                And T1.DocNo=@DocNo ");
                SQL.AppendLine("                ) ");
                SQL.AppendLine("        ) C On A.DrDocno=C.DocNo And B.ItCode=C.ItCode ");
                SQL.AppendLine("        Inner join TblItem D On B.ItCode=D.ItCode ");
                SQL.AppendLine("        Where A.DocNo=@DocNo And A.DrDocNo Is Not Null ");
            }
            SQL.AppendLine("    ) Tbl Where AcNo is Not Null Group By AcNo ");
            SQL.AppendLine(") B On 0=0 ");
            SQL.AppendLine("Where A.DocNo=@JournalDocNo;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            if (TxtPLDocNo.Text.Length>0)
                Sm.CmParam<String>(ref cm, "@JnDesc", " (PL)");
            else
                Sm.CmParam<String>(ref cm, "@JnDesc", " (DR)");
            if(mDocNoFormat == "1")
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(Sm.GetDte(DteDocDt), 1));
            else
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNo3(Sm.GetDte(DteDocDt), "Journal", "TblJournalHdr", mEntCode, "1"));
            Sm.CmParam<String>(ref cm, "@MainCurCode", mMainCurCode);
            Sm.CmParam<String>(ref cm, "@CustomerAcNoNonInvoice", mCustomerAcNoNonInvoice);
            Sm.CmParam<String>(ref cm, "@AcNoForCOGS", mAcNoForCOGS);
            Sm.CmParam<String>(ref cm, "@AcNoForSaleOfFinishedGoods", mAcNoForSaleOfFinishedGoods);
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@EntCode", mEntCode);
            return cm;
        }

        private MySqlCommand SaveJournal2(string DocNo, bool IsService)
        {
            //CBD

            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblDOCt2Hdr Set JournalDocNo=@JournalDocNo Where DocNo=@DocNo;");

            SQL.AppendLine("Insert Into TblJournalHdr ");
            SQL.AppendLine("(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.JournalDocNo, ");
            SQL.AppendLine("A.DocDt, ");
            SQL.AppendLine("Concat('DO To Customer (CBD) : ', A.DocNo) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, ");
            SQL.AppendLine("(Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, ");
            SQL.AppendLine("B.CCCode, A.Remark, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblDOCt2Hdr A ");
            SQL.AppendLine("Left Join TblWarehouse B On A.WhsCode=B.WhsCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, @EntCode, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From (");
            //1

            SQL.AppendLine("        Select E.AcNo, ");
            SQL.AppendLine("        0.00 As DAmt, ");
            SQL.AppendLine("        IfNull(C.UPrice, 0) ");
            if (!IsService)
                SQL.AppendLine("        *IfNull(C.ExcRate, 0) ");
            SQL.AppendLine("        *B.Qty As CAmt ");
            SQL.AppendLine("        From TblDOCt2Hdr A ");
            SQL.AppendLine("        Inner join TblDOCt2Dtl B On A.DocNo=B.DocNo ");
            if (IsService)
            {
                SQL.AppendLine("        Inner JOin TblSOContractDtl C On B.SOContractDocNo = C.DocNo And B.SOContractDNo = C.DNo ");
            }
            else
            {
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
            }
            SQL.AppendLine("        Inner Join TblItem D On B.ItCode=D.ItCode ");
            SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo Is Not Null ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");
            SQL.AppendLine("        Union All ");            

            //2
            SQL.AppendLine("        Select @AcNoForCOGS As AcNo, ");
            SQL.AppendLine("        IfNull(C.UPrice, 0) ");
            if (!IsService)
                SQL.AppendLine("        *IfNull(C.ExcRate, 0) ");
            SQL.AppendLine("        *B.Qty As DAmt, ");
            SQL.AppendLine("        0.00 As CAmt ");
            SQL.AppendLine("        From TblDOCt2Hdr A ");
            SQL.AppendLine("        Inner join TblDOCt2Dtl B On A.DocNo=B.DocNo ");
            if (IsService)
            {
                SQL.AppendLine("        Inner JOin TblSOContractDtl C On B.SOContractDocNo = C.DocNo And B.SOContractDNo = C.DNo ");
            }
            else
            {
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
            }
            SQL.AppendLine("        Where A.DocNo=@DocNo ");
            SQL.AppendLine("        Union All ");
            

            //3
            SQL.AppendLine("    Select T.AcNo, ");
            SQL.AppendLine("    Case When T.Amt>=0 Then T.Amt Else 0 End As DAmt, ");
            SQL.AppendLine("    Case When T.Amt>=0 Then 0 Else Abs(T.Amt) End As CAmt ");
            SQL.AppendLine("    From (");
            SQL.AppendLine("        Select Concat(T3.ParValue, @CtCode) As AcNo, ");
            SQL.AppendLine("        Case When T1.CurCode=@MainCurCode Then 1 Else ");
            SQL.AppendLine("        IfNull(( ");
            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
            SQL.AppendLine("            Where RateDt<=@DocDt And CurCode1=T1.CurCode And CurCode2=@MainCurCode ");
            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
            SQL.AppendLine("            ), 0) End*");
            SQL.AppendLine("        (T1.Amt-IfNull(T2.Amt, 0)) As Amt ");
            SQL.AppendLine("        From (");
            SQL.AppendLine("            Select CurCode, Sum(TotalAmt) As Amt ");
            SQL.AppendLine("            From TblSalesInvoiceHdr ");
            SQL.AppendLine("            Where CancelInd='N' ");
            SQL.AppendLine("            And SODocNo In ( ");
            SQL.AppendLine("                Select Distinct C.SODocNo ");
            SQL.AppendLine("                From TblDOCt2Hdr A ");
            SQL.AppendLine("                Inner join TblDRHdr B On A.DRDocNo=B.DocNo ");
            SQL.AppendLine("                Inner join TblDRDtl C On B.DocNo=C.DocNo ");
            SQL.AppendLine("                Where A.DocNo=@DocNo  ");
            SQL.AppendLine("            ) ");
            SQL.AppendLine("            Group By CurCode ");
            SQL.AppendLine("        ) T1 ");
            SQL.AppendLine("        Left Join (");
            SQL.AppendLine("            Select X1.CurCode, Sum(X2.CAmt-X2.DAmt) As Amt ");
            SQL.AppendLine("            From TblSalesInvoiceHdr X1 ");
            SQL.AppendLine("            Inner Join TblSalesInvoiceDtl2 X2 On X1.DocNo=X2.DocNo And X2.AcInd='N' ");
            SQL.AppendLine("            Where X1.CancelInd='N' ");
            SQL.AppendLine("            And X1.SODocNo In ( ");
            SQL.AppendLine("                Select Distinct C.SODocNo ");
            SQL.AppendLine("                From TblDOCt2Hdr A ");
            SQL.AppendLine("                Inner join TblDRHdr B On A.DRDocNo=B.DocNo ");
            SQL.AppendLine("                Inner join TblDRDtl C On B.DocNo=C.DocNo ");
            SQL.AppendLine("                Where A.DocNo=@DocNo  ");
            SQL.AppendLine("            ) ");
            SQL.AppendLine("            Group By X1.CurCode ");
            SQL.AppendLine("        ) T2 On T1.CurCode=T2.CurCode ");
            SQL.AppendLine("        Inner Join TblParameter T3 On T3.ParCode='CustomerAcNoDownPayment' And T3.ParValue Is Not Null ");
            SQL.AppendLine("    ) T ");
            SQL.AppendLine("    Union All ");

            //4
            SQL.AppendLine("    Select T.AcNo, ");
            SQL.AppendLine("    Case When T.Amt>=0 Then 0 Else Abs(T.Amt) End As DAmt, ");
            SQL.AppendLine("    Case When T.Amt>=0 Then T.Amt Else 0 End As CAmt ");
            SQL.AppendLine("    From (");
            SQL.AppendLine("            Select @AcNoForSaleOfFinishedGoods As AcNo, ");
            SQL.AppendLine("            0 As DAmt, ");
            SQL.AppendLine("            Case When T1.CurCode=@MainCurCode Then 1 Else ");
            SQL.AppendLine("            IfNull(( ");
            SQL.AppendLine("                Select Amt From TblCurrencyRate ");
            SQL.AppendLine("                Where RateDt<=@DocDt And CurCode1=T1.CurCode And CurCode2=@MainCurCode ");
            SQL.AppendLine("                Order By RateDt Desc Limit 1 ");
            SQL.AppendLine("                ), 0) End*");
            SQL.AppendLine("            (T1.Amt-IfNull(T2.Amt, 0)) As Amt ");
            SQL.AppendLine("            From (");
            SQL.AppendLine("                Select CurCode, Sum(TotalAmt) As Amt ");
            SQL.AppendLine("                From TblSalesInvoiceHdr ");
            SQL.AppendLine("                Where CancelInd='N' ");
            SQL.AppendLine("                And SODocNo In ( ");
            SQL.AppendLine("                    Select Distinct C.SODocNo ");
            SQL.AppendLine("                    From TblDOCt2Hdr A ");
            SQL.AppendLine("                    Inner join TblDRHdr B On A.DRDocNo=B.DocNo ");
            SQL.AppendLine("                    Inner join TblDRDtl C On B.DocNo=C.DocNo ");
            SQL.AppendLine("                    Where A.DocNo=@DocNo  ");
            SQL.AppendLine("                ) ");
            SQL.AppendLine("                Group By CurCode ");
            SQL.AppendLine("            ) T1 ");
            SQL.AppendLine("            Left Join (");
            SQL.AppendLine("                Select X1.CurCode, Sum(X2.CAmt-X2.DAmt) As Amt ");
            SQL.AppendLine("                From TblSalesInvoiceHdr X1 ");
            SQL.AppendLine("                Inner Join TblSalesInvoiceDtl2 X2 On X1.DocNo=X2.DocNo And X2.AcInd='N' ");
            SQL.AppendLine("                Where X1.CancelInd='N' ");
            SQL.AppendLine("                And X1.SODocNo In ( ");
            SQL.AppendLine("                    Select Distinct C.SODocNo ");
            SQL.AppendLine("                    From TblDOCt2Hdr A ");
            SQL.AppendLine("                    Inner join TblDRHdr B On A.DRDocNo=B.DocNo ");
            SQL.AppendLine("                    Inner join TblDRDtl C On B.DocNo=C.DocNo ");
            SQL.AppendLine("                    Where A.DocNo=@DocNo  ");
            SQL.AppendLine("                ) ");
            SQL.AppendLine("                Group By X1.CurCode ");
            SQL.AppendLine("            ) T2 On T1.CurCode=T2.CurCode ");
            SQL.AppendLine("        ) T ");
            SQL.AppendLine("    ) Tbl Where AcNo is Not Null Group By AcNo ");
            SQL.AppendLine(") B On 0=0 ");
            SQL.AppendLine("Where A.DocNo=@JournalDocNo;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            if(mDocNoFormat == "1")
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(Sm.GetDte(DteDocDt), 1));
            else
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNo3(Sm.GetDte(DteDocDt), "Journal", "TblJournalHdr", mEntCode, "1"));
            Sm.CmParam<String>(ref cm, "@CtCode", mCtCode);
            Sm.CmParam<String>(ref cm, "@MainCurCode", mMainCurCode);
            Sm.CmParam<String>(ref cm, "@AcNoForCOGS", mAcNoForCOGS);
            Sm.CmParam<String>(ref cm, "@AcNoForSaleOfFinishedGoods", mAcNoForSaleOfFinishedGoods);
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@EntCode", mEntCode);
            return cm;
        }

        #endregion

        #region Cancel Data

        private void CancelData()
        {
            UpdateCancelledItem();

            string DNo = "##XXX##";

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdBool(Grd1, Row, 1) && !Sm.GetGrdBool(Grd1, Row, 2) && Sm.GetGrdStr(Grd1, Row, 4).Length > 0)
                    DNo += "##" + Sm.GetGrdStr(Grd1, Row, 0) + "##";

            bool IsCbd = IsCBD();

            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || 
                IsCancelledDataNotValid(DNo, IsCbd)) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(CancelDOCt2Dtl(DNo));

            if (TxtDRDocNo.Text.Length > 0)
            {
                for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd2, Row, 0).Length > 0) cml.Add(SaveDOCt2Dtl2(TxtDocNo.Text, Row, IsCbd));
            }

            if (TxtPLDocNo.Text.Length > 0)
            {
                for (int Row = 0; Row < Grd2.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd2, Row, 0).Length > 0) cml.Add(SaveDOCt2Dtl3(TxtDocNo.Text, Row));
            }

            cml.Add(SaveStockMovement(TxtDocNo.Text, DNo, "Y"));

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 4).Length > 0 &&                    
                    Sm.GetGrdBool(Grd1, Row, 1) && 
                    !Sm.GetGrdBool(Grd1, Row, 2))
                    cml.Add(SaveStockSummary(2, Row));

            if (TxtDRDocNo.Text.Length>0) cml.Add(UpdateSO(TxtDocNo.Text));
            if (TxtPLDocNo.Text.Length > 0) cml.Add(UpdateSO2(TxtDocNo.Text));

            if (mIsAutoJournalActived && IsJournalDataExisted() && !mIsDOCt2JournalDisabled)
            {
                if (IsCbd)
                    cml.Add(SaveJournal2());
                else
                    cml.Add(SaveJournal());
            }

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private void UpdateCancelledItem()
        {
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText =
                        "Select DNo, CancelInd From TblDOCt2Dtl " +
                        "Where DocNo=@DocNo Order By DNo;"
                };
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DNo", "CancelInd" });

                if (dr.HasRows)
                {
                    Grd1.ProcessTab = true;
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        {
                            if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 0), Sm.DrStr(dr, 0)))
                            {
                                if (Sm.CompareStr(Sm.DrStr(dr, 1), "Y"))
                                    Sm.SetGrdValue("B", Grd1, dr, c, Row, 1, 1);
                                Sm.SetGrdValue("B", Grd1, dr, c, Row, 2, 1);
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        private bool IsCancelledDataNotValid(string DNo, bool IsCBD)
        {
            return
                Sm.IsClosingJournalInvalid(mIsAutoJournalActived, true, Sm.GetDte(DteDocDt)) ||
                IsCancelledItemNotExisted(DNo) ||
                IsDOCtAlreadyProcessed() ||
                IsCBDDOCtInvalid(IsCBD)
                ;
        }

        private bool IsCBDDOCtInvalid(bool IsCBD)
        {
            if (IsCBD)
            {
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    if (!Sm.GetGrdBool(Grd1, r, 1) && Sm.GetGrdStr(Grd1, r, 4).Length > 0)
                    {
                        Sm.StdMsg(mMsgType.Warning, 
                            "For DO to customer (CBD), " + Environment.NewLine + 
                            "You need to cancel all the items.");
                        return true;
                    }
                }
            }
            return false;
        }

        private bool IsCancelledItemNotExisted(string DNo)
        {
            if (Sm.CompareStr(DNo, "##XXX##"))
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel at least 1 item.");
                return true;
            }
            return false;
        }

        private bool IsDOCtAlreadyProcessed()
        {
            return Sm.IsDataExist(
                "Select A.DocNo "+
                "From TblDOCt2Dtl2 A "+
                "Inner Join TblDOCt2Hdr B On A.DocNo = b.DocNo "+
                "Where A.ProcessInd='F' And A.DocNo=@Param And B.CBDInd='N'  "+
                "Union All  "+
                "Select A.DocNo  "+
                "From TblDOCt2Dtl3 A "+
                "Inner Join TblDOCt2Hdr B On A.Docno = B.DocNo "+
                "Where A.ProcessInd='F' And A.DocNo=@Param And B.CBDInd='N'  ",
                TxtDocNo.Text, "Data already processed."
                );
        }

        private bool IsJournalDataExisted()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select DocNo From TblDOCt2Hdr " +
                    "Where JournalDocNo Is Not Null And DocNo=@DocNo Limit 1;"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            return Sm.IsDataExist(cm);
        }

        private MySqlCommand CancelDOCt2Dtl(string DNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblDOCt2Dtl Set ");
            SQL.AppendLine("    CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And CancelInd='N' ");
            SQL.AppendLine("And Position(Concat('##', DNo, '##') In @DNo)>0; ");

            if (TxtQueueNo.Text.Length>0)
            {
                SQL.AppendLine("Update TblLoadingQueue Set ProcessInd='O' ");
                SQL.AppendLine("Where Docno=@QueueNo And ProcessInd='F';");
            }

            if (TxtDRDocNo.Text.Length > 0)
            {
                SQL.AppendLine("Delete From TblDOCt2Dtl2 Where DocNo=@DocNo; ");

                SQL.AppendLine("Update TblDRHdr Set ");
                SQL.AppendLine("   ProcessInd='O', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
                SQL.AppendLine("Where DocNo=@DRDocNo ");
                SQL.AppendLine("And CancelInd='N' ");
                SQL.AppendLine("And ProcessInd='F' ");
                SQL.AppendLine("And Not Exists( ");
                SQL.AppendLine("    Select 1 ");
                SQL.AppendLine("    From TblDOCt2Hdr A ");
                SQL.AppendLine("    Inner Join TblDOCt2Dtl B On A.DocNo=B.DocNo And B.CancelInd='N' ");
                SQL.AppendLine("    Where A.DRDocNo=@DRDocNo Limit 1 ");
                SQL.AppendLine("); ");
            }

            if (TxtPLDocNo.Text.Length > 0)
            {
                SQL.AppendLine("Delete From TblDOCt2Dtl3 Where DocNo=@DocNo; ");

                SQL.AppendLine("Update TblPLDtl Set ");
                SQL.AppendLine("   ProcessInd='O', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
                SQL.AppendLine("Where DocNo=@PLDocNo And SectionNo=@SectionNo And ProcessInd='F'; ");
            }
            
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DNo", DNo);

            if (TxtDRDocNo.Text.Length > 0)
                Sm.CmParam<String>(ref cm, "@DRDocNo", TxtDRDocNo.Text);

            if (TxtPLDocNo.Text.Length > 0)
            {
                Sm.CmParam<String>(ref cm, "@PLDocNo", TxtPLDocNo.Text);
                Sm.CmParam<String>(ref cm, "@SectionNo", mSectionNo);
            }

            if (TxtQueueNo.Text.Length > 0)
                Sm.CmParam<String>(ref cm, "@QueueNo", TxtQueueNo.Text);

            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal()
        {
            //Non CBD

            var cm = new MySqlCommand();
            string Filter = string.Empty;
            var CurrentDt = Sm.ServerCurrentDate();
            var DocDt = Sm.GetDte(DteDocDt);
            var IsClosingJournalUseCurrentDt = Sm.IsClosingJournalUseCurrentDt(DocDt);

            if (Grd1.Rows.Count >= 1)
            {
                string DNo = string.Empty;
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    DNo = Sm.GetGrdStr(Grd1, Row, 0);
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !Sm.GetGrdBool(Grd1, Row, 2) && DNo.Length > 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(B.DNo=@DNo" + Row.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@DNo" + Row.ToString(), DNo);
                    }
                }
            }

            if (Filter.Length != 0) Filter = " And (" + Filter + ") ";

            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblDOCt2Dtl Set ");
            SQL.AppendLine("    JournalDocNo=@JournalDocNo ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine(Filter.Replace("B.", string.Empty));
            SQL.AppendLine("; ");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, ");
            if (IsClosingJournalUseCurrentDt)
                SQL.AppendLine("Replace(CurDate(), '-', ''), ");
            else
                SQL.AppendLine("DocDt, ");
            SQL.AppendLine("Concat('Cancelling ', JnDesc) As JnDesc, ");
            SQL.AppendLine("MenuCode, MenuDesc, CCCode, Remark, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblJournalHdr ");
            SQL.AppendLine("Where DocNo In (Select JournalDocNo From TblDOCt2Hdr Where DocNo=@DocNo); ");

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, @EntCode, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From (");

            //1
            SQL.AppendLine("        Select E.AcNo, ");
            SQL.AppendLine("        IfNull(C.UPrice, 0)*IfNull(C.Excrate, 0)*B.Qty As DAmt, ");
            SQL.AppendLine("        0.00 As CAmt ");
            SQL.AppendLine("        From TblDOCt2Hdr A ");
            SQL.AppendLine("        Inner join TblDOCt2Dtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
            SQL.AppendLine("        Inner Join TblItem D On B.ItCode=D.ItCode ");
            SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo Is Not Null ");
            SQL.AppendLine("        Where A.DocNo=@DocNo And A.DrDocNo Is Not Null ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select E.AcNo As AcNo, ");
            SQL.AppendLine("        IfNull(C.UPrice, 0)*IfNull(C.Excrate, 0)*B.Qty As DAmt, ");
            SQL.AppendLine("        0.00 As CAmt ");
            SQL.AppendLine("        From TblDOCt2Hdr A ");
            SQL.AppendLine("        Inner join TblDOCt2Dtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
            SQL.AppendLine("        Inner join TblItem D On B.ItCode=D.ItCode ");
            SQL.AppendLine("        Inner join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo Is Not Null ");
            SQL.AppendLine("        Where A.DocNo=@DocNo And A.PLDocNo Is Not Null ");
            SQL.AppendLine("        Union All ");

            //2
            SQL.AppendLine("        Select Concat(@CustomerAcNoNonInvoice, A.CtCode) As AcNo, ");
            SQL.AppendLine("        0.00 As DAmt, ");
            SQL.AppendLine("        Case When C.CurCode=@MainCurCode Then 1.00 Else ");
            SQL.AppendLine("        IfNull(( ");
            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
            SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=C.CurCode And CurCode2=@MainCurCode ");
            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
            SQL.AppendLine("        ), 0) End*C.UPrice* ");
            SQL.AppendLine("        Case C.PriceUomCode ");
            SQL.AppendLine("            When D.InventoryUomCode Then B.Qty ");
            SQL.AppendLine("            When D.InventoryUomCode2 Then B.Qty2 ");
            SQL.AppendLine("            When D.InventoryUomCode3 Then B.Qty3 ");
            SQL.AppendLine("            Else 0 ");
            SQL.AppendLine("        End As CAmt ");
            SQL.AppendLine("        From TblDOCt2Hdr A ");
            SQL.AppendLine("        Inner join TblDOCt2Dtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("        Inner Join  ");
            SQL.AppendLine("        ( ");
            SQL.AppendLine("            Select Distinct A.DocNo, G.ItCode, C.CurCode, F.PriceUomCode, ");
            SQL.AppendLine("            (E.UPrice-(E.UPrice*0.01*IfNull(H.DiscRate, 0))) As UPrice ");
            SQL.AppendLine("            From TblDrHdr A ");
            SQL.AppendLine("            Inner Join TblDrDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("            Inner Join TblSOHdr C On B.SODocNo = C.DocNo  ");
            SQL.AppendLine("            Inner Join TblSODtl D On C.DocNo = D.DocNo ");
            SQL.AppendLine("            Inner Join TblCtQtDtl E On C.CtQTDocno = E.DocNo And D.CtQTDNo = E.DNo ");
            SQL.AppendLine("            Inner Join TblItemPriceHdr F On E.ItemPriceDocNo=F.DocNo ");
            SQL.AppendLine("            Inner Join TblItemPriceDtl G On E.ItemPriceDocNo=G.DocNo And E.ItemPriceDNo=G.DNo ");
            SQL.AppendLine("            Left Join TblSOQuotPromoItem H On C.SOQuotPromoDocNo=H.DocNo And G.ItCode=H.ItCode  ");
            SQL.AppendLine("            Where Exists( ");
            SQL.AppendLine("                Select 1 ");
            SQL.AppendLine("                From TblDOCt2Hdr T1 ");
            SQL.AppendLine("                Inner Join TblDOCt2Dtl2 T2 ");
            SQL.AppendLine("                Where T1.DocNo=T2.DocNo ");
            SQL.AppendLine("                And T1.DRDocNo=B.DocNo ");
            SQL.AppendLine("                And T2.DRDNo=B.DNo ");
            SQL.AppendLine("                And T1.DocNo=@DocNo ");
            SQL.AppendLine("                ) ");
            SQL.AppendLine("        ) C On A.DrDocno=C.DocNo And B.ItCode=C.ItCode ");
            SQL.AppendLine("        Inner join TblItem D On B.ItCode=D.ItCode ");
            SQL.AppendLine("        Where A.DocNo=@DocNo And A.DrDocNo Is Not Null ");

            SQL.AppendLine("        Union All ");

            SQL.AppendLine("        Select Concat(@CustomerAcNoNonInvoice, A.CtCode) As AcNo, ");
            SQL.AppendLine("        0.00 As DAmt, ");
            SQL.AppendLine("        Case When C.CurCode=@MainCurCode Then 1.00 Else ");
            SQL.AppendLine("        IfNull(( ");
            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
            SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=C.CurCode And CurCode2=@MainCurCode ");
            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
            SQL.AppendLine("        ), 0) End*C.UPrice* ");
            SQL.AppendLine("        Case C.PriceUomCode ");
            SQL.AppendLine("            When D.InventoryUomCode Then B.Qty ");
            SQL.AppendLine("            When D.InventoryUomCode2 Then B.Qty2 ");
            SQL.AppendLine("            When D.InventoryUomCode3 Then B.Qty3 ");
            SQL.AppendLine("            Else 0.00 ");
            SQL.AppendLine("        End As CAmt ");
            SQL.AppendLine("        From TblDOCt2Hdr A ");
            SQL.AppendLine("        Inner join TblDOCt2Dtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("        Inner Join  ");
            SQL.AppendLine("        ( ");
            SQL.AppendLine("            Select Distinct A.DocNo, G.ItCode, C.CurCode, F.PriceUomCode, ");
            SQL.AppendLine("            (E.UPrice-(E.UPrice*0.01*IfNull(H.DiscRate, 0))) As UPrice ");
            SQL.AppendLine("            From TblPLHdr A ");
            SQL.AppendLine("            Inner Join TblPLDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("            Inner Join TblSOHdr C On B.SODocNo = C.DocNo  ");
            SQL.AppendLine("            Inner Join TblSODtl D On C.DocNo = D.DocNo ");
            SQL.AppendLine("            Inner Join TblCtQtDtl E On C.CtQTDocno = E.DocNo And D.CtQTDno = E.DNo ");
            SQL.AppendLine("            Inner Join TblItemPriceHdr F On E.ItemPriceDocNo=F.DocNo ");
            SQL.AppendLine("            Inner Join TblItemPriceDtl G On E.ItemPriceDocNo=G.DocNo And E.ItemPriceDNo=G.DNo ");
            SQL.AppendLine("            Left Join TblSOQuotPromoItem H On C.SOQuotPromoDocNo=H.DocNo And G.ItCode=H.ItCode  ");
            SQL.AppendLine("            Where Exists( ");
            SQL.AppendLine("                Select 1 ");
            SQL.AppendLine("                From TblDOCt2Hdr T1 ");
            SQL.AppendLine("                Inner Join TblDOCt2Dtl3 T2 ");
            SQL.AppendLine("                Where T1.DocNo=T2.DocNo ");
            SQL.AppendLine("                And T1.PLDocNo=B.DocNo ");
            SQL.AppendLine("                And T2.PLDNo=B.DNo ");
            SQL.AppendLine("                And T1.DocNo=@DocNo ");
            SQL.AppendLine("                ) ");
            SQL.AppendLine("        ) C On A.PLDocno=C.DocNo And B.ItCode=C.ItCode ");
            SQL.AppendLine("        Inner join TblItem D On B.ItCode=D.ItCode ");
            SQL.AppendLine("        Where A.DocNo=@DocNo And A.PLDocNo Is Not Null ");

            SQL.AppendLine("        Union All ");

            SQL.AppendLine("        Select Concat(@CustomerAcNoNonInvoice, A.CtCode) As AcNo, ");
            SQL.AppendLine("        0.00 As DAmt, ");
            SQL.AppendLine("        Case When C.CurCode=@MainCurCode Then 1.00 Else ");
            SQL.AppendLine("        IfNull(( ");
            SQL.AppendLine("            Select Amt From TblCurrencyRate ");
            SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=C.CurCode And CurCode2=@MainCurCode ");
            SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
            SQL.AppendLine("        ), 0) End*C.UPrice* ");
            SQL.AppendLine("        Case C.PriceUomCode ");
            SQL.AppendLine("            When D.InventoryUomCode Then B.Qty ");
            SQL.AppendLine("            When D.InventoryUomCode2 Then B.Qty2 ");
            SQL.AppendLine("            When D.InventoryUomCode3 Then B.Qty3 ");
            SQL.AppendLine("            Else 0 ");
            SQL.AppendLine("        End As CAmt ");
            SQL.AppendLine("        From TblDOCt2Hdr A ");
            SQL.AppendLine("        Inner join TblDOCt2Dtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("        Inner Join  ");
            SQL.AppendLine("        ( ");
            SQL.AppendLine("            Select Distinct A.DocNo, D.ItCode, C.CurCode, D.PackagingUnitUomCode As PriceUomCode, ");
            SQL.AppendLine("            D.Amt As UPrice ");
            SQL.AppendLine("            From TblDrHdr A ");
            SQL.AppendLine("            Inner Join TblDrDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("            Inner Join TblSOContractHdr C On B.SODocNo = C.DocNo  ");
            SQL.AppendLine("            Inner Join TblSOContractDtl D On C.DocNo = D.DocNo ");
            SQL.AppendLine("            Where Exists( ");
            SQL.AppendLine("                Select 1 ");
            SQL.AppendLine("                From TblDOCt2Hdr T1 ");
            SQL.AppendLine("                Inner Join TblDOCt2Dtl2 T2 ");
            SQL.AppendLine("                Where T1.DocNo=T2.DocNo ");
            SQL.AppendLine("                And T1.DRDocNo=B.DocNo ");
            SQL.AppendLine("                And T2.DRDNo=B.DNo ");
            SQL.AppendLine("                And T1.DocNo=@DocNo ");
            SQL.AppendLine("                ) ");
            SQL.AppendLine("        ) C On A.DrDocno=C.DocNo And B.ItCode=C.ItCode ");
            SQL.AppendLine("        Inner join TblItem D On B.ItCode=D.ItCode ");
            SQL.AppendLine("        Where A.DocNo=@DocNo And A.DrDocNo Is Not Null ");

            SQL.AppendLine("        Union All ");

            //3
            if (mIsAcNoForSaleUseItemCategory)
            {
                SQL.AppendLine("        Select E.AcNo5 As AcNo, 0.00 As DAmt, IfNull(C.UPrice, 0)*IfNull(C.Excrate, 0)*B.Qty As CAmt ");
                SQL.AppendLine("        From TblDOCt2Hdr A ");
                SQL.AppendLine("        Inner join TblDOCt2Dtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine(Filter);
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner join TblItem D On B.ItCode=D.ItCode ");
                SQL.AppendLine("        Inner join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo5 Is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo And A.DrDocNo Is Not Null ");
                SQL.AppendLine("        Union All ");
                SQL.AppendLine("        Select E.AcNo5 As AcNo, 0.00 As DAmt, IfNull(C.UPrice, 0)*IfNull(C.Excrate, 0)*B.Qty As CAmt ");
                SQL.AppendLine("        From TblDOCt2Hdr A ");
                SQL.AppendLine("        Inner join TblDOCt2Dtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine(Filter);
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner join TblItem D On B.ItCode=D.ItCode ");
                SQL.AppendLine("        Inner join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo5 Is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo And A.PLDocNo Is Not Null ");
                SQL.AppendLine("        Union All ");
            }
            else
            {
                SQL.AppendLine("        Select @AcNoForCOGS As AcNo, 0.00 As DAmt, IfNull(C.UPrice, 0)*IfNull(C.Excrate, 0)*B.Qty As CAmt ");
                SQL.AppendLine("        From TblDOCt2Hdr A ");
                SQL.AppendLine("        Inner join TblDOCt2Dtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine(Filter);
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Where A.DocNo=@DocNo And A.DrDocNo Is Not Null ");
                SQL.AppendLine("        Union All ");
                SQL.AppendLine("        Select @AcNoForCOGS As AcNo, 0.00 As DAmt, IfNull(C.UPrice, 0)*IfNull(C.Excrate, 0)*B.Qty As CAmt ");
                SQL.AppendLine("        From TblDOCt2Hdr A ");
                SQL.AppendLine("        Inner join TblDOCt2Dtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine(Filter);
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Where A.DocNo=@DocNo And A.PLDocNo Is Not Null ");
                SQL.AppendLine("        Union All ");
            }

            //4 
            if (mIsAcNoForSaleUseItemCategory)
            {
                SQL.AppendLine("        Select E.AcNo4 As AcNo, ");
                SQL.AppendLine("        Case When C.CurCode=@MainCurCode Then 1.00 Else ");
                SQL.AppendLine("        IfNull(( ");
                SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=C.CurCode And CurCode2=@MainCurCode ");
                SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("        ), 0.00) End*C.UPrice*");
                SQL.AppendLine("        Case C.PriceUomCode ");
                SQL.AppendLine("            When D.InventoryUomCode Then B.Qty ");
                SQL.AppendLine("            When D.InventoryUomCode2 Then B.Qty2 ");
                SQL.AppendLine("            When D.InventoryUomCode3 Then B.Qty3 ");
                SQL.AppendLine("            Else 0.00 ");
                SQL.AppendLine("        End As DAmt, ");
                SQL.AppendLine("        0.00 As CAmt ");
                SQL.AppendLine("        From TblDOCt2Hdr A ");
                SQL.AppendLine("        Inner join TblDOCt2Dtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine(Filter);
                SQL.AppendLine("        Inner Join  ");
                SQL.AppendLine("        ( ");
                SQL.AppendLine("            Select Distinct A.DocNo, G.ItCode, C.CurCode, F.PriceUomCode, ");
                SQL.AppendLine("            (E.UPrice-(E.UPrice*0.01*IfNull(H.DiscRate, 0))) As UPrice ");
                SQL.AppendLine("            From TblDrHdr A ");
                SQL.AppendLine("            Inner Join TblDrDtl B On A.DocNo = B.DocNo ");
                SQL.AppendLine("            Inner Join TblSOHdr C On B.SODocNo = C.DocNo  ");
                SQL.AppendLine("            Inner Join TblSODtl D On C.DocNo = D.DocNo ");
                SQL.AppendLine("            Inner Join TblCtQtDtl E On C.CtQTDocno = E.DocNo And D.CtQTDNo = E.DNo ");
                SQL.AppendLine("            Inner Join TblItemPriceHdr F On E.ItemPriceDocNo=F.DocNo ");
                SQL.AppendLine("            Inner Join TblItemPriceDtl G On E.ItemPriceDocNo=G.DocNo And E.ItemPriceDNo=G.DNo ");
                SQL.AppendLine("            Left Join TblSOQuotPromoItem H On C.SOQuotPromoDocNo=H.DocNo And G.ItCode=H.ItCode  ");
                SQL.AppendLine("            Where Exists( ");
                SQL.AppendLine("                Select 1 ");
                SQL.AppendLine("                From TblDOCt2Hdr T1 ");
                SQL.AppendLine("                Inner Join TblDOCt2Dtl2 T2 ");
                SQL.AppendLine("                Where T1.DocNo=T2.DocNo ");
                SQL.AppendLine("                And T1.DRDocNo=B.DocNo ");
                SQL.AppendLine("                And T2.DRDNo=B.DNo ");
                SQL.AppendLine("                And T1.DocNo=@DocNo ");
                SQL.AppendLine("                ) ");
                SQL.AppendLine("        ) C On A.DrDocno=C.DocNo And B.ItCode=C.ItCode ");
                SQL.AppendLine("        Inner join TblItem D On B.ItCode=D.ItCode ");
                SQL.AppendLine("        Inner join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo4 Is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo And A.DrDocNo Is Not Null ");

                SQL.AppendLine("        Union All ");

                SQL.AppendLine("        Select E.AcNo4 As AcNo, ");
                SQL.AppendLine("        Case When C.CurCode=@MainCurCode Then 1.00 Else ");
                SQL.AppendLine("        IfNull(( ");
                SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=C.CurCode And CurCode2=@MainCurCode ");
                SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("        ), 0.00) End*C.UPrice*");
                SQL.AppendLine("        Case C.PriceUomCode ");
                SQL.AppendLine("            When D.InventoryUomCode Then B.Qty ");
                SQL.AppendLine("            When D.InventoryUomCode2 Then B.Qty2 ");
                SQL.AppendLine("            When D.InventoryUomCode3 Then B.Qty3 ");
                SQL.AppendLine("            Else 0.00 ");
                SQL.AppendLine("        End As DAmt, ");
                SQL.AppendLine("        0.00 As CAmt ");
                SQL.AppendLine("        From TblDOCt2Hdr A ");
                SQL.AppendLine("        Inner join TblDOCt2Dtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine(Filter);
                SQL.AppendLine("        Inner Join  ");
                SQL.AppendLine("        ( ");
                SQL.AppendLine("            Select Distinct A.DocNo, G.ItCode, C.CurCode, F.PriceUomCode, ");
                SQL.AppendLine("            (E.UPrice-(E.UPrice*0.01*IfNull(H.DiscRate, 0))) As UPrice ");
                SQL.AppendLine("            From TblPLHdr A ");
                SQL.AppendLine("            Inner Join TblPLDtl B On A.DocNo = B.DocNo ");
                SQL.AppendLine("            Inner Join TblSOHdr C On B.SODocNo = C.DocNo  ");
                SQL.AppendLine("            Inner Join TblSODtl D On C.DocNo = D.DocNo ");
                SQL.AppendLine("            Inner Join TblCtQtDtl E On C.CtQTDocno = E.DocNo And D.CtQTDno = E.DNo ");
                SQL.AppendLine("            Inner Join TblItemPriceHdr F On E.ItemPriceDocNo=F.DocNo ");
                SQL.AppendLine("            Inner Join TblItemPriceDtl G On E.ItemPriceDocNo=G.DocNo And E.ItemPriceDNo=G.DNo ");
                SQL.AppendLine("            Left Join TblSOQuotPromoItem H On C.SOQuotPromoDocNo=H.DocNo And G.ItCode=H.ItCode  ");
                SQL.AppendLine("            Where Exists( ");
                SQL.AppendLine("                Select 1 ");
                SQL.AppendLine("                From TblDOCt2Hdr T1 ");
                SQL.AppendLine("                Inner Join TblDOCt2Dtl3 T2 ");
                SQL.AppendLine("                Where T1.DocNo=T2.DocNo ");
                SQL.AppendLine("                And T1.PLDocNo=B.DocNo ");
                SQL.AppendLine("                And T2.PLDNo=B.DNo ");
                SQL.AppendLine("                And T1.DocNo=@DocNo ");
                SQL.AppendLine("                ) ");
                SQL.AppendLine("        ) C On A.PLDocno=C.DocNo And B.ItCode=C.ItCode ");
                SQL.AppendLine("        Inner join TblItem D On B.ItCode=D.ItCode ");
                SQL.AppendLine("        Inner join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo4 Is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo And A.PLDocNo Is Not Null ");

                SQL.AppendLine("        Union All ");

                SQL.AppendLine("        Select E.AcNo4 As AcNo, ");
                SQL.AppendLine("        Case When C.CurCode=@MainCurCode Then 1.00 Else ");
                SQL.AppendLine("        IfNull(( ");
                SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=C.CurCode And CurCode2=@MainCurCode ");
                SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("        ), 0.00) End*C.UPrice*");
                SQL.AppendLine("        Case C.PriceUomCode ");
                SQL.AppendLine("            When D.InventoryUomCode Then B.Qty ");
                SQL.AppendLine("            When D.InventoryUomCode2 Then B.Qty2 ");
                SQL.AppendLine("            When D.InventoryUomCode3 Then B.Qty3 ");
                SQL.AppendLine("            Else 0.00 ");
                SQL.AppendLine("        End As DAmt, ");
                SQL.AppendLine("        0.00 As CAmt ");
                SQL.AppendLine("        From TblDOCt2Hdr A ");
                SQL.AppendLine("        Inner join TblDOCt2Dtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine(Filter);
                SQL.AppendLine("        Inner Join  ");
                SQL.AppendLine("        ( ");
                SQL.AppendLine("            Select Distinct A.DocNo, D.ItCode, C.CurCode, D.PackagingUnitUomCode As PriceUomCode, ");
                SQL.AppendLine("            D.Amt As UPrice ");
                SQL.AppendLine("            From TblDrHdr A ");
                SQL.AppendLine("            Inner Join TblDrDtl B On A.DocNo = B.DocNo ");
                SQL.AppendLine("            Inner Join TblSOContractHdr C On B.SODocNo = C.DocNo  ");
                SQL.AppendLine("            Inner Join TblSOContractDtl D On C.DocNo = D.DocNo ");
                SQL.AppendLine("            Where Exists( ");
                SQL.AppendLine("                Select 1 ");
                SQL.AppendLine("                From TblDOCt2Hdr T1 ");
                SQL.AppendLine("                Inner Join TblDOCt2Dtl2 T2 ");
                SQL.AppendLine("                Where T1.DocNo=T2.DocNo ");
                SQL.AppendLine("                And T1.DRDocNo=B.DocNo ");
                SQL.AppendLine("                And T2.DRDNo=B.DNo ");
                SQL.AppendLine("                And T1.DocNo=@DocNo ");
                SQL.AppendLine("                ) ");
                SQL.AppendLine("        ) C On A.DrDocno=C.DocNo And B.ItCode=C.ItCode ");
                SQL.AppendLine("        Inner join TblItem D On B.ItCode=D.ItCode ");
                SQL.AppendLine("        Inner join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo4 Is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo And A.DrDocNo Is Not Null ");
            }
            else
            {
                SQL.AppendLine("        Select @AcNoForSaleOfFinishedGoods As AcNo, ");
                SQL.AppendLine("        Case When C.CurCode=@MainCurCode Then 1.00 Else ");
                SQL.AppendLine("        IfNull(( ");
                SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=C.CurCode And CurCode2=@MainCurCode ");
                SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("        ), 0.00) End*C.UPrice*");
                SQL.AppendLine("        Case C.PriceUomCode ");
                SQL.AppendLine("            When D.InventoryUomCode Then B.Qty ");
                SQL.AppendLine("            When D.InventoryUomCode2 Then B.Qty2 ");
                SQL.AppendLine("            When D.InventoryUomCode3 Then B.Qty3 ");
                SQL.AppendLine("            Else 0.00 ");
                SQL.AppendLine("        End As DAmt, ");
                SQL.AppendLine("        0.00 As CAmt ");
                SQL.AppendLine("        From TblDOCt2Hdr A ");
                SQL.AppendLine("        Inner join TblDOCt2Dtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine(Filter);
                SQL.AppendLine("        Inner Join  ");
                SQL.AppendLine("        ( ");
                SQL.AppendLine("            Select Distinct A.DocNo, G.ItCode, C.CurCode, F.PriceUomCode, ");
                SQL.AppendLine("            (E.UPrice-(E.UPrice*0.01*IfNull(H.DiscRate, 0))) As UPrice ");
                SQL.AppendLine("            From TblDrHdr A ");
                SQL.AppendLine("            Inner Join TblDrDtl B On A.DocNo = B.DocNo ");
                SQL.AppendLine("            Inner Join TblSOHdr C On B.SODocNo = C.DocNo  ");
                SQL.AppendLine("            Inner Join TblSODtl D On C.DocNo = D.DocNo ");
                SQL.AppendLine("            Inner Join TblCtQtDtl E On C.CtQTDocno = E.DocNo And D.CtQTDNo = E.DNo ");
                SQL.AppendLine("            Inner Join TblItemPriceHdr F On E.ItemPriceDocNo=F.DocNo ");
                SQL.AppendLine("            Inner Join TblItemPriceDtl G On E.ItemPriceDocNo=G.DocNo And E.ItemPriceDNo=G.DNo ");
                SQL.AppendLine("            Left Join TblSOQuotPromoItem H On C.SOQuotPromoDocNo=H.DocNo And G.ItCode=H.ItCode  ");
                SQL.AppendLine("            Where Exists( ");
                SQL.AppendLine("                Select 1 ");
                SQL.AppendLine("                From TblDOCt2Hdr T1 ");
                SQL.AppendLine("                Inner Join TblDOCt2Dtl2 T2 ");
                SQL.AppendLine("                Where T1.DocNo=T2.DocNo ");
                SQL.AppendLine("                And T1.DRDocNo=B.DocNo ");
                SQL.AppendLine("                And T2.DRDNo=B.DNo ");
                SQL.AppendLine("                And T1.DocNo=@DocNo ");
                SQL.AppendLine("                ) ");
                SQL.AppendLine("        ) C On A.DrDocno=C.DocNo And B.ItCode=C.ItCode ");
                SQL.AppendLine("        Inner join TblItem D On B.ItCode=D.ItCode ");
                SQL.AppendLine("        Where A.DocNo=@DocNo And A.DrDocNo Is Not Null ");

                SQL.AppendLine("        Union All ");

                SQL.AppendLine("        Select @AcNoForSaleOfFinishedGoods As AcNo, ");
                SQL.AppendLine("        Case When C.CurCode=@MainCurCode Then 1.00 Else ");
                SQL.AppendLine("        IfNull(( ");
                SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=C.CurCode And CurCode2=@MainCurCode ");
                SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("        ), 0.00) End*C.UPrice*");
                SQL.AppendLine("        Case C.PriceUomCode ");
                SQL.AppendLine("            When D.InventoryUomCode Then B.Qty ");
                SQL.AppendLine("            When D.InventoryUomCode2 Then B.Qty2 ");
                SQL.AppendLine("            When D.InventoryUomCode3 Then B.Qty3 ");
                SQL.AppendLine("            Else 0.00 ");
                SQL.AppendLine("        End As DAmt, ");
                SQL.AppendLine("        0.00 As CAmt ");
                SQL.AppendLine("        From TblDOCt2Hdr A ");
                SQL.AppendLine("        Inner join TblDOCt2Dtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine(Filter);
                SQL.AppendLine("        Inner Join  ");
                SQL.AppendLine("        ( ");
                SQL.AppendLine("            Select Distinct A.DocNo, G.ItCode, C.CurCode, F.PriceUomCode, ");
                SQL.AppendLine("            (E.UPrice-(E.UPrice*0.01*IfNull(H.DiscRate, 0))) As UPrice ");
                SQL.AppendLine("            From TblPLHdr A ");
                SQL.AppendLine("            Inner Join TblPLDtl B On A.DocNo = B.DocNo ");
                SQL.AppendLine("            Inner Join TblSOHdr C On B.SODocNo = C.DocNo  ");
                SQL.AppendLine("            Inner Join TblSODtl D On C.DocNo = D.DocNo ");
                SQL.AppendLine("            Inner Join TblCtQtDtl E On C.CtQTDocno = E.DocNo And D.CtQTDno = E.DNo ");
                SQL.AppendLine("            Inner Join TblItemPriceHdr F On E.ItemPriceDocNo=F.DocNo ");
                SQL.AppendLine("            Inner Join TblItemPriceDtl G On E.ItemPriceDocNo=G.DocNo And E.ItemPriceDNo=G.DNo ");
                SQL.AppendLine("            Left Join TblSOQuotPromoItem H On C.SOQuotPromoDocNo=H.DocNo And G.ItCode=H.ItCode  ");
                SQL.AppendLine("            Where Exists( ");
                SQL.AppendLine("                Select 1 ");
                SQL.AppendLine("                From TblDOCt2Hdr T1 ");
                SQL.AppendLine("                Inner Join TblDOCt2Dtl3 T2 ");
                SQL.AppendLine("                Where T1.DocNo=T2.DocNo ");
                SQL.AppendLine("                And T1.PLDocNo=B.DocNo ");
                SQL.AppendLine("                And T2.PLDNo=B.DNo ");
                SQL.AppendLine("                And T1.DocNo=@DocNo ");
                SQL.AppendLine("                ) ");
                SQL.AppendLine("        ) C On A.PLDocno=C.DocNo And B.ItCode=C.ItCode ");
                SQL.AppendLine("        Inner join TblItem D On B.ItCode=D.ItCode ");
                SQL.AppendLine("        Where A.DocNo=@DocNo And A.PLDocNo Is Not Null ");

                SQL.AppendLine("        Union All ");

                SQL.AppendLine("        Select @AcNoForSaleOfFinishedGoods As AcNo, ");
                SQL.AppendLine("        Case When C.CurCode=@MainCurCode Then 1.00 Else ");
                SQL.AppendLine("        IfNull(( ");
                SQL.AppendLine("            Select Amt From TblCurrencyRate ");
                SQL.AppendLine("            Where RateDt<=A.DocDt And CurCode1=C.CurCode And CurCode2=@MainCurCode ");
                SQL.AppendLine("            Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("        ), 0.00) End*C.UPrice*");
                SQL.AppendLine("        Case C.PriceUomCode ");
                SQL.AppendLine("            When D.InventoryUomCode Then B.Qty ");
                SQL.AppendLine("            When D.InventoryUomCode2 Then B.Qty2 ");
                SQL.AppendLine("            When D.InventoryUomCode3 Then B.Qty3 ");
                SQL.AppendLine("            Else 0.00 ");
                SQL.AppendLine("        End As DAmt, ");
                SQL.AppendLine("        0.00 As CAmt ");
                SQL.AppendLine("        From TblDOCt2Hdr A ");
                SQL.AppendLine("        Inner join TblDOCt2Dtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine(Filter);
                SQL.AppendLine("        Inner Join  ");
                SQL.AppendLine("        ( ");
                SQL.AppendLine("            Select Distinct A.DocNo, D.ItCode, C.CurCode, D.PackagingUnitUomCode As PriceUomCode, ");
                SQL.AppendLine("            D.Amt As UPrice ");
                SQL.AppendLine("            From TblDrHdr A ");
                SQL.AppendLine("            Inner Join TblDrDtl B On A.DocNo = B.DocNo ");
                SQL.AppendLine("            Inner Join TblSOContractHdr C On B.SODocNo = C.DocNo  ");
                SQL.AppendLine("            Inner Join TblSOContractDtl D On C.DocNo = D.DocNo ");
                SQL.AppendLine("            Where Exists( ");
                SQL.AppendLine("                Select 1 ");
                SQL.AppendLine("                From TblDOCt2Hdr T1 ");
                SQL.AppendLine("                Inner Join TblDOCt2Dtl2 T2 ");
                SQL.AppendLine("                Where T1.DocNo=T2.DocNo ");
                SQL.AppendLine("                And T1.DRDocNo=B.DocNo ");
                SQL.AppendLine("                And T2.DRDNo=B.DNo ");
                SQL.AppendLine("                And T1.DocNo=@DocNo ");
                SQL.AppendLine("                ) ");
                SQL.AppendLine("        ) C On A.DrDocno=C.DocNo And B.ItCode=C.ItCode ");
                SQL.AppendLine("        Inner join TblItem D On B.ItCode=D.ItCode ");
                SQL.AppendLine("        Where A.DocNo=@DocNo And A.DrDocNo Is Not Null ");
            }

            SQL.AppendLine("    ) Tbl Where AcNo is Not Null Group By AcNo ");
            SQL.AppendLine(") B On 0=0 ");
            SQL.AppendLine("Where A.DocNo=@JournalDocNo;");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (IsClosingJournalUseCurrentDt)
            {
                if (mDocNoFormat == "1")
                    Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(CurrentDt, 1));
                else
                    Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNo3(CurrentDt, "Journal", "TblJournalHdr", mEntCode, "1"));
            }
            else
            {
                if(mDocNoFormat == "1")
                    Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(DocDt, 1));
                else
                    Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNo3(DocDt, "Journal", "TblJournalHdr", mEntCode, "1"));
            }
            Sm.CmParam<String>(ref cm, "@MainCurCode", mMainCurCode);
            Sm.CmParam<String>(ref cm, "@CustomerAcNoNonInvoice", mCustomerAcNoNonInvoice);
            Sm.CmParam<String>(ref cm, "@AcNoForCOGS", mAcNoForCOGS);
            Sm.CmParam<String>(ref cm, "@AcNoForSaleOfFinishedGoods", mAcNoForSaleOfFinishedGoods);
            Sm.CmParam<String>(ref cm, "@EntCode", mEntCode);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal2()
        {
            // CBD
            var SQL = new StringBuilder();
            var CurrentDt = Sm.ServerCurrentDate();
            var DocDt = Sm.GetDte(DteDocDt);
            var IsClosingJournalUseCurrentDt = Sm.IsClosingJournalUseCurrentDt(DocDt);

            SQL.AppendLine("Update TblDOCt2Dtl Set ");
            SQL.AppendLine("    JournalDocNo=@JournalDocNo ");
            SQL.AppendLine("Where DocNo=@DocNo; ");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, ");
            if (IsClosingJournalUseCurrentDt)
                SQL.AppendLine("Replace(CurDate(), '-', ''), ");
            else
                SQL.AppendLine("DocDt, ");
            SQL.AppendLine("Concat('Cancelling ', JnDesc) As JnDesc, ");
            SQL.AppendLine("MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt ");
            SQL.AppendLine("From TblJournalHdr Where DocNo In (Select JournalDocNo From TblDOCt2Hdr Where DocNo=@DocNo);");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, DNo, AcNo, CAMt As DAmt, DAmt As CAmt, @EntCode, CreateBy, CreateDt ");
            SQL.AppendLine("From TblJournalDtl Where DocNo In (Select JournalDocNo From TblDOCt2Hdr Where DocNo=@DocNo);");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (IsClosingJournalUseCurrentDt)
            {
                if (mDocNoFormat == "1")
                    Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(CurrentDt, 1));
                else
                    Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNo3(CurrentDt, "Journal", "TblJournalHdr", mEntCode, "1"));
            }
            else
            {
                if (mDocNoFormat == "1")
                    Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(DocDt, 1));
                else
                    Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNo3(DocDt, "Journal", "TblJournalHdr", mEntCode, "1"));
            }
            Sm.CmParam<String>(ref cm, "@EntCode", mEntCode);

            return cm;
        }
        #endregion
        private MySqlCommand UpdateDOCt2File(string DocNo, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblDOCt2hdr Set ");
            SQL.AppendLine("    FileName=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo  ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);

            return cm;
        }

        private MySqlCommand UpdateDOCt2File2(string DocNo, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblDOCt2hdr Set ");
            SQL.AppendLine("    FileName2=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo  ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);

            return cm;
        }

        private MySqlCommand UpdateDOCt2File3(string DocNo, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblDOCt2hdr Set ");
            SQL.AppendLine("    FileName3=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo  ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);

            return cm;
        }
        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowDOCt2Hdr(DocNo);
                ShowSOInfo();
                ShowItemInfo();
                ShowDOCt2Dtl(DocNo);
                ComputeItemDOQty();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowDOCt2Hdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.WhsCode, A.DRDocNo, A.PLDocNo, ");
            SQL.AppendLine("A.CtCode, B.CtName, A.SectionNo, A.Cnt, A.Seal, ");
            SQL.AppendLine("A.LocalDocNo, A.ExpDriver, A.ExpPlatNo, A.QueueNo, A.ResiNo, A.Remark, ");
            SQL.AppendLine("C.SAName, D.VdName, ");
            SQL.AppendLine("A.EmpCode1, E.EmpName As EmpName1, A.EmpCode2, F.EmpName As EmpName2, A.EmpCode3, G.EmpName As EmpName3, A.EmpCode4, H.EmpName As EmpName4, ");
            SQL.AppendLine("KBContractNo, KBContractDt, KBPLNo, KBPLDt, KBRegistrationNo, KBRegistrationDt, ");
            SQL.AppendLine("A.KBSubmissionNo, A.InspectionSheetNo, A.CustomsDocCode, A.FileName, A.FileName2, A.FileName3 ");
            SQL.AppendLine("From TblDOCt2Hdr A ");
            SQL.AppendLine("Inner Join TblCustomer B On A.CtCode=B.CtCode ");
            SQL.AppendLine("Left Join TblDRHdr C On A.DRDocNo=C.DocNo ");
            SQL.AppendLine("Left Join TblVendor D On C.ExpVdCode=D.VdCode ");
            SQL.AppendLine("Left Join TblEmployee E On A.EmpCode1=E.EmpCode ");
            SQL.AppendLine("Left Join TblEmployee F On A.EmpCode2=F.EmpCode ");
            SQL.AppendLine("Left Join TblEmployee G On A.EmpCode3=G.EmpCode ");
            SQL.AppendLine("Left Join TblEmployee H On A.EmpCode4=H.EmpCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DocNo", 

                    //1-5
                    "DocDt", "WhsCode", "CtCode", "CtName", "DRDocNo",  
                    
                    //6-10
                    "PLDocNo", "SectionNo", "Cnt", "Seal", "LocalDocNo", 
                    
                    //11-15
                    "SAName", "VdName", "ExpDriver", "ExpPlatNo", "QueueNo", 
                    
                    //16-20
                    "ResiNo", "Remark", "EmpCode1", "EmpName1", "EmpCode2", 
                    
                    //21-25
                    "EmpName2", "EmpCode3", "EmpName3", "EmpCode4", "EmpName4",

                    //26-30
                    "KBContractNo", "KBContractDt", "KBPLNo", "KBPLDt", "KBRegistrationNo", 
                    
                    //31-34
                    "KBRegistrationDt", "KBSubmissionNo", "CustomsDocCode", "InspectionSheetNo", "FileName",

                    //36-37
                    "FileName2", "FileName3"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                    Sl.SetLueWhsCode(ref LueWhsCode, Sm.DrStr(dr, c[2]));
                    mCtCode = Sm.DrStr(dr, c[3]);
                    TxtCtCode.EditValue = Sm.DrStr(dr, c[4]);
                    TxtDRDocNo.EditValue = Sm.DrStr(dr, c[5]);
                    TxtPLDocNo.EditValue = Sm.DrStr(dr, c[6]);
                    mSectionNo = Sm.DrStr(dr, c[7]);
                    TxtCnt.EditValue = Sm.DrStr(dr, c[8]);
                    TxtSeal.EditValue = Sm.DrStr(dr, c[9]);
                    TxtLocalDocNo.EditValue = Sm.DrStr(dr, c[10]);
                    TxtSAName.EditValue = Sm.DrStr(dr, c[11]);
                    TxtExpVdCode.EditValue = Sm.DrStr(dr, c[12]);
                    TxtExpDriver.EditValue = Sm.DrStr(dr, c[13]);
                    TxtExpPlatNo.EditValue = Sm.DrStr(dr, c[14]);
                    TxtQueueNo.EditValue = Sm.DrStr(dr, c[15]);
                    TxtResiNo.EditValue = Sm.DrStr(dr, c[16]);
                    MeeRemark.EditValue = Sm.DrStr(dr, c[17]);
                    mEmpCode1 = Sm.DrStr(dr, c[18]);
                    TxtEmpCode1.EditValue = Sm.DrStr(dr, c[19]);
                    mEmpCode2 = Sm.DrStr(dr, c[20]);
                    TxtEmpCode2.EditValue = Sm.DrStr(dr, c[21]);
                    mEmpCode3 = Sm.DrStr(dr, c[22]);
                    TxtEmpCode3.EditValue = Sm.DrStr(dr, c[23]);
                    mEmpCode4 = Sm.DrStr(dr, c[24]);
                    TxtEmpCode4.EditValue = Sm.DrStr(dr, c[25]);
                    TxtKBContractNo.EditValue = Sm.DrStr(dr, c[26]);
                    Sm.SetDte(DteKBContractDt, Sm.DrStr(dr, c[27]));
                    TxtKBPLNo.EditValue = Sm.DrStr(dr, c[28]);
                    Sm.SetDte(DteKBPLDt, Sm.DrStr(dr, c[29]));
                    TxtKBRegistrationNo.EditValue = Sm.DrStr(dr, c[30]);
                    Sm.SetDte(DteKBRegistrationDt, Sm.DrStr(dr, c[31]));
                    TxtKBSubmissionNo.EditValue = Sm.DrStr(dr, c[32]);
                    mCustomsDocCode = Sm.DrStr(dr, c[33]);
                    TxtInspectionSheet.EditValue = Sm.DrStr(dr, c[34]);
                    TxtFile.EditValue = Sm.DrStr(dr, c[35]);
                    TxtFile2.EditValue = Sm.DrStr(dr, c[36]);
                    TxtFile3.EditValue = Sm.DrStr(dr, c[37]);
                }, true
            );
        }

        private void ShowDOCt2Dtl(string DocNo)
        {
            var SQL = new StringBuilder();

            bool IsService = Sm.IsDataExist("Select 1 From TblDOCt2Dtl A Inner Join TblItem B On A.ItCode = B.ItCode Where A.DocNo = @Param And B.ServiceItemInd = 'Y' Limit 1; ", DocNo);

            SQL.AppendLine("Select B.DNo, B.CancelInd, B.ItCode, C.ItCodeInternal, C.ItName, B.PropCode, D.PropName, B.BatchNo, B.Source, B.Lot, B.Bin, ");
            SQL.AppendLine("IfNull(If(E1.Qty < E2.Qty, E1.Qty, E2.Qty), 0) + Case When B.CancelInd='N' Then B.Qty Else 0 End As Stock, B.Qty, C.InventoryUomCode, ");
            SQL.AppendLine("IfNull(If(E1.Qty < E2.Qty, E1.Qty, E2.Qty), 0) + Case When B.CancelInd='N' Then B.Qty2 Else 0 End As Stock2,  B.Qty2, C.InventoryUomCode2, ");
            SQL.AppendLine("IfNull(If(E1.Qty < E2.Qty, E1.Qty, E2.Qty), 0) + Case When B.CancelInd='N' Then B.Qty3 Else 0 End As Stock3,  B.Qty3, C.InventoryUomCode3, B.Remark, ");
            SQL.AppendLine("C.ItGrpCode, B.Notes, C.ServiceItemInd, B.SOContractDocNo, B.SOContractDNo, F.No, H.Qty QtyDR ");
            SQL.AppendLine("From TblDOCt2Hdr A ");
            SQL.AppendLine("Inner Join TblDOCt2Dtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblItem C On B.ItCode=C.ItCode ");
            SQL.AppendLine("Left Join TblProperty D On B.PropCode=D.PropCode ");
            //if (!IsService)
            //{
                SQL.AppendLine("Left Join TblStockSummary E1 ");
                SQL.AppendLine("    On A.WhsCode=E1.WhsCode ");
                SQL.AppendLine("    And B.Lot=E1.Lot ");
                SQL.AppendLine("    And B.Bin=E1.Bin ");
                //SQL.AppendLine("    And B.ItCode=E.ItCode ");
                //SQL.AppendLine("    And B.PropCode=E.PropCode ");
                //SQL.AppendLine("    And B.BatchNo=E.BatchNo ");
                SQL.AppendLine("    And B.Source=E1.Source ");
            //}
            //else
            //{
                SQL.AppendLine("Left Join ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select T2.DocNo, T2.DNo, Sum(T1.Qty - IfNull(T2.Qty, 0.00)) Qty, Sum(T1.Qty - IfNull(T2.Qty, 0.00)) Qty2, Sum(T1.Qty - IfNull(T2.Qty, 0.00)) Qty3 ");
                SQL.AppendLine("    From TblDRDtl T1 ");
                SQL.AppendLine("    Left Join TblDOCt2Dtl T2 On T1.SODocNo = T2.SOContractDocNo And T1.SODNo = T2.SOContractDNo ");
                SQL.AppendLine("        And T2.CancelInd = 'N' ");
                SQL.AppendLine("    Group By T2.DocNo, T2.DNo ");
                SQL.AppendLine(") E2 On B.DocNo = E2.DocNo And B.DNo = E2.DNo ");
            //}
            SQL.AppendLine("Left Join TblSOContractDtl F On B.SOContractDocNo = F.DocNo And B.SOContractDNo = F.DNo ");
            SQL.AppendLine("Inner Join TblDRHdr G On A.DRDocNo=G.DocNo ");
            SQL.AppendLine("Inner Join TblDRDtl H On A.DRDocNo=H.DocNo And H.SODocNo = B.SOContractDocNo And H.SODNo = B.SOContractDNo  ");
            
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Order By B.DNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 

                    //1-5
                    "CancelInd", "ItCode", "ItCodeInternal", "ItName", "PropCode",   
                    
                    //6-10
                    "PropName", "BatchNo", "Source", "Lot", "Bin",   
                    
                    //11-15
                    "QtyDR", "Qty", "InventoryUomCode", "QtyDR", "Qty2", 
                    
                    //16-20
                    "InventoryUomCode2", "QtyDR", "Qty3", "InventoryUomCode3", "Remark",

                    //21-25
                    "ItGrpCode", "Notes", "ServiceItemInd", "SOContractDocNo", "SOContractDNo",

                    //26
                    "No"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 12);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 13);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 14);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 15);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 16);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 17);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 18);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 19);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 20);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 21);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 22);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 23);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 27, 24);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 28, 25);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 29, 26);
                }, false, false, true, false
            );
            Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 1, 2 });
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 14, 15, 17, 18, 20, 21 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Additional Method

        internal string GetSOContractDocNo()
        {
            string SOContractDocNo = string.Empty;

            for (int i = 0; i < Grd2.Rows.Count - 1; ++i)
            {
                if (Sm.GetGrdStr(Grd2, i, 0).Length > 0)
                {
                    if (SOContractDocNo.Length > 0) SOContractDocNo += ",";
                    SOContractDocNo += Sm.GetGrdStr(Grd2, i, 1);
                }
            }

            return SOContractDocNo;
        }

        private bool IsItemService()
        {
            return Sm.GetGrdStr(Grd1, 0, 26) == "Y";
        }

        private void CheckServiceItemInd()
        {
            string ItCodes = string.Empty;
            for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
            {
                if (ItCodes.Length > 0) ItCodes += ",";
                ItCodes += Sm.GetGrdStr(Grd1, i, 4);
            }

            if (ItCodes.Length > 0)
            {
                var SQL = new StringBuilder();
                var lIS = new List<ItemService>();

                SQL.AppendLine("Select ItCode, ServiceItemInd ");
                SQL.AppendLine("From TblItem ");
                SQL.AppendLine("Where Find_In_Set(ItCode, @ItCodes); ");

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    var cm = new MySqlCommand();
                    cm.Connection = cn;
                    Sm.CmParam<String>(ref cm, "@ItCodes", ItCodes);
                    cm.CommandText = SQL.ToString();
                    cm.CommandTimeout = 600;
                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] { "ItCode", "ServiceItemInd" });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            lIS.Add(new ItemService()
                            {
                                ItCode = Sm.DrStr(dr, c[0]),
                                ServiceItemInd = Sm.DrStr(dr, c[1])
                            });
                        }
                    }
                    dr.Close();
                }

                if (lIS.Count > 0)
                {
                    Grd1.BeginUpdate();
                    for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
                    {
                        foreach (var x in lIS.Where(w => w.ItCode == Sm.GetGrdStr(Grd1, i, 4)))
                        {
                            Grd1.Cells[i, 26].Value = x.ServiceItemInd;
                        }
                    }
                    Grd1.EndUpdate();
                }

                lIS.Clear();
            }
        }

        private bool IsAuthorizedToAccessPEB()
        {
            bool mFlag = false;
            string mCurrentGrpCode = Sm.GetValue("Select GrpCode From TblUser Where UserCode = @Param; ", Gv.CurrentUserCode);

            string[] mPEBGrpCodeList = mPEBGrpCode.Split(',');
            foreach (string m in mPEBGrpCodeList)
            {
                if (m == mCurrentGrpCode)
                {
                    mFlag = true;
                    break;
                }
            }

            return mFlag;
        }

        private bool CheckCreditLimit()
        {
            if (mIsCreditLimitValidate)
            {
                decimal DOSI, SIIP, IPVC, RIDO, CL, Outs = 0;
                decimal DONow= 0;
                string CTQT = string.Empty;
                string mDRDocType = string.Empty;

                //nentuin customer code
                string CtCode = string.Empty;
                if (TxtDRDocNo.Text.Length > 0)
                {
                    CtCode = Sm.GetValue("Select CtCode From TblDRHdr Where Docno = '" + TxtDRDocNo.Text + "' ");
                    mDRDocType = Sm.GetValue("Select DocType From TblDRHdr Where DocNo = @Param;", TxtDRDocNo.Text);
                }
                else
                {
                    CtCode = Sm.GetValue("Select C.CtCode From TblPLHdr A "+
                    "Inner Join TblSIHdr B On A.SIDocno = B.DocNo "+
                    "Inner Join TblSP C On B.SPDocNo = C.DocNo "+
                    "Where A.DocNo= '"+TxtPLDocNo.Text+"' ");
                }

                if (mDRDocType == "2")
                {
                    var mCLSQL = new StringBuilder();
                    var mSODocNo = new StringBuilder();
                    string mSODocNo2 = string.Empty;

                    mCLSQL.AppendLine("Select Sum(C.CreditLimit) ");
                    mCLSQL.AppendLine("From ");
                    mCLSQL.AppendLine("( ");
                    mCLSQL.AppendLine("    Select Distinct T2.SODocNo ");
                    mCLSQL.AppendLine("    From TblDRHdr T1 ");
                    mCLSQL.AppendLine("    Inner Join TblDRDtl T2 On T1.DocNo = T2.DocNo And T1.Doctype = '2' ");
                    mCLSQL.AppendLine("        And T1.DocNo = @Param ");
                    mCLSQL.AppendLine(") A ");
                    mCLSQL.AppendLine("Inner Join TblSOContractHdr B On A.SODOcNo = B.DocNo ");
                    mCLSQL.AppendLine("Inner Join TblBOQHdr C On B.BOQDocNo = C.DocNo ");
                    mCLSQL.AppendLine("Where A.DocNo = @Param; ");

                    mSODocNo.AppendLine("Select Distinct SODocNo ");
                    mSODocNo.AppendLine("From TblDRDtl ");
                    mSODocNo.AppendLine("Where DocNo = @Param; ");                    

                    //nentuin nilai credit limit
                    CL = Decimal.Parse(Sm.GetValue(mCLSQL.ToString(), TxtDRDocNo.Text));

                    //nentuin customer quotation yang aktif
                    CTQT = Sm.GetValue(mSODocNo.ToString(), TxtDRDocNo.Text);


                    //nentuin amount DO yang sdg dibuat
                    for (int Row = 0; Row < Grd2.Rows.Count; Row++)
                    {
                        DONow += (Sm.GetGrdDec(Grd2, Row, 16) * Sm.GetGrdDec(Grd2, Row, 12));
                    }

                    DOSI = GetDOSI2(CTQT);
                    SIIP = GetSIIP(CtCode);
                    IPVC = GetIPVC(CtCode);
                    RIDO = GetRIDO2(CtCode, CTQT);
                    Outs = DOSI + SIIP + IPVC + DONow - RIDO;
                }
                else
                {
                    //nentuin nilai credit limit
                    CL = Decimal.Parse(Sm.GetValue("Select CreditLimit From TblCtQthdr Where CtCode='" + CtCode + "' And Status = 'A' And ActInd = 'Y' "));

                    //nentuin customer quotation yang aktif
                    CTQT = Sm.GetValue("Select DocNo From TblCtQthdr Where CtCode='" + CtCode + "' And Status = 'A' And ActInd = 'Y' ");


                    //nentuin amount DO yang sdg dibuat
                    for (int Row = 0; Row < Grd2.Rows.Count; Row++)
                    {
                        DONow += (Sm.GetGrdDec(Grd2, Row, 16) * Sm.GetGrdDec(Grd2, Row, 12));
                    }

                    DOSI = GetDOSI(CtCode);
                    SIIP = GetSIIP(CtCode);
                    IPVC = GetIPVC(CtCode);
                    RIDO = GetRIDO(CtCode, CTQT);
                    Outs = DOSI + SIIP + IPVC + DONow - RIDO;
                }

                if (CL < Outs)
                {
                    Sm.StdMsg(
                    mMsgType.Warning,
                    "Credit Limit : " + Sm.FormatNum(CL, 0) + Environment.NewLine +
                    "Outstanding : " + Sm.FormatNum(Outs, 0) + Environment.NewLine +
                    "Balance : " + Sm.FormatNum(CL - Outs, 0) + Environment.NewLine +
                    "Total amount should not be greater than Credit Limit."
                    );
                    return true;
                }
            }
            return false;
        }

        private decimal GetIPVC(string CtCode)
        {
            var cm = new MySqlCommand
            {
                CommandText =
                    "Select ifnull(SUM(A.Amt), 0) As AmtIPVC From TblIncomingpaymentHdr A "+
                    "Inner Join TblVoucherRequestHdr B On A.VoucherRequestDocNo = B.DocNo "+
                    "Where A.cancelInd = 'N' And A.CtCode = @CtCode And "+
                    "B.DocNo Not In ( "+
                    "    Select A.VoucherRequestDocNo "+ 
                    "    From TblVoucherHdr A "+
                    "    Inner Join TblVoucherRequestHdr B On A.VoucherRequestDocno = B.Docno "+
                    "    Where A.CancelInd = 'N'); "
            };
            Sm.CmParam<String>(ref cm, "@CtCode", CtCode);
            return Sm.GetValueDec(cm);
        }

        private decimal GetSIIP(string CtCode)
        {
            var cm = new MySqlCommand
            {
                CommandText =
                    "Select ifnull(Sum(Amt), 0) As AmtSIIP from tblsalesinvoicehdr t where ctcode = @CtCode and cancelind='N' and "+ 
                    "not exists( "+
                    "    select a.docno "+
                    "    from tblincomingpaymenthdr a, tblincomingpaymentdtl b "+
                    "    where a.docno=b.docno "+
                    "    and status<>'C'  "+
                    "    and cancelind='N' "+
                    "    and b.InvoiceDocNo=t.docno and b.InvoiceType='1' "+
                    "); "
            };
            Sm.CmParam<String>(ref cm, "@CtCode", CtCode);
            return Sm.GetValueDec(cm);
        }

        private decimal GetDOSI(string CtCode)
        {
            var cm = new MySqlCommand
            {
                CommandText =
                    "Select IFNULL(SUM(T.Qty*T.PriceAfterTax), 0) AmtDOSI "+
                    "From ( "+
                    "   Select '1' As DocType, "+
                    "   If(C.QtyInventory=0, 0, (B.Qty/C.QtyInventory)*C.Qty) As Qty, "+
                    "   (G.UPrice-(G.UPrice*0.01*IfNull(J.DiscRate, 0))) +  "+
                    "   ((G.UPrice-(G.UPrice*0.01*IfNull(J.DiscRate, 0)))*0.01*E.TaxRate) As PriceAfterTax "+
                    "   From TblDOCt2Hdr A "+
                    "   Inner Join TblDOCt2Dtl2 B On A.DocNo=B.DocNo And B.Qty>0 And B.ProcessInd='O' "+ 
                    "   Inner Join TblDRDtl C On A.DRDocNo=C.DocNo And B.DRDNo=C.DNo "+
                    "   Inner Join TblSOHdr D On C.SODocNo=D.DocNo And D.CancelInd = 'N'  "+
                    "   Inner Join TblSODtl E On C.SODocNo=E.DocNo And C.SODNo=E.DNo "+
                    "   Inner Join TblCtQtHdr F On D.CtQtDocNo=F.DocNo   "+
                    "   Inner Join TblCtQtDtl G On D.CtQtDocNo=G.DocNo And E.CtQtDNo=G.DNo "+
                    "   Inner Join TblItemPriceHdr H On G.ItemPriceDocNo=H.DocNo  "+
                    "   Inner Join TblItemPriceDtl I On G.ItemPriceDocNo=I.DocNo And G.ItemPriceDNo=I.DNo "+
                    "   Left Join TblSOQuotPromoItem J On D.SOQuotPromoDocNo=J.DocNo And I.ItCode=J.ItCode  "+ 
                    "   Inner Join TblDRHdr M On A.DRDocNo=M.DocNo "+
                    "   Where A.CtCode=@CtCode "+
                    "   Union All "+
                    "   Select '2' As DocType, "+  
                    "   If(C.QtyInventory=0, 0, (B.Qty/C.QtyInventory)*C.Qty) As Qty, "+ 
                    "   (G.UPrice-(G.UPrice*0.01*IfNull(J.DiscRate, 0))) + "+
                    "   ((G.UPrice-(G.UPrice*0.01*IfNull(J.DiscRate, 0)))*0.01*E.TaxRate) As PriceAfterTax "+
                    "   From TblDOCt2Hdr A  "+
                    "   Inner Join TblDOCt2Dtl3 B On A.DocNo=B.DocNo And B.Qty>0 And B.ProcessInd='O' "+ 
                    "   Inner Join TblPLDtl C On A.PLDocNo=C.DocNo And B.PLDNo=C.DNo  "+
                    "   Inner Join TblSOHdr D On C.SODocNo=D.DocNo And D.CancelInd = 'N'  " + 
                    "   inner Join TblSODtl E On C.SODocNo=E.DocNo And C.SODNo=E.DNo "+
                    "   Inner Join TblCtQtHdr F On D.CtQtDocNo=F.DocNo "+
                    "   Inner Join TblCtQtDtl G On D.CtQtDocNo=G.DocNo And E.CtQtDNo=G.DNo "+ 
                    "   Inner Join TblItemPriceHdr H On G.ItemPriceDocNo=H.DocNo  "+
                    "   Inner Join TblItemPriceDtl I On G.ItemPriceDocNo=I.DocNo And G.ItemPriceDNo=I.DNo "+ 
                    "   Left Join TblSOQuotPromoItem J On D.SOQuotPromoDocNo=J.DocNo And I.ItCode=J.ItCode "+
                    "   Where A.CtCode=@CtCode " +
                    ") T ",

            };
            Sm.CmParam<String>(ref cm, "@CtCode", CtCode);
            return Sm.GetValueDec(cm);
        }

        private decimal GetDOSI2(string SODocNo)
        {
            var cm = new MySqlCommand
            {
                CommandText =
                    "Select IFNULL(SUM(T.Qty*T.PriceAfterTax), 0) AmtDOSI " +
                    "From ( " +
                    "   Select '1' As DocType, " +
                    "   If(C.QtyInventory=0, 0, (B.Qty/C.QtyInventory)*C.Qty) As Qty, " +
                    "   E.Amt As PriceAfterTax " +
                    "   From TblDOCt2Hdr A " +
                    "   Inner Join TblDOCt2Dtl2 B On A.DocNo=B.DocNo And B.Qty>0 And B.ProcessInd='O' " +
                    "   Inner Join TblDRDtl C On A.DRDocNo=C.DocNo And B.DRDNo=C.DNo " +
                    "   Inner Join TblSOContractHdr D On C.SODocNo=D.DocNo And D.CancelInd = 'N' And D.DocNo = @SODocNo " +
                    "   Inner Join TblSOContractDtl E On C.SODocNo=E.DocNo And C.SODNo=E.DNo " +
                    "   Inner Join TblDRHdr M On A.DRDocNo=M.DocNo " +
                    ") T ",
            };
            Sm.CmParam<String>(ref cm, "@SODocNo", SODocNo);
            return Sm.GetValueDec(cm);
        }

        private decimal GetRIDO(string CtCode, string CtQtDocNo)
        {
            var cm = new MySqlCommand
            {
                CommandText =
                    "Select ifnull(SUM(T.Amt), 0) As RIDOAMT From ( "+
                    "   Select B.ItCode, (A.Qty* C.UPrice) As AMt   "+
                    "   from TblRecvCtDtl A  "+
                    "   Inner Join TblDoCt2Dtl B On A.DOCtDocNo = B.DocNo And A.DOCtDno = B.Dno   "+
                    "   Inner Join (  "+
	                "       Select A.DocNo, B.DNo, C.UPrice  "+
	                "       From TblDOCt2Hdr A  "+
	                "       Inner Join TblDOCt2Dtl B On A.DocNo=B.DocNo And B.CancelInd='N'  "+
	                "       Left Join  "+
	                "       (  "+
		            "           Select Distinct A.DocNo, G.ItCode, C.CurCode,  "+
		            "           (E.UPrice-(E.UPrice*0.01*IfNull(H.DiscRate, 0))) As UPrice  "+
		            "           From TblDrhdr A  "+
		            "           Inner Join TblDrDtl B On A.DocNo = B.DocNo  "+
		            "           Inner Join TblSOHdr C On B.SODocNo = C.DocNo And C.cancelInd = 'N'  "+
		            "           Inner Join TblSODtl D On C.DocNo = D.DocNo  "+
		            "           Inner Join TblCtQtDtl E On C.CtQTDocno = E.DocNo And D.CtQTDno = E.DNo  "+
		            "           Inner Join TblItemPriceHdr F On E.ItemPriceDocNo=F.DocNo  "+
		            "           Inner Join TblItemPriceDtl G On E.ItemPriceDocNo=G.DocNo And E.ItemPriceDNo=G.DNo  "+
		            "           Left Join TblSOQuotPromoItem H On C.SOQuotPromoDocNo=H.DocNo And G.ItCode=H.ItCode  "+
		            "           Inner Join TblItem I On G.ItCode=I.ItCode  "+
		            "           Where A.cancelInd = 'N' And A.CtCode = @CtCode And E.DocNo = @CtQtDocNo  "+
	                "        ) C On A.DrDocno = C.DocNo And B.ItCode=C.ItCode  "+
                    "   )C On B.DocNo = C.DocNo And B.Dno = C.Dno  "+
                    ")T "

            };
            Sm.CmParam<String>(ref cm, "@CtCode", CtCode);
            Sm.CmParam<String>(ref cm, "@CtQtDocNo", CtQtDocNo);
            return Sm.GetValueDec(cm);
        }

        private decimal GetRIDO2(string CtCode, string SODocNo)
        {
            var cm = new MySqlCommand
            {
                CommandText =
                    "Select ifnull(SUM(T.Amt), 0) As RIDOAMT From ( " +
                    "   Select B.ItCode, (A.Qty* C.UPrice) As AMt   " +
                    "   from TblRecvCtDtl A  " +
                    "   Inner Join TblDoCt2Dtl B On A.DOCtDocNo = B.DocNo And A.DOCtDno = B.Dno   " +
                    "   Inner Join (  " +
                    "       Select A.DocNo, B.DNo, C.UPrice  " +
                    "       From TblDOCt2Hdr A  " +
                    "       Inner Join TblDOCt2Dtl B On A.DocNo=B.DocNo And B.CancelInd='N'  " +
                    "       Left Join  " +
                    "       (  " +
                    "           Select Distinct A.DocNo, D.ItCode, C.CurCode,  " +
                    "           D.Amt As UPrice  " +
                    "           From TblDrhdr A  " +
                    "           Inner Join TblDrDtl B On A.DocNo = B.DocNo  " +
                    "           Inner Join TblSOContractHdr C On B.SODocNo = C.DocNo And C.cancelInd = 'N' And C.DocNo = @SODocNo  " +
                    "           Inner Join TblSOContractDtl D On C.DocNo = D.DocNo  " +
                    "           Where A.cancelInd = 'N' And A.CtCode = @CtCode " +
                    "        ) C On A.DrDocno = C.DocNo And B.ItCode=C.ItCode  " +
                    "   )C On B.DocNo = C.DocNo And B.Dno = C.Dno  " +
                    ")T "

            };
            Sm.CmParam<String>(ref cm, "@CtCode", CtCode);
            Sm.CmParam<String>(ref cm, "@SODocNo", SODocNo);
            return Sm.GetValueDec(cm);
        }

        private void GetParameter()
        {
            mIsDOCtShowQueueNo = Sm.GetParameterBoo("IsDOCtShowQueueNo");
            SetNumberOfInventoryUomCode();
            mIsItGrpCodeShow = Sm.GetParameterBoo("IsItGrpCodeShow");
            mIsCreditLimitValidate = Sm.GetParameterBoo("IsCreditLimitValidate");
            mIsAutoJournalActived = Sm.GetParameterBoo("IsAutoJournalActived");
            mMainCurCode = Sm.GetParameter("MainCurCode");
            mCustomerAcNoNonInvoice = Sm.GetParameter("CustomerAcNoNonInvoice");
            mAcNoForCOGS = Sm.GetParameter("AcNoForCOGS");
            mAcNoForSaleOfFinishedGoods = Sm.GetParameter("AcNoForSaleOfFinishedGoods");
            mIsShowLocalDocNo = Sm.GetParameterBoo("IsShowLocalDocNo");
            mIsDOCtCopyLocalDocNoFromDRPL = Sm.GetParameterBoo("IsDOCtCopyLocalDocNoFromDRPL");
            mIsRemarkForJournalMandatory = Sm.GetParameterBoo("IsRemarkForJournalMandatory");
            mIsAcNoForSaleUseItemCategory = Sm.GetParameterBoo("IsAcNoForSaleUseItemCategory");
            mIsDOCtContainerSealAllowEditable = Sm.GetParameterBoo("IsDOCtContainerSealAllowEditable");
            mIsKawasanBerikatEnabled = Sm.GetParameter("KB_Server").Length > 0;
            mPEBGrpCode = Sm.GetParameter("PEBGrpCode");
            mPEBFilePath = Sm.GetParameter("PEBFilePath");
            mPEBPassword = Sm.GetParameter("PEBPassword");
            mPEBDocType = Sm.GetParameter("PEBDocType");
            mIsBOMShowSpecifications = Sm.GetParameterBoo("IsBOMShowSpecifications");
            mIsDOCt2JournalDisabled = Sm.GetParameterBoo("IsDOCt2JournalDisabled");
            if (mPEBDocType.Length <= 0) mPEBDocType = "30";
            mDocNoFormat = Sm.GetParameter("DocNoFormat");
            mIsInspectionSheetMandatory = Sm.GetParameterBoo("IsInspectionSheetMandatory");
            mHostAddrForFTPClient = Sm.GetParameter("HostAddrForFTPClient");
            mSharedFolderForFTPClient = Sm.GetParameter("SharedFolderForFTPClient");
            mUsernameForFTPClient = Sm.GetParameter("UsernameForFTPClient");
            mPasswordForFTPClient = Sm.GetParameter("PasswordForFTPClient");
            mIsDOCDRAllowToUploadFile = Sm.GetParameterBoo("IsDOCDRAllowToUploadFile");
            mPortForFTPClient = Sm.GetParameter("PortForFTPClient");
            mFileSizeMaxUploadFTPClient = Sm.GetParameter("FileSizeMaxUploadFTPClient");
            mIsCustomerItemNameMandatory = Sm.GetParameterBoo("IsCustomerItemNameMandatory");
            mIsSalesTransactionShowSOContractRemark = Sm.GetParameterBoo("IsSalesTransactionShowSOContractRemark");
            mIsSalesTransactionUseItemNotes = Sm.GetParameterBoo("IsSalesTransactionUseItemNotes");
            mIsSO2NotUsePackagingLabel = Sm.GetParameterBoo("IsSO2NotUsePackagingLabel");
            mIsBinDOCt2NotShow = Sm.GetParameterBoo("IsBinDOCt2NotShow");
            mIsBatchNoDOCt2NotShow = Sm.GetParameterBoo("IsBatchNoDOCt2NotShow");
            mIsRemarkHdrEditableOvertime = Sm.GetParameterBoo("IsRemarkHdrEditableOvertime");
            mIsDetailShowColumnNumber = Sm.GetParameterBoo("IsDetailShowColumnNumber");
        }

        private bool IsDataExists(string SQL, string Param, string Warning)
        {
            var cm = new MySqlCommand() { CommandText = SQL };
            Sm.CmParam<String>(ref cm, "@Param", Param);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, Warning);
                return true;
            }
            return false;
        }

        private void SetLuePropertyCode(ref DXE.LookUpEdit Lue, string ItCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.PropCode As Col1, B.PropName As Col2 ");
            SQL.AppendLine("From TblItemProperty A ");
            SQL.AppendLine("Inner Join TblProperty B On A.PropCode = B.PropCode ");
            SQL.AppendLine("Where ItCode = '" + ItCode + "'  ");

            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void SetNumberOfInventoryUomCode()
        {
            string NumberOfInventoryUomCode = Sm.GetParameter("NumberOfInventoryUomCode");
            if (NumberOfInventoryUomCode.Length == 0)
                mNumberOfInventoryUomCode = 1;
            else
                mNumberOfInventoryUomCode = int.Parse(NumberOfInventoryUomCode);
        }

        internal void ShowSOInfo()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            if (TxtDRDocNo.Text.Length > 0)
            {
                SQL.AppendLine("Select T.* FROM (   ");
                SQL.AppendLine("Select A.DNo, A.SODocNo, A.SODNo, F.AgtName,  ");
                SQL.AppendLine("E.ItCode, G.ItCodeInternal, G.ItName,  ");
                SQL.AppendLine("A.QtyPackagingUnit, C.PackagingUnitUomCode, A.QtyInventory, G.InventoryUomCode, G.ItGrpCode, ");
                SQL.AppendLine("(D.UPrice-(D.UPrice*0.01)) + ((D.UPrice-(D.UPrice*0.01))*0.01*C.TaxRate) As PriceAfterTax,  ");
                SQL.AppendLine("Null As ProjectCode, Null As ProjectName, Null As PONo ");
                SQL.AppendLine(", IFNULL(K.CtItCode , ' ') CtItCode, IFNULL(K.CtItName , ' ') CtItName, C.Remark SOContractRemark, Null as SOContractNo   ");
                SQL.AppendLine("From TblDRDtl A  ");
                SQL.AppendLine("Inner Join TblSOHdr B On A.SODocNo=B.DocNo  ");
                SQL.AppendLine("Inner Join TblSODtl C On A.SODocNo=C.DocNo And A.SODNo=C.DNo  ");
                SQL.AppendLine("Inner Join TblCtQtDtl D On B.CtQtDocNo=D.DocNo And C.CtQtDNo=D.DNo  ");
                SQL.AppendLine("Inner Join TblItemPriceDtl E On D.ItemPriceDocNo=E.DocNo And D.ItemPriceDNo=E.DNo  ");
                SQL.AppendLine("Left Join TblAgent F On C.AgtCode=F.AgtCode  ");
                SQL.AppendLine("Inner Join TblItem G On E.ItCode=G.ItCode  ");
                SQL.AppendLine("LEFT JOIN tblcustomeritem K ON G.ItCode=K.ItCode ");
                SQL.AppendLine("Where A.DocNo=@DocNo  ");
                SQL.AppendLine("Union All  ");
                SQL.AppendLine("Select A.DNo, A.SODocNo, A.SODNo, F.AgtName,  ");
                SQL.AppendLine("C.ItCode, G.ItCodeInternal, G.ItName,  ");
                SQL.AppendLine("A.QtyPackagingUnit, C.PackagingUnitUomCode, A.QtyInventory, G.InventoryUomCode, G.ItGrpCode,  ");
                SQL.AppendLine("C.Amt As PriceAfterTax,  ");
                SQL.AppendLine("J.ProjectCode, J.ProjectName, B.PONo ");
                SQL.AppendLine(", IFNULL(K.CtItCode , ' ') CtItCode, IFNULL(K.CtItName , ' ') CtItName, C.Remark SOContractRemark, C.No SOContractNo  ");
                SQL.AppendLine("From TblDRDtl A  ");
                SQL.AppendLine("Inner Join TblSOContractHdr B On A.SODocNo=B.DocNo  ");
                SQL.AppendLine("Inner Join TblSOContractDtl C On A.SODocNo=C.DocNo And A.SODNo=C.DNo  ");
                SQL.AppendLine("Left Join TblAgent F On C.AgtCode=F.AgtCode  ");
                SQL.AppendLine("Inner Join TblItem G On C.ItCode=G.ItCode  ");
                SQL.AppendLine("Left Join TblBOQHdr H ON B.BOQDocNo = H.DocNo  ");
                SQL.AppendLine("Left Join TblLOPHdr I On H.LOPDocNo = I.DocNo  ");
                SQL.AppendLine("Left join TblProjectGroup J On I.PGCode = J.PGCode  ");
                SQL.AppendLine("LEFT JOIN tblcustomeritem K ON G.ItCode=K.ItCode ");
                SQL.AppendLine("Where A.DocNo=@DocNo  ");
                SQL.AppendLine(")T  ");
                SQL.AppendLine("Order By T.ItCode, T.DNo; ");

                Sm.CmParam<String>(ref cm, "@DocNo", TxtDRDocNo.Text);
            }

            if (TxtPLDocNo.Text.Length > 0)
            {
                SQL.AppendLine("Select A.DNo, A.SODocNo, A.SODNo, D.AgtName, ");
                SQL.AppendLine("A.ItCode, E.ItCodeInternal, E.ItName, ");
                SQL.AppendLine("A.QtyPackagingUnit, C.PackagingUnitUomCode, A.QtyInventory, E.InventoryUomCode, E.ItGrpCode, ");
                SQL.AppendLine("(F.UPrice-(F.UPrice*0.01)) + ((F.UPrice-(F.UPrice*0.01))*0.01*C.TaxRate) As PriceAfterTax, ");
                SQL.AppendLine("Null As ProjectCode, Null As ProjectName, Null As PONo ");
                SQL.AppendLine(", IFNULL(K.CtItCode , ' ') CtItCode, IFNULL(K.CtItName , ' ') CtItName, ");
                SQL.AppendLine("Null as SOContractRemark, Null as SOContractNo ");
                SQL.AppendLine("From TblPLDtl A ");
                SQL.AppendLine("Inner Join TblSOHdr B On A.SODocNo=B.DocNo ");
                SQL.AppendLine("Inner Join TblSODtl C On A.SODocNo=C.DocNo And A.SODNo=C.DNo ");
                SQL.AppendLine("Left Join TblAgent D On C.AgtCode=D.AgtCode ");
                SQL.AppendLine("Inner Join TblItem E On A.ItCode=E.ItCode ");
                SQL.AppendLine("Inner Join TblCtQtDtl F On B.CtQtDocNo=F.DocNo And C.CtQtDNo=F.DNo ");
                SQL.AppendLine("LEFT JOIN tblcustomeritem K ON E.ItCode=K.ItCode ");
                //SQL.AppendLine("Left Join TblSOContractHdr L On B.SODocNo=L.DocNo  ");
                //SQL.AppendLine("Left Join TblSOContractDtl M On L.DocNo=M.DocNo  ");
                SQL.AppendLine("Where A.DocNo=@DocNo And A.SectionNo=@SectionNo ");
                SQL.AppendLine("Order By A.ItCode, A.DNo;");

                Sm.CmParam<String>(ref cm, "@SectionNo", mSectionNo);
                Sm.CmParam<String>(ref cm, "@DocNo", TxtPLDocNo.Text);
            }

            Sm.ShowDataInGrid(
                ref Grd2, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 

                    //1-5
                    "SODocNo", "SODNo", "AgtName", "ItCode", "ItCodeInternal", 
                    
                    //6-10
                    "ItName", "QtyPackagingUnit", "PackagingUnitUomCode", "QtyInventory", "InventoryUomCode",

                    //11-15
                    "ItGrpCode", "PriceAfterTax", "ProjectCode", "ProjectName", "PONo",

                    //16-19
                    "CtItCode", "CtItName", "SOContractRemark", "SOContractNo"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 9);
                    Grd.Cells[Row, 12].Value = 0m;
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 10);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 12);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 13);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 14);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 15);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 16);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 17);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 18);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 19);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 9, 11, 12, 13, 16 });
            Sm.FocusGrd(Grd2, 0, 1);
        }

        internal void ShowItemInfo()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            if (TxtDRDocNo.Text.Length > 0)
            {
                SQL.AppendLine("Select T.* From ( ");
                SQL.AppendLine("Select E.ItCode, F.ItCodeInternal, F.ItName, ");
                SQL.AppendLine("Sum(A.QtyInventory) As QtyInventory, F.InventoryUomCode, F.ItGrpCode ");
                SQL.AppendLine("From TblDRDtl A ");
                SQL.AppendLine("Inner Join TblSOHdr B On A.SODocNo=B.DocNo ");
                SQL.AppendLine("Inner Join TblSODtl C On A.SODocNo=C.DocNo And A.SODNo=C.DNo ");
                SQL.AppendLine("Inner Join TblCtQtDtl D On B.CtQtDocNo=D.DocNo And C.CtQtDNo=D.DNo ");
                SQL.AppendLine("Inner Join TblItemPriceDtl E On D.ItemPriceDocNo=E.DocNo And D.ItemPriceDNo=E.DNo "); 
                SQL.AppendLine("Inner Join TblItem F On E.ItCode=F.ItCode ");
                SQL.AppendLine("Where A.DocNo=@DocNo ");
                SQL.AppendLine("Group By E.ItCode, F.ItCodeInternal, F.ItName, F.InventoryUomCode ");
                SQL.AppendLine("Union All ");
                SQL.AppendLine("Select C.ItCode, F.ItCodeInternal, F.ItName, ");
                SQL.AppendLine("Sum(A.QtyInventory) As QtyInventory, F.InventoryUomCode, F.ItGrpCode ");
                SQL.AppendLine("From TblDRDtl A ");
                SQL.AppendLine("Inner Join TblSOContractHdr B On A.SODocNo=B.DocNo ");
                SQL.AppendLine("Inner Join TblSOContractDtl C On A.SODocNo=C.DocNo And A.SODNo=C.DNo ");
                SQL.AppendLine("Inner Join TblItem F On C.ItCode=F.ItCode ");
                SQL.AppendLine("Where A.DocNo=@DocNo ");
                SQL.AppendLine("Group By C.ItCode, F.ItCodeInternal, F.ItName, F.InventoryUomCode ");
                SQL.AppendLine(") T ");
                SQL.AppendLine("Order By T.ItCode;");

                Sm.CmParam<String>(ref cm, "@DocNo", TxtDRDocNo.Text);
            }
            else
            {
                SQL.AppendLine("Select A.ItCode, B.ItCodeInternal, B.ItName, ");
                SQL.AppendLine("Sum(A.QtyInventory) As QtyInventory, B.InventoryUomCode, B.ItGrpCode ");
                SQL.AppendLine("From TblPLDtl A ");
                SQL.AppendLine("Inner Join TblItem B On A.ItCode=B.ItCode ");
                SQL.AppendLine("Where A.DocNo=@DocNo And A.SectionNo=@SectionNo ");
                SQL.AppendLine("Group By A.ItCode, B.ItCodeInternal, B.ItName, B.InventoryUomCode ");
                SQL.AppendLine("Order By A.ItCode;");

                Sm.CmParam<String>(ref cm, "@SectionNo", mSectionNo);
                Sm.CmParam<String>(ref cm, "@DocNo", TxtPLDocNo.Text);
            }
            Sm.ShowDataInGrid(
                ref Grd3, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "ItCode", 
                    
                    //1-5
                    "ItCodeInternal", "ItName", "QtyInventory", "InventoryUomCode", "ItGrpCode"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                    Grd.Cells[Row, 5].Value = 0m;
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);

                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd3, Grd3.Rows.Count - 1, new int[] { 4, 5, 6 });
            Sm.FocusGrd(Grd3, 0, 1);
        }

        private void ShowShippingAddressInfo()
        {
            StringBuilder 
                SQL = new StringBuilder(),
                Msg = new StringBuilder();

            SQL.AppendLine("Select A.SAName, A.SAAddress, B.CityName, C.CntName, A.SAPostalCd, A.SAPhone, A.SAFax, A.SAEmail, A.SAMobile ");
            SQL.AppendLine("From TblDRHdr A ");
            SQL.AppendLine("Left Join TblCity B On A.SACityCode=B.CityCode ");
            SQL.AppendLine("Left Join TblCountry C On A.SACntCode=C.CntCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDRDocNo.Text);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "SAName", 
                        
                        //1-5
                        "SAAddress", "CityName", "CntName", "SAPostalCd", "SAPhone", 
                        
                        //6-8
                        "SAFax", "SAEmail", "SAMobile" 
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        Msg.Append("Shipping Name : ");
                        Msg.AppendLine(Sm.DrStr(dr, c[0]));
                        Msg.Append("Address : ");
                        Msg.AppendLine(Sm.DrStr(dr, c[1]));
                        Msg.Append("City : ");
                        Msg.AppendLine(Sm.DrStr(dr, c[2]));
                        Msg.Append("Country : ");
                        Msg.AppendLine(Sm.DrStr(dr, c[3]));
                        Msg.Append("Postal Code : ");
                        Msg.AppendLine(Sm.DrStr(dr, c[4]));
                        Msg.Append("Phone : ");
                        Msg.AppendLine(Sm.DrStr(dr, c[5]));
                        Msg.Append("Fax : ");
                        Msg.AppendLine(Sm.DrStr(dr, c[6]));
                        Msg.Append("Email : ");
                        Msg.AppendLine(Sm.DrStr(dr, c[7]));
                        Msg.Append("Mobile : ");
                        Msg.AppendLine(Sm.DrStr(dr, c[8]));
                    }, false
                );

            Sm.StdMsg(mMsgType.Info, Msg.ToString());
        }

        private void ShowExpeditionInfo()
        {
            StringBuilder 
                SQL = new StringBuilder(),
                Msg = new StringBuilder();

            SQL.AppendLine("Select B.VdName, D.TTName, A.ExpDriver, A.ExpPlatNo, A.ExpMobile, C.CityName ");
            SQL.AppendLine("From TblDRHdr A ");
            SQL.AppendLine("Left Join TblVendor B On A.ExpVdCode=B.VdCode ");
            SQL.AppendLine("Left Join TblCity C On A.ExpCityCode=C.CityCode ");
            SQL.AppendLine("Left Join TblTransportType D On A.ExpTTCode=D.TTCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDRDocNo.Text);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "VdName", 
                        
                        //1-5
                        "TTName", "ExpDriver", "ExpPlatNo", "ExpMobile", "CityName"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        Msg.Append("Expedition Name : ");
                        Msg.AppendLine(Sm.DrStr(dr, c[0]));
                        Msg.Append("Transport : ");
                        Msg.AppendLine(Sm.DrStr(dr, c[1]));
                        Msg.Append("Driver : ");
                        Msg.AppendLine(Sm.DrStr(dr, c[2]));
                        Msg.Append("Vehicle Plat# : ");
                        Msg.AppendLine(Sm.DrStr(dr, c[3]));
                        Msg.Append("Mobile : ");
                        Msg.AppendLine(Sm.DrStr(dr, c[4]));
                        Msg.Append("City : ");
                        Msg.AppendLine(Sm.DrStr(dr, c[5]));
                    }, false
                );

            Sm.StdMsg(mMsgType.Info, Msg.ToString());
        }

        internal string GetSelectedItem()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 4).Length != 0)
                        SQL +=
                            "##" +
                            Sm.GetGrdStr(Grd1, Row, 4) +
                            Sm.GetGrdStr(Grd1, Row, 8) +
                            Sm.GetGrdStr(Grd1, Row, 10) +
                            Sm.GetGrdStr(Grd1, Row, 11) +
                            Sm.GetGrdStr(Grd1, Row, 12) +
                            Sm.GetGrdStr(Grd1, Row, 13) +
                            "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        internal string GetSelectedItem2()
        {
            var SQL = string.Empty;

            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; ++Row)
                {
                    if(Sm.GetGrdStr(Grd1, Row, 4).Length > 0)
                    {
                        SQL += Sm.GetGrdStr(Grd1, Row, 27) + "," + Sm.GetGrdStr(Grd1, Row, 28);
                    }
                }
            }
            return (SQL.Length == 0 ? "XXX" : SQL);
        }

        private void ComputeItemDOQty()
        {
            decimal Qty = 0m;
            string ItCode = string.Empty;
            for (int Row = 0; Row < Grd3.Rows.Count-1; Row++)
            {
                if (Sm.GetGrdStr(Grd3, Row, 0).Length != 0)
                {
                    Qty = 0m;
                    ItCode = Sm.GetGrdStr(Grd3, Row, 0);
                    for (int Row2 = 0; Row2 < Grd1.Rows.Count-1; Row2++)
                    {
                        if (
                            !Sm.GetGrdBool(Grd1, Row2, 1) &&
                            Sm.GetGrdStr(Grd1, Row2, 4).Length != 0 &&
                            Sm.CompareStr(ItCode, Sm.GetGrdStr(Grd1, Row2, 4))
                            )
                            Qty += Sm.GetGrdDec(Grd1, Row2, 15);
                    }
                    Grd3.Cells[Row, 5].Value = Qty;
                    Grd3.Cells[Row, 6].Value = Sm.GetGrdDec(Grd3, Row, 4) - Qty;
                }
            }
            ComputeSODOQty();
        }

        private void ComputeSODOQty()
        {
            decimal QtyA = 0m, Qty2A = 0m;
            string ItCode = string.Empty;
            for (int Row = 0; Row < Grd3.Rows.Count-1; Row++)
            {
                if (Sm.GetGrdStr(Grd3, Row, 0).Length != 0)
                {
                    QtyA = Sm.GetGrdDec(Grd3, Row, 5);
                    ItCode = Sm.GetGrdStr(Grd3, Row, 0);
                    for (int Row2 = 0; Row2 < Grd2.Rows.Count-1; Row2++)
                    {
                        if (
                            Sm.GetGrdStr(Grd2, Row2, 5).Length != 0 &&
                            Sm.CompareStr(ItCode, Sm.GetGrdStr(Grd2, Row2, 5))
                            )
                        {
                            Qty2A = Sm.GetGrdDec(Grd2, Row2, 11);
                            if (Qty2A >= QtyA)
                            {
                                Grd2.Cells[Row2, 12].Value = QtyA;
                                Grd2.Cells[Row2, 13].Value = Qty2A-QtyA;
                                QtyA = 0;
                            }
                            else
                            {
                                Grd2.Cells[Row2, 12].Value = Qty2A;
                                Grd2.Cells[Row2, 13].Value = 0;
                                QtyA -= Qty2A;
                            }
                        }
                    }
                }
            }
        }

        internal void ComputeItemDOQty(string ItCode)
        {
            decimal Qty = 0m;
            for (int Row = 0; Row < Grd3.Rows.Count-1; Row++)
            {
                if (
                    Sm.GetGrdStr(Grd3, Row, 0).Length != 0 &&
                    Sm.CompareStr(ItCode, Sm.GetGrdStr(Grd3, Row, 0))
                    )
                {
                    for (int Row2 = 0; Row2 < Grd1.Rows.Count-1; Row2++)
                    {
                        if (
                            !Sm.GetGrdBool(Grd1, Row2, 1) &&
                            Sm.GetGrdStr(Grd1, Row2, 4).Length != 0 &&
                            Sm.CompareStr(ItCode, Sm.GetGrdStr(Grd1, Row2, 4))
                            )
                            Qty += Sm.GetGrdDec(Grd1, Row2, 15);
                    }
                    Grd3.Cells[Row, 5].Value = Qty;
                    Grd3.Cells[Row, 6].Value = Sm.GetGrdDec(Grd3, Row, 4) - Qty;
                    break;
                }
            }
            ComputeSODOQty(ItCode);
        }

        private void ComputeSODOQty(string ItCode)
        {
            decimal Qty = 0m, Qty2 = 0m;
            for (int Row = 0; Row < Grd3.Rows.Count-1; Row++)
            {
                if (
                    Sm.GetGrdStr(Grd3, Row, 0).Length != 0 &&
                    Sm.CompareStr(ItCode, Sm.GetGrdStr(Grd3, Row, 0))
                    )
                {
                    Qty = Sm.GetGrdDec(Grd3, Row, 5);
                    for (int Row2 = 0; Row2 < Grd2.Rows.Count-1; Row2++)
                    {
                        if (
                            Sm.GetGrdStr(Grd2, Row2, 5).Length != 0 &&
                            Sm.CompareStr(ItCode, Sm.GetGrdStr(Grd2, Row2, 5))
                            )
                        {
                            Qty2 = Sm.GetGrdDec(Grd2, Row2, 11);
                            if (Qty2 >= Qty)
                            {
                                Grd2.Cells[Row2, 12].Value = Qty;
                                Grd2.Cells[Row2, 13].Value = Qty2 - Qty;
                                Qty = 0;
                            }
                            else
                            {
                                Grd2.Cells[Row2, 12].Value = Qty2;
                                Grd2.Cells[Row2, 13].Value = 0;
                                Qty -= Qty2;
                            }
                        }
                    }
                    break;
                }
            }
        }

        private void ReComputeSOStock()
        {
            string DocNoDNo = string.Empty;

            for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
            {
                if (DocNoDNo.Length > 0) DocNoDNo += ",";
                DocNoDNo += string.Concat(Sm.GetGrdStr(Grd1, i, 27), Sm.GetGrdStr(Grd1, i, 28));
            }

            if (DocNoDNo.Length > 0)
            {
                var SQL = new StringBuilder();
                var cm = new MySqlCommand();

                SQL.AppendLine("Select A.SOContractDocNo, A.SOContractDNo, (A.Qty - IfNull(B.DOQty, 0.00)) StockQty ");
                SQL.AppendLine("From TblDRDtl A ");
                SQL.AppendLine("Left Join ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select T1.SOContractDocNo, T1.SOContractDNo, Sum(T1.Qty) DOQty ");
                SQL.AppendLine("    From TblDOCt2Dtl T1 ");
                SQL.AppendLine("    Inner Join TblDOCt2Hdr T2 On T1.DocNo = T2.DocNo ");
                SQL.AppendLine("    Where T2.SOContractDocNo Is Not Null And T2.SOContractDNo Is Not Null ");
                SQL.AppendLine("    And Find_In_Set(Concat(T1.SOContractDocNo, T1.SOContractDNo), @DocNoDNo) ");
                SQL.AppendLine("    Group By T1.SOContractDocNo, T1.SOContractDNo ");
                SQL.AppendLine(") B On A.DocNo = B.SOContractDocNo And A.DNo = B.SOContractDNo ");
                SQL.AppendLine("Where A.DocNo = @DRDocNo; ");

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    Sm.CmParam<String>(ref cm, "@DocNoDNo", DocNoDNo);
                    Sm.CmParam<String>(ref cm, "@DRDocNo", TxtDRDocNo.Text);
                    cm.CommandText = SQL.ToString();
                    cm.CommandTimeout = 600;
                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] { "SOContractDocNo", "SOContractDNo", "StockQty" });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
                            {
                                if (Sm.GetGrdStr(Grd1, i, 27) == Sm.DrStr(dr, c[0]) &&
                                    Sm.GetGrdStr(Grd1, i, 28) == Sm.DrStr(dr, c[1]))
                                {
                                    Sm.SetGrdValue("N", Grd1, dr, c, i, 14, 2);
                                    Sm.SetGrdValue("N", Grd1, dr, c, i, 17, 2);
                                    Sm.SetGrdValue("N", Grd1, dr, c, i, 20, 2);
                                    break;
                                }
                            }
                        }
                    }
                    dr.Close();
                }
            }
        }

        private void ReComputeStock()
        {
            string Filter = string.Empty, Source = string.Empty, Lot = string.Empty, Bin = string.Empty;
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Source, Lot, Bin, Qty, Qty2, Qty3 ");
            SQL.AppendLine("From TblStockSummary ");
            SQL.AppendLine("Where WhsCode=@WhsCode ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand();
                cm.Connection = cn;
                Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
                if (Grd1.Rows.Count != 1)
                {
                    int No = 1;
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    {
                        if (Sm.GetGrdStr(Grd1, Row, 11).Length != 0)
                        {
                            Sm.GenerateSQLConditionForInventory(ref cm, ref Filter, No, ref Grd1, Row, 11);
                            No += 1;
                        }
                    }
                }
                if (Filter.Length == 0)
                    Filter = " And 0=1 ";
                else
                    Filter = " And (" + Filter + ")";

                cm.CommandText = SQL.ToString() + Filter;
                cm.CommandTimeout = 600;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                    { 
                        //0
                        "Source", 
                        
                        //1-5
                        "Lot", "Bin", "Qty", "Qty2", "Qty3" 
                    });

                if (dr.HasRows)
                {
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        Source = Sm.DrStr(dr, 0);
                        Lot = Sm.DrStr(dr, 1);
                        Bin = Sm.DrStr(dr, 2);
                        for (int row = 0; row < Grd1.Rows.Count - 1; row++)
                        {
                            if (
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, row, 11), Source) &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, row, 12), Lot) &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, row, 13), Bin)
                                )
                            {
                                Sm.SetGrdValue("N", Grd1, dr, c, row, 14, 3);
                                Sm.SetGrdValue("N", Grd1, dr, c, row, 17, 4);
                                Sm.SetGrdValue("N", Grd1, dr, c, row, 20, 5);
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        public static string GetNumber(string palet)
        {
            string number = string.Empty;
            for (int ind = 0; ind < palet.Length; ind++)
            {
                if (Char.IsNumber(palet[ind]) == true)
                {
                    number = number + palet[ind];
                }

            }
            return number;
        }

        private void ParPrint(string DocNo, string PLDocNo, int parValue)
        {
            string Doctitle = Sm.GetParameter("DocTitle");

            var l = new List<DOCt>();
            var ldtl = new List<DOCtDtl>();
            var ldtl2 = new List<DOCtDtl2>();
            var ldtl3 = new List<DOCtDtl3>();
            var ldtl4 = new List<DOCtDtl4>();
            var l1 = new List<DOCtIMSService>();
            var l1d = new List<DOCtIMSServiceDtl>();
            var l1s = new List<DOCtIMSServiceSummary>();
            var l2 = new List<DOCtIMSInventory>();
            var l2d = new List<DOCtIMSInventoryDtl>();

            string[] TableName;
            if(Doctitle == "IMS")
                TableName = new string[] { "DOCtIMSService", "DOCtIMSServiceDtl", "DOCtIMSServiceSummary", "DOCtIMSInventory", "DOCtIMSInventoryDtl" };
            else
                TableName = new string[] { "DOToCt", "DOToCtDtl", "DOCtDtl2", "DOCtDtl3", "DOCtDtl4" };
            List<IList> myLists = new List<IList>();
            var cm = new MySqlCommand();

            if (Doctitle == "IMS")
            {
                #region Header IMS Service
                var SQL1 = new StringBuilder();
                var cm1 = new MySqlCommand();

                SQL1.AppendLine("SELECT Distinct @CompanyLogo As CompanyLogo, A.DocNo, E.PONo, DATE_FORMAT(A.DocDt, '%d/%b/%Y') DocDt, B1.CtName, ");
                SQL1.AppendLine("IFNULL(I.ProjectCode, E.ProjectCode2) ProjectCode, IFNULL(I.ProjectName, H.ProjectName) ProjectName, ");
                SQL1.AppendLine("A.Remark, J.UserName ");
                SQL1.AppendLine("FROM TblDOCt2Hdr A ");
                SQL1.AppendLine("INNER JOIN TblDRHdr B ON A.DRDocNo = B.DocNo ");
                SQL1.AppendLine("    AND A.DocNo = @DocNo ");
                SQL1.AppendLine("INNER JOIN TblCustomer B1 ON A.CtCode = B1.CtCode ");
                SQL1.AppendLine("INNER JOIN TblDOCt2Dtl2 C ON A.DocNo = C.DocNo ");
                SQL1.AppendLine("INNER JOIN TblDRDtl D ON B.DocNo = D.DocNo AND C.DRDNo = D.DNo ");
                SQL1.AppendLine("INNER JOIN TblSOContractHdr E ON D.SODocNo = E.DocNo ");
                SQL1.AppendLine("INNER JOIN TblSOContractDtl F ON E.DocNo = F.DocNo AND D.SODNo = F.DNo ");
                SQL1.AppendLine("INNER JOIN TblBOQHdr G ON E.BOQDocNo = G.DocNo ");
                SQL1.AppendLine("INNER JOIN TblLOPHdr H ON G.LOPDocNo = H.DocNo ");
                SQL1.AppendLine("LEFT JOIN TblProjectGroup I ON H.PGCode = I.PGCode ");
                SQL1.AppendLine("INNER JOIN TblUser J ON A.CreateBy = J.UserCode ");
                SQL1.AppendLine("INNER JOIN TblItem K ON F.ItCode = K.ItCode AND K.ServiceItemInd = 'Y'; ");

                using (var cn1 = new MySqlConnection(Gv.ConnectionString))
                {
                    cn1.Open();
                    cm1.Connection = cn1;
                    cm1.CommandText = SQL1.ToString();
                    Sm.CmParam<String>(ref cm1, "@DocNo", DocNo);
                    Sm.CmParam<String>(ref cm1, "@CompanyLogo", @Sm.CompanyLogo());

                    var dr1 = cm1.ExecuteReader();
                    var c1 = Sm.GetOrdinal(dr1, new string[]
                    {
                        //0
                        "CompanyLogo",
                        //1-5
                        "DocNo",
                        "PONo",
                        "DocDt",
                        "CtName",
                        "ProjectCode",
                        //6-8
                        "ProjectName",
                        "Remark",
                        "UserName",
                    });

                    if (dr1.HasRows)
                    {
                        while (dr1.Read())
                        {
                            l1.Add(new DOCtIMSService()
                            {
                                CompanyLogo = Sm.DrStr(dr1, c1[0]),
                                DocNo = Sm.DrStr(dr1, c1[1]),
                                PONo = Sm.DrStr(dr1, c1[2]),
                                DocDt = Sm.DrStr(dr1, c1[3]),
                                CtName = Sm.DrStr(dr1, c1[4]),
                                ProjectCode = Sm.DrStr(dr1, c1[5]),
                                ProjectName = Sm.DrStr(dr1, c1[6]),
                                Remark = Sm.DrStr(dr1, c1[7]),
                                UserName = Sm.DrStr(dr1, c1[8]),
                                WorkingDuration = string.Empty,
                                SJNNo = TxtLocalDocNo.Text,
                                ResiNo = TxtResiNo.Text,
                                InspectionNo = TxtInspectionSheet.Text,
                            });
                        }
                    }
                    dr1.Close();
                }
                myLists.Add(l1);

                #endregion

                #region Detail IMS Service
                var SQL11 = new StringBuilder();
                var cm11 = new MySqlCommand();

                string mSODocNo = string.Empty;
                string mItCode = string.Empty;

                for (int i = 0; i < Grd1.Rows.Count; ++i)
                {
                    if (Sm.GetGrdStr(Grd1, i, 4).Length > 0)
                    {
                        if (mItCode.Length > 0) mItCode += ",";
                        mItCode += Sm.GetGrdStr(Grd1, i, 4);
                    }
                }

                for (int i = 0; i < Grd2.Rows.Count; ++i)
                {
                    if (Sm.GetGrdStr(Grd2, i, 1).Length > 0)
                    {
                        if (mSODocNo.Length > 0) mSODocNo += ",";
                        mSODocNo += Sm.GetGrdStr(Grd2, i, 1);
                    }
                }

                #region Old Code
                //SQL11.AppendLine("SELECT A.DocNo, C.DNo, J.ItName, F1.DeliveryDt, C.Qty, J.InventoryUomCode Uom, ");
                //SQL11.AppendLine("(C.Qty + IFNULL(K.Qty, 0.00)) ProgressQty, F.Qty TotalQty, (F.Qty - (C.Qty + IFNULL(K.Qty, 0.00))) OutstandingQty ");
                //SQL11.AppendLine("FROM TblDOCt2Hdr A ");
                //SQL11.AppendLine("INNER JOIN TblDRHdr B ON A.DRDocNo = B.DocNo ");
                //SQL11.AppendLine("    AND A.DocNo = @DocNo ");
                //SQL11.AppendLine("INNER JOIN TblCustomer B1 ON A.CtCode = B1.CtCode ");
                //SQL11.AppendLine("INNER JOIN TblDOCt2Dtl2 C ON A.DocNo = C.DocNo ");
                //SQL11.AppendLine("INNER JOIN TblDRDtl D ON B.DocNo = D.DocNo AND C.DRDNo = D.DNo ");
                //SQL11.AppendLine("INNER JOIN TblSOContractHdr E ON D.SODocNo = E.DocNo ");
                //SQL11.AppendLine("INNER JOIN TblSOContractDtl4 F ON E.DocNo = F.DocNo AND D.SODNo = F.DNo ");
                //SQL11.AppendLine("INNER JOIN ");
                //SQL11.AppendLine("( ");
                //SQL11.AppendLine("    SELECT DocNo, ItCode, ");
                //SQL11.AppendLine("    GROUP_CONCAT(DISTINCT IFNULL(DATE_FORMAT(DeliveryDt, '%d/%b/%Y'), '')) DeliveryDt, ");
                //SQL11.AppendLine("    SUM(Qty) Qty ");
                //SQL11.AppendLine("    FROM TblSOContractDtl  ");
                //SQL11.AppendLine("    GROUP BY DocNo, ItCode ");
                //SQL11.AppendLine(") F1 ON E.DocNo = F1.DocNo ");
                //SQL11.AppendLine("INNER JOIN TblBOQHdr G ON E.BOQDocNo = G.DocNo ");
                //SQL11.AppendLine("INNER JOIN TblLOPHdr H ON G.LOPDocNo = H.DocNo ");
                //SQL11.AppendLine("INNER JOIN TblBOMDtl2 I ON F.BOMDocNo = I.DocNo AND F.BOMDNo = I.DNo ");
                //SQL11.AppendLine("INNER JOIN TblItem J ON I.ItCode = J.ItCode AND F1.ItCode = J.ItCode AND J.ServiceItemInd = 'Y' ");
                //SQL11.AppendLine("LEFT JOIN ");
                //SQL11.AppendLine("( ");
                //SQL11.AppendLine("    SELECT T4.SODocNo, T6.ItCode, SUM(T3.Qty) Qty ");
                //SQL11.AppendLine("    FROM TblDOCt2Hdr T1 ");
                //SQL11.AppendLine("    INNER JOIN TblDRHdr T2 ON T1.DRDocNo = T2.DocNo AND T1.DocNo != @DocNo ");
                //SQL11.AppendLine("    INNER JOIN TblDOCt2Dtl2 T3 ON T1.DocNo = T3.DocNo ");
                //SQL11.AppendLine("    INNER JOIN TblDRDtl T4 ON T2.DocNo = T4.DocNo AND T3.DRDNo = T4.DNo AND FIND_IN_SET(T4.SODocNo, @SODocNo) ");
                //SQL11.AppendLine("    INNER JOIN TblSOContractDtl4 T5 ON T4.SODocNo = T5.DocNo AND T4.SODNo = T5.DNo ");
                //SQL11.AppendLine("    INNER JOIN TblBOMDtl2 T6 ON T5.BOMDocNo = T6.DocNo AND T5.BOMDNo = T6.DNo AND FIND_IN_SET(T6.ItCode, @ItCode) ");
                //SQL11.AppendLine("    GROUP BY T4.SODocNo, T6.ItCode ");
                //SQL11.AppendLine(") K ON E.DocNo = K.SODocNo AND J.ItCode = K.ItCode; ");
                #endregion
                SQL11.AppendLine("SELECT Distinct A.DocNo, B.DNo, E.ItName, IfNull(Date_Format(I.DeliveryDt, '%d/%b%Y'), '') DeliveryDt, B.Qty, E.InventoryUomCode Uom,  ");
                SQL11.AppendLine("(B.Qty + IFNULL(H.Qty, 0.00)) ProgressQty, F.Qty TotalQty, (F.Qty - (B.Qty + IFNULL(H.Qty, 0.00))) OutstandingQty, L.ItName ItNameDismantle, I.No ");
                SQL11.AppendLine("FROM TblDOCt2Hdr A ");
                SQL11.AppendLine("INNER JOIN TblDOCt2Dtl B ON A.DocNo = B.DocNo ");
                SQL11.AppendLine("    AND A.DocNo = @DocNo ");
                SQL11.AppendLine("INNER JOIN TblCustomer C ON A.CtCode = C.CtCode ");
                SQL11.AppendLine("INNER JOIN TblDRHdr D ON A.DRDocNo = D.DocNo ");
                SQL11.AppendLine("INNER JOIN TblItem E ON B.ItCode = E.ItCode AND E.ServiceItemInd = 'Y' ");
                SQL11.AppendLine("LEFT JOIN ");
                SQL11.AppendLine("( ");
                SQL11.AppendLine("    SELECT T1.DocNo, T4.ItCode, T4.Qty,T4.seqno ");
                SQL11.AppendLine("    FROM TblDOCt2Hdr T1 ");
                SQL11.AppendLine("    INNER JOIN TblDOCt2Dtl2 T2 ON T1.DocNo = T2.DocNo AND T1.DocNo = @DocNo ");
                SQL11.AppendLine("    INNER JOIN TblDRDtl T3 ON T1.DRDocNo = T3.DocNo AND T2.DRDNo = T3.DNo ");
                SQL11.AppendLine("    INNER JOIN TblSOContractDtl4 T4 ON T3.SODocNo = T4.DocNo AND T3.SODNo = T4.DNo ");
                SQL11.AppendLine(") F ON A.DocNo = F.DocNo AND E.ItCode = F.ItCode ");
                SQL11.AppendLine("Left Join TblSOCOntractHdr G On B.SOContractDocNo = G.DOcNo ");
                SQL11.AppendLine("Left Join TblSOContractDtl I On B.SOContractDocNo = G.DocNo And B.SOContractDNo = I.DNo AND f.itcode = i.itcode AND I.DocNo=@SODocNo ");
                SQL11.AppendLine("Left Join tblsocontractdtl4 J ON G.DocNo=J.DocNo AND I.DNo=J.DNo AND I.ItCode=J.ItCode ");
                SQL11.AppendLine("LEFT JOIN tblboqdtl2 K ON J.BOQDocNo=K.DocNo AND J.ItCode=K.ItCode AND J.SeqNo=K.SeqNo AND J.`No`=I.`No` ");
                SQL11.AppendLine("LEFT JOIN tblitem L ON K.ItCodeDismantle=L.ItCode ");
                #region Old COde comment by wed
                //SQL11.AppendLine("LEFT JOIN ");
                //SQL11.AppendLine("( ");
                //SQL11.AppendLine("    SELECT T1.DocNo, T4.ItCode, GROUP_CONCAT(DISTINCT IFNULL(DATE_FORMAT(T4.DeliveryDt, '%d/%b/%Y'), '') SEPARATOR ', ') DeliveryDt ");
                //SQL11.AppendLine("    FROM TblDOCt2Hdr T1 ");
                //SQL11.AppendLine("    INNER JOIN TblDOCt2Dtl2 T2 ON T1.DocNo = T2.DocNo AND T1.DocNo = @DocNo ");
                //SQL11.AppendLine("    INNER JOIN TblDRDtl T3 ON T1.DRDocNo = T3.DocNo AND T2.DRDNo = T3.DNo ");
                //SQL11.AppendLine("    INNER JOIN TblSOContractDtl T4 ON T3.SODocNo = T4.DocNo AND T3.SODNo = T4.DNo ");
                //SQL11.AppendLine("    GROUP BY T1.DocNo, T4.ItCode ");
                //SQL11.AppendLine(") G ON A.DocNo = G.DocNo AND E.ItCode = G.ItCode ");
                #endregion
                SQL11.AppendLine("LEFT JOIN ");
                SQL11.AppendLine("( ");
                SQL11.AppendLine("    SELECT T1.ItCode, SUM(T1.Qty) Qty ");
                SQL11.AppendLine("    FROM TblDOCt2Dtl T1 ");
                SQL11.AppendLine("    WHERE T1.DocNo IN  ");
                SQL11.AppendLine("    ( ");
                SQL11.AppendLine("        SELECT DISTINCT X1.DocNo ");
                SQL11.AppendLine("        FROM TblDOCt2Hdr X1 ");
                SQL11.AppendLine("        INNER JOIN TblDOCt2Dtl2 X2 ON X1.DocNo = X2.DocNo AND X1.DocNo != @DocNo ");
                SQL11.AppendLine("        INNER JOIN TblDRDtl X3 ON X1.DRDocNo = X3.DocNo AND X2.DRDNo = X3.DNo ");
                SQL11.AppendLine("            AND FIND_IN_SET(X3.SODocNo, @SODocNo) ");
                SQL11.AppendLine("        INNER JOIN TblSOContractDtl4 X4 ON X3.SODocNo = X4.DocNo ");
                SQL11.AppendLine("            AND FIND_IN_SET(X4.ItCode, @ItCode) ");
                SQL11.AppendLine("    ) ");
                SQL11.AppendLine("    AND T1.CancelInd = 'N' ");
                SQL11.AppendLine(") H ON E.ItCode = H.ItCode ");
                SQL11.AppendLine("; ");

                using (var cn11 = new MySqlConnection(Gv.ConnectionString))
                {
                    cn11.Open();
                    cm11.Connection = cn11;
                    cm11.CommandText = SQL11.ToString();
                    Sm.CmParam<String>(ref cm11, "@DocNo", DocNo);
                    Sm.CmParam<String>(ref cm11, "@SODocNo", mSODocNo);
                    Sm.CmParam<String>(ref cm11, "@ItCode", mItCode);

                    var dr11 = cm11.ExecuteReader();
                    var c11 = Sm.GetOrdinal(dr11, new string[]
                    {
                        //0
                        "DocNo",
                        //1-5
                        "DNo",
                        "ItName",
                        "DeliveryDt",
                        "Qty",
                        "Uom",
                        //6-10
                        "ProgressQty",
                        "TotalQty",
                        "OutstandingQty",
                        "ItNameDismantle",
                        "No",
                    });

                    if (dr11.HasRows)
                    {
                        int mNo = 1;
                        while (dr11.Read())
                        {
                            l1d.Add(new DOCtIMSServiceDtl()
                            {
                                No = DocNo = Sm.DrStr(dr11, c11[10]),
                                DocNo = Sm.DrStr(dr11, c11[0]),
                                DNo = Sm.DrStr(dr11, c11[1]),
                                ItName = Sm.DrStr(dr11, c11[2]),
                                DeliveryDt = Sm.DrStr(dr11, c11[3]),
                                Qty = Sm.DrDec(dr11, c11[4]),
                                Uom = Sm.DrStr(dr11, c11[5]),
                                ProgressQty = Sm.DrDec(dr11, c11[6]),
                                TotalQty = Sm.DrDec(dr11, c11[7]),
                                OutstandingQty = Sm.DrDec(dr11, c11[8]),
                                ItNameDismantle = Sm.DrStr(dr11, c11[9]),
                           
                            });
                            mNo += 1;
                        }
                    }
                    dr11.Close();
                }
                myLists.Add(l1d);

                #endregion

                #region Summary IMS Service

                if (l1d.Count > 0)
                {
                    decimal mQty = 0m, mProgressQty = 0m, mTotalQty = 0m, mOutstandingQty = 0m;
                    string mDocNo = TxtDocNo.Text;
                    for (int i = 0; i < l1d.Count; ++i)
                    {
                        mQty += l1d[i].Qty;
                        mProgressQty += l1d[i].ProgressQty;
                        mTotalQty += l1d[i].TotalQty;
                        mOutstandingQty += l1d[i].OutstandingQty;
                    }

                    l1s.Add(new DOCtIMSServiceSummary() 
                    {
                        DocNo = mDocNo,
                        Qty = mQty,
                        ProgressQty = mProgressQty,
                        TotalQty = mTotalQty,
                        OutstandingQty = mOutstandingQty
                    });
                }

                myLists.Add(l1s);

                #endregion

                #region Header IMS Inventory
                var SQL2 = new StringBuilder();
                var cm2 = new MySqlCommand();

                SQL2.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
                SQL2.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
                SQL2.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyPhone', ");
                SQL2.AppendLine("(Select ParValue From TblParameter Where Parcode='DOCDRPrintOutSignName') As 'Sender', ");
                SQL2.AppendLine("A.DocNo, E.PONo, DATE_FORMAT(A.DocDt, '%d/%b/%Y') DocDt, B1.CtName, ");
                SQL2.AppendLine("IFNULL(I.ProjectCode, E.ProjectCode2) ProjectCode, IFNULL(I.ProjectName, H.ProjectName) ProjectName, J.UserName, A.ResiNo, A.InspectionSheetNo, A.LocalDocNo ");
                SQL2.AppendLine("FROM TblDOCt2Hdr A ");
                SQL2.AppendLine("INNER JOIN TblDRHdr B ON A.DRDocNo = B.DocNo ");
                SQL2.AppendLine("    AND A.DocNo = @DocNo ");
                SQL2.AppendLine("INNER JOIN TblCustomer B1 ON A.CtCode = B1.CtCode ");
                SQL2.AppendLine("INNER JOIN TblDOCt2Dtl2 C ON A.DocNo = C.DocNo ");
                SQL2.AppendLine("INNER JOIN TblDRDtl D ON B.DocNo = D.DocNo AND C.DRDNo = D.DNo ");
                SQL2.AppendLine("INNER JOIN TblSOContractHdr E ON D.SODocNo = E.DocNo ");
                SQL2.AppendLine("INNER JOIN TblSOContractDtl F ON E.DocNo = F.DocNo AND D.SODNo = F.DNo ");
                SQL2.AppendLine("INNER JOIN ");
                SQL2.AppendLine("( ");
	            SQL2.AppendLine("    SELECT DocNo, ItCode, ");
	            SQL2.AppendLine("    GROUP_CONCAT(DISTINCT IFNULL(DATE_FORMAT(DeliveryDt, '%d/%b/%Y'), '')) DeliveryDt, ");
	            SQL2.AppendLine("    SUM(Qty) Qty ");
	            SQL2.AppendLine("    FROM TblSOContractDtl  ");
	            SQL2.AppendLine("    GROUP BY DocNo, ItCode ");
                SQL2.AppendLine(") F1 ON E.DocNo = F1.DocNo ");
                SQL2.AppendLine("INNER JOIN TblBOQHdr G ON E.BOQDocNo = G.DocNo ");
                SQL2.AppendLine("INNER JOIN TblLOPHdr H ON G.LOPDocNo = H.DocNo ");
                SQL2.AppendLine("LEFT JOIN TblProjectGroup I ON H.PGCode = I.PGCode ");
                SQL2.AppendLine("INNER JOIN TblUser J ON A.CreateBy = J.UserCode ");
                SQL2.AppendLine("INNER JOIN TblItem K ON F.ItCode = K.ItCode AND K.InventoryItemInd = 'Y'; ");
             
                using (var cn2 = new MySqlConnection(Gv.ConnectionString))
                {
                    cn2.Open();
                    cm2.Connection = cn2;
                    cm2.CommandText = SQL2.ToString();
                    Sm.CmParam<String>(ref cm2, "@DocNo", DocNo);
                    Sm.CmParam<String>(ref cm2, "@CompanyLogo", @Sm.CompanyLogo());

                    var dr2 = cm2.ExecuteReader();
                    var c2 = Sm.GetOrdinal(dr2, new string[]
                    {
                        //0
                        "CompanyLogo",
                        //1-5
                        "CompanyName",
                        "CompanyAddress",
                        "CompanyPhone",
                        "DocNo",
                        "PONo",
                        //6-10
                        "DocDt",
                        "CtName",
                        "ProjectCode",
                        "ProjectName",
                        "UserName",

                        //11-14
                        "ResiNo",
                        "InspectionSheetNo", 
                        "LocalDocNo",
                        "Sender"
                    });

                    if (dr2.HasRows)
                    {
                        while (dr2.Read())
                        {
                            l2.Add(new DOCtIMSInventory()
                            {
                                CompanyLogo = Sm.DrStr(dr2, c2[0]),
                                CompanyName = Sm.DrStr(dr2, c2[1]),
                                CompanyAddress = Sm.DrStr(dr2, c2[2]),
                                CompanyPhone = Sm.DrStr(dr2, c2[3]),
                                DocNo = Sm.DrStr(dr2, c2[4]),
                                PONo = Sm.DrStr(dr2, c2[5]),
                                DocDt = Sm.DrStr(dr2, c2[6]),
                                CtName = Sm.DrStr(dr2, c2[7]),
                                ProjectCode = Sm.DrStr(dr2, c2[8]),
                                ProjectName = Sm.DrStr(dr2, c2[9]),
                                UserName = Sm.DrStr(dr2, c2[10]),
                                ResiNo = Sm.DrStr(dr2, c2[11]),
                                InspectionSheet = Sm.DrStr(dr2, c2[12]),
                                LocalDocNo = Sm.DrStr(dr2, c2[13]),
                                Sender = Sm.DrStr(dr2, c2[14]),
                            });
                        }
                    }
                    dr2.Close();
                }

                myLists.Add(l2);

                #endregion

                #region Detail IMS Inventory
                var SQL21 = new StringBuilder();
                var cm21 = new MySqlCommand();

                SQL21.AppendLine("SELECT A.DocNo, B.DNo, H.ItName ItNameDismantle, C.ItName, C.ItCodeInternal, B.Qty, C.InventoryUomCode Uom, B.Remark, ");
                SQL21.AppendLine("D.No As SOCNo, D.Remark As SOCRemark  ");
                SQL21.AppendLine("FROM TblDOCt2Hdr A ");
                SQL21.AppendLine("INNER JOIN TblDOCt2Dtl B ON A.DocNo = B.DocNo ");
                SQL21.AppendLine("    AND A.DocNo = @DocNo ");
                SQL21.AppendLine("    AND B.CancelInd = 'N' ");
                SQL21.AppendLine("INNER JOIN TblItem C ON B.ItCode = C.ItCode ");
                SQL21.AppendLine("Inner JOin TblSOContractDtl D On B.SOContractDocNo = D.DOcNo And B.SOContractDNo = D.DNo ");
                SQL21.AppendLine("inner join tblsocontracthdr E on D.DocNo = E.DocNo ");
                SQL21.AppendLine("inner join tblboqhdr F on E.BOQDocNo = F.Docno ");
                SQL21.AppendLine("left join tblboqdtl2 G on F.DocNo = G.DocNo and D.No = G.No ");
                SQL21.AppendLine("left join tblitem H on G.ItCodeDismantle = H.ItCode ");
                SQL21.AppendLine("Group By A.DocNo;");


                using (var cn21 = new MySqlConnection(Gv.ConnectionString))
                {
                    cn21.Open();
                    cm21.Connection = cn21;
                    cm21.CommandText = SQL21.ToString();
                    Sm.CmParam<String>(ref cm21, "@DocNo", TxtDocNo.Text);

                    var dr21 = cm21.ExecuteReader();
                    var c21 = Sm.GetOrdinal(dr21, new string[]
                    {
                        //0
                        "DocNo",

                        //1-5
                        "DNo",
                        "ItName",
                        "ItCodeInternal",
                        "Qty",
                        "Uom",
                        
                        //6-9
                        "Remark",
                        "SOCNo",
                        "SOCRemark",
                        "ItNameDismantle",
                    });

                    if (dr21.HasRows)
                    {
                        while (dr21.Read())
                        {
                            l2d.Add(new DOCtIMSInventoryDtl()
                            {
                                DocNo = Sm.DrStr(dr21, c21[0]),
                                DNo = Sm.DrStr(dr21, c21[1]),
                                ItName = Sm.DrStr(dr21, c21[2]),
                                ItCodeInternal = Sm.DrStr(dr21, c21[3]),
                                Qty = Sm.DrDec(dr21, c21[4]),
                                Uom = Sm.DrStr(dr21, c21[5]),
                                Remark = Sm.DrStr(dr21, c21[6]),
                                SOCNo = Sm.DrStr(dr21, c21[7]),
                                SOCRemark = Sm.DrStr(dr21, c21[8]),
                                ItNameDismantle = Sm.DrStr(dr21, c21[9]),
                            });
                        }
                    }
                    dr21.Close();
                }

                myLists.Add(l2d);

                #endregion
            }
            else
            {

                #region Header
                var SQL = new StringBuilder();

                SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyPhone', ");
                SQL.AppendLine("Replace(A.DocNo, Concat('/',(Select ParValue From Tblparameter Where ParCode = 'DocTitle')),'') As DocNo, ");
                SQL.AppendLine("DATE_FORMAT(A.DocDt,'%d %M %Y') As DocDt, A.LocalDocNo As DocNoInternal, C.CtName, ");
                SQL.AppendLine("D.SAName, D.SAAddress, E.CityName, F.CntName, D.SAPostalCd, D.SAPhone, D.SAFax, D.SAEmail, D.SAMobile, A.Remark, '1' As Indicator, A.ExpDriver, A.ExpPlatNo, G.VdName As ExpName, ");
                SQL.AppendLine("(Select Length(substring(parvalue,11)) As FormatNum from tblparameter Where Parcode='FormatNum0') As 'FormatNum', H.Account, I.SPPlaceDelivery, H.LocalDocNo, ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyAddressCity', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle5') As 'CompanyFax', ");
                SQL.AppendLine("(Select parvalue from tblparameter where parcode='IsShowBatchNo') As 'IsShowBatchNo', ");
                SQL.AppendLine("(SELECT ParValue FROM TblParameter WHERE ParCode = 'DocTitle') AS 'DocTitle', ");
                SQL.AppendLine("CASE WHEN A.DRDocNo IS NULL THEN N.OverSeaInd ELSE L.OverSeaInd END AS OverSeaInd, ");
                SQL.AppendLine("A.Cnt, A.Seal, A.ExpPlatNo, J.SpName, D.LocalDocNo As DRLocalDocNo, D.Note, J.Peb, Date_Format(J.PebDt,'%d-%m-%Y')As PebDt ");
                SQL.AppendLine("From TblDOCt2Hdr A ");
                SQL.AppendLine("Inner Join TblCustomer C On A.CtCode = C.CtCode ");
                SQL.AppendLine("Left Join TblDRHdr D On D.DocNo=A.DRDocNo ");
                SQL.AppendLine("Left Join TblCity E On D.SACityCode = E.CityCode ");
                SQL.AppendLine("Left Join TblCountry F On D.SACntCode= F.CntCode ");
                SQL.AppendLine("Left Join TblVendor G On D.ExpVdCode=G.VdCode ");
                SQL.AppendLine("Left Join TblPlHdr H On A.PlDocNo=H.DocNo ");
                SQL.AppendLine("Left Join TblSiHdr I On H.SiDocNo=I.DocNo ");
                SQL.AppendLine("Left Join TblSP J On I.SpDocNo = J.DocNo ");
                SQL.AppendLine("LEFT JOIN TblDRDtl K ON D.DocNo = K.DocNo ");
                SQL.AppendLine("LEFT JOIN TblSOHdr L ON K.SODocNo = L.DocNo ");
                SQL.AppendLine("LEFT JOIN TblPLDtl M ON H.DocNo = M.DocNo ");
                SQL.AppendLine("LEFT JOIN TblSOHdr N ON M.SODocNo = N.DocNo ");
                SQL.AppendLine("Where A.DocNo=@DocNo; ");

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = SQL.ToString();
                    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
                    Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());

                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[]
                {
                //0
                "CompanyLogo",
                //1-5
                "CompanyName",
                "CompanyAddress",
                "CompanyPhone",
                "DocNo",
                "DocDt",
                //6-10
                "DocNoInternal",
                "CtName",
                "SANAme",
                "SAAddress",
                "CityName",
                //11-15
                "CntName",
                "SAPostalCd",
                "SAPhone",
                "SAFax",
                "SAEmail",
                //16-17
                "SAMobile",
                "Remark",
                "Indicator",
                "ExpDriver", 
                "ExpPlatNo",
                //21-25
                "ExpName",
                "FormatNum",
                "Account",
                "SPPlaceDelivery",
                "LocalDocNo",
                //26-30
                "CompanyAddressCity",
                "CompanyFax",
                "Cnt",
                "Seal",
                "SpName",
                
                //31-35
                "IsShowBatchNo",
                "OverSeaInd",
                "DocTitle",
                "DRLocalDocNo",
                "Note",

                //36-37
                "Peb",
                "PebDt"

                });

                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            l.Add(new DOCt()
                            {
                                CompanyLogo = Sm.DrStr(dr, c[0]),
                                CompanyName = Sm.DrStr(dr, c[1]),
                                CompanyAddress = Sm.DrStr(dr, c[2]),
                                CompanyPhone = Sm.DrStr(dr, c[3]),
                                DocNo = Sm.DrStr(dr, c[4]),
                                DocDt = Sm.DrStr(dr, c[5]),
                                DocNoInternal = Sm.DrStr(dr, c[6]),
                                CtName = Sm.DrStr(dr, c[7]),
                                Name = Sm.DrStr(dr, c[8]),
                                Address = Sm.DrStr(dr, c[9]),
                                CityName = Sm.DrStr(dr, c[10]),
                                CntName = Sm.DrStr(dr, c[11]),
                                PostalCd = Sm.DrStr(dr, c[12]),
                                Phone = Sm.DrStr(dr, c[13]),
                                Fax = Sm.DrStr(dr, c[14]),
                                Email = Sm.DrStr(dr, c[15]),
                                Mobile = Sm.DrStr(dr, c[16]),
                                HRemark = Sm.DrStr(dr, c[17]),
                                Indicator = Sm.DrStr(dr, c[18]),
                                ExpDriver = Sm.DrStr(dr, c[19]),
                                ExpPlatNo = Sm.DrStr(dr, c[20]),
                                ExpName = Sm.DrStr(dr, c[21]),
                                FormatNum = Sm.DrStr(dr, c[22]),
                                Account = Sm.DrStr(dr, c[23]),
                                SPPlaceDelivery = Sm.DrStr(dr, c[24]),
                                LocalDocNo = Sm.DrStr(dr, c[25]),
                                CompanyAddressCity = Sm.DrStr(dr, c[26]),
                                CompanyFax = Sm.DrStr(dr, c[27]),
                                Cnt = Sm.DrStr(dr, c[28]),
                                Seal = Sm.DrStr(dr, c[29]),
                                SpName = Sm.DrStr(dr, c[30]),
                                IsShowBatchNo = Sm.DrStr(dr, c[31]),
                                OverSeaInd = Sm.DrStr(dr, c[32]),
                                DocTitle = Sm.DrStr(dr, c[33]),
                                DRLocalDocNo = Sm.DrStr(dr, c[34]),
                                Note = Sm.DrStr(dr, c[35]),
                                Peb = Sm.DrStr(dr, c[36]),
                                PebDt = Sm.DrStr(dr, c[37]),
                                PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
                            });
                        }
                    }
                    dr.Close();
                }
                myLists.Add(l);

                #endregion

                #region Detail

                var cmDtl = new MySqlCommand();

                var SQLDtl = new StringBuilder();

                using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
                {

                    cnDtl.Open();

                    cmDtl.Connection = cnDtl;

                    SQLDtl.AppendLine("Select B.ItCode, ");
                    SQLDtl.AppendLine("IF((SELECT ParValue FROM TblParameter WHERE ParCode = 'DocTitle')='IOK',C.ForeignName,C.ItName) AS ItName, ");
                    SQLDtl.AppendLine("B.BatchNo, B.Source, B.Lot, B.Bin, ");
                    SQLDtl.AppendLine("B.Qty, C.InventoryUomCode, ");
                    SQLDtl.AppendLine("B.Qty2, C.InventoryUomCode2, ");
                    SQLDtl.AppendLine("B.Qty3, C.InventoryUomCode3, 0 As PackingQty, D.PropName As PackingUomCode, B.Remark, C.ItGrpCode ");
                    SQLDtl.AppendLine("From TblDOCt2Hdr A ");
                    SQLDtl.AppendLine("Inner Join TblDOCt2Dtl B On A.DocNo=B.DocNo");
                    SQLDtl.AppendLine("Inner Join TblItem C On B.ItCode=C.ItCode ");
                    SQLDtl.AppendLine("Left Join TblProperty D On B.PropCode=D.PropCode ");
                    SQLDtl.AppendLine("Where A.DocNo=@DocNo  And B.CancelInd = 'N'");
                    SQLDtl.AppendLine("Order By B.ItCode");

                    cmDtl.CommandText = SQLDtl.ToString();

                    Sm.CmParam<String>(ref cmDtl, "@DocNo", DocNo);
                    var drDtl = cmDtl.ExecuteReader();
                    var cDtl = Sm.GetOrdinal(drDtl, new string[]
                {
                    "ItCode" ,

                    "ItName" ,
                    "BatchNo",
                    "Source",
                    "Lot",
                    "Bin",
                    
                    "Qty" ,
                    "Qty2",
                    "Qty3",
                    "InventoryUomCode" ,
                    "InventoryUomCode2" ,
                    
                    "InventoryUomCode3" ,
                    "Remark",
                    "PackingQty",
                    "PackingUOMCode",
                    "ItGrpCode",
                });

                    if (drDtl.HasRows)
                    {
                        while (drDtl.Read())
                        {
                            ldtl.Add(new DOCtDtl()
                            {
                                ItCode = Sm.DrStr(drDtl, cDtl[0]),
                                ItName = Sm.DrStr(drDtl, cDtl[1]),
                                BatchNo = Sm.DrStr(drDtl, cDtl[2]),
                                Source = Sm.DrStr(drDtl, cDtl[3]),
                                Lot = Sm.DrStr(drDtl, cDtl[4]),
                                Bin = Sm.DrStr(drDtl, cDtl[5]),
                                Qty = Sm.DrDec(drDtl, cDtl[6]),
                                Qty2 = Sm.DrDec(drDtl, cDtl[7]),
                                Qty3 = Sm.DrDec(drDtl, cDtl[8]),
                                InventoryUomCode = Sm.DrStr(drDtl, cDtl[9]),
                                InventoryUomCode2 = Sm.DrStr(drDtl, cDtl[10]),
                                InventoryUomCode3 = Sm.DrStr(drDtl, cDtl[11]),
                                Remark = Sm.DrStr(drDtl, cDtl[12]),
                                PackingQty = Sm.DrDec(drDtl, cDtl[13]),
                                PackingUomCode = Sm.DrStr(drDtl, cDtl[14]),
                                ItGrpCode = Sm.DrStr(drDtl, cDtl[15]),
                            });
                        }
                    }
                    drDtl.Close();
                }
                myLists.Add(ldtl);

                #endregion

                #region Detail2

                //printout MAI

                var cmDtl2 = new MySqlCommand();

                var SQLDtl2 = new StringBuilder();

                using (var cnDtl2 = new MySqlConnection(Gv.ConnectionString))
                {

                    cnDtl2.Open();

                    cmDtl2.Connection = cnDtl2;
                    #region Old
                    //SQLDtl2.AppendLine("Select Z.DocNo, B.ItCode, ");
                    //SQLDtl2.AppendLine("IF((SELECT ParValue FROM TblParameter WHERE ParCode = 'DocTitle')='IOK',B.ForeignName,B.ItName) AS ItName, ");
                    //SQLDtl2.AppendLine("B.ItCodeInternal, J.ItGrpName, B.HSCode As SPHSCOde, E.UPrice, A2.Qty,");
                    //SQLDtl2.AppendLine("Round((E.UPrice * Round(A2.Qty, 4)), 2) As Amount, G.PriceUomCode, Round(A2.QtyInventory,2) As QtyInventory, B.InventoryUomCode, ");
                    //SQLDtl2.AppendLine("IfNull(Round(A2.QtyPackagingUnit *  N.NWRate,2),0) As NW, IfNull(Round(A2.QtyPackagingUnit * N.GWRate,2),0) As GW, K.CtItCode, G.CurCode,  L.OptDesc As Material, ");
                    //SQLDtl2.AppendLine("M.OptDesc As Colour, A2.TotalVolume,  A2.QtyPackagingUnit, C.PackagingUnitUomCode, ifnull(A2.QtyPL, 0) As QtyPL, (A2.Qty /A2.QtyPackagingUnit) QtyPcs ");
                    //SQLDtl2.AppendLine("From TblDoct2Hdr Z  ");
                    //SQLDtl2.AppendLine("Inner Join TblPlhdr A On Z.PLDocNo=A.DocNo ");
                    //SQLDtl2.AppendLine("Inner Join TblPlDtl A2 On A.DocNo = A2.DocNo ");
                    //SQLDtl2.AppendLine("Inner Join TblItem B On A2.ItCode = B.ItCode ");
                    //SQLDtl2.AppendLine("Inner Join TblSODtl C On A2.SoDocNo = C.DocNo And A2.SoDno = C.Dno ");
                    //SQLDtl2.AppendLine("Inner Join TblSOHdr D On C.DocNo = D.DocNo And A2.SODocNo = D.DocNo ");
                    //SQLDtl2.AppendLine("Inner Join TblCtQtDtl E On D.CtQtDocNo = E.DocNo And C.CtQtDNo = E.Dno ");
                    //SQLDtl2.AppendLine("Inner Join TblCtQtHdr F On D.CtQtDocNo = F.DocNo ");
                    //SQLDtl2.AppendLine("Inner Join TblItemPriceHdr G On E.ItemPriceDocNo = G.DocNo ");
                    //SQLDtl2.AppendLine("left Join ");
                    //SQLDtl2.AppendLine("( ");
                    //SQLDtl2.AppendLine("    select ItCode, Qty, Qty2, UomCode, Nw, GW From TblItempackagingunit ");
                    //SQLDtl2.AppendLine(")H On H.ItCode = A2.ItCode And  C.PackagingUnitUomCode = H.UomCode ");
                    //SQLDtl2.AppendLine("Inner Join TblSihdr I On A.SiDocNo=I.DocNo ");
                    //SQLDtl2.AppendLine("Left Join TblItemGroup J On B.ItGrpCode = J.ItGrpCode ");
                    //SQLDtl2.AppendLine("Left Join TblCustomerItem K On B.ItCode=K.ItCode And D.CtCode=K.CtCode ");
                    //SQLDtl2.AppendLine("Left Join TblOption L On L.OptCat='ItemInformation5' And B.Information5=L.OptCode ");
                    //SQLDtl2.AppendLine("Left Join TblOption M On M.OptCat='ItemInformation1' And B.Information1=M.OptCode ");
                    //SQLDtl2.AppendLine("Inner Join TblSIDtl N On A.SiDocNo = N.DocNo And A2.SODocNo = N.SoDocNo And A2.SODno = N.SODno ");
                    //SQLDtl2.AppendLine("Where Z.DocNo=@DocNo;");
                    #endregion

                    SQLDtl2.AppendLine("Select A.DocNo, B.ItCode, C.ItName, C.ItCodeInternal, C.HSCode As SPHSCOde, Sum(B.Qty)As Qty, Sum(B.Qty2)As Qty2, ");
                    SQLDtl2.AppendLine("Sum(B.Qty3)As Qty3, (B.Qty /E.QtyPackagingUnit) QtyPcs, E.QtyPackagingUnit, E.TotalVolume, ");
                    SQLDtl2.AppendLine("IfNull(Round(E.QtyPackagingUnit *  K.NWRate,2),0) As NW, IfNull(Round(E.QtyPackagingUnit * K.GWRate,2),0) As GW, ");
                    SQLDtl2.AppendLine("H.CtItCode, I.OptDesc As Material, J.OptDesc As Colour ");
                    SQLDtl2.AppendLine("From TblDoct2HDr A ");
                    SQLDtl2.AppendLine("Inner Join TblPlhdr D On A.PLDocNo=D.DocNo ");
                    SQLDtl2.AppendLine("Inner Join TblPlDtl E On D.DocNo = E.DocNo ");
                    SQLDtl2.AppendLine("Inner Join TblItem C On E.ItCode = C.ItCode ");
                    SQLDtl2.AppendLine("Inner Join TblSODtl F On E.SoDocNo = F.DocNo And E.SoDno = F.Dno ");
                    SQLDtl2.AppendLine("Inner Join TblSOHdr G On F.DocNo = G.DocNo And E.SODocNo = G.DocNo ");
                    SQLDtl2.AppendLine("Left Join TblCustomerItem H On C.ItCode=H.ItCode And G.CtCode=H.CtCode ");
                    SQLDtl2.AppendLine("Left Join TblOption I On I.OptCat='ItemInformation5' And C.Information5=I.OptCode ");
                    SQLDtl2.AppendLine("Left Join TblOption J On J.OptCat='ItemInformation1' And C.Information1=J.OptCode ");
                    SQLDtl2.AppendLine("Left Join TblSIDtl K On D.SiDocNo = K.DocNo And E.SODocNo = K.SoDocNo And E.SODno = K.SODno ");
                    SQLDtl2.AppendLine("Left Join ");
                    SQLDtl2.AppendLine("( ");
                    SQLDtl2.AppendLine("    Select DocNo, ItCode, Qty, Qty2, Qty3, Cancelind From TblDoct2Dtl ");
                    SQLDtl2.AppendLine(")B On A.DocNo=B.DOcNo And C.ItCode=B.ItCode ");
                    SQLDtl2.AppendLine("Where A.DocNo=@DocNo  And B.CancelInd='N' ");
                    SQLDtl2.AppendLine("Group By A.DOcNo, C.ItName ");

                    cmDtl2.CommandText = SQLDtl2.ToString();

                    Sm.CmParam<String>(ref cmDtl2, "@DocNo", TxtDocNo.Text);
                    var drDtl2 = cmDtl2.ExecuteReader();
                    var cDtl2 = Sm.GetOrdinal(drDtl2, new string[]
                {
                     //0
                     "DocNo",

                     //1-5
                     "ItCode", "ItName", "ItCodeInternal", "SPHSCOde", "Qty", 

                     //6-10
                     "Qty2", "Qty3", "QtyPcs", "QtyPackagingUnit", "TotalVolume",
                      
                     //11-15
                     "NW", "GW", "CtItCode", "Material", "Colour", 


                });

                    if (drDtl2.HasRows)
                    {
                        int nomor = 0;
                        while (drDtl2.Read())
                        {
                            nomor = nomor + 1;
                            ldtl2.Add(new DOCtDtl2()
                            {
                                nomor = nomor,
                                DocNo = Sm.DrStr(drDtl2, cDtl2[0]),

                                ItCode = Sm.DrStr(drDtl2, cDtl2[1]),
                                ItName = Sm.DrStr(drDtl2, cDtl2[2]),
                                ItCodeInternal = Sm.DrStr(drDtl2, cDtl2[3]),
                                SpHsCode = Sm.DrStr(drDtl2, cDtl2[4]),
                                Qty = Sm.DrDec(drDtl2, cDtl2[5]),

                                Qty2 = Sm.DrDec(drDtl2, cDtl2[6]),
                                Qty3 = Sm.DrDec(drDtl2, cDtl2[7]),
                                QtyPcs = Sm.DrDec(drDtl2, cDtl2[8]),
                                QtyPackagingUnit = Sm.DrDec(drDtl2, cDtl2[9]),
                                TotalVolume = Sm.DrDec(drDtl2, cDtl2[10]),

                                NW = Sm.DrDec(drDtl2, cDtl2[11]),
                                GW = Sm.DrDec(drDtl2, cDtl2[12]),
                                CtItCode = Sm.DrStr(drDtl2, cDtl2[13]),
                                Material = Sm.DrStr(drDtl2, cDtl2[14]),
                                Colour = Sm.DrStr(drDtl2, cDtl2[15]),


                            });
                        }
                    }
                    drDtl2.Close();
                }
                myLists.Add(ldtl2);

                #endregion

                #region Detail 3

                var cmDtl3 = new MySqlCommand();

                var SQLDtl3 = new StringBuilder();

                using (var cnDtl3 = new MySqlConnection(Gv.ConnectionString))
                {

                    cnDtl3.Open();

                    cmDtl3.Connection = cnDtl3;

                    SQLDtl3.AppendLine("Select T.* From ( ");
                    SQLDtl3.AppendLine("Select G.ItCode, A.DNo, ROUND(A.QtyPackagingUnit,0) AS PalletNo, A.QtyPackagingUnit, ");
                    SQLDtl3.AppendLine("IF((SELECT ParValue FROM TblParameter WHERE ParCode = 'DocTitle')='IOK',G.ForeignName,G.ItName) AS ItName, ");
                    SQLDtl3.AppendLine("G.Length, G.Width, G.Height, ");
                    SQLDtl3.AppendLine("ROUND(ROUND((A.QtyInventory * G.SalesUomCodeConvert12),1)) AS QtySales, ");
                    SQLDtl3.AppendLine("ROUND(A.QtyInventory,4) AS QtyInventory ");
                    SQLDtl3.AppendLine("From TblDRDtl A ");
                    SQLDtl3.AppendLine("Inner Join TblSOHdr B On A.SODocNo=B.DocNo ");
                    SQLDtl3.AppendLine("Inner Join TblSODtl C On A.SODocNo=C.DocNo And A.SODNo=C.DNo ");
                    SQLDtl3.AppendLine("Inner Join TblCtQtDtl D On B.CtQtDocNo=D.DocNo And C.CtQtDNo=D.DNo ");
                    SQLDtl3.AppendLine("Inner Join TblItemPriceDtl E On D.ItemPriceDocNo=E.DocNo And D.ItemPriceDNo=E.DNo ");
                    SQLDtl3.AppendLine("Left Join TblAgent F On C.AgtCode=F.AgtCode ");
                    SQLDtl3.AppendLine("Inner Join TblItem G On E.ItCode=G.ItCode ");
                    SQLDtl3.AppendLine("Where A.DocNo=@DocNo ");
                    SQLDtl3.AppendLine("Union All ");
                    SQLDtl3.AppendLine("Select C.ItCode, A.DNo, ROUND(A.QtyPackagingUnit,0) AS PalletNo, A.QtyPackagingUnit, ");
                    SQLDtl3.AppendLine("IF((SELECT ParValue FROM TblParameter WHERE ParCode = 'DocTitle')='IOK',G.ForeignName,G.ItName) AS ItName, ");
                    SQLDtl3.AppendLine("G.Length, G.Width, G.Height, ");
                    SQLDtl3.AppendLine("ROUND(ROUND((A.QtyInventory * G.SalesUomCodeConvert12),1)) AS QtySales, ");
                    SQLDtl3.AppendLine("ROUND(A.QtyInventory,4) AS QtyInventory ");
                    SQLDtl3.AppendLine("From TblDRDtl A ");
                    SQLDtl3.AppendLine("Inner Join TblSOContractHdr B On A.SODocNo=B.DocNo ");
                    SQLDtl3.AppendLine("Inner Join TblSOContractDtl C On A.SODocNo=C.DocNo And A.SODNo=C.DNo ");
                    SQLDtl3.AppendLine("Left Join TblAgent F On C.AgtCode=F.AgtCode ");
                    SQLDtl3.AppendLine("Inner Join TblItem G On C.ItCode=G.ItCode ");
                    SQLDtl3.AppendLine("Where A.DocNo=@DocNo ");
                    SQLDtl3.AppendLine(")T ");
                    SQLDtl3.AppendLine("Order By T.ItCode, T.DNo;");

                    cmDtl3.CommandText = SQLDtl3.ToString();

                    Sm.CmParam<String>(ref cmDtl3, "@DocNo", TxtDRDocNo.Text);
                    var drDtl3 = cmDtl3.ExecuteReader();
                    var cDtl3 = Sm.GetOrdinal(drDtl3, new string[]
                {
                    //0
                    "PalletNo" ,

                    //1-5
                    "ItName" ,
                    "Length",
                    "Width",
                    "Height",
                    "QtySales",
                    
                    //6-7
                    "QtyInventory",
                    "QtyPackagingUnit"
                });

                    if (drDtl3.HasRows)
                    {
                        while (drDtl3.Read())
                        {
                            ldtl3.Add(new DOCtDtl3()
                            {
                                PalletNo = Sm.DrStr(drDtl3, cDtl3[0]),
                                ItName = Sm.DrStr(drDtl3, cDtl3[1]),
                                Length = Sm.DrDec(drDtl3, cDtl3[2]),
                                Width = Sm.DrDec(drDtl3, cDtl3[3]),
                                Height = Sm.DrDec(drDtl3, cDtl3[4]),
                                QtySales = Sm.DrDec(drDtl3, cDtl3[5]),
                                QtyInventory = Sm.DrDec(drDtl3, cDtl3[6]),
                            });
                        }
                    }
                    drDtl3.Close();
                }
                myLists.Add(ldtl3);

                #endregion

                #region Detail4

                var cmDtl4 = new MySqlCommand();

                var SQLDtl4 = new StringBuilder();

                using (var cnDtl4 = new MySqlConnection(Gv.ConnectionString))
                {

                    cnDtl4.Open();

                    cmDtl4.Connection = cnDtl4;

                    SQLDtl4.AppendLine("Select A.DocNo, B.ItCode, C.SpHsCode, ");
                    SQLDtl4.AppendLine("IF((SELECT ParValue FROM TblParameter WHERE ParCode = 'DocTitle')='IOK',D.ForeignName,D.ItName) AS ItName, ");
                    SQLDtl4.AppendLine("E.ItGrpName, D.ItCodeInternal, ");
                    SQLDtl4.AppendLine("SUm(B.Qty)Qty, C.NW, C.GW, F.CtItCode, F.CtItCode, C.QtyInventory, C.SectionNo ");
                    SQLDtl4.AppendLine("From TblDoct2Hdr A ");
                    SQLDtl4.AppendLine("Inner Join TblDoct2Dtl B On A.DocNo=B.DocNo  And CancelInd='N'");
                    SQLDtl4.AppendLine("Inner Join ");
                    SQLDtl4.AppendLine("( ");
                    SQLDtl4.AppendLine("	Select X.DocNo  As PLDocNo, X.ItCode, X.SpHsCode, SUM(X.GW) GW, SUM(X.NW) NW, Sum(X.QtyInventory) QtyInventory, X.SectionNo ");
                    SQLDtl4.AppendLine("	From ( ");
                    SQLDtl4.AppendLine("			Select C.DocNo, D.ItCode, D.QtyPackagingUnit, D.Qty, D.QtyInventory, E.SpHsCode, ");
                    SQLDtl4.AppendLine("			(D.QtyPackagingUnit * H.GW) As GW, (D.QtyPackagingUnit * H.NW) As NW, D.SectionNo ");
                    SQLDtl4.AppendLine("			From TblPlHdr C ");
                    SQLDtl4.AppendLine("			Inner Join TblPlDtl D On C.DocNo=D.DocNo ");
                    SQLDtl4.AppendLine("			Inner Join TblSiHdr E On C.SiDocNo=E.DocNo ");
                    SQLDtl4.AppendLine("			Inner Join TblSoDtl F On D.SoDocNo=F.DocNo And D.SoDno=F.DNo ");
                    SQLDtl4.AppendLine("			Inner Join TblSoHdr G On F.DocNo And D.SODocNo = G.DocNo ");
                    SQLDtl4.AppendLine("			Left Join ");
                    SQLDtl4.AppendLine("			( ");
                    SQLDtl4.AppendLine("				Select ItCode, UomCode, Nw, GW From TblItempackagingunit ");
                    SQLDtl4.AppendLine("			)H On D.ItCode = H.ItCode And  F.PackagingUnitUomCode = H.UomCode ");
                    SQLDtl4.AppendLine("			Where C.DocNo =@PLDocNo ");
                    SQLDtl4.AppendLine("		)X ");
                    SQLDtl4.AppendLine("		Group By X.DocNo, X.ItCode, X.SpHsCode, X.SectionNo ");
                    SQLDtl4.AppendLine(")C On A.PLDocNo = C.PLDocNo And B.ItCode = C.ItCode And A.SectionNo=C.SectionNo ");
                    SQLDtl4.AppendLine("Inner Join TblItem D On B.ItCode = D.ItCode ");
                    SQLDtl4.AppendLine("Left Join TblItemGroup E On D.ItGrpCode = E.ItgrpCode ");
                    SQLDtl4.AppendLine("Left Join TblCustomerItem F On D.ItCode = F.ItCode ");
                    SQLDtl4.AppendLine("Where A.PLDocNo =@PLDocNo And A.DocNo=@DocNo  And A.sectionNo=@SectionNo ");
                    SQLDtl4.AppendLine("Group BY  A.DocNo, B.ItCode, C.SpHsCode, D.Itname, E.ItGrpName, D.ItCodeInternal ");

                    cmDtl4.CommandText = SQLDtl4.ToString();

                    Sm.CmParam<String>(ref cmDtl4, "@DocNo", DocNo);
                    Sm.CmParam<String>(ref cmDtl4, "@PLDocNo", PLDocNo);
                    Sm.CmParam<String>(ref cmDtl4, "@SectionNo", mSectionNo);
                    var drDtl4 = cmDtl4.ExecuteReader();
                    var cDtl4 = Sm.GetOrdinal(drDtl4, new string[]
                {
                    "DocNo" ,
                    //1-5
                    "ItCode" ,
                    "SpHsCode",
                    "ItName",
                    "ItGrpName",
                    "ItCodeInternal",
                    //6-10
                    "Qty" ,
                    "NW",
                    "GW",
                    "CtItCode",
                    "QtyInventory"

                });

                    if (drDtl4.HasRows)
                    {
                        int nomor = 0;
                        while (drDtl4.Read())
                        {
                            nomor = nomor + 1;
                            ldtl4.Add(new DOCtDtl4()
                            {
                                nomor = nomor,
                                DocNo = Sm.DrStr(drDtl4, cDtl4[0]),
                                ItCode = Sm.DrStr(drDtl4, cDtl4[1]),
                                SpHsCode = Sm.DrStr(drDtl4, cDtl4[2]),
                                ItName = Sm.DrStr(drDtl4, cDtl4[3]),
                                ItGrpName = Sm.DrStr(drDtl4, cDtl4[4]),
                                ItCodeInternal = Sm.DrStr(drDtl4, cDtl4[5]),
                                Qty = Sm.DrDec(drDtl4, cDtl4[6]),
                                NW = Sm.DrDec(drDtl4, cDtl4[7]),
                                GW = Sm.DrDec(drDtl4, cDtl4[8]),
                                CtItCode = Sm.DrStr(drDtl4, cDtl4[9]),
                                QtyInventory = Sm.DrDec(drDtl4, cDtl4[10])

                            });
                        }
                    }
                    drDtl4.Close();
                }
                myLists.Add(ldtl4);

                #endregion
            }

            string RptName = Sm.GetParameter("FormPrintOutDOCT2");
            if (RptName.Length > 0)
            {
                if (RptName == "IOK")
                {
                    if (TxtDRDocNo.Text.Length > 0)
                    {
                        Sm.PrintReport("DOToCt" + RptName, myLists, TableName, false);
                    }
                    else
                    {
                        switch (parValue)
                        {
                            case 1:
                                Sm.PrintReport("DOToCt", myLists, TableName, false);
                            break;
                            case 2:
                                Sm.PrintReport("DOToCt2", myLists, TableName, false);
                            break;
                            case 3:
                                Sm.PrintReport("DOToCt3", myLists, TableName, false);
                            break;
                        }
                    }
                }
                else if (RptName == "IMS")
                {
                    if (l1.Count > 0) Sm.PrintReport("DOToCtIMSService", myLists, TableName, false);
                    if (l2.Count > 0) Sm.PrintReport("DOToCtIMSInventory", myLists, TableName, false);
                }
                else
                {
                    switch (parValue)
                    {
                        case 1:
                            Sm.PrintReport("DOToCt" + RptName, myLists, TableName, false);
                            break;
                        case 2:
                            Sm.PrintReport("DOToCt2" + RptName, myLists, TableName, false);
                            break;
                        case 3:
                            Sm.PrintReport("DOToCt3" + RptName, myLists, TableName, false);
                            break;
                    }
                }
            }
            else
            {
                switch (parValue)
                {
                    case 1:
                        Sm.PrintReport("DOToCt", myLists, TableName, false);
                        break;
                    case 2:
                        Sm.PrintReport("DOToCt2", myLists, TableName, false);
                        break;
                    case 3:
                        Sm.PrintReport("DOToCt3", myLists, TableName, false);
                        break;
                }
                if (RptName=="MAI" && TxtDocNo.Text.Length > 0) //[UPDATE ARI] 21/4/2017  Printout Release Note
                    Sm.PrintReport("DOExMAI", myLists, TableName, false);
            }
        }

        private bool IsCBD()
        {
            if (TxtDRDocNo.Text.Length > 0)
                return Sm.IsDataExist(
                    "Select 1 from TblDRHdr Where DocNo=@Param And CBDInd='Y';", 
                    TxtDRDocNo.Text);
            return false;
        }

        private void UploadFile(string DocNo)
        {
            if (IsUploadFileNotValid()) return;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile.Text));
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", TxtFile.Text));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);

                PbUpload.Invoke(
                    (MethodInvoker)delegate { PbUpload.Maximum = (int)file.Length; });

                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);
                    PbUpload.Invoke(
                        (MethodInvoker)delegate
                        {
                            PbUpload.Value = (int)file.Position;
                        });
                }
            }
            while (bytesRead != 0);

            Sm.StdMsg(mMsgType.Info, "File uploaded successfully");

            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdateDOCt2File(DocNo, toUpload.Name));
            Sm.ExecCommands(cml);
        }

        private void UploadFile2(string DocNo)
        {
            if (IsUploadFileNotValid2()) return;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile2.Text));
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", TxtFile2.Text));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);

                PbUpload2.Invoke(
                    (MethodInvoker)delegate { PbUpload2.Maximum = (int)file.Length; });

                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);
                    PbUpload2.Invoke(
                        (MethodInvoker)delegate
                        {
                            PbUpload2.Value = (int)file.Position;
                        });
                }
            }
            while (bytesRead != 0);

            Sm.StdMsg(mMsgType.Info, "File uploaded successfully");

            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdateDOCt2File2(DocNo, toUpload.Name));
            Sm.ExecCommands(cml);
        }

        private void UploadFile3(string DocNo)
        {
            if (IsUploadFileNotValid3()) return;

            FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile3.Text));
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", TxtFile3.Text));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);

                PbUpload3.Invoke(
                    (MethodInvoker)delegate { PbUpload3.Maximum = (int)file.Length; });

                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);
                    PbUpload3.Invoke(
                        (MethodInvoker)delegate
                        {
                            PbUpload3.Value = (int)file.Position;
                        });
                }
            }
            while (bytesRead != 0);

            Sm.StdMsg(mMsgType.Info, "File uploaded successfully");

            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add(UpdateDOCt2File3(DocNo, toUpload.Name));
            Sm.ExecCommands(cml);
        }

        private bool IsUploadFileNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                IsFTPClientDataNotValid() ||
                IsFileSizeNotvalid() ||
                IsFileNameAlreadyExisted();
        }

        private bool IsUploadFileNotValid2()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                IsFTPClientDataNotValid2() ||
                IsFileSizeNotvalid2() ||
                IsFileNameAlreadyExisted2();
        }

        private bool IsUploadFileNotValid3()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                IsFTPClientDataNotValid3() ||
                IsFileSizeNotvalid3() ||
                IsFileNameAlreadyExisted3();
        }

        private bool IsFTPClientDataNotValid()
        {

            if (mIsDOCDRAllowToUploadFile && TxtFile.Text.Length > 0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }


            if (mIsDOCDRAllowToUploadFile && TxtFile.Text.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }

            if (mIsDOCDRAllowToUploadFile && TxtFile.Text.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }


            if (mIsDOCDRAllowToUploadFile && TxtFile.Text.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }

            return false;
        }

        private bool IsFTPClientDataNotValid2()
        {

            if (mIsDOCDRAllowToUploadFile && TxtFile2.Text.Length > 0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }

            if (mIsDOCDRAllowToUploadFile && TxtFile2.Text.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }

            if (mIsDOCDRAllowToUploadFile && TxtFile2.Text.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }

            if (mIsDOCDRAllowToUploadFile && TxtFile2.Text.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }
            return false;
        }

        private bool IsFTPClientDataNotValid3()
        {

            if (mIsDOCDRAllowToUploadFile && TxtFile3.Text.Length > 0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }


            if (mIsDOCDRAllowToUploadFile && TxtFile3.Text.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }


            if (mIsDOCDRAllowToUploadFile && TxtFile3.Text.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }


            if (mIsDOCDRAllowToUploadFile && TxtFile3.Text.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }
            return false;
        }

        private bool IsFileSizeNotvalid()
        {
            if (mIsDOCDRAllowToUploadFile && TxtFile.Text.Length > 0)
            {
                FileInfo f = new FileInfo(TxtFile.Text);

                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }


                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File too large to Upload.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileSizeNotvalid2()
        {
            if (mIsDOCDRAllowToUploadFile && TxtFile2.Text.Length > 0)
            {
                FileInfo f = new FileInfo(TxtFile2.Text);
                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }

                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File too large to Upload.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileSizeNotvalid3()
        {
            if (mIsDOCDRAllowToUploadFile && TxtFile3.Text.Length > 0)
            {
                FileInfo f = new FileInfo(TxtFile3.Text);
                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }

                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File too large to Upload.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted()
        {
            if (mIsDOCDRAllowToUploadFile && TxtFile.Text.Length > 0)
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile.Text));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select DocNo From TblDOCt2hdr ");
                SQL.AppendLine("Where FileName=@FileName ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted2()
        {
            if (mIsDOCDRAllowToUploadFile && TxtFile2.Text.Length > 0)
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile2.Text));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select DocNo From TblDOCt2hdr ");
                SQL.AppendLine("Where FileName2=@FileName ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted3()
        {
            if (mIsDOCDRAllowToUploadFile && TxtFile3.Text.Length > 0)
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile3.Text));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select DocNo From TblDOCt2hdr ");
                SQL.AppendLine("Where FileName3=@FileName ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }

        private void DownloadFile(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                this.Text = "Connecting...";
                Application.DoEvents();

                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                this.Text = "Retrieving Information...";
                Application.DoEvents();

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                this.Text = "Downloading File...";
                Application.DoEvents();

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Set up progress bar
                PbUpload.Value = 0;
                PbUpload.Maximum = dataLength;
                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        PbUpload.Value = PbUpload.Maximum;

                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);

                        if (PbUpload.Value + bytesRead <= PbUpload.Maximum)
                        {
                            PbUpload.Value += bytesRead;

                            PbUpload.Refresh();
                            Application.DoEvents();
                        }
                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                MessageBox.Show("There was an error connecting to the FTP Server.");
            }
        }

        private void DownloadFile2(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                this.Text = "Connecting...";
                Application.DoEvents();

                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                this.Text = "Retrieving Information...";
                Application.DoEvents();

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                this.Text = "Downloading File...";
                Application.DoEvents();

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Set up progress bar
                PbUpload2.Value = 0;
                PbUpload2.Maximum = dataLength;
                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        PbUpload2.Value = PbUpload2.Maximum;

                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);

                        if (PbUpload2.Value + bytesRead <= PbUpload2.Maximum)
                        {
                            PbUpload2.Value += bytesRead;

                            PbUpload2.Refresh();
                            Application.DoEvents();
                        }

                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                MessageBox.Show("There was an error connecting to the FTP Server.");
            }
        }

        private void DownloadFile3(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                this.Text = "Connecting...";
                Application.DoEvents();

                FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;

                this.Text = "Retrieving Information...";
                Application.DoEvents();

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.GetFileSize;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                this.Text = "Downloading File...";
                Application.DoEvents();

                //Now get the actual data
                request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.UsePassive = true;
                request.UseBinary = true;
                request.KeepAlive = false;

                //Set up progress bar
                PbUpload3.Value = 0;
                PbUpload3.Maximum = dataLength;
                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        PbUpload3.Value = PbUpload3.Maximum;

                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);

                        if (PbUpload3.Value + bytesRead <= PbUpload3.Maximum)
                        {
                            PbUpload3.Value += bytesRead;

                            PbUpload3.Refresh();
                            Application.DoEvents();
                        }

                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                MessageBox.Show("There was an error connecting to the FTP Server.");
            }
        }


        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void ChkFile_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile.Checked == false)
            {
                TxtFile.EditValue = string.Empty;
            }
        }
        private void ChkFile2_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile2.Checked == false)
            {
                TxtFile2.EditValue = string.Empty;
            }
        }
        private void ChkFile3_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile3.Checked == false)
            {
                TxtFile3.EditValue = string.Empty;
            }
        }

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue2(Sl.SetLueWhsCode), string.Empty);
            ClearGrd1();
            ComputeItemDOQty();
        }

        private void TxtExpDriver_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtExpDriver);
        }

        private void TxtExpPlatNo_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtExpPlatNo);
        }

        private void TxtLocalDocNo_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtLocalDocNo);
        }
        private void TxtInspectionSheet_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtInspectionSheet);
        }
        #endregion

        #region Button Event

        private void BtnDRDocNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmDOCt9Dlg(this, 1));
        }

        private void BtnDRDocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtDRDocNo, "Delivery Request#", false))
            {
                try
                {
                    var f = new FrmDR2(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = TxtDRDocNo.Text;
                    f.ShowDialog();
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
            }
        }

        private void BtnPLDocNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmDOCt9Dlg(this, 2));
        }

        private void BtnPLDocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtPLDocNo, "Packing list#", false))
            {
                try
                {
                    var f = new FrmPL(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = TxtPLDocNo.Text;
                    f.ShowDialog();
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
            }
        }

        private void BtnSAName_Click(object sender, EventArgs e)
        {
            try
            {
                if (!Sm.IsTxtEmpty(TxtSAName, "Shipping name", false))
                    ShowShippingAddressInfo();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnExpVdCode_Click(object sender, EventArgs e)
        {
            try
            {
                if (!Sm.IsTxtEmpty(TxtExpVdCode, "Expedition name", false))
                    ShowExpeditionInfo();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnEmpCode1_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmDOCt9Dlg4(this, 1));
        }

        private void BtnEmpCode2_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmDOCt9Dlg4(this, 2));
        }

        private void BtnEmpCode3_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmDOCt9Dlg4(this, 3));
        }

        private void BtnEmpCode4_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmDOCt9Dlg4(this, 4));
        }

        private void BtnQueueNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmDOCt9Dlg3(this));
        }

        private void BtnKBContractNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmDOCt9Dlg5(this));
        }

        private void BtnPEB_Click(object sender, EventArgs e)
        {
            if (mIsKawasanBerikatEnabled && IsAuthorizedToAccessPEB())
            {
                if (BtnSave.Enabled)
                {
                    Sm.FormShowDialog(new FrmDOCt9Dlg6(this));
                }
            }
        }

        #endregion

        #region Grid Event

        private void Grd2_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            e.DoDefault = false;
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd2, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmSO2(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd2, e.RowIndex, 1);
                f.ShowDialog();
            }
            if (e.ColIndex == 6 && Sm.GetGrdStr(Grd2, e.RowIndex, 5).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd2, e.RowIndex, 5);
                f.ShowDialog();
            }
        }

        private void Grd2_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd2, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmSO2(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd2, e.RowIndex, 1);
                f.ShowDialog();
            }
            if (e.ColIndex == 6 && Sm.GetGrdStr(Grd2, e.RowIndex, 5).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd2, e.RowIndex, 5);
                f.ShowDialog();
            }
        }

        private void Grd3_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1 && Sm.GetGrdStr(Grd3, e.RowIndex, 0).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd3, e.RowIndex, 0);
                f.ShowDialog();
            }
        }

        private void Grd3_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 1 && Sm.GetGrdStr(Grd3, e.RowIndex, 0).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd3, e.RowIndex, 0);
                f.ShowDialog();
            }
        }

        #endregion

        #endregion

        #region Class

        private class ItemService
        {
            public string ItCode { get; set; }
            public string ServiceItemInd { get; set; }
        }

        private class DOCtIMSService
        {
            public string DocNo { get; set; }
            public string PONo { get; set; }
            public string DocDt { get; set; }
            public string CtName { get; set; }
            public string ProjectCode { get; set; }
            public string ProjectName { get; set; }
            public string WorkingDuration { get; set; }
            public string Remark { get; set; }
            public string UserName { get; set; }
            public string CompanyLogo { get; set; }
            public string SJNNo { get; set; }
            public string ResiNo { get; set; }
            public string InspectionNo { get; set; }
        }

        private class DOCtIMSServiceDtl
        {
            public string No { get; set; }
            public string DocNo { get; set; }
            public string DNo { get; set; }
            public string ItName { get; set; }
            public string DeliveryDt { get; set; }
            public decimal Qty { get; set; }
            public string Uom { get; set; }
            public decimal ProgressQty { get; set; }
            public decimal TotalQty { get; set; }
            public decimal OutstandingQty { get; set; }
            public string ItNameDismantle { get; set; }
           
        }

        private class DOCtIMSServiceSummary
        {
            public string DocNo { get; set; }
            public decimal Qty { get; set; }
            public decimal ProgressQty { get; set; }
            public decimal TotalQty { get; set; }
            public decimal OutstandingQty { get; set; }
           
        }

        private class DOCtIMSInventory
        {
            public string DocNo { get; set; }
            public string PONo { get; set; }
            public string DocDt { get; set; }
            public string CtName { get; set; }
            public string ProjectCode { get; set; }
            public string ProjectName { get; set; }
            public string UserName { get; set; }
            public string CompanyLogo { get; set; }
            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyPhone { get; set; }
            public string ResiNo { get; set; }
            public string InspectionSheet { get; set; }
            public string LocalDocNo { get; set; }
            public string Sender { get; set; }
        }

        private class DOCtIMSInventoryDtl
        {
            public string DocNo { get; set; }
            public string DNo { get; set; }
            public string ItName { get; set; }
            public string ItCodeInternal { get; set; }
            public decimal Qty { get; set; }
            public string Uom { get; set; }
            public string Remark { get; set; }
            public string SOCNo { get; set; }
            public string SOCRemark { get; set; }
            public string ItNameDismantle { get; set; }
        }

        #endregion
    }
}
