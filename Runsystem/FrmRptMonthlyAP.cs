﻿#region Update
/*
    22/09/2017 [HAR] tambah info site dan total
    04/10/2017 [TKG] bug fixing perhitungan total 
    17/05/2019 [TKG] bug saat menghitung opening balance.
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using System.Data;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

#endregion

namespace RunSystem
{
    public partial class FrmRptMonthlyAP : RunSystem.FrmBase6
    {
        #region Field

        private string 
            mMenuCode = string.Empty, mAccessInd = string.Empty,
            mAcNo = string.Empty, mYr = string.Empty, mMth = string.Empty;

        #endregion

        #region Constructor

        public FrmRptMonthlyAP(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                GetParameter();
                Sl.SetLueVdCode(ref LueVdCode);
                string CurrentDate = Sm.ServerCurrentDateTime();
                Sl.SetLueYr(LueYr, string.Empty);
                Sl.SetLueMth(LueMth);
                Sm.SetLue(LueYr, Sm.Left(CurrentDate, 4));
                Sm.SetLue(LueMth, CurrentDate.Substring(4, 2));
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            var VendorAcNoAP = Sm.GetParameter("VendorAcNoAP");
            if (VendorAcNoAP.Length > 0)
                mAcNo = Sm.Left(VendorAcNoAP, VendorAcNoAP.Length-1);
        }

        private void Process1(ref List<Result> l, string VdCode)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.VdCode, A.VdName, B.SiteCode, B.SiteName, C.CurCode ");
            SQL.AppendLine("From TblVendor A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select SiteCode, SiteName From TblSite Where ActInd='Y' ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select Null As SiteCode, Null As SiteName ");
            SQL.AppendLine(") B On 1=1 ");
            SQL.AppendLine("Inner Join TblCurrency C On 1=1");
            if (VdCode.Length > 0)
                SQL.AppendLine("Where A.VdCode=@VdCode ");
            SQL.AppendLine("Order By A.VdCode, B.SiteCode, C.CurCode;");

            if (VdCode.Length > 0)
                Sm.CmParam<String>(ref cm, "@VdCode", VdCode);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    "VdCode", 
                    "VdName", "SiteCode", "SiteName", "CurCode" 
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Result()
                        {
                            VdCode = Sm.DrStr(dr, c[0]),
                            VdName = Sm.DrStr(dr, c[1]),
                            SiteCode = Sm.DrStr(dr, c[2]),
                            SiteName = Sm.DrStr(dr, c[3]),
                            CurCode = Sm.DrStr(dr, c[4]),
                            OpeningBalance = 0m,
                            Purchase = 0m,
                            DAmt = 0m,
                            CAmt = 0m,
                            PurchaseReturn = 0m,
                            Payment = 0m,
                            Settlement = 0m,
                            EndingBalance = 0m
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process2(ref List<Result> l, string VdCode, string Dt)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string 
                VdCodeTemp = string.Empty, 
                SiteCodeTemp = string.Empty, 
                CurCodeTemp = string.Empty;
            decimal Amt = 0m;
            int t = 0;

            SQL.AppendLine("Select T1.VdCode, T1.SiteCode, T1.CurCode, ");
            SQL.AppendLine("T1.Amt-IfNull(T2.Amt, 0.000000)-IfNull(T3.Amt, 0.000000)-IfNull(T4.Amt, 0.000000)+IfNull(T5.Amt, 0.000000) As Amt ");
            
            SQL.AppendLine("From (");
            SQL.AppendLine("    Select A.VdCode, A.SiteCode, A.CurCode, ");
            SQL.AppendLine("    Sum(A.Amt+A.TaxAmt-A.Downpayment) As Amt ");
            SQL.AppendLine("    From TblPurchaseInvoiceHdr A ");
            SQL.AppendLine("    Where A.CancelInd='N' ");
            SQL.AppendLine("    And A.DocDt<@Dt ");
            if (VdCode.Length > 0) SQL.AppendLine("     And A.VdCode=@VdCode ");
            SQL.AppendLine("    Group By A.VdCode, A.SiteCode, A.CurCode ");
            SQL.AppendLine(") T1 ");

            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select D.VdCode, D.SiteCode, D.CurCode, Sum(C.Amt) Amt ");
            SQL.AppendLine("    From TblVoucherHdr A ");
            SQL.AppendLine("    Inner Join TblOutgoingPaymentHdr B On A.VoucherRequestDocNo=B.VoucherRequestDocNo ");
            if (VdCode.Length > 0) 
                SQL.AppendLine("        And B.VdCode=@VdCode ");
            SQL.AppendLine("    Inner Join TblOutgoingPaymentDtl C On B.DocNo=C.DocNo And C.InvoiceType='1' ");
            SQL.AppendLine("    Inner Join TblPurchaseInvoiceHdr D On C.InvoiceDocNo=D.DocNo And D.DocDt<@Dt And D.CancelInd='N' ");
            SQL.AppendLine("    Where A.CancelInd='N' ");
            SQL.AppendLine("    And A.DocDt<@Dt ");
            SQL.AppendLine("    Group By D.VdCode, D.SiteCode, D.CurCode ");
            SQL.AppendLine(") T2 On T1.VdCode=T2.VdCode And IfNull(T1.SiteCode, '')=IfNull(T2.SiteCode, '') And T1.CurCode=T2.CurCode ");

            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select B.VdCode, B.SiteCode, B.CurCode, Sum(A.Amt) Amt ");
            SQL.AppendLine("    From TblAPSHdr A ");
            SQL.AppendLine("    Inner Join TblPurchaseInvoiceHdr B On A.PurchaseInvoiceDocNo=B.DocNo And B.DocDt<@Dt And B.CancelInd='N' ");
            SQL.AppendLine("    Where A.CancelInd='N' ");
            SQL.AppendLine("    And A.DocDt<@Dt ");
            if (VdCode.Length > 0)
                SQL.AppendLine("        And B.VdCode=@VdCode ");
            SQL.AppendLine("    Group By B.VdCode, B.SiteCode, B.CurCode ");
            SQL.AppendLine(") T3 On T1.VdCode=T3.VdCode And IfNull(T1.SiteCode, '')=IfNull(T3.SiteCode, '') And T1.CurCode=T3.CurCode ");

            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select VdCode, SiteCode, CurCode, Sum(Amt) Amt ");
            SQL.AppendLine("    From TblPurchaseReturnInvoiceHdr ");
            SQL.AppendLine("    Where CancelInd='N' ");
            SQL.AppendLine("    And DocDt<@Dt ");
            if (VdCode.Length > 0)
                SQL.AppendLine("        And VdCode=@VdCode ");
            SQL.AppendLine("    Group By VdCode, SiteCode, CurCode ");
            SQL.AppendLine(") T4 On T1.VdCode=T4.VdCode And IfNull(T1.SiteCode, '')=IfNull(T4.SiteCode, '') And T1.CurCode=T4.CurCode ");

            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select D.VdCode, D.SiteCode, D.CurCode, Sum(C.Amt) Amt ");
            SQL.AppendLine("    From TblVoucherHdr A ");
            SQL.AppendLine("    Inner Join TblOutgoingPaymentHdr B On A.VoucherRequestDocNo=B.VoucherRequestDocNo ");
            if (VdCode.Length > 0)
                SQL.AppendLine("        And B.VdCode=@VdCode ");
            SQL.AppendLine("    Inner Join TblOutgoingPaymentDtl C On B.DocNo=C.DocNo And C.InvoiceType='2' ");
            SQL.AppendLine("    Inner Join TblPurchaseReturnInvoiceHdr D On C.InvoiceDocNo=D.DocNo And D.CancelInd='N' ");
            SQL.AppendLine("    Where A.CancelInd='N' ");
            SQL.AppendLine("    And A.DocDt<@Dt ");
            SQL.AppendLine("    Group By D.VdCode, D.SiteCode, D.CurCode ");
            SQL.AppendLine(") T5 On T1.VdCode=T5.VdCode And IfNull(T1.SiteCode, '')=IfNull(T5.SiteCode, '') And T1.CurCode=T5.CurCode ");
            SQL.AppendLine("Where T1.Amt-IfNull(T2.Amt, 0.000000)-IfNull(T3.Amt, 0.000000)-IfNull(T4.Amt, 0.000000)+IfNull(T5.Amt, 0.000000)>0.000000 ");
            
            SQL.AppendLine("Order By T1.VdCode, T1.SiteCode, T1.CurCode;");

            Sm.CmParam<String>(ref cm, "@Dt", Dt);
            if (VdCode.Length > 0) Sm.CmParam<String>(ref cm, "@VdCode", VdCode);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "VdCode", "SiteCode", "CurCode", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        VdCodeTemp = Sm.DrStr(dr, c[0]);
                        SiteCodeTemp = Sm.DrStr(dr, c[1]);
                        CurCodeTemp = Sm.DrStr(dr, c[2]);
                        Amt = Sm.DrDec(dr, c[3]);
                        for (int i = t; i < l.Count; i++)
                        {
                            if (
                                string.Compare(l[i].VdCode, VdCodeTemp) == 0 &&
                                string.Compare(l[i].SiteCode, SiteCodeTemp) == 0 &&
                                string.Compare(l[i].CurCode, CurCodeTemp) == 0
                                )
                            {
                                l[i].OpeningBalance = Amt;
                                t = i;
                                break;
                            }
                        }
                    }
                }
                dr.Close();
            }
        }

        private void Process3(ref List<Result> l, string VdCode, string Dt1, string Dt2)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string
                VdCodeTemp = string.Empty,
                SiteCodeTemp = string.Empty,
                CurCodeTemp = string.Empty;
            decimal Amt = 0m;
            int t = 0;

            SQL.AppendLine("Select A.VdCode, A.SiteCode, A.CurCode, ");
            SQL.AppendLine("Sum(A.Amt+A.TaxAmt-A.Downpayment) As Amt ");
            SQL.AppendLine("From TblPurchaseInvoiceHdr A ");
            SQL.AppendLine("Where A.CancelInd='N' ");
            SQL.AppendLine("And A.DocDt>=@Dt1 ");
            SQL.AppendLine("And A.DocDt<@Dt2 ");
            if (VdCode.Length > 0) 
                SQL.AppendLine("And A.VdCode=@VdCode ");
            SQL.AppendLine("Group By A.VdCode, A.SiteCode, A.CurCode ");
            SQL.AppendLine("Order By A.VdCode, A.SiteCode, A.CurCode;");

            Sm.CmParam<String>(ref cm, "@Dt1", Dt1);
            Sm.CmParam<String>(ref cm, "@Dt2", Dt2);
            if (VdCode.Length > 0) Sm.CmParam<String>(ref cm, "@VdCode", VdCode);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "VdCode", "SiteCode", "CurCode", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        VdCodeTemp = Sm.DrStr(dr, c[0]);
                        SiteCodeTemp = Sm.DrStr(dr, c[1]);
                        CurCodeTemp = Sm.DrStr(dr, c[2]);
                        Amt = Sm.DrDec(dr, c[3]);
                        for (int i = t; i < l.Count; i++)
                        {
                            if (
                                string.Compare(l[i].VdCode, VdCodeTemp) == 0 &&
                                string.Compare(l[i].SiteCode, SiteCodeTemp) == 0 &&
                                string.Compare(l[i].CurCode, CurCodeTemp) == 0
                                )
                            {
                                l[i].Purchase = Amt;
                                t = i;
                                break;
                            }
                        }
                    }
                }
                dr.Close();
            }
        }

        private void Process4(ref List<Result> l, string VdCode, string Dt1, string Dt2)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string
                VdCodeTemp = string.Empty,
                SiteCodeTemp = string.Empty,
                CurCodeTemp = string.Empty;
            decimal DAmt = 0m, CAmt = 0m;
            int t = 0;

            SQL.AppendLine("Select A.VdCode, A.SiteCode, A.CurCode, ");
            SQL.AppendLine("Sum(B.DAmt) As DAmt, Sum(B.CAmt) As CAmt ");
            SQL.AppendLine("From TblPurchaseInvoiceHdr A ");
            SQL.AppendLine("Inner Join TblPurchaseInvoiceDtl4 B ");
            SQL.AppendLine("    On A.DocNo=B.DocNo ");
            SQL.AppendLine("    And Length(B.AcNo)>@AcNoLength ");
            SQL.AppendLine("    And Left(B.AcNo, @AcNoLength)=@AcNo ");
            SQL.AppendLine("Where A.CancelInd='N' ");
            SQL.AppendLine("And A.DocDt>=@Dt1 ");
            SQL.AppendLine("And A.DocDt<@Dt2 ");
            if (VdCode.Length > 0) SQL.AppendLine(" And A.VdCode=@VdCode ");
            SQL.AppendLine("Group By A.VdCode, A.SiteCode, A.CurCode ");
            SQL.AppendLine("Order By A.VdCode, A.SiteCode, A.CurCode; ");

            Sm.CmParam<String>(ref cm, "@Dt1", Dt1);
            Sm.CmParam<String>(ref cm, "@Dt2", Dt2);
            Sm.CmParam<String>(ref cm, "@AcNo", mAcNo);
            Sm.CmParam<int>(ref cm, "@AcNoLength", mAcNo.Length);
            if (VdCode.Length > 0) Sm.CmParam<String>(ref cm, "@VdCode", VdCode);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "VdCode", "SiteCode", "CurCode", "DAmt", "CAmt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        VdCodeTemp = Sm.DrStr(dr, c[0]);
                        SiteCodeTemp = Sm.DrStr(dr, c[1]);
                        CurCodeTemp = Sm.DrStr(dr, c[2]);
                        DAmt = Sm.DrDec(dr, c[3]);
                        CAmt = Sm.DrDec(dr, c[4]);
                        for (int i = t; i < l.Count; i++)
                        {
                            if (
                                string.Compare(l[i].VdCode, VdCodeTemp) == 0 &&
                                string.Compare(l[i].SiteCode, SiteCodeTemp) == 0 &&
                                string.Compare(l[i].CurCode, CurCodeTemp) == 0
                                )
                            {
                                l[i].DAmt = DAmt;
                                l[i].CAmt = CAmt;
                                t = i;
                                break;
                            }
                        }
                    }
                }
                dr.Close();
            }
        }

        private void Process5(ref List<Result> l, string VdCode, string Dt1, string Dt2)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string
                VdCodeTemp = string.Empty,
                SiteCodeTemp = string.Empty,
                CurCodeTemp = string.Empty;
            decimal Amt = 0m;
            int t = 0;

            SQL.AppendLine("Select VdCode, SiteCode, CurCode, ");
            SQL.AppendLine("Sum(Amt) As Amt ");
            SQL.AppendLine("From TblPurchaseReturnInvoiceHdr ");
            SQL.AppendLine("Where CancelInd='N' ");
            SQL.AppendLine("And DocDt>=@Dt1 ");
            SQL.AppendLine("And DocDt<@Dt2 ");
            if (VdCode.Length > 0)
                SQL.AppendLine("And VdCode=@VdCode ");
            SQL.AppendLine("Group By VdCode, SiteCode, CurCode ");
            SQL.AppendLine("Order By VdCode, SiteCode, CurCode;");

            Sm.CmParam<String>(ref cm, "@Dt1", Dt1);
            Sm.CmParam<String>(ref cm, "@Dt2", Dt2);
            if (VdCode.Length > 0) Sm.CmParam<String>(ref cm, "@VdCode", VdCode);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "VdCode", "SiteCode", "CurCode", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        VdCodeTemp = Sm.DrStr(dr, c[0]);
                        SiteCodeTemp = Sm.DrStr(dr, c[1]);
                        CurCodeTemp = Sm.DrStr(dr, c[2]);
                        Amt = Sm.DrDec(dr, c[3]);
                        for (int i = t; i < l.Count; i++)
                        {
                            if (
                                string.Compare(l[i].VdCode, VdCodeTemp) == 0 &&
                                string.Compare(l[i].SiteCode, SiteCodeTemp) == 0 &&
                                string.Compare(l[i].CurCode, CurCodeTemp) == 0
                                )
                            {
                                l[i].PurchaseReturn = Amt;
                                t = i;
                                break;
                            }
                        }
                    }
                }
                dr.Close();
            }
        }

        private void Process6(ref List<Result> l, string VdCode, string Dt1, string Dt2)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string
                VdCodeTemp = string.Empty,
                SiteCodeTemp = string.Empty,
                CurCodeTemp = string.Empty;
            decimal Amt = 0m;
            int t = 0;


            SQL.AppendLine("Select VdCode, SiteCode, CurCode, Sum(Amt) Amt ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select D.VdCode, D.SiteCode, D.CurCode, Sum(C.Amt) Amt ");
            SQL.AppendLine("    From TblVoucherHdr A ");
            SQL.AppendLine("    Inner Join TblOutgoingPaymentHdr B On A.VoucherRequestDocNo=B.VoucherRequestDocNo ");
            if (VdCode.Length > 0)
                SQL.AppendLine("        And B.VdCode=@VdCode ");
            SQL.AppendLine("    Inner Join TblOutgoingPaymentDtl C On B.DocNo=C.DocNo And C.InvoiceType='1' ");
            SQL.AppendLine("    Inner Join TblPurchaseInvoiceHdr D On C.InvoiceDocNo=D.DocNo ");
            SQL.AppendLine("    Where A.CancelInd='N' ");
            SQL.AppendLine("    And A.DocDt>=@Dt1 ");
            SQL.AppendLine("    And A.DocDt<@Dt2 ");
            SQL.AppendLine("    Group By D.VdCode, D.SiteCode, D.CurCode ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select D.VdCode, D.SiteCode, D.CurCode, -1.00*Sum(C.Amt) As Amt ");
            SQL.AppendLine("    From TblVoucherHdr A ");
            SQL.AppendLine("    Inner Join TblOutgoingPaymentHdr B On A.VoucherRequestDocNo=B.VoucherRequestDocNo ");
            if (VdCode.Length > 0)
                SQL.AppendLine("        And B.VdCode=@VdCode ");
            SQL.AppendLine("    Inner Join TblOutgoingPaymentDtl C On B.DocNo=C.DocNo And C.InvoiceType='2' ");
            SQL.AppendLine("    Inner Join TblPurchaseReturnInvoiceHdr D On C.InvoiceDocNo=D.DocNo ");
            SQL.AppendLine("    Where A.CancelInd='N' ");
            SQL.AppendLine("    And A.DocDt>=@Dt1 ");
            SQL.AppendLine("    And A.DocDt<@Dt2 ");
            SQL.AppendLine("    Group By D.VdCode, D.SiteCode, D.CurCode ");
            SQL.AppendLine(") T ");
            SQL.AppendLine("Group By VdCode, SiteCode, CurCode ");
            SQL.AppendLine("Order By VdCode, SiteCode, CurCode;");

            Sm.CmParam<String>(ref cm, "@Dt1", Dt1);
            Sm.CmParam<String>(ref cm, "@Dt2", Dt2);
            if (VdCode.Length > 0) Sm.CmParam<String>(ref cm, "@VdCode", VdCode);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "VdCode", "SiteCode", "CurCode", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        VdCodeTemp = Sm.DrStr(dr, c[0]);
                        SiteCodeTemp = Sm.DrStr(dr, c[1]);
                        CurCodeTemp = Sm.DrStr(dr, c[2]);
                        Amt = Sm.DrDec(dr, c[3]);
                        for (int i = t; i < l.Count; i++)
                        {
                            if (
                                string.Compare(l[i].VdCode, VdCodeTemp) == 0 &&
                                string.Compare(l[i].SiteCode, SiteCodeTemp) == 0 &&
                                string.Compare(l[i].CurCode, CurCodeTemp) == 0
                                )
                            {
                                l[i].Payment = Amt;
                                t = i;
                                break;
                            }
                        }
                    }
                }
                dr.Close();
            }
        }

        private void Process7(ref List<Result> l, string VdCode, string Dt1, string Dt2)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string
                VdCodeTemp = string.Empty,
                SiteCodeTemp = string.Empty,
                CurCodeTemp = string.Empty;
            decimal Amt = 0m;
            int t = 0;

            SQL.AppendLine("Select B.VdCode, B.SiteCode, B.CurCode, Sum(A.Amt) Amt ");
            SQL.AppendLine("From TblAPSHdr A ");
            SQL.AppendLine("Inner Join TblPurchaseInvoiceHdr B On A.PurchaseInvoiceDocNo=B.DocNo ");
            SQL.AppendLine("Where A.CancelInd='N' ");
            SQL.AppendLine("And A.DocDt>=@Dt1 ");
            SQL.AppendLine("And A.DocDt<@Dt2 ");
            SQL.AppendLine("Group By B.VdCode, B.SiteCode, B.CurCode ");
            SQL.AppendLine("Order By B.VdCode, B.SiteCode, B.CurCode;");

            Sm.CmParam<String>(ref cm, "@Dt1", Dt1);
            Sm.CmParam<String>(ref cm, "@Dt2", Dt2);
            if (VdCode.Length > 0) Sm.CmParam<String>(ref cm, "@VdCode", VdCode);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "VdCode", "SiteCode", "CurCode", "Amt" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        VdCodeTemp = Sm.DrStr(dr, c[0]);
                        SiteCodeTemp = Sm.DrStr(dr, c[1]);
                        CurCodeTemp = Sm.DrStr(dr, c[2]);
                        Amt = Sm.DrDec(dr, c[3]);
                        for (int i = t; i < l.Count; i++)
                        {
                            if (
                                string.Compare(l[i].VdCode, VdCodeTemp) == 0 &&
                                string.Compare(l[i].SiteCode, SiteCodeTemp) == 0 &&
                                string.Compare(l[i].CurCode, CurCodeTemp) == 0
                                )
                            {
                                l[i].Settlement = Amt;
                                t = i;
                                break;
                            }
                        }
                    }
                }
                dr.Close();
            }
        }

        private void Process8(ref List<Result> l)
        {
            for (int i = l.Count - 1; i >= 0; i--)
            {
                if (l[i].OpeningBalance == 0 &&
                    l[i].Purchase == 0 &&
                    l[i].DAmt == 0 &&
                    l[i].CAmt == 0 &&
                    l[i].PurchaseReturn == 0 &&
                    l[i].Payment == 0 &&
                    l[i].Settlement == 0
                    )
                    l.Remove(l[i]);
                else
                    l[i].EndingBalance =
                        l[i].OpeningBalance +
                        l[i].Purchase -
                        l[i].PurchaseReturn -
                        l[i].Payment -
                        l[i].Settlement;
            }
        }

        private void Process9(ref List<Result> l)
        {
            int Row = 0;
            Grd1.GroupObject.Clear();
            Grd1.Group();
            Grd1.BeginUpdate();
            Grd1.Rows.Count = 0;
            foreach (var i in l.OrderBy(x => x.VdName).ThenBy(x => x.SiteCode).ThenBy(x => x.CurCode))
            {
                Grd1.Rows.Add();
                Grd1.Cells[Row, 0].Value = Row + 1;
                Grd1.Cells[Row, 1].Value = i.VdCode;
                Grd1.Cells[Row, 2].Value = i.VdName;
                Grd1.Cells[Row, 3].Value = i.SiteCode;
                Grd1.Cells[Row, 4].Value = i.SiteName;
                Grd1.Cells[Row, 5].Value = i.CurCode;
                Grd1.Cells[Row, 6].Value = i.OpeningBalance;
                Grd1.Cells[Row, 8].Value = i.Purchase+i.DAmt-i.CAmt;
                Grd1.Cells[Row, 10].Value = (i.DAmt*-1m)+i.CAmt;
                Grd1.Cells[Row, 12].Value = -1m*i.PurchaseReturn;
                Grd1.Cells[Row, 14].Value = -1m * i.Payment;
                Grd1.Cells[Row, 16].Value = -1m * i.Settlement;
                Grd1.Cells[Row, 18].Value = i.EndingBalance;
                Row++;
            }

            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 6, 8, 10, 12, 14, 16, 18 });

            Grd1.EndUpdate();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 19;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-5
                        "Vendor's Code",
                        "Vendor's Name",
                        "Site Code",
                        "Site Name",
                        "Currency",

                        //6-10
                        "Opening"+Environment.NewLine+"Balance",
                        "",
                        "Purchase",
                        "",
                        "Cost/Disc"+Environment.NewLine+"/etc",

                        //11-15
                        "",
                        "Purchase"+Environment.NewLine+"Return",
                        "",
                        "Payment",
                        "",

                        //16-18
                        "Settlement",
                        "",
                        "Ending"+Environment.NewLine+"Balance"
                    },
                    new int[] 
                    {
                        //0
                        50, 

                        //1-5
                        100, 200, 0, 200, 60, 
                        
                        //6-10
                        130, 20, 130, 20, 130, 
                        
                        //11-15
                        20, 130, 20, 130, 20, 
                        
                        //16-18
                        130, 20, 130
                    }
                );
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 8, 10, 12, 14, 16, 18 });
            Sm.GrdColButton(Grd1, new int[] { 7, 9, 11, 13, 15, 17 });
            Sm.GrdFormatDec(Grd1, new int[] { 6, 8, 10, 12, 14, 16, 18 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3 }, false);
            Sm.SetGrdProperty(Grd1, false);
            Grd1.Rows.AutoHeight();
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            mYr = string.Empty;
            mMth = string.Empty;

            Sm.ClearGrd(Grd1, false);
            if (Sm.IsLueEmpty(LueYr, "Year") || Sm.IsLueEmpty(LueMth, "Month")) return;

            mYr = Sm.GetLue(LueYr);
            mMth = Sm.GetLue(LueMth);

            Cursor.Current = Cursors.WaitCursor;

            var l = new List<Result>();

            var cm = new MySqlCommand();

            var VdCode = Sm.GetLue(LueVdCode);
            var Yr = Sm.GetLue(LueYr);
            var Mth = Sm.GetLue(LueMth);
            var Yr2 = Yr;
            var Mth2 = Mth;

            if (Mth == "12")
            {
                Yr2 = Yr + 1;
                Mth2 = "01";
            }
            else
                Mth2 = Sm.Right("0"+(decimal.Parse(Mth)+1).ToString(), 2);

            var Dt1 = string.Concat(Yr, Mth, "01");
            var Dt2 = string.Concat(Yr2, Mth2, "01");

            try
            {
                Process1(ref l, VdCode);
                if (l.Count > 0)
                {
                    Process2(ref l, VdCode, Dt1);
                    Process3(ref l, VdCode, Dt1, Dt2);
                    Process4(ref l, VdCode, Dt1, Dt2);
                    Process5(ref l, VdCode, Dt1, Dt2);
                    Process6(ref l, VdCode, Dt1, Dt2);
                    Process7(ref l, VdCode, Dt1, Dt2);
                    Process8(ref l);
                    Process9(ref l);
                }
                else
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                l.Clear();
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[]{ 7, 9, 11, 13, 15, 17}, e.ColIndex) && 
                Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length > 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) 
                    Sm.FormShowDialog(new FrmRptMonthlyAPDlg(this, e.RowIndex, GetDocType(e.ColIndex), mYr, mMth, mAcNo));

            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 7, 9, 11, 13, 15, 17 }, e.ColIndex) && 
                Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length > 0)
                Sm.FormShowDialog(new FrmRptMonthlyAPDlg(this, e.RowIndex, GetDocType(e.ColIndex), mYr, mMth, mAcNo));
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        private byte GetDocType(int c)
        {
            if (c == 7) return 0;
            if (c == 9) return 1;
            if (c == 11) return 2;
            if (c == 13) return 3;
            if (c == 15) return 4;
            if (c == 17) return 5;
            return 11;
        }

        #endregion

        #endregion

        #region Event

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue1(Sl.SetLueVdCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkVdCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Vendor");
        }

       

        #endregion

        #region Class

        private class Result
        {
            public string VdCode { get; set; }
            public string VdName { get; set; }
            public string SiteCode { get; set; }
            public string SiteName { get; set; }
            public string CurCode { get; set; }
            public decimal OpeningBalance { get; set; }
            public decimal Purchase { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
            public decimal PurchaseReturn { get; set; }
            public decimal Payment { get; set; }
            public decimal Settlement { get; set; }
            public decimal EndingBalance { get; set; }
        }

        #endregion
    }
}
