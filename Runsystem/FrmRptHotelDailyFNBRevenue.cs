﻿#region Update
/*
    26/09/2017 [TKG] reporting Hotel's Daily FNB Revenue.
    29/09/2017 [WED] tambah printout
    03/10/2017 [TKG] menggunakan periode
    05/10/2017 [TKG] menggunakan COA 4.1
    11/10/2017 [TKG] perhitungan menggunakan data tgl 1 hari sebelumnya.
    17/10/2017 [ARI] tambah kolom month
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptHotelDailyFNBRevenue : RunSystem.FrmBase6
    {
        #region Field

        private string 
            mMenuCode = string.Empty, 
            mAccessInd = string.Empty;

        #endregion

        #region Constructor

        public FrmRptHotelDailyFNBRevenue(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                Sm.SetDefaultPeriod(ref DteDt1, ref DteDt2, 0);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        private void SetGrd()
        {
            Grd1.Cols.Count = 13;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Date",
                        "Month",
                        "Name",
                        "Posting",
                        "Section",
                       
                        //6-10
                        "COA",
                        "Revenue",
                        "Service"+Environment.NewLine+"Charge",
                        "Tax",
                        "Cost",

                        //11-12
                        "Profit",
                        "Profit %"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        80, 100, 200, 200, 200, 
                        
                        //6-10
                        200, 120, 120, 120, 120,
                        
                        //11-12
                        120, 120
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 1 });
            Sm.GrdFormatDec(Grd1, new int[] { 7, 8, 9, 10, 11, 12 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2  }, false);
            Sm.SetGrdProperty(Grd1, false);
        }
        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
               Sm.IsDteEmpty(DteDt1, "Start date") ||
               Sm.IsDteEmpty(DteDt2, "End date") ||
               Sm.IsFilterByDateInvalid(ref DteDt1, ref DteDt2)
               ) return;


            var mlResult = new List<Result>();

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                Process1(ref mlResult);
                if (mlResult.Count > 0) 
                    Process2(ref mlResult);
                else
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                mlResult.Clear();
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Additional Method

        private void Process1(ref List<Result> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Date, Month, Name, Item_Type, Posting, Section, ");
            SQL.AppendLine("COA_ID, COA_Desc, ");
            SQL.AppendLine("Sum(Amt) As Amt, ");
            SQL.AppendLine("Sum(ServiceCharge) As ServiceCharge, ");
            SQL.AppendLine("Sum(Tax) As Tax, ");
            SQL.AppendLine("Sum(Cost) As Cost ");
            SQL.AppendLine("From ( ");

            SQL.AppendLine("    Select Date, date_format(date,'%M')As Month, Name, Item_Type, Posting, Section, COA_ID, COA_Desc, ");
            SQL.AppendLine("    Sum(IfNull(Amount, 0.00)*IfNull(Currency_Rate, 0.00)) As Amt, ");
            SQL.AppendLine("    Sum(IfNull(service_tax_amt, 0.00)*IfNull(Currency_Rate, 0.00)) As ServiceCharge, ");
            SQL.AppendLine("    Sum(IfNull(local_tax_amt, 0.00)*IfNull(Currency_Rate, 0.00)) As Tax, ");
            SQL.AppendLine("    Sum(IfNull(cost, 0.00)*IfNull(Currency_Rate, 0.00)) As Cost ");
            SQL.AppendLine("    From TblTransactions ");
            SQL.AppendLine("    Where Date Between @Dt1 And @Dt2 ");
            SQL.AppendLine("    And COA_ID Like '4.1%' ");
            SQL.AppendLine("    And Find_In_Set(");
            SQL.AppendLine("        IfNull(Posting, ''), ");
            SQL.AppendLine("        IfNull((Select ParValue From TblParameter Where ParCode='HotelSectionForFNB'), '') ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("    Group By Date, Name, Item_Type, Posting, Section, COA_ID, COA_Desc ");

            SQL.AppendLine(") T ");
            SQL.AppendLine("Group By Date, Name, Item_Type, Posting, Section, COA_ID, COA_Desc; ");


            //Sm.CmParamDt(ref cm, "@Dt1", Sm.GetDte(DteDt1));
            //Sm.CmParamDt(ref cm, "@Dt2", Sm.GetDte(DteDt2));

            var Dt1 = Sm.FormatDate(Sm.ConvertDate(Sm.GetDte(DteDt1)).AddDays(-1));
            var Dt2 = Sm.FormatDate(Sm.ConvertDate(Sm.GetDte(DteDt2)).AddDays(-1));

            Sm.CmParamDt(ref cm, "@Dt1", Dt1);
            Sm.CmParamDt(ref cm, "@Dt2", Dt2);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                { 
                    //0
                    "Date",

                    //1-5
                    "Month",
                    "Name", 
                    "Item_Type", 
                    "Posting", 
                    "Section", 
                   
                    //6-10
                    "COA_ID", 
                    "COA_Desc", 
                    "Amt", 
                    "ServiceCharge", 
                    "Tax", 

                    //11
                    "Cost"
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new Result()
                        {
                            Dt = Sm.DrStr(dr, c[0]),
                            Month = Sm.DrStr(dr, c[1]),
                            name = Sm.DrStr(dr, c[2]),
                            item_type = Sm.DrStr(dr, c[3]),
                            posting = Sm.DrStr(dr, c[4]),
                            section = Sm.DrStr(dr, c[5]),
                            coa_id = Sm.DrStr(dr, c[6]),
                            coa_desc = Sm.DrStr(dr, c[7]),
                            Amt = Sm.DrDec(dr, c[8]),
                            ServiceCharge = Sm.DrDec(dr, c[9]),
                            Tax = Sm.DrDec(dr, c[10]),
                            Cost = Sm.DrDec(dr, c[11])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void Process2(ref List<Result> l)
        {
            iGRow r;
            var Name = string.Empty;
            Grd1.BeginUpdate();
            for (var i = 0; i < l.Count; i++)
            {
                r = Grd1.Rows.Add();
                r.Cells[0].Value = i + 1;
                r.Cells[1].Value = Sm.ConvertDate(l[i].Dt).AddDays(1);
                Name = string.Empty;
                if (l[i].name.Length > 0)
                {
                    if (l[i].item_type.Length > 0)
                        Name = string.Concat(l[i].name, " (", l[i].item_type, ")");
                    else
                        Name = l[i].name;
                }
                else
                {
                    if (l[i].item_type.Length > 0) Name = l[i].item_type;
                }
                r.Cells[2].Value = l[i].Month;
                r.Cells[3].Value = Name;
                r.Cells[4].Value = l[i].posting;
                r.Cells[5].Value = l[i].section;
                r.Cells[6].Value = string.Concat(l[i].coa_id, " (", l[i].coa_desc, ")");
                r.Cells[7].Value = l[i].Amt;
                r.Cells[8].Value = l[i].ServiceCharge;
                r.Cells[9].Value = l[i].Tax;
                r.Cells[10].Value = l[i].Cost;
                r.Cells[11].Value = l[i].Amt - l[i].Cost;
                r.Cells[12].Value = l[i].Cost==0m?0m:((l[i].Amt - l[i].Cost) / l[i].Cost) * 100m;
                
            }
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 7, 8, 9, 10, 11, 12 });
            Grd1.EndUpdate();
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "No Data displayed.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueInvalid()
        {
            if (Grd1.Rows.Count > 0)
            {
                if (Sm.GetGrdStr(Grd1, 0, 1).Length <= 0)
                {
                    Sm.StdMsg(mMsgType.Warning, "No Data displayed.");
                    return true;
                }
            }
            return false;
        }

        override protected void PrintData()
        {
            try
            {
                ParPrint();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void ParPrint()
        {
            if (
               Sm.IsDteEmpty(DteDt1, "Start date") ||
               Sm.IsDteEmpty(DteDt2, "End date") ||
               Sm.IsFilterByDateInvalid(ref DteDt1, ref DteDt2)
               ) return;

            if (IsGrdEmpty() || IsGrdValueInvalid() || Sm.StdMsgYN("Print", "") == DialogResult.No) return;

            string[] TableName = { "ResultHeader", "Result" };

            var l = new List<ResultHeader>();
            var mlResult = new List<Result>();
            var lDtl = new List<ResultDetail>();

            List<IList> myLists = new List<IList>();

            #region Header

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select @CompanyLogo As CompanyLogo,(Select ParValue From tblparameter Where ParCode='ReportTitle1')As CompanyName, ");
            SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle4')As CompanyAddress, ");
            SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle5')As CompanyPhone, ");
            SQL.AppendLine("(Select MenuDesc From TblMenu Where MenuCode = @MenuCode) As MenuDesc ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                         //0
                         "CompanyLogo",

                         //1-3
                         "CompanyName",
                         "CompanyAddress",
                         "CompanyPhone",
                         "MenuDesc"
                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new ResultHeader()
                        {
                            CompanyLogo = Sm.DrStr(dr, c[0]),
                            CompanyName = Sm.DrStr(dr, c[1]),
                            CompanyAddress = Sm.DrStr(dr, c[2]),
                            CompanyPhone = Sm.DrStr(dr, c[3]),
                            MenuDesc = Sm.DrStr(dr, c[4]),
                            DocDt = string.Concat(
                               String.Format("{0:dd/MMM/yyyy}", Sm.ConvertDateTime(Sm.GetDte(DteDt1))),
                               " - ",
                               String.Format("{0:dd/MMM/yyyy}", Sm.ConvertDateTime(Sm.GetDte(DteDt2)))
                               ),
                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
                        });
                    }
                }
                dr.Close();
            }

            myLists.Add(l);

            #endregion

            #region Detail

            Process1(ref mlResult);
            if (mlResult.Count > 0)
            {
                mlResult.ForEach(i =>
                {
                    var Name = string.Empty;
                    if (i.name.Length > 0)
                    {
                        if (i.item_type.Length > 0)
                            Name = string.Concat(i.name, " (", i.item_type, ")");
                        else
                            Name = i.name;
                    }
                    else
                    {
                        if (i.item_type.Length > 0) Name = i.item_type;
                    }

                    lDtl.Add(new ResultDetail()
                    {
                        name = Name,
                        item_type = i.item_type,
                        posting = i.posting,
                        section = i.section,
                        coa_id = i.coa_id,
                        coa_desc = string.Concat(i.coa_id, " (", i.coa_desc, ")"),
                        Amt = i.Amt,
                        ServiceCharge = i.ServiceCharge,
                        Tax = i.Tax,
                        Cost = i.Cost,
                        Profit = (i.Amt - i.Cost),
                        ProfitPercent = i.Cost == 0m ? 0m : ((i.Amt - i.Cost) / i.Cost) * 100m
                    });

                });
            }

            myLists.Add(lDtl);

            #endregion

            Sm.PrintReport("HotelDailyFNBRevenue", myLists, TableName, false);
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void DteDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDt2).Length == 0) DteDt2.EditValue = DteDt1.EditValue;
            Sm.ClearGrd(Grd1, false);
        }

        private void DteDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.ClearGrd(Grd1, false);
        }

        #endregion

        #endregion

        #region Class

        private class Result
        {
            public string Dt { get; set; }
            public string Month { get; set; }
            public string name { get; set; }
            public string item_type { get; set; }
            public string posting { get; set; }
            public string section { get; set; }
            public string coa_id { get; set; }
            public string coa_desc { get; set; }
            public decimal Amt { get; set; }
            public decimal ServiceCharge { get; set; }
            public decimal Tax { get; set; }
            public decimal Cost { get; set; }
        }

        private class ResultHeader
        {
            public string CompanyLogo { get; set; }
            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyPhone { get; set; }
            public string MenuDesc { get; set; }
            public string DocDt { get; set; }
            public string PrintBy { get; set; }
        }

        private class ResultDetail
        {
            public string name { get; set; }
            public string item_type { get; set; }
            public string posting { get; set; }
            public string section { get; set; }
            public string coa_id { get; set; }
            public string coa_desc { get; set; }
            public decimal Amt { get; set; }
            public decimal ServiceCharge { get; set; }
            public decimal Tax { get; set; }
            public decimal Cost { get; set; }
            public decimal Profit { get; set; }
            public decimal ProfitPercent { get; set; }
        }

        #endregion
    }
}
