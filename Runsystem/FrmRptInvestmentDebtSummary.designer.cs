﻿namespace RunSystem
{
    partial class FrmRptInvestmentDebtSummary
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.TxtInvestmentCode = new DevExpress.XtraEditors.TextEdit();
            this.ChkInvestmentCode = new DevExpress.XtraEditors.CheckEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtTotalGainLoss = new DevExpress.XtraEditors.TextEdit();
            this.ChkInvestmentType = new DevExpress.XtraEditors.CheckEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.LueInvestmentType = new DevExpress.XtraEditors.LookUpEdit();
            this.ChkBankAcCode = new DevExpress.XtraEditors.CheckEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.LueBankAcCode = new DevExpress.XtraEditors.LookUpEdit();
            this.DteValuationDt = new DevExpress.XtraEditors.DateEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtInvestmentName = new DevExpress.XtraEditors.TextEdit();
            this.ChkInvestmentName = new DevExpress.XtraEditors.CheckEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtTotalInterestReceivable = new DevExpress.XtraEditors.TextEdit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInvestmentCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkInvestmentCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalGainLoss.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkInvestmentType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueInvestmentType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkBankAcCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBankAcCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteValuationDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteValuationDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInvestmentName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkInvestmentName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalInterestReceivable.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(780, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnExcel.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnPrint.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            // 
            // BtnRefresh
            // 
            this.BtnRefresh.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRefresh.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnRefresh.Appearance.Options.UseBackColor = true;
            this.BtnRefresh.Appearance.Options.UseFont = true;
            this.BtnRefresh.Appearance.Options.UseForeColor = true;
            this.BtnRefresh.Appearance.Options.UseTextOptions = true;
            this.BtnRefresh.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnRefresh.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.TxtTotalInterestReceivable);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.TxtInvestmentName);
            this.panel2.Controls.Add(this.ChkInvestmentName);
            this.panel2.Controls.Add(this.DteValuationDt);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.ChkBankAcCode);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.LueBankAcCode);
            this.panel2.Controls.Add(this.ChkInvestmentType);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.LueInvestmentType);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.TxtTotalGainLoss);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.TxtInvestmentCode);
            this.panel2.Controls.Add(this.ChkInvestmentCode);
            this.panel2.Size = new System.Drawing.Size(780, 154);
            // 
            // BtnChart
            // 
            this.BtnChart.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnChart.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnChart.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnChart.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnChart.Appearance.Options.UseBackColor = true;
            this.BtnChart.Appearance.Options.UseFont = true;
            this.BtnChart.Appearance.Options.UseForeColor = true;
            this.BtnChart.Appearance.Options.UseTextOptions = true;
            this.BtnChart.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // Grd1
            // 
            this.Grd1.DefaultAutoGroupRow.Height = 21;
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.FrozenArea.ColCount = 3;
            this.Grd1.FrozenArea.SortFrozenRows = true;
            this.Grd1.GridLines.Mode = TenTec.Windows.iGridLib.iGGridLinesMode.None;
            this.Grd1.GroupBox.BackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintBackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Grd1.GroupBox.Visible = true;
            this.Grd1.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 25;
            this.Grd1.Header.UseXPStyles = false;
            this.Grd1.ScrollBarSettings.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.SearchAsType.Mode = TenTec.Windows.iGridLib.iGSearchAsTypeMode.Seek;
            this.Grd1.SearchAsType.SearchCol = null;
            this.Grd1.Size = new System.Drawing.Size(780, 319);
            this.Grd1.TabIndex = 37;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.VScrollBar.CustomButtons.AddRange(new TenTec.Windows.iGridLib.iGScrollBarCustomButton[] {
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.ExpandAll, -1, null, true, null),
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.CollapseAll, -1, null, true, null)});
            this.Grd1.VScrollBar.Visibility = TenTec.Windows.iGridLib.iGScrollBarVisibility.Always;
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel3
            // 
            this.panel3.Location = new System.Drawing.Point(0, 154);
            this.panel3.Size = new System.Drawing.Size(780, 319);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(55, 4);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(102, 14);
            this.label2.TabIndex = 8;
            this.label2.Text = "Investment Code";
            // 
            // TxtInvestmentCode
            // 
            this.TxtInvestmentCode.EnterMoveNextControl = true;
            this.TxtInvestmentCode.Location = new System.Drawing.Point(159, 2);
            this.TxtInvestmentCode.Name = "TxtInvestmentCode";
            this.TxtInvestmentCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtInvestmentCode.Properties.Appearance.Options.UseFont = true;
            this.TxtInvestmentCode.Properties.MaxLength = 250;
            this.TxtInvestmentCode.Size = new System.Drawing.Size(360, 20);
            this.TxtInvestmentCode.TabIndex = 9;
            this.TxtInvestmentCode.Validated += new System.EventHandler(this.TxtInvestmentCode_Validated);
            // 
            // ChkInvestmentCode
            // 
            this.ChkInvestmentCode.Location = new System.Drawing.Point(522, 0);
            this.ChkInvestmentCode.Name = "ChkInvestmentCode";
            this.ChkInvestmentCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkInvestmentCode.Properties.Appearance.Options.UseFont = true;
            this.ChkInvestmentCode.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkInvestmentCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkInvestmentCode.Properties.Caption = " ";
            this.ChkInvestmentCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkInvestmentCode.Size = new System.Drawing.Size(19, 22);
            this.ChkInvestmentCode.TabIndex = 10;
            this.ChkInvestmentCode.ToolTip = "Remove filter";
            this.ChkInvestmentCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkInvestmentCode.ToolTipTitle = "Run System";
            this.ChkInvestmentCode.CheckedChanged += new System.EventHandler(this.ChkInvestmentCode_CheckedChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(8, 130);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(149, 14);
            this.label3.TabIndex = 24;
            this.label3.Text = "Total Unrealized Gain/Loss";
            // 
            // TxtTotalGainLoss
            // 
            this.TxtTotalGainLoss.EnterMoveNextControl = true;
            this.TxtTotalGainLoss.Location = new System.Drawing.Point(159, 128);
            this.TxtTotalGainLoss.Name = "TxtTotalGainLoss";
            this.TxtTotalGainLoss.Properties.Appearance.BackColor = System.Drawing.Color.Silver;
            this.TxtTotalGainLoss.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotalGainLoss.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotalGainLoss.Properties.Appearance.Options.UseFont = true;
            this.TxtTotalGainLoss.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotalGainLoss.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotalGainLoss.Properties.MaxLength = 250;
            this.TxtTotalGainLoss.Properties.ReadOnly = true;
            this.TxtTotalGainLoss.Size = new System.Drawing.Size(360, 20);
            this.TxtTotalGainLoss.TabIndex = 25;
            // 
            // ChkInvestmentType
            // 
            this.ChkInvestmentType.Location = new System.Drawing.Point(522, 43);
            this.ChkInvestmentType.Name = "ChkInvestmentType";
            this.ChkInvestmentType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkInvestmentType.Properties.Appearance.Options.UseFont = true;
            this.ChkInvestmentType.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkInvestmentType.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkInvestmentType.Properties.Caption = " ";
            this.ChkInvestmentType.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkInvestmentType.Size = new System.Drawing.Size(19, 22);
            this.ChkInvestmentType.TabIndex = 16;
            this.ChkInvestmentType.ToolTip = "Remove filter";
            this.ChkInvestmentType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkInvestmentType.ToolTipTitle = "Run System";
            this.ChkInvestmentType.CheckedChanged += new System.EventHandler(this.ChkInvestmentType_CheckedChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(55, 47);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(102, 14);
            this.label5.TabIndex = 14;
            this.label5.Text = "Investment Type";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueInvestmentType
            // 
            this.LueInvestmentType.EnterMoveNextControl = true;
            this.LueInvestmentType.Location = new System.Drawing.Point(159, 44);
            this.LueInvestmentType.Margin = new System.Windows.Forms.Padding(5);
            this.LueInvestmentType.Name = "LueInvestmentType";
            this.LueInvestmentType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInvestmentType.Properties.Appearance.Options.UseFont = true;
            this.LueInvestmentType.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInvestmentType.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueInvestmentType.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInvestmentType.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueInvestmentType.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInvestmentType.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueInvestmentType.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueInvestmentType.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueInvestmentType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueInvestmentType.Properties.DropDownRows = 20;
            this.LueInvestmentType.Properties.NullText = "[Empty]";
            this.LueInvestmentType.Properties.PopupWidth = 500;
            this.LueInvestmentType.Size = new System.Drawing.Size(360, 20);
            this.LueInvestmentType.TabIndex = 15;
            this.LueInvestmentType.ToolTip = "F4 : Show/hide list";
            this.LueInvestmentType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueInvestmentType.EditValueChanged += new System.EventHandler(this.LueInvestmentType_EditValueChanged);
            // 
            // ChkBankAcCode
            // 
            this.ChkBankAcCode.Location = new System.Drawing.Point(522, 63);
            this.ChkBankAcCode.Name = "ChkBankAcCode";
            this.ChkBankAcCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkBankAcCode.Properties.Appearance.Options.UseFont = true;
            this.ChkBankAcCode.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkBankAcCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkBankAcCode.Properties.Caption = " ";
            this.ChkBankAcCode.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkBankAcCode.Size = new System.Drawing.Size(19, 22);
            this.ChkBankAcCode.TabIndex = 19;
            this.ChkBankAcCode.ToolTip = "Remove filter";
            this.ChkBankAcCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkBankAcCode.ToolTipTitle = "Run System";
            this.ChkBankAcCode.CheckedChanged += new System.EventHandler(this.ChkBankAcCode_CheckedChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(7, 67);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(150, 14);
            this.label7.TabIndex = 17;
            this.label7.Text = "Investment Bank Account";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueBankAcCode
            // 
            this.LueBankAcCode.EnterMoveNextControl = true;
            this.LueBankAcCode.Location = new System.Drawing.Point(159, 65);
            this.LueBankAcCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueBankAcCode.Name = "LueBankAcCode";
            this.LueBankAcCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankAcCode.Properties.Appearance.Options.UseFont = true;
            this.LueBankAcCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankAcCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueBankAcCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankAcCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueBankAcCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankAcCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueBankAcCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankAcCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueBankAcCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueBankAcCode.Properties.DropDownRows = 20;
            this.LueBankAcCode.Properties.NullText = "[Empty]";
            this.LueBankAcCode.Properties.PopupWidth = 500;
            this.LueBankAcCode.Size = new System.Drawing.Size(360, 20);
            this.LueBankAcCode.TabIndex = 18;
            this.LueBankAcCode.ToolTip = "F4 : Show/hide list";
            this.LueBankAcCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueBankAcCode.EditValueChanged += new System.EventHandler(this.LueBankAcCode_EditValueChanged);
            // 
            // DteValuationDt
            // 
            this.DteValuationDt.EditValue = null;
            this.DteValuationDt.EnterMoveNextControl = true;
            this.DteValuationDt.Location = new System.Drawing.Point(159, 86);
            this.DteValuationDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteValuationDt.Name = "DteValuationDt";
            this.DteValuationDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteValuationDt.Properties.Appearance.Options.UseFont = true;
            this.DteValuationDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteValuationDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteValuationDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteValuationDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteValuationDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteValuationDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteValuationDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteValuationDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteValuationDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteValuationDt.Size = new System.Drawing.Size(360, 20);
            this.DteValuationDt.TabIndex = 21;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(70, 88);
            this.label8.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(87, 14);
            this.label8.TabIndex = 20;
            this.label8.Text = "Valuation Date";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(52, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(105, 14);
            this.label1.TabIndex = 11;
            this.label1.Text = "Investment Name";
            // 
            // TxtInvestmentName
            // 
            this.TxtInvestmentName.EnterMoveNextControl = true;
            this.TxtInvestmentName.Location = new System.Drawing.Point(159, 23);
            this.TxtInvestmentName.Name = "TxtInvestmentName";
            this.TxtInvestmentName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtInvestmentName.Properties.Appearance.Options.UseFont = true;
            this.TxtInvestmentName.Properties.MaxLength = 250;
            this.TxtInvestmentName.Size = new System.Drawing.Size(360, 20);
            this.TxtInvestmentName.TabIndex = 12;
            this.TxtInvestmentName.Validated += new System.EventHandler(this.TxtInvestmentName_Validated);
            // 
            // ChkInvestmentName
            // 
            this.ChkInvestmentName.Location = new System.Drawing.Point(522, 21);
            this.ChkInvestmentName.Name = "ChkInvestmentName";
            this.ChkInvestmentName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkInvestmentName.Properties.Appearance.Options.UseFont = true;
            this.ChkInvestmentName.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkInvestmentName.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkInvestmentName.Properties.Caption = " ";
            this.ChkInvestmentName.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkInvestmentName.Size = new System.Drawing.Size(19, 22);
            this.ChkInvestmentName.TabIndex = 13;
            this.ChkInvestmentName.ToolTip = "Remove filter";
            this.ChkInvestmentName.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkInvestmentName.ToolTipTitle = "Run System";
            this.ChkInvestmentName.CheckedChanged += new System.EventHandler(this.ChkInvestmentName_CheckedChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(8, 109);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(148, 14);
            this.label4.TabIndex = 22;
            this.label4.Text = " Total Interest Receivable";
            // 
            // TxtTotalInterestReceivable
            // 
            this.TxtTotalInterestReceivable.EnterMoveNextControl = true;
            this.TxtTotalInterestReceivable.Location = new System.Drawing.Point(159, 107);
            this.TxtTotalInterestReceivable.Name = "TxtTotalInterestReceivable";
            this.TxtTotalInterestReceivable.Properties.Appearance.BackColor = System.Drawing.Color.Silver;
            this.TxtTotalInterestReceivable.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotalInterestReceivable.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotalInterestReceivable.Properties.Appearance.Options.UseFont = true;
            this.TxtTotalInterestReceivable.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotalInterestReceivable.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotalInterestReceivable.Properties.MaxLength = 250;
            this.TxtTotalInterestReceivable.Properties.ReadOnly = true;
            this.TxtTotalInterestReceivable.Size = new System.Drawing.Size(360, 20);
            this.TxtTotalInterestReceivable.TabIndex = 23;
            // 
            // FrmRptInvestmentDebtSummary
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(850, 473);
            this.Name = "FrmRptInvestmentDebtSummary";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TxtInvestmentCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkInvestmentCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalGainLoss.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkInvestmentType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueInvestmentType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkBankAcCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBankAcCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteValuationDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteValuationDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInvestmentName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkInvestmentName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalInterestReceivable.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.TextEdit TxtInvestmentCode;
        private DevExpress.XtraEditors.CheckEdit ChkInvestmentCode;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.TextEdit TxtTotalGainLoss;
        private DevExpress.XtraEditors.CheckEdit ChkInvestmentType;
        private System.Windows.Forms.Label label5;
        private DevExpress.XtraEditors.LookUpEdit LueInvestmentType;
        private DevExpress.XtraEditors.CheckEdit ChkBankAcCode;
        private System.Windows.Forms.Label label7;
        private DevExpress.XtraEditors.LookUpEdit LueBankAcCode;
        internal DevExpress.XtraEditors.DateEdit DteValuationDt;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.TextEdit TxtTotalInterestReceivable;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.TextEdit TxtInvestmentName;
        private DevExpress.XtraEditors.CheckEdit ChkInvestmentName;
    }
}