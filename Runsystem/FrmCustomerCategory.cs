﻿#region update
/*
    16/11/2019 [TKG/SIER] tambah short code 
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmCustomerCategory : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal FrmCustomerCategoryFind FrmFind;

        #endregion

        #region Constructor

        public FrmCustomerCategory(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetFormControl(mState.View);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtCtCtCode, TxtCtCtName, ChkLocalInd, TxtCtCtShortCode
                    }, true);
                    BtnAcNo.Enabled = false;
                    TxtCtCtCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtCtCtCode, TxtCtCtName, ChkLocalInd, TxtCtCtShortCode
                    }, false);
                    BtnAcNo.Enabled = true;
                    TxtCtCtCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtCtCtName, ChkLocalInd, TxtCtCtShortCode
                    }, false);
                    BtnAcNo.Enabled = true;
                    TxtCtCtName.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtCtCtCode, TxtCtCtName, TxtAcNo, TxtAcDesc, TxtCtCtShortCode
            });
            ChkLocalInd.Checked = false;
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmCustomerCategoryFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
            ChkLocalInd.Checked = true;
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtCtCtCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtCtCtCode, "", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblCustomerCategory Where CtCtCode=@CtCtCode" };
                Sm.CmParam<String>(ref cm, "@CtCtCode", TxtCtCtCode.Text);
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsDataNotValid()) return;
                
                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into TblCustomerCategory(CtCtCode, CtCtName, CtCtShortCode, LocalInd, AcNo, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@CtCtCode, @CtCtName, @CtCtShortCode, @LocalInd,  @AcNo,  @UserCode,CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("   Update CtCtName=@CtCtName, CtCtShortCode=@CtCtShortCode, LocalInd=@LocalInd, AcNo=@AcNo, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@CtCtCode", TxtCtCtCode.Text);
                Sm.CmParam<String>(ref cm, "@CtCtName", TxtCtCtName.Text);
                Sm.CmParam<String>(ref cm, "@CtCtShortCode", TxtCtCtShortCode.Text);
                Sm.CmParam<String>(ref cm, "@AcNo", TxtAcNo.Text);
                Sm.CmParam<String>(ref cm, "@LocalInd", ChkLocalInd.Checked?"Y":"N");
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.ExecCommand(cm);

                ShowData(TxtCtCtCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string CtCtCtCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@CtCtCode", CtCtCtCode);
                Sm.ShowDataInCtrl(
                        ref cm,
                        "Select A.CtCtCode, A.CtCtName, A.LocalInd, A.CtCtShortCode, A.AcNo, B.AcDesc " +
                        "From TblCustomerCategory A " +
                        "Left Join TblCOA B On A.AcNo=B.AcNo " +
                        "Where A.CtCtCode=@CtCtCode;",
                        new string[] 
                        {
                            "CtCtCode", 
                            "CtCtName", "LocalInd", "CtCtShortCode", "AcNo", "AcDesc"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtCtCtCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtCtCtName.EditValue = Sm.DrStr(dr, c[1]);
                            ChkLocalInd.Checked = Sm.DrStr(dr, c[2]) == "Y";
                            TxtCtCtShortCode.EditValue = Sm.DrStr(dr, c[3]);
                            TxtAcNo.EditValue = Sm.DrStr(dr, c[4]);
                            TxtAcDesc.EditValue = Sm.DrStr(dr, c[5]);
                            
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtCtCtCode, "Customer's category code", false) ||
                Sm.IsTxtEmpty(TxtCtCtName, "Customer's category name", false) ||
                IsCtCtCodeExisted();
        }

        private bool IsCtCtCodeExisted()
        {
            if (!TxtCtCtCode.Properties.ReadOnly && Sm.IsDataExist("Select CtCtCode From TblCustomerCategory Where CtCtCode='" + TxtCtCtCode.Text + "' Limit 1"))
            {
                Sm.StdMsg(mMsgType.Warning, "Customer's category code ( " + TxtCtCtCode.Text + " ) already existed.");
                return true;
            }
            return false;
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtCtCtCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtCtCtCode);
        }

        private void TxtCtCtName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtCtCtName);
        }

        private void TxtCtCtShortCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtCtCtShortCode);
        }

        #endregion

        private void BtnAcNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmCustomerCategoryDlg(this));
        }

        #endregion
    }
}
