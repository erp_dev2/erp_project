﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptSOBasedOnWeb : RunSystem.FrmBase6
    {
        #region Field

        private string 
            mMenuCode = string.Empty, 
            mAccessInd = string.Empty, 
            mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmRptSOBasedOnWeb(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);

                string CurrentDate = Sm.ServerCurrentDateTime();
                DteDocDt1.DateTime = Sm.ConvertDate(CurrentDate).AddDays(-1);
                DteDocDt2.DateTime = Sm.ConvertDate(CurrentDate);
                SetGrd();
                SetSQL();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT DISTINCT A.Id, B.DocDt, A.SODocNo, H.ItCode, I.ItCodeInternal, I.ItName, I.ForeignName, E.Qty, D.CurCode, F.UPrice, J.SPName ");
            SQL.AppendLine("FROM TblGetDataWebDtl A ");
            SQL.AppendLine("INNER JOIN TblGetDataWebHdr B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("INNER JOIN TblSalesInvoiceHdr C ON A.SalesInvoiceDocNo = C.DocNo ");
            SQL.AppendLine("INNER JOIN TblSOHdr D ON C.SODocNo = D.DocNo AND D.CancelInd = 'N' ");
            SQL.AppendLine("INNER JOIN TblSODtl E ON D.DocNo = E.DocNo ");
            SQL.AppendLine("INNER JOIN TblCtQtDtl F ON D.CtQtDocNo = F.DocNo AND E.CtQtDNo = F.DNo ");
            SQL.AppendLine("INNER JOIN TblItemPriceHdr G ON F.ItemPriceDocNo = G.DocNo ");
            SQL.AppendLine("INNER JOIN TblItemPriceDtl H ON F.ItemPriceDocNo = H.DocNo AND F.ItemPriceDNo = H.DNo ");
            SQL.AppendLine("INNER JOIN TblItem I ON H.ItCode = I.ItCode ");
            SQL.AppendLine("LEFT JOIN TblSalesPerson J ON D.SPCode = J.SPCode ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 12;
            Grd1.FrozenArea.ColCount = 3;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "ID#", 
                        "Date",
                        "SO Document#",
                        "Item's Code",
                        "Item's"+Environment.NewLine+"Local Code",

                        //6-10
                        "Item's Name",
                        "Foreign Name",
                        "Quantity",
                        "Currency",
                        "Unit Price",

                        //11
                        "Sales Person's"+Environment.NewLine+"Name"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        60, 100, 120, 100, 100, 

                        //6-10
                        250, 250, 120, 100, 100, 
                        
                        //11
                        150
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 8, 10 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2 });
            Sm.GrdColInvisible(Grd1, new int[] { 4, 5, 7 }, false);
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4, 5, 7 }, !ChkHideInfoInGrd.Checked);
        }

        private bool IsFilterByDateInvalid()
        {
            var DocDt1 = Sm.GetDte(DteDocDt1);
            var DocDt2 = Sm.GetDte(DteDocDt2);

            if (Decimal.Parse(DocDt1) > Decimal.Parse(DocDt2))
            {
                Sm.StdMsg(mMsgType.Warning, "End date is earlier than start date.");
                return true;
            }
            return false;
        }

        override protected void ShowData()
        {
            try
            {
                if (
                    Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                    Sm.IsDteEmpty(DteDocDt2, "End date") ||
                    IsFilterByDateInvalid()
                    ) return;

                Cursor.Current = Cursors.WaitCursor;

                string Filter = string.Empty;
                var cm = new MySqlCommand();

                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "B.DocDt");
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "H.ItCode", "I.ItName", "I.ItCodeInternal", "I.ForeignName" });

                Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                mSQL + Filter + " Order By B.DocDt, A.SODocNo;",
                new string[]
                    {
                        //0
                        "Id", 

                        //1-5
                        "DocDt", "SODocNo", "ItCode", "ItName", "Qty", 

                        //6-10
                        "CurCode", "UPrice", "SPName", "ItCodeInternal", "ForeignName"
                    },
                (
                    MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Grd1.Cells[Row, 0].Value = Row + 1;
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 8);
                }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            //Grd1.Rows.AutoHeight();
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Events

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        #endregion

        #endregion

    }
}
