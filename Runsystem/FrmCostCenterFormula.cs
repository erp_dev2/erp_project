﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

using TenTec.Windows.iGridLib;

#endregion

namespace RunSystem
{
    public partial class FrmCostCenterFormula : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal int mStateInd = 0;
        internal FrmCostCenterFormulaFind FrmFind;
        internal string 
            mCostCenterFormulaConstCount = string.Empty,
            mCostCenterFormulaSourceCount = string.Empty;

        #endregion

        #region Constructor

        public FrmCostCenterFormula(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Cost Center Formula";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                SetLueCCCode(ref LueCCCode);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grid 1

            Grd1.Cols.Count = 3;
            Grd1.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "Code",

                        //1-2
                        "Description",
                        "Value"
                    },
                     new int[] 
                    {
                        //0
                        0,

                        //1-2
                        100, 150
                    }
                );
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1 });
            Sm.GrdColInvisible(Grd1, new int[] { 0 }, false);

            #endregion

            #region Grid 2

            Grd2.Cols.Count = 3;
            Grd2.FrozenArea.ColCount = 2;

            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[] 
                    {
                        //0
                        "Code",

                        //1-2
                        "Description",
                        "Value"
                    },
                    new int[] 
                    {
                        //0
                        0,

                        //1-2
                        100, 150
                    }
                );
            Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 1 });
            Sm.GrdColInvisible(Grd2, new int[] { 0 }, false);

            #endregion

        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        LueCCCode, TxtFormula
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2 });
                    Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 1, 2 });
                    LueCCCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        LueCCCode, TxtFormula
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 2 });
                    Sm.GrdColReadOnly(false, true, Grd2, new int[] { 2 });
                    LueCCCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtFormula }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 2 });
                    Sm.GrdColReadOnly(false, true, Grd2, new int[] { 2 });
                    TxtFormula.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                LueCCCode, TxtFormula
            });
            mStateInd = 0;
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ClearGrd()
        {
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
            Grd2.Rows.Clear();
            Grd2.Rows.Count = 1;
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmCostCenterFormulaFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                mStateInd = 1;
                SetLueCCCode(ref LueCCCode);
                InsertRow(mCostCenterFormulaSourceCount, mCostCenterFormulaConstCount);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsLueEmpty(LueCCCode, "")) return;
            SetFormControl(mState.Edit);
            mStateInd = 2;
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (mStateInd == 1)
                    InsertData(sender, e);
                else if(mStateInd == 2)
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            //ParPrint();
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if ((Sm.StdMsgYN("Save", "") == DialogResult.No) || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(SaveCCFormulaHdr(Sm.GetLue(LueCCCode)));
            for (int r = 0; r < Grd1.Rows.Count; r++)
                if (Sm.GetGrdStr(Grd1, r, 1).Length > 0) cml.Add(SaveCCFormulaDtl1(Sm.GetLue(LueCCCode), r));

            for (int r = 0; r < Grd2.Rows.Count; r++)
                if (Sm.GetGrdStr(Grd2, r, 1).Length > 0) cml.Add(SaveCCFormulaDtl2(Sm.GetLue(LueCCCode), r));

            Sm.ExecCommands(cml);

            //BtnInsertClick(sender, e);
            ShowData(Sm.GetLue(LueCCCode));
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsLueEmpty(LueCCCode, "Cost Center") ||
                IsGrd1Empty() ||
                IsGrd2Empty() ||
                IsGrdValueNotValid() ||
                IsDataExists();
        }

        private bool IsDataExists()
        {
            if (Sm.IsDataExist("Select CCCode From TblCostCenterFormulaHdr Where CCCode = '" + Sm.GetLue(LueCCCode) + "';"))
            {
                Sm.StdMsg(mMsgType.Warning, "This data ('" + LueCCCode.Text + "') is already exists.");
                return true;
            }
            return false;
        }

        private bool IsGrd1Empty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 data source.");
                return true;
            }
            return false;
        }

        private bool IsGrd2Empty()
        {
            if (Grd2.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 data constanta.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            if (Grd1.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (
                        Sm.IsGrdValueEmpty(Grd1, Row, 1, false, "Source Description is empty.")
                        )
                        return true;
                }
            }
            if (Grd2.Rows.Count > 1)
            {
                for (int r = 0; r < Grd2.Rows.Count - 1; r++)
                {
                    if (
                        Sm.IsGrdValueEmpty(Grd2, r, 1, false, "Constanta Description is empty.")
                        )
                        return true;
                }
            }
            return false;
        }

        private MySqlCommand SaveCCFormulaHdr(string CCCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblCostCenterFormulaHdr ");
            SQL.AppendLine("(CCCode, Formula, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@CCCode, @Formula, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@CCCode", CCCode);
            Sm.CmParam<String>(ref cm, "@Formula", TxtFormula.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveCCFormulaDtl1(string CCCode, int Row)
        {
            var SQLDtl1 = new StringBuilder();

            SQLDtl1.AppendLine("Insert Into TblCostCenterFormulaDtl1(CCCode, SourceCode, SourceValue, CreateBy, CreateDt) ");
            SQLDtl1.AppendLine("Values(@CCCode, @SourceCode, @SourceValue, @CreateBy, CurrentDateTime()); ");
            
            var cm = new MySqlCommand(){ CommandText = SQLDtl1.ToString() };
            Sm.CmParam<String>(ref cm, "@CCCode", CCCode);
            Sm.CmParam<String>(ref cm, "@SourceCode", Sm.GetGrdStr(Grd1, Row, 0));
            Sm.CmParam<String>(ref cm, "@SourceValue", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveCCFormulaDtl2(string CCCode, int Row)
        {
            var SQLDtl2 = new StringBuilder();

            SQLDtl2.AppendLine("Insert Into TblCostCenterFormulaDtl2(CCCode, ConstCode, ConstValue, CreateBy, CreateDt) ");
            SQLDtl2.AppendLine("Values(@CCCode, @ConstCode, @ConstValue, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQLDtl2.ToString() };
            Sm.CmParam<String>(ref cm, "@CCCode", CCCode);
            Sm.CmParam<String>(ref cm, "@ConstCode", Sm.GetGrdStr(Grd2, Row, 0));
            Sm.CmParam<String>(ref cm, "@ConstValue", Sm.GetGrdStr(Grd2, Row, 2));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(UpdateCCFormulaHdr(Sm.GetLue(LueCCCode)));
            if (Grd1.Rows.Count > 1)
            {
                for (int r1 = 0; r1 < Grd1.Rows.Count; r1++)
                {
                    if (Sm.GetGrdStr(Grd1, r1, 1).Length > 0) cml.Add(UpdateCCFormulaDtl1(Sm.GetLue(LueCCCode), r1));
                }
            }

            if (Grd2.Rows.Count > 1)
            {
                for (int r2 = 0; r2 < Grd2.Rows.Count; r2++)
                {
                    if (Sm.GetGrdStr(Grd2, r2, 1).Length > 0) cml.Add(UpdateCCFormulaDtl2(Sm.GetLue(LueCCCode), r2));
                }
            }
            
            Sm.ExecCommands(cml);

            ShowData(Sm.GetLue(LueCCCode));
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsLueEmpty(LueCCCode, "") ||
                IsGrdValueNotValid();
        }

        private MySqlCommand UpdateCCFormulaHdr(string CCCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblCostCenterFormulaHdr ");
            SQL.AppendLine("Set Formula = @Formula, LastUpBy = @CreateBy, LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("Where CCCode = @CCCode; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@CCCode", CCCode);
            Sm.CmParam<String>(ref cm, "@Formula", TxtFormula.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand UpdateCCFormulaDtl1(string CCCode, int Row)
        {
            var SQLDtl1 = new StringBuilder();

            SQLDtl1.AppendLine("Update TblCostCenterFormulaDtl1 ");
            SQLDtl1.AppendLine("Set SourceValue = @SourceValue, LastUpBy = @CreateBy, LastUpDt = CurrentDateTime() ");
            SQLDtl1.AppendLine("Where CCCode = @CCCode And SourceCode = @SourceCode; ");

            var cm = new MySqlCommand() { CommandText = SQLDtl1.ToString() };
            Sm.CmParam<String>(ref cm, "@CCCode", CCCode);
            Sm.CmParam<String>(ref cm, "@SourceCode", Sm.GetGrdStr(Grd1, Row, 0));
            Sm.CmParam<String>(ref cm, "@SourceValue", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand UpdateCCFormulaDtl2(string CCCode, int Row)
        {
            var SQLDtl2 = new StringBuilder();

            SQLDtl2.AppendLine("Update TblCostCenterFormulaDtl2 ");
            SQLDtl2.AppendLine("Set ConstValue = @ConstValue, LastUpBy = @CreateBy, LastUpDt = CurrentDateTime() ");
            SQLDtl2.AppendLine("Where CCCode = @CCCode And ConstCode = @ConstCode; ");

            var cm = new MySqlCommand() { CommandText = SQLDtl2.ToString() };
            Sm.CmParam<String>(ref cm, "@CCCode", CCCode);
            Sm.CmParam<String>(ref cm, "@ConstCode", Sm.GetGrdStr(Grd2, Row, 0));
            Sm.CmParam<String>(ref cm, "@ConstValue", Sm.GetGrdStr(Grd2, Row, 2));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }


        #endregion

        #endregion

        #region Show Data

        public void ShowData(string CCCode)
        {
            try
            {
                ClearData();
                ShowCCFormulaHdr(CCCode);
                ShowCCFormulaDtl1(CCCode);
                ShowCCFormulaDtl2(CCCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowCCFormulaHdr(string CCCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.CCCode, A.Formula ");
            SQL.AppendLine("From TblCostCenterFormulaHdr A ");
            SQL.AppendLine("Where A.CCCode=@CCCode; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@CCCode", CCCode);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "CCCode",

                        //1
                        "Formula"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        Sm.SetLue(LueCCCode, Sm.DrStr(dr, c[0]));
                        TxtFormula.EditValue = Sm.DrStr(dr, c[1]);
                    }, true
                );
        }

        private void ShowCCFormulaDtl1(string CCCode)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@CCCode", CCCode);

            var SQLDtl1 = new StringBuilder();

            SQLDtl1.AppendLine("Select A.CCCode, A.SourceCode, B.OptDesc As SourceDesc, A.SourceValue ");
            SQLDtl1.AppendLine("From TblCostCenterFormulaDtl1 A ");
            SQLDtl1.AppendLine("Left Join TblOption B On A.SourceCode = B.OptCode And B.OptCat = 'CostCenterFormulaSource' ");
            SQLDtl1.AppendLine("Where A.CCCode = @CCCode; ");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQLDtl1.ToString(),
                new string[] 
                { 
                    //0
                    "CCCode", 

                    //1-3
                    "SourceCode", "SourceDesc", "SourceValue"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 3);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ShowCCFormulaDtl2(string CCCode)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@CCCode", CCCode);

            var SQLDtl2 = new StringBuilder();

            SQLDtl2.AppendLine("Select A.CCCode, A.ConstCode, B.OptDesc As ConstDesc, A.ConstValue ");
            SQLDtl2.AppendLine("From TblCostCenterFormulaDtl2 A ");
            SQLDtl2.AppendLine("Left Join TblOption B On A.ConstCode = B.OptCode And B.OptCat = 'CostCenterFormulaConst' ");
            SQLDtl2.AppendLine("Where A.CCCode = @CCCode; ");

            Sm.ShowDataInGrid(
                ref Grd2, ref cm, SQLDtl2.ToString(),
                new string[] 
                { 
                    //0
                    "CCCode", 

                    //1-3
                    "ConstCode", "ConstDesc", "ConstValue"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 3);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd2, 0, 1);
        }

        #endregion

        #region Additional Methods

        private void GetParameter()
        {
            mCostCenterFormulaSourceCount = Sm.GetParameter("CostCenterFormulaSourceCount");
            mCostCenterFormulaConstCount = Sm.GetParameter("CostCenterFormulaConstCount");
        }

        private void InsertRow(string SourceCount, string ConstCount)
        {
            #region Source - Grd1

            var SQL1 = new StringBuilder();
            var cm1 = new MySqlCommand();

            SQL1.AppendLine("Select OptCode, OptDesc From TblOption Where OptCat = 'CostCenterFormulaSource'; ");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm1, SQL1.ToString(),
                new string[] 
                { 
                    //0
                    "OptCode",
                    
                    //1
                    "OptDesc"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 1);
                }, false, false, true, false
            );

            #endregion

            #region Constanta - Grd2

            var SQL2 = new StringBuilder();
            var cm2 = new MySqlCommand();

            SQL2.AppendLine("Select OptCode, OptDesc From TblOption Where OptCat = 'CostCenterFormulaConst'; ");

            Sm.ShowDataInGrid(
                ref Grd2, ref cm2, SQL2.ToString(),
                new string[] 
                { 
                    //0
                    "OptCode",
                    
                    //1
                    "OptDesc"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 1, 1);
                }, false, false, true, false
            );

            if (SourceCount.Length > 0 && SourceCount != "0")
                Grd1.Rows.Count = int.Parse(SourceCount);
            else
                Grd1.Rows.Count = 1;

            if (ConstCount.Length > 0 && ConstCount != "0")
                Grd2.Rows.Count = int.Parse(ConstCount);
            else
                Grd2.Rows.Count = 1;

            #endregion
        }
        
        private void SetLueCCCode(ref DXE.LookUpEdit Lue)
        {
            try
            {
                var SQL = new StringBuilder();
                
                SQL.AppendLine("Select T.CCCode As Col1, T.CCName As Col2 ");
                SQL.AppendLine("From TblCostCenter T ");
                SQL.AppendLine("Where T.ActInd = 'Y' ");
                SQL.AppendLine("Order By T.CCName; ");

                Sm.SetLue2(
                    ref Lue, SQL.ToString(),
                    0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #endregion

        #region Grid

        #endregion
    }
}
