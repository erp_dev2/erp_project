﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptStockMovement3 : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty;
        private bool 
            mIsItGrpCode = false,
            mIsShowForeignName = false,
            mIsFilterByItCt = false; 

        #endregion

        #region Constructor

        public FrmRptStockMovement3(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetPeriod(ref DteDocDt1, ref DteDocDt2, ref ChkDocDt, -1);

                GetParameter();
                Sl.SetLueItCtCodeFilterByItCt(ref LueItCtCode, mIsFilterByItCt ? "Y" : "N");
                Sl.SetLueVdCode(ref LueVdCode);
                Sl.SetLueCtCode(ref LueCtCode);
                
                SetGrd();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mIsShowForeignName = Sm.GetParameterBoo("IsShowForeignName");
            mIsFilterByItCt = Sm.GetParameterBoo("IsFilterByItCt");
            mIsItGrpCode = Sm.GetParameter("IsItGrpCodeShow") == "N";
        }

        private string GetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("/* Stock Movement 3-1 */ ");

            SQL.AppendLine("Select A.DocType, A.WhsCode, A.DocDt, A.ItCode, A.BatchNo, A.Source, ");
	        SQL.AppendLine("C.VdCode, IfNull(B.UPrice, 0) As BuyPrice, ");
	        SQL.AppendLine("D.CtCode, D.CurCode, IfNull(E.UPrice, 0) As SellPrice, "); 
	        SQL.AppendLine("A.Qty, A.Qty2, F.ItGrpCode, G.ItGrpName, F.ForeignName, H.ItScName  ");
	        SQL.AppendLine("From TblStockMovement A ");
	        SQL.AppendLine("Left Join TblStockPrice B "); 
		    SQL.AppendLine("    On A.ItCode=B.ItCode ");
		    SQL.AppendLine("    And A.PropCode=B.PropCode ");
		    SQL.AppendLine("    And A.BatchNo=B.BatchNo ");
		    SQL.AppendLine("    And A.Source=B.Source ");
	        SQL.AppendLine("Left Join TblRecvVdHdr C On Left(A.Source, Length(A.Source)-4)=Concat('13*', C.DocNo) ");
	        SQL.AppendLine("Left Join TblDOCtHdr D On A.DocType='07' And A.DocNo=D.DocNo ");
            SQL.AppendLine("Left Join TblDOCtDtl E On D.DocNo=E.DocNo And A.DNo=E.DNo ");
	        SQL.AppendLine("Left Join TblItem F On A.ItCode=F.ItCode ");
            if (mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=F.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }

            SQL.AppendLine("Left Join TblItemGroup G On F.ItGrpCode = G.ItGrpCode ");
            SQL.AppendLine("Left Join TblItemSubcategory H On F.ItScCode = H.ItScCode ");

            return SQL.ToString();
        }

        private string GetSQL2(string subSQL)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("/* Stock Movement 3-2 */ ");

            SQL.AppendLine("Select T5.OptDesc, T1.DocDt, T1.ItCode, T2.ItName, T1.BatchNo, "); 
	        SQL.AppendLine("T3.VdName, T1.BuyPrice, T4.CtName, T1.CurCode, T1.SellPrice, ");
	        SQL.AppendLine("T1.01_Qty, T1.01_Qty2, T1.02_Qty, T1.02_Qty2, T1.03_Qty, T1.03_Qty2, T1.04_Qty, T1.04_Qty2, ");
	        SQL.AppendLine("T1.05_Qty, T1.05_Qty2, T1.06_Qty, T1.06_Qty2, T1.07_Qty, T1.07_Qty2, T1.08_Qty, T1.08_Qty2, T2.ItGrpCode, T6.ItGrpName, T2.ForeignName, T7.ItScName ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select DocType, DocDt, ItCode, BatchNo, "); 
	        SQL.AppendLine("    VdCode, BuyPrice, CtCode, CurCode, SellPrice, ");
	        SQL.AppendLine("    Sum(01_Qty) As 01_Qty, Sum(01_Qty2) As 01_Qty2, ");
	        SQL.AppendLine("    Sum(02_Qty) As 02_Qty, Sum(02_Qty2) As 02_Qty2, ");
	        SQL.AppendLine("    Sum(03_Qty) As 03_Qty, Sum(03_Qty2) As 03_Qty2, ");
	        SQL.AppendLine("    Sum(04_Qty) As 04_Qty, Sum(04_Qty2) As 04_Qty2, ");
	        SQL.AppendLine("    Sum(05_Qty) As 05_Qty, Sum(05_Qty2) As 05_Qty2, ");
	        SQL.AppendLine("    Sum(06_Qty) As 06_Qty, Sum(06_Qty2) As 06_Qty2, ");
	        SQL.AppendLine("    Sum(07_Qty) As 07_Qty, Sum(07_Qty2) As 07_Qty2, ");
	        SQL.AppendLine("    Sum(08_Qty) As 08_Qty, Sum(08_Qty2) As 08_Qty2 ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("        Select DocType, DocDt, ItCode, BatchNo, ");
            SQL.AppendLine("        VdCode, BuyPrice, CtCode, CurCode, SellPrice, ");
            SQL.AppendLine("        Case When WhsCode='BB' Then Qty Else 0 End As 01_Qty, ");
            SQL.AppendLine("        Case When WhsCode='BB' Then Qty2 Else 0 End As 01_Qty2, ");
            SQL.AppendLine("        Case When WhsCode='BM MK BB' Then Qty Else 0 End As 02_Qty, ");  
            SQL.AppendLine("        Case When WhsCode='BM MK BB' Then Qty2 Else 0 End As 02_Qty2, ");
            SQL.AppendLine("        Case When WhsCode='BM MK WB' Then Qty Else 0 End As 03_Qty, ");  
            SQL.AppendLine("        Case When WhsCode='BM MK WB' Then Qty2 Else 0 End As 03_Qty2, ");
            SQL.AppendLine("        Case When WhsCode='BS' Then Qty Else 0 End As 04_Qty, "); 
            SQL.AppendLine("        Case When WhsCode='BS' Then Qty2 Else 0 End As 04_Qty2, ");
            SQL.AppendLine("        Case When WhsCode='KR' Then Qty Else 0 End As 05_Qty, ");  
            SQL.AppendLine("        Case When WhsCode='KR' Then Qty2 Else 0 End As 05_Qty2, ");
            SQL.AppendLine("        Case When WhsCode='GF' Then Qty Else 0 End As 06_Qty, ");  
            SQL.AppendLine("        Case When WhsCode='GF' Then Qty2 Else 0 End As 06_Qty2, ");
            SQL.AppendLine("        Case When WhsCode='G S SBY' Then Qty Else 0 End As 07_Qty, "); 
            SQL.AppendLine("        Case When WhsCode='G S SBY' Then Qty2 Else 0 End As 07_Qty2, ");
            SQL.AppendLine("        Case When WhsCode='G S WONEEL' Then Qty Else 0 End As 08_Qty, ");  
            SQL.AppendLine("        Case When WhsCode='G S WONEEL' Then Qty2 Else 0 End As 08_Qty2 ");
            SQL.AppendLine("        From ( ");
	        SQL.AppendLine(subSQL);
            SQL.AppendLine("        ) X "); 
            SQL.AppendLine("    ) T ");
            //SQL.AppendLine("    Where 01_Qty<>0 Or 01_Qty2<>0 Or 02_Qty<>0 Or 02_Qty2<>0 Or ");
            //SQL.AppendLine("    03_Qty<>0 Or 03_Qty2<>0 Or 04_Qty<>0 Or 04_Qty2<>0 Or ");
            //SQL.AppendLine("    05_Qty<>0 Or 05_Qty2<>0 Or 06_Qty<>0 Or 06_Qty2<>0 Or ");
            //SQL.AppendLine("    07_Qty<>0 Or 07_Qty2<>0 Or 08_Qty<>0 Or 08_Qty2<>0 ");
            SQL.AppendLine("    Group By DocType, DocDt, ItCode, BatchNo, VdCode, BuyPrice, CtCode, CurCode, SellPrice ");
            SQL.AppendLine(") T1 ");
            SQL.AppendLine("Left Join TblItem T2 On T1.ItCode=T2.ItCode ");
            if (mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=T2.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }

            SQL.AppendLine("Left Join TblVendor T3 On T1.VdCode=T3.VdCode ");
            SQL.AppendLine("Left Join TblCustomer T4 On T1.CtCode=T4.CtCode ");
            SQL.AppendLine("Left Join TblOption T5 On T1.DocType=T5.OptCode And T5.OptCat='InventoryTransType' ");
            SQL.AppendLine("Left Join TblItemGroup T6 On T2.ItGrpCode = T6.ItGrpCode ");
            SQL.AppendLine("Left Join TblItemSubcategory T7 On T2.ItScCode = T7.ItScCode ");
            SQL.AppendLine("Order By T1.ItCode, T1.BatchNo, T1.DocDt, T5.OptDesc, T3.VdName Desc, T4.CtName;");

            return SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.ReadOnly = true;
            Grd1.Header.Rows.Count = 2;
            Grd1.Cols.Count = 30;
            Grd1.FrozenArea.ColCount = 3;

            SetGrdHdr(ref Grd1, 0, 0, "No", 2, 50);
            SetGrdHdr(ref Grd1, 0, 1, "Item's Code", 2, 80);
            SetGrdHdr(ref Grd1, 0, 2, "Item's Name", 2, 200);
            SetGrdHdr(ref Grd1, 0, 3, "Batch#", 2, 200);
            SetGrdHdr(ref Grd1, 0, 4, "Date", 2, 80);
            SetGrdHdr(ref Grd1, 0, 5, "Type", 2, 250);
            SetGrdHdr(ref Grd1, 0, 6, "Vendor", 2, 150);
            SetGrdHdr(ref Grd1, 0, 7, "Buy", 2, 90);
            SetGrdHdr(ref Grd1, 0, 8, "Customer", 2, 150);
            SetGrdHdr(ref Grd1, 0, 9, "Sell", 2, 90);
            SetGrdHdr2(ref Grd1, 1, 10, "Pickel", 2);
            SetGrdHdr2(ref Grd1, 1, 12, "Makloon Pickle", 2);
            SetGrdHdr2(ref Grd1, 1, 14, "Makloon WetBlue", 2);
            SetGrdHdr2(ref Grd1, 1, 16, "Basah", 2);
            SetGrdHdr2(ref Grd1, 1, 18, "Kering", 2);
            SetGrdHdr2(ref Grd1, 1, 20, "Finish", 2);
            SetGrdHdr2(ref Grd1, 1, 22, "Seleksi Surabaya", 2);
            SetGrdHdr2(ref Grd1, 1, 24, "Seleksi Woneel", 2);
            SetGrdHdr(ref Grd1, 0, 26, "Item Group Code", 2, 100);
            SetGrdHdr(ref Grd1, 0, 27, "Item Group Name", 2, 150);
            SetGrdHdr(ref Grd1, 0, 28, "Foreign Name", 2, 170);
            SetGrdHdr(ref Grd1, 0, 29, "Sub-Category", 2, 150);

            Sm.GrdFormatDec(Grd1,
                new int[] { 
                    7, 9, 10,
                    11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
                    21, 22, 23, 24, 25
                }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 4 });
            if (mIsShowForeignName)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 1, 6, 7, 8, 9, 29 }, false);
                Grd1.Cols[28].Move(3);
            }
            else
            {
                Sm.GrdColInvisible(Grd1, new int[] { 1, 6, 7, 8, 9, 28, 29 }, false);
            }
            if (mIsItGrpCode)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 26, 27 }, false);
            }
            Grd1.Cols[26].Move(3);
            Grd1.Cols[27].Move(4);
            Grd1.Cols[29].Move(5);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 6, 7, 8, 9, 29 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetGrdHdr(ref iGrid Grd, int row, int col, string Title, int SpanRows, int ColWidth)
        {
            Grd.Header.Cells[row, col].Value = Title;
            Grd.Header.Cells[row, col].TextAlign = iGContentAlignment.MiddleCenter;
            Grd.Header.Cells[row, col].SpanRows = SpanRows;
            Grd.Cols[col].Width = ColWidth;
        }

        private void SetGrdHdr2(ref iGrid Grd, int row, int col, string Title, int SpanCols)
        {
            Grd.Header.Cells[row, col].Value = Title;
            Grd.Header.Cells[row, col].TextAlign = iGContentAlignment.MiddleCenter;
            Grd.Header.Cells[row, col].SpanCols = SpanCols;
            Grd.Cols[col].Width = 160;

            SetGrdHdr(ref Grd1, 0, col, "SF", 1, 80);
            SetGrdHdr(ref Grd1, 0, col+1, "Lembar", 1, 80);
        }
        
        override protected void ShowData()
        {
            try
            {
                if (Sm.IsDteEmpty(DteDocDt1, "Date") || Sm.IsDteEmpty(DteDocDt2, "Date")) return;
                
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;
                var SQL = new StringBuilder();

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "A.DocDt");
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "A.ItCode", "F.ItName", "F.ForeignName" });
                Sm.FilterStr(ref Filter, ref cm, TxtBatchNo.Text, "A.BatchNo", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueItCtCode), "F.ItCtCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueVdCode), "C.VdCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCode), "D.CtCode", true);

                Sm.ShowDataInGrid(
                ref Grd1, ref cm, GetSQL2(GetSQL()+Filter),
                new string[]
                    {
                        //0
                        "ItCode", 

                        //1-5
                        "ItName", "BatchNo", "DocDt", "OptDesc", "VdName", 
                        
                        //6-10
                        "BuyPrice", "CtName", "SellPrice", "01_Qty", "01_Qty2", 

                        //11-15
                        "02_Qty", "02_Qty2", "03_Qty", "03_Qty2", "04_Qty", 
                        
                        //16-20
                        "04_Qty2", "05_Qty", "05_Qty2", "06_Qty", "06_Qty2", 

                        //21-25
                        "07_Qty", "07_Qty2", "08_Qty", "08_Qty2", "ItGrpCode",
                        
                        //26-27
                        "ItGrpName", "ForeignName", "ItScName"
                    },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Grd.Cells[Row, 0].Value = Row+1;
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 12);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 13);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 14);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 15);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 16);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 17);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 18);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 19);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 20);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 22, 21);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 23, 22);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 24, 23);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 25, 24);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 25);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 27, 26);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 28, 27);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 29, 28);
                }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }
        
        #region Grid Nethod

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        
        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Date");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void TxtBatchNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBatchNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Batch#");
        }

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue1(Sl.SetLueVdCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkVdCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Vendor");
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");
        }

        private void LueItCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCtCode, new Sm.RefreshLue2(Sl.SetLueItCtCode), mIsFilterByItCt ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkItCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Item's category");
        }

        #endregion

        #endregion
    }
}
