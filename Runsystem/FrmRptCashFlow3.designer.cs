﻿namespace RunSystem
{
    partial class FrmRptCashFlow3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TcRptCashFlow3 = new System.Windows.Forms.TabControl();
            this.TpFilter = new System.Windows.Forms.TabPage();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.LueProjectName = new DevExpress.XtraEditors.LookUpEdit();
            this.LueMth = new DevExpress.XtraEditors.LookUpEdit();
            this.LueYr = new DevExpress.XtraEditors.LookUpEdit();
            this.TpShowData = new System.Windows.Forms.TabPage();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.TxtDeliveryDays = new DevExpress.XtraEditors.TextEdit();
            this.TxtSOContractRevisionDocNo = new DevExpress.XtraEditors.TextEdit();
            this.TxtSOContractDocNo = new DevExpress.XtraEditors.TextEdit();
            this.TxtSiteName = new DevExpress.XtraEditors.TextEdit();
            this.TxtProjectCode = new DevExpress.XtraEditors.TextEdit();
            this.TxtProjectName = new DevExpress.XtraEditors.TextEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtProfitToNetto = new DevExpress.XtraEditors.TextEdit();
            this.label16 = new System.Windows.Forms.Label();
            this.TxtRAPToNetto = new DevExpress.XtraEditors.TextEdit();
            this.TxtResource = new DevExpress.XtraEditors.TextEdit();
            this.TxtRAP = new DevExpress.XtraEditors.TextEdit();
            this.label15 = new System.Windows.Forms.Label();
            this.TxtNetto = new DevExpress.XtraEditors.TextEdit();
            this.label14 = new System.Windows.Forms.Label();
            this.TxtExclPPN = new DevExpress.XtraEditors.TextEdit();
            this.label13 = new System.Windows.Forms.Label();
            this.TxtInclPPN = new DevExpress.XtraEditors.TextEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            this.panel3.SuspendLayout();
            this.TcRptCashFlow3.SuspendLayout();
            this.TpFilter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueProjectName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueMth.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueYr.Properties)).BeginInit();
            this.TpShowData.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDeliveryDays.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSOContractRevisionDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSOContractDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSiteName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProjectCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProjectName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProfitToNetto.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRAPToNetto.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtResource.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRAP.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtNetto.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtExclPPN.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInclPPN.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(838, 0);
            this.panel1.Size = new System.Drawing.Size(70, 554);
            // 
            // BtnExcel
            // 
            this.BtnExcel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnExcel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnExcel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnExcel.Appearance.Options.UseBackColor = true;
            this.BtnExcel.Appearance.Options.UseFont = true;
            this.BtnExcel.Appearance.Options.UseForeColor = true;
            this.BtnExcel.Appearance.Options.UseTextOptions = true;
            this.BtnExcel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnRefresh
            // 
            this.BtnRefresh.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnRefresh.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRefresh.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnRefresh.Appearance.Options.UseBackColor = true;
            this.BtnRefresh.Appearance.Options.UseFont = true;
            this.BtnRefresh.Appearance.Options.UseForeColor = true;
            this.BtnRefresh.Appearance.Options.UseTextOptions = true;
            this.BtnRefresh.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.TcRptCashFlow3);
            this.panel2.Size = new System.Drawing.Size(838, 193);
            // 
            // BtnChart
            // 
            this.BtnChart.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnChart.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnChart.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnChart.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnChart.Appearance.Options.UseBackColor = true;
            this.BtnChart.Appearance.Options.UseFont = true;
            this.BtnChart.Appearance.Options.UseForeColor = true;
            this.BtnChart.Appearance.Options.UseTextOptions = true;
            this.BtnChart.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // Grd1
            // 
            this.Grd1.DefaultAutoGroupRow.Height = 21;
            this.Grd1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.Grd1.FrozenArea.ColCount = 3;
            this.Grd1.FrozenArea.SortFrozenRows = true;
            this.Grd1.GridLines.Mode = TenTec.Windows.iGridLib.iGGridLinesMode.None;
            this.Grd1.GroupBox.BackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintBackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd1.GroupBox.HintForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Grd1.GroupBox.Visible = true;
            this.Grd1.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.UseXPStyles = false;
            this.Grd1.ScrollBarSettings.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd1.SearchAsType.Mode = TenTec.Windows.iGridLib.iGSearchAsTypeMode.Seek;
            this.Grd1.SearchAsType.SearchCol = null;
            this.Grd1.Size = new System.Drawing.Size(838, 361);
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.VScrollBar.CustomButtons.AddRange(new TenTec.Windows.iGridLib.iGScrollBarCustomButton[] {
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.ExpandAll, -1, null, true, null),
            new TenTec.Windows.iGridLib.iGScrollBarCustomButton(TenTec.Windows.iGridLib.iGScrollBarCustomButtonAlign.Far, TenTec.Windows.iGridLib.iGActions.CollapseAll, -1, null, true, null)});
            this.Grd1.VScrollBar.Visibility = TenTec.Windows.iGridLib.iGScrollBarVisibility.Always;
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Location = new System.Drawing.Point(0, 532);
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel3
            // 
            this.panel3.Location = new System.Drawing.Point(0, 193);
            this.panel3.Size = new System.Drawing.Size(838, 361);
            // 
            // TcRptCashFlow3
            // 
            this.TcRptCashFlow3.Controls.Add(this.TpFilter);
            this.TcRptCashFlow3.Controls.Add(this.TpShowData);
            this.TcRptCashFlow3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TcRptCashFlow3.Location = new System.Drawing.Point(0, 0);
            this.TcRptCashFlow3.Name = "TcRptCashFlow3";
            this.TcRptCashFlow3.SelectedIndex = 0;
            this.TcRptCashFlow3.Size = new System.Drawing.Size(834, 189);
            this.TcRptCashFlow3.TabIndex = 26;
            // 
            // TpFilter
            // 
            this.TpFilter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.TpFilter.Controls.Add(this.label2);
            this.TpFilter.Controls.Add(this.label1);
            this.TpFilter.Controls.Add(this.label7);
            this.TpFilter.Controls.Add(this.LueProjectName);
            this.TpFilter.Controls.Add(this.LueMth);
            this.TpFilter.Controls.Add(this.LueYr);
            this.TpFilter.Location = new System.Drawing.Point(4, 23);
            this.TpFilter.Name = "TpFilter";
            this.TpFilter.Padding = new System.Windows.Forms.Padding(3);
            this.TpFilter.Size = new System.Drawing.Size(826, 162);
            this.TpFilter.TabIndex = 0;
            this.TpFilter.Text = "Filter";
            this.TpFilter.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.White;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(9, 50);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 14);
            this.label2.TabIndex = 40;
            this.label2.Text = "Project Name";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.White;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(48, 29);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 14);
            this.label1.TabIndex = 39;
            this.label1.Text = "Month";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.White;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Red;
            this.label7.Location = new System.Drawing.Point(58, 8);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(32, 14);
            this.label7.TabIndex = 38;
            this.label7.Text = "Year";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueProjectName
            // 
            this.LueProjectName.EnterMoveNextControl = true;
            this.LueProjectName.Location = new System.Drawing.Point(93, 47);
            this.LueProjectName.Margin = new System.Windows.Forms.Padding(5);
            this.LueProjectName.Name = "LueProjectName";
            this.LueProjectName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProjectName.Properties.Appearance.Options.UseFont = true;
            this.LueProjectName.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProjectName.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueProjectName.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProjectName.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueProjectName.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProjectName.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueProjectName.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProjectName.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueProjectName.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueProjectName.Properties.DropDownRows = 30;
            this.LueProjectName.Properties.NullText = "[Empty]";
            this.LueProjectName.Properties.PopupWidth = 350;
            this.LueProjectName.Size = new System.Drawing.Size(317, 20);
            this.LueProjectName.TabIndex = 37;
            this.LueProjectName.ToolTip = "F4 : Show/hide list";
            this.LueProjectName.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueProjectName.EditValueChanged += new System.EventHandler(this.LueProjectName_EditValueChanged);
            // 
            // LueMth
            // 
            this.LueMth.EnterMoveNextControl = true;
            this.LueMth.Location = new System.Drawing.Point(93, 26);
            this.LueMth.Margin = new System.Windows.Forms.Padding(5);
            this.LueMth.Name = "LueMth";
            this.LueMth.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMth.Properties.Appearance.Options.UseFont = true;
            this.LueMth.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMth.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueMth.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMth.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueMth.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMth.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueMth.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMth.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueMth.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueMth.Properties.DropDownRows = 13;
            this.LueMth.Properties.NullText = "[Empty]";
            this.LueMth.Properties.PopupWidth = 100;
            this.LueMth.Size = new System.Drawing.Size(75, 20);
            this.LueMth.TabIndex = 35;
            this.LueMth.ToolTip = "F4 : Show/hide list";
            this.LueMth.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // LueYr
            // 
            this.LueYr.EnterMoveNextControl = true;
            this.LueYr.Location = new System.Drawing.Point(93, 5);
            this.LueYr.Margin = new System.Windows.Forms.Padding(5);
            this.LueYr.Name = "LueYr";
            this.LueYr.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueYr.Properties.Appearance.Options.UseFont = true;
            this.LueYr.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueYr.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueYr.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueYr.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueYr.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueYr.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueYr.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueYr.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueYr.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueYr.Properties.DropDownRows = 12;
            this.LueYr.Properties.NullText = "[Empty]";
            this.LueYr.Properties.PopupWidth = 100;
            this.LueYr.Size = new System.Drawing.Size(75, 20);
            this.LueYr.TabIndex = 33;
            this.LueYr.ToolTip = "F4 : Show/hide list";
            this.LueYr.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // TpShowData
            // 
            this.TpShowData.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.TpShowData.Controls.Add(this.splitContainer1);
            this.TpShowData.Location = new System.Drawing.Point(4, 23);
            this.TpShowData.Name = "TpShowData";
            this.TpShowData.Padding = new System.Windows.Forms.Padding(3);
            this.TpShowData.Size = new System.Drawing.Size(760, 162);
            this.TpShowData.TabIndex = 1;
            this.TpShowData.Text = "Show Data";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(3, 3);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.BackColor = System.Drawing.Color.White;
            this.splitContainer1.Panel1.Controls.Add(this.TxtDeliveryDays);
            this.splitContainer1.Panel1.Controls.Add(this.TxtSOContractRevisionDocNo);
            this.splitContainer1.Panel1.Controls.Add(this.TxtSOContractDocNo);
            this.splitContainer1.Panel1.Controls.Add(this.TxtSiteName);
            this.splitContainer1.Panel1.Controls.Add(this.TxtProjectCode);
            this.splitContainer1.Panel1.Controls.Add(this.TxtProjectName);
            this.splitContainer1.Panel1.Controls.Add(this.label9);
            this.splitContainer1.Panel1.Controls.Add(this.label8);
            this.splitContainer1.Panel1.Controls.Add(this.label6);
            this.splitContainer1.Panel1.Controls.Add(this.label5);
            this.splitContainer1.Panel1.Controls.Add(this.label4);
            this.splitContainer1.Panel1.Controls.Add(this.label3);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.BackColor = System.Drawing.Color.White;
            this.splitContainer1.Panel2.Controls.Add(this.TxtProfitToNetto);
            this.splitContainer1.Panel2.Controls.Add(this.label16);
            this.splitContainer1.Panel2.Controls.Add(this.TxtRAPToNetto);
            this.splitContainer1.Panel2.Controls.Add(this.TxtResource);
            this.splitContainer1.Panel2.Controls.Add(this.TxtRAP);
            this.splitContainer1.Panel2.Controls.Add(this.label15);
            this.splitContainer1.Panel2.Controls.Add(this.TxtNetto);
            this.splitContainer1.Panel2.Controls.Add(this.label14);
            this.splitContainer1.Panel2.Controls.Add(this.TxtExclPPN);
            this.splitContainer1.Panel2.Controls.Add(this.label13);
            this.splitContainer1.Panel2.Controls.Add(this.TxtInclPPN);
            this.splitContainer1.Panel2.Controls.Add(this.label12);
            this.splitContainer1.Panel2.Controls.Add(this.label11);
            this.splitContainer1.Panel2.Controls.Add(this.label10);
            this.splitContainer1.Size = new System.Drawing.Size(754, 156);
            this.splitContainer1.SplitterDistance = 362;
            this.splitContainer1.TabIndex = 0;
            // 
            // TxtDeliveryDays
            // 
            this.TxtDeliveryDays.EnterMoveNextControl = true;
            this.TxtDeliveryDays.Location = new System.Drawing.Point(96, 108);
            this.TxtDeliveryDays.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDeliveryDays.Name = "TxtDeliveryDays";
            this.TxtDeliveryDays.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDeliveryDays.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDeliveryDays.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDeliveryDays.Properties.Appearance.Options.UseFont = true;
            this.TxtDeliveryDays.Properties.MaxLength = 80;
            this.TxtDeliveryDays.Properties.ReadOnly = true;
            this.TxtDeliveryDays.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TxtDeliveryDays.Size = new System.Drawing.Size(288, 20);
            this.TxtDeliveryDays.TabIndex = 50;
            this.TxtDeliveryDays.Validated += new System.EventHandler(this.TxtDeliveryDays_Validated);
            // 
            // TxtSOContractRevisionDocNo
            // 
            this.TxtSOContractRevisionDocNo.EnterMoveNextControl = true;
            this.TxtSOContractRevisionDocNo.Location = new System.Drawing.Point(96, 87);
            this.TxtSOContractRevisionDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSOContractRevisionDocNo.Name = "TxtSOContractRevisionDocNo";
            this.TxtSOContractRevisionDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtSOContractRevisionDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSOContractRevisionDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSOContractRevisionDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtSOContractRevisionDocNo.Properties.MaxLength = 80;
            this.TxtSOContractRevisionDocNo.Properties.ReadOnly = true;
            this.TxtSOContractRevisionDocNo.Size = new System.Drawing.Size(288, 20);
            this.TxtSOContractRevisionDocNo.TabIndex = 49;
            // 
            // TxtSOContractDocNo
            // 
            this.TxtSOContractDocNo.EnterMoveNextControl = true;
            this.TxtSOContractDocNo.Location = new System.Drawing.Point(96, 66);
            this.TxtSOContractDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSOContractDocNo.Name = "TxtSOContractDocNo";
            this.TxtSOContractDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtSOContractDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSOContractDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSOContractDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtSOContractDocNo.Properties.MaxLength = 80;
            this.TxtSOContractDocNo.Properties.ReadOnly = true;
            this.TxtSOContractDocNo.Size = new System.Drawing.Size(288, 20);
            this.TxtSOContractDocNo.TabIndex = 48;
            // 
            // TxtSiteName
            // 
            this.TxtSiteName.EnterMoveNextControl = true;
            this.TxtSiteName.Location = new System.Drawing.Point(96, 45);
            this.TxtSiteName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSiteName.Name = "TxtSiteName";
            this.TxtSiteName.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtSiteName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSiteName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSiteName.Properties.Appearance.Options.UseFont = true;
            this.TxtSiteName.Properties.MaxLength = 80;
            this.TxtSiteName.Properties.ReadOnly = true;
            this.TxtSiteName.Size = new System.Drawing.Size(288, 20);
            this.TxtSiteName.TabIndex = 47;
            // 
            // TxtProjectCode
            // 
            this.TxtProjectCode.EnterMoveNextControl = true;
            this.TxtProjectCode.Location = new System.Drawing.Point(96, 24);
            this.TxtProjectCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtProjectCode.Name = "TxtProjectCode";
            this.TxtProjectCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtProjectCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtProjectCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtProjectCode.Properties.Appearance.Options.UseFont = true;
            this.TxtProjectCode.Properties.MaxLength = 80;
            this.TxtProjectCode.Properties.ReadOnly = true;
            this.TxtProjectCode.Size = new System.Drawing.Size(288, 20);
            this.TxtProjectCode.TabIndex = 46;
            // 
            // TxtProjectName
            // 
            this.TxtProjectName.EnterMoveNextControl = true;
            this.TxtProjectName.Location = new System.Drawing.Point(96, 3);
            this.TxtProjectName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtProjectName.Name = "TxtProjectName";
            this.TxtProjectName.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtProjectName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtProjectName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtProjectName.Properties.Appearance.Options.UseFont = true;
            this.TxtProjectName.Properties.MaxLength = 80;
            this.TxtProjectName.Properties.ReadOnly = true;
            this.TxtProjectName.Size = new System.Drawing.Size(288, 20);
            this.TxtProjectName.TabIndex = 45;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.White;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(4, 109);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(88, 14);
            this.label9.TabIndex = 44;
            this.label9.Text = "Delivery Day(s)";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.White;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(33, 89);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(59, 14);
            this.label8.TabIndex = 43;
            this.label8.Text = "Revision#";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.White;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(29, 68);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(63, 14);
            this.label6.TabIndex = 42;
            this.label6.Text = "Contract#";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.White;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(64, 46);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(28, 14);
            this.label5.TabIndex = 41;
            this.label5.Text = "Site";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.White;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(14, 25);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(78, 14);
            this.label4.TabIndex = 40;
            this.label4.Text = "Project Code";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.White;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(46, 4);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 14);
            this.label3.TabIndex = 39;
            this.label3.Text = "Project";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtProfitToNetto
            // 
            this.TxtProfitToNetto.EnterMoveNextControl = true;
            this.TxtProfitToNetto.Location = new System.Drawing.Point(125, 130);
            this.TxtProfitToNetto.Margin = new System.Windows.Forms.Padding(5);
            this.TxtProfitToNetto.Name = "TxtProfitToNetto";
            this.TxtProfitToNetto.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtProfitToNetto.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtProfitToNetto.Properties.Appearance.Options.UseBackColor = true;
            this.TxtProfitToNetto.Properties.Appearance.Options.UseFont = true;
            this.TxtProfitToNetto.Properties.MaxLength = 80;
            this.TxtProfitToNetto.Properties.ReadOnly = true;
            this.TxtProfitToNetto.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TxtProfitToNetto.Size = new System.Drawing.Size(288, 20);
            this.TxtProfitToNetto.TabIndex = 64;
            this.TxtProfitToNetto.Validated += new System.EventHandler(this.TxtProfitToNetto_Validated);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.White;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(5, 131);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(117, 14);
            this.label16.TabIndex = 63;
            this.label16.Text = "Profit To Netto (%)";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtRAPToNetto
            // 
            this.TxtRAPToNetto.EnterMoveNextControl = true;
            this.TxtRAPToNetto.Location = new System.Drawing.Point(125, 109);
            this.TxtRAPToNetto.Margin = new System.Windows.Forms.Padding(5);
            this.TxtRAPToNetto.Name = "TxtRAPToNetto";
            this.TxtRAPToNetto.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtRAPToNetto.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtRAPToNetto.Properties.Appearance.Options.UseBackColor = true;
            this.TxtRAPToNetto.Properties.Appearance.Options.UseFont = true;
            this.TxtRAPToNetto.Properties.MaxLength = 80;
            this.TxtRAPToNetto.Properties.ReadOnly = true;
            this.TxtRAPToNetto.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TxtRAPToNetto.Size = new System.Drawing.Size(288, 20);
            this.TxtRAPToNetto.TabIndex = 62;
            this.TxtRAPToNetto.Validated += new System.EventHandler(this.TxtRAPToNetto_Validated);
            // 
            // TxtResource
            // 
            this.TxtResource.EnterMoveNextControl = true;
            this.TxtResource.Location = new System.Drawing.Point(125, 4);
            this.TxtResource.Margin = new System.Windows.Forms.Padding(5);
            this.TxtResource.Name = "TxtResource";
            this.TxtResource.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtResource.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtResource.Properties.Appearance.Options.UseBackColor = true;
            this.TxtResource.Properties.Appearance.Options.UseFont = true;
            this.TxtResource.Properties.MaxLength = 80;
            this.TxtResource.Properties.ReadOnly = true;
            this.TxtResource.Size = new System.Drawing.Size(288, 20);
            this.TxtResource.TabIndex = 57;
            // 
            // TxtRAP
            // 
            this.TxtRAP.EnterMoveNextControl = true;
            this.TxtRAP.Location = new System.Drawing.Point(125, 88);
            this.TxtRAP.Margin = new System.Windows.Forms.Padding(5);
            this.TxtRAP.Name = "TxtRAP";
            this.TxtRAP.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtRAP.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtRAP.Properties.Appearance.Options.UseBackColor = true;
            this.TxtRAP.Properties.Appearance.Options.UseFont = true;
            this.TxtRAP.Properties.MaxLength = 80;
            this.TxtRAP.Properties.ReadOnly = true;
            this.TxtRAP.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TxtRAP.Size = new System.Drawing.Size(288, 20);
            this.TxtRAP.TabIndex = 61;
            this.TxtRAP.Validated += new System.EventHandler(this.TxtRAP_Validated);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.White;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(65, 5);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(57, 14);
            this.label15.TabIndex = 51;
            this.label15.Text = "Resource";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtNetto
            // 
            this.TxtNetto.EnterMoveNextControl = true;
            this.TxtNetto.Location = new System.Drawing.Point(125, 67);
            this.TxtNetto.Margin = new System.Windows.Forms.Padding(5);
            this.TxtNetto.Name = "TxtNetto";
            this.TxtNetto.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtNetto.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtNetto.Properties.Appearance.Options.UseBackColor = true;
            this.TxtNetto.Properties.Appearance.Options.UseFont = true;
            this.TxtNetto.Properties.MaxLength = 80;
            this.TxtNetto.Properties.ReadOnly = true;
            this.TxtNetto.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TxtNetto.Size = new System.Drawing.Size(288, 20);
            this.TxtNetto.TabIndex = 60;
            this.TxtNetto.Validated += new System.EventHandler(this.TxtNetto_Validated);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.White;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(66, 26);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(56, 14);
            this.label14.TabIndex = 52;
            this.label14.Text = "Incl. PPN";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtExclPPN
            // 
            this.TxtExclPPN.EnterMoveNextControl = true;
            this.TxtExclPPN.Location = new System.Drawing.Point(125, 46);
            this.TxtExclPPN.Margin = new System.Windows.Forms.Padding(5);
            this.TxtExclPPN.Name = "TxtExclPPN";
            this.TxtExclPPN.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtExclPPN.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtExclPPN.Properties.Appearance.Options.UseBackColor = true;
            this.TxtExclPPN.Properties.Appearance.Options.UseFont = true;
            this.TxtExclPPN.Properties.MaxLength = 80;
            this.TxtExclPPN.Properties.ReadOnly = true;
            this.TxtExclPPN.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TxtExclPPN.Size = new System.Drawing.Size(288, 20);
            this.TxtExclPPN.TabIndex = 59;
            this.TxtExclPPN.Validated += new System.EventHandler(this.TxtExclPPN_Validated);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.White;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(64, 47);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(58, 14);
            this.label13.TabIndex = 53;
            this.label13.Text = "Excl. PPN";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtInclPPN
            // 
            this.TxtInclPPN.EnterMoveNextControl = true;
            this.TxtInclPPN.Location = new System.Drawing.Point(125, 25);
            this.TxtInclPPN.Margin = new System.Windows.Forms.Padding(5);
            this.TxtInclPPN.Name = "TxtInclPPN";
            this.TxtInclPPN.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtInclPPN.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtInclPPN.Properties.Appearance.Options.UseBackColor = true;
            this.TxtInclPPN.Properties.Appearance.Options.UseFont = true;
            this.TxtInclPPN.Properties.MaxLength = 80;
            this.TxtInclPPN.Properties.ReadOnly = true;
            this.TxtInclPPN.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TxtInclPPN.Size = new System.Drawing.Size(288, 20);
            this.TxtInclPPN.TabIndex = 58;
            this.TxtInclPPN.Validated += new System.EventHandler(this.TxtInclPPN_Validated);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.White;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(83, 69);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(39, 14);
            this.label12.TabIndex = 54;
            this.label12.Text = "Netto";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.White;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(93, 90);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(29, 14);
            this.label11.TabIndex = 55;
            this.label11.Text = "RAP";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.White;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(12, 110);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(110, 14);
            this.label10.TabIndex = 56;
            this.label10.Text = "RAP To Netto (%)";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // FrmRptCashFlow3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(908, 554);
            this.Name = "FrmRptCashFlow3";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            this.panel3.ResumeLayout(false);
            this.TcRptCashFlow3.ResumeLayout(false);
            this.TpFilter.ResumeLayout(false);
            this.TpFilter.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueProjectName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueMth.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueYr.Properties)).EndInit();
            this.TpShowData.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TxtDeliveryDays.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSOContractRevisionDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSOContractDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSiteName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProjectCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProjectName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtProfitToNetto.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRAPToNetto.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtResource.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRAP.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtNetto.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtExclPPN.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInclPPN.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl TcRptCashFlow3;
        private System.Windows.Forms.TabPage TpFilter;
        private System.Windows.Forms.TabPage TpShowData;
        public DevExpress.XtraEditors.LookUpEdit LueProjectName;
        private DevExpress.XtraEditors.LookUpEdit LueMth;
        private DevExpress.XtraEditors.LookUpEdit LueYr;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        internal DevExpress.XtraEditors.TextEdit TxtDeliveryDays;
        internal DevExpress.XtraEditors.TextEdit TxtSOContractRevisionDocNo;
        internal DevExpress.XtraEditors.TextEdit TxtSOContractDocNo;
        internal DevExpress.XtraEditors.TextEdit TxtSiteName;
        internal DevExpress.XtraEditors.TextEdit TxtProjectCode;
        internal DevExpress.XtraEditors.TextEdit TxtProjectName;
        internal DevExpress.XtraEditors.TextEdit TxtRAPToNetto;
        internal DevExpress.XtraEditors.TextEdit TxtResource;
        internal DevExpress.XtraEditors.TextEdit TxtRAP;
        private System.Windows.Forms.Label label15;
        internal DevExpress.XtraEditors.TextEdit TxtNetto;
        private System.Windows.Forms.Label label14;
        internal DevExpress.XtraEditors.TextEdit TxtExclPPN;
        private System.Windows.Forms.Label label13;
        internal DevExpress.XtraEditors.TextEdit TxtInclPPN;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        internal DevExpress.XtraEditors.TextEdit TxtProfitToNetto;
        private System.Windows.Forms.Label label16;
    }
}