﻿#region Update
/*
    21/07/2019 [TKG] New application
    12/08/2019 [TKG] tambah location
    28/08/2019 [TKG] menghilangkan bin
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmIMMTransferBinReq : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string 
            mMenuCode = string.Empty, mAccessInd = string.Empty,
            mIMMRecvSellerWhsCode = string.Empty,
            mDocTitle = string.Empty,
            mDocAbbr = string.Empty,
            mBarcode = string.Empty,
            mDocNo = string.Empty, mDocSeqNo = string.Empty; //if this application is called from other application;
        internal FrmIMMTransferBinReqFind FrmFind;
        private bool mIsNew = false;
        
        #endregion

        #region Constructor

        public FrmIMMTransferBinReq(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                base.FrmLoad(sender, e);
                //if this application is called from other application
                
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo, mDocSeqNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 15;
            Grd1.FrozenArea.ColCount = 4;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "",
                        
                        //1-5
                        "Cancel",
                        "Cancel",
                        "Source",
                        "Product's Code",
                        "Product's Description",
                        
                        //6-10
                        "Color",
                        "Warehouse Code",
                        "Warehouse",
                        "Location Code",
                        "Location",

                        //11-14
                        "Bin",
                        "Quantity",
                        "Seller",
                        "Transfer Bin#"
                    },
                     new int[] 
                    {
                        //0
                        20,

                        //1-5
                        60, 0, 150, 120, 200, 

                        //6-10
                        100, 0, 200, 0, 200, 

                        //11-14
                        100, 100, 200, 120
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1, 2 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 });
            Sm.GrdFormatDec(Grd1, new int[] { 12 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 0 });
            Sm.GrdColInvisible(Grd1, new int[] { 2, 7, 9, 14 }, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 14 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { DteDocDt, LueWhsCode, LueLocCode, TxtBarcode }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1 });
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { DteDocDt, LueWhsCode, LueLocCode, TxtBarcode }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0 });
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1 });
                    Grd1.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            mIsNew = false;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            { TxtDocNo, DteDocDt, LueWhsCode, LueLocCode, TxtBarcode });
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 10 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmIMMTransferBinReqFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sl.SetLueWhsCode(ref LueWhsCode);
                Sl.SetLueIMMLocCode(ref LueLocCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, string.Empty, false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 0)
                {
                    e.DoDefault = false;
                    if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmIMMTransferBinReqDlg(this));
                }
                if (e.ColIndex == 1)
                {
                    if (Sm.GetGrdBool(Grd1, e.RowIndex, 2)) e.DoDefault = false;
                }
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled && TxtDocNo.Text.Length == 0)
            {
                Sm.GrdRemoveRow(Grd1, e, BtnSave);
                Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 0 && BtnSave.Enabled)
                Sm.FormShowDialog(new FrmIMMTransferBinReqDlg(this));
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 1 }, e.ColIndex) && BtnSave.Enabled && TxtDocNo.Text.Length>0)
            {
                var v = Sm.GetGrdBool(Grd1, 0, 1);
                for (int r = 1; r < Grd1.Rows.Count - 1; r++)
                    if (Sm.GetGrdStr(Grd1, r, 3).Length>0) Grd1.Cells[r, 1].Value = v;
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No) return;
            
            Cursor.Current = Cursors.WaitCursor;

            string DocNo = string.Empty;
            decimal DocSeqNo = 0m;

            GenerateDocNo(ref DocNo, ref DocSeqNo);

            var cml = new List<MySqlCommand>();

            cml.Add(SaveIMMTransferBinReqHdr(DocNo, DocSeqNo));
            for (int r = 0; r < Grd1.Rows.Count; r++)
                if (Sm.GetGrdStr(Grd1, r, 3).Length > 0) cml.Add(SaveIMMTransferBinReqDtl(DocNo, DocSeqNo, r));

            Sm.ExecCommands(cml);

            BtnInsertClick(sender, e);
        }

        private void GenerateDocNo(ref string DocNo, ref decimal DocSeqNo)
        {
            string DocDt = Sm.GetDte(DteDocDt);
            string Yr = Sm.Left(DocDt, 4);
            string Mth = DocDt.Substring(4, 2);

            DocNo = string.Concat(mDocTitle, "/", mDocAbbr, "/", Yr, "/", Mth, "/");

            var SQL = new StringBuilder();

            SQL.Append("Select IfNull(( ");
            SQL.Append("    Select DocSeqNo From TblIMMTransferBinReqHdr ");
            SQL.Append("    Where DocNo=@Param Order By DocSeqNo Desc Limit 1 ");
            SQL.Append("   ), 0.00) As DocSeqNo");

            DocSeqNo = Sm.GetValueDec(SQL.ToString(), DocNo);
            DocSeqNo += 1;
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueWhsCode, "Warehouse (Destination)") ||
                Sm.IsLueEmpty(LueLocCode, "Location") ||
                IsGrdEmpty() ||
                IsGrdValueNotValid();
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 product.");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int r = 0; r < Grd1.Rows.Count - 1; r++)
                if (Sm.IsGrdValueEmpty(Grd1, r, 3, false, "Source is empty.") || 
                    IsStockInvalid1(r) ||
                    IsStockInvalid2(r))
                    return true;
            return false;
        }

        private bool IsStockInvalid1(int r)
        {
            string 
                Source = Sm.GetGrdStr(Grd1, r, 3),
                WhsCode = Sm.GetGrdStr(Grd1, r, 7),
                Bin = Sm.GetGrdStr(Grd1, r, 11)
                ;
            
            if (Sm.IsDataExist("Select 1 From TblIMMStockSummary Where WhsCode=@Param1 And Bin=@Param2 And Source=@Param3 And Qty<=0.00;",
                WhsCode, Bin, Source))
            {
                Sm.StdMsg(mMsgType.Warning, 
                    "Source : " + Source + Environment.NewLine +
                    "Product's Code : " + Sm.GetGrdStr(Grd1, r, 4) + Environment.NewLine +
                    "Product's Description : " + Sm.GetGrdStr(Grd1, r, 5) + Environment.NewLine +
                    "Color : " + Sm.GetGrdStr(Grd1, r, 6) + Environment.NewLine +
                    "Warehouse : " + Sm.GetGrdStr(Grd1, r, 8) + Environment.NewLine +
                    "Bin : " + Sm.GetGrdStr(Grd1, r, 11) + Environment.NewLine +
                    "Location : " + Sm.GetGrdStr(Grd1, r, 10) + Environment.NewLine +
                    "Seller : " + Sm.GetGrdStr(Grd1, r, 13) + Environment.NewLine + Environment.NewLine +
                    "Stock is empty."
                    );
                return true;
            }
            return false;
        }

        private bool IsStockInvalid2(int r)
        {
            string Source = Sm.GetGrdStr(Grd1, r, 2);

            if (Sm.IsDataExist("Select 1 From TblIMMTransferBinReqDtl Where CancelInd='N' And Source=@Param And TransferBinDocNo Is Null;", Source))
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Source : " + Source + Environment.NewLine +
                    "Product's Code : " + Sm.GetGrdStr(Grd1, r, 4) + Environment.NewLine +
                    "Product's Description : " + Sm.GetGrdStr(Grd1, r, 5) + Environment.NewLine +
                    "Color : " + Sm.GetGrdStr(Grd1, r, 6) + Environment.NewLine +
                    "Warehouse : " + Sm.GetGrdStr(Grd1, r, 8) + Environment.NewLine +
                    "Bin : " + Sm.GetGrdStr(Grd1, r, 11) + Environment.NewLine +
                    "Location : " + Sm.GetGrdStr(Grd1, r, 10) + Environment.NewLine +
                    "Seller : " + Sm.GetGrdStr(Grd1, r, 13) + Environment.NewLine + Environment.NewLine +
                    "Invalid stock because outstanding request existed."
                    );
                return true;
            }
            return false;
        }

        private MySqlCommand SaveIMMTransferBinReqHdr(string DocNo, decimal DocSeqNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblIMMTransferBinReqHdr ");
            SQL.AppendLine("(DocNo, DocSeqNo, DocDt, WhsCode, LocCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocSeqNo, @DocDt, @WhsCode, @LocCode, @UserCode, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<Decimal>(ref cm, "@DocSeqNo", DocSeqNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            Sm.CmParam<String>(ref cm, "@LocCode", Sm.GetLue(LueLocCode));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveIMMTransferBinReqDtl(string DocNo, decimal DocSeqNo, int r)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblIMMTransferBinReqDtl(DocNo, DocSeqNo, Source, CancelInd, WhsCode, LocCode, Bin, Qty, TransferBinDocNo, TransferBinDocSeqNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocSeqNo, @Source, 'N', @WhsCode, @LocCode, @Bin, @Qty, Null, Null, @UserCode, CurrentDateTime()); ");

            SQL.AppendLine("Update TblIMMStockSummary Set ");
            SQL.AppendLine("    LocCode=@NewLocCode, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where Source=@Source And WhsCode=@WhsCode And Bin=@Bin; ");

            var cm = new MySqlCommand(){ CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<Decimal>(ref cm, "@DocSeqNo", DocSeqNo);
            Sm.CmParam<String>(ref cm, "@Source", Sm.GetGrdStr(Grd1, r, 3));
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetGrdStr(Grd1, r, 7));
            Sm.CmParam<String>(ref cm, "@LocCode", Sm.GetGrdStr(Grd1, r, 9));
            Sm.CmParam<String>(ref cm, "@Bin", Sm.GetGrdStr(Grd1, r, 11));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, r, 12));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@NewLocCode", Sm.GetLue(LueLocCode));
            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty) == DialogResult.No || IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            for (int r = 0; r < Grd1.Rows.Count; r++)
                if (Sm.GetGrdBool(Grd1, r, 1) && !Sm.GetGrdBool(Grd1, r, 2))
                    cml.Add(EditIMMTransferBinReq(r));
            
            Sm.ExecCommands(cml);

            ShowData(mDocNo, mDocSeqNo);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsGrdValueNotValid2();
        }

        private bool IsGrdValueNotValid2()
        {
            RequeryInfo();
            bool IsCancel = false;
            for (int r = 0; r < Grd1.Rows.Count - 1; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 3).Length > 0)
                {
                    if (Sm.GetGrdBool(Grd1, r, 1) || !Sm.GetGrdBool(Grd1, r, 1))
                    {
                        IsCancel = true;
                        if (Sm.GetGrdStr(Grd1, r, 12).Length != 0)
                        {
                            Sm.StdMsg(mMsgType.Warning,
                               "Source : " + Sm.GetGrdStr(Grd1, r, 3) + Environment.NewLine +
                               "Product's Code : " + Sm.GetGrdStr(Grd1, r, 4) + Environment.NewLine +
                               "Product's Description : " + Sm.GetGrdStr(Grd1, r, 5) + Environment.NewLine +
                               "Color : " + Sm.GetGrdStr(Grd1, r, 6) + Environment.NewLine +
                               "Warehouse : " + Sm.GetGrdStr(Grd1, r, 8) + Environment.NewLine +
                               "Bin : " + Sm.GetGrdStr(Grd1, r, 11) + Environment.NewLine +
                               "Location : " + Sm.GetGrdStr(Grd1, r, 10) + Environment.NewLine +
                               "Seller : " + Sm.GetGrdStr(Grd1, r, 13) + Environment.NewLine + Environment.NewLine +
                               "This request alreqdy processed to " + Sm.GetGrdStr(Grd1, r, 14) + "."
                               );
                            return true;
                        }
                    }
                }
            }
            if (!IsCancel)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel minimum 1 product.");
                return true;
            }

            return false;
        }

        private MySqlCommand EditIMMTransferBinReq(int r)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblIMMTransferBinReqDtl Set ");
            SQL.AppendLine("    CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And DocSeqNo=@DocSeqNo And Source=@Source And CancelInd='N'; ");

            SQL.AppendLine("Update TblIMMStockSummary Set ");
            SQL.AppendLine("    LocCode=Null, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where Source=@Source And WhsCode=@WhsCode And Bin=@Bin; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", mDocNo);
            Sm.CmParam<String>(ref cm, "@DocSeqNo", mDocSeqNo);
            Sm.CmParam<String>(ref cm, "@Source", Sm.GetGrdStr(Grd1, r, 3));
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetGrdStr(Grd1, r, 7));
            Sm.CmParam<String>(ref cm, "@Bin", Sm.GetGrdStr(Grd1, r, 11));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private void RequeryInfo()
        {
            string Filter = string.Empty, Source = string.Empty;
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select Source, CancelInd, Concat(TransferBinDocNo, TransferBinDocSeqNo) As TransferBin ");
            SQL.AppendLine("From TblIMMTransferBinReqDtl ");
            SQL.AppendLine("Where DocNo=@DocNo And DocSeqNo=@DocSeqNo ");

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 3).Length > 0)
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += " Source=@Source" + r.ToString();

                    Sm.CmParam<String>(ref cm, "@Source" + r.ToString(), Sm.GetGrdStr(Grd1, r, 3));
                }
            }

            if (Filter.Length == 0)
                Filter = " And 0=1 ";
            else
                Filter = " And (" + Filter + ") ";


            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                Sm.CmParam<String>(ref cm, "@DocNo", mDocNo);
                Sm.CmParam<String>(ref cm, "@DocSeqNo", mDocSeqNo);
                cm.CommandText = SQL.ToString() + Filter;
                cm.CommandTimeout = 600;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]{ "Source", "CancelInd", "TransferBin" });
                if (dr.HasRows)
                {
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        Source = Sm.DrStr(dr, 0);
                        for (int r = 0; r < Grd1.Rows.Count - 1; r++)
                        {
                            if (Sm.CompareStr(Sm.GetGrdStr(Grd1, r, 3), Source))
                            {
                                Sm.SetGrdValue("B", Grd1, dr, c, r, 2, 1);
                                Sm.SetGrdValue("S", Grd1, dr, c, r, 14, 2);
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo, string DocSeqNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowIMMBinTransferReqHdr(DocNo, DocSeqNo);
                ShowIMMBinTransferReqDtl(DocNo, DocSeqNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowIMMBinTransferReqHdr(string DocNo, string DocSeqNo)
        {
            mDocNo = DocNo;
            mDocSeqNo = DocSeqNo;

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DocSeqNo", DocSeqNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select DocNo, DocDt, WhsCode, LocCode From TblIMMTransferBinReqHdr Where DocNo=@DocNo And DocSeqNo=@DocSeqNo;",
                    new string[] 
                    { 
                        "DocNo", 
                        "DocDt", "WhsCode", "LocCode"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = string.Concat(DocNo, DocSeqNo);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        Sm.SetLue(LueWhsCode, Sm.DrStr(dr, c[2]));
                        Sm.SetLue(LueLocCode, Sm.DrStr(dr, c[3]));
                    }, true
                );
        }

        private void ShowIMMBinTransferReqDtl(string DocNo, string DocSeqNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.CancelInd, A.Source, D.ProdCode, D.ProdDesc, D.Color, A.WhsCode, B.WhsName, A.LocCode, F.LocName, A.Bin, A.Qty, E.SellerName, Concat(A.TransferBinDocNo, A.TransferBinDocSeqNo) As TransferBin ");
            SQL.AppendLine("From TblIMMTransferBinReqDtl A ");
            SQL.AppendLine("Inner Join TblWarehouse B On A.WhsCode=B.WhsCode ");
            SQL.AppendLine("Inner Join TblIMMStockPrice C On A.Source=C.Source ");
            SQL.AppendLine("Inner Join TblIMMProduct D On C.ProdCode=D.ProdCode ");
            SQL.AppendLine("Inner Join TblIMMSeller E On C.SellerCode=E.SellerCode ");
            SQL.AppendLine("Left Join TblIMMLocation F On A.LocCode=F.LocCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo And A.DocSeqNo=@DocSeqNo ");
            SQL.AppendLine("Order By D.ProdDesc, D.ProdCode, A.Source, B.WhsName, A.Bin;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DocSeqNo", DocSeqNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "CancelInd", 

                    //1-5
                    "Source", "ProdCode", "ProdDesc", "Color", "WhsCode", 
                    
                    //6-10
                    "WhsName", "LocCode", "LocName", "Bin", "Qty", 
                    
                    //11-12
                    "SellerName", "TransferBin"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 2, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 10);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 11);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 12);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 12 });
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mIMMRecvSellerWhsCode = Sm.GetParameter("IMMRecvSellerWhsCode");
            mDocTitle = Sm.GetParameter("DocTitle");
            mDocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='IMMTransferBinReq';");
        }

        private void ShowBarcodeInfo()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.Source, C.ProdCode, C.ProdDesc, C.Color, A.WhsCode, B.WhsName, A.LocCode, E.LocName, A.Bin, A.Qty, D.SellerName ");
            SQL.AppendLine("From TblIMMStockSummary A ");
            SQL.AppendLine("Inner Join TblWarehouse B On A.WhsCode=B.WhsCode ");
            SQL.AppendLine("Inner Join TblIMMProduct C On A.ProdCode=C.ProdCode ");
            SQL.AppendLine("Inner Join TblIMMSeller D On A.SellerCode=D.SellerCode ");
            SQL.AppendLine("Left Join TblIMMLocation E On A.LocCode=E.LocCode ");
            SQL.AppendLine("Where A.Source=@Source ");
            SQL.AppendLine("And A.Qty>0.00 ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select 1 From TblIMMTransferBinReqDtl ");
            SQL.AppendLine("    Where CancelInd='N' ");
            SQL.AppendLine("    And Source=A.Source ");
            SQL.AppendLine("    And TransferBinDocNo Is Null ");
            SQL.AppendLine("    ) ");
            SQL.AppendLine("And Exists( ");
            SQL.AppendLine("    Select 1 From TblGroupWarehouse ");
            SQL.AppendLine("    Where WhsCode=A.WhsCode ");
            SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
            SQL.AppendLine("    ) ");

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                var cm = new MySqlCommand();
                string Filter = string.Empty, Source = string.Empty;

                if (Grd1.Rows.Count >= 1)
                {
                    for (int r = 0; r < Grd1.Rows.Count; r++)
                    {
                        Source = Sm.GetGrdStr(Grd1, r, 3);
                        if (Source.Length != 0)
                        {
                            if (Filter.Length > 0) Filter += " Or ";
                            Filter += " (A.Source=@Source0" + r.ToString() + " ) ";
                            Sm.CmParam<String>(ref cm, "@Source0" + r.ToString(), Source);
                        }
                    }
                }
                if (Filter.Length > 0)
                    Filter = " And Not (" + Filter + ") Limit 1;";
                else
                    Filter = " Limit 1;";
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@Source", TxtBarcode.Text);

                int Row = Grd1.Rows.Count - 1;
                Grd1.BeginUpdate();
                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandTimeout = 600;
                    cm.CommandText = SQL.ToString()+Filter;
                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] 
                    { 
                        //0
                        "Source", 
                        //1-5
                        "ProdCode", "ProdDesc", "Color", "WhsCode", "WhsName", 
                        //6-10
                        "LocCode", "LocName", "Bin", "Qty", "SellerName"
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            Grd1.Rows.Add();
                            Grd1.Cells[Row, 1].Value = false;
                            Grd1.Cells[Row, 2].Value = false;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 0);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 1);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 3);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 7, 4);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 8, 5);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 9, 6);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 10, 7);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 11, 8);
                            Sm.SetGrdValue("N", Grd1, dr, c, Row, 12, 9);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 13, 10);
                            Grd1.Cells[Row, 14].Value = null;
                            Row++;
                        }
                    }
                    else
                    {
                        Sm.StdMsg(mMsgType.Warning, "Invalid data.");
                        Console.Beep();
                    }
                    dr.Close();
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Grd1.EndUpdate();
                TxtBarcode.EditValue = null;
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtBarcode_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Back) e.SuppressKeyPress = true;
        }

        private void TxtBarcode_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                mIsNew = true;
                mBarcode = string.Empty;
                ShowBarcodeInfo();
                TxtBarcode.Focus();
            }
            else
            {
                if (mIsNew)
                {
                    mBarcode = string.Empty;
                    TxtBarcode.Text = string.Empty;
                }
                mBarcode = mBarcode + e.KeyChar;
                mIsNew = false;
                TxtBarcode.Focus();
            }
        }

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue1(Sl.SetLueWhsCode));
        }

        private void LueLocCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueLocCode, new Sm.RefreshLue1(Sl.SetLueIMMLocCode));
        }

        #endregion

        #endregion
    }
}
