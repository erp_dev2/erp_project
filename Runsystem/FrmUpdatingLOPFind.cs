﻿#region Update
/*
    18/01/2020 [TKG/IMS] tambah BOQfrmfrmfrmfrfrmlea
    30/01/2020 [TKG/IMS] tambah Quotation Letter#
 *  31/08/2020 [ICA/YK] tambah kolom LOP Document
    24/11/2021 [ISD/ALL] tambah filter LOP Document
    05/08/2022 [VIN/ALL] BUG left join tblcity
    07/02/2023 [RDA/MNET] field project name tersimpan ke table UpdatingLOPHdr + show + find
    24/02/2023 [WED/MNET] bug source kolom PIC masih ambil dari employee, padahal parameter IsLOPShowPICSales sudah Y
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmUpdatingLOPFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmUpdatingLOP mFrmParent;
        private string mSQL = "";

        #endregion

        #region Constructor

        public FrmUpdatingLOPFind(FrmUpdatingLOP FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -7);
                Sl.SetLueCtCode(ref LueCtCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A1.DocNo, A1.DocDt, A1.CancelInd, A1.LOPDocNo,  ");
            if (mFrmParent.mIsProjectNameEditable)
                SQL.AppendLine("A1.ProjectName, ");
            else
                SQL.AppendLine("A.ProjectName, ");
            SQL.AppendLine("B.Ctname, C.OptDesc, E.Cityname, A.PICCode, A.EstValue, A1.Remark, F.DocNo As BOQDocNo, A.QtLetterNo, ");
            if (mFrmParent.mIsLOPShowPICSales) SQL.AppendLine("D.UserName PICName, ");
            else SQL.AppendLine("D.Empname As PICName,  ");
            SQL.AppendLine("A1.CreateBy, A1.CreateDt, A1.LastUpBy, A1.LastUpDt  ");
            SQL.AppendLine("From TblUpdatingLOPHdr A1 ");
            SQL.AppendLine("Inner Join TblLOPHdr A On A1.LOPDocNo = A.DocNo ");
            if (mFrmParent.mIsFilterBySite)
            {
                SQL.AppendLine("And (A.SiteCode Is Null Or ( ");
                SQL.AppendLine("    A.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(A.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("            ) ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine(")) ");
            }
            SQL.AppendLine("Inner Join TblCustomer B  On A.CtCode = B.CtCode  ");
            SQL.AppendLine("Inner Join TblOption C  On A.ProjectType = C.OptCode And C.OptCat ='ProjectType' ");
            if (mFrmParent.mIsLOPShowPICSales) SQL.AppendLine("Inner Join TblUser D On A.PICCode = D.UserCode ");
            else SQL.AppendLine("Inner Join TblEmployee D  On A.picCode = D.EmpCode   ");
            SQL.AppendLine("left Join TblCity E On A.CityCode = E.CityCode ");
            SQL.AppendLine("Left Join TblBOQHdr F On A.DocNo=F.LOPDocNo And F.Status In ('O', 'A') ");
            SQL.AppendLine("Where A1.DocDt between @DocDt1 And @DocDt2 ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 21;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#", 
                        "Date",
                        "Cancel",
                        "Project Name",
                        "Customer",

                        //6-10
                        "Project Type",
                        "City",
                        "PIC Code",
                        (mFrmParent.mIsLOPShowPICSales) ? "PIC Sales" : "PIC Name",
                        "Estimated Value",

                        //11-15
                        "BOQ",
                        "Quotation Letter#",
                        "Remark",
                        "Created By",
                        "Created Date",
                        
                        //16-20
                        "Created Time",
                        "Last Updated By",
                        "Last Updated Date", 
                        "Last Updated Time",
                        "LOP Document#"
                    },
                    new int[] 
                    {
                        //0
                        50,
                        //1-5
                        160, 100, 60, 180,  150, 
                        //6-10
                        100, 100, 100, 180, 120, 
                        //11-15
                        130, 150, 200, 120, 120,
                        //16-19
                        120, 120, 120, 120, 160
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 3 });
            Sm.GrdFormatDate(Grd1, new int[] { 2, 15, 18 });
            Sm.GrdFormatTime(Grd1, new int[] { 12, 16, 19 });
            Sm.GrdFormatDec(Grd1, new int[] { 10 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 8, 13, 14, 15, 16, 17, 18, 19 }, false);
            Sm.SetGrdProperty(Grd1, false);
            Grd1.Cols[20].Move(4);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 8, 13, 14, 15, 16, 17, 18, 19 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A1.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCode), "A.CtCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtProjectName.Text, new string[] { "A.ProjectName" });
                Sm.FilterStr(ref Filter, ref cm, TxtLOPDocNo.Text, "A1.LOPDocNo", false);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A1.CreateDt Desc;",
                        new string[]
                        {
                            //0
                            "DocNo",
                            //1-5
                            "DocDt", "CancelInd", "ProjectName", "Ctname", "OptDesc", 
                            //6-10
                            "CityName", "PICCode", "PICName", "EstValue", "BOQDocNo", 
                            //11-15
                            "QtLetterNo", "Remark", "CreateBy", "CreateDt", "LastUpBy", 
                            //16-17
                            "LastUpDt", "LOPDocNo"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 16, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 15);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 18, 16);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 19, 16);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 17);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event
        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");

        }

        private void ChkProjectName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Project Name#");
        }

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            ///Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            //Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void TxtProjectName_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);

        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void TxtLOPDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkLOPDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "LOP Document#");
        }

        #endregion

    }
}
