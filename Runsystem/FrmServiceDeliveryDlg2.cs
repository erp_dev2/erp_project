﻿#region Update
/*
    07/02/2023 [WED/MNET] new apps
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmServiceDeliveryDlg2 : RunSystem.FrmBase4
    {
        #region Field

        private FrmServiceDelivery mFrmParent;
        private string mCtCode = string.Empty, mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmServiceDeliveryDlg2(FrmServiceDelivery FrmParent, string CtCode)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mCtCode = CtCode;
        }

        #endregion

        #region Methods

        #region Form Load

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -90);
            SetGrd();
            SetSQL();
        }

        #endregion

        #region Standard Methods

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt ");
            SQL.AppendLine("From TblDOCtHdr A ");
            SQL.AppendLine("Where A.Status = 'A' ");
            SQL.AppendLine("And A.CtCode = @CtCode ");
            SQL.AppendLine("And A.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("And A.DocNo Not In ( ");
            SQL.AppendLine("    Select Distinct DOCtDocNo ");
            SQL.AppendLine("    From TblServiceDeliveryHdr T1 ");
            SQL.AppendLine("    Inner Join TblServiceDeliveryDtl T2 On T1.DocNo = T2.DocNo ");
            SQL.AppendLine("        And T1.Status In ('O', 'A') ");
            SQL.AppendLine("        And T1.DOCtDocNo Is Not Null ");
            SQL.AppendLine("    And T2.CancelInd = 'N' ");
            SQL.AppendLine(") ");

            mSQL = SQL.ToString();
        }
        #endregion

        #region Set Grid

        private void SetGrd()
        {
            Grd1.Cols.Count = 4;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[]
                {
                    //0
                    "No.",

                    //1-3
                    "Document#",
                    "",
                    "Date"
                }, new int[]
                {
                    //0
                    50,

                    //1-3
                    180, 20, 80
                }
            );

            Sm.GrdColButton(Grd1, new int[] { 2 });
            Sm.GrdFormatDate(Grd1, new int[] { 3 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 3 });
            Sm.SetGrdProperty(Grd1, false);
        }

        #endregion

        #region Grid Method

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2)
            {
                var f = new FrmDOCt(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        #endregion

        #region Show Data
        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " And 0 = 0 ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@CtCode", mCtCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, new string[] { "A.DocNo" });
                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By A.CreateDt Desc;",
                    new string[] { "DocNo",  "DocDt" },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 1);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 2);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.TxtDOCtDocNo.Text = (Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }
        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        #endregion

        #endregion
    }
}
