﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptJournalProduction : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = "", mAccessInd = "", mSQL = "";

        #endregion

        #region Constructor

        public FrmRptJournalProduction(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, D.RecvProductionDocNo, ");
            SQL.AppendLine("IfNull(B.Amt, 0) As Amt1, IfNull(C.Amt, 0) As Amt2, IfNull(D.Amt, 0) As Amt3 ");
            SQL.AppendLine("From TblShopFloorControlHdr A");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T1.DocNo, Sum(T2.Qty*T3.UPrice*T3.ExcRate) As Amt ");
            SQL.AppendLine("    From TblShopFloorControlHdr T1 ");
            SQL.AppendLine("    Inner Join TblShopFloorControl3Dtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    Inner Join TblStockPrice T3 On T2.Source=T3.Source ");
            SQL.AppendLine("    Where (T1.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("    And T1.CancelInd='N' ");
            SQL.AppendLine("    Group By T1.DocNo ");
            SQL.AppendLine(") B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T1.DocNo, Sum(T2.Qty*T2.UPrice) As Amt ");
            SQL.AppendLine("    From TblShopFloorControlHdr T1 ");
            SQL.AppendLine("    Inner Join TblShopFloorControlDtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    Where (T1.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("    And T1.CancelInd='N' ");
            SQL.AppendLine("    Group By T1.DocNo ");
            SQL.AppendLine(") C On A.DocNo=C.DocNo ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.ShopFloorControlDocNo As DocNo, T1.DocNo As RecvProductionDocNo, ");
            SQL.AppendLine("    Sum(T2.Qty*T3.UPrice*T3.ExcRate) As Amt ");
            SQL.AppendLine("    From TblRecvProductionHdr T1 ");
            SQL.AppendLine("    Inner Join TblRecvProductionDtl T2 On T1.DocNo=T2.DocNo And T2.CancelInd='N' ");
            SQL.AppendLine("    Inner Join TblStockPrice T3 On T2.Source=T3.Source ");
            SQL.AppendLine("    Where (T1.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("    Group By T2.ShopFloorControlDocNo, T1.DocNo ");
            SQL.AppendLine(") D On A.DocNo=D.DocNo ");
            SQL.AppendLine("Where (A.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("And A.CancelInd='N' ");
            SQL.AppendLine("Order By A.DocDt, A.DocNo;");
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 7;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#", 
                        "Date",
                        "Received#",
                        "Raw Material",
                        "Production Result",
                        
                        //6
                        "Received"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        150, 80, 150, 130, 130, 
                        
                        //6
                        130
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 2 });
            Sm.GrdFormatDec(Grd1, new int[] { 4, 5, 6 }, 2);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                
                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, mSQL,
                        new string[]
                        {
                            //0
                            "DocNo",

                            //1-5
                            "DocDt", "RecvProductionDocNo", "Amt1", "Amt2", "Amt3"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        #endregion

        #endregion
    }
}
