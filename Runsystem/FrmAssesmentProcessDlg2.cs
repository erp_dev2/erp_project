﻿#region Update
//27/03/2018 [HAR] info position jadi competence level name
//18/07/2022 [RIS/PRODUCT] Hanya menarik assesment yang aktif
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;

#endregion

namespace RunSystem
{
    public partial class FrmAssesmentProcessDlg2 : RunSystem.FrmBase4
    {
        #region Field

        private FrmAssesmentProcess mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmAssesmentProcessDlg2(FrmAssesmentProcess FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            this.Text = "List Of Assesment";
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            SetGrd();
            SetSQL();
            Sl.SetLuePosCode(ref LueOptionCode);
        }

        #endregion

        #region Standard Method

        override protected void SetSQL()
        {

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.AsmCode, A.Asmname, A.ActInd, A.DocDt, B.LevelName, C.OptDesc from TblAssesmentHdr A ");
            SQL.AppendLine("Left Join TblLevelhdr B On A.Competencelevel = B.levelCode ");
            SQL.AppendLine("Left Join TblOption C On A.Competenceleveltype = C.OptCode And C.OptCat = 'CompetenceLevelType' ");
            SQL.AppendLine("Where A.ActInd = 'Y' ");
           
            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 7;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                         //0
                        "No.",

                        //1-5
                        "Assesment"+Environment.NewLine+"Code", 
                        "Assesment"+Environment.NewLine+"Name",
                        "Active",
                        "Date",
                        "Competence Level",
                        
                        //6
                        "Potency / Competence",                    
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 3 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6 });
            Sm.GrdFormatDate(Grd1, new int[] { 4 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3, 5 }, false);
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdAutoSize(Grd1);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                String Filter = " ";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "A.AsmCode", "A.AsmName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueOptionCode), "A.Competenceleveltype", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, mSQL + Filter + " ",
                        new string[] 
                        { 
                            //0
                            "AsmCode", 
                            //1-5
                            "AsmName", "DocDt", "LevelName", "OptDesc", "ActInd"
                           
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("B", Grd1, dr, c, Row, 3, 5);
                            Sm.SetGrdValue("D", Grd1, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 4);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 0))
            {
                mFrmParent.TxtAsmCode.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1);
                mFrmParent.TxtAsmName.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 2);
                mFrmParent.TxtCompetenceLevelName.EditValue = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 6);
                mFrmParent.ShowDataAsm(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Close();
            }

        }

        private void SetLueCompetenceLevelType(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select OptCode As Col1, OptDesc As Col2 From TblOption " +
                "Where OptCat = 'CompetenceLevelType' Order By Col2",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");

        }

        private void Grd1_CellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            ChooseData();

        }


        #endregion

        #region Event 

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LueOptionCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueOptionCode, new Sm.RefreshLue1(SetLueCompetenceLevelType));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkOptionCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Potency / Competence");
        }


        #endregion

       
       
    }
}
