﻿#region Update
/*
    19/04/2017 [TKG] Berdasarkan parameter IsNotCopySalesLocalDocNo utk menentukan default local document#
    11/08/2017 [HAR] BUg di grid 14 packaging tidak muncul
    17/11/2017 [TKG] Pada saat Show Data, informasi customer tidak muncul.
    16/07/2018 [WED] isi semua Shipping Mark dari Shipping Mark master Customer saat insert
    16/07/2018 [WED] urutan pallet muncul otomatis sesuai pallet qty nya, berdasarkan parameter IsPalletInPLEditable
    17/07/2018 [ARI] PRINTOUT ->SM ambil dari SM1 header, kecuali jika customer HONG IK SM hardcode HONG IK (sebelumnya SM hardcode HONG IK)
    20/07/2018 [WED] nomor pallet IOK minta 3 digit
    01/08/2018 [ARI] label local document diganti No. invoice (pake parameter localdocument)
    31/08/2018 [HAR] BUG waktu show data notify party gak muncul
    04/09/2018 [HAR] tambah  button download
    12/11/2018 [HAR] ftp tambah parameter buat nentuin format koneksi FTP
    16/11/2018 [HAR] printout shipping mark ambil dari picture (1 tempat dengan signamture)
    18/11/2018 [TKG] ubah setting container size saat print
    24/01/2019 [HAR] bug waktu cek process inidcator PL (ReListProcessInd dan SetProcessInd)
    08/07/2019 [WED] ShowDataDetail, GrossWeight dan NettWeight terbalik
    18/11/2021 [RIS/IOK] Menambahkan kolom actual name saat print out berdasarkan param IsSO2UseActualItem
    19/03/2022 [TKG/PHT] merubah GetParameter() dan proses save
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

using FastReport;
using FastReport.Data;
using System.IO;
using System.Net;
using System.Threading;
#endregion

namespace RunSystem
{
    public partial class FrmPL : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "", mDocNo = "";
        internal FrmPLFind FrmFind;
        iGCell fCell;
        bool fAccept;
        
        internal int bal, left, right = 0;
        internal int start, end = 0;
        internal string pallet = string.Empty;
        internal bool mIsCreditLimitValidate = false;
        internal bool mIsCustomerItemNameMandatory = false;
        private bool
            mIsLocalDocNoAccesible = false,
            mIsNotCopySalesLocalDocNo = false,
            mIsPalletInPLEditable = false,
            mIsSO2UseActualItem = false;

        private string mLocalDocument = "0";
        private string
            mPortForFTPClient = string.Empty,
            mHostAddrForFTPClient = string.Empty,
            mSharedFolderForFTPClient = string.Empty,
            mUsernameForFTPClient = string.Empty,
            mPasswordForFTPClient = string.Empty,
            mFileSizeMaxUploadFTPClient = string.Empty,
            mFormatFTPClient = string.Empty;
        private byte[] downloadedData;
        #endregion

        #region Constructor

        public FrmPL(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Packing List";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);

                if (mLocalDocument == "1")
                    LblLocalDoc.Text = "      No. Invoice";

                base.FrmLoad(sender, e);
                LueItCode1.Visible = false;
                LueItCode2.Visible = false;
                LueItCode3.Visible = false;
                LueItCode4.Visible = false;
                LueItCode5.Visible = false;
                LueItCode6.Visible = false;
                LueItCode7.Visible = false;
                LueItCode8.Visible = false;
                LueItCode9.Visible = false;
                LueItCode10.Visible = false;
                LueItCode11.Visible = false;
                LueItCode12.Visible = false;
                LueItCode13.Visible = false;
                LueItCode14.Visible = false;
                LueItCode15.Visible = false;
                LueItCode16.Visible = false;
                LueItCode17.Visible = false;
                LueItCode18.Visible = false;
                LueItCode19.Visible = false;
                LueItCode20.Visible = false;
                LueItCode21.Visible = false;
                LueItCode22.Visible = false;
                LueItCode23.Visible = false;
                LueItCode24.Visible = false;
                LueItCode25.Visible = false;

                SetTabControls("2", ref LueSize2);
                SetTabControls("3", ref LueSize3);
                SetTabControls("4", ref LueSize4);
                SetTabControls("5", ref LueSize5);
                SetTabControls("6", ref LueSize6);
                SetTabControls("7", ref LueSize7);
                SetTabControls("8", ref LueSize8);
                SetTabControls("9", ref LueSize9);
                SetTabControls("10", ref LueSize10);
                SetTabControls("11", ref LueSize11);
                SetTabControls("12", ref LueSize12);
                SetTabControls("13", ref LueSize13);
                SetTabControls("14", ref LueSize14);
                SetTabControls("15", ref LueSize15);
                SetTabControls("16", ref LueSize16);
                SetTabControls("17", ref LueSize17);
                SetTabControls("18", ref LueSize18);
                SetTabControls("19", ref LueSize19);
                SetTabControls("20", ref LueSize20);
                SetTabControls("21", ref LueSize21);
                SetTabControls("22", ref LueSize22);
                SetTabControls("23", ref LueSize23);
                SetTabControls("24", ref LueSize24);
                SetTabControls("25", ref LueSize25);
                SetTabControls("1", ref LueSize1);
                Sm.FocusGrd(Grd11, 0, 1);
                
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
             }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'IsSO2UseActualItem', 'IsPalletInPLEditable', 'IsNotCopySalesLocalDocNo', 'IsCreditLimitValidate', 'IsCustomerItemNameMandatory', ");
            SQL.AppendLine("'LocalDocument', 'HostAddrForFTPClient', 'SharedFolderForFTPClient', 'UsernameForFTPClient', 'PasswordForFTPClient', ");
            SQL.AppendLine("'PortForFTPClient', 'FileSizeMaxUploadFTPClient', 'FormatFTPClient', 'IsLocalDocNoAccesible' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsNotCopySalesLocalDocNo": mIsNotCopySalesLocalDocNo = ParValue == "Y"; break;
                            case "IsSO2UseActualItem": mIsSO2UseActualItem = ParValue == "Y"; break;
                            case "IsPalletInPLEditable": mIsPalletInPLEditable = ParValue == "Y"; break;
                            case "IsCreditLimitValidate": mIsCreditLimitValidate = ParValue == "Y"; break;
                            case "IsCustomerItemNameMandatory": mIsCustomerItemNameMandatory = ParValue == "Y"; break;
                            case "IsLocalDocNoAccesible": mIsLocalDocNoAccesible = ParValue == "Y"; break;

                            //string
                            case "LocalDocument": mLocalDocument = ParValue; break;
                            case "HostAddrForFTPClient": mHostAddrForFTPClient = ParValue; break;
                            case "SharedFolderForFTPClient": mSharedFolderForFTPClient = ParValue; break;
                            case "UsernameForFTPClient": mUsernameForFTPClient = ParValue; break;
                            case "PasswordForFTPClient": mPasswordForFTPClient = ParValue; break;
                            case "PortForFTPClient": mPortForFTPClient = ParValue; break;
                            case "FileSizeMaxUploadFTPClient": mFileSizeMaxUploadFTPClient = ParValue; break;
                            case "FormatFTPClient": mFormatFTPClient = ParValue; break;
                        }
                    }
                }
                dr.Close();
            }
        }

        private void SetGrd()
        {
            // note : Kalau ada nambah grid container baru, tolong tambah juga grid tsb di method ProcessPallets() dan IsSealEmpty(). ty.
            SetGrdItem(new List<iGrid> 
                { 
                    Grd11, Grd12, Grd13, Grd14, Grd15,
                    Grd16, Grd17, Grd18, Grd19, Grd20,
                    Grd21, Grd22, Grd23, Grd24, Grd25,
                    Grd26, Grd27, Grd28, Grd29, Grd30,
                    Grd31, Grd32, Grd33, Grd34, Grd35
                });

            #region Grid1
            Grd1.Cols.Count = 25;
            Grd1.FrozenArea.ColCount = 4;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "",
                        
                        //1-5
                        "Item's"+Environment.NewLine+"Code",
                        "Item's"+Environment.NewLine+"Name",
                        "",
                        "Item's"+Environment.NewLine+"Local Code",
                        "SO#",

                        //6-10
                        "",
                        "SO DNo",
                        "Packaging"+Environment.NewLine+"Unit",
                        "Outstanding"+Environment.NewLine+"Packaging"+Environment.NewLine+"Quantity",
                        "Packaging"+Environment.NewLine+"Quantity",

                        //11-15
                        "Balance",
                        "Outstanding"+Environment.NewLine+"SO",
                        "Requested"+Environment.NewLine+"Quantity (Sales)",
                        "Balance",
                        "UoM"+Environment.NewLine+"(Sales)",

                        //16-20
                        "Requested"+Environment.NewLine+"Quantity (Inventory)",
                        "UoM"+Environment.NewLine+"(Inventory)",
                        "Gross Weight",
                        "Netto Weight",
                        "Delivery"+Environment.NewLine+"Date",

                        //21-24
                        "Remark",
                        //untuk credit limit 
                        "UPrice",
                        "Customer's"+Environment.NewLine+"Item Code",
                        "Customer's"+Environment.NewLine+"Item Name"
                    },
                     new int[] 
                    {
                        20, 
                        80, 200, 20, 150, 150, 
                        20, 50, 100, 100, 100,   
                        80, 100, 100, 80, 80, 
                        130, 80, 100, 100, 100, 
                        150, 150, 0, 0
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 20 });
            Sm.GrdColReadOnly(Grd1, new int[] { 1, 2, 4, 5, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24 });
            Sm.GrdFormatDec(Grd1, new int[] { 8, 9, 10, 11, 12, 13, 14, 16, 18, 19, 22}, 0);
            Sm.GrdColButton(Grd1, new int[] { 0, 3, 6});
            Sm.GrdColInvisible(Grd1, new int[] { 0, 1, 4, 7, 22 }, false);
            if (mIsCustomerItemNameMandatory)
            {
                Grd1.Cols[23].Move(5);
                Grd1.Cols[24].Move(6);
            }
            else
            {
                Sm.GrdColInvisible(Grd1, new int[] { 23, 24 }, false);
            }
            #endregion
        }


        private void SetGrdItem(List<iGrid> ListofGrd)
        {
            ListofGrd.ForEach(Grd =>
            {
                Grd.Cols.Count = 23;
                Grd.FrozenArea.ColCount = 2;
                Grd.ReadOnly = false;
                Sm.GrdHdrWithColWidth(
                        Grd,
                        new string[] 
                        {
                        //0  
                        "Item's"+Environment.NewLine+"Code",

                        //1-5
                        "Item's"+Environment.NewLine+"Name",
                        "",
                        "Item's"+Environment.NewLine+"Local Code",
                        "SO#",
                        "",

                        //6-10
                        "SO DNo",
                        "Pallets",
                        "Pallets"+Environment.NewLine+"Quantity",
                        "Packaging"+Environment.NewLine+"Unit",
                        "Packaging"+Environment.NewLine+"Quantity",
                       
                        //11-15
                        "Requested"+Environment.NewLine+"Quantity (Sales)",
                        "UoM"+Environment.NewLine+"(Sales)",
                        "Requested"+Environment.NewLine+"Quantity (Inventory)",
                        "UoM"+Environment.NewLine+"(Inventory)",
                        "Gross Weight",
                        //16-20
                        "Netto Weight",
                        "Remark",
                        "ProcessInd",
                        "ProcessInd2",
                        "Customer's"+Environment.NewLine+"Item Code",

                        //21-22
                        "Customer's"+Environment.NewLine+"Item Name",
                        "Total Volume",
                        },
                        new int[] 
                        { 
                            80, 
                            200, 20, 100, 150, 20, 
                            50, 100, 100, 100, 100,  
                            100, 80, 100, 80, 100, 
                            100, 100, 80, 80, 0,
                            0, 120
                        }
                    );
                Sm.GrdColReadOnly(Grd, new int[] { 3, 4, 6, 9, 11, 12, 13, 14, 15, 16, 18, 19, 20 });
                Sm.GrdFormatDec(Grd, new int[] { 8, 10, 11, 13, 15, 16, 22 }, 0);
                Sm.GrdColButton(Grd, new int[] {  2, 5 });
                Sm.GrdColInvisible(Grd, new int[] { 0, 3, 6, 18, 19 }, false);
                Grd.Cols[22].Move(17);
                if (mIsCustomerItemNameMandatory)
                {
                    Grd.Cols[20].Move(4);
                    Grd.Cols[21].Move(5);
                }
                else
                {
                    Sm.GrdColInvisible(Grd, new int[] { 20, 21 }, false);
                }
                Grd.Cols[2].Move(1);
                Grd.Cols[5].Move(4);
            });
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, LueCtCode, TxtSIDocNo, LueNotify, TxtInvoice, TxtLocalDocNo,
                        TxtSalesContract, TxtLcNo, LCDocDt, MeeAccount, MeeIssued, MeeIssued2, MeeIssued3,
                        MeeIssued4, MeeOrder, MeeOrder2, MeeOrder3, MeeOrder4, MeeRemark, MeeRemark2, TxtSource,
                        TxtCnt1, TxtCnt2, TxtCnt3, TxtCnt4, TxtCnt5, 
                        TxtCnt6, TxtCnt7, TxtCnt8, TxtCnt9, TxtCnt10,
                        TxtCnt11, TxtCnt12, TxtCnt13, TxtCnt14, TxtCnt15, 
                        TxtCnt16, TxtCnt17, TxtCnt18, TxtCnt19, TxtCnt20,
                        TxtCnt21, TxtCnt22, TxtCnt23, TxtCnt24, TxtCnt25,
                        TxtSeal1, TxtSeal2, TxtSeal3, TxtSeal4, TxtSeal5, 
                        TxtSeal6, TxtSeal7, TxtSeal8, TxtSeal9, TxtSeal10,
                        TxtSeal11, TxtSeal12, TxtSeal13, TxtSeal14, TxtSeal15,
                        TxtSeal16, TxtSeal17, TxtSeal18, TxtSeal19, TxtSeal20,
                        TxtSeal21, TxtSeal22, TxtSeal23, TxtSeal24, TxtSeal25, 
                        TxtTotal1, TxtTotal2, TxtTotal3, TxtTotal4, TxtTotal5, 
                        TxtTotal6, TxtTotal7, TxtTotal8, TxtTotal9, TxtTotal10,
                        TxtTotal11, TxtTotal12, TxtTotal13, TxtTotal14, TxtTotal15,
                        TxtTotal16, TxtTotal17, TxtTotal18, TxtTotal19, TxtTotal20,
                        TxtTotal21, TxtTotal22, TxtTotal23, TxtTotal24, TxtTotal25, 
                        LueSize1, LueSize2, LueSize3, LueSize4, LueSize5,
                        LueSize6, LueSize7, LueSize8, LueSize9, LueSize10, 
                        LueSize11, LueSize12, LueSize13, LueSize14, LueSize15,
                        LueSize16, LueSize17, LueSize18, LueSize19, LueSize20, 
                        LueSize21, LueSize22, LueSize23, LueSize24, LueSize25,
                        MeeSM1, MeeSM2, MeeSM3, MeeSM4, MeeSM5,
                        MeeSM6, MeeSM7, MeeSM8, MeeSM9, MeeSM10, 
                        MeeSM11, MeeSM12, MeeSM13, MeeSM14, MeeSM15,
                        MeeSM16, MeeSM17, MeeSM18, MeeSM19, MeeSM20, 
                        MeeSM21, MeeSM22, MeeSM23, MeeSM24, MeeSM25,

                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd11, new int[] { 0, 1, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 20, 21, 22 });
                    Sm.GrdColReadOnly(true, true, Grd12, new int[] { 0, 1, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 20, 21, 22 });
                    Sm.GrdColReadOnly(true, true, Grd13, new int[] { 0, 1, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 20, 21, 22 });
                    Sm.GrdColReadOnly(true, true, Grd14, new int[] { 0, 1, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 20, 21, 22 });
                    Sm.GrdColReadOnly(true, true, Grd15, new int[] { 0, 1, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 20, 21, 22 });
                    Sm.GrdColReadOnly(true, true, Grd16, new int[] { 0, 1, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 20, 21, 22 });
                    Sm.GrdColReadOnly(true, true, Grd17, new int[] { 0, 1, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 20, 21, 22 });
                    Sm.GrdColReadOnly(true, true, Grd18, new int[] { 0, 1, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 20, 21, 22 });
                    Sm.GrdColReadOnly(true, true, Grd19, new int[] { 0, 1, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 20, 21, 22 });
                    Sm.GrdColReadOnly(true, true, Grd20, new int[] { 0, 1, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 20, 21, 22 });
                    Sm.GrdColReadOnly(true, true, Grd21, new int[] { 0, 1, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 20, 21, 22 });
                    Sm.GrdColReadOnly(true, true, Grd22, new int[] { 0, 1, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 20, 21, 22 });
                    Sm.GrdColReadOnly(true, true, Grd23, new int[] { 0, 1, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 20, 21, 22 });
                    Sm.GrdColReadOnly(true, true, Grd24, new int[] { 0, 1, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 20, 21, 22 });
                    Sm.GrdColReadOnly(true, true, Grd25, new int[] { 0, 1, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 20, 21, 22 });
                    Sm.GrdColReadOnly(true, true, Grd26, new int[] { 0, 1, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 20, 21, 22 });
                    Sm.GrdColReadOnly(true, true, Grd27, new int[] { 0, 1, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 20, 21, 22 });
                    Sm.GrdColReadOnly(true, true, Grd28, new int[] { 0, 1, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 20, 21, 22 });
                    Sm.GrdColReadOnly(true, true, Grd29, new int[] { 0, 1, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 20, 21, 22 });
                    Sm.GrdColReadOnly(true, true, Grd30, new int[] { 0, 1, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 20, 21, 22 });
                    Sm.GrdColReadOnly(true, true, Grd31, new int[] { 0, 1, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 20, 21, 22 });
                    Sm.GrdColReadOnly(true, true, Grd33, new int[] { 0, 1, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 20, 21, 22 });
                    Sm.GrdColReadOnly(true, true, Grd33, new int[] { 0, 1, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 20, 21, 22 });
                    Sm.GrdColReadOnly(true, true, Grd33, new int[] { 0, 1, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 20, 21, 22 });
                    Sm.GrdColReadOnly(true, true, Grd33, new int[] { 0, 1, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 20, 21, 22 });
                    Sm.GrdColReadOnly(true, true, Grd33, new int[] { 0, 1, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 20, 21, 22 });
                    Sm.GrdColReadOnly(true, true, Grd32, new int[] { 0, 1, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 20, 21, 22 });
                    Sm.GrdColReadOnly(true, true, Grd33, new int[] { 0, 1, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 20, 21, 22 });
                    Sm.GrdColReadOnly(true, true, Grd34, new int[] { 0, 1, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 20, 21, 22 });
                    Sm.GrdColReadOnly(true, true, Grd35, new int[] { 0, 1, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 20, 21, 22 });
                    BtnSI.Enabled = false;
                    BtnSI2.Enabled = true;
                    BtnSource.Enabled = false;
                    TxtDocNo.Focus();
                    Sm.FocusGrd(Grd11, 0, 1);
                    break;
                case mState.Insert:
                    if (mIsLocalDocNoAccesible)
                    {
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                        {   
                            DteDocDt, LueCtCode,LueNotify, MeeAccount, TxtLocalDocNo,
                            TxtSalesContract, TxtLcNo, LCDocDt, MeeIssued, MeeIssued2, MeeIssued3,
                            MeeIssued4, MeeOrder, MeeOrder2, MeeOrder3, MeeOrder4, MeeRemark, MeeRemark2, 
                            TxtCnt1, TxtCnt2, TxtCnt3, TxtCnt4, TxtCnt5, 
                            TxtCnt6, TxtCnt7, TxtCnt8, TxtCnt9, TxtCnt10,
                            TxtCnt11, TxtCnt12, TxtCnt13, TxtCnt14, TxtCnt15, 
                            TxtCnt16, TxtCnt17, TxtCnt18, TxtCnt19, TxtCnt20,
                            TxtCnt21, TxtCnt22, TxtCnt23, TxtCnt24, TxtCnt25,
                            TxtSeal1, TxtSeal2, TxtSeal3, TxtSeal4, TxtSeal5, 
                            TxtSeal6, TxtSeal7, TxtSeal8, TxtSeal9, TxtSeal10,
                            TxtSeal11, TxtSeal12, TxtSeal13, TxtSeal14, TxtSeal15,
                            TxtSeal16, TxtSeal17, TxtSeal18, TxtSeal19, TxtSeal20,
                            TxtSeal21, TxtSeal22, TxtSeal23, TxtSeal24, TxtSeal25, 
                            LueSize1, LueSize2, LueSize3, LueSize4, LueSize5,
                            LueSize6, LueSize7, LueSize8, LueSize9, LueSize10, 
                            LueSize11, LueSize12, LueSize13, LueSize14, LueSize15,
                            LueSize16, LueSize17, LueSize18, LueSize19, LueSize20, 
                            LueSize21, LueSize22, LueSize23, LueSize24, LueSize25,
                            MeeSM1, MeeSM2, MeeSM3, MeeSM4, MeeSM5,
                            MeeSM6, MeeSM7, MeeSM8, MeeSM9, MeeSM10, 
                            MeeSM11, MeeSM12, MeeSM13, MeeSM14, MeeSM15,
                            MeeSM16, MeeSM17, MeeSM18, MeeSM19, MeeSM20, 
                            MeeSM21, MeeSM22, MeeSM23, MeeSM24, MeeSM25,
                        }, false);
                    }
                    else
                    {
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                        {   
                            DteDocDt, LueCtCode,LueNotify, MeeAccount,
                            TxtSalesContract, TxtLcNo, LCDocDt, MeeIssued, MeeIssued2, MeeIssued3,
                            MeeIssued4, MeeOrder, MeeOrder2, MeeOrder3, MeeOrder4, MeeRemark, MeeRemark2, 
                            TxtCnt1, TxtCnt2, TxtCnt3, TxtCnt4, TxtCnt5, 
                            TxtCnt6, TxtCnt7, TxtCnt8, TxtCnt9, TxtCnt10,
                            TxtCnt11, TxtCnt12, TxtCnt13, TxtCnt14, TxtCnt15, 
                            TxtCnt16, TxtCnt17, TxtCnt18, TxtCnt19, TxtCnt20,
                            TxtCnt21, TxtCnt22, TxtCnt23, TxtCnt24, TxtCnt25,
                            TxtSeal1, TxtSeal2, TxtSeal3, TxtSeal4, TxtSeal5, 
                            TxtSeal6, TxtSeal7, TxtSeal8, TxtSeal9, TxtSeal10,
                            TxtSeal11, TxtSeal12, TxtSeal13, TxtSeal14, TxtSeal15,
                            TxtSeal16, TxtSeal17, TxtSeal18, TxtSeal19, TxtSeal20,
                            TxtSeal21, TxtSeal22, TxtSeal23, TxtSeal24, TxtSeal25, 
                            LueSize1, LueSize2, LueSize3, LueSize4, LueSize5,
                            LueSize6, LueSize7, LueSize8, LueSize9, LueSize10, 
                            LueSize11, LueSize12, LueSize13, LueSize14, LueSize15,
                            LueSize16, LueSize17, LueSize18, LueSize19, LueSize20, 
                            LueSize21, LueSize22, LueSize23, LueSize24, LueSize25,
                            MeeSM1, MeeSM2, MeeSM3, MeeSM4, MeeSM5,
                            MeeSM6, MeeSM7, MeeSM8, MeeSM9, MeeSM10, 
                            MeeSM11, MeeSM12, MeeSM13, MeeSM14, MeeSM15,
                            MeeSM16, MeeSM17, MeeSM18, MeeSM19, MeeSM20, 
                            MeeSM21, MeeSM22, MeeSM23, MeeSM24, MeeSM25,
                        }, false);
                    }
                    Sm.GrdColReadOnly(false, true, Grd11, new int[] { 1, 7, 8, 10, 17, 18, 22 });
                    Sm.GrdColReadOnly(false, true, Grd12, new int[] { 1, 7, 8, 10, 17, 18, 22 });
                    Sm.GrdColReadOnly(false, true, Grd13, new int[] { 1, 7, 8, 10, 17, 18, 22 });
                    Sm.GrdColReadOnly(false, true, Grd14, new int[] { 1, 7, 8, 10, 17, 18, 22 });
                    Sm.GrdColReadOnly(false, true, Grd15, new int[] { 1, 7, 8, 10, 17, 18, 22 });
                    Sm.GrdColReadOnly(false, true, Grd16, new int[] { 1, 7, 8, 10, 17, 18, 22 });
                    Sm.GrdColReadOnly(false, true, Grd17, new int[] { 1, 7, 8, 10, 17, 18, 22 });
                    Sm.GrdColReadOnly(false, true, Grd18, new int[] { 1, 7, 8, 10, 17, 18, 22 });
                    Sm.GrdColReadOnly(false, true, Grd19, new int[] { 1, 7, 8, 10, 17, 18, 22 });
                    Sm.GrdColReadOnly(false, true, Grd20, new int[] { 1, 7, 8, 10, 17, 18, 22 });
                    Sm.GrdColReadOnly(false, true, Grd21, new int[] { 1, 7, 8, 10, 17, 18, 22 });
                    Sm.GrdColReadOnly(false, true, Grd22, new int[] { 1, 7, 8, 10, 17, 18, 22 });
                    Sm.GrdColReadOnly(false, true, Grd23, new int[] { 1, 7, 8, 10, 17, 18, 22 });
                    Sm.GrdColReadOnly(false, true, Grd24, new int[] { 1, 7, 8, 10, 17, 18, 22 });
                    Sm.GrdColReadOnly(false, true, Grd25, new int[] { 1, 7, 8, 10, 17, 18, 22 });
                    Sm.GrdColReadOnly(false, true, Grd26, new int[] { 1, 7, 8, 10, 17, 18, 22 });
                    Sm.GrdColReadOnly(false, true, Grd27, new int[] { 1, 7, 8, 10, 17, 18, 22 });
                    Sm.GrdColReadOnly(false, true, Grd28, new int[] { 1, 7, 8, 10, 17, 18, 22 });
                    Sm.GrdColReadOnly(false, true, Grd29, new int[] { 1, 7, 8, 10, 17, 18, 22 });
                    Sm.GrdColReadOnly(false, true, Grd30, new int[] { 1, 7, 8, 10, 17, 18, 22 });
                    Sm.GrdColReadOnly(false, true, Grd31, new int[] { 1, 7, 8, 10, 17, 18, 22 });
                    Sm.GrdColReadOnly(false, true, Grd32, new int[] { 1, 7, 8, 10, 17, 18, 22 });
                    Sm.GrdColReadOnly(false, true, Grd33, new int[] { 1, 7, 8, 10, 17, 18, 22 });
                    Sm.GrdColReadOnly(false, true, Grd34, new int[] { 1, 7, 8, 10, 17, 22 });
                    Sm.GrdColReadOnly(false, true, Grd35, new int[] { 1, 7, 8, 10, 17, 22 });

                    #region Pallet ngga bisa di edit
                    if (!mIsPalletInPLEditable)
                    {
                        Sm.GrdColReadOnly(true, true, Grd11, new int[] { 7 });
                        Sm.GrdColReadOnly(true, true, Grd12, new int[] { 7 });
                        Sm.GrdColReadOnly(true, true, Grd13, new int[] { 7 });
                        Sm.GrdColReadOnly(true, true, Grd14, new int[] { 7 });
                        Sm.GrdColReadOnly(true, true, Grd15, new int[] { 7 });
                        Sm.GrdColReadOnly(true, true, Grd16, new int[] { 7 });
                        Sm.GrdColReadOnly(true, true, Grd17, new int[] { 7 });
                        Sm.GrdColReadOnly(true, true, Grd18, new int[] { 7 });
                        Sm.GrdColReadOnly(true, true, Grd19, new int[] { 7 });
                        Sm.GrdColReadOnly(true, true, Grd20, new int[] { 7 });
                        Sm.GrdColReadOnly(true, true, Grd21, new int[] { 7 });
                        Sm.GrdColReadOnly(true, true, Grd22, new int[] { 7 });
                        Sm.GrdColReadOnly(true, true, Grd23, new int[] { 7 });
                        Sm.GrdColReadOnly(true, true, Grd24, new int[] { 7 });
                        Sm.GrdColReadOnly(true, true, Grd25, new int[] { 7 });
                        Sm.GrdColReadOnly(true, true, Grd26, new int[] { 7 });
                        Sm.GrdColReadOnly(true, true, Grd27, new int[] { 7 });
                        Sm.GrdColReadOnly(true, true, Grd28, new int[] { 7 });
                        Sm.GrdColReadOnly(true, true, Grd29, new int[] { 7 });
                        Sm.GrdColReadOnly(true, true, Grd30, new int[] { 7 });
                        Sm.GrdColReadOnly(true, true, Grd31, new int[] { 7 });
                        Sm.GrdColReadOnly(true, true, Grd32, new int[] { 7 });
                        Sm.GrdColReadOnly(true, true, Grd33, new int[] { 7 });
                        Sm.GrdColReadOnly(true, true, Grd34, new int[] { 7 });
                        Sm.GrdColReadOnly(true, true, Grd35, new int[] { 7 });
                    }
                    #endregion

                    BtnSI.Enabled = true;
                    BtnSI2.Enabled = true;
                    BtnSource.Enabled = true;
                    DteDocDt.Focus();
                    Sm.FocusGrd(Grd11, 0, 1);
                    break;
                case mState.Edit:
                    var cm = new MySqlCommand()
                    {
                        CommandText =
                            "Select A.DocNo From TblSP A "+ 
                            "Inner Join TblSIHdr B On A.DocNo = B.SPDocNo " +
                            "Where A.Status <> 'P' And B.DocNo=@DocNo "
                    };
                    Sm.CmParam<String>(ref cm, "@DocNo", TxtSIDocNo.Text);

                    if (Sm.IsDataExist(cm))
                    {
                        if (mIsLocalDocNoAccesible)
                        {
                            Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                            {
                               MeeAccount, TxtLocalDocNo,
                               TxtSalesContract, DteDocDt, TxtLcNo, LCDocDt, MeeIssued, MeeIssued2, MeeIssued3, 
                               MeeIssued4, MeeOrder, MeeOrder2, MeeOrder3, MeeOrder4,MeeRemark, MeeRemark2, TxtSource,
                            }, false);
                        }
                        else
                        {
                            Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                            {
                               MeeAccount,
                               TxtSalesContract, DteDocDt, TxtLcNo, LCDocDt, MeeIssued, MeeIssued2, MeeIssued3, 
                               MeeIssued4, MeeOrder, MeeOrder2, MeeOrder3, MeeOrder4,MeeRemark, MeeRemark2, TxtSource,
                            }, false);
                        }
                    }
                    else
                    {
                        if (mIsLocalDocNoAccesible)
                        {
                            Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                            {   
                                MeeAccount, TxtLocalDocNo,
                                TxtSalesContract, DteDocDt, TxtLcNo, LCDocDt, MeeIssued, MeeIssued2, MeeIssued3,
                                MeeIssued4, MeeOrder, MeeOrder2, MeeOrder3, MeeOrder4,MeeRemark, MeeRemark2,  TxtSource,
                                TxtCnt1, TxtCnt2, TxtCnt3, TxtCnt4, TxtCnt5, 
                                TxtCnt6, TxtCnt7, TxtCnt8, TxtCnt9, TxtCnt10,
                                TxtCnt11, TxtCnt12, TxtCnt13, TxtCnt14, TxtCnt15,
                                TxtSeal1, TxtSeal2, TxtSeal3, TxtSeal4, TxtSeal5, 
                                TxtSeal6, TxtSeal7, TxtSeal8, TxtSeal9, TxtSeal10,
                                TxtSeal11, TxtSeal12, TxtSeal13, TxtSeal14, TxtSeal15, 
                                LueSize1, LueSize2, LueSize3, LueSize4, LueSize5,
                                LueSize6, LueSize7, LueSize8, LueSize9, LueSize10, 
                                LueSize11, LueSize12, LueSize13, LueSize14, LueSize15,
                                LueSize16, LueSize17, LueSize18, LueSize19, LueSize20, 
                                LueSize21, LueSize22, LueSize23, LueSize24, LueSize25,
                                MeeSM1, MeeSM2, MeeSM3, MeeSM4, MeeSM5,
                                MeeSM6, MeeSM7, MeeSM8, MeeSM9, MeeSM10, 
                                MeeSM11, MeeSM12, MeeSM13, MeeSM14, MeeSM15,
                                MeeSM16, MeeSM17, MeeSM18, MeeSM19, MeeSM20, 
                                MeeSM21, MeeSM22, MeeSM23, MeeSM24, MeeSM25,
                            }, false);
                        }
                        else
                        {

                            Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                            {   
                                MeeAccount,
                                TxtSalesContract, DteDocDt, TxtLcNo, LCDocDt, MeeIssued, MeeIssued2, MeeIssued3,
                                MeeIssued4, MeeOrder, MeeOrder2, MeeOrder3, MeeOrder4,MeeRemark, MeeRemark2,  TxtSource,
                                TxtCnt1, TxtCnt2, TxtCnt3, TxtCnt4, TxtCnt5, 
                                TxtCnt6, TxtCnt7, TxtCnt8, TxtCnt9, TxtCnt10,
                                TxtCnt11, TxtCnt12, TxtCnt13, TxtCnt14, TxtCnt15,
                                TxtSeal1, TxtSeal2, TxtSeal3, TxtSeal4, TxtSeal5, 
                                TxtSeal6, TxtSeal7, TxtSeal8, TxtSeal9, TxtSeal10,
                                TxtSeal11, TxtSeal12, TxtSeal13, TxtSeal14, TxtSeal15, 
                                LueSize1, LueSize2, LueSize3, LueSize4, LueSize5,
                                LueSize6, LueSize7, LueSize8, LueSize9, LueSize10, 
                                LueSize11, LueSize12, LueSize13, LueSize14, LueSize15,
                                LueSize16, LueSize17, LueSize18, LueSize19, LueSize20, 
                                LueSize21, LueSize22, LueSize23, LueSize24, LueSize25,
                                MeeSM1, MeeSM2, MeeSM3, MeeSM4, MeeSM5,
                                MeeSM6, MeeSM7, MeeSM8, MeeSM9, MeeSM10, 
                                MeeSM11, MeeSM12, MeeSM13, MeeSM14, MeeSM15,
                                MeeSM16, MeeSM17, MeeSM18, MeeSM19, MeeSM20, 
                                MeeSM21, MeeSM22, MeeSM23, MeeSM24, MeeSM25,
                            }, false);
                        }
                    }

                    Sm.GrdColReadOnly(false, true, Grd11, new int[] { 1, 7, 8, 10, 17, 22 });
                    Sm.GrdColReadOnly(false, true, Grd12, new int[] { 1, 7, 8, 10, 17, 22 });
                    Sm.GrdColReadOnly(false, true, Grd13, new int[] { 1, 7, 8, 10, 17, 22 });
                    Sm.GrdColReadOnly(false, true, Grd14, new int[] { 1, 7, 8, 10, 17, 22 });
                    Sm.GrdColReadOnly(false, true, Grd15, new int[] { 1, 7, 8, 10, 17, 22 });
                    Sm.GrdColReadOnly(false, true, Grd16, new int[] { 1, 7, 8, 10, 17, 22 });
                    Sm.GrdColReadOnly(false, true, Grd17, new int[] { 1, 7, 8, 10, 17, 22 });
                    Sm.GrdColReadOnly(false, true, Grd18, new int[] { 1, 7, 8, 10, 17, 22 });
                    Sm.GrdColReadOnly(false, true, Grd19, new int[] { 1, 7, 8, 10, 17, 22 });
                    Sm.GrdColReadOnly(false, true, Grd20, new int[] { 1, 7, 8, 10, 17, 22 });
                    Sm.GrdColReadOnly(false, true, Grd21, new int[] { 1, 7, 8, 10, 17, 22 });
                    Sm.GrdColReadOnly(false, true, Grd22, new int[] { 1, 7, 8, 10, 17, 22 });
                    Sm.GrdColReadOnly(false, true, Grd23, new int[] { 1, 7, 8, 10, 17, 22 });
                    Sm.GrdColReadOnly(false, true, Grd24, new int[] { 1, 7, 8, 10, 17, 22 });
                    Sm.GrdColReadOnly(false, true, Grd25, new int[] { 1, 7, 8, 10, 17, 22 });
                    Sm.GrdColReadOnly(false, true, Grd26, new int[] { 1, 7, 8, 10, 17, 22 });
                    Sm.GrdColReadOnly(false, true, Grd27, new int[] { 1, 7, 8, 10, 17, 22 });
                    Sm.GrdColReadOnly(false, true, Grd28, new int[] { 1, 7, 8, 10, 17, 22 });
                    Sm.GrdColReadOnly(false, true, Grd29, new int[] { 1, 7, 8, 10, 17, 22 });
                    Sm.GrdColReadOnly(false, true, Grd30, new int[] { 1, 7, 8, 10, 17, 22 });
                    Sm.GrdColReadOnly(false, true, Grd31, new int[] { 1, 7, 8, 10, 17, 22 });
                    Sm.GrdColReadOnly(false, true, Grd32, new int[] { 1, 7, 8, 10, 17, 22 });
                    Sm.GrdColReadOnly(false, true, Grd33, new int[] { 1, 7, 8, 10, 17, 22 });
                    Sm.GrdColReadOnly(false, true, Grd34, new int[] { 1, 7, 8, 10, 17, 22 });
                    Sm.GrdColReadOnly(false, true, Grd35, new int[] { 1, 7, 8, 10, 17, 22 });

                    #region Pallet ngga bisa di edit
                    if (!mIsPalletInPLEditable)
                    {
                        Sm.GrdColReadOnly(true, true, Grd11, new int[] { 7 });
                        Sm.GrdColReadOnly(true, true, Grd12, new int[] { 7 });
                        Sm.GrdColReadOnly(true, true, Grd13, new int[] { 7 });
                        Sm.GrdColReadOnly(true, true, Grd14, new int[] { 7 });
                        Sm.GrdColReadOnly(true, true, Grd15, new int[] { 7 });
                        Sm.GrdColReadOnly(true, true, Grd16, new int[] { 7 });
                        Sm.GrdColReadOnly(true, true, Grd17, new int[] { 7 });
                        Sm.GrdColReadOnly(true, true, Grd18, new int[] { 7 });
                        Sm.GrdColReadOnly(true, true, Grd19, new int[] { 7 });
                        Sm.GrdColReadOnly(true, true, Grd20, new int[] { 7 });
                        Sm.GrdColReadOnly(true, true, Grd21, new int[] { 7 });
                        Sm.GrdColReadOnly(true, true, Grd22, new int[] { 7 });
                        Sm.GrdColReadOnly(true, true, Grd23, new int[] { 7 });
                        Sm.GrdColReadOnly(true, true, Grd24, new int[] { 7 });
                        Sm.GrdColReadOnly(true, true, Grd25, new int[] { 7 });
                        Sm.GrdColReadOnly(true, true, Grd26, new int[] { 7 });
                        Sm.GrdColReadOnly(true, true, Grd27, new int[] { 7 });
                        Sm.GrdColReadOnly(true, true, Grd28, new int[] { 7 });
                        Sm.GrdColReadOnly(true, true, Grd29, new int[] { 7 });
                        Sm.GrdColReadOnly(true, true, Grd30, new int[] { 7 });
                        Sm.GrdColReadOnly(true, true, Grd31, new int[] { 7 });
                        Sm.GrdColReadOnly(true, true, Grd32, new int[] { 7 });
                        Sm.GrdColReadOnly(true, true, Grd33, new int[] { 7 });
                        Sm.GrdColReadOnly(true, true, Grd34, new int[] { 7 });
                        Sm.GrdColReadOnly(true, true, Grd35, new int[] { 7 });
                    }
                    #endregion
                    
                    BtnSI.Enabled = false;
                    BtnSI2.Enabled = true;
                    BtnSource.Enabled = false;
                    DteDocDt.Focus();
                    Sm.FocusGrd(Grd11, 0, 1);
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, TxtLocalDocNo, DteDocDt, LueCtCode, TxtSIDocNo, LueNotify, TxtInvoice, MeeAccount,
                TxtSalesContract, TxtLcNo, LCDocDt, MeeIssued, MeeIssued2, MeeIssued3,
                MeeIssued4, MeeOrder, MeeOrder2, MeeOrder3, MeeOrder4, MeeRemark, MeeRemark2, TxtSource,
                TxtCnt1, TxtCnt2, TxtCnt3, TxtCnt4, TxtCnt5, 
                TxtCnt6, TxtCnt7, TxtCnt8, TxtCnt9, TxtCnt10,
                TxtCnt11, TxtCnt12, TxtCnt13, TxtCnt14, TxtCnt15, 
                TxtCnt16, TxtCnt17, TxtCnt18, TxtCnt19, TxtCnt20,
                TxtCnt21, TxtCnt22, TxtCnt23, TxtCnt24, TxtCnt25,
                TxtSeal1, TxtSeal2, TxtSeal3, TxtSeal4, TxtSeal5, 
                TxtSeal6, TxtSeal7, TxtSeal8, TxtSeal9, TxtSeal10,
                TxtSeal11, TxtSeal12, TxtSeal13, TxtSeal14, TxtSeal15,
                TxtSeal16, TxtSeal17, TxtSeal18, TxtSeal19, TxtSeal20,
                TxtSeal21, TxtSeal22, TxtSeal23, TxtSeal24, TxtSeal25,  
                LueSize1, LueSize2, LueSize3, LueSize4, LueSize5,
                LueSize6, LueSize7, LueSize8, LueSize9, LueSize10, 
                LueSize11, LueSize12, LueSize13, LueSize14, LueSize15,
                LueSize16, LueSize17, LueSize18, LueSize19, LueSize20, 
                LueSize21, LueSize22, LueSize23, LueSize24, LueSize25,
                MeeSM1, MeeSM2, MeeSM3, MeeSM4, MeeSM5,
                MeeSM6, MeeSM7, MeeSM8, MeeSM9, MeeSM10, 
                MeeSM11, MeeSM12, MeeSM13, MeeSM14, MeeSM15,
                MeeSM16, MeeSM17, MeeSM18, MeeSM19, MeeSM20, 
                MeeSM21, MeeSM22, MeeSM23, MeeSM24, MeeSM25,
                TxtTab
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit>
            {
                TxtTotal1, TxtTotal2, TxtTotal3, TxtTotal4, TxtTotal5, 
                TxtTotal6, TxtTotal7, TxtTotal8, TxtTotal9, TxtTotal10,
                TxtTotal11, TxtTotal12, TxtTotal13, TxtTotal14, TxtTotal15,
                TxtTotal16, TxtTotal17, TxtTotal18, TxtTotal19, TxtTotal20,
                TxtTotal21, TxtTotal22, TxtTotal23, TxtTotal24, TxtTotal25,
            }, 0);

            ClearGrd(new List<iGrid>(){
                Grd11, Grd12, Grd13, Grd14, Grd15,
                Grd16, Grd17, Grd18, Grd19, Grd20,
                Grd21, Grd22, Grd23, Grd24, Grd25,
                Grd26, Grd27, Grd28, Grd29, Grd30,
                Grd31, Grd32, Grd33, Grd34, Grd35,
                Grd1
            });
        }
    
        private void ClearGrd(List<iGrid> ListOfGrd)
        {
            ListOfGrd.ForEach(Grd =>
            {
                Sm.ClearGrd(Grd, true);
                Sm.SetGrdNumValueZero(ref Grd, 0, new int[] { 8, 10, 11, 13, 22 });
                Sm.FocusGrd(Grd, 0, 0);
            });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmPLFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);

                Sm.SetDteCurrentDate(DteDocDt);
                LCDocDt.DateTime = Sm.ConvertDate(Sm.ServerCurrentDateTime());
                SetLueCtCode(ref LueCtCode, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
            CheckProcessInd(Grd11, TxtCnt1, TxtSeal1);
            CheckProcessInd(Grd12, TxtCnt2, TxtSeal2);
            CheckProcessInd(Grd13, TxtCnt3, TxtSeal3);
            CheckProcessInd(Grd14, TxtCnt4, TxtSeal4);
            CheckProcessInd(Grd15, TxtCnt5, TxtSeal5);
            CheckProcessInd(Grd16, TxtCnt6, TxtSeal6);
            CheckProcessInd(Grd17, TxtCnt7, TxtSeal7);
            CheckProcessInd(Grd18, TxtCnt8, TxtSeal8);
            CheckProcessInd(Grd19, TxtCnt9, TxtSeal9);
            CheckProcessInd(Grd20, TxtCnt10, TxtSeal10);
            CheckProcessInd(Grd21, TxtCnt11, TxtSeal11);
            CheckProcessInd(Grd22, TxtCnt12, TxtSeal12);
            CheckProcessInd(Grd23, TxtCnt13, TxtSeal13);
            CheckProcessInd(Grd24, TxtCnt14, TxtSeal14);
            CheckProcessInd(Grd25, TxtCnt15, TxtSeal15);
            CheckProcessInd(Grd26, TxtCnt16, TxtSeal16);
            CheckProcessInd(Grd27, TxtCnt17, TxtSeal17);
            CheckProcessInd(Grd28, TxtCnt18, TxtSeal18);
            CheckProcessInd(Grd29, TxtCnt19, TxtSeal19);
            CheckProcessInd(Grd30, TxtCnt20, TxtSeal20);
            CheckProcessInd(Grd31, TxtCnt21, TxtSeal21);
            CheckProcessInd(Grd32, TxtCnt22, TxtSeal22);
            CheckProcessInd(Grd33, TxtCnt23, TxtSeal23);
            CheckProcessInd(Grd34, TxtCnt24, TxtSeal24);
            CheckProcessInd(Grd35, TxtCnt25, TxtSeal25);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                InsertData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            ParPrint();
        }

        #endregion

        #region Grid method

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                
                    var f1 = new FrmItem(mMenuCode);
                    f1.Tag = mMenuCode;
                    f1.WindowState = FormWindowState.Normal;
                    f1.StartPosition = FormStartPosition.CenterScreen;
                    f1.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                    f1.ShowDialog();
            }

            if (e.ColIndex == 6 && Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length != 0)
            {
              
                    var f1 = new FrmSO2(mMenuCode);
                    f1.Tag = mMenuCode;
                    f1.WindowState = FormWindowState.Normal;
                    f1.StartPosition = FormStartPosition.CenterScreen;
                    f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 5);
                    f1.ShowDialog();
            }
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {

                var f1 = new FrmItem(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f1.ShowDialog();
            }

            if (e.ColIndex == 6 && Sm.GetGrdStr(Grd1, e.RowIndex, 5).Length != 0)
            {

                var f1 = new FrmSO2(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 5);
                f1.ShowDialog();
            }
        }

        #region Grd Request Edit

        private void Grd11_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
           
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 1 }, e.ColIndex)) 
            {
                LueRequestEdit(Grd11, LueItCode1, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd11, e.RowIndex);
                SetLueItCode(ref LueItCode1, Sm.GetLue(LueCtCode), TxtSIDocNo.Text);
              //  Sm.SetGrdNumValueZero(ref Grd11, Grd11.Rows.Count - 1, new int[] { 22 });
            }

            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd11, e.RowIndex, 0).Length != 0)
            {

                var f1 = new FrmItem(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mItCode = Sm.GetGrdStr(Grd11, e.RowIndex, 0);
                f1.ShowDialog();
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd11, e.RowIndex, 4).Length != 0)
            {

                var f1 = new FrmSO2(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd11, e.RowIndex, 4);
                f1.ShowDialog();
            }
        }

        private void Grd12_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 1 }, e.ColIndex))
            {
                LueRequestEdit(Grd12, LueItCode2, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd12, e.RowIndex);
                SetLueItCode(ref LueItCode2, Sm.GetLue(LueCtCode), TxtSIDocNo.Text);
            }

            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd12, e.RowIndex, 0).Length != 0)
            {

                var f1 = new FrmItem(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mItCode = Sm.GetGrdStr(Grd12, e.RowIndex, 0);
                f1.ShowDialog();
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd12, e.RowIndex, 4).Length != 0)
            {

                var f1 = new FrmSO2(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd12, e.RowIndex, 4);
                f1.ShowDialog();
            }
        }

        private void Grd13_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 1 }, e.ColIndex))
            {
                LueRequestEdit(Grd13, LueItCode3, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd13, e.RowIndex);
                SetLueItCode(ref LueItCode3, Sm.GetLue(LueCtCode), TxtSIDocNo.Text);
            }

            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd13, e.RowIndex, 0).Length != 0)
            {

                var f1 = new FrmItem(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mItCode = Sm.GetGrdStr(Grd13, e.RowIndex, 0);
                f1.ShowDialog();
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd13, e.RowIndex, 4).Length != 0)
            {

                var f1 = new FrmSO2(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd13, e.RowIndex, 4);
                f1.ShowDialog();
            }
        }

        private void Grd14_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 1 }, e.ColIndex))
            {
                LueRequestEdit(Grd14, LueItCode4, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd14, e.RowIndex);
                SetLueItCode(ref LueItCode4, Sm.GetLue(LueCtCode), TxtSIDocNo.Text);
            }

            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd14, e.RowIndex, 0).Length != 0)
            {

                var f1 = new FrmItem(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mItCode = Sm.GetGrdStr(Grd14, e.RowIndex, 0);
                f1.ShowDialog();
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd14, e.RowIndex, 4).Length != 0)
            {

                var f1 = new FrmSO2(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd14, e.RowIndex, 4);
                f1.ShowDialog();
            }
        }

        private void Grd15_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 1 }, e.ColIndex))
            {
                LueRequestEdit(Grd15, LueItCode5, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd15, e.RowIndex);
                SetLueItCode(ref LueItCode5, Sm.GetLue(LueCtCode), TxtSIDocNo.Text);
            }

            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd15, e.RowIndex, 0).Length != 0)
            {

                var f1 = new FrmItem(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mItCode = Sm.GetGrdStr(Grd15, e.RowIndex, 0);
                f1.ShowDialog();
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd15, e.RowIndex, 4).Length != 0)
            {

                var f1 = new FrmSO2(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd15, e.RowIndex, 4);
                f1.ShowDialog();
            }
        }

        private void Grd16_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 1 }, e.ColIndex) )
            {
                LueRequestEdit(Grd16, LueItCode6, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd16, e.RowIndex);
                SetLueItCode(ref LueItCode6, Sm.GetLue(LueCtCode), TxtSIDocNo.Text);
            }

            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd16, e.RowIndex, 0).Length != 0)
            {

                var f1 = new FrmItem(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mItCode = Sm.GetGrdStr(Grd16, e.RowIndex, 0);
                f1.ShowDialog();
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd16, e.RowIndex, 4).Length != 0)
            {

                var f1 = new FrmSO2(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd16, e.RowIndex, 4);
                f1.ShowDialog();
            }
        }

        private void Grd17_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 1 }, e.ColIndex) )
            {
                LueRequestEdit(Grd17, LueItCode7, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd17, e.RowIndex);
                SetLueItCode(ref LueItCode7, Sm.GetLue(LueCtCode), TxtSIDocNo.Text);
            }

            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd17, e.RowIndex, 0).Length != 0)
            {

                var f1 = new FrmItem(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mItCode = Sm.GetGrdStr(Grd17, e.RowIndex, 0);
                f1.ShowDialog();
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd17, e.RowIndex, 4).Length != 0)
            {

                var f1 = new FrmSO2(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd17, e.RowIndex, 4);
                f1.ShowDialog();
            }
        }

        private void Grd18_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 1 }, e.ColIndex))
            {
                LueRequestEdit(Grd18, LueItCode8, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd18, e.RowIndex);
                SetLueItCode(ref LueItCode8, Sm.GetLue(LueCtCode), TxtSIDocNo.Text);
            }

            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd18, e.RowIndex, 0).Length != 0)
            {

                var f1 = new FrmItem(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mItCode = Sm.GetGrdStr(Grd18, e.RowIndex, 0);
                f1.ShowDialog();
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd18, e.RowIndex, 4).Length != 0)
            {

                var f1 = new FrmSO2(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd18, e.RowIndex, 4);
                f1.ShowDialog();
            }
        }

        private void Grd19_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 1 }, e.ColIndex) )
            {
                LueRequestEdit(Grd19, LueItCode9, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd19, e.RowIndex);
                SetLueItCode(ref LueItCode9, Sm.GetLue(LueCtCode), TxtSIDocNo.Text);
            }

            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd19, e.RowIndex, 0).Length != 0)
            {

                var f1 = new FrmItem(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mItCode = Sm.GetGrdStr(Grd19, e.RowIndex, 0);
                f1.ShowDialog();
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd19, e.RowIndex, 4).Length != 0)
            {

                var f1 = new FrmSO2(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd19, e.RowIndex, 4);
                f1.ShowDialog();
            }
        }

        private void Grd20_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 1 }, e.ColIndex) )
            {
                LueRequestEdit(Grd20, LueItCode10, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd20, e.RowIndex);
                SetLueItCode(ref LueItCode10, Sm.GetLue(LueCtCode), TxtSIDocNo.Text);
            }

            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd20, e.RowIndex, 0).Length != 0)
            {

                var f1 = new FrmItem(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mItCode = Sm.GetGrdStr(Grd20, e.RowIndex, 0);
                f1.ShowDialog();
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd20, e.RowIndex, 4).Length != 0)
            {

                var f1 = new FrmSO2(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd20, e.RowIndex, 4);
                f1.ShowDialog();
            }
        }

        private void Grd21_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 1 }, e.ColIndex) )
            {
                LueRequestEdit(Grd21, LueItCode11, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd21, e.RowIndex);
                SetLueItCode(ref LueItCode11, Sm.GetLue(LueCtCode), TxtSIDocNo.Text);
            }

            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd21, e.RowIndex, 0).Length != 0)
            {

                var f1 = new FrmItem(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mItCode = Sm.GetGrdStr(Grd21, e.RowIndex, 0);
                f1.ShowDialog();
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd21, e.RowIndex, 4).Length != 0)
            {

                var f1 = new FrmSO2(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd21, e.RowIndex, 4);
                f1.ShowDialog();
            }
        }

        private void Grd22_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 1 }, e.ColIndex) )
            {
                LueRequestEdit(Grd22, LueItCode12, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd22, e.RowIndex);
                SetLueItCode(ref LueItCode12, Sm.GetLue(LueCtCode), TxtSIDocNo.Text);
            }

            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd22, e.RowIndex, 0).Length != 0)
            {

                var f1 = new FrmItem(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mItCode = Sm.GetGrdStr(Grd22, e.RowIndex, 0);
                f1.ShowDialog();
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd22, e.RowIndex, 4).Length != 0)
            {

                var f1 = new FrmSO2(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd22, e.RowIndex, 4);
                f1.ShowDialog();
            }
        }

        private void Grd23_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 1 }, e.ColIndex))
            {
                LueRequestEdit(Grd23, LueItCode13, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd23, e.RowIndex);
                SetLueItCode(ref LueItCode13, Sm.GetLue(LueCtCode), TxtSIDocNo.Text);
            }

            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd23, e.RowIndex, 0).Length != 0)
            {

                var f1 = new FrmItem(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mItCode = Sm.GetGrdStr(Grd23, e.RowIndex, 0);
                f1.ShowDialog();
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd23, e.RowIndex, 4).Length != 0)
            {

                var f1 = new FrmSO2(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd23, e.RowIndex, 4);
                f1.ShowDialog();
            }
        }

        private void Grd24_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 1 }, e.ColIndex) )
            {
                LueRequestEdit(Grd24, LueItCode14, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd24, e.RowIndex);
                SetLueItCode(ref LueItCode14, Sm.GetLue(LueCtCode), TxtSIDocNo.Text);
            }

            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd24, e.RowIndex, 0).Length != 0)
            {

                var f1 = new FrmItem(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mItCode = Sm.GetGrdStr(Grd24, e.RowIndex, 0);
                f1.ShowDialog();
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd24, e.RowIndex, 4).Length != 0)
            {

                var f1 = new FrmSO2(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd24, e.RowIndex, 4);
                f1.ShowDialog();
            }
        }

        private void Grd25_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 1 }, e.ColIndex) )
            {
                LueRequestEdit(Grd25, LueItCode15, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd25, e.RowIndex);
                SetLueItCode(ref LueItCode15, Sm.GetLue(LueCtCode), TxtSIDocNo.Text);
            }

            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd25, e.RowIndex, 0).Length != 0)
            {

                var f1 = new FrmItem(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mItCode = Sm.GetGrdStr(Grd25, e.RowIndex, 0);
                f1.ShowDialog();
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd25, e.RowIndex, 4).Length != 0)
            {

                var f1 = new FrmSO2(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd25, e.RowIndex, 4);
                f1.ShowDialog();
            }
        }

        private void Grd26_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 1 }, e.ColIndex) )
            {
                LueRequestEdit(Grd26, LueItCode16, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd26, e.RowIndex);
                SetLueItCode(ref LueItCode16, Sm.GetLue(LueCtCode), TxtSIDocNo.Text);
            }

            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd26, e.RowIndex, 0).Length != 0)
            {

                var f1 = new FrmItem(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mItCode = Sm.GetGrdStr(Grd26, e.RowIndex, 0);
                f1.ShowDialog();
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd26, e.RowIndex, 4).Length != 0)
            {

                var f1 = new FrmSO2(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd26, e.RowIndex, 4);
                f1.ShowDialog();
            }
        }

        private void Grd27_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 1 }, e.ColIndex) )
            {
                LueRequestEdit(Grd27, LueItCode17, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd27, e.RowIndex);
                SetLueItCode(ref LueItCode17, Sm.GetLue(LueCtCode), TxtSIDocNo.Text);
            }

            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd27, e.RowIndex, 0).Length != 0)
            {

                var f1 = new FrmItem(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mItCode = Sm.GetGrdStr(Grd27, e.RowIndex, 0);
                f1.ShowDialog();
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd27, e.RowIndex, 4).Length != 0)
            {

                var f1 = new FrmSO2(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd27, e.RowIndex, 4);
                f1.ShowDialog();
            }
        }

        private void Grd28_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 1 }, e.ColIndex) )
            {
                LueRequestEdit(Grd28, LueItCode18, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd28, e.RowIndex);
                SetLueItCode(ref LueItCode18, Sm.GetLue(LueCtCode), TxtSIDocNo.Text);
            }

            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd28, e.RowIndex, 0).Length != 0)
            {

                var f1 = new FrmItem(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mItCode = Sm.GetGrdStr(Grd28, e.RowIndex, 0);
                f1.ShowDialog();
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd28, e.RowIndex, 4).Length != 0)
            {

                var f1 = new FrmSO2(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd28, e.RowIndex, 4);
                f1.ShowDialog();
            }
        }

        private void Grd29_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 1 }, e.ColIndex) )
            {
                LueRequestEdit(Grd29, LueItCode19, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd29, e.RowIndex);
                SetLueItCode(ref LueItCode19, Sm.GetLue(LueCtCode), TxtSIDocNo.Text);
            }

            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd29, e.RowIndex, 0).Length != 0)
            {

                var f1 = new FrmItem(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mItCode = Sm.GetGrdStr(Grd29, e.RowIndex, 0);
                f1.ShowDialog();
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd29, e.RowIndex, 4).Length != 0)
            {

                var f1 = new FrmSO2(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd29, e.RowIndex, 4);
                f1.ShowDialog();
            }
        }

        private void Grd30_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 1 }, e.ColIndex) )
            {
                LueRequestEdit(Grd30, LueItCode20, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd30, e.RowIndex);
                SetLueItCode(ref LueItCode20, Sm.GetLue(LueCtCode), TxtSIDocNo.Text);
            }

            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd30, e.RowIndex, 0).Length != 0)
            {

                var f1 = new FrmItem(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mItCode = Sm.GetGrdStr(Grd30, e.RowIndex, 0);
                f1.ShowDialog();
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd30, e.RowIndex, 4).Length != 0)
            {

                var f1 = new FrmSO2(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd30, e.RowIndex, 4);
                f1.ShowDialog();
            }
        }

        private void Grd31_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 1 }, e.ColIndex) )
            {
                LueRequestEdit(Grd31, LueItCode21, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd31, e.RowIndex);
                SetLueItCode(ref LueItCode21, Sm.GetLue(LueCtCode), TxtSIDocNo.Text);
            }

            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd31, e.RowIndex, 0).Length != 0)
            {

                var f1 = new FrmItem(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mItCode = Sm.GetGrdStr(Grd31, e.RowIndex, 0);
                f1.ShowDialog();
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd31, e.RowIndex, 4).Length != 0)
            {

                var f1 = new FrmSO2(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd31, e.RowIndex, 4);
                f1.ShowDialog();
            }
        }

        private void Grd32_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 1 }, e.ColIndex) )
            {
                LueRequestEdit(Grd32, LueItCode22, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd32, e.RowIndex);
                SetLueItCode(ref LueItCode22, Sm.GetLue(LueCtCode), TxtSIDocNo.Text);
            }

            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd32, e.RowIndex, 0).Length != 0)
            {

                var f1 = new FrmItem(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mItCode = Sm.GetGrdStr(Grd32, e.RowIndex, 0);
                f1.ShowDialog();
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd32, e.RowIndex, 4).Length != 0)
            {

                var f1 = new FrmSO2(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd32, e.RowIndex, 4);
                f1.ShowDialog();
            }
        }

        private void Grd33_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 1 }, e.ColIndex) )
            {
                LueRequestEdit(Grd33, LueItCode23, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd33, e.RowIndex);
                SetLueItCode(ref LueItCode23, Sm.GetLue(LueCtCode), TxtSIDocNo.Text);
            }

            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd33, e.RowIndex, 0).Length != 0)
            {

                var f1 = new FrmItem(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mItCode = Sm.GetGrdStr(Grd33, e.RowIndex, 0);
                f1.ShowDialog();
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd33, e.RowIndex, 4).Length != 0)
            {

                var f1 = new FrmSO2(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd33, e.RowIndex, 4);
                f1.ShowDialog();
            }
        }

        private void Grd34_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 1 }, e.ColIndex) )
            {
                LueRequestEdit(Grd34, LueItCode24, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd34, e.RowIndex);
                SetLueItCode(ref LueItCode24, Sm.GetLue(LueCtCode), TxtSIDocNo.Text);
            }

            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd34, e.RowIndex, 0).Length != 0)
            {

                var f1 = new FrmItem(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mItCode = Sm.GetGrdStr(Grd34, e.RowIndex, 0);
                f1.ShowDialog();
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd34, e.RowIndex, 4).Length != 0)
            {

                var f1 = new FrmSO2(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd34, e.RowIndex, 4);
                f1.ShowDialog();
            }
        }

        private void Grd35_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 1 }, e.ColIndex) )
            {
                LueRequestEdit(Grd35, LueItCode25, ref fCell, ref fAccept, e);
                Sm.GrdRequestEdit(Grd35, e.RowIndex);
                SetLueItCode(ref LueItCode25, Sm.GetLue(LueCtCode), TxtSIDocNo.Text);
            }

            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd35, e.RowIndex, 0).Length != 0)
            {

                var f1 = new FrmItem(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mItCode = Sm.GetGrdStr(Grd35, e.RowIndex, 0);
                f1.ShowDialog();
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd35, e.RowIndex, 4).Length != 0)
            {

                var f1 = new FrmSO2(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd35, e.RowIndex, 4);
                f1.ShowDialog();
            }
        }

        #endregion

        #region Grd key Down

        private void Grd11_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                decimal QtyPackaging, pack = 0m;
                decimal QtySales, sale = 0m;
                decimal QtyInventory, inv = 0m;

                if (Grd11.Rows[Grd11.Rows[Grd11.Rows.Count - 1].Index].Selected)
                    MessageBox.Show("You can't remove last row.", Gv.CompanyName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                else
                {
                    for (int Index = Grd11.SelectedRows.Count - 1; Index >= 0; Index--)
                    {
                        for (int Row2 = 0; Row2 < Grd1.Rows.Count - 1; Row2++)
                        {
                            QtyPackaging = Sm.GetGrdDec(Grd11, Grd11.SelectedRows[Index].Index, 10);
                            QtySales = Sm.GetGrdDec(Grd11, Grd11.SelectedRows[Index].Index, 11);
                            QtyInventory = Sm.GetGrdDec(Grd11, Grd11.SelectedRows[Index].Index, 13);
                           
                            if (
                                Sm.GetGrdStr(Grd1, Row2, 1).Length != 0 &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd11, Grd11.SelectedRows[Index].Index, 0) + Sm.GetGrdStr(Grd11, Grd11.SelectedRows[Index].Index, 4) + Sm.GetGrdStr(Grd11, Grd11.SelectedRows[Index].Index, 6),
                                Sm.GetGrdStr(Grd1, Row2, 1) + Sm.GetGrdStr(Grd1, Row2, 5) + Sm.GetGrdStr(Grd1, Row2, 7))
                                )
                            {
                                pack = Sm.GetGrdDec(Grd1, Row2, 10) - QtyPackaging;
                                sale = Sm.GetGrdDec(Grd1, Row2, 13) - QtySales;
                                inv = Sm.GetGrdDec(Grd1, Row2, 16) - QtyInventory;
                                Grd1.Cells[Row2, 10].Value = pack;
                                Grd1.Cells[Row2, 13].Value = sale;
                                Grd1.Cells[Row2, 16].Value = inv;
                                Grd1.Cells[Row2, 11].Value = Sm.GetGrdDec(Grd1, Row2, 9) - Sm.GetGrdDec(Grd1, Row2, 10);
                                Grd1.Cells[Row2, 14].Value = Sm.GetGrdDec(Grd1, Row2, 12) - Sm.GetGrdDec(Grd1, Row2, 13);
                            }
                        }
                        Grd11.Rows.RemoveAt(Grd11.SelectedRows[Index].Index);
                    }
                }
                Sm.GrdKeyDown(Grd11, e, BtnFind, BtnSave);
                ComputeUom2();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void Grd12_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                decimal QtyPackaging, pack = 0m;
                decimal QtySales, sale = 0m;
                decimal QtyInventory, inv = 0m;

                if (Grd12.Rows[Grd12.Rows[Grd12.Rows.Count - 1].Index].Selected)
                    MessageBox.Show("You can't remove last row.", Gv.CompanyName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                else
                {
                    for (int Index = Grd12.SelectedRows.Count - 1; Index >= 0; Index--)
                    {

                        for (int Row2 = 0; Row2 < Grd1.Rows.Count - 1; Row2++)
                        {
                            QtyPackaging = Sm.GetGrdDec(Grd12, Grd12.SelectedRows[Index].Index, 10);
                            QtySales = Sm.GetGrdDec(Grd12, Grd12.SelectedRows[Index].Index, 11);
                            QtyInventory = Sm.GetGrdDec(Grd12, Grd12.SelectedRows[Index].Index, 13);
                            if (
                                Sm.GetGrdStr(Grd1, Row2, 1).Length != 0 &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd12, Grd12.SelectedRows[Index].Index, 0) + Sm.GetGrdStr(Grd12, Grd12.SelectedRows[Index].Index, 4) + Sm.GetGrdStr(Grd12, Grd12.SelectedRows[Index].Index, 6),
                                Sm.GetGrdStr(Grd1, Row2, 1) + Sm.GetGrdStr(Grd1, Row2, 5) + Sm.GetGrdStr(Grd1, Row2, 7))
                                )
                            {
                                pack = Sm.GetGrdDec(Grd1, Row2, 10) - QtyPackaging;
                                sale = Sm.GetGrdDec(Grd1, Row2, 13) - QtySales;
                                inv = Sm.GetGrdDec(Grd1, Row2, 16) - QtyInventory;
                                Grd1.Cells[Row2, 10].Value = pack;
                                Grd1.Cells[Row2, 13].Value = sale;
                                Grd1.Cells[Row2, 16].Value = inv;
                                Grd1.Cells[Row2, 11].Value = Sm.GetGrdDec(Grd1, Row2, 9) - Sm.GetGrdDec(Grd1, Row2, 10);
                                Grd1.Cells[Row2, 14].Value = Sm.GetGrdDec(Grd1, Row2, 12) - Sm.GetGrdDec(Grd1, Row2, 13);
                            }
                        }
                        Grd12.Rows.RemoveAt(Grd12.SelectedRows[Index].Index);
                    }
                }
                Sm.GrdKeyDown(Grd12, e, BtnFind, BtnSave);
                ComputeUom2();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void Grd13_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                decimal QtyPackaging, pack = 0m;
                decimal QtySales, sale = 0m;
                decimal QtyInventory, inv = 0m;

                if (Grd13.Rows[Grd13.Rows[Grd13.Rows.Count - 1].Index].Selected)
                    MessageBox.Show("You can't remove last row.", Gv.CompanyName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                else
                {
                    for (int Index = Grd13.SelectedRows.Count - 1; Index >= 0; Index--)
                    {

                        for (int Row2 = 0; Row2 < Grd1.Rows.Count - 1; Row2++)
                        {
                            QtyPackaging = Sm.GetGrdDec(Grd13, Grd13.SelectedRows[Index].Index, 10);
                            QtySales = Sm.GetGrdDec(Grd13, Grd13.SelectedRows[Index].Index, 11);
                            QtyInventory = Sm.GetGrdDec(Grd13, Grd13.SelectedRows[Index].Index, 13);
                            if (
                                Sm.GetGrdStr(Grd1, Row2, 1).Length != 0 &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd13, Grd13.SelectedRows[Index].Index, 0) + Sm.GetGrdStr(Grd13, Grd13.SelectedRows[Index].Index, 4) + Sm.GetGrdStr(Grd13, Grd13.SelectedRows[Index].Index, 6),
                                Sm.GetGrdStr(Grd1, Row2, 1) + Sm.GetGrdStr(Grd1, Row2, 5) + Sm.GetGrdStr(Grd1, Row2, 7))
                                )
                            {
                                pack = Sm.GetGrdDec(Grd1, Row2, 10) - QtyPackaging;
                                sale = Sm.GetGrdDec(Grd1, Row2, 13) - QtySales;
                                inv = Sm.GetGrdDec(Grd1, Row2, 16) - QtyInventory;
                                Grd1.Cells[Row2, 10].Value = pack;
                                Grd1.Cells[Row2, 13].Value = sale;
                                Grd1.Cells[Row2, 16].Value = inv;
                                Grd1.Cells[Row2, 11].Value = Sm.GetGrdDec(Grd1, Row2, 9) - Sm.GetGrdDec(Grd1, Row2, 10);
                                Grd1.Cells[Row2, 14].Value = Sm.GetGrdDec(Grd1, Row2, 12) - Sm.GetGrdDec(Grd1, Row2, 13);
                            }
                        }
                        Grd13.Rows.RemoveAt(Grd13.SelectedRows[Index].Index);
                    }
                }
                Sm.GrdKeyDown(Grd13, e, BtnFind, BtnSave);
                ComputeUom2();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void Grd14_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                decimal QtyPackaging, pack = 0m;
                decimal QtySales, sale = 0m;
                decimal QtyInventory, inv = 0m;

                if (Grd14.Rows[Grd14.Rows[Grd14.Rows.Count - 1].Index].Selected)
                    MessageBox.Show("You can't remove last row.", Gv.CompanyName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                else
                {
                    for (int Index = Grd14.SelectedRows.Count - 1; Index >= 0; Index--)
                    {

                        for (int Row2 = 0; Row2 < Grd1.Rows.Count - 1; Row2++)
                        {
                            QtyPackaging = Sm.GetGrdDec(Grd14, Grd14.SelectedRows[Index].Index, 10);
                            QtySales = Sm.GetGrdDec(Grd14, Grd14.SelectedRows[Index].Index, 11);
                            QtyInventory = Sm.GetGrdDec(Grd14, Grd14.SelectedRows[Index].Index, 13);
                            if (
                                Sm.GetGrdStr(Grd1, Row2, 1).Length != 0 &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd14, Grd14.SelectedRows[Index].Index, 0) + Sm.GetGrdStr(Grd14, Grd14.SelectedRows[Index].Index, 4) + Sm.GetGrdStr(Grd14, Grd14.SelectedRows[Index].Index, 6),
                                Sm.GetGrdStr(Grd1, Row2, 1) + Sm.GetGrdStr(Grd1, Row2, 5) + Sm.GetGrdStr(Grd1, Row2, 7))
                                )
                            {
                                pack = Sm.GetGrdDec(Grd1, Row2, 10) - QtyPackaging;
                                sale = Sm.GetGrdDec(Grd1, Row2, 13) - QtySales;
                                inv = Sm.GetGrdDec(Grd1, Row2, 16) - QtyInventory;
                                Grd1.Cells[Row2, 10].Value = pack;
                                Grd1.Cells[Row2, 13].Value = sale;
                                Grd1.Cells[Row2, 16].Value = inv;
                                Grd1.Cells[Row2, 11].Value = Sm.GetGrdDec(Grd1, Row2, 9) - Sm.GetGrdDec(Grd1, Row2, 10);
                                Grd1.Cells[Row2, 14].Value = Sm.GetGrdDec(Grd1, Row2, 12) - Sm.GetGrdDec(Grd1, Row2, 13);
                            }
                        }
                        Grd14.Rows.RemoveAt(Grd14.SelectedRows[Index].Index);
                    }
                }
                Sm.GrdKeyDown(Grd14, e, BtnFind, BtnSave);
                ComputeUom2();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void Grd15_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                decimal QtyPackaging, pack = 0m;
                decimal QtySales, sale = 0m;
                decimal QtyInventory, inv = 0m;

                if (Grd15.Rows[Grd15.Rows[Grd15.Rows.Count - 1].Index].Selected)
                    MessageBox.Show("You can't remove last row.", Gv.CompanyName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                else
                {
                    for (int Index = Grd15.SelectedRows.Count - 1; Index >= 0; Index--)
                    {

                        for (int Row2 = 0; Row2 < Grd1.Rows.Count - 1; Row2++)
                        {
                            QtyPackaging = Sm.GetGrdDec(Grd15, Grd15.SelectedRows[Index].Index, 10);
                            QtySales = Sm.GetGrdDec(Grd15, Grd15.SelectedRows[Index].Index, 11);
                            QtyInventory = Sm.GetGrdDec(Grd15, Grd15.SelectedRows[Index].Index, 13);
                            if (
                                Sm.GetGrdStr(Grd1, Row2, 1).Length != 0 &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd15, Grd15.SelectedRows[Index].Index, 0) + Sm.GetGrdStr(Grd15, Grd15.SelectedRows[Index].Index, 4) + Sm.GetGrdStr(Grd15, Grd15.SelectedRows[Index].Index, 6),
                                Sm.GetGrdStr(Grd1, Row2, 1) + Sm.GetGrdStr(Grd1, Row2, 5) + Sm.GetGrdStr(Grd1, Row2, 7))
                                )
                            {
                                pack = Sm.GetGrdDec(Grd1, Row2, 10) - QtyPackaging;
                                sale = Sm.GetGrdDec(Grd1, Row2, 13) - QtySales;
                                inv = Sm.GetGrdDec(Grd1, Row2, 16) - QtyInventory;
                                Grd1.Cells[Row2, 10].Value = pack;
                                Grd1.Cells[Row2, 13].Value = sale;
                                Grd1.Cells[Row2, 16].Value = inv;
                                Grd1.Cells[Row2, 11].Value = Sm.GetGrdDec(Grd1, Row2, 9) - Sm.GetGrdDec(Grd1, Row2, 10);
                                Grd1.Cells[Row2, 14].Value = Sm.GetGrdDec(Grd1, Row2, 12) - Sm.GetGrdDec(Grd1, Row2, 13);
                            }
                        }
                        Grd15.Rows.RemoveAt(Grd15.SelectedRows[Index].Index);
                    }
                }
                Sm.GrdKeyDown(Grd15, e, BtnFind, BtnSave);
                ComputeUom2();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void Grd16_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                decimal QtyPackaging, pack = 0m;
                decimal QtySales, sale = 0m;
                decimal QtyInventory, inv = 0m;

                if (Grd16.Rows[Grd16.Rows[Grd16.Rows.Count - 1].Index].Selected)
                    MessageBox.Show("You can't remove last row.", Gv.CompanyName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                else
                {
                    for (int Index = Grd16.SelectedRows.Count - 1; Index >= 0; Index--)
                    {

                        for (int Row2 = 0; Row2 < Grd1.Rows.Count - 1; Row2++)
                        {
                            QtyPackaging = Sm.GetGrdDec(Grd16, Grd16.SelectedRows[Index].Index, 10);
                            QtySales = Sm.GetGrdDec(Grd16, Grd16.SelectedRows[Index].Index, 11);
                            QtyInventory = Sm.GetGrdDec(Grd16, Grd16.SelectedRows[Index].Index, 13);
                            if (
                                Sm.GetGrdStr(Grd1, Row2, 1).Length != 0 &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd16, Grd16.SelectedRows[Index].Index, 0) + Sm.GetGrdStr(Grd16, Grd16.SelectedRows[Index].Index, 4) + Sm.GetGrdStr(Grd16, Grd16.SelectedRows[Index].Index, 6),
                                Sm.GetGrdStr(Grd1, Row2, 1) + Sm.GetGrdStr(Grd1, Row2, 5) + Sm.GetGrdStr(Grd1, Row2, 7))
                                )
                            {
                                pack = Sm.GetGrdDec(Grd1, Row2, 10) - QtyPackaging;
                                sale = Sm.GetGrdDec(Grd1, Row2, 13) - QtySales;
                                inv = Sm.GetGrdDec(Grd1, Row2, 16) - QtyInventory;
                                Grd1.Cells[Row2, 10].Value = pack;
                                Grd1.Cells[Row2, 13].Value = sale;
                                Grd1.Cells[Row2, 16].Value = inv;
                                Grd1.Cells[Row2, 11].Value = Sm.GetGrdDec(Grd1, Row2, 9) - Sm.GetGrdDec(Grd1, Row2, 10);
                                Grd1.Cells[Row2, 14].Value = Sm.GetGrdDec(Grd1, Row2, 12) - Sm.GetGrdDec(Grd1, Row2, 13);
                            }
                        }
                        Grd16.Rows.RemoveAt(Grd16.SelectedRows[Index].Index);
                    }
                }
                Sm.GrdKeyDown(Grd16, e, BtnFind, BtnSave);
                ComputeUom2();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void Grd17_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                decimal QtyPackaging, pack = 0m;
                decimal QtySales, sale = 0m;
                decimal QtyInventory, inv = 0m;

                if (Grd17.Rows[Grd17.Rows[Grd17.Rows.Count - 1].Index].Selected)
                    MessageBox.Show("You can't remove last row.", Gv.CompanyName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                else
                {
                    for (int Index = Grd17.SelectedRows.Count - 1; Index >= 0; Index--)
                    {

                        for (int Row2 = 0; Row2 < Grd1.Rows.Count - 1; Row2++)
                        {
                            QtyPackaging = Sm.GetGrdDec(Grd17, Grd17.SelectedRows[Index].Index, 10);
                            QtySales = Sm.GetGrdDec(Grd17, Grd17.SelectedRows[Index].Index, 11);
                            QtyInventory = Sm.GetGrdDec(Grd17, Grd17.SelectedRows[Index].Index, 13);
                            if (
                                Sm.GetGrdStr(Grd1, Row2, 1).Length != 0 &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd17, Grd17.SelectedRows[Index].Index, 0) + Sm.GetGrdStr(Grd17, Grd17.SelectedRows[Index].Index, 4) + Sm.GetGrdStr(Grd17, Grd17.SelectedRows[Index].Index, 6),
                                Sm.GetGrdStr(Grd1, Row2, 1) + Sm.GetGrdStr(Grd1, Row2, 5) + Sm.GetGrdStr(Grd1, Row2, 7))
                                )
                            {
                                pack = Sm.GetGrdDec(Grd1, Row2, 10) - QtyPackaging;
                                sale = Sm.GetGrdDec(Grd1, Row2, 13) - QtySales;
                                inv = Sm.GetGrdDec(Grd1, Row2, 16) - QtyInventory;
                                Grd1.Cells[Row2, 10].Value = pack;
                                Grd1.Cells[Row2, 13].Value = sale;
                                Grd1.Cells[Row2, 16].Value = inv;
                                Grd1.Cells[Row2, 11].Value = Sm.GetGrdDec(Grd1, Row2, 9) - Sm.GetGrdDec(Grd1, Row2, 10);
                                Grd1.Cells[Row2, 14].Value = Sm.GetGrdDec(Grd1, Row2, 12) - Sm.GetGrdDec(Grd1, Row2, 13);
                            }
                        }
                        Grd17.Rows.RemoveAt(Grd17.SelectedRows[Index].Index);
                    }
                }
                Sm.GrdKeyDown(Grd17, e, BtnFind, BtnSave);
                ComputeUom2();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void Grd18_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                decimal QtyPackaging, pack = 0m;
                decimal QtySales, sale = 0m;
                decimal QtyInventory, inv = 0m;

                if (Grd18.Rows[Grd18.Rows[Grd18.Rows.Count - 1].Index].Selected)
                    MessageBox.Show("You can't remove last row.", Gv.CompanyName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                else
                {
                    for (int Index = Grd18.SelectedRows.Count - 1; Index >= 0; Index--)
                    {

                        for (int Row2 = 0; Row2 < Grd1.Rows.Count - 1; Row2++)
                        {
                            QtyPackaging = Sm.GetGrdDec(Grd18, Grd18.SelectedRows[Index].Index, 10);
                            QtySales = Sm.GetGrdDec(Grd18, Grd18.SelectedRows[Index].Index, 11);
                            QtyInventory = Sm.GetGrdDec(Grd18, Grd18.SelectedRows[Index].Index, 13);
                            if (
                                Sm.GetGrdStr(Grd1, Row2, 1).Length != 0 &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd18, Grd18.SelectedRows[Index].Index, 0) + Sm.GetGrdStr(Grd18, Grd18.SelectedRows[Index].Index, 4) + Sm.GetGrdStr(Grd18, Grd18.SelectedRows[Index].Index, 6),
                                Sm.GetGrdStr(Grd1, Row2, 1) + Sm.GetGrdStr(Grd1, Row2, 5) + Sm.GetGrdStr(Grd1, Row2, 7))
                                )
                            {
                                pack = Sm.GetGrdDec(Grd1, Row2, 10) - QtyPackaging;
                                sale = Sm.GetGrdDec(Grd1, Row2, 13) - QtySales;
                                inv = Sm.GetGrdDec(Grd1, Row2, 16) - QtyInventory;
                                Grd1.Cells[Row2, 10].Value = pack;
                                Grd1.Cells[Row2, 13].Value = sale;
                                Grd1.Cells[Row2, 16].Value = inv;
                                Grd1.Cells[Row2, 11].Value = Sm.GetGrdDec(Grd1, Row2, 9) - Sm.GetGrdDec(Grd1, Row2, 10);
                                Grd1.Cells[Row2, 14].Value = Sm.GetGrdDec(Grd1, Row2, 12) - Sm.GetGrdDec(Grd1, Row2, 13);
                            }
                        }
                        Grd18.Rows.RemoveAt(Grd18.SelectedRows[Index].Index);
                    }
                }
                Sm.GrdKeyDown(Grd18, e, BtnFind, BtnSave);
                ComputeUom2();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void Grd19_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                decimal QtyPackaging, pack = 0m;
                decimal QtySales, sale = 0m;
                decimal QtyInventory, inv = 0m;

                if (Grd19.Rows[Grd19.Rows[Grd19.Rows.Count - 1].Index].Selected)
                    MessageBox.Show("You can't remove last row.", Gv.CompanyName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                else
                {
                    for (int Index = Grd19.SelectedRows.Count - 1; Index >= 0; Index--)
                    {

                        for (int Row2 = 0; Row2 < Grd1.Rows.Count - 1; Row2++)
                        {
                            QtyPackaging = Sm.GetGrdDec(Grd19, Grd19.SelectedRows[Index].Index, 10);
                            QtySales = Sm.GetGrdDec(Grd19, Grd19.SelectedRows[Index].Index, 11);
                            QtyInventory = Sm.GetGrdDec(Grd19, Grd19.SelectedRows[Index].Index, 13);
                            if (
                                Sm.GetGrdStr(Grd1, Row2, 1).Length != 0 &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd19, Grd19.SelectedRows[Index].Index, 0) + Sm.GetGrdStr(Grd19, Grd19.SelectedRows[Index].Index, 4) + Sm.GetGrdStr(Grd19, Grd19.SelectedRows[Index].Index, 6),
                                Sm.GetGrdStr(Grd1, Row2, 1) + Sm.GetGrdStr(Grd1, Row2, 5) + Sm.GetGrdStr(Grd1, Row2, 7))
                                )
                            {
                                pack = Sm.GetGrdDec(Grd1, Row2, 10) - QtyPackaging;
                                sale = Sm.GetGrdDec(Grd1, Row2, 13) - QtySales;
                                inv = Sm.GetGrdDec(Grd1, Row2, 16) - QtyInventory;
                                Grd1.Cells[Row2, 10].Value = pack;
                                Grd1.Cells[Row2, 13].Value = sale;
                                Grd1.Cells[Row2, 16].Value = inv;
                                Grd1.Cells[Row2, 11].Value = Sm.GetGrdDec(Grd1, Row2, 9) - Sm.GetGrdDec(Grd1, Row2, 10);
                                Grd1.Cells[Row2, 14].Value = Sm.GetGrdDec(Grd1, Row2, 12) - Sm.GetGrdDec(Grd1, Row2, 13);
                            }
                        }
                        Grd19.Rows.RemoveAt(Grd19.SelectedRows[Index].Index);
                    }
                }
                Sm.GrdKeyDown(Grd19, e, BtnFind, BtnSave);
                ComputeUom2();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void Grd20_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                decimal QtyPackaging, pack = 0m;
                decimal QtySales, sale = 0m;
                decimal QtyInventory, inv = 0m;

                if (Grd20.Rows[Grd20.Rows[Grd20.Rows.Count - 1].Index].Selected)
                    MessageBox.Show("You can't remove last row.", Gv.CompanyName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                else
                {
                    for (int Index = Grd20.SelectedRows.Count - 1; Index >= 0; Index--)
                    {

                        for (int Row2 = 0; Row2 < Grd1.Rows.Count - 1; Row2++)
                        {
                            QtyPackaging = Sm.GetGrdDec(Grd20, Grd20.SelectedRows[Index].Index, 10);
                            QtySales = Sm.GetGrdDec(Grd20, Grd20.SelectedRows[Index].Index, 11);
                            QtyInventory = Sm.GetGrdDec(Grd20, Grd20.SelectedRows[Index].Index, 13);
                            if (
                                Sm.GetGrdStr(Grd1, Row2, 1).Length != 0 &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd20, Grd20.SelectedRows[Index].Index, 0) + Sm.GetGrdStr(Grd20, Grd20.SelectedRows[Index].Index, 4) + Sm.GetGrdStr(Grd20, Grd20.SelectedRows[Index].Index, 6),
                                Sm.GetGrdStr(Grd1, Row2, 1) + Sm.GetGrdStr(Grd1, Row2, 5) + Sm.GetGrdStr(Grd1, Row2, 7))
                                )
                            {
                                pack = Sm.GetGrdDec(Grd1, Row2, 10) - QtyPackaging;
                                sale = Sm.GetGrdDec(Grd1, Row2, 13) - QtySales;
                                inv = Sm.GetGrdDec(Grd1, Row2, 16) - QtyInventory;
                                Grd1.Cells[Row2, 10].Value = pack;
                                Grd1.Cells[Row2, 13].Value = sale;
                                Grd1.Cells[Row2, 16].Value = inv;
                                Grd1.Cells[Row2, 11].Value = Sm.GetGrdDec(Grd1, Row2, 9) - Sm.GetGrdDec(Grd1, Row2, 10);
                                Grd1.Cells[Row2, 14].Value = Sm.GetGrdDec(Grd1, Row2, 12) - Sm.GetGrdDec(Grd1, Row2, 13);
                            }
                        }
                        Grd20.Rows.RemoveAt(Grd20.SelectedRows[Index].Index);
                    }
                }
                Sm.GrdKeyDown(Grd20, e, BtnFind, BtnSave);
                ComputeUom2();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void Grd21_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                decimal QtyPackaging, pack = 0m;
                decimal QtySales, sale = 0m;
                decimal QtyInventory, inv = 0m;

                if (Grd21.Rows[Grd21.Rows[Grd21.Rows.Count - 1].Index].Selected)
                    MessageBox.Show("You can't remove last row.", Gv.CompanyName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                else
                {
                    for (int Index = Grd21.SelectedRows.Count - 1; Index >= 0; Index--)
                    {

                        for (int Row2 = 0; Row2 < Grd1.Rows.Count - 1; Row2++)
                        {
                            QtyPackaging = Sm.GetGrdDec(Grd21, Grd21.SelectedRows[Index].Index, 10);
                            QtySales = Sm.GetGrdDec(Grd21, Grd21.SelectedRows[Index].Index, 11);
                            QtyInventory = Sm.GetGrdDec(Grd21, Grd21.SelectedRows[Index].Index, 13);
                            if (
                                Sm.GetGrdStr(Grd1, Row2, 1).Length != 0 &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd21, Grd21.SelectedRows[Index].Index, 0) + Sm.GetGrdStr(Grd21, Grd21.SelectedRows[Index].Index, 4) + Sm.GetGrdStr(Grd21, Grd21.SelectedRows[Index].Index, 6),
                                Sm.GetGrdStr(Grd1, Row2, 1) + Sm.GetGrdStr(Grd1, Row2, 5) + Sm.GetGrdStr(Grd1, Row2, 7))
                                )
                            {
                                pack = Sm.GetGrdDec(Grd1, Row2, 10) - QtyPackaging;
                                sale = Sm.GetGrdDec(Grd1, Row2, 13) - QtySales;
                                inv = Sm.GetGrdDec(Grd1, Row2, 16) - QtyInventory;
                                Grd1.Cells[Row2, 10].Value = pack;
                                Grd1.Cells[Row2, 13].Value = sale;
                                Grd1.Cells[Row2, 16].Value = inv;
                                Grd1.Cells[Row2, 11].Value = Sm.GetGrdDec(Grd1, Row2, 9) - Sm.GetGrdDec(Grd1, Row2, 10);
                                Grd1.Cells[Row2, 14].Value = Sm.GetGrdDec(Grd1, Row2, 12) - Sm.GetGrdDec(Grd1, Row2, 13);
                            }
                        }
                        Grd21.Rows.RemoveAt(Grd21.SelectedRows[Index].Index);
                    }
                }
                Sm.GrdKeyDown(Grd21, e, BtnFind, BtnSave);
                ComputeUom2();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void Grd22_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                decimal QtyPackaging, pack = 0m;
                decimal QtySales, sale = 0m;
                decimal QtyInventory, inv = 0m;

                if (Grd22.Rows[Grd22.Rows[Grd22.Rows.Count - 1].Index].Selected)
                    MessageBox.Show("You can't remove last row.", Gv.CompanyName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                else
                {
                    for (int Index = Grd22.SelectedRows.Count - 1; Index >= 0; Index--)
                    {

                        for (int Row2 = 0; Row2 < Grd1.Rows.Count - 1; Row2++)
                        {
                            QtyPackaging = Sm.GetGrdDec(Grd22, Grd22.SelectedRows[Index].Index, 10);
                            QtySales = Sm.GetGrdDec(Grd22, Grd22.SelectedRows[Index].Index, 11);
                            QtyInventory = Sm.GetGrdDec(Grd22, Grd22.SelectedRows[Index].Index, 13);
                            if (
                                Sm.GetGrdStr(Grd1, Row2, 1).Length != 0 &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd22, Grd22.SelectedRows[Index].Index, 0) + Sm.GetGrdStr(Grd22, Grd22.SelectedRows[Index].Index, 4) + Sm.GetGrdStr(Grd22, Grd22.SelectedRows[Index].Index, 6),
                                Sm.GetGrdStr(Grd1, Row2, 1) + Sm.GetGrdStr(Grd1, Row2, 5) + Sm.GetGrdStr(Grd1, Row2, 7))
                                )
                            {
                                pack = Sm.GetGrdDec(Grd1, Row2, 10) - QtyPackaging;
                                sale = Sm.GetGrdDec(Grd1, Row2, 13) - QtySales;
                                inv = Sm.GetGrdDec(Grd1, Row2, 16) - QtyInventory;
                                Grd1.Cells[Row2, 10].Value = pack;
                                Grd1.Cells[Row2, 13].Value = sale;
                                Grd1.Cells[Row2, 16].Value = inv;
                                Grd1.Cells[Row2, 11].Value = Sm.GetGrdDec(Grd1, Row2, 9) - Sm.GetGrdDec(Grd1, Row2, 10);
                                Grd1.Cells[Row2, 14].Value = Sm.GetGrdDec(Grd1, Row2, 12) - Sm.GetGrdDec(Grd1, Row2, 13);
                            }
                        }
                        Grd22.Rows.RemoveAt(Grd22.SelectedRows[Index].Index);
                    }
                }
                Sm.GrdKeyDown(Grd22, e, BtnFind, BtnSave);
                ComputeUom2();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void Grd23_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                decimal QtyPackaging, pack = 0m;
                decimal QtySales, sale = 0m;
                decimal QtyInventory, inv = 0m;

                if (Grd23.Rows[Grd23.Rows[Grd23.Rows.Count - 1].Index].Selected)
                    MessageBox.Show("You can't remove last row.", Gv.CompanyName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                else
                {
                    for (int Index = Grd23.SelectedRows.Count - 1; Index >= 0; Index--)
                    {

                        for (int Row2 = 0; Row2 < Grd1.Rows.Count - 1; Row2++)
                        {
                            QtyPackaging = Sm.GetGrdDec(Grd23, Grd23.SelectedRows[Index].Index, 10);
                            QtySales = Sm.GetGrdDec(Grd23, Grd23.SelectedRows[Index].Index, 11);
                            QtyInventory = Sm.GetGrdDec(Grd23, Grd23.SelectedRows[Index].Index, 13);
                            if (
                                Sm.GetGrdStr(Grd1, Row2, 1).Length != 0 &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd23, Grd23.SelectedRows[Index].Index, 0) + Sm.GetGrdStr(Grd23, Grd23.SelectedRows[Index].Index, 4) + Sm.GetGrdStr(Grd23, Grd23.SelectedRows[Index].Index, 6),
                                Sm.GetGrdStr(Grd1, Row2, 1) + Sm.GetGrdStr(Grd1, Row2, 5) + Sm.GetGrdStr(Grd1, Row2, 7))
                                )
                            {
                                pack = Sm.GetGrdDec(Grd1, Row2, 10) - QtyPackaging;
                                sale = Sm.GetGrdDec(Grd1, Row2, 13) - QtySales;
                                inv = Sm.GetGrdDec(Grd1, Row2, 16) - QtyInventory;
                                Grd1.Cells[Row2, 10].Value = pack;
                                Grd1.Cells[Row2, 13].Value = sale;
                                Grd1.Cells[Row2, 16].Value = inv;
                                Grd1.Cells[Row2, 11].Value = Sm.GetGrdDec(Grd1, Row2, 9) - Sm.GetGrdDec(Grd1, Row2, 10);
                                Grd1.Cells[Row2, 14].Value = Sm.GetGrdDec(Grd1, Row2, 12) - Sm.GetGrdDec(Grd1, Row2, 13);
                            }
                        }
                        Grd23.Rows.RemoveAt(Grd23.SelectedRows[Index].Index);
                    }
                }
                Sm.GrdKeyDown(Grd23, e, BtnFind, BtnSave);
                ComputeUom2();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void Grd24_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                decimal QtyPackaging, pack = 0m;
                decimal QtySales, sale = 0m;
                decimal QtyInventory, inv = 0m;

                if (Grd24.Rows[Grd24.Rows[Grd24.Rows.Count - 1].Index].Selected)
                    MessageBox.Show("You can't remove last row.", Gv.CompanyName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                else
                {
                    for (int Index = Grd24.SelectedRows.Count - 1; Index >= 0; Index--)
                    {

                        for (int Row2 = 0; Row2 < Grd1.Rows.Count - 1; Row2++)
                        {
                            QtyPackaging = Sm.GetGrdDec(Grd24, Grd24.SelectedRows[Index].Index, 10);
                            QtySales = Sm.GetGrdDec(Grd24, Grd24.SelectedRows[Index].Index, 11);
                            QtyInventory = Sm.GetGrdDec(Grd24, Grd24.SelectedRows[Index].Index, 13);
                            if (
                                Sm.GetGrdStr(Grd1, Row2, 1).Length != 0 &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd24, Grd24.SelectedRows[Index].Index, 0) + Sm.GetGrdStr(Grd24, Grd24.SelectedRows[Index].Index, 4) + Sm.GetGrdStr(Grd24, Grd24.SelectedRows[Index].Index, 6),
                                Sm.GetGrdStr(Grd1, Row2, 1) + Sm.GetGrdStr(Grd1, Row2, 5) + Sm.GetGrdStr(Grd1, Row2, 7))
                                )
                            {
                                pack = Sm.GetGrdDec(Grd1, Row2, 10) - QtyPackaging;
                                sale = Sm.GetGrdDec(Grd1, Row2, 13) - QtySales;
                                inv = Sm.GetGrdDec(Grd1, Row2, 16) - QtyInventory;
                                Grd1.Cells[Row2, 10].Value = pack;
                                Grd1.Cells[Row2, 13].Value = sale;
                                Grd1.Cells[Row2, 16].Value = inv;
                                Grd1.Cells[Row2, 11].Value = Sm.GetGrdDec(Grd1, Row2, 9) - Sm.GetGrdDec(Grd1, Row2, 10);
                                Grd1.Cells[Row2, 14].Value = Sm.GetGrdDec(Grd1, Row2, 12) - Sm.GetGrdDec(Grd1, Row2, 13);
                            }
                        }
                        Grd24.Rows.RemoveAt(Grd24.SelectedRows[Index].Index);
                    }
                }
                Sm.GrdKeyDown(Grd24, e, BtnFind, BtnSave);
                ComputeUom2();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void Grd25_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                decimal QtyPackaging, pack = 0m;
                decimal QtySales, sale = 0m;
                decimal QtyInventory, inv = 0m;

                if (Grd25.Rows[Grd25.Rows[Grd25.Rows.Count - 1].Index].Selected)
                    MessageBox.Show("You can't remove last row.", Gv.CompanyName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                else
                {
                    for (int Index = Grd25.SelectedRows.Count - 1; Index >= 0; Index--)
                    {

                        for (int Row2 = 0; Row2 < Grd1.Rows.Count - 1; Row2++)
                        {
                            QtyPackaging = Sm.GetGrdDec(Grd25, Grd25.SelectedRows[Index].Index, 10);
                            QtySales = Sm.GetGrdDec(Grd25, Grd25.SelectedRows[Index].Index, 11);
                            QtyInventory = Sm.GetGrdDec(Grd25, Grd25.SelectedRows[Index].Index, 13);
                            if (
                                Sm.GetGrdStr(Grd1, Row2, 1).Length != 0 &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd25, Grd25.SelectedRows[Index].Index, 0) + Sm.GetGrdStr(Grd25, Grd25.SelectedRows[Index].Index, 4) + Sm.GetGrdStr(Grd25, Grd25.SelectedRows[Index].Index, 6),
                                Sm.GetGrdStr(Grd1, Row2, 1) + Sm.GetGrdStr(Grd1, Row2, 5) + Sm.GetGrdStr(Grd1, Row2, 7))
                                )
                            {
                                pack = Sm.GetGrdDec(Grd1, Row2, 10) - QtyPackaging;
                                sale = Sm.GetGrdDec(Grd1, Row2, 13) - QtySales;
                                inv = Sm.GetGrdDec(Grd1, Row2, 16) - QtyInventory;
                                Grd1.Cells[Row2, 10].Value = pack;
                                Grd1.Cells[Row2, 13].Value = sale;
                                Grd1.Cells[Row2, 16].Value = inv;
                                Grd1.Cells[Row2, 11].Value = Sm.GetGrdDec(Grd1, Row2, 9) - Sm.GetGrdDec(Grd1, Row2, 10);
                                Grd1.Cells[Row2, 14].Value = Sm.GetGrdDec(Grd1, Row2, 12) - Sm.GetGrdDec(Grd1, Row2, 13);
                            }
                        }
                        Grd25.Rows.RemoveAt(Grd25.SelectedRows[Index].Index);
                    }
                }
                Sm.GrdKeyDown(Grd25, e, BtnFind, BtnSave);
                ComputeUom2();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void Grd26_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                decimal QtyPackaging, pack = 0m;
                decimal QtySales, sale = 0m;
                decimal QtyInventory, inv = 0m;

                if (Grd26.Rows[Grd26.Rows[Grd26.Rows.Count - 1].Index].Selected)
                    MessageBox.Show("You can't remove last row.", Gv.CompanyName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                else
                {
                    for (int Index = Grd26.SelectedRows.Count - 1; Index >= 0; Index--)
                    {

                        for (int Row2 = 0; Row2 < Grd1.Rows.Count - 1; Row2++)
                        {
                            QtyPackaging = Sm.GetGrdDec(Grd26, Grd26.SelectedRows[Index].Index, 10);
                            QtySales = Sm.GetGrdDec(Grd26, Grd26.SelectedRows[Index].Index, 11);
                            QtyInventory = Sm.GetGrdDec(Grd26, Grd26.SelectedRows[Index].Index, 13);
                            if (
                                Sm.GetGrdStr(Grd1, Row2, 1).Length != 0 &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd26, Grd26.SelectedRows[Index].Index, 0) + Sm.GetGrdStr(Grd26, Grd26.SelectedRows[Index].Index, 4) + Sm.GetGrdStr(Grd26, Grd26.SelectedRows[Index].Index, 6),
                                Sm.GetGrdStr(Grd1, Row2, 1) + Sm.GetGrdStr(Grd1, Row2, 5) + Sm.GetGrdStr(Grd1, Row2, 7))
                                )
                            {
                                pack = Sm.GetGrdDec(Grd1, Row2, 10) - QtyPackaging;
                                sale = Sm.GetGrdDec(Grd1, Row2, 13) - QtySales;
                                inv = Sm.GetGrdDec(Grd1, Row2, 16) - QtyInventory;
                                Grd1.Cells[Row2, 10].Value = pack;
                                Grd1.Cells[Row2, 13].Value = sale;
                                Grd1.Cells[Row2, 16].Value = inv;
                                Grd1.Cells[Row2, 11].Value = Sm.GetGrdDec(Grd1, Row2, 9) - Sm.GetGrdDec(Grd1, Row2, 10);
                                Grd1.Cells[Row2, 14].Value = Sm.GetGrdDec(Grd1, Row2, 12) - Sm.GetGrdDec(Grd1, Row2, 13);
                            }
                        }
                        Grd26.Rows.RemoveAt(Grd26.SelectedRows[Index].Index);
                    }
                }
                Sm.GrdKeyDown(Grd26, e, BtnFind, BtnSave);
                ComputeUom2();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void Grd27_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                decimal QtyPackaging, pack = 0m;
                decimal QtySales, sale = 0m;
                decimal QtyInventory, inv = 0m;

                if (Grd27.Rows[Grd27.Rows[Grd27.Rows.Count - 1].Index].Selected)
                    MessageBox.Show("You can't remove last row.", Gv.CompanyName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                else
                {
                    for (int Index = Grd27.SelectedRows.Count - 1; Index >= 0; Index--)
                    {

                        for (int Row2 = 0; Row2 < Grd1.Rows.Count - 1; Row2++)
                        {
                            QtyPackaging = Sm.GetGrdDec(Grd27, Grd27.SelectedRows[Index].Index, 10);
                            QtySales = Sm.GetGrdDec(Grd27, Grd27.SelectedRows[Index].Index, 11);
                            QtyInventory = Sm.GetGrdDec(Grd27, Grd27.SelectedRows[Index].Index, 13);
                            if (
                                Sm.GetGrdStr(Grd1, Row2, 1).Length != 0 &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd27, Grd27.SelectedRows[Index].Index, 0) + Sm.GetGrdStr(Grd27, Grd27.SelectedRows[Index].Index, 4) + Sm.GetGrdStr(Grd27, Grd27.SelectedRows[Index].Index, 6),
                                Sm.GetGrdStr(Grd1, Row2, 1) + Sm.GetGrdStr(Grd1, Row2, 5) + Sm.GetGrdStr(Grd1, Row2, 7))
                                )
                            {
                                pack = Sm.GetGrdDec(Grd1, Row2, 10) - QtyPackaging;
                                sale = Sm.GetGrdDec(Grd1, Row2, 13) - QtySales;
                                inv = Sm.GetGrdDec(Grd1, Row2, 16) - QtyInventory;
                                Grd1.Cells[Row2, 10].Value = pack;
                                Grd1.Cells[Row2, 13].Value = sale;
                                Grd1.Cells[Row2, 16].Value = inv;
                                Grd1.Cells[Row2, 11].Value = Sm.GetGrdDec(Grd1, Row2, 9) - Sm.GetGrdDec(Grd1, Row2, 10);
                                Grd1.Cells[Row2, 14].Value = Sm.GetGrdDec(Grd1, Row2, 12) - Sm.GetGrdDec(Grd1, Row2, 13);
                            }
                        }
                        Grd27.Rows.RemoveAt(Grd27.SelectedRows[Index].Index);
                    }
                }
                Sm.GrdKeyDown(Grd27, e, BtnFind, BtnSave);
                ComputeUom2();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void Grd28_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                decimal QtyPackaging, pack = 0m;
                decimal QtySales, sale = 0m;
                decimal QtyInventory, inv = 0m;

                if (Grd28.Rows[Grd28.Rows[Grd28.Rows.Count - 1].Index].Selected)
                    MessageBox.Show("You can't remove last row.", Gv.CompanyName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                else
                {
                    for (int Index = Grd28.SelectedRows.Count - 1; Index >= 0; Index--)
                    {

                        for (int Row2 = 0; Row2 < Grd1.Rows.Count - 1; Row2++)
                        {
                            QtyPackaging = Sm.GetGrdDec(Grd28, Grd28.SelectedRows[Index].Index, 10);
                            QtySales = Sm.GetGrdDec(Grd28, Grd28.SelectedRows[Index].Index, 11);
                            QtyInventory = Sm.GetGrdDec(Grd28, Grd28.SelectedRows[Index].Index, 13);
                            if (
                                Sm.GetGrdStr(Grd1, Row2, 1).Length != 0 &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd28, Grd28.SelectedRows[Index].Index, 0) + Sm.GetGrdStr(Grd28, Grd28.SelectedRows[Index].Index, 4) + Sm.GetGrdStr(Grd28, Grd28.SelectedRows[Index].Index, 6),
                                Sm.GetGrdStr(Grd1, Row2, 1) + Sm.GetGrdStr(Grd1, Row2, 5) + Sm.GetGrdStr(Grd1, Row2, 7))
                                )
                            {
                                pack = Sm.GetGrdDec(Grd1, Row2, 10) - QtyPackaging;
                                sale = Sm.GetGrdDec(Grd1, Row2, 13) - QtySales;
                                inv = Sm.GetGrdDec(Grd1, Row2, 16) - QtyInventory;
                                Grd1.Cells[Row2, 10].Value = pack;
                                Grd1.Cells[Row2, 13].Value = sale;
                                Grd1.Cells[Row2, 16].Value = inv;
                                Grd1.Cells[Row2, 11].Value = Sm.GetGrdDec(Grd1, Row2, 9) - Sm.GetGrdDec(Grd1, Row2, 10);
                                Grd1.Cells[Row2, 14].Value = Sm.GetGrdDec(Grd1, Row2, 12) - Sm.GetGrdDec(Grd1, Row2, 13);
                            }
                        }
                        Grd28.Rows.RemoveAt(Grd28.SelectedRows[Index].Index);
                    }
                }
                Sm.GrdKeyDown(Grd28, e, BtnFind, BtnSave);
                ComputeUom2();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void Grd29_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                decimal QtyPackaging, pack = 0m;
                decimal QtySales, sale = 0m;
                decimal QtyInventory, inv = 0m;

                if (Grd29.Rows[Grd29.Rows[Grd29.Rows.Count - 1].Index].Selected)
                    MessageBox.Show("You can't remove last row.", Gv.CompanyName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                else
                {
                    for (int Index = Grd29.SelectedRows.Count - 1; Index >= 0; Index--)
                    {

                        for (int Row2 = 0; Row2 < Grd1.Rows.Count - 1; Row2++)
                        {
                            QtyPackaging = Sm.GetGrdDec(Grd29, Grd29.SelectedRows[Index].Index, 10);
                            QtySales = Sm.GetGrdDec(Grd29, Grd29.SelectedRows[Index].Index, 11);
                            QtyInventory = Sm.GetGrdDec(Grd29, Grd29.SelectedRows[Index].Index, 13);
                            if (
                                Sm.GetGrdStr(Grd1, Row2, 1).Length != 0 &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd29, Grd29.SelectedRows[Index].Index, 0) + Sm.GetGrdStr(Grd29, Grd29.SelectedRows[Index].Index, 4) + Sm.GetGrdStr(Grd29, Grd29.SelectedRows[Index].Index, 6),
                                Sm.GetGrdStr(Grd1, Row2, 1) + Sm.GetGrdStr(Grd1, Row2, 5) + Sm.GetGrdStr(Grd1, Row2, 7))
                                )
                            {
                                pack = Sm.GetGrdDec(Grd1, Row2, 10) - QtyPackaging;
                                sale = Sm.GetGrdDec(Grd1, Row2, 13) - QtySales;
                                inv = Sm.GetGrdDec(Grd1, Row2, 16) - QtyInventory;
                                Grd1.Cells[Row2, 10].Value = pack;
                                Grd1.Cells[Row2, 13].Value = sale;
                                Grd1.Cells[Row2, 16].Value = inv;
                                Grd1.Cells[Row2, 11].Value = Sm.GetGrdDec(Grd1, Row2, 9) - Sm.GetGrdDec(Grd1, Row2, 10);
                                Grd1.Cells[Row2, 14].Value = Sm.GetGrdDec(Grd1, Row2, 12) - Sm.GetGrdDec(Grd1, Row2, 13);
                            }
                        }
                        Grd29.Rows.RemoveAt(Grd29.SelectedRows[Index].Index);
                    }
                }
                Sm.GrdKeyDown(Grd29, e, BtnFind, BtnSave);
                ComputeUom2();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void Grd30_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                decimal QtyPackaging, pack = 0m;
                decimal QtySales, sale = 0m;
                decimal QtyInventory, inv = 0m;

                if (Grd30.Rows[Grd30.Rows[Grd30.Rows.Count - 1].Index].Selected)
                    MessageBox.Show("You can't remove last row.", Gv.CompanyName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                else
                {
                    for (int Index = Grd30.SelectedRows.Count - 1; Index >= 0; Index--)
                    {

                        for (int Row2 = 0; Row2 < Grd1.Rows.Count - 1; Row2++)
                        {
                            QtyPackaging = Sm.GetGrdDec(Grd30, Grd30.SelectedRows[Index].Index, 10);
                            QtySales = Sm.GetGrdDec(Grd30, Grd30.SelectedRows[Index].Index, 11);
                            QtyInventory = Sm.GetGrdDec(Grd30, Grd30.SelectedRows[Index].Index, 13);
                            if (
                                Sm.GetGrdStr(Grd1, Row2, 1).Length != 0 &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd30, Grd30.SelectedRows[Index].Index, 0) + Sm.GetGrdStr(Grd30, Grd30.SelectedRows[Index].Index, 4) + Sm.GetGrdStr(Grd30, Grd30.SelectedRows[Index].Index, 6),
                                Sm.GetGrdStr(Grd1, Row2, 1) + Sm.GetGrdStr(Grd1, Row2, 5) + Sm.GetGrdStr(Grd1, Row2, 7))
                                )
                            {
                                pack = Sm.GetGrdDec(Grd1, Row2, 10) - QtyPackaging;
                                sale = Sm.GetGrdDec(Grd1, Row2, 13) - QtySales;
                                inv = Sm.GetGrdDec(Grd1, Row2, 16) - QtyInventory;
                                Grd1.Cells[Row2, 10].Value = pack;
                                Grd1.Cells[Row2, 13].Value = sale;
                                Grd1.Cells[Row2, 16].Value = inv;
                                Grd1.Cells[Row2, 11].Value = Sm.GetGrdDec(Grd1, Row2, 9) - Sm.GetGrdDec(Grd1, Row2, 10);
                                Grd1.Cells[Row2, 14].Value = Sm.GetGrdDec(Grd1, Row2, 12) - Sm.GetGrdDec(Grd1, Row2, 13);
                            }
                        }
                        Grd30.Rows.RemoveAt(Grd30.SelectedRows[Index].Index);
                    }
                }
                Sm.GrdKeyDown(Grd30, e, BtnFind, BtnSave);
                ComputeUom2();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void Grd31_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                decimal QtyPackaging, pack = 0m;
                decimal QtySales, sale = 0m;
                decimal QtyInventory, inv = 0m;

                if (Grd31.Rows[Grd31.Rows[Grd31.Rows.Count - 1].Index].Selected)
                    MessageBox.Show("You can't remove last row.", Gv.CompanyName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                else
                {
                    for (int Index = Grd31.SelectedRows.Count - 1; Index >= 0; Index--)
                    {

                        for (int Row2 = 0; Row2 < Grd1.Rows.Count - 1; Row2++)
                        {
                            QtyPackaging = Sm.GetGrdDec(Grd31, Grd31.SelectedRows[Index].Index, 10);
                            QtySales = Sm.GetGrdDec(Grd31, Grd31.SelectedRows[Index].Index, 11);
                            QtyInventory = Sm.GetGrdDec(Grd31, Grd31.SelectedRows[Index].Index, 13);
                            if (
                                Sm.GetGrdStr(Grd1, Row2, 1).Length != 0 &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd31, Grd31.SelectedRows[Index].Index, 0) + Sm.GetGrdStr(Grd31, Grd31.SelectedRows[Index].Index, 4) + Sm.GetGrdStr(Grd31, Grd31.SelectedRows[Index].Index, 6),
                                Sm.GetGrdStr(Grd1, Row2, 1) + Sm.GetGrdStr(Grd1, Row2, 5) + Sm.GetGrdStr(Grd1, Row2, 7))
                                )
                            {
                                pack = Sm.GetGrdDec(Grd1, Row2, 10) - QtyPackaging;
                                sale = Sm.GetGrdDec(Grd1, Row2, 13) - QtySales;
                                inv = Sm.GetGrdDec(Grd1, Row2, 16) - QtyInventory;
                                Grd1.Cells[Row2, 10].Value = pack;
                                Grd1.Cells[Row2, 13].Value = sale;
                                Grd1.Cells[Row2, 16].Value = inv;
                                Grd1.Cells[Row2, 11].Value = Sm.GetGrdDec(Grd1, Row2, 9) - Sm.GetGrdDec(Grd1, Row2, 10);
                                Grd1.Cells[Row2, 14].Value = Sm.GetGrdDec(Grd1, Row2, 12) - Sm.GetGrdDec(Grd1, Row2, 13);
                            }
                        }
                        Grd31.Rows.RemoveAt(Grd31.SelectedRows[Index].Index);
                    }
                }
                Sm.GrdKeyDown(Grd31, e, BtnFind, BtnSave);
                ComputeUom2();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void Grd32_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                decimal QtyPackaging, pack = 0m;
                decimal QtySales, sale = 0m;
                decimal QtyInventory, inv = 0m;

                if (Grd32.Rows[Grd32.Rows[Grd32.Rows.Count - 1].Index].Selected)
                    MessageBox.Show("You can't remove last row.", Gv.CompanyName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                else
                {
                    for (int Index = Grd32.SelectedRows.Count - 1; Index >= 0; Index--)
                    {

                        for (int Row2 = 0; Row2 < Grd1.Rows.Count - 1; Row2++)
                        {
                            QtyPackaging = Sm.GetGrdDec(Grd32, Grd32.SelectedRows[Index].Index, 10);
                            QtySales = Sm.GetGrdDec(Grd32, Grd32.SelectedRows[Index].Index, 11);
                            QtyInventory = Sm.GetGrdDec(Grd32, Grd32.SelectedRows[Index].Index, 13);
                            if (
                                Sm.GetGrdStr(Grd1, Row2, 1).Length != 0 &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd32, Grd32.SelectedRows[Index].Index, 0) + Sm.GetGrdStr(Grd32, Grd32.SelectedRows[Index].Index, 4) + Sm.GetGrdStr(Grd32, Grd32.SelectedRows[Index].Index, 6),
                                Sm.GetGrdStr(Grd1, Row2, 1) + Sm.GetGrdStr(Grd1, Row2, 5) + Sm.GetGrdStr(Grd1, Row2, 7))
                                )
                            {
                                pack = Sm.GetGrdDec(Grd1, Row2, 10) - QtyPackaging;
                                sale = Sm.GetGrdDec(Grd1, Row2, 13) - QtySales;
                                inv = Sm.GetGrdDec(Grd1, Row2, 16) - QtyInventory;
                                Grd1.Cells[Row2, 10].Value = pack;
                                Grd1.Cells[Row2, 13].Value = sale;
                                Grd1.Cells[Row2, 16].Value = inv;
                                Grd1.Cells[Row2, 11].Value = Sm.GetGrdDec(Grd1, Row2, 9) - Sm.GetGrdDec(Grd1, Row2, 10);
                                Grd1.Cells[Row2, 14].Value = Sm.GetGrdDec(Grd1, Row2, 12) - Sm.GetGrdDec(Grd1, Row2, 13);
                            }
                        }
                        Grd32.Rows.RemoveAt(Grd32.SelectedRows[Index].Index);
                    }
                }
                Sm.GrdKeyDown(Grd32, e, BtnFind, BtnSave);
                ComputeUom2();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void Grd33_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                decimal QtyPackaging, pack = 0m;
                decimal QtySales, sale = 0m;
                decimal QtyInventory, inv = 0m;

                if (Grd33.Rows[Grd33.Rows[Grd33.Rows.Count - 1].Index].Selected)
                    MessageBox.Show("You can't remove last row.", Gv.CompanyName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                else
                {
                    for (int Index = Grd33.SelectedRows.Count - 1; Index >= 0; Index--)
                    {

                        for (int Row2 = 0; Row2 < Grd1.Rows.Count - 1; Row2++)
                        {
                            QtyPackaging = Sm.GetGrdDec(Grd33, Grd33.SelectedRows[Index].Index, 10);
                            QtySales = Sm.GetGrdDec(Grd33, Grd33.SelectedRows[Index].Index, 11);
                            QtyInventory = Sm.GetGrdDec(Grd33, Grd33.SelectedRows[Index].Index, 13);
                            if (
                                Sm.GetGrdStr(Grd1, Row2, 1).Length != 0 &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd33, Grd33.SelectedRows[Index].Index, 0) + Sm.GetGrdStr(Grd33, Grd33.SelectedRows[Index].Index, 4) + Sm.GetGrdStr(Grd33, Grd33.SelectedRows[Index].Index, 6),
                                Sm.GetGrdStr(Grd1, Row2, 1) + Sm.GetGrdStr(Grd1, Row2, 5) + Sm.GetGrdStr(Grd1, Row2, 7))
                                )
                            {
                                pack = Sm.GetGrdDec(Grd1, Row2, 10) - QtyPackaging;
                                sale = Sm.GetGrdDec(Grd1, Row2, 13) - QtySales;
                                inv = Sm.GetGrdDec(Grd1, Row2, 16) - QtyInventory;
                                Grd1.Cells[Row2, 10].Value = pack;
                                Grd1.Cells[Row2, 13].Value = sale;
                                Grd1.Cells[Row2, 16].Value = inv;
                                Grd1.Cells[Row2, 11].Value = Sm.GetGrdDec(Grd1, Row2, 9) - Sm.GetGrdDec(Grd1, Row2, 10);
                                Grd1.Cells[Row2, 14].Value = Sm.GetGrdDec(Grd1, Row2, 12) - Sm.GetGrdDec(Grd1, Row2, 13);
                            }
                        }
                        Grd33.Rows.RemoveAt(Grd33.SelectedRows[Index].Index);
                    }
                }
                Sm.GrdKeyDown(Grd33, e, BtnFind, BtnSave);
                ComputeUom2();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void Grd34_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                decimal QtyPackaging, pack = 0m;
                decimal QtySales, sale = 0m;
                decimal QtyInventory, inv = 0m;

                if (Grd34.Rows[Grd34.Rows[Grd34.Rows.Count - 1].Index].Selected)
                    MessageBox.Show("You can't remove last row.", Gv.CompanyName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                else
                {
                    for (int Index = Grd34.SelectedRows.Count - 1; Index >= 0; Index--)
                    {

                        for (int Row2 = 0; Row2 < Grd1.Rows.Count - 1; Row2++)
                        {
                            QtyPackaging = Sm.GetGrdDec(Grd34, Grd34.SelectedRows[Index].Index, 10);
                            QtySales = Sm.GetGrdDec(Grd34, Grd34.SelectedRows[Index].Index, 11);
                            QtyInventory = Sm.GetGrdDec(Grd34, Grd34.SelectedRows[Index].Index, 13);
                            if (
                                Sm.GetGrdStr(Grd1, Row2, 1).Length != 0 &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd34, Grd34.SelectedRows[Index].Index, 0) + Sm.GetGrdStr(Grd34, Grd34.SelectedRows[Index].Index, 4) + Sm.GetGrdStr(Grd34, Grd34.SelectedRows[Index].Index, 6),
                                Sm.GetGrdStr(Grd1, Row2, 1) + Sm.GetGrdStr(Grd1, Row2, 5) + Sm.GetGrdStr(Grd1, Row2, 7))
                                )
                            {
                                pack = Sm.GetGrdDec(Grd1, Row2, 10) - QtyPackaging;
                                sale = Sm.GetGrdDec(Grd1, Row2, 13) - QtySales;
                                inv = Sm.GetGrdDec(Grd1, Row2, 16) - QtyInventory;
                                Grd1.Cells[Row2, 10].Value = pack;
                                Grd1.Cells[Row2, 13].Value = sale;
                                Grd1.Cells[Row2, 16].Value = inv;
                                Grd1.Cells[Row2, 11].Value = Sm.GetGrdDec(Grd1, Row2, 9) - Sm.GetGrdDec(Grd1, Row2, 10);
                                Grd1.Cells[Row2, 14].Value = Sm.GetGrdDec(Grd1, Row2, 12) - Sm.GetGrdDec(Grd1, Row2, 13);
                            }
                        }
                        Grd34.Rows.RemoveAt(Grd34.SelectedRows[Index].Index);
                    }
                }
                Sm.GrdKeyDown(Grd34, e, BtnFind, BtnSave);
                ComputeUom2();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void Grd35_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                decimal QtyPackaging, pack = 0m;
                decimal QtySales, sale = 0m;
                decimal QtyInventory, inv = 0m;

                if (Grd35.Rows[Grd35.Rows[Grd35.Rows.Count - 1].Index].Selected)
                    MessageBox.Show("You can't remove last row.", Gv.CompanyName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                else
                {
                    for (int Index = Grd35.SelectedRows.Count - 1; Index >= 0; Index--)
                    {

                        for (int Row2 = 0; Row2 < Grd1.Rows.Count - 1; Row2++)
                        {
                            QtyPackaging = Sm.GetGrdDec(Grd35, Grd35.SelectedRows[Index].Index, 10);
                            QtySales = Sm.GetGrdDec(Grd35, Grd35.SelectedRows[Index].Index, 11);
                            QtyInventory = Sm.GetGrdDec(Grd35, Grd35.SelectedRows[Index].Index, 13);
                            if (
                                Sm.GetGrdStr(Grd1, Row2, 1).Length != 0 &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd35, Grd35.SelectedRows[Index].Index, 0) + Sm.GetGrdStr(Grd35, Grd35.SelectedRows[Index].Index, 4) + Sm.GetGrdStr(Grd35, Grd35.SelectedRows[Index].Index, 6),
                                Sm.GetGrdStr(Grd1, Row2, 1) + Sm.GetGrdStr(Grd1, Row2, 5) + Sm.GetGrdStr(Grd1, Row2, 7))
                                )
                            {
                                pack = Sm.GetGrdDec(Grd1, Row2, 10) - QtyPackaging;
                                sale = Sm.GetGrdDec(Grd1, Row2, 13) - QtySales;
                                inv = Sm.GetGrdDec(Grd1, Row2, 16) - QtyInventory;
                                Grd1.Cells[Row2, 10].Value = pack;
                                Grd1.Cells[Row2, 13].Value = sale;
                                Grd1.Cells[Row2, 16].Value = inv;
                                Grd1.Cells[Row2, 11].Value = Sm.GetGrdDec(Grd1, Row2, 9) - Sm.GetGrdDec(Grd1, Row2, 10);
                                Grd1.Cells[Row2, 14].Value = Sm.GetGrdDec(Grd1, Row2, 12) - Sm.GetGrdDec(Grd1, Row2, 13);
                            }
                        }
                        Grd35.Rows.RemoveAt(Grd35.SelectedRows[Index].Index);
                    }
                }
                Sm.GrdKeyDown(Grd35, e, BtnFind, BtnSave);
                ComputeUom2();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Grd Elipsis

        private void Grd11_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd11, e.RowIndex, 0).Length != 0)
            {

                var f1 = new FrmItem(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mItCode = Sm.GetGrdStr(Grd11, e.RowIndex, 0);
                f1.ShowDialog();
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd11, e.RowIndex, 4).Length != 0)
            {

                var f1 = new FrmSO2(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd11, e.RowIndex, 4);
                f1.ShowDialog();
            }
        }

        private void Grd12_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd12, e.RowIndex, 0).Length != 0)
            {

                var f1 = new FrmItem(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mItCode = Sm.GetGrdStr(Grd12, e.RowIndex, 0);
                f1.ShowDialog();
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd12, e.RowIndex, 4).Length != 0)
            {

                var f1 = new FrmSO2(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd12, e.RowIndex, 4);
                f1.ShowDialog();
            }
        }

        private void Grd13_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd13, e.RowIndex, 0).Length != 0)
            {

                var f1 = new FrmItem(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mItCode = Sm.GetGrdStr(Grd13, e.RowIndex, 0);
                f1.ShowDialog();
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd13, e.RowIndex, 4).Length != 0)
            {

                var f1 = new FrmSO2(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd13, e.RowIndex, 4);
                f1.ShowDialog();
            }
        }

        private void Grd14_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd14, e.RowIndex, 0).Length != 0)
            {

                var f1 = new FrmItem(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mItCode = Sm.GetGrdStr(Grd14, e.RowIndex, 0);
                f1.ShowDialog();
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd14, e.RowIndex, 4).Length != 0)
            {

                var f1 = new FrmSO2(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd14, e.RowIndex, 4);
                f1.ShowDialog();
            }
        }

        private void Grd15_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd15, e.RowIndex, 0).Length != 0)
            {

                var f1 = new FrmItem(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mItCode = Sm.GetGrdStr(Grd15, e.RowIndex, 0);
                f1.ShowDialog();
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd15, e.RowIndex, 4).Length != 0)
            {

                var f1 = new FrmSO2(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd15, e.RowIndex, 4);
                f1.ShowDialog();
            }
        }

        private void Grd16_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd16, e.RowIndex, 0).Length != 0)
            {

                var f1 = new FrmItem(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mItCode = Sm.GetGrdStr(Grd16, e.RowIndex, 0);
                f1.ShowDialog();
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd16, e.RowIndex, 4).Length != 0)
            {

                var f1 = new FrmSO2(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd16, e.RowIndex, 4);
                f1.ShowDialog();
            }
        }

        private void Grd17_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd17, e.RowIndex, 0).Length != 0)
            {

                var f1 = new FrmItem(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mItCode = Sm.GetGrdStr(Grd17, e.RowIndex, 0);
                f1.ShowDialog();
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd17, e.RowIndex, 4).Length != 0)
            {

                var f1 = new FrmSO2(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd17, e.RowIndex, 4);
                f1.ShowDialog();
            }
        }

        private void Grd18_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd18, e.RowIndex, 0).Length != 0)
            {

                var f1 = new FrmItem(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mItCode = Sm.GetGrdStr(Grd18, e.RowIndex, 0);
                f1.ShowDialog();
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd18, e.RowIndex, 4).Length != 0)
            {

                var f1 = new FrmSO2(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd18, e.RowIndex, 4);
                f1.ShowDialog();
            }
        }

        private void Grd19_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd19, e.RowIndex, 0).Length != 0)
            {

                var f1 = new FrmItem(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mItCode = Sm.GetGrdStr(Grd19, e.RowIndex, 0);
                f1.ShowDialog();
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd19, e.RowIndex, 4).Length != 0)
            {

                var f1 = new FrmSO2(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd19, e.RowIndex, 4);
                f1.ShowDialog();
            }
        }

        private void Grd20_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd20, e.RowIndex, 0).Length != 0)
            {

                var f1 = new FrmItem(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mItCode = Sm.GetGrdStr(Grd20, e.RowIndex, 0);
                f1.ShowDialog();
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd20, e.RowIndex, 4).Length != 0)
            {

                var f1 = new FrmSO2(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd20, e.RowIndex, 4);
                f1.ShowDialog();
            }
        }

        private void Grd21_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd21, e.RowIndex, 0).Length != 0)
            {

                var f1 = new FrmItem(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mItCode = Sm.GetGrdStr(Grd21, e.RowIndex, 0);
                f1.ShowDialog();
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd21, e.RowIndex, 4).Length != 0)
            {

                var f1 = new FrmSO2(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd21, e.RowIndex, 4);
                f1.ShowDialog();
            }
        }

        private void Grd22_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd22, e.RowIndex, 0).Length != 0)
            {

                var f1 = new FrmItem(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mItCode = Sm.GetGrdStr(Grd22, e.RowIndex, 0);
                f1.ShowDialog();
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd22, e.RowIndex, 4).Length != 0)
            {

                var f1 = new FrmSO2(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd22, e.RowIndex, 4);
                f1.ShowDialog();
            }
        }

        private void Grd23_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd23, e.RowIndex, 0).Length != 0)
            {

                var f1 = new FrmItem(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mItCode = Sm.GetGrdStr(Grd23, e.RowIndex, 0);
                f1.ShowDialog();
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd23, e.RowIndex, 4).Length != 0)
            {

                var f1 = new FrmSO2(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd23, e.RowIndex, 4);
                f1.ShowDialog();
            }
        }

        private void Grd24_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2&& Sm.GetGrdStr(Grd24, e.RowIndex, 0).Length != 0)
            {

                var f1 = new FrmItem(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mItCode = Sm.GetGrdStr(Grd24, e.RowIndex, 0);
                f1.ShowDialog();
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd24, e.RowIndex, 4).Length != 0)
            {

                var f1 = new FrmSO2(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd24, e.RowIndex, 4);
                f1.ShowDialog();
            }
        }

        private void Grd25_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd25, e.RowIndex, 0).Length != 0)
            {

                var f1 = new FrmItem(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mItCode = Sm.GetGrdStr(Grd25, e.RowIndex, 0);
                f1.ShowDialog();
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd25, e.RowIndex, 4).Length != 0)
            {

                var f1 = new FrmSO2(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd25, e.RowIndex, 4);
                f1.ShowDialog();
            }
        }

        private void Grd26_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd26, e.RowIndex, 0).Length != 0)
            {

                var f1 = new FrmItem(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mItCode = Sm.GetGrdStr(Grd26, e.RowIndex, 0);
                f1.ShowDialog();
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd26, e.RowIndex, 4).Length != 0)
            {

                var f1 = new FrmSO2(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd26, e.RowIndex, 4);
                f1.ShowDialog();
            }
        }

        private void Grd27_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd27, e.RowIndex, 0).Length != 0)
            {

                var f1 = new FrmItem(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mItCode = Sm.GetGrdStr(Grd27, e.RowIndex, 0);
                f1.ShowDialog();
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd27, e.RowIndex, 4).Length != 0)
            {

                var f1 = new FrmSO2(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd27, e.RowIndex, 4);
                f1.ShowDialog();
            }
        }

        private void Grd28_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd28, e.RowIndex, 0).Length != 0)
            {

                var f1 = new FrmItem(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mItCode = Sm.GetGrdStr(Grd28, e.RowIndex, 0);
                f1.ShowDialog();
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd28, e.RowIndex, 4).Length != 0)
            {

                var f1 = new FrmSO2(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd28, e.RowIndex, 4);
                f1.ShowDialog();
            }
        }

        private void Grd29_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd29, e.RowIndex, 0).Length != 0)
            {

                var f1 = new FrmItem(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mItCode = Sm.GetGrdStr(Grd29, e.RowIndex, 0);
                f1.ShowDialog();
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd29, e.RowIndex, 4).Length != 0)
            {

                var f1 = new FrmSO2(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd29, e.RowIndex, 4);
                f1.ShowDialog();
            }
        }

        private void Grd30_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd30, e.RowIndex, 0).Length != 0)
            {

                var f1 = new FrmItem(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mItCode = Sm.GetGrdStr(Grd30, e.RowIndex, 0);
                f1.ShowDialog();
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd30, e.RowIndex, 4).Length != 0)
            {

                var f1 = new FrmSO2(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd30, e.RowIndex, 4);
                f1.ShowDialog();
            }
        }

        private void Grd31_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd31, e.RowIndex, 0).Length != 0)
            {

                var f1 = new FrmItem(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mItCode = Sm.GetGrdStr(Grd31, e.RowIndex, 0);
                f1.ShowDialog();
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd31, e.RowIndex, 4).Length != 0)
            {

                var f1 = new FrmSO2(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd31, e.RowIndex, 4);
                f1.ShowDialog();
            }
        }

        private void Grd32_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd32, e.RowIndex, 0).Length != 0)
            {

                var f1 = new FrmItem(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mItCode = Sm.GetGrdStr(Grd32, e.RowIndex, 0);
                f1.ShowDialog();
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd32, e.RowIndex, 4).Length != 0)
            {

                var f1 = new FrmSO2(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd32, e.RowIndex, 4);
                f1.ShowDialog();
            }
        }

        private void Grd33_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd33, e.RowIndex, 0).Length != 0)
            {

                var f1 = new FrmItem(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mItCode = Sm.GetGrdStr(Grd33, e.RowIndex, 0);
                f1.ShowDialog();
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd33, e.RowIndex, 4).Length != 0)
            {

                var f1 = new FrmSO2(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd33, e.RowIndex, 4);
                f1.ShowDialog();
            }
        }

        private void Grd34_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd34, e.RowIndex, 0).Length != 0)
            {

                var f1 = new FrmItem(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mItCode = Sm.GetGrdStr(Grd34, e.RowIndex, 0);
                f1.ShowDialog();
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd34, e.RowIndex, 4).Length != 0)
            {

                var f1 = new FrmSO2(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd34, e.RowIndex, 4);
                f1.ShowDialog();
            }
        }

        private void Grd35_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd35, e.RowIndex, 0).Length != 0)
            {

                var f1 = new FrmItem(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mItCode = Sm.GetGrdStr(Grd35, e.RowIndex, 0);
                f1.ShowDialog();
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd35, e.RowIndex, 4).Length != 0)
            {

                var f1 = new FrmSO2(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = Sm.GetGrdStr(Grd35, e.RowIndex, 4);
                f1.ShowDialog();
            }
        }


        #endregion

        #region Grd after commit

        private void Grd11_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            //decimal QtyPackaging, pack = 0m;
            //decimal QtySales, sale = 0m;
            //decimal QtyInventory, inve = 0m;
            //pack = Sm.GetGrdDec(Grd11, e.RowIndex, 8);
            //sale = Sm.GetGrdDec(Grd11, e.RowIndex, 9);
            //inve = Sm.GetGrdDec(Grd11, e.RowIndex, 11);
            if (Sm.IsGrdColSelected(new int[] { 10 }, e.ColIndex)&& Grd1.Rows.Count>1)
            {
                ComputeUom(Grd11);
                ComputePackagingQty(Sm.GetGrdStr(Grd11, e.RowIndex, 0), Sm.GetGrdStr(Grd11, e.RowIndex, 4), Sm.GetGrdStr(Grd11, e.RowIndex, 6));
                ComputeUom2();
                SetProcessInd(Grd11);
                //for (int Row2 = 0; Row2 < Grd1.Rows.Count - 1; Row2++)
                //{
                //    QtyPackaging = Sm.GetGrdDec(Grd1, Row2, 10)-pack ;
                //    QtySales = Sm.GetGrdDec(Grd1, Row2, 13)-sale ;
                //    QtyInventory = Sm.GetGrdDec(Grd1, Row2, 16)-inve ;
                //    if (
                //        Sm.GetGrdStr(Grd1, Row2, 1).Length != 0 &&
                //        Sm.CompareStr(Sm.GetGrdStr(Grd11, e.RowIndex, 0) + Sm.GetGrdStr(Grd11, e.RowIndex, 4) + Sm.GetGrdStr(Grd11, e.RowIndex, 6), 
                //        Sm.GetGrdStr(Grd1, Row2, 1) + Sm.GetGrdStr(Grd1, Row2, 5) + Sm.GetGrdStr(Grd1, Row2, 7))
                //        )
                //    {
                //        QtyPackaging += Sm.GetGrdDec(Grd11, e.RowIndex, 8);
                //        QtySales += Sm.GetGrdDec(Grd11, e.RowIndex, 9);
                //        QtyInventory += Sm.GetGrdDec(Grd11, e.RowIndex, 11);
                //    }
                //    Grd1.Cells[Row2, 10].Value = QtyPackaging;
                //    Grd1.Cells[Row2, 13].Value = QtySales;
                //    Grd1.Cells[Row2, 16].Value = QtyInventory;
                //    Grd1.Cells[Row2, 11].Value = Sm.GetGrdDec(Grd1, Row2, 9) - Sm.GetGrdDec(Grd1, Row2, 10);
                //    Grd1.Cells[Row2, 14].Value = Sm.GetGrdDec(Grd1, Row2, 12) - Sm.GetGrdDec(Grd1, Row2, 13);
                //}
            }

        }

        private void Grd12_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 10 }, e.ColIndex) && Grd1.Rows.Count > 1)
            {
                ComputeUom(Grd12);
                ComputePackagingQty(Sm.GetGrdStr(Grd12, e.RowIndex, 0), Sm.GetGrdStr(Grd12, e.RowIndex, 4), Sm.GetGrdStr(Grd12, e.RowIndex, 6));
                ComputeUom2();
                SetProcessInd(Grd12);
            }
        }

        private void Grd13_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 10 }, e.ColIndex) && Grd1.Rows.Count > 1)
            {
                ComputeUom(Grd13);
                ComputePackagingQty(Sm.GetGrdStr(Grd13, e.RowIndex, 0), Sm.GetGrdStr(Grd13, e.RowIndex, 4), Sm.GetGrdStr(Grd13, e.RowIndex, 6));
                ComputeUom2();
                SetProcessInd(Grd13);
            }
        }

        private void Grd14_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 10 }, e.ColIndex) && Grd1.Rows.Count > 1)
            {
                ComputeUom(Grd14);
                ComputePackagingQty(Sm.GetGrdStr(Grd14, e.RowIndex, 0), Sm.GetGrdStr(Grd14, e.RowIndex, 4), Sm.GetGrdStr(Grd14, e.RowIndex, 6));
                ComputeUom2();
                SetProcessInd(Grd14);
            }
        }

        private void Grd15_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 10 }, e.ColIndex) && Grd1.Rows.Count > 1)
            {
                ComputeUom(Grd15);
                ComputePackagingQty(Sm.GetGrdStr(Grd15, e.RowIndex, 0), Sm.GetGrdStr(Grd15, e.RowIndex, 4), Sm.GetGrdStr(Grd15, e.RowIndex, 6));
                ComputeUom2();
                SetProcessInd(Grd15);
            }
        }

        private void Grd16_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 10 }, e.ColIndex) && Grd1.Rows.Count > 1)
            {
                ComputeUom(Grd16);
                ComputePackagingQty(Sm.GetGrdStr(Grd16, e.RowIndex, 0), Sm.GetGrdStr(Grd16, e.RowIndex, 4), Sm.GetGrdStr(Grd16, e.RowIndex, 6));
                ComputeUom2();
                SetProcessInd(Grd16);
            }
        }

        private void Grd17_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 10 }, e.ColIndex) && Grd1.Rows.Count > 1)
            {
                ComputeUom(Grd17);
                ComputePackagingQty(Sm.GetGrdStr(Grd17, e.RowIndex, 0), Sm.GetGrdStr(Grd17, e.RowIndex, 4), Sm.GetGrdStr(Grd17, e.RowIndex, 6));
                ComputeUom2();
                SetProcessInd(Grd17);
            }
        }

        private void Grd18_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 10 }, e.ColIndex) && Grd1.Rows.Count > 1)
            {
                ComputeUom(Grd18);
                ComputePackagingQty(Sm.GetGrdStr(Grd18, e.RowIndex, 0), Sm.GetGrdStr(Grd18, e.RowIndex, 4), Sm.GetGrdStr(Grd18, e.RowIndex, 6));
                ComputeUom2();
                SetProcessInd(Grd18);
            }
        }

        private void Grd19_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 10 }, e.ColIndex) && Grd1.Rows.Count > 1)
            {
                ComputeUom(Grd19);
                ComputePackagingQty(Sm.GetGrdStr(Grd19, e.RowIndex, 0), Sm.GetGrdStr(Grd19, e.RowIndex, 4), Sm.GetGrdStr(Grd19, e.RowIndex, 6));
                ComputeUom2();
                SetProcessInd(Grd19);
            }
        }

        private void Grd20_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 10 }, e.ColIndex) && Grd1.Rows.Count > 1)
            {
                ComputeUom(Grd20);
                ComputePackagingQty(Sm.GetGrdStr(Grd20, e.RowIndex, 0), Sm.GetGrdStr(Grd20, e.RowIndex, 4), Sm.GetGrdStr(Grd20, e.RowIndex, 6));
                ComputeUom2();
                SetProcessInd(Grd20);
            }
        }

        private void Grd21_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 10 }, e.ColIndex) && Grd1.Rows.Count > 1)
            {
                ComputeUom(Grd21);
                ComputePackagingQty(Sm.GetGrdStr(Grd21, e.RowIndex, 0), Sm.GetGrdStr(Grd21, e.RowIndex, 4), Sm.GetGrdStr(Grd21, e.RowIndex, 6));
                ComputeUom2();
                SetProcessInd(Grd21);
            }
        }

        private void Grd22_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 10 }, e.ColIndex) && Grd1.Rows.Count > 1)
            {
                ComputeUom(Grd22);
                ComputePackagingQty(Sm.GetGrdStr(Grd22, e.RowIndex, 0), Sm.GetGrdStr(Grd22, e.RowIndex, 4), Sm.GetGrdStr(Grd22, e.RowIndex, 6));
                ComputeUom2();
                SetProcessInd(Grd22);
            }
        }

        private void Grd23_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 10 }, e.ColIndex) && Grd1.Rows.Count > 1)
            {
                ComputeUom(Grd23);
                ComputePackagingQty(Sm.GetGrdStr(Grd23, e.RowIndex, 0), Sm.GetGrdStr(Grd23, e.RowIndex, 4), Sm.GetGrdStr(Grd23, e.RowIndex, 6));
                ComputeUom2();
                SetProcessInd(Grd23);
            }
        }

        private void Grd24_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 10 }, e.ColIndex) && Grd1.Rows.Count > 1)
            {
                ComputeUom(Grd24);
                ComputePackagingQty(Sm.GetGrdStr(Grd24, e.RowIndex, 0), Sm.GetGrdStr(Grd24, e.RowIndex, 4), Sm.GetGrdStr(Grd24, e.RowIndex, 6));
                ComputeUom2();
                SetProcessInd(Grd24);
            }
        }

        private void Grd25_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 10 }, e.ColIndex) && Grd1.Rows.Count > 1)
            {
                ComputeUom(Grd25);
                ComputePackagingQty(Sm.GetGrdStr(Grd25, e.RowIndex, 0), Sm.GetGrdStr(Grd25, e.RowIndex, 4), Sm.GetGrdStr(Grd25, e.RowIndex, 6));
                ComputeUom2();
                SetProcessInd(Grd25);
            }
        }

        private void Grd26_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 10 }, e.ColIndex) && Grd1.Rows.Count > 1)
            {
                ComputeUom(Grd26);
                ComputePackagingQty(Sm.GetGrdStr(Grd26, e.RowIndex, 0), Sm.GetGrdStr(Grd26, e.RowIndex, 4), Sm.GetGrdStr(Grd26, e.RowIndex, 6));
                ComputeUom2();
                SetProcessInd(Grd26);
            }
        }

        private void Grd27_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 10 }, e.ColIndex) && Grd1.Rows.Count > 1)
            {
                ComputeUom(Grd27);
                ComputePackagingQty(Sm.GetGrdStr(Grd27, e.RowIndex, 0), Sm.GetGrdStr(Grd27, e.RowIndex, 4), Sm.GetGrdStr(Grd27, e.RowIndex, 6));
                ComputeUom2();
                SetProcessInd(Grd27);
            }
        }

        private void Grd28_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 10 }, e.ColIndex) && Grd1.Rows.Count > 1)
            {
                ComputeUom(Grd28);
                ComputePackagingQty(Sm.GetGrdStr(Grd28, e.RowIndex, 0), Sm.GetGrdStr(Grd28, e.RowIndex, 4), Sm.GetGrdStr(Grd28, e.RowIndex, 6));
                ComputeUom2();
                SetProcessInd(Grd28);
            }
        }

        private void Grd29_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 10 }, e.ColIndex) && Grd1.Rows.Count > 1)
            {
                ComputeUom(Grd29);
                ComputePackagingQty(Sm.GetGrdStr(Grd29, e.RowIndex, 0), Sm.GetGrdStr(Grd29, e.RowIndex, 4), Sm.GetGrdStr(Grd29, e.RowIndex, 6));
                ComputeUom2();
                SetProcessInd(Grd29);
            }
        }

        private void Grd30_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 10 }, e.ColIndex) && Grd1.Rows.Count > 1)
            {
                ComputeUom(Grd30);
                ComputePackagingQty(Sm.GetGrdStr(Grd30, e.RowIndex, 0), Sm.GetGrdStr(Grd30, e.RowIndex, 4), Sm.GetGrdStr(Grd30, e.RowIndex, 6));
                ComputeUom2();
                SetProcessInd(Grd30);

            }
        }

        private void Grd31_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 10 }, e.ColIndex) && Grd1.Rows.Count > 1)
            {
                ComputeUom(Grd31);
                ComputePackagingQty(Sm.GetGrdStr(Grd31, e.RowIndex, 0), Sm.GetGrdStr(Grd31, e.RowIndex, 4), Sm.GetGrdStr(Grd31, e.RowIndex, 6));
                ComputeUom2();
                SetProcessInd(Grd31);
            }
        }

        private void Grd32_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 10 }, e.ColIndex) && Grd1.Rows.Count > 1)
            {
                ComputeUom(Grd32);
                ComputePackagingQty(Sm.GetGrdStr(Grd32, e.RowIndex, 0), Sm.GetGrdStr(Grd32, e.RowIndex, 4), Sm.GetGrdStr(Grd32, e.RowIndex, 6));
                ComputeUom2();
                SetProcessInd(Grd32);
            }
        }

        private void Grd33_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 10 }, e.ColIndex) && Grd1.Rows.Count > 1)
            {
                ComputeUom(Grd33);
                ComputePackagingQty(Sm.GetGrdStr(Grd33, e.RowIndex, 0), Sm.GetGrdStr(Grd33, e.RowIndex, 4), Sm.GetGrdStr(Grd33, e.RowIndex, 6));
                ComputeUom2();
                SetProcessInd(Grd33);
            }
        }

        private void Grd34_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 10 }, e.ColIndex) && Grd1.Rows.Count > 1)
            {
                ComputeUom(Grd34);
                ComputePackagingQty(Sm.GetGrdStr(Grd34, e.RowIndex, 0), Sm.GetGrdStr(Grd34, e.RowIndex, 4), Sm.GetGrdStr(Grd34, e.RowIndex, 6));
                ComputeUom2();
                SetProcessInd(Grd34);
            }
        }

        private void Grd35_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 10 }, e.ColIndex) && Grd1.Rows.Count > 1)
            {
                ComputeUom(Grd35);
                ComputePackagingQty(Sm.GetGrdStr(Grd35, e.RowIndex, 0), Sm.GetGrdStr(Grd35, e.RowIndex, 4), Sm.GetGrdStr(Grd35, e.RowIndex, 6));
                ComputeUom2();
                SetProcessInd(Grd35);
            }
        }

        #endregion

        #region grd header double click
        private void Grd11_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (e.ColIndex == 17)
            {
                if (Sm.GetGrdStr(Grd1, 0, 17).Length != 0)
                {
                    var Remark = Sm.GetGrdStr(Grd11, 0, 17);
                    for (int Row = 0; Row < Grd11.Rows.Count - 1; Row++)
                        if (Sm.GetGrdStr(Grd11, Row, 0).Length != 0) Grd11.Cells[Row, 17].Value = Remark;
                }
            }
        }

        private void Grd12_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (e.ColIndex == 17)
            {
                if (Sm.GetGrdStr(Grd1, 0, 17).Length != 0)
                {
                    var Remark = Sm.GetGrdStr(Grd12, 0, 17);
                    for (int Row = 0; Row < Grd12.Rows.Count - 1; Row++)
                        if (Sm.GetGrdStr(Grd12, Row, 0).Length != 0) Grd12.Cells[Row, 17].Value = Remark;
                }
            }
        }

        private void Grd13_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (e.ColIndex == 17)
            {
                if (Sm.GetGrdStr(Grd13, 0, 17).Length != 0)
                {
                    var Remark = Sm.GetGrdStr(Grd13, 0, 17);
                    for (int Row = 0; Row < Grd13.Rows.Count - 1; Row++)
                        if (Sm.GetGrdStr(Grd13, Row, 0).Length != 0) Grd13.Cells[Row, 17].Value = Remark;
                }
            }
        }

        private void Grd14_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (e.ColIndex == 17)
            {
                if (Sm.GetGrdStr(Grd14, 0, 17).Length != 0)
                {
                    var Remark = Sm.GetGrdStr(Grd14, 0, 17);
                    for (int Row = 0; Row < Grd14.Rows.Count - 1; Row++)
                        if (Sm.GetGrdStr(Grd14, Row, 0).Length != 0) Grd14.Cells[Row, 17].Value = Remark;
                }
            }
        }

        private void Grd15_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (e.ColIndex == 17)
            {
                if (Sm.GetGrdStr(Grd15, 0, 17).Length != 0)
                {
                    var Remark = Sm.GetGrdStr(Grd15, 0, 17);
                    for (int Row = 0; Row < Grd15.Rows.Count - 1; Row++)
                        if (Sm.GetGrdStr(Grd15, Row, 0).Length != 0) Grd15.Cells[Row, 17].Value = Remark;
                }
            }
        }

        private void Grd16_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (e.ColIndex == 17)
            {
                if (Sm.GetGrdStr(Grd16, 0, 17).Length != 0)
                {
                    var Remark = Sm.GetGrdStr(Grd16, 0, 17);
                    for (int Row = 0; Row < Grd16.Rows.Count - 1; Row++)
                        if (Sm.GetGrdStr(Grd16, Row, 0).Length != 0) Grd16.Cells[Row, 17].Value = Remark;
                }
            }
        }

        private void Grd17_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (e.ColIndex == 17)
            {
                if (Sm.GetGrdStr(Grd17, 0, 17).Length != 0)
                {
                    var Remark = Sm.GetGrdStr(Grd17, 0, 17);
                    for (int Row = 0; Row < Grd17.Rows.Count - 1; Row++)
                        if (Sm.GetGrdStr(Grd17, Row, 0).Length != 0) Grd17.Cells[Row, 17].Value = Remark;
                }
            }
        }

        private void Grd18_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (e.ColIndex == 17)
            {
                if (Sm.GetGrdStr(Grd18, 0, 17).Length != 0)
                {
                    var Remark = Sm.GetGrdStr(Grd18, 0, 17);
                    for (int Row = 0; Row < Grd18.Rows.Count - 1; Row++)
                        if (Sm.GetGrdStr(Grd18, Row, 0).Length != 0) Grd18.Cells[Row, 17].Value = Remark;
                }
            }
        }

        private void Grd19_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (e.ColIndex == 17)
            {
                if (Sm.GetGrdStr(Grd19, 0, 17).Length != 0)
                {
                    var Remark = Sm.GetGrdStr(Grd19, 0, 17);
                    for (int Row = 0; Row < Grd19.Rows.Count - 1; Row++)
                        if (Sm.GetGrdStr(Grd19, Row, 0).Length != 0) Grd19.Cells[Row, 17].Value = Remark;
                }
            }
        }

        private void Grd20_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (e.ColIndex == 17)
            {
                if (Sm.GetGrdStr(Grd20, 0, 17).Length != 0)
                {
                    var Remark = Sm.GetGrdStr(Grd20, 0, 17);
                    for (int Row = 0; Row < Grd20.Rows.Count - 1; Row++)
                        if (Sm.GetGrdStr(Grd20, Row, 0).Length != 0) Grd20.Cells[Row, 17].Value = Remark;
                }
            }
        }

        private void Grd21_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (e.ColIndex == 17)
            {
                if (Sm.GetGrdStr(Grd21, 0, 17).Length != 0)
                {
                    var Remark = Sm.GetGrdStr(Grd21, 0, 17);
                    for (int Row = 0; Row < Grd21.Rows.Count - 1; Row++)
                        if (Sm.GetGrdStr(Grd21, Row, 0).Length != 0) Grd21.Cells[Row, 17].Value = Remark;
                }
            }
        }

        private void Grd22_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (e.ColIndex == 17)
            {
                if (Sm.GetGrdStr(Grd22, 0, 17).Length != 0)
                {
                    var Remark = Sm.GetGrdStr(Grd22, 0, 17);
                    for (int Row = 0; Row < Grd22.Rows.Count - 1; Row++)
                        if (Sm.GetGrdStr(Grd22, Row, 0).Length != 0) Grd22.Cells[Row, 17].Value = Remark;
                }
            }
        }

        private void Grd23_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (e.ColIndex == 17)
            {
                if (Sm.GetGrdStr(Grd23, 0, 17).Length != 0)
                {
                    var Remark = Sm.GetGrdStr(Grd23, 0, 17);
                    for (int Row = 0; Row < Grd23.Rows.Count - 1; Row++)
                        if (Sm.GetGrdStr(Grd23, Row, 0).Length != 0) Grd23.Cells[Row, 17].Value = Remark;
                }
            }
        }

        private void Grd24_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (e.ColIndex == 17)
            {
                if (Sm.GetGrdStr(Grd24, 0, 17).Length != 0)
                {
                    var Remark = Sm.GetGrdStr(Grd24, 0, 17);
                    for (int Row = 0; Row < Grd24.Rows.Count - 1; Row++)
                        if (Sm.GetGrdStr(Grd24, Row, 0).Length != 0) Grd24.Cells[Row, 17].Value = Remark;
                }
            }
        }

        private void Grd25_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (e.ColIndex == 17)
            {
                if (Sm.GetGrdStr(Grd25, 0, 17).Length != 0)
                {
                    var Remark = Sm.GetGrdStr(Grd25, 0, 17);
                    for (int Row = 0; Row < Grd25.Rows.Count - 1; Row++)
                        if (Sm.GetGrdStr(Grd25, Row, 0).Length != 0) Grd25.Cells[Row, 17].Value = Remark;
                }
            }
        }

        private void Grd26_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (e.ColIndex == 17)
            {
                if (Sm.GetGrdStr(Grd26, 0, 17).Length != 0)
                {
                    var Remark = Sm.GetGrdStr(Grd26, 0, 17);
                    for (int Row = 0; Row < Grd26.Rows.Count - 1; Row++)
                        if (Sm.GetGrdStr(Grd26, Row, 0).Length != 0) Grd26.Cells[Row, 17].Value = Remark;
                }
            }
        }

        private void Grd27_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (e.ColIndex == 17)
            {
                if (Sm.GetGrdStr(Grd27, 0, 17).Length != 0)
                {
                    var Remark = Sm.GetGrdStr(Grd27, 0, 17);
                    for (int Row = 0; Row < Grd27.Rows.Count - 1; Row++)
                        if (Sm.GetGrdStr(Grd27, Row, 0).Length != 0) Grd27.Cells[Row, 17].Value = Remark;
                }
            }
        }

        private void Grd28_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (e.ColIndex == 17)
            {
                if (Sm.GetGrdStr(Grd28, 0, 17).Length != 0)
                {
                    var Remark = Sm.GetGrdStr(Grd28, 0, 17);
                    for (int Row = 0; Row < Grd28.Rows.Count - 1; Row++)
                        if (Sm.GetGrdStr(Grd28, Row, 0).Length != 0) Grd28.Cells[Row, 17].Value = Remark;
                }
            }
        }

        private void Grd29_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (e.ColIndex == 17)
            {
                if (Sm.GetGrdStr(Grd29, 0, 17).Length != 0)
                {
                    var Remark = Sm.GetGrdStr(Grd29, 0, 17);
                    for (int Row = 0; Row < Grd29.Rows.Count - 1; Row++)
                        if (Sm.GetGrdStr(Grd29, Row, 0).Length != 0) Grd29.Cells[Row, 17].Value = Remark;
                }
            }
        }

        private void Grd30_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (e.ColIndex == 17)
            {
                if (Sm.GetGrdStr(Grd30, 0, 17).Length != 0)
                {
                    var Remark = Sm.GetGrdStr(Grd30, 0, 17);
                    for (int Row = 0; Row < Grd30.Rows.Count - 1; Row++)
                        if (Sm.GetGrdStr(Grd30, Row, 0).Length != 0) Grd30.Cells[Row, 17].Value = Remark;
                }
            }
        }

        private void Grd31_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (e.ColIndex == 17)
            {
                if (Sm.GetGrdStr(Grd31, 0, 17).Length != 0)
                {
                    var Remark = Sm.GetGrdStr(Grd31, 0, 17);
                    for (int Row = 0; Row < Grd31.Rows.Count - 1; Row++)
                        if (Sm.GetGrdStr(Grd31, Row, 0).Length != 0) Grd31.Cells[Row, 17].Value = Remark;
                }
            }
        }

        private void Grd32_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (e.ColIndex == 17)
            {
                if (Sm.GetGrdStr(Grd32, 0, 17).Length != 0)
                {
                    var Remark = Sm.GetGrdStr(Grd32, 0, 17);
                    for (int Row = 0; Row < Grd32.Rows.Count - 1; Row++)
                        if (Sm.GetGrdStr(Grd32, Row, 0).Length != 0) Grd32.Cells[Row, 17].Value = Remark;
                }
            }
        }

        private void Grd33_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (e.ColIndex == 17)
            {
                if (Sm.GetGrdStr(Grd33, 0, 17).Length != 0)
                {
                    var Remark = Sm.GetGrdStr(Grd33, 0, 17);
                    for (int Row = 0; Row < Grd33.Rows.Count - 1; Row++)
                        if (Sm.GetGrdStr(Grd33, Row, 0).Length != 0) Grd33.Cells[Row, 17].Value = Remark;
                }
            }
        }

        private void Grd34_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (e.ColIndex == 17)
            {
                if (Sm.GetGrdStr(Grd34, 0, 17).Length != 0)
                {
                    var Remark = Sm.GetGrdStr(Grd34, 0, 17);
                    for (int Row = 0; Row < Grd34.Rows.Count - 1; Row++)
                        if (Sm.GetGrdStr(Grd34, Row, 0).Length != 0) Grd34.Cells[Row, 17].Value = Remark;
                }
            }
        }

        private void Grd35_ColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (e.ColIndex == 17)
            {
                if (Sm.GetGrdStr(Grd35, 0, 17).Length != 0)
                {
                    var Remark = Sm.GetGrdStr(Grd35, 0, 17);
                    for (int Row = 0; Row < Grd35.Rows.Count - 1; Row++)
                        if (Sm.GetGrdStr(Grd35, Row, 0).Length != 0) Grd35.Cells[Row, 17].Value = Remark;
                }
            }
        }

        #endregion

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = string.Empty;
            if (TxtDocNo.Text.Length == 0)
                 DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "PL", "TblPLHdr");
            else
                DocNo = TxtDocNo.Text;
            
            if (!mIsPalletInPLEditable) ProcessPallets();
            
            var cml = new List<MySqlCommand>();

            cml.Add(SavePLHdr(DocNo));
            cml.Add(SavePLDtl(DocNo));

            //int DNo = 0;
            //SavePLDtl(ref cml, DocNo, ref DNo, "1", Grd11);
            //SavePLDtl(ref cml, DocNo, ref DNo, "2", Grd12);
            //SavePLDtl(ref cml, DocNo, ref DNo, "3", Grd13);
            //SavePLDtl(ref cml, DocNo, ref DNo, "4", Grd14);
            //SavePLDtl(ref cml, DocNo, ref DNo, "5", Grd15);
            //SavePLDtl(ref cml, DocNo, ref DNo, "6", Grd16);
            //SavePLDtl(ref cml, DocNo, ref DNo, "7", Grd17);
            //SavePLDtl(ref cml, DocNo, ref DNo, "8", Grd18);
            //SavePLDtl(ref cml, DocNo, ref DNo, "9", Grd19);
            //SavePLDtl(ref cml, DocNo, ref DNo, "10", Grd20);
            //SavePLDtl(ref cml, DocNo, ref DNo, "11", Grd21);
            //SavePLDtl(ref cml, DocNo, ref DNo, "12", Grd22);
            //SavePLDtl(ref cml, DocNo, ref DNo, "13", Grd23);
            //SavePLDtl(ref cml, DocNo, ref DNo, "14", Grd24);
            //SavePLDtl(ref cml, DocNo, ref DNo, "15", Grd25);
            //SavePLDtl(ref cml, DocNo, ref DNo, "16", Grd26);
            //SavePLDtl(ref cml, DocNo, ref DNo, "17", Grd27);
            //SavePLDtl(ref cml, DocNo, ref DNo, "18", Grd28);
            //SavePLDtl(ref cml, DocNo, ref DNo, "19", Grd29);
            //SavePLDtl(ref cml, DocNo, ref DNo, "20", Grd30);
            //SavePLDtl(ref cml, DocNo, ref DNo, "21", Grd31);
            //SavePLDtl(ref cml, DocNo, ref DNo, "22", Grd32);
            //SavePLDtl(ref cml, DocNo, ref DNo, "23", Grd33);
            //SavePLDtl(ref cml, DocNo, ref DNo, "24", Grd34);
            //SavePLDtl(ref cml, DocNo, ref DNo, "25", Grd35);

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            SetProcessInd(Grd11); SetProcessInd(Grd16); SetProcessInd(Grd21); SetProcessInd(Grd26); SetProcessInd(Grd31);
            SetProcessInd(Grd12); SetProcessInd(Grd17); SetProcessInd(Grd22); SetProcessInd(Grd27); SetProcessInd(Grd32);
            SetProcessInd(Grd13); SetProcessInd(Grd18); SetProcessInd(Grd23); SetProcessInd(Grd28); SetProcessInd(Grd33);
            SetProcessInd(Grd14); SetProcessInd(Grd19); SetProcessInd(Grd24); SetProcessInd(Grd29); SetProcessInd(Grd34);
            SetProcessInd(Grd15); SetProcessInd(Grd20); SetProcessInd(Grd25); SetProcessInd(Grd30); SetProcessInd(Grd35);
            CheckStatusPL();

            return
                Sm.IsLueEmpty(LueCtCode, "Customer") ||
                Sm.IsTxtEmpty(TxtSIDocNo, "Shipping Instruction", false) ||
                Sm.IsLueEmpty(LueNotify, "Notify Party") ||
                Sm.IsMeeEmpty(MeeAccount, "Account of Messrs") ||
                IsGrdEmpty() ||
                IsSealEmpty() ||
                IsGrdQtyNotValid() ||
                IsSPCancelled() ||
                IsSPFinal() ||
                IsProcessIndNotValid() ||
                IsProcessDetailNotValid()||
                CheckCreditLimit()
                ;
        }

        private bool IsSealEmpty()
        {
            var mListGrd = new List<iGrid> 
            {
                Grd11, Grd12, Grd13, Grd14, Grd15,
                Grd16, Grd17, Grd18, Grd19, Grd20,
                Grd21, Grd22, Grd23, Grd24, Grd25,
                Grd26, Grd27, Grd28, Grd29, Grd30,
                Grd31, Grd32, Grd33, Grd34, Grd35
            };

            var mListLueSize = new List<LookUpEdit>
            {
                LueSize1, LueSize2, LueSize3, LueSize4, LueSize5, 
                LueSize6, LueSize7, LueSize8, LueSize9, LueSize10, 
                LueSize11, LueSize12, LueSize13, LueSize14, LueSize15, 
                LueSize16, LueSize17, LueSize18, LueSize19, LueSize20, 
                LueSize21, LueSize22, LueSize23, LueSize24, LueSize25
            };

            var mListTxtCnt = new List<TextEdit>
            {
                TxtCnt1, TxtCnt2, TxtCnt3, TxtCnt4, TxtCnt5, 
                TxtCnt6, TxtCnt7, TxtCnt8, TxtCnt9, TxtCnt10, 
                TxtCnt11, TxtCnt12, TxtCnt13, TxtCnt14, TxtCnt15, 
                TxtCnt16, TxtCnt17, TxtCnt18, TxtCnt19, TxtCnt20, 
                TxtCnt21, TxtCnt22, TxtCnt23, TxtCnt24, TxtCnt25 
            };

            var mListTxtSeal = new List<TextEdit>
            {
                TxtSeal1, TxtSeal2, TxtSeal3, TxtSeal4, TxtSeal5, 
                TxtSeal6, TxtSeal7, TxtSeal8, TxtSeal9, TxtSeal10, 
                TxtSeal11, TxtSeal12, TxtSeal13, TxtSeal14, TxtSeal15, 
                TxtSeal16, TxtSeal17, TxtSeal18, TxtSeal19, TxtSeal20, 
                TxtSeal21, TxtSeal22, TxtSeal23, TxtSeal24, TxtSeal25 
            };

            int mIndex = 0; bool mIsEmpty = false;

            mListGrd.ForEach(Grd =>
            {
                if (Grd.Rows.Count > 1)
                {
                    if (Sm.IsLueEmpty(mListLueSize[mIndex], "Size #" + (mIndex + 1))) mIsEmpty = true;
                    if (Sm.IsTxtEmpty(mListTxtCnt[mIndex], "Container #" + (mIndex + 1), false)) mIsEmpty = true;
                    if (Sm.IsTxtEmpty(mListTxtSeal[mIndex], "Seal #" + (mIndex + 1), false)) mIsEmpty = true;
                }
                mIndex += 1;
            });

            mListGrd.Clear();
            mListLueSize.Clear();
            mListTxtCnt.Clear();
            mListTxtSeal.Clear();

            return mIsEmpty;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 Item.");
                return true;
            }
            return false;
        }

        private bool IsSPFinal()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select A.DocNo From TblSP A "+ 
                    "Inner Join TblSIHdr B On A.DocNo = B.SPDocNo " +
                    "Where A.Status = 'F' And B.DocNo=@DocNo "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtSIDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data is already Final.");
                return true;
            }
            return false;
        }

        private bool IsSPCancelled()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select A.DocNo From TblSP A " +
                    "Inner Join TblSIHdr B On A.DocNo = B.SPDocNo " +
                    "Where A.Status = 'C' And B.DocNo=@DocNo "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtSIDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This document already cancelled.");
                return true;
            }
            return false;
        }

        private bool IsGrdQtyNotValid()
        {
            //RecomputeBalance();
            string Msg = "";

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                Msg =
                   "Item's Code : " + Sm.GetGrdStr(Grd1, Row, 1) + Environment.NewLine +
                   "Item's Name : " + Sm.GetGrdStr(Grd1, Row, 2) + Environment.NewLine +
                   "SO : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine +
                   "Item's Local : " + Sm.GetGrdStr(Grd1, Row, 5) + Environment.NewLine +
                   "Packaging Unit : " + Sm.GetGrdStr(Grd1, Row, 8) + Environment.NewLine;

                if (
                    Sm.IsGrdValueEmpty(Grd1, Row, 10, true, Msg + "Requested Quantity (Packaging Unit) should not be 0.") ||
                    Sm.IsGrdValueEmpty(Grd1, Row, 13, true, Msg + "Requested Quantity (Sales) should not be 0.") ||
                    Sm.IsGrdValueEmpty(Grd1, Row, 16, true, Msg + "Requested Quantity (Inventory) should not be 0.")
                    )
                    return true;



                if (Sm.GetGrdDec(Grd1, Row, 11) != 0)
                {
                    Sm.StdMsg(mMsgType.Warning,
                        Msg +
                        "Requested quantity ( " + Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 10), 0) +
                        " ) not balance with outstanding quantity ( " + Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 9), 0) +
                        " ).");
                    return true;
                }

                if (Sm.GetGrdDec(Grd1, Row, 14) !=0)
                {
                    Sm.StdMsg(mMsgType.Warning,
                        Msg +
                        "Requested quantity ( " + Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 13), 0) +
                        " ) not balance with outstanding quantity ( " + Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 12), 0) +
                        " ).");
                    return true;
                }
            }
         
            return false;
        }

        private bool IsProcessIndNotValid()
        {
            for (int Section = 1; Section <= 25; Section++ )
            {
                    var cm = new MySqlCommand()
                    {
                        CommandText =
                            "Select SectionNo From TblDOCt2Hdr " +
                            "Where PLDocNo=@DocNo And SectionNo = '"+Section+"' "
                    };
                    Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

                    string ProcessInd = Sm.GetValue(
                            "Select ProcessInd From TblPLDtl " +
                            "Where DocNo='"+TxtDocNo.Text+"' And SectionNo = '" + Section + "' Limit 1 ");
                    
                
                    if (Sm.IsDataExist(cm) && ProcessInd == "O")
                    {
                        Sm.StdMsg(mMsgType.Warning, "This container has been processed to DO.");
                        return true;
                    }
            }
            return false;
        }    
                
        private MySqlCommand SavePLHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblPLHdr(DocNo, LocalDocNo, DocDt, SIDocNo, Account, InvoiceNo, SalesContractNo, LCNo, LCDt, Issued1, ");
            SQL.AppendLine("Issued2, Issued3, Issued4, ToOrder1, ToOrder2, ToOrder3, ToOrder4, ");
            SQL.AppendLine("Cnt1, Cnt2, Cnt3, Cnt4, Cnt5, ");
            SQL.AppendLine("Cnt6, Cnt7, Cnt8, Cnt9, Cnt10, ");
            SQL.AppendLine("Cnt11, Cnt12, Cnt13, Cnt14, Cnt15, ");
            SQL.AppendLine("Cnt16, Cnt17, Cnt18, Cnt19, Cnt20, ");
            SQL.AppendLine("Cnt21, Cnt22, Cnt23, Cnt24, Cnt25, ");
            SQL.AppendLine("Seal1, Seal2, Seal3, Seal4, Seal5, ");
            SQL.AppendLine("Seal6, Seal7, Seal8, Seal9, Seal10,");
            SQL.AppendLine("Seal11, Seal12, Seal13, Seal14, Seal15, ");
            SQL.AppendLine("Seal16, Seal17, Seal18, Seal19, Seal20,");
            SQL.AppendLine("Seal21, Seal22, Seal23, Seal24, Seal25, ");
            SQL.AppendLine("Size1, Size2, Size3, Size4, Size5, ");
            SQL.AppendLine("Size6, Size7, Size8, Size9, Size10, ");
            SQL.AppendLine("Size11, Size12, Size13, Size14, Size15, ");
            SQL.AppendLine("Size16, Size17, Size18, Size19, Size20, ");
            SQL.AppendLine("Size21, Size22, Size23, Size24, Size25, ");
            SQL.AppendLine("SM1, SM2, SM3, SM4, SM5, ");
            SQL.AppendLine("SM6, SM7, SM8, SM9, SM10,  ");
            SQL.AppendLine("SM11, SM12, SM13, SM14, SM15, ");
            SQL.AppendLine("SM16, SM17, SM18, SM19, SM20,  ");
            SQL.AppendLine("SM21, SM22, SM23, SM24, SM25, ");
            SQL.AppendLine("Remark, Remark2, Remark3, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @LocalDocNo, @DocDt, @SIDocNo, @Account, @InvoiceNo, @SalesContractNo, @LCNo, @LCDt, @Issued1, ");
            SQL.AppendLine("@Issued2, @Issued3, @Issued4, @ToOrder1, @ToOrder2, @ToOrder3, @ToOrder4, ");
            SQL.AppendLine("@Cnt1, @Cnt2, @Cnt3, @Cnt4, @Cnt5, ");
            SQL.AppendLine("@Cnt6, @Cnt7, @Cnt8, @Cnt9, @Cnt10,");
            SQL.AppendLine("@Cnt11, @Cnt12, @Cnt13, @Cnt14, @Cnt15, ");
            SQL.AppendLine("@Cnt16, @Cnt17, @Cnt18, @Cnt19, @Cnt20,");
            SQL.AppendLine("@Cnt21, @Cnt22, @Cnt23, @Cnt24, @Cnt25, ");
            SQL.AppendLine("@Seal1, @Seal2, @Seal3, @Seal4, @Seal5, ");
            SQL.AppendLine("@Seal6, @Seal7, @Seal8, @Seal9, @Seal10, ");
            SQL.AppendLine("@Seal11, @Seal12, @Seal13, @Seal14, @Seal15, ");
            SQL.AppendLine("@Seal16, @Seal17, @Seal18, @Seal19, @Seal20, ");
            SQL.AppendLine("@Seal21, @Seal22, @Seal23, @Seal24, @Seal25, ");
            SQL.AppendLine("@Size1, @Size2, @Size3, @Size4, @Size5, ");
            SQL.AppendLine("@Size6, @Size7, @Size8, @Size9, @Size10, ");
            SQL.AppendLine("@Size11, @Size12, @Size13, @Size14, @Size15, ");
            SQL.AppendLine("@Size16, @Size17, @Size18, @Size19, @Size20, ");
            SQL.AppendLine("@Size21, @Size22, @Size23, @Size24, @Size25, ");
            SQL.AppendLine("@SM1, @SM2, @SM3, @SM4, @SM5, ");
            SQL.AppendLine("@SM6, @SM7, @SM8, @SM9, @SM10,  ");
            SQL.AppendLine("@SM11, @SM12, @SM13, @SM14, @SM15, ");
            SQL.AppendLine("@SM16, @SM17, @SM18, @SM19, @SM20,  ");
            SQL.AppendLine("@SM21, @SM22, @SM23, @SM24, @SM25, ");
            SQL.AppendLine("@Remark, @Remark2, @Remark3, @UserCode, CurrentDateTime()) ");
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("    Update DocDt=@DocDt, LocalDocNo=@LocalDocNo, Account=@Account, InvoiceNo=@InvoiceNo, SalesContractNo=@SalesContractNo, LCNo=@LCNo, LCDt=@LCDt, Issued1=@Issued1, ");
            SQL.AppendLine("    Issued2=@Issued2, Issued3=@Issued3, Issued4=@Issued4, ToOrder1=@ToOrder1, ToOrder2=@ToOrder2, ToOrder3=@ToOrder3, ToOrder4=@ToOrder4, ");
            SQL.AppendLine("    Cnt1=@Cnt1, Cnt2=@Cnt2, Cnt3=@Cnt3, Cnt4=@Cnt4, Cnt5=@Cnt5, ");
            SQL.AppendLine("    Cnt6=@Cnt6, Cnt7=@Cnt7, Cnt8=@Cnt8, Cnt9=@Cnt9, Cnt10=@Cnt10, ");
            SQL.AppendLine("    Cnt11=@Cnt11, Cnt12=@Cnt12, Cnt13=@Cnt13, Cnt14=@Cnt14, Cnt15=@Cnt15, ");
            SQL.AppendLine("    Cnt16=@Cnt16, Cnt17=@Cnt17, Cnt18=@Cnt18, Cnt19=@Cnt19, Cnt20=@Cnt20, ");
            SQL.AppendLine("    Cnt21=@Cnt21, Cnt22=@Cnt22, Cnt23=@Cnt23, Cnt24=@Cnt24, Cnt25=@Cnt25, ");
            SQL.AppendLine("    Seal1=@Seal1, Seal2=@Seal2, Seal3=@Seal3, Seal4=@Seal4, Seal5=@Seal5, ");
            SQL.AppendLine("    Seal6=@Seal6, Seal7=@Seal7, Seal8=@Seal8, Seal9=@Seal9, Seal10=@Seal10, ");
            SQL.AppendLine("    Seal11=@Seal11, Seal12=@Seal12, Seal13=@Seal13, Seal14=@Seal14, Seal15=@Seal15, ");
            SQL.AppendLine("    Seal16=@Seal16, Seal17=@Seal17, Seal18=@Seal18, Seal19=@Seal19, Seal20=@Seal20, ");
            SQL.AppendLine("    Seal21=@Seal21, Seal22=@Seal22, Seal23=@Seal23, Seal24=@Seal24, Seal25=@Seal25, ");
            SQL.AppendLine("    Size1=@Size1, Size2=@Size2, Size3=@Size3, Size4=@Size4, Size5=@Size5, ");
            SQL.AppendLine("    Size6=@Size6, Size7=@Size7, Size8=@Size8, Size9=@Size9, Size10=@Size10, ");
            SQL.AppendLine("    Size11=@Size11, Size12=@Size12, Size13=@Size13, Size14=@Size14, Size15=@Size15, ");
            SQL.AppendLine("    Size16=@Size16, Size17=@Size17, Size18=@Size18, Size19=@Size19, Size20=@Size20, ");
            SQL.AppendLine("    Size21=@Size21, Size22=@Size22, Size23=@Size23, Size24=@Size24, Size25=@Size25, ");
            SQL.AppendLine("    SM1=@SM1, SM2=@SM2, SM3=@SM3, SM4=@SM4, SM5=@SM5, ");
            SQL.AppendLine("    SM6=@SM6, SM7=@SM7, SM8=@SM8, SM9=@SM9, SM10=@SM10,  ");
            SQL.AppendLine("    SM11=@SM11, SM12=@SM12, SM13=@SM13, SM14=@SM14, SM15=@SM15, ");
            SQL.AppendLine("    SM16=@SM16, SM17=@SM17, SM18=@SM18, SM19=@SM19, SM20=@SM20,  ");
            SQL.AppendLine("    SM21=@SM21, SM22=@SM22, SM23=@SM23, SM24=@SM24, SM25=@SM25, ");
            SQL.AppendLine("    Remark=@Remark, Remark2=@Remark2, Remark3=@Remark3, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");
            SQL.AppendLine("Delete From TblPLDtl Where DocNo = @DocNo ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@LocalDocNo", TxtLocalDocNo.Text);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@SIDocNo", TxtSIDocNo.Text);
            Sm.CmParam<String>(ref cm, "@Account", MeeAccount.Text);
            Sm.CmParam<String>(ref cm, "@InvoiceNo", TxtInvoice.Text);
            Sm.CmParam<String>(ref cm, "@SalesContractNo", TxtSalesContract.Text);
            Sm.CmParam<String>(ref cm, "@LCNo", TxtLcNo.Text);
            Sm.CmParamDt(ref cm, "@LCDt", Sm.GetDte(LCDocDt));
            Sm.CmParam<String>(ref cm, "@Issued1", MeeIssued.Text);
            Sm.CmParam<String>(ref cm, "@Issued2", MeeIssued2.Text);
            Sm.CmParam<String>(ref cm, "@Issued3", MeeIssued3.Text);
            Sm.CmParam<String>(ref cm, "@Issued4", MeeIssued4.Text);
            Sm.CmParam<String>(ref cm, "@ToOrder1", MeeOrder.Text);
            Sm.CmParam<String>(ref cm, "@ToOrder2", MeeOrder2.Text);
            Sm.CmParam<String>(ref cm, "@ToOrder3", MeeOrder3.Text);
            Sm.CmParam<String>(ref cm, "@ToOrder4", MeeOrder4.Text);
            Sm.CmParam<String>(ref cm, "@Cnt1", TxtCnt1.Text);
            Sm.CmParam<String>(ref cm, "@Cnt2", TxtCnt2.Text);
            Sm.CmParam<String>(ref cm, "@Cnt3", TxtCnt3.Text);
            Sm.CmParam<String>(ref cm, "@Cnt4", TxtCnt4.Text);
            Sm.CmParam<String>(ref cm, "@Cnt5", TxtCnt5.Text);
            Sm.CmParam<String>(ref cm, "@Cnt6", TxtCnt6.Text);
            Sm.CmParam<String>(ref cm, "@Cnt7", TxtCnt7.Text);
            Sm.CmParam<String>(ref cm, "@Cnt8", TxtCnt8.Text);
            Sm.CmParam<String>(ref cm, "@Cnt9", TxtCnt9.Text);
            Sm.CmParam<String>(ref cm, "@Cnt10", TxtCnt10.Text);
            Sm.CmParam<String>(ref cm, "@Cnt11", TxtCnt11.Text);
            Sm.CmParam<String>(ref cm, "@Cnt12", TxtCnt12.Text);
            Sm.CmParam<String>(ref cm, "@Cnt13", TxtCnt13.Text);
            Sm.CmParam<String>(ref cm, "@Cnt14", TxtCnt14.Text);
            Sm.CmParam<String>(ref cm, "@Cnt15", TxtCnt15.Text);
            Sm.CmParam<String>(ref cm, "@Cnt16", TxtCnt16.Text);
            Sm.CmParam<String>(ref cm, "@Cnt17", TxtCnt17.Text);
            Sm.CmParam<String>(ref cm, "@Cnt18", TxtCnt18.Text);
            Sm.CmParam<String>(ref cm, "@Cnt19", TxtCnt19.Text);
            Sm.CmParam<String>(ref cm, "@Cnt20", TxtCnt20.Text);
            Sm.CmParam<String>(ref cm, "@Cnt21", TxtCnt21.Text);
            Sm.CmParam<String>(ref cm, "@Cnt22", TxtCnt22.Text);
            Sm.CmParam<String>(ref cm, "@Cnt23", TxtCnt23.Text);
            Sm.CmParam<String>(ref cm, "@Cnt24", TxtCnt24.Text);
            Sm.CmParam<String>(ref cm, "@Cnt25", TxtCnt25.Text);
            Sm.CmParam<String>(ref cm, "@Seal1", TxtSeal1.Text);
            Sm.CmParam<String>(ref cm, "@Seal2", TxtSeal2.Text);
            Sm.CmParam<String>(ref cm, "@Seal3", TxtSeal3.Text);
            Sm.CmParam<String>(ref cm, "@Seal4", TxtSeal4.Text);
            Sm.CmParam<String>(ref cm, "@Seal5", TxtSeal5.Text);
            Sm.CmParam<String>(ref cm, "@Seal6", TxtSeal6.Text);
            Sm.CmParam<String>(ref cm, "@Seal7", TxtSeal7.Text);
            Sm.CmParam<String>(ref cm, "@Seal8", TxtSeal8.Text);
            Sm.CmParam<String>(ref cm, "@Seal9", TxtSeal9.Text);
            Sm.CmParam<String>(ref cm, "@Seal10", TxtSeal10.Text);
            Sm.CmParam<String>(ref cm, "@Seal11", TxtSeal11.Text);
            Sm.CmParam<String>(ref cm, "@Seal12", TxtSeal12.Text);
            Sm.CmParam<String>(ref cm, "@Seal13", TxtSeal13.Text);
            Sm.CmParam<String>(ref cm, "@Seal14", TxtSeal14.Text);
            Sm.CmParam<String>(ref cm, "@Seal15", TxtSeal15.Text);
            Sm.CmParam<String>(ref cm, "@Seal16", TxtSeal16.Text);
            Sm.CmParam<String>(ref cm, "@Seal17", TxtSeal17.Text);
            Sm.CmParam<String>(ref cm, "@Seal18", TxtSeal18.Text);
            Sm.CmParam<String>(ref cm, "@Seal19", TxtSeal19.Text);
            Sm.CmParam<String>(ref cm, "@Seal20", TxtSeal20.Text);
            Sm.CmParam<String>(ref cm, "@Seal21", TxtSeal21.Text);
            Sm.CmParam<String>(ref cm, "@Seal22", TxtSeal22.Text);
            Sm.CmParam<String>(ref cm, "@Seal23", TxtSeal23.Text);
            Sm.CmParam<String>(ref cm, "@Seal24", TxtSeal24.Text);
            Sm.CmParam<String>(ref cm, "@Seal25", TxtSeal25.Text);
            Sm.CmParam<String>(ref cm, "@Size1", Sm.GetLue(LueSize1));
            Sm.CmParam<String>(ref cm, "@Size2", Sm.GetLue(LueSize2));
            Sm.CmParam<String>(ref cm, "@Size3", Sm.GetLue(LueSize3));
            Sm.CmParam<String>(ref cm, "@Size4", Sm.GetLue(LueSize4));
            Sm.CmParam<String>(ref cm, "@Size5", Sm.GetLue(LueSize5));
            Sm.CmParam<String>(ref cm, "@Size6", Sm.GetLue(LueSize6));
            Sm.CmParam<String>(ref cm, "@Size7", Sm.GetLue(LueSize7));
            Sm.CmParam<String>(ref cm, "@Size8", Sm.GetLue(LueSize8));
            Sm.CmParam<String>(ref cm, "@Size9", Sm.GetLue(LueSize9));
            Sm.CmParam<String>(ref cm, "@Size10", Sm.GetLue(LueSize10));
            Sm.CmParam<String>(ref cm, "@Size11", Sm.GetLue(LueSize11));
            Sm.CmParam<String>(ref cm, "@Size12", Sm.GetLue(LueSize12));
            Sm.CmParam<String>(ref cm, "@Size13", Sm.GetLue(LueSize13));
            Sm.CmParam<String>(ref cm, "@Size14", Sm.GetLue(LueSize14));
            Sm.CmParam<String>(ref cm, "@Size15", Sm.GetLue(LueSize15));
            Sm.CmParam<String>(ref cm, "@Size16", Sm.GetLue(LueSize16));
            Sm.CmParam<String>(ref cm, "@Size17", Sm.GetLue(LueSize17));
            Sm.CmParam<String>(ref cm, "@Size18", Sm.GetLue(LueSize18));
            Sm.CmParam<String>(ref cm, "@Size19", Sm.GetLue(LueSize19));
            Sm.CmParam<String>(ref cm, "@Size20", Sm.GetLue(LueSize20));
            Sm.CmParam<String>(ref cm, "@Size21", Sm.GetLue(LueSize21));
            Sm.CmParam<String>(ref cm, "@Size22", Sm.GetLue(LueSize22));
            Sm.CmParam<String>(ref cm, "@Size23", Sm.GetLue(LueSize23));
            Sm.CmParam<String>(ref cm, "@Size24", Sm.GetLue(LueSize24));
            Sm.CmParam<String>(ref cm, "@Size25", Sm.GetLue(LueSize25));

            Sm.CmParam<String>(ref cm, "@SM1", MeeSM1.Text);
            Sm.CmParam<String>(ref cm, "@SM2", MeeSM2.Text);
            Sm.CmParam<String>(ref cm, "@SM3", MeeSM3.Text);
            Sm.CmParam<String>(ref cm, "@SM4", MeeSM4.Text);
            Sm.CmParam<String>(ref cm, "@SM5", MeeSM5.Text);
            Sm.CmParam<String>(ref cm, "@SM6", MeeSM6.Text);
            Sm.CmParam<String>(ref cm, "@SM7", MeeSM7.Text);
            Sm.CmParam<String>(ref cm, "@SM8", MeeSM8.Text);
            Sm.CmParam<String>(ref cm, "@SM9", MeeSM9.Text);
            Sm.CmParam<String>(ref cm, "@SM10", MeeSM10.Text);
            Sm.CmParam<String>(ref cm, "@SM11", MeeSM11.Text);
            Sm.CmParam<String>(ref cm, "@SM12", MeeSM12.Text);
            Sm.CmParam<String>(ref cm, "@SM13", MeeSM13.Text);
            Sm.CmParam<String>(ref cm, "@SM14", MeeSM14.Text);
            Sm.CmParam<String>(ref cm, "@SM15", MeeSM15.Text);
            Sm.CmParam<String>(ref cm, "@SM16", MeeSM16.Text);
            Sm.CmParam<String>(ref cm, "@SM17", MeeSM17.Text);
            Sm.CmParam<String>(ref cm, "@SM18", MeeSM18.Text);
            Sm.CmParam<String>(ref cm, "@SM19", MeeSM19.Text);
            Sm.CmParam<String>(ref cm, "@SM20", MeeSM20.Text);
            Sm.CmParam<String>(ref cm, "@SM21", MeeSM21.Text);
            Sm.CmParam<String>(ref cm, "@SM22", MeeSM22.Text);
            Sm.CmParam<String>(ref cm, "@SM23", MeeSM23.Text);
            Sm.CmParam<String>(ref cm, "@SM24", MeeSM24.Text);
            Sm.CmParam<String>(ref cm, "@SM25", MeeSM25.Text);

            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@Remark2", MeeRemark2.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SavePLDtl(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirstOrExisted = true;
            int DNo = 0;

            SavePLDtl(ref SQL, ref cm, DocNo, ref DNo, "1", Grd11, ref IsFirstOrExisted);
            SavePLDtl(ref SQL, ref cm, DocNo, ref DNo, "2", Grd12, ref IsFirstOrExisted);
            SavePLDtl(ref SQL, ref cm, DocNo, ref DNo, "3", Grd13, ref IsFirstOrExisted);
            SavePLDtl(ref SQL, ref cm, DocNo, ref DNo, "4", Grd14, ref IsFirstOrExisted);
            SavePLDtl(ref SQL, ref cm, DocNo, ref DNo, "5", Grd15, ref IsFirstOrExisted);
            SavePLDtl(ref SQL, ref cm, DocNo, ref DNo, "6", Grd16, ref IsFirstOrExisted);
            SavePLDtl(ref SQL, ref cm, DocNo, ref DNo, "7", Grd17, ref IsFirstOrExisted);
            SavePLDtl(ref SQL, ref cm, DocNo, ref DNo, "8", Grd18, ref IsFirstOrExisted);
            SavePLDtl(ref SQL, ref cm, DocNo, ref DNo, "9", Grd19, ref IsFirstOrExisted);
            SavePLDtl(ref SQL, ref cm, DocNo, ref DNo, "10", Grd20, ref IsFirstOrExisted);
            SavePLDtl(ref SQL, ref cm, DocNo, ref DNo, "11", Grd21, ref IsFirstOrExisted);
            SavePLDtl(ref SQL, ref cm, DocNo, ref DNo, "12", Grd22, ref IsFirstOrExisted);
            SavePLDtl(ref SQL, ref cm, DocNo, ref DNo, "13", Grd23, ref IsFirstOrExisted);
            SavePLDtl(ref SQL, ref cm, DocNo, ref DNo, "14", Grd24, ref IsFirstOrExisted);
            SavePLDtl(ref SQL, ref cm, DocNo, ref DNo, "15", Grd25, ref IsFirstOrExisted);
            SavePLDtl(ref SQL, ref cm, DocNo, ref DNo, "16", Grd26, ref IsFirstOrExisted);
            SavePLDtl(ref SQL, ref cm, DocNo, ref DNo, "17", Grd27, ref IsFirstOrExisted);
            SavePLDtl(ref SQL, ref cm, DocNo, ref DNo, "18", Grd28, ref IsFirstOrExisted);
            SavePLDtl(ref SQL, ref cm, DocNo, ref DNo, "19", Grd29, ref IsFirstOrExisted);
            SavePLDtl(ref SQL, ref cm, DocNo, ref DNo, "20", Grd30, ref IsFirstOrExisted);
            SavePLDtl(ref SQL, ref cm, DocNo, ref DNo, "21", Grd31, ref IsFirstOrExisted);
            SavePLDtl(ref SQL, ref cm, DocNo, ref DNo, "22", Grd32, ref IsFirstOrExisted);
            SavePLDtl(ref SQL, ref cm, DocNo, ref DNo, "23", Grd33, ref IsFirstOrExisted);
            SavePLDtl(ref SQL, ref cm, DocNo, ref DNo, "24", Grd34, ref IsFirstOrExisted);
            SavePLDtl(ref SQL, ref cm, DocNo, ref DNo, "25", Grd35, ref IsFirstOrExisted);

            cm.CommandText =
                IsFirstOrExisted?string.Empty:
                "/* Packing List (Dtl) */ " +
                "Set @Dt:=CurrentDateTime(); " +
                "Insert Into TblPLDtl (DocNo, DNo, ProcessInd, SectionNo, ItCode, SODocNo, SODNo, PLNo, QtyPL, QtyPackagingUnit, Qty, QtyInventory, TotalVolume, Remark, CreateBy, CreateDt) " +
                "Values " + SQL.ToString() + ";";

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private void SavePLDtl(
            ref StringBuilder SQL, 
            ref MySqlCommand cm, 
            string DocNo, 
            ref int DNo, 
            string SectionNo, 
            iGrid Grd, 
            ref bool IsFirstOrExisted)
        {
            for (int r = 0; r < Grd.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd, r, 0).Length > 0 && Sm.GetGrdStr(Grd, r, 1).Length > 0)
                {
                    DNo++;
                    if (IsFirstOrExisted)
                        IsFirstOrExisted = false;
                    else
                        SQL.AppendLine(", ");
                    SQL.AppendLine(
                        " (@DocNo, @DNo_" + SectionNo + "_" + r.ToString() +
                        ", @ProcessInd_" + SectionNo + "_" + r.ToString() +
                        ", @SectionNo_" + SectionNo + "_" + r.ToString() +
                        ", @ItCode_" + SectionNo + "_" + r.ToString() +
                        ", @SODocNo_" + SectionNo + "_" + r.ToString() +
                        ", @SODNo_" + SectionNo + "_" + r.ToString() +
                        ", @PLNo_" + SectionNo + "_" + r.ToString() +
                        ", @QtyPL_" + SectionNo + "_" + r.ToString() +
                        ", @QtyPackagingUnit_" + SectionNo + "_" + r.ToString() +
                        ", @Qty_" + SectionNo + "_" + r.ToString() +
                        ", @QtyInventory_" + SectionNo + "_" + r.ToString() +
                        ", @TotalVolume_" + SectionNo + "_" + r.ToString() +
                        ", @Remark_" + SectionNo + "_" + r.ToString() +
                        ", @UserCode, @Dt) ");

                    Sm.CmParam<String>(ref cm, "@DNo_" + SectionNo + "_" + r.ToString(), Sm.Right("000" + DNo.ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@ProcessInd_" + SectionNo + "_" + r.ToString(), Sm.GetGrdStr(Grd, r, 18));
                    Sm.CmParam<String>(ref cm, "@SectionNo_" + SectionNo + "_" + r.ToString(), SectionNo);
                    Sm.CmParam<String>(ref cm, "@ItCode_" + SectionNo + "_" + r.ToString(), Sm.GetGrdStr(Grd, r, 0));
                    Sm.CmParam<String>(ref cm, "@SODocNo_" + SectionNo + "_" + r.ToString(), Sm.GetGrdStr(Grd, r, 4));
                    Sm.CmParam<String>(ref cm, "@SODno_" + SectionNo + "_" + r.ToString(), Sm.GetGrdStr(Grd, r, 6));
                    Sm.CmParam<String>(ref cm, "@PLNo_" + SectionNo + "_" + r.ToString(), Sm.GetGrdStr(Grd, r, 7));
                    Sm.CmParam<Decimal>(ref cm, "@QtyPL_" + SectionNo + "_" + r.ToString(), Sm.GetGrdDec(Grd, r, 8));
                    Sm.CmParam<Decimal>(ref cm, "@QtyPackagingUnit_" + SectionNo + "_" + r.ToString(), Sm.GetGrdDec(Grd, r, 10));
                    Sm.CmParam<Decimal>(ref cm, "@Qty_" + SectionNo + "_" + r.ToString(), Sm.GetGrdDec(Grd, r, 11));
                    Sm.CmParam<Decimal>(ref cm, "@QtyInventory_" + SectionNo + "_" + r.ToString(), Sm.GetGrdDec(Grd, r, 13));
                    Sm.CmParam<Decimal>(ref cm, "@TotalVolume_" + SectionNo + "_" + r.ToString(), Sm.GetGrdDec(Grd, r, 22));
                    Sm.CmParam<String>(ref cm, "@Remark_" + SectionNo + "_" + r.ToString(), Sm.GetGrdStr(Grd, r, 17));
                }
            }
        }


        //private void SavePLDtl(ref List<MySqlCommand> cml, string DocNo, ref int DNo, string SectionNo, iGrid Grd)
        //{
        //    for (int Row = 0; Row < Grd.Rows.Count; Row++)
        //        if (Sm.GetGrdStr(Grd, Row, 0).Length > 0 && Sm.GetGrdStr(Grd, Row, 1).Length > 0)
        //            cml.Add(SavePLDtl(DocNo, ref DNo, SectionNo, Grd, Row));
        //}

        //private MySqlCommand SavePLDtl(string DocNo, ref int DNo, string SectionNo, iGrid Grd, int Row)
        //{
        //    var SQL = new StringBuilder();

        //    DNo += 1;

        //    SQL.AppendLine("Insert Into TblPLDtl(DocNo, DNo, ProcessInd, SectionNo, ItCode, SODocNo, SODNo, PLNo, QtyPL, QtyPackagingUnit, Qty, QtyInventory, TotalVolume, Remark, CreateBy, CreateDt) ");
        //    SQL.AppendLine("Values(@DocNo, @DNo, @ProcessInd, @SectionNo, @ItCode, @SODocNo, @SODNo, @PLNo, @QtyPL, @QtyPackagingUnit, @Qty, @QtyInventory, @TotalVolume, @Remark, @CreateBy, CurrentDateTime()); ");

        //    var cm = new MySqlCommand() { CommandText = SQL.ToString() };
        //    Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
        //    Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + DNo.ToString(), 3));
        //    Sm.CmParam<String>(ref cm, "@ProcessInd", Sm.GetGrdStr(Grd, Row, 18));
        //    Sm.CmParam<String>(ref cm, "@SectionNo", SectionNo);
        //    Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd, Row, 0));
        //    Sm.CmParam<String>(ref cm, "@SODocNo", Sm.GetGrdStr(Grd, Row, 4));
        //    Sm.CmParam<String>(ref cm, "@SODno", Sm.GetGrdStr(Grd, Row, 6));
        //    Sm.CmParam<String>(ref cm, "@PLNo", Sm.GetGrdStr(Grd, Row, 7));
        //    Sm.CmParam<Decimal>(ref cm, "@QtyPL", Sm.GetGrdDec(Grd, Row, 8));
        //    Sm.CmParam<Decimal>(ref cm, "@QtyPackagingUnit", Sm.GetGrdDec(Grd, Row, 10));
        //    Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd, Row, 11));
        //    Sm.CmParam<Decimal>(ref cm, "@QtyInventory", Sm.GetGrdDec(Grd, Row, 13));
        //    Sm.CmParam<Decimal>(ref cm, "@TotalVolume", Sm.GetGrdDec(Grd, Row, 22));
        //    Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd, Row, 17));
        //    Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

        //    return cm;
        //}

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();
                ShowPLHdr(DocNo, 0);
                ShowPLDtl(DocNo, "1", ref Grd11, TxtTotal1);
                ShowPLDtl(DocNo, "2", ref Grd12, TxtTotal2);
                ShowPLDtl(DocNo, "3", ref Grd13, TxtTotal3);
                ShowPLDtl(DocNo, "4", ref Grd14, TxtTotal4);
                ShowPLDtl(DocNo, "5", ref Grd15, TxtTotal5);
                ShowPLDtl(DocNo, "6", ref Grd16, TxtTotal6);
                ShowPLDtl(DocNo, "7", ref Grd17, TxtTotal7);
                ShowPLDtl(DocNo, "8", ref Grd18, TxtTotal8);
                ShowPLDtl(DocNo, "9", ref Grd19, TxtTotal9);
                ShowPLDtl(DocNo, "10", ref Grd20, TxtTotal10);
                ShowPLDtl(DocNo, "11", ref Grd21, TxtTotal11);
                ShowPLDtl(DocNo, "12", ref Grd22, TxtTotal12);
                ShowPLDtl(DocNo, "13", ref Grd23, TxtTotal13);
                ShowPLDtl(DocNo, "14", ref Grd24, TxtTotal14);
                ShowPLDtl(DocNo, "15", ref Grd25, TxtTotal15);
                ShowPLDtl(DocNo, "16", ref Grd26, TxtTotal16);
                ShowPLDtl(DocNo, "17", ref Grd27, TxtTotal17);
                ShowPLDtl(DocNo, "18", ref Grd28, TxtTotal18);
                ShowPLDtl(DocNo, "19", ref Grd29, TxtTotal19);
                ShowPLDtl(DocNo, "20", ref Grd30, TxtTotal20);
                ShowPLDtl(DocNo, "21", ref Grd31, TxtTotal21);
                ShowPLDtl(DocNo, "22", ref Grd32, TxtTotal22);
                ShowPLDtl(DocNo, "23", ref Grd33, TxtTotal23);
                ShowPLDtl(DocNo, "24", ref Grd34, TxtTotal24);
                ShowPLDtl(DocNo, "25", ref Grd35, TxtTotal25);
                ShowSOData3(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        public void ShowPLHdr(string DocNo, int Con)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select A.DocNo, A.LocalDocNo, A.DocDt,  C.CtCode, A.SiDOcNo, B.SpCtNotifyParty, A.Account, A.InvoiceNo, A.SalesContractNo, A.LCNo, " +
                    "A.LcDt, A.Issued1, A.Issued2, A.Issued3, A.Issued4, "+
                    "A.ToOrder1, A.ToOrder2, A.ToOrder3, A.ToOrder4, "+
                    "A.Cnt1, A.Cnt2, A.Cnt3, A.Cnt4, A.Cnt5, "+
                    "A.Cnt6, A.Cnt7, A.Cnt8, A.Cnt9, A.Cnt10, " +
                    "A.Cnt11, A.Cnt12, A.Cnt13, A.Cnt14, A.Cnt15, "+
                    "A.Cnt16, A.Cnt17, A.Cnt18, A.Cnt19, A.Cnt20, " +
                    "A.Cnt21, A.Cnt22, A.Cnt23, A.Cnt24, A.Cnt25, " +
                    "A.Seal1, A.Seal2, A.Seal3, A.Seal4, A.Seal5, " +
                    "A.Seal6, A.Seal7, A.Seal8, A.Seal9, A.Seal10, "+
                    "A.Seal11, A.Seal12, A.Seal13, A.Seal14, A.Seal15, "+
                    "A.Seal16, A.Seal17, A.Seal18, A.Seal19, A.Seal20, " +
                    "A.Seal21, A.Seal22, A.Seal23, A.Seal24, A.Seal25, " +
                    "A.Size1, A.Size2, A.Size3, A.Size4, A.Size5, "+
                    "A.Size6, A.Size7, A.Size8, A.Size9, A.Size10, "+
                    "A.Size11, A.Size12, A.Size13, A.Size14, A.Size15, " +
                    "A.Size16, A.Size17, A.Size18, A.Size19, A.Size20, " +
                    "A.Size21, A.Size22, A.Size23, A.Size24, A.Size25, " +
                    "A.SM1, A.SM2, A.SM3, A.SM4, A.SM5, " +
                    "A.SM6, A.SM7, A.SM8, A.SM9, A.SM10, " +
                    "A.SM11, A.SM12, A.SM13, A.SM14, A.SM15, " +
                    "A.SM16, A.SM17, A.SM18, A.SM19, A.SM20, " +
                    "A.SM21, A.SM22, A.SM23, A.SM24, A.SM25, " +
                    "A.remark, A.remark2  " +
                    "From TblPLHdr A "+
                    "Inner Join TblSIhdr B On A.SIDocNo = B.DocNo "+
                    "Inner Join TblSP C On B.SpDocNo = C.DocNo " +
                    "Where A.DocNo=@DocNo",
                    new string[] 
                    { 
                        //0
                        "DocNo", 
                        //1-5
                        "LocalDocNo", "DocDt", "CtCode", "SIDOcNo", "SpCtNotifyParty",    
                        //6-10
                        "Account","InvoiceNo", "SalesContractNo", "LCNo", "LcDt",   
                        //11-15
                        "Issued1","Issued2", "Issued3", "Issued4", "ToOrder1",    
                        //16-20
                        "ToOrder2","ToOrder3","ToOrder4", "Cnt1","Cnt2",  
                        //21-25
                        "Cnt3","Cnt4","Cnt5","Cnt6", "Cnt7",
                        //26-30
                        "Cnt8","Cnt9","Cnt10","Cnt11", "Cnt12",
                        //31-35
                        "Cnt13","Cnt14","Cnt15","Cnt16", "Cnt17",
                        //35-40
                        "Cnt18","Cnt19","Cnt20","Cnt21", "Cnt22", 
                        //41-45
                        "Cnt23","Cnt24","Cnt25","Seal1", "Seal2",
                        //46-50
                        "Seal3", "Seal4","Seal5","Seal6", "Seal7",
                        //51-55
                        "Seal8", "Seal9","Seal10","Seal11", "Seal12",
                        //56-60
                        "Seal13", "Seal14", "Seal15","Seal16", "Seal17",
                        //61-65
                        "Seal18", "Seal19","Seal20","Seal21", "Seal22",
                        //66-70
                        "Seal23", "Seal24", "Seal25","Size1", "Size2",
                        //71-75
                        "Size3",  "Size4", "Size5","Size6", "Size7",
                        //76-80
                        "Size8", "Size9", "Size10","Size11", "Size12",
                        //81-85
                        "Size13", "Size14", "Size15","Size16", "Size17",
                        //86-90
                        "Size18", "Size19", "Size20","Size21", "Size22",
                        //91-95
                        "Size23", "Size24", "Size25","SM1", "SM2",
                        //96-100
                        "SM3",  "SM4", "SM5","SM6", "SM7",
                        //101-105
                        "SM8", "SM9", "SM10","SM11", "SM12",
                        //106-110
                        "SM13", "SM14", "SM15","SM16", "SM17",
                        //111-115
                        "SM18", "SM19", "SM20","SM21", "SM22",
                        //116-120
                        "SM23", "SM24", "SM25","Remark", "Remark2",
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        if (Con == 0)
                        {
                            TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                            Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[2]));
                            TxtSIDocNo.EditValue = Sm.DrStr(dr, c[4]);
                            TxtInvoice.EditValue = Sm.DrStr(dr, c[7]);
                        }
                        else
                        {
                            TxtSource.EditValue = Sm.DrStr(dr, c[0]);
                        }
                        TxtLocalDocNo.EditValue = Sm.DrStr(dr, c[1]);
                        SetLueCtCode(ref LueCtCode, Sm.DrStr(dr, c[3]));
                        SetLueNotify(ref LueNotify, Sm.GetLue(LueCtCode), Sm.DrStr(dr, c[5]));
                        Sm.SetLue(LueNotify, Sm.DrStr(dr, c[5]));
                        MeeAccount.EditValue = Sm.DrStr(dr, c[6]);
                        TxtSalesContract.EditValue = Sm.DrStr(dr, c[8]);
                        TxtLcNo.EditValue = Sm.DrStr(dr, c[9]);
                        Sm.SetDte(LCDocDt, Sm.DrStr(dr, c[10]));
                        MeeIssued.EditValue = Sm.DrStr(dr, c[11]);
                        MeeIssued2.EditValue = Sm.DrStr(dr, c[12]);
                        MeeIssued3.EditValue = Sm.DrStr(dr, c[13]);
                        MeeIssued4.EditValue = Sm.DrStr(dr, c[14]);
                        MeeOrder.EditValue = Sm.DrStr(dr, c[15]);
                        MeeOrder2.EditValue = Sm.DrStr(dr, c[16]);
                        MeeOrder3.EditValue = Sm.DrStr(dr, c[17]);
                        MeeOrder4.EditValue = Sm.DrStr(dr, c[18]);
                        TxtCnt1.EditValue = Sm.DrStr(dr, c[19]);
                        TxtCnt2.EditValue = Sm.DrStr(dr, c[20]);
                        TxtCnt3.EditValue = Sm.DrStr(dr, c[21]);
                        TxtCnt4.EditValue = Sm.DrStr(dr, c[22]);
                        TxtCnt5.EditValue = Sm.DrStr(dr, c[23]);
                        TxtCnt6.EditValue = Sm.DrStr(dr, c[24]);
                        TxtCnt7.EditValue = Sm.DrStr(dr, c[25]);
                        TxtCnt8.EditValue = Sm.DrStr(dr, c[26]);
                        TxtCnt9.EditValue = Sm.DrStr(dr, c[27]);
                        TxtCnt10.EditValue = Sm.DrStr(dr, c[28]);
                        TxtCnt11.EditValue = Sm.DrStr(dr, c[29]);
                        TxtCnt12.EditValue = Sm.DrStr(dr, c[30]);
                        TxtCnt13.EditValue = Sm.DrStr(dr, c[31]);
                        TxtCnt14.EditValue = Sm.DrStr(dr, c[32]);
                        TxtCnt15.EditValue = Sm.DrStr(dr, c[33]);
                        TxtCnt16.EditValue = Sm.DrStr(dr, c[34]);
                        TxtCnt17.EditValue = Sm.DrStr(dr, c[35]);
                        TxtCnt18.EditValue = Sm.DrStr(dr, c[36]);
                        TxtCnt19.EditValue = Sm.DrStr(dr, c[37]);
                        TxtCnt20.EditValue = Sm.DrStr(dr, c[38]);
                        TxtCnt21.EditValue = Sm.DrStr(dr, c[39]);
                        TxtCnt22.EditValue = Sm.DrStr(dr, c[40]);
                        TxtCnt23.EditValue = Sm.DrStr(dr, c[41]);
                        TxtCnt24.EditValue = Sm.DrStr(dr, c[42]);
                        TxtCnt25.EditValue = Sm.DrStr(dr, c[43]);
                        TxtSeal1.EditValue = Sm.DrStr(dr, c[44]);
                        TxtSeal2.EditValue = Sm.DrStr(dr, c[45]);
                        TxtSeal3.EditValue = Sm.DrStr(dr, c[46]);
                        TxtSeal4.EditValue = Sm.DrStr(dr, c[47]);
                        TxtSeal5.EditValue = Sm.DrStr(dr, c[48]);
                        TxtSeal6.EditValue = Sm.DrStr(dr, c[49]);
                        TxtSeal7.EditValue = Sm.DrStr(dr, c[50]);
                        TxtSeal8.EditValue = Sm.DrStr(dr, c[51]);
                        TxtSeal9.EditValue = Sm.DrStr(dr, c[52]);
                        TxtSeal10.EditValue = Sm.DrStr(dr, c[53]);
                        TxtSeal11.EditValue = Sm.DrStr(dr, c[54]);
                        TxtSeal12.EditValue = Sm.DrStr(dr, c[55]);
                        TxtSeal13.EditValue = Sm.DrStr(dr, c[56]);
                        TxtSeal14.EditValue = Sm.DrStr(dr, c[57]);
                        TxtSeal15.EditValue = Sm.DrStr(dr, c[58]);
                        TxtSeal16.EditValue = Sm.DrStr(dr, c[59]);
                        TxtSeal17.EditValue = Sm.DrStr(dr, c[60]);
                        TxtSeal18.EditValue = Sm.DrStr(dr, c[61]);
                        TxtSeal19.EditValue = Sm.DrStr(dr, c[62]);
                        TxtSeal20.EditValue = Sm.DrStr(dr, c[63]);
                        TxtSeal21.EditValue = Sm.DrStr(dr, c[64]);
                        TxtSeal22.EditValue = Sm.DrStr(dr, c[65]);
                        TxtSeal23.EditValue = Sm.DrStr(dr, c[66]);
                        TxtSeal24.EditValue = Sm.DrStr(dr, c[67]);
                        TxtSeal25.EditValue = Sm.DrStr(dr, c[68]);
                        Sm.SetLue(LueSize1, Sm.DrStr(dr, c[69]));
                        Sm.SetLue(LueSize2, Sm.DrStr(dr, c[70]));
                        Sm.SetLue(LueSize3, Sm.DrStr(dr, c[71]));
                        Sm.SetLue(LueSize4, Sm.DrStr(dr, c[72]));
                        Sm.SetLue(LueSize5, Sm.DrStr(dr, c[73]));
                        Sm.SetLue(LueSize6, Sm.DrStr(dr, c[74]));
                        Sm.SetLue(LueSize7, Sm.DrStr(dr, c[75]));
                        Sm.SetLue(LueSize8, Sm.DrStr(dr, c[76]));
                        Sm.SetLue(LueSize9, Sm.DrStr(dr, c[77]));
                        Sm.SetLue(LueSize10, Sm.DrStr(dr, c[78]));
                        Sm.SetLue(LueSize11, Sm.DrStr(dr, c[79]));
                        Sm.SetLue(LueSize12, Sm.DrStr(dr, c[80]));
                        Sm.SetLue(LueSize13, Sm.DrStr(dr, c[81]));
                        Sm.SetLue(LueSize14, Sm.DrStr(dr, c[82]));
                        Sm.SetLue(LueSize15, Sm.DrStr(dr, c[83]));
                        Sm.SetLue(LueSize16, Sm.DrStr(dr, c[84]));
                        Sm.SetLue(LueSize17, Sm.DrStr(dr, c[85]));
                        Sm.SetLue(LueSize18, Sm.DrStr(dr, c[86]));
                        Sm.SetLue(LueSize19, Sm.DrStr(dr, c[87]));
                        Sm.SetLue(LueSize20, Sm.DrStr(dr, c[88]));
                        Sm.SetLue(LueSize21, Sm.DrStr(dr, c[89]));
                        Sm.SetLue(LueSize22, Sm.DrStr(dr, c[90]));
                        Sm.SetLue(LueSize23, Sm.DrStr(dr, c[91]));
                        Sm.SetLue(LueSize24, Sm.DrStr(dr, c[92]));
                        Sm.SetLue(LueSize25, Sm.DrStr(dr, c[93]));

                        MeeSM1.EditValue = Sm.DrStr(dr, c[94]);
                        MeeSM2.EditValue = Sm.DrStr(dr, c[95]);
                        MeeSM3.EditValue = Sm.DrStr(dr, c[96]);
                        MeeSM4.EditValue = Sm.DrStr(dr, c[97]);
                        MeeSM5.EditValue = Sm.DrStr(dr, c[98]);
                        MeeSM6.EditValue = Sm.DrStr(dr, c[99]);
                        MeeSM7.EditValue = Sm.DrStr(dr, c[100]);
                        MeeSM8.EditValue = Sm.DrStr(dr, c[101]);
                        MeeSM9.EditValue = Sm.DrStr(dr, c[102]);
                        MeeSM10.EditValue = Sm.DrStr(dr, c[103]);
                        MeeSM11.EditValue = Sm.DrStr(dr, c[104]);
                        MeeSM12.EditValue = Sm.DrStr(dr, c[105]);
                        MeeSM13.EditValue = Sm.DrStr(dr, c[106]);
                        MeeSM14.EditValue = Sm.DrStr(dr, c[107]);
                        MeeSM15.EditValue = Sm.DrStr(dr, c[108]);
                        MeeSM16.EditValue = Sm.DrStr(dr, c[109]);
                        MeeSM17.EditValue = Sm.DrStr(dr, c[110]);
                        MeeSM18.EditValue = Sm.DrStr(dr, c[111]);
                        MeeSM19.EditValue = Sm.DrStr(dr, c[112]);
                        MeeSM20.EditValue = Sm.DrStr(dr, c[113]);
                        MeeSM21.EditValue = Sm.DrStr(dr, c[114]);
                        MeeSM22.EditValue = Sm.DrStr(dr, c[115]);
                        MeeSM23.EditValue = Sm.DrStr(dr, c[116]);
                        MeeSM24.EditValue = Sm.DrStr(dr, c[117]);
                        MeeSM25.EditValue = Sm.DrStr(dr, c[118]);

                        MeeRemark.Text = Sm.DrStr(dr, c[119]);
                        MeeRemark2.Text = Sm.DrStr(dr, c[120]);
                    }, true
                );
        }

        private void ShowPLDtl(string DocNo, string SectionNo, ref iGrid ItemGrd, DXE.TextEdit TxtTotal)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@SectionNo", SectionNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.ItCode, B.ItCodeInternal, B.ItName, A.SODOcNo, A.SODno, A.PLNo, A.QtyPL,  ");
            SQL.AppendLine("C.PackagingUnitUomCode, A.QtyPackagingUnit, A.Qty, A.QtyInventory, G.PriceUomCode, ");
            SQL.AppendLine("B.InventoryUomCode, A.Remark, A.ProcessInd, ");
            SQL.AppendLine("H.CtItCode, H.CtItName, A.TotalVolume, (A.QtyPackagingUnit*K.NWRate) NW, (A.QtyPackagingUnit*K.GWRate) GW  ");
            SQL.AppendLine("From TblPLDtl A  ");
            SQL.AppendLine("Inner Join TblItem B On A.ItCode = B.ItCode ");
            SQL.AppendLine("Inner Join TblSODtl C On A.SoDocNo = C.DocNo And A.SoDno = C.Dno ");
            SQL.AppendLine("Inner Join TblSOHdr D On C.DocNo = D.DocNo And A.SODocNo = D.DocNo  ");
            SQL.AppendLine("Inner Join TblCtQtDtl F On D.CtQtDocNo = F.DocNo And C.CtQtDNo = F.Dno ");
            SQL.AppendLine("Inner Join TblItemPriceHdr G On F.ItemPriceDocNo = G.DocNo ");
            SQL.AppendLine("Left Join TblCustomerItem H On B.ItCode= H.ItCode And D.CtCode=H.CtCode ");
            SQL.AppendLine("Inner Join TblPLhdr I ON A.DocNo = I.DocNO  ");
            SQL.AppendLine("Inner Join TblSIHdr J On I.SiDocNo = J.DocNo  ");
            SQL.AppendLine("Inner Join TblSIDtl K On I.SiDocNo = K.DocNO And A.SODocNo = K.SoDOcNo And A.SODno = K.SoDno  "); 
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("And A.SectionNo=@SectionNo ");
            SQL.AppendLine("Order By A.DNo, A.ItCode;");

            Sm.ShowDataInGrid(
                ref ItemGrd, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "ItCode", 

                    //1-5
                    "ItName", "ItCodeInternal", "SODOcNo", "SODNo", "PLNo",   
 
                    //6-10
                    "QtyPL", "PackagingUnitUomCode", "QtyPackagingUnit", "Qty", "QtyInventory", 
                    
                    //11-15
                    "PriceUomCode", "InventoryUomCode", "Remark", "ProcessInd", "CtItCode",

                    //16-17
                    "CtItName", "TotalVolume", "GW", "NW"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 10);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 12);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 13);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 14);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 15);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 16);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 22, 17);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 18); 
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 19);
                    ComputeTotal(Grd, TxtTotal);
                    ComputeUom4ShowData(Grd);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref ItemGrd, ItemGrd.Rows.Count - 1, new int[] { 8, 10, 11, 13, 22, 15, 16  });
        }
        
        public void ShowSOData(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select H.ItCode, I.ItName, I.ItCodeInternal, D.DocNo, C.DNo,  ");
            SQL.AppendLine("C.PackagingUnitUomCode, 0 As QtyPackagingUnit,  ");
            SQL.AppendLine("ifnull(B.QtyPackagingUnit, 0) As OutstandingPackaging, ");
            SQL.AppendLine("0 As Qty, G.PriceUomCode, ifnull(B.Qty, 0) AS OutstandingQty, ");
            SQL.AppendLine("0 As QtyInventory, I.InventoryUomCode, C.DeliveryDt, B.Remark,   ");
            SQL.AppendLine("((F.UPrice-(F.UPrice*0.01*IfNull(J.DiscRate, 0)))+ ((F.UPrice-(F.UPrice*0.01*IfNull(J.DiscRate, 0)))*0.01*C.TaxRate)) As PriceAfterTax, ");
            SQL.AppendLine("K.CtItCode, K.CtItName ");
            SQL.AppendLine("From TblSIHdr A  ");
            SQL.AppendLine("Inner join TblSIDtl B On A.DocNo = B.DocNo  ");
            SQL.AppendLine("Inner Join TblSODtl C On B.SODocNo = C.DocNo And B.SODNo = C.DNo  ");
            SQL.AppendLine("Inner Join TblSOHdr D On C.DocNo = D.DocNo And B.SODocNo = D.DocNo  ");
            SQL.AppendLine("Inner Join TblCtQtDtl F On D.CtQtDocNo = F.DocNo And C.CtQtDNo = F.Dno ");
            SQL.AppendLine("Inner Join TblItemPriceHdr G On F.ItemPriceDocNo = G.DocNo ");
            SQL.AppendLine("Inner Join TblitemPriceDtl H On F.ItemPriceDocNo = H.DocNo And F.ItemPriceDNo = H.Dno ");
            SQL.AppendLine("Inner Join TblItem I On H.ItCode = I.ItCode  ");
            SQL.AppendLine("Left Join TblSOQuotPromoItem J On D.SOQuotPromoDocNo=J.DocNo And H.ItCode=J.ItCode ");
            SQL.AppendLine("Left Join TblCustomerItem K On I.ItCode=K.ItCode And D.CtCode=K.CtCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            //SQL.AppendLine("Where A.DocNo=@DocNo And D.DocNo Not In (Select SODocNo From TblPlDtl) Order By B.DNo ");


            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", TxtSIDocNo.Text);

            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "ItCode",
 
                    //1-5
                    "ItName", "ItCodeInternal", "DocNo",  "DNo", "PackagingUnitUomCode", 
                    
                    //6-10
                    "OutstandingPackaging", "QtyPackagingUnit", "OutstandingQty", "Qty", "PriceUomCode", 
                    
                    //11-15
                    "QtyInventory", "InventoryUomCode", "DeliveryDt","Remark", "PriceAftertax",
 
                    //16-17
                    "CtItCode", "CtItName"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Grd1.Cells[Row, 0].Value = Row + 1;
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 7);
                    Grd.Cells[Row, 11].Value = Sm.GetGrdDec(Grd, Row, 9) - Sm.GetGrdDec(Grd, Row, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 9);
                    Grd.Cells[Row, 14].Value = Sm.GetGrdDec(Grd, Row, 12) - Sm.GetGrdDec(Grd, Row, 13);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 11);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 12);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 20, 13);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 14);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 22, 15);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 16);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 17);
                    Sm.SetGrdNumValueZero(ref Grd1, Row, new int[] { 18, 19});
                }, false, false, true, false
            );
            ComputeUom2();
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 9, 10, 11, 12, 13, 14, 16, 18, 19 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        public void ShowSOData2(string DocNo, string Dno, iGrid Grdxx, int RowInd)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, B.Dno, H.ItCode, B.PackagingUnitUomCOde, ");
            SQL.AppendLine("G.PriceUomCode, I.InventoryUomCOde ");
            SQL.AppendLine("From TblSOHdr A  ");
            SQL.AppendLine("Inner Join TblSODtl B On A.DocNo = B.DocNo  ");
            SQL.AppendLine("Inner Join TblCtQtDtl F On A.CtQtDocNo = F.DocNo And B.CtQtDNo = F.Dno  ");
            SQL.AppendLine("Inner Join TblItemPriceHdr G On F.ItemPriceDocNo = G.DocNo  ");
            SQL.AppendLine("Inner Join TblitemPriceDtl H On F.ItemPriceDocNo = H.DocNo And F.ItemPriceDNo = H.Dno  ");
            SQL.AppendLine("Inner Join TblItem I On H.ItCode = I.ItCode   ");
            SQL.AppendLine("Where A.DocNo=@DocNo And B.Dno = @DNo  ");


            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Dno);

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                cm.CommandTimeout = 100;
                using (var dr = cm.ExecuteReader())
                {
                    var c = Sm.GetOrdinal(dr,
                        new string[] 
                        { 
                            //0
                           "DocNo",  

                            //1-3
                           "DNo", "PackagingUnitUomCOde", "PriceUomCode", "InventoryUomCOde"
                        }
                        );
                    if (dr.HasRows)
                    {
                        Grdxx.ProcessTab = true;
                        Grdxx.BeginUpdate();
                        while (dr.Read())
                        {
                            for (int Row = 0; Row < Grdxx.Rows.Count - 1; Row++)
                            {
                                if (Sm.CompareStr(DocNo, Sm.DrStr(dr, 0)) &&
                                    Sm.CompareStr(Dno, Sm.DrStr(dr, 1)))
                                {
                                    Sm.SetGrdValue("S", Grdxx, dr, c, RowInd, 9, 2);
                                    Grdxx.Cells[RowInd, 8].Value  = Grdxx.Cells[RowInd, 10].Value = Grdxx.Cells[RowInd, 11].Value = Grdxx.Cells[RowInd, 13].Value = 0;
                                    Sm.SetGrdValue("S", Grdxx, dr, c, RowInd, 12, 3);
                                    Sm.SetGrdValue("S", Grdxx, dr, c, RowInd, 14, 4);
                                    Grdxx.Cells[RowInd, 22].Value = 0;
                                    break;
                                }
                            }
                        }
                        Grdxx.EndUpdate();
                    }
                    dr.Close();
                    ComputeUom(Grdxx);
                }
            }
        }

        public void ShowSOData3(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select H.ItCode, I.ItName, I.ItCodeInternal, D.DocNo, C.DNo,  ");
            SQL.AppendLine("C.PackagingUnitUomCode, B.QtyPackagingUnit As QtyPackagingUnit,  ");
            SQL.AppendLine("ifnull(B.QtyPackagingUnit, 0) As OutstandingPackaging, ");
            SQL.AppendLine("B.Qty As Qty, G.PriceUomCode, ifnull(B.Qty, 0) AS OutstandingQty, ");
            SQL.AppendLine("B.QtyInventory As QtyInventory, I.InventoryUomCode, C.DeliveryDt, B.Remark,   ");
            SQL.AppendLine("((F.UPrice-(F.UPrice*0.01*IfNull(J.DiscRate, 0)))+ ((F.UPrice-(F.UPrice*0.01*IfNull(J.DiscRate, 0)))*0.01*C.TaxRate)) As PriceAfterTax, ");
            SQL.AppendLine("K.CtItCode, K.CtItName ");
            SQL.AppendLine("From TblPLHdr X");
            SQL.AppendLine("Inner Join TblSIHdr A On X.SIDocNo = A.DocNo ");
            SQL.AppendLine("Inner join TblSIDtl B On A.DocNo = B.DocNo  ");
            SQL.AppendLine("Inner Join TblSODtl C On B.SODocNo = C.DocNo And B.SODNo = C.DNo  ");
            SQL.AppendLine("Inner Join TblSOHdr D On C.DocNo = D.DocNo And B.SODocNo = D.DocNo  ");
            SQL.AppendLine("Inner Join TblCtQtDtl F On D.CtQtDocNo = F.DocNo And C.CtQtDNo = F.Dno ");
            SQL.AppendLine("Inner Join TblItemPriceHdr G On F.ItemPriceDocNo = G.DocNo ");
            SQL.AppendLine("Inner Join TblitemPriceDtl H On F.ItemPriceDocNo = H.DocNo And F.ItemPriceDNo = H.Dno ");
            SQL.AppendLine("Inner Join TblItem I On H.ItCode = I.ItCode  ");
            SQL.AppendLine("Left Join TblSOQuotPromoItem J On D.SOQuotPromoDocNo=J.DocNo And H.ItCode=J.ItCode ");
            SQL.AppendLine("Left Join TblCustomerItem K On I.ItCode=K.ItCode And D.CtCode=K.CtCode ");
            SQL.AppendLine("Where X.DocNo=@DocNo  Order By B.DNo ");


            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "ItCode",
 
                    //1-5
                    "ItName", "ItCodeInternal", "DocNo",  "DNo", "PackagingUnitUomCode", 
                    
                    //6-10
                    "OutstandingPackaging", "QtyPackagingUnit", "OutstandingQty", "Qty", "PriceUomCode", 
                    
                    //11-15
                    "QtyInventory", "InventoryUomCode", "DeliveryDt","Remark", "PriceAfterTax",
 
                    //16-17
                    "CtItCode", "CtItName"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Grd1.Cells[Row, 0].Value = Row + 1;
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 7);
                    Grd.Cells[Row, 11].Value = Sm.GetGrdDec(Grd, Row, 9) - Sm.GetGrdDec(Grd, Row, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 8);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 9);
                    Grd.Cells[Row, 14].Value = Sm.GetGrdDec(Grd, Row, 12) - Sm.GetGrdDec(Grd, Row, 13);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 11);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 12);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 20, 13);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 14);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 22, 15);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 16);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 17);
                }, false, false, true, false
            );
            ComputeUom2();
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 9, 10, 11, 12, 13, 14, 16, 22 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Additional Method

        private void ProcessPallets()
        {
            var mListGrd = new List<iGrid> 
            {
                Grd11, Grd12, Grd13, Grd14, Grd15,
                Grd16, Grd17, Grd18, Grd19, Grd20,
                Grd21, Grd22, Grd23, Grd24, Grd25,
                Grd26, Grd27, Grd28, Grd29, Grd30,
                Grd31, Grd32, Grd33, Grd34, Grd35
            };

            decimal mPalletsA = 0m, mPalletsB = 0m;
            string mDash = "-", mPLNo = string.Empty, mA = string.Empty, mB = string.Empty;

            mListGrd.ForEach(Grd =>
            {
                for (int i = 0; i < Grd.Rows.Count; i++)
                {
                    Grd.Cells[i, 7].Value = null;
                    if(Sm.GetGrdStr(Grd, i, 0).Length > 0)
                    {
                        if (Sm.GetGrdDec(Grd, i, 8) == 0)
                        {
                            Grd.Cells[i, 7].Value = mDash;
                        }
                        else
                        {
                            mPalletsA = Decimal.Round((mPalletsB + 1), 0);
                            mPalletsB = Decimal.Round((mPalletsB + Sm.GetGrdDec(Grd, i, 8)), 0);
                            mA = Sm.Right(string.Concat("0000", mPalletsA.ToString()), 3);
                            mB = Sm.Right(string.Concat("0000", mPalletsB.ToString()), 3);
                            if (mPalletsA == mPalletsB)
                            {
                                mPLNo = mA;
                            }
                            else
                            {
                                mPLNo = string.Concat(mA, mDash, mB);
                            }

                            Grd.Cells[i, 7].Value = mPLNo;
                        }
                    }
                }
            });

            mListGrd.Clear();
        }

        #region credit limit 
        private bool CheckCreditLimit()
        {
            if (mIsCreditLimitValidate)
            {
                decimal DOSI = 0;
                decimal SIIP = 0m;
                decimal IPVC = 0m;
                decimal RIDO = 0m;
                decimal CL = 0m;
                decimal Outs = 0m;
                decimal PLNow = 0;
                decimal PLDO = 0;
                string CTQT = string.Empty;

                //nentuin customer code
                string CtCode = string.Empty;
                CtCode = Sm.GetLue(LueCtCode);

                //nentuin nilai credit limit
                CL = Decimal.Parse(Sm.GetValue("Select CreditLimit From TblCtQthdr Where CtCode='" + CtCode + "' And Status = 'A' And ActInd = 'Y' "));

                //nentuin customer quotation yang aktif
                CTQT = Sm.GetValue("Select DocNo From TblCtQthdr Where CtCode='" + CtCode + "' And Status = 'A' And ActInd = 'Y' ");

                if (GetDOSI(CtCode) == 0)
                {
                    DOSI = 0;
                }
                else
                {
                    DOSI = GetDOSI(CtCode);
                }


                //nentuin amount PL yang sdg dibuat
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    PLNow += (Sm.GetGrdDec(Grd1, Row, 13) * Sm.GetGrdDec(Grd1, Row, 22));
                }

                SIIP = GetSIIP(CtCode);
                IPVC = GetIPVC(CtCode);
                RIDO = GetRIDO(CtCode, CTQT);
                PLDO = GetPLDO(CtCode);

                Outs = DOSI + SIIP + IPVC + PLNow + PLDO - RIDO;

                if (CL < Outs)
                {
                    Sm.StdMsg(
                    mMsgType.Warning,
                    "Credit Limit : " + Sm.FormatNum(CL, 0) + Environment.NewLine +
                    "Outstanding : " + Sm.FormatNum(Outs, 0) + Environment.NewLine +
                    "Balance : " + Sm.FormatNum(CL - Outs, 0) + Environment.NewLine +
                    "Total amount should not be greater than Credit Limit."
                    );
                    return true;
                }
                return false;
            }
            return false;
        }

        //Sales Retur  terhadap Quotation aktif
        private decimal GetRIDO(string CtCode, string CtQtDocNo)
        {
            var cm = new MySqlCommand
            {
                CommandText =
                    "Select ifnull(SUM(T.Amt), 0) As RIDOAMT From ( " +
                    "   Select B.ItCode, (A.Qty* C.UPrice) As AMt   " +
                    "   from TblRecvCtDtl A  " +
                    "   Inner Join TblDoCt2Dtl B On A.DOCtDocNo = B.DocNo And A.DOCtDno = B.Dno   " +
                    "   Inner Join (  " +
                    "       Select A.DocNo, B.DNo, C.UPrice  " +
                    "       From TblDOCt2Hdr A  " +
                    "       Inner Join TblDOCt2Dtl B On A.DocNo=B.DocNo And B.CancelInd='N'  " +
                    "       Left Join  " +
                    "       (  " +
                    "           Select Distinct A.DocNo, G.ItCode, C.CurCode,  " +
                    "           ((E.UPrice-(E.UPrice*0.01*IfNull(H.DiscRate, 0)))+ ((E.UPrice-(E.UPrice*0.01*IfNull(H.DiscRate, 0)))*0.01*D.TaxRate)) As UPrice  " +
                    "           From TblDrhdr A  " +
                    "           Inner Join TblDrDtl B On A.DocNo = B.DocNo  " +
                    "           Inner Join TblSOHdr C On B.SODocNo = C.DocNo And C.cancelInd = 'N'  " +
                    "           Inner Join TblSODtl D On C.DocNo = D.DocNo  " +
                    "           Inner Join TblCtQtDtl E On C.CtQTDocno = E.DocNo And D.CtQTDno = E.DNo  " +
                    "           Inner Join TblItemPriceHdr F On E.ItemPriceDocNo=F.DocNo  " +
                    "           Inner Join TblItemPriceDtl G On E.ItemPriceDocNo=G.DocNo And E.ItemPriceDNo=G.DNo  " +
                    "           Left Join TblSOQuotPromoItem H On C.SOQuotPromoDocNo=H.DocNo And G.ItCode=H.ItCode  " +
                    "           Inner Join TblItem I On G.ItCode=I.ItCode  " +
                    "           Where A.cancelInd = 'N' And A.CtCode = @CtCode And E.DocNo = @CtQtDocNo  " +
                    "        ) C On A.DrDocno = C.DocNo And B.ItCode=C.ItCode  " +
                    "   )C On B.DocNo = C.DocNo And B.Dno = C.Dno  " +
                    ")T "
            };
            Sm.CmParam<String>(ref cm, "@CtCode", CtCode);
            Sm.CmParam<String>(ref cm, "@CtQtDocNo", CtQtDocNo);
            return Sm.GetValueDec(cm);
        }

        //Incoming payment yang belum divoucherkan
        private decimal GetIPVC(string CtCode)
        {
            var cm = new MySqlCommand
            {
                CommandText =
                    "Select ifnull(SUM(A.Amt), 0) As AmtIPVC From TblIncomingpaymentHdr A " +
                    "Inner Join TblVoucherRequestHdr B On A.VoucherRequestDocNo = B.DocNo " +
                    "Where A.cancelInd = 'N' And A.CtCode = @CtCode And " +
                    "B.DocNo Not In ( " +
                    "    Select A.VoucherRequestDocNo " +
                    "    From TblVoucherHdr A " +
                    "    Inner Join TblVoucherRequestHdr B On A.VoucherRequestDocno = B.Docno " +
                    "    Where A.CancelInd = 'N' ); "
            };
            Sm.CmParam<String>(ref cm, "@CtCode", CtCode);
            return Sm.GetValueDec(cm);
        }

        //SalesInvoice yang belum di incoming paymentkan
        private decimal GetSIIP(string CtCode)
        {
            var cm = new MySqlCommand
            {
                CommandText =
                    "Select ifnull(Sum(Amt), 0) As AmtSIIP from tblsalesinvoicehdr t where ctcode = @CtCode and cancelind='N' and " +
                    "not exists( " +
                    "    select a.docno " +
                    "    from tblincomingpaymenthdr a, tblincomingpaymentdtl b " +
                    "    where a.docno=b.docno " +
                    "    and status<>'C'  " +
                    "    and cancelind='N' " +
                    "    and b.InvoiceDocNo=t.docno and b.InvoiceType='1' " +
                    "); "
            };
            Sm.CmParam<String>(ref cm, "@CtCode", CtCode);
            return Sm.GetValueDec(cm);
        }

        //delivery order yang belum di sales invoicekan
        private decimal GetDOSI(string CtCode)
        {
            var cm = new MySqlCommand
            {
                CommandText =
                    "Select IFNULL(SUM(T.Qty * T.PriceAfterTax), 0) AmtDOSI " +
                    "From ( " +
                    "   Select '1' As DocType, " +
                    "   If(C.QtyInventory=0, 0, (B.Qty/C.QtyInventory)*C.Qty) As Qty, " +
                    "   (G.UPrice-(G.UPrice*0.01*IfNull(J.DiscRate, 0))) +  " +
                    "   ((G.UPrice-(G.UPrice*0.01*IfNull(J.DiscRate, 0)))*0.01*E.TaxRate) As PriceAfterTax " +
                    "   From TblDOCt2Hdr A " +
                    "   Inner Join TblDOCt2Dtl2 B On A.DocNo=B.DocNo And B.Qty>0 And B.ProcessInd='O' " +
                    "   Inner Join TblDRDtl C On A.DRDocNo=C.DocNo And B.DRDNo=C.DNo " +
                    "   Inner Join TblSOHdr D On C.SODocNo=D.DocNo And D.CancelInd = 'N'  " +
                    "   Inner Join TblSODtl E On C.SODocNo=E.DocNo And C.SODNo=E.DNo " +
                    "   Inner Join TblCtQtHdr F On D.CtQtDocNo=F.DocNo   " +
                    "   Inner Join TblCtQtDtl G On D.CtQtDocNo=G.DocNo And E.CtQtDNo=G.DNo " +
                    "   Inner Join TblItemPriceHdr H On G.ItemPriceDocNo=H.DocNo  " +
                    "   Inner Join TblItemPriceDtl I On G.ItemPriceDocNo=I.DocNo And G.ItemPriceDNo=I.DNo " +
                    "   Left Join TblSOQuotPromoItem J On D.SOQuotPromoDocNo=J.DocNo And I.ItCode=J.ItCode  " +
                    "   Inner Join TblDRHdr M On A.DRDocNo=M.DocNo " +
                    "   Where A.CtCode=@CtCode " +
                    "   Union All " +
                    "   Select '2' As DocType, " +
                    "   If(C.QtyInventory=0, 0, (B.Qty/C.QtyInventory)*C.Qty) As Qty, " +
                    "   (G.UPrice-(G.UPrice*0.01*IfNull(J.DiscRate, 0))) + " +
                    "   ((G.UPrice-(G.UPrice*0.01*IfNull(J.DiscRate, 0)))*0.01*E.TaxRate) As PriceAfterTax " +
                    "   From TblDOCt2Hdr A  " +
                    "   Inner Join TblDOCt2Dtl3 B On A.DocNo=B.DocNo And B.Qty>0 And B.ProcessInd='O' " +
                    "   Inner Join TblPLDtl C On A.PLDocNo=C.DocNo And B.PLDNo=C.DNo  " +
                    "   Inner Join TblSOHdr D On C.SODocNo=D.DocNo And D.CancelInd = 'N'  " +
                    "   inner Join TblSODtl E On C.SODocNo=E.DocNo And C.SODNo=E.DNo " +
                    "   Inner Join TblCtQtHdr F On D.CtQtDocNo=F.DocNo " +
                    "   Inner Join TblCtQtDtl G On D.CtQtDocNo=G.DocNo And E.CtQtDNo=G.DNo " +
                    "   Inner Join TblItemPriceHdr H On G.ItemPriceDocNo=H.DocNo  " +
                    "   Inner Join TblItemPriceDtl I On G.ItemPriceDocNo=I.DocNo And G.ItemPriceDNo=I.DNo " +
                    "   Left Join TblSOQuotPromoItem J On D.SOQuotPromoDocNo=J.DocNo And I.ItCode=J.ItCode " +
                    "   Where A.CtCode=@CtCode " +
                    " )T; "
            };
            Sm.CmParam<String>(ref cm, "@CtCode", CtCode);
            return Sm.GetValueDec(cm);
        }

        //Packing list yang belum di DO kan
        private decimal GetPLDO(string CtCode)
        {
            var cm = new MySqlCommand
            {
                CommandText =

                    "Select  ifnull(SUM((B.Qty*  "+
                    "((G.UPrice-(G.UPrice*0.01*IfNull(J.DiscRate, 0)))+ ((G.UPrice-(G.UPrice*0.01*IfNull(J.DiscRate, 0)))*0.01*E.TaxRate)))), 0) "+
                    "From TblPLhdr A "+
                    "Inner Join TblPLDtl B On A.DocNo = B.DocNO "+
                    "Inner Join TblSOHdr D On B.SODocNo=D.DocNo And D.CancelInd = 'N'  "+
                    "Inner Join TblSODtl E On B.SODocNo=E.DocNo And B.SODNo=E.DNo "+
                    "Inner Join TblCtQtHdr F On D.CtQtDocNo=F.DocNo "+
                    "Inner Join TblCtQtDtl G On D.CtQtDocNo=G.DocNo And E.CtQtDNo=G.DNo "+
                    "Inner Join TblItemPriceHdr H On G.ItemPriceDocNo=H.DocNo "+
                    "Inner Join TblItemPriceDtl I On G.ItemPriceDocNo=I.DocNo And G.ItemPriceDNo=I.DNo "+
                    "Left Join TblSOQuotPromoItem J On D.SOQuotPromoDocNo=J.DocNo And I.ItCode=J.ItCode  "+
                    "Where B.ProcessInd = 'O' And D.CtCode = @CtCode "

            };
            Sm.CmParam<String>(ref cm, "@CtCode", CtCode);
            return Sm.GetValueDec(cm);
        }

        #endregion

        private bool CheckCnt(TextEdit txtCnt, string Cnt)
        {
            TextEdit[] txtContainer = { TxtCnt1, TxtCnt2, TxtCnt3, TxtCnt4, TxtCnt5,
                               TxtCnt6, TxtCnt7, TxtCnt8, TxtCnt9, TxtCnt10,
                               TxtCnt11, TxtCnt12, TxtCnt13, TxtCnt14, TxtCnt15,
                               TxtCnt16, TxtCnt17, TxtCnt18, TxtCnt19, TxtCnt20,
                               TxtCnt21, TxtCnt22, TxtCnt23, TxtCnt24, TxtCnt25
                                      };

            for (int i = 0; i < txtContainer.Length; i++ )
            {
                if (txtContainer[i] != txtCnt && txtContainer[i].Text.Length !=0)
                {
                    if (txtContainer[i].Text == Cnt)
                    {
                        StdMtd.StdMsg(mMsgType.Warning, "Container name already exist");
                        txtCnt.EditValue = "";
                        return true;
                    }
                }
            }
            return false;
        }

        private bool CheckSeal(TextEdit txtSeal, string Seal)
        {
            TextEdit[] SealText = { TxtSeal1, TxtSeal2, TxtSeal3, TxtSeal4, TxtSeal5,
                               TxtSeal6, TxtSeal7, TxtSeal8, TxtSeal9, TxtSeal10,
                               TxtSeal11, TxtSeal12, TxtSeal13, TxtSeal14, TxtSeal15,
                               TxtSeal16, TxtSeal17, TxtSeal18, TxtSeal19, TxtSeal20,
                               TxtSeal21, TxtSeal22, TxtSeal23, TxtSeal24, TxtSeal25
                                  };

            for (int i = 0; i < SealText.Length; i++)
            {
                if (SealText[i] != txtSeal && SealText[i].Text.Length != 0)
                {
                    if (SealText[i].Text == Seal)
                    {
                        StdMtd.StdMsg(mMsgType.Warning, "Seal name already exist");
                        txtSeal.EditValue = "";
                        return true;
                    }
                }
            }
            return false;
        }

        private void SetTabControls(string TabPage, ref DXE.LookUpEdit LueSize)
        {
            tabControl1.SelectTab("tabPage" + TabPage);
            SetLueSize(ref LueSize);
        }

        private void ComputeTotal(iGrid Grd, DXE.TextEdit Txt)
        {
            decimal Total = 0m;
            for (int Row = 0; Row <= Grd.Rows.Count - 1; Row++)
                if (Sm.GetGrdStr(Grd, Row, 0).Length != 0) 
                    Total += Sm.GetGrdDec(Grd, Row, 10);
            Txt.Text = Sm.FormatNum(Total, 0);
        }

        private void CheckProcessInd(iGrid GrdXX, TextEdit Cnt, TextEdit Seal)
        {
            int Ind = 0;
            for (int Row = 0; Row <= GrdXX.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(GrdXX, Row, 0).Length != 0 && Sm.GetGrdStr(GrdXX, Row, 18) == "F")
                {
                    Ind = Ind + 1;
                }
            }
            if (Ind != 0)
            {
                Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                       {
                           Cnt, Seal
                       }, true);
                Sm.GrdColReadOnly(true, true, GrdXX, new int[] { 0, 1, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18 });
            }
            else
            {
                var cm = new MySqlCommand()
                {
                    CommandText =
                        "Select A.DocNo From TblSP A " +
                        "Inner Join TblSIHdr B On A.DocNo = B.SPDocNo " +
                        "Where A.Status <> 'P' And B.DocNo=@DocNo "
                };
                Sm.CmParam<String>(ref cm, "@DocNo", TxtSIDocNo.Text);

                if (Sm.IsDataExist(cm))
                {
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                       {
                           Cnt, Seal
                       }, false);
                    Sm.GrdColReadOnly(true, true, GrdXX, new int[] { 0, 1, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18 });
                }
                else
                {
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                       {
                           Cnt, Seal
                       }, false);
                    Sm.GrdColReadOnly(false, true, GrdXX, new int[] { 1, 7, 8, 10, 17 });
                }
            }

            #region Pallet ngga bisa di edit
            if (!mIsPalletInPLEditable)
            {
                Sm.GrdColReadOnly(true, true, Grd11, new int[] { 7 });
                Sm.GrdColReadOnly(true, true, Grd12, new int[] { 7 });
                Sm.GrdColReadOnly(true, true, Grd13, new int[] { 7 });
                Sm.GrdColReadOnly(true, true, Grd14, new int[] { 7 });
                Sm.GrdColReadOnly(true, true, Grd15, new int[] { 7 });
                Sm.GrdColReadOnly(true, true, Grd16, new int[] { 7 });
                Sm.GrdColReadOnly(true, true, Grd17, new int[] { 7 });
                Sm.GrdColReadOnly(true, true, Grd18, new int[] { 7 });
                Sm.GrdColReadOnly(true, true, Grd19, new int[] { 7 });
                Sm.GrdColReadOnly(true, true, Grd20, new int[] { 7 });
                Sm.GrdColReadOnly(true, true, Grd21, new int[] { 7 });
                Sm.GrdColReadOnly(true, true, Grd22, new int[] { 7 });
                Sm.GrdColReadOnly(true, true, Grd23, new int[] { 7 });
                Sm.GrdColReadOnly(true, true, Grd24, new int[] { 7 });
                Sm.GrdColReadOnly(true, true, Grd25, new int[] { 7 });
                Sm.GrdColReadOnly(true, true, Grd26, new int[] { 7 });
                Sm.GrdColReadOnly(true, true, Grd27, new int[] { 7 });
                Sm.GrdColReadOnly(true, true, Grd28, new int[] { 7 });
                Sm.GrdColReadOnly(true, true, Grd29, new int[] { 7 });
                Sm.GrdColReadOnly(true, true, Grd30, new int[] { 7 });
                Sm.GrdColReadOnly(true, true, Grd31, new int[] { 7 });
                Sm.GrdColReadOnly(true, true, Grd32, new int[] { 7 });
                Sm.GrdColReadOnly(true, true, Grd33, new int[] { 7 });
                Sm.GrdColReadOnly(true, true, Grd34, new int[] { 7 });
                Sm.GrdColReadOnly(true, true, Grd35, new int[] { 7 });
            }
            #endregion
        }

        private void ComputePackagingQty(string ItCode, string SODocNo, string SoDNo )
        {
            decimal QtyPackaging = 0m;
            decimal QtySales = 0m;
            decimal QtyInventory = 0m;
            decimal TotalVolume = 0m;
            for (int Row = 0; Row < Grd11.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd11, Row, 0).Length != 0 &&
                    Sm.CompareStr(Sm.GetGrdStr(Grd11, Row, 0) + Sm.GetGrdStr(Grd11, Row, 4) + Sm.GetGrdStr(Grd11, Row, 6),
                    ItCode + SODocNo + SoDNo))
                {
                    QtyPackaging += Sm.GetGrdDec(Grd11, Row, 10);
                    QtySales += Sm.GetGrdDec(Grd11, Row, 11);
                    QtyInventory += Sm.GetGrdDec(Grd11, Row, 13);
                    TotalVolume += Sm.GetGrdDec(Grd11, Row, 22);
                   // Grd11.Cells[Row, 22].Value = 0;
                }
            }

            for (int Row = 0; Row < Grd12.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd12, Row, 0).Length != 0 &&
                    Sm.CompareStr(Sm.GetGrdStr(Grd12, Row, 0) + Sm.GetGrdStr(Grd12, Row, 4) + Sm.GetGrdStr(Grd12, Row, 6),
                    ItCode + SODocNo + SoDNo))
                {
                    QtyPackaging += Sm.GetGrdDec(Grd12, Row, 10);
                    QtySales += Sm.GetGrdDec(Grd12, Row, 11);
                    QtyInventory += Sm.GetGrdDec(Grd12, Row, 13);
                }
            }

            for (int Row = 0; Row < Grd13.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd13, Row, 0).Length != 0 &&
                    Sm.CompareStr(Sm.GetGrdStr(Grd13, Row, 0) + Sm.GetGrdStr(Grd13, Row, 4) + Sm.GetGrdStr(Grd13, Row, 6),
                    ItCode + SODocNo + SoDNo))
                {
                    QtyPackaging += Sm.GetGrdDec(Grd13, Row, 10);
                    QtySales += Sm.GetGrdDec(Grd13, Row, 11);
                    QtyInventory += Sm.GetGrdDec(Grd13, Row, 13);
                }
            }

            for (int Row = 0; Row < Grd14.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd14, Row, 0).Length != 0 &&
                    Sm.CompareStr(Sm.GetGrdStr(Grd14, Row, 0) + Sm.GetGrdStr(Grd14, Row, 4) + Sm.GetGrdStr(Grd14, Row, 6),
                    ItCode + SODocNo + SoDNo))
                {
                    QtyPackaging += Sm.GetGrdDec(Grd14, Row, 10);
                    QtySales += Sm.GetGrdDec(Grd14, Row, 11);
                    QtyInventory += Sm.GetGrdDec(Grd14, Row, 13);
                }
            }

            for (int Row = 0; Row < Grd15.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd15, Row, 0).Length != 0 &&
                    Sm.CompareStr(Sm.GetGrdStr(Grd15, Row, 0) + Sm.GetGrdStr(Grd15, Row, 4) + Sm.GetGrdStr(Grd15, Row, 6),
                    ItCode + SODocNo + SoDNo))
                {
                    QtyPackaging += Sm.GetGrdDec(Grd15, Row, 10);
                    QtySales += Sm.GetGrdDec(Grd15, Row, 11);
                    QtyInventory += Sm.GetGrdDec(Grd15, Row, 13);
                }
            }

            for (int Row = 0; Row < Grd16.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd16, Row, 0).Length != 0 &&
                    Sm.CompareStr(Sm.GetGrdStr(Grd16, Row, 0) + Sm.GetGrdStr(Grd16, Row, 4) + Sm.GetGrdStr(Grd16, Row, 6),
                    ItCode + SODocNo + SoDNo))
                {
                    QtyPackaging += Sm.GetGrdDec(Grd16, Row, 10);
                    QtySales += Sm.GetGrdDec(Grd16, Row, 11);
                    QtyInventory += Sm.GetGrdDec(Grd16, Row, 13);
                }
            }

            for (int Row = 0; Row < Grd17.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd17, Row, 0).Length != 0 &&
                    Sm.CompareStr(Sm.GetGrdStr(Grd17, Row, 0) + Sm.GetGrdStr(Grd17, Row, 4) + Sm.GetGrdStr(Grd17, Row, 6),
                    ItCode + SODocNo + SoDNo))
                {
                    QtyPackaging += Sm.GetGrdDec(Grd17, Row, 10);
                    QtySales += Sm.GetGrdDec(Grd17, Row, 11);
                    QtyInventory += Sm.GetGrdDec(Grd17, Row, 13);
                }
            }

            for (int Row = 0; Row < Grd18.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd18, Row, 0).Length != 0 &&
                    Sm.CompareStr(Sm.GetGrdStr(Grd18, Row, 0) + Sm.GetGrdStr(Grd18, Row, 4) + Sm.GetGrdStr(Grd18, Row, 6),
                    ItCode + SODocNo + SoDNo))
                {
                    QtyPackaging += Sm.GetGrdDec(Grd18, Row, 10);
                    QtySales += Sm.GetGrdDec(Grd18, Row, 11);
                    QtyInventory += Sm.GetGrdDec(Grd18, Row, 13);
                }
            }

            for (int Row = 0; Row < Grd19.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd19, Row, 0).Length != 0 &&
                    Sm.CompareStr(Sm.GetGrdStr(Grd19, Row, 0) + Sm.GetGrdStr(Grd19, Row, 4) + Sm.GetGrdStr(Grd19, Row, 6),
                    ItCode + SODocNo + SoDNo))
                {
                    QtyPackaging += Sm.GetGrdDec(Grd19, Row, 10);
                    QtySales += Sm.GetGrdDec(Grd19, Row, 11);
                    QtyInventory += Sm.GetGrdDec(Grd19, Row, 13);
                }
            }

            for (int Row = 0; Row < Grd20.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd20, Row, 0).Length != 0 &&
                    Sm.CompareStr(Sm.GetGrdStr(Grd20, Row, 0) + Sm.GetGrdStr(Grd20, Row, 4) + Sm.GetGrdStr(Grd20, Row, 6),
                    ItCode + SODocNo + SoDNo))
                {
                    QtyPackaging += Sm.GetGrdDec(Grd20, Row, 10);
                    QtySales += Sm.GetGrdDec(Grd20, Row, 11);
                    QtyInventory += Sm.GetGrdDec(Grd20, Row, 13);
                }
            }

            for (int Row = 0; Row < Grd21.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd21, Row, 0).Length != 0 &&
                    Sm.CompareStr(Sm.GetGrdStr(Grd21, Row, 0) + Sm.GetGrdStr(Grd21, Row, 4) + Sm.GetGrdStr(Grd21, Row, 6),
                    ItCode + SODocNo + SoDNo))
                {
                    QtyPackaging += Sm.GetGrdDec(Grd21, Row, 10);
                    QtySales += Sm.GetGrdDec(Grd21, Row, 11);
                    QtyInventory += Sm.GetGrdDec(Grd21, Row, 13);
                }
            }

            for (int Row = 0; Row < Grd22.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd22, Row, 0).Length != 0 &&
                    Sm.CompareStr(Sm.GetGrdStr(Grd22, Row, 0) + Sm.GetGrdStr(Grd22, Row, 4) + Sm.GetGrdStr(Grd22, Row, 6),
                    ItCode + SODocNo + SoDNo))
                {
                    QtyPackaging += Sm.GetGrdDec(Grd22, Row, 10);
                    QtySales += Sm.GetGrdDec(Grd22, Row, 11);
                    QtyInventory += Sm.GetGrdDec(Grd22, Row, 13);
                }
            }

            for (int Row = 0; Row < Grd23.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd23, Row, 0).Length != 0 &&
                    Sm.CompareStr(Sm.GetGrdStr(Grd23, Row, 0) + Sm.GetGrdStr(Grd23, Row, 4) + Sm.GetGrdStr(Grd23, Row, 6),
                    ItCode + SODocNo + SoDNo))
                {
                    QtyPackaging += Sm.GetGrdDec(Grd23, Row, 10);
                    QtySales += Sm.GetGrdDec(Grd23, Row, 11);
                    QtyInventory += Sm.GetGrdDec(Grd23, Row, 13);
                }
            }

            for (int Row = 0; Row < Grd24.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd24, Row, 0).Length != 0 &&
                    Sm.CompareStr(Sm.GetGrdStr(Grd24, Row, 0) + Sm.GetGrdStr(Grd24, Row, 4) + Sm.GetGrdStr(Grd24, Row, 6),
                    ItCode + SODocNo + SoDNo))
                {
                    QtyPackaging += Sm.GetGrdDec(Grd24, Row, 10);
                    QtySales += Sm.GetGrdDec(Grd24, Row, 11);
                    QtyInventory += Sm.GetGrdDec(Grd24, Row, 13);
                }
            }

            for (int Row = 0; Row < Grd25.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd25, Row, 0).Length != 0 &&
                    Sm.CompareStr(Sm.GetGrdStr(Grd25, Row, 0) + Sm.GetGrdStr(Grd25, Row, 4) + Sm.GetGrdStr(Grd25, Row, 6),
                    ItCode + SODocNo + SoDNo))
                {
                    QtyPackaging += Sm.GetGrdDec(Grd25, Row, 10);
                    QtySales += Sm.GetGrdDec(Grd25, Row, 11);
                    QtyInventory += Sm.GetGrdDec(Grd25, Row, 13);
                }
            }

            for (int Row = 0; Row < Grd26.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd26, Row, 0).Length != 0 &&
                    Sm.CompareStr(Sm.GetGrdStr(Grd26, Row, 0) + Sm.GetGrdStr(Grd26, Row, 4) + Sm.GetGrdStr(Grd26, Row, 6),
                    ItCode + SODocNo + SoDNo))
                {
                    QtyPackaging += Sm.GetGrdDec(Grd26, Row, 10);
                    QtySales += Sm.GetGrdDec(Grd26, Row, 11);
                    QtyInventory += Sm.GetGrdDec(Grd26, Row, 13);
                }
            }

            for (int Row = 0; Row < Grd27.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd27, Row, 0).Length != 0 &&
                    Sm.CompareStr(Sm.GetGrdStr(Grd27, Row, 0) + Sm.GetGrdStr(Grd27, Row, 4) + Sm.GetGrdStr(Grd27, Row, 6),
                    ItCode + SODocNo + SoDNo))
                {
                    QtyPackaging += Sm.GetGrdDec(Grd27, Row, 10);
                    QtySales += Sm.GetGrdDec(Grd27, Row, 11);
                    QtyInventory += Sm.GetGrdDec(Grd27, Row, 13);
                }
            }

            for (int Row = 0; Row < Grd28.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd28, Row, 0).Length != 0 &&
                    Sm.CompareStr(Sm.GetGrdStr(Grd28, Row, 0) + Sm.GetGrdStr(Grd28, Row, 4) + Sm.GetGrdStr(Grd28, Row, 6),
                    ItCode + SODocNo + SoDNo))
                {
                    QtyPackaging += Sm.GetGrdDec(Grd28, Row, 10);
                    QtySales += Sm.GetGrdDec(Grd28, Row, 11);
                    QtyInventory += Sm.GetGrdDec(Grd28, Row, 13);
                }
            }

            for (int Row = 0; Row < Grd29.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd29, Row, 0).Length != 0 &&
                    Sm.CompareStr(Sm.GetGrdStr(Grd29, Row, 0) + Sm.GetGrdStr(Grd29, Row, 4) + Sm.GetGrdStr(Grd29, Row, 6),
                    ItCode + SODocNo + SoDNo))
                {
                    QtyPackaging += Sm.GetGrdDec(Grd29, Row, 10);
                    QtySales += Sm.GetGrdDec(Grd29, Row, 11);
                    QtyInventory += Sm.GetGrdDec(Grd29, Row, 13);
                }
            }

            for (int Row = 0; Row < Grd30.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd30, Row, 0).Length != 0 &&
                    Sm.CompareStr(Sm.GetGrdStr(Grd30, Row, 0) + Sm.GetGrdStr(Grd30, Row, 4) + Sm.GetGrdStr(Grd30, Row, 6),
                    ItCode + SODocNo + SoDNo))
                {
                    QtyPackaging += Sm.GetGrdDec(Grd30, Row, 10);
                    QtySales += Sm.GetGrdDec(Grd30, Row, 11);
                    QtyInventory += Sm.GetGrdDec(Grd30, Row, 13);
                }
            }

            for (int Row = 0; Row < Grd31.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd31, Row, 0).Length != 0 &&
                    Sm.CompareStr(Sm.GetGrdStr(Grd31, Row, 0) + Sm.GetGrdStr(Grd31, Row, 4) + Sm.GetGrdStr(Grd31, Row, 6),
                    ItCode + SODocNo + SoDNo))
                {
                    QtyPackaging += Sm.GetGrdDec(Grd31, Row, 10);
                    QtySales += Sm.GetGrdDec(Grd31, Row, 11);
                    QtyInventory += Sm.GetGrdDec(Grd31, Row, 13);
                }
            }

            for (int Row = 0; Row < Grd32.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd32, Row, 0).Length != 0 &&
                    Sm.CompareStr(Sm.GetGrdStr(Grd32, Row, 0) + Sm.GetGrdStr(Grd32, Row, 4) + Sm.GetGrdStr(Grd32, Row, 6),
                    ItCode + SODocNo + SoDNo))
                {
                    QtyPackaging += Sm.GetGrdDec(Grd32, Row, 10);
                    QtySales += Sm.GetGrdDec(Grd32, Row, 11);
                    QtyInventory += Sm.GetGrdDec(Grd32, Row, 13);
                }
            }

            for (int Row = 0; Row < Grd33.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd33, Row, 0).Length != 0 &&
                    Sm.CompareStr(Sm.GetGrdStr(Grd33, Row, 0) + Sm.GetGrdStr(Grd33, Row, 4) + Sm.GetGrdStr(Grd33, Row, 6),
                    ItCode + SODocNo + SoDNo))
                {
                    QtyPackaging += Sm.GetGrdDec(Grd33, Row, 10);
                    QtySales += Sm.GetGrdDec(Grd33, Row, 11);
                    QtyInventory += Sm.GetGrdDec(Grd33, Row, 13);
                }
            }

            for (int Row = 0; Row < Grd34.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd34, Row, 0).Length != 0 &&
                    Sm.CompareStr(Sm.GetGrdStr(Grd34, Row, 0) + Sm.GetGrdStr(Grd34, Row, 4) + Sm.GetGrdStr(Grd34, Row, 6),
                    ItCode + SODocNo + SoDNo))
                {
                    QtyPackaging += Sm.GetGrdDec(Grd34, Row, 10);
                    QtySales += Sm.GetGrdDec(Grd34, Row, 11);
                    QtyInventory += Sm.GetGrdDec(Grd34, Row, 13);
                }
            }

            for (int Row = 0; Row < Grd35.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd35, Row, 0).Length != 0 &&
                    Sm.CompareStr(Sm.GetGrdStr(Grd35, Row, 0) + Sm.GetGrdStr(Grd35, Row, 4) + Sm.GetGrdStr(Grd35, Row, 6),
                    ItCode + SODocNo + SoDNo))
                {
                    QtyPackaging += Sm.GetGrdDec(Grd35, Row, 10);
                    QtySales += Sm.GetGrdDec(Grd35, Row, 11);
                    QtyInventory += Sm.GetGrdDec(Grd35, Row, 13);
                }
            }




            for (int Row2 = 0; Row2 < Grd1.Rows.Count - 1; Row2++)
            {
                if (
                    Sm.GetGrdStr(Grd1, Row2, 1).Length != 0 &&
                    Sm.CompareStr(ItCode + SODocNo + SoDNo,
                    Sm.GetGrdStr(Grd1, Row2, 1) + Sm.GetGrdStr(Grd1, Row2, 5) + Sm.GetGrdStr(Grd1, Row2, 7)))
                {
                    Grd1.Cells[Row2, 10].Value = QtyPackaging;
                    Grd1.Cells[Row2, 13].Value = QtySales;
                    Grd1.Cells[Row2, 16].Value = QtyInventory;
                    Grd1.Cells[Row2, 11].Value = Sm.GetGrdDec(Grd1, Row2, 9) - Sm.GetGrdDec(Grd1, Row2, 10);
                    Grd1.Cells[Row2, 14].Value = Sm.GetGrdDec(Grd1, Row2, 12) - Sm.GetGrdDec(Grd1, Row2, 13);
                }
            }

        }
       
        private void SetLueBL(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select OptCode As Col1, OptDesc As Col2 From TblOption Where OptCat = 'BLRequirement'  Order By OptDesc",
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public void ComputeUom2()
        {
            decimal QtyNConvert = 0m, QtyGConvert = 0m;

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdDec(Grd1, Row, 10) != 0 && Sm.GetGrdStr(Grd1, Row, 8).Length != 0)
                {
                    try
                    {
                        QtyNConvert = (Decimal.Parse(Sm.GetValue("Select NW From TblItemPackagingUnit Where ItCode = '" + Sm.GetGrdStr(Grd1, Row, 1) + "' And UomCode = '" + Sm.GetGrdStr(Grd1, Row, 8) + "' ")));
                        QtyGConvert = (Decimal.Parse(Sm.GetValue("Select GW From TblItemPackagingUnit Where ItCode = '" + Sm.GetGrdStr(Grd1, Row, 1) + "' And UomCode = '" + Sm.GetGrdStr(Grd1, Row, 8) + "' ")));

                        Grd1.Cells[Row, 18].Value = Sm.GetGrdDec(Grd1, Row, 10) * QtyGConvert;
                        Grd1.Cells[Row, 19].Value = Sm.GetGrdDec(Grd1, Row, 10) * QtyNConvert;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("" + ex + "");
                    }
                }
            }
        }

        public void ComputeUom(iGrid GrdXX)
        {
            decimal QtyPackaging = 0m, QtyUomConvert12 = 0m, QtyUomConvert21 = 0m,  QtyNConvert = 0m, QtyGConvert = 0m;
            string UomSales, UomSales1, UomInv, QueryQtyUomConvert12, QueryQtyUomConvert21, QueryQtyNConvert, QueryQtyGConvert;

            int mCollumnChecked = 7;
            if (!mIsPalletInPLEditable) mCollumnChecked = 8;

            for (int Row = 0; Row < GrdXX.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(GrdXX, Row, mCollumnChecked).Length != 0)
                {
                    try
                    {
                        QtyPackaging = Sm.GetGrdDec(GrdXX, Row, 10);
                        UomSales = Sm.GetGrdStr(GrdXX, Row, 12);
                        UomInv = Sm.GetGrdStr(GrdXX, Row, 14);
                        UomSales1 = Sm.GetValue("Select SalesUomCode From TblItem Where ItCode = '" + Sm.GetGrdStr(GrdXX, Row, 0) + "' ");
                        
                        QueryQtyUomConvert12 =  Sm.GetValue("Select Qty From TblItemPackagingUnit Where ItCode = '" + Sm.GetGrdStr(GrdXX, Row, 0) + "' And UomCode = '" + Sm.GetGrdStr(GrdXX, Row, 9) + "' ");
                        QueryQtyUomConvert21 =  Sm.GetValue("Select Qty2 From TblItemPackagingUnit Where ItCode = '" + Sm.GetGrdStr(GrdXX, Row, 0) + "' And UomCode = '" + Sm.GetGrdStr(GrdXX, Row, 9) + "' ");

                        QtyUomConvert12 = QueryQtyUomConvert12.Length == 0 ? 0 : Decimal.Parse(QueryQtyUomConvert12);
                        QtyUomConvert21 = QueryQtyUomConvert21.Length == 0 ? 0 : Decimal.Parse(QueryQtyUomConvert21);

                        if (UomSales1 == UomSales)
                        {
                            GrdXX.Cells[Row, 11].Value = QtyPackaging * QtyUomConvert12;
                           
                            if (UomSales == UomInv)
                            {
                                GrdXX.Cells[Row, 13].Value = Sm.GetGrdDec(GrdXX, Row, 11);
                            }
                            else
                            {
                                if (UomInv == UomSales1)
                                {
                                    GrdXX.Cells[Row, 13].Value = QtyPackaging * QtyUomConvert12;
                                }
                                else
                                {
                                    GrdXX.Cells[Row, 13].Value = QtyPackaging * QtyUomConvert21;
                                }
                            }
                        }
                        else
                        {
                            GrdXX.Cells[Row, 11].Value = QtyPackaging * QtyUomConvert21;
                           
                            if (UomSales == UomInv)
                            {
                                GrdXX.Cells[Row, 13].Value = Sm.GetGrdDec(GrdXX, Row, 11);
                            }
                            else
                            {
                                if (UomInv == UomSales1)
                                {
                                    GrdXX.Cells[Row, 13].Value = QtyPackaging * QtyUomConvert12;
                                }
                                else
                                {
                                    GrdXX.Cells[Row, 13].Value = QtyPackaging * QtyUomConvert21;
                                }
                            }
                        }

                        QueryQtyNConvert = Sm.GetValue("Select NW From TblItemPackagingUnit Where ItCode = '" + Sm.GetGrdStr(GrdXX, Row, 0) + "' And UomCode = '" + Sm.GetGrdStr(GrdXX, Row, 9) + "' ");
                        QueryQtyGConvert = Sm.GetValue("Select GW From TblItemPackagingUnit Where ItCode = '" + Sm.GetGrdStr(GrdXX, Row, 0) + "' And UomCode = '" + Sm.GetGrdStr(GrdXX, Row, 9) + "' ");

                        QtyNConvert = QueryQtyNConvert.Length == 0 ? 0 : Decimal.Parse(QueryQtyNConvert);
                        QtyGConvert = QueryQtyGConvert.Length == 0 ? 0 : Decimal.Parse(QueryQtyGConvert);

                        GrdXX.Cells[Row, 15].Value = Sm.GetGrdDec(GrdXX, Row, 10) * QtyGConvert;
                        GrdXX.Cells[Row, 16].Value = Sm.GetGrdDec(GrdXX, Row, 10) * QtyNConvert;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("" + ex + "");
                    }
                }
            }
        }

        public void ComputeUom4ShowData(iGrid GrdXX)
        {
            decimal QtyNConvert = 0m, QtyGConvert = 0m;

            for (int Row = 0; Row < GrdXX.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdDec(GrdXX, Row, 10) != 0 && Sm.GetGrdStr(GrdXX, Row, 8).Length != 0)
                {
                    try
                    {
                      
                        string NWcon = Sm.GetValue("Select NW From TblItemPackagingUnit Where ItCode = '" + Sm.GetGrdStr(GrdXX, Row, 0) + "' And UomCode = '" + Sm.GetGrdStr(GrdXX, Row, 9) + "' ");
                        string GWCon = Sm.GetValue("Select GW From TblItemPackagingUnit Where ItCode = '" + Sm.GetGrdStr(GrdXX, Row, 0) + "' And UomCode = '" + Sm.GetGrdStr(GrdXX, Row, 9) + "' ");

                        if (NWcon.Length > 0)
                        {
                            QtyNConvert = Decimal.Parse(NWcon);
                        }
                        else
                        {
                            QtyNConvert = 0;
                        }

                        if (GWCon.Length > 0)
                        {
                            QtyGConvert = Decimal.Parse(GWCon);
                        }
                        else
                        {
                            QtyGConvert = 0;
                        }

                        if (Sm.GetGrdDec(GrdXX, Row, 15) == 0)
                        {
                            GrdXX.Cells[Row, 15].Value = Sm.GetGrdDec(GrdXX, Row, 10) * QtyGConvert;
                        }

                        if (Sm.GetGrdDec(GrdXX, Row, 16) == 0)
                        {
                            GrdXX.Cells[Row, 16].Value = Sm.GetGrdDec(GrdXX, Row, 10) * QtyNConvert;
                        }

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("" + ex + "");
                    }
                }
            }
        }



        internal void RecomputeBalance()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, B.DNo, ");
            SQL.AppendLine("(B.QtyPackagingUnit-IfNull(C.QtyPackagingUnit, 0)) As OutstandingQtyPackagingUnit,  ");
            SQL.AppendLine("(B.Qty-IfNull(C.Qty, 0)) As OutstandingQty ");
            SQL.AppendLine("From TblSOHdr A ");
            SQL.AppendLine("Inner Join TblSODtl B On A.DocNo=B.DocNo and B.ProcessInd<>'F' ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.SODocNo As DocNo, T2.SODNo As DNo,  ");
            SQL.AppendLine("    Sum(IfNull(T2.QtyPackagingUnit, 0)) As QtyPackagingUnit, Sum(IfNull(T2.Qty, 0)) As Qty  ");
            SQL.AppendLine("    From TblPLHdr T1  ");
            SQL.AppendLine("    Inner Join TblPLDtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    Inner Join TblSOHdr T3 On T2.SODocNo=T3.DocNo And T3.CancelInd='N' And T3.Status Not In ('M', 'F') And T3.CtCode=@CtCode ");
            SQL.AppendLine("    Inner Join TblSODtl T4 On T2.SODocNo=T4.DocNo And T2.SODNo=T4.DNo and T4.ProcessInd<>'F' ");

            SQL.AppendLine("    Where Concat(T2.SODocNo, T2.SODNo) Not In (" + GetSelectedSO2() + ")  ");

            SQL.AppendLine("    Group By T2.SODocNo, T2.SODNo ");
            SQL.AppendLine(") C On B.DocNo=C.DocNo And B.DNo=C.DNo ");
            SQL.AppendLine("Where A.CancelInd='N'  ");
            SQL.AppendLine("And A.Status Not In ('M', 'F') ");
            SQL.AppendLine("And A.CtCode=@CtCode ");
            SQL.AppendLine("And Locate(Concat('##', A.DocNo, B.DNo, '##'), @SelectedSO)>0 ");
            SQL.AppendLine("Order By A.DocNo, B.DNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@SelectedSO", GetSelectedSO());
            Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                cm.CommandTimeout = 100;
                using (var dr = cm.ExecuteReader())
                {
                    var c = Sm.GetOrdinal(dr,
                        new string[] 
                        { 
                            //0
                           "DocNo",  

                            //1-3
                           "DNo", "OutstandingQtyPackagingUnit", "OutstandingQty"
                        }
                        );
                    if (dr.HasRows)
                    {
                        Grd1.ProcessTab = true;
                        Grd1.BeginUpdate();
                        while (dr.Read())
                        {
                            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                            {
                                if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 5), Sm.DrStr(dr, 0)) &&
                                    Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 7), Sm.DrStr(dr, 1)))
                                {
                                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 9, 2);
                                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 12, 3);
                                    break;
                                }
                            }
                        }
                        Grd1.EndUpdate();
                    }
                    dr.Close();
                    //ComputeUom2();
                }
            }
        }

        private string GetSelectedSO()
        {
            var SQL = "";
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 5).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL += "##" + Sm.GetGrdStr(Grd1, Row, 5) + Sm.GetGrdStr(Grd1, Row, 7) + "##";
                    }
                }
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        internal string GetSelectedSO2()
        {
            var SQL = "";
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 5).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL +=
                            "'" +
                            Sm.GetGrdStr(Grd1, Row, 5) + Sm.GetGrdStr(Grd1, Row, 7) +
                            "'";
                    }
                }
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }

        public void SetProcessInd(iGrid GrdXX) //set processind didetail O jika masih kosong 
        {
            for (int Row = 0; Row < GrdXX.Rows.Count - 1; Row++)
            {
                if(Sm.GetGrdStr(GrdXX, Row, 18).Length == 0)
                {
                    GrdXX.Cells[Row, 18].Value = "O";
                }

                if (Sm.GetGrdStr(GrdXX, Row, 19).Length == 0)
                {
                    GrdXX.Cells[Row, 19].Value = "O";
                }
            }
        }

        private void SetLueCtCode(ref LookUpEdit Lue, string CtCode)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            
            if (CtCode.Length == 0)
            {
                SQL.AppendLine("Select Distinct A.CtCode As Col1, B.CtName As Col2 ");
                SQL.AppendLine("From ( ");
                SQL.AppendLine("Select Distinct CtCode ");
                SQL.AppendLine("From TblSOHdr ");
                SQL.AppendLine("Where OverSeaInd = 'Y' And Status = 'O' And CancelInd= 'N' ");
                SQL.AppendLine("Union All ");
                SQL.AppendLine("Select Distinct CtCode  ");
                SQL.AppendLine("From TblSP ");
                SQL.AppendLine(") A ");
                SQL.AppendLine("Inner Join TblCustomer B On A.CtCode= B.CtCode And B.ActInd = 'Y' ");
                SQL.AppendLine("Order By B.CtName");
            }
            else
            {
                SQL.AppendLine("Select CtCode As Col1, CtName As Col2 ");
                SQL.AppendLine("From TblCustomer Where CtCode=@CtCode;");
                Sm.CmParam<String>(ref cm, "@CtCode", CtCode);
            }
            cm.CommandText = SQL.ToString();
            Sm.SetLue2(
                ref Lue, ref cm,
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (CtCode.Length > 0) Sm.SetLue(LueCtCode, CtCode);
        }

        private void LueRequestEdit(
          iGrid Grd,
          DevExpress.XtraEditors.LookUpEdit Lue,
          ref iGCell fCell,
          ref bool fAccept,
          TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, 0).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, 0));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        private void SetLueNotify(ref LookUpEdit Lue, string CtCode, string NotifyParty)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select Distinct Col1 From (");
            SQL.AppendLine("    Select NotifyParty As Col1 From TblCustomerNotifyParty ");
            SQL.AppendLine("    Where CtCode=@CtCode ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select @NotifyParty As Col1 ");
            SQL.AppendLine(") T Where Col1<>'' Order By Col1;");

            Sm.CmParam<String>(ref cm, "@NotifyParty", NotifyParty);
            Sm.CmParam<String>(ref cm, "@CtCode", CtCode);

            cm.CommandText = SQL.ToString();

            Sm.SetLue1(ref Lue, ref cm, "Notify Party");
        }

        private void SetLueSize(ref DXE.LookUpEdit Lue)
        {
            Sm.SetLue2(
               ref Lue,
               "Select  OptCode As Col1, OptDesc As Col2 From TblOption  " +
               "Where OptCat = 'ContainerSize' Order By OptCode ",
               0, 35, false, true, "Code", "Size", "Col2", "Col1");
        }

        private void SetLueItCode(ref DXE.LookUpEdit Lue, string CtCode, string SIDocNo)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select E.ItCode As Col1, F.ItName As Col2, ");
                SQL.AppendLine("A.DocNo As Col3, Concat(A.DocNo, B.DNo) As Col4,  ");
                SQL.AppendLine("Concat(Convert(Format(B.QtyPackagingUnit, 2) Using utf8), ' ', B.PackagingUnitUomCode) As Col5  ");
                SQL.AppendLine("From TblSOHdr A ");
                SQL.AppendLine("Inner Join TblSODtl B On A.DocNo=B.DocNo and B.ProcessInd<>'F' ");
                SQL.AppendLine("Inner Join TblCtQtDtl C On A.CtQtDocNo=C.DocNo And B.CtQtDNo=C.DNo ");
                SQL.AppendLine("Inner Join TblItemPriceHdr D On C.ItemPriceDocNo=D.DOcNo ");
                SQL.AppendLine("Inner Join TblItemPriceDtl E On C.ItemPriceDocNo=E.DOcNo And C.ItemPriceDNo = E.DNo ");
                SQL.AppendLine("Inner Join TblItem F On E.ItCode = F.ItCode ");
                SQL.AppendLine("Inner join TblSIDtl G On A.DocNo = G.SODocNo And B.Dno = G.SODno ");
                SQL.AppendLine("Where A.CancelInd='N' And A.OverSeaInd = 'Y' ");
                SQL.AppendLine("And A.Status Not In ('M', 'F') ");
                SQL.AppendLine("And A.CtCode='"+CtCode+"' And G.DocNo = '"+SIDocNo+"'   ");
            
                Sm.SetLue5(
                    ref Lue,
                    SQL.ToString(),
                    100, 200, 130, 0, 180, true, true, true, false, true, "Item Code", "Item Name", "SO", "DNo", "Quantity", "Col2", "Col4");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        public static string GetNumber(string palet)
        {
            string number = string.Empty;
            for (int ind = 0; ind < palet.Length; ind++)
            {
                if (Char.IsNumber(palet[ind]) == true)
                {
                    number = number + palet[ind];
                }

            }
            return number;
        }

        private void ReListProcessInd(int SectionNo)
        {
            var IGrid  = new iGrid();

            switch(SectionNo)
            {
                case 1 :
                    IGrid = Grd11;
                    break;
                case 2: 
                    IGrid = Grd12;
                    break;
                case 3 :
                    IGrid = Grd13;
                    break;
                case 4: 
                    IGrid = Grd14;
                    break;
                case 5 :
                    IGrid = Grd15;
                    break;
                case 6: 
                    IGrid = Grd16;
                    break;
                case 7 :
                    IGrid = Grd17;
                    break;
                case 8: 
                    IGrid = Grd18;
                    break;
                case 9 :
                    IGrid = Grd19;
                    break;
                case 10: 
                    IGrid = Grd20;
                    break;
                case 11 :
                    IGrid = Grd21;
                    break;
                case 12: 
                    IGrid = Grd22;
                    break;
                case 13 :
                    IGrid = Grd23;
                    break;
                case 14: 
                    IGrid = Grd24;
                    break;
                case 15 :
                    IGrid = Grd25;
                    break;
                case 16: 
                    IGrid = Grd26;
                    break;
                case 17: 
                    IGrid = Grd27;
                    break;
                case 18: 
                    IGrid = Grd28;
                    break;
                case 19 :
                    IGrid = Grd29;
                    break;
                case 20: 
                    IGrid = Grd30;
                    break;
                case 21 :
                    IGrid = Grd31;
                    break;
                case 22: 
                    IGrid = Grd32;
                    break;
                case 23: 
                    IGrid = Grd33;
                    break;
                case 24 :
                    IGrid = Grd34;
                    break;
                case 25: 
                    IGrid = Grd35;
                    break;


            }
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.SectionNo, SODocNo, A.SODno, A.ProcessInd From TblPLDtl A  ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("And A.SectionNo=@SectionNo ");
            SQL.AppendLine("Order By A.DNo, A.ItCode ");
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText = SQL.ToString()
                };

                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cm, "@SectionNo", SectionNo.ToString());
                

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                    { 
                        //0
                        "SectionNo",
 
                        //1-2
                        "SODocNo", "SODno", "ProcessInd"
                    });
                
                if (dr.HasRows)
                {
                    IGrid.BeginUpdate();
                    while (dr.Read())
                    {
                        for (int Row = 0; Row < IGrid.Rows.Count - 1; Row++)
                        {
                            if (Sm.CompareStr(Sm.GetGrdStr(IGrid, Row, 4), Sm.DrStr(dr, 1)) &&
                                Sm.CompareStr(Sm.GetGrdStr(IGrid, Row, 6), Sm.DrStr(dr, 2))
                                )
                            {
                                IGrid.Cells[Row, 19].Value = Sm.DrStr(dr, 3);
                                //Sm.SetGrdValue("S", IGrid, dr, c, Row, 19, 3);
                            }
                        }
                    }
                    IGrid.EndUpdate();
                }
                dr.Close();
            }
        }

        private void CheckStatusPL()
        {
            for (int Sect = 1; Sect <= 25; Sect++)
            {
                ReListProcessInd(Sect);
            }
        }

        private bool IsProcessDetailNotValid() //bandingin status awal dgn status terbaru
        {
            for (int Section = 1; Section <= 25; Section++)
            {
                var IGrid = new iGrid();
                switch (Section)
                {
                    case 1:
                        IGrid = Grd11;
                        break;
                    case 2:
                        IGrid = Grd12;
                        break;
                    case 3:
                        IGrid = Grd13;
                        break;
                    case 4:
                        IGrid = Grd14;
                        break;
                    case 5:
                        IGrid = Grd15;
                        break;
                    case 6:
                        IGrid = Grd16;
                        break;
                    case 7:
                        IGrid = Grd17;
                        break;
                    case 8:
                        IGrid = Grd18;
                        break;
                    case 9:
                        IGrid = Grd19;
                        break;
                    case 10:
                        IGrid = Grd20;
                        break;
                    case 11:
                        IGrid = Grd21;
                        break;
                    case 12:
                        IGrid = Grd22;
                        break;
                    case 13:
                        IGrid = Grd23;
                        break;
                    case 14:
                        IGrid = Grd24;
                        break;
                    case 15:
                        IGrid = Grd25;
                        break;
                    case 16:
                        IGrid = Grd26;
                        break;
                    case 17:
                        IGrid = Grd27;
                        break;
                    case 18:
                        IGrid = Grd28;
                        break;
                    case 19:
                        IGrid = Grd29;
                        break;
                    case 20:
                        IGrid = Grd30;
                        break;
                    case 21:
                        IGrid = Grd31;
                        break;
                    case 22:
                        IGrid = Grd32;
                        break;
                    case 23:
                        IGrid = Grd33;
                        break;
                    case 24:
                        IGrid = Grd34;
                        break;
                    case 25:
                        IGrid = Grd35;
                        break;
                } 
                for (int rowZ = 0; rowZ < IGrid.Rows.Count-1; rowZ++)
                {
                    if (Sm.GetGrdStr(IGrid, rowZ, 19) != Sm.GetGrdStr(IGrid, rowZ, 18) && TxtDocNo.Text.Length !=0 )
                    {
                        Sm.StdMsg(mMsgType.Warning, "This container '" + Section + "' has been processed.");
                        return true;
                    }
                }
            }
            return false;
        }

        private void ParPrint()
        {
            string Doctitle = Sm.GetParameter("DocTitle");
            string SMark = "N";
            if (Sm.IsTxtEmpty(TxtDocNo, "Document number", false) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;
           
            #region Printout iok
            if (Doctitle == "IOK")
            {
                var l = new List<PLHdr>();
                var ldtl = new List<PLDtl>();
                var ldtl2 = new List<PLDtl2>();
                var ldtl3 = new List<PLDtl3>();
                var ldtl4 = new List<PLDtl4>();
               

                string[] TableName = { "PLHdr", "PLDtl", "PLDtl2", "PLDtl3", "PLDtl4", "PLDtl5" };

                List<IList> myLists = new List<IList>();
                var cm = new MySqlCommand();
                if (ChkShippingMark.Checked == true)
                {
                    SMark = "Y";
                }

                #region Header
                var SQL = new StringBuilder();
                SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyAddressCity', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyPhone', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle5') As 'CompanyFax', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='Shipper1') As 'Shipper1', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='Shipper2') As 'Shipper2', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='Shipper3') As 'Shipper3', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='Shipper4') As 'Shipper4', ");
                SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='SIWeightUom') As 'UomNWGW', ");
                SQL.AppendLine("A.DocNo, A.LocalDocno, DATE_FORMAT(A.DocDt,'%d %M %Y') As DocDt, G.CtName As Customer, A.Account, C.SpCtNotifyparty, ");
                SQL.AppendLine("A.invoiceNo, A.SalesContractNo, A.LCNo, DATE_FORMAT(A.LCDt,'%M %d, %Y') As LCDt, ");
                SQL.AppendLine("A.Issued1, A.Issued2, A.Issued3, A.Issued4, A.ToOrder1, A.ToOrder2, A.ToOrder3, A.ToOrder4, ");
                SQL.AppendLine("A.Remark, A.Remark2, A.Remark3, D.PortName As PortLoading, E.PortName As PortDischarge, ");
                SQL.AppendLine("DATE_FORMAT(F.StfDt,'%d %M %Y') As StfDt, F.SpName, A.SM1, F.CtCode,  ");
                SQL.AppendLine("Concat(IfNull(H.ParValue, ''), F.CtCode, '.JPG') As ShippingMark ");
                SQL.AppendLine("From TblPLHdr A ");
                SQL.AppendLine("Inner Join TblPLDtl B On A.DocNo = B.DocNo ");
                SQL.AppendLine("Inner Join TblSIHdr C On A.SIDocno = C.DocNo ");
                SQL.AppendLine("Inner Join TblPort D On C.SpPortCode1 = D.PortCode ");
                SQL.AppendLine("Inner Join TblPort E On C.SpPortCode2 = E.PortCode ");
                SQL.AppendLine("Inner Join TblSP F On C.SpDocNo = F.DocNo ");
                SQL.AppendLine("Left Join TblCustomer G On F.CtCode = G.CtCode ");
                SQL.AppendLine("Left Join TblParameter H On H.ParCode = 'ImgFileSignature' ");
                SQL.AppendLine("Where A.DocNo=@DocNo ");

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = SQL.ToString();
                    Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                    Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] 
                        {
                        //0
                         "CompanyLogo",

                         //1-5
                         "CompanyName",
                         "CompanyAddress",
                         "CompanyAddressCity",
                         "CompanyPhone",
                         "DocNo",
                         //6-10
                         "DocDt",
                         "Account",
                         "SpCtNotifyParty",
                         "InvoiceNo",
                         "SalesContractNo",
                         //11-15
                         "LCNo",
                         "LCDt",
                         "Issued1",
                         "Issued2",
                         "Issued3",
                         //16-20
                         "Issued4",
                         "ToOrder1",
                         "ToOrder2",
                         "ToOrder3",
                         "ToOrder4",
                         //21-25
                         "Remark",
                         "Remark2",
                         "PortLoading",
                         "PortDischarge",
                         "StfDt",
                         //26-30
                         "UomNWGW",
                         "SpName",
                         "CompanyFax",
                         "LocalDocNo",
                         "Customer",
                         //31-34
                         "Remark3",
                         "SM1",
                         "CtCode",
                         "ShippingMark"
                        });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            l.Add(new PLHdr()
                            {
                                CompanyLogo = Sm.DrStr(dr, c[0]),

                                CompanyName = Sm.DrStr(dr, c[1]),
                                CompanyAddress = Sm.DrStr(dr, c[2]),
                                CompanyAddressCity = Sm.DrStr(dr, c[3]),
                                CompanyPhone = Sm.DrStr(dr, c[4]),
                                DocNo = Sm.DrStr(dr, c[5]),
                                DocDt = Sm.DrStr(dr, c[6]),

                                Account = Sm.DrStr(dr, c[7]),
                                NotifyParty = Sm.DrStr(dr, c[8]),
                                Invoice = Sm.DrStr(dr, c[9]),
                                SalesContract = Sm.DrStr(dr, c[10]),
                                LCNo = Sm.DrStr(dr, c[11]),

                                LCDate = Sm.DrStr(dr, c[12]),
                                Issued1 = Sm.DrStr(dr, c[13]),
                                Issued2 = Sm.DrStr(dr, c[14]),
                                Issued3 = Sm.DrStr(dr, c[15]),
                                Issued4 = Sm.DrStr(dr, c[16]),
                                ToOrder1 = Sm.DrStr(dr, c[17]),
                                ToOrder2 = Sm.DrStr(dr, c[18]),
                                ToOrder3 = Sm.DrStr(dr, c[19]),
                                ToOrder4 = Sm.DrStr(dr, c[20]),
                                Remark = Sm.DrStr(dr, c[21]),
                                Remark2 = Sm.DrStr(dr, c[22]),
                                PortLoading = Sm.DrStr(dr, c[23]),
                                PortDischarge = Sm.DrStr(dr, c[24]),
                                Stufing = Sm.DrStr(dr, c[25]),
                                UomNWGW = Sm.DrStr(dr, c[26]),
                                SpName = Sm.DrStr(dr, c[27]),
                                SM = SMark,
                                CompanyFax = Sm.DrStr(dr, c[28]),
                                LocalDocNo = Sm.DrStr(dr, c[29]),
                                Customer = Sm.DrStr(dr, c[30]),
                                Remark3 = Sm.DrStr(dr, c[31]),

                                SM1 = Sm.DrStr(dr, c[32]),
                                CtCode = Sm.DrStr(dr, c[33]),
                                ShippingMark = Sm.DrStr(dr, c[34]),
                                IsSO2UseActualItem = mIsSO2UseActualItem,
                                PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
                            });
                        }
                    }
                    dr.Close();
                }
                myLists.Add(l);
                #endregion

                #region Detail
                var cmDtl = new MySqlCommand();

                var SQLDtl = new StringBuilder();
                using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
                {
                    string thc = Sm.GetValue("Select Concat(Right(OptDesc, 1),' ', 'HC') From TblOption Where OptCat = 'ContainerSize' Limit 1;");
                    cnDtl.Open();
                    cmDtl.Connection = cnDtl;

                    SQLDtl.AppendLine("Select A.DocNo, A2.Dno, A2.SectionNo, A2.ItCode, B.ForeignName As ItName, ");
                    SQLDtl.AppendLine("A2.Qty, G.PriceUomCode, Round(A2.QtyInventory, 4) As QtyInventory, B.InventoryUomCode, A2.PLNo, ifnull(A2.QtyPL, 0) As QtyPL,  ");
                    SQLDtl.AppendLine("(A2.QtyPackagingUnit * H.NW) As NW, (Select Parvalue From Tblparameter Where parCode = 'SIWeightUom') As NGuom,  ");
                    SQLDtl.AppendLine("(A2.QtyPackagingUnit * H.GW) As GW, F3.userName,  ");
                    SQLDtl.AppendLine("Case ");
                    SQLDtl.AppendLine("When A2.SectionNo = '1' Then Concat('Container', ': ', A.Cnt1, '   ', ifnull(concat('1x',A.Size1, @thc), ''), '   ', 'Seal', ': ', A.seal1) ");
                    SQLDtl.AppendLine("When A2.SectionNo = '2' Then Concat('Container', ': ', A.Cnt2, '   ', ifnull(concat('1x',A.Size2, @thc), ''), '   ', 'Seal', ': ', A.seal2) ");
                    SQLDtl.AppendLine("When A2.SectionNo = '3' Then Concat('Container', ': ', A.Cnt3, '   ', ifnull(concat('1x',A.Size3, @thc), ''), '   ', 'Seal', ': ', A.seal3) ");
                    SQLDtl.AppendLine("When A2.SectionNo = '4' Then Concat('Container', ': ', A.Cnt4, '   ', ifnull(concat('1x',A.Size4, @thc), ''), '   ', 'Seal', ': ', A.seal4) ");
                    SQLDtl.AppendLine("When A2.SectionNo = '5' Then Concat('Container', ': ', A.Cnt5, '   ', ifnull(concat('1x',A.Size5, @thc), ''), '   ', 'Seal', ': ', A.seal5) ");
                    SQLDtl.AppendLine("When A2.SectionNo = '6' Then Concat('Container', ': ', A.Cnt6, '   ', ifnull(concat('1x',A.Size6, @thc), ''), '   ', 'Seal', ': ', A.seal6) ");
                    SQLDtl.AppendLine("When A2.SectionNo = '7' Then Concat('Container', ': ', A.Cnt7, '   ', ifnull(concat('1x',A.Size7, @thc), ''), '   ', 'Seal', ': ', A.seal7) ");
                    SQLDtl.AppendLine("When A2.SectionNo = '8' Then Concat('Container', ': ', A.Cnt8, '   ', ifnull(concat('1x',A.Size8, @thc), ''), '   ', 'Seal', ': ', A.seal8) ");
                    SQLDtl.AppendLine("When A2.SectionNo = '9' Then Concat('Container', ': ', A.Cnt9, '   ', ifnull(concat('1x',A.Size9, @thc), ''), '   ', 'Seal', ': ', A.seal9) ");
                    SQLDtl.AppendLine("When A2.SectionNo = '10' Then Concat('Container', ': ', A.Cnt10, '   ', ifnull(concat('1x',A.Size10, @thc), ''), '   ', 'Seal', ': ', A.seal10) ");
                    SQLDtl.AppendLine("When A2.SectionNo = '11' Then Concat('Container', ': ', A.Cnt11, '   ', ifnull(concat('1x',A.Size11, @thc), ''), '   ', 'Seal', ': ', A.seal11) ");
                    SQLDtl.AppendLine("When A2.SectionNo = '12' Then Concat('Container', ': ', A.Cnt12, '   ', ifnull(concat('1x',A.Size12, @thc), ''), '   ', 'Seal', ': ', A.seal12) ");
                    SQLDtl.AppendLine("When A2.SectionNo = '13' Then Concat('Container', ': ', A.Cnt13, '   ', ifnull(concat('1x',A.Size13, @thc), ''), '   ', 'Seal', ': ', A.seal13) ");
                    SQLDtl.AppendLine("When A2.SectionNo = '14' Then Concat('Container', ': ', A.Cnt14, '   ', ifnull(concat('1x',A.Size14, @thc), ''), '   ', 'Seal', ': ', A.seal14) ");
                    SQLDtl.AppendLine("When A2.SectionNo = '15' Then Concat('Container', ': ', A.Cnt15, '   ', ifnull(concat('1x',A.Size15, @thc), ''), '   ', 'Seal', ': ', A.seal15) ");
                    SQLDtl.AppendLine("When A2.SectionNo = '16' Then Concat('Container', ': ', A.Cnt16, '   ', ifnull(concat('1x',A.Size16, @thc), ''), '   ', 'Seal', ': ', A.seal16) ");
                    SQLDtl.AppendLine("When A2.SectionNo = '17' Then Concat('Container', ': ', A.Cnt17, '   ', ifnull(concat('1x',A.Size17, @thc), ''), '   ', 'Seal', ': ', A.seal17) ");
                    SQLDtl.AppendLine("When A2.SectionNo = '18' Then Concat('Container', ': ', A.Cnt18, '   ', ifnull(concat('1x',A.Size18, @thc), ''), '   ', 'Seal', ': ', A.seal18) ");
                    SQLDtl.AppendLine("When A2.SectionNo = '19' Then Concat('Container', ': ', A.Cnt19, '   ', ifnull(concat('1x',A.Size19, @thc), ''), '   ', 'Seal', ': ', A.seal19) ");
                    SQLDtl.AppendLine("When A2.SectionNo = '20' Then Concat('Container', ': ', A.Cnt20, '   ', ifnull(concat('1x',A.Size20, @thc), ''), '   ', 'Seal', ': ', A.seal20) ");
                    SQLDtl.AppendLine("When A2.SectionNo = '21' Then Concat('Container', ': ', A.Cnt21, '   ', ifnull(concat('1x',A.Size21, @thc), ''), '   ', 'Seal', ': ', A.seal21) ");
                    SQLDtl.AppendLine("When A2.SectionNo = '22' Then Concat('Container', ': ', A.Cnt22, '   ', ifnull(concat('1x',A.Size22, @thc), ''), '   ', 'Seal', ': ', A.seal22) ");
                    SQLDtl.AppendLine("When A2.SectionNo = '23' Then Concat('Container', ': ', A.Cnt23, '   ', ifnull(concat('1x',A.Size23, @thc), ''), '   ', 'Seal', ': ', A.seal23) ");
                    SQLDtl.AppendLine("When A2.SectionNo = '24' Then Concat('Container', ': ', A.Cnt24, '   ', ifnull(concat('1x',A.Size24, @thc), ''), '   ', 'Seal', ': ', A.seal24) ");
                    SQLDtl.AppendLine("When A2.SectionNo = '25' Then Concat('Container', ': ', A.Cnt25, '   ', ifnull(concat('1x',A.Size25, @thc), ''), '   ', 'Seal', ': ', A.seal25) ");
                    SQLDtl.AppendLine("end as Cnt, ");
                    SQLDtl.AppendLine("Case ");
                    SQLDtl.AppendLine("When A2.SectionNo = '1' Then A.Cnt1 ");
                    SQLDtl.AppendLine("When A2.SectionNo = '2' Then A.Cnt2 ");
                    SQLDtl.AppendLine("When A2.SectionNo = '3' Then A.Cnt3 ");
                    SQLDtl.AppendLine("When A2.SectionNo = '4' Then A.Cnt4 ");
                    SQLDtl.AppendLine("When A2.SectionNo = '5' Then A.Cnt5 ");
                    SQLDtl.AppendLine("When A2.SectionNo = '6' Then A.Cnt6 ");
                    SQLDtl.AppendLine("When A2.SectionNo = '7' Then A.Cnt7 ");
                    SQLDtl.AppendLine("When A2.SectionNo = '8' Then A.Cnt8 ");
                    SQLDtl.AppendLine("When A2.SectionNo = '9' Then A.Cnt9 ");
                    SQLDtl.AppendLine("When A2.SectionNo = '10' Then A.Cnt10 ");
                    SQLDtl.AppendLine("When A2.SectionNo = '11' Then A.Cnt11 ");
                    SQLDtl.AppendLine("When A2.SectionNo = '12' Then A.Cnt12 ");
                    SQLDtl.AppendLine("When A2.SectionNo = '13' Then A.Cnt13 ");
                    SQLDtl.AppendLine("When A2.SectionNo = '14' Then A.Cnt14 ");
                    SQLDtl.AppendLine("When A2.SectionNo = '15' Then A.Cnt15 ");
                    SQLDtl.AppendLine("When A2.SectionNo = '16' Then A.Cnt16 ");
                    SQLDtl.AppendLine("When A2.SectionNo = '17' Then A.Cnt17 ");
                    SQLDtl.AppendLine("When A2.SectionNo = '18' Then A.Cnt18 ");
                    SQLDtl.AppendLine("When A2.SectionNo = '19' Then A.Cnt19 ");
                    SQLDtl.AppendLine("When A2.SectionNo = '20' Then A.Cnt20 ");
                    SQLDtl.AppendLine("When A2.SectionNo = '21' Then A.Cnt21 ");
                    SQLDtl.AppendLine("When A2.SectionNo = '22' Then A.Cnt22 ");
                    SQLDtl.AppendLine("When A2.SectionNo = '23' Then A.Cnt23 ");
                    SQLDtl.AppendLine("When A2.SectionNo = '24' Then A.Cnt24 ");
                    SQLDtl.AppendLine("When A2.SectionNo = '25' Then A.Cnt25 ");
                    SQLDtl.AppendLine("end as CntName, ");
                    SQLDtl.AppendLine("if(I.Qty =0, 0, if(G.PriceUomCode = B.SalesUomCode2, A2.Qty, ((I.Qty2*A2.Qty)/I.Qty))) As Qty2, A2.Remark ");
                    SQLDtl.AppendLine("From TblPlhdr A ");
                    SQLDtl.AppendLine("Inner Join TblPlDtl A2 On A.DocNo = A2.DocNo ");
                    SQLDtl.AppendLine("Inner Join TblItem B On A2.ItCode = B.ItCode  ");
                    SQLDtl.AppendLine("Inner Join TblSODtl C On A2.SoDocNo = C.DocNo And A2.SoDno = C.Dno ");
                    SQLDtl.AppendLine("Inner Join TblSOHdr D On C.DocNo = D.DocNo And A2.SODocNo = D.DocNo  ");
                    SQLDtl.AppendLine("Inner Join TblCtQtDtl F On D.CtQtDocNo = F.DocNo And C.CtQtDNo = F.Dno ");
                    SQLDtl.AppendLine("Inner Join TblCtQtHdr F2 On D.CtQtDocNo = F2.DocNo ");
                    SQLDtl.AppendLine("Inner Join TblUser F3 On F2.SpCode = F3.UserCode ");
                    SQLDtl.AppendLine("Inner Join TblItemPriceHdr G On F.ItemPriceDocNo = G.DocNo ");
                    SQLDtl.AppendLine("Left Join TblItemPackagingUnit I On C.PackagingUnitUomCode = I.UomCOde And A2.ItCode = I.ItCode ");
                    SQLDtl.AppendLine("left Join ");
                    SQLDtl.AppendLine("( ");
                    SQLDtl.AppendLine("  select ItCode, UomCode, Nw, GW From TblItempackagingunit ");
                    SQLDtl.AppendLine(")H On H.ItCode = A2.ItCode And  C.PackagingUnitUomCode = H.UomCode ");
                    SQLDtl.AppendLine("Where A.DocNo=@DocNo ");
                    SQLDtl.AppendLine("Order by A2.Dno ");


                    cmDtl.CommandText = SQLDtl.ToString();

                    Sm.CmParam<String>(ref cmDtl, "@DocNo", TxtDocNo.Text);
                    Sm.CmParam<String>(ref cmDtl, "@thc", thc);

                    var drDtl = cmDtl.ExecuteReader();
                    var cDtl = Sm.GetOrdinal(drDtl, new string[] 
                    {
                     //0
                     "Cnt" ,

                     //1-5
                     "ItCode" ,
                     "ItName",
                     "PLNo",
                     "QtyPL",
                     "Qty",

                     //6-10
                     "QtyInventory",
                     "NW",
                     "GW",
                     "Username",
                     "SectionNo",
                     //11-14
                     "Qty2",
                     "Remark",
                     "DNo"
                    });
                    if (drDtl.HasRows)
                    {
                        while (drDtl.Read())
                        {
                            ldtl.Add(new PLDtl()
                            {
                                Cnt = Sm.DrStr(drDtl, cDtl[0]),
                                ItCode = Sm.DrStr(drDtl, cDtl[1]),
                                ItName = Sm.DrStr(drDtl, cDtl[2]),
                                PLNo = Sm.DrStr(drDtl, cDtl[3]),
                                QtyPL = Sm.DrDec(drDtl, cDtl[4]),
                                Qty = Sm.DrDec(drDtl, cDtl[5]),
                                QtyInventory = Sm.DrDec(drDtl, cDtl[6]),
                                NW = Sm.DrDec(drDtl, cDtl[7]),
                                GW = Sm.DrDec(drDtl, cDtl[8]),
                                Username = Sm.DrStr(drDtl, cDtl[9]),
                                SectionNo = Sm.DrDec(drDtl, cDtl[10]),

                                Qty2 = Sm.DrDec(drDtl, cDtl[11]),
                                Remark = Sm.DrStr(drDtl, cDtl[12]),
                                DNo = Sm.DrDec(drDtl, cDtl[13]),
                            });
                        }
                    }
                    drDtl.Close();
                }
                myLists.Add(ldtl);
                #endregion

                #region Detail 2
                var cmDtl2 = new MySqlCommand();

                var SQLDtl2 = new StringBuilder();
                using (var cnDtl2 = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl2.Open();
                    cmDtl2.Connection = cnDtl2;

                    SQLDtl2.AppendLine("Select Count(X.Size) As Jumlah, X.Size ");
                    SQLDtl2.AppendLine("From ( ");
                    SQLDtl2.AppendLine("Select B.OptDesc As Size From TblPlHdr A ");
                    SQLDtl2.AppendLine("Inner Join TblOpTion B On A.Size1 = B.OptCode And B.OptCat = 'ContainerSize'  ");
                    SQLDtl2.AppendLine("Where DocNo = @DocNo ");
                    SQLDtl2.AppendLine("Union all ");
                    SQLDtl2.AppendLine("Select B.OptDesc As Size From TblPlHdr A ");
                    SQLDtl2.AppendLine("Inner Join TblOpTion B On A.Size2 = B.OptCode And B.OptCat = 'ContainerSize'  ");
                    SQLDtl2.AppendLine("Where DocNo = @DocNo ");
                    SQLDtl2.AppendLine("Union all ");
                    SQLDtl2.AppendLine("Select B.OptDesc As Size From TblPlHdr A ");
                    SQLDtl2.AppendLine("Inner Join TblOpTion B On A.Size3 = B.OptCode And B.OptCat = 'ContainerSize'  ");
                    SQLDtl2.AppendLine("Where DocNo = @DocNo ");
                    SQLDtl2.AppendLine("Union all ");
                    SQLDtl2.AppendLine("Select B.OptDesc As Size From TblPlHdr A ");
                    SQLDtl2.AppendLine("Inner Join TblOpTion B On A.Size4 = B.OptCode And B.OptCat = 'ContainerSize'   ");
                    SQLDtl2.AppendLine("Where DocNo = @DocNo ");
                    SQLDtl2.AppendLine("Union all ");
                    SQLDtl2.AppendLine("Select B.OptDesc As Size From TblPlHdr A ");
                    SQLDtl2.AppendLine("Inner Join TblOpTion B On A.Size5 = B.OptCode And B.OptCat = 'ContainerSize'   ");
                    SQLDtl2.AppendLine("Where DocNo = @DocNo ");
                    SQLDtl2.AppendLine("Union all ");
                    SQLDtl2.AppendLine("Select B.OptDesc As Size From TblPlHdr A ");
                    SQLDtl2.AppendLine("Inner Join TblOpTion B On A.Size6 = B.OptCode And B.OptCat = 'ContainerSize'  ");
                    SQLDtl2.AppendLine("Where DocNo = @DocNo ");
                    SQLDtl2.AppendLine("Union all ");
                    SQLDtl2.AppendLine("Select B.OptDesc As Size From TblPlHdr A ");
                    SQLDtl2.AppendLine("Inner Join TblOpTion B On A.Size7 = B.OptCode And B.OptCat = 'ContainerSize'  ");
                    SQLDtl2.AppendLine("Where DocNo = @DocNo ");
                    SQLDtl2.AppendLine("Union all ");
                    SQLDtl2.AppendLine("Select B.OptDesc As Size From TblPlHdr A ");
                    SQLDtl2.AppendLine("Inner Join TblOpTion B On A.Size8 = B.OptCode And B.OptCat = 'ContainerSize'  ");
                    SQLDtl2.AppendLine("Where DocNo = @DocNo");
                    SQLDtl2.AppendLine("Union all");
                    SQLDtl2.AppendLine("Select B.OptDesc As Size From TblPlHdr A ");
                    SQLDtl2.AppendLine("Inner Join TblOpTion B On A.Size9 = B.OptCode And B.OptCat = 'ContainerSize'  ");
                    SQLDtl2.AppendLine("Where DocNo = @DocNo");
                    SQLDtl2.AppendLine("Union all");
                    SQLDtl2.AppendLine("Select B.OptDesc As Size From TblPlHdr A ");
                    SQLDtl2.AppendLine("Inner Join TblOpTion B On A.Size10 = B.OptCode And B.OptCat = 'ContainerSize'  ");
                    SQLDtl2.AppendLine("Where DocNo = @DocNo");
                    SQLDtl2.AppendLine("Union all");
                    SQLDtl2.AppendLine("Select B.OptDesc As Size From TblPlHdr A ");
                    SQLDtl2.AppendLine("Inner Join TblOpTion B On A.Size11 = B.OptCode And B.OptCat = 'ContainerSize'  ");
                    SQLDtl2.AppendLine("Where DocNo = @DocNo");
                    SQLDtl2.AppendLine("Union all");
                    SQLDtl2.AppendLine("Select B.OptDesc As Size From TblPlHdr A ");
                    SQLDtl2.AppendLine("Inner Join TblOpTion B On A.Size12 = B.OptCode And B.OptCat = 'ContainerSize'  ");
                    SQLDtl2.AppendLine("Where DocNo = @DocNo");
                    SQLDtl2.AppendLine("Union all");
                    SQLDtl2.AppendLine("Select B.OptDesc As Size From TblPlHdr A ");
                    SQLDtl2.AppendLine("Inner Join TblOpTion B On A.Size13 = B.OptCode And B.OptCat = 'ContainerSize'  ");
                    SQLDtl2.AppendLine("Where DocNo = @DocNo");
                    SQLDtl2.AppendLine("Union all");
                    SQLDtl2.AppendLine("Select B.OptDesc As Size From TblPlHdr A ");
                    SQLDtl2.AppendLine("Inner Join TblOpTion B On A.Size14 = B.OptCode And B.OptCat = 'ContainerSize'  ");
                    SQLDtl2.AppendLine("Where DocNo = @DocNo");
                    SQLDtl2.AppendLine("Union all");
                    SQLDtl2.AppendLine("Select B.OptDesc As Size From TblPlHdr A ");
                    SQLDtl2.AppendLine("Inner Join TblOpTion B On A.Size15 = B.OptCode And B.OptCat = 'ContainerSize'  ");
                    SQLDtl2.AppendLine("Where DocNo = @DocNo");
                    SQLDtl2.AppendLine("Union all ");
                    SQLDtl2.AppendLine("Select B.OptDesc As Size From TblPlHdr A ");
                    SQLDtl2.AppendLine("Inner Join TblOpTion B On A.Size16 = B.OptCode And B.OptCat = 'ContainerSize'  ");
                    SQLDtl2.AppendLine("Where DocNo = @DocNo ");
                    SQLDtl2.AppendLine("Union all ");
                    SQLDtl2.AppendLine("Select B.OptDesc As Size From TblPlHdr A ");
                    SQLDtl2.AppendLine("Inner Join TblOpTion B On A.Size17 = B.OptCode And B.OptCat = 'ContainerSize'  ");
                    SQLDtl2.AppendLine("Where DocNo = @DocNo ");
                    SQLDtl2.AppendLine("Union all ");
                    SQLDtl2.AppendLine("Select B.OptDesc As Size From TblPlHdr A ");
                    SQLDtl2.AppendLine("Inner Join TblOpTion B On A.Size18 = B.OptCode And B.OptCat = 'ContainerSize'  ");
                    SQLDtl2.AppendLine("Where DocNo = @DocNo");
                    SQLDtl2.AppendLine("Union all");
                    SQLDtl2.AppendLine("Select B.OptDesc As Size From TblPlHdr A ");
                    SQLDtl2.AppendLine("Inner Join TblOpTion B On A.Size19 = B.OptCode And B.OptCat = 'ContainerSize'  ");
                    SQLDtl2.AppendLine("Where DocNo = @DocNo");
                    SQLDtl2.AppendLine("Union all");
                    SQLDtl2.AppendLine("Select B.OptDesc As Size From TblPlHdr A ");
                    SQLDtl2.AppendLine("Inner Join TblOpTion B On A.Size20 = B.OptCode And B.OptCat = 'ContainerSize'  ");
                    SQLDtl2.AppendLine("Where DocNo = @DocNo");
                    SQLDtl2.AppendLine("Union all");
                    SQLDtl2.AppendLine("Select B.OptDesc As Size From TblPlHdr A ");
                    SQLDtl2.AppendLine("Inner Join TblOpTion B On A.Size21 = B.OptCode And B.OptCat = 'ContainerSize'  ");
                    SQLDtl2.AppendLine("Where DocNo = @DocNo");
                    SQLDtl2.AppendLine("Union all");
                    SQLDtl2.AppendLine("Select B.OptDesc As Size From TblPlHdr A ");
                    SQLDtl2.AppendLine("Inner Join TblOpTion B On A.Size22 = B.OptCode And B.OptCat = 'ContainerSize'  ");
                    SQLDtl2.AppendLine("Where DocNo = @DocNo");
                    SQLDtl2.AppendLine("Union all");
                    SQLDtl2.AppendLine("Select B.OptDesc As Size From TblPlHdr A ");
                    SQLDtl2.AppendLine("Inner Join TblOpTion B On A.Size23 = B.OptCode And B.OptCat = 'ContainerSize'  ");
                    SQLDtl2.AppendLine("Where DocNo = @DocNo");
                    SQLDtl2.AppendLine("Union all");
                    SQLDtl2.AppendLine("Select B.OptDesc As Size From TblPlHdr A ");
                    SQLDtl2.AppendLine("Inner Join TblOpTion B On A.Size24 = B.OptCode And B.OptCat = 'ContainerSize'  ");
                    SQLDtl2.AppendLine("Where DocNo = @DocNo");
                    SQLDtl2.AppendLine("Union all");
                    SQLDtl2.AppendLine("Select B.OptDesc As Size From TblPlHdr A ");
                    SQLDtl2.AppendLine("Inner Join TblOpTion B On A.Size25 = B.OptCode And B.OptCat = 'ContainerSize'  ");
                    SQLDtl2.AppendLine("Where DocNo = @DocNo");
                    SQLDtl2.AppendLine(")X ");
                    SQLDtl2.AppendLine("group By X.Size");
                    SQLDtl2.AppendLine("having count(X.Size)>=1");

                    cmDtl2.CommandText = SQLDtl2.ToString();

                    Sm.CmParam<String>(ref cmDtl2, "@DocNo", TxtDocNo.Text);

                    var drDtl2 = cmDtl2.ExecuteReader();
                    var cDtl2 = Sm.GetOrdinal(drDtl2, new string[] 
                    {
                     //0
                     "Jumlah" ,

                     //1-5
                     "Size" ,
                    });
                    if (drDtl2.HasRows)
                    {
                        while (drDtl2.Read())
                        {
                            ldtl2.Add(new PLDtl2()
                            {
                                Jumlah = Sm.DrStr(drDtl2, cDtl2[0]),
                                Size = Sm.DrStr(drDtl2, cDtl2[1]),
                            });
                        }
                    }
                    drDtl2.Close();
                }
                myLists.Add(ldtl2);
                #endregion

                #region Detail3
                var cmDtl3 = new MySqlCommand();

                var SQLDtl3 = new StringBuilder();
                using (var cnDtl3 = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl3.Open();
                    cmDtl3.Connection = cnDtl3;

                    SQLDtl3.AppendLine("Select  X2.DocNo, X2.SectionNo, SUM(X2.NW) As NW, SUm(X2.GW) As GW, X2.NGUOM From ( ");
                    SQLDtl3.AppendLine("Select X.DocNo, X.SectionNo, Round(SUM(X.NW), 0) As Nw, Round(SUm(X.GW), 0) As GW, X.NGUOM From ( ");
                    SQLDtl3.AppendLine("Select A.DocNo, A2.SectionNo, (A2.QtyPackagingUnit * H.NW) As NW, (Select Parvalue From Tblparameter Where parCode = 'SIWeightUom') As NGuom,  ");
                    SQLDtl3.AppendLine("(A2.QtyPackagingUnit * H.GW) As GW ");
                    SQLDtl3.AppendLine("From TblPlhdr A  ");
                    SQLDtl3.AppendLine("Inner Join TblPlDtl A2 On A.DocNo = A2.DocNo ");
                    SQLDtl3.AppendLine("Inner Join TblSODtl C On A2.SoDocNo = C.DocNo And A2.SoDno = C.Dno  ");
                    SQLDtl3.AppendLine("left Join ");
                    SQLDtl3.AppendLine("( ");
                    SQLDtl3.AppendLine("  select ItCode, UomCode, Nw, GW From TblItempackagingunit ");
                    SQLDtl3.AppendLine(")H On H.ItCode = A2.ItCode And  C.PackagingUnitUomCode = H.UomCode ");
                    SQLDtl3.AppendLine("Where A.DocNo=@DocNo ");
                    SQLDtl3.AppendLine("Order by  A2.QtyPL Asc ");
                    SQLDtl3.AppendLine(")X ");
                    SQLDtl3.AppendLine("Group By X.DocNo, X.sectionNo ");
                    SQLDtl3.AppendLine(")X2 ");


                    cmDtl3.CommandText = SQLDtl3.ToString();

                    Sm.CmParam<String>(ref cmDtl3, "@DocNo", TxtDocNo.Text);

                    var drDtl3 = cmDtl3.ExecuteReader();
                    var cDtl3 = Sm.GetOrdinal(drDtl3, new string[] 
                    {
                     //0
                     "NW",

                     //1-5
                     "GW",
                     "NGUoM"
                    });
                    if (drDtl3.HasRows)
                    {
                        while (drDtl3.Read())
                        {
                            ldtl3.Add(new PLDtl3()
                            {
                                NW = Sm.DrDec(drDtl3, cDtl3[0]),
                                GW = Sm.DrDec(drDtl3, cDtl3[1]),
                                NGUOM = Sm.DrStr(drDtl3, cDtl3[2]),
                            });
                        }
                    }
                    drDtl3.Close();
                }
                myLists.Add(ldtl3);
                #endregion

                #region Detail4 for internal
                var cmDtl4 = new MySqlCommand();

                var SQLDtl4 = new StringBuilder();
                using (var cnDtl4 = new MySqlConnection(Gv.ConnectionString))
                {
                    string thc = Sm.GetValue("Select Concat(Right(OptDesc, 1),' ', 'HC') From TblOption Where OptCat = 'ContainerSize' Limit 1;");
                    cnDtl4.Open();
                    cmDtl4.Connection = cnDtl4;

                    SQLDtl4.AppendLine("Select A.DocNo, A2.Dno, A2.SectionNo, A2.ItCode, B.ItName, C.packagingunituomCode As packagingUnit, ");
                    SQLDtl4.AppendLine("A2.Qty, G.PriceUomCode, Round(A2.QtyInventory, 4) As QtyInventory, B.InventoryUomCode, A2.PLNo, ifnull(A2.QtyPL, 0) As QtyPL,  ");
                    SQLDtl4.AppendLine("(Select Parvalue From Tblparameter Where parCode = 'SIWeightUom') As NGuom,  ");
                    SQLDtl4.AppendLine("Case ");
                    SQLDtl4.AppendLine("When A2.SectionNo = '1' Then A.seal1 ");
                    SQLDtl4.AppendLine("When A2.SectionNo = '2' Then A.seal2 ");
                    SQLDtl4.AppendLine("When A2.SectionNo = '3' Then A.seal3 ");
                    SQLDtl4.AppendLine("When A2.SectionNo = '4' Then A.seal4 ");
                    SQLDtl4.AppendLine("When A2.SectionNo = '5' Then A.seal5 ");
                    SQLDtl4.AppendLine("When A2.SectionNo = '6' Then A.seal6 ");
                    SQLDtl4.AppendLine("When A2.SectionNo = '7' Then A.seal7 ");
                    SQLDtl4.AppendLine("When A2.SectionNo = '8' Then A.seal8 ");
                    SQLDtl4.AppendLine("When A2.SectionNo = '9' Then A.seal9 ");
                    SQLDtl4.AppendLine("When A2.SectionNo = '10' Then A.seal10 ");
                    SQLDtl4.AppendLine("When A2.SectionNo = '11' Then A.seal11 ");
                    SQLDtl4.AppendLine("When A2.SectionNo = '12' Then A.seal12 ");
                    SQLDtl4.AppendLine("When A2.SectionNo = '13' Then A.seal13 ");
                    SQLDtl4.AppendLine("When A2.SectionNo = '14' Then A.seal14 ");
                    SQLDtl4.AppendLine("When A2.SectionNo = '15' Then A.seal15 ");
                    SQLDtl4.AppendLine("When A2.SectionNo = '16' Then A.seal16 ");
                    SQLDtl4.AppendLine("When A2.SectionNo = '17' Then A.seal17 ");
                    SQLDtl4.AppendLine("When A2.SectionNo = '18' Then A.seal18 ");
                    SQLDtl4.AppendLine("When A2.SectionNo = '19' Then A.seal19 ");
                    SQLDtl4.AppendLine("When A2.SectionNo = '20' Then A.seal20 ");
                    SQLDtl4.AppendLine("When A2.SectionNo = '21' Then A.seal21 ");
                    SQLDtl4.AppendLine("When A2.SectionNo = '22' Then A.seal22 ");
                    SQLDtl4.AppendLine("When A2.SectionNo = '23' Then A.seal23 ");
                    SQLDtl4.AppendLine("When A2.SectionNo = '24' Then A.seal24 ");
                    SQLDtl4.AppendLine("When A2.SectionNo = '25' Then A.seal25 ");
                    SQLDtl4.AppendLine("end as Seal, ");
                    SQLDtl4.AppendLine("Case ");
                    SQLDtl4.AppendLine("When A2.SectionNo = '1' Then A.Cnt1 ");
                    SQLDtl4.AppendLine("When A2.SectionNo = '2' Then A.Cnt2 ");
                    SQLDtl4.AppendLine("When A2.SectionNo = '3' Then A.Cnt3 ");
                    SQLDtl4.AppendLine("When A2.SectionNo = '4' Then A.Cnt4 ");
                    SQLDtl4.AppendLine("When A2.SectionNo = '5' Then A.Cnt5 ");
                    SQLDtl4.AppendLine("When A2.SectionNo = '6' Then A.Cnt6 ");
                    SQLDtl4.AppendLine("When A2.SectionNo = '7' Then A.Cnt7 ");
                    SQLDtl4.AppendLine("When A2.SectionNo = '8' Then A.Cnt8 ");
                    SQLDtl4.AppendLine("When A2.SectionNo = '9' Then A.Cnt9 ");
                    SQLDtl4.AppendLine("When A2.SectionNo = '10' Then A.Cnt10 ");
                    SQLDtl4.AppendLine("When A2.SectionNo = '11' Then A.Cnt11 ");
                    SQLDtl4.AppendLine("When A2.SectionNo = '12' Then A.Cnt12 ");
                    SQLDtl4.AppendLine("When A2.SectionNo = '13' Then A.Cnt13 ");
                    SQLDtl4.AppendLine("When A2.SectionNo = '14' Then A.Cnt14 ");
                    SQLDtl4.AppendLine("When A2.SectionNo = '15' Then A.Cnt15 ");
                    SQLDtl4.AppendLine("When A2.SectionNo = '16' Then A.Cnt16 ");
                    SQLDtl4.AppendLine("When A2.SectionNo = '17' Then A.Cnt17 ");
                    SQLDtl4.AppendLine("When A2.SectionNo = '18' Then A.Cnt18 ");
                    SQLDtl4.AppendLine("When A2.SectionNo = '19' Then A.Cnt19 ");
                    SQLDtl4.AppendLine("When A2.SectionNo = '20' Then A.Cnt20 ");
                    SQLDtl4.AppendLine("When A2.SectionNo = '21' Then A.Cnt21 ");
                    SQLDtl4.AppendLine("When A2.SectionNo = '22' Then A.Cnt22 ");
                    SQLDtl4.AppendLine("When A2.SectionNo = '23' Then A.Cnt23 ");
                    SQLDtl4.AppendLine("When A2.SectionNo = '24' Then A.Cnt24 ");
                    SQLDtl4.AppendLine("When A2.SectionNo = '25' Then A.Cnt25 ");
                    SQLDtl4.AppendLine("end as CntName, ");

                    SQLDtl4.AppendLine("Case ");
                    SQLDtl4.AppendLine("When A2.SectionNo = '1' Then A.SM1 ");
                    SQLDtl4.AppendLine("When A2.SectionNo = '2' Then A.SM2 ");
                    SQLDtl4.AppendLine("When A2.SectionNo = '3' Then A.SM3 ");
                    SQLDtl4.AppendLine("When A2.SectionNo = '4' Then A.SM4 ");
                    SQLDtl4.AppendLine("When A2.SectionNo = '5' Then A.SM5 ");
                    SQLDtl4.AppendLine("When A2.SectionNo = '6' Then A.SM6 ");
                    SQLDtl4.AppendLine("When A2.SectionNo = '7' Then A.SM7 ");
                    SQLDtl4.AppendLine("When A2.SectionNo = '8' Then A.SM8 ");
                    SQLDtl4.AppendLine("When A2.SectionNo = '9' Then A.SM9 ");
                    SQLDtl4.AppendLine("When A2.SectionNo = '10' Then A.SM10 ");
                    SQLDtl4.AppendLine("When A2.SectionNo = '11' Then A.SM11 ");
                    SQLDtl4.AppendLine("When A2.SectionNo = '12' Then A.SM12 ");
                    SQLDtl4.AppendLine("When A2.SectionNo = '13' Then A.SM13 ");
                    SQLDtl4.AppendLine("When A2.SectionNo = '14' Then A.SM14 ");
                    SQLDtl4.AppendLine("When A2.SectionNo = '15' Then A.SM15 ");
                    SQLDtl4.AppendLine("When A2.SectionNo = '16' Then A.SM16 ");
                    SQLDtl4.AppendLine("When A2.SectionNo = '17' Then A.SM17 ");
                    SQLDtl4.AppendLine("When A2.SectionNo = '18' Then A.SM18 ");
                    SQLDtl4.AppendLine("When A2.SectionNo = '19' Then A.SM19 ");
                    SQLDtl4.AppendLine("When A2.SectionNo = '20' Then A.SM20 ");
                    SQLDtl4.AppendLine("When A2.SectionNo = '21' Then A.SM21 ");
                    SQLDtl4.AppendLine("When A2.SectionNo = '22' Then A.SM22 ");
                    SQLDtl4.AppendLine("When A2.SectionNo = '23' Then A.SM23 ");
                    SQLDtl4.AppendLine("When A2.SectionNo = '24' Then A.SM24 ");
                    SQLDtl4.AppendLine("When A2.SectionNo = '25' Then A.SM25 ");
                    SQLDtl4.AppendLine("end as SM, ");

                    SQLDtl4.AppendLine("if(I.Qty =0, 0, if(G.PriceUomCode = B.SalesUomCode2, A2.Qty, ((I.Qty2*A2.Qty)/I.Qty))) As Qty2, A2.Remark, ");
                    if (mIsSO2UseActualItem) {
                        SQLDtl4.AppendLine("J.ItName As ItName2 ");
                    }
                    else {
                        SQLDtl4.AppendLine("Null As ItName2 ");
                    }
                    SQLDtl4.AppendLine("From TblPlhdr A ");
                    SQLDtl4.AppendLine("Inner Join TblPlDtl A2 On A.DocNo = A2.DocNo ");
                    SQLDtl4.AppendLine("Inner Join TblItem B On A2.ItCode = B.ItCode  ");
                    SQLDtl4.AppendLine("Inner Join TblSODtl C On A2.SoDocNo = C.DocNo And A2.SoDno = C.Dno ");
                    SQLDtl4.AppendLine("Inner Join TblSOHdr D On C.DocNo = D.DocNo And A2.SODocNo = D.DocNo  ");
                    SQLDtl4.AppendLine("Inner Join TblCtQtDtl F On D.CtQtDocNo = F.DocNo And C.CtQtDNo = F.Dno ");
                    SQLDtl4.AppendLine("Inner Join TblCtQtHdr F2 On D.CtQtDocNo = F2.DocNo ");
                    SQLDtl4.AppendLine("Inner Join TblUser F3 On F2.SpCode = F3.UserCode ");
                    SQLDtl4.AppendLine("Inner Join TblItemPriceHdr G On F.ItemPriceDocNo = G.DocNo ");
                    SQLDtl4.AppendLine("Left Join TblItemPackagingUnit I On C.PackagingUnitUomCode = I.UomCOde And A2.ItCode = I.ItCode ");
                    SQLDtl4.AppendLine("left Join ");
                    SQLDtl4.AppendLine("( ");
                    SQLDtl4.AppendLine("  select ItCode, UomCode, Nw, GW From TblItempackagingunit ");
                    SQLDtl4.AppendLine(")H On H.ItCode = A2.ItCode And  C.PackagingUnitUomCode = H.UomCode ");
                    SQLDtl4.AppendLine("Left Join TblItem J On C.ItCode2 = J.ItCode ");
                    SQLDtl4.AppendLine("Where A.DocNo=@DocNo And A2.SectionNo =@SectionNo ");
                    SQLDtl4.AppendLine("Order by A2.Dno ");


                    cmDtl4.CommandText = SQLDtl4.ToString();

                    Sm.CmParam<String>(ref cmDtl4, "@DocNo", TxtDocNo.Text);
                    Sm.CmParam<String>(ref cmDtl4, "@SectionNo", TxtTab.Text);

                    var drDtl4 = cmDtl4.ExecuteReader();
                    var cDtl4 = Sm.GetOrdinal(drDtl4, new string[] 
                    {
                     //0
                     "CntName" ,

                     //1-5
                     "Seal",
                     "ItCode" ,
                     "ItName",
                     "PLNo",
                     "QtyPL",

                     //6-10
                     "Qty",
                     "QtyInventory",
                     "SectionNo",
                     "Qty2",
                     "Remark",
                     
                     //11
                     "DNo",
                     "PackagingUnit",
                     "SM",
                     "ItName2"

                    });
                    if (drDtl4.HasRows)
                    {
                        try
                        {
                            while (drDtl4.Read())
                            {
                                pallet = Sm.DrStr(drDtl4, cDtl4[4]);
                                if (pallet.Length == 7)
                                {
                                    left = Convert.ToInt32(Sm.Left(pallet, 3));
                                    right = Convert.ToInt32(Sm.Right(pallet, 3));
                                    bal = right - left;
                                    start = left;
                                    end = right;
                                }
                                else if (pallet.Length == 3)
                                {
                                    left = Convert.ToInt32(Sm.Left(pallet, 3));
                                    bal = 0;
                                    start = left;
                                    end = left;
                                }

                                for (int a = 0; a <= bal; a++)
                                {
                                    ldtl4.Add(new PLDtl4()
                                    {
                                        Test = start,
                                        Cnt = Sm.DrStr(drDtl4, cDtl4[0]),
                                        Seal = Sm.DrStr(drDtl4, cDtl4[1]),
                                        ItCode = Sm.DrStr(drDtl4, cDtl4[2]),
                                        ItName = Sm.DrStr(drDtl4, cDtl4[3]),
                                        PLNo = Sm.DrStr(drDtl4, cDtl4[4]),
                                        QtyPL = Sm.DrDec(drDtl4, cDtl4[5]),
                                        Qty = Sm.DrDec(drDtl4, cDtl4[6]),
                                        QtyInventory = Sm.DrDec(drDtl4, cDtl4[7]),
                                        SectionNo = Sm.DrStr(drDtl4, cDtl4[8]),
                                        Qty2 = Sm.DrDec(drDtl4, cDtl4[9]),
                                        Remark = Sm.DrStr(drDtl4, cDtl4[10]),
                                        DNo = Sm.DrStr(drDtl4, cDtl4[11]),
                                        PackagingUnit = GetNumber(Sm.DrStr(drDtl4, cDtl4[12])),
                                        SM = Sm.DrStr(drDtl4, cDtl4[13]),
                                        ItName2 = Sm.DrStr(drDtl4, cDtl4[14]),
                                    });
                                    start = start + 1;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("" + ex + "");
                        }
                    }
                    drDtl4.Close();
                }
                myLists.Add(ldtl4);
                #endregion

                Sm.PrintReport("PL", myLists, TableName, false);
                if (TxtTab.Text.Length > 0)
                {
                    Sm.PrintReport("PL2", myLists, TableName, false);
                }
            }
            #endregion

            #region Printout mai

            if (Doctitle == "MAI")
            {
                var l2 = new List<PLHdr2>();
                var ldtl5 = new List<PLDtl5>();
                var ldtl6 = new List<PLDtl6>();
                string[] TableName = { "PLHdr2", "PLDtl5", "PLDtl6" };

                List<IList> myLists = new List<IList>();
                var cm2 = new MySqlCommand();

                #region Header
                var SQL2 = new StringBuilder();
                SQL2.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
                SQL2.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
                SQL2.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyAddressCity', ");
                SQL2.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyPhone', ");
                SQL2.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle5') As 'CompanyFax', ");
                SQL2.AppendLine("A.DocNo, DATE_FORMAT(A.DocDt,'%M %d, %Y') As DocDt, A.LocalDocno, A.Account, A.InvoiceNo, A.SalesContractNo, DATE_FORMAT(C.StfDt,'%d %M %Y') As StfDt, ");
                SQL2.AppendLine("D.PortName As PortLoading, E.PortName As PortDischarge, B.SPPlaceDelivery, C.SpName, B2.SoLocal, ");
                SQL2.AppendLine("A.Remark2 As Note, B.LocalDocNo As SILocal, B.SpBLNo, B.MotherVessel, Concat_WS('   ',Ifnull(Seal1,''),ifnull(seal2,''), ifnull(seal3,''),Ifnull(Seal4,''), ");
                SQL2.AppendLine("ifnull(seal5,''), ifnull(seal6,''),Ifnull(Seal7,''),ifnull(seal8,''), ifnull(seal9,''), ifnull(seal10,''),Ifnull(Seal11,''),ifnull(seal12,''), ifnull(seal13,''), ");
                SQL2.AppendLine("Ifnull(Seal14,''),ifnull(seal15,''),ifnull(seal16,''),Ifnull(Seal17,''),ifnull(seal18,''), ifnull(seal19,''), ifnull(seal20,''), ");
                SQL2.AppendLine("Ifnull(Seal21,''),ifnull(seal22,''),ifnull(seal23,''),Ifnull(Seal24,''),Ifnull(Seal25,''))as Seal, ");
                SQL2.AppendLine("Concat_WS('   ',Ifnull(cnt1,''),ifnull(cnt2,''), ifnull(cnt3,''),Ifnull(cnt4,''),ifnull(cnt5,''), ifnull(cnt6,''),Ifnull(cnt7,''),ifnull(cnt8,''), ");
                SQL2.AppendLine("ifnull(cnt9,''), ifnull(cnt10,''),Ifnull(cnt11,''),ifnull(cnt12,''), ifnull(cnt13,''),Ifnull(cnt14,''),ifnull(cnt15,''),ifnull(cnt16,''),Ifnull(cnt17,''), ");
                SQL2.AppendLine("ifnull(cnt18,''), ifnull(cnt19,''), ifnull(cnt20,''),Ifnull(cnt21,''),ifnull(cnt22,''),ifnull(cnt23,''),Ifnull(cnt24,''),Ifnull(cnt25,''))as cnt, B2.CtPONo ");
                SQL2.AppendLine("From TblPLHdr A ");
                SQL2.AppendLine("Inner Join TblSIhdr B On A.SIDocNo = B.DocNo ");
                SQL2.AppendLine("Left Join ( ");
                SQL2.AppendLine("		Select  A.DocNo As SIDOCno, Group_concat(Distinct C.LocalDocNo separator ', ' )As SoLocal,  Group_concat(Distinct C.CtPONo separator ', ' )As CtPONo ");
                SQL2.AppendLine("       From tblsidtl A ");
                SQL2.AppendLine("		Inner Join TblSodtl B On A.SODocNo=B.DocNo And A.SODNo=B.Dno ");
                SQL2.AppendLine("		Inner JOin TblSoHdr C On B.DocNo=C.DocNo ");
                SQL2.AppendLine("		Group By A.DocNo ");
                SQL2.AppendLine(")B2 On B.DocNo=B2.SiDocNo ");
                SQL2.AppendLine("Inner Join TblSP C On B.SpDocNo = C.DocNo ");
                SQL2.AppendLine("Inner Join TblPort D On B.SpPortCode1 = D.PortCode ");
                SQL2.AppendLine("Inner Join TblPort E On B.SpPortCode2 = E.PortCode ");
                SQL2.AppendLine("Where A.DocNo=@DocNo ");

                using (var cn2 = new MySqlConnection(Gv.ConnectionString))
                {
                    cn2.Open();
                    cm2.Connection = cn2;
                    cm2.CommandText = SQL2.ToString();
                    Sm.CmParam<String>(ref cm2, "@DocNo", TxtDocNo.Text);
                    Sm.CmParam<String>(ref cm2, "@CompanyLogo", @Sm.CompanyLogo());
                    var dr2 = cm2.ExecuteReader();
                    var c2 = Sm.GetOrdinal(dr2, new string[] 
                        {
                        //0
                         "CompanyLogo",

                         //1-5
                         "CompanyName",
                         "CompanyAddress",
                         "CompanyAddressCity",
                         "CompanyPhone",
                         "CompanyFax",
                        
                         //6-10
                         "DocNo",
                         "DocDt",
                         "LocalDocNo",
                         "Account",
                         "InvoiceNo",
                        
                         //11-15
                         "SalesContractNo",
                         "StfDt",
                         "PortLoading",
                         "PortDischarge",
                         "SPPlaceDelivery",

                         //16-20
                         "SpName",
                         "SoLocal",
                         "Note",
                         "SILocal",
                         "SpBLNo",

                         //21-24
                         "MotherVessel",
                         "Seal",
                         "Cnt",
                         "CtPONo"

                        });
                    if (dr2.HasRows)
                    {
                        while (dr2.Read())
                        {
                            l2.Add(new PLHdr2()
                            {
                                CompanyLogo = Sm.DrStr(dr2, c2[0]),

                                CompanyName = Sm.DrStr(dr2, c2[1]),
                                CompanyAddress = Sm.DrStr(dr2, c2[2]),
                                CompanyAddressCity = Sm.DrStr(dr2, c2[3]),
                                CompanyPhone = Sm.DrStr(dr2, c2[4]),
                                CompanyFax = Sm.DrStr(dr2, c2[5]),

                                DocNo = Sm.DrStr(dr2, c2[6]),
                                DocDt = Sm.DrStr(dr2, c2[7]),
                                LocalDocNo = Sm.DrStr(dr2, c2[8]),
                                Account = Sm.DrStr(dr2, c2[9]),
                                InvoiceNo = Sm.DrStr(dr2, c2[10]),

                                SalesContractNo = Sm.DrStr(dr2, c2[11]),
                                StfDt = Sm.DrStr(dr2, c2[12]),
                                PortLoading = Sm.DrStr(dr2, c2[13]),
                                PortDischarge = Sm.DrStr(dr2, c2[14]),
                                SPPlaceDelivery = Sm.DrStr(dr2, c2[15]),

                                SpName = Sm.DrStr(dr2, c2[16]),
                                SoLocal = Sm.DrStr(dr2, c2[17]),
                                Note = Sm.DrStr(dr2, c2[18]),
                                SILocal = Sm.DrStr(dr2, c2[19]),
                                SpBLNo = Sm.DrStr(dr2, c2[20]),
                                MotherVessel = Sm.DrStr(dr2, c2[21]),
                                Seal = Sm.DrStr(dr2, c2[22]),
                                Cnt = Sm.DrStr(dr2, c2[23]),
                                CtPONo = Sm.DrStr(dr2, c2[24]),
                                PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
                            });
                        }
                    }
                    dr2.Close();
                }
                myLists.Add(l2);
                #endregion

                #region Detail 5
                var cmDtl5 = new MySqlCommand();

                var SQLDtl5 = new StringBuilder();
                using (var cnDtl5 = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl5.Open();
                    cmDtl5.Connection = cnDtl5;

                    SQLDtl5.AppendLine("Select A.DocNo, B.ItCode, ");
                    if (mIsCustomerItemNameMandatory)
                        SQLDtl5.AppendLine("IfNull(K.CtItName, B.ItName) As ItName, ");
                    else
                        SQLDtl5.AppendLine("B.ItName, ");
                    SQLDtl5.AppendLine("B.ItCodeInternal, J.ItGrpName, ");
                    SQLDtl5.AppendLine("B.HSCode As SPHSCOde, ");
                    SQLDtl5.AppendLine("E.UPrice, A2.Qty, ");
                    SQLDtl5.AppendLine("Round((E.UPrice * Round(A2.Qty, 4)), 2) As Amount, G.PriceUomCode, Round(A2.QtyInventory,2) As QtyInventory, B.InventoryUomCode, ");
                    SQLDtl5.AppendLine("Round(if(N.NWRate =0, (A2.QtyPackagingUnit * H.NW), N.NWRate),2) As NW,Round(if(N.GWRate =0, (A2.QtyPackagingUnit * H.GW), N.GWRate),2) As GW, K.CtItCode, G.CurCode,  L.OptDesc As Material, ");
                    SQLDtl5.AppendLine("M.OptDesc As Colour, A2.TotalVolume,  A2.QtyPackagingUnit, C.PackagingUnitUomCode, ifnull(A2.QtyPL, 0) As QtyPL, (A2.Qty /A2.QtyPackagingUnit) QtyPcs ");
                    SQLDtl5.AppendLine("From TblPlhdr A  ");
                    SQLDtl5.AppendLine("Inner Join TblPlDtl A2 On A.DocNo = A2.DocNo ");
                    SQLDtl5.AppendLine("Inner Join TblItem B On A2.ItCode = B.ItCode ");
                    SQLDtl5.AppendLine("Inner Join TblSODtl C On A2.SoDocNo = C.DocNo And A2.SoDno = C.Dno ");
                    SQLDtl5.AppendLine("Inner Join TblSOHdr D On C.DocNo = D.DocNo And A2.SODocNo = D.DocNo ");
                    SQLDtl5.AppendLine("Inner Join TblCtQtDtl E On D.CtQtDocNo = E.DocNo And C.CtQtDNo = E.Dno ");
                    SQLDtl5.AppendLine("Inner Join TblCtQtHdr F On D.CtQtDocNo = F.DocNo ");
                    SQLDtl5.AppendLine("Inner Join TblItemPriceHdr G On E.ItemPriceDocNo = G.DocNo ");
                    SQLDtl5.AppendLine("left Join ");
                    SQLDtl5.AppendLine("( ");
                    SQLDtl5.AppendLine("    select ItCode, Qty, Qty2, UomCode, Nw, GW From TblItempackagingunit ");
                    SQLDtl5.AppendLine(")H On H.ItCode = A2.ItCode And  C.PackagingUnitUomCode = H.UomCode ");
                    SQLDtl5.AppendLine("Inner Join TblSihdr I On A.SiDocNo=I.DocNo ");
                    SQLDtl5.AppendLine("Left Join TblItemGroup J On B.ItGrpCode = J.ItGrpCode ");
                    SQLDtl5.AppendLine("Left Join TblCustomerItem K On B.ItCode=K.ItCode And D.CtCode=K.CtCode ");
                    SQLDtl5.AppendLine("Left Join TblOption L On L.OptCat='ItemInformation5' And B.Information5=L.OptCode ");
                    SQLDtl5.AppendLine("Left Join TblOption M On M.OptCat='ItemInformation1' And B.Information1=M.OptCode ");
                    SQLDtl5.AppendLine("Inner Join TblSIDtl N On A.SiDocNo = N.DocNo And A2.SODocNo = N.SoDocNo And A2.SODno = N.SODno ");
                    SQLDtl5.AppendLine("Where A.DocNo=@DocNo;");

                    

                    cmDtl5.CommandText = SQLDtl5.ToString();

                    Sm.CmParam<String>(ref cmDtl5, "@DocNo", TxtDocNo.Text);
                    

                    var drDtl5 = cmDtl5.ExecuteReader();
                    var cDtl5 = Sm.GetOrdinal(drDtl5, new string[] 
                    {
                     //0
                     "DocNo",

                     //1-5
                     "ItCode", "ItName", "ItCodeInternal", "ItGrpName","SPHSCOde",

                     //6-10
                     "UPrice", "Qty", "Amount", "PriceUomCode", "QtyInventory",
                     
                     //11-15
                     "InventoryUomCode", "NW", "GW", "CtItCode", "CurCode",

                     //16-19
                     "Material", "Colour", "TotalVolume", "QtyPackagingUnit", "QtyPL",

                     //20
                     "QtyPcs"

                    });
                    if (drDtl5.HasRows)
                    {
                        int nomor = 0;
                        while (drDtl5.Read())
                        {
                            nomor = nomor + 1;
                            ldtl5.Add(new PLDtl5()
                            {
                                nomor=nomor,
                                DocNo = Sm.DrStr(drDtl5, cDtl5[0]),

                                ItCode = Sm.DrStr(drDtl5, cDtl5[1]),
                                ItName = Sm.DrStr(drDtl5, cDtl5[2]),
                                ItCodeInternal = Sm.DrStr(drDtl5, cDtl5[3]),
                                ItGrpName = Sm.DrStr(drDtl5, cDtl5[4]),
                                SPHSCOde = Sm.DrStr(drDtl5, cDtl5[5]),

                                UPrice = Sm.DrDec(drDtl5, cDtl5[6]),
                                Qty = Sm.DrDec(drDtl5, cDtl5[7]),
                                Amount = Sm.DrDec(drDtl5, cDtl5[8]),
                                PriceUomCode = Sm.DrStr(drDtl5, cDtl5[9]),
                                QtyInventory = Sm.DrDec(drDtl5, cDtl5[10]),

                                InventoryUomCode = Sm.DrStr(drDtl5, cDtl5[11]),
                                NW = Sm.DrDec(drDtl5, cDtl5[12]),
                                GW = Sm.DrDec(drDtl5, cDtl5[13]),
                                CtItCode = Sm.DrStr(drDtl5, cDtl5[14]),
                                CurCode = Sm.DrStr(drDtl5, cDtl5[15]),

                                Material = Sm.DrStr(drDtl5, cDtl5[16]),
                                Colour = Sm.DrStr(drDtl5, cDtl5[17]),
                                TotalVolume = Sm.DrDec(drDtl5, cDtl5[18]),
                                QtyPackagingUnit = Sm.DrDec(drDtl5, cDtl5[19]),
                                QtyPL = Sm.DrDec(drDtl5, cDtl5[20]),
                                QtyPcs = Sm.DrDec(drDtl5, cDtl5[21]),
                            });
                        }
                    }
                    drDtl5.Close();
                }
                myLists.Add(ldtl5);
                #endregion

                #region Detail 6
                var cmDtl6 = new MySqlCommand();

                var SQLDtl6 = new StringBuilder();
                using (var cnDtl6 = new MySqlConnection(Gv.ConnectionString))
                {
                    cnDtl6.Open();
                    cmDtl6.Connection = cnDtl6;

                    SQLDtl6.AppendLine("Select X.DocNo, SUM(X.Qty) As Qty, SUM(X.Amount) As Amount, SUM(X.NW)As NW, SUM(X.GW) As GW From ( ");
                    SQLDtl6.AppendLine("Select A.DocNo, A2.Qty, Round((E.UPrice * Round(A2.Qty, 4)), 2) As Amount, ");
                    SQLDtl6.AppendLine("(A2.QtyPackagingUnit * H.NW) As NW,(A2.QtyPackagingUnit * H.GW) As GW ");
                    SQLDtl6.AppendLine("From TblPlhdr A ");
                    SQLDtl6.AppendLine("Inner Join TblPlDtl A2 On A.DocNo = A2.DocNo ");
                    SQLDtl6.AppendLine("Inner Join TblItem B On A2.ItCode = B.ItCode ");
                    SQLDtl6.AppendLine("Inner Join TblSODtl C On A2.SoDocNo = C.DocNo And A2.SoDno = C.Dno ");
                    SQLDtl6.AppendLine("Inner Join TblSOHdr D On C.DocNo = D.DocNo And A2.SODocNo = D.DocNo ");
                    SQLDtl6.AppendLine("Inner Join TblCtQtDtl E On D.CtQtDocNo = E.DocNo And C.CtQtDNo = E.Dno ");
                    SQLDtl6.AppendLine("Inner Join TblCtQtHdr F On D.CtQtDocNo = F.DocNo ");
                    SQLDtl6.AppendLine("Inner Join TblItemPriceHdr G On E.ItemPriceDocNo = G.DocNo ");
                    SQLDtl6.AppendLine("left Join ");
                    SQLDtl6.AppendLine("( ");
                    SQLDtl6.AppendLine("Select ItCode, UomCode, Nw, GW From TblItempackagingunit ");
                    SQLDtl6.AppendLine(")H On H.ItCode = A2.ItCode And  C.PackagingUnitUomCode = H.UomCode ");
                    SQLDtl6.AppendLine("Inner Join TblSihdr I On A.SiDocNo=I.DocNo ");
                    SQLDtl6.AppendLine("Left Join TblItemGroup J On B.ItGrpCode = J.ItGrpCode ");
                    SQLDtl6.AppendLine("Left Join TblCustomerItem K On B.ItCode = K.ItCode And D.CtCode=K.CtCode ");
                    SQLDtl6.AppendLine("Where A.DocNo=@DocNo ");
                    SQLDtl6.AppendLine(")X ");
                    SQLDtl6.AppendLine("Group by X.DocNo ");

                    cmDtl6.CommandText = SQLDtl6.ToString();

                    Sm.CmParam<String>(ref cmDtl6, "@DocNo", TxtDocNo.Text);

                    var drDtl6 = cmDtl6.ExecuteReader();
                    var cDtl6 = Sm.GetOrdinal(drDtl6, new string[] 
                    {
                     //0-4
                     "DocNo", "Qty", "Amount", "NW", "GW",
                   
                    });
                    if (drDtl6.HasRows)
                    {
                        while (drDtl6.Read())
                        {
                            ldtl6.Add(new PLDtl6()
                            {
                                DocNo = Sm.DrStr(drDtl6, cDtl6[0]),
                                Qty = Sm.DrDec(drDtl6, cDtl6[1]),
                                Amount = Sm.DrDec(drDtl6, cDtl6[2]),
                                NW = Sm.DrDec(drDtl6, cDtl6[3]),
                                GW = Sm.DrDec(drDtl6, cDtl6[4]),
                            });
                        }
                    }
                    drDtl6.Close();
                }
                myLists.Add(ldtl6);
                #endregion

                Sm.PrintReport("PLMAI", myLists, TableName, false);
              
                {
                    Sm.PrintReport("PLMAI2", myLists, TableName, false );
                }
            
            }

            #endregion
        }

        private void DownloadFileKu(string TxtFile)
        {
            SFD1.FileName = TxtFile;
            SFD1.DefaultExt = "pdf";
            SFD1.AddExtension = true;

            if (SFD1.ShowDialog() == DialogResult.OK)
            {
                DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, TxtFile, mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
                Application.DoEvents();
                //Write the bytes to a file
                FileStream newFile = new FileStream(SFD1.FileName, FileMode.Create);
                newFile.Write(downloadedData, 0, downloadedData.Length);
                newFile.Close();
                MessageBox.Show("Saved Successfully");
            }
            else
                MessageBox.Show("No File was Downloaded Yet!");
        }

        private void DownloadFile(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                //this.Text = string.Concat(this.Text, "         ", "Connecting...");
                Application.DoEvents();

                if (mFormatFTPClient == "1")
                {
                    #region type 1
                    FtpWebRequest request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                    //this.Text = string.Concat(this.Text, "         ", "Retrieving Information...");
                    Application.DoEvents();

                    //Get the file size first (for progress bar)
                    request.Method = WebRequestMethods.Ftp.GetFileSize;
                    request.Credentials = new NetworkCredential(username, password);
                    request.UsePassive = true;
                    request.UseBinary = true;
                    request.KeepAlive = true;

                    int dataLength = (int)request.GetResponse().ContentLength;

                    //this.Text = string.Concat(this.Text, "         ", "Downloading File...");
                    Application.DoEvents();

                    //Now get the actual data
                    request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                    request.Method = WebRequestMethods.Ftp.DownloadFile;
                    request.Credentials = new NetworkCredential(username, password);
                    request.UsePassive = true;
                    request.UseBinary = true;
                    request.KeepAlive = false;

                    //Streams
                    FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                    Stream reader = response.GetResponseStream();

                    //Download to memory
                    MemoryStream memStream = new MemoryStream();
                    byte[] buffer = new byte[1024];

                    while (true)
                    {
                        Application.DoEvents();

                        int bytesRead = reader.Read(buffer, 0, buffer.Length);

                        if (bytesRead == 0)
                        {
                            Application.DoEvents();
                            break;
                        }
                        else
                        {
                            memStream.Write(buffer, 0, bytesRead);
                        }
                    }

                    downloadedData = memStream.ToArray();

                    reader.Close();
                    memStream.Close();
                    response.Close();
                    #endregion
                }
                else
                {
                    #region  type 2
                    FtpWebRequest request = FtpWebRequest.Create("ftp://" + FTPAddress + "/" + filename) as FtpWebRequest;
                    //this.Text = string.Concat(this.Text, "         ", "Retrieving Information...");
                    Application.DoEvents();

                    //Get the file size first (for progress bar)
                    request.Method = WebRequestMethods.Ftp.GetFileSize;
                    request.Credentials = new NetworkCredential(username, password);
                    request.UsePassive = true;
                    request.UseBinary = true;
                    request.KeepAlive = true;

                    int dataLength = (int)request.GetResponse().ContentLength;

                    //this.Text = string.Concat(this.Text, "         ", "Downloading File...");
                    Application.DoEvents();

                    //Now get the actual data
                    request = FtpWebRequest.Create("ftp://" + FTPAddress + "/" + filename) as FtpWebRequest;
                    request.Method = WebRequestMethods.Ftp.DownloadFile;
                    request.Credentials = new NetworkCredential(username, password);
                    request.UsePassive = true;
                    request.UseBinary = true;
                    request.KeepAlive = false;

                    //Streams
                    FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                    Stream reader = response.GetResponseStream();

                    //Download to memory
                    MemoryStream memStream = new MemoryStream();
                    byte[] buffer = new byte[1024];

                    while (true)
                    {
                        Application.DoEvents();

                        int bytesRead = reader.Read(buffer, 0, buffer.Length);

                        if (bytesRead == 0)
                        {
                            Application.DoEvents();
                            break;
                        }
                        else
                        {
                            memStream.Write(buffer, 0, bytesRead);
                        }
                    }

                    downloadedData = memStream.ToArray();

                    reader.Close();
                    memStream.Close();
                    response.Close();
                    #endregion
                }
            }
            catch (Exception exc)
            {
                Sm.StdMsg(mMsgType.Warning, exc.ToString());
            }
        }


        #endregion

        #endregion

        #region Event

        #region Event Lue Item

        #region lueEditvalue

        private void LueItCode1_EditValueChanged_1(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCode1, new Sm.RefreshLue3(SetLueItCode), Sm.GetLue(LueCtCode), TxtSIDocNo.Text);
        }

        private void LueItCode2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCode2, new Sm.RefreshLue3(SetLueItCode), Sm.GetLue(LueCtCode), TxtSIDocNo.Text);
        }

        private void LueItCode3_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCode3, new Sm.RefreshLue3(SetLueItCode), Sm.GetLue(LueCtCode), TxtSIDocNo.Text);
        }

        private void LueItCode4_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCode4, new Sm.RefreshLue3(SetLueItCode), Sm.GetLue(LueCtCode), TxtSIDocNo.Text);
        }

        private void LueItCode5_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCode5, new Sm.RefreshLue3(SetLueItCode), Sm.GetLue(LueCtCode), TxtSIDocNo.Text);
        }

        private void LueItCode6_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCode6, new Sm.RefreshLue3(SetLueItCode), Sm.GetLue(LueCtCode), TxtSIDocNo.Text);
        }

        private void LueItCode7_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCode7, new Sm.RefreshLue3(SetLueItCode), Sm.GetLue(LueCtCode), TxtSIDocNo.Text);
        }

        private void LueItCode8_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCode8, new Sm.RefreshLue3(SetLueItCode), Sm.GetLue(LueCtCode), TxtSIDocNo.Text);
        }

        private void LueItCode9_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCode9, new Sm.RefreshLue3(SetLueItCode), Sm.GetLue(LueCtCode), TxtSIDocNo.Text);
        }

        private void LueItCode10_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCode10, new Sm.RefreshLue3(SetLueItCode), Sm.GetLue(LueCtCode), TxtSIDocNo.Text);
        }

        private void LueItCode11_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCode11, new Sm.RefreshLue3(SetLueItCode), Sm.GetLue(LueCtCode), TxtSIDocNo.Text);
        }

        private void LueItCode12_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCode12, new Sm.RefreshLue3(SetLueItCode), Sm.GetLue(LueCtCode), TxtSIDocNo.Text);
        }

        private void LueItCode13_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCode13, new Sm.RefreshLue3(SetLueItCode), Sm.GetLue(LueCtCode), TxtSIDocNo.Text);
        }

        private void LueItCode14_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCode14, new Sm.RefreshLue3(SetLueItCode), Sm.GetLue(LueCtCode), TxtSIDocNo.Text);
        }

        private void LueItCode15_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCode15, new Sm.RefreshLue3(SetLueItCode), Sm.GetLue(LueCtCode), TxtSIDocNo.Text);
        }

        private void LueItCode16_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCode16, new Sm.RefreshLue3(SetLueItCode), Sm.GetLue(LueCtCode), TxtSIDocNo.Text);
        }

        private void LueItCode17_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCode17, new Sm.RefreshLue3(SetLueItCode), Sm.GetLue(LueCtCode), TxtSIDocNo.Text);
        }

        private void LueItCode18_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCode18, new Sm.RefreshLue3(SetLueItCode), Sm.GetLue(LueCtCode), TxtSIDocNo.Text);
        }

        private void LueItCode19_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCode19, new Sm.RefreshLue3(SetLueItCode), Sm.GetLue(LueCtCode), TxtSIDocNo.Text);
        }

        private void LueItCode20_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCode20, new Sm.RefreshLue3(SetLueItCode), Sm.GetLue(LueCtCode), TxtSIDocNo.Text);
        }

        private void LueItCode21_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCode21, new Sm.RefreshLue3(SetLueItCode), Sm.GetLue(LueCtCode), TxtSIDocNo.Text);
        }

        private void LueItCode22_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCode22, new Sm.RefreshLue3(SetLueItCode), Sm.GetLue(LueCtCode), TxtSIDocNo.Text);
        }

        private void LueItCode23_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCode23, new Sm.RefreshLue3(SetLueItCode), Sm.GetLue(LueCtCode), TxtSIDocNo.Text);
        }

        private void LueItCode24_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCode24, new Sm.RefreshLue3(SetLueItCode), Sm.GetLue(LueCtCode), TxtSIDocNo.Text);
        }

        private void LueItCode25_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueItCode25, new Sm.RefreshLue3(SetLueItCode), Sm.GetLue(LueCtCode), TxtSIDocNo.Text);
        }

        #endregion

        #region LueKey

        private void LueItCode1_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd11, ref fAccept, e);
        }

        private void LueItCode2_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd12, ref fAccept, e);
        }

        private void LueItCode3_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd13, ref fAccept, e);
        }

        private void LueItCode4_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd14, ref fAccept, e);
        }

        private void LueItCode5_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd15, ref fAccept, e);
        }

        private void LueItCode6_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd16, ref fAccept, e);
        }

        private void LueItCode7_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd17, ref fAccept, e);
        }

        private void LueItCode8_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd18, ref fAccept, e);
        }

        private void LueItCode9_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd19, ref fAccept, e);
        }

        private void LueItCode10_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd20, ref fAccept, e);
        }

        private void LueItCode11_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd21, ref fAccept, e);
        }

        private void LueItCode12_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd22, ref fAccept, e);
        }

        private void LueItCode13_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd23, ref fAccept, e);
        }

        private void LueItCode14_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd24, ref fAccept, e);
        }

        private void LueItCode15_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd25, ref fAccept, e);
        }

        private void LueItCode16_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd26, ref fAccept, e);
        }

        private void LueItCode17_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd27, ref fAccept, e);
        }

        private void LueItCode18_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd28, ref fAccept, e);
        }

        private void LueItCode19_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd29, ref fAccept, e);
        }

        private void LueItCode20_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd30, ref fAccept, e);
        }

        private void LueItCode21_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd31, ref fAccept, e);
        }

        private void LueItCode22_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd32, ref fAccept, e);
        }

        private void LueItCode23_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd33, ref fAccept, e);
        }

        private void LueItCode24_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd34, ref fAccept, e);
        }

        private void LueItCode25_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd35, ref fAccept, e);
        }


        #endregion

        #region LueLeave

        private void LueItCode1_Leave(object sender, EventArgs e)
        {
            if (LueItCode1.Visible && fAccept && fCell.ColIndex == 1)
            {
                if (Sm.GetLue(LueItCode1).Length == 0)
                    Grd11.Cells[fCell.RowIndex, 0].Value =
                    Grd11.Cells[fCell.RowIndex, 1].Value = null;
                else
                {
                    Grd11.Cells[fCell.RowIndex, 0].Value = LueItCode1.GetColumnValue("Col1");
                    Grd11.Cells[fCell.RowIndex, 1].Value = LueItCode1.GetColumnValue("Col2");
                    Grd11.Cells[fCell.RowIndex, 4].Value = LueItCode1.GetColumnValue("Col3");
                    string Col4 = LueItCode1.GetColumnValue("Col4").ToString();
                    Grd11.Cells[fCell.RowIndex, 6].Value = Col4.Substring(Col4.Length - 3, 3); 
                    ShowSOData2(Sm.GetGrdStr(Grd11, fCell.RowIndex, 4), Sm.GetGrdStr(Grd11, fCell.RowIndex, 6), Grd11, fCell.RowIndex);
                }
                LueItCode1.Visible = false;
            }
        }

        private void LueItCode2_Leave(object sender, EventArgs e)
        {
            if (LueItCode2.Visible && fAccept && fCell.ColIndex == 1)
            {
                if (Sm.GetLue(LueItCode2).Length == 0)
                    Grd12.Cells[fCell.RowIndex, 0].Value =
                    Grd12.Cells[fCell.RowIndex, 1].Value = null;
                else
                {
                    Grd12.Cells[fCell.RowIndex, 0].Value = LueItCode2.GetColumnValue("Col1");
                    Grd12.Cells[fCell.RowIndex, 1].Value = LueItCode2.GetColumnValue("Col2");
                    Grd12.Cells[fCell.RowIndex, 4].Value = LueItCode2.GetColumnValue("Col3");
                    string Col4 = LueItCode2.GetColumnValue("Col4").ToString();
                    Grd12.Cells[fCell.RowIndex, 6].Value = Col4.Substring(Col4.Length - 3, 3);
                    ShowSOData2(Sm.GetGrdStr(Grd12, fCell.RowIndex, 4), Sm.GetGrdStr(Grd12, fCell.RowIndex, 6), Grd12, fCell.RowIndex);
                }
                LueItCode2.Visible = false;
            }
        }

        private void LueItCode3_Leave(object sender, EventArgs e)
        {
            if (LueItCode3.Visible && fAccept && fCell.ColIndex == 1)
            {
                if (Sm.GetLue(LueItCode3).Length == 0)
                    Grd13.Cells[fCell.RowIndex, 0].Value =
                    Grd13.Cells[fCell.RowIndex, 1].Value = null;
                else
                {
                    Grd13.Cells[fCell.RowIndex, 0].Value = LueItCode3.GetColumnValue("Col1");
                    Grd13.Cells[fCell.RowIndex, 1].Value = LueItCode3.GetColumnValue("Col2");
                    Grd13.Cells[fCell.RowIndex, 4].Value = LueItCode3.GetColumnValue("Col3");
                    string Col4 = LueItCode3.GetColumnValue("Col4").ToString();
                    Grd13.Cells[fCell.RowIndex, 6].Value = Col4.Substring(Col4.Length - 3, 3);
                    ShowSOData2(Sm.GetGrdStr(Grd13, fCell.RowIndex, 4), Sm.GetGrdStr(Grd13, fCell.RowIndex, 6), Grd13, fCell.RowIndex);
                }
                LueItCode3.Visible = false;
            }
        }

        private void LueItCode4_Leave(object sender, EventArgs e)
        {
            if (LueItCode4.Visible && fAccept && fCell.ColIndex == 1)
            {
                if (Sm.GetLue(LueItCode4).Length == 0)
                    Grd14.Cells[fCell.RowIndex, 0].Value =
                    Grd14.Cells[fCell.RowIndex, 1].Value = null;
                else
                {
                    Grd14.Cells[fCell.RowIndex, 0].Value = LueItCode4.GetColumnValue("Col1");
                    Grd14.Cells[fCell.RowIndex, 1].Value = LueItCode4.GetColumnValue("Col2");
                    Grd14.Cells[fCell.RowIndex, 4].Value = LueItCode4.GetColumnValue("Col3");
                    string Col4 = LueItCode4.GetColumnValue("Col4").ToString();
                    Grd14.Cells[fCell.RowIndex, 6].Value = Col4.Substring(Col4.Length - 3, 3);
                    ShowSOData2(Sm.GetGrdStr(Grd14, fCell.RowIndex, 4), Sm.GetGrdStr(Grd14, fCell.RowIndex, 6), Grd14, fCell.RowIndex);
                }
                LueItCode4.Visible = false;
            }
        }

        private void LueItCode5_Leave(object sender, EventArgs e)
        {
            if (LueItCode5.Visible && fAccept && fCell.ColIndex == 1)
            {
                if (Sm.GetLue(LueItCode5).Length == 0)
                    Grd15.Cells[fCell.RowIndex, 0].Value =
                    Grd15.Cells[fCell.RowIndex, 1].Value = null;
                else
                {
                    Grd15.Cells[fCell.RowIndex, 0].Value = LueItCode5.GetColumnValue("Col1");
                    Grd15.Cells[fCell.RowIndex, 1].Value = LueItCode5.GetColumnValue("Col2");
                    Grd15.Cells[fCell.RowIndex, 4].Value = LueItCode5.GetColumnValue("Col3");
                    string Col5 = LueItCode5.GetColumnValue("Col4").ToString();
                    Grd15.Cells[fCell.RowIndex, 6].Value = Col5.Substring(Col5.Length - 3, 3);
                    ShowSOData2(Sm.GetGrdStr(Grd15, fCell.RowIndex, 4), Sm.GetGrdStr(Grd15, fCell.RowIndex, 6), Grd15, fCell.RowIndex);
                }
                LueItCode5.Visible = false;
            }
        }

        private void LueItCode6_Leave(object sender, EventArgs e)
        {
            if (LueItCode6.Visible && fAccept && fCell.ColIndex == 1)
            {
                if (Sm.GetLue(LueItCode6).Length == 0)
                    Grd16.Cells[fCell.RowIndex, 0].Value =
                    Grd16.Cells[fCell.RowIndex, 1].Value = null;
                else
                {
                    Grd16.Cells[fCell.RowIndex, 0].Value = LueItCode6.GetColumnValue("Col1");
                    Grd16.Cells[fCell.RowIndex, 1].Value = LueItCode6.GetColumnValue("Col2");
                    Grd16.Cells[fCell.RowIndex, 4].Value = LueItCode6.GetColumnValue("Col3");
                    string Col6 = LueItCode6.GetColumnValue("Col4").ToString();
                    Grd16.Cells[fCell.RowIndex, 6].Value = Col6.Substring(Col6.Length - 3, 3);
                    ShowSOData2(Sm.GetGrdStr(Grd16, fCell.RowIndex, 4), Sm.GetGrdStr(Grd16, fCell.RowIndex, 6), Grd16, fCell.RowIndex);
                }
                LueItCode6.Visible = false;
            }
        }

        private void LueItCode7_Leave(object sender, EventArgs e)
        {
            if (LueItCode7.Visible && fAccept && fCell.ColIndex == 1)
            {
                if (Sm.GetLue(LueItCode7).Length == 0)
                    Grd17.Cells[fCell.RowIndex, 0].Value =
                    Grd17.Cells[fCell.RowIndex, 1].Value = null;
                else
                {
                    Grd17.Cells[fCell.RowIndex, 0].Value = LueItCode7.GetColumnValue("Col1");
                    Grd17.Cells[fCell.RowIndex, 1].Value = LueItCode7.GetColumnValue("Col2");
                    Grd17.Cells[fCell.RowIndex, 4].Value = LueItCode7.GetColumnValue("Col3");
                    string Col7 = LueItCode7.GetColumnValue("Col4").ToString();
                    Grd17.Cells[fCell.RowIndex, 6].Value = Col7.Substring(Col7.Length - 3, 3);
                    ShowSOData2(Sm.GetGrdStr(Grd17, fCell.RowIndex, 4), Sm.GetGrdStr(Grd17, fCell.RowIndex, 6), Grd17, fCell.RowIndex);
                }
                LueItCode7.Visible = false;
            }
        }

        private void LueItCode8_Leave(object sender, EventArgs e)
        {
            if (LueItCode8.Visible && fAccept && fCell.ColIndex == 1)
            {
                if (Sm.GetLue(LueItCode8).Length == 0)
                    Grd18.Cells[fCell.RowIndex, 0].Value =
                    Grd18.Cells[fCell.RowIndex, 1].Value = null;
                else
                {
                    Grd18.Cells[fCell.RowIndex, 0].Value = LueItCode8.GetColumnValue("Col1");
                    Grd18.Cells[fCell.RowIndex, 1].Value = LueItCode8.GetColumnValue("Col2");
                    Grd18.Cells[fCell.RowIndex, 4].Value = LueItCode8.GetColumnValue("Col3");
                    string Col8 = LueItCode8.GetColumnValue("Col4").ToString();
                    Grd18.Cells[fCell.RowIndex, 6].Value = Col8.Substring(Col8.Length - 3, 3);
                    ShowSOData2(Sm.GetGrdStr(Grd18, fCell.RowIndex, 4), Sm.GetGrdStr(Grd18, fCell.RowIndex, 6), Grd18, fCell.RowIndex);
                }
                LueItCode8.Visible = false;
            }
        }

        private void LueItCode9_Leave(object sender, EventArgs e)
        {
            if (LueItCode9.Visible && fAccept && fCell.ColIndex == 1)
            {
                if (Sm.GetLue(LueItCode9).Length == 0)
                    Grd19.Cells[fCell.RowIndex, 0].Value =
                    Grd19.Cells[fCell.RowIndex, 1].Value = null;
                else
                {
                    Grd19.Cells[fCell.RowIndex, 0].Value = LueItCode9.GetColumnValue("Col1");
                    Grd19.Cells[fCell.RowIndex, 1].Value = LueItCode9.GetColumnValue("Col2");
                    Grd19.Cells[fCell.RowIndex, 4].Value = LueItCode9.GetColumnValue("Col3");
                    string Col9 = LueItCode9.GetColumnValue("Col4").ToString();
                    Grd19.Cells[fCell.RowIndex, 6].Value = Col9.Substring(Col9.Length - 3, 3);
                    ShowSOData2(Sm.GetGrdStr(Grd19, fCell.RowIndex, 4), Sm.GetGrdStr(Grd19, fCell.RowIndex, 6), Grd19, fCell.RowIndex);
                }
                LueItCode9.Visible = false;
            }
        }

        private void LueItCode10_Leave(object sender, EventArgs e)
        {
            if (LueItCode10.Visible && fAccept && fCell.ColIndex == 1)
            {
                if (Sm.GetLue(LueItCode10).Length == 0)
                    Grd20.Cells[fCell.RowIndex, 0].Value =
                    Grd20.Cells[fCell.RowIndex, 1].Value = null;
                else
                {
                    Grd20.Cells[fCell.RowIndex, 0].Value = LueItCode10.GetColumnValue("Col1");
                    Grd20.Cells[fCell.RowIndex, 1].Value = LueItCode10.GetColumnValue("Col2");
                    Grd20.Cells[fCell.RowIndex, 4].Value = LueItCode10.GetColumnValue("Col3");
                    string Col10 = LueItCode10.GetColumnValue("Col4").ToString();
                    Grd20.Cells[fCell.RowIndex, 6].Value = Col10.Substring(Col10.Length - 3, 3);
                    ShowSOData2(Sm.GetGrdStr(Grd20, fCell.RowIndex, 4), Sm.GetGrdStr(Grd20, fCell.RowIndex, 6), Grd20, fCell.RowIndex);
                }
                LueItCode10.Visible = false;
            }
        }

        private void LueItCode11_Leave(object sender, EventArgs e)
        {
            if (LueItCode11.Visible && fAccept && fCell.ColIndex == 1)
            {
                if (Sm.GetLue(LueItCode11).Length == 0)
                    Grd21.Cells[fCell.RowIndex, 0].Value =
                    Grd21.Cells[fCell.RowIndex, 1].Value = null;
                else
                {
                    Grd21.Cells[fCell.RowIndex, 0].Value = LueItCode11.GetColumnValue("Col1");
                    Grd21.Cells[fCell.RowIndex, 1].Value = LueItCode11.GetColumnValue("Col2");
                    Grd21.Cells[fCell.RowIndex, 4].Value = LueItCode11.GetColumnValue("Col3");
                    string Col11 = LueItCode11.GetColumnValue("Col4").ToString();
                    Grd21.Cells[fCell.RowIndex, 6].Value = Col11.Substring(Col11.Length - 3, 3);
                    ShowSOData2(Sm.GetGrdStr(Grd21, fCell.RowIndex, 4), Sm.GetGrdStr(Grd21, fCell.RowIndex, 6), Grd21, fCell.RowIndex);
                }
                LueItCode11.Visible = false;
            }
        }

        private void LueItCode12_Leave(object sender, EventArgs e)
        {
            if (LueItCode12.Visible && fAccept && fCell.ColIndex == 1)
            {
                if (Sm.GetLue(LueItCode12).Length == 0)
                    Grd22.Cells[fCell.RowIndex, 0].Value =
                    Grd22.Cells[fCell.RowIndex, 1].Value = null;
                else
                {
                    Grd22.Cells[fCell.RowIndex, 0].Value = LueItCode12.GetColumnValue("Col1");
                    Grd22.Cells[fCell.RowIndex, 1].Value = LueItCode12.GetColumnValue("Col2");
                    Grd22.Cells[fCell.RowIndex, 4].Value = LueItCode12.GetColumnValue("Col3");
                    string Col12 = LueItCode12.GetColumnValue("Col4").ToString();
                    Grd22.Cells[fCell.RowIndex, 6].Value = Col12.Substring(Col12.Length - 3, 3);
                    ShowSOData2(Sm.GetGrdStr(Grd22, fCell.RowIndex, 4), Sm.GetGrdStr(Grd22, fCell.RowIndex, 6), Grd22, fCell.RowIndex);
                }
                LueItCode12.Visible = false;
            }
        }

        private void LueItCode13_Leave(object sender, EventArgs e)
        {
            if (LueItCode13.Visible && fAccept && fCell.ColIndex == 1)
            {
                if (Sm.GetLue(LueItCode13).Length == 0)
                    Grd23.Cells[fCell.RowIndex, 0].Value =
                    Grd23.Cells[fCell.RowIndex, 1].Value = null;
                else
                {
                    Grd23.Cells[fCell.RowIndex, 0].Value = LueItCode13.GetColumnValue("Col1");
                    Grd23.Cells[fCell.RowIndex, 1].Value = LueItCode13.GetColumnValue("Col2");
                    Grd23.Cells[fCell.RowIndex, 4].Value = LueItCode13.GetColumnValue("Col3");
                    string Col13 = LueItCode13.GetColumnValue("Col4").ToString();
                    Grd23.Cells[fCell.RowIndex, 6].Value = Col13.Substring(Col13.Length - 3, 3);
                    ShowSOData2(Sm.GetGrdStr(Grd23, fCell.RowIndex, 4), Sm.GetGrdStr(Grd23, fCell.RowIndex, 6), Grd23, fCell.RowIndex);
                }
                LueItCode13.Visible = false;
            }
        }

        private void LueItCode14_Leave(object sender, EventArgs e)
        {
            if (LueItCode14.Visible && fAccept && fCell.ColIndex == 1)
            {
                if (Sm.GetLue(LueItCode14).Length == 0)
                    Grd24.Cells[fCell.RowIndex, 0].Value =
                    Grd24.Cells[fCell.RowIndex, 1].Value = null;
                else
                {
                    Grd24.Cells[fCell.RowIndex, 0].Value = LueItCode14.GetColumnValue("Col1");
                    Grd24.Cells[fCell.RowIndex, 1].Value = LueItCode14.GetColumnValue("Col2");
                    Grd24.Cells[fCell.RowIndex, 4].Value = LueItCode14.GetColumnValue("Col3");
                    string Col14 = LueItCode14.GetColumnValue("Col4").ToString();
                    Grd24.Cells[fCell.RowIndex, 6].Value = Col14.Substring(Col14.Length - 3, 3);
                    ShowSOData2(Sm.GetGrdStr(Grd24, fCell.RowIndex, 4), Sm.GetGrdStr(Grd24, fCell.RowIndex, 6), Grd24, fCell.RowIndex);
                }
                LueItCode14.Visible = false;
            }
        }

        private void LueItCode15_Leave(object sender, EventArgs e)
        {
            if (LueItCode15.Visible && fAccept && fCell.ColIndex == 1)
            {
                if (Sm.GetLue(LueItCode15).Length == 0)
                    Grd25.Cells[fCell.RowIndex, 0].Value =
                    Grd25.Cells[fCell.RowIndex, 1].Value = null;
                else
                {
                    Grd25.Cells[fCell.RowIndex, 0].Value = LueItCode15.GetColumnValue("Col1");
                    Grd25.Cells[fCell.RowIndex, 1].Value = LueItCode15.GetColumnValue("Col2");
                    Grd25.Cells[fCell.RowIndex, 4].Value = LueItCode15.GetColumnValue("Col3");
                    string Col15 = LueItCode15.GetColumnValue("Col4").ToString();
                    Grd25.Cells[fCell.RowIndex, 6].Value = Col15.Substring(Col15.Length - 3, 3);
                    ShowSOData2(Sm.GetGrdStr(Grd25, fCell.RowIndex, 4), Sm.GetGrdStr(Grd25, fCell.RowIndex, 6), Grd25, fCell.RowIndex);
                }
                LueItCode15.Visible = false;
            }
        }

        private void LueItCode16_Leave(object sender, EventArgs e)
        {
            if (LueItCode16.Visible && fAccept && fCell.ColIndex == 1)
            {
                if (Sm.GetLue(LueItCode16).Length == 0)
                    Grd26.Cells[fCell.RowIndex, 0].Value =
                    Grd26.Cells[fCell.RowIndex, 1].Value = null;
                else
                {
                    Grd26.Cells[fCell.RowIndex, 0].Value = LueItCode16.GetColumnValue("Col1");
                    Grd26.Cells[fCell.RowIndex, 1].Value = LueItCode16.GetColumnValue("Col2");
                    Grd26.Cells[fCell.RowIndex, 4].Value = LueItCode16.GetColumnValue("Col3");
                    string Col16 = LueItCode16.GetColumnValue("Col4").ToString();
                    Grd26.Cells[fCell.RowIndex, 6].Value = Col16.Substring(Col16.Length - 3, 3);
                    ShowSOData2(Sm.GetGrdStr(Grd26, fCell.RowIndex, 4), Sm.GetGrdStr(Grd26, fCell.RowIndex, 6), Grd26, fCell.RowIndex);
                }
                LueItCode16.Visible = false;
            }
        }

        private void LueItCode17_Leave(object sender, EventArgs e)
        {
            if (LueItCode17.Visible && fAccept && fCell.ColIndex == 1)
            {
                if (Sm.GetLue(LueItCode17).Length == 0)
                    Grd27.Cells[fCell.RowIndex, 0].Value =
                    Grd27.Cells[fCell.RowIndex, 1].Value = null;
                else
                {
                    Grd27.Cells[fCell.RowIndex, 0].Value = LueItCode17.GetColumnValue("Col1");
                    Grd27.Cells[fCell.RowIndex, 1].Value = LueItCode17.GetColumnValue("Col2");
                    Grd27.Cells[fCell.RowIndex, 4].Value = LueItCode17.GetColumnValue("Col3");
                    string Col17 = LueItCode17.GetColumnValue("Col4").ToString();
                    Grd27.Cells[fCell.RowIndex, 6].Value = Col17.Substring(Col17.Length - 3, 3);
                    ShowSOData2(Sm.GetGrdStr(Grd27, fCell.RowIndex, 4), Sm.GetGrdStr(Grd27, fCell.RowIndex, 6), Grd27, fCell.RowIndex);
                }
                LueItCode17.Visible = false;
            }
        }

        private void LueItCode18_Leave(object sender, EventArgs e)
        {
            if (LueItCode18.Visible && fAccept && fCell.ColIndex == 1)
            {
                if (Sm.GetLue(LueItCode18).Length == 0)
                    Grd28.Cells[fCell.RowIndex, 0].Value =
                    Grd28.Cells[fCell.RowIndex, 1].Value = null;
                else
                {
                    Grd28.Cells[fCell.RowIndex, 0].Value = LueItCode18.GetColumnValue("Col1");
                    Grd28.Cells[fCell.RowIndex, 1].Value = LueItCode18.GetColumnValue("Col2");
                    Grd28.Cells[fCell.RowIndex, 4].Value = LueItCode18.GetColumnValue("Col3");
                    string Col18 = LueItCode18.GetColumnValue("Col4").ToString();
                    Grd28.Cells[fCell.RowIndex, 6].Value = Col18.Substring(Col18.Length - 3, 3);
                    ShowSOData2(Sm.GetGrdStr(Grd28, fCell.RowIndex, 4), Sm.GetGrdStr(Grd28, fCell.RowIndex, 6), Grd28, fCell.RowIndex);
                }
                LueItCode18.Visible = false;
            }
        }

        private void LueItCode19_Leave(object sender, EventArgs e)
        {
            if (LueItCode19.Visible && fAccept && fCell.ColIndex == 1)
            {
                if (Sm.GetLue(LueItCode19).Length == 0)
                    Grd29.Cells[fCell.RowIndex, 0].Value =
                    Grd29.Cells[fCell.RowIndex, 1].Value = null;
                else
                {
                    Grd29.Cells[fCell.RowIndex, 0].Value = LueItCode19.GetColumnValue("Col1");
                    Grd29.Cells[fCell.RowIndex, 1].Value = LueItCode19.GetColumnValue("Col2");
                    Grd29.Cells[fCell.RowIndex, 4].Value = LueItCode19.GetColumnValue("Col3");
                    string Col19 = LueItCode19.GetColumnValue("Col4").ToString();
                    Grd29.Cells[fCell.RowIndex, 6].Value = Col19.Substring(Col19.Length - 3, 3);
                    ShowSOData2(Sm.GetGrdStr(Grd29, fCell.RowIndex, 4), Sm.GetGrdStr(Grd29, fCell.RowIndex, 6), Grd29, fCell.RowIndex);
                }
                LueItCode19.Visible = false;
            }
        }

        private void LueItCode20_Leave(object sender, EventArgs e)
        {
            if (LueItCode20.Visible && fAccept && fCell.ColIndex == 1)
            {
                if (Sm.GetLue(LueItCode20).Length == 0)
                    Grd30.Cells[fCell.RowIndex, 0].Value =
                    Grd30.Cells[fCell.RowIndex, 1].Value = null;
                else
                {
                    Grd30.Cells[fCell.RowIndex, 0].Value = LueItCode20.GetColumnValue("Col1");
                    Grd30.Cells[fCell.RowIndex, 1].Value = LueItCode20.GetColumnValue("Col2");
                    Grd30.Cells[fCell.RowIndex, 4].Value = LueItCode20.GetColumnValue("Col3");
                    string Col20 = LueItCode20.GetColumnValue("Col4").ToString();
                    Grd30.Cells[fCell.RowIndex, 6].Value = Col20.Substring(Col20.Length - 3, 3);
                    ShowSOData2(Sm.GetGrdStr(Grd30, fCell.RowIndex, 4), Sm.GetGrdStr(Grd30, fCell.RowIndex, 6), Grd30, fCell.RowIndex);
                }
                LueItCode20.Visible = false;
            }
        }

        private void LueItCode21_Leave(object sender, EventArgs e)
        {
            if (LueItCode21.Visible && fAccept && fCell.ColIndex == 1)
            {
                if (Sm.GetLue(LueItCode21).Length == 0)
                    Grd31.Cells[fCell.RowIndex, 0].Value =
                    Grd31.Cells[fCell.RowIndex, 1].Value = null;
                else
                {
                    Grd31.Cells[fCell.RowIndex, 0].Value = LueItCode21.GetColumnValue("Col1");
                    Grd31.Cells[fCell.RowIndex, 1].Value = LueItCode21.GetColumnValue("Col2");
                    Grd31.Cells[fCell.RowIndex, 4].Value = LueItCode21.GetColumnValue("Col3");
                    string Col21 = LueItCode21.GetColumnValue("Col4").ToString();
                    Grd31.Cells[fCell.RowIndex, 6].Value = Col21.Substring(Col21.Length - 3, 3);
                    ShowSOData2(Sm.GetGrdStr(Grd31, fCell.RowIndex, 4), Sm.GetGrdStr(Grd31, fCell.RowIndex, 6), Grd31, fCell.RowIndex);
                }
                LueItCode21.Visible = false;
            }
        }

        private void LueItCode22_Leave(object sender, EventArgs e)
        {
            if (LueItCode22.Visible && fAccept && fCell.ColIndex == 1)
            {
                if (Sm.GetLue(LueItCode22).Length == 0)
                    Grd32.Cells[fCell.RowIndex, 0].Value =
                    Grd32.Cells[fCell.RowIndex, 1].Value = null;
                else
                {
                    Grd32.Cells[fCell.RowIndex, 0].Value = LueItCode22.GetColumnValue("Col1");
                    Grd32.Cells[fCell.RowIndex, 1].Value = LueItCode22.GetColumnValue("Col2");
                    Grd32.Cells[fCell.RowIndex, 4].Value = LueItCode22.GetColumnValue("Col3");
                    string Col22 = LueItCode22.GetColumnValue("Col4").ToString();
                    Grd32.Cells[fCell.RowIndex, 6].Value = Col22.Substring(Col22.Length - 3, 3);
                    ShowSOData2(Sm.GetGrdStr(Grd32, fCell.RowIndex, 4), Sm.GetGrdStr(Grd32, fCell.RowIndex, 6), Grd32, fCell.RowIndex);
                }
                LueItCode22.Visible = false;
            }
        }

        private void LueItCode23_Leave(object sender, EventArgs e)
        {
            if (LueItCode23.Visible && fAccept && fCell.ColIndex == 1)
            {
                if (Sm.GetLue(LueItCode23).Length == 0)
                    Grd33.Cells[fCell.RowIndex, 0].Value =
                    Grd33.Cells[fCell.RowIndex, 1].Value = null;
                else
                {
                    Grd33.Cells[fCell.RowIndex, 0].Value = LueItCode23.GetColumnValue("Col1");
                    Grd33.Cells[fCell.RowIndex, 1].Value = LueItCode23.GetColumnValue("Col2");
                    Grd33.Cells[fCell.RowIndex, 4].Value = LueItCode23.GetColumnValue("Col3");
                    string Col23 = LueItCode23.GetColumnValue("Col4").ToString();
                    Grd33.Cells[fCell.RowIndex, 6].Value = Col23.Substring(Col23.Length - 3, 3);
                    ShowSOData2(Sm.GetGrdStr(Grd33, fCell.RowIndex, 4), Sm.GetGrdStr(Grd33, fCell.RowIndex, 6), Grd33, fCell.RowIndex);
                }
                LueItCode23.Visible = false;
            }
        }

        private void LueItCode24_Leave(object sender, EventArgs e)
        {
            if (LueItCode24.Visible && fAccept && fCell.ColIndex == 1)
            {
                if (Sm.GetLue(LueItCode24).Length == 0)
                    Grd34.Cells[fCell.RowIndex, 0].Value =
                    Grd34.Cells[fCell.RowIndex, 1].Value = null;
                else
                {
                    Grd34.Cells[fCell.RowIndex, 0].Value = LueItCode24.GetColumnValue("Col1");
                    Grd34.Cells[fCell.RowIndex, 1].Value = LueItCode24.GetColumnValue("Col2");
                    Grd34.Cells[fCell.RowIndex, 4].Value = LueItCode24.GetColumnValue("Col3");
                    string Col24 = LueItCode24.GetColumnValue("Col4").ToString();
                    Grd34.Cells[fCell.RowIndex, 6].Value = Col24.Substring(Col24.Length - 3, 3);
                    ShowSOData2(Sm.GetGrdStr(Grd34, fCell.RowIndex, 4), Sm.GetGrdStr(Grd34, fCell.RowIndex, 6), Grd34, fCell.RowIndex);
                }
                LueItCode24.Visible = false;
            }
        }

        private void LueItCode25_Leave(object sender, EventArgs e)
        {
            if (LueItCode25.Visible && fAccept && fCell.ColIndex == 1)
            {
                if (Sm.GetLue(LueItCode25).Length == 0)
                    Grd35.Cells[fCell.RowIndex, 0].Value =
                    Grd35.Cells[fCell.RowIndex, 1].Value = null;
                else
                {
                    Grd35.Cells[fCell.RowIndex, 0].Value = LueItCode25.GetColumnValue("Col1");
                    Grd35.Cells[fCell.RowIndex, 1].Value = LueItCode25.GetColumnValue("Col2");
                    Grd35.Cells[fCell.RowIndex, 4].Value = LueItCode25.GetColumnValue("Col3");
                    string Col25 = LueItCode15.GetColumnValue("Col4").ToString();
                    Grd35.Cells[fCell.RowIndex, 6].Value = Col25.Substring(Col25.Length - 3, 3);
                    ShowSOData2(Sm.GetGrdStr(Grd35, fCell.RowIndex, 4), Sm.GetGrdStr(Grd35, fCell.RowIndex, 6), Grd35, fCell.RowIndex);
                }
                LueItCode25.Visible = false;
            }
        }


        #endregion

        #region Event textedit Container

        private void TxtCnt1_Validated(object sender, EventArgs e)
        {
            CheckCnt(TxtCnt1, TxtCnt1.Text);
        }

        private void TxtCnt2_Validated(object sender, EventArgs e)
        {
            CheckCnt(TxtCnt2, TxtCnt2.Text);
        }

        private void TxtCnt3_Validated(object sender, EventArgs e)
        {
            CheckCnt(TxtCnt3, TxtCnt3.Text);
        }

        private void TxtCnt4_Validated(object sender, EventArgs e)
        {
            CheckCnt(TxtCnt4, TxtCnt4.Text);
        }

        private void TxtCnt5_Validated(object sender, EventArgs e)
        {
            CheckCnt(TxtCnt5, TxtCnt5.Text);
        }

        private void TxtCnt6_Validated(object sender, EventArgs e)
        {
            CheckCnt(TxtCnt6, TxtCnt6.Text);
        }

        private void TxtCnt7_Validated(object sender, EventArgs e)
        {
            CheckCnt(TxtCnt7, TxtCnt7.Text);
        }

        private void TxtCnt8_Validated(object sender, EventArgs e)
        {
            CheckCnt(TxtCnt8, TxtCnt8.Text);
        }

        private void TxtCnt9_Validated(object sender, EventArgs e)
        {
            CheckCnt(TxtCnt9, TxtCnt9.Text);
        }

        private void TxtCnt10_Validated(object sender, EventArgs e)
        {
            CheckCnt(TxtCnt10, TxtCnt10.Text);
        }

        private void TxtCnt11_Validated(object sender, EventArgs e)
        {
            CheckCnt(TxtCnt11, TxtCnt11.Text);
        }

        private void TxtCnt12_Validated(object sender, EventArgs e)
        {
            CheckCnt(TxtCnt12, TxtCnt12.Text);
        }

        private void TxtCnt13_Validated(object sender, EventArgs e)
        {
            CheckCnt(TxtCnt13, TxtCnt13.Text);
        }

        private void TxtCnt14_Validated(object sender, EventArgs e)
        {
            CheckCnt(TxtCnt14, TxtCnt14.Text);
        }

        private void TxtCnt15_Validated(object sender, EventArgs e)
        {
            CheckCnt(TxtCnt15, TxtCnt15.Text);
        }

        private void TxtCnt16_Validated(object sender, EventArgs e)
        {
            CheckCnt(TxtCnt16, TxtCnt16.Text);
        }

        private void TxtCnt17_Validated(object sender, EventArgs e)
        {
            CheckCnt(TxtCnt17, TxtCnt17.Text);
        }

        private void TxtCnt18_Validated(object sender, EventArgs e)
        {
            CheckCnt(TxtCnt18, TxtCnt18.Text);
        }

        private void TxtCnt19_Validated(object sender, EventArgs e)
        {
            CheckCnt(TxtCnt19, TxtCnt19.Text);
        }

        private void TxtCnt20_Validated(object sender, EventArgs e)
        {
            CheckCnt(TxtCnt20, TxtCnt20.Text);
        }

        private void TxtCnt21_Validated(object sender, EventArgs e)
        {
            CheckCnt(TxtCnt21, TxtCnt21.Text);
        }

        private void TxtCnt22_Validated(object sender, EventArgs e)
        {
            CheckCnt(TxtCnt22, TxtCnt22.Text);
        }

        private void TxtCnt23_Validated(object sender, EventArgs e)
        {
            CheckCnt(TxtCnt23, TxtCnt23.Text);
        }

        private void TxtCnt24_Validated(object sender, EventArgs e)
        {
            CheckCnt(TxtCnt24, TxtCnt24.Text);
        }

        private void TxtCnt25_Validated(object sender, EventArgs e)
        {
            CheckCnt(TxtCnt25, TxtCnt25.Text);
        }


        #endregion

        #region event texedit seal

        private void TxtSeal1_Validated(object sender, EventArgs e)
        {
            CheckSeal(TxtSeal1, TxtSeal1.Text);
        }

        private void TxtSeal2_Validated(object sender, EventArgs e)
        {
            CheckSeal(TxtSeal2, TxtSeal2.Text);
        }

        private void TxtSeal3_Validated(object sender, EventArgs e)
        {
            CheckSeal(TxtSeal3, TxtSeal3.Text);
        }

        private void TxtSeal4_Validated(object sender, EventArgs e)
        {
            CheckSeal(TxtSeal4, TxtSeal4.Text);
        }

        private void TxtSeal5_Validated(object sender, EventArgs e)
        {
            CheckSeal(TxtSeal5, TxtSeal5.Text);
        }

        private void TxtSeal6_Validated(object sender, EventArgs e)
        {
            CheckSeal(TxtSeal6, TxtSeal6.Text);
        }

        private void TxtSeal7_Validated(object sender, EventArgs e)
        {
            CheckSeal(TxtSeal7, TxtSeal7.Text);
        }

        private void TxtSeal8_Validated(object sender, EventArgs e)
        {
            CheckSeal(TxtSeal8, TxtSeal8.Text);
        }

        private void TxtSeal9_Validated(object sender, EventArgs e)
        {
            CheckSeal(TxtSeal9, TxtSeal9.Text);
        }

        private void TxtSeal10_Validated(object sender, EventArgs e)
        {
            CheckSeal(TxtSeal10, TxtSeal10.Text);
        }

        private void TxtSeal11_Validated(object sender, EventArgs e)
        {
            CheckSeal(TxtSeal11, TxtSeal11.Text);
        }

        private void TxtSeal12_Validated(object sender, EventArgs e)
        {
            CheckSeal(TxtSeal12, TxtSeal12.Text);
        }

        private void TxtSeal13_Validated(object sender, EventArgs e)
        {
            CheckSeal(TxtSeal13, TxtSeal13.Text);
        }

        private void TxtSeal14_Validated(object sender, EventArgs e)
        {
            CheckSeal(TxtSeal14, TxtSeal14.Text);
        }

        private void TxtSeal15_Validated(object sender, EventArgs e)
        {
            CheckSeal(TxtSeal15, TxtSeal15.Text);
        }

        private void TxtSeal16_Validated(object sender, EventArgs e)
        {
            CheckSeal(TxtSeal16, TxtSeal16.Text);
        }

        private void TxtSeal17_Validated(object sender, EventArgs e)
        {
            CheckSeal(TxtSeal17, TxtSeal17.Text);
        }

        private void TxtSeal18_Validated(object sender, EventArgs e)
        {
            CheckSeal(TxtSeal18, TxtSeal18.Text);
        }

        private void TxtSeal19_EditValueChanged(object sender, EventArgs e)
        {
            CheckSeal(TxtSeal19, TxtSeal19.Text);
        }

        private void TxtSeal20_Validated(object sender, EventArgs e)
        {
            CheckSeal(TxtSeal20, TxtSeal20.Text);
        }

        private void TxtSeal21_Validated(object sender, EventArgs e)
        {
            CheckSeal(TxtSeal21, TxtSeal21.Text);
        }

        private void TxtSeal22_Validated(object sender, EventArgs e)
        {
            CheckSeal(TxtSeal22, TxtSeal22.Text);
        }

        private void TxtSeal23_Validated(object sender, EventArgs e)
        {
            CheckSeal(TxtSeal23, TxtSeal23.Text);
        }

        private void TxtSeal24_Validated(object sender, EventArgs e)
        {
            CheckSeal(TxtSeal24, TxtSeal24.Text);
        }

        private void TxtSeal25_Validated(object sender, EventArgs e)
        {
            CheckSeal(TxtSeal25, TxtSeal25.Text);
        }


        #endregion

        #endregion

        #region Events LueSize

        private void LueSize1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSize1, new Sm.RefreshLue1(SetLueSize));
        }

        private void LueSize2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSize2, new Sm.RefreshLue1(SetLueSize));
        }

        private void LueSize3_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSize3, new Sm.RefreshLue1(SetLueSize));
        }

        private void LueSize4_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSize4, new Sm.RefreshLue1(SetLueSize));
        }

        private void LueSize5_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSize5, new Sm.RefreshLue1(SetLueSize));
        }

        private void LueSize6_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSize6, new Sm.RefreshLue1(SetLueSize));
        }

        private void LueSize7_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSize7, new Sm.RefreshLue1(SetLueSize));
        }

        private void LueSize8_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSize8, new Sm.RefreshLue1(SetLueSize));
        }

        private void LueSize9_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSize9, new Sm.RefreshLue1(SetLueSize));
        }

        private void LueSize10_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSize10, new Sm.RefreshLue1(SetLueSize));
        }

        private void LueSize11_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSize11, new Sm.RefreshLue1(SetLueSize));
        }

        private void LueSize12_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSize12, new Sm.RefreshLue1(SetLueSize));
        }

        private void LueSize13_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSize13, new Sm.RefreshLue1(SetLueSize));
        }

        private void LueSize14_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSize14, new Sm.RefreshLue1(SetLueSize));
        }

        private void LueSize15_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSize15, new Sm.RefreshLue1(SetLueSize));
        }

        private void LueSize16_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSize16, new Sm.RefreshLue1(SetLueSize));
        }

        private void LueSize17_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSize17, new Sm.RefreshLue1(SetLueSize));
        }

        private void LueSize18_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSize18, new Sm.RefreshLue1(SetLueSize));
        }

        private void LueSize19_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSize19, new Sm.RefreshLue1(SetLueSize));
        }

        private void LueSize20_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSize20, new Sm.RefreshLue1(SetLueSize));
        }

        private void LueSize21_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSize21, new Sm.RefreshLue1(SetLueSize));
        }

        private void LueSize22_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSize22, new Sm.RefreshLue1(SetLueSize));
        }

        private void LueSize23_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSize23, new Sm.RefreshLue1(SetLueSize));
        }

        private void LueSize24_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSize24, new Sm.RefreshLue1(SetLueSize));
        }

        private void LueSize25_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSize25, new Sm.RefreshLue1(SetLueSize));
        }

        #endregion

        #region Button Click

        private void BtnSI_Click_1(object sender, EventArgs e)
        {
            TxtSIDocNo.Text = "";
            if (!Sm.IsLueEmpty(LueCtCode, "Customer"))
            {
                Sm.FormShowDialog(new FrmPLDlg(this, Sm.GetLue(LueCtCode)));
            }

            if (TxtSIDocNo.Text.Length != 0)
            {
                TxtLocalDocNo.EditValue = mIsNotCopySalesLocalDocNo ? "" : Sm.GetValue("Select LocalDocNo From TblSIhdr Where DocNo='" + TxtSIDocNo.Text + "' ");
            }
        }

        private void BtnSI2_Click(object sender, EventArgs e)
        {
            if (TxtSIDocNo.Text.Length != 0)
            {
                var f1 = new FrmSI(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = TxtSIDocNo.Text;
                f1.ShowDialog();
            }
        }

        private void BtnSource_Click(object sender, EventArgs e)
        {
            if (!Sm.IsLueEmpty(LueCtCode, "Customer"))
                Sm.FormShowDialog(new FrmPLDlg2(this, Sm.GetLue(LueCtCode)));
        }

        #endregion

        private void LueCtCode_EditValueChanged_1(object sender, EventArgs e)
        {
            if (TxtDocNo.Text.Length == 0)
            {
                TxtSIDocNo.Text = "";
                TxtLocalDocNo.Text = "";
            }
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                MeeAccount,
                TxtSalesContract, TxtLcNo, LCDocDt, MeeIssued, MeeIssued2, MeeIssued3,
                MeeIssued4, MeeOrder, MeeOrder2, MeeOrder3, MeeOrder4, TxtSource,
                TxtCnt1, TxtCnt2, TxtCnt3, TxtCnt4, TxtCnt5, TxtCnt6, TxtCnt7, TxtCnt8, TxtCnt9, TxtCnt10,
                TxtCnt11, TxtCnt12, TxtCnt13, TxtCnt14, TxtCnt15,
                TxtSeal1, TxtSeal2, TxtSeal3, TxtSeal4, TxtSeal5, TxtSeal6, TxtSeal7, TxtSeal8, TxtSeal9, TxtSeal10,
                TxtSeal11, TxtSeal12, TxtSeal13, TxtSeal14, TxtSeal15, 
                MeeSM1, MeeSM2, MeeSM3, MeeSM4, MeeSM5, MeeSM6, MeeSM7, MeeSM8, MeeSM9, MeeSM10, MeeSM11, MeeSM12, MeeSM13, 
                MeeSM14, MeeSM15, MeeSM16, MeeSM17, MeeSM18, MeeSM19, MeeSM20, MeeSM21, MeeSM22, MeeSM23, MeeSM24, MeeSM25, 
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit>
            {
                TxtTotal1, TxtTotal2, TxtTotal3, TxtTotal4, TxtTotal5, TxtTotal6, TxtTotal7, TxtTotal8, TxtTotal9, TxtTotal10,
                TxtTotal11, TxtTotal12, TxtTotal13, TxtTotal14, TxtTotal15
            }, 0);

            ClearGrd(new List<iGrid>(){
                Grd11, Grd12, Grd13, Grd14, Grd15,
                Grd16, Grd17, Grd18, Grd19, Grd20,
                Grd21, Grd22, Grd23, Grd24, Grd25, Grd1
            });

            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue2(SetLueCtCode), string.Empty);


            if (Sm.GetLue(LueCtCode).Length == 0)
            {
                LueNotify.EditValue = null;
                Sm.SetControlReadOnly(LueNotify, true);
            }
            else
            {
                SetLueNotify(ref LueNotify, Sm.GetLue(LueCtCode), "");
                Sm.SetControlReadOnly(LueNotify, false);

                string mShippingMark = Sm.GetValue("Select IfNull(ShippingMark, '') SM From TblCustomer Where CtCode = @Param Limit 1; ", Sm.GetLue(LueCtCode));
                MeeSM1.EditValue = MeeSM2.EditValue = MeeSM3.EditValue = MeeSM4.EditValue = MeeSM5.EditValue =
                MeeSM6.EditValue = MeeSM7.EditValue = MeeSM8.EditValue = MeeSM9.EditValue = MeeSM10.EditValue =
                MeeSM11.EditValue = MeeSM12.EditValue = MeeSM13.EditValue = MeeSM14.EditValue = MeeSM15.EditValue =
                MeeSM16.EditValue = MeeSM17.EditValue = MeeSM18.EditValue = MeeSM19.EditValue = MeeSM20.EditValue =
                MeeSM21.EditValue = MeeSM22.EditValue = MeeSM23.EditValue = MeeSM24.EditValue = MeeSM25.EditValue = mShippingMark;
            }
        }

        private void LueNotify_EditValueChanged(object sender, EventArgs e)
        {
            if (TxtDocNo.Text.Length == 0)
            {
                Sm.RefreshLookUpEdit(LueNotify, new Sm.RefreshLue3(SetLueNotify), Sm.GetLue(LueCtCode), string.Empty);
            }
        }

        private void BtnDownload_Click(object sender, EventArgs e)
        {
            List<string> FileName = new List<string>();

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.LocalDocNo, A.FileName1, A.FileName2, A.FileName3, A.FileName4, A.FileName5, ");
            SQL.AppendLine("A.FileName6, A.FileName7, A.FileName8, A.FileName9, A.FileName10, ");
            SQL.AppendLine("A.FileName11, A.FileName12, A.FileName13, A.FileName14, A.FileName15, ");
            SQL.AppendLine("A.FileName16, A.FileName17, A.FileName18, A.FileName19, A.FileName20 ");
            SQL.AppendLine("From TblAttachmentFile A ");
            SQL.AppendLine("Where A.LocalDocNo=@LocalDocNo;");

            Sm.CmParam<String>(ref cm, "@localDocNo", TxtLocalDocNo.Text);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "FileName1", 
                        //1-5
                        "FileName2", "FileName3", "FileName4", "FileName5", "FileName6",  
                        //6-10
                        "FileName7", "FileName8", "FileName9", "FileName10", "FileName11", 
                        //11-15
                        "FileName12", "FileName13", "FileName14", "FileName15", "FileName16", 
                        //16-19
                        "FileName17", "FileName18", "FileName19", "FileName20"                         
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        if (Sm.DrStr(dr, c[0]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[0]));
                        if (Sm.DrStr(dr, c[1]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[1]));
                        if (Sm.DrStr(dr, c[2]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[2]));
                        if (Sm.DrStr(dr, c[3]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[3]));
                        if (Sm.DrStr(dr, c[4]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[4]));
                        if (Sm.DrStr(dr, c[5]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[5]));
                        if (Sm.DrStr(dr, c[6]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[6]));
                        if (Sm.DrStr(dr, c[7]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[7]));
                        if (Sm.DrStr(dr, c[8]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[8]));
                        if (Sm.DrStr(dr, c[9]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[9]));
                        if (Sm.DrStr(dr, c[10]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[10]));
                        if (Sm.DrStr(dr, c[11]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[11]));
                        if (Sm.DrStr(dr, c[12]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[12]));
                        if (Sm.DrStr(dr, c[13]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[13]));
                        if (Sm.DrStr(dr, c[14]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[14]));
                        if (Sm.DrStr(dr, c[15]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[15]));
                        if (Sm.DrStr(dr, c[16]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[16]));
                        if (Sm.DrStr(dr, c[17]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[17]));
                        if (Sm.DrStr(dr, c[18]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[18]));
                        if (Sm.DrStr(dr, c[19]).Length > 0)
                            FileName.Add(Sm.DrStr(dr, c[19]));
                    }, false
                );
            if (FileName.Count > 0)
            {
                for (int i = 0; i < FileName.Count; i++)
                {
                    DownloadFileKu(FileName[i]);
                }
            }
            else
            {
                Sm.StdMsg(mMsgType.Warning, "No Attachment File");
            }
        }

        #endregion

        #region Report Class

        private class PLHdr
        {
            public string CompanyLogo { set; get; }
            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyAddressCity { get; set; }
            public string CompanyPhone { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string Account { get; set; }
            public string NotifyParty { get; set; }
            public string Invoice { get; set; }
            public string SalesContract { get; set; }
            public string LCNo { get; set; }
            public string LCDate { get; set; }
            public string Issued1 { get; set; }
            public string Issued2 { get; set; }
            public string Issued3 { get; set; }
            public string Issued4 { get; set; }
            public string ToOrder1 { get; set; }
            public string ToOrder2 { get; set; }
            public string ToOrder3 { get; set; }
            public string ToOrder4 { get; set; }
            public string PortLoading { get; set; }
            public string PortDischarge { get; set; }
            public string Stufing { get; set; }
            public string Remark { get; set; }
            public string Remark2 { get; set; }
            public string Remark3 { get; set; }
            public string UomNWGW { get; set; }
            public string SpName { get; set; }
            public string SM { get; set; }
            public string CompanyFax { get; set; }
            public string LocalDocNo { get; set; }
            public string Customer { get; set; }
            public string PrintBy { get; set; }
            public string SM1 { get; set; }
            public string CtCode { get; set; }
            public string ShippingMark { get; set; }
            public bool IsSO2UseActualItem { get; set; }
        }

        private class PLDtl
        {
            public string Cnt { get; set; }
            public string ItCode { get; set; }
            public string ItName { get; set; }
            public string PLNo { get; set; }
            public decimal QtyPL { get; set; }
            public decimal Qty { get; set; }
            public decimal Qty2 { get; set; }
            public decimal QtyInventory { get; set; }
            public decimal NW { get; set; }
            public decimal GW { get; set; }
            public string Username { get; set; }
            public decimal SectionNo { get; set; }
            public string Remark { get; set; }
            public decimal DNo { get; set; }
        }

        private class PLDtl2
        {
            public string Jumlah { get; set; }
            public string Size { get; set; }
        }

        private class PLDtl3
        {
            public decimal NW { get; set; }
            public decimal GW { get; set; }
            public string NGUOM { get; set; }
        }

        private class PLDtl4
        {
            public string Cnt { get; set; }
            public string Seal { get; set; }
            public string ItCode { get; set; }
            public string ItName { get; set; }
            public string PLNo { get; set; }
            public decimal QtyPL { get; set; }
            public decimal Qty { get; set; }
            public decimal Qty2 { get; set; }
            public decimal QtyInventory { get; set; }
            public string SectionNo { get; set; }
            public string Remark { get; set; }
            public string DNo { get; set; }
            public int Test { get; set; }
            public string PackagingUnit { get; set; }
            public string SM { get; set; }
            public string ItName2 { get; set; }

        }

        private class PLHdr2
        {
            public string CompanyLogo { set; get; }

            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyAddressCity { get; set; }
            public string CompanyPhone { get; set; }
            public string CompanyFax { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string LocalDocNo { get; set; }
            public string Account { get; set; }
            public string InvoiceNo { get; set; }
            public string SalesContractNo { get; set; }
            public string StfDt { get; set; }
            public string PortLoading { get; set; }
            public string PortDischarge { get; set; }
            public string SPPlaceDelivery { get; set; }
            public string SpName { get; set; }
            public string SoLocal { get; set; }
            public string PrintBy { get; set; }
            public string Note { get; set; }
            public string SILocal { get; set; }
            public string SpBLNo { get; set; }
            public string MotherVessel { get; set; }
            public string Seal { get; set; }
            public string Cnt { get; set; }
            public string CtPONo { get; set; }

        }

        private class PLDtl5
        {
            public int nomor { get; set; }
            public string DocNo { get; set; }
            public string ItCode { get; set; }
            public string ItName { get; set; }
            public string ItCodeInternal { get; set; }
            public string ItGrpName { get; set; }
            public string SPHSCOde { get; set; }
            public decimal UPrice { get; set; }
            public decimal Qty { get; set; }
            public decimal Amount { get; set; }
            public string PriceUomCode { get; set; }
            public decimal QtyInventory { get; set; }
            public string InventoryUomCode { get; set; }
            public decimal NW { get; set; }
            public decimal GW { get; set; }
            public string CtItCode { get; set; }
            public string CurCode { get; set; }
            public string Material { get; set; }
            public string Colour { get; set; }
            public decimal TotalVolume { get; set; }
            public decimal QtyPackagingUnit { get; set; }
            public decimal QtyPL { get; set; }
            public decimal QtyPcs { get; set; }
        }

        private class PLDtl6
        {
            public string DocNo { get; set; }
            public decimal Qty { get; set; }
            public decimal Amount { get; set; }
            public decimal NW { get; set; }
            public decimal GW { get; set; }
        }

        #endregion 
       
    }
}
