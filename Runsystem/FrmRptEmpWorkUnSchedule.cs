﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptEmpWorkUnSchedule : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = "", mAccessInd = "", mSQL = "";

        #endregion

        #region Constructor

        public FrmRptEmpWorkUnSchedule(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -7);
                Sl.SetLueDeptCode(ref LueDeptCode);
                Sl.SetLueSiteCode2(ref LueSiteCode, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select * From ( ");
	        SQL.AppendLine("Select A.EmpCode, A.Empname, A.EmpCodeOld, A.DeptCode, B.DeptName, D.PosName, ResignDt, ");
	        SQL.AppendLine("A.SiteCode, C.SiteName ");
	        SQL.AppendLine("from tblemployee A ");
	        SQL.AppendLine("Inner Join TblDepartment B On A.DeptCode = B.DeptCode ");
	        SQL.AppendLine("Left Join TblSite C On A.SiteCode = C.SiteCode ");
	        SQL.AppendLine("Left Join TblPosition D On A.PosCode = D.PosCode ");
	        SQL.AppendLine("Where A.ResignDt is null Or (A.ResignDt Is Not Null And A.ResignDt>=@DocDt2) ");
            SQL.AppendLine(")T ");
            SQL.AppendLine("Where T.EmpCode not in ");
            SQL.AppendLine("( ");
            SQL.AppendLine("	Select EmpCode From TblEmpWorkSchedule ");
            SQL.AppendLine("	Where Dt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine(") ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 10;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Employee"+Environment.NewLine+"Code", 
                        "",
                        "Employee"+Environment.NewLine+"Name",
                        "Old Code", 
                        "Department",
                        
                        //6-9
                        "Position",
                        "Resign"+Environment.NewLine+"Date",
                        "Site Code",
                        "Site Name",
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        100, 40, 200, 120, 200, 
                        
                        //6-9
                        150, 100, 100, 170
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 7, 8 });
            Sm.GrdColButton(Grd1, new int[] { 2 });
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 4, 8 }, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1,4 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            if (
            Sm.IsDteEmpty(DteDocDt1, "Start date") ||
            Sm.IsDteEmpty(DteDocDt2, "End date") ||
            Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
            ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = "And 0=0";

                var cm = new MySqlCommand();


                Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "T.EmpCode", "T.EmpCodeOld", "T.EmpName" });
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "T.DeptCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "T.SiteCode", true);
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL +
                        Filter + " Order By T.EmpName",
                        new string[]
                        {
                            //0
                            "EmpCode", 
                            //1-5
                            "EmpName","EmpCodeOld","DeptName","PosName","ResignDt",
                            //6-7
                            "SiteCode", "SiteName"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Nethod

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            Grd1.Rows.AutoHeight();
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Event

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue2(Sl.SetLueSiteCode), string.Empty);
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }

        #endregion

    }
}
