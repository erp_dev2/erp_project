﻿#region Update
/*
    19/10/2022 [MAU/PRODUCT] tambah dialog rujukan Voucher
 */
#endregion
#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmSalesReturnInvoiceDlg5 : RunSystem.FrmBase4
    {
        #region Field

        //private Frm mFrmParent;
        private FrmSalesReturnInvoice mFrmParent;
        string mSQL = string.Empty;
        private string mMenuCode = string.Empty;

        #endregion

        #region Constructor

        public FrmSalesReturnInvoiceDlg5(FrmSalesReturnInvoice FrmParent) //(FrmTemplate FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Form Method
        override protected void FrmLoad(object sender, EventArgs e)
        {
            base.FrmLoad(sender, e);
            this.Text = "List Of Voucher";
            Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
            BtnChoose.Visible = false;
            SetGrd();
            SetSQL();
            ShowData();
        }
        #endregion
        #region Standard method

        //SetGrd
        private void SetGrd()
        {
            Grd1.Cols.Count = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[]
                    {
                        //0
                        "No",

                        //1-2
                        "Voucher#", ""

                    }, new int[]
                    {
                        50, 300, 20
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 2 });
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1 });
            Sm.GrdColInvisible(Grd1, new int[] { }, false);
            Sm.SetGrdProperty(Grd1, true);

        }
        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine(" SELECT A.DocNo FROM tblvoucherhdr A ");
            SQL.AppendLine(" WHERE FIND_IN_SET(A.DocNo, @DocNo) ");


            mSQL = SQL.ToString();
        }
        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtVCDocNo.Text, new string[] { "A.DocNo" });
                Sm.CmParam(ref cm, "@DocNo", mFrmParent.TxtVoucher.Text);




                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, mSQL + Filter  ,
                        new string[]
                        { 
                            //0
                            "DocNo",


                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd1.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);


                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }
        #endregion

        #region Grid Method
        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmVoucher(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }
        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmVoucher(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }
        #endregion

        #region Event



        #region Button Event

        #endregion

        #region Misc Control Event
        private void TxtVoucher_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkVoucher_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Voucher Doc No");
        }
        #endregion



        #endregion


    }
}
