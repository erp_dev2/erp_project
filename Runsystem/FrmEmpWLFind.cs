﻿#region Update
/*
    11/07/2018 [HAR] filter employee old code
    21/10/2020 [ICA/PHT] Menambah fitur upload file
    22/10/2020 [ICA/PHT] Menambah Fitur download file di find
    06/11/2020 [TKG/PHT] tambah point
    09/02/2022 [TKG/PHT] tambah ReferenceNo+EmpWLCt
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

using System.IO;
using System.Net;
using System.Threading;

#endregion

namespace RunSystem
{
    public partial class FrmEmpWLFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmEmpWL mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmEmpWLFind(FrmEmpWL FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -7);
                Sl.SetLueDeptCode(ref LueDeptCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelInd, ");
            SQL.AppendLine("A.EmpCode,B.EmpCodeOld, B.EmpName, C.PosName, D.DeptName, F.SiteName, E.WLName, A.StartDt, A.EndDt, A.Reason1, E.Point, ");
            SQL.AppendLine("A.FileName, A.ReferenceNo, G.OptDesc As EmpWLCtDesc, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("From TblEmpWL A ");
            SQL.AppendLine("Inner Join TblEmployee B ");
            SQL.AppendLine("    On A.EmpCode=B.EmpCode ");
            if (mFrmParent.mIsFilterBySite)
            {
                SQL.AppendLine("    And (B.SiteCode Is Null Or (B.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select SiteCode From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(B.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ))) ");
            }
            SQL.AppendLine("Left Join TblPosition C On B.PosCode=C.PosCode ");
            SQL.AppendLine("Left Join TblDepartment D On B.DeptCode=D.DeptCode ");
            SQL.AppendLine("Inner Join TblWarningLetter E On A.WLCode=E.WLCode ");
            SQL.AppendLine("Left Join TblSite F On B.SiteCode=F.SiteCode ");
            SQL.AppendLine("Left Join TblOption G On A.EmpWLCt=G.OptCode And G.OptCat='EmpWLCt' ");
            SQL.AppendLine("Where A.DocDt Between @DocDt1 And @DocDt2 ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 25;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#", 
                        "Date",
                        "Cancel",
                        "Employee's Code",
                        "Old Code",
                        
                        //6-10
                        "Employee's Name",
                        "Position",
                        "Department",
                        "Site",
                        "Warning Letter",
                        
                        //11-15
                        "Start",
                        "End",
                        "Reference#",
                        "Category",
                        "Reason",

                        //16-20
                        "Point",
                        "File Name", 
                        "",
                        "Created By",
                        "Created Date",
                        
                        //21-24
                        "Created Time", 
                        "Last Updated By",
                        "Last Updated Date", 
                        "Last Updated Time"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        200, 80, 60, 130, 100,  
                        
                        //6-10
                        250, 150, 150, 150, 200,    
                        
                        //11-15
                        80, 80, 180, 120, 200, 
                        
                        //16-20
                        120, 200, 20, 130, 130, 
                        
                        //21-24
                        130, 130, 130, 130
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 16 }, 0);
            Sm.GrdColCheck(Grd1, new int[] { 3 });
            Sm.GrdFormatDate(Grd1, new int[] { 2, 11, 12, 20, 23 });
            Sm.GrdFormatTime(Grd1, new int[] { 21, 24 });
            Sm.GrdColInvisible(Grd1, new int[] { 4, 5, 7, 19, 20, 21, 22, 23, 24 }, false);
            Sm.GrdColButton(Grd1, new int[] { 18 });
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 19, 20, 21, 22, 23, 24 });
            if (!mFrmParent.mIsEmpWLAllowToUploadFile) Sm.GrdColInvisible(Grd1, new int[] { 17, 18 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4, 5, 7, 19, 20, 21, 22, 23, 24 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "B.DeptCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "A.EmpCode", "B.EmpName", "B.EmpCodeOld" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.CreateDt Desc;",
                        new string[]
                        {
                            //0
                            "DocNo",

                            //1-5
                            "DocDt", "CancelInd", "EmpCode", "EmpCodeOld", "EmpName",  

                            //6-10
                            "PosName", "DeptName", "SiteName", "WLName", "StartDt",  
                            
                            //11-15
                            "EndDt", "ReferenceNo", "EmpWLCtDesc", "Reason1", "Point", 
                            
                            //16-20
                            "FileName", "CreateBy", "CreateDt", "LastUpBy", "LastUpDt", 
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 15);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 16);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 17);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 20, 18);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 21, 18);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 19);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 23, 20);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 24, 20);
                        }, true, false, false, false
                    );
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 14 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 18 && Sm.GetGrdStr(Grd1, e.RowIndex, 17).Length != 0)
            {
                mFrmParent.DownloadFile(mFrmParent.mHostAddrForFTPClient, mFrmParent.mPortForFTPClient, Sm.GetGrdStr(Grd1, e.RowIndex, 17), mFrmParent.mUsernameForFTPClient, mFrmParent.mPasswordForFTPClient, mFrmParent.mSharedFolderForFTPClient);
                SFD.FileName = Sm.GetGrdStr(Grd1, e.RowIndex, 17);
                SFD.DefaultExt = "pdf";
                SFD.AddExtension = true;

                if (mFrmParent.downloadedData != null && mFrmParent.downloadedData.Length != 0)
                {

                    if (SFD.ShowDialog() == DialogResult.OK)
                    {
                        Application.DoEvents();

                        //Write the bytes to a file
                        FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                        newFile.Write(mFrmParent.downloadedData, 0, mFrmParent.downloadedData.Length);
                        newFile.Close();
                        MessageBox.Show("Saved Successfully");
                    }
                }
                else
                    MessageBox.Show("No File was Downloaded Yet!");
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }

        #endregion

        #endregion
    }
}
