﻿#region Update
/*
    28/10/2022 [SET/PRODUCT] New application
    07/11/2022 [IBL/PRODUCT] Tambah auto vr - vc - jn
    08/11/2022 [IBL/PRODUCT] Tambah docapproval
    09/12/2022 [SET/PRODUCT] Cash Type belum muncul ketika showdata
    12/12/2022 [SET/BBT] Currency non-editable & source dari Bank Account yang dipilih
    12/12/2022 [SET/BBT] Source Cash Type berdasar level tertinggi (2)
    13/12/2022 [SET/BBT] Aprroval berdasar department
    16/12/2022 [SET/BBT] Menampilkan Local Document
    19/12/2022 [MAU/BBT] Doc Type VR dan VC auto create dari Menu Petty Cash Disbursement menjadi "Petty Cash Disbursement"
    02/02/2023 [TYO/MNET] menambah tab dropping request berdasarkan param IsPCDUseDroppingRequest
    12/02/2023 [MAU/MNET] penyesuaian source Cost Center pada jurnal PCD
    17/02/2023 [SET/BBT] event btn_click copy data & show FileName
    20/02/2023 [DITA/MNET] harusnya if dropping req dipilih, validasi dropping request amt = pcd amt tapi kalo dropping req ga dipilih, gada validasi itu
    21/02/2023 [MAU/MNET] penyesuaian sumber CC Journal PCD
    23/02/2023 [RDA/MNET] penyesuaian ac no debit petty cash disbursement (menggunakan param IsPCDUseDroppingRequest)
    27/02/2023 [RDA/MNET] fix if null untuk acno ketika project code kosong 
    02/03/2023 [WED/MNET] bug fix nama frm ketika di show di applikasi masih 'Cash Advance Settlement'
    04/04/2023 [MAU/BBT] hanya bisa save item yang memiliki cost category , clear grid ketika mengganti cost center saat insert

*/

#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;
using System.Reflection;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using System.IO;
using System.Net;
using System.Threading;

#endregion

namespace RunSystem
{
    public partial class FrmPettyCashDisbursement : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string
            mMenuCode = string.Empty, mAccessInd = string.Empty,
            mDocNo = string.Empty,
            mPIC = string.Empty,
            mDocTitle = string.Empty,
            mDeptCode = string.Empty,
            mVoucherDocTypeCASBA = string.Empty,
            mBankAccountTypeForPCD = string.Empty,
            mCASDlgFilterPICDataSource = string.Empty,
            mVoucherCASJournalAcNoSource = string.Empty,
            mCASApprovalGroupValidation = string.Empty
            ;
        private bool
            mIsAutoJournalActived = false,
            IsInsert = false,
            mIsVoucherBankAccountFilteredByGrp = false,
            mIsFilterByCC = false,
            mIsBankAccountUseCostCenterAndInterOffice = false,
            mIsCostCategoryCASMandatory = false,
            mIsCASUsePartialAmount = false,
            mIsPCDAllowMinusAmount = false,
            mIsClosingJournalBasedOnMultiProfitCenter = false,
            mIsTransactionUseDetailApprovalInformation = false,
            mIsPCDAllowToCopyData = false,
            mIsRptCashBookUseEndDt = false,
            mIsFilterByBankAccount = false,
            mIsPCDUseDroppingRequest = false
            ;
        internal bool
            mIsSystemUseCostCenter = false,
            mIsCASUseDraftDocument = false,
            mIsPCDAllowToProcessZeroAmount = false,
            mIsCASFromTRProcessAllAmount = false,
            mIsCostCategoryFilteredInCashAdvanceSettlement = false,
            mIsVRBudgetUseCASBA = false,
            mIsVoucherCASBA = false,
            mIsCASShowCOACostCategory = false,
            mIsCheckCOAJournalNotExists = false,
            mIsCASUseItemRate = false,
            mIsPCDUseCostCenter = false,
            mIsCASCCFilterMandatory = false,
            mIsVRForBudgetUseSOContract = false,
            mIsVRUseSOContract = false,
            mIsCASUseItemRateNotBasedOnCBP = false,
            mIsCASUseCompletedInd = false,
            mIsFilterByDept = false,
            mIsCASUseBudgetCategory = false
            ;
        private string
            mBankAccountFormat = string.Empty,
            mMainCurCode = string.Empty,
            mVoucherCodeFormatType = string.Empty,
            mCashAdvanceJournalDebitFormat = string.Empty,
            mCASJournalFormula = string.Empty,
            mCASNotEqualToCostFormula = string.Empty,
            mDocNoFormat = string.Empty,
            mVoucherPCDCCJournalFormat = string.Empty
            ;
        iGCell fCell;
        bool fAccept;

        private string
            mPortForFTPClient = string.Empty,
            mHostAddrForFTPClient = string.Empty,
            mSharedFolderForFTPClient = string.Empty,
            mUsernameForFTPClient = string.Empty,
            mPasswordForFTPClient = string.Empty,
            mFileSizeMaxUploadFTPClient = string.Empty,
            mFormatFTPClient = string.Empty
            ;

        private byte[] downloadedData;

        internal FrmPettyCashDisbursementFind FrmFind;

        #endregion

        #region Constructor

        public FrmPettyCashDisbursement(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Petty Cash Disbursement";

                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                //Tc1.SelectedTabPage = Tp2;
                SetLuePICCode(ref LuePICCode, string.Empty);
                SetLueCashTypeGrpCode(ref LueCashTypeGrp);
                //SetLueCashTypeCode(ref LueCashType, string.Empty, string.Empty);
                Sl.SetLueCurCode(ref LueCurCode2);
                LueCurCode2.Visible = false;
                Sl.SetLueDeptCode(ref LueDeptCode, string.Empty);
                Sl.SetLueCurCode(ref LueCurCode1);
                //LueTypeCode.Visible = false;
                Tc1.SelectedTabPage = Tp1;
                if (!mIsPCDUseCostCenter)
                {
                    LblCCCode.Visible = LueCCCode.Visible = false;
                }

                if (mIsPCDUseDroppingRequest)
                    TpgDroppingRequest.PageVisible = true;
                //ChkCompletedInd.Visible = mIsCASUseCompletedInd;
                SetGrd();


                SetFormControl(mState.View);
                //if (!mIsCASUseDraftDocument)
                //{
                //    LblDocStatus.Visible = false;//hide
                //    LueDocStatus.Visible = false;
                //}
                //else
                //    LblDocStatus.ForeColor = Color.Red;

                if (!mIsPCDAllowToCopyData)
                {
                    TxtCopyData.Visible = false;
                    LblCopyData.Visible = false;
                    BtnCopyData.Visible = false;
                }

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = false;
                    BtnPrint.Visible = true;
                }
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grid 1

            Grd1.Cols.Count = 11;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                Grd1,
                new string[]
                {
                    //0
                    "",

                    //1-5
                    "Item Code",
                    "Item Name",
                    "UoM",
                    "Quantity",
                    "Unit Price",

                    //6-10
                    "Currency",
                    "Amount Cost",
                    "Remark",
                    "Cost Category Code",
                    "Cost Category",
                },
                new int[] { 
                    //0
                    20, 

                    //1-5
                    130, 100, 100, 200, 100,
                    
                    //6-10
                    130, 130, 100, 100, 100,
                }
            );
            Sm.GrdColButton(Grd1, new int[] { 0 });
            Sm.GrdFormatDec(Grd1, new int[] { 4, 5, 7 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 1, 9 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 2, 3, 7, 9, 10 });
            //if (!mIsCASUseItemRateNotBasedOnCBP) Sm.GrdColReadOnly(true, true, Grd1, new int[] { 28 });
            //if (mIsCASUseItemRate)
            //{
            //    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 6 });
            //    Grd1.Cols[28].Move(5);
            //    Grd1.Cols[27].Move(5);
            //    Grd1.Cols[26].Move(5);
            //    Grd1.Cols[25].Move(5);
            //    Grd1.Cols[24].Move(5);
            //    Grd1.Cols[23].Move(5);
            //}
            //else
            //{
            //    Sm.GrdColInvisible(Grd1, new int[] { 23, 26, 27, 28 });
            //}

            //Grd1.Cols[13].Move(3);
            //Grd1.Cols[18].Move(4);

            //if (!mIsVRForBudgetUseSOContract && !mIsVRUseSOContract)
            //    Sm.GrdColInvisible(Grd1, new int[] { 29 });
            //else
            //{
            //    Grd1.Cols[29].Move(3);
            //    Grd1.Cols[13].Move(3);
            //}

            //Grd1.Cols[22].Move(7);
            //Grd1.Cols[21].Move(7);
            //if(!mIsCASShowCOACostCategory)
            //    Sm.GrdColInvisible(Grd1, new int[] { 21, 22 }, false);

            //if(!mIsCASUseBudgetCategory)
            //    Sm.GrdColInvisible(Grd1, new int[] { 30, 31, 32, 33 });
            //else
            //{
            //    Grd1.Cols[30].Move(5);
            //    Grd1.Cols[32].Move(6);
            //    Sm.GrdColInvisible(Grd1, new int[] { 18, 31 });
            //    Grd1.Cols[33].Move(11);
            //}

            #endregion

            #region Grid 2

            /*Grd2.Cols.Count = 5;
            Sm.GrdHdrWithColWidth(
                Grd2,
                new string[] 
                {
                    //0
                    "Cost Category Code",

                    //1-4
                    "Cost Category",
                    "Amount (Cost)",
                    "COA#",
                    "COA"
                },
                new int[] { 
                    0, 
                    200, 130, 100, 200
                }
            );
            Sm.GrdFormatDec(Grd2, new int[] { 2 }, 0);
            Sm.GrdColInvisible(Grd2, new int[] { 0 }, false);
            Sm.GrdColReadOnly(true, true, Grd2, new int[] { 0, 1, 2, 3, 4 });
            Grd2.Cols[4].Move(2);
            Grd2.Cols[3].Move(2);
            if (!mIsCASShowCOACostCategory)
                Sm.GrdColInvisible(Grd2, new int[] { 3, 4 });*/

            #endregion

            #region Grid 3

            /*Grd3.Cols.Count = 5;
            Sm.GrdHdrWithColWidth(
                Grd3,
                new string[] 
                {
                    //0
                    "Voucher#",

                    //1-4
                    "",
                    "Currency",
                    "Amount"+Environment.NewLine+"(Cost)",
                    "Amount"+Environment.NewLine+"(Cash Advance)"
                },
                new int[] { 
                    130, 
                    20, 80, 130, 130
                }
            );
            Sm.GrdColButton(Grd3, new int[] { 1 });
            Sm.GrdFormatDec(Grd3, new int[] { 3, 4 }, 0);
            Sm.GrdColReadOnly(true, true, Grd3, new int[] { 0, 2, 3, 4 });*/

            #endregion

            #region Grid 4

            /*Grd4.Cols.Count = 5;
            Sm.GrdHdrWithColWidth(
                Grd4,
                new string[] { "DNo", "Description", "Amount", "Type Code", "Type" },
                new int[] { 0, 300, 130, 100, 100 }
            );
            Sm.GrdFormatDec(Grd4, new int[] { 2 }, 0);
            Sm.GrdColReadOnly(true, true, Grd4, new int[] { 0, 3 });
            Sm.GrdColInvisible(Grd4, new int[] { 3, 4 }, false);

            if (mIsCASUseBudgetCategory)
            {
                Grd4.Cols[4].Move(2);
                Sm.GrdColInvisible(Grd4, new int[] { 4 }, true);
            }

            #endregion

            #region Grid 5

            Grd5.Cols.Count = 6;
            Sm.GrdHdrWithColWidth(
                    Grd5,
                    new string[] 
                    {
                        //0
                        "No",

                        //1-4
                        "User", 
                        "Position",
                        "Status",
                        "Date",
                        "Remark"
                    },
                    new int[] { 50, 150, 150, 100, 80, 200 }
                );
            Sm.GrdFormatDate(Grd5, new int[] { 4 });*/

            #endregion

            #region Grid 5
            Grd5.Cols.Count = 6;
            Sm.GrdHdrWithColWidth(
                    Grd5,
                    new string[]
                    {
                        //0
                        "No",

                        //1-4
                        "User",
                        "Position",
                        "Status",
                        "Date",
                        "Remark"
                    },
                    new int[] { 50, 150, 150, 100, 80, 200 }
                );
            Sm.GrdFormatDate(Grd5, new int[] { 4 });
            #endregion
        }

        private void HideInfoInGrd()
        {
            if (mIsCASUseItemRate) Sm.GrdColInvisible(Grd1, new int[] { }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                        DteDocDt, MeeCancelReason, ChkCancelInd, LueBankAcCode, TxtVoucherDocNo, MeeRemark, LueCCCode, TxtLocalDocument, LueCashTypeGrp, LueCashType, LueDeptCode, LuePICCode, LueCurCode1,
                        TxtFile, ChkFile, TxtDRQDocNo, TxtDepartment, TxtYear, TxtMonth, TxtPRJIDocNo, TxtBudgetCategory, TxtDRQAmount, TxtPCDTotalAmount, TxtBalance, MeeRemarkDRQ }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 4, 5, 6, 7, 8 });
                    TxtDocNo.Focus();
                    BtnVoucher.Enabled = true;
                    LblCopyData.Visible = false;
                    BtnCopyData.Visible = false;
                    TxtCopyData.Visible = false;
                    BtnDRQDocNo.Visible = false;
                    if(TxtDRQDocNo.Text.Length > 0)
                        BtnDRQ.Visible = true;
                    else
                        BtnDRQ.Visible = false;
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                        DteDocDt, LueBankAcCode, MeeRemark, TxtLocalDocument, LueCashTypeGrp, LueCashType, LueCCCode, LueCurCode2, LuePICCode, LueDeptCode, ChkFile
                    }, false);
                    Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 4, 5, 7 });
                    if (mIsPCDUseCostCenter)
                    {
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                        {
                            LueCCCode
                        }, false);
                    }
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 4, 5, 6, 8 });
                    SetLueCCCode(ref LueCCCode);
                    DteDocDt.Focus();
                    BtnVoucher.Enabled = false;
                    LblCopyData.Visible = true;
                    BtnCopyData.Visible = true;
                    TxtCopyData.Visible = true;
                    BtnDRQDocNo.Visible = true;
                    BtnDRQ.Visible = true;
                    break;
                case mState.Edit:
                    //if (mIsCASUseDraftDocument && Sm.GetLue(LueDocStatus) == "D")
                    //{
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeCancelReason, ChkCancelInd }, false);
                        MeeCancelReason.Focus();
                        Sm.GrdColReadOnly(false, true, Grd1, new int[] { });
                        //if (!mIsCASUseItemRate) Sm.GrdColReadOnly(false, true, Grd1, new int[] { 6 });
                        //else
                        //{
                        //    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 23, 27 });
                        //    if (mIsCASUseItemRateNotBasedOnCBP)
                        //        Sm.GrdColReadOnly(false, true, Grd1, new int[] { 28 });
                        //}
                        //Sm.GrdColReadOnly(false, true, Grd4, new int[] { 0, 1, 2 });
                        //if (mIsCASUseCompletedInd) ChkCompletedInd.Properties.ReadOnly = false;
                    //}
                    //else
                    //{
                    //    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeCancelReason, ChkCancelInd }, false);
                    //    MeeCancelReason.Focus();
                    //    Sm.GrdColReadOnly(true, true, Grd1, new int[] { });
                    //    LblCopyData.Visible = true;
                    //    BtnCopyData.Visible = true;
                    //    TxtCopyData.Visible = true;
                    //}
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            IsInsert = false;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit>
            {
                TxtDocNo, DteDocDt, TxtStatus, MeeCancelReason, LuePICCode, LueDeptCode, LueCashTypeGrp, LueCashType,
                LueCurCode1, LueBankAcCode, MeeRemark, TxtVoucherDocNo, LueCCCode, TxtLocalDocument,
                TxtCopyData, TxtFile, TxtDRQDocNo, TxtDepartment, TxtYear, TxtMonth, TxtPRJIDocNo, TxtBudgetCategory, 
                MeeRemarkDRQ
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtAmt1, TxtAmt2, TxtBalance, TxtPCDTotalAmount, TxtDRQAmount}, 0);
            ChkCancelInd.Checked = false;
            ChkFile.Checked = false;
            ClearGrd();
            if (mIsPCDUseCostCenter) SetLueCCCode(ref LueCCCode);
            Sm.FocusGrd(Grd1, 0, 0);

        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmPettyCashDisbursementFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                SetLueBankAcCode(ref LueBankAcCode, string.Empty);
                //if (mIsCASUseDraftDocument)
                //{
                //    Sl.SetLueOption(ref LueDocStatus, "CASDocumentStatus");
                //    Sm.SetLue(LueDocStatus, "D");
                //}
                //else
                //{
                //    Sl.SetLueOption(ref LueDocStatus, "CASDocumentStatus");
                //    Sm.SetLue(LueDocStatus, "F");
                //}
                IsInsert = true;
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, string.Empty, false)) return;
            if (IsDataCancelledAlready()) return;
            SetFormControl(mState.Edit);
            IsInsert = false;
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            try
            {
                if (Sm.StdMsgYN("Print", "") == DialogResult.No ||
                    Sm.IsTxtEmpty(TxtDocNo, "Document#", false)
                    ) return;

                string Doctitle = Sm.GetParameter("DocTitle");
                string[] TableName = { "CASHdr", "CASDtl", "CASJournal", "CASSignIMS", "CASSignAMKA", "CASSignSIER" };
                var l = new List<CASHdr>();
                var lDtl = new List<CASDtl>();
                var lJournal = new List<CASJournal>();
                var lSign = new List<CASSignIMS>();
                var lSignAMKA = new List<CASSignAMKA>();
                var lSignSIER = new List<CASSignSIER>();

                var myLists = new List<IList>();

                if (Doctitle == "AMKA")
                {
                    #region Header AMKA

                    var cm2 = new MySqlCommand();
                    var SQL2 = new StringBuilder();


                    SQL2.AppendLine("SELECT Distinct @CompanyLogo As CompanyLogo,(Select ParValue From TblParameter Where ParCode='ReportTitle1')As Company, ");
                    SQL2.AppendLine("A.DocNo, Date_Format(A.DocDt,'%d %M %Y') As DocDt, E.VoucherDocNo, E.VoucherDocDt, C.Amt VoucherAmt, D.UserName, F.DeptName, A.Remark, Concat(IfNull(G.ParValue, ''), A.CreateBy, '.JPG') AS EmpPict5, H.UserName AS UserNameDibuat, H.UserCode AS UserCodeDibuat ");
                    SQL2.AppendLine("FROM tblcashadvancesettlementhdr A  ");
                    SQL2.AppendLine("LEFT JOIN tblcashadvancesettlementdtl3 B ON A.DocNo = B.DocNo  ");
                    SQL2.AppendLine("INNER JOIN tblvoucherhdr C ON B.VoucherDocNo = C.DocNo  ");
                    SQL2.AppendLine("INNER JOIN tbluser D ON A.PIC = D.UserCode  ");
                    SQL2.AppendLine("LEFT JOIN (  ");
                    SQL2.AppendLine("SELECT A.DocNo, GROUP_CONCAT(C.DocNo SEPARATOR ' \n') AS VoucherDocNo, GROUP_CONCAT(Date_Format(C.DocDt,'%d %M %Y') SEPARATOR '\n') AS VoucherDocDt  ");
                    SQL2.AppendLine("FROM tblcashadvancesettlementhdr A  ");
                    SQL2.AppendLine("LEFT JOIN tblcashadvancesettlementdtl3 B ON A.DocNo = B.DocNo  ");
                    SQL2.AppendLine("INNER JOIN tblvoucherhdr C ON B.VoucherDocNo = C.DocNo  ");
                    SQL2.AppendLine("GROUP BY A.DocNo  ");
                    SQL2.AppendLine(")E ON E.DocNo = A.DocNo  ");
                    SQL2.AppendLine("INNER JOIN tbldepartment F ON A.DeptCode = F.DeptCode  ");
                    SQL2.AppendLine("LEFT JOIN TblParameter G On G.ParCode = 'ImgFileSignature' ");
                    SQL2.AppendLine("INNER JOIN tbluser H ON A.CreateBy = H.UserCode  ");
                    SQL2.AppendLine("Where A.DocNo=@DocNo ");
                    SQL2.AppendLine("GROUP BY A.DocNo;  ");

                    using (var cn = new MySqlConnection(Gv.ConnectionString))
                    {

                        cn.Open();
                        cm2.Connection = cn;
                        cm2.CommandText = SQL2.ToString();
                        Sm.CmParam<String>(ref cm2, "@DocNo", TxtDocNo.Text);
                        Sm.CmParam<String>(ref cm2, "@CompanyLogo", @Sm.CompanyLogo());


                        var dr = cm2.ExecuteReader();
                        var c = Sm.GetOrdinal(dr, new string[]
                            {
                             //0
                             "CompanyLogo",

                             //1-5
                             "DocNo",
                             "DocDt",
                             "VoucherDocNo",
                             "VoucherDocDt",
                             "UserName",

                             //6-10
                             "DeptName",
                             "Remark",
                             "VoucherAmt",
                             "UserNameDibuat",
                             "UserCodeDibuat",
                             
                             //11
                             "EmpPict5",
                            });
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                l.Add(new CASHdr()
                                {
                                    CompanyLogo = Sm.DrStr(dr, c[0]),
                                    DocNo = Sm.DrStr(dr, c[1]),
                                    DocDt = Sm.DrStr(dr, c[2]),
                                    VoucherDocNo = Sm.DrStr(dr, c[3]),
                                    VoucherDocDt = Sm.DrStr(dr, c[4]),

                                    PJ = Sm.DrStr(dr, c[5]),
                                    Department = Sm.DrStr(dr, c[6]),
                                    Remark = Sm.DrStr(dr, c[7]),
                                    VoucherAmt = Sm.DrDec(dr, c[8]),
                                    UserNameDibuat = Sm.DrStr(dr, c[9]),
                                    UserCodeDibuat = Sm.DrStr(dr, c[10]),

                                    EmpPict5 = Sm.DrStr(dr, c[11]),

                                    PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))

                                });
                            }
                        }

                        dr.Close();
                    }
                    myLists.Add(l);

                    #endregion
                }
                else if (Doctitle == "SIER")
                {
                    #region Header Sier

                    var cm2 = new MySqlCommand();
                    var SQL2 = new StringBuilder();


                    SQL2.AppendLine("SELECT Distinct @CompanyLogo As CompanyLogo,(Select ParValue From TblParameter Where ParCode='ReportTitle1')As Company, ");
                    SQL2.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
                    SQL2.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyAddressCity', ");
                    SQL2.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyPhone', ");
                    SQL2.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle5') As 'CompanyFax', ");
                    SQL2.AppendLine("A.DocNo, Date_Format(A.DocDt,'%d/%b/%Y') As DocDt, ");
                    SQL2.AppendLine("Case A.Status When 'O' Then 'Outstanding' When 'A' Then 'Approved' When 'C' Then 'Cancel' End As Status, ");
                    SQL2.AppendLine("F.UserName, A.Remark, C.DeptName,  ");
                    SQL2.AppendLine("G.DivisionName AS Division, A.LocalDocNo, B.CurCode,   ");
                    SQL2.AppendLine("CONCAT(D.BankAcNm, ' : ', D.BankAcNo, ' [', E.BankName, '] ') AS Bank, SUM(B.Amt1) AS Amt1, SUM(B.Amt2) AS Amt2,   ");
                    SQL2.AppendLine("(SUM(B.Amt2) - SUM(B.Amt1)) AS DifferenceAmt  ");
                    SQL2.AppendLine("FROM tblcashadvancesettlementhdr A  ");
                    SQL2.AppendLine("LEFT JOIN tblcashadvancesettlementdtl3 B ON A.DocNo = B.DocNo  ");
                    SQL2.AppendLine("LEFT JOIN tbldepartment C ON A.DeptCode = C.DeptCode  ");
                    SQL2.AppendLine("INNER JOIN tblbankaccount D ON A.BankAcCode = D.BankAcCode  ");
                    SQL2.AppendLine("LEFT JOIN tblbank E ON D.BankCode = E.BankCode  ");
                    SQL2.AppendLine("INNER JOIN tbluser F ON A.PIC = F.UserCode  ");
                    SQL2.AppendLine("LEFT JOIN tbldivision G ON C.DivisionCode = G.DivisionCode  ");
                    SQL2.AppendLine("Where A.DocNo=@DocNo ");
                    SQL2.AppendLine("GROUP BY A.DocNo;  ");

                    using (var cn = new MySqlConnection(Gv.ConnectionString))
                    {

                        cn.Open();
                        cm2.Connection = cn;
                        cm2.CommandText = SQL2.ToString();
                        Sm.CmParam<String>(ref cm2, "@DocNo", TxtDocNo.Text);
                        Sm.CmParam<String>(ref cm2, "@CompanyLogo", @Sm.CompanyLogo());


                        var dr = cm2.ExecuteReader();
                        var c = Sm.GetOrdinal(dr, new string[]
                            {
                             //0
                             "CompanyLogo",

                             //1-5
                             "DocNo",
                             "DocDt",
                             "Status",
                             "UserName",
                             "Remark",

                             //6-10
                             "DeptName",
                             "Division",
                             "LocalDocNo",
                             "Bank",
                             "Amt1",
                             
                             //11-13
                             "Amt2",
                             "DifferenceAmt",
                             "CurCode",
                            });
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                l.Add(new CASHdr()
                                {
                                    CompanyLogo = Sm.DrStr(dr, c[0]),
                                    DocNo = Sm.DrStr(dr, c[1]),
                                    DocDt = Sm.DrStr(dr, c[2]),
                                    Status = Sm.DrStr(dr, c[3]),
                                    PJ = Sm.DrStr(dr, c[4]),

                                    Remark = Sm.DrStr(dr, c[5]),
                                    Department = Sm.DrStr(dr, c[6]),
                                    Division = Sm.DrStr(dr, c[7]),
                                    LocalDocNo = Sm.DrStr(dr, c[8]),
                                    Bank = Sm.DrStr(dr, c[9]),
                                    Amt = Sm.DrDec(dr, c[10]),

                                    Amt2 = Sm.DrDec(dr, c[11]),
                                    DifferenceAmt = Sm.DrDec(dr, c[12]),
                                    CurName = Sm.DrStr(dr, c[13]),
                                    FooterImage = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + @"\FooterImage.png",

                                    PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))

                                });
                            }
                        }

                        dr.Close();
                    }
                    myLists.Add(l);

                    #endregion
                }
                else
                {
                    #region Header

                    var cm = new MySqlCommand();
                    var SQL = new StringBuilder();


                    SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo,(Select ParValue From TblParameter Where ParCode='ReportTitle1')As Company, ");
                    SQL.AppendLine("(Select A.EmpName FROM TBLEmployee A INNER JOIN  tblparameter D ON A.EmpCode=D.ParValue and D.ParCode='CASPrintOutSignName') AS Verifikator1, ");
                    SQL.AppendLine("A.DocNo, Date_Format(A.DocDt,'%d %M %Y') As DocDt, A.Remark,");
                    SQL.AppendLine("D.Description, E.VoucherDocNo VCDocNo, E.Amt2 As VCAmt, Date_Format(C.DocDt,'%d %M %Y') As VCDocDt,  ");
                    SQL.AppendLine("T2.UserName UserName1, G.UserName UserName2, H.DeptName, I.Description as Description2 ");
                    SQL.AppendLine(",C.VoucherRequestDocNo VRDocNo, T2.UserName, DATE_FORMAT(T1.DocDt,'%d %M %Y') VRDocDt, T1.Amt VRAmt ");
                    SQL.AppendLine("FROM tblcashadvancesettlementhdr A   ");
                    SQL.AppendLine("left JOIN tblvoucherrequesthdr B ON A.VoucherRequestDocNo=B.DocNo  ");
                    SQL.AppendLine("left Join tblcashadvancesettlementDtl4 D on A.DocNo=D.Docno  ");
                    SQL.AppendLine("left Join tblcashadvancesettlementDtl3 E on A.DocNo=E.Docno  ");
                    SQL.AppendLine("inner JOIN tblvoucherhdr C on E.VoucherDocNo=C.DocNo  ");
                    SQL.AppendLine("INNER JOIN tblvoucherrequesthdr T1 ON C.VoucherRequestDocNo= T1.DocNo ");
                    SQL.AppendLine("left JOIN tbluser F ON B.CreateBy= F.UserCode ");
                    SQL.AppendLine("INNER JOIN tbluser G ON A.PIC= G.UserCode ");
                    SQL.AppendLine("INNER JOIN tbluser T2 ON T1.PIC= T2.UserCode ");
                    SQL.AppendLine("INNER JOIN tbldepartment H ON A.Deptcode = H.DeptCode ");
                    SQL.AppendLine("INNER JOIN tblvoucherdtl I ON C.DocNo = I.DocNo");

                    SQL.AppendLine("Where A.DocNo=@DocNo; ");

                    using (var cn = new MySqlConnection(Gv.ConnectionString))
                    {

                        cn.Open();
                        cm.Connection = cn;
                        cm.CommandText = SQL.ToString();
                        Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                        Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());


                        var dr = cm.ExecuteReader();
                        var c = Sm.GetOrdinal(dr, new string[]
                            {
                             //0
                             "CompanyLogo",

                             //1-5
                             "DocNo",
                             "DocDt",
                             "VRDocNo",
                             "VRDocDt",
                             "VRAmt",

                             //6-10
                             "VCDocNo",
                             "VCDocDt",
                             "Description",
                             "VCAmt",
                             "Verifikator1",

                             //11-15
                             "UserName1",
                             "UserName2",
                             "DeptName",
                             "Remark",
                             "Description2"

                             //16-20
                            
                            });
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                l.Add(new CASHdr()
                                {
                                    CompanyLogo = Sm.DrStr(dr, c[0]),
                                    DocNo = Sm.DrStr(dr, c[1]),
                                    DocDt = Sm.DrStr(dr, c[2]),
                                    VoucherRequestDocNo = Sm.DrStr(dr, c[3]),
                                    VoucherRequestDocDt = Sm.DrStr(dr, c[4]),

                                    VoucherRequestAmt = Sm.DrDec(dr, c[5]),
                                    //VoucherDocNo2 = Sm.GetGrdStr(Grd3, 1, 1),
                                    VoucherDocNo = Sm.DrStr(dr, c[6]),

                                    VoucherDocDt = Sm.DrStr(dr, c[7]),
                                    TotalCost = decimal.Parse(TxtAmt1.Text),
                                    TotalCAS = decimal.Parse(TxtAmt2.Text),

                                    //TotalDetail = decimal.Parse(TxtAmt3.Text),
                                    Description = Sm.DrStr(dr, c[8]),
                                    VoucherAmt = Sm.DrDec(dr, c[9]),
                                    Terbilang = Sm.Terbilang(decimal.Parse(TxtAmt1.Text)),
                                    Verifikator1 = Sm.DrStr(dr, c[10]),

                                    PJ = Sm.DrStr(dr, c[11]),
                                    SettledBy = Sm.DrStr(dr, c[12]),
                                    Department = Sm.DrStr(dr, c[13]),
                                    Remark = Sm.DrStr(dr, c[14]),
                                    Description2 = Sm.DrStr(dr, c[15]),

                                    PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))

                                });
                            }
                        }

                        dr.Close();
                    }
                    myLists.Add(l);

                    #endregion                 
                }

                #region Detail

                var cmDtl = new MySqlCommand();
                var SQLDtl = new StringBuilder();

                if (Doctitle == "SIER")
                {
                    SQLDtl.AppendLine("SELECT A.DocNo, E.DocNo AS VoucherDocNo, F.DocNo AS VRDocNo, B.Description,   ");
                    SQLDtl.AppendLine("B.Remark, B.Amt, G.BCName AS BudgetCategory  ");
                    SQLDtl.AppendLine("FROM tblcashadvancesettlementhdr A  ");
                    SQLDtl.AppendLine("LEFT JOIN tblcashadvancesettlementdtl B ON A.DocNo = B.DocNo  ");
                    SQLDtl.AppendLine("LEFT JOIN tblcashadvancesettlementdtl3 C ON B.DocNo = C.DocNo AND B.DNo = C.DNo ");
                    //SQLDtl.AppendLine("LEFT JOIN tblcashadvancesettlementdtl4 D ON A.DocNo = D.DocNo  ");
                    SQLDtl.AppendLine("INNER JOIN tblvoucherhdr E ON B.VoucherDocNo = E.DocNo  ");
                    SQLDtl.AppendLine("INNER JOIN tblvoucherrequesthdr F ON E.VoucherRequestDocNo = F.DocNo  ");
                    SQLDtl.AppendLine("LEFT JOIN tblbudgetcategory G ON B.BCCode = G.BCCode  ");
                    SQLDtl.AppendLine("Where A.DocNo=@DocNo");
                    SQLDtl.AppendLine("GROUP BY A.DocNo, B.Dno Order By B.DNo;  ");


                    using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
                    {
                        cnDtl.Open();
                        cmDtl.Connection = cnDtl;
                        cmDtl.CommandText = SQLDtl.ToString();
                        Sm.CmParam<String>(ref cmDtl, "@DocNo", TxtDocNo.Text);

                        var drDtl = cmDtl.ExecuteReader();
                        var cDtl = Sm.GetOrdinal(drDtl, new string[]
                                {
                             //0
                             "VoucherDocNo",

                             //1-5
                             "VRDocNo",
                             "Description",
                             "Remark",
                             "Amt",
                             "BudgetCategory",


                                });
                        if (drDtl.HasRows)
                        {
                            int nomor = 0;
                            while (drDtl.Read())
                            {
                                nomor = nomor + 1;
                                lDtl.Add(new CASDtl()
                                {
                                    nomor = nomor,
                                    VoucherDocNo = Sm.DrStr(drDtl, cDtl[0]),
                                    VoucherRequestDocNo = Sm.DrStr(drDtl, cDtl[1]),
                                    Description = Sm.DrStr(drDtl, cDtl[2]),
                                    Remark = Sm.DrStr(drDtl, cDtl[3]),
                                    Amt = Sm.DrDec(drDtl, cDtl[4]),
                                    BudgetCategory = Sm.DrStr(drDtl, cDtl[5]),


                                });
                            }
                        }

                        drDtl.Close();
                    }
                    myLists.Add(lDtl);
                }
                else
                {
                    SQLDtl.AppendLine("Select A.Dno, A.VoucherDocNo, A.CCtCode, B.AcNo, F.CCName, B.CCtName,  ");
                    SQLDtl.AppendLine("A.CurCode, A.Amt, A.PIC, D.UserName, A.DeptCode, E.DeptName, A.Remark, G.OptDesc, A.FileName, ");
                    SQLDtl.AppendLine("If(C.DocType ='61', (C.Amt- H.DailyAmt) , C.Amt) As VoucherAmt, concat(C.Remark, ' : ', B.CCTName, ' (', IfNull(F.CCName, '-'), ')' ) As Description  ");
                    SQLDtl.AppendLine("From TblCashAdvanceSettlementDtl A  ");
                    SQLDtl.AppendLine("Left Join TblCostCategory B On A.CCtCode=B.CCtCode  ");
                    if (Doctitle == "AMKA")
                    {
                        SQLDtl.AppendLine("LEFT JOIN (  ");
                        SQLDtl.AppendLine("         SELECT A.DocNo, A.DocType, A.Amt, A.VoucherRequestDocNo, ");
                        SQLDtl.AppendLine("         GROUP_CONCAT(B.Description SEPARATOR '\n')AS Remark  ");
                        SQLDtl.AppendLine("         FROM tblvoucherhdr A  ");
                        SQLDtl.AppendLine("         INNER JOIN tblvoucherdtl B ON A.DocNo = B.DocNo  ");
                        SQLDtl.AppendLine("         GROUP BY A.DocNo, A.DocType, A.VoucherRequestDocNo, A.Amt ");
                        SQLDtl.AppendLine("     )C ON C.DocNo = A.VoucherDocNo  ");
                    }
                    else
                    {
                        SQLDtl.AppendLine("Left Join TblVoucherHdr C On A.VoucherDocNo=C.DocNo  ");
                    }
                    SQLDtl.AppendLine("Left Join TblUser D On A.PIC=D.UserCode  ");
                    SQLDtl.AppendLine("Left Join TblDepartment E On A.DeptCode=E.DeptCode  ");
                    SQLDtl.AppendLine("Left Join TblCostCenter F On B.CCCode=F.CCCode  ");
                    SQLDtl.AppendLine("Left Join TblOption G On C.DocType=G.OptCode And G.OptCat='VoucherDocType'  ");
                    SQLDtl.AppendLine("Left Join (  ");
                    SQLDtl.AppendLine("        Select A.VoucherRequestDocNo As DocNo, SUM(B.Amt2) As DailyAmt   ");
                    SQLDtl.AppendLine("        From TblTravelRequestDtl8 A   ");
                    SQLDtl.AppendLine("        Inner Join TblTravelRequestDtl7 B On A.DocNo = B.DocNo  And A.EmpCode = B.PICCode  ");
                    SQLDtl.AppendLine("        Inner Join TblTravelRequestHdr C On A.DocNo = C.DocNo And CancelInd = 'N'  ");
                    SQLDtl.AppendLine("        Group By A.VoucherRequestDocNo  ");
                    SQLDtl.AppendLine("     )H On H.DocNo = C.VoucherRequestDocNo  ");

                    SQLDtl.AppendLine(" Where A.DocNo=@DocNo Order By A.DNo; ");



                    using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
                    {
                        cnDtl.Open();
                        cmDtl.Connection = cnDtl;
                        cmDtl.CommandText = SQLDtl.ToString();
                        Sm.CmParam<String>(ref cmDtl, "@DocNo", TxtDocNo.Text);

                        var drDtl = cmDtl.ExecuteReader();
                        var cDtl = Sm.GetOrdinal(drDtl, new string[]
                                {
                             //0
                             "CCName",

                             //1-5
                             "CCtName",
                             "AcNo",
                             "Remark",
                             "Amt",
                             "VoucherAmt",

                             //16-20
                             "Description"

                                });
                        if (drDtl.HasRows)
                        {
                            int nomor = 0;
                            while (drDtl.Read())
                            {
                                nomor = nomor + 1;
                                lDtl.Add(new CASDtl()
                                {
                                    nomor = nomor,
                                    CostCenter = Sm.DrStr(drDtl, cDtl[0]),
                                    CostCategory = Sm.DrStr(drDtl, cDtl[1]),
                                    AcNo = Sm.DrStr(drDtl, cDtl[2]),
                                    Desc = Sm.DrStr(drDtl, cDtl[3]),
                                    Amt = Sm.DrDec(drDtl, cDtl[4]),
                                    VoucherAmt = Sm.DrDec(drDtl, cDtl[5]),
                                    Description = Sm.DrStr(drDtl, cDtl[6]),


                                });
                            }
                        }

                        drDtl.Close();
                    }
                    myLists.Add(lDtl);
                }
                #endregion

                #region CASJournal

                var cmJournal = new MySqlCommand();
                var SQLJournal = new StringBuilder();

                SQLJournal.AppendLine("SELECT C.DocNo As JournalDocNo, A.DocNo As CASDocNo, D.AcNo, E.AcDesc, D.DAmt, D.CAmt ");
                SQLJournal.AppendLine("FROM tblcashadvancesettlementhdr A ");
                SQLJournal.AppendLine("INNER JOIN tblcashadvancesettlementdtl B ON A.DocNo = B.DocNo ");
                SQLJournal.AppendLine("INNER JOIN tbljournalhdr C ON A.JournalDocNo = C.DocNo ");
                SQLJournal.AppendLine("LEFT JOIN tbljournaldtl D ON C.DocNo = D.DocNo ");
                SQLJournal.AppendLine("LEFT JOIN tblcoa E ON D.AcNo = E.AcNo ");
                SQLJournal.AppendLine("Where A.DocNo=@DocNo Order By D.CAmt Asc, D.AcNo");


                using (var cnJournal = new MySqlConnection(Gv.ConnectionString))
                {
                    cnJournal.Open();
                    cmJournal.Connection = cnJournal;
                    cmJournal.CommandText = SQLJournal.ToString();
                    Sm.CmParam<String>(ref cmJournal, "@DocNo", TxtDocNo.Text);

                    var drJournal = cmJournal.ExecuteReader();
                    var cJournal = Sm.GetOrdinal(drJournal, new string[]
                            {
                             //0
                             "JournalDocNo",

                             //1-5
                             "CASDocNo",
                             "AcNo",
                             "AcDesc",
                             "DAmt",
                             "CAmt",

                            });
                    if (drJournal.HasRows)
                    {
                        int nomor = 0;
                        while (drJournal.Read())
                        {
                            lJournal.Add(new CASJournal()
                            {
                                AcNo = Sm.DrStr(drJournal, cJournal[2]),
                                AcDesc = Sm.DrStr(drJournal, cJournal[3]),
                                DAmt = Sm.DrDec(drJournal, cJournal[4]),
                                CAmt = Sm.DrDec(drJournal, cJournal[5])

                            });
                        }
                    }

                    drJournal.Close();
                }
                myLists.Add(lJournal);

                #endregion

                #region Sign IMS

                var cm3 = new MySqlCommand();

                var SQL3 = new StringBuilder();
                using (var cn3 = new MySqlConnection(Gv.ConnectionString))
                {
                    cn3.Open();
                    cm3.Connection = cn3;

                    SQL3.AppendLine(" Select Distinct  ");
                    SQL3.AppendLine("B.UserCode, C.UserCode2, D.UserCode3, B.UserName, C.UserName2, D.UserName3  ");
                    SQL3.AppendLine("From tblcashadvancesettlementhdr A  ");
                    SQL3.AppendLine("Inner Join (  ");
                    SQL3.AppendLine("    Select Distinct  ");
                    SQL3.AppendLine("    A.DocNo , B.UserCode As UserCode,  Concat(Upper(left(C.UserName,1)),Substring(Lower(C.UserName), 2, Length(C.UserName))) As UserName  ");
                    SQL3.AppendLine("    From tblcashadvancesettlementhdr A  ");
                    SQL3.AppendLine("    Inner Join TblDocApproval B On B.DocType='CashAdvanceSettlement' And A.DocNo=B.DocNo  ");
                    SQL3.AppendLine("    Inner Join TblUser C On B.UserCode=C.UserCode  ");
                    SQL3.AppendLine("    Inner Join TblDocApprovalSetting D On B.ApprovalDNo=D.DNo And D.DocType = 'CashAdvanceSettlement'  ");
                    SQL3.AppendLine("    Where D.Level = '1' And A.DocNo=@DocNo  ");
                    SQL3.AppendLine(" ) B On A.DocNo = B.DocNo  ");
                    SQL3.AppendLine("Left Join (  ");
                    SQL3.AppendLine("    Select Distinct  ");
                    SQL3.AppendLine("    A.DocNo, B.UserCode As UserCode2, Concat(Upper(left(C.UserName,1)),Substring(Lower(C.UserName), 2, Length(C.UserName))) As UserName2 ");
                    SQL3.AppendLine("    From tblcashadvancesettlementhdr A  ");
                    SQL3.AppendLine("    Inner Join TblDocApproval B On B.DocType='CashAdvanceSettlement' And A.DocNo=B.DocNo  ");
                    SQL3.AppendLine("    Inner Join TblUser C On B.UserCode=C.UserCode  ");
                    SQL3.AppendLine("    Inner Join TblDocApprovalSetting D On B.ApprovalDNo=D.DNo And D.DocType = 'CashAdvanceSettlement'  ");
                    SQL3.AppendLine("    Where D.Level = '2' And A.DocNo=@DocNo  ");
                    SQL3.AppendLine(" ) C On A.DocNo = C.DocNo  ");
                    SQL3.AppendLine("Left Join (   ");
                    SQL3.AppendLine("    Select Distinct  ");
                    SQL3.AppendLine("   A.DocNo, B.UserCode As UserCode3, Concat(Upper(left(C.UserName,1)),Substring(Lower(C.UserName), 2, Length(C.UserName)))  As UserName3  ");
                    SQL3.AppendLine("    From tblcashadvancesettlementhdr A  ");
                    SQL3.AppendLine("    Inner Join TblDocApproval B On B.DocType='CashAdvanceSettlement' And A.DocNo=B.DocNo  ");
                    SQL3.AppendLine("    Inner Join TblUser C On B.UserCode=C.UserCode  ");
                    SQL3.AppendLine("    Inner Join TblDocApprovalSetting D On B.ApprovalDNo=D.DNo And D.DocType = 'CashAdvanceSettlement'  ");
                    SQL3.AppendLine("    Where D.Level = '3' And A.DocNo=@DocNo  ");
                    SQL3.AppendLine(") D On A.DocNo = D.DocNo  ");

                    cm3.CommandText = SQL3.ToString();
                    Sm.CmParam<String>(ref cm3, "@DocNo", TxtDocNo.Text);
                    var dr3 = cm3.ExecuteReader();
                    var c3 = Sm.GetOrdinal(dr3, new string[]
                                {
                                 //0
                                 "UserName" ,

                                 //1-2
                                 "Username2" ,
                                 "Username3" ,

                                });
                    if (dr3.HasRows)
                    {
                        while (dr3.Read())
                        {

                            lSign.Add(new CASSignIMS()
                            {
                                UserName = Sm.DrStr(dr3, c3[0]),
                                UserName2 = Sm.DrStr(dr3, c3[1]),
                                UserName3 = Sm.DrStr(dr3, c3[2]),
                            });
                        }
                    }
                    dr3.Close();
                }
                myLists.Add(lSign);

                #endregion               

                #region Signature AMKA

                var cm4 = new MySqlCommand();
                var SQL4 = new StringBuilder();

                SQL4.AppendLine("SELECT T1.EmpPict1, T1.Description1, T1.UserName1, T1.ApproveDt1, T1.UserCode1, EmpPict2, T2.Description2, T2.UserName2, T2.ApproveDt2, T2.UserCode2, EmpPict3, T3.Description3, T3.Username3, T3.ApproveDt3, T3.UserCode3, EmpPict4, T4.Description4, T4.Username4, T4.ApproveDt4, T4.UserCode4 ");
                SQL4.AppendLine("FROM ");
                SQL4.AppendLine("( ");
                SQL4.AppendLine("     SELECT A.DocNo, Concat(IfNull(G.ParValue, ''), C.UserCode, '.JPG') AS EmpPict1, 'Atasan YBS' AS Description1, C.UserName AS UserName1, IFNULL(DATE_FORMAT(A.LastUpDt, '%d/%b/%Y'), '') ApproveDt1, C.UserCode AS UserCode1 ");
                SQL4.AppendLine("     FROM TblDocApproval A ");
                SQL4.AppendLine("     INNER JOIN TblDocApprovalSetting B ON A.DocType = B.DocType ");
                SQL4.AppendLine("         AND A.docno = @DocNo ");
                SQL4.AppendLine("         AND B.DNo = A.ApprovalDNo ");
                SQL4.AppendLine("         And B.Level = 1 ");
                SQL4.AppendLine("         AND A.Status = 'A' ");
                SQL4.AppendLine("     INNER JOIN TblUser C ON A.UserCode = C.UserCode ");
                SQL4.AppendLine("     LEFT JOIN TblParameter G On G.ParCode = 'ImgFileSignature' ");
                SQL4.AppendLine(") T1 ");
                SQL4.AppendLine("LEFT JOIN ");
                SQL4.AppendLine("( ");
                SQL4.AppendLine("     SELECT A.DocNo, Concat(IfNull(G.ParValue, ''), C.UserCode, '.JPG') AS EmpPict2, 'Verifikasi Oleh' AS Description2, C.UserName AS UserName2, IFNULL(DATE_FORMAT(A.LastUpDt, '%d/%b/%Y'), '') ApproveDt2, C.UserCode AS UserCode2 ");
                SQL4.AppendLine("     FROM TblDocApproval A ");
                SQL4.AppendLine("     INNER JOIN TblDocApprovalSetting B ON A.DocType = B.DocType ");
                SQL4.AppendLine("         AND A.docno = @DocNo ");
                SQL4.AppendLine("         AND B.DNo = A.ApprovalDNo ");
                SQL4.AppendLine("         And B.Level = 2 ");
                SQL4.AppendLine("         AND A.Status = 'A' ");
                SQL4.AppendLine("     INNER JOIN TblUser C ON A.UserCode = C.UserCode ");
                SQL4.AppendLine("     LEFT JOIN TblParameter G On G.ParCode = 'ImgFileSignature' ");
                SQL4.AppendLine(") T2 ON T1.DocNo = T2.DocNo ");
                SQL4.AppendLine("LEFT JOIN ");
                SQL4.AppendLine("( ");
                SQL4.AppendLine("     SELECT A.DocNo, Concat(IfNull(G.ParValue, ''), C.UserCode, '.JPG') AS EmpPict3, ");
                SQL4.AppendLine("     'Diperiksa' AS Description3, C.UserName AS UserName3, IFNULL(DATE_FORMAT(A.LastUpDt, '%d/%b/%Y'), '') ApproveDt3, C.UserCode AS UserCode3 ");
                SQL4.AppendLine("    FROM TblDocApproval A ");
                SQL4.AppendLine("    INNER JOIN TblDocApprovalSetting B ON A.DocType = B.DocType ");
                SQL4.AppendLine("        AND A.DocNo = @DocNo ");
                SQL4.AppendLine("        AND B.DNo = A.ApprovalDNo ");
                SQL4.AppendLine("        And B.Level = 3 ");
                SQL4.AppendLine("        AND A.Status = 'A' ");
                SQL4.AppendLine("    INNER JOIN TblUser C ON A.UserCode = C.UserCode ");
                SQL4.AppendLine("    LEFT JOIN TblParameter G On G.ParCode = 'ImgFileSignature' ");
                SQL4.AppendLine(")T3 ON T1.DocNo = T3.DocNo ");
                SQL4.AppendLine("LEFT JOIN ");
                SQL4.AppendLine("( ");
                SQL4.AppendLine("     SELECT A.DocNo, Concat(IfNull(G.ParValue, ''), C.UserCode, '.JPG') AS EmpPict4, ");
                SQL4.AppendLine("     'Disetujui' AS Description4, C.UserName AS UserName4, IFNULL(DATE_FORMAT(A.LastUpDt, '%d/%b/%Y'), '') ApproveDt4, C.UserCode AS UserCode4 ");
                SQL4.AppendLine("    FROM TblDocApproval A ");
                SQL4.AppendLine("    INNER JOIN TblDocApprovalSetting B ON A.DocType = B.DocType ");
                SQL4.AppendLine("        AND A.DocNo = @DocNo ");
                SQL4.AppendLine("        AND B.DNo = A.ApprovalDNo ");
                SQL4.AppendLine("        And B.Level = 4 ");
                SQL4.AppendLine("        AND A.Status = 'A' ");
                SQL4.AppendLine("    INNER JOIN TblUser C ON A.UserCode = C.UserCode ");
                SQL4.AppendLine("    LEFT JOIN TblParameter G On G.ParCode = 'ImgFileSignature' ");
                SQL4.AppendLine(")T4 ON T1.DocNo = T4.DocNo ");
                //SQL4.AppendLine("LEFT JOIN ");
                //SQL4.AppendLine("( ");
                //SQL4.AppendLine("    SELECT distinct A.DocNo, Concat(IfNull(G.ParValue, ''), D.UserCode, '.JPG') AS EmpPict5, ");
                //SQL4.AppendLine("    'DiBuat' AS Description5, D.UserName AS UserName5, IFNULL(DATE_FORMAT(A.LastUpDt, '%d/%b/%Y'), '') ApproveDt5, D.UserCode AS UserCode5 ");
                //SQL4.AppendLine("    FROM TblDocApproval A ");
                //SQL4.AppendLine("    INNER JOIN tblcashadvancesettlementhdr B ON A.DocNo = B.DocNo ");
                //SQL4.AppendLine("    INNER JOIN tbluser D ON B.PIC = D.UserCode ");
                //SQL4.AppendLine("    LEFT JOIN TblParameter G On G.ParCode = 'ImgFileSignature' ");
                //SQL4.AppendLine("    WHERE B.DocNo = @DocNo ");
                //SQL4.AppendLine(")T5 ON T1.DocNo = T5.DocNo; ");



                using (var cn4 = new MySqlConnection(Gv.ConnectionString))
                {
                    cn4.Open();
                    cm4.Connection = cn4;
                    cm4.CommandText = SQL4.ToString();
                    Sm.CmParam<String>(ref cm4, "@DocNo", TxtDocNo.Text);
                    var dr4 = cm4.ExecuteReader();
                    var c4 = Sm.GetOrdinal(dr4, new string[]
                {
                    //0

                    "EmpPict1",
                    "Description1",
                    "Username1",
                    "ApproveDt1",
                    "UserCode1",

                    "EmpPict2",
                    "Description2",
                    "Username2",
                    "ApproveDt2",
                    "UserCode2",

                    "EmpPict3",
                    "Description3",
                    "Username3",
                    "ApproveDt3",
                    "UserCode3",

                    "EmpPict4",
                    "Description4",
                    "Username4",
                    "ApproveDt4",
                    "UserCode4",
                });
                    if (dr4.HasRows)
                    {
                        while (dr4.Read())
                        {
                            lSignAMKA.Add(new CASSignAMKA()
                            {
                                EmpPict1 = Sm.DrStr(dr4, c4[0]),
                                Description1 = Sm.DrStr(dr4, c4[1]),
                                UserName1 = Sm.DrStr(dr4, c4[2]),
                                ApproveDt1 = Sm.DrStr(dr4, c4[3]),
                                UserCode1 = Sm.DrStr(dr4, c4[4]),
                                EmpPict2 = Sm.DrStr(dr4, c4[5]),
                                Description2 = Sm.DrStr(dr4, c4[6]),
                                UserName2 = Sm.DrStr(dr4, c4[7]),
                                ApproveDt2 = Sm.DrStr(dr4, c4[8]),
                                UserCode2 = Sm.DrStr(dr4, c4[9]),
                                EmpPict3 = Sm.DrStr(dr4, c4[10]),
                                Description3 = Sm.DrStr(dr4, c4[11]),
                                UserName3 = Sm.DrStr(dr4, c4[12]),
                                ApproveDt3 = Sm.DrStr(dr4, c4[13]),
                                UserCode3 = Sm.DrStr(dr4, c4[14]),
                                EmpPict4 = Sm.DrStr(dr4, c4[15]),
                                Description4 = Sm.DrStr(dr4, c4[16]),
                                UserName4 = Sm.DrStr(dr4, c4[17]),
                                ApproveDt4 = Sm.DrStr(dr4, c4[18]),
                                UserCode4 = Sm.DrStr(dr4, c4[19]),
                            });
                        }
                    }
                    dr4.Close();
                }
                myLists.Add(lSignAMKA);

                #endregion

                #region Signature SIER

                var cm5 = new MySqlCommand();
                var SQL5 = new StringBuilder();
                using (var cn5 = new MySqlConnection(Gv.ConnectionString))
                {
                    cn5.Open();
                    cm5.Connection = cn5;

                    SQL5.AppendLine("SELECT A.DocNo, Concat(IfNull(G.ParValue, ''), B.UserCode, '.JPG') AS EmpPict1, 'Proposed By,' AS Description1,  ");
                    SQL5.AppendLine("B.UserName AS UserName1, CONCAT('Date : ',DATE_FORMAT(LEFT(A.CreateDt,8), '%d/%m/%Y')) ApproveDt1, C.UserCode AS UserCode1, ");
                    SQL5.AppendLine("D.PosName AS PosName1 ");
                    SQL5.AppendLine("FROM tblcashadvancesettlementhdr A ");
                    SQL5.AppendLine("INNER JOIN tbluser B ON A.CreateBy = B.UserCode ");
                    SQL5.AppendLine("LEFT JOIN tblemployee C ON B.UserCode = C.UserCode ");
                    SQL5.AppendLine("LEFT JOIN tblposition D ON C.PosCode = D.PosCode ");
                    SQL5.AppendLine("LEFT JOIN TblParameter G On G.ParCode = 'ImgFileSignature' ");
                    SQL5.AppendLine("WHERE A.DocNo = @DocNo; ");



                    cm5.CommandText = SQL5.ToString();
                    Sm.CmParam<String>(ref cm5, "@DocNo", TxtDocNo.Text);
                    var dr5 = cm5.ExecuteReader();
                    var c5 = Sm.GetOrdinal(dr5, new string[]
                {
                    //0

                    "EmpPict1",

                    "Description1",
                    "Username1",
                    "ApproveDt1",
                    "UserCode1",
                    "PosName1",
                });
                    if (dr5.HasRows)
                    {
                        while (dr5.Read())
                        {
                            lSignSIER.Add(new CASSignSIER()
                            {
                                EmpPict1 = Sm.DrStr(dr5, c5[0]),
                                Description1 = Sm.DrStr(dr5, c5[1]),
                                UserName1 = Sm.DrStr(dr5, c5[2]),
                                ApproveDt1 = Sm.DrStr(dr5, c5[3]),
                                UserCode1 = Sm.DrStr(dr5, c5[4]),

                                PosName1 = Sm.DrStr(dr5, c5[5]),
                            });
                        }
                    }
                    dr5.Close();
                }
                myLists.Add(lSignSIER);

                #endregion


                if (Doctitle == "IMS")
                    Sm.PrintReport("CAS", myLists, TableName, false);

                else if (Doctitle == "SIER")
                    Sm.PrintReport("CASSIER", myLists, TableName, false);

                else if (Doctitle == "AMKA")
                    Sm.PrintReport("CASAMKA", myLists, TableName, false);

                else
                    Sm.PrintReport("CAS", myLists, TableName, false);

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }


        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "PettyCashDisbursement", "TblPettyCashDisbursementHdr");
            string VoucherRequestDocNo = string.Empty;
            string VoucherDocNo = string.Empty;
            var IsNeedApproval = IsDocNeedApproval();
            var cml = new List<MySqlCommand>();

            if (mVoucherCodeFormatType == "2")
            {
                VoucherRequestDocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "VoucherRequest", "TblVoucherRequestHdr");
                if (mDocNoFormat == "1")
                    VoucherDocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "Voucher", "TblVoucherHdr");
                else
                    VoucherDocNo = Sm.GenerateDocNo3(Sm.GetDte(DteDocDt), "Voucher", "TblVoucherHdr", string.Empty, "1");
            }
            else
            {
                VoucherRequestDocNo = GenerateVoucherRequestDocNo();
                if (mDocNoFormat == "1")
                {
                    VoucherDocNo = GenerateVoucherDocNo(VoucherRequestDocNo);
                }
                else
                    VoucherDocNo = Sm.GenerateDocNo3(Sm.GetDte(DteDocDt), "Voucher", "TblVoucherHdr", string.Empty, "1");
            }

            cml.Add(SavePettyCashDisbursement(DocNo, IsNeedApproval));
            cml.Add(SaveVoucherRequest(VoucherRequestDocNo, DocNo));
            cml.Add(SaveVoucher(VoucherRequestDocNo, VoucherDocNo, DocNo));
            if (mIsAutoJournalActived)
                cml.Add(SaveJournal(DocNo));

            Sm.ExecCommands(cml);

            //for (int r = 0; r < Grd1.Rows.Count; r++)
            //{
            //    if (Sm.GetGrdStr(Grd1, r, 1).Length > 0)
            //    {
            //        if (Sm.GetGrdStr(Grd1, r, 14).Length > 0 && Sm.GetGrdStr(Grd1, r, 14) != "openFileDialog1")
            //        {
            //            UploadFile(DocNo, r, Sm.GetGrdStr(Grd1, r, 14));
            //            // IsFile = true;
            //        }
            //    }
            //}
            if (TxtFile.Text.Length > 0 && TxtFile.Text != "openFileDialog1")
                UploadFile(DocNo);

            ShowData(DocNo);
        }

        private MySqlCommand SaveJournal(string DocNo)
        {
            var SQL = new StringBuilder();
            string mMenuCode = Sm.GetValue("Select MenuCode From TblMenu Where Param = 'FrmVoucher';");

            SQL.AppendLine("Update TblVoucherHdr A ");
            SQL.AppendLine("Inner Join TblPettyCashDisbursementHdr B On A.VoucherRequestDocNo = B.VoucherRequestDocNo ");
            SQL.AppendLine("Set A.JournalDocNo = @JournalDocNo ");
            SQL.AppendLine("Where B.DocNo = @DocNo ");
            SQL.AppendLine("And B.Status = 'A'; ");

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, A.DocDt, ");
            SQL.AppendLine("Concat('Voucher (Petty Cash Disbursement) : ', B.VoucherDocNo) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, (Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc ");

            if (mVoucherPCDCCJournalFormat == "2")
            {
                if (TxtPRJIDocNo.Text.Length == 0)
                    SQL.AppendLine(", A.CCCode ");
                else
                    SQL.AppendLine(", I.CCCode");
            }
            else 
                SQL.AppendLine(", A.CCCode");


            
            SQL.AppendLine(", A.Remark, A.CreateBy, A.CreateDt");

            SQL.AppendLine("From TblPettyCashDisbursementHdr A ");
            SQL.AppendLine("Inner Join TblVoucherRequestHdr B On A.VoucherRequestDocNo = B.DocNo ");
            SQL.AppendLine("	And A.DocNo = @DocNo ");
            SQL.AppendLine("	And A.Status = 'A' ");
            SQL.AppendLine("Inner Join TblVoucherHdr C On B.VoucherDocNo = C.DocNo ");
            SQL.AppendLine("Left JOIN tbldroppingrequesthdr D ON A.DroppingRequestDocNo = D.DocNo ");
            SQL.AppendLine("Left JOIN tblprojectimplementationhdr E ON D.PRJIDocNo = E.DocNo ");
            SQL.AppendLine("Left JOIN tblsocontractrevisionhdr F ON E.SOContractDocNo = F.DocNo ");
            SQL.AppendLine("Left JOIN tblsocontracthdr G ON F.SOCDocNo = G.DocNo ");
            SQL.AppendLine("Left JOIN tblboqhdr H ON G.BOQDocNo = H.DocNo ");
            SQL.AppendLine("Left JOIN tbllophdr I ON H.LOPDocNo = I.DocNo ;");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From (");
            if (mIsPCDUseDroppingRequest)
                SQL.AppendLine("        Select IFNULL(Concat(I.ParValue, H.ProjectCode2), I.ParValue) As AcNo, ");
            else
                SQL.AppendLine("        Select C.AcNo, ");
            SQL.AppendLine("        Case ");
            SQL.AppendLine("            When D.ParValue != B.CurCode Then ");
            SQL.AppendLine("            B.Qty * (B.UPrice * ( ");
            SQL.AppendLine("            IfNull(( ");
            SQL.AppendLine("                Select Amt From TblCurrencyRate ");
            SQL.AppendLine("                Where RateDt<=@DocDt And CurCode1=B.CurCode And CurCode2=D.ParValue ");
            SQL.AppendLine("                Order By RateDt Desc Limit 1 ");
            SQL.AppendLine("            ), 0.00) ");
            SQL.AppendLine("            )) ");
            SQL.AppendLine("            Else IfNull(B.Qty, 0.00) * IfNull(B.UPrice, 0.00) ");
            SQL.AppendLine("        End As DAmt, 0.00 As CAmt ");
            SQL.AppendLine("        From TblPettyCashDisbursementHdr A ");
            SQL.AppendLine("        Inner Join TblPettyCashDisbursementDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("        	And A.DocNo = @DocNo ");
            SQL.AppendLine("        	And A.Status = 'A' ");
            SQL.AppendLine("        Inner Join TblCostCategory C On B.CCtCode = C.CCtCode ");
            SQL.AppendLine("        	And C.AcNo Is Not Null ");
            SQL.AppendLine("        Inner Join TblParameter D On D.ParCode = 'MainCurCode' And D.ParValue Is Not Null ");
            if (mIsPCDUseDroppingRequest)
            {
                SQL.AppendLine("    LEFT JOIN tbldroppingrequesthdr E ON A.DroppingRequestDocNo=E.DocNo ");
                SQL.AppendLine("    LEFT JOIN tblprojectimplementationhdr F ON E.PRJIDocNo=F.DocNo ");
                SQL.AppendLine("    LEFT JOIN tblsocontractrevisionhdr G ON F.SOContractDocNo = G.DocNo ");
                SQL.AppendLine("    LEFT JOIN tblsocontracthdr H ON G.SOCDocNo = H.DocNo ");
                SQL.AppendLine("    LEFT JOIN tblparameter I ON I.ParCode = 'DefferedChargesAcNo' And I.ParValue Is Not Null ");
            }
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select B.COAAcNo As AcNo, 0.00 As DAmt, ");
            SQL.AppendLine("        IfNull(C.CAmt, 0.00) As CAmt ");
            SQL.AppendLine("        From TblPettyCashDisbursementHdr A ");
            SQL.AppendLine("        Inner Join TblBankAccount B On A.BankAcCode = B.BankAcCode ");
            SQL.AppendLine("        	And A.DocNo = @DocNo ");
            SQL.AppendLine("        	And A.Status = 'A' ");
            SQL.AppendLine("        Left Join ( ");
            SQL.AppendLine("            Select X1.DocNo, Sum( ");
            SQL.AppendLine("            Case ");
            SQL.AppendLine("                When X2.ParValue != X1.CurCode Then ");
            SQL.AppendLine("                X1.Qty * (X1.UPrice * ( ");
            SQL.AppendLine("                IfNull(( ");
            SQL.AppendLine("                    Select Amt From TblCurrencyRate ");
            SQL.AppendLine("                    Where RateDt<=@DocDt And CurCode1=X1.CurCode And CurCode2=X2.ParValue ");
            SQL.AppendLine("                    Order By RateDt Desc Limit 1 ");
            SQL.AppendLine("                ), 0.00) ");
            SQL.AppendLine("                )) ");
            SQL.AppendLine("                Else IfNull(X1.Qty, 0.00) * IfNull(X1.UPrice, 0.00) ");
            SQL.AppendLine("            End) As CAmt ");
            SQL.AppendLine("            From TblPettyCashDisbursementDtl X1 ");
            SQL.AppendLine("            Inner Join TblParameter X2 On X2.ParCode = 'MainCurCode' And X2.ParValue Is Not Null ");
            SQL.AppendLine("            Where X1.DocNo = @DocNo ");
            SQL.AppendLine("            Group By X1.DocNo ");
            SQL.AppendLine("        ) C On A.DocNo = C.DocNo ");
            SQL.AppendLine("    )Tbl ");
            SQL.AppendLine("    Where AcNo Is Not Null ");
            SQL.AppendLine("    Group By AcNo");
            SQL.AppendLine(")B On 1=1 ");
            SQL.AppendLine("Where A.DocNo = @JournalDocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(Sm.GetDte(DteDocDt), 1));

            return cm;
        }

        private string GenerateVoucherRequestDocNo()
        {
            var SQL = new StringBuilder();
            var IsDocSeqNoEnabled = Sm.GetParameterBoo("IsDocSeqNoEnabled");
            var IsVoucherDocSeqNoEnabled = Sm.GetParameterBoo("IsVoucherDocSeqNoEnabled");
            string
                Yr = Sm.GetDte(DteDocDt).Substring(2, 2),
                Mth = Sm.GetDte(DteDocDt).Substring(4, 2),
                DocTitle = Sm.GetParameter("DocTitle"),
                DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='VoucherRequest'"),
                DocSeqNo = "4";

            if (IsDocSeqNoEnabled || IsVoucherDocSeqNoEnabled) DocSeqNo = Sm.GetParameter("DocSeqNo");

            SQL.Append("Select Concat('" + Yr + "','/', '" + Mth + "', '/', (select ifnull ( ");
            SQL.Append("(Select Right(Concat(Repeat('0', " + DocSeqNo + "), Convert(DocNoTemp+1, Char)), " + DocSeqNo + ") As Numb From ( ");
            SQL.Append("Select Convert(SUBSTRING(DocNo,7," + DocSeqNo + "), Decimal) As DocNoTemp ");
            SQL.Append("From TblVoucherRequestHdr ");
            //SQL.Append("(Select Right(Concat('0000', Convert(DocNoTemp+1, Char)), 4) As Numb From ( ");
            //SQL.Append("Select Convert(SUBSTRING(DocNo,7,4), Decimal) As DocNoTemp From TblVoucherRequestHdr ");
            SQL.Append("Where Left(DocNo, 5)= Concat('" + Yr + "','/', '" + Mth + "') ");
            SQL.Append("Order By SUBSTRING(DocNo,7," + DocSeqNo + ") Desc Limit 1) As temp ");
            SQL.Append("), Right(Concat(Repeat('0', " + DocSeqNo + "), '1'), " + DocSeqNo + ") ");
            //SQL.Append("), '0001' ");
            SQL.Append(") As Number), '/', '" + DocAbbr + "' ) As DocNo ");

            return Sm.GetValue(SQL.ToString());
        }

        private string GenerateVoucherDocNo(string VRDocNo)
        {
            var SQL = new StringBuilder();
            string LocalCode = string.Empty;
            string DocDt = Sm.GetDte(DteDocDt);
            var IsDocSeqNoEnabled = Sm.GetParameterBoo("IsDocSeqNoEnabled");
            var IsVoucherDocSeqNoEnabled = Sm.GetParameterBoo("IsVoucherDocSeqNoEnabled");
            var DocType = "75";
            string
                Yr = DocDt.Substring(2, 2),
                Mth = DocDt.Substring(4, 2),
                DocTitle = Sm.GetValue("Select ParValue From TblParameter Where ParCode='DocTitle'"),
                DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='Voucher'"),
                type = string.Empty,
                DocSeqNo = "4",
                AcType = Sm.GetValue("Select C.AcType " +
                            "From TblVoucherRequestHdr C  " +
                            "Inner Join TblBankAccount D On C.BankAcCode = D.BankAcCode " +
                            "Where C.DocNo = '" + VRDocNo + "' "),
                BankAcCode = Sm.GetValue("Select C.BankAcCode " +
                            "From TblVoucherRequestHdr C  " +
                            "Inner Join TblBankAccount D On C.BankAcCode = D.BankAcCode " +
                            "Where C.DocNo = '" + VRDocNo + "' ");

            if (IsDocSeqNoEnabled || IsVoucherDocSeqNoEnabled) DocSeqNo = Sm.GetParameter("DocSeqNo");

            if (AcType == "C")
                type = Sm.GetValue("Select ifnull(AutoNoCredit, '') From TblBankAccount Where BankAcCode = '" + BankAcCode + "' ");
            else
                type = Sm.GetValue("Select ifnull(AutoNoDebit, '') From TblBankAccount Where BankAcCode = '" + BankAcCode + "' ");

            if (type == string.Empty)
            {
                SQL.Append("Select Concat('" + Yr + "','/', '" + Mth + "', '/', (select ifnull ( ");
                SQL.Append("   (Select Right(Concat(Repeat('0', " + DocSeqNo + "), Convert(DocNoTemp+1, Char)), " + DocSeqNo + ") As Numb From ( ");
                SQL.Append("    Select Convert(SUBSTRING(DocNo,7," + DocSeqNo + "), Decimal) As DocNoTemp ");
                SQL.Append("       From TblVoucherHdr ");
                SQL.Append("Where Left(DocNo, 5)= Concat('" + Yr + "','/', '" + Mth + "') ");
                SQL.Append("Order By SUBSTRING(DocNo,7," + DocSeqNo + ") Desc Limit 1) As temp ");
                SQL.Append("   ), Right(Concat(Repeat('0', " + DocSeqNo + "), '1'), " + DocSeqNo + ") ");
                SQL.Append(") As Number), '/', '" + DocAbbr + "' ) As DocNo ");

            }
            else
            {
                SQL.Append("Select Concat('" + Yr + "','/', '" + Mth + "', '/', (select ifnull ( ");
                SQL.Append("   (Select Right(Concat(Repeat('0', " + DocSeqNo + "), Convert(DocNoTemp+1, Char)), " + DocSeqNo + ") As Numb From ( ");
                SQL.Append("    Select Convert(SUBSTRING(DocNo,7," + DocSeqNo + "), Decimal) As DocNoTemp ");
                SQL.Append("    From TblVoucherHdr ");
                SQL.Append("Where Left(DocNo, 5)= Concat('" + Yr + "','/', '" + Mth + "') ");
                SQL.Append("And Right(DocNo, '" + type.Length + "') = '" + type + "' ");
                SQL.Append("Order By SUBSTRING(DocNo,7," + DocSeqNo + ") Desc Limit 1) As temp ");
                SQL.Append("   ), Right(Concat(Repeat('0', " + DocSeqNo + "), '1'), " + DocSeqNo + ") ");
                SQL.Append(") As Number), '/', '" + DocAbbr + "', '/', '" + type + "' ) As DocNo ");
            }
            return Sm.GetValue(SQL.ToString());
        }

        //private string GenerateVoucherRequestDocNo()
        //{
        //    string
        //        Yr = Sm.GetDte(DteDocDt).Substring(2, 2),
        //        Mth = Sm.GetDte(DteDocDt).Substring(4, 2),
        //        DocTitle = Sm.GetParameter("DocTitle"),
        //        DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='VoucherRequest'");

        //    var SQL = new StringBuilder();

        //    SQL.Append("Select Concat('" + Yr + "','/', '" + Mth + "', '/', (select ifnull ( ");
        //    SQL.Append("(Select Right(Concat('0000', Convert(DocNoTemp+1, Char)), 4) As Numb From ( ");
        //    SQL.Append("Select Convert(SUBSTRING(DocNo,7,4), Decimal) As DocNoTemp From TblVoucherRequestHdr ");
        //    SQL.Append("Where Left(DocNo, 5)= Concat('" + Yr + "','/', '" + Mth + "') ");
        //    SQL.Append("Order By SUBSTRING(DocNo,7,4) Desc Limit 1) As temp ");
        //    SQL.Append("), '0001') As Number), '/', '" + DocAbbr + "' ) As DocNo ");

        //    return Sm.GetValue(SQL.ToString());
        //}

        private MySqlCommand UpdateStatusApproval(string DocNo)
        {
            var SQL = new StringBuilder();
            SQL.AppendLine("Update TblCashAdvanceSettlementHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@CashAdvanceSettlementDocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select 1 From TblDocApproval ");
            SQL.AppendLine("    Where DocType='CashAdvanceSettlement' ");
            SQL.AppendLine("    And DocNo=@CashAdvanceSettlementDocNo ");
            SQL.AppendLine("); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@CashAdvanceSettlementDocNo", DocNo);


            return cm;
        }

        private MySqlCommand SaveDocApproval(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select T.DocType, @CashAdvanceSettlementDocNo, '001', T.DNo, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblDocApprovalSetting T ");
            SQL.AppendLine("Where T.DocType='CashAdvanceSettlement' And T.DeptCode=@DeptCode ");
            SQL.AppendLine("And (T.StartAmt=0 ");
            SQL.AppendLine("Or T.StartAmt<=IfNull(( ");
            SQL.AppendLine("    Select IfNull(Amt1, 0) ");
            SQL.AppendLine("    From TblCashAdvanceSettlementHdr ");
            SQL.AppendLine("    Where DocNo=@CashAdvanceSettlementDocNo ");
            SQL.AppendLine("), 0)) ");
            SQL.AppendLine("And (T.EndAmt=0 ");
            SQL.AppendLine("Or T.EndAmt>=IfNull(( ");
            SQL.AppendLine("    Select IfNull(Amt1, 0) ");
            SQL.AppendLine("    From TblCashAdvanceSettlementHdr ");
            SQL.AppendLine("    Where DocNo=@CashAdvanceSettlementDocNo ");
            SQL.AppendLine("), 0)) ");
            if (mCASApprovalGroupValidation == "1")
            {
                SQL.AppendLine("And (T.DAGCode Is Null Or ");
                SQL.AppendLine("(T.DAGCode Is Not Null ");
                SQL.AppendLine("And T.DAGCode In ( ");
                SQL.AppendLine("    Select A.DAGCode ");
                SQL.AppendLine("    From TblDocApprovalGroupHdr A, TblDocApprovalGroupDtl B ");
                SQL.AppendLine("    Where A.DAGCode=B.DAGCode ");
                SQL.AppendLine("    And A.ActInd='Y' ");
                SQL.AppendLine("    And B.EmpCode=@PIC ");
                SQL.AppendLine("))) ");
            }
            if (mCASApprovalGroupValidation == "2")
            {
                SQL.AppendLine("And (T.DAGCode Is Null Or ");
                SQL.AppendLine("(T.DAGCode Is Not Null ");
                SQL.AppendLine("And T.DAGCode In ( ");
                SQL.AppendLine("    Select A.DAGCode ");
                SQL.AppendLine("    From TblDocApprovalGroupHdr A, TblDocApprovalGroupDtl B ");
                SQL.AppendLine("    Where A.DAGCode=B.DAGCode ");
                SQL.AppendLine("    And A.ActInd='Y' ");
                SQL.AppendLine("    And B.EmpCode=@PIC ");
                SQL.AppendLine("))) ");
            }
            SQL.AppendLine(";");

            SQL.AppendLine("Update TblCashAdvanceSettlementHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@CashAdvanceSettlementDocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select 1 From TblDocApproval ");
            SQL.AppendLine("    Where DocType='CashAdvanceSettlement' ");
            SQL.AppendLine("    And DocNo=@CashAdvanceSettlementDocNo ");
            SQL.AppendLine("); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@CashAdvanceSettlementDocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@DeptCode", mDeptCode);
            Sm.CmParam<String>(ref cm, "@PIC", mPIC);


            return cm;
        }
        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LuePICCode, "PIC") ||
                Sm.IsLueEmpty(LueDeptCode, "Deparment") ||
                Sm.IsLueEmpty(LueBankAcCode, "Bank account") ||
                //Sm.IsTxtEmpty(TxtCurCode, "Currency", false) ||
                (mIsPCDUseCostCenter && Sm.IsLueEmpty(LueCCCode, "Cost Center")) ||
                Sm.IsLueEmpty(LueCashTypeGrp, "Cash Type Group") ||
                Sm.IsLueEmpty(LueCashType, "Cash Type") ||
                //(mIsClosingJournalBasedOnMultiProfitCenter ?
                //    Sm.IsClosingJournalInvalid(true, false, Sm.GetDte(DteDocDt), GetProfitCenterCode()) :
                //    Sm.IsClosingJournalInvalid(Sm.GetDte(DteDocDt))) ||
                IsGrdEmpty() ||
                //IsDocTypeNotValid() ||
                //IsTotalDetailNotValid() ||
                //(!mIsCASUsePartialAmount && IsTotalDetailNotValid2()) ||
                IsGrdValueNotValid() ||
                //IsUploadFileNotValid() ||
                IsCostCenterInvalid() ||
                IsTotalCostInvalid() ||
                (mIsPCDUseDroppingRequest && TxtDRQDocNo.Text.Length > 0 && IsTotalBalanceInvalid()) ||
                IsCostCategoryEmpty()
                
                //(Sm.IsLueEmpty(LueDocStatus, "Document Status") && mIsCASUseDraftDocument) ||
                //IsCompletedInvalid() ||
                //(mIsCheckCOAJournalNotExists && IsCOAJournalNotValid()) ||
                //IsJournalSettingInvalid()
                ;
        }

        private bool IsCostCategoryEmpty()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {                
                if(Grd1.Cells[Row, 9].Text.Length == 0)
                {
                    Sm.StdMsg(mMsgType.Warning, "Cost Category is empty");
                    return true;
                } 
            }

            return false;
        }

        private bool IsCompletedInvalid()
        {
            if (!mIsCASUseCompletedInd) return false;

            decimal Amt1 = Decimal.Parse(TxtAmt1.Text);
            decimal Amt2 = Decimal.Parse(TxtAmt2.Text);

            if (Amt1 > Amt2)
            {
                //if (!ChkCompletedInd.Checked)
                //{
                //    Sm.StdMsg(mMsgType.Warning, "You need to marked this document as completed.");
                //    ChkCompletedInd.Focus();
                //    return true;
                //}
            }

            return false;
        }

        private bool IsCostCenterInvalid()
        {
            if (!mIsPCDUseCostCenter) return false;

            var SQL = new StringBuilder();
            string CCCode = Sm.GetLue(LueCCCode);
            string CCtCode = string.Empty;
            string CCCode2 = string.Empty;

            for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
            {
                if (CCtCode.Length > 0) CCtCode += ",";
                CCtCode += Sm.GetGrdStr(Grd1, i, 9);
            }

            if (CCtCode.Length > 0)
            {
                SQL.AppendLine("Select Group_Concat(Distinct CCCode) CCCode ");
                SQL.AppendLine("From TblCostCategory ");
                SQL.AppendLine("Where Find_In_Set(CCtCode, @Param); ");

                CCCode2 = Sm.GetValue(SQL.ToString(), CCtCode);

                if (CCCode2 != CCCode)
                {
                    Sm.StdMsg(mMsgType.Warning, "Cost center is not match to listed cost category.");
                    LueCCCode.Focus();
                    return true;
                }
            }

            return false;
        }

        //private bool IsCOAJournalNotValid()
        //{
        //    var l = new List<COA>();
        //    var SQL = new StringBuilder();
        //    var cm = new MySqlCommand();
        //    string mCCtCode = string.Empty, mVoucherDocNo = string.Empty;
        //    decimal
        //       Amt1 = decimal.Parse(TxtAmt1.Text),
        //       Amt2 = decimal.Parse(TxtAmt2.Text);

        //    for (int r = 0; r < Grd1.Rows.Count; r++)
        //    {
        //        if (Sm.GetGrdStr(Grd1, r, 1).Length > 0)
        //        {
        //            if (mCCtCode.Length > 0) mCCtCode += " Or ";
        //            mCCtCode += " (CCtCode=@CCtCode00" + r.ToString() + ") ";
        //            Sm.CmParam<String>(ref cm, "@CCtCode00" + r.ToString(), Sm.GetGrdStr(Grd1, r, 3));
        //        }
        //    }

        //    if (mCCtCode.Length > 0) mCCtCode = " And (" + mCCtCode + ") ";

        //    for (int r = 0; r < Grd1.Rows.Count; r++)
        //    {
        //        if (Sm.GetGrdStr(Grd1, r, 1).Length > 0)
        //        {
        //            if (mVoucherDocNo.Length > 0) mVoucherDocNo += " Or ";
        //            mVoucherDocNo += " (A.DocNo=@VoucherDocNo00" + r.ToString() + ") ";
        //            Sm.CmParam<String>(ref cm, "@VoucherDocNo00" + r.ToString(), Sm.GetGrdStr(Grd1, r, 1));
        //        }
        //    }

        //    if (mVoucherDocNo.Length > 0) mVoucherDocNo = " And (" + mVoucherDocNo + ") ";

        //    bool IsNeedApproval = IsDocNeedApproval();

        //    if (Grd4.Rows.Count <= 1)
        //    {
        //        //if ((IsNeedApproval && !ChkCancelInd.Checked &&
        //        //    ((mIsCASUseDraftDocument && Sm.GetLue(LueDocStatus) == "F") || !mIsCASUseDraftDocument)) ||
        //        //   ((!IsNeedApproval && !ChkCancelInd.Checked &&
        //        //     (mIsAutoJournalActived && ((mIsCASUseDraftDocument && Sm.GetLue(LueDocStatus) == "F") || !mIsCASUseDraftDocument)))))
        //        if (!ChkCancelInd.Checked &&
        //            (!mIsCASUseDraftDocument || (mIsCASUseDraftDocument && Sm.GetLue(LueDocStatus) == "F")) &&
        //            (IsNeedApproval || (!IsNeedApproval && mIsAutoJournalActived))
        //            )
        //        {

        //            SQL.AppendLine("Select AcNo From ");
        //            SQL.AppendLine("( ");
        //            //Amt1=Amt2
        //            if (mIsVRBudgetUseCASBA && mIsVoucherCASBA)
        //            {
        //                SQL.AppendLine("        Select AcNo");
        //                SQL.AppendLine("        From TblCostCategory Where 1 = 1 " + mCCtCode);

        //                SQL.AppendLine("        Union All ");
        //                SQL.AppendLine("        Select B.COAAcNo As AcNo ");
        //                SQL.AppendLine("        From TblVoucherHdr A ");
        //                SQL.AppendLine("        Inner Join TblBankAccount B On A.BankAcCode = B.BankAcCode " + mVoucherDocNo);

        //                SQL.AppendLine("        Union All ");
        //                SQL.AppendLine("        Select B.COAAcNo As AcNo ");
        //                SQL.AppendLine("        From TblVoucherHdr A ");
        //                SQL.AppendLine("        Inner Join TblBankAccount B On A.BankAcCode2 = B.BankAcCode " + mVoucherDocNo);
        //            }
        //            else
        //            {
        //                if (Amt1 == Amt2)
        //                {
        //                    SQL.AppendLine("        Select AcNo ");
        //                    SQL.AppendLine("        From TblCostCategory Where 1=1 " + mCCtCode);
        //                    if (mIsVRBudgetUseCASBA && mIsVoucherCASBA)
        //                    {
        //                        SQL.AppendLine("        Union All ");
        //                        SQL.AppendLine("        Select B.COAAcNo As AcNo ");
        //                        SQL.AppendLine("        From TblVoucherHdr A ");
        //                        SQL.AppendLine("        Inner Join TblBankAccount B On A.BankAcCode = B.BankAcCode " + mVoucherDocNo);

        //                        SQL.AppendLine("        Union All ");
        //                        SQL.AppendLine("        Select B.COAAcNo As AcNo ");
        //                        SQL.AppendLine("        From TblVoucherHdr A ");
        //                        SQL.AppendLine("        Inner Join TblBankAccount B On A.BankAcCode2 = B.BankAcCode " + mVoucherDocNo);
        //                    }
        //                    else
        //                    {
        //                        if (mCashAdvanceJournalDebitFormat == "1")
        //                        {
        //                            SQL.AppendLine("        Union All ");
        //                            SQL.AppendLine("        Select ParValue As AcNo ");
        //                            SQL.AppendLine("        From TblParameter Where ParCode='CashAdvanceJournalDebitAcNo' ");
        //                        }
        //                        else
        //                        {
        //                            SQL.AppendLine("        Union All ");
        //                            SQL.AppendLine("        Select AcNo9 As AcNo ");
        //                            SQL.AppendLine("        From TblDepartment Where DeptCode = @DeptCode");
        //                        }
        //                    }
        //                }
        //            }

        //            SQL.AppendLine(")T ; ");

        //            using (var cn = new MySqlConnection(Gv.ConnectionString))
        //            {
        //                cn.Open();
        //                cm.Connection = cn;
        //                cm.CommandTimeout = 600;
        //                cm.CommandText = SQL.ToString();

        //                Sm.CmParam<String>(ref cm, "@DeptCode", mDeptCode);
        //                var dr = cm.ExecuteReader();
        //                var c = Sm.GetOrdinal(dr, new string[] { "AcNo" });
        //                if (dr.HasRows)
        //                {
        //                    while (dr.Read())
        //                    {
        //                        l.Add(new COA()
        //                        {
        //                            AcNo = Sm.DrStr(dr, c[0])

        //                        });
        //                    }
        //                }
        //                dr.Close();
        //            }


        //            foreach (var x in l.Where(w => w.AcNo.Length <= 0))
        //            {
        //                Sm.StdMsg(mMsgType.Warning, "There is/are one or more COA Account that not exists for crating journal transaction.");
        //                return true;
        //            }

        //        }
        //    } 



        //    return false;
        //}

        private bool IsJournalSettingInvalid()
        {
            if (mIsAutoJournalActived && mIsCheckCOAJournalNotExists)
            {
                var SQL = new StringBuilder();
                decimal
                Amt1 = decimal.Parse(TxtAmt1.Text),
                Amt2 = decimal.Parse(TxtAmt2.Text);
                var IsNeedApproval = IsDocNeedApproval();

                string mCashAdvanceJournalDebitAcNo = Sm.GetValue("Select ParValue From TblParameter Where Parcode='CashAdvanceJournalDebitAcNo'");
                var Msg =
                "Journal's setting is invalid." + Environment.NewLine +
                "Please contact Finance/Accounting department." + Environment.NewLine;

                if ((/*!(Grd4.Rows.Count > 1) && */!IsNeedApproval && (!mIsCASUseDraftDocument || (mIsCASUseDraftDocument/* && Sm.GetLue(LueDocStatus) == "F"*/))) || !(Amt1 != Amt2 /*&& ChkCompletedInd.Checked*/))
                    if (mCASJournalFormula == "1")
                    {
                        if (Amt1 == Amt2)
                        {
                            if (IsJournalSettingInvalid_CostCategory(Msg)) return true;
                            if (mIsBankAccountUseCostCenterAndInterOffice)
                            {
                                if (IsJournalSettingInvalid_BankAccount(Msg, "", "COAAcNoInterOffice")) return true;
                            }
                            else
                            {
                                if (mIsBankAccountUseCostCenterAndInterOffice)
                                {
                                    if (IsJournalSettingInvalid_BankAccount(Msg, "", "COAAcNoInterOffice")) return true;
                                    if (mCashAdvanceJournalDebitAcNo.Length == 0 && mVoucherCASJournalAcNoSource == "2")
                                    {
                                        Sm.StdMsg(mMsgType.Warning, Msg + "Parameter CashAdvanceJournalDebitAcNo is empty.");
                                        return true;
                                    }
                                }
                                else
                                {
                                    if (IsJournalSettingInvalid_Department(Msg, "AcNo9")) return true;
                                }
                            }
                        }
                    }
                    else
                    {
                        if (Amt1 == Amt2)
                        {
                            if (IsJournalSettingInvalid_CostCategory(Msg)) return true;
                            if (IsJournalSettingInvalid_BankAccount(Msg, "", "COAAcNo")) return true;
                        }
                        else
                        {
                            if (Amt1 < Amt2)
                            {
                                if (IsJournalSettingInvalid_BankAccount(Msg, "", "COAAcNo") || IsJournalSettingInvalid_BankAccount(Msg, "2", "COAAcNo")) return true;
                                if (IsJournalSettingInvalid_BankAccount(Msg, "", "COAAcNo")) return true;
                            }
                        }
                    }
            }
            return false;
        }

        private bool IsJournalSettingInvalid_CostCategory(string Msg)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirst = true;
            string CCtName = string.Empty, CCtCode = string.Empty;
            SQL.AppendLine("Select B.CCtName ");
            SQL.AppendLine("From TblCashAdvanceSettlementDtl A, TblCostCategory B ");
            SQL.AppendLine("Where A.CCtCode=B.CCtCode And B.AcNo Is Null ");
            SQL.AppendLine("And B.CCtCode In (");
            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                CCtCode = Sm.GetGrdStr(Grd1, r, 3);
                if (CCtCode.Length > 0)
                {
                    if (IsFirst)
                        IsFirst = false;
                    else
                        SQL.AppendLine(", ");
                    SQL.AppendLine("@CCtCode_" + r.ToString());
                    Sm.CmParam<String>(ref cm, "@CCtCode_" + r.ToString(), CCtCode);
                }
            }
            SQL.AppendLine(") Limit 1;");

            cm.CommandText = SQL.ToString();
            CCtName = Sm.GetValue(cm);
            if (CCtName.Length > 0)
            {
                Sm.StdMsg(mMsgType.Warning, Msg + "Cost category's COA account# (" + CCtName + ") is empty.");
                return true;
            }
            return false;
        }

        private bool IsJournalSettingInvalid_BankAccount(string Msg, string formula, string COA)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            decimal
                Amt1 = decimal.Parse(TxtAmt1.Text),
                Amt2 = decimal.Parse(TxtAmt2.Text);
            string BankAcCode = string.Empty, BankAcName = string.Empty;

            SQL.AppendLine("Select B.BankAcNm ");
            SQL.AppendLine("From TblCashAdvanceSettlementHdr A, TblBankAccount B ");
            SQL.AppendLine("Where A.BankAcCode" + formula + " = B.BankAcCode ");
            if (Amt1 < Amt2)
            {
                SQL.AppendLine("A.AcType" + formula + " = 'C'");
            }
            SQL.AppendLine("And B." + COA + " Is Null ");
            SQL.AppendLine("And B.BankAcCode=@BankAcCode ");
            SQL.AppendLine("Limit 1;");
            Sm.CmParam<String>(ref cm, "@BankAcCode", Sm.GetLue(LueBankAcCode));
            cm.CommandText = SQL.ToString();
            BankAcName = Sm.GetValue(cm);
            if (BankAcName.Length > 0)
            {
                Sm.StdMsg(mMsgType.Warning, Msg + "Bank Account's COA account# (" + BankAcName + ") is empty.");
                return true;
            }
            return false;
        }

        private bool IsJournalSettingInvalid_Department(string Msg, string COA)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirst = true;
            string DeptName = string.Empty, DeptCode = string.Empty;
            SQL.AppendLine("Select B.DeptName ");
            SQL.AppendLine("From TblCashAdvanceSettlementHdr A, TblDepartment B ");
            SQL.AppendLine("Where A.DeptCode = B.DeptCode And B." + COA + " Is Null ");
            SQL.AppendLine("And B.DeptCode In (");
            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                DeptCode = Sm.GetGrdStr(Grd1, r, 10);
                if (DeptCode.Length > 0)
                {
                    if (IsFirst)
                        IsFirst = false;
                    else
                        SQL.AppendLine(", ");
                    SQL.AppendLine("@DeptCode_" + r.ToString());
                    Sm.CmParam<String>(ref cm, "@DeptCode_" + r.ToString(), DeptCode);
                }
            }
            SQL.AppendLine(") Limit 1;");

            cm.CommandText = SQL.ToString();
            DeptName = Sm.GetValue(cm);
            if (DeptName.Length > 0)
            {
                Sm.StdMsg(mMsgType.Warning, Msg + "Department's COA account# (" + DeptName + ") is empty.");
                return true;
            }
            return false;
        }

        private bool IsDocTypeNotValid()
        {
            if (!mIsVRBudgetUseCASBA) return false;

            GetVCDocType();
            mIsVoucherCASBA = IsVoucherCASBA();

            string VCDocType = string.Empty;

            for (int i = 0; i < Grd1.Rows.Count; ++i)
            {
                if (Sm.GetGrdStr(Grd1, i, 1).Length > 0)
                {
                    if (VCDocType.Length == 0) VCDocType = Sm.GetGrdStr(Grd1, i, 20);

                    if (VCDocType != Sm.GetGrdStr(Grd1, i, 20))
                    {
                        Tc1.SelectedTabPage = Tp1;
                        Sm.StdMsg(mMsgType.Warning, "You could not add different type of voucher.");
                        Sm.FocusGrd(Grd1, i, 1);
                        return true;
                    }

                    VCDocType = Sm.GetGrdStr(Grd1, i, 20);
                }
            }

            return false;
        }

        private bool IsUploadFileNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 14).Length > 0 && Sm.GetGrdStr(Grd1, Row, 14) != "openFileDialog1")
                    {
                        if (IsUploadFileNotValid(Row, Sm.GetGrdStr(Grd1, Row, 14))) return true;
                    }
                }
            }

            return false;
        }

        private bool IsDocNeedApproval()
        {
            return Sm.IsDataExist("Select 1 From TblDocApprovalSetting Where DocType='PettyCashDisbursement' /*And DeptCode = @Param*/ Limit 1;", mDeptCode);

        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 record.");
                return true;
            }
            return false;
        }

        private bool IsTotalDetailNotValid() //membandingkan antara total details dengan total cash advanced jika total cost 0
        {
            var AmtCost = decimal.Parse(TxtAmt1.Text);
            var AmtCA = decimal.Parse(TxtAmt2.Text);
            var AmtDetail = 0m;

            if ((mIsPCDAllowMinusAmount ? AmtCost == 0 : AmtCost <= 0) && AmtDetail != AmtCA && mIsPCDAllowToProcessZeroAmount/* && Sm.GetLue(LueDocStatus) == "F"*/)
            {
                Sm.StdMsg(mMsgType.Warning, "Total amount cash advance different with total amount detail.");
                return true;
            }

            return false;
        }

        private bool IsTotalDetailNotValid2() //membandingkan antara total details dengan selisih total cash advanced dan total cost 
        {
            var AmtCost = decimal.Parse(TxtAmt1.Text);
            var AmtCA = decimal.Parse(TxtAmt2.Text);
            var AmtDetail = 0m;

            decimal Selisih = 0m;
            if (AmtCA > AmtCost)
                Selisih = AmtCA - AmtCost;
            else
                Selisih = AmtCost - AmtCA;

            if (Selisih != AmtDetail)
            {
                Sm.StdMsg(mMsgType.Warning, "The different between Total(Cash Advance) and Total(Cost) should be the same with Total(Detail).");
                //TxtAmt3.Focus();
                return true;
            }

            return false;
        }

        private bool IsGrdValueNotValid()
        {
            //mDeptCode = Sm.GetGrdStr(Grd1, 0, 10);
            //TxtDeptCode.EditValue = Sm.GetGrdStr(Grd1, 0, 11);
            //mPIC = Sm.GetGrdStr(Grd1, 0, 8);
            //TxtPIC.EditValue = Sm.GetGrdStr(Grd1, 0, 9);
            //var CurCode = TxtCurCode.Text;

            for (int r = 0; r < Grd1.Rows.Count - 1; r++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, r, 1, false, "Item is empty.")) return true;
                //if ((!mIsCASAllowToProcessZeroAmount || mIsCostCategoryCASMandatory) && Sm.IsGrdValueEmpty(Grd1, r, 4, false, "Cost category is empty.")) return true;
                //if (!mIsCASAllowToProcessZeroAmount && Sm.IsGrdValueEmpty(Grd1, r, 6, true, "Settlement amount should not be 0.00.")) return true;
                //if (mIsCASUseBudgetCategory && Sm.IsGrdValueEmpty(Grd1, r, 32, false, "Budget category is empty.")) return true;
                //if (mIsCASUseItemRate && Sm.IsGrdValueEmpty(Grd1, r, 26, false, "Item is empty.")) return true;
                //if (mIsCASUseItemRate && Sm.IsGrdValueEmpty(Grd1, r, 6, true, "Amount (Cost) is zero.")) return true;
                //if (!Sm.CompareStr(CurCode, Sm.GetGrdStr(Grd1, r, 5)))
                //{
                //    Sm.StdMsg(mMsgType.Warning,
                //        "Voucher# : " + Sm.GetGrdStr(Grd1, r, 1) + Environment.NewLine +
                //        "Currency : " + Sm.GetGrdStr(Grd1, r, 5) + Environment.NewLine + Environment.NewLine +
                //        "Currency is not valid."
                //        );
                //    return true;
                //}
                //if (!Sm.CompareStr(mPIC, Sm.GetGrdStr(Grd1, r, 8)))
                //{
                //    Sm.StdMsg(mMsgType.Warning,
                //        "Voucher# : " + Sm.GetGrdStr(Grd1, r, 1) + Environment.NewLine +
                //        "Person in charge : " + Sm.GetGrdStr(Grd1, r, 9) + Environment.NewLine + Environment.NewLine +
                //        "Person in charge is not valid."
                //        );
                //    return true;
                //}
                //if (!Sm.CompareStr(mDeptCode, Sm.GetGrdStr(Grd1, r, 10)))
                //{
                //    Sm.StdMsg(mMsgType.Warning,
                //        "Voucher# : " + Sm.GetGrdStr(Grd1, r, 1) + Environment.NewLine +
                //        "Department : " + Sm.GetGrdStr(Grd1, r, 11) + Environment.NewLine + Environment.NewLine +
                //        "Department is not valid."
                //        );
                //    return true;
                //}
            }

            //if (Sm.GetLue(LueDocStatus) == "F")
            //{
            //    var Amt1 = decimal.Parse(TxtAmt1.Text);
            //    var Amt2 = decimal.Parse(TxtAmt2.Text);

            //    if (!mIsCASUsePartialAmount && !IsVoucherCASBA())
            //    {
                    //if (Amt1 != Amt2)
                    //{ 
                    //    if (Grd4.Rows.Count <= 1)
                    //    {
                    //        Sm.StdMsg(mMsgType.Warning, "In details information, You need to input at least 1 record.");
                    //        return true;
                    //    }
                    //}
                    //else
                    //{
                    //    if (Grd4.Rows.Count > 1)
                    //    {
                    //        Sm.StdMsg(mMsgType.Warning, "You don't need to input any record in details information.");
                    //        return true;
                    //    }
                    //}
            //    }
            //}

            return false;
        }

        private MySqlCommand SavePettyCashDisbursement(string DocNo, bool IsNeedApproval)
        {
            var SQLHdr = new StringBuilder();
            var SQLDtl = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirst = true;

            #region Hdr

            SQLHdr.AppendLine("Insert Into TblPettyCashDisbursementHdr ");
            SQLHdr.AppendLine("(DocNo, LocalDocNo, DocDt, Status, CancelInd, TotalAmt, BalancePettyCash, CancelReason, PIC, DeptCode, ");
            if (mIsPCDUseCostCenter)
                SQLHdr.AppendLine("CCCode, ");
            if (mIsPCDUseDroppingRequest)
                SQLHdr.AppendLine("DroppingRequestDocNo, ");
            SQLHdr.AppendLine("CurCode, BankAcCode, CashTypeGrpCode, CashTypeCode, Remark, VoucherRequestDocNo, CreateBy, CreateDt) ");
            SQLHdr.AppendLine("Values(@DocNo, @LocalDocNo, @DocDt, @Status, 'N', @TotalAmt, @BalancePettyCash, Null, @PIC, @DeptCode, ");
            if (mIsPCDUseCostCenter)
                SQLHdr.AppendLine("@CCCode, ");
            if (mIsPCDUseDroppingRequest)
                SQLHdr.AppendLine("@DroppingRequestDocNo, ");
            SQLHdr.AppendLine("@CurCode, @BankAcCode, @CashTypeGrpCode, @CashTypeCode, @Remark, Null, @UserCode, CurrentDateTime()); ");

            #endregion

            #region Dtl

            SQLDtl.AppendLine("Insert Into TblPettyCashDisbursementDtl ");
            SQLDtl.AppendLine("(DocNo, DNo, ItCode, UoM, Qty, UPrice, CurCode, Amt, CCtCode, Remark, CreateBy, CreateDt) ");
            SQLDtl.AppendLine("Values ");
            for (int r = 0; r < Grd1.Rows.Count - 1; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 1).Length > 0)
                {
                    if (IsFirst)
                        IsFirst = false;
                    else
                        SQLDtl.AppendLine(", ");
                    SQLDtl.AppendLine("(@DocNo, @DNo_" + r.ToString() + ", @ItCode_" + r.ToString() + ", @UoM_" + r.ToString() + ", @Qty_" + r.ToString() + ", @UPrice_" + r.ToString() + ", @CurCode_" + r.ToString() + ", @Amt_" + r.ToString() + ", @CCtCode_" + r.ToString() + ", ");
                    SQLDtl.AppendLine("@Remark_" + r.ToString() + ", @UserCode, CurrentDateTime()) ");
                    #region approval
                    //if (!NoNeedApproval)
                    //{
                    //    SQL3.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
                    //    SQL3.AppendLine("Select T.DocType, @DocNo, @DNo_" + r.ToString() + ", T.DNo, @CreateBy, @Dt ");
                    //    SQL3.AppendLine("From TblDocApprovalSetting T ");
                    //    SQL3.AppendLine("Where T.DeptCode=@DeptCode ");
                    //    if (mIsApprovalBySiteMandatory)
                    //        SQL3.AppendLine("And IfNull(T.SiteCode, '')=@SiteCode ");
                    //    if (mIsMRApprovalByAmount)
                    //    {

                    //        SQL3.AppendLine("And (T.EndAmt=0 ");
                    //        SQL3.AppendLine("Or T.EndAmt>=IfNull(( ");
                    //        if (mAmtSourceMRApproval == "2")
                    //            SQL3.AppendLine("    Select IfNull(TotalPrice, 0) ");
                    //        else if (mAmtSourceMRApproval == "1")
                    //            SQL3.AppendLine("    Select IfNull(EstPrice, 0) ");
                    //        else if (mAmtSourceMRApproval == "3")
                    //            SQL3.AppendLine("    Select IfNull(@GrandTotal, 0) ");
                    //        SQL3.AppendLine("    From TblMaterialRequestDtl ");
                    //        SQL3.AppendLine("    Where DocNo=@DocNo And DNo=@DNo_" + r.ToString() + " ");
                    //        SQL3.AppendLine("), 0)) ");

                    //        SQL3.AppendLine("And (T.StartAmt=0 ");
                    //        SQL3.AppendLine("Or T.StartAmt<=IfNull(( ");
                    //        if (mAmtSourceMRApproval == "2")
                    //            SQL3.AppendLine("    Select IfNull(TotalPrice, 0) ");
                    //        else if (mAmtSourceMRApproval == "1")
                    //            SQL3.AppendLine("    Select IfNull(EstPrice, 0) ");
                    //        else if (mAmtSourceMRApproval == "3")
                    //            SQL3.AppendLine("    Select IfNull(@GrandTotal, 0) ");
                    //        SQL3.AppendLine("    From TblMaterialRequestDtl ");
                    //        SQL3.AppendLine("    Where DocNo=@DocNo And DNo=@DNo_" + r.ToString() + " ");
                    //        SQL3.AppendLine("), 0)) ");

                    //    }
                    //    if (mIsMRSPPJB)
                    //        SQL3.AppendLine("And T.DocType = 'MaterialRequestSPPJB' ");
                    //    else
                    //        SQL3.AppendLine("And T.DocType='MaterialRequest' ");
                    //    SQL3.AppendLine("; ");
                    //}

                    //SQL3.AppendLine("Update TblMaterialRequestDtl ");
                    //SQL3.AppendLine("Set Status = 'A' ");
                    //SQL3.AppendLine("Where DocNo = @DocNo And DNo=@DNo_" + r.ToString() + " ");
                    //SQL3.AppendLine("And Not Exists( ");
                    //SQL3.AppendLine("    Select 1 ");
                    //SQL3.AppendLine("    From TblDocApproval ");
                    //if (mIsMRSPPJB)
                    //    SQL3.AppendLine("    Where DocType = 'MaterialRequestSPPJB' ");
                    //else
                    //    SQL3.AppendLine("    Where DocType = 'MaterialRequest' ");
                    //SQL3.AppendLine("    And DocNo=@DocNo And DNo=@DNo_" + r.ToString() + " ");
                    //SQL3.AppendLine("); ");

                    //if (mIsUseECatalog)
                    //{
                    //    SQL3.AppendLine("Update TblMaterialRequestDtl ");
                    //    SQL3.AppendLine("Set PtCode = @PtCode_" + r.ToString() + ", CityCode = @CityCode_" + r.ToString() + " ");
                    //    SQL3.AppendLine("Where Docno = @DocNo ");
                    //    SQL3.AppendLine("And DNo = @DNo_" + r.ToString() + " ");
                    //    SQL3.AppendLine("; ");
                    //}
                    #endregion

                    Sm.CmParam<String>(ref cm, "@DNo_" + r.ToString(), Sm.Right("00" + (r + 1).ToString(), 3));
                    Sm.CmParam<String>(ref cm, "@ItCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 1));
                    Sm.CmParam<String>(ref cm, "@UoM_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 3));
                    Sm.CmParam<Decimal>(ref cm, "@Qty_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 4));
                    Sm.CmParam<Decimal>(ref cm, "@UPrice_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 5));
                    Sm.CmParam<String>(ref cm, "@CurCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 6));
                    Sm.CmParam<Decimal>(ref cm, "@Amt_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 7));
                    Sm.CmParam<String>(ref cm, "@Remark_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 8));
                    Sm.CmParam<String>(ref cm, "@CCtCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 9));
                    //Sm.CmParam<Decimal>(ref cm, "@EstPrice_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 29));
                    //Sm.CmParam<Decimal>(ref cm, "@TotalPrice_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 38));
                    //Sm.CmParam<String>(ref cm, "@DurationUom_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 36));
                    //Sm.CmParam<Decimal>(ref cm, "@Duration_" + r.ToString(), Sm.GetGrdDec(Grd1, r, 35));
                    //if (mIsUseECatalog)
                    //{
                    //    Sm.CmParam<String>(ref cm, "@PtCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 39));
                    //    Sm.CmParam<String>(ref cm, "@CityCode_" + r.ToString(), Sm.GetGrdStr(Grd1, r, 42));
                }
            }
            SQLDtl.AppendLine(";");

            #endregion

            #region Approval
            if (IsNeedApproval)
            {
                SQLHdr.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
                SQLHdr.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @UserCode, CurrentDateTime() ");
                SQLHdr.AppendLine("From TblDocApprovalSetting T ");
                SQLHdr.AppendLine("Where T.DocType='PettyCashDisbursement' And T.DeptCOde = @DeptCode "); // And T.DeptCode=@DeptCode 
                SQLHdr.AppendLine("And (T.StartAmt=0 ");
                SQLHdr.AppendLine("Or T.StartAmt<=IfNull(( ");
                SQLHdr.AppendLine("    Select IfNull(TotalAmt, 0) ");
                SQLHdr.AppendLine("    From TblPettyCashDisbursementHdr ");
                SQLHdr.AppendLine("    Where DocNo=@DocNo ");
                SQLHdr.AppendLine("), 0)) ");
                SQLHdr.AppendLine("And (T.EndAmt=0 ");
                SQLHdr.AppendLine("Or T.EndAmt>=IfNull(( ");
                SQLHdr.AppendLine("    Select IfNull(TotalAmt, 0) ");
                SQLHdr.AppendLine("    From TblPettyCashDisbursementHdr ");
                SQLHdr.AppendLine("    Where DocNo=@DocNo ");
                SQLHdr.AppendLine("), 0)) ");
                SQLHdr.AppendLine("And (T.DAGCode Is Null Or ");
                SQLHdr.AppendLine("(T.DAGCode Is Not Null ");
                SQLHdr.AppendLine("And T.DAGCode In ( ");
                SQLHdr.AppendLine("    Select A.DAGCode ");
                SQLHdr.AppendLine("    From TblDocApprovalGroupHdr A, TblDocApprovalGroupDtl B ");
                SQLHdr.AppendLine("    Where A.DAGCode=B.DAGCode ");
                SQLHdr.AppendLine("    And A.ActInd='Y' ");
                SQLHdr.AppendLine("    And B.EmpCode=@PIC ");
                SQLHdr.AppendLine("))); ");
            }

            SQLHdr.AppendLine("Update TblPettyCashDisbursementHdr Set Status='A' ");
            SQLHdr.AppendLine("Where DocNo=@DocNo ");
            SQLHdr.AppendLine("And Not Exists( ");
            SQLHdr.AppendLine("    Select 1 From TblDocApproval ");
            SQLHdr.AppendLine("    Where DocType='PettyCashDisbursement' ");
            SQLHdr.AppendLine("    And DocNo=@DocNo ");
            SQLHdr.AppendLine("); ");
            #endregion

            cm.CommandText = SQLHdr.ToString() + SQLDtl.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@LocalDocNo", TxtLocalDocument.Text);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@Status", IsNeedApproval ? "O" : "A");
            Sm.CmParam<Decimal>(ref cm, "@TotalAmt", Decimal.Parse(TxtAmt1.Text));
            Sm.CmParam<Decimal>(ref cm, "@BalancePettyCash", Decimal.Parse(TxtAmt2.Text));
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@PIC", Sm.GetLue(LuePICCode));
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetLue(LueDeptCode));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode1));
            if (mIsPCDUseCostCenter)
                Sm.CmParam<String>(ref cm, "@CCCode", Sm.GetLue(LueCCCode));
            Sm.CmParam<String>(ref cm, "@BankAcCode", Sm.GetLue(LueBankAcCode));
            Sm.CmParam<String>(ref cm, "@CashTypeGrpCode", Sm.GetLue(LueCashTypeGrp));
            Sm.CmParam<String>(ref cm, "@CashTypeCode", Sm.GetLue(LueCashType));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            if (mIsPCDUseDroppingRequest)
                Sm.CmParam<String>(ref cm, "@DroppingRequestDocNo", TxtDRQDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;

        }

        private MySqlCommand SaveVoucherRequest(string VoucherRequestDocNo, string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblPettyCashDisbursementHdr Set VoucherRequestDocNo=@DocNo Where DocNo=@PettyCashDisbursementDocNo; ");

            SQL.AppendLine("Insert Into TblVoucherRequestHdr(DocNo, DocDt, CancelInd, Status, PIC, DeptCode, DocType, AcType, BankAcCode, CurCode, Amt, PaymentType, Remark, CreateBy,CreateDt)  ");
            SQL.AppendLine("Select @DocNo, DocDt, 'N', 'O', PIC, DeptCode, '75', 'C', BankAcCode, CurCode, TotalAmt, 'C', Remark, CreateBy, CreateDt ");
            SQL.AppendLine("From TblPettyCashDisbursementHdr Where DocNo = @PettyCashDisbursementDocNo; ");

            SQL.AppendLine("Insert Into TblVoucherRequestDtl(DocNo, DNo, Description, Amt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocNo, '001', @PettyCashDisbursementDocNo, TotalAmt, Remark, CreateBy, CreateDt ");
            SQL.AppendLine("From TblPettyCashDisbursementHdr Where DocNo = @PettyCashDisbursementDocNo; ");

            SQL.AppendLine("Update TblVoucherRequestHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select 1 From TblDocApproval ");
            SQL.AppendLine("    Where DocType='PettyCashDisbursement' ");
            SQL.AppendLine("    And DocNo=@PettyCashDisbursementDocNo ");
            SQL.AppendLine("); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", VoucherRequestDocNo);
            Sm.CmParam<String>(ref cm, "@PettyCashDisbursementDocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@DeptCode", mDeptCode);
            Sm.CmParam<String>(ref cm, "@PIC", mPIC);

            return cm;
        }

        private MySqlCommand SaveVoucher(string VoucherRequestDocNo, string VoucherDocNo, string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblVoucherRequestHdr Set VoucherDocNo=@VoucherDocNo ");
            SQL.AppendLine("Where DocNo=@VoucherRequestDocNo ");
            SQL.AppendLine("And Status = 'A'; ");

            SQL.AppendLine("Insert Into TblVoucherHdr (DocNo, DocDt, CancelInd, DocType, VoucherRequestDocNo, AcType, BankAcCode, CurCode, Amt, JournalDocNo, PIC, ");
            SQL.AppendLine("PaymentType, CashTypeGrpCode, CashTypeCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @VoucherDocNo, B.DocDt, 'N', B.DocType, B.DocNo, 'C', B.BankAcCode, B.CurCode, B.Amt, NULL, B.PIC, ");
            SQL.AppendLine("'C', A.CashTypeGrpCode, A.CashTypeCode, B.CreateBy, B.CreateDt ");
            SQL.AppendLine("From TblPettyCashDisbursementHdr A ");
            SQL.AppendLine("Inner Join TblVoucherRequestHdr B On A.VoucherRequestDocNo = B.DocNo ");
            SQL.AppendLine("Where A.DocNo = @DocNo ");
            SQL.AppendLine("And A.Status = 'A'; ");

            SQL.AppendLine("Insert Into TblVoucherDtl (DocNo, DNo, Description, Amt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select B.VoucherDocNo, C.DNo, C.Description, IfNull(C.Amt, 0.00), C.Remark, C.CreateBy, C.CreateDt ");
            SQL.AppendLine("From TblPettyCashDisbursementHdr A ");
            SQL.AppendLine("Inner Join TblVoucherRequestHdr B On A.VoucherRequestDocNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblVoucherRequestDtl C On B.DocNo = C.DocNo ");
            SQL.AppendLine("Where A.DocNo = @DocNo ");
            SQL.AppendLine("And A.Status = 'A'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo", VoucherRequestDocNo);
            Sm.CmParam<String>(ref cm, "@VoucherDocNo", VoucherDocNo);
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            return cm;
        }

        private MySqlCommand SaveCashAdvanceSettlementDtl(string DocNo, int r)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblCashAdvanceSettlementDtl(DocNo, DNo, VoucherDocNo, CCtCode, ");
            if (mIsCASUseItemRate)
                SQL.AppendLine("ItCode, Qty, Rate, ");
            if (mIsCASUseBudgetCategory)
                SQL.AppendLine("BCCode, Description, ");
            SQL.AppendLine("CurCode, Amt, PIC, DeptCode, Remark, FileName, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @VoucherDocNo, @CCtCode, ");
            if (mIsCASUseItemRate)
                SQL.AppendLine("@ItCode, @Qty, @Rate, ");
            if (mIsCASUseBudgetCategory)
                SQL.AppendLine("@BCCode, @Description, ");
            SQL.AppendLine("@CurCode, @Amt, @PIC, @DeptCode, @Remark, @FileName, @UserCode, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00000" + (r + 1).ToString(), 5));
            Sm.CmParam<String>(ref cm, "@VoucherDocNo", Sm.GetGrdStr(Grd1, r, 1));
            Sm.CmParam<String>(ref cm, "@CCtCode", Sm.GetGrdStr(Grd1, r, 3));
            if (mIsCASUseItemRate)
            {
                Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, r, 24));
                Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, r, 27));
                Sm.CmParam<Decimal>(ref cm, "@Rate", Sm.GetGrdDec(Grd1, r, 28));
            }
            if (mIsCASUseBudgetCategory)
            {
                Sm.CmParam<String>(ref cm, "@BCCode", Sm.GetGrdStr(Grd1, r, 31));
                Sm.CmParam<String>(ref cm, "@Description", Sm.GetGrdStr(Grd1, r, 33));
            }
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetGrdStr(Grd1, r, 5));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd1, r, 6));
            Sm.CmParam<String>(ref cm, "@PIC", Sm.GetGrdStr(Grd1, r, 8));
            Sm.CmParam<String>(ref cm, "@DeptCode", Sm.GetGrdStr(Grd1, r, 10));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, r, 12));
            Sm.CmParam<String>(ref cm, "@FileName", Sm.GetGrdStr(Grd1, r, 14));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }
        
        private MySqlCommand UpdateFile(string DocNo, string FileName)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblPettyCashDisbursementHdr Set ");
            SQL.AppendLine("    FileName=@FileName ");
            SQL.AppendLine("Where DocNo=@DocNo  ; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@FileName", FileName);

            return cm;
        }

        private MySqlCommand SaveCashAdvanceSettlementDtl2(string DocNo, int r)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblCashAdvanceSettlementDtl2(DocNo, DNo, CCtCode, Amt, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @CCtCode, @Amt, @UserCode, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00000" + (r + 1).ToString(), 5));
            //Sm.CmParam<String>(ref cm, "@CCtCode", Sm.GetGrdStr(Grd2, r, 0));
            //Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd2, r, 2));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveCashAdvanceSettlementDtl3(string DocNo, int r)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblCashAdvanceSettlementDtl3(DocNo, DNo, VoucherDocNo, CurCode, Amt1, Amt2, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @VoucherDocNo, @CurCode, @Amt1, @Amt2, @UserCode, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00000" + (r + 1).ToString(), 5));
            //Sm.CmParam<String>(ref cm, "@VoucherDocNo", Sm.GetGrdStr(Grd3, r, 0));
            //Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetGrdStr(Grd3, r, 2));
            //Sm.CmParam<Decimal>(ref cm, "@Amt1", Sm.GetGrdDec(Grd3, r, 3));
            //Sm.CmParam<Decimal>(ref cm, "@Amt2", Sm.GetGrdDec(Grd3, r, 4));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveCashAdvanceSettlementDtl4(string DocNo, int r)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblCashAdvanceSettlementDtl4(DocNo, DNo, Description, ");
            if (mIsCASUseBudgetCategory)
                SQL.AppendLine(" TypeCode, ");
            SQL.AppendLine(" Amt, CreateBy, CreateDt)  ");
            SQL.AppendLine("Values(@DocNo, @DNo, @Description, ");
            if (mIsCASUseBudgetCategory)
                SQL.AppendLine("@TypeCode, ");
            SQL.AppendLine("@Amt, @UserCode, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00000" + (r + 1).ToString(), 5));
            //Sm.CmParam<String>(ref cm, "@Description", Sm.GetGrdStr(Grd4, r, 1));
            //Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd4, r, 2));
            if (mIsCASUseBudgetCategory)
                //Sm.CmParam<String>(ref cm, "@TypeCode", Sm.GetGrdStr(Grd4, r, 3));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Edit data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;
            string VoucherRequestDocNo = string.Empty;
            //var IsNeedApproval = IsDocNeedApproval();
            string mDocNo = TxtDocNo.Text;
            //string mInitProcessInd = Sm.GetValue("Select DocStatus From TblCashAdvanceSettlementHdr Where DocNo = @Param; ", TxtDocNo.Text);
            //bool IsCreatedJournal2 = Sm.IsDataExist("Select 1 From TblCashAdvanceSettlementHdr Where DocNo = @Param And JournalDocNo2 Is Not Null; ", TxtDocNo.Text);

            //if (mVoucherCodeFormatType == "2")
            //    VoucherRequestDocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "VoucherRequest", "TblVoucherRequestHdr");
            //else
            //    VoucherRequestDocNo = GenerateVoucherRequestDocNo();

            var cml = new List<MySqlCommand>();

            cml.Add(EditPettyCashDisbursementHdr());
            cml.Add(UpdateVoucherRequest());
            cml.Add(UpdateVoucher());
            if(mIsAutoJournalActived)
                cml.Add(SaveJournal2());

            //if (mIsCASUseDraftDocument)
            //{
            //    for (int r = 0; r < Grd1.Rows.Count; r++)
            //        if (Sm.GetGrdStr(Grd1, r, 1).Length > 0)
            //            cml.Add(SaveCashAdvanceSettlementDtl(mDocNo, r));

            //for (int r = 0; r < Grd2.Rows.Count; r++)
            //    if (Sm.GetGrdStr(Grd2, r, 0).Length > 0)
            //        cml.Add(SaveCashAdvanceSettlementDtl2(mDocNo, r));

            //for (int r = 0; r < Grd3.Rows.Count; r++)
            //    if (Sm.GetGrdStr(Grd3, r, 0).Length > 0)
            //        cml.Add(SaveCashAdvanceSettlementDtl3(mDocNo, r));

            //for (int r = 0; r < Grd4.Rows.Count; r++)
            //    if (Sm.GetGrdStr(Grd4, r, 1).Length > 0)
            //        cml.Add(SaveCashAdvanceSettlementDtl4(mDocNo, r));
            //}

            //if (!ChkCancelInd.Checked)
            //{
            //    //hanya untuk yg pakai draft-final, yang statusnya dari draft jadi final.
            //    //kalau dia sudah final dari awal, tidak perlu process ini
            //    if (mIsCASUseDraftDocument/* && Sm.GetLue(LueDocStatus) == "F"*/ && mInitProcessInd != "F")
            //    {
            //        if (!mIsCASUsePartialAmount/* && Grd4.Rows.Count > 1*/)
            //            cml.Add(SaveVoucherRequest(VoucherRequestDocNo, mDocNo));
            //        else
            //        {
            //            if (IsNeedApproval)
            //                cml.Add(SaveDocApproval(mDocNo));
            //            else
            //            {
            //                if (mIsAutoJournalActived)
            //                    cml.Add(SaveJournal(mDocNo));
            //            }
            //        }
            //    }
            //}
            //else // if cancelled
            //{
            //    if (mCASJournalFormula == "1")
            //    {
            //        //jika param auto journal nya aktif, dan belum pernah dibuat journal pembalik sebelumnya
            //        if (mIsAutoJournalActived && !IsCreatedJournal2)
            //        {
            //            if (!mIsCASUseDraftDocument || (mIsCASUseDraftDocument/* && Sm.GetLue(LueDocStatus) == "F"*/))
            //            {
            //                cml.Add(SaveJournal2());
            //            }
            //        }
            //    }
            //    else
            //    {
            //        cml.Add(SaveJournal2());
            //        cml.Add(UpdateVoucherRequest());
            //    }
            //}

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                //Sm.IsClosingJournalInvalid(mIsAutoJournalActived, true, Sm.GetDte(DteDocDt)) ||
                //(mIsClosingJournalBasedOnMultiProfitCenter ? 
                //    Sm.IsClosingJournalInvalid(mIsAutoJournalActived, true, Sm.GetDte(DteDocDt), GetProfitCenterCode()) : 
                //    Sm.IsClosingJournalInvalid(mIsAutoJournalActived, true, Sm.GetDte(DteDocDt))) ||
                //(!mIsCASUseDraftDocument && IsDocNotCancelled()) ||
                IsGrdEmpty() ||
                //(!ChkCancelInd.Checked && IsGrdValueNotValid()) ||
                //(!ChkCancelInd.Checked && IsTotalDetailNotValid()) ||
                //(!ChkCancelInd.Checked && IsTotalDetailNotValid2()) ||
                IsDataCancelledAlready() //||
                //IsDocAlreadyProcessToVoucher() //||
                //(mIsCASUseDraftDocument && Sm.IsLueEmpty(LueDocStatus, "Document Status"))
                //(mIsCheckCOAJournalNotExists && IsCOAJournalNotValid());
                ;
        }

        private bool IsDocNotCancelled()
        {
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this document.");
                return true;
            }
            return false;
        }

        private bool IsDataCancelledAlready()
        {
            return Sm.IsDataExist(
                "Select 1 From TblPettyCashDisbursementHdr Where (CancelInd='Y' Or Status='C') And DocNo=@Param;",
                TxtDocNo.Text,
                "This document already cancelled."
                );
        }

        private bool IsDocumentFinalAlready()
        {
            return Sm.IsDataExist(
                "Select 1 From TblCashAdvanceSettlementHdr Where DocStatus='F' And DocNo=@Param;",
                TxtDocNo.Text,
                "This document already final."
                );
        }

        private bool IsDocAlreadyProcessToVoucher()
        {
            return Sm.IsDataExist(
                "Select 1 From TblVoucherHdr " +
                "Where CancelInd='N' " +
                "And VoucherRequestDocNo In ( " +
                "   Select VoucherRequestDocNo From TblCashAdvanceSettlementHdr Where DocNo=@Param " +
                ");",
                TxtDocNo.Text,
                "This document already processed to voucher."
                );
        }

        private MySqlCommand EditPettyCashDisbursementHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblPettyCashDisbursementHdr Set ");
            //if (mIsCASUseCompletedInd)
            //    SQL.AppendLine("    CompletedInd = @CompletedInd, ");
            SQL.AppendLine("    CancelReason=@CancelReason, CancelInd=@CancelInd, ");
            SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N'; ");

            //if (mIsCASUseDraftDocument)
            //{
            //    SQL.AppendLine("Delete From  TblCashAdvanceSettlementDtl Where Docno = @DocNo; ");
            //    SQL.AppendLine("Delete From  TblCashAdvanceSettlementDtl2 Where Docno = @DocNo; ");
            //    SQL.AppendLine("Delete From  TblCashAdvanceSettlementDtl3 Where Docno = @DocNo; ");
            //    SQL.AppendLine("Delete From  TblCashAdvanceSettlementDtl4 Where Docno = @DocNo; ");
            //}

            //if (ChkCancelInd.Checked)
            //{
            //    SQL.AppendLine("Update TblVoucherRequestHdr Set ");
            //    SQL.AppendLine("    CancelReason=@CancelReason, CancelInd=@CancelInd, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            //    SQL.AppendLine("Where DocNo=@VRDocno And CancelInd='N'; ");
            //}

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            //if (mIsCASUseCompletedInd)
            //    Sm.CmParam<String>(ref cm, "@CompletedInd", ChkCompletedInd.Checked ? "Y" : "N");
            //Sm.CmParam<String>(ref cm, "@VRDocNo", TxtVoucherDocNo.Text);
            //Sm.CmParam<Decimal>(ref cm, "@Amt1", Decimal.Parse(TxtAmt1.Text));
            //Sm.CmParam<Decimal>(ref cm, "@Amt2", Decimal.Parse(TxtAmt2.Text));
            //Sm.CmParam<Decimal>(ref cm, "@Amt3", Decimal.Parse(TxtAmt3.Text));
            //Sm.CmParam<String>(ref cm, "@DocStatus", Sm.GetLue(LueDocStatus));
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@CancelInd", ChkCancelInd.Checked? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand UpdateVoucherRequest()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblVoucherRequestHdr A ");
            SQL.AppendLine("Inner Join TblPettyCashDisbursementHdr B On A.Docno = B.VoucherRequestDocNo ");
            SQL.AppendLine("Set A.CancelInd = 'Y', A.CancelReason = @CancelReason, A.LastUpBy = @UserCode, A.LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("Where B.DocNo = @DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand UpdateVoucher()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblVoucherHdr A ");
            SQL.AppendLine("Inner Join TblPettyCashDisbursementHdr B On A.VoucherRequestDocNo = B.VoucherRequestDocNo ");
            SQL.AppendLine("Set A.CancelInd = 'Y', A.CancelReason = @CancelReason, A.LastUpBy = @UserCode, A.LastUpDt = CurrentDateTime() ");
            SQL.AppendLine("Where B.DocNo = @DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal2()
        {
            var SQL = new StringBuilder();
            var CurrentDt = Sm.ServerCurrentDate();
            var DocDt = Sm.GetDte(DteDocDt);
            var IsClosingJournalUseCurrentDt = Sm.IsClosingJournalUseCurrentDt(DocDt);

            SQL.AppendLine("Update TblVoucherHdr A ");
            SQL.AppendLine("Inner Join TblPettyCashDisbursementHdr B On A.VoucherRequestDocNo = B.VoucherRequestDocNo ");
            SQL.AppendLine("Set A.JournalDocNo2 = @JournalDocNo ");
            SQL.AppendLine("Where B.DocNo = @DocNo ");
            SQL.AppendLine("And B.CancelInd = 'Y' ");
            SQL.AppendLine("And A.JournalDocNo Is Not Null; ");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, ");
            if (IsClosingJournalUseCurrentDt)
                SQL.AppendLine("Replace(CurDate(), '-', ''), ");
            else
                SQL.AppendLine("DocDt, ");
            SQL.AppendLine("Concat('Cancelling ', JnDesc) As JnDesc, ");
            SQL.AppendLine("MenuCode, MenuDesc, CCCode, Remark, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblJournalHdr ");
            SQL.AppendLine("Where DocNo In ( ");
            SQL.AppendLine("    Select JournalDocNo ");
            SQL.AppendLine("    From TblVoucherHdr A ");
            SQL.AppendLine("    Inner Join TblPettyCashDisbursementHdr B On A.VoucherRequestDocNo = B.VoucherRequestDocNo ");
            SQL.AppendLine("    Where B.DocNo=@DocNo ");
            SQL.AppendLine("    And A.CancelInd='Y' ");
            SQL.AppendLine("    And A.JournalDocNo Is Not Null ");
            SQL.AppendLine(");");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, DNo, AcNo, CAMt As DAmt, DAmt As CAmt, EntCode, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblJournalDtl ");
            SQL.AppendLine("Where DocNo In ( ");
            SQL.AppendLine("    Select A.JournalDocNo ");
            SQL.AppendLine("    From TblVoucherHdr A ");
            SQL.AppendLine("    Inner Join TblPettyCashDisbursementHdr B On A.VoucherRequestDocNo = B.VoucherRequestDocNo ");
            SQL.AppendLine("    Where B.DocNo=@DocNo ");
            SQL.AppendLine("    And A.CancelInd='Y' ");
            SQL.AppendLine("    And A.JournalDocNo Is Not Null ");
            SQL.AppendLine("    ); ");

            #region old
            //if (mIsVRForBudgetUseSOContract || mIsVRUseSOContract)
            //{
            //    SQL.AppendLine("Update TblJournalDtl A ");
            //    SQL.AppendLine("Inner Join TblCashAdvanceSettlementHdr B On A.DocNo = B.JournalDocNo2 ");
            //    SQL.AppendLine("    And B.DocNo = @DocNo ");
            //    SQL.AppendLine("Inner Join TblCashAdvanceSettlementDtl C On B.DocNo = C.DocNo ");
            //    SQL.AppendLine("Inner Join TblVoucherRequestHdr D On C.VoucherDocNo = D.VoucherDocNo ");
            //    SQL.AppendLine("Set A.SOContractDocNo = D.SOContractDocNo ");
            //    SQL.AppendLine("Where D.SOContractDocNo Is Not Null; ");
            //}

            //SQL.AppendLine("Update TblJournalDtl A ");
            //SQL.AppendLine("Inner Join TblCashAdvanceSettlementHdr B On A.DocNo = B.JournalDocNo2 And B.DocNo = @DocNo ");
            //SQL.AppendLine("Inner JOin TblCashAdvanceSettlementDtl B1 On B.DocNo = B1.DocNo ");
            //SQL.AppendLine("Inner Join TblVoucherHdr C On B1.VoucherDocNo = C.DocNo And C.Doctype = '61' ");
            //SQL.AppendLine("Inner Join TblTravelRequestDtl8 D On C.VoucherRequestDocNo = D.VoucherRequestDocNo ");
            //SQL.AppendLine("Inner Join TblTravelRequestHdr E On D.DocNo = E.DocNo ");
            //SQL.AppendLine("Set A.SOContractDocNo = E.SOContractDocNo ");
            //SQL.AppendLine("Where B.DocNo = @DocNo ");
            //SQL.AppendLine("And E.SOContractDocNo Is Not Null; ");
            #endregion

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (IsClosingJournalUseCurrentDt)
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(CurrentDt, 1));
            else
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(DocDt, 1));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        #endregion

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowPettyCashDisbursementHdr(DocNo);
                ShowPettyCashDisbursementDtl(DocNo);
                if (mIsPCDUseDroppingRequest)
                    ShowDroppingRequestHdr(DocNo); ComputeBalance();
                //ShowCashAdvanceSettlementDtl2(DocNo);
                //ShowCashAdvanceSettlementDtl3(DocNo);
                //ShowCashAdvanceSettlementDtl4(DocNo);
                Sm.ShowDocApproval(DocNo, "PettyCashDisbursement", ref Grd5, mIsTransactionUseDetailApprovalInformation);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        internal void CopyData(string DocNo)
        {
            try
            {
                ClearData();
                ShowPettyCashDisbursementHdr(DocNo);
                TxtDocNo.EditValue = TxtStatus.EditValue = MeeCancelReason.EditValue = /*TxtPIC.EditValue = TxtDeptCode.EditValue = */TxtVoucherDocNo.EditValue = null;
                Sm.SetDteCurrentDate(DteDocDt);
                ChkCancelInd.Checked = false;
                TxtCopyData.EditValue = DocNo;
                ShowPettyCashDisbursementDtl(DocNo);
                //Sm.SetLue(LueDocStatus, "F");
                //ShowCashAdvanceSettlementDtl(DocNo);
                //ShowCashAdvanceSettlementDtl2(DocNo);
                //ShowCashAdvanceSettlementDtl3(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void ShowPettyCashDisbursementHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.LocalDocNo, ");
            SQL.AppendLine("Case A.Status When 'O' Then 'Outstanding' When 'A' Then 'Approved' When 'C' Then 'Cancel' End As StatusDesc, A.CancelReason, A.CancelInd, A.PIC, A.DeptCode, ");
            SQL.AppendLine("A.CurCode, A.BankAcCode, A.TotalAmt, A.BalancePettyCash, A.CashTypeGrpCode, A.CashTypeCode, A.Remark, E.DocNo VoucherDocNo, A.FileName, ");
            //if (mIsCASUseCompletedInd)
            //    SQL.AppendLine("A.CompletedInd, ");
            //else
            //    SQL.AppendLine("'N' As CompletedInd, ");

            if (mIsPCDUseCostCenter)
                SQL.AppendLine("A.CCCode ");
            else
                SQL.AppendLine("Null As CCCode ");
            SQL.AppendLine("From TblPettyCashDisbursementHdr A ");
            SQL.AppendLine("Left Join TblUser B On A.PIC=B.UserCode ");
            SQL.AppendLine("Left Join TblDepartment C On A.DeptCode=C.DeptCode ");
            SQL.AppendLine("Left Join TblVoucherRequestHdr D On A.VoucherRequestDocNo = D.DocNo ");
            SQL.AppendLine("Left Join TblVoucherHdr E On D.VoucherDocNo = E.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DocNo", 

                    //1-5
                    "DocDt", "StatusDesc", "CancelReason", "CancelInd", "PIC", 

                    //6-10
                    "DeptCode", "CurCode", "BankAcCode", "CCCode", "Remark", 

                    //11-15
                    "TotalAmt", "BalancePettyCash", "CashTypeGrpCode", "CashTypeCode", "VoucherDocNo",

                    //16-19
                    "LocalDocNo", "FileName", //"CCCode", "CompletedInd", 
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                    TxtStatus.EditValue = Sm.DrStr(dr, c[2]);
                    MeeCancelReason.EditValue = Sm.DrStr(dr, c[3]);
                    ChkCancelInd.Checked = Sm.DrStr(dr, c[4]) == "Y";
                    Sm.SetLue(LuePICCode, Sm.DrStr(dr, c[5]));
                    Sl.SetLueDeptCode(ref LueDeptCode, Sm.DrStr(dr, c[6]));
                    Sm.SetLue(LueCurCode1, Sm.DrStr(dr, c[7]));
                    SetLueBankAcCode(ref LueBankAcCode, Sm.DrStr(dr, c[8]));
                    if (mIsPCDUseCostCenter)
                    {
                        Sl.SetLueCCCode(ref LueCCCode, Sm.DrStr(dr, c[9]), mIsFilterByCC ? "Y" : "N");
                        Sm.SetLue(LueCCCode, Sm.DrStr(dr, c[9]));
                    }
                    MeeRemark.EditValue = Sm.DrStr(dr, c[10]);
                    TxtAmt1.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[11]), 0);
                    TxtAmt2.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[12]), 0);
                    Sm.SetLue(LueCashTypeGrp, Sm.DrStr(dr, c[13]));
                    SetLueCashTypeCode(ref LueCashType, string.Empty, Sm.DrStr(dr, c[14]));
                    TxtVoucherDocNo.EditValue = Sm.DrStr(dr, c[15]);
                    TxtLocalDocument.EditValue = Sm.DrStr(dr, c[16]);
                    TxtFile.EditValue = Sm.DrStr(dr, c[17]);
                }, true
            );
        }

        internal void ShowDroppingRequestHdr(String DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.DocNo, E.DeptName, B.Yr, B.Mth, B.PRJIDocNo, D.BCName, B.Amt, B.Remark, (B.Amt - A.TotalAmt) Balance ");
            SQL.AppendLine("From TblPettyCashDisbursementHdr A ");
            SQL.AppendLine("Left Join TblDroppingRequestHdr B On A.DroppingRequestDocNo = B.DocNo ");
            SQL.AppendLine("Left Join TblDroppingRequestDtl2 C on B.DocNo = C.DocNo ");
            SQL.AppendLine("Left Join TblBudgetCategory D On C.BCCode = D.BCCode ");
            SQL.AppendLine("Left Join TblDepartment E On B.DeptCode = E.DeptCode ");
            SQL.AppendLine("Where A.DocNo = @DocNo ");


            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[]
                { 
                    //0
                    "DocNo", 

                    //1-5
                    "DeptName", "Yr", "Mth", "PRJIDocNo", "BCName", 

                    //6-8
                    "Amt", "Remark", "Balance"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDRQDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    TxtDepartment.EditValue = Sm.DrStr(dr, c[1]);
                    TxtYear.EditValue = Sm.DrStr(dr, c[2]);
                    TxtMonth.EditValue = Sm.DrStr(dr, c[3]);
                    TxtPRJIDocNo.EditValue = Sm.DrStr(dr, c[4]);
                    TxtBudgetCategory.EditValue = Sm.DrStr(dr, c[5]);
                    TxtDRQAmount.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[6]), 0);
                    MeeRemarkDRQ.EditValue = Sm.DrStr(dr, c[7]);
                    TxtBalance.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[8]), 0);
                }, true
            );
        }

        internal void GetDroppingRequest(String DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, D.DeptName, A.Yr, A.Mth, A.PRJIDocNo, C.BCName, A.Amt, A.Remark ");
            SQL.AppendLine("From TblDroppingRequestHdr A ");
            SQL.AppendLine("Left Join TblDroppingRequestDtl2 B On A.DocNo = B.DocNo ");
            SQL.AppendLine("Left Join TblBudgetCategory C On B.BCCode = C.BCCode ");
            SQL.AppendLine("Left Join TblDepartment D On A.DeptCode = D.DeptCode ");
            SQL.AppendLine("Where A.DocNo = @DroppingRequestDocNo ");


            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DroppingRequestDocNo", DocNo);

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[]
                { 
                    //0
                    "DocNo", 

                    //1-5
                    "DeptName", "Yr", "Mth", "PRJIDocNo", "BCName", 

                    //6-7
                    "Amt", "Remark"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDRQDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    TxtDepartment.EditValue = Sm.DrStr(dr, c[1]);
                    TxtYear.EditValue = Sm.DrStr(dr, c[2]);
                    TxtMonth.EditValue = Sm.DrStr(dr, c[3]);
                    TxtPRJIDocNo.EditValue = Sm.DrStr(dr, c[4]);
                    TxtBudgetCategory.EditValue = Sm.DrStr(dr, c[5]);
                    TxtDRQAmount.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[6]), 0);
                    MeeRemarkDRQ.EditValue = Sm.DrStr(dr, c[7]);
                }, true
            );
        }

        private void ShowPettyCashDisbursementDtl(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.Dno, A.ItCode, C.ItName, A.UoM, A.Qty, A.UPrice, A.CurCode, A.Remark, A.CCtCode, B.CCtName ");
            SQL.AppendLine(" ");
            //SQL.AppendLine("If(C.DocType ='61', ");
            //if (mIsCASFromTRProcessAllAmount)
            //    SQL.AppendLine("C.Amt");
            //else
                //SQL.AppendLine("(C.Amt- H.DailyAmt) ");
            //SQL.AppendLine(", C.Amt) As VoucherAmt, C.DocType VCDocType, ");
            //if (mIsVRForBudgetUseSOContract || mIsVRUseSOContract)
            //    SQL.AppendLine("K.SOContractDocNo, ");
            //else
            //    SQL.AppendLine("Null As SOContractDocNo, ");
            //if (mIsCASUseItemRate)
            //{
            //    SQL.AppendLine("A.ItCode, J.ItName, A.Qty, A.Rate, ");
            //}
            //else
            //{
            //    SQL.AppendLine("Null As ItCode, Null As ItName, 0.00 As Qty, 0.00 As Rate, ");
            //}
            //if (mIsCASUseBudgetCategory)
            //    SQL.AppendLine("A.BCCode, M.BCName, A.Description ");
            //else
            //    SQL.AppendLine("Null As BCCode, Null As BCName, null As Description ");
            SQL.AppendLine("From TblPettyCashDisbursementDtl A ");
            SQL.AppendLine("Left Join TblCostCategory B On A.CCtCode=B.CCtCode ");
            SQL.AppendLine("Left Join TblItem C On A.ItCode=C.ItCode ");
            //SQL.AppendLine("Inner Join TblVoucherHdr C On A.VoucherDocNo=C.DocNo ");
            //SQL.AppendLine("Left Join TblUser D On A.PIC=D.UserCode ");
            //SQL.AppendLine("Left Join TblDepartment E On A.DeptCode=E.DeptCode ");
            //SQL.AppendLine("Left Join TblCostCenter F On B.CCCode=F.CCCode ");
            //SQL.AppendLine("Left Join TblOption G On C.DocType=G.OptCode And G.OptCat='VoucherDocType' ");
            //if (!mIsCASFromTRProcessAllAmount)
            //{
            //    SQL.AppendLine("Left Join ( ");
            //    SQL.AppendLine("    Select A.VoucherRequestDocNo As DocNo, SUM(B.Amt2) As DailyAmt  ");
            //    SQL.AppendLine("    From TblTravelRequestDtl8 A  ");
            //    SQL.AppendLine("    Inner Join TblTravelRequestDtl7 B On A.DocNo = B.DocNo  And A.EmpCode = B.PICCode ");
            //    SQL.AppendLine("    Inner Join TblTravelRequestHdr C On A.DocNo = C.DocNo And CancelInd = 'N' ");
            //    SQL.AppendLine("    Group By A.VoucherRequestDocNo ");
            //    SQL.AppendLine(")H On H.DocNo = C.VoucherRequestDocNo ");
            //}
            //SQL.AppendLine("Left Join TblCOA I On B.AcNo = I.AcNo ");
            //if (mIsCASUseItemRate)
            //{
            //    SQL.AppendLine("Left Join TblItem J On A.ItCode = J.ItCode ");
            //}

            //if (mIsVRUseSOContract || mIsVRForBudgetUseSOContract)
            //    SQL.AppendLine("Left Join TblVoucherRequestHdr K On C.VOucherRequestDocNo = K.DocNo ");
            //if (mIsCASUseBudgetCategory)
            //    SQL.AppendLine("Left Join TblBudgetCategory M On A.BCCode = M.BCCode ");

            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo; ");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
                    ref Grd1, ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        "ItCode", 
                        "ItName", "UoM", "Qty", "UPrice", "CurCode", 
                        "Remark", "CCtCode", "CCtName",
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        //Grd1.Cells[Row, 7].Value = Sm.GetGrdDec(Grd1, Row, 3) * Sm.GetGrdDec(Grd1, Row, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                        //if (mIsCASUseItemRate)
                        //{
                        //    Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 17);
                        //    Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 18);
                        //    Sm.SetGrdValue("N", Grd, dr, c, Row, 27, 19);
                        //    Sm.SetGrdValue("N", Grd, dr, c, Row, 28, 20);
                        //}
                        //if (mIsVRUseSOContract || mIsVRForBudgetUseSOContract)
                        //    Sm.SetGrdValue("S", Grd, dr, c, Row, 29, 21);
                        //if (mIsCASUseBudgetCategory)
                        //{
                        //    Sm.SetGrdValue("S", Grd, dr, c, Row, 31, 22);
                        //    Sm.SetGrdValue("S", Grd, dr, c, Row, 32, 23);
                        //    Sm.SetGrdValue("S", Grd, dr, c, Row, 33, 24);
                        //}
                    }, false, false, true, false
            );
            ComputeAmt();
            ComputeTotalCost();
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 4, 5, 7 });
            Sm.FocusGrd(Grd1, 0, 0);
        }

        /*private void ShowCashAdvanceSettlementDtl2(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.CCtCode, Concat(B.CCtName, ' (', IfNull(C.CCName, '-'), ')') As CCtName, A.Amt, B.AcNo, D.AcDesc ");
            SQL.AppendLine("From TblCashAdvanceSettlementDtl2 A ");
            SQL.AppendLine("Left Join TblCostCategory B On A.CCtCode=B.CCtCode ");
            SQL.AppendLine("Left Join TblCostCenter C On B.CCCode=C.CCCode ");
            SQL.AppendLine("Left Join TblCOA D On B.AcNo = D.AcNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo; ");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
                    ref Grd2, ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        "CCtCode", 
                        "CCtName", "Amt", "AcNo", "AcDesc"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 4);
                    }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 2 });
            Sm.FocusGrd(Grd2, 0, 0);
        }*/

        /*private void ShowCashAdvanceSettlementDtl3(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select VoucherDocNo, CurCode, Amt1, Amt2 ");
            SQL.AppendLine("From TblCashAdvanceSettlementDtl3 ");
            SQL.AppendLine("Where DocNo=@DocNo Order By DNo; ");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
                    ref Grd3, ref cm, SQL.ToString(),
                    new string[] { "VoucherDocNo", "CurCode", "Amt1", "Amt2" },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                    }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd3, Grd3.Rows.Count - 1, new int[] { 3, 4 });
            Sm.FocusGrd(Grd3, 0, 0);
        }*/

        /*private void ShowCashAdvanceSettlementDtl4(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT A.DNo, A.Description, A.Amt ");
            if (mIsCASUseBudgetCategory)
                SQL.AppendLine(", B.OptDesc Type, A.TypeCode ");
            else
                SQL.AppendLine(", NULL As Type , NULL as TypeCode");
            SQL.AppendLine("From tblcashadvancesettlementdtl4 A ");
            if (mIsCASUseBudgetCategory)
                SQL.AppendLine("INNER JOIN tbloption B ON A.TypeCode = B.OptCode AND B.OptCat='CASDetailsType' ");
            SQL.AppendLine("Where DocNo=@DocNo Order By DNo ");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInGrid(
                    ref Grd4, ref cm, SQL.ToString(),
                    new string[] { "DNo", "Description", "Amt", "Type", "TypeCode" },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 2, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 4);
                    }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd4, Grd4.Rows.Count - 1, new int[] { 2 });
            Sm.FocusGrd(Grd4, 0, 0);
        }*/

        #endregion

        #region Additional Method

        private void SetLuePICCode(ref DXE.LookUpEdit Lue, string PIC)
        {
            try
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select UserCode As Col1, UserName As Col2 ");
                SQL.AppendLine("From TblUser ");
                //if (mIsPICGrouped)
                if(PIC.Length != 0)
                    SQL.AppendLine("where UserCode=@PIC ");
                else
                    SQL.AppendLine("where UserCode=@UserCode ");
                SQL.AppendLine("Order By UserName; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@PIC", PIC);

                Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }
        
        private void SetLueCurCode(ref DXE.LookUpEdit Lue, string CurCode)
        {
            try
            {
                var SQL = new StringBuilder();
                //string CurCodeBank = string.Empty;
                if (Sm.GetLue(LueBankAcCode).Length > 0)
                {
                    CurCode = Sm.GetValue("Select CurCode From Tblbankaccount " +
                        "Where BankAcCode = @Param ", Sm.GetLue(LueBankAcCode));
                }

                SQL.AppendLine("Select CurCode As Col1, CurName As Col2 ");
                SQL.AppendLine("From TblCurrency ");
                //if (mIsPICGrouped)
                    SQL.AppendLine("where CurCode=@CurCode ");
                SQL.AppendLine("Order By CurName; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };

                Sm.CmParam<String>(ref cm, "@CurCode", CurCode);

                Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
                if (CurCode.Length != 0) Sm.SetLue(Lue, CurCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        internal void GetCostCatgegory()
        {
            for(int r = 0; r < Grd1.Rows.Count - 1; r++)
            {
                SetCostCategory(r);
            }
        }

        internal string GetCCCode2()
        {
            string mCCCode = Sm.GetLue(LueCCCode);
            return mCCCode;
        }

        private void SetCostCategory(int r)
        {
            string CCCode = Sm.GetLue(LueCCCode);
            string ItCode = Sm.GetGrdStr(Grd1, r, 1);
            //for (int r = 0; r < Grd1.Rows.Count - 1; r++)
            //{
            //    CostCategoryCode = Sm.GetValue("Select  " +
            //        "From tblcostcategory A " +
            //        "Inner Join tblitemcostcategory B On A.CCtCode = B.CCtCode And A.CCCode = B.CCCode" +
            //        "Where B.CCCode = @Param1 And B.ItCode = @Param2;", Sm.GetLue(LueCCCode), Sm.GetGrdStr(Grd1, r, 1), string.Empty);

            //    Grd1.Cells[r, 6].Value = Cost;
            //}

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.CCtCode, A.CCtName ");
            SQL.AppendLine("From tblcostcategory A ");
            SQL.AppendLine("Inner Join tblitemcostcategory B On A.CCtCode = B.CCtCode And A.CCCode = B.CCCode ");
            SQL.AppendLine("Where B.CCCode = @CCCode And B.ItCode = @ItCode; "); //

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@CCCode", CCCode);
                Sm.CmParam<String>(ref cm, "@ItCode", ItCode);

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "CCtCode", "CCtName" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        Sm.SetGrdValue("S", Grd1, dr, c, r, 9, 0);
                        Sm.SetGrdValue("S", Grd1, dr, c, r, 10, 1);
                    }
                }

                dr.Close();
            }
        }

        private string GetCCCodeFromDept()
        {
            string CCCode = string.Empty;
            var SQL = new StringBuilder();

            SQL.AppendLine("Select CCCode ");
            SQL.AppendLine("From TblCostCenter ");
            SQL.AppendLine("Where DeptCode Is Not Null ");
            SQL.AppendLine("And DeptCode = @Param ");
            SQL.AppendLine("Limit 1; ");

            CCCode = Sm.GetValue(SQL.ToString(), mDeptCode);

            return CCCode;
        }

        internal void GetCCCode()
        {
            if (mIsPCDUseCostCenter)
            {
                string CCCode = Sm.GetLue(LueCCCode);
                string DeptCode = string.Empty;

                for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
                {
                    if (DeptCode.Length > 0) DeptCode += ",";
                    DeptCode += Sm.GetGrdStr(Grd1, i, 10);
                }

                if (DeptCode.Length > 0)
                {
                    var SQL = new StringBuilder();

                    SQL.AppendLine("Select Group_Concat(Distinct IfNull(CCCode, '')) CCCode ");
                    SQL.AppendLine("From TblCostCenter ");
                    SQL.AppendLine("Where ActInd = 'Y' ");
                    SQL.AppendLine("And Find_IN_Set(DeptCode, @Param) ");

                    if (Sm.GetValue(SQL.ToString(), DeptCode) != CCCode)
                    {
                        SetLueCCCode(ref LueCCCode);
                    }
                }
            }
        }

        private void SetLueCCCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();
            string DeptCode = string.Empty;

            if (Grd1.Rows.Count > 1)
                DeptCode = Sm.GetGrdStr(Grd1, 0, 10);

            SQL.AppendLine("Select A.CCCode Col1, A.CCName Col2 ");
            SQL.AppendLine("From TblCostCenter A ");
            SQL.AppendLine("Where A.ActInd = 'Y' ");
            if (mIsFilterByCC)
            {
                SQL.AppendLine("And Exists ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select 1 ");
                SQL.AppendLine("    From TblGroupCostCenter T ");
                SQL.AppendLine("    Where T.CCCode = A.CCCode ");
                SQL.AppendLine("    And GrpCode In ");
                SQL.AppendLine("    ( ");
                SQL.AppendLine("        Select GrpCode ");
                SQL.AppendLine("        From TblUser ");
                SQL.AppendLine("        Where UserCode = @UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            //SQL.AppendLine("And A.DeptCode = @DeptCode ");
            SQL.AppendLine("Order By A.CCName; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DeptCode", DeptCode);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private string GetProfitCenterCode()
        {
            var Value = Sm.GetLue(LueCCCode);
            if (Value.Length == 0) return string.Empty;

            return
                Sm.GetValue(
                    "Select ProfitCenterCode From TblCostCenter " +
                    "Where ProfitCenterCode Is Not Null And CCCode=@Param;",
                    Value);
        }
        
        private decimal GetBalancePettyCash()
        {
            var cm = new MySqlCommand();
            string BalancePettyCash = string.Empty;
            string Filter = string.Empty;
            var SQL = new StringBuilder();
            string DocDt = Sm.GetDte(DteDocDt);
            string Month = Sm.Right(Sm.Left(DocDt, 6), 2);
            string Year = Sm.Left(DocDt, 4);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@Dt", Sm.GetDte(DteDocDt));
            Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueBankAcCode), "BankAcCode", true);

            #region test belum sesuai
            /*
            SQL.AppendLine("Select Balanced, BankAcCode From ( ");
            SQL.AppendLine("    Select Distinct T.DocDt, T.DocNo, T.BankAcCode, if(@prev != T.BankAcCode, @bal:=((T.Opening+T.Debit)-T.Credit), @bal:= @bal+(Debit-Credit)) As Balanced, ");
            SQL.AppendLine("    @prev:=T.BankAcCode ");
            SQL.AppendLine("    From ( ");
            SQL.AppendLine("        Select A.DocNo, A.DocDt, A.BankAcCode, B.DNo, Right(A.DocDt, 2) VcDate, Substring(A.DocDt, 5, 2) As VcMonth, Left(A.DocDt, 4) As VcYear, ");
            SQL.AppendLine("        B.Description, A.PIC, A.CashTypeCode, A.CashTypeCode2, 0.00 As Debit, B.Amt As Credit, 0.00 As Opening, A.CreateDt  ");
            SQL.AppendLine("        From TblVoucherHdr A ");
            SQL.AppendLine("        Inner Join TblVoucherDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("        Inner Join TblBankAccount C On A.BankAcCode = C.BankAcCode ");
            SQL.AppendLine("        Inner Join TblParameter D On D.ParCode = 'MainCurCode'  ");
            SQL.AppendLine("        Where Substring(A.DocDt, 5, 2) = '" + Month + "'  ");
            SQL.AppendLine("        And Left(A.DocDt, 4)='" + Year + "' And A.CancelInd='N' And A.BankAcCode Is Not Null And A.AcType='C' And A.BankAcCode Is Not Null ");
            SQL.AppendLine("        And Left(A.DocDt, 4)='" + Year + "' And A.CancelInd='N' And A.BankAcCode Is Not Null And A.AcType='C' And A.BankAcCode Is Not Null ");
            SQL.AppendLine("        And Exists ( ");
            SQL.AppendLine("            Select 1 From TblGroupBankAccount ");
            SQL.AppendLine("            Where BankAcCode=IfNull(A.BankAcCode, '') ");
            SQL.AppendLine("            And GrpCode In ( ");
            SQL.AppendLine("                Select GrpCode From TblUser ");
            SQL.AppendLine("                Where UserCode= @UserCode ) ) ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select A.DocNo, A.DocDt, A.BankAcCode, B.DNo, Right(A.DocDt, 2) VcDate, Substring(A.DocDt, 5, 2) As VcMonth, Left(A.DocDt, 4) As VcYear, ");
            SQL.AppendLine("        B.Description, A.PIC, A.CashTypeCode, A.CashTypeCode2, B.Amt As Debit, 0.00 As Credit, 0.00 As Opening, A.CreateDt ");
            SQL.AppendLine("        From TblVoucherHdr A ");
            SQL.AppendLine("        Inner Join TblVoucherDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("        Inner Join TblBankAccount C On A.BankAcCode = C.BankAcCode ");
            SQL.AppendLine("        Inner Join TblParameter D On D.ParCode = 'MainCurCode' ");
            SQL.AppendLine("        Where Substring(A.DocDt, 5, 2) = '" + Month + "' And Left(A.DocDt, 4)='" + Year + "' And A.CancelInd='N' ");
            SQL.AppendLine("        And A.BankAcCode Is Not Null ");
            SQL.AppendLine("        And A.AcType='D' And A.BankAcCode Is Not Null ");
            SQL.AppendLine("        And Exists ( ");
            SQL.AppendLine("            Select 1 From TblGroupBankAccount ");
            SQL.AppendLine("            Where BankAcCode=IfNull(A.BankAcCode, '') ");
            SQL.AppendLine("            And GrpCode In ( ");
            SQL.AppendLine("                Select GrpCode From TblUser ");
            SQL.AppendLine("                Where UserCode= @UserCode ) ) ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select A.DocNo, A.DocDt, A.BankAcCode2 BankAcCode, B.DNo, Right(A.DocDt, 2) As VcDate, Substring(A.DocDt, 5, 2) As VcMonth, Left(A.DocDt, 4) As VcYear, ");
            SQL.AppendLine("        B.Description, A.PIC, A.CashTypeCode, A.CashTypeCode2, 0.00 As Debit, (B.Amt*A.ExcRate) As Credit, 0.00 As Opening, A.CreateDt ");
            SQL.AppendLine("        From TblVoucherHdr A ");
            SQL.AppendLine("        Inner Join TblVoucherDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("        Inner Join TblBankAccount C On A.BankAcCode = C.BankAcCode ");
            SQL.AppendLine("        Inner Join TblParameter D On D.ParCode = 'MainCurCode' ");
            SQL.AppendLine("        Where Substring(A.DocDt, 5, 2) = '" + Month + "' And Left(A.DocDt, 4) = '" + Year + "' And A.CancelInd='N' ");
            SQL.AppendLine("        And A.Actype2 Is Not Null And A.Actype2='C' And A.BankAcCode2 Is Not Null ");
            SQL.AppendLine("        And Exists ( ");
            SQL.AppendLine("            Select 1 From TblGroupBankAccount ");
            SQL.AppendLine("            Where BankAcCode=IfNull(A.BankAcCode2, '') ");
            SQL.AppendLine("            And GrpCode In ( ");
            SQL.AppendLine("                Select GrpCode From TblUser ");
            SQL.AppendLine("                Where UserCode= @UserCode ) ) ");
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select A.DocNo, A.DocDt, A.BankAcCode2 BankAcCode, B.Dno, Right(A.DocDt, 2) As VcDate, Substring(A.DocDt, 5, 2) As VcMonth, Left(A.DocDt, 4) As VcYear, ");
            SQL.AppendLine("        B.Description, A.PIC, A.CashTypeCode, A.CashTypeCode2, (B.Amt*A.ExcRate) As Debit, 0.00 As Credit, 0.00 As Opening, A.CreateDt ");
            SQL.AppendLine("        From TblVoucherHdr A ");
            SQL.AppendLine("        Inner Join TblVoucherDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("        Inner Join TblBankAccount C On A.BankAcCode = C.BankAcCode ");
            SQL.AppendLine("        Inner Join TblParameter D On D.ParCode = 'MainCurCode' ");
            SQL.AppendLine("        Where Substring(A.DocDt, 5, 2) = '" + Month + "' And Left(A.DocDt, 4)='" + Year + "' And A.CancelInd='N' ");
            SQL.AppendLine("        And A.Actype2 Is Not Null And A.Actype2='D' And A.BankAcCode2 Is Not Null ");
            SQL.AppendLine("        And Exists ( ");
            SQL.AppendLine("            Select 1 From TblGroupBankAccount ");
            SQL.AppendLine("            Where BankAcCode=IfNull(A.BankAcCode2, '') ");
            SQL.AppendLine("            And GrpCode In ( ");
            SQL.AppendLine("                Select GrpCode From TblUser ");
            SQL.AppendLine("                Where UserCode= @UserCode ) ) ");
            SQL.AppendLine("      Union All ");
            SQL.AppendLine("      Select A.DocNo, A.DocDt, B.BankAcCode, '001', '01',  " + Month + " As VcMonth,  " + Year + ",  ");
            SQL.AppendLine("      'Opening Balance', null As PIC, D.CashTypeCode, D.CashTypeCode2, 0.00 As Debit, 0.00 As Credit, ifnull(B.Amt, 0) As Opening, ");
            SQL.AppendLine("      concat(" + Year + "," + Month + ",'01','0002') As CreateDt ");
            SQL.AppendLine("      From tblClosingBalanceIncashHdr A  ");
            SQL.AppendLine("      Inner Join TblClosingBalanceIncashDtl B On A.DocNo = B.DocNo And A.CancelInd = 'N' And CancelReason Is Null ");
            SQL.AppendLine("      Inner Join TblVoucherHdr D On B.BankAcCode = D.BankAcCode  ");
            if (mIsFilterByBankAccount)
            {
                SQL.AppendLine("And B.BankAcCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupBankAccount ");
                SQL.AppendLine("    Where BankAcCode=IfNull(B.BankAcCode, '') ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("      Inner Join TblBankAccount C On B.BankAcCode = C.BankAcCode ");
            SQL.AppendLine("      where ");
            if (Month == "01")
            {
                int Yr = Convert.ToInt32(Year) - 1;
                SQL.AppendLine(" A.yr = '" + Yr + "' ");
                SQL.AppendLine(" And A.Mth = '12' ");
            }
            else
            {
                SQL.AppendLine(" A.yr = " + Year + " and A.Mth =   Cast(" + Month + " As Decimal(10,2)) -1 ");
            }
            SQL.AppendLine("Group By B.BankAcCode ");
            SQL.AppendLine("      Order By BankAcCode, VcDate, VcMonth, CreateDt ");
            SQL.AppendLine("    ) T ");
            SQL.AppendLine("    Left Join TblUser T1 On T.Pic = T1.UserCode ");
            SQL.AppendLine("    Inner Join TblBankAccount T2 On T.BankAcCode=T2.BankAcCode ");
            SQL.AppendLine("    Left Join TblEntity T3 On T2.EntCode=T3.EntCode ");
            SQL.AppendLine("    Left Join TblSite T4 On T2.SiteCode=T4.SiteCode ");
            SQL.AppendLine("    Left Join TblCashType T5 On T.CashTypeCode = T5.CashTypeCode ");
            SQL.AppendLine("    Left Join TblCashType T6 On T.CashTypeCode2 = T6.CashTypeCode ");
            SQL.AppendLine("    Inner Join ( ");
            SQL.AppendLine("        Select @bal := 0, @prev:='' ");
            SQL.AppendLine("    ) B On 0=0 ");
            SQL.AppendLine(") Tbl ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("Order BY DocDt DESC, DocNo Desc ");
            SQL.AppendLine("LIMIT 1; ");
            */
            #endregion

            SQL.AppendLine("Select Balanced From ( ");
            SQL.AppendLine("Select Distinct T.DocDt, T2.CurCode, T.ExcRate, T.BankAcCode, Concat(T2.BankAcNm, Case When T2.BankAcNo Is Null Then '' Else Concat('-', T2.BankAcNo) End) As BankAcNm, ");
            SQL.AppendLine("T.DocNo, T.DNo, T.VcYear, T.VcDate, T.VcMonth, T3.EntName, T2.BankAcNo, T4.SiteName, ");
            //if (mIsRptCashBookShowCashTypeInfo)
            //    SQL.AppendLine("T5.CashTypeName, T6.CashTypeName as CashTypeName2, ");
            //else
            //    SQL.AppendLine("NULL As CashTypeName, NULL As CashTypeName2, ");

            SQL.AppendLine("Case T.VCMonth When '01' Then 'January' ");
            SQL.AppendLine("        When '02' Then 'February' ");
            SQL.AppendLine("        When '03' Then 'March'  ");
            SQL.AppendLine("        When '04' Then 'April' ");
            SQL.AppendLine("        When '05' Then 'May' ");
            SQL.AppendLine("        When '06' Then 'June' ");
            SQL.AppendLine("        When '07' Then 'July' ");
            SQL.AppendLine("        When '08' Then 'August' ");
            SQL.AppendLine("        When '09' Then 'September' ");
            SQL.AppendLine("        When '10' Then 'October' ");
            SQL.AppendLine("        When '11' Then 'November' ");
            SQL.AppendLine("        When '12' Then 'December' End As Month, ");
            SQL.AppendLine("T.Description,  ");
            SQL.AppendLine("T.Debit, T.Credit, T.Opening, ");
            SQL.AppendLine("if(@prev != T.BankAcCode, @bal:=((T.Opening+T.Debit)-T.Credit), @bal:= @bal+(Debit-Credit)) As Balanced, ");
            SQL.AppendLine("@prev:=T.BankAcCode, T1.Username, T.CreateDt, T.VoucherRemark  ");
            SQL.AppendLine("From(  ");
            SQL.AppendLine("    Select A.DocDt, A.ExcRate, A.BankAcCode, A.DocNo, B.Dno, Right(A.DocDt, 2) As VcDate, Substring(A.DocDt, 5, 2) As VcMonth, Left(A.DocDt, 4) As VcYear, B.Description, ");
            SQL.AppendLine("    0.00 As Debit, B.Amt As Credit, 0.00 As Opening, A.PIC, A.CreateDt, A.Remark AS VoucherRemark, A.CashTypeCode, A.CashTypeCode2 ");
            SQL.AppendLine("    From TblVoucherHdr A ");
            SQL.AppendLine("    Inner Join TblVoucherDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("    Inner Join TblBankAccount C On A.BankAcCode = C.BankAcCode ");
            //SQL.AppendLine(Query);
            SQL.AppendLine("    Inner Join TblParameter D On D.ParCode = 'MainCurCode' ");
            SQL.AppendLine("    Where Substring(A.DocDt, 5, 2) = " + Month + " ");
            SQL.AppendLine("    And Left(A.DocDt, 4)=" + Year + " ");
            if (mIsRptCashBookUseEndDt)
                SQL.AppendLine("    And Right(A.DocDt, 2) <= @Dt ");
            SQL.AppendLine("    And A.CancelInd='N' ");
            SQL.AppendLine("    And A.BankAcCode Is Not Null ");
            SQL.AppendLine("    And A.AcType='C' ");
            if (mIsFilterByBankAccount)
            {
                SQL.AppendLine("And A.BankAcCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupBankAccount ");
                SQL.AppendLine("    Where BankAcCode=IfNull(A.BankAcCode, '') ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("      Union All ");
            SQL.AppendLine("      Select A.DocDt, A.ExcRate, ");
            SQL.AppendLine("      A.BankAcCode, A.DocNo, B.Dno, Right(A.DocDt, 2) As VcDate, Substring(A.DocDt, 5, 2) As VcMonth, Left(A.DocDt, 4) As VcYear, ");
            SQL.AppendLine("      B.Description, B.Amt As Debit, ");
            SQL.AppendLine("      0.00 As Credit, 0.00 As Opening, A.PIC, A.CreateDt, A.Remark AS VoucherRemark, A.CashTypeCode, A.CashTypeCode2 ");
            SQL.AppendLine("      From TblVoucherHdr A ");
            SQL.AppendLine("      Inner Join TblVoucherDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("      Inner Join TblBankAccount C On A.BankAcCode = C.BankAcCode ");
            //SQL.AppendLine(Query);
            SQL.AppendLine("      Inner Join TblParameter D On D.ParCode = 'MainCurCode' ");
            SQL.AppendLine("      Where Substring(A.DocDt, 5, 2) = " + Month + " And Left(A.DocDt, 4)= " + Year + " And A.CancelInd='N' ");
            if (mIsRptCashBookUseEndDt)
                SQL.AppendLine("      And Right(A.DocDt, 2) <= @Dt ");
            SQL.AppendLine("      And A.BankAcCode Is Not Null ");
            SQL.AppendLine("      And A.AcType='D' ");
            if (mIsFilterByBankAccount)
            {
                SQL.AppendLine("And A.BankAcCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupBankAccount ");
                SQL.AppendLine("    Where BankAcCode=IfNull(A.BankAcCode, '') ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            #region
            /*if (mIsRptCashBookBankAcCode2FilterByBankAccountDisabled)
            {
                SQL.AppendLine("      Union All ");
                SQL.AppendLine("      Select A.DocDt, A.ExcRate, A.BankAcCode2 As BankAcCode, A.DocNo, B.Dno, Right(A.DocDt, 2) As VcDate, Substring(A.DocDt, 5, 2) As VcMonth, Left(A.DocDt, 4) As VcYear, ");
                SQL.AppendLine("      B.Description, 0.00 As Debit, ");
                SQL.AppendLine("      (B.Amt*A.ExcRate) As Credit, ");
                SQL.AppendLine("      0.00 As Opening, A.PIC, A.CreateDt, A.Remark AS VoucherRemark, A.CashTypeCode, A.CashTypeCode2 ");
                SQL.AppendLine("      From TblVoucherHdr A  ");
                SQL.AppendLine("      Inner Join TblVoucherDtl B On A.DocNo = B.DocNo ");
                SQL.AppendLine("      Inner Join TblBankAccount C On A.BankAcCode2=C.BankAcCode ");
                SQL.AppendLine(Query);
                SQL.AppendLine("      Inner Join TblParameter D On D.ParCode = 'MainCurCode' ");
                SQL.AppendLine("      Where Substring(A.DocDt, 5, 2) = @MonthFilter And Left(A.DocDt, 4) = @Year And A.CancelInd='N' ");
                if (mIsRptCashBookUseEndDt)
                    SQL.AppendLine("    And Right(A.DocDt, 2) <= @Dt ");
                SQL.AppendLine("      And A.AcType2 Is Not Null ");
                SQL.AppendLine("      And A.AcType2='C' ");
                if (mIsFilterByBankAccount)
                {
                    SQL.AppendLine("And A.BankAcCode2 Is Not Null ");
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupBankAccount ");
                    SQL.AppendLine("    Where BankAcCode=IfNull(A.BankAcCode2, '') ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode=@UserCode ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine(") ");
                }
                SQL.AppendLine("      Union All ");
                SQL.AppendLine("      Select A.DocDt, A.ExcRate, A.BankAcCode2 As BankAcCode, A.DocNo, B.Dno, Right(A.DocDt, 2) As VcDate, Substring(A.DocDt, 5, 2) As VcMonth, Left(A.DocDt, 4) As VcYear, ");
                SQL.AppendLine("      B.Description, (B.Amt*A.ExcRate) As Debit, ");
                SQL.AppendLine("      0.00 As Credit, 0.00 As Opening, A.PIC, A.CreateDt, A.Remark AS VoucherRemark, A.CashTypeCode, A.CashTypeCode2 ");
                SQL.AppendLine("      From TblVoucherHdr A ");
                SQL.AppendLine("      Inner Join TblVoucherDtl B On A.DocNo = B.DocNo ");
                SQL.AppendLine("      Inner Join TblBankAccount C On A.BankAcCode2=C.BankAcCode ");
                SQL.AppendLine(Query);
                SQL.AppendLine("      Inner Join TblParameter D On D.ParCode = 'MainCurCode' ");
                SQL.AppendLine("      Where Substring(A.DocDt, 5, 2) = @MonthFilter ");
                SQL.AppendLine("      And Left(A.DocDt, 4)=@Year ");
                if (mIsRptCashBookUseEndDt)
                    SQL.AppendLine("      And Right(A.DocDt, 2) <= @Dt ");
                SQL.AppendLine("      And A.CancelInd='N' ");
                SQL.AppendLine("      And A.AcType2 Is Not Null ");
                SQL.AppendLine("      And A.AcType2='D' ");
                if (mIsFilterByBankAccount)
                {
                    SQL.AppendLine("And A.BankAcCode2 Is Not Null ");
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupBankAccount ");
                    SQL.AppendLine("    Where BankAcCode=IfNull(A.BankAcCode2, '') ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode=@UserCode ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine(") ");
                }
            }
            else
            {
                SQL.AppendLine("      Union All ");
                SQL.AppendLine("      Select A.DocDt, A.ExcRate, A.BankAcCode2 As BankAcCode, A.DocNo, B.Dno, Right(A.DocDt, 2) As VcDate, Substring(A.DocDt, 5, 2) As VcMonth, Left(A.DocDt, 4) As VcYear, ");
                SQL.AppendLine("      B.Description, 0.00 As Debit, ");
                SQL.AppendLine("      (B.Amt*A.ExcRate) As Credit, ");
                SQL.AppendLine("      0.00 As Opening, A.PIC, A.CreateDt, A.Remark AS VoucherRemark, A.CashTypeCode, A.CashTypeCode2 ");
                SQL.AppendLine("      From TblVoucherHdr A  ");
                SQL.AppendLine("      Inner Join TblVoucherDtl B On A.DocNo = B.DocNo ");
                SQL.AppendLine("      Inner Join TblBankAccount C On A.BankAcCode = C.BankAcCode ");
                SQL.AppendLine(Query);
                SQL.AppendLine("      Inner Join TblParameter D On D.ParCode = 'MainCurCode' ");
                SQL.AppendLine("      Where Substring(A.DocDt, 5, 2) = @MonthFilter And Left(A.DocDt, 4) = @Year And A.CancelInd='N' ");
                if (mIsRptCashBookUseEndDt)
                    SQL.AppendLine("    And Right(A.DocDt, 2) <= @Dt ");
                SQL.AppendLine("      And A.Actype2 Is Not Null ");
                SQL.AppendLine("      And A.Actype2='C' ");
                if (mIsFilterByBankAccount)
                {
                    SQL.AppendLine("And A.BankAcCode2 Is Not Null ");
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupBankAccount ");
                    SQL.AppendLine("    Where BankAcCode=IfNull(A.BankAcCode2, '') ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode=@UserCode ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine(") ");
                }
                SQL.AppendLine("      Union All ");
                SQL.AppendLine("      Select A.DocDt, A.ExcRate, A.BankAcCode2 As BankAcCode, A.DocNo, B.Dno, Right(A.DocDt, 2) As VcDate, Substring(A.DocDt, 5, 2) As VcMonth, Left(A.DocDt, 4) As VcYear, ");
                SQL.AppendLine("      B.Description, (B.Amt*A.ExcRate) As Debit, ");
                SQL.AppendLine("      0.00 As Credit, 0.00 As Opening, A.PIC, A.CreateDt, A.Remark AS VoucherRemark, A.CashTypeCode, A.CashTypeCode2 ");
                SQL.AppendLine("      From TblVoucherHdr A ");
                SQL.AppendLine("      Inner Join TblVoucherDtl B On A.DocNo = B.DocNo ");
                SQL.AppendLine("      Inner Join TblBankAccount C On A.BankAcCode = C.BankAcCode ");
                SQL.AppendLine(Query);
                SQL.AppendLine("      Inner Join TblParameter D On D.ParCode = 'MainCurCode' ");
                SQL.AppendLine("      Where Substring(A.DocDt, 5, 2) = @MonthFilter ");
                SQL.AppendLine("      And Left(A.DocDt, 4)=@Year ");
                if (mIsRptCashBookUseEndDt)
                    SQL.AppendLine("      And Right(A.DocDt, 2) <= @Dt ");
                SQL.AppendLine("      And A.CancelInd='N' ");
                SQL.AppendLine("      And A.Actype2 Is Not Null ");
                SQL.AppendLine("      And A.Actype2='D' ");
                if (mIsFilterByBankAccount)
                {
                    SQL.AppendLine("And A.BankAcCode2 Is Not Null ");
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupBankAccount ");
                    SQL.AppendLine("    Where BankAcCode=IfNull(A.BankAcCode2, '') ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode=@UserCode ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine(") ");
                }
            }*/
            #endregion
            SQL.AppendLine("      Union All ");
            SQL.AppendLine("      Select A.DocDt, 0.00 As ExcRate, B.BankAcCode, A.DocNo, '001', '01',  @MonthFilter As VcMonth,  @Year,  ");
            SQL.AppendLine("      'Opening Balance', 0.00 As Debit, 0.00 As Credit, ifnull(B.Amt, 0) As Opening, A.Createby, concat(" + Year + "," + Month + ",'01','0002') As CreateDt, ");
            SQL.AppendLine("      A.Remark AS VoucherRemark, D.CashTypeCode, D.CashTypeCode2 ");
            SQL.AppendLine("      From tblClosingBalanceIncashHdr A  ");
            SQL.AppendLine("      Inner Join TblClosingBalanceIncashDtl B On A.DocNo = B.DocNo And A.CancelInd = 'N' And CancelReason Is Null ");
            SQL.AppendLine("      Inner Join TblVoucherHdr D On B.BankAcCode = D.BankAcCode  ");
            if (mIsFilterByBankAccount)
            {
                SQL.AppendLine("And B.BankAcCode Is Not Null ");
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupBankAccount ");
                SQL.AppendLine("    Where BankAcCode=IfNull(B.BankAcCode, '') ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("      Inner Join TblBankAccount C On B.BankAcCode = C.BankAcCode ");
            #region
            /*if (!mIsClosingBalanceInCashBasedOnMultiProfitCenter)
                SQL.AppendLine(Query);
            else
            {
                if (mIsRptCashBookUseProfitCenter)
                {
                    SQL.AppendLine(" And A.ProfitCenterCode Is Not Null ");

                    if (!mIsAllProfitCenterSelected)
                    {
                        var Filter3 = string.Empty;
                        int j = 0;
                        foreach (var x in mlProfitCenter.Distinct())
                        {
                            if (Filter3.Length > 0) Filter3 += " Or ";
                            Filter3 += " (A.ProfitCenterCode=@ProfitCenterA" + j.ToString() + ") ";
                            Sm.CmParam<String>(ref cm, "@ProfitCenterA" + j.ToString(), x);
                            j++;
                        }
                        if (Filter3.Length == 0)
                            SQL.AppendLine("    And 1=0 ");
                        else
                            SQL.AppendLine("    And (" + Filter3 + ") ");
                    }
                    else
                    {
                        if (ChkProfitCenterCode.Checked)
                        {
                            SQL.AppendLine("    And Find_In_Set(A.ProfitCenterCode, @ProfitCenterCode) ");
                            if (ChkProfitCenterCode.Checked)
                                Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                        }
                        else
                        {
                            SQL.AppendLine("    And A.ProfitCenterCode In ( ");
                            SQL.AppendLine("        Select Distinct ProfitCenterCode From TblGroupProfitCenter T ");
                            SQL.AppendLine("        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                            SQL.AppendLine("    ) ");
                        }

                    }
                    SQL.AppendLine("    And A.ProfitCenterCode In (Select ProfitCenterCode From TblProfitCenter Where Level>=@ClosingBalanceInCashProfitCenterLevelToBeValidated)  ");



                    if (!ChkProfitCenterCode.Checked)
                    {
                        SQL.AppendLine("   And (C.SiteCode Is Null Or (C.SiteCode Is Not Null And C.SiteCode In ( ");
                        SQL.AppendLine("        Select Distinct SiteCode From TblGroupSite ");
                        SQL.AppendLine("        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                        SQL.AppendLine("        ))) ");
                    }
                }
            }*/
            #endregion
            SQL.AppendLine("      where ");
            if (Month == "01")
            {
                int Yr = Convert.ToInt32(Year) - 1;
                SQL.AppendLine(" A.yr = '" + Yr + "' ");
                SQL.AppendLine(" And A.Mth = '12' ");
            }
            else
            {
                SQL.AppendLine(" A.yr = " + Year + " and A.Mth =   Cast(" + Month + " As Decimal(10,2)) -1 ");
            }
            SQL.AppendLine("Group By B.BankAcCode ");

            #region
            /*if (mIsClosingBalanceInCashBasedOnMultiProfitCenter)
            {
                SQL.AppendLine("      Union All ");
                SQL.AppendLine("      Select A.DocDt, 0.00 As ExcRate, B.BankAcCode, A.DocNo, '001', '01',  @MonthFilter As VcMonth,  @Year,  ");
                SQL.AppendLine("      'Opening Balance', 0.00 As Debit, 0.00 As Credit, ifnull(B.Amt, 0) As Opening, A.Createby, concat(@year,@MonthFilter,'01','0002') As CreateDt, ");
                SQL.AppendLine("      A.Remark AS VoucherRemark, D.CashTypeCode, D.CashTypeCode2 ");
                SQL.AppendLine("      From tblClosingBalanceIncashHdr A  ");
                SQL.AppendLine("      Inner Join TblClosingBalanceIncashDtl B On A.DocNo = B.DocNo And A.CancelInd = 'N' And CancelReason Is Null ");
                SQL.AppendLine("      Inner Join TblVoucherHdr D On B.BankAcCode = D.BankAcCode2  ");
                if (mIsFilterByBankAccount)
                {
                    SQL.AppendLine("And B.BankAcCode Is Not Null ");
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupBankAccount ");
                    SQL.AppendLine("    Where BankAcCode=IfNull(B.BankAcCode, '') ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode=@UserCode ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine(") ");
                }
                SQL.AppendLine("      Inner Join TblBankAccount C On B.BankAcCode = C.BankAcCode ");
                if (!mIsClosingBalanceInCashBasedOnMultiProfitCenter)
                    SQL.AppendLine(Query);
                else
                {
                    if (mIsRptCashBookUseProfitCenter)
                    {
                        SQL.AppendLine(" And A.ProfitCenterCode Is Not Null ");

                        if (!mIsAllProfitCenterSelected)
                        {
                            var Filter3 = string.Empty;
                            int j = 0;
                            foreach (var x in mlProfitCenter.Distinct())
                            {
                                if (Filter3.Length > 0) Filter3 += " Or ";
                                Filter3 += " (A.ProfitCenterCode=@ProfitCenterB" + j.ToString() + ") ";
                                Sm.CmParam<String>(ref cm, "@ProfitCenterB" + j.ToString(), x);
                                j++;
                            }
                            if (Filter3.Length == 0)
                                SQL.AppendLine("    And 1=0 ");
                            else
                                SQL.AppendLine("    And (" + Filter3 + ") ");
                        }
                        else
                        {
                            if (ChkProfitCenterCode.Checked)
                            {
                                SQL.AppendLine("    And Find_In_Set(A.ProfitCenterCode, @ProfitCenterCode) ");
                                if (ChkProfitCenterCode.Checked)
                                    Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                            }
                            else
                            {
                                SQL.AppendLine("    And A.ProfitCenterCode In ( ");
                                SQL.AppendLine("        Select Distinct ProfitCenterCode From TblGroupProfitCenter T ");
                                SQL.AppendLine("        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                                SQL.AppendLine("    ) ");
                            }

                        }
                        SQL.AppendLine("    And A.ProfitCenterCode In (Select ProfitCenterCode From TblProfitCenter Where Level>=@ClosingBalanceInCashProfitCenterLevelToBeValidated)  ");



                        if (!ChkProfitCenterCode.Checked)
                        {
                            SQL.AppendLine("   And (C.SiteCode Is Null Or (C.SiteCode Is Not Null And C.SiteCode In ( ");
                            SQL.AppendLine("        Select Distinct SiteCode From TblGroupSite ");
                            SQL.AppendLine("        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                            SQL.AppendLine("        ))) ");
                        }
                    }
                }
                SQL.AppendLine("      where ");
                if (Sm.GetLue(LueMth) == "01")
                {
                    int Yr = Convert.ToInt32(Sm.GetLue(LueYr)) - 1;
                    SQL.AppendLine(" A.yr = '" + Yr + "' ");
                    SQL.AppendLine(" And A.Mth = '12' ");
                }
                else
                {
                    SQL.AppendLine(" A.yr = @year and A.Mth =   Cast(@MonthFilter As Decimal(10,2)) -1 ");
                }
                SQL.AppendLine("Group By B.BankAcCode ");
            }*/
            #endregion

            SQL.AppendLine("      Order By BankAcCode, VcDate, VcMonth, CreateDt ");

            SQL.AppendLine(") T ");
            SQL.AppendLine("Left Join TblUser T1 On T.Pic = T1.UserCode ");
            SQL.AppendLine("Inner Join TblBankAccount T2 On T.BankAcCode=T2.BankAcCode ");
            SQL.AppendLine("Left Join TblEntity T3 On T2.EntCode=T3.EntCode ");
            SQL.AppendLine("Left Join TblSite T4 On T2.SiteCode=T4.SiteCode ");
            SQL.AppendLine("Left Join TblCashType T5 On T.CashTypeCode = T5.CashTypeCode ");
            SQL.AppendLine("Left Join TblCashType T6 On T.CashTypeCode2 = T6.CashTypeCode ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("       Select @bal := 0, @prev:='' ");
            SQL.AppendLine(") B On 0=0 ");
            SQL.AppendLine(") Tbl ");
            SQL.AppendLine(Filter);
            //SQL.AppendLine(" Order By BankAcCode, VcDate, DocNo, Dno, VcMonth, CreateDt; ");
            SQL.AppendLine(" Order By DocDt Desc, DocNo Desc  ");
            SQL.AppendLine(" Limit 1; ");

            cm.CommandText = SQL.ToString();

            BalancePettyCash = Sm.GetValue(cm);

            return Sm.GetDecValue(BalancePettyCash);
        }

        internal void ClearItem()
        {
            if (!mIsCASUseItemRate) return;

            if (!mIsCASUseItemRateNotBasedOnCBP)
            {
                for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
                {
                    Grd1.Cells[i, 24].Value = Grd1.Cells[i, 26].Value = null;
                    Grd1.Cells[i, 27].Value = Grd1.Cells[i, 28].Value = 0m;
                    ComputeRateAmount(i);
                }
                ComputeAmt1();
                SetCostCategoryInfo();
                SetVoucherInfo();
                ComputeAmt2();
            }
        }

        internal void CalculateRate(int Row, string ItCode, string Yr, string CCCode)
        {
            decimal Rate = 0m, Qty = 0m, Amt = 0m;

            Qty = Sm.GetGrdDec(Grd1, Row, 27);

            if (!mIsCASUseItemRateNotBasedOnCBP)
                Rate = GetRate(Row, ItCode, Yr, CCCode);

            Amt = Rate * Qty;
            Grd1.Cells[Row, 28].Value = Sm.FormatNum(Rate, 0);
            Grd1.Cells[Row, 6].Value = Sm.FormatNum(Amt, 0);
        }

        private void BtnFile_Click(object sender, EventArgs e)
        {
            try
            {
                ChkFile.Checked = true;
                OD.InitialDirectory = "c:";
                OD.Filter = "PDF files (*.pdf)|*.pdf" +
                    "|rar/zip Files(*.rar;*.zip)|*.RAR;*.ZIP" +
                    "|Image files (*.PNG;*.JPG;*.JPEG)|*.PNG;*.JPG;*.JPEG" +
                    "|Word files (*.doc;*docx)|*.doc;*docx" +
                    "|Excel files (*.xls;*xlsx;*.csv)|*.xls;*xlsx;*.csv" +
                    "|Text files (*.txt)|*.txt" +
                    "|Powerpoint files (*.ppt;*.pptx)|*.ppt;*.pptx";
                OD.FilterIndex = 2;
                OD.ShowDialog();

                TxtFile.Text = OD.FileName;
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void BtnVoucher_Click(object sender, EventArgs e)
        {
            var f = new FrmVoucher(mMenuCode);
            f.Tag = mMenuCode;
            f.WindowState = FormWindowState.Normal;
            f.StartPosition = FormStartPosition.CenterScreen;
            f.mDocNo = TxtVoucherDocNo.Text;
            f.ShowDialog();
        }

        private decimal GetRate(int Row, string ItCode, string Yr, string CCCode)
        {
            decimal rate = 0m;
            string data = string.Empty;
            string Mth = Sm.GetDte(DteDocDt).Substring(4, 2);

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select Rate" + Mth + " As Rate ");
            SQL.AppendLine("From TblCompanyBudgetPlanHdr A ");
            SQL.AppendLine("Inner Join TblCompanyBudgetPlanDtl2 B On A.DocNo = B.DocNo ");
            SQL.AppendLine("    And A.CancelInd = 'N' ");
            SQL.AppendLine("    And A.Yr = @Yr ");
            SQL.AppendLine("    And A.CCCode = @CCCode ");
            SQL.AppendLine("    And A.DocType = '2' ");
            SQL.AppendLine("    And B.ItCode = @ItCode ");
            SQL.AppendLine("Inner Join TblCostCategory C On C.CCtCode = @CCtCode ");
            SQL.AppendLine("    And C.AcNo = B.AcNo ");
            SQL.AppendLine("Limit 1; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@Yr", Yr);
                Sm.CmParam<String>(ref cm, "@CCCode", CCCode);
                Sm.CmParam<String>(ref cm, "@ItCode", ItCode);
                Sm.CmParam<String>(ref cm, "@CCtCode", Sm.GetGrdStr(Grd1, Row, 3));

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "Rate" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        data = Sm.DrStr(dr, c[0]);
                    }
                }

                dr.Close();
            }

            if (data.Length > 0) rate = Decimal.Parse(data);

            return rate;
        }

        private void ComputeRateAmount(int Row)
        {
            decimal Rate = 0m, Qty = 0m, Amt = 0m;

            Qty = Sm.GetGrdDec(Grd1, Row, 27);
            Rate = Sm.GetGrdDec(Grd1, Row, 28);
            Amt = Qty * Rate;

            Grd1.Cells[Row, 6].Value = Sm.FormatNum(Amt, 0);
        }

        private bool IsVoucherCASBA()
        {
            if (!mIsVRBudgetUseCASBA) return false;

            bool mFlag = true;

            for (int i = 0; i < Grd1.Rows.Count; ++i)
            {
                if (Sm.GetGrdStr(Grd1, i, 1).Length > 0)
                {
                    if (Sm.GetGrdStr(Grd1, i, 20) != mVoucherDocTypeCASBA)
                    {
                        mFlag = false;
                        break;
                    }
                }
            }

            return mFlag;
        }

        private void BtnCopyData_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmPettyCashDisbursementDlg2(this));
        }

        private void GetVCDocType()
        {
            string VCDocNo = string.Empty;
            var l = new List<Voucher>();

            for (int i = 0; i < Grd1.Rows.Count; ++i)
            {
                if (Sm.GetGrdStr(Grd1, i, 1).Length > 0)
                {
                    if (VCDocNo.Length > 0) VCDocNo += ",";
                    VCDocNo += Sm.GetGrdStr(Grd1, i, 1);
                }
            }

            if (VCDocNo.Length > 0)
            {
                var SQL = new StringBuilder();
                var cm = new MySqlCommand();

                SQL.AppendLine("Select DocNo, DocType ");
                SQL.AppendLine("From TblVoucherHdr ");
                SQL.AppendLine("Where Find_In_Set(DocNo, @DocNo); ");

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = SQL.ToString();
                    Sm.CmParam<String>(ref cm, "@DocNo", VCDocNo);

                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] { "DocNo","DocType" });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            l.Add(new Voucher()
                            {
                                DocNo = Sm.DrStr(dr, c[0]),
                                DocType = Sm.DrStr(dr, c[1])

                            });
                        }
                    }

                    dr.Close();
                }

                if (l.Count > 0)
                {
                    for (int i = 0; i < Grd1.Rows.Count; ++i)
                    {
                        if(Sm.GetGrdStr(Grd1, i, 1).Length > 0)
                        {
                            foreach(var x in l.Where(w => w.DocNo == Sm.GetGrdStr(Grd1, i, 1)))
                            {
                                Grd1.Cells[i, 20].Value = x.DocType;
                            }
                        }
                    }
                }
            }

            l.Clear();
        }

        private void GetParameter()
        {
            mCASApprovalGroupValidation = Sm.GetParameter("CASApprovalGroupValidation");
            mIsAutoJournalActived = Sm.GetParameterBoo("IsAutoJournalActived");
            mIsVoucherBankAccountFilteredByGrp = Sm.GetParameterBoo("IsVoucherBankAccountFilteredByGrp");
            mBankAccountFormat = Sm.GetParameter("BankAccountFormat");
            mMainCurCode = Sm.GetParameter("MainCurCode");
            mVoucherCodeFormatType = Sm.GetParameter("VoucherCodeFormatType");
            mHostAddrForFTPClient = Sm.GetParameter("HostAddrForFTPClient");
            mSharedFolderForFTPClient = Sm.GetParameter("SharedFolderForFTPClient");
            mUsernameForFTPClient = Sm.GetParameter("UsernameForFTPClient");
            mPasswordForFTPClient = Sm.GetParameter("PasswordForFTPClient");
            mPortForFTPClient = Sm.GetParameter("PortForFTPClient");
            mFileSizeMaxUploadFTPClient = Sm.GetParameter("FileSizeMaxUploadFTPClient");
            mIsSystemUseCostCenter = Sm.GetParameterBoo("IsSystemUseCostCenter");
            mIsCASUseDraftDocument = Sm.GetParameterBoo("IsCASUseDraftDocument");
            mIsPCDAllowToProcessZeroAmount = Sm.GetParameterBoo("IsPCDAllowToProcessZeroAmount");
            mIsCASFromTRProcessAllAmount = Sm.GetParameterBoo("IsCASFromTRProcessAllAmount");
            mIsCostCategoryFilteredInCashAdvanceSettlement = Sm.GetParameterBoo("IsCostCategoryFilteredInCashAdvanceSettlement");
            mCashAdvanceJournalDebitFormat = Sm.GetParameter("CashAdvanceJournalDebitFormat");
            mIsVRBudgetUseCASBA = Sm.GetParameterBoo("IsVRBudgetUseCASBA");
            mVoucherDocTypeCASBA = Sm.GetParameter("VoucherDocTypeCASBA");
            mIsCASShowCOACostCategory = Sm.GetParameterBoo("IsCASShowCOACostCategory");
            mFormatFTPClient = Sm.GetParameter("FormatFTPClient");
            mIsCheckCOAJournalNotExists = Sm.GetParameterBoo("IsCheckCOAJournalNotExists");
            mIsCASUseItemRate = Sm.GetParameterBoo("IsCASUseItemRate");
            mIsPCDUseCostCenter = Sm.GetParameterBoo("IsPCDUseCostCenter");
            mIsFilterByCC = Sm.GetParameterBoo("IsFilterByCC");
            mIsCASCCFilterMandatory = Sm.GetParameterBoo("IsCASCCFilterMandatory");
            mIsVRForBudgetUseSOContract = Sm.GetParameterBoo("IsVRForBudgetUseSOContract");
            mIsVRUseSOContract = Sm.GetParameterBoo("IsVRUseSOContract");

            mIsCASUseItemRateNotBasedOnCBP = Sm.GetParameterBoo("IsCASUseItemRateNotBasedOnCBP");
            mIsCASUseCompletedInd = Sm.GetParameterBoo("IsCASUseCompletedInd");
            mIsBankAccountUseCostCenterAndInterOffice = Sm.GetParameterBoo("IsBankAccountUseCostCenterAndInterOffice");
            mIsCostCategoryCASMandatory = Sm.GetParameterBoo("IsCostCategoryCASMandatory");
            if (mCashAdvanceJournalDebitFormat.Length == 0) mCashAdvanceJournalDebitFormat = "1";
            mIsCASUsePartialAmount = Sm.GetParameterBoo("IsCASUsePartialAmount");

            mBankAccountTypeForPCD = Sm.GetParameter("BankAccountTypeForPCD");
            mCASJournalFormula = Sm.GetParameter("CASJournalFormula");
            mIsPCDAllowMinusAmount = Sm.GetParameterBoo("IsPCDAllowMinusAmount");
            mCASDlgFilterPICDataSource = Sm.GetParameter("CASDlgFilterPICDataSource");
            mVoucherCASJournalAcNoSource = Sm.GetParameter("VoucherCASJournalAcNoSource");
            mIsFilterByDept = Sm.GetParameterBoo("IsFilterByDept");
            mIsCASUseBudgetCategory = Sm.GetParameterBoo("IsCASUseBudgetCategory");
            mCASNotEqualToCostFormula = Sm.GetParameter("CASNotEqualToCostFormula");
            mIsTransactionUseDetailApprovalInformation = Sm.GetParameterBoo("IsTransactionUseDetailApprovalInformation");
            mIsPCDAllowToCopyData = Sm.GetParameterBoo("IsPCDAllowToCopyData");
            mIsFilterByBankAccount = Sm.GetParameterBoo("IsFilterByBankAccount");
            mIsRptCashBookUseEndDt = Sm.GetParameterBoo("IsRptCashBookUseEndDt");
            mIsPCDUseDroppingRequest = Sm.GetParameterBoo("IsPCDUseDroppingRequest");
            mVoucherPCDCCJournalFormat = Sm.GetParameter("VoucherPCDCCJournalFormat");

            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'IsClosingJournalBasedOnMultiProfitCenter','DocNoFormat' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //string
                            case "DocNoFormat": mDocNoFormat = ParValue; break;

                            //boolean
                            case "IsClosingJournalBasedOnMultiProfitCenter": mIsClosingJournalBasedOnMultiProfitCenter = ParValue == "Y"; break;
                        }
                    }
                }
                dr.Close();
            }
            if (mCASJournalFormula.Length == 0) mCASJournalFormula = "1";
            if (mCASDlgFilterPICDataSource.Length == 0) mCASDlgFilterPICDataSource = "1";
            if (mVoucherCASJournalAcNoSource.Length == 0) mVoucherCASJournalAcNoSource = "1";
            if (mCASNotEqualToCostFormula.Length == 0) mCASNotEqualToCostFormula = "1";
            if (mDocNoFormat.Length == 0) mDocNoFormat = "1";
        }

        private void ClearGrd()
        {
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] {  });

            //ClearGrd2();
            //ClearGrd3();

            //Grd4.Rows.Clear();
            //Grd4.Rows.Count = 1;
            //Sm.SetGrdNumValueZero(ref Grd4, 0, new int[] { 2 });

            Sm.ClearGrd(Grd5, false);
        }

        /*private void ClearGrd2()
        {
            Grd2.Rows.Clear();
            Grd2.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd2, 0, new int[] { 2 });
        }*/

        /*private void ClearGrd3()
        {
            Grd3.Rows.Clear();
            Grd3.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd3, 0, new int[] { 3, 4 });
        }*/

        internal void ComputeAmt1()
        {
            decimal Amt = 0m;
            if (Grd1.Rows.Count >= 1)
            {
                for (int r = 0; r < Grd1.Rows.Count; r++)
                    if (Sm.GetGrdStr(Grd1, r, 6).Length > 0)
                        Amt += Sm.GetGrdDec(Grd1, r, 6);
            }
            TxtAmt1.EditValue = Sm.FormatNum(Amt, 0);
        }

        private void BtnDRQDocNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmPettyCashDisbursementDlg3(this));
        }

     

        private bool IsTotalBalanceInvalid()
        {
            decimal DRAmount = Convert.ToDecimal(TxtDRQAmount.Text);
            decimal PCDAmount = Convert.ToDecimal(TxtPCDTotalAmount.Text);
            decimal Balance = DRAmount - PCDAmount;

            if (Balance != 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Dropping Request Amount and PCD's Amount don't have the same amount");
                return true;
            }
                

            return false;
        }

        internal void ComputeBalance()
        {
            decimal DRAmount = Convert.ToDecimal(TxtDRQAmount.Text);
            decimal PCDAmount = Convert.ToDecimal(TxtPCDTotalAmount.Text);
            decimal Balance = DRAmount - PCDAmount;

            TxtBalance.EditValue = Sm.FormatNum(Balance, 0);

        }
        internal void ComputeAmt()
        {
            decimal Amt = 0m;
            if (Grd1.Rows.Count >= 1)
            {
                for (int r = 0; r < Grd1.Rows.Count; r++)
                {
                    Amt = Sm.GetGrdDec(Grd1, r, 4) * Sm.GetGrdDec(Grd1, r, 5);
                    Grd1.Cells[r, 7].Value = Amt;
                }
            }
        }

        private void TxtDRQAmount_EditValueChanged(object sender, EventArgs e)
        {
            
        }

        private void ChkFile_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkFile.Checked == false)
            {
                TxtFile.EditValue = string.Empty;
            }
        }

        private void BtnDRQ_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtDRQDocNo, "Voucher request#", false))
            {
                var f = new FrmDroppingRequest(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtDRQDocNo.Text;
                f.ShowDialog();
            }
        }

        internal void ComputeTotalCost()
        {
            decimal Amt = 0m;
            if (Grd1.Rows.Count > 1)
            {
                for (int r = 0; r < Grd1.Rows.Count - 1; r++)
                {
                    Amt += Sm.GetGrdDec(Grd1, r, 7);
                    //Grd1.Cells[r, 7].Value = Amt;
                }
            }
            TxtAmt1.EditValue = Sm.FormatNum(Amt, 0);
        }

        internal void ComputeAmt2()
        {
            decimal Amt = 0m;
            //if (Grd3.Rows.Count >= 1)
            //{
            //    for (int r = 0; r < Grd3.Rows.Count; r++)
            //        if (Sm.GetGrdStr(Grd3, r, 4).Length > 0)
            //            Amt += Sm.GetGrdDec(Grd3, r, 4);
            //}
            TxtAmt2.EditValue = Sm.FormatNum(Amt, 0);
        }

        private void ComputeAmt3()
        {
            decimal Amt = 0m;
            //if (Grd4.Rows.Count >= 1)
            //{
            //    for (int r = 0; r < Grd4.Rows.Count; r++)
            //        if (Sm.GetGrdStr(Grd4, r, 2).Length > 0)
            //            Amt += Sm.GetGrdDec(Grd4, r, 2);
            //}
            //TxtAmt3.EditValue = Sm.FormatNum(Amt, 0);
        }

        internal void SetCostCategoryInfo()
        {
            //ClearGrd2();
            var CCtCode = string.Empty;
            var IsExisted = false;
            int row = 0;
            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                CCtCode = Sm.GetGrdStr(Grd1, r, 3);
                if (CCtCode.Length > 0)
                {
                    IsExisted = false;
                    //if (Grd2.Rows.Count > 0)
                    //{
                    //    for (int i = 0; i < Grd2.Rows.Count; i++)
                    //    {
                    //        if (Sm.CompareStr(Sm.GetGrdStr(Grd2, i, 0), CCtCode))
                    //        {
                    //            Grd2.Cells[i, 2].Value = Sm.GetGrdDec(Grd2, i, 2) + Sm.GetGrdDec(Grd1, r, 6);
                    //            IsExisted = true;
                    //            break;
                    //        }
                    //    }
                    //}
                    if (!IsExisted)
                    {
                        //row = Grd2.Rows.Count - 1;
                        //Grd2.Cells[row, 0].Value = CCtCode;
                        //Grd2.Cells[row, 1].Value = Sm.GetGrdStr(Grd1, r, 4);
                        //Grd2.Cells[row, 2].Value = Sm.GetGrdDec(Grd1, r, 6);
                        //Grd2.Cells[row, 3].Value = Sm.GetGrdStr(Grd1, r, 21);
                        //Grd2.Cells[row, 4].Value = Sm.GetGrdStr(Grd1, r, 22);
                        //Grd2.Rows.Add();
                    }
                }
            }
        }

        internal void SetVoucherInfo()
        {
            //ClearGrd3();
            var DocNo = string.Empty;
            var IsExisted = false;
            int row = 0;
            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                DocNo = Sm.GetGrdStr(Grd1, r, 1);
                if (DocNo.Length > 0)
                {
                    IsExisted = false;
                   
                }
            }
            ComputeAmt2();
        }

        public static void SetLueCashTypeGrpCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select CashTypeGrpCode As Col1, CashTypeGrpName As Col2 From TblCashTypeGroup ");
            SQL.AppendLine("Order By CashTypeGrpName; ");

            Sm.SetLue2(
                ref Lue,
                SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        public static void SetLueCashTypeCode(ref LookUpEdit Lue, string CashTypeGrpCode, string CashTypeCode)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select CashTypeCode As Col1, CashTypeName As Col2 From TblCashType ");
            if(CashTypeCode.Length > 0)
                SQL.AppendLine("Where CashTypeCode = @CashTypeCode ");
            if (CashTypeGrpCode.Length > 0)
                SQL.AppendLine("Where CashTypeGrpCode = @CashTypeGrpCode And `Level` = (SELECT MAX(`Level`) FROM tblcashtype WHERE CashTypeGrpCode = @CashTypeGrpCode) ");
            SQL.AppendLine("Order By CashTypeName;");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@CashTypeGrpCode", CashTypeGrpCode);
            Sm.CmParam<String>(ref cm, "@CashTypeCode", CashTypeCode);

            Sm.SetLue2(
                ref Lue, ref cm,
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (CashTypeCode.Length != 0) Sm.SetLue(Lue, CashTypeCode);
        }

        internal void SetDeptCode()
        {
            if (!mIsCostCategoryFilteredInCashAdvanceSettlement) return;

            mDeptCode = Sm.GetGrdStr(Grd1, 0, 10);
            //TxtDeptCode.EditValue = Sm.GetGrdStr(Grd1, 0, 11);
        }

        private void SetLueBankAcCode(ref LookUpEdit Lue, string BankAcCode)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.BankAcCode As Col1, ");
            if (mBankAccountFormat == "1")
            {
                SQL.AppendLine("Trim(Concat( ");
                SQL.AppendLine("Case When B.BankName Is Not Null Then Concat(B.BankName, ' ') Else '' End,  ");
                SQL.AppendLine("Case When A.BankAcNo Is Not Null  ");
                SQL.AppendLine("    Then Concat(A.BankAcNo) ");
                SQL.AppendLine("    Else IfNull(A.BankAcNm, '') End, ");
                SQL.AppendLine("Case When A.Remark Is Not Null Then Concat(' ', '(', A.Remark, ')') Else '' End ");
                SQL.AppendLine(")) As Col2 ");
            }
            else if (mBankAccountFormat == "2")
            {
                SQL.AppendLine("Trim(Concat( ");
                SQL.AppendLine("    Case When A.BankAcNo Is Not Null  ");
                SQL.AppendLine("    Then Concat(IfNull(A.BankAcNm, ''), ' : ', A.BankAcNo) ");
                SQL.AppendLine("    Else IfNull(A.BankAcNm, '') End, ");
                SQL.AppendLine("    Case When B.BankName Is Not Null Then Concat(' [', B.BankName, ']') Else '' End ");
                SQL.AppendLine("    )) As Col2 ");
            }
            else
            {
                SQL.AppendLine("Trim(Concat( ");
                SQL.AppendLine("Case When A.BankAcNo Is Not Null  ");
                SQL.AppendLine("    Then Concat(A.BankAcNo, ' [', IfNull(A.BankAcNm, ''), ']') ");
                SQL.AppendLine("    Else IfNull(A.BankAcNm, '') End, ");
                SQL.AppendLine("Case When B.BankName Is Not Null Then Concat(' ', B.BankName) Else '' End ");
                SQL.AppendLine(")) As Col2 ");
            }
            SQL.AppendLine("From TblBankAccount A ");
            SQL.AppendLine("Left Join TblBank B On A.BankCode=B.BankCode ");
            
            if (BankAcCode.Length != 0)
                SQL.AppendLine("Where A.BankAcCode=@BankAcCode ");
            else
            {
                SQL.AppendLine("Where A.ActInd = 'Y' ");
                if (mIsVoucherBankAccountFilteredByGrp)
                {
                    SQL.AppendLine("And Exists( ");
                    SQL.AppendLine("    Select 1 From TblGroupBankAccount ");
                    SQL.AppendLine("    Where BankAcCode=A.BankAcCode ");
                    SQL.AppendLine("    And GrpCode In ( ");
                    SQL.AppendLine("        Select GrpCode From TblUser ");
                    SQL.AppendLine("        Where UserCode=@UserCode ");
                    SQL.AppendLine("    ) ");
                    SQL.AppendLine(") ");
                }
            }

            if (BankAcCode.Length == 0)
            {
                if (mBankAccountTypeForPCD.Length > 0)
                {
                    SQL.AppendLine("And Find_In_set(A.BankAcTp, @BankAccountTypeForPCD) ");
                }
            }

            SQL.AppendLine("Order By A.Sequence;");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@BankAcCode", BankAcCode);
            Sm.CmParam<String>(ref cm, "@BankAccountTypeForPCD", mBankAccountTypeForPCD);

            Sm.SetLue2(
                ref Lue, ref cm,
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (BankAcCode.Length != 0) Sm.SetLue(Lue, BankAcCode);
        }

        internal void SetLueCCtCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.CCtCode As Col1, Concat(A.CCtName, ' (', IfNull(B.CCName, '-'), ')') As Col2 ");
            SQL.AppendLine("From TblCostCategory A ");
            SQL.AppendLine("Left Join TblCostCenter B On A.CCCode=B.CCCode ");
            if (mIsSystemUseCostCenter)
            {
                SQL.AppendLine("Where Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupCostCenter ");
                SQL.AppendLine("    Where CCCode=B.CCCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("Order By A.CCtName, B.CCName; ");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            Sm.SetLue2(
                ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void DownloadFile(string FTPAddress, string port, string filename, string username, string password, string FileShared)
        {
            downloadedData = new byte[0];

            try
            {
                FtpWebRequest request;

                if (mFormatFTPClient == "1")
                {
                    request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + FileShared + "/" + filename) as FtpWebRequest;
                }
                else
                {
                    request = FtpWebRequest.Create("ftp://" + string.Concat(FTPAddress, ":", port) + "/" + filename) as FtpWebRequest;
                }

                //Get the file size first (for progress bar)
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(username, password);
                request.KeepAlive = false;
                request.UseBinary = true;

                int dataLength = (int)request.GetResponse().ContentLength;

                //Streams
                FtpWebResponse response = request.GetResponse() as FtpWebResponse;
                Stream reader = response.GetResponseStream();

                //Download to memory
                MemoryStream memStream = new MemoryStream();
                byte[] buffer = new byte[1024];

                while (true)
                {
                    Application.DoEvents();

                    int bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                    {
                        Application.DoEvents();
                        break;
                    }
                    else
                    {
                        memStream.Write(buffer, 0, bytesRead);
                    }
                }

                downloadedData = memStream.ToArray();

                reader.Close();
                memStream.Close();
                response.Close();

                //MessageBox.Show("Downloaded Successfully");
            }
            catch (Exception)
            {
                Sm.StdMsg(mMsgType.Warning, "There was an error connecting to the FTP Server.");
            }
        }
        
        private void UploadFile(string DocNo)
        {
            FileInfo toUpload = new FileInfo(string.Format(@"{0}", TxtFile.Text));
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}:{1}/{2}/{3}", mHostAddrForFTPClient, mPortForFTPClient, mSharedFolderForFTPClient, toUpload.Name));
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(mUsernameForFTPClient, mPasswordForFTPClient);

            Stream ftpStream = request.GetRequestStream();

            FileStream file = File.OpenRead(string.Format(@"{0}", TxtFile.Text));

            int length = 1024;
            byte[] buffer = new byte[length];
            int bytesRead = 0;

            do
            {
                bytesRead = file.Read(buffer, 0, length);
                ftpStream.Write(buffer, 0, bytesRead);

                PbUpload.Invoke(
                    (MethodInvoker)delegate { PbUpload.Maximum = (int)file.Length; });

                byte[] buffers = new byte[10240];
                int read;
                while ((read = file.Read(buffers, 0, buffers.Length)) > 0)
                {
                    ftpStream.Write(buffers, 0, read);
                    PbUpload.Invoke(
                        (MethodInvoker)delegate
                        {
                            PbUpload.Value = (int)file.Position;
                        });
                }
            }
            while (bytesRead != 0);

            Sm.StdMsg(mMsgType.Info, "File uploaded successfully");

            file.Close();
            ftpStream.Close();

            var cml = new List<MySqlCommand>();
            cml.Add( UpdateFile(DocNo, toUpload.Name));
            Sm.ExecCommands(cml);
        }

        private bool IsTotalCostInvalid()
        {
            decimal TotalCost = Decimal.Parse(TxtAmt1.Text);
            decimal Balance = Decimal.Parse(TxtAmt2.Text);

            if (TotalCost > Balance)
            {
                Sm.StdMsg(mMsgType.Warning, "Total Cost is more than Balance.");
                return true;
            }

            return false;
        }

        private bool IsUploadFileNotValid(int Row, string FileName)
        {
            return
                IsFTPClientDataNotValid(Row, FileName) ||
                IsFileSizeNotvalid(Row, FileName) ||
                IsFileNameAlreadyExisted(Row, FileName)
             ;
        }

        private bool IsFTPClientDataNotValid(int Row, string FileName)
        {

            if (FileName.Length > 0 && mHostAddrForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Host address is empty.");
                return true;
            }

            if (FileName.Length > 0 && mSharedFolderForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder is empty.");
                return true;
            }

            if (FileName.Length > 0 && mUsernameForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for shared folder's username  is empty.");
                return true;
            }

            if (FileName.Length > 0 && mPortForFTPClient.Length <= 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Parameter for Port number is empty.");
                return true;
            }
            return false;
        }

        private bool IsFileSizeNotvalid(int Row, string FileName)
        {
            if (FileName.Length > 0)
            {
                FileInfo f = new FileInfo(FileName);
                long bytes = 0;
                double kilobytes = 0;
                double megabytes = 0;
                double gibabytes = 0;

                if (f.Exists)
                {
                    bytes = f.Length;
                    kilobytes = (double)bytes / 1024;
                    megabytes = kilobytes / 1024;
                    gibabytes = megabytes / 1024;
                }

                if (megabytes > Double.Parse(mFileSizeMaxUploadFTPClient))
                {
                    Sm.StdMsg(mMsgType.Warning, "File too large to Upload in row " + (Row + 1));
                    return true;
                }
            }
            return false;
        }

        private bool IsFileNameAlreadyExisted(int Row, string FileName)
        {
            if (FileName.Length > 0)
            {
                FileInfo toUpload = new FileInfo(string.Format(@"{0}", FileName));

                var SQL = new StringBuilder();

                SQL.AppendLine("Select DocNo From TblCashAdvanceSettlementDtl ");
                SQL.AppendLine("Where FileName=@FileName ");
                SQL.AppendLine("Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@FileName", toUpload.Name);

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "This file (" + toUpload.Name + ") have been used to other document.");
                    return true;
                }
            }
            return false;
        }



        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        private void LueCCtCode_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueDocStatus_EditValueChanged(object sender, EventArgs e)
        {
            //Sm.RefreshLookUpEdit(LueDocStatus, new Sm.RefreshLue2(Sl.SetLueOption), "CASDocumentStatus");
        }

        private void ChkHideInfoInGrd_CheckedChanged(object sender, EventArgs e)
        {
            HideInfoInGrd();
        }

        private void LueCCCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
                Sm.RefreshLookUpEdit(LueCCCode, new Sm.RefreshLue1(SetLueCCCode));
            ClearGrd();
        }

        private void LuePICCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
                Sm.RefreshLookUpEdit(LuePICCode, new Sm.RefreshLue2(SetLuePICCode), string.Empty);
        }

        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
                Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
        }

        private void LueCurCode1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCurCode1, new Sm.RefreshLue1(Sl.SetLueCurCode));
        }

        private void LueCashTypeGrp_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueCashTypeGrp, new Sm.RefreshLue1(SetLueCashTypeGrpCode));
                string CashTypeGrp = Sm.GetLue(LueCashTypeGrp);
                LueCashType.EditValue = "<Refresh>";
                Sm.RefreshLookUpEdit(LueCashType, new Sm.RefreshLue3(SetLueCashTypeCode), CashTypeGrp, "");
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueCashType }, false);
            }
            else
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueCashType }, true);
        }

        private void LueCashType_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueCashType, new Sm.RefreshLue3(SetLueCashTypeCode), Sm.GetLue(LueCashTypeGrp), string.Empty);
            }
        }

        private void LueCashType_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
                Sm.RefreshLookUpEdit(LueCashType, new Sm.RefreshLue3(SetLueCashTypeCode), string.Empty, string.Empty);
        }

        private void LueCurCode2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCurCode2, new Sm.RefreshLue1(Sl.SetLueCurCode));
        }

        private void LueCCCode_Validated(object sender, EventArgs e)
        {
            GetCostCatgegory();
        }

        private void LueBankAcCode_Validated(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueBankAcCode).Length > 0 && BtnSave.Enabled)
            {
                decimal Balance = GetBalancePettyCash();
                TxtAmt2.EditValue = Sm.FormatNum(Balance, 2);
            }
            else
                Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtAmt2 }, 0);

            SetLueCurCode(ref LueCurCode1, string.Empty);
        }

        private void DteDocDt_Validated(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueBankAcCode).Length > 0 && BtnSave.Enabled)
            {
                decimal Balance = GetBalancePettyCash();
                TxtAmt2.EditValue = Sm.FormatNum(Balance, 2);
            }
            else
                Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtAmt2 }, 0);
        }

        private void LueCashTypeGrp_Validated(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueCashTypeGrp).Length > 0)
                SetLueCashTypeCode(ref LueCashType, Sm.GetLue(LueCashTypeGrp), string.Empty);
        }

        #region Grid

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 6 }, e.ColIndex))
            {
                if (e.ColIndex == 6) Sm.LueRequestEdit(ref Grd1, ref LueCurCode2, ref fCell, ref fAccept, e);
            }

            if (BtnSave.Enabled)
            {
                if (Sm.IsGrdColSelected(new int[] { 0, 4, 5, 6, 8 }, e.ColIndex))
                {

                    Sm.GrdRequestEdit(Grd1, e.RowIndex);
                    
                    Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 4, 5, 7 });
                }
            }

            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmVoucher("***");
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
            if (e.ColIndex == 16)
            {
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 14).Length > 0)
                {
                    DownloadFile(mHostAddrForFTPClient, mPortForFTPClient, Sm.GetGrdStr(Grd1, e.RowIndex, 14), mUsernameForFTPClient, mPasswordForFTPClient, mSharedFolderForFTPClient);
                    SFD.FileName = Sm.GetGrdStr(Grd1, e.RowIndex, 14);
                    SFD.DefaultExt = "pdf";
                    SFD.AddExtension = true;

                    if (!Sm.IsGrdValueEmpty(Grd1, e.RowIndex, 14, false, "File name is empty") && downloadedData != null && downloadedData.Length != 0)
                    {
                        if (SFD.ShowDialog() == DialogResult.OK)
                        {
                            Application.DoEvents();

                            //Write the bytes to a file
                            FileStream newFile = new FileStream(SFD.FileName, FileMode.Create);
                            newFile.Write(downloadedData, 0, downloadedData.Length);
                            newFile.Close();
                            Sm.StdMsg(mMsgType.Info, "File Downloaded");
                        }
                    }
                    else
                        Sm.StdMsg(mMsgType.Warning, "No File Downloaded");
                }
            }

            Sm.GrdRequestEdit(Grd1, e.RowIndex);

            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 4, 5, 7 });
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (e.ColIndex == 0)
                {
                    if (mIsPCDUseCostCenter && !Sm.IsLueEmpty(LueCCCode, "Cost Center"))
                        Sm.FormShowDialog(new FrmPettyCashDisbursementDlg(this));
                    else
                        return;
                }
               
                if (e.ColIndex == 15)
                {
                    try
                    {
                        OD.InitialDirectory = "c:";
                        OD.Filter = "Pdf files (*.pdf)|*.pdf|Office Files|*.xls;*.xlsx";
                        OD.Filter = "Image files (*.PNG;*.JPG;*.JPEG)|*.PNG;*.JPG;*.JPEG" +
                                "|PDF files (*.pdf)|*.pdf" +
                                "|Excel files (*.xls;*xlsx;*.csv)|*.xls;*xlsx;*.csv";
                        OD.FilterIndex = 2;
                        OD.ShowDialog();
                        Grd1.Cells[e.RowIndex, 14].Value = OD.FileName;
                    }
                    catch (Exception Exc)
                    {
                        Sm.ShowErrorMsg(Exc);
                    }
                }

                if (mIsCASUseItemRate)
                {

                    if (e.ColIndex == 25 && !Sm.IsGrdValueEmpty(Grd1, e.RowIndex, 24, false, "Item code is empty."))
                    {
                        var f = new FrmItem("***");
                        f.Tag = "***";
                        f.WindowState = FormWindowState.Normal;
                        f.StartPosition = FormStartPosition.CenterScreen;
                        f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 24);
                        f.ShowDialog();
                    }
                }
            }

            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmVoucher("***");
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        private void TxtAmt1_EditValueChanged(object sender, EventArgs e)
        {
            if (TxtAmt1.Text.Length > 0)
                TxtPCDTotalAmount.Text = TxtAmt1.Text;

            if (TxtDRQAmount.Text.Length > 0 && TxtPCDTotalAmount.Text.Length > 0)
            {
                decimal DRAmount = Convert.ToDecimal(TxtDRQAmount.Text);
                decimal PCDAmount = Convert.ToDecimal(TxtPCDTotalAmount.Text);
                decimal Balance = DRAmount - PCDAmount;

                TxtBalance.EditValue = Sm.FormatNum(Balance, 0);
            }
        }

        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
           
            if(e.ColIndex == 4 || e.ColIndex == 5)
            {
                ComputeAmt();
                ComputeTotalCost();
            }
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 4, 5, 7 }, e);
        }

        private void LueBankAcCode_EditValueChanged(object sender, EventArgs e)
        {

        }

        private void DteDocDt_EditValueChanged(object sender, EventArgs e)
        {
            if (mIsCASUseItemRate && IsInsert)
            {
                ClearItem();
            }
        }

        private void LueCurCode_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LueCurCode_Leave(object sender, EventArgs e)
        {
            if (LueCurCode2.Visible && fAccept)
            {
                if (Sm.GetLue(LueCurCode2).Length == 0)
                    Grd1.Cells[fCell.RowIndex, fCell.ColIndex].Value = null;
                else
                    Grd1.Cells[fCell.RowIndex, fCell.ColIndex].Value = Sm.GetLue(LueCurCode2);
                LueCurCode2.Visible = false;
            }
        }

        private void LueRequestEdit(
          iGrid Grd,
          DevExpress.XtraEditors.LookUpEdit Lue,
          ref iGCell fCell,
          ref bool fAccept,
          TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        #endregion

        #endregion

        #endregion

        #region Report Class

        private class Voucher
        {
            public string DocNo { get; set; }
            public string DocType { get; set; }
        }

        class CASHdr
        {
            public string CompanyLogo { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string VoucherRequestDocNo { get; set; }
            public string VoucherRequestDocDt { get; set; }

            public decimal VoucherRequestAmt { get; set; }
            public string VoucherDocNo { get; set; }
            public string VoucherDocNo2 { get; set; }

            public string VoucherDocDt { get; set; }
            public decimal VoucherAmt { get; set; }
            public decimal Amt { get; set; }
            public decimal Amt2 { get; set; }
            public decimal DifferenceAmt { get; set; }



            public string CreateDt { get; set; }
            public string Remark { get; set; }
            public string SiteInd { get; set; }
            public string PrintBy { get; set; }
            public string UserNameDibuat { get; set; }

            public string CurName { get; set; }
            public decimal TotalCost { get; set; }
            public decimal TotalCAS { get; set; }
            public decimal TotalDetail { get; set; }
            public string Description { get; set; }

            public string Terbilang { get; set; }
            public string Verifikator1 { get; set; }
            public string SettledBy { get; set; }
            public string PJ { get; set; }
            public string Department { get; set; }

            public string Description2 { get; set; }
            public string UserCodeDibuat { get; set; }
            public string EmpPict5 { get; set; }
            public string Status { get; set; }
            public string Division { get; set; }
            public string LocalDocNo { get; set; }
            public string Bank { get; set; }

            public string FooterImage { get; set; }

        }

        class CASDtl
        {
            public int nomor { get; set; }
            public string CostCenter { get; set; }
            public string CostCategory { get; set; }
            public string AcNo { get; set; }
            public string Desc { get; set; }
            public decimal Amt { get; set; }
            public decimal VoucherAmt { get; set; }
            public string Description { get; set; }
            public string VoucherDocNo { get; set; }
            public string VoucherRequestDocNo { get; set; }
            public string Remark { get; set; }
            public string BudgetCategory { get; set; }
        }

        class CASJournal
        {
            public string AcNo { get; set; }
            public string AcDesc { get; set; }
            public decimal DAmt { get; set; }
            public decimal CAmt { get; set; }
        }

        class CASSignIMS
        {
            public string UserName { get; set; }
            public string UserName2 { get; set; }
            public string UserName3 { get; set; }
        }

        private class COA
        {
            public string AcNo { get; set; }
        }

        private class CASSignAMKA
        {
            public string EmpPict1 { get; set; }
            public string Description1 { get; set; }
            public string UserName1 { get; set; }
            public string ApproveDt1 { get; set; }
            public string UserCode1 { get; set; }
            public string EmpPict2 { get; set; }
            public string Description2 { get; set; }
            public string UserName2 { get; set; }
            public string ApproveDt2 { get; set; }
            public string UserCode2 { get; set; }
            public string EmpPict3 { get; set; }
            public string Description3 { get; set; }
            public string UserName3 { get; set; }
            public string ApproveDt3 { get; set; }
            public string UserCode3 { get; set; }
            public string EmpPict4 { get; set; }
            public string Description4 { get; set; }
            public string UserName4 { get; set; }
            public string ApproveDt4 { get; set; }
            public string UserCode4 { get; set; }
        }

        private class CASSignSIER
        {
            public string EmpPict1 { get; set; }
            public string Description1 { get; set; }
            public string UserName1 { get; set; }
            public string ApproveDt1 { get; set; }
            public string UserCode1 { get; set; }
            public string PosName1 { get; set; }

        }

        #endregion 
    }
}
