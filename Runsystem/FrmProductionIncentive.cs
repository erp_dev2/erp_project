﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmProductionIncentive : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "";
        internal FrmProductionIncentiveFind FrmFind;

        #endregion

        #region Constructor

        public FrmProductionIncentive(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Insentif Production";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

                SetGrd();
                SetFormControl(mState.View);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grid 1

            Grd1.Cols.Count = 29;
            Grd1.FrozenArea.ColCount = 6;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "No",
                        
                        //1-5
                        "Employee"+Environment.NewLine+"Code",
                        "",
                        "Employee Old"+Environment.NewLine+"Code",
                        "Employee"+Environment.NewLine+"Name",
                        "Department",
                        //6-10
                        "Position",
                        "FH",
                        "FP",
                        "FK",
                        "Permission",
                        //11-15
                        "FSP",
                        "FPD",
                        "FF",
                        "MAR",
                        "Insentif",
                        //16-20
                        "CS",
                        "CT",
                        "SKT",
                        "STB",
                        "KD",
                        //21-25
                        "TTL Hari",
                        "%",
                        "XPROP (%)",
                        "Insentif Final",
                        "",
                        //26-27
                        "SK",
                        "SK DNo",
                        "Payrun Code"
                    },
                     new int[] 
                    {
                        //0
                        40,
                        //1-5
                        100, 20, 100,  150, 120,  
                        //6-10
                        120, 80, 80, 80, 80, 
                        //11-15
                        80, 80, 80, 80, 80, 
                        //16-20
                        80, 80, 80, 80, 80, 
                        //21-25
                        80, 80, 80, 80, 80, 
                        //26
                        150, 20, 150
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 2 });
            Sm.GrdFormatDec(Grd1, new int[] { 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 3, 25, 26, 27 }, false);
            Grd1.Cols[28].Move(7);
            #endregion

        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 2, 3 }, !ChkHideInfoInGrd.Checked);
        }


        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                 TxtDocNo, DteDocDt, DteStartDt, DteEndDt, LueSiteCode, MeeRemark
            });
            ClearGrd();
            ChkCancelInd.Checked = false;
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, DteStartDt, DteEndDt, LueSiteCode, MeeRemark
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28 });
                    ChkCancelInd.Properties.ReadOnly = true;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, DteStartDt, DteEndDt, LueSiteCode, MeeRemark
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 8, 9, 10, 11, 12, 13, 14, 22 });
                    ChkCancelInd.Properties.ReadOnly = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    DteDocDt.Focus();
                    ChkCancelInd.Properties.ReadOnly = false;
                    break;
                default:
                    break;
            }
        }


        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27 });
        }


        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmProductionIncentiveFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sl.SetLueSiteCode2(ref LueSiteCode, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    InsertData();
                }
                else
                {
                    CancelData();
                }

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmEmployee(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mEmpCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdEnter(Grd1, e);
            Sm.GrdTabInLastCell(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmEmployee(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mEmpCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 7, 8, 9, 11, 12, 13, 14 }, e.ColIndex) )
            {
                ComputeInsentif(e.RowIndex);
                ComputeKD(e.RowIndex);
                ComputeXProp(e.RowIndex);
            }

            if (Sm.IsGrdColSelected(new int[] { 16, 17, 18, 19 }, e.ColIndex) && TxtDocNo.Text.Length == 0)
            {
                ComputeKD(e.RowIndex);
                ComputeXProp(e.RowIndex);
            }
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
           
            if (e.ColIndex == 8)
            {
                if (Sm.GetGrdDec(Grd1, 0, 8) > 0)
                {
                    var FP = Sm.GetGrdDec(Grd1, 0, 8);
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    {
                        if (Sm.GetGrdStr(Grd1, Row, 1).Length != 0) 
                            Grd1.Cells[Row, 8].Value = FP;
                        ComputeInsentif(Row);
                        ComputeKD(Row);
                        ComputeXProp(Row);
                    }
                }
            }
            if (e.ColIndex == 9)
            {
                if (Sm.GetGrdDec(Grd1, 0, 9) > 0)
                {
                    var FK = Sm.GetGrdDec(Grd1, 0, 9);
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    {
                        if (Sm.GetGrdStr(Grd1, Row, 1).Length != 0) 
                            Grd1.Cells[Row, 9].Value = FK;
                        ComputeInsentif(Row);
                        ComputeKD(Row);
                        ComputeXProp(Row);
                    }
                }
            }
            if (e.ColIndex == 11)
            {
                if (Sm.GetGrdDec(Grd1, 0, 11) > 0)
                {
                    var FSP = Sm.GetGrdDec(Grd1, 0, 11);
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    {
                        if (Sm.GetGrdStr(Grd1, Row, 1).Length != 0) 
                            Grd1.Cells[Row, 11].Value = FSP;
                        ComputeInsentif(Row);
                        ComputeKD(Row);
                        ComputeXProp(Row);
                    }
                }
            }

            if (e.ColIndex == 12)
            {
                if (Sm.GetGrdDec(Grd1, 0, 12) > 0)
                {
                    var FPD = Sm.GetGrdDec(Grd1, 0, 12);
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    {
                        if (Sm.GetGrdStr(Grd1, Row, 1).Length != 0) 
                            Grd1.Cells[Row, 12].Value = FPD;
                        ComputeInsentif(Row);
                        ComputeKD(Row);
                        ComputeXProp(Row);
                    }
                }
            }

            if (e.ColIndex == 13)
            {
                if (Sm.GetGrdDec(Grd1, 0, 13) > 0)
                {
                    var FF = Sm.GetGrdDec(Grd1, 0, 13);
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    {
                        if (Sm.GetGrdStr(Grd1, Row, 1).Length != 0) 
                            Grd1.Cells[Row, 13].Value = FF;
                        ComputeInsentif(Row);
                        ComputeKD(Row);
                        ComputeXProp(Row);
                    }
                }
            }

            if (e.ColIndex == 14)
            {
                if (Sm.GetGrdDec(Grd1, 0, 14) > 0)
                {
                    var MAR = Sm.GetGrdDec(Grd1, 0, 14);
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    {
                        if (Sm.GetGrdStr(Grd1, Row, 1).Length != 0) 
                            Grd1.Cells[Row, 14].Value = MAR;
                        ComputeInsentif(Row);
                        ComputeKD(Row);
                        ComputeXProp(Row);
                    }
                }
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (IsDataNotValid() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "ProductionIncentive", "TblProductionIncentiveHdr");
            var cml = new List<MySqlCommand>();

            cml.Add(SaveInsentifProdHdr(DocNo));

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0) cml.Add(SaveInsentifProdDtl(DocNo, Row));

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsDataNotValid()
        {

            return
                Sm.IsTxtEmpty(DteDocDt, "Document Date", false) ||
                IsGrdEmpty()||
                IsDataAlreadyExist()||
                IsFHValueNotValid()
                ;
        }

        private bool IsFHValueNotValid()
        {
            string Msg = string.Empty;

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                Msg =
                    "Employee Code : " + Sm.GetGrdStr(Grd1, Row, 1) + Environment.NewLine +
                    "Item's Name : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine + Environment.NewLine;

                if (Sm.GetGrdDec(Grd1, Row, 7) <= 0m)
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "FH should be greater than 0.");
                    return true;
                }
            }
            return false;
        }


        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 employee in list.");
                return true;
            }
            return false;
        }

        private bool IsDataAlreadyExist()
        {
           
            string StartDt = Sm.GetDte(DteStartDt).Substring(0, 8);
            string EndDt = Sm.GetDte(DteEndDt).Substring(0, 8);
            int ExistInd = 0;

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                var SQL = new StringBuilder();
               
                SQL.AppendLine("    Select B.EmpCode  ");
		        SQL.AppendLine("    From TblProductionIncentiveHdr A ");
		        SQL.AppendLine("    Inner Join TblProductionIncentiveDtl B On A.DocNo = B.DocNo");
                SQL.AppendLine("    Where B.EmpCode = '" + Sm.GetGrdStr(Grd1, Row, 1) + "' And A.CancelInd = 'N' And ");
                SQL.AppendLine("    ('" + StartDt + "' between A.StartDt And A.EndDt Or '" + EndDt + "' between A.StartDt And A.EndDt ); ");
              
                var cm = new MySqlCommand() { CommandText = SQL.ToString() };

                if (Sm.IsDataExist(cm))
                {
                    Sm.StdMsg(mMsgType.Warning, "employee " + String.Concat(Sm.GetGrdStr(Grd1, Row, 1) + " " + Sm.GetGrdStr(Grd1, Row, 4)) + " for this period has been created .");
                    return true;
                }
            }

            return false;
        }

        private MySqlCommand SaveInsentifProdHdr(string DocNo)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblProductionIncentiveHdr (DocNo, DocDt, CancelInd, StartDt, EndDt, SiteCode, Remark, CreateBy, CreateDt) " +
                    "Values (@DocNo, @DocDt, 'N', @StartDt, @EndDt, @SiteCode, @Remark, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));
            Sm.CmParam<String>(ref cm, "@SiteCode", Sm.GetLue(LueSiteCode));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveInsentifProdDtl(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblProductionIncentiveDtl(DocNo, EmpCode, EmpDecreeDocNo, EmpDecreeDNo, FP, FK, Permission, FSP, FPD, FF, MAR, CS, CT, SKT, STB, Prosen, Amt,  CreateBy, CreateDt) " +
                    "Values(@DocNo, @EmpCode, @EmpDecreeDocNo, @EmpDecreeDNo, @FP, @FK, @Permission, @FSP, @FPD, @FF, @MAR, @CS, @CT, @SKT, @STB, @Prosen, @Amt, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<String>(ref cm, "@EmpDecreeDocNo", Sm.GetGrdStr(Grd1, Row, 26));
            Sm.CmParam<String>(ref cm, "@EmpDecreeDNo", Sm.GetGrdStr(Grd1, Row, 27));
            Sm.CmParam<Decimal>(ref cm, "@FP", Sm.GetGrdDec(Grd1, Row, 8));
            Sm.CmParam<Decimal>(ref cm, "@FK", Sm.GetGrdDec(Grd1, Row, 9));
            Sm.CmParam<Decimal>(ref cm, "@Permission", Sm.GetGrdDec(Grd1, Row, 10)); 
            Sm.CmParam<Decimal>(ref cm, "@FSP", Sm.GetGrdDec(Grd1, Row, 11)); 
            Sm.CmParam<Decimal>(ref cm, "@FPD", Sm.GetGrdDec(Grd1, Row, 12)); 
            Sm.CmParam<Decimal>(ref cm, "@FF", Sm.GetGrdDec(Grd1, Row, 13));
            Sm.CmParam<Decimal>(ref cm, "@MAR", Sm.GetGrdDec(Grd1, Row, 14));
            Sm.CmParam<Decimal>(ref cm, "@CS", Sm.GetGrdDec(Grd1, Row, 16));
            Sm.CmParam<Decimal>(ref cm, "@CT", Sm.GetGrdDec(Grd1, Row, 17));
            Sm.CmParam<Decimal>(ref cm, "@SKT", Sm.GetGrdDec(Grd1, Row, 18));
            Sm.CmParam<Decimal>(ref cm, "@STB", Sm.GetGrdDec(Grd1, Row, 19));
            Sm.CmParam<Decimal>(ref cm, "@Prosen", Sm.GetGrdDec(Grd1, Row, 22));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd1, Row, 24));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Cancel data

        private void CancelData()
        {
            if (IsCancelledDataNotValid() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(CancelProductionIncentiveHdr());
            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsCancelledDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDataCancelledAlready();
        }

        private bool IsDataCancelledAlready()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo From TblProductionIncentiveHdr ");
            SQL.AppendLine("Where CancelInd='Y' And DocNo=@DocNo ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data is already cancelled .");
                return true;
            }

            return false;
        }

        private MySqlCommand CancelProductionIncentiveHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblProductionIncentiveHdr Set ");
            SQL.AppendLine("    CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N' And DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowProductionIncentiveHdr(DocNo);
                ShowProductionIncentiveDtl(DocNo);

            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowProductionIncentiveHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                         ref cm,
                         "Select DocNo, DocDt, CancelInd, StartDt, EndDt, SiteCode, Remark From TblProductionIncentiveHdr Where DocNo=@DocNo ",
                         new string[] { "DocNo", "DocDt", "CancelInd", "StartDt", "EndDt", "SiteCode", "Remark" },
                         (MySqlDataReader dr, int[] c) =>
                         {
                             TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                             Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1])); 
                             ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y") ? true : false;
                             Sm.SetDte(DteStartDt, Sm.DrStr(dr, c[3]));
                             Sm.SetDte(DteEndDt, Sm.DrStr(dr, c[4])); 
                             Sl.SetLueSiteCode2(ref LueSiteCode, Sm.DrStr(dr, c[5]));
                             MeeRemark.EditValue = Sm.DrStr(dr, c[6]);
                         }, true
                     );
        }

        private void ShowProductionIncentiveDtl(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.EmpCode, B.EmpCodeOld, B.EmpName, C.Deptname, D.PosName, ifnull(E.Amt, 0) As FH, A.FP, A.FK, ");
            SQL.AppendLine("A.Permission, A.FSP, A.FPD, A.FF, A.MAR, A.CS, A.CT, A.SKT, A.STB, A.Prosen, A.Amt, A.EmpDecreeDocNo, A.EmpDecreeDno, A.PayrunCode ");
            SQL.AppendLine("From TblProductionIncentiveDtl A ");
            SQL.AppendLine("Inner Join TblEmployee B On A.EmpCode=B.EmpCode ");
            SQL.AppendLine("inner Join TblDepartment C On B.DeptCode = C.DeptCode ");
            SQL.AppendLine("Left Join TblPosition D On B.PosCode = D.PosCode ");
            SQL.AppendLine("left Join TblEmpDecreeDtl E On A.EmpDecreeDocNo = E.DocNo And A.EmpDecreeDNo=E.Dno And E.ActInd = 'Y'  ");
            SQL.AppendLine("Where A.DocNo=@DocNo order By B.EmpName ");
           
            Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "EmpCode", 
                        //1-5
                        "EmpCodeOld", "EmpName", "Deptname", "PosName", "FH", 
                        //6-10
                        "FP", "FK", "Permission", "FSP", "FPD", 
                        //11-15
                        "FF", "MAR", "CS",  "CT", "SKT", 
                        //16-20
                        "STB", "Prosen", "Amt", "EmpDecreeDocNo", "EmpDecreeDno",
                        //21
                        "PayrunCode"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd1.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 1);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 4);
                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 7, 5);

                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 8, 6);
                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 9, 7);
                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 10, 8);
                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 11, 9);
                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 12, 10);

                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 13, 11);
                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 14, 12);
                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 16, 13);
                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 17, 14);
                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 18, 15);

                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 19, 16);
                        Grd1.Cells[Row, 21].Value = Sm.GetValue("SELECT DATEDIFF('"+Sm.GetDte(DteEndDt).Substring(0, 8)+"','"+Sm.GetDte(DteStartDt).Substring(0, 8)+"')+1");
                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 22, 17);
                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 24, 18);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 26, 19);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 27, 20);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 28, 21);
                        
                    }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25 });
            Sm.FocusGrd(Grd1, 0, 1);
            ComputeAll2();
        }

        public void ShowEmpSite(string SiteCode, string StartDt, string EndDt)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@SiteCode", SiteCode);
            Sm.CmParamDt(ref cm, "@StartDt", StartDt);
            Sm.CmParamDt(ref cm, "@EndDt", EndDt);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.EmpCode, A.EmpCodeOld, A.Empname, C.Deptname, D.PosName, ifnull(B.Amt, 0) As FHAmt, B.DocNo, B.Dno, ifnull(F.WlFactor, '1') As WlFactor, ");
            SQL.AppendLine("ifnull(G.SKT, 0) As Skt, ifnull(H.StandBy, 0) As Stb, ifnull(I.CT, 0) As CT, ifnull(J.CS, 0) As CS ");
            SQL.AppendLine("From tblEmployee A ");
            SQL.AppendLine("left Join TblEmpDecreeDtl B On A.EmpCode = B.EmpCode And B.ActInd = 'Y'  ");  
            SQL.AppendLine("Inner Join TblDepartment C on A.DeptCode = C.DeptCode ");
            SQL.AppendLine("Inner Join TblPosition D On A.PosCode = D.PosCode ");
            SQL.AppendLine("Left Join (");
            SQL.AppendLine("    Select A.EmpCode, ");
            SQL.AppendLine("    (SUBSTRING_INDEX(B.Optdesc,'/',1)/ SUBSTRING_INDEX(B.Optdesc,'/',-1)) As WlFactor ");
            SQL.AppendLine("    From TblEmpWl A ");
            SQL.AppendLine("    Inner Join TblOption B On A.WlCode = B.OptCode And B.OptCat = 'WlFactor'");
            SQL.AppendLine("    Where CancelInd = 'N' And A.DocNo In ");
            SQL.AppendLine("    ( Select Max(DocNo) from tblEMpWl ");
            SQL.AppendLine("    Where ((StartDt between @StartDt And @EndDt) Or (EndDt between @StartDt And @EndDt )) ");
            SQL.AppendLine("    Group By EmpCode ");
            SQL.AppendLine("    ) And  ");
            SQL.AppendLine("        (StartDt between @StartDt And @EndDt ");
            SQL.AppendLine("        Or ");
            SQL.AppendLine("        EndDt between @StartDt And @EndDt )");
            SQL.AppendLine(")F On A.EmpCode = F.EmpCode ");
            
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select EmpCode, SUM(DurationDay) As SKT ");
            SQL.AppendLine("    from tblEmpLeavehdr ");
            SQL.AppendLine("    Where CancelInd = 'N' ");
            SQL.AppendLine("    And Status = 'A' ");
            SQL.AppendLine("    And LeaveCode = '" + Sm.GetParameter("SickLeaveCode") + "' ");
            SQL.AppendLine("    And (StartDt between @StartDt And @EndDt ");
            SQL.AppendLine("    Or ");
            SQL.AppendLine("    EndDt between @StartDt And @EndDt )");
            SQL.AppendLine("    Group By EmpCode ");
            SQL.AppendLine(")G On A.EmpCode = G.EmpCode ");
            
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select EmpCode, SUM(DurationDay) As StandBy ");
            SQL.AppendLine("    from tblEmpLeavehdr ");
            SQL.AppendLine("    Where CancelInd = 'N' ");
            SQL.AppendLine("    And Status = 'A' ");
            SQL.AppendLine("    And LeaveCode = '" + Sm.GetParameter("StandbyLeaveCode") + "' ");
            SQL.AppendLine("    And (StartDt between @StartDt And @EndDt ");
            SQL.AppendLine("    Or ");
            SQL.AppendLine("    EndDt between @StartDt And @EndDt )"); 
            SQL.AppendLine("    Group By EmpCode ");
            SQL.AppendLine(")H On A.EmpCode = H.EmpCode ");

            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("Select EmpCode, SUM(DurationDay) As CT ");
            SQL.AppendLine("from tblEmpLeavehdr ");
            SQL.AppendLine("Where CancelInd = 'N' ");
            SQL.AppendLine("And Status = 'A' ");
            SQL.AppendLine("And LeaveCode = '" + Sm.GetParameter("AnnualLeaveCode") + "' ");
            SQL.AppendLine("    And (StartDt between @StartDt And @EndDt ");
            SQL.AppendLine("    Or ");
            SQL.AppendLine("    EndDt between @StartDt And @EndDt )"); 
            SQL.AppendLine("Group By EmpCode ");
            SQL.AppendLine(")I On A.EmpCode = I.EmpCode ");

            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("Select EmpCode, SUM(DurationDay) As CS ");
            SQL.AppendLine("from tblEmpLeavehdr ");
            SQL.AppendLine("Where CancelInd = 'N' ");
            SQL.AppendLine("And Status = 'A' ");
            SQL.AppendLine("And LeaveCode = '" + Sm.GetParameter("SiteLeaveCode") + "' ");
            SQL.AppendLine("    And (StartDt between @StartDt And @EndDt ");
            SQL.AppendLine("    Or ");
            SQL.AppendLine("    EndDt between @StartDt And @EndDt )"); 
            SQL.AppendLine("Group By EmpCode ");
            SQL.AppendLine(")J On A.EmpCode = J.EmpCode ");

            SQL.AppendLine("Where A.SiteCode=@SiteCode Order By A.EmpName ");
           
            Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "EmpCode",

                        //1-5
                        "EmpCodeOld", "EmpName", "DeptName", "PosName", "FHAmt", 
                        //6-10
                        "DocNo", "DNo", "WLFactor", "CS", "CT",
                        //11-15
                        "SKT", "STB"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd1.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 5);
                        Grd1.Cells[Row, 20].Value = Grd1.Cells[Row, 21].Value = Sm.GetValue("SELECT DATEDIFF('" + Sm.GetDte(DteEndDt).Substring(0, 8) + "','" + Sm.GetDte(DteStartDt).Substring(0, 8) + "')+1");
                        Grd1.Cells[Row, 22].Value = "100";
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 8);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 9);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 10);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 11);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 12);
                        Grd1.Cells[Row, 10].Value = Grd1.Cells[Row, 8].Value = 
                        Grd1.Cells[Row, 9].Value =  Grd1.Cells[Row, 12].Value = 
                        Grd1.Cells[Row, 13].Value = Grd1.Cells[Row, 14].Value = 
                        Grd1.Cells[Row, 15].Value = Grd1.Cells[Row, 23].Value = 
                        Grd1.Cells[Row, 24].Value = Grd1.Cells[Row, 25].Value = 0m;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 27, 7);
                    }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26 });
            Sm.FocusGrd(Grd1, 0, 1);
            ComputeAll2();
        }

        #endregion

        #region Additional Method

        public void ComputeInsentif(int Row)
        {
            try
            {
                decimal INC = 1m;

                for (int Col = 7; Col <= 14; Col++)
                {
                    if (Sm.GetGrdDec(Grd1, Row, Col) > 0 && Col!=10)
                    {
                        INC = INC * Sm.GetGrdDec(Grd1, Row, Col);
                    }
                }
                if (INC > 1)
                {
                    Grd1.Cells[Row, 15].Value = INC;
                }
                else
                {
                    Grd1.Cells[Row, 15].Value = 0m;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }

        }

        private void ComputeKD(int Row)
        {
            try
            {
                decimal CS, CT, SKT, STB, TTL = 0m;
                CS = Sm.GetGrdDec(Grd1, Row, 16);
                CT = Sm.GetGrdDec(Grd1, Row, 17);
                SKT = Sm.GetGrdDec(Grd1, Row, 18);
                STB = Sm.GetGrdDec(Grd1, Row, 19);
                TTL = Sm.GetGrdDec(Grd1, Row, 21);

                Grd1.Cells[Row, 20].Value = TTL - (CS + CT + SKT + STB);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void ComputeXProp(int Row)
        {
            try
            {
                decimal KD, TTL, Prosen = 0m;
                KD = Sm.GetGrdDec(Grd1, Row, 20);
                TTL = Sm.GetGrdDec(Grd1, Row, 21);
                Prosen = Sm.GetGrdDec(Grd1, Row, 22);

                Grd1.Cells[Row, 23].Value = Math.Round((KD / TTL * Prosen), 0);
                Grd1.Cells[Row, 24].Value = Sm.GetGrdDec(Grd1, Row, 15) * (Sm.GetGrdDec(Grd1, Row, 23) / 100);
                Grd1.Cells[Row, 25].Value = Sm.GetGrdDec(Grd1, Row, 15) - Sm.GetGrdDec(Grd1, Row, 24);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void ComputeAll2()
        {
            try
            {
                for (int Row = 0; Row <= Grd1.Rows.Count - 1; Row++)
                {
                    decimal INC = 1m;
                    for (int Col = 7; Col <= 14; Col++)
                    {
                        if (Sm.GetGrdDec(Grd1, Row, Col) > 0 && Col != 10)
                        {
                            INC = INC * Sm.GetGrdDec(Grd1, Row, Col);
                        }
                    }
                    if (INC > 1)
                    {
                        Grd1.Cells[Row, 15].Value = INC;
                    }
                    else
                    {
                        Grd1.Cells[Row, 15].Value = 0m;
                    }
                    Grd1.Cells[Row, 20].Value = Sm.GetGrdDec(Grd1, Row, 21) - (Sm.GetGrdDec(Grd1, Row, 16) + Sm.GetGrdDec(Grd1, Row, 17) + Sm.GetGrdDec(Grd1, Row, 18) + Sm.GetGrdDec(Grd1, Row, 19));
                    if (Sm.GetGrdDec(Grd1, Row, 21) != 0)
                        Grd1.Cells[Row, 23].Value = Math.Round((Sm.GetGrdDec(Grd1, Row, 20) / Sm.GetGrdDec(Grd1, Row, 21) * Sm.GetGrdDec(Grd1, Row, 22)), 0);
                    Grd1.Cells[Row, 25].Value = Sm.GetGrdDec(Grd1, Row, 15) - Sm.GetGrdDec(Grd1, Row, 24);
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }

        }

        #endregion

        #endregion

        #region Event

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue2(Sl.SetLueSiteCode2), string.Empty);

            if (Sm.GetLue(LueSiteCode).Length > 0 && TxtDocNo.Text.Length == 0)
            {
                ShowEmpSite(Sm.GetLue(LueSiteCode), Sm.GetDte(DteStartDt), Sm.GetDte(DteEndDt));
            }
        }
        #endregion
    }
}
