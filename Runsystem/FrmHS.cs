﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmHS : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "";
        internal FrmHSFind FrmFind;

        #endregion

        #region Constructor

        public FrmHS(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetFormControl(mState.View);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();

        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtHSCode, TxtHSName
                    }, true);
                    TxtHSCode.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtHSCode, TxtHSName
                    }, false);
                    TxtHSCode.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtHSName
                    }, false);
                    TxtHSName.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtHSCode, TxtHSName
            });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmHSFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtHSCode, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnDeleteClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtHSCode, "", false) || Sm.StdMsgYN("Delete", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var cm = new MySqlCommand() { CommandText = "Delete From TblHS Where HsCode=@HsCode" };
                Sm.CmParam<String>(ref cm, "@HsCode", TxtHSCode.Text);
                Sm.ExecCommand(cm);

                BtnCancelClick(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (IsDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();

                SQL.AppendLine("Insert Into TblHS(HsCode, HsName, CreateBy, CreateDt) ");
                SQL.AppendLine("Values(@HsCode, @HsName, @UserCode, CurrentDateTime()) ");
                SQL.AppendLine("On Duplicate Key ");
                SQL.AppendLine("   Update HsName=@HsName, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@HsCode", TxtHSCode.Text);
                Sm.CmParam<String>(ref cm, "@HsName", TxtHSName.Text);
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.ExecCommand(cm);

                ShowData(TxtHSCode.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Show Data

        public void ShowData(string HsCode)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@HsCode", HsCode);
                Sm.ShowDataInCtrl(
                        ref cm,
                        "Select * From TblHS Where HsCode = @HsCode",
                        new string[] 
                        {
                            "HsCode", "HsName"
                        },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtHSCode.EditValue = Sm.DrStr(dr, c[0]);
                            TxtHSName.EditValue = Sm.DrStr(dr, c[1]);
                        }, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtHSCode, "HS code", false) ||
                Sm.IsTxtEmpty(TxtHSName, "HS name", false) ||
                IsHsCodeExisted();
        }

        private bool IsHsCodeExisted()
        {
            if (!TxtHSCode.Properties.ReadOnly && Sm.IsDataExist("Select HsCode From TblHS Where HsCode ='" + TxtHSCode.Text + "' Limit 1"))
            {
                Sm.StdMsg(mMsgType.Warning, "HS code ( " + TxtHSCode.Text + " ) already existed.");
                return true;
            }
            return false;
        }

        #endregion

        #endregion

        #region Event

        private void TxtHsCode_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtHSCode);
        }

        private void TxtHsName_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtHSName);
        }

        #endregion
    }
}
