﻿#region Update
/*
    14/06/2022 [BRI/IOK] New Loop */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmWODlg5 : RunSystem.FrmBase4
    {
        #region Field

        private FrmWO mFrmParent;
        private string mSQL = string.Empty, mDocNo = string.Empty, mDNo = string.Empty;

        #endregion

        #region Constructor

        public FrmWODlg5(FrmWO FrmParent, string DocNo, string DNo)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mDocNo = DocNo;
            mDNo = DNo;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                this.Text = "List of History";
                base.FrmLoad(sender, e);
                BtnRefresh.Visible = BtnChoose.Visible = BtnPrint.Visible = BtnExcel.Visible = false;
                panel2.Visible = false;
                SetGrd();
                SetSQL();
                ShowData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 9;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[]
                    {
                        //0
                        "No",
                        
                        //1-5
                        "DocNo",
                        "DNo",
                        "Start"+Environment.NewLine+"Date",
                        "Start"+Environment.NewLine+"Time",
                        "End"+Environment.NewLine+"Date",

                        //6-8
                        "End"+Environment.NewLine+"Time",
                        "Activity",
                        "Last Edited"
                    },
                    new int[]
                    {
                        //0
                        20,

                        //1-5
                        100, 50, 100, 100, 100,

                        //6-8
                        100, 250, 100
                    }
                );
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8 });
            Sm.GrdFormatDate(Grd1, new int[] { 3, 5, 8 });
            Sm.GrdFormatTime(Grd1, new int[] { 4, 6 });
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo, DNo, Dt1, Tm1, Dt2, Tm2, Remark, CreateDt ");
            SQL.AppendLine("From TblWODtl3 ");
            SQL.AppendLine("Where DocNo=@DocNo And DNo=@DNo ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select DocNo, DNo, Dt1, Tm1, Dt2, Tm2, Remark, CreateDt ");
            SQL.AppendLine("From TblWODtl ");
            SQL.AppendLine("Where DocNo=@DocNo And DNo=@DNo ");
            SQL.AppendLine("Order By CreateDt Desc ");

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@DocNo", mDocNo);
                Sm.CmParam<String>(ref cm, "@DNo", mDNo);


                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL,
                        new string[]
                        { 
                             //0
                             "DocNo", 
                             
                             //1-5
                             "DNo",
                             "Dt1",
                             "Tm1",
                             "Dt2",
                             "Tm2",

                             //6-7
                             "Remark",
                             "CreateDt"
                         },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("T2", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("T2", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 8, 7);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {

        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

    }
}
