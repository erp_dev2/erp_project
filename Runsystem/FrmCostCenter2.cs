﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmCostCenter2 : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty;
        //internal Frm FrmFind;

        #endregion

        #region Constructor

        public FrmCostCenter2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                BtnPrint.Visible = false;
                ExecQuery();
                SetFormControl(mState.View);
                Sl.SetLueCCCode(ref LueCostCenterCode);
                Sl.SetLueCCCode(ref LueParent);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            //if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        LueCostCenterCode,TxtCostCenterNew, TxtParent, LueParent
                    }, true);
                    LueCostCenterCode.Focus();
                    ChkCOA.Properties.ReadOnly = true;
                    ChkCostCategory.Properties.ReadOnly = true;
                    ChkItemCostCategory.Properties.ReadOnly = true;
                    BtnCOA.Enabled = false;
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    {  
                        LueCostCenterCode,TxtCostCenterNew, TxtParent, LueParent
                    }, false);
                    LueCostCenterCode.Focus();
                    ChkCOA.Properties.ReadOnly = false;
                    ChkCOA.Checked = true;
                    ChkCostCategory.Properties.ReadOnly = false;
                    ChkCostCategory.Checked = true;
                    ChkItemCostCategory.Properties.ReadOnly = false;
                    ChkItemCostCategory.Checked = true;
                    BtnCOA.Enabled = true;
                    break;
                case mState.Edit:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
               LueCostCenterCode, TxtCostCenterNew, TxtParent, LueParent
            });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            //if (FrmFind == null) FrmFind = new FrmAPSFind(this);
            //Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.Insert);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
           
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                    InsertData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
         
            if (IsInsertedDataNotValid() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(SaveCostCenter());
            if (ChkCostCategory.Checked == true)
            {
                cml.Add(SaveCostCategory());
            }
            if (ChkCOA.Checked == true)
            {
                cml.Add(SaveCOA());
            }
            if (ChkItemCostCategory.Checked == true)
            {
                cml.Add(SaveItemCostCategory());
            }
            
            Sm.ExecCommands(cml);
            Sm.StdMsg(mMsgType.Info, "Data has been saved");
            ClearData();
        }

        private bool IsInsertedDataNotValid()
        {
            return
                 Sm.IsLueEmpty(LueCostCenterCode, "Cost Center Source") ||
                 Sm.IsTxtEmpty(TxtCostCenterNew, "Cost Center New", false) ||
                 Sm.IsTxtEmpty(TxtParent, "Parent COA", false)||
                 IsCCCodeExisted();
        }

        private bool IsCCCodeExisted()
        {
            if (!TxtCostCenterNew.Properties.ReadOnly && Sm.IsDataExist("Select CCName From TblCostCenter Where CCName = '" + TxtCostCenterNew.Text + "' Limit 1"))
            {
                Sm.StdMsg(mMsgType.Warning, "Cost Center Name already existed.");
                return true;
            }
            return false;
        }


        private MySqlCommand SaveCostCenter()
        {
            string CCOldName = Sm.GetValue("Select CCName From TblCostCenter Where CCCode = " + Sm.GetLue(LueCostCenterCode) + " ");

            var cm = new MySqlCommand()
            {
                CommandText =
                "Insert Into TblCostCenter(CCCode, CCName, Parent, CreateBy, CreateDt) "+
                "Values(@CCCode, @CCName, @Parent, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@CCCode", GenerateCCCode());
            Sm.CmParam<String>(ref cm, "@CCName", TxtCostCenterNew.Text);
            Sm.CmParam<String>(ref cm, "@Parent", Sm.GetLue(LueParent));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveCOA()
        {
            string CCOldName = Sm.GetValue("Select CCName From TblCostCenter Where CCCode = "+Sm.GetLue(LueCostCenterCode)+" ");

            var cm = new MySqlCommand()
            {
                CommandText =
                "Insert Into TblCOA (AcNo, AcDesc, Parent, Level, AcType, EntCode, CreateBy, CreateDt) "+
                "Select Concat(@Parent, Right(AcNo, 4)) As AcNo, "+
                "Replace(AcDesc, @CCOld, @Name) As AcDesc,  "+
                "@Parent As Parent,  "+
                "length(Concat(@Parent, Right(AcNo, 4))) - LENGTH(Replace(Concat(@Parent, Right(AcNo, 4)), '.', ''))+1 As Level, AcType, EntCode, @CreateBy, CurrentDateTime() " +
                "From TblCoa Where AcDesc Like '%"+CCOldName+"%'  "+
                "And Level =(Select MAX(Level) From tblCoa Where AcDesc Like '%" + CCOldName + "%' ) "
            };
            Sm.CmParam<String>(ref cm, "@CCOld", CCOldName);
            Sm.CmParam<String>(ref cm, "@Name", TxtCostCenterNew.Text);
            Sm.CmParam<String>(ref cm, "@Parent", TxtParent.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveCostCategory()
        {
            string CCOldName = Sm.GetValue("Select CCName From TblCostCenter Where CCCode = " + Sm.GetLue(LueCostCenterCode) + " ");
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert into tblCostCategory (CCtCode, CCCode, CCtName, AcNo, Indicator, CreateBy, CreateDt ) " +
                    "Select Right(Concat('00000', X.CCtCode) , 5) As CCtCode, @CCCodeNew, "+
                    "Replace(X.CCtname, @CCOld, @Name) As CCtName, "+
                    "Concat(@Parent, right(X.AcNo, 4)) As AcNo, X.Indicator, " +
                    "@CreateBy, CurrentDateTime()   " +
                    "From (  "+
                    "   Select @rownum:=@rownum+1 As CCtCode, CCtName, AcNo, CreateBy, CreateDt, CCtCode As Indicator "+
                    "   From TblCOstCategory "+
                    "   Inner Join ( "+
	                "       Select @rownum := (Select Max(Cast(Trim(CCtCode) As Decimal)) CCtCodeMax From TblCostCategory)  "+
	                "    ) B On 0=0 "+
                    "Where CCCode = '"+Sm.GetLue(LueCostCenterCode)+"')X ; " 
            };
            Sm.CmParam<String>(ref cm, "@CCOld", CCOldName);
            Sm.CmParam<String>(ref cm, "@Name", TxtCostCenterNew.Text);
            Sm.CmParam<String>(ref cm, "@CCCodeNew", GenerateCCCode());
            Sm.CmParam<String>(ref cm, "@Parent", TxtParent.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveItemCostCategory()
        {

            var cm = new MySqlCommand()
            {
                CommandText =
                "Insert Into TblItemCostCategory(ItCode, CCCode, CCtCode, CreateBy, CreateDt) " +
                "Select A.ItCode, @CCCode, B.CCtCode, @CreateBy, CurrentDateTime() " +
                "From TblItemCostCategory A "+
                "Inner Join ( "+
                "   Select CCtCode, Indicator From TblCostCategory Where CCCode=@CCCode "+
                ")B On A.CCtCode=B.Indicator "+
                "Where A.CCCode=@CCCodeSource ;" 
            };
            Sm.CmParam<String>(ref cm, "@CCCode", GenerateCCCode());
            Sm.CmParam<String>(ref cm, "@CCCodeSource", Sm.GetLue(LueCostCenterCode));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            return cm;
        }

        #endregion

        #endregion

        #region Additional Method

        private void ExecQuery()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("ALTER TABLE `tblcostcenter` ");
            SQL.AppendLine("    CHANGE COLUMN `CCName` `CCName` VARCHAR(255) NOT NULL COLLATE 'latin1_swedish_ci' AFTER `CCCode`; ");

            Sm.ExecQuery(SQL.ToString());
        }

        private string GenerateCCCode()
        {
            return Sm.GetValue(
                "Select Right(Concat('00000', CCCodeMax) , 5) As CCCodeTemp " +
                "From (Select Max(Cast(Trim(CCCode) As Decimal))+1 CCCodeMax From TblCostCenter) T;"
                );
        }

        #endregion

        #endregion

        #region Event

        private void BtnCOA_Click(object sender, EventArgs e)
        {
            var f = new FrmCostCenter2Dlg(this);
            f.Tag = mMenuCode;
            f.WindowState = FormWindowState.Normal;
            f.StartPosition = FormStartPosition.CenterScreen;
            f.ShowDialog();
        }

        private void LueCostCenterCode_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCostCenterCode, new Sm.RefreshLue1(Sl.SetLueCCCode));
        }

        private void LueParent_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            Sm.RefreshLookUpEdit(LueParent, new Sm.RefreshLue1(Sl.SetLueCCCode));
        }
        #endregion
    }
}
