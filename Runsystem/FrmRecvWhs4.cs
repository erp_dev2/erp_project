﻿#region Update
/*
    17/07/2017 [TKG] tambah journal antar entity
    15/04/2018 [TKG] ubah proses journal untuk beda entity
    17/04/2018 [TKG] ubah proses journal untuk beda entity menjadi 2 dokumen
    28/05/2018 [WED] tambah LocalDocNo RecvWhs4 dan LocalDocNo DOWhs2
    17/07/2018 [TKG] tambah cost center saat journal
    25/02/2019 [TKG] tambah informasi kawasan berikat
    18/03/2019 [TKG] proses DO scr partial
    29/07/2019 [TKG] bug validasi kalau data sudah pernah diproses
    16/12/2019 [TKG/IMS] journal untuk moving average
    13/05/2020 [WED/IMS] journal khusus IMS berdasarkan parameter IsRecvWhs4ProcessTo1Journal
    11/06/2020 [WED/IMS] ubah COA journal WIP dan remark header diisi nomor dokumen SO Contract
    08/12/2020 [WED/IMS] perubahan journal untuk IMS, yg berdasarkan parameter IsRecvWhs4ProcessTo1Journal
    12/07/2021 [ICA/IMS] Menambah kolom DO To Other Warehouse for Project DocNo
    12/08/2021 [RDA/ALL] validasi coa untuk auto journal menu Receiving From Warehouse (DO With Transfer Request)
    22/10/2021 [TKG/IMS] ubah query journal
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRecvWhs4 : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string
            mMenuCode = string.Empty, mAccessInd = string.Empty,
            mDocNo = string.Empty; //if this application is called from other application
        internal FrmRecvWhs4Find FrmFind;
        internal int mNumberOfInventoryUomCode = 1;
        private string mDocType = "27", mDocType2 = "26",
            mDocTitle = string.Empty;
        internal bool 
            mIsItGrpCodeShow = false,
            mIsRecvWhsWithTRCopyRemarkFromDO = false,
            mIsRecvWhs4ProcessDOWhsPartialEnabled = false;
        private bool 
            mIsAutoJournalActived = false, 
            mIsEntityMandatory = false, 
            mIsMovingAvgEnabled = false, 
            mIsRecvWhs4ProcessTo1Journal = false,
            mIsCheckCOAJournalNotExists = false
            ;
        internal bool mIsKawasanBerikatEnabled = false;

        #endregion

        #region Constructor

        public FrmRecvWhs4(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Received Item From Other Warehouse With Transfer Request";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                if (mIsKawasanBerikatEnabled) Tp2.PageVisible = true;
                SetGrd();
                SetFormControl(mState.View);
                Sl.SetLueWhsCode(ref LueWhsCode2);
                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }
        
        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 32;
            Grd1.FrozenArea.ColCount = 6;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",
                        
                        //1-5
                        "Cancel",
                        "Old Cancel",
                        "",
                        "DO#",
                        "DO#"+Environment.NewLine+"D#",

                        //6-10
                        "",
                        "Item's Code",
                        "",
                        "Item's Name",
                        "Batch#",
                        
                        //11-15
                        "Source",
                        "Lot",
                        "Bin",
                        "DO"+Environment.NewLine+"Quantity",
                        "Quantity",
                        
                        //16-20
                        "UoM",
                        "DO"+Environment.NewLine+"Quantity",
                        "Quantity",
                        "UoM",
                        "DO"+Environment.NewLine+"Quantity",
                        
                        //21-25
                        "Quantity",
                        "UoM",
                        "Remark",
                        "Stock",
                        "Stock",

                        //26-30
                        "Stock",
                        "Group",
                        "Batch Old",
                        "Source Old",
                        "Local#", 

                        //31
                        "DO To Other Warehouse"+Environment.NewLine+"For Project#"
                    },
                     new int[] 
                    {
                        //0
                        20,
 
                        //1-5
                        50, 50, 20, 150, 0, 
                        
                        //6-10
                        20, 100, 20, 200, 150, 
                        
                        //11-15
                        150, 100, 100, 100, 100,
                        
                        //16-20
                        100, 100, 100, 100, 100, 
                        
                        //21-25
                        100, 100, 200, 80, 80,

                        //26-30
                        80, 100, 100, 150, 130,

                        //31
                        200
                    }
                );

            Sm.GrdColCheck(Grd1, new int[] { 1, 2 });
            Sm.GrdFormatDec(Grd1, new int[] { 14, 15, 17, 18, 20, 21, 24, 25, 26 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 3, 6, 8 });
            Sm.GrdColInvisible(Grd1, new int[] { 0, 1, 2, 5, 7, 8, 11, 17, 18, 19, 20, 21, 22, 27, 28, 29 }, false);
            Grd1.Cols[30].Move(6);
            if (mIsItGrpCodeShow)
            {
                Grd1.Cols[27].Visible = true;
                Grd1.Cols[27].Move(7);
            }
            Grd1.Cols[31].Move(5);
            ShowInventoryUomCode();
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 7, 8, 11 }, !ChkHideInfoInGrd.Checked);
        }

        private void ShowInventoryUomCode()
        {
            if (mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 17, 18, 19 }, true);

            if (mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 17, 18, 19, 20, 21, 22 }, true);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, LueWhsCode, LueWhsCode2, MeeRemark, TxtLocalDocNo
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 7, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31 });
                    BtnKBContractNo.Enabled = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueWhsCode, LueWhsCode2, MeeRemark, TxtLocalDocNo
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 3, 10, 15, 18, 21, 23 });
                    BtnKBContractNo.Enabled = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1 });
                    TxtDocNo.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, LueWhsCode, LueWhsCode2, MeeRemark, 
                TxtLocalDocNo, TxtKBContractNo, DteKBContractDt, TxtKBPLNo, DteKBPLDt, 
                TxtKBRegistrationNo, DteKBRegistrationDt
            });
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdBoolValueFalse(ref Grd1, 0, new int[] { 1, 2 });
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 14, 15, 17, 18, 20, 21, 24, 25, 26 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmRecvWhs4Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sl.SetLueWhsCode(ref LueWhsCode, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0) InsertData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    if (e.ColIndex == 3 && !Sm.IsLueEmpty(LueWhsCode, "Warehouse") && !Sm.IsLueEmpty(LueWhsCode2, "Transfer from other warehouse"))
                    {
                        e.DoDefault = false;
                        if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmRecvWhs4Dlg(this, Sm.GetLue(LueWhsCode), Sm.GetLue(LueWhsCode2)));
                    }

                    if (Sm.IsGrdColSelected(new int[] { 3, 15, 18, 21, 23 }, e.ColIndex))
                    {
                        Sm.GrdRequestEdit(Grd1, e.RowIndex);
                        Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 1, 2 });
                        Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 14, 15, 17, 18, 20, 21 });
                    }
                }
                else
                {
                    if (e.ColIndex == 1 && (Sm.GetGrdBool(Grd1, e.RowIndex, 2) || Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length == 0))
                        e.DoDefault = false;
                }
            }

            if (e.ColIndex == 6 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmDOWhs2(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }


            if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 7);
                f.ShowDialog();
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && BtnSave.Enabled && !Sm.IsLueEmpty(LueWhsCode, "Warehouse") && !Sm.IsLueEmpty(LueWhsCode2, "Transfer from other warehouse") && TxtDocNo.Text.Length == 0)
                Sm.FormShowDialog(new FrmRecvWhs4Dlg(this, Sm.GetLue(LueWhsCode), Sm.GetLue(LueWhsCode2)));

            if (e.ColIndex == 6 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                var f = new FrmDOWhs2(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }


            if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 7);
                f.ShowDialog();
            }
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 15, 18, 21 }, e);
            Sm.GrdAfterCommitEditTrimString(Grd1, new int[] { 23 }, e);

            if (e.ColIndex == 15)
            {
                Sm.ComputeQtyBasedOnConvertionFormula("12", Grd1, e.RowIndex, 7, 15, 18, 21, 16, 19, 22);
                Sm.ComputeQtyBasedOnConvertionFormula("13", Grd1, e.RowIndex, 7, 15, 21, 18, 16, 22, 19);
            }

            if (e.ColIndex == 18)
            {
                Sm.ComputeQtyBasedOnConvertionFormula("21", Grd1, e.RowIndex, 7, 18, 15, 21, 19, 16, 22);
                Sm.ComputeQtyBasedOnConvertionFormula("23", Grd1, e.RowIndex, 7, 18, 21, 15, 19, 22, 16);
            }

            if (e.ColIndex == 21)
            {
                Sm.ComputeQtyBasedOnConvertionFormula("31", Grd1, e.RowIndex, 7, 21, 15, 18, 22, 16, 19);
                Sm.ComputeQtyBasedOnConvertionFormula("32", Grd1, e.RowIndex, 7, 21, 18, 15, 22, 19, 16);
            }

            if (e.ColIndex == 15 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 16), Sm.GetGrdStr(Grd1, e.RowIndex, 19)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 18, Grd1, e.RowIndex, 15);

            if (e.ColIndex == 15 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 16), Sm.GetGrdStr(Grd1, e.RowIndex, 22)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 21, Grd1, e.RowIndex, 15);

            if (e.ColIndex == 18 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 19), Sm.GetGrdStr(Grd1, e.RowIndex, 22)))
                Sm.CopyGrdValue(Grd1, e.RowIndex, 21, Grd1, e.RowIndex, 18);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || 
                IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "RecvWhs4", "TblRecvWhs4Hdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveRecvWhs4Hdr(DocNo));
            for (int Row = 0; Row < Grd1.Rows.Count-1; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 4).Length > 0) cml.Add(SaveRecvWhs4Dtl(DocNo, Row));

            cml.Add(SaveStockMovement(DocNo));

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 7).Length > 0) cml.Add(SaveStockSummary(DocNo, Row));
                if (Sm.GetGrdStr(Grd1, Row, 7).Length > 0) cml.Add(SaveStockPrice(DocNo, Row));
            }

            cml.Add(UpdateDOWhsProcessInd(DocNo));
            if (Sm.GetParameter("DocTitle") == "IMS")
            {
                for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
                    cml.Add(UpdateDOWhs4RecvQty(DocNo, i));
            }

            if (mIsRecvWhs4ProcessTo1Journal) //IMS
            {
                if (mIsAutoJournalActived)
                    cml.Add(SaveJournal2(DocNo));
            }
            else
            {
                if (mIsAutoJournalActived && mIsEntityMandatory && IsEntityDifferent())  
                    cml.Add(SaveJournal(DocNo));
            }

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueWhsCode2, "Warehouse (From)") ||
                Sm.IsLueEmpty(LueWhsCode, "Warehouse (To)") ||
                Sm.IsClosingJournalInvalid(Sm.GetDte(DteDocDt)) ||
                IsGrdEmpty() ||
                IsGrdExceedMaxRecords() ||
                IsGrdValueNotValid() ||
                IsJournalSettingInvalid ()
                ;
        }

        private bool IsJournalSettingInvalid() 
        {
            if (!mIsAutoJournalActived || !mIsCheckCOAJournalNotExists) return false;

            var Msg =
                "Journal's setting is invalid." + Environment.NewLine +
                "Please contact Finance/Accounting department." + Environment.NewLine;

            string mAcNoForDOToProductOfWIP = Sm.GetValue("Select ParValue From TblParameter Where Parcode='AcNoForDOToProductOfWIP'");
            string mAcNoForRecvWhs4OfOffsetWIP = Sm.GetValue("Select ParValue From TblParameter Where Parcode='AcNoForRecvWhs4OfOffsetWIP'");

            if (mIsRecvWhs4ProcessTo1Journal) //IMS
            {
                //SaveJournal1
                if (mIsAutoJournalActived)
                {
                    //Parameter
                    if (mAcNoForRecvWhs4OfOffsetWIP.Length == 0)
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Parameter AcNoForRecvWhs4OfOffsetWIP is empty.");
                        return true;
                    }

                    if (mAcNoForDOToProductOfWIP.Length == 0)
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Parameter AcNoForDOToProductOfWIP is empty.");
                        return true;
                    }

                    //Table
                    if (IsJournalSettingInvalid_ItemCategory(Msg, "AcNo")) return true;
                    if (IsJournalSettingInvalid_ItemCategory(Msg, "AcNo2")) return true;
                }
            }
            else 
            {
                //SaveJournal
                if (mIsAutoJournalActived && mIsEntityMandatory && IsEntityDifferent())
                {
                    #region Journal 1

                    if (IsJournalSettingInvalid_ItemCategory(Msg, "AcNo")) return true;

                    if (IsJournalSettingInvalid_Entity(Msg, "AcNo4", Sm.GetLue(LueWhsCode2))) return true;
                    if (IsJournalSettingInvalid_Entity(Msg, "AcNo5", Sm.GetLue(LueWhsCode2))) return true;
                    if (IsJournalSettingInvalid_Entity(Msg, "AcNo6", Sm.GetLue(LueWhsCode2))) return true;
                    
                    #endregion

                    #region Journal 2

                    if (IsJournalSettingInvalid_Entity(Msg, "AcNo3", Sm.GetLue(LueWhsCode))) return true;

                    #endregion
                }
            }
            return false;
        }

        private bool IsJournalSettingInvalid_Entity(string Msg, string COA, string WhsCode)
        {
            if (Sm.GetValue("Select " + COA + " From TblEntity Where EntCode = @Param", Sm.GetEntityWarehouse(WhsCode)).Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, Msg + "Entity Warehouse's COA account# (" + WhsCode + ") is empty.");
                return true;
            }
            else
                return false;
        }

        private bool IsJournalSettingInvalid_ItemCategory(string Msg, string COA)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirst = true;
            string ItCode = string.Empty, ItCtName = string.Empty;

            SQL.AppendLine("Select B.ItCtName From TblItem A, TblItemCategory B ");
            SQL.AppendLine("Where A.ItCtCode=B.ItCtCode And B." + COA + " Is Null ");
            SQL.AppendLine("And A.ItCode In (");
            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                ItCode = Sm.GetGrdStr(Grd1, r, 7);
                if (ItCode.Length > 0)
                {
                    if (IsFirst)
                        IsFirst = false;
                    else
                        SQL.AppendLine(", ");
                    SQL.AppendLine("@ItCode_" + r.ToString());
                    Sm.CmParam<String>(ref cm, "@ItCode_" + r.ToString(), ItCode);
                }
            }
            SQL.AppendLine(") Limit 1;");

            cm.CommandText = SQL.ToString();
            ItCtName = Sm.GetValue(cm);
            if (ItCtName.Length > 0)
            {
                Sm.StdMsg(mMsgType.Warning, Msg + "Item category's COA account" + (COA == "AcNo" ? "(Stock)" : "") + (COA == "AcNo2" ? "(Cost)" : "") + "# (" + ItCtName + ") is empty.");
                return true;
            }
            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 item.");
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {

            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Received data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            string Msg = string.Empty;

            ReComputeStock(Sm.GetLue(LueWhsCode2));
            if (mIsRecvWhs4ProcessDOWhsPartialEnabled) ReComputeDOWhs();

            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, Row, 4, false, "DO# is empty.")) return true;

                Msg =
                    "DO# : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine +
                    "Item's Code : " + Sm.GetGrdStr(Grd1, Row, 7) + Environment.NewLine +
                    "Item's Name : " + Sm.GetGrdStr(Grd1, Row, 9) + Environment.NewLine +
                    "Batch# : " + Sm.GetGrdStr(Grd1, Row, 10) + Environment.NewLine +
                    "Source : " + Sm.GetGrdStr(Grd1, Row, 11) + Environment.NewLine +
                    "Lot : " + Sm.GetGrdStr(Grd1, Row, 12) + Environment.NewLine +
                    "Bin : " + Sm.GetGrdStr(Grd1, Row, 13) + Environment.NewLine + Environment.NewLine;

                if (Sm.GetGrdDec(Grd1, Row, 15) <= 0m)
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Quantity should be greater than 0.");
                    return true;
                }

                if (Sm.GetGrdDec(Grd1, Row, 15) > Sm.GetGrdDec(Grd1, Row, 14))
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Quantity should not be bigger than DO quantity.");
                    return true;
                }

                if (Sm.GetGrdDec(Grd1, Row, 15) > Sm.GetGrdDec(Grd1, Row, 24))
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Quantity should not be bigger than stock.");
                    return true;
                }

                if (Grd1.Cols[18].Visible)
                {
                    if (Sm.GetGrdDec(Grd1, Row, 18) <= 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Quantity (2) should be greater than 0.");
                        return true;
                    }

                    if (Sm.GetGrdDec(Grd1, Row, 18) > Sm.GetGrdDec(Grd1, Row, 17))
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Quantity (2) should not be bigger than DO quantity (2).");
                        return true;
                    }

                    if (Sm.GetGrdDec(Grd1, Row, 18) > Sm.GetGrdDec(Grd1, Row, 25))
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Quantity should not be bigger than stock (2).");
                        return true;
                    }
                }

                if (Grd1.Cols[21].Visible)
                {
                    if (Sm.GetGrdDec(Grd1, Row, 21) <= 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Quantity (3) should be greater than 0.");
                        return true;
                    }

                    if (Sm.GetGrdDec(Grd1, Row, 21) > Sm.GetGrdDec(Grd1, Row, 20))
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Quantity (3) should not be bigger than DO quantity (3).");
                        return true;
                    }

                    if (Sm.GetGrdDec(Grd1, Row, 21) > Sm.GetGrdDec(Grd1, Row, 26))
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Quantity should not be bigger than stock (3).");
                        return true;
                    }
                }
            }
            return false;
        }

        private MySqlCommand SaveRecvWhs4Hdr(string DocNo)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblRecvWhs4Hdr(DocNo, DocDt, LocalDocNo, WhsCode, WhsCode2, KBContractNo, KBContractDt, KBPLNo, KBPLDt, KBRegistrationNo, KBRegistrationDt, Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DocDt, @LocalDocNo, @WhsCode, @WhsCode2, @KBContractNo, @KBContractDt, @KBPLNo, @KBPLDt, @KBRegistrationNo, @KBRegistrationDt, @Remark, @CreateBy, CurrentDateTime());"
            };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@LocalDocNo", TxtLocalDocNo.Text);
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            Sm.CmParam<String>(ref cm, "@WhsCode2", Sm.GetLue(LueWhsCode2));
            Sm.CmParam<String>(ref cm, "@KBContractNo", TxtKBContractNo.Text);
            Sm.CmParamDt(ref cm, "@KBContractDt", Sm.GetDte(DteKBContractDt));
            Sm.CmParam<String>(ref cm, "@KBPLNo", TxtKBPLNo.Text);
            Sm.CmParamDt(ref cm, "@KBPLDt", Sm.GetDte(DteKBPLDt));
            Sm.CmParam<String>(ref cm, "@KBRegistrationNo", TxtKBRegistrationNo.Text);
            Sm.CmParamDt(ref cm, "@KBRegistrationDt", Sm.GetDte(DteKBRegistrationDt));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveRecvWhs4Dtl(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblRecvWhs4Dtl(DocNo, DNo, CancelInd, DOWhsDocNo, DOwhsDNo, BatchNo, Source, Qty, Qty2, Qty3, BatchNoOld, SourceOld, Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DNo, 'N', @DOWhsDocNo, @DOWhsDNo, IfNull(@BatchNo, '-'), @Source, @Qty, @Qty2, @Qty3, IfNull(@BatchNoOld, '-'), @SourceOld, @Remark, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@DOWhsDocNo", Sm.GetGrdStr(Grd1, Row, 4));
            Sm.CmParam<String>(ref cm, "@DOWhsDNo", Sm.GetGrdStr(Grd1, Row, 5));
            if (Sm.GetGrdStr(Grd1, Row, 10) != Sm.GetGrdStr(Grd1, Row, 28))
            {
                Sm.CmParam<String>(ref cm, "@BatchNo", Sm.GetGrdStr(Grd1, Row, 10));
                Sm.CmParam<String>(ref cm, "@Source", string.Concat(mDocType, "*", DocNo, "*", Sm.Right("00" + (Row + 1).ToString(), 3)));
            }
            else
            {
                Sm.CmParam<String>(ref cm, "@BatchNo", Sm.GetGrdStr(Grd1, Row, 28));
                Sm.CmParam<String>(ref cm, "@Source", Sm.GetGrdStr(Grd1, Row, 29));
            }
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 15));
            Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd1, Row, 18));
            Sm.CmParam<Decimal>(ref cm, "@Qty3", Sm.GetGrdDec(Grd1, Row, 21));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 23));
            Sm.CmParam<String>(ref cm, "@BatchNoOld", Sm.GetGrdStr(Grd1, Row, 28));
            Sm.CmParam<String>(ref cm, "@SourceOld", Sm.GetGrdStr(Grd1, Row, 29));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveStockMovement(string DocNo)
        {
            var SQL = new StringBuilder();

            #region movement batch old
            
            SQL.AppendLine("Insert Into TblStockMovement ");
            SQL.AppendLine("(DocType, DocNo, DNo, Source, CancelInd, Source2, ");
            SQL.AppendLine("DocDt, WhsCode, Lot, Bin, ItCode, BatchNo, ");
            SQL.AppendLine("Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocType, A.DocNo, B.DNo, C.Source, 'N', '', ");
            SQL.AppendLine("A.DocDt, A.WhsCode, C.Lot, C.Bin, C.ItCode, C.BatchNo, ");
            SQL.AppendLine("B.Qty, B.Qty2, B.Qty3, ");
            SQL.AppendLine("Case When A.Remark Is Null Then ");
            SQL.AppendLine("    Case When B.Remark Is Null Then Null Else B.Remark End ");
            SQL.AppendLine("Else ");
            SQL.AppendLine("    Case When B.Remark Is Null Then A.Remark Else Concat(A.Remark, ' ( ', B.Remark, ' )') End ");
            SQL.AppendLine("End As Remark, ");
            SQL.AppendLine("@UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblRecvWhs4Hdr A ");
            SQL.AppendLine("Inner Join TblRecvWhs4Dtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblDOWhsDtl C On B.DOWhsDocNo=C.DocNo And B.DOWhsDNo=C.DNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo And B.BatchNo=B.BatchNoOld; ");

            SQL.AppendLine("Insert Into TblStockMovement ");
            SQL.AppendLine("(DocType, DocNo, DNo, Source, CancelInd, Source2, ");
            SQL.AppendLine("DocDt, WhsCode, Lot, Bin, ItCode, BatchNo, ");
            SQL.AppendLine("Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocType2, C.DocNo, C.DNo, C.Source, 'N', '', ");
            SQL.AppendLine("D.DocDt, A.WhsCode2, C.Lot, C.Bin, C.ItCode, C.BatchNo, ");
            SQL.AppendLine("-1*B.Qty, -1*B.Qty2, -1*B.Qty3, ");
            SQL.AppendLine("Case When A.Remark Is Null Then ");
            SQL.AppendLine("    Case When B.Remark Is Null Then Null Else B.Remark End ");
            SQL.AppendLine("Else ");
            SQL.AppendLine("    Case When B.Remark Is Null Then A.Remark Else Concat(A.Remark, ' ( ', B.Remark, ' )') End ");
            SQL.AppendLine("End As Remark, ");
            SQL.AppendLine("@UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblRecvWhs4Hdr A ");
            SQL.AppendLine("Inner Join TblRecvWhs4Dtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblDOWhsDtl C On B.DOWhsDocNo=C.DocNo And B.DOWhsDNo=C.DNo ");
            SQL.AppendLine("Inner Join TblDOWhsHdr D On C.DocNo=D.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            #endregion

            #region movement batch new jika batch tidak sama
            
            SQL.AppendLine("Insert Into TblStockMovement ");
            SQL.AppendLine("(DocType, DocNo, DNo, Source, CancelInd, Source2, ");
            SQL.AppendLine("DocDt, WhsCode, Lot, Bin, ItCode, BatchNo, ");
            SQL.AppendLine("Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocType, A.DocNo, B.DNo, B.Source, 'N', '', ");
            SQL.AppendLine("A.DocDt, A.WhsCode, C.Lot, C.Bin, C.ItCode, B.BatchNo, ");
            SQL.AppendLine("B.Qty, B.Qty2, B.Qty3, ");
            SQL.AppendLine("Case When A.Remark Is Null Then ");
            SQL.AppendLine("    Case When B.Remark Is Null Then Null Else B.Remark End ");
            SQL.AppendLine("Else ");
            SQL.AppendLine("    Case When B.Remark Is Null Then A.Remark Else Concat(A.Remark, ' ( ', B.Remark, ' )') End ");
            SQL.AppendLine("End As Remark, ");
            SQL.AppendLine("@UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblRecvWhs4Hdr A ");
            SQL.AppendLine("Inner Join TblRecvWhs4Dtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblDOWhsDtl C On B.DOWhsDocNo=C.DocNo And B.DOWhsDNo=C.DNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo And B.BatchNo<>B.BatchNoOld; ");

            #endregion

            SQL.AppendLine("Insert Into TblStockSummary(WhsCode, Lot, Bin, ItCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.WhsCode, C.Lot, C.Bin, C.ItCode, C.BatchNo, C.Source, 0, 0, 0, Null, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblRecvWhs4Hdr A ");
            SQL.AppendLine("Inner Join TblRecvWhs4Dtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblDOWhsDtl C On B.DOWhsDocNo=C.DocNo And B.DOWhsDNo=C.DNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists(Select 1 From TblStockSummary Where WhsCode = A.WhsCode And Lot=C.Lot And Bin=C.Bin And Source = C.Source); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DocType", mDocType);
            Sm.CmParam<String>(ref cm, "@DocType2", mDocType2);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveStockSummary(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblStockSummary Set ");
            SQL.AppendLine("    Qty=Qty-@Qty, Qty2=Qty2-@Qty2, Qty3=Qty3-@Qty3, ");
            SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where WhsCode=@WhsCode2 And Lot=@Lot And Bin=@Bin ");
            SQL.AppendLine("And Source=@SourceOld; ");

            if (Sm.GetGrdStr(Grd1, Row, 10) != Sm.GetGrdStr(Grd1, Row, 28))
            {
                //bikin stock batch baru
                SQL.AppendLine("Insert Into TblStockSummary(WhsCode, Lot, Bin, ItCode, BatchNo, Source, Qty, Qty2, Qty3, CreateBy, CreateDt) ");
                SQL.AppendLine("Values (@WhsCode, @Lot, @Bin, @ItCode, @BatchNo, Concat(@DocType, '*', @DocNo, '*', @DNo), @Qty, @Qty2, @Qty3, @UserCode, CurrentDateTime()); ");
            }
            else
            {
                SQL.AppendLine("Update TblStockSummary Set ");
                SQL.AppendLine("    Qty=Qty+@Qty, Qty2=Qty2+@Qty2, Qty3=Qty3+@Qty3, ");
                SQL.AppendLine("    LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
                SQL.AppendLine("Where WhsCode=@WhsCode And Lot=@Lot And Bin=@Bin ");
                SQL.AppendLine("And Source=@SourceOld; ");
            }
           
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@DocType", mDocType);
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            Sm.CmParam<String>(ref cm, "@WhsCode2", Sm.GetLue(LueWhsCode2));
            Sm.CmParam<String>(ref cm, "@Lot", Sm.GetGrdStr(Grd1, Row, 12));
            Sm.CmParam<String>(ref cm, "@Bin", Sm.GetGrdStr(Grd1, Row, 13));
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 7));
            Sm.CmParam<String>(ref cm, "@BatchNo", Sm.GetGrdStr(Grd1, Row, 10));
            Sm.CmParam<String>(ref cm, "@Source", Sm.GetGrdStr(Grd1, Row, 11));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 15));
            Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd1, Row, 18));
            Sm.CmParam<Decimal>(ref cm, "@Qty3", Sm.GetGrdDec(Grd1, Row, 21));
            Sm.CmParam<String>(ref cm, "@BatchNoOld", Sm.GetGrdStr(Grd1, Row, 28));
            Sm.CmParam<String>(ref cm, "@SourceOld", Sm.GetGrdStr(Grd1, Row, 29));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveStockPrice(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            if (Sm.GetGrdStr(Grd1, Row, 10) != Sm.GetGrdStr(Grd1, Row, 28))
            {
                SQL.AppendLine("Insert Into TblStockPrice (ItCode, BatchNo, PropCode, Source, CurCode, UPrice, ExcRate, CreateBy, CreateDt ) ");
                SQL.AppendLine("Select ItCode, @BatchNo, PropCode, Concat(@DocType, '*', @DocNo, '*', @DNo), CurCode, UPrice, ExcRate, CreateBy, CreateDt ");
                SQL.AppendLine("From TblStockPrice ");
                SQL.AppendLine("Where Source=@SourceOld ; ");
            }
            else
            {
                SQL.AppendLine("Select ''  ");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@BatchNo", Sm.GetGrdStr(Grd1, Row, 10));
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DocType", mDocType);
            Sm.CmParam<String>(ref cm, "@SourceOld", Sm.GetGrdStr(Grd1, Row, 29));

            return cm;
        }

        private MySqlCommand UpdateDOWhs4RecvQty(string DocNo, int Row)
        {
            var SQL = new StringBuilder(); // 4 5

            SQL.AppendLine("Update TblDOWhs4Dtl A ");
            SQL.AppendLine("Inner Join TblDOWhsDtl B On A.DOWhsDocNo = B.DocNo And A.DOWhsDNo = B.DNo ");
            SQL.AppendLine("    And B.DocNo = @DocNo And B.DNo = @DNo ");
            SQL.AppendLine("Set A.RecvQty = A.RecvQty + @Qty; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", Sm.GetGrdStr(Grd1, Row, 4));
            Sm.CmParam<String>(ref cm, "@DNo", Sm.GetGrdStr(Grd1, Row, 5));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 21));

            return cm;
        }

        private MySqlCommand UpdateDOWhsProcessInd(string DocNo)
        {
            var SQL = new StringBuilder();

            if (mIsRecvWhs4ProcessDOWhsPartialEnabled)
            {
                SQL.AppendLine("Update TblDOWhsDtl T1 ");
                SQL.AppendLine("Inner Join ( ");
                SQL.AppendLine("    Select B.DOWhsDocNo As DocNo, B.DOWhsDNo As DNo, Sum(Qty) As Qty ");
                SQL.AppendLine("    From TblRecvWhs4Hdr A, TblRecvWhs4Dtl B ");
                SQL.AppendLine("    Where A.DocNo=B.DocNo ");
                SQL.AppendLine("    And B.CancelInd='N' ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 ");
                SQL.AppendLine("        From TblRecvWhs4Dtl ");
                SQL.AppendLine("        Where DocNo=@DocNo  ");
                SQL.AppendLine("        And DOWhsDocNo=B.DOWhsDocNo ");	
                SQL.AppendLine("        And DOWhsDNo=B.DOWhsDNo ");
				SQL.AppendLine("    ) ");
                SQL.AppendLine("    Group By B.DOWhsDocNo, B.DOWhsDNo ");
                SQL.AppendLine(") T2 On T1.DocNo=T2.DocNo And T1.DNo=T2.DNo  ");
                SQL.AppendLine("Set ");
                SQL.AppendLine("    T1.ProcessInd= ");
                SQL.AppendLine("        Case When T1.Qty<=IfNull(T2.Qty, 0) Then 'F' ");
                SQL.AppendLine("        Else ");
                SQL.AppendLine("            Case When IfNull(T2.Qty, 0)=0.00 Then 'O' Else 'P' End ");
                SQL.AppendLine("        End, ");
                SQL.AppendLine("    T1.LastUpBy=@UserCode, T1.LastUpDt=CurrentDateTime()  ");
                SQL.AppendLine("Where T1.CancelInd='N'; ");
            }
            else
            {
                SQL.AppendLine("Update TblDOWhsDtl T1 ");
                SQL.AppendLine("Inner Join ( ");
                SQL.AppendLine("    Select B.DOWhsDocNo As DocNo, B.DOWhsDNo As DNo ");
                SQL.AppendLine("    From TblRecvWhs4Hdr A ");
                SQL.AppendLine("    Inner Join TblRecvWhs4Dtl B On A.DocNo=B.DocNo And B.CancelInd='N' ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
                SQL.AppendLine(") T2 On T1.DocNo=T2.DocNo And T1.DNo=T2.DNo  ");
                SQL.AppendLine("Set T1.ProcessInd='F', T1.LastUpBy=@UserCode, T1.LastUpDt=CurrentDateTime()  ");
                SQL.AppendLine("Where T1.CancelInd='N'; ");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            
            return cm;
        }

        private MySqlCommand SaveJournal(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblRecvWhs4Hdr Set ");
            SQL.AppendLine("    JournalDocNo=@JournalDocNo, ");
            SQL.AppendLine("    JournalDocNo2=@JournalDocNo2 ");
            SQL.AppendLine("Where DocNo=@DocNo;");

            #region Journal 1

            SQL.AppendLine("Insert Into TblJournalHdr ");
            SQL.AppendLine("(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select JournalDocNo, ");
            SQL.AppendLine("DocDt, ");
            SQL.AppendLine("Concat('Receiving From Warehouse (DO With Transfer Request) : ', DocNo) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, ");
            SQL.AppendLine("(Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, ");
            SQL.AppendLine("@CCCodeWhs2, Remark, CreateBy, CreateDt ");
            SQL.AppendLine("From TblRecvWhs4Hdr ");
            SQL.AppendLine("Where DocNo=@DocNo And JournalDocNo Is Not Null;");

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, B.EntCode, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");

            SQL.AppendLine("    Select T.EntCode, T.AcNo, Sum(T.DAmt) As DAmt, Sum(T.CAmt) As CAmt ");
            SQL.AppendLine("    From ( ");

            if (mIsMovingAvgEnabled)
            {
                SQL.AppendLine("    Select @EntCodeWhs2 As EntCode, ");
                SQL.AppendLine("    D.AcNo5 As AcNo, ");
                SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate As DAmt, ");
                SQL.AppendLine("    0.00 As CAmt ");
                SQL.AppendLine("    From TblRecvWhs4Hdr A ");
                SQL.AppendLine("    Inner Join TblRecvWhs4Dtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs2 And D.AcNo5 Is Not Null ");
                SQL.AppendLine("    Inner Join TblItem E On C.ItCode=E.ItCode ");
                SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='N' ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs2 As EntCode, ");
                SQL.AppendLine("    T.AcNo, ");
                SQL.AppendLine("    0.00 As DAmt, ");
                SQL.AppendLine("    Sum(T.Amt) As CAmt ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select F.AcNo, ");
                SQL.AppendLine("        B.Qty*C.UPrice*C.ExcRate As Amt ");
                SQL.AppendLine("        From TblRecvWhs4Hdr A ");
                SQL.AppendLine("        Inner Join TblRecvWhs4Dtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblDOWhsDtl D On B.DOWhsDocNo=D.DocNo And B.DOWhsDNo=D.DNo");
                SQL.AppendLine("        Inner Join TblItem E On D.ItCode=E.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.AcNo Is Not Null And F.MovingAvgInd='N' ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("    ) T Group By T.AcNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs2 As EntCode, ");
                SQL.AppendLine("    D.AcNo4 As AcNo, ");
                SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate As DAmt, ");
                SQL.AppendLine("    0.00 As CAmt ");
                SQL.AppendLine("    From TblRecvWhs4Hdr A ");
                SQL.AppendLine("    Inner Join TblRecvWhs4Dtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs2 And D.AcNo4 Is Not Null ");
                SQL.AppendLine("    Inner Join TblItem E On C.ItCode=E.ItCode ");
                SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='N' ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs2 As EntCode, ");
                SQL.AppendLine("    D.AcNo6 As AcNo, ");
                SQL.AppendLine("    0.00 As DAmt, ");
                SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate As CAmt ");
                SQL.AppendLine("    From TblRecvWhs4Hdr A ");
                SQL.AppendLine("    Inner Join TblRecvWhs4Dtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs2 And D.AcNo6 Is Not Null ");
                SQL.AppendLine("    Inner Join TblItem E On C.ItCode=E.ItCode ");
                SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='N' ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs2 As EntCode, ");
                SQL.AppendLine("    D.AcNo5 As AcNo, ");
                SQL.AppendLine("    B.Qty*IfNull(G.MovingAvgPrice, 0.00) As DAmt, ");
                SQL.AppendLine("    0.00 As CAmt ");
                SQL.AppendLine("    From TblRecvWhs4Hdr A ");
                SQL.AppendLine("    Inner Join TblRecvWhs4Dtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs2 And D.AcNo5 Is Not Null ");
                SQL.AppendLine("    Inner Join TblItem E On B.ItCode=E.ItCode ");
                SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='Y' ");
                SQL.AppendLine("    Inner Join TblItemMovingAvg G On C.ItCode=G.ItCode ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs2 As EntCode, ");
                SQL.AppendLine("    T.AcNo, ");
                SQL.AppendLine("    0.00 As DAmt, ");
                SQL.AppendLine("    Sum(T.Amt) As CAmt ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select F.AcNo, ");
                SQL.AppendLine("        B.Qty*IfNull(G.MovingAvgPrice, 0.00) As Amt ");
                SQL.AppendLine("        From TblRecvWhs4Hdr A ");
                SQL.AppendLine("        Inner Join TblRecvWhs4Dtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblDOWhsDtl D On B.DOWhsDocNo=D.DocNo And B.DOWhsDNo=D.DNo");
                SQL.AppendLine("        Inner Join TblItem E On D.ItCode=E.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.AcNo Is Not Null And F.MovingAvgInd='Y' ");
                SQL.AppendLine("        Inner Join TblItemMovingAvg G On C.ItCode=G.ItCode ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("    ) T Group By T.AcNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs2 As EntCode, ");
                SQL.AppendLine("    D.AcNo4 As AcNo, ");
                SQL.AppendLine("    B.Qty*IfNull(G.MovingAvgPrice, 0.00) As DAmt, ");
                SQL.AppendLine("    0.00 As CAmt ");
                SQL.AppendLine("    From TblRecvWhs4Hdr A ");
                SQL.AppendLine("    Inner Join TblRecvWhs4Dtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs2 And D.AcNo4 Is Not Null ");
                SQL.AppendLine("    Inner Join TblItem E On B.ItCode=E.ItCode ");
                SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='Y' ");
                SQL.AppendLine("    Inner Join TblItemMovingAvg G On C.ItCode=G.ItCode ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs2 As EntCode, ");
                SQL.AppendLine("    D.AcNo6 As AcNo, ");
                SQL.AppendLine("    0.00 As DAmt, ");
                SQL.AppendLine("    B.Qty*IfNull(G.MovingAvgPrice, 0.00) As CAmt ");
                SQL.AppendLine("    From TblRecvWhs4Hdr A ");
                SQL.AppendLine("    Inner Join TblRecvWhs4Dtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs2 And D.AcNo6 Is Not Null ");
                SQL.AppendLine("    Inner Join TblItem E On B.ItCode=E.ItCode ");
                SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='Y' ");
                SQL.AppendLine("    Inner Join TblItemMovingAvg G On C.ItCode=G.ItCode ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
            }
            else
            {
                SQL.AppendLine("    Select @EntCodeWhs2 As EntCode, ");
                SQL.AppendLine("    D.AcNo5 As AcNo, ");
                SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate As DAmt, ");
                SQL.AppendLine("    0.00 As CAmt ");
                SQL.AppendLine("    From TblRecvWhs4Hdr A ");
                SQL.AppendLine("    Inner Join TblRecvWhs4Dtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs2 And D.AcNo5 Is Not Null ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs2 As EntCode, ");
                SQL.AppendLine("    T.AcNo, ");
                SQL.AppendLine("    0.00 As DAmt, ");
                SQL.AppendLine("    Sum(T.Amt) As CAmt ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select F.AcNo, ");
                SQL.AppendLine("        B.Qty*C.UPrice*C.ExcRate As Amt ");
                SQL.AppendLine("        From TblRecvWhs4Hdr A ");
                SQL.AppendLine("        Inner Join TblRecvWhs4Dtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblDOWhsDtl D On B.DOWhsDocNo=D.DocNo And B.DOWhsDNo=D.DNo");
                SQL.AppendLine("        Inner Join TblItem E On D.ItCode=E.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.AcNo Is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("    ) T Group By T.AcNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs2 As EntCode, ");
                SQL.AppendLine("    D.AcNo4 As AcNo, ");
                SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate As DAmt, ");
                SQL.AppendLine("    0.00 As CAmt ");
                SQL.AppendLine("    From TblRecvWhs4Hdr A ");
                SQL.AppendLine("    Inner Join TblRecvWhs4Dtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs2 And D.AcNo4 Is Not Null ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs2 As EntCode, ");
                SQL.AppendLine("    D.AcNo6 As AcNo, ");
                SQL.AppendLine("    0.00 As DAmt, ");
                SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate As CAmt ");
                SQL.AppendLine("    From TblRecvWhs4Hdr A ");
                SQL.AppendLine("    Inner Join TblRecvWhs4Dtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs2 And D.AcNo6 Is Not Null ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
            }
            SQL.AppendLine("    ) T ");
            SQL.AppendLine("    Group By T.EntCode, T.AcNo ");

            SQL.AppendLine(") B On 1=1 ");
            SQL.AppendLine("Where A.DocNo=@JournalDocNo;");

            #endregion

            #region Journal 2

            SQL.AppendLine("Insert Into TblJournalHdr ");
            SQL.AppendLine("(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select JournalDocNo2, ");
            SQL.AppendLine("DocDt, ");
            SQL.AppendLine("Concat('Receiving From Warehouse (DO With Transfer Request) : ', DocNo) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, ");
            SQL.AppendLine("(Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, ");
            SQL.AppendLine("@CCCodeWhs, Remark, CreateBy, CreateDt ");
            SQL.AppendLine("From TblRecvWhs4Hdr ");
            SQL.AppendLine("Where DocNo=@DocNo And JournalDocNo2 Is Not Null;");

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, B.EntCode, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");

            SQL.AppendLine("    Select T.EntCode, T.AcNo, Sum(T.DAmt) As DAmt, Sum(T.CAmt) As CAmt ");
            SQL.AppendLine("    From ( ");

            if (mIsMovingAvgEnabled)
            {
                SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
                SQL.AppendLine("    T.AcNo, ");
                SQL.AppendLine("    Sum(T.Amt) As DAmt, ");
                SQL.AppendLine("    0.00 As CAmt ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select F.AcNo, ");
                SQL.AppendLine("        B.Qty*C.UPrice*C.ExcRate As Amt ");
                SQL.AppendLine("        From TblRecvWhs4Hdr A ");
                SQL.AppendLine("        Inner Join TblRecvWhs4Dtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblDOWhsDtl D On B.DOWhsDocNo=D.DocNo And B.DOWhsDNo=D.DNo");
                SQL.AppendLine("        Inner Join TblItem E On D.ItCode=E.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.AcNo Is Not Null And F.MovingAvgInd='N' ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("    ) T Group By T.AcNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
                SQL.AppendLine("    D.AcNo3 As AcNo, ");
                SQL.AppendLine("    0.00 As DAmt, ");
                SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate As CAmt ");
                SQL.AppendLine("    From TblRecvWhs4Hdr A ");
                SQL.AppendLine("    Inner Join TblRecvWhs4Dtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs And D.AcNo3 Is Not Null ");
                SQL.AppendLine("    Inner Join TblItem E On C.ItCode=E.ItCode ");
                SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='N' ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
                SQL.AppendLine("    T.AcNo, ");
                SQL.AppendLine("    Sum(T.Amt) As DAmt, ");
                SQL.AppendLine("    0.00 As CAmt ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select F.AcNo, ");
                SQL.AppendLine("        B.Qty*IfNull(G.MovingAvgPrice, 0.00) As Amt ");
                SQL.AppendLine("        From TblRecvWhs4Hdr A ");
                SQL.AppendLine("        Inner Join TblRecvWhs4Dtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblDOWhsDtl D On B.DOWhsDocNo=D.DocNo And B.DOWhsDNo=D.DNo ");
                SQL.AppendLine("        Inner Join TblItem E On D.ItCode=E.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.AcNo Is Not Null And F.MovingAvgInd='Y' ");
                SQL.AppendLine("        Inner Join TblItemMovingAvg G On C.ItCode=G.ItCode ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("    ) T Group By T.AcNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
                SQL.AppendLine("    D.AcNo3 As AcNo, ");
                SQL.AppendLine("    0.00 As DAmt, ");
                SQL.AppendLine("    B.Qty*IfNull(G.MovingAvgPrice, 0.00) As CAmt ");
                SQL.AppendLine("    From TblRecvWhs4Hdr A ");
                SQL.AppendLine("    Inner Join TblRecvWhs4Dtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs And D.AcNo3 Is Not Null ");
                SQL.AppendLine("    Inner Join TblItem E On C.ItCode=E.ItCode ");
                SQL.AppendLine("    Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.MovingAvgInd='Y' ");
                SQL.AppendLine("    Inner Join TblItemMovingAvg G On C.ItCode=G.ItCode ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
            }
            else
            {
                SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
                SQL.AppendLine("    T.AcNo, ");
                SQL.AppendLine("    Sum(T.Amt) As DAmt, ");
                SQL.AppendLine("    0.00 As CAmt ");
                SQL.AppendLine("    From ( ");
                SQL.AppendLine("        Select F.AcNo, ");
                SQL.AppendLine("        B.Qty*C.UPrice*C.ExcRate As Amt ");
                SQL.AppendLine("        From TblRecvWhs4Hdr A ");
                SQL.AppendLine("        Inner Join TblRecvWhs4Dtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblDOWhsDtl D On B.DOWhsDocNo=D.DocNo And B.DOWhsDNo=D.DNo ");
                SQL.AppendLine("        Inner Join TblItem E On D.ItCode=E.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory F On E.ItCtCode=F.ItCtCode And F.AcNo Is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("    ) T Group By T.AcNo ");
                SQL.AppendLine("    Union All ");
                SQL.AppendLine("    Select @EntCodeWhs As EntCode, ");
                SQL.AppendLine("    D.AcNo3 As AcNo, ");
                SQL.AppendLine("    0.00 As DAmt, ");
                SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate As CAmt ");
                SQL.AppendLine("    From TblRecvWhs4Hdr A ");
                SQL.AppendLine("    Inner Join TblRecvWhs4Dtl B On A.DocNo=B.DocNo ");
                SQL.AppendLine("    Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("    Inner Join TblEntity D On D.EntCode=@EntCodeWhs And D.AcNo3 Is Not Null ");
                SQL.AppendLine("    Where A.DocNo=@DocNo ");
            }
            SQL.AppendLine("    ) T ");
            SQL.AppendLine("    Group By T.EntCode, T.AcNo ");

            SQL.AppendLine(") B On 1=1 ");
            SQL.AppendLine("Where A.DocNo=@JournalDocNo2;");

            #endregion

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(Sm.GetDte(DteDocDt), 1));
            Sm.CmParam<String>(ref cm, "@JournalDocNo2", Sm.GenerateDocNoJournal(Sm.GetDte(DteDocDt), 2));
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@EntCodeWhs", Sm.GetEntityWarehouse(Sm.GetLue(LueWhsCode)));
            Sm.CmParam<String>(ref cm, "@EntCodeWhs2", Sm.GetEntityWarehouse(Sm.GetLue(LueWhsCode2)));
            Sm.CmParam<String>(ref cm, "@CCCodeWhs", GetCostCenterWarehouse(Sm.GetLue(LueWhsCode)));
            Sm.CmParam<String>(ref cm, "@CCCodeWhs2", GetCostCenterWarehouse(Sm.GetLue(LueWhsCode2)));

            return cm;
        }

        private MySqlCommand SaveJournal2(string DocNo) // khusus param IsRecvWhs4ProcessTo1Journal
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Set @JournalDocNo := ");
            SQL.AppendLine(Sm.GetNewJournalDocNo(Sm.GetDte(DteDocDt), 1));
            SQL.AppendLine(";");

            SQL.AppendLine("Update TblRecvWhs4Hdr Set ");
            SQL.AppendLine("    JournalDocNo=@JournalDocNo ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And WhsCode In (Select WhsCode From TblWarehouse Where MovingAvgInd='N'); ");

            SQL.AppendLine("Set @SOContractDocNo:= (");
            SQL.AppendLine("    Select Group_Concat(Distinct T5.SOContractDocNo Separator ', ') As SOContractDocNo ");
            SQL.AppendLine("    From TblRecvWhs4Hdr T1 ");
            SQL.AppendLine("    Inner Join TblRecvWhs4Dtl T2 On T1.DocNo = T2.DocNo ");
            SQL.AppendLine("    Inner Join TblDOWhsHdr T4 On T2.DOWhsDocno = T4.DocNo ");
            SQL.AppendLine("    Inner Join TblTransferRequestWhsHdr T5 ");
            SQL.AppendLine("        On T4.TransferRequestWhsDocNo = T5.DocNo ");
            SQL.AppendLine("        And T5.SOContractDocNo Is Not Null ");
            SQL.AppendLine("    Where T1.DocNo=@DocNo ");
            SQL.AppendLine("); ");

            SQL.AppendLine("Set @DOWhs4DocNo:= (");
            SQL.AppendLine("    Select Group_Concat(Distinct T6.DocNo Separator ', ') As DOWhs4DocNo ");
            SQL.AppendLine("    From TblRecvWhs4Hdr T1 ");
            SQL.AppendLine("    Inner Join TblRecvWhs4Dtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    Inner Join TblDOWhsDtl T3 On T2.DOWhsDocno=T3.DocNo And T2.DOWhsDNo=T3.DNo ");
            SQL.AppendLine("    Inner Join TblDOWhs4Dtl T6 On T3.DocNo=T6.DOWhsDocNo And T3.DNo=T6.DOWhsDNo And T6.DocNo Is Not Null ");
            SQL.AppendLine("    Where T1.DocNo=@DocNo ");
            SQL.AppendLine("    ); ");

            SQL.AppendLine("Insert Into TblJournalHdr ");
            SQL.AppendLine("(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select JournalDocNo, DocDt, ");
            SQL.AppendLine("Concat('Receiving Item From Warehouse (DO With Transfer Request) : ', DocNo, ', Do To Other Warehouse for Project : ', IFNULL(@DOWhs4DocNo, '-')) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, (Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, ");
            SQL.AppendLine("@CCCodeWhs2, Concat('SO Contract# : ', @SOContractDocNo), CreateBy, CreateDt ");
            SQL.AppendLine("From TblRecvWhs4Hdr ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And JournalDocNo Is Not Null ");
            SQL.AppendLine("And JournalDocNo=@JournalDocNo; ");

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, SOContractDocNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, B.EntCode, @SOContractDocNo, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");

            SQL.AppendLine("    Select T.EntCode, T.AcNo, Sum(T.DAmt) As DAmt, Sum(T.CAmt) As CAmt ");
            SQL.AppendLine("    From ( ");

            SQL.AppendLine("    SELECT @EntCodeWhs2 AS EntCode, E.AcNo, ");
            SQL.AppendLine("    0.00 AS DAmt, (B.Qty*C.UPrice*C.ExcRate) As CAmt ");
            SQL.AppendLine("    FROM TblRecvWhs4Hdr A ");
            SQL.AppendLine("    INNER JOIN TblRecvWhs4Dtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("    INNER JOIN TblStockPrice C ON B.Source = C.Source ");
            SQL.AppendLine("    INNER JOIN TblItem D ON C.ItCode = D.ItCode ");
            SQL.AppendLine("    INNER JOIN TblItemCategory E ON D.ItCtCode=E.ItCtCode And E.AcNo Is Not Null AND E.MovingAvgInd = 'N' ");
            SQL.AppendLine("    Where A.DocNo=@DocNo ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    SELECT @EntCodeWhs2 AS EntCode, F.Parvalue AcNo, ");
            SQL.AppendLine("    0.00 AS DAmt, (B.Qty*C.UPrice*C.ExcRate) As CAmt ");
            SQL.AppendLine("    FROM TblRecvWhs4Hdr A ");
            SQL.AppendLine("    INNER JOIN TblRecvWhs4Dtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("    INNER JOIN TblStockPrice C ON B.Source = C.Source ");
            SQL.AppendLine("    INNER JOIN TblItem D ON C.ItCode = D.ItCode ");
            SQL.AppendLine("    INNER JOIN TblItemCategory E ON D.ItCtCode = E.ItCtCode AND E.MovingAvgInd = 'N' ");
            SQL.AppendLine("    INNER JOIN TblParameter F ON F.ParCode = 'AcNoForRecvWhs4OfOffsetWIP' AND F.ParValue IS NOT NULL ");
            SQL.AppendLine("    Where A.DocNo=@DocNo ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    SELECT @EntCodeWhs2 AS EntCode, F.ParValue AcNo, ");
            SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate DAmt, 0.00 As CAmt ");
            SQL.AppendLine("    FROM TblRecvWhs4Hdr A ");
            SQL.AppendLine("    INNER JOIN TblRecvWhs4Dtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("    INNER JOIN TblStockPrice C ON B.Source = C.Source ");
            SQL.AppendLine("    INNER JOIN TblItem D ON C.ItCode = D.ItCode ");
            SQL.AppendLine("    INNER JOIN TblItemCategory E ON D.ItCtCode = E.ItCtCode AND E.MovingAvgInd = 'N' ");
            SQL.AppendLine("    INNER JOIN TblParameter F ON F.ParCode = 'AcNoForDOToProductOfWIP' AND F.ParValue IS NOT NULL ");
            SQL.AppendLine("    Where A.DocNo=@DocNo ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    SELECT @EntCodeWhs2 AS EntCode, E.AcNo2 As AcNo, ");
            SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate DAmt, 0.00 As CAmt ");
            SQL.AppendLine("    FROM TblRecvWhs4Hdr A ");
            SQL.AppendLine("    INNER JOIN TblRecvWhs4Dtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("    INNER JOIN TblStockPrice C ON B.Source = C.Source ");
            SQL.AppendLine("    INNER JOIN TblItem D ON C.ItCode = D.ItCode ");
            SQL.AppendLine("    INNER JOIN TblItemCategory E ON D.ItCtCode = E.ItCtCode AND E.MovingAvgInd = 'N' And E.AcNo2 Is Not Null ");
            SQL.AppendLine("    Where A.DocNo=@DocNo ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select @EntCodeWhs2 As EntCode, E.AcNo, ");
            SQL.AppendLine("    0.00 As DAmt, ");
            SQL.AppendLine("    B.Qty*IfNull(C.MovingAvgPrice, 0.00) CAmt ");
            SQL.AppendLine("    From TblRecvWhs4Hdr A ");
            SQL.AppendLine("    Inner Join TblRecvWhs4Dtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("    Inner Join TblStockMovement C On B.DocNo=C.DocNo And B.DNo=C.DNo ");
            SQL.AppendLine("    Inner Join TblItem D ON C.ItCode = D.ItCode ");
            SQL.AppendLine("    Inner Join TblItemCategory E ON D.ItCtCode = E.ItCtCode And E.AcNo Is Not Null AND E.MovingAvgInd = 'Y' ");
            SQL.AppendLine("    Where A.DocNo=@DocNo ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select @EntCodeWhs2 AS EntCode, F.ParValue AcNo, ");
            SQL.AppendLine("    0.00 As DAmt, ");
            SQL.AppendLine("    B.Qty*IFNULL(C.MovingAvgPrice, 0.00) As CAmt ");
            SQL.AppendLine("    FROM TblRecvWhs4Hdr A ");
            SQL.AppendLine("    INNER JOIN TblRecvWhs4Dtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("    Inner Join TblStockMovement C On B.DocNo=C.DocNo And B.DNo=C.DNo ");
            SQL.AppendLine("    INNER JOIN TblItem D ON C.ItCode = D.ItCode ");
            SQL.AppendLine("    INNER JOIN TblItemCategory E ON D.ItCtCode = E.ItCtCode AND E.MovingAvgInd = 'Y' ");
            SQL.AppendLine("    INNER JOIN TblParameter F ON F.ParCode = 'AcNoForRecvWhs4OfOffsetWIP' AND F.ParValue IS NOT NULL ");
            SQL.AppendLine("    Where A.DocNo=@DocNo ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select @EntCodeWhs2 AS EntCode, F.ParValue AcNo, ");
            SQL.AppendLine("    B.Qty*IFNULL(C.MovingAvgPrice, 0.00) DAmt, 0.00 As CAmt ");
            SQL.AppendLine("    From TblRecvWhs4Hdr A ");
            SQL.AppendLine("    Inner Join TblRecvWhs4Dtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("    Inner Join TblStockMovement C On B.DocNo=C.DocNo And B.DNo=C.DNo ");
            SQL.AppendLine("    Inner Join TblItem D ON C.ItCode = D.ItCode ");
            SQL.AppendLine("    Inner Join TblItemCategory E ON D.ItCtCode = E.ItCtCode AND E.MovingAvgInd = 'Y' ");
            SQL.AppendLine("    Inner Join TblParameter F ON F.ParCode = 'AcNoForDOToProductOfWIP' AND F.ParValue IS NOT NULL ");
            SQL.AppendLine("    Where A.DocNo=@DocNo ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    SELECT @EntCodeWhs2 AS EntCode, E.AcNo2 AS AcNo, ");
            SQL.AppendLine("    B.Qty*IFNULL(C.MovingAvgPrice, 0.00) DAmt, 0.00 As CAmt ");
            SQL.AppendLine("    FROM TblRecvWhs4Hdr A ");
            SQL.AppendLine("    INNER JOIN TblRecvWhs4Dtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("    Inner Join TblStockMovement C On B.DocNo=C.DocNo And B.DNo=C.DNo ");
            SQL.AppendLine("    INNER JOIN TblItem D ON C.ItCode = D.ItCode ");
            SQL.AppendLine("    INNER JOIN TblItemCategory E ON D.ItCtCode = E.ItCtCode AND E.MovingAvgInd = 'Y' And E.AcNo2 Is Not Null ");
            SQL.AppendLine("    Where A.DocNo=@DocNo ");
            SQL.AppendLine("    ) T ");
            SQL.AppendLine("    Group By T.EntCode, T.AcNo ");

            SQL.AppendLine(") B On 1=1 ");
            SQL.AppendLine("Where A.DocNo=@JournalDocNo ");
            SQL.AppendLine("And A.DocNo In (Select JournalDocNo From TblRecvWhs4Hdr Where DocNo=@DocNo And JournalDocNo Is Not Null); ");

            //SQL.AppendLine("Insert Into TblJournalHdr ");
            //SQL.AppendLine("(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
            //SQL.AppendLine("Select A.JournalDocNo, ");
            //SQL.AppendLine("A.DocDt, ");
            //SQL.AppendLine("Concat('Receiving From Warehouse (DO With Transfer Request) : ', A.DocNo) As JnDesc, ");
            //SQL.AppendLine("@MenuCode As MenuCode, ");
            //SQL.AppendLine("(Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, ");
            //SQL.AppendLine("@CCCodeWhs2, Concat('SO Contract# : ', B.SOContractDocNo), A.CreateBy, A.CreateDt ");
            //SQL.AppendLine("From TblRecvWhs4Hdr A ");
            //SQL.AppendLine("Inner Join ");
            //SQL.AppendLine("( ");
            //SQL.AppendLine("    Select T1.DocNo, Group_Concat(Distinct T5.SOContractDocNo Separator ', ') SOContractDocNo ");
            //SQL.AppendLine("    From TblRecvWhs4Hdr T1 ");
            //SQL.AppendLine("    Inner Join TblRecvWhs4Dtl T2 On T1.DocNo = T2.DocNo And T1.DocNo = @DocNo ");
            //SQL.AppendLine("    Inner Join TblDOWhsDtl T3 On T2.DOWhsDocno = T3.DocNo And T2.DOWhsDNo = T3.DNo ");
            //SQL.AppendLine("    Inner Join TblDOWhsHdr T4 On T3.DocNo = T4.DocNo ");
            //SQL.AppendLine("    Inner Join TblTransferRequestWhsHdr T5 On T4.TransferRequestWhsDocNo = T5.DocNo ");
            //SQL.AppendLine("        AND T4.TransferRequestWhsDocNo IS NOT NULL ");
            //SQL.AppendLine("        AND T5.SOContractDocNo IS NOT NULL ");
            //SQL.AppendLine("    Group By T1.DocNo ");
            //SQL.AppendLine(") B On A.DocNo = B.DocNo ");
            //SQL.AppendLine("Where A.DocNo=@DocNo And A.JournalDocNo Is Not Null; ");

            //SQL.AppendLine("Set @Row:=0; ");

            //SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, SOContractDocNo, CreateBy, CreateDt) ");
            //SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            //SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, B.EntCode, B.SOContractDocNo, A.CreateBy, A.CreateDt ");
            //SQL.AppendLine("From TblJournalHdr A ");
            //SQL.AppendLine("Inner Join ( ");

            //SQL.AppendLine("    Select T.EntCode, T.AcNo, Sum(T.DAmt) As DAmt, Sum(T.CAmt) As CAmt, Group_COncat(Distinct IfNull(T.SOContractDocNo, '')) As SOContractDocNo ");
            //SQL.AppendLine("    From ( ");

            //SQL.AppendLine("    SELECT @EntCodeWhs2 AS EntCode, I.SOContractDocNo, ");
            //SQL.AppendLine("    E.AcNo, 0.00 AS DAmt, ");
            //SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate CAmt ");
            //SQL.AppendLine("    FROM TblRecvWhs4Hdr A ");
            //SQL.AppendLine("    INNER JOIN TblRecvWhs4Dtl B ON A.DocNo = B.DocNo ");
            //SQL.AppendLine("        AND A.DocNo = @DocNo ");
            //SQL.AppendLine("    INNER JOIN TblStockPrice C ON B.Source = C.Source ");
            //SQL.AppendLine("    INNER JOIN TblItem D ON C.ItCode = D.ItCode ");
            //SQL.AppendLine("    INNER JOIN TblItemCategory E ON D.ItCtCode = E.ItCtCode And E.AcNo Is Not Null AND E.MovingAvgInd = 'N' ");
            //SQL.AppendLine("    INNER JOIN TblDOWhsDtl G ON B.DOWhsDocNo = G.DocNo AND B.DOWhsDNo = G.DNo ");
            //SQL.AppendLine("    INNER JOIN TblDOWhsHdr H ON G.DocNO = H.DocNo ");
            //SQL.AppendLine("    INNER JOIN TblTransferRequestWhsHdr I ON H.TransferRequestWhsDocNo = I.DocNo ");
            //SQL.AppendLine("        AND H.TransferRequestWhsDocNo IS NOT NULL ");
            //SQL.AppendLine("        AND I.SOContractDocNo IS NOT NULL ");
            //SQL.AppendLine("    UNION ALL ");
            //SQL.AppendLine("    SELECT @EntCodeWhs2 AS EntCode, I.SOContractDocNo, ");
            //SQL.AppendLine("    F.Parvalue AcNo, 0.00 AS DAmt, ");
            //SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate CAmt ");
            //SQL.AppendLine("    FROM TblRecvWhs4Hdr A ");
            //SQL.AppendLine("    INNER JOIN TblRecvWhs4Dtl B ON A.DocNo = B.DocNo ");
            //SQL.AppendLine("        AND A.DocNo = @DocNo ");
            //SQL.AppendLine("    INNER JOIN TblStockPrice C ON B.Source = C.Source ");
            //SQL.AppendLine("    INNER JOIN TblItem D ON C.ItCode = D.ItCode ");
            //SQL.AppendLine("    INNER JOIN TblItemCategory E ON D.ItCtCode = E.ItCtCode AND E.MovingAvgInd = 'N' ");
            //SQL.AppendLine("    INNER JOIN TblParameter F ON F.ParCode = 'AcNoForRecvWhs4OfOffsetWIP' AND F.ParValue IS NOT NULL ");
            //SQL.AppendLine("    INNER JOIN TblDOWhsDtl G ON B.DOWhsDocNo = G.DocNo AND B.DOWhsDNo = G.DNo ");
            //SQL.AppendLine("    INNER JOIN TblDOWhsHdr H ON G.DocNO = H.DocNo ");
            //SQL.AppendLine("    INNER JOIN TblTransferRequestWhsHdr I ON H.TransferRequestWhsDocNo = I.DocNo ");
            //SQL.AppendLine("        AND H.TransferRequestWhsDocNo IS NOT NULL ");
            //SQL.AppendLine("        AND I.SOContractDocNo IS NOT NULL ");
            ////SQL.AppendLine("    INNER JOIN TblSOContractHdr J ON I.SOContractDocNo = J.DocNo And J.ProjectCode Is Not Null ");
            //SQL.AppendLine("    UNION ALL ");
            //SQL.AppendLine("    SELECT @EntCodeWhs2 AS EntCode, I.SOContractDocNo, ");
            //SQL.AppendLine("    F.Parvalue AcNo, ");
            //SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate DAmt, 0.00 As CAmt ");
            //SQL.AppendLine("    FROM TblRecvWhs4Hdr A ");
            //SQL.AppendLine("    INNER JOIN TblRecvWhs4Dtl B ON A.DocNo = B.DocNo ");
            //SQL.AppendLine("        AND A.DocNo = @DocNo ");
            //SQL.AppendLine("    INNER JOIN TblStockPrice C ON B.Source = C.Source ");
            //SQL.AppendLine("    INNER JOIN TblItem D ON C.ItCode = D.ItCode ");
            //SQL.AppendLine("    INNER JOIN TblItemCategory E ON D.ItCtCode = E.ItCtCode AND E.MovingAvgInd = 'N' ");
            //SQL.AppendLine("    INNER JOIN TblParameter F ON F.ParCode = 'AcNoForDOToProductOfWIP' AND F.ParValue IS NOT NULL ");
            //SQL.AppendLine("    INNER JOIN TblDOWhsDtl G ON B.DOWhsDocNo = G.DocNo AND B.DOWhsDNo = G.DNo ");
            //SQL.AppendLine("    INNER JOIN TblDOWhsHdr H ON G.DocNO = H.DocNo ");
            //SQL.AppendLine("    INNER JOIN TblTransferRequestWhsHdr I ON H.TransferRequestWhsDocNo = I.DocNo ");
            //SQL.AppendLine("        AND H.TransferRequestWhsDocNo IS NOT NULL ");
            //SQL.AppendLine("        AND I.SOContractDocNo IS NOT NULL ");
            ////SQL.AppendLine("    INNER JOIN TblSOContractHdr J ON I.SOContractDocNo = J.DocNo And J.ProjectCode Is Not Null ");
            //SQL.AppendLine("    UNION ALL ");
            //SQL.AppendLine("    SELECT @EntCodeWhs2 AS EntCode, I.SOContractDocNo, ");
            ////SQL.AppendLine("    CONCAT(F.Parvalue, '.', J.ProjectCode) AcNo, ");
            //SQL.AppendLine("    E.AcNo2 As AcNo, ");
            //SQL.AppendLine("    B.Qty*C.UPrice*C.ExcRate DAmt, 0.00 As CAmt ");
            //SQL.AppendLine("    FROM TblRecvWhs4Hdr A ");
            //SQL.AppendLine("    INNER JOIN TblRecvWhs4Dtl B ON A.DocNo = B.DocNo ");
            //SQL.AppendLine("        AND A.DocNo = @DocNo ");
            //SQL.AppendLine("    INNER JOIN TblStockPrice C ON B.Source = C.Source ");
            //SQL.AppendLine("    INNER JOIN TblItem D ON C.ItCode = D.ItCode ");
            //SQL.AppendLine("    INNER JOIN TblItemCategory E ON D.ItCtCode = E.ItCtCode AND E.MovingAvgInd = 'N' And E.AcNo2 Is Not Null ");
            ////SQL.AppendLine("    INNER JOIN TblParameter F ON F.ParCode = 'AcNoForRecvWhs4OfRawMaterial' AND F.ParValue IS NOT NULL ");
            //SQL.AppendLine("    INNER JOIN TblDOWhsDtl G ON B.DOWhsDocNo = G.DocNo AND B.DOWhsDNo = G.DNo ");
            //SQL.AppendLine("    INNER JOIN TblDOWhsHdr H ON G.DocNO = H.DocNo ");
            //SQL.AppendLine("    INNER JOIN TblTransferRequestWhsHdr I ON H.TransferRequestWhsDocNo = I.DocNo ");
            //SQL.AppendLine("        AND H.TransferRequestWhsDocNo IS NOT NULL ");
            //SQL.AppendLine("        AND I.SOContractDocNo IS NOT NULL ");
            ////SQL.AppendLine("    INNER JOIN TblSOContractHdr J ON I.SOContractDocNo = J.DocNo And J.ProjectCode Is Not Null ");

            //SQL.AppendLine("    UNION ALL ");

            //SQL.AppendLine("    SELECT @EntCodeWhs2 AS EntCode, I.SOContractDocNo, ");
            //SQL.AppendLine("    E.AcNo, 0.00 AS DAmt, ");
            //SQL.AppendLine("    B.Qty*IFNULL(F.MovingAvgPrice, 0.00) CAmt ");
            //SQL.AppendLine("    FROM TblRecvWhs4Hdr A ");
            //SQL.AppendLine("    INNER JOIN TblRecvWhs4Dtl B ON A.DocNo = B.DocNo ");
            //SQL.AppendLine("        AND A.DocNo = @DocNo ");
            //SQL.AppendLine("    INNER JOIN TblStockPrice C ON B.Source = C.Source ");
            //SQL.AppendLine("    INNER JOIN TblItem D ON C.ItCode = D.ItCode ");
            //SQL.AppendLine("    INNER JOIN TblItemCategory E ON D.ItCtCode = E.ItCtCode And E.AcNo Is Not Null AND E.MovingAvgInd = 'Y' ");
            //SQL.AppendLine("    INNER JOIN TblItemMovingAvg F On D.ItCode = F.ItCode ");
            //SQL.AppendLine("    INNER JOIN TblDOWhsDtl G ON B.DOWhsDocNo = G.DocNo AND B.DOWhsDNo = G.DNo ");
            //SQL.AppendLine("    INNER JOIN TblDOWhsHdr H ON G.DocNO = H.DocNo ");
            //SQL.AppendLine("    INNER JOIN TblTransferRequestWhsHdr I ON H.TransferRequestWhsDocNo = I.DocNo ");
            //SQL.AppendLine("        AND H.TransferRequestWhsDocNo IS NOT NULL ");
            //SQL.AppendLine("        AND I.SOContractDocNo IS NOT NULL ");
            //SQL.AppendLine("    UNION ALL ");
            //SQL.AppendLine("    SELECT @EntCodeWhs2 AS EntCode, I.SOContractDocNo, ");
            //SQL.AppendLine("    F.Parvalue AcNo, 0.00 AS DAmt, ");
            //SQL.AppendLine("    B.Qty*IFNULL(G1.MovingAvgPrice, 0.00) CAmt ");
            //SQL.AppendLine("    FROM TblRecvWhs4Hdr A ");
            //SQL.AppendLine("    INNER JOIN TblRecvWhs4Dtl B ON A.DocNo = B.DocNo ");
            //SQL.AppendLine("        AND A.DocNo = @DocNo ");
            //SQL.AppendLine("    INNER JOIN TblStockPrice C ON B.Source = C.Source ");
            //SQL.AppendLine("    INNER JOIN TblItem D ON C.ItCode = D.ItCode ");
            //SQL.AppendLine("    INNER JOIN TblItemCategory E ON D.ItCtCode = E.ItCtCode AND E.MovingAvgInd = 'Y' ");
            //SQL.AppendLine("    INNER JOIN TblParameter F ON F.ParCode = 'AcNoForRecvWhs4OfOffsetWIP' AND F.ParValue IS NOT NULL ");
            //SQL.AppendLine("    INNER JOIN TblItemMovingAvg G1 On D.ItCode = G1.ItCode ");
            //SQL.AppendLine("    INNER JOIN TblDOWhsDtl G ON B.DOWhsDocNo = G.DocNo AND B.DOWhsDNo = G.DNo ");
            //SQL.AppendLine("    INNER JOIN TblDOWhsHdr H ON G.DocNO = H.DocNo ");
            //SQL.AppendLine("    INNER JOIN TblTransferRequestWhsHdr I ON H.TransferRequestWhsDocNo = I.DocNo ");
            //SQL.AppendLine("        AND H.TransferRequestWhsDocNo IS NOT NULL ");
            //SQL.AppendLine("        AND I.SOContractDocNo IS NOT NULL ");
            ////SQL.AppendLine("    INNER JOIN TblSOContractHdr J ON I.SOContractDocNo = J.DocNo And J.ProjectCode Is Not Null ");
            //SQL.AppendLine("    UNION ALL ");
            //SQL.AppendLine("    SELECT @EntCodeWhs2 AS EntCode, I.SOContractDocNo, ");
            //SQL.AppendLine("    F.Parvalue AcNo, ");
            //SQL.AppendLine("    B.Qty*IFNULL(G1.MovingAvgPrice, 0.00) DAmt, 0.00 As CAmt ");
            //SQL.AppendLine("    FROM TblRecvWhs4Hdr A ");
            //SQL.AppendLine("    INNER JOIN TblRecvWhs4Dtl B ON A.DocNo = B.DocNo ");
            //SQL.AppendLine("        AND A.DocNo = @DocNo ");
            //SQL.AppendLine("    INNER JOIN TblStockPrice C ON B.Source = C.Source ");
            //SQL.AppendLine("    INNER JOIN TblItem D ON C.ItCode = D.ItCode ");
            //SQL.AppendLine("    INNER JOIN TblItemCategory E ON D.ItCtCode = E.ItCtCode AND E.MovingAvgInd = 'Y' ");
            //SQL.AppendLine("    INNER JOIN TblParameter F ON F.ParCode = 'AcNoForDOToProductOfWIP' AND F.ParValue IS NOT NULL ");
            //SQL.AppendLine("    INNER JOIN TblItemMovingAvg G1 On D.ItCode = G1.ItCode ");
            //SQL.AppendLine("    INNER JOIN TblDOWhsDtl G ON B.DOWhsDocNo = G.DocNo AND B.DOWhsDNo = G.DNo ");
            //SQL.AppendLine("    INNER JOIN TblDOWhsHdr H ON G.DocNO = H.DocNo ");
            //SQL.AppendLine("    INNER JOIN TblTransferRequestWhsHdr I ON H.TransferRequestWhsDocNo = I.DocNo ");
            //SQL.AppendLine("        AND H.TransferRequestWhsDocNo IS NOT NULL ");
            //SQL.AppendLine("        AND I.SOContractDocNo IS NOT NULL ");
            ////SQL.AppendLine("    INNER JOIN TblSOContractHdr J ON I.SOContractDocNo = J.DocNo And J.ProjectCode Is Not Null ");
            //SQL.AppendLine("    UNION ALL ");
            //SQL.AppendLine("    SELECT @EntCodeWhs2 AS EntCode, I.SOContractDocNo, ");
            ////SQL.AppendLine("    CONCAT(F.Parvalue, '.', J.ProjectCode) AcNo, ");
            //SQL.AppendLine("    E.AcNo2 As AcNo, ");
            //SQL.AppendLine("    B.Qty*IFNULL(G1.MovingAvgPrice, 0.00) DAmt, 0.00 As CAmt ");
            //SQL.AppendLine("    FROM TblRecvWhs4Hdr A ");
            //SQL.AppendLine("    INNER JOIN TblRecvWhs4Dtl B ON A.DocNo = B.DocNo ");
            //SQL.AppendLine("        AND A.DocNo = @DocNo ");
            //SQL.AppendLine("    INNER JOIN TblStockPrice C ON B.Source = C.Source ");
            //SQL.AppendLine("    INNER JOIN TblItem D ON C.ItCode = D.ItCode ");
            //SQL.AppendLine("    INNER JOIN TblItemCategory E ON D.ItCtCode = E.ItCtCode AND E.MovingAvgInd = 'Y' And E.AcNo2 Is Not Null ");
            ////SQL.AppendLine("    INNER JOIN TblParameter F ON F.ParCode = 'AcNoForRecvWhs4OfRawMaterial' AND F.ParValue IS NOT NULL ");
            //SQL.AppendLine("    INNER JOIN TblItemMovingAvg G1 On D.ItCode = G1.ItCode ");
            //SQL.AppendLine("    INNER JOIN TblDOWhsDtl G ON B.DOWhsDocNo = G.DocNo AND B.DOWhsDNo = G.DNo ");
            //SQL.AppendLine("    INNER JOIN TblDOWhsHdr H ON G.DocNO = H.DocNo ");
            //SQL.AppendLine("    INNER JOIN TblTransferRequestWhsHdr I ON H.TransferRequestWhsDocNo = I.DocNo ");
            //SQL.AppendLine("        AND H.TransferRequestWhsDocNo IS NOT NULL ");
            //SQL.AppendLine("        AND I.SOContractDocNo IS NOT NULL ");
            ////SQL.AppendLine("    INNER JOIN TblSOContractHdr J ON I.SOContractDocNo = J.DocNo And J.ProjectCode Is Not Null ");

            //SQL.AppendLine("    ) T ");
            //SQL.AppendLine("    Group By T.EntCode, T.AcNo ");

            //SQL.AppendLine(") B On 1=1 ");
            //SQL.AppendLine("Where A.DocNo=@JournalDocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            //Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(Sm.GetDte(DteDocDt), 1));
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@EntCodeWhs", Sm.GetEntityWarehouse(Sm.GetLue(LueWhsCode)));
            Sm.CmParam<String>(ref cm, "@EntCodeWhs2", Sm.GetEntityWarehouse(Sm.GetLue(LueWhsCode2)));
            Sm.CmParam<String>(ref cm, "@CCCodeWhs", GetCostCenterWarehouse(Sm.GetLue(LueWhsCode)));
            Sm.CmParam<String>(ref cm, "@CCCodeWhs2", GetCostCenterWarehouse(Sm.GetLue(LueWhsCode2)));

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowRecvWhs4Hdr(DocNo);
                ShowRecvWhs4Dtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowRecvWhs4Hdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select DocNo, DocDt, LocalDocNo, WhsCode, WhsCode2, KBContractNo, KBContractDt, KBPLNo, KBPLDt, KBRegistrationNo, KBRegistrationDt, Remark From TblRecvWhs4Hdr Where DocNo=@DocNo;",
                    new string[] 
                    { 
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "WhsCode", "WhsCode2", "Remark", "LocalDocNo",
 
                        //6-10
                        "KBContractNo", "KBContractDt", "KBPLNo", "KBPLDt", "KBRegistrationNo", 
                        
                        //11
                        "KBRegistrationDt"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        Sl.SetLueWhsCode(ref LueWhsCode, Sm.DrStr(dr, c[2]));
                        Sm.SetLue(LueWhsCode2, Sm.DrStr(dr, c[3]));
                        MeeRemark.EditValue = Sm.DrStr(dr, c[4]);
                        TxtLocalDocNo.EditValue = Sm.DrStr(dr, c[5]);
                        TxtKBContractNo.EditValue = Sm.DrStr(dr, c[6]);
                        Sm.SetDte(DteKBContractDt, Sm.DrStr(dr, c[7]));
                        TxtKBPLNo.EditValue = Sm.DrStr(dr, c[8]);
                        Sm.SetDte(DteKBPLDt, Sm.DrStr(dr, c[9]));
                        TxtKBRegistrationNo.EditValue = Sm.DrStr(dr, c[10]);
                        Sm.SetDte(DteKBRegistrationDt, Sm.DrStr(dr, c[11])); 
                    }, true
                );
        }

        private void ShowRecvWhs4Dtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DNo, A.CancelInd, A.DOWhsDocNo, A.DOWhsDNo, D.DocNo as DOWhs4DocNo, ");
            SQL.AppendLine("B.ItCode, C.ItName, A.BatchNo, A.Source, B.Lot, B.Bin, ");
            SQL.AppendLine("B.Qty As DOQty, A.Qty, B.Qty2 As DOQty2, A.Qty2, B.Qty3 As DOQty3, A.Qty3, ");
            SQL.AppendLine("C.InventoryUomCode, C.InventoryUomCode2, C.InventoryUomCode3, A.Remark, C.ItGrpCode, A.BatchNoOld, A.SourceOld, Z.DOWhsTRLocalDocNo ");
            SQL.AppendLine("From TblRecvWhs4Dtl A ");
            SQL.AppendLine("Inner Join TblDOWhsDtl B On A.DOWhsDocNo = B.DocNo And A.DOWhsDNo = B.DNo And A.DocNo = @DocNo ");
            SQL.AppendLine("Inner Join (Select T.DocNo, T.LocalDocNo As DOWhsTRLocalDocNo From TblDOWhsHdr T Where TransferRequestWhsDocNo Is Not Null) Z On B.DocNo = Z.DocNo ");
            SQL.AppendLine("Inner Join TblItem C On B.ItCode=C.ItCode ");
            SQL.AppendLine("Left Join TblDOWhs4Dtl D On D.DOWhsDocNo = B.DocNo And D.DOWhsDNo = B.DNo ");
            SQL.AppendLine("Order By A.DNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                    { 
                        //0
                        "DNo", 

                        //1-5
                        "CancelInd", "DOWhsDocNo", "DOWhsDNo", "ItCode", "ItName", 
                        
                        //6-10
                        "BatchNo", "Source", "Lot", "Bin", "DOQty",  
                        
                        //11-15
                        "Qty", "InventoryUomCode", "DOQty2", "Qty2", "InventoryUomCode2", 
                        
                        //16-20
                        "DOQty3", "Qty3", "InventoryUomCode3", "Remark", "ItGrpCode",
                        
                        //21-24
                        "BatchNoOld", "SourceOld", "DOWhsTRLocalDocNo", "DOWhs4DocNo"

                    },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 9);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 11);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 12);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 13);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 14);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 15);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 16);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 17);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 18);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 19);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 27, 20);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 28, 21);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 29, 22);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 30, 23);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 31, 24);
                }, false, false, true, false
            );
            Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 1, 2 });
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 14, 15, 17, 18, 20, 21, 24, 25, 26 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        #endregion

        #region Additional Method

        private string GetSOContractDocNo(string DocNo)
        {
            string SOContractDocNo = string.Empty;
            var SQL = new StringBuilder();

            SQL.AppendLine("    Select Group_Concat(Distinct IfNull(I.SOContractDocNo, '')) SOContractDocNo ");
            SQL.AppendLine("    FROM TblRecvWhs4Hdr A ");
            SQL.AppendLine("    INNER JOIN TblRecvWhs4Dtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("        AND A.DocNo = @Param ");
            SQL.AppendLine("    INNER JOIN TblDOWhsDtl G ON B.DOWhsDocNo = G.DocNo AND B.DOWhsDNo = G.DNo ");
            SQL.AppendLine("    INNER JOIN TblDOWhsHdr H ON G.DocNO = H.DocNo ");
            SQL.AppendLine("    INNER JOIN TblTransferRequestWhsHdr I ON H.TransferRequestWhsDocNo = I.DocNo ");
            SQL.AppendLine("        AND H.TransferRequestWhsDocNo IS NOT NULL ");
            SQL.AppendLine("        AND I.SOContractDocNo IS NOT NULL ");

            SOContractDocNo = Sm.GetValue(SQL.ToString(), DocNo);

            return SOContractDocNo;
        }

        private string GetCostCenterWarehouse(string WhsCode)
        {
            return Sm.GetValue("Select CCCode From TblWarehouse Where WhsCode=@Param;", WhsCode);
        }

        private bool IsEntityDifferent()
        {
            var EntCode1 = Sm.GetEntityWarehouse(Sm.GetLue(LueWhsCode));
            var EntCode2 = Sm.GetEntityWarehouse(Sm.GetLue(LueWhsCode2));
            return (EntCode1 != EntCode2);
        }

        private void GetParameter()
        {
            SetNumberOfInventoryUomCode();
            mIsItGrpCodeShow = Sm.GetParameterBoo("IsItGrpCodeShow");
            mIsRecvWhsWithTRCopyRemarkFromDO = Sm.GetParameterBoo("IsRecvWhsWithTRCopyRemarkFromDO");
            mIsAutoJournalActived = Sm.GetParameterBoo("IsAutoJournalActived");
            mIsEntityMandatory = Sm.GetParameterBoo("IsEntityMandatory");
            mIsKawasanBerikatEnabled = Sm.GetParameter("KB_Server").Length > 0;
            mIsRecvWhs4ProcessDOWhsPartialEnabled = Sm.GetParameterBoo("IsRecvWhs4ProcessDOWhsPartialEnabled");
            mIsMovingAvgEnabled = Sm.GetParameterBoo("IsMovingAvgEnabled");
            mIsRecvWhs4ProcessTo1Journal = Sm.GetParameterBoo("IsRecvWhs4ProcessTo1Journal");
            mIsCheckCOAJournalNotExists = Sm.GetParameterBoo("IsCheckCOAJournalNotExists");
            mDocTitle = Sm.GetParameter("DocTitle");
        }

        private void SetNumberOfInventoryUomCode()
        {
            string NumberOfInventoryUomCode = Sm.GetParameter("NumberOfInventoryUomCode");
            if (NumberOfInventoryUomCode.Length == 0)
                mNumberOfInventoryUomCode = 1;
            else
                mNumberOfInventoryUomCode = int.Parse(NumberOfInventoryUomCode);
        }

        internal string GetSelectedDO()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 4).Length != 0)
                        SQL +=
                            "##" +
                            Sm.GetGrdStr(Grd1, Row, 4) +
                            Sm.GetGrdStr(Grd1, Row, 5) +
                            "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        internal string GetSelectedItem()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 7).Length != 0)
                        SQL +=
                            "##" +
                            Sm.GetGrdStr(Grd1, Row, 7) +
                            Sm.GetGrdStr(Grd1, Row, 10) +
                            Sm.GetGrdStr(Grd1, Row, 11) +
                            Sm.GetGrdStr(Grd1, Row, 12) +
                            Sm.GetGrdStr(Grd1, Row, 13) +
                            "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        private void ReComputeStock(string WhsCode)
        {
            string Filter = string.Empty, Source = string.Empty, Lot = string.Empty, Bin = string.Empty;
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Source, Lot, Bin, Qty, Qty2, Qty3 ");
            SQL.AppendLine("From TblStockSummary ");
            SQL.AppendLine("Where WhsCode=@WhsCode ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand();
                cm.Connection = cn;
                Sm.CmParam<String>(ref cm, "@WhsCode", WhsCode);
                if (Grd1.Rows.Count != 1)
                {
                    int No = 1;
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    {
                        if (Sm.GetGrdStr(Grd1, Row, 11).Length != 0)
                        {
                            Sm.GenerateSQLConditionForInventory(ref cm, ref Filter, No, ref Grd1, Row, 11, 12, 13);
                            No += 1;
                        }
                    }
                }
                cm.CommandText = SQL.ToString() + " And (" + Filter + ")";
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                    { 
                        //0
                        "Source", 
                        
                        //1-5
                        "Lot", "Bin", "Qty", "Qty2", "Qty3" 
                    });

                if (dr.HasRows)
                {
                    Grd1.ProcessTab = true;
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        Source = Sm.DrStr(dr, 0);
                        Lot = Sm.DrStr(dr, 1);
                        Bin = Sm.DrStr(dr, 2);
                        for (int row = 0; row < Grd1.Rows.Count - 1; row++)
                        {
                            if (
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, row, 11), Source) &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, row, 12), Lot) &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, row, 13), Bin)
                                )
                            {
                                Sm.SetGrdValue("N", Grd1, dr, c, row, 24, 3);
                                Sm.SetGrdValue("N", Grd1, dr, c, row, 25, 4);
                                Sm.SetGrdValue("N", Grd1, dr, c, row, 26, 5);
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        private void ReComputeDOWhs()
        {
            string Filter = string.Empty, Filter2 = string.Empty, DocNo = string.Empty, DNo = string.Empty;
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            if (Grd1.Rows.Count >= 1)
            {
                for (int r = 0; r< Grd1.Rows.Count; r++)
                {
                    DocNo = Sm.GetGrdStr(Grd1, r, 4);
                    DNo = Sm.GetGrdStr(Grd1, r, 5);
                    if (DocNo.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(B.DocNo=@DocNo0" + r.ToString() + " And B.DNo=@DNo0" + r.ToString() + ") ";

                        if (Filter2.Length > 0) Filter2 += " Or ";
                        Filter2 += "(T2.DOWhsDocNo=@DocNo0" + r.ToString() + " And T2.DOWhsDNo=@DNo0" + r.ToString() + ") ";

                        Sm.CmParam<String>(ref cm, "@DocNo0" + r.ToString(), DocNo);
                        Sm.CmParam<String>(ref cm, "@DNo0" + r.ToString(), DNo);
                    }
                }
            }

            if (Filter.Length > 0)
            {
                Filter = " And ( " + Filter + ") ";
                Filter2 = " And ( " + Filter2 + ") ";
            }
            else
            {
                Filter = " And 1=0 ";
                Filter2 = Filter;
            }

            SQL.AppendLine("Select B.DocNo, B.DNo, ");
            SQL.AppendLine("B.Qty-IfNull(C.Qty, 0) As Qty, ");
            SQL.AppendLine("B.Qty2-IfNull(C.Qty2, 0) As Qty2, ");
            SQL.AppendLine("B.Qty3-IfNull(C.Qty3, 0) As Qty3 ");
            SQL.AppendLine("From TblDOWhsHdr A ");
            SQL.AppendLine("Inner Join TblDOWhsDtl B ");
            SQL.AppendLine("    On A.DocNo=B.DocNo ");
            SQL.AppendLine("    And B.CancelInd='N' ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.DOWhsDocNo, T2.DOWhsDNo, Sum(Qty) Qty, Sum(Qty2) Qty2, Sum(Qty3) Qty3 ");
            SQL.AppendLine("    From TblRecvWhs4Hdr T1 ");
            SQL.AppendLine("    Inner Join TblRecvWhs4Dtl T2 On T1.DocNo=T2.DocNo And T2.CancelInd='N' ");
            SQL.AppendLine(Filter2);
            SQL.AppendLine("    Group By T2.DOWhsDocNo, T2.DOWhsDNo ");
            SQL.AppendLine(") C On B.DocNo=C.DOWhsDocNo And B.DNo=C.DOWhsDNo ");
            SQL.AppendLine("Where A.Status= 'A' ");
            SQL.AppendLine("And (A.TransferRequestWhsDocNo Is Not Null Or A.TransferRequestWhs2DocNo Is Not Null) ");
            SQL.AppendLine("And A.CancelInd = 'N';");
            

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                    { 
                        //0
                        "DocNo", 
                        
                        //1-4
                        "DNo", "Qty", "Qty2", "Qty3" 
                    });

                if (dr.HasRows)
                {
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        DocNo = Sm.DrStr(dr, 0);
                        DNo = Sm.DrStr(dr, 1);
                        for (int row = 0; row < Grd1.Rows.Count - 1; row++)
                        {
                            if (
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, row, 4), DocNo) &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, row, 5), DNo)
                                )
                            {
                                Sm.SetGrdValue("N", Grd1, dr, c, row, 14, 2);
                                Sm.SetGrdValue("N", Grd1, dr, c, row, 17, 3);
                                Sm.SetGrdValue("N", Grd1, dr, c, row, 20, 4);
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue2(Sl.SetLueWhsCode), string.Empty);
            ClearGrd();
        }

        private void LueWhsCode2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode2, new Sm.RefreshLue1(Sl.SetLueWhsCode));
            ClearGrd();
        }

        #endregion

        #region Button Event

        private void BtnKBContractNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmRecvWhs4Dlg3(this));
        }

        #endregion

        #endregion
    }
}

