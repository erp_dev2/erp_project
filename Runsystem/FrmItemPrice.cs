﻿#region Update
/*
    19/02/2018 [TKG] saat memlih source, item yg ditampilkan diurutkan berdasarkan nama item.
    07/03/2018 [TKG] bug ketika menampilkan item dengan konversi 0.
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmItemPrice : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty, mDocNo = string.Empty;
        private bool mIsItemPriceFormatDNoCustomized = false;
        internal FrmItemPriceFind FrmFind;

        #endregion

        #region Constructor

        public FrmItemPrice(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Item's Price";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                Sl.SetLueCurCode(ref LueCurCode);
                SetLuePriceUomCode(ref LueUomCode);
                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }


        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 7;
            Grd1.FrozenArea.ColCount = 4;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",

                        //1-5
                        "",
                        "Item's Code",
                        "",
                        "Item's Name",
                        "Unit Price",

                        //6
                        "Remark"
                    },
                     new int[] 
                    {
                        0,
                        20, 100, 20, 300, 120,
                        300
                    }
                );
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 2, 4 });
            Sm.GrdColButton(Grd1, new int[] { 1, 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 5 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 3 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 4 });
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 3 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, ChkActInd, DteStartDt, LueCurCode, LueUomCode, 
                        MeeRemark
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 5, 6 });
                    BtnSourcePrice.Enabled = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { DteDocDt, DteStartDt, LueCurCode, LueUomCode, MeeRemark }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1, 5, 6 });
                    BtnSourcePrice.Enabled = true;
                    DteDocDt.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, DteStartDt, LueCurCode, LueUomCode, 
                TxtDocNoSource, MeeRemark
            });
            ChkActInd.Checked = false;
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ClearGrd()
        {
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 5 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmItemPriceFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                DteStartDt.DateTime = DteDocDt.DateTime;
                Sm.SetLue(LueCurCode, Sm.GetParameter("MainCurCode"));
                ChkActInd.Checked = true;
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (Sm.StdMsgYN("Save", "", mMenuCode) == DialogResult.No || IsDataNotValid()) return;
                Cursor.Current = Cursors.WaitCursor;

                string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "ItemPrice", "TblItemPriceHdr");

                var cml = new List<MySqlCommand>();

                cml.Add(SaveItemPriceHdr(DocNo));
                if (Grd1.Rows.Count > 1)
                    for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                        if (Sm.GetGrdStr(Grd1, Row, 2).Length > 0) cml.Add(SaveItemPriceDtl(DocNo, Row));
                
                Sm.ExecCommands(cml);
                
                ShowData(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            if (e.ColIndex == 5)
            {
                Grd1.Cells[e.RowIndex, 5].Value = Math.Truncate(Sm.GetGrdDec(Grd1, e.RowIndex, 5)*10000)/10000;
            }
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmItemPriceDlg(this));
            }

            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }

            if (!IsItCodeEmpty(e) && Sm.IsGrdColSelected(new int[] { 1, 5, 7 }, e.ColIndex) && BtnSave.Enabled)
                Sm.GrdRequestEdit(Grd1, e.RowIndex);

        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdEnter(Grd1, e);
            Sm.GrdTabInLastCell(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 1 && TxtDocNo.Text.Length == 0)
            {
                Sm.FormShowDialog(new FrmItemPriceDlg(this));
            }
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (e.ColIndex == 5)
            {
                if (Sm.StdMsgYN("Question", "Do you want to copy price based on first record ?") == DialogResult.Yes)
                {
                    if (Sm.GetGrdStr(Grd1, 0, 5).Length != 0)
                    {
                        var UPrice = Sm.GetGrdDec(Grd1, 0, 5);
                        for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        {
                            if (Sm.GetGrdStr(Grd1, Row, 2).Length != 0)
                                Grd1.Cells[Row, 5].Value = UPrice;
                        }
                    }
                }
            }
        }

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowItemPriceHdr(DocNo);
                ShowItemPriceDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }

        }

        private void ShowItemPriceHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                         ref cm,
                          "Select DocNo, DocDt, ActInd, StartDt, CurCode, PriceUomCode, Remark " +
                          "From TblItemPricehdr Where DocNo = @DocNo;",
                         new string[] 
                           {
                              //0
                              "DocNo",
                              //1-5
                              "DocDt", "ActInd", "StartDt", "PriceUomCode", "CurCode",
                              //6
                              "Remark",

                           },
                         (MySqlDataReader dr, int[] c) =>
                         {
                             TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                             Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                             ChkActInd.Checked = Sm.DrStr(dr, c[2]) == "Y";
                             Sm.SetDte(DteStartDt, Sm.DrStr(dr, c[3]));
                             Sm.SetLue(LueUomCode, Sm.DrStr(dr, c[4]));
                             Sm.SetLue(LueCurCode, Sm.DrStr(dr, c[5]));
                             MeeRemark.EditValue = Sm.DrStr(dr, c[6]);

                         }, true
                     );
        }

        private void ShowItemPriceDtl(string DocNo)
        {
            var cm = new MySqlCommand();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    "Select A.DNo,A.ItCode,B.ItName, A.UPrice,A.Remark "+
                    "From TblItemPriceDtl A "+
                    "Left Join TblItem B On A.ItCode=B.ItCode "+
                    "Where A.DocNo=@DocNo Order By B.ItName",

                    new string[] 
                       { 
                           //0
                           "DNo",
                           //1-5
                           "ItCode","ItName","UPrice","Remark",

                       },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 0, 0);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("N", Grd1, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("S", Grd1, dr, c, Row, 6, 4);


                    }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 5 });
            Sm.FocusGrd(Grd1, 0, 1);
        }



        #endregion

        #region Save Data

        private bool IsDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsDteEmpty(DteStartDt, "Start Date") ||
                Sm.IsLueEmpty(LueCurCode, "Currency") ||
                Sm.IsLueEmpty(LueUomCode, "UoM") ||
                IsGrd1Empty() ||
                (!mIsItemPriceFormatDNoCustomized && IsGrdExceedMaxRecords()) ||
                IsGrd1ValueNotValid();
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Item data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsGrd1Empty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 record in Item Price list.");
                return true;
            }
            return false;
        }

        private bool IsGrd1ValueNotValid()
        {
            if (Grd1.Rows.Count > 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.IsGrdValueEmpty(Grd1, Row, 2, false, "Item Name is empty.")) return true;
                    if (Sm.IsGrdValueEmpty(Grd1, Row, 5, true, "Unit Price is empty.")) return true;
                }
            }
            return false;
        }

        private MySqlCommand SaveItemPriceHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblItemPriceHdr Set ActInd='N' Where ActInd='Y' And CurCode=@CurCode And PriceUomCode=@PriceUomCode; ");

            SQL.AppendLine("Insert Into TblItemPriceHdr(DocNo, DocDt, ActInd, StartDt, CurCode, PriceUomCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@DocNo, @DocDt, @ActInd, @StartDt, @CurCode, @PriceUomCode, @Remark, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand(){ CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked?"Y":"N");
            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<String>(ref cm, "@PriceUomCode", Sm.GetLue(LueUomCode));
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveItemPriceDtl(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblItemPriceDtl(DocNo,DNo,ItCode,UPrice,Remark, CreateBy, CreateDt) " +
                    "Values (@DocNo,@DNo,@ItCode,@UPrice,@Remark, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            if (mIsItemPriceFormatDNoCustomized)
                Sm.CmParam<String>(ref cm, "@DNo", (Row + 1).ToString());
            else
                Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<Decimal>(ref cm, "@UPrice", Sm.GetGrdDec(Grd1, Row, 5));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 6));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mIsItemPriceFormatDNoCustomized = Sm.GetParameter("IsItemPriceFormatDNoCustomized") == "Y";
        }

        public void ShowItemPrice(string DocNo)
        {
            string UomCodeNew = Sm.GetLue(LueUomCode);
            string ConvertHarga; string ConvertHarga2;
            decimal ConvertUom = 0m; decimal ConvertPrice = 0m;  decimal Price = 0m;

            var SQL = new StringBuilder();
            SQL.AppendLine("Select B.ItCode, C.ItName, B.Uprice, A.PriceUomCode, A.CurCode ");
            SQL.AppendLine("From TblItemPriceHdr A ");
            SQL.AppendLine("Inner Join TblItemPriceDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblItem C On B.ItCode=C.ItCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("Order By C.ItName;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "ItCode", 
                    
                    //1-4
                    "ItName", "Uprice", "PriceUomCode", "CurCode"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 1);

                    if (Sm.DrStr(dr, c[4]) == Sm.GetLue(LueCurCode))
                    {
                        if (UomCodeNew == Sm.DrStr(dr, c[3]))
                        {
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 2);
                        }
                        else
                        {
                            string SalesUomCode = Sm.GetValue("Select SalesUomCode From Tblitem Where ItCode = '" + Sm.DrStr(dr, c[0]) + "'");
                            if (SalesUomCode == Sm.DrStr(dr, c[3]))
                            {
                                ConvertUom = Decimal.Parse(Sm.GetValue("Select SalesUomCodeConvert12 From Tblitem Where ItCode = '" + Sm.DrStr(dr, c[0]) + "' "));
                                if (ConvertUom != 0m)
                                    Price = Sm.DrDec(dr, c[2]) / ConvertUom;
                                else
                                    Price = 0m;
                                Grd.Cells[Row, 5].Value = Math.Truncate(Price * 10000) / 10000;
                            }
                            else
                            {
                                ConvertUom = Decimal.Parse(Sm.GetValue("Select SalesUomCodeConvert21 From Tblitem Where ItCode = '" + Sm.DrStr(dr, c[0]) + "' "));
                                if (ConvertUom != 0m)
                                    Price = Sm.DrDec(dr, c[2]) / ConvertUom;
                                else
                                    Price = 0m;
                                Grd.Cells[Row, 5].Value = Math.Truncate(Price * 10000) / 10000;
                            }
                        }
                    }
                    else
                    {
                        ConvertHarga = 
                            Sm.GetValue("Select ifnull(Amt, 0) As Amt From TblCurrencyRate Where CurCode1='" + Sm.DrStr(dr, c[4]) + "' And CurCode2= '" + Sm.GetLue(LueCurCode) + "' "+
                            "And RateDt In (Select MAX(RateDt) From TblCurrencyRate Where CurCode1='" + Sm.DrStr(dr, c[4]) + "' And CurCode2= '" + Sm.GetLue(LueCurCode) + "' )");
                        ConvertHarga2 =
                            Sm.GetValue("Select ifnull(Amt, 0) As Amt From TblCurrencyRate Where CurCode2='" + Sm.DrStr(dr, c[4]) + "' And CurCode1= '" + Sm.GetLue(LueCurCode) + "' " +
                            "And RateDt In (Select MAX(RateDt) From TblCurrencyRate Where CurCode2='" + Sm.DrStr(dr, c[4]) + "' And CurCode1= '" + Sm.GetLue(LueCurCode) + "' )");

                        //nilai konversi
                        if (ConvertHarga.Length != 0)
                        {
                            ConvertPrice = Decimal.Parse(ConvertHarga);
                        }
                        else
                        {
                            if (ConvertHarga2.Length != 0)
                            {
                                ConvertPrice = 1 / Decimal.Parse(ConvertHarga2);
                            }
                            else
                            {
                                ConvertPrice = 0m;
                            }
                        }


                        //kalkulasi hasil konversi
                        if (UomCodeNew == Sm.DrStr(dr, c[3]))
                        {
                            Grd.Cells[Row, 5].Value = Math.Truncate((ConvertPrice * Sm.DrDec(dr, c[2])) * 10000) / 10000;
                        }
                        else
                        {
                            string SalesUomCode = Sm.GetValue("Select SalesUomCode From Tblitem Where ItCode = '" + Sm.DrStr(dr, c[0]) + "'");
                            if (SalesUomCode == Sm.DrStr(dr, c[3]))
                            {
                                ConvertUom = Decimal.Parse(Sm.GetValue("Select SalesUomCodeConvert12 From Tblitem Where ItCode = '" + Sm.DrStr(dr, c[0]) + "' "));
                                if (ConvertUom > 0)
                                {
                                    Price = (Sm.DrDec(dr, c[2]) / ConvertUom) * ConvertPrice;
                                    Grd.Cells[Row, 5].Value = Math.Truncate(Price * 10000) / 10000;
                                }
                                else
                                {
                                    Grd.Cells[Row, 5].Value = 0;
                                }
                            }
                            else
                            {
                                ConvertUom = Decimal.Parse(Sm.GetValue("Select SalesUomCodeConvert21 From Tblitem Where ItCode = '" + Sm.DrStr(dr, c[0]) + "' "));
                                if (ConvertUom > 0)
                                {
                                    Price = (Sm.DrDec(dr, c[2]) / ConvertUom) * ConvertPrice;
                                    Grd.Cells[Row, 5].Value = Math.Truncate(Price * 10000) / 10000;
                                }
                                else
                                {
                                    Grd.Cells[Row, 5].Value = 0;
                                }
                            }
                        }

                    }

                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 5 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void SetLuePriceUomCode(ref DXE.LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct T.Col1, T.Col2 ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select A.SalesUomCode As Col1, B.UomName As Col2  ");
            SQL.AppendLine("    From Tblitem  A ");
            SQL.AppendLine("    Inner Join TblUom B On A.SalesUomCode = B.UomCode ");
            SQL.AppendLine("    Where A.SalesItemInd = 'Y'  ");
            SQL.AppendLine("    UNION ALL ");
            SQL.AppendLine("    Select  A.SalesUomCode2 As Col1, B.UomName As Col2  ");
            SQL.AppendLine("    From Tblitem  A ");
            SQL.AppendLine("    Inner Join TblUom B On A.SalesUomCode2 = B.UomCode ");
            SQL.AppendLine("    Where A.SalesItemInd = 'Y' ");
            SQL.AppendLine(")T  ");

            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }


        internal string GetSelectedItem()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 2).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL += "'" + Sm.GetGrdStr(Grd1, Row, 2) + "'";
                    }
                }
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }

        internal string GetSelectedItem2()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 10).Length != 0)
                    {
                        if (SQL.Length != 0) SQL += ", ";
                        SQL +=
                            "'" +
                            Sm.GetGrdStr(Grd1, Row, 10) +
                            "'";
                    }
                }
            }
            return (SQL.Length == 0 ? "'XXX'" : SQL);
        }

        private bool IsItCodeEmpty(iGRequestEditEventArgs e)
        {
            if (Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length == 0)
            {
                e.DoDefault = false;
                return true;
            }
            return false;
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void BtnSourcePrice_Click(object sender, EventArgs e)
        {
            if (!Sm.IsLueEmpty(LueUomCode, "UoM"))
            {
                TxtDocNoSource.Text = string.Empty;
                Sm.ClearGrd(Grd1, true);
                Sm.FormShowDialog(new FrmItemPriceDlg2(this));
            }
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtDocNo);
        }
        private void LueCurCode_EditValueChanged(object sender, EventArgs e)
        {
            TxtDocNoSource.Text = string.Empty;
            Sm.ClearGrd(Grd1, true);
            Sm.RefreshLookUpEdit(LueCurCode, new Sm.RefreshLue1(Sl.SetLueCurCode));
        }

        private void MeeRemark_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtDocNo);
        }

        private void LueUomCode_EditValueChanged(object sender, EventArgs e)
        {
            TxtDocNoSource.Text = string.Empty;
            Sm.ClearGrd(Grd1, true);
            Sm.RefreshLookUpEdit(LueUomCode, new Sm.RefreshLue1(SetLuePriceUomCode));
        }


        #endregion

        #endregion        
    }
}
