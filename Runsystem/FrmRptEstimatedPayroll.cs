﻿#region Update
/* 
    09/03/2018 [TKG] New reporting 
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptEstimatedPayroll : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty;
        private bool mIsFilterBySiteHR = false;
        private decimal mNoWorkDayPerMth = 0m;
        private List<AD> l;

        #endregion

        #region Constructor

        public FrmRptEstimatedPayroll(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                Sl.SetLueSiteCode(ref LueSiteCode, string.Empty, mIsFilterBySiteHR ? "Y" : "N");
                l = new List<AD>();
                SetAD(ref l);
                SetGrd();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mIsFilterBySiteHR = Sm.GetParameterBoo("IsFilterBySiteHR");
            mNoWorkDayPerMth = Sm.GetParameterDec("NoWorkDayPerMth");
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 5 + l.Count;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No",

                    //1-4
                    "Site Code",
                    "Site Name",
                    "Salary",
                    "Brutto"
                },
                new int[] 
                {
                    //0
                    50,

                    //1-4
                    0, 200, 130, 130
                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 3, 4 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, false);
            for (int i = 0; i < l.Count; i++)
            {
                Grd1.Cols[5 + i].Text = l[i].ADName.Trim().Replace(" ", Environment.NewLine);
                Grd1.Cols[5 + i].Width = 150;
                Grd1.Header.Cells[0, 5 + i].TextAlign = iGContentAlignment.TopCenter;
                Grd1.Cols[5 + i].CellStyle.ValueType = typeof(Decimal);
                Grd1.Cols[5 + i].CellStyle.TextAlign = iGContentAlignment.TopRight;
                Grd1.Cols[5 + i].CellStyle.FormatString = (Gv.FormatNum0.Length != 0) ? Gv.FormatNum0 : "{0:#,##0.00##}";
            }
            Grd1.Cols[4].Move(Grd1.Cols.Count-1);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                Process1();
                if (Grd1.Rows.Count > 0)
                {
                    Process2();
                    Process3();
                    for (int c = 3; c < Grd1.Cols.Count; c++)
                    {
                        iGSubtotalManager.BackColor = Color.LightSalmon;
                        iGSubtotalManager.ShowSubtotalsInCells = true;
                        iGSubtotalManager.ShowSubtotals(Grd1, new int[] { c });
                    }
                }
                else
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Grd1.Cols.AutoWidth();
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        private void Process1()
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            string Filter = " ";

            SQL.AppendLine("Select A.SiteCode, A.SiteName, ");
            SQL.AppendLine("B.Amt, IfNull(B.Amt, 0.00)+IfNull(C.Amt, 0.00)-IfNull(D.Amt, 0.00) As Brutto ");
            SQL.AppendLine("From TblSite A ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T2.SiteCode, Sum(T1.Amt) As Amt ");
            SQL.AppendLine("    From TblEmployeeSalary T1 ");
            SQL.AppendLine("    Inner Join TblEmployee T2 ");
            SQL.AppendLine("        On T1.EmpCode=T2.EmpCode ");
            SQL.AppendLine("        And (T2.ResignDt Is Null Or (T2.ResignDt Is Not Null And T2.ResignDt>@CurrentDate)) ");
            SQL.AppendLine("    Where T1.ActInd='Y' ");
            SQL.AppendLine("    Group By T2.SiteCode ");
            SQL.AppendLine(") B On A.SiteCode=B.SiteCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T.SiteCode, Sum(T.Amt) Amt From ( ");
            SQL.AppendLine("        Select T3.SiteCode, T1.Amt ");
            SQL.AppendLine("        From TblEmployeeAllowanceDeduction T1 ");
            SQL.AppendLine("        Inner Join TblAllowanceDeduction T2 On T1.ADCode=T2.ADCode And T2.AmtType='1' And T2.ADType='A' ");
            SQL.AppendLine("        Inner Join TblEmployee T3 ");
            SQL.AppendLine("            On T1.EmpCode=T3.EmpCode ");
            SQL.AppendLine("            And (T3.ResignDt Is Null Or (T3.ResignDt Is Not Null And T3.ResignDt>@CurrentDate)) ");
            SQL.AppendLine("        Where T1.StartDt<=@CurrentDate And @CurrentDate<=T1.EndDt ");
            SQL.AppendLine("    Union All ");
            SQL.AppendLine("        Select T2.SiteCode, IfNull(@NoWorkDayPerMth, 0.00)*T1.Amt As Amt ");
            SQL.AppendLine("        From TblEmployeeAllowanceDeduction T1 ");
            SQL.AppendLine("        Inner Join TblEmployee T2 ");
            SQL.AppendLine("            On T1.EmpCode=T2.EmpCode ");
            SQL.AppendLine("            And (T2.ResignDt Is Null Or (T2.ResignDt Is Not Null And T2.ResignDt>@CurrentDate)) ");
            SQL.AppendLine("        Where T1.StartDt<=@CurrentDate And @CurrentDate<=T1.EndDt ");
            SQL.AppendLine("        And T1.ADCode In (Select ParValue From TblParameter Where ParCode In ('ADCodeMeal', 'ADCodeTransport') And ParValue Is Not Null) ");
            SQL.AppendLine("    ) T Group By T.SiteCode ");
            SQL.AppendLine(") C On A.SiteCode=C.SiteCode ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select T3.SiteCode, Sum(T1.Amt) As Amt ");
            SQL.AppendLine("    From TblEmployeeAllowanceDeduction T1 ");
            SQL.AppendLine("    Inner Join TblAllowanceDeduction T2 On T1.ADCode=T2.ADCode And T2.AmtType='1' And T2.ADType='D' ");
            SQL.AppendLine("    Inner Join TblEmployee T3 ");
            SQL.AppendLine("        On T1.EmpCode=T3.EmpCode ");
            SQL.AppendLine("        And (T3.ResignDt Is Null Or (T3.ResignDt Is Not Null And T3.ResignDt>@CurrentDate)) ");
            SQL.AppendLine("    Where T1.StartDt<=@CurrentDate And @CurrentDate<=T1.EndDt ");
            SQL.AppendLine("    Group By T3.SiteCode ");
            SQL.AppendLine(") D On A.SiteCode=D.SiteCode ");
            SQL.AppendLine("Where A.ActInd='Y' ");
            if (mIsFilterBySiteHR)
            {
                SQL.AppendLine("    And A.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select SiteCode From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(A.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ) ");
            }

            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<Decimal>(ref cm, "@NoWorkDayPerMth", mNoWorkDayPerMth);
            Sm.CmParamDt(ref cm, "@CurrentDate", Sm.Left(Sm.ServerCurrentDateTime(), 8));
            Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueSiteCode), "A.SiteCode", true);
            

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString() + Filter + " Order By A.SiteName;",
                new string[] { 
                    //0
                    "SiteCode", 

                    //1-3
                    "SiteName", "Amt", "Brutto"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Grd.Cells[Row, 0].Value = Row+1;
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                }, true, false, false, false
            );
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 3, 4 });
        }

        private void Process2()
        {
            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                for (int c = 5; c < Grd1.Cols.Count; c++)
                    Grd1.Cells[r, c].Value = 0m;
            }
        }

        private void Process3()
        {
            string
                Filter = string.Empty,
                SiteCode = string.Empty,
                ADCode = string.Empty,
                SiteCodeTemp = string.Empty,
                ADCodeTemp = string.Empty;
            decimal Amt = 0m;
            int r = 0;
            var cm = new MySqlCommand();
            Sm.CmParamDt(ref cm, "@CurrentDate", Sm.Left(Sm.ServerCurrentDateTime(), 8));
            Sm.CmParam<Decimal>(ref cm, "@NoWorkDayPerMth", mNoWorkDayPerMth);

            if (Grd1.Rows.Count >= 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    SiteCode = Sm.GetGrdStr(Grd1, Row, 1);
                    if (SiteCode.Length != 0)
                    {
                        if (Filter.Length > 0) Filter += " Or ";
                        Filter += "(B.SiteCode=@SiteCode0" + Row.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@SiteCode0" + Row.ToString(), SiteCode);
                    }
                }
            }

            if (Filter.Length > 0)
                Filter = " And (" + Filter + ") ";
            else
                Filter = " And 0=1 ";

            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.SiteCode, T.ADCode, Sum(T.Amt) Amt ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("   Select B.SiteCode, A.ADCode, A.Amt ");
            SQL.AppendLine("   From TblEmployeeAllowanceDeduction A ");
            SQL.AppendLine("   Inner Join TblEmployee B On A.EmpCode=B.EmpCode  ");
            SQL.AppendLine("       And (B.ResignDt Is Null Or (B.ResignDt Is Not Null And B.ResignDt>@CurrentDate)) ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("   Inner Join TblAllowanceDeduction C On A.ADCode=C.ADCode And C.AmtType='1' ");
            SQL.AppendLine("   Where A.StartDt<=@CurrentDate And @CurrentDate<=A.EndDt ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("   Select B.SiteCode, A.ADCode, IfNull(@NoWorkDayPerMth, 0.00)*A.Amt As Amt ");
            SQL.AppendLine("   From TblEmployeeAllowanceDeduction A ");
            SQL.AppendLine("   Inner Join TblEmployee B On A.EmpCode=B.EmpCode  ");
            SQL.AppendLine("       And (B.ResignDt Is Null Or (B.ResignDt Is Not Null And B.ResignDt>@CurrentDate)) ");
            SQL.AppendLine(Filter);
            SQL.AppendLine("   Where A.StartDt<=@CurrentDate And @CurrentDate<=A.EndDt ");
            SQL.AppendLine("   And A.ADCode In (Select ParValue From TblParameter Where ParCode In ('ADCodeMeal', 'ADCodeTransport') And ParValue Is Not Null) ");
            SQL.AppendLine(") T Group By T.SiteCode, T.ADCode Order By T.SiteCode;");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "SiteCode", "ADCode", "Amt" });
                if (dr.HasRows)
                {
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        SiteCode = dr.GetString(c[0]);
                        ADCode = dr.GetString(c[1]);
                        Amt = dr.GetDecimal(c[2]);

                        if (Sm.CompareStr(SiteCode, SiteCodeTemp))
                        {
                            for (int x = 0; x < l.Count; x++)
                            {
                                if (Sm.CompareStr(ADCode, l[x].ADCode))
                                {
                                    Grd1.Cells[r, l[x].Col].Value = Sm.GetGrdDec(Grd1, r, l[x].Col) + Amt;
                                    break;
                                }
                            }
                        }
                        else
                        {
                            for (int i = 0; i < Grd1.Rows.Count; i++)
                            {
                                if (Sm.CompareStr(SiteCode, Sm.GetGrdStr(Grd1, i, 1)))
                                {
                                    for (int x = 0; x < l.Count; x++)
                                    {
                                        if (Sm.CompareStr(ADCode, l[x].ADCode))
                                        {
                                            Grd1.Cells[i, l[x].Col].Value = Sm.GetGrdDec(Grd1, i, l[x].Col) + Amt;
                                            break;
                                        }
                                    }
                                    r = i;
                                    break;
                                }
                            }
                            SiteCodeTemp = SiteCode;
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        private void SetAD(ref List<AD> l)
        {
            var GrdCol = 4;
            var cm = new MySqlCommand();
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = 
                    "Select ADCode, ADName From TblAllowanceDeduction " +
                    "Where (AmtType='1' Or ADCode In (Select ParValue From TblParameter Where ParCode In ('ADCodeMeal', 'ADCodeTransport') And ParValue Is Not Null)) " +
                    "Order By ADType, ADName;";
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ADCode", "ADName" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        GrdCol += 1;
                        l.Add(new AD()
                        {
                            ADCode = Sm.DrStr(dr, c[0]),
                            ADName = Sm.DrStr(dr, c[1]),
                            Col = GrdCol
                        });
                    }
                }
                dr.Close();
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueSiteCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueSiteCode, new Sm.RefreshLue3(Sl.SetLueSiteCode), string.Empty, mIsFilterBySiteHR ? "Y" : "N");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkSiteCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Site");
        }

        #endregion

        #endregion

        #region Class

        private class AD
        {
            public string ADCode { get; set; }
            public string ADName { get; set; }
            public int Col { get; set; }
        }

        #endregion

    }
}
