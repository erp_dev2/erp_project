﻿#region Update
/*
    18/11/2019 [WED/IMS] Delivery Date SO Contract di copy ke form main
    27/11/2019 [VIN/IMS] tambah kolom project code, project name, customer po#
    06/01/2020 [WED/IMS] SOContractRevision dihapus dari left join
    30/01/2020 [TKG/IMS] tambah project code, project name, po# (tidak perlu dari NTP)
    20/02/2020 [WED/IMS] salah query ProcessInd di non CBD nya
    10/06/2020 [IBL/IMS] Tambah kolom SO Contract Remark
    01/07/2020 [HAR/IMS] feedback remark so contract ambil dari detail item SO Contract
    23/09/2020 [WED/IMS] tambah informasi kolom No dari SO Contract berdasarkan parameter IsDetailShowColumnNumber
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmDR2Dlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmDR2 mFrmParent;
        string mSQL = string.Empty, mCtCode = "", mAgtCode = "";
        bool mCBDInd = false;

        #endregion

        #region Constructor

        public FrmDR2Dlg(FrmDR2 FrmParent, string CtCode, string AgtCode, bool CBDInd)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mCtCode = CtCode;
            mAgtCode = AgtCode;
            mCBDInd = CBDInd;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -7);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            if (mCBDInd)
            {
                SQL.AppendLine("SELECT  ");
                SQL.AppendLine("A.SAName, B.ItCode, C.ItCodeInternal, C.ItName, C.Specification, B.PackagingUnitUomCode, ");
                SQL.AppendLine("A.DocNo, A.DocDt, B.DNo, B.PackagingUnitUomCode PriceUomCode, C.InventoryUomCode, B.DeliveryDt, ");
                SQL.AppendLine("B.QtyPackagingUnit, 0 As OutstandingQtyPackagingUnit, B.Qty, 0 As OutstandingQty, ");
                SQL.AppendLine("I.DocNo as VoucherDocNo, B.Amt As PriceAfterTax, A.LocalDocNo ");
                SQL.AppendLine(", IFNULL(L.ProjectName, E.ProjectName) ProjectName, ");
                SQL.AppendLine("IFNULL(L.ProjectCode, A.ProjectCode2) ProjectCode,  ");
                SQL.AppendLine("ifnull(A.PONo, K.DocNo) CustomerPO, B.Remark, B.No ");
                SQL.AppendLine("FROM TblSOContractHdr A ");
                SQL.AppendLine("INNER JOIN TblSOContractDtl B ON A.DocNo = B.DocNo ");
                SQL.AppendLine("    AND (A.DocDt BETWEEN @DocDt1 AND @DocDt2) ");
                SQL.AppendLine("    AND A.DocType = '2' ");
                SQL.AppendLine("    AND A.CancelInd = 'N' ");
                SQL.AppendLine("    AND A.Status = 'A' ");
                SQL.AppendLine("    AND A.ProcessIndForDR <> 'F' ");
                SQL.AppendLine("    AND B.ProcessIndForDRCBD <> 'F' ");
                //SQL.AppendLine("INNER JOIN ");
                //SQL.AppendLine("( ");
                //SQL.AppendLine("    SELECT X1.SOCDocNo, MAX(X1.DocNo) SOCRDocNo ");
                //SQL.AppendLine("    FROM TblSOContractRevisionHdr X1 ");
                //SQL.AppendLine("    INNER JOIN TblSOContractHdr X2 ON X1.SOCDocNo = X2.DocNo ");
                //SQL.AppendLine("        AND (X2.DocDt BETWEEN @DocDt1 AND @DocDt2) ");
                //SQL.AppendLine("        AND X2.DocType = '2' ");
                //SQL.AppendLine("        AND X2.CancelInd = 'N' ");
                //SQL.AppendLine("        AND X2.Status = 'A' ");
                //SQL.AppendLine("        AND X2.ProcessIndForDR <> 'F' ");
                //SQL.AppendLine("    GROUP BY X1.SOCDocNo ");
                //SQL.AppendLine(") B1 ON A.DocNo = B1.SOCDocNo ");
                //SQL.AppendLine("INNER JOIN TblSOContractRevisionDtl B2 ON B1.SOCRDocNo = B2.DocNo ");
                //SQL.AppendLine("    AND A.DocNo = B2.SOCDocNo ");
                //SQL.AppendLine("    AND B.DNo = B2.DNo ");
                SQL.AppendLine("INNER JOIN TblItem C ON B.ItCode = C.ItCode ");
                SQL.AppendLine("INNER JOIN TblBOQHdr D ON A.BOQDocNo = D.DocNo ");
                SQL.AppendLine("    AND D.ActInd = 'Y' ");
                SQL.AppendLine("    AND D.ProcessInd = 'F' ");
                SQL.AppendLine("    AND D.Status = 'A' ");
                SQL.AppendLine("INNER JOIN TblLOPHdr E ON D.LOPDocNo = E.DocNo AND E.CtCode = @CtCode ");
                SQL.AppendLine("    AND E.CancelInd = 'N' ");
                SQL.AppendLine("    AND E.Status = 'A' ");
                SQL.AppendLine("INNER JOIN TblSalesInvoiceHdr F ON A.DocNo = F.SODocNo ");
                SQL.AppendLine("INNER JOIN TblIncomingPaymentDtl G ON F.DocNo = G.InvoiceDocNo ");
                SQL.AppendLine("INNER JOIN TblIncomingPaymentHdr H ON G.DocNo = H.DocNo AND H.CancelInd = 'N' ");
                SQL.AppendLine("INNER JOIN TblVoucherHdr I ON I.VoucherRequestDocNo = H.VoucherRequestDocNo AND I.CancelInd = 'N' ");
                //SQL.AppendLine("LEFT  JOIN tblsocontractrevisionhdr J ON A.DocNo=J.SOCDocNo ");
            	SQL.AppendLine("LEFT  JOIN tblnoticetoproceed K ON E.DocNo=K.LOPDocNo ");
                SQL.AppendLine("left JOIN  tblprojectgroup L ON E.PGCode=L.PGCode ");
            }
            else
            {
                SQL.AppendLine("SELECT  ");
                SQL.AppendLine("A.SAName, B.ItCode, C.ItCodeInternal, C.ItName, C.Specification, B.PackagingUnitUomCode, ");
                SQL.AppendLine("A.DocNo, A.DocDt, B.DNo, B.PackagingUnitUomCode PriceUomCode, C.InventoryUomCode, B.DeliveryDt, ");
                SQL.AppendLine("B.QtyPackagingUnit, (B.QtyPackagingUnit - IFNULL(F.QtyPackagingUnit, 0)) AS OutstandingQtyPackagingUnit, ");
                SQL.AppendLine("B.Qty, (B.Qty - IFNULL(F.Qty, 0)) As OutstandingQty, ");
                SQL.AppendLine("null as VoucherDocNo, B.Amt As PriceAfterTax, A.LocalDocNo, ");
                SQL.AppendLine("I.ProjectName, I.ProjectCode, A.PONo As CustomerPO, B.Remark, B.No ");
                SQL.AppendLine("FROM TblSOContractHdr A ");
                SQL.AppendLine("INNER JOIN TblSOContractDtl B ON A.DocNo = B.DocNo ");
                SQL.AppendLine("    AND (A.DocDt BETWEEN @DocDt1 AND @DocDt2) ");
                SQL.AppendLine("    AND A.DocType = '2' ");
                SQL.AppendLine("    AND A.CancelInd = 'N' ");
                SQL.AppendLine("    AND A.Status = 'A' ");
                SQL.AppendLine("    AND A.ProcessIndForDR NOT IN ('M', 'F') ");
                SQL.AppendLine("    AND B.ProcessIndForDR <> 'F' ");
                SQL.AppendLine("    AND A.OverseaInd = 'N' ");
                SQL.AppendLine("    AND A.DocNo NOT IN ");
                SQL.AppendLine("    ( ");
                SQL.AppendLine("        SELECT SODocNo ");
                SQL.AppendLine("        FROM TblSalesInvoiceHdr ");
                SQL.AppendLine("        WHERE CancelInd = 'N' ");
                SQL.AppendLine("        AND SODocNo IS NOT NULL ");
                SQL.AppendLine("    ) ");
                if (mAgtCode.Length > 0)
                    SQL.AppendLine("    AND B.AgtCode = @AgtCode ");
                //SQL.AppendLine("INNER JOIN ");
                //SQL.AppendLine("( ");
                //SQL.AppendLine("    SELECT X1.SOCDocNo, MAX(X1.DocNo) SOCRDocNo ");
                //SQL.AppendLine("    FROM TblSOContractRevisionHdr X1 ");
                //SQL.AppendLine("    INNER JOIN TblSOContractHdr X2 ON X1.SOCDocNo = X2.DocNo ");
                //SQL.AppendLine("        AND (X2.DocDt BETWEEN @DocDt1 AND @DocDt2) ");
                //SQL.AppendLine("        AND X2.DocType = '2' ");
                //SQL.AppendLine("        AND X2.CancelInd = 'N' ");
                //SQL.AppendLine("        AND X2.Status = 'A' ");
                //SQL.AppendLine("        AND X2.ProcessIndForDR NOT IN ('M', 'F') ");
                //SQL.AppendLine("        AND X2.OverseaInd = 'N' ");
                //SQL.AppendLine("    GROUP BY X1.SOCDocNo ");
                //SQL.AppendLine(") B1 ON A.DocNo = B1.SOCDocNo ");
                //SQL.AppendLine("INNER JOIN TblSOContractRevisionDtl B2 ON B1.SOCRDocNo = B2.DocNo ");
                //SQL.AppendLine("    AND A.DocNo = B2.SOCDocNo ");
                //SQL.AppendLine("    AND B.DNo = B2.DNo ");
                SQL.AppendLine("INNER JOIN TblItem C ON B.ItCode = C.ItCode ");
                SQL.AppendLine("INNER JOIN TblBOQHdr D ON A.BOQDocNo = D.DocNo ");
                SQL.AppendLine("    AND D.ActInd = 'Y' ");
                SQL.AppendLine("    AND D.ProcessInd = 'F' ");
                SQL.AppendLine("    AND D.Status = 'A' ");
                SQL.AppendLine("INNER JOIN TblLOPHdr E ON D.LOPDocNo = E.DocNo AND E.CtCode = @CtCode ");
                SQL.AppendLine("    AND E.CancelInd = 'N' ");
                SQL.AppendLine("    AND E.Status = 'A' ");
                SQL.AppendLine("LEFT JOIN ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    SELECT T2.SODocNo AS DocNo, T2.SODNo AS DNo, ");
                SQL.AppendLine("    SUM(IFNULL(T2.QtyPackagingUnit, 0)) AS QtyPackagingUnit, SUM(IFNULL(T2.Qty, 0)) AS Qty ");
                SQL.AppendLine("    FROM TblDRHdr T1 ");
                SQL.AppendLine("    INNER JOIN TblDRDtl T2 ON T1.DocNo = T2.DocNo ");
                SQL.AppendLine("        AND T1.CancelInd = 'N' ");
                SQL.AppendLine("    INNER JOIN TblSOContractHdr T3 ON T2.SODocNo = T3.DocNo ");
                SQL.AppendLine("        AND T3.CancelInd = 'N' ");
                SQL.AppendLine("        AND T3.DocType = '2' ");
                SQL.AppendLine("        AND T3.ProcessIndForDR NOT IN ('M', 'F') ");
                SQL.AppendLine("    INNER JOIN TblBOQHdr T4 ON T3.BOQDocNo = T4.DocNo ");
                SQL.AppendLine("    INNER JOIN TblLOPHdr T5 ON T4.LOPDocNo = T5.DocNo AND T5.CtCode = @CtCode ");
                SQL.AppendLine("    GROUP BY T2.SODocNo, T2.SODNo ");
                SQL.AppendLine(") F ON A.DocNo = F.DocNo AND B.DNo = F.DNo ");
                //SQL.AppendLine("LEFT JOIN tblsocontractrevisionhdr G ON A.DocNo=G.SOCDocNo ");
                SQL.AppendLine("LEFT JOIN tblnoticetoproceed H on E.DocNo= H.LOPDocNo ");
                SQL.AppendLine("LEFT JOIN tblprojectgroup I ON E.PGCode=I.PGCode ");

            }


            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 27;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No",

                    //1-5
                    "",
                    "Shipping Name",
                    "Item's"+Environment.NewLine+"Code", 
                    "",
                    "Item's Name",

                    //6-10
                    "Local Code",
                    "Packaging"+Environment.NewLine+"UoM", 
                    "Quantity"+Environment.NewLine+"(Packaging)",
                    "Oustanding Quantity"+Environment.NewLine+"(Packaging)",
                    "",
                    
                    //11-15
                    "Sales Order#",
                    "SO"+Environment.NewLine+"Date",
                    "SO DNo",
                    "Quantity"+Environment.NewLine+"(Sales)",
                    "Outstanding Quantity"+Environment.NewLine+"(Sales)",
                    
                    //16-20
                    "UoM"+Environment.NewLine+"(Sales)",
                    "UoM"+Environment.NewLine+"(Inventory)",
                    "Delivery"+Environment.NewLine+"Date",
                    "Voucher",
                    "Price"+Environment.NewLine+"After Tax",

                    //21-25
                     mFrmParent.mLocalDocument=="1"?"Sales"+Environment.NewLine+"Contract#":"Local"+Environment.NewLine+"Document#",
                    
                    "Project Name",
                    "Project Code",
                    "Customer's PO#",
                    "SO Contract's Remark",

                    //26
                    "SO Contract's"+Environment.NewLine+"No"
                },
                new int[] 
                {
                    //0
                    50, 

                    //1-5
                    20, 200, 80, 20, 250,  
                    
                    //6-10
                    80, 100, 100, 100, 20, 
                    
                    //11-15
                    150, 130, 80, 100, 100,   
                    
                    //16-20
                    80, 80, 100, 100, 100,

                    //21-25
                    130, 100, 200, 150, 200,

                    //26
                    100
                }
            );
            
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdFormatDec(Grd1, new int[] { 8, 9, 14, 15, 20 }, 0 );
            Sm.GrdColButton(Grd1, new int[] { 4, 10 });
            Sm.GrdFormatDate(Grd1, new int[] { 12, 18 });
            Sm.GrdColReadOnly(Grd1, new int[] { 2, 3, 5, 6, 7, 8, 9, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26 });
            Grd1.Cols[21].Move(14);
            Grd1.Cols[26].Move(3);

            if (!mFrmParent.mIsDetailShowColumnNumber)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 26 });
            }
            
            if (mCBDInd == false)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 3, 4, 6, 13, 19, 20 }, false);
            }
            else
            {
                Sm.GrdColInvisible(Grd1, new int[] { 1, 3, 4, 6, 13, 19, 20 }, false);
            }
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 3, 4, 6, 13 }, !ChkHideInfoInGrd.Checked);
        }
    

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = "Where 0=0 ";
                
                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@CtCode", mCtCode);
                Sm.CmParam<String>(ref cm, "@AgtCode", mAgtCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "B.ItCode", "C.ItCodeInternal", "C.ItName" });

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By A.DocDt, A.DocNo, B.DNo;",
                    new string[]
                    {
                        //0
                        "SAName", 
                        //1-5
                        "ItCode", "ItName", "ItCodeInternal", "PackagingUnitUomCode", "QtyPackagingUnit",  
                        //6-10
                        "OutstandingQtyPackagingUnit", "DocNo", "DocDt", "DNo", "Qty",
                        //11-15
                        "OutstandingQty", "PriceUomCode", "InventoryUomCode", "DeliveryDt", "VoucherDocNo",
                        //16-20
                        "PriceAfterTax", "LocalDocNo", "ProjectName", "ProjectCode" , "CustomerPO",
                        //21-22
                        "Remark", "No"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Grd.Cells[Row, 1].Value = false;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 5);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 7);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 12, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 9);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 10);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 12);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 13);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 18, 14);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 15);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 16);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 17);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 18);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 23, 19);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 24, 20);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 21);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 22);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        protected override void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            ChooseData();
        }

        override protected void ChooseData()
        {

            if (mCBDInd == false)
            {
                int Row1 = 0, Row2 = 0;
                bool IsChoose = false;

                if (Grd1.Rows.Count != 0)
                {
                    mFrmParent.Grd1.BeginUpdate();
                    for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                    {
                        if (Sm.GetGrdBool(Grd1, Row, 1) && !IsItemAlreadyChosen(Row))
                        {
                            if (IsChoose == false) IsChoose = true;

                            Row1 = mFrmParent.Grd1.Rows.Count - 1;
                            Row2 = Row;

                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 1, Grd1, Row2, 3);
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 2, Grd1, Row2, 5);
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 4, Grd1, Row2, 6);
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 5, Grd1, Row2, 11);
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 7, Grd1, Row2, 13);
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 8, Grd1, Row2, 7);
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 9, Grd1, Row2, 9);
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 10, Grd1, Row2, 9);
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 12, Grd1, Row2, 15);
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 13, Grd1, Row2, 15);
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 15, Grd1, Row2, 16);
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 18, Grd1, Row2, 17);
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 19, Grd1, Row2, 18);
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 21, Grd1, Row2, 20);
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 23, Grd1, Row2, 22);
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 22, Grd1, Row2, 23);
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 24, Grd1, Row2, 24);
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 25, Grd1, Row2, 25);
                            Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 27, Grd1, Row2, 26);

                            mFrmParent.Grd1.Rows.Add();
                            Sm.SetGrdNumValueZero(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 9, 10, 11, 13, 14, 21 });
                        }
                    }
                    mFrmParent.ComputeUom();
                    mFrmParent.Grd1.EndUpdate();
                }

                if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 item.");
            }
            else
            {
                if (Sm.IsFindGridValid(Grd1, 1))
                {
                    mFrmParent.ShowDataSO(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 19));
                    this.Hide();
                }
            }

        }

        private bool IsItemAlreadyChosen(int Row)
        {
            var Item =
                Sm.GetGrdStr(Grd1, Row, 3) +
                Sm.GetGrdStr(Grd1, Row, 11) +
                Sm.GetGrdStr(Grd1, Row, 13) +
                Sm.GetGrdStr(Grd1, Row, 7) +
                Sm.GetGrdStr(Grd1, Row, 16);
            for (int Index = 0; Index < mFrmParent.Grd1.Rows.Count - 1; Index++)
                if (Sm.CompareStr(
                    Sm.GetGrdStr(mFrmParent.Grd1, Index, 1) +
                    Sm.GetGrdStr(mFrmParent.Grd1, Index, 5) +
                    Sm.GetGrdStr(mFrmParent.Grd1, Index, 7) +
                    Sm.GetGrdStr(mFrmParent.Grd1, Index, 8) +
                    Sm.GetGrdStr(mFrmParent.Grd1, Index, 15),
                    Item
                    )) return true;
            return false;
        }
       

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 10 && Sm.GetGrdStr(Grd1, e.RowIndex, 11).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmSOContract2(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 11);
                f.ShowDialog();
            }
            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 3).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 3);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 10 && Sm.GetGrdStr(Grd1, e.RowIndex, 11).Length != 0)
            {
                var f = new FrmSOContract2(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 11);
                f.ShowDialog();
            }

            if (e.ColIndex == 4 && Sm.GetGrdStr(Grd1, e.RowIndex, 3).Length != 0)
            {
                var f = new FrmItem(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 3);
                f.ShowDialog();
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        #endregion
    }
}
