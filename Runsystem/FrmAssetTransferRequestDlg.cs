﻿#region Update
    // 03/08/2017 [WED] Tambah filter berdasarkan LocCode di TO, jika parameter IsAssetTransferUseLocation
    // 04/08/2017 [WED] Filter berdasarkan cost center tidak melihat parameter IsSystemUseCostCenter
    // 21/08/2017 [WED] Cost center From, dicocokkan dengan cost center yg ada di Reporting Asset Summary (sebelumnya dari master Asset nya)
    // 24/05/2018 [HAR] tambah inputan asset category from dan asset category to 
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmAssetTransferRequestDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmAssetTransferRequest mFrmParent;
        private string 
            mSQL = string.Empty, 
            mCCCode = string.Empty,
            mLocCode = string.Empty;
        private int mCurRow;

        #endregion

        #region Constructor

        public FrmAssetTransferRequestDlg(FrmAssetTransferRequest FrmParent, int CurRow, string CCCode, string LocCode)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mCurRow = CurRow;
            mCCCode = CCCode;
            mLocCode = LocCode;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private string GetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.AssetCode, B.Assetname, B.Displayname ");
            SQL.AppendLine("From TblAssetSummary A  ");    
			SQL.AppendLine("Inner Join TblAsset B On A.AssetCode = B.AssetCode ");
            if (mFrmParent.mIsAssetTransferUseLocation)
            {
                SQL.AppendLine("Inner Join TblTOHdr C On A.AssetCode = C.AssetCode And C.LocCode = @LocCode And C.ActInd = 'Y' ");
            }
            SQL.AppendLine("Where B.ActiveInd = 'Y' ");
            //if (mFrmParent.mIsSystemUseCostCenter)
                SQL.AppendLine("And (A.CCCode Is Not Null And A.CCCode=@CCCode ) ");

            mSQL = SQL.ToString();

            return mSQL;
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 6;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No",

                        //1-4
                        "", 
                        "Asset Code", 
                        "", 
                        "Asset Name",
                        "Display Name" 
                    },
                     new int[] 
                    {
                        //0
                        50,
 
                        //1-5
                        20, 100, 20, 250, 250
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColButton(Grd1, new int[] { 3 });
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 2, 4, 5 });
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }


        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string Filter = " And A.AssetCode Not In ("+mFrmParent.GetSelectedItem()+")";

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@CCCode", mCCCode);
                Sm.CmParam<String>(ref cm, "@LocCode", mLocCode);
                Sm.FilterStr(ref Filter, ref cm, TxtAssetCode.Text, new string[] { "A.AssetCode", "B.AssetName", "B.DisplayName" });
                
                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        GetSQL() + Filter + " Order By B.AssetName;",
                        new string[] 
                        { 
                            //0
                            "AssetCode",
 
                            //1-2
                            "AssetName", "DisplayName"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 2);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 2);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsItCodeAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;

                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 2, Grd1, Row, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 4, Grd1, Row, 4);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 5, Grd1, Row, 5);

                        mFrmParent.Grd1.Rows.Add();
                    }
                }
            }

            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 asset.");
        }


        private bool IsItCodeAlreadyChosen(int Row)
        {
            for (int Index = 0; Index < mFrmParent.Grd1.Rows.Count - 1; Index++)
            {
                if (
                    Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd1, Index, 2), Sm.GetGrdStr(Grd1, Row, 2)) &&
                    Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd1, Index, 4), Sm.GetGrdStr(Grd1, Row, 4)) 
                    )
                    return true;
            }
            return false;
        }

        #endregion

        #region Grid Method

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmAsset(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mAssetCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 2).Length != 0)
            {
                var f = new FrmAsset(mFrmParent.mMenuCode);
                f.Tag = mFrmParent.mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mAssetCode = Sm.GetGrdStr(Grd1, e.RowIndex, 2);
                f.ShowDialog();
            }
        }

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Events

        private void ChkAssetCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Asset");
        }

        private void TxtAssetCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }
        
        #endregion 

        #endregion

    }
}
