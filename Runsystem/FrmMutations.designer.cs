﻿namespace RunSystem
{
    partial class FrmMutations
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel3 = new System.Windows.Forms.Panel();
            this.LueProductionWorkGroup = new DevExpress.XtraEditors.LookUpEdit();
            this.LblProductionWorkGroup = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.MeeCancelReason = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.ChkCancelInd = new DevExpress.XtraEditors.CheckEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.LueWhsCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.Grd1 = new TenTec.Windows.iGridLib.iGrid();
            this.panel5 = new System.Windows.Forms.Panel();
            this.PanelTotalA = new System.Windows.Forms.Panel();
            this.TxtInventoryUomCodeA = new DevExpress.XtraEditors.TextEdit();
            this.TxtTotalA = new DevExpress.XtraEditors.TextEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.PanelTotal2A = new System.Windows.Forms.Panel();
            this.TxtInventoryUomCode2A = new DevExpress.XtraEditors.TextEdit();
            this.TxtTotal2A = new DevExpress.XtraEditors.TextEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.PanelTotal3A = new System.Windows.Forms.Panel();
            this.TxtInventoryUomCode3A = new DevExpress.XtraEditors.TextEdit();
            this.TxtTotal3A = new DevExpress.XtraEditors.TextEdit();
            this.label15 = new System.Windows.Forms.Label();
            this.BtnMutatedFrom = new System.Windows.Forms.Button();
            this.LueBatchNo = new DevExpress.XtraEditors.LookUpEdit();
            this.LuePropCode = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd2 = new TenTec.Windows.iGridLib.iGrid();
            this.panel6 = new System.Windows.Forms.Panel();
            this.PanelTotalB = new System.Windows.Forms.Panel();
            this.TxtInventoryUomCodeB = new DevExpress.XtraEditors.TextEdit();
            this.TxtTotalB = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.PanelTotal2B = new System.Windows.Forms.Panel();
            this.TxtInventoryUomCode2B = new DevExpress.XtraEditors.TextEdit();
            this.TxtTotal2B = new DevExpress.XtraEditors.TextEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.PanelTotal3B = new System.Windows.Forms.Panel();
            this.TxtInventoryUomCode3B = new DevExpress.XtraEditors.TextEdit();
            this.TxtTotal3B = new DevExpress.XtraEditors.TextEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.BtnMutatedTo = new System.Windows.Forms.Button();
            this.ChkHideInfoInGrd = new DevExpress.XtraEditors.CheckEdit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueProductionWorkGroup.Properties)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCancelReason.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            this.panel5.SuspendLayout();
            this.PanelTotalA.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInventoryUomCodeA.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalA.Properties)).BeginInit();
            this.PanelTotal2A.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInventoryUomCode2A.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal2A.Properties)).BeginInit();
            this.PanelTotal3A.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInventoryUomCode3A.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal3A.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBatchNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePropCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).BeginInit();
            this.panel6.SuspendLayout();
            this.PanelTotalB.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInventoryUomCodeB.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalB.Properties)).BeginInit();
            this.PanelTotal2B.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInventoryUomCode2B.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal2B.Properties)).BeginInit();
            this.PanelTotal3B.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInventoryUomCode3B.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal3B.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.ChkHideInfoInGrd);
            this.panel1.Location = new System.Drawing.Point(795, 0);
            this.panel1.Size = new System.Drawing.Size(70, 475);
            this.panel1.Controls.SetChildIndex(this.BtnFind, 0);
            this.panel1.Controls.SetChildIndex(this.BtnInsert, 0);
            this.panel1.Controls.SetChildIndex(this.BtnEdit, 0);
            this.panel1.Controls.SetChildIndex(this.BtnDelete, 0);
            this.panel1.Controls.SetChildIndex(this.BtnSave, 0);
            this.panel1.Controls.SetChildIndex(this.BtnCancel, 0);
            this.panel1.Controls.SetChildIndex(this.BtnPrint, 0);
            this.panel1.Controls.SetChildIndex(this.ChkHideInfoInGrd, 0);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.splitContainer1);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Size = new System.Drawing.Size(795, 475);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.LueProductionWorkGroup);
            this.panel3.Controls.Add(this.LblProductionWorkGroup);
            this.panel3.Controls.Add(this.panel4);
            this.panel3.Controls.Add(this.LueWhsCode);
            this.panel3.Controls.Add(this.label8);
            this.panel3.Controls.Add(this.DteDocDt);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.TxtDocNo);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(795, 93);
            this.panel3.TabIndex = 0;
            // 
            // LueProductionWorkGroup
            // 
            this.LueProductionWorkGroup.EnterMoveNextControl = true;
            this.LueProductionWorkGroup.Location = new System.Drawing.Point(84, 68);
            this.LueProductionWorkGroup.Margin = new System.Windows.Forms.Padding(5);
            this.LueProductionWorkGroup.Name = "LueProductionWorkGroup";
            this.LueProductionWorkGroup.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProductionWorkGroup.Properties.Appearance.Options.UseFont = true;
            this.LueProductionWorkGroup.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProductionWorkGroup.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueProductionWorkGroup.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProductionWorkGroup.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueProductionWorkGroup.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProductionWorkGroup.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueProductionWorkGroup.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProductionWorkGroup.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueProductionWorkGroup.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueProductionWorkGroup.Properties.DropDownRows = 30;
            this.LueProductionWorkGroup.Properties.NullText = "[Empty]";
            this.LueProductionWorkGroup.Properties.PopupWidth = 300;
            this.LueProductionWorkGroup.Size = new System.Drawing.Size(283, 20);
            this.LueProductionWorkGroup.TabIndex = 23;
            this.LueProductionWorkGroup.ToolTip = "F4 : Show/hide list";
            this.LueProductionWorkGroup.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueProductionWorkGroup.EditValueChanged += new System.EventHandler(this.LueProductionWorkGroup_EditValueChanged);
            // 
            // LblProductionWorkGroup
            // 
            this.LblProductionWorkGroup.AutoSize = true;
            this.LblProductionWorkGroup.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblProductionWorkGroup.ForeColor = System.Drawing.Color.Red;
            this.LblProductionWorkGroup.Location = new System.Drawing.Point(42, 71);
            this.LblProductionWorkGroup.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblProductionWorkGroup.Name = "LblProductionWorkGroup";
            this.LblProductionWorkGroup.Size = new System.Drawing.Size(40, 14);
            this.LblProductionWorkGroup.TabIndex = 22;
            this.LblProductionWorkGroup.Text = "Group";
            this.LblProductionWorkGroup.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.label9);
            this.panel4.Controls.Add(this.MeeCancelReason);
            this.panel4.Controls.Add(this.MeeRemark);
            this.panel4.Controls.Add(this.ChkCancelInd);
            this.panel4.Controls.Add(this.label12);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(375, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(420, 93);
            this.panel4.TabIndex = 24;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(5, 8);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(135, 14);
            this.label9.TabIndex = 25;
            this.label9.Text = "Reason For Cancellation";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeCancelReason
            // 
            this.MeeCancelReason.EnterMoveNextControl = true;
            this.MeeCancelReason.Location = new System.Drawing.Point(144, 5);
            this.MeeCancelReason.Margin = new System.Windows.Forms.Padding(5);
            this.MeeCancelReason.Name = "MeeCancelReason";
            this.MeeCancelReason.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.Appearance.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeCancelReason.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeCancelReason.Properties.MaxLength = 400;
            this.MeeCancelReason.Properties.PopupFormSize = new System.Drawing.Size(300, 20);
            this.MeeCancelReason.Properties.ShowIcon = false;
            this.MeeCancelReason.Size = new System.Drawing.Size(268, 20);
            this.MeeCancelReason.TabIndex = 26;
            this.MeeCancelReason.ToolTip = "F4 : Show/hide text";
            this.MeeCancelReason.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeCancelReason.ToolTipTitle = "Run System";
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(144, 47);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 400;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(300, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(268, 20);
            this.MeeRemark.TabIndex = 29;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            this.MeeRemark.Validated += new System.EventHandler(this.MeeRemark_Validated);
            // 
            // ChkCancelInd
            // 
            this.ChkCancelInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkCancelInd.Location = new System.Drawing.Point(144, 26);
            this.ChkCancelInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkCancelInd.Name = "ChkCancelInd";
            this.ChkCancelInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkCancelInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkCancelInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkCancelInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseFont = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkCancelInd.Properties.Caption = "Cancel";
            this.ChkCancelInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkCancelInd.Size = new System.Drawing.Size(61, 22);
            this.ChkCancelInd.TabIndex = 27;
            this.ChkCancelInd.CheckedChanged += new System.EventHandler(this.ChkCancelInd_CheckedChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(93, 50);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(47, 14);
            this.label12.TabIndex = 28;
            this.label12.Text = "Remark";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueWhsCode
            // 
            this.LueWhsCode.EnterMoveNextControl = true;
            this.LueWhsCode.Location = new System.Drawing.Point(84, 47);
            this.LueWhsCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueWhsCode.Name = "LueWhsCode";
            this.LueWhsCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.Appearance.Options.UseFont = true;
            this.LueWhsCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueWhsCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueWhsCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueWhsCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWhsCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueWhsCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueWhsCode.Properties.DropDownRows = 30;
            this.LueWhsCode.Properties.NullText = "[Empty]";
            this.LueWhsCode.Properties.PopupWidth = 300;
            this.LueWhsCode.Size = new System.Drawing.Size(283, 20);
            this.LueWhsCode.TabIndex = 21;
            this.LueWhsCode.ToolTip = "F4 : Show/hide list";
            this.LueWhsCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueWhsCode.EditValueChanged += new System.EventHandler(this.LueWhsCode_EditValueChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(13, 50);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(69, 14);
            this.label8.TabIndex = 20;
            this.label8.Text = "Warehouse";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(84, 26);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(108, 20);
            this.DteDocDt.TabIndex = 19;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(49, 29);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 14);
            this.label2.TabIndex = 18;
            this.label2.Text = "Date";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(84, 5);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 50;
            this.TxtDocNo.Properties.ReadOnly = true;
            this.TxtDocNo.Size = new System.Drawing.Size(283, 20);
            this.TxtDocNo.TabIndex = 17;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(9, 8);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 14);
            this.label1.TabIndex = 16;
            this.label1.Text = "Document#";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 93);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.Grd1);
            this.splitContainer1.Panel1.Controls.Add(this.panel5);
            this.splitContainer1.Panel1.Controls.Add(this.BtnMutatedFrom);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.LueBatchNo);
            this.splitContainer1.Panel2.Controls.Add(this.LuePropCode);
            this.splitContainer1.Panel2.Controls.Add(this.Grd2);
            this.splitContainer1.Panel2.Controls.Add(this.panel6);
            this.splitContainer1.Panel2.Controls.Add(this.BtnMutatedTo);
            this.splitContainer1.Size = new System.Drawing.Size(795, 382);
            this.splitContainer1.SplitterDistance = 176;
            this.splitContainer1.TabIndex = 1;
            // 
            // Grd1
            // 
            this.Grd1.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd1.DefaultRow.Height = 20;
            this.Grd1.DefaultRow.Sortable = false;
            this.Grd1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 21;
            this.Grd1.Location = new System.Drawing.Point(0, 25);
            this.Grd1.Name = "Grd1";
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd1.SingleClickEdit = true;
            this.Grd1.Size = new System.Drawing.Size(795, 122);
            this.Grd1.TabIndex = 30;
            this.Grd1.TreeCol = null;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd1_ColHdrDoubleClick);
            this.Grd1.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd1_EllipsisButtonClick);
            this.Grd1.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd1_RequestEdit);
            this.Grd1.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd1_AfterCommitEdit);
            this.Grd1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd1_KeyDown);
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel5.Controls.Add(this.PanelTotalA);
            this.panel5.Controls.Add(this.PanelTotal2A);
            this.panel5.Controls.Add(this.PanelTotal3A);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel5.Location = new System.Drawing.Point(0, 147);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(795, 29);
            this.panel5.TabIndex = 25;
            // 
            // PanelTotalA
            // 
            this.PanelTotalA.Controls.Add(this.TxtInventoryUomCodeA);
            this.PanelTotalA.Controls.Add(this.TxtTotalA);
            this.PanelTotalA.Controls.Add(this.label7);
            this.PanelTotalA.Dock = System.Windows.Forms.DockStyle.Right;
            this.PanelTotalA.Location = new System.Drawing.Point(52, 0);
            this.PanelTotalA.Name = "PanelTotalA";
            this.PanelTotalA.Size = new System.Drawing.Size(243, 29);
            this.PanelTotalA.TabIndex = 31;
            // 
            // TxtInventoryUomCodeA
            // 
            this.TxtInventoryUomCodeA.EnterMoveNextControl = true;
            this.TxtInventoryUomCodeA.Location = new System.Drawing.Point(137, 5);
            this.TxtInventoryUomCodeA.Margin = new System.Windows.Forms.Padding(5);
            this.TxtInventoryUomCodeA.Name = "TxtInventoryUomCodeA";
            this.TxtInventoryUomCodeA.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtInventoryUomCodeA.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtInventoryUomCodeA.Properties.Appearance.Options.UseBackColor = true;
            this.TxtInventoryUomCodeA.Properties.Appearance.Options.UseFont = true;
            this.TxtInventoryUomCodeA.Properties.MaxLength = 16;
            this.TxtInventoryUomCodeA.Size = new System.Drawing.Size(98, 20);
            this.TxtInventoryUomCodeA.TabIndex = 34;
            // 
            // TxtTotalA
            // 
            this.TxtTotalA.EnterMoveNextControl = true;
            this.TxtTotalA.Location = new System.Drawing.Point(45, 5);
            this.TxtTotalA.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotalA.Name = "TxtTotalA";
            this.TxtTotalA.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotalA.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotalA.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotalA.Properties.Appearance.Options.UseFont = true;
            this.TxtTotalA.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotalA.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotalA.Size = new System.Drawing.Size(89, 20);
            this.TxtTotalA.TabIndex = 33;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(5, 8);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(35, 14);
            this.label7.TabIndex = 32;
            this.label7.Text = "Total";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // PanelTotal2A
            // 
            this.PanelTotal2A.Controls.Add(this.TxtInventoryUomCode2A);
            this.PanelTotal2A.Controls.Add(this.TxtTotal2A);
            this.PanelTotal2A.Controls.Add(this.label6);
            this.PanelTotal2A.Dock = System.Windows.Forms.DockStyle.Right;
            this.PanelTotal2A.Location = new System.Drawing.Point(295, 0);
            this.PanelTotal2A.Name = "PanelTotal2A";
            this.PanelTotal2A.Size = new System.Drawing.Size(250, 29);
            this.PanelTotal2A.TabIndex = 35;
            this.PanelTotal2A.Visible = false;
            // 
            // TxtInventoryUomCode2A
            // 
            this.TxtInventoryUomCode2A.EnterMoveNextControl = true;
            this.TxtInventoryUomCode2A.Location = new System.Drawing.Point(144, 5);
            this.TxtInventoryUomCode2A.Margin = new System.Windows.Forms.Padding(5);
            this.TxtInventoryUomCode2A.Name = "TxtInventoryUomCode2A";
            this.TxtInventoryUomCode2A.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtInventoryUomCode2A.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtInventoryUomCode2A.Properties.Appearance.Options.UseBackColor = true;
            this.TxtInventoryUomCode2A.Properties.Appearance.Options.UseFont = true;
            this.TxtInventoryUomCode2A.Properties.MaxLength = 16;
            this.TxtInventoryUomCode2A.Size = new System.Drawing.Size(98, 20);
            this.TxtInventoryUomCode2A.TabIndex = 38;
            // 
            // TxtTotal2A
            // 
            this.TxtTotal2A.EnterMoveNextControl = true;
            this.TxtTotal2A.Location = new System.Drawing.Point(52, 5);
            this.TxtTotal2A.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal2A.Name = "TxtTotal2A";
            this.TxtTotal2A.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal2A.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal2A.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal2A.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal2A.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotal2A.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotal2A.Size = new System.Drawing.Size(89, 20);
            this.TxtTotal2A.TabIndex = 37;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(4, 8);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(46, 14);
            this.label6.TabIndex = 36;
            this.label6.Text = "Total 2";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // PanelTotal3A
            // 
            this.PanelTotal3A.Controls.Add(this.TxtInventoryUomCode3A);
            this.PanelTotal3A.Controls.Add(this.TxtTotal3A);
            this.PanelTotal3A.Controls.Add(this.label15);
            this.PanelTotal3A.Dock = System.Windows.Forms.DockStyle.Right;
            this.PanelTotal3A.Location = new System.Drawing.Point(545, 0);
            this.PanelTotal3A.Name = "PanelTotal3A";
            this.PanelTotal3A.Size = new System.Drawing.Size(250, 29);
            this.PanelTotal3A.TabIndex = 39;
            this.PanelTotal3A.Visible = false;
            // 
            // TxtInventoryUomCode3A
            // 
            this.TxtInventoryUomCode3A.EnterMoveNextControl = true;
            this.TxtInventoryUomCode3A.Location = new System.Drawing.Point(144, 5);
            this.TxtInventoryUomCode3A.Margin = new System.Windows.Forms.Padding(5);
            this.TxtInventoryUomCode3A.Name = "TxtInventoryUomCode3A";
            this.TxtInventoryUomCode3A.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtInventoryUomCode3A.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtInventoryUomCode3A.Properties.Appearance.Options.UseBackColor = true;
            this.TxtInventoryUomCode3A.Properties.Appearance.Options.UseFont = true;
            this.TxtInventoryUomCode3A.Properties.MaxLength = 16;
            this.TxtInventoryUomCode3A.Size = new System.Drawing.Size(98, 20);
            this.TxtInventoryUomCode3A.TabIndex = 42;
            // 
            // TxtTotal3A
            // 
            this.TxtTotal3A.EnterMoveNextControl = true;
            this.TxtTotal3A.Location = new System.Drawing.Point(52, 5);
            this.TxtTotal3A.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal3A.Name = "TxtTotal3A";
            this.TxtTotal3A.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal3A.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal3A.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal3A.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal3A.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotal3A.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotal3A.Size = new System.Drawing.Size(89, 20);
            this.TxtTotal3A.TabIndex = 41;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(4, 8);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(46, 14);
            this.label15.TabIndex = 40;
            this.label15.Text = "Total 3";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnMutatedFrom
            // 
            this.BtnMutatedFrom.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnMutatedFrom.Dock = System.Windows.Forms.DockStyle.Top;
            this.BtnMutatedFrom.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnMutatedFrom.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnMutatedFrom.ForeColor = System.Drawing.Color.AliceBlue;
            this.BtnMutatedFrom.Location = new System.Drawing.Point(0, 0);
            this.BtnMutatedFrom.Name = "BtnMutatedFrom";
            this.BtnMutatedFrom.Size = new System.Drawing.Size(795, 25);
            this.BtnMutatedFrom.TabIndex = 23;
            this.BtnMutatedFrom.Text = "Mutated From";
            this.BtnMutatedFrom.UseVisualStyleBackColor = false;
            // 
            // LueBatchNo
            // 
            this.LueBatchNo.EnterMoveNextControl = true;
            this.LueBatchNo.Location = new System.Drawing.Point(414, 43);
            this.LueBatchNo.Margin = new System.Windows.Forms.Padding(5);
            this.LueBatchNo.Name = "LueBatchNo";
            this.LueBatchNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBatchNo.Properties.Appearance.Options.UseFont = true;
            this.LueBatchNo.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBatchNo.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueBatchNo.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBatchNo.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueBatchNo.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBatchNo.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueBatchNo.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBatchNo.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueBatchNo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueBatchNo.Properties.DropDownRows = 30;
            this.LueBatchNo.Properties.NullText = "[Empty]";
            this.LueBatchNo.Properties.PopupWidth = 500;
            this.LueBatchNo.Size = new System.Drawing.Size(249, 20);
            this.LueBatchNo.TabIndex = 46;
            this.LueBatchNo.ToolTip = "F4 : Show/hide list";
            this.LueBatchNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueBatchNo.EditValueChanged += new System.EventHandler(this.LueBatchNo_EditValueChanged);
            this.LueBatchNo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueBatchNo_KeyDown);
            this.LueBatchNo.Leave += new System.EventHandler(this.LueBatchNo_Leave);
            // 
            // LuePropCode
            // 
            this.LuePropCode.EnterMoveNextControl = true;
            this.LuePropCode.Location = new System.Drawing.Point(242, 44);
            this.LuePropCode.Margin = new System.Windows.Forms.Padding(5);
            this.LuePropCode.Name = "LuePropCode";
            this.LuePropCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePropCode.Properties.Appearance.Options.UseFont = true;
            this.LuePropCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePropCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePropCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePropCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePropCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePropCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePropCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePropCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePropCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePropCode.Properties.DropDownRows = 30;
            this.LuePropCode.Properties.NullText = "[Empty]";
            this.LuePropCode.Properties.PopupWidth = 500;
            this.LuePropCode.Size = new System.Drawing.Size(151, 20);
            this.LuePropCode.TabIndex = 44;
            this.LuePropCode.ToolTip = "F4 : Show/hide list";
            this.LuePropCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LuePropCode.EditValueChanged += new System.EventHandler(this.LuePropCode_EditValueChanged);
            this.LuePropCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LuePropCode_KeyDown);
            this.LuePropCode.Leave += new System.EventHandler(this.LuePropCode_Leave);
            // 
            // Grd2
            // 
            this.Grd2.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd2.DefaultRow.Height = 20;
            this.Grd2.DefaultRow.Sortable = false;
            this.Grd2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd2.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd2.Header.Height = 21;
            this.Grd2.Location = new System.Drawing.Point(0, 25);
            this.Grd2.Name = "Grd2";
            this.Grd2.RowHeader.Visible = true;
            this.Grd2.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd2.SingleClickEdit = true;
            this.Grd2.Size = new System.Drawing.Size(795, 147);
            this.Grd2.TabIndex = 43;
            this.Grd2.TreeCol = null;
            this.Grd2.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd2.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd2_ColHdrDoubleClick);
            this.Grd2.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd2_EllipsisButtonClick);
            this.Grd2.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd2_RequestEdit);
            this.Grd2.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd2_AfterCommitEdit);
            this.Grd2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd2_KeyDown);
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel6.Controls.Add(this.PanelTotalB);
            this.panel6.Controls.Add(this.PanelTotal2B);
            this.panel6.Controls.Add(this.PanelTotal3B);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel6.Location = new System.Drawing.Point(0, 172);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(795, 30);
            this.panel6.TabIndex = 45;
            // 
            // PanelTotalB
            // 
            this.PanelTotalB.Controls.Add(this.TxtInventoryUomCodeB);
            this.PanelTotalB.Controls.Add(this.TxtTotalB);
            this.PanelTotalB.Controls.Add(this.label3);
            this.PanelTotalB.Location = new System.Drawing.Point(29, 0);
            this.PanelTotalB.Name = "PanelTotalB";
            this.PanelTotalB.Size = new System.Drawing.Size(243, 30);
            this.PanelTotalB.TabIndex = 46;
            // 
            // TxtInventoryUomCodeB
            // 
            this.TxtInventoryUomCodeB.EnterMoveNextControl = true;
            this.TxtInventoryUomCodeB.Location = new System.Drawing.Point(137, 5);
            this.TxtInventoryUomCodeB.Margin = new System.Windows.Forms.Padding(5);
            this.TxtInventoryUomCodeB.Name = "TxtInventoryUomCodeB";
            this.TxtInventoryUomCodeB.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtInventoryUomCodeB.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtInventoryUomCodeB.Properties.Appearance.Options.UseBackColor = true;
            this.TxtInventoryUomCodeB.Properties.Appearance.Options.UseFont = true;
            this.TxtInventoryUomCodeB.Properties.MaxLength = 16;
            this.TxtInventoryUomCodeB.Size = new System.Drawing.Size(98, 20);
            this.TxtInventoryUomCodeB.TabIndex = 49;
            // 
            // TxtTotalB
            // 
            this.TxtTotalB.EnterMoveNextControl = true;
            this.TxtTotalB.Location = new System.Drawing.Point(45, 5);
            this.TxtTotalB.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotalB.Name = "TxtTotalB";
            this.TxtTotalB.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotalB.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotalB.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotalB.Properties.Appearance.Options.UseFont = true;
            this.TxtTotalB.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotalB.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotalB.Size = new System.Drawing.Size(89, 20);
            this.TxtTotalB.TabIndex = 48;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(5, 8);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 14);
            this.label3.TabIndex = 47;
            this.label3.Text = "Total";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // PanelTotal2B
            // 
            this.PanelTotal2B.Controls.Add(this.TxtInventoryUomCode2B);
            this.PanelTotal2B.Controls.Add(this.TxtTotal2B);
            this.PanelTotal2B.Controls.Add(this.label4);
            this.PanelTotal2B.Dock = System.Windows.Forms.DockStyle.Right;
            this.PanelTotal2B.Location = new System.Drawing.Point(295, 0);
            this.PanelTotal2B.Name = "PanelTotal2B";
            this.PanelTotal2B.Size = new System.Drawing.Size(250, 30);
            this.PanelTotal2B.TabIndex = 50;
            this.PanelTotal2B.Visible = false;
            // 
            // TxtInventoryUomCode2B
            // 
            this.TxtInventoryUomCode2B.EnterMoveNextControl = true;
            this.TxtInventoryUomCode2B.Location = new System.Drawing.Point(144, 5);
            this.TxtInventoryUomCode2B.Margin = new System.Windows.Forms.Padding(5);
            this.TxtInventoryUomCode2B.Name = "TxtInventoryUomCode2B";
            this.TxtInventoryUomCode2B.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtInventoryUomCode2B.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtInventoryUomCode2B.Properties.Appearance.Options.UseBackColor = true;
            this.TxtInventoryUomCode2B.Properties.Appearance.Options.UseFont = true;
            this.TxtInventoryUomCode2B.Properties.MaxLength = 16;
            this.TxtInventoryUomCode2B.Size = new System.Drawing.Size(98, 20);
            this.TxtInventoryUomCode2B.TabIndex = 53;
            // 
            // TxtTotal2B
            // 
            this.TxtTotal2B.EnterMoveNextControl = true;
            this.TxtTotal2B.Location = new System.Drawing.Point(52, 5);
            this.TxtTotal2B.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal2B.Name = "TxtTotal2B";
            this.TxtTotal2B.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal2B.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal2B.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal2B.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal2B.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotal2B.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotal2B.Size = new System.Drawing.Size(89, 20);
            this.TxtTotal2B.TabIndex = 52;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(4, 8);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 14);
            this.label4.TabIndex = 51;
            this.label4.Text = "Total 2";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // PanelTotal3B
            // 
            this.PanelTotal3B.Controls.Add(this.TxtInventoryUomCode3B);
            this.PanelTotal3B.Controls.Add(this.TxtTotal3B);
            this.PanelTotal3B.Controls.Add(this.label5);
            this.PanelTotal3B.Dock = System.Windows.Forms.DockStyle.Right;
            this.PanelTotal3B.Location = new System.Drawing.Point(545, 0);
            this.PanelTotal3B.Name = "PanelTotal3B";
            this.PanelTotal3B.Size = new System.Drawing.Size(250, 30);
            this.PanelTotal3B.TabIndex = 54;
            this.PanelTotal3B.Visible = false;
            // 
            // TxtInventoryUomCode3B
            // 
            this.TxtInventoryUomCode3B.EnterMoveNextControl = true;
            this.TxtInventoryUomCode3B.Location = new System.Drawing.Point(144, 5);
            this.TxtInventoryUomCode3B.Margin = new System.Windows.Forms.Padding(5);
            this.TxtInventoryUomCode3B.Name = "TxtInventoryUomCode3B";
            this.TxtInventoryUomCode3B.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtInventoryUomCode3B.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtInventoryUomCode3B.Properties.Appearance.Options.UseBackColor = true;
            this.TxtInventoryUomCode3B.Properties.Appearance.Options.UseFont = true;
            this.TxtInventoryUomCode3B.Properties.MaxLength = 16;
            this.TxtInventoryUomCode3B.Size = new System.Drawing.Size(98, 20);
            this.TxtInventoryUomCode3B.TabIndex = 57;
            // 
            // TxtTotal3B
            // 
            this.TxtTotal3B.EnterMoveNextControl = true;
            this.TxtTotal3B.Location = new System.Drawing.Point(52, 5);
            this.TxtTotal3B.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotal3B.Name = "TxtTotal3B";
            this.TxtTotal3B.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotal3B.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotal3B.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotal3B.Properties.Appearance.Options.UseFont = true;
            this.TxtTotal3B.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotal3B.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotal3B.Size = new System.Drawing.Size(89, 20);
            this.TxtTotal3B.TabIndex = 56;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(4, 8);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(46, 14);
            this.label5.TabIndex = 55;
            this.label5.Text = "Total 3";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnMutatedTo
            // 
            this.BtnMutatedTo.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnMutatedTo.Dock = System.Windows.Forms.DockStyle.Top;
            this.BtnMutatedTo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnMutatedTo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnMutatedTo.ForeColor = System.Drawing.Color.AliceBlue;
            this.BtnMutatedTo.Location = new System.Drawing.Point(0, 0);
            this.BtnMutatedTo.Name = "BtnMutatedTo";
            this.BtnMutatedTo.Size = new System.Drawing.Size(795, 25);
            this.BtnMutatedTo.TabIndex = 39;
            this.BtnMutatedTo.Text = "Mutated To";
            this.BtnMutatedTo.UseVisualStyleBackColor = false;
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ChkHideInfoInGrd.EditValue = true;
            this.ChkHideInfoInGrd.Location = new System.Drawing.Point(0, 453);
            this.ChkHideInfoInGrd.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ChkHideInfoInGrd.Name = "ChkHideInfoInGrd";
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.ChkHideInfoInGrd.Properties.Caption = "Hide";
            this.ChkHideInfoInGrd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkHideInfoInGrd.Size = new System.Drawing.Size(70, 22);
            this.ChkHideInfoInGrd.TabIndex = 10;
            this.ChkHideInfoInGrd.ToolTip = "Hide some informations in the list";
            this.ChkHideInfoInGrd.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkHideInfoInGrd.ToolTipTitle = "Run System";
            this.ChkHideInfoInGrd.CheckedChanged += new System.EventHandler(this.ChkHideInfoInGrd_CheckedChanged);
            // 
            // FrmMutations
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(865, 475);
            this.Name = "FrmMutations";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueProductionWorkGroup.Properties)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCancelReason.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWhsCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            this.panel5.ResumeLayout(false);
            this.PanelTotalA.ResumeLayout(false);
            this.PanelTotalA.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInventoryUomCodeA.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalA.Properties)).EndInit();
            this.PanelTotal2A.ResumeLayout(false);
            this.PanelTotal2A.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInventoryUomCode2A.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal2A.Properties)).EndInit();
            this.PanelTotal3A.ResumeLayout(false);
            this.PanelTotal3A.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInventoryUomCode3A.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal3A.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBatchNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePropCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).EndInit();
            this.panel6.ResumeLayout(false);
            this.PanelTotalB.ResumeLayout(false);
            this.PanelTotalB.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInventoryUomCodeB.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalB.Properties)).EndInit();
            this.PanelTotal2B.ResumeLayout(false);
            this.PanelTotal2B.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInventoryUomCode2B.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal2B.Properties)).EndInit();
            this.PanelTotal3B.ResumeLayout(false);
            this.PanelTotal3B.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtInventoryUomCode3B.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotal3B.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel3;
        private DevExpress.XtraEditors.LookUpEdit LueWhsCode;
        private System.Windows.Forms.Label label8;
        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label9;
        private DevExpress.XtraEditors.MemoExEdit MeeCancelReason;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private DevExpress.XtraEditors.CheckEdit ChkCancelInd;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button BtnMutatedFrom;
        private System.Windows.Forms.Button BtnMutatedTo;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel PanelTotalB;
        internal DevExpress.XtraEditors.TextEdit TxtInventoryUomCodeB;
        internal DevExpress.XtraEditors.TextEdit TxtTotalB;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel PanelTotal2B;
        internal DevExpress.XtraEditors.TextEdit TxtInventoryUomCode2B;
        internal DevExpress.XtraEditors.TextEdit TxtTotal2B;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel PanelTotal3B;
        internal DevExpress.XtraEditors.TextEdit TxtInventoryUomCode3B;
        internal DevExpress.XtraEditors.TextEdit TxtTotal3B;
        private System.Windows.Forms.Label label5;
        protected internal TenTec.Windows.iGridLib.iGrid Grd2;
        private DevExpress.XtraEditors.LookUpEdit LuePropCode;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel PanelTotalA;
        internal DevExpress.XtraEditors.TextEdit TxtInventoryUomCodeA;
        internal DevExpress.XtraEditors.TextEdit TxtTotalA;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel PanelTotal2A;
        internal DevExpress.XtraEditors.TextEdit TxtInventoryUomCode2A;
        internal DevExpress.XtraEditors.TextEdit TxtTotal2A;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel PanelTotal3A;
        internal DevExpress.XtraEditors.TextEdit TxtInventoryUomCode3A;
        internal DevExpress.XtraEditors.TextEdit TxtTotal3A;
        private System.Windows.Forms.Label label15;
        protected internal TenTec.Windows.iGridLib.iGrid Grd1;
        protected DevExpress.XtraEditors.CheckEdit ChkHideInfoInGrd;
        private DevExpress.XtraEditors.LookUpEdit LueProductionWorkGroup;
        private System.Windows.Forms.Label LblProductionWorkGroup;
        private DevExpress.XtraEditors.LookUpEdit LueBatchNo;

    }
}