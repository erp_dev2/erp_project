﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmDA : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "";
        //internal FrmFind FrmFind;

        #endregion

        #region Constructor

        public FrmDA(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtLocalDocNo_Validated(object sender, EventArgs e)
        {

        }

        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {

        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void BtnBrowseFile_Click(object sender, EventArgs e)
        {

        }

        private void BtnDownload_Click(object sender, EventArgs e)
        {

        }

        #endregion

        #endregion
    }
}
