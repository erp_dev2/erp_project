﻿namespace RunSystem
{
    partial class FrmDeliveryType
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TxtDTName = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtDTCode = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.ChkCtQtInd = new DevExpress.XtraEditors.CheckEdit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDTName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDTCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCtQtInd.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.ChkCtQtInd);
            this.panel2.Controls.Add(this.MeeRemark);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.TxtDTName);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.TxtDTCode);
            this.panel2.Controls.Add(this.label1);
            // 
            // TxtDTName
            // 
            this.TxtDTName.EnterMoveNextControl = true;
            this.TxtDTName.Location = new System.Drawing.Point(160, 64);
            this.TxtDTName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDTName.Name = "TxtDTName";
            this.TxtDTName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDTName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDTName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDTName.Properties.Appearance.Options.UseFont = true;
            this.TxtDTName.Properties.MaxLength = 40;
            this.TxtDTName.Size = new System.Drawing.Size(319, 20);
            this.TxtDTName.TabIndex = 12;
            this.TxtDTName.Validated += new System.EventHandler(this.TxtDTName_Validated);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(88, 67);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 14);
            this.label2.TabIndex = 11;
            this.label2.Text = "Type Name";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDTCode
            // 
            this.TxtDTCode.EnterMoveNextControl = true;
            this.TxtDTCode.Location = new System.Drawing.Point(160, 41);
            this.TxtDTCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDTCode.Name = "TxtDTCode";
            this.TxtDTCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDTCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDTCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDTCode.Properties.Appearance.Options.UseFont = true;
            this.TxtDTCode.Properties.MaxLength = 16;
            this.TxtDTCode.Size = new System.Drawing.Size(110, 20);
            this.TxtDTCode.TabIndex = 10;
            this.TxtDTCode.Validated += new System.EventHandler(this.TxtDTCode_Validated);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(91, 44);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 14);
            this.label1.TabIndex = 9;
            this.label1.Text = "Type Code";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(160, 88);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 800;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(550, 20);
            this.MeeRemark.TabIndex = 14;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(111, 91);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 14);
            this.label5.TabIndex = 13;
            this.label5.Text = "Remark";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkCtQtInd
            // 
            this.ChkCtQtInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkCtQtInd.Location = new System.Drawing.Point(158, 111);
            this.ChkCtQtInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkCtQtInd.Name = "ChkCtQtInd";
            this.ChkCtQtInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkCtQtInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkCtQtInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkCtQtInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkCtQtInd.Properties.Appearance.Options.UseFont = true;
            this.ChkCtQtInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkCtQtInd.Properties.Caption = "Customer Quotation Indicator";
            this.ChkCtQtInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkCtQtInd.Size = new System.Drawing.Size(221, 22);
            this.ChkCtQtInd.TabIndex = 16;
            // 
            // FrmDeliveryType
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 233);
            this.Name = "FrmDeliveryType";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDTName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDTCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCtQtInd.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.TextEdit TxtDTName;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtDTCode;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private System.Windows.Forms.Label label5;
        private DevExpress.XtraEditors.CheckEdit ChkCtQtInd;
    }
}