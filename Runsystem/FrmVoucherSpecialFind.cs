﻿#region Update
/* 
    18/02/2021 [WED/IMS] new apps
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmVoucherSpecialFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmVoucherSpecial mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmVoucherSpecialFind(FrmVoucherSpecial FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                Sl.SetLueVoucherDocType(ref LueDocType);
                Sl.SetLueDeptCode(ref LueDeptCode);
                SetSQL();
                SetGrd();
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelInd, C.LocalDocNo, D.OptDesc As DocTypeDesc, ");
            SQL.AppendLine("F.BankAcTp, F.BankAcNm, A.VoucherRequestSpecialDocNo, Sum(C.Amt) Amt, Null As VdName, ");
            SQL.AppendLine("Null As VdInvNo, I.EntName, Upper(E.UserName) PIC, ");
            SQL.AppendLine("Concat( ");
            SQL.AppendLine("    Case When G.BankName Is Not Null Then Concat(G.BankName, ' : ') Else '' End, ");
            SQL.AppendLine("    Case When F.BankAcNo Is Not Null ");
            SQL.AppendLine("    Then Concat(F.BankAcNo, ' [', IfNull(F.BankAcNm, ''), ']') ");
            SQL.AppendLine("    Else IfNull(F.BankAcNm, '') End ");
            SQL.AppendLine(") As BankAcDesc, J.DeptCode, J.DeptName, Null As BCName, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("From TblVoucherSpecialHdr A ");
            SQL.AppendLine("Inner Join TblVoucherSpecialDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("    And (A.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("Inner Join TblVoucherHdr C On B.VoucherDocNo = C.DocNo ");
            SQL.AppendLine("Inner Join TblOption D On D.OptCat = 'VOucherDocType' And C.DocType = D.OptCode ");
            SQL.AppendLine("Left JOin TblUser E On C.PIC = E.UserCode ");
            SQL.AppendLine("Left Join TblBankAccount F On C.BankAcCode = F.BankAcCode ");
            SQL.AppendLine("Left Join TblBank G On F.BankCode = G.BankCode ");
            SQL.AppendLine("Inner Join TblVoucherRequestHdr H On C.VoucherRequestDocNo = H.DocNo ");
            SQL.AppendLine("Left Join TblEntity I On H.EntCode = I.EntCode ");
            SQL.AppendLine("Left Join TblDepartment J On H.DeptCode = J.DeptCode ");
            SQL.AppendLine("Where 0 = 0 ");
            if (mFrmParent.mIsVoucherFilteredByDept)
            {
                SQL.AppendLine("And (H.DeptCode Is Null ");
                SQL.AppendLine("    Or (H.DeptCode Is Not Null And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupDepartment ");
                SQL.AppendLine("        Where DeptCode=H.DeptCode ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        )))) ");
            }
            if (mFrmParent.mIsVoucherBankAccountFilteredByGrp)
            {
                SQL.AppendLine("And (C.BankAcCode Is Null ");
                SQL.AppendLine("    Or (C.BankAcCode Is Not Null And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupBankAccount ");
                SQL.AppendLine("    Where BankAcCode=C.BankAcCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    )))) ");
            }

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 23;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Document#", 
                    "Date",
                    "Cancel", 
                    "Local"+Environment.NewLine+"Document#", 
                    "Type",
                    
                    //6-10
                    "Voucher Request#",
                    "Person In Charge",
                    "Entity",
                    "Vendor",
                    "Vendor's"+Environment.NewLine+"Invoice#",
                    
                    //11-15
                    "Debit/Credit To",
                    "Bank Account",
                    "Account Type",
                    "Amount",
                    "Department",

                    //16-20
                    "Budget Category",
                    "Created By",
                    "Created Date",
                    "Created Time", 
                    "Last Updated By", 

                    //21-22
                    "Last Updated Date", 
                    "Last Updated Time"
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    130, 80, 50, 130, 150, 
                    
                    //6-10
                    130, 200, 200, 200, 130, 

                    //11-15
                    250, 200, 200, 130, 200,

                    //16-20
                    200, 130, 130, 130, 130,

                    //21-22
                    130, 130
                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 14 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2, 18, 21 });
            Sm.GrdFormatTime(Grd1, new int[] { 19, 22 });
            Sm.GrdColCheck(Grd1, new int[] { 3 });
            Sm.GrdColInvisible(Grd1, new int[] { 17, 18, 19, 20, 21, 22 }, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 17, 18, 19, 20, 21, 22 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";
                string mGroupBy = " Group By A.DocNo, A.DocDt, A.CancelInd, C.LocalDocNo, D.OptDesc, F.BankAcTp, F.BankAcNm, A.VoucherRequestSpecialDocNo, I.EntName, E.UserName, G.BankName, F.BankAcNo, F.BankAcNm, J.DeptCode, J.DeptName, A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ";

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                if (mFrmParent.mIsVoucherFilteredByDept)
                {
                    Sm.CmParam<String>(ref cm, "@VoucherDocTypeManual", mFrmParent.mVoucherDocTypeManual);
                }

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtAcTp.Text, "F.BankAcTp", false);
                Sm.FilterStr(ref Filter, ref cm, TxtVoucherRequestDocNo.Text, "A.VoucherRequestSpecialDocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtLocalDocNo.Text, "C.LocalDocNo", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDocType), "C.DocType", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueDeptCode), "H.DeptCode", false);

                
                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + mGroupBy + " Order By A.CreateDt Desc;",
                    new string[]
                    {
                        //0
                        "DocNo",

                        //1-5
                        "DocDt", "CancelInd", "LocalDocNo", "DocTypeDesc", "VoucherRequestSpecialDocNo", 

                        //6-10
                        "PIC", "EntName", "VdName", "VdInvNo", "BankAcDesc", 
                        
                        //11-15
                        "BankAcNm", "BankAcTp", "Amt", "DeptName", "BCName", 
                        
                        //16-19
                        "CreateBy", "CreateDt", "LastUpBy", "LastUpDt"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 13);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 14);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 15);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 16);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 18, 17);
                        Sm.SetGrdValue("T", Grd, dr, c, Row, 19, 17);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 18);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 21, 19);
                        Sm.SetGrdValue("T", Grd, dr, c, Row, 22, 19);
                    }, true, false, false, false
                );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {

        }

        #endregion

        #endregion 

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }
        private void TxtAcTp_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }
        private void ChkAcTp_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Account Type");
        }
        private void TxtLocalDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkLocalDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Local#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LueDocType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDocType, new Sm.RefreshLue1(Sl.SetLueVoucherDocType));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDocType_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Type");
        }
        private void LueDeptCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueDeptCode, new Sm.RefreshLue1(Sl.SetLueDeptCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkDeptCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Department");
        }
        private void TxtVoucherRequestDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkVoucherRequestDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "VR Special#");
        }

        #endregion

        #endregion
    }
}
