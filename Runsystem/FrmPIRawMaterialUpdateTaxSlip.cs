﻿#region Update
/*
    15/05/2018 [TKG] update PI taxslip
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DevExpress.XtraEditors;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPIRawMaterialUpdateTaxSlip : RunSystem.FrmBase12
    {
        #region Field, Property

        private string mMenuCode = string.Empty, mAccessInd = string.Empty;

        #endregion

        #region Constructor

        public FrmPIRawMaterialUpdateTaxSlip(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                string CurrentDateTime = Sm.ServerCurrentDateTime();
                Sl.SetLueYr(LueYr, "");
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));
                Sl.SetLueMth(LueMth);
                Sm.SetLue(LueMth, CurrentDateTime.Substring(4, 2));
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnProcessClick(object sender, EventArgs e)
        {
            var l = new List<Result>();
            try
            {
                if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No && IsProcessedDataNotValid()) return;

                Cursor.Current = Cursors.WaitCursor;

                var SQL = new StringBuilder();
                var cm = new MySqlCommand();
                
                Process1(ref l);
                if (l.Count > 0)
                {
                    Process2(ref l);
                    Sm.StdMsg(mMsgType.Info, "Process is completed.");
                }   
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                l.Clear();
                Cursor.Current = Cursors.Default;
            }
        }

        private void Process1(ref List<Result> l)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.DocNo ");
            SQL.AppendLine("From TblPurchaseInvoiceRawMaterialHdr A ");
            SQL.AppendLine("Inner Join TblVoucherHdr B On A.VoucherRequestDocNo=B.VoucherRequestDocNo And B.CancelInd='N' ");
            SQL.AppendLine("Where Left(A.DocDt, 6)=@YrMth ");
            SQL.AppendLine("And A.CancelInd='N' ");
            SQL.AppendLine("Order By A.DocDt, A.DocNo; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@YrMth", string.Concat(Sm.GetLue(LueYr), Sm.GetLue(LueMth)));

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocNo" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                        l.Add(new Result(){ DocNo = Sm.DrStr(dr, c[0]) });
                }
                dr.Close();
            }
        }

        private void Process2(ref List<Result> l)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            for (int i = 0; i < l.Count; i++)
            {
                SQL.AppendLine("Update TblPurchaseInvoiceRawMaterialHdr A ");
                SQL.AppendLine("Inner Join TblVoucherHdr B On A.VoucherRequestDocNo=B.VoucherRequestDocNo And B.CancelInd='N' ");
                SQL.AppendLine("Set A.TaxSlip=@TaxSlip"+i.ToString());
                SQL.AppendLine(" Where A.DocNo=@DocNo" + i.ToString());
                SQL.AppendLine(";");

                Sm.CmParam<String>(ref cm, "@TaxSlip" + i.ToString(), string.Concat(Sm.Right(string.Concat("00000", (i+1).ToString()), 6), "/PPH22"));
                Sm.CmParam<String>(ref cm, "@DocNo" + i.ToString(), l[i].DocNo);
            }

            cm.CommandText = SQL.ToString();

            var cml = new List<MySqlCommand>();
            cml.Add(cm);
            Sm.ExecCommands(cml);
        }

        private bool IsProcessedDataNotValid()
        {
            return
                Sm.IsLueEmpty(LueYr, "Year") ||
                Sm.IsLueEmpty(LueMth, "Month") ||
                IsThereNoDataToBeProcessed();
        }

        private bool IsThereNoDataToBeProcessed()
        { 
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 ");
            SQL.AppendLine("From TblPurchaseInvoiceRawMaterialHdr A ");
            SQL.AppendLine("Inner Join TblVoucherHdr B On A.VoucherRequestDocNo=B.DocNo And B.CancelInd='N' ");
            SQL.AppendLine("Where Left(A.DocDt, 6)=@Param ");
            SQL.AppendLine("And A.CancelInd='N' ");
            SQL.AppendLine("Limit 1;");

            return !Sm.IsDataExist(SQL.ToString(), string.Concat(Sm.GetLue(LueYr), Sm.GetLue(LueMth)));
        }

        #endregion

        #region Class

        private class Result
        {
            public string DocNo { get; set; }
        }

        #endregion
    }
}
