﻿namespace RunSystem
{
    partial class FrmPettyCashDisbursement
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmPettyCashDisbursement));
            this.panel3 = new System.Windows.Forms.Panel();
            this.TcHeader = new DevExpress.XtraTab.XtraTabControl();
            this.TpgGeneral = new DevExpress.XtraTab.XtraTabPage();
            this.panel8 = new System.Windows.Forms.Panel();
            this.LueCurCode1 = new DevExpress.XtraEditors.LookUpEdit();
            this.LuePICCode = new DevExpress.XtraEditors.LookUpEdit();
            this.LueDeptCode = new DevExpress.XtraEditors.LookUpEdit();
            this.panel9 = new System.Windows.Forms.Panel();
            this.label11 = new System.Windows.Forms.Label();
            this.LueCashType = new DevExpress.XtraEditors.LookUpEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.LueCashTypeGrp = new DevExpress.XtraEditors.LookUpEdit();
            this.BtnCopyData = new DevExpress.XtraEditors.SimpleButton();
            this.TxtCopyData = new DevExpress.XtraEditors.TextEdit();
            this.LblCopyData = new System.Windows.Forms.Label();
            this.TxtLocalDocument = new DevExpress.XtraEditors.TextEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.LueCCCode = new DevExpress.XtraEditors.LookUpEdit();
            this.LblCCCode = new System.Windows.Forms.Label();
            this.LueBankAcCode = new DevExpress.XtraEditors.LookUpEdit();
            this.LblBankAcCode = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.TxtStatus = new DevExpress.XtraEditors.TextEdit();
            this.ChkCancelInd = new DevExpress.XtraEditors.CheckEdit();
            this.MeeCancelReason = new DevExpress.XtraEditors.MemoExEdit();
            this.label17 = new System.Windows.Forms.Label();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.TpgDroppingRequest = new DevExpress.XtraTab.XtraTabPage();
            this.panel10 = new System.Windows.Forms.Panel();
            this.MeeRemarkDRQ = new DevExpress.XtraEditors.MemoExEdit();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.TxtPCDTotalAmount = new DevExpress.XtraEditors.TextEdit();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.TxtBudgetCategory = new DevExpress.XtraEditors.TextEdit();
            this.label19 = new System.Windows.Forms.Label();
            this.TxtPRJIDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label18 = new System.Windows.Forms.Label();
            this.TxtMonth = new DevExpress.XtraEditors.TextEdit();
            this.label16 = new System.Windows.Forms.Label();
            this.TxtYear = new DevExpress.XtraEditors.TextEdit();
            this.label15 = new System.Windows.Forms.Label();
            this.TxtDepartment = new DevExpress.XtraEditors.TextEdit();
            this.label13 = new System.Windows.Forms.Label();
            this.BtnDRQDocNo = new DevExpress.XtraEditors.SimpleButton();
            this.BtnDRQ = new DevExpress.XtraEditors.SimpleButton();
            this.TxtDRQDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.BtnVoucher = new DevExpress.XtraEditors.SimpleButton();
            this.TxtVoucherDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.Tc1 = new DevExpress.XtraTab.XtraTabControl();
            this.Tp1 = new DevExpress.XtraTab.XtraTabPage();
            this.panel4 = new System.Windows.Forms.Panel();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.LueCurCode2 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd1 = new TenTec.Windows.iGridLib.iGrid();
            this.panel7 = new System.Windows.Forms.Panel();
            this.TxtAmt2 = new DevExpress.XtraEditors.TextEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.TxtAmt1 = new DevExpress.XtraEditors.TextEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.Tp2 = new DevExpress.XtraTab.XtraTabPage();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.PbUpload = new System.Windows.Forms.ProgressBar();
            this.BtnFile = new DevExpress.XtraEditors.SimpleButton();
            this.ChkFile = new DevExpress.XtraEditors.CheckEdit();
            this.TxtFile = new DevExpress.XtraEditors.TextEdit();
            this.LblFile = new System.Windows.Forms.Label();
            this.Tp3 = new DevExpress.XtraTab.XtraTabPage();
            this.Grd5 = new TenTec.Windows.iGridLib.iGrid();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.SFD = new System.Windows.Forms.SaveFileDialog();
            this.OD = new System.Windows.Forms.OpenFileDialog();
            this.ChkHideInfoInGrd = new DevExpress.XtraEditors.CheckEdit();
            this.TxtDRQAmount = new DevExpress.XtraEditors.TextEdit();
            this.TxtBalance = new DevExpress.XtraEditors.TextEdit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TcHeader)).BeginInit();
            this.TcHeader.SuspendLayout();
            this.TpgGeneral.SuspendLayout();
            this.panel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePICCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDeptCode.Properties)).BeginInit();
            this.panel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueCashType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCashTypeGrp.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCopyData.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLocalDocument.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCCCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBankAcCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCancelReason.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            this.TpgDroppingRequest.SuspendLayout();
            this.panel10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemarkDRQ.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPCDTotalAmount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBudgetCategory.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPRJIDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMonth.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtYear.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDepartment.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDRQDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVoucherDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Tc1)).BeginInit();
            this.Tc1.SuspendLayout();
            this.Tp1.SuspendLayout();
            this.panel4.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAmt2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAmt1.Properties)).BeginInit();
            this.Tp2.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile.Properties)).BeginInit();
            this.Tp3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd5)).BeginInit();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDRQAmount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBalance.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.ChkHideInfoInGrd);
            this.panel1.Location = new System.Drawing.Point(951, 0);
            this.panel1.Size = new System.Drawing.Size(70, 536);
            this.panel1.Controls.SetChildIndex(this.BtnFind, 0);
            this.panel1.Controls.SetChildIndex(this.BtnInsert, 0);
            this.panel1.Controls.SetChildIndex(this.BtnEdit, 0);
            this.panel1.Controls.SetChildIndex(this.BtnDelete, 0);
            this.panel1.Controls.SetChildIndex(this.BtnSave, 0);
            this.panel1.Controls.SetChildIndex(this.BtnCancel, 0);
            this.panel1.Controls.SetChildIndex(this.BtnPrint, 0);
            this.panel1.Controls.SetChildIndex(this.ChkHideInfoInGrd, 0);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.Tc1);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Size = new System.Drawing.Size(951, 536);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.TcHeader);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(951, 264);
            this.panel3.TabIndex = 8;
            // 
            // TcHeader
            // 
            this.TcHeader.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TcHeader.HeaderAutoFill = DevExpress.Utils.DefaultBoolean.True;
            this.TcHeader.Location = new System.Drawing.Point(0, 0);
            this.TcHeader.Name = "TcHeader";
            this.TcHeader.SelectedTabPage = this.TpgGeneral;
            this.TcHeader.Size = new System.Drawing.Size(951, 264);
            this.TcHeader.TabIndex = 13;
            this.TcHeader.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.TpgGeneral,
            this.TpgDroppingRequest});
            // 
            // TpgGeneral
            // 
            this.TpgGeneral.Appearance.Header.Options.UseTextOptions = true;
            this.TpgGeneral.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TpgGeneral.Controls.Add(this.panel8);
            this.TpgGeneral.Name = "TpgGeneral";
            this.TpgGeneral.Size = new System.Drawing.Size(945, 236);
            this.TpgGeneral.Text = "General";
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel8.Controls.Add(this.LueCurCode1);
            this.panel8.Controls.Add(this.LuePICCode);
            this.panel8.Controls.Add(this.LueDeptCode);
            this.panel8.Controls.Add(this.panel9);
            this.panel8.Controls.Add(this.TxtLocalDocument);
            this.panel8.Controls.Add(this.label10);
            this.panel8.Controls.Add(this.LueCCCode);
            this.panel8.Controls.Add(this.LblCCCode);
            this.panel8.Controls.Add(this.LueBankAcCode);
            this.panel8.Controls.Add(this.LblBankAcCode);
            this.panel8.Controls.Add(this.label14);
            this.panel8.Controls.Add(this.label7);
            this.panel8.Controls.Add(this.label3);
            this.panel8.Controls.Add(this.MeeRemark);
            this.panel8.Controls.Add(this.label5);
            this.panel8.Controls.Add(this.label27);
            this.panel8.Controls.Add(this.TxtStatus);
            this.panel8.Controls.Add(this.ChkCancelInd);
            this.panel8.Controls.Add(this.MeeCancelReason);
            this.panel8.Controls.Add(this.label17);
            this.panel8.Controls.Add(this.TxtDocNo);
            this.panel8.Controls.Add(this.DteDocDt);
            this.panel8.Controls.Add(this.label1);
            this.panel8.Controls.Add(this.label2);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel8.Location = new System.Drawing.Point(0, 0);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(945, 236);
            this.panel8.TabIndex = 12;
            // 
            // LueCurCode1
            // 
            this.LueCurCode1.EnterMoveNextControl = true;
            this.LueCurCode1.Location = new System.Drawing.Point(141, 152);
            this.LueCurCode1.Margin = new System.Windows.Forms.Padding(5);
            this.LueCurCode1.Name = "LueCurCode1";
            this.LueCurCode1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode1.Properties.Appearance.Options.UseFont = true;
            this.LueCurCode1.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode1.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCurCode1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCurCode1.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode1.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCurCode1.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode1.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCurCode1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCurCode1.Properties.DropDownRows = 25;
            this.LueCurCode1.Properties.NullText = "[Empty]";
            this.LueCurCode1.Properties.PopupWidth = 220;
            this.LueCurCode1.Size = new System.Drawing.Size(314, 20);
            this.LueCurCode1.TabIndex = 92;
            this.LueCurCode1.ToolTip = "F4 : Show/hide list";
            this.LueCurCode1.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCurCode1.EditValueChanged += new System.EventHandler(this.LueCurCode1_EditValueChanged);
            // 
            // LuePICCode
            // 
            this.LuePICCode.EnterMoveNextControl = true;
            this.LuePICCode.Location = new System.Drawing.Point(141, 110);
            this.LuePICCode.Margin = new System.Windows.Forms.Padding(5);
            this.LuePICCode.Name = "LuePICCode";
            this.LuePICCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePICCode.Properties.Appearance.Options.UseFont = true;
            this.LuePICCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePICCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePICCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePICCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePICCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePICCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePICCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePICCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePICCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePICCode.Properties.DropDownRows = 30;
            this.LuePICCode.Properties.NullText = "[Empty]";
            this.LuePICCode.Properties.PopupWidth = 300;
            this.LuePICCode.Size = new System.Drawing.Size(314, 20);
            this.LuePICCode.TabIndex = 91;
            this.LuePICCode.ToolTip = "F4 : Show/hide list";
            this.LuePICCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LuePICCode.EditValueChanged += new System.EventHandler(this.LuePICCode_EditValueChanged);
            // 
            // LueDeptCode
            // 
            this.LueDeptCode.EnterMoveNextControl = true;
            this.LueDeptCode.Location = new System.Drawing.Point(140, 131);
            this.LueDeptCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueDeptCode.Name = "LueDeptCode";
            this.LueDeptCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.Appearance.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueDeptCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueDeptCode.Properties.DropDownRows = 30;
            this.LueDeptCode.Properties.NullText = "[Empty]";
            this.LueDeptCode.Properties.PopupWidth = 300;
            this.LueDeptCode.Size = new System.Drawing.Size(315, 20);
            this.LueDeptCode.TabIndex = 90;
            this.LueDeptCode.ToolTip = "F4 : Show/hide list";
            this.LueDeptCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueDeptCode.EditValueChanged += new System.EventHandler(this.LueDeptCode_EditValueChanged);
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.label11);
            this.panel9.Controls.Add(this.LueCashType);
            this.panel9.Controls.Add(this.label4);
            this.panel9.Controls.Add(this.LueCashTypeGrp);
            this.panel9.Controls.Add(this.BtnCopyData);
            this.panel9.Controls.Add(this.TxtCopyData);
            this.panel9.Controls.Add(this.LblCopyData);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel9.Location = new System.Drawing.Point(553, 0);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(392, 236);
            this.panel9.TabIndex = 89;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Red;
            this.label11.Location = new System.Drawing.Point(38, 52);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(64, 14);
            this.label11.TabIndex = 44;
            this.label11.Text = "Cash Type";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueCashType
            // 
            this.LueCashType.EnterMoveNextControl = true;
            this.LueCashType.Location = new System.Drawing.Point(104, 50);
            this.LueCashType.Margin = new System.Windows.Forms.Padding(5);
            this.LueCashType.Name = "LueCashType";
            this.LueCashType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCashType.Properties.Appearance.Options.UseFont = true;
            this.LueCashType.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCashType.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCashType.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCashType.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCashType.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCashType.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCashType.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCashType.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCashType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCashType.Properties.DropDownRows = 30;
            this.LueCashType.Properties.NullText = "[Empty]";
            this.LueCashType.Properties.PopupWidth = 530;
            this.LueCashType.Size = new System.Drawing.Size(250, 20);
            this.LueCashType.TabIndex = 43;
            this.LueCashType.ToolTip = "F4 : Show/hide list";
            this.LueCashType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCashType.EditValueChanged += new System.EventHandler(this.LueCashType_EditValueChanged);
            this.LueCashType.Validated += new System.EventHandler(this.LueCashType_Validated);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(2, 31);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(101, 14);
            this.label4.TabIndex = 42;
            this.label4.Text = "Cash Type Group";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueCashTypeGrp
            // 
            this.LueCashTypeGrp.EnterMoveNextControl = true;
            this.LueCashTypeGrp.Location = new System.Drawing.Point(104, 29);
            this.LueCashTypeGrp.Margin = new System.Windows.Forms.Padding(5);
            this.LueCashTypeGrp.Name = "LueCashTypeGrp";
            this.LueCashTypeGrp.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCashTypeGrp.Properties.Appearance.Options.UseFont = true;
            this.LueCashTypeGrp.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCashTypeGrp.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCashTypeGrp.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCashTypeGrp.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCashTypeGrp.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCashTypeGrp.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCashTypeGrp.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCashTypeGrp.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCashTypeGrp.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCashTypeGrp.Properties.DropDownRows = 30;
            this.LueCashTypeGrp.Properties.NullText = "[Empty]";
            this.LueCashTypeGrp.Properties.PopupWidth = 530;
            this.LueCashTypeGrp.Size = new System.Drawing.Size(250, 20);
            this.LueCashTypeGrp.TabIndex = 41;
            this.LueCashTypeGrp.ToolTip = "F4 : Show/hide list";
            this.LueCashTypeGrp.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCashTypeGrp.EditValueChanged += new System.EventHandler(this.LueCashTypeGrp_EditValueChanged);
            this.LueCashTypeGrp.Validated += new System.EventHandler(this.LueCashTypeGrp_Validated);
            // 
            // BtnCopyData
            // 
            this.BtnCopyData.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnCopyData.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCopyData.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCopyData.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCopyData.Appearance.Options.UseBackColor = true;
            this.BtnCopyData.Appearance.Options.UseFont = true;
            this.BtnCopyData.Appearance.Options.UseForeColor = true;
            this.BtnCopyData.Appearance.Options.UseTextOptions = true;
            this.BtnCopyData.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnCopyData.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnCopyData.Image = ((System.Drawing.Image)(resources.GetObject("BtnCopyData.Image")));
            this.BtnCopyData.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnCopyData.Location = new System.Drawing.Point(362, 7);
            this.BtnCopyData.Name = "BtnCopyData";
            this.BtnCopyData.Size = new System.Drawing.Size(24, 21);
            this.BtnCopyData.TabIndex = 40;
            this.BtnCopyData.ToolTip = "Show VR";
            this.BtnCopyData.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnCopyData.ToolTipTitle = "Run System";
            this.BtnCopyData.Click += new System.EventHandler(this.BtnCopyData_Click);
            // 
            // TxtCopyData
            // 
            this.TxtCopyData.EnterMoveNextControl = true;
            this.TxtCopyData.Location = new System.Drawing.Point(104, 7);
            this.TxtCopyData.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCopyData.Name = "TxtCopyData";
            this.TxtCopyData.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtCopyData.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCopyData.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCopyData.Properties.Appearance.Options.UseFont = true;
            this.TxtCopyData.Properties.MaxLength = 30;
            this.TxtCopyData.Properties.ReadOnly = true;
            this.TxtCopyData.Size = new System.Drawing.Size(250, 20);
            this.TxtCopyData.TabIndex = 40;
            // 
            // LblCopyData
            // 
            this.LblCopyData.AutoSize = true;
            this.LblCopyData.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblCopyData.ForeColor = System.Drawing.Color.Black;
            this.LblCopyData.Location = new System.Drawing.Point(39, 10);
            this.LblCopyData.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblCopyData.Name = "LblCopyData";
            this.LblCopyData.Size = new System.Drawing.Size(63, 14);
            this.LblCopyData.TabIndex = 39;
            this.LblCopyData.Text = "Copy Data";
            this.LblCopyData.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtLocalDocument
            // 
            this.TxtLocalDocument.EnterMoveNextControl = true;
            this.TxtLocalDocument.Location = new System.Drawing.Point(141, 26);
            this.TxtLocalDocument.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLocalDocument.Name = "TxtLocalDocument";
            this.TxtLocalDocument.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtLocalDocument.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLocalDocument.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLocalDocument.Properties.Appearance.Options.UseFont = true;
            this.TxtLocalDocument.Properties.MaxLength = 30;
            this.TxtLocalDocument.Properties.ReadOnly = true;
            this.TxtLocalDocument.Size = new System.Drawing.Size(314, 20);
            this.TxtLocalDocument.TabIndex = 72;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(78, 29);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(59, 14);
            this.label10.TabIndex = 71;
            this.label10.Text = "Local Doc";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueCCCode
            // 
            this.LueCCCode.EnterMoveNextControl = true;
            this.LueCCCode.Location = new System.Drawing.Point(141, 195);
            this.LueCCCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueCCCode.Name = "LueCCCode";
            this.LueCCCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCCCode.Properties.Appearance.Options.UseFont = true;
            this.LueCCCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCCCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCCCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCCCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCCCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCCCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCCCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCCCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCCCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCCCode.Properties.DropDownRows = 30;
            this.LueCCCode.Properties.NullText = "[Empty]";
            this.LueCCCode.Properties.PopupWidth = 530;
            this.LueCCCode.Size = new System.Drawing.Size(314, 20);
            this.LueCCCode.TabIndex = 83;
            this.LueCCCode.ToolTip = "F4 : Show/hide list";
            this.LueCCCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCCCode.EditValueChanged += new System.EventHandler(this.LueCCCode_EditValueChanged);
            this.LueCCCode.Validated += new System.EventHandler(this.LueCCCode_Validated);
            // 
            // LblCCCode
            // 
            this.LblCCCode.AutoSize = true;
            this.LblCCCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblCCCode.ForeColor = System.Drawing.Color.Red;
            this.LblCCCode.Location = new System.Drawing.Point(65, 197);
            this.LblCCCode.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblCCCode.Name = "LblCCCode";
            this.LblCCCode.Size = new System.Drawing.Size(72, 14);
            this.LblCCCode.TabIndex = 82;
            this.LblCCCode.Text = "Cost Center";
            this.LblCCCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueBankAcCode
            // 
            this.LueBankAcCode.EnterMoveNextControl = true;
            this.LueBankAcCode.Location = new System.Drawing.Point(141, 174);
            this.LueBankAcCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueBankAcCode.Name = "LueBankAcCode";
            this.LueBankAcCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankAcCode.Properties.Appearance.Options.UseFont = true;
            this.LueBankAcCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankAcCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueBankAcCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankAcCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueBankAcCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankAcCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueBankAcCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankAcCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueBankAcCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueBankAcCode.Properties.DropDownRows = 30;
            this.LueBankAcCode.Properties.NullText = "[Empty]";
            this.LueBankAcCode.Properties.PopupWidth = 530;
            this.LueBankAcCode.Size = new System.Drawing.Size(314, 20);
            this.LueBankAcCode.TabIndex = 85;
            this.LueBankAcCode.ToolTip = "F4 : Show/hide list";
            this.LueBankAcCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueBankAcCode.Validated += new System.EventHandler(this.LueBankAcCode_Validated);
            // 
            // LblBankAcCode
            // 
            this.LblBankAcCode.AutoSize = true;
            this.LblBankAcCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblBankAcCode.ForeColor = System.Drawing.Color.Red;
            this.LblBankAcCode.Location = new System.Drawing.Point(54, 177);
            this.LblBankAcCode.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblBankAcCode.Name = "LblBankAcCode";
            this.LblBankAcCode.Size = new System.Drawing.Size(83, 14);
            this.LblBankAcCode.TabIndex = 84;
            this.LblBankAcCode.Text = "Bank Account";
            this.LblBankAcCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(82, 156);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(55, 14);
            this.label14.TabIndex = 81;
            this.label14.Text = "Currency";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Red;
            this.label7.Location = new System.Drawing.Point(36, 113);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(101, 14);
            this.label7.TabIndex = 79;
            this.label7.Text = "Person In Charge";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(64, 135);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 14);
            this.label3.TabIndex = 80;
            this.label3.Text = "Department";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeRemark
            // 
            this.MeeRemark.EditValue = "";
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(140, 216);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 1000;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(600, 70);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(409, 20);
            this.MeeRemark.TabIndex = 86;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(89, 218);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 14);
            this.label5.TabIndex = 87;
            this.label5.Text = "Remark";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(2, 92);
            this.label27.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(135, 14);
            this.label27.TabIndex = 77;
            this.label27.Text = "Reason For Cancellation";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtStatus
            // 
            this.TxtStatus.EnterMoveNextControl = true;
            this.TxtStatus.Location = new System.Drawing.Point(141, 68);
            this.TxtStatus.Margin = new System.Windows.Forms.Padding(5);
            this.TxtStatus.Name = "TxtStatus";
            this.TxtStatus.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtStatus.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtStatus.Properties.Appearance.Options.UseBackColor = true;
            this.TxtStatus.Properties.Appearance.Options.UseFont = true;
            this.TxtStatus.Properties.MaxLength = 16;
            this.TxtStatus.Properties.ReadOnly = true;
            this.TxtStatus.Size = new System.Drawing.Size(121, 20);
            this.TxtStatus.TabIndex = 76;
            // 
            // ChkCancelInd
            // 
            this.ChkCancelInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkCancelInd.Location = new System.Drawing.Point(464, 87);
            this.ChkCancelInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkCancelInd.Name = "ChkCancelInd";
            this.ChkCancelInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkCancelInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkCancelInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkCancelInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseFont = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkCancelInd.Properties.Caption = "Cancel";
            this.ChkCancelInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkCancelInd.Size = new System.Drawing.Size(61, 22);
            this.ChkCancelInd.TabIndex = 88;
            this.ChkCancelInd.CheckedChanged += new System.EventHandler(this.ChkCancelInd_CheckedChanged);
            // 
            // MeeCancelReason
            // 
            this.MeeCancelReason.EnterMoveNextControl = true;
            this.MeeCancelReason.Location = new System.Drawing.Point(141, 89);
            this.MeeCancelReason.Margin = new System.Windows.Forms.Padding(5);
            this.MeeCancelReason.Name = "MeeCancelReason";
            this.MeeCancelReason.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.Appearance.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeCancelReason.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCancelReason.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeCancelReason.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeCancelReason.Properties.MaxLength = 1000;
            this.MeeCancelReason.Properties.PopupFormSize = new System.Drawing.Size(530, 20);
            this.MeeCancelReason.Properties.ShowIcon = false;
            this.MeeCancelReason.Size = new System.Drawing.Size(314, 20);
            this.MeeCancelReason.TabIndex = 78;
            this.MeeCancelReason.ToolTip = "F4 : Show/hide text";
            this.MeeCancelReason.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeCancelReason.ToolTipTitle = "Run System";
            this.MeeCancelReason.Validated += new System.EventHandler(this.MeeCancelReason_Validated);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(44, 70);
            this.label17.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(93, 14);
            this.label17.TabIndex = 75;
            this.label17.Text = "Approval Status";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(141, 5);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 30;
            this.TxtDocNo.Properties.ReadOnly = true;
            this.TxtDocNo.Size = new System.Drawing.Size(314, 20);
            this.TxtDocNo.TabIndex = 70;
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(141, 47);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.MaxLength = 8;
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(121, 20);
            this.DteDocDt.TabIndex = 74;
            this.DteDocDt.EditValueChanged += new System.EventHandler(this.DteDocDt_EditValueChanged);
            this.DteDocDt.Validated += new System.EventHandler(this.DteDocDt_Validated);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(64, 8);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 14);
            this.label1.TabIndex = 69;
            this.label1.Text = "Document#";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(104, 51);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 14);
            this.label2.TabIndex = 73;
            this.label2.Text = "Date";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TpgDroppingRequest
            // 
            this.TpgDroppingRequest.Appearance.Header.Options.UseTextOptions = true;
            this.TpgDroppingRequest.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TpgDroppingRequest.Controls.Add(this.panel10);
            this.TpgDroppingRequest.Name = "TpgDroppingRequest";
            this.TpgDroppingRequest.PageVisible = false;
            this.TpgDroppingRequest.Size = new System.Drawing.Size(945, 236);
            this.TpgDroppingRequest.Text = "For Dropping Request";
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel10.Controls.Add(this.TxtBalance);
            this.panel10.Controls.Add(this.TxtDRQAmount);
            this.panel10.Controls.Add(this.MeeRemarkDRQ);
            this.panel10.Controls.Add(this.label23);
            this.panel10.Controls.Add(this.label22);
            this.panel10.Controls.Add(this.TxtPCDTotalAmount);
            this.panel10.Controls.Add(this.label21);
            this.panel10.Controls.Add(this.label20);
            this.panel10.Controls.Add(this.TxtBudgetCategory);
            this.panel10.Controls.Add(this.label19);
            this.panel10.Controls.Add(this.TxtPRJIDocNo);
            this.panel10.Controls.Add(this.label18);
            this.panel10.Controls.Add(this.TxtMonth);
            this.panel10.Controls.Add(this.label16);
            this.panel10.Controls.Add(this.TxtYear);
            this.panel10.Controls.Add(this.label15);
            this.panel10.Controls.Add(this.TxtDepartment);
            this.panel10.Controls.Add(this.label13);
            this.panel10.Controls.Add(this.BtnDRQDocNo);
            this.panel10.Controls.Add(this.BtnDRQ);
            this.panel10.Controls.Add(this.TxtDRQDocNo);
            this.panel10.Controls.Add(this.label12);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel10.Location = new System.Drawing.Point(0, 0);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(945, 236);
            this.panel10.TabIndex = 12;
            // 
            // MeeRemarkDRQ
            // 
            this.MeeRemarkDRQ.EditValue = "";
            this.MeeRemarkDRQ.EnterMoveNextControl = true;
            this.MeeRemarkDRQ.Location = new System.Drawing.Point(155, 196);
            this.MeeRemarkDRQ.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemarkDRQ.Name = "MeeRemarkDRQ";
            this.MeeRemarkDRQ.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.MeeRemarkDRQ.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkDRQ.Properties.Appearance.Options.UseBackColor = true;
            this.MeeRemarkDRQ.Properties.Appearance.Options.UseFont = true;
            this.MeeRemarkDRQ.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkDRQ.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemarkDRQ.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkDRQ.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemarkDRQ.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkDRQ.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemarkDRQ.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkDRQ.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemarkDRQ.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemarkDRQ.Properties.MaxLength = 1000;
            this.MeeRemarkDRQ.Properties.PopupFormSize = new System.Drawing.Size(600, 70);
            this.MeeRemarkDRQ.Properties.ShowIcon = false;
            this.MeeRemarkDRQ.Size = new System.Drawing.Size(314, 20);
            this.MeeRemarkDRQ.TabIndex = 91;
            this.MeeRemarkDRQ.ToolTip = "F4 : Show/hide text";
            this.MeeRemarkDRQ.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemarkDRQ.ToolTipTitle = "Run System";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(102, 198);
            this.label23.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(47, 14);
            this.label23.TabIndex = 92;
            this.label23.Text = "Remark";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(104, 177);
            this.label22.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(48, 14);
            this.label22.TabIndex = 89;
            this.label22.Text = "Balance";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPCDTotalAmount
            // 
            this.TxtPCDTotalAmount.EnterMoveNextControl = true;
            this.TxtPCDTotalAmount.Location = new System.Drawing.Point(155, 153);
            this.TxtPCDTotalAmount.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPCDTotalAmount.Name = "TxtPCDTotalAmount";
            this.TxtPCDTotalAmount.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtPCDTotalAmount.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPCDTotalAmount.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPCDTotalAmount.Properties.Appearance.Options.UseFont = true;
            this.TxtPCDTotalAmount.Properties.MaxLength = 30;
            this.TxtPCDTotalAmount.Properties.ReadOnly = true;
            this.TxtPCDTotalAmount.Size = new System.Drawing.Size(314, 20);
            this.TxtPCDTotalAmount.TabIndex = 88;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Black;
            this.label21.Location = new System.Drawing.Point(35, 156);
            this.label21.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(117, 14);
            this.label21.TabIndex = 87;
            this.label21.Text = "PCD\'s Total Amount";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(2, 134);
            this.label20.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(153, 14);
            this.label20.TabIndex = 85;
            this.label20.Text = "Dropping Request Amount";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtBudgetCategory
            // 
            this.TxtBudgetCategory.EnterMoveNextControl = true;
            this.TxtBudgetCategory.Location = new System.Drawing.Point(155, 111);
            this.TxtBudgetCategory.Margin = new System.Windows.Forms.Padding(5);
            this.TxtBudgetCategory.Name = "TxtBudgetCategory";
            this.TxtBudgetCategory.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtBudgetCategory.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBudgetCategory.Properties.Appearance.Options.UseBackColor = true;
            this.TxtBudgetCategory.Properties.Appearance.Options.UseFont = true;
            this.TxtBudgetCategory.Properties.MaxLength = 30;
            this.TxtBudgetCategory.Properties.ReadOnly = true;
            this.TxtBudgetCategory.Size = new System.Drawing.Size(314, 20);
            this.TxtBudgetCategory.TabIndex = 84;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(53, 114);
            this.label19.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(100, 14);
            this.label19.TabIndex = 83;
            this.label19.Text = "Budget Category";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPRJIDocNo
            // 
            this.TxtPRJIDocNo.EnterMoveNextControl = true;
            this.TxtPRJIDocNo.Location = new System.Drawing.Point(155, 90);
            this.TxtPRJIDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPRJIDocNo.Name = "TxtPRJIDocNo";
            this.TxtPRJIDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtPRJIDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPRJIDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPRJIDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtPRJIDocNo.Properties.MaxLength = 30;
            this.TxtPRJIDocNo.Properties.ReadOnly = true;
            this.TxtPRJIDocNo.Size = new System.Drawing.Size(314, 20);
            this.TxtPRJIDocNo.TabIndex = 82;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(8, 93);
            this.label18.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(145, 14);
            this.label18.TabIndex = 81;
            this.label18.Text = "Project Implementation#";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtMonth
            // 
            this.TxtMonth.EnterMoveNextControl = true;
            this.TxtMonth.Location = new System.Drawing.Point(155, 69);
            this.TxtMonth.Margin = new System.Windows.Forms.Padding(5);
            this.TxtMonth.Name = "TxtMonth";
            this.TxtMonth.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtMonth.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtMonth.Properties.Appearance.Options.UseBackColor = true;
            this.TxtMonth.Properties.Appearance.Options.UseFont = true;
            this.TxtMonth.Properties.MaxLength = 30;
            this.TxtMonth.Properties.ReadOnly = true;
            this.TxtMonth.Size = new System.Drawing.Size(314, 20);
            this.TxtMonth.TabIndex = 80;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(110, 71);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(42, 14);
            this.label16.TabIndex = 79;
            this.label16.Text = "Month";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtYear
            // 
            this.TxtYear.EnterMoveNextControl = true;
            this.TxtYear.Location = new System.Drawing.Point(155, 48);
            this.TxtYear.Margin = new System.Windows.Forms.Padding(5);
            this.TxtYear.Name = "TxtYear";
            this.TxtYear.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtYear.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtYear.Properties.Appearance.Options.UseBackColor = true;
            this.TxtYear.Properties.Appearance.Options.UseFont = true;
            this.TxtYear.Properties.MaxLength = 30;
            this.TxtYear.Properties.ReadOnly = true;
            this.TxtYear.Size = new System.Drawing.Size(314, 20);
            this.TxtYear.TabIndex = 78;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(120, 50);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(32, 14);
            this.label15.TabIndex = 77;
            this.label15.Text = "Year";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtDepartment
            // 
            this.TxtDepartment.EnterMoveNextControl = true;
            this.TxtDepartment.Location = new System.Drawing.Point(155, 27);
            this.TxtDepartment.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDepartment.Name = "TxtDepartment";
            this.TxtDepartment.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDepartment.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDepartment.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDepartment.Properties.Appearance.Options.UseFont = true;
            this.TxtDepartment.Properties.MaxLength = 30;
            this.TxtDepartment.Properties.ReadOnly = true;
            this.TxtDepartment.Size = new System.Drawing.Size(314, 20);
            this.TxtDepartment.TabIndex = 76;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(80, 28);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(73, 14);
            this.label13.TabIndex = 75;
            this.label13.Text = "Department";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnDRQDocNo
            // 
            this.BtnDRQDocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDRQDocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDRQDocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDRQDocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDRQDocNo.Appearance.Options.UseBackColor = true;
            this.BtnDRQDocNo.Appearance.Options.UseFont = true;
            this.BtnDRQDocNo.Appearance.Options.UseForeColor = true;
            this.BtnDRQDocNo.Appearance.Options.UseTextOptions = true;
            this.BtnDRQDocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDRQDocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDRQDocNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnDRQDocNo.Image")));
            this.BtnDRQDocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDRQDocNo.Location = new System.Drawing.Point(474, 5);
            this.BtnDRQDocNo.Name = "BtnDRQDocNo";
            this.BtnDRQDocNo.Size = new System.Drawing.Size(24, 21);
            this.BtnDRQDocNo.TabIndex = 73;
            this.BtnDRQDocNo.ToolTip = "Show Customer\'s Data";
            this.BtnDRQDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDRQDocNo.ToolTipTitle = "Run System";
            this.BtnDRQDocNo.Click += new System.EventHandler(this.BtnDRQDocNo_Click);
            // 
            // BtnDRQ
            // 
            this.BtnDRQ.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDRQ.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDRQ.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDRQ.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDRQ.Appearance.Options.UseBackColor = true;
            this.BtnDRQ.Appearance.Options.UseFont = true;
            this.BtnDRQ.Appearance.Options.UseForeColor = true;
            this.BtnDRQ.Appearance.Options.UseTextOptions = true;
            this.BtnDRQ.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDRQ.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDRQ.Image = ((System.Drawing.Image)(resources.GetObject("BtnDRQ.Image")));
            this.BtnDRQ.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDRQ.Location = new System.Drawing.Point(499, 5);
            this.BtnDRQ.Name = "BtnDRQ";
            this.BtnDRQ.Size = new System.Drawing.Size(24, 21);
            this.BtnDRQ.TabIndex = 74;
            this.BtnDRQ.ToolTip = "Copy from existed Invoice";
            this.BtnDRQ.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDRQ.ToolTipTitle = "Run System";
            this.BtnDRQ.Click += new System.EventHandler(this.BtnDRQ_Click);
            // 
            // TxtDRQDocNo
            // 
            this.TxtDRQDocNo.EnterMoveNextControl = true;
            this.TxtDRQDocNo.Location = new System.Drawing.Point(155, 6);
            this.TxtDRQDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDRQDocNo.Name = "TxtDRQDocNo";
            this.TxtDRQDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDRQDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDRQDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDRQDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDRQDocNo.Properties.MaxLength = 30;
            this.TxtDRQDocNo.Properties.ReadOnly = true;
            this.TxtDRQDocNo.Size = new System.Drawing.Size(314, 20);
            this.TxtDRQDocNo.TabIndex = 72;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(44, 8);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(114, 14);
            this.label12.TabIndex = 71;
            this.label12.Text = "Dropping Request#";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnVoucher
            // 
            this.BtnVoucher.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnVoucher.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnVoucher.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnVoucher.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnVoucher.Appearance.Options.UseBackColor = true;
            this.BtnVoucher.Appearance.Options.UseFont = true;
            this.BtnVoucher.Appearance.Options.UseForeColor = true;
            this.BtnVoucher.Appearance.Options.UseTextOptions = true;
            this.BtnVoucher.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnVoucher.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnVoucher.Image = ((System.Drawing.Image)(resources.GetObject("BtnVoucher.Image")));
            this.BtnVoucher.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnVoucher.Location = new System.Drawing.Point(252, 16);
            this.BtnVoucher.Name = "BtnVoucher";
            this.BtnVoucher.Size = new System.Drawing.Size(24, 21);
            this.BtnVoucher.TabIndex = 33;
            this.BtnVoucher.ToolTip = "Show VR";
            this.BtnVoucher.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnVoucher.ToolTipTitle = "Run System";
            this.BtnVoucher.Click += new System.EventHandler(this.BtnVoucher_Click);
            // 
            // TxtVoucherDocNo
            // 
            this.TxtVoucherDocNo.EnterMoveNextControl = true;
            this.TxtVoucherDocNo.Location = new System.Drawing.Point(69, 17);
            this.TxtVoucherDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtVoucherDocNo.Name = "TxtVoucherDocNo";
            this.TxtVoucherDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtVoucherDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtVoucherDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtVoucherDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtVoucherDocNo.Properties.MaxLength = 30;
            this.TxtVoucherDocNo.Properties.ReadOnly = true;
            this.TxtVoucherDocNo.Size = new System.Drawing.Size(175, 20);
            this.TxtVoucherDocNo.TabIndex = 32;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(8, 20);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(62, 14);
            this.label9.TabIndex = 31;
            this.label9.Text = "Voucher#";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Tc1
            // 
            this.Tc1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Tc1.HeaderAutoFill = DevExpress.Utils.DefaultBoolean.True;
            this.Tc1.Location = new System.Drawing.Point(0, 264);
            this.Tc1.Name = "Tc1";
            this.Tc1.SelectedTabPage = this.Tp1;
            this.Tc1.Size = new System.Drawing.Size(951, 272);
            this.Tc1.TabIndex = 35;
            this.Tc1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.Tp1,
            this.Tp2,
            this.Tp3});
            // 
            // Tp1
            // 
            this.Tp1.Appearance.Header.Options.UseTextOptions = true;
            this.Tp1.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp1.Controls.Add(this.panel4);
            this.Tp1.Name = "Tp1";
            this.Tp1.Size = new System.Drawing.Size(945, 244);
            this.Tp1.Text = "List of Voucher";
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.splitContainer1);
            this.panel4.Controls.Add(this.panel7);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(945, 244);
            this.panel4.TabIndex = 22;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 32);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.LueCurCode2);
            this.splitContainer1.Panel1.Controls.Add(this.Grd1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.splitContainer1.Panel2.Controls.Add(this.TxtVoucherDocNo);
            this.splitContainer1.Panel2.Controls.Add(this.BtnVoucher);
            this.splitContainer1.Panel2.Controls.Add(this.label9);
            this.splitContainer1.Size = new System.Drawing.Size(945, 212);
            this.splitContainer1.SplitterDistance = 150;
            this.splitContainer1.TabIndex = 27;
            // 
            // LueCurCode2
            // 
            this.LueCurCode2.EnterMoveNextControl = true;
            this.LueCurCode2.Location = new System.Drawing.Point(362, 65);
            this.LueCurCode2.Margin = new System.Windows.Forms.Padding(5);
            this.LueCurCode2.Name = "LueCurCode2";
            this.LueCurCode2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode2.Properties.Appearance.Options.UseFont = true;
            this.LueCurCode2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCurCode2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCurCode2.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode2.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCurCode2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCurCode2.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCurCode2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCurCode2.Properties.DropDownRows = 25;
            this.LueCurCode2.Properties.NullText = "[Empty]";
            this.LueCurCode2.Properties.PopupWidth = 220;
            this.LueCurCode2.Size = new System.Drawing.Size(188, 20);
            this.LueCurCode2.TabIndex = 67;
            this.LueCurCode2.ToolTip = "F4 : Show/hide list";
            this.LueCurCode2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCurCode2.EditValueChanged += new System.EventHandler(this.LueCurCode2_EditValueChanged);
            this.LueCurCode2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueCurCode_KeyDown);
            this.LueCurCode2.Leave += new System.EventHandler(this.LueCurCode_Leave);
            // 
            // Grd1
            // 
            this.Grd1.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd1.DefaultRow.Height = 20;
            this.Grd1.DefaultRow.Sortable = false;
            this.Grd1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 21;
            this.Grd1.Location = new System.Drawing.Point(0, 0);
            this.Grd1.Name = "Grd1";
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd1.SingleClickEdit = true;
            this.Grd1.Size = new System.Drawing.Size(945, 150);
            this.Grd1.TabIndex = 33;
            this.Grd1.TreeCol = null;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd1_EllipsisButtonClick);
            this.Grd1.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd1_RequestEdit);
            this.Grd1.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd1_AfterCommitEdit);
            this.Grd1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd1_KeyDown);
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.TxtAmt2);
            this.panel7.Controls.Add(this.label8);
            this.panel7.Controls.Add(this.TxtAmt1);
            this.panel7.Controls.Add(this.label6);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel7.Location = new System.Drawing.Point(0, 0);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(945, 32);
            this.panel7.TabIndex = 27;
            // 
            // TxtAmt2
            // 
            this.TxtAmt2.EnterMoveNextControl = true;
            this.TxtAmt2.Location = new System.Drawing.Point(389, 6);
            this.TxtAmt2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAmt2.Name = "TxtAmt2";
            this.TxtAmt2.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAmt2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAmt2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAmt2.Properties.Appearance.Options.UseFont = true;
            this.TxtAmt2.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtAmt2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtAmt2.Properties.ReadOnly = true;
            this.TxtAmt2.Size = new System.Drawing.Size(173, 20);
            this.TxtAmt2.TabIndex = 32;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(275, 9);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(111, 14);
            this.label8.TabIndex = 31;
            this.label8.Text = "Balance Petty Cash";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtAmt1
            // 
            this.TxtAmt1.EnterMoveNextControl = true;
            this.TxtAmt1.Location = new System.Drawing.Point(84, 6);
            this.TxtAmt1.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAmt1.Name = "TxtAmt1";
            this.TxtAmt1.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtAmt1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAmt1.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAmt1.Properties.Appearance.Options.UseFont = true;
            this.TxtAmt1.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtAmt1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtAmt1.Properties.ReadOnly = true;
            this.TxtAmt1.Size = new System.Drawing.Size(173, 20);
            this.TxtAmt1.TabIndex = 30;
            this.TxtAmt1.EditValueChanged += new System.EventHandler(this.TxtAmt1_EditValueChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(8, 9);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(73, 14);
            this.label6.TabIndex = 29;
            this.label6.Text = "Total (Cost)";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Tp2
            // 
            this.Tp2.Appearance.Header.Options.UseTextOptions = true;
            this.Tp2.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp2.Controls.Add(this.panel5);
            this.Tp2.Name = "Tp2";
            this.Tp2.Size = new System.Drawing.Size(766, 0);
            this.Tp2.Text = "Upload File";
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.panel6);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(766, 0);
            this.panel5.TabIndex = 22;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.PbUpload);
            this.panel6.Controls.Add(this.BtnFile);
            this.panel6.Controls.Add(this.ChkFile);
            this.panel6.Controls.Add(this.TxtFile);
            this.panel6.Controls.Add(this.LblFile);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(766, 237);
            this.panel6.TabIndex = 29;
            // 
            // PbUpload
            // 
            this.PbUpload.Location = new System.Drawing.Point(47, 30);
            this.PbUpload.Name = "PbUpload";
            this.PbUpload.Size = new System.Drawing.Size(390, 17);
            this.PbUpload.TabIndex = 36;
            // 
            // BtnFile
            // 
            this.BtnFile.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnFile.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFile.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFile.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFile.Appearance.Options.UseBackColor = true;
            this.BtnFile.Appearance.Options.UseFont = true;
            this.BtnFile.Appearance.Options.UseForeColor = true;
            this.BtnFile.Appearance.Options.UseTextOptions = true;
            this.BtnFile.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnFile.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnFile.Image = ((System.Drawing.Image)(resources.GetObject("BtnFile.Image")));
            this.BtnFile.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnFile.Location = new System.Drawing.Point(470, 8);
            this.BtnFile.Name = "BtnFile";
            this.BtnFile.Size = new System.Drawing.Size(24, 21);
            this.BtnFile.TabIndex = 35;
            this.BtnFile.ToolTip = "BrowseFile";
            this.BtnFile.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnFile.ToolTipTitle = "Run System";
            this.BtnFile.Click += new System.EventHandler(this.BtnFile_Click);
            // 
            // ChkFile
            // 
            this.ChkFile.Location = new System.Drawing.Point(442, 8);
            this.ChkFile.Name = "ChkFile";
            this.ChkFile.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkFile.Properties.Appearance.Options.UseFont = true;
            this.ChkFile.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkFile.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ChkFile.Properties.Caption = " ";
            this.ChkFile.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkFile.Size = new System.Drawing.Size(20, 22);
            this.ChkFile.TabIndex = 34;
            this.ChkFile.ToolTip = "Remove filter";
            this.ChkFile.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkFile.ToolTipTitle = "Run System";
            this.ChkFile.CheckedChanged += new System.EventHandler(this.ChkFile_CheckedChanged);
            // 
            // TxtFile
            // 
            this.TxtFile.EnterMoveNextControl = true;
            this.TxtFile.Location = new System.Drawing.Point(47, 7);
            this.TxtFile.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFile.Name = "TxtFile";
            this.TxtFile.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFile.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFile.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFile.Properties.Appearance.Options.UseFont = true;
            this.TxtFile.Properties.MaxLength = 16;
            this.TxtFile.Size = new System.Drawing.Size(390, 20);
            this.TxtFile.TabIndex = 33;
            // 
            // LblFile
            // 
            this.LblFile.AutoSize = true;
            this.LblFile.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblFile.ForeColor = System.Drawing.Color.Black;
            this.LblFile.Location = new System.Drawing.Point(13, 10);
            this.LblFile.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblFile.Name = "LblFile";
            this.LblFile.Size = new System.Drawing.Size(24, 14);
            this.LblFile.TabIndex = 32;
            this.LblFile.Text = "File";
            this.LblFile.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Tp3
            // 
            this.Tp3.Appearance.Header.Options.UseTextOptions = true;
            this.Tp3.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Tp3.Controls.Add(this.Grd5);
            this.Tp3.Name = "Tp3";
            this.Tp3.Size = new System.Drawing.Size(766, 0);
            this.Tp3.Text = "Approval Information";
            // 
            // Grd5
            // 
            this.Grd5.BackColorEvenRows = System.Drawing.Color.WhiteSmoke;
            this.Grd5.BackColorOddRows = System.Drawing.Color.White;
            this.Grd5.DefaultAutoGroupRow.Height = 21;
            this.Grd5.DefaultRow.Height = 20;
            this.Grd5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd5.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd5.FrozenArea.ColCount = 3;
            this.Grd5.FrozenArea.SortFrozenRows = true;
            this.Grd5.GroupBox.BackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd5.GroupBox.HintBackColor = System.Drawing.Color.CornflowerBlue;
            this.Grd5.GroupBox.HintForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Grd5.Header.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd5.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd5.Header.UseXPStyles = false;
            this.Grd5.Location = new System.Drawing.Point(0, 0);
            this.Grd5.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.Grd5.Name = "Grd5";
            this.Grd5.ProcessTab = false;
            this.Grd5.ReadOnly = true;
            this.Grd5.RowTextStartColNear = 3;
            this.Grd5.ScrollBarSettings.Appearance = TenTec.Windows.iGridLib.iGControlPaintAppearance.StyleFlat;
            this.Grd5.SearchAsType.Mode = TenTec.Windows.iGridLib.iGSearchAsTypeMode.Seek;
            this.Grd5.SearchAsType.SearchCol = null;
            this.Grd5.SingleClickEdit = true;
            this.Grd5.Size = new System.Drawing.Size(766, 0);
            this.Grd5.TabIndex = 26;
            this.Grd5.TreeCol = null;
            this.Grd5.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Size = new System.Drawing.Size(150, 100);
            this.splitContainer2.TabIndex = 0;
            // 
            // OD
            // 
            this.OD.FileName = "openFileDialog1";
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ChkHideInfoInGrd.EditValue = true;
            this.ChkHideInfoInGrd.Location = new System.Drawing.Point(0, 514);
            this.ChkHideInfoInGrd.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ChkHideInfoInGrd.Name = "ChkHideInfoInGrd";
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.ChkHideInfoInGrd.Properties.Caption = "Hide";
            this.ChkHideInfoInGrd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkHideInfoInGrd.Size = new System.Drawing.Size(70, 22);
            this.ChkHideInfoInGrd.TabIndex = 11;
            this.ChkHideInfoInGrd.ToolTip = "Hide some informations in the list";
            this.ChkHideInfoInGrd.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkHideInfoInGrd.ToolTipTitle = "Run System";
            this.ChkHideInfoInGrd.CheckedChanged += new System.EventHandler(this.ChkHideInfoInGrd_CheckedChanged);
            // 
            // TxtDRQAmount
            // 
            this.TxtDRQAmount.EnterMoveNextControl = true;
            this.TxtDRQAmount.Location = new System.Drawing.Point(155, 132);
            this.TxtDRQAmount.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDRQAmount.Name = "TxtDRQAmount";
            this.TxtDRQAmount.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDRQAmount.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDRQAmount.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDRQAmount.Properties.Appearance.Options.UseFont = true;
            this.TxtDRQAmount.Properties.MaxLength = 30;
            this.TxtDRQAmount.Properties.ReadOnly = true;
            this.TxtDRQAmount.Size = new System.Drawing.Size(314, 20);
            this.TxtDRQAmount.TabIndex = 86;
            // 
            // TxtBalance
            // 
            this.TxtBalance.EnterMoveNextControl = true;
            this.TxtBalance.Location = new System.Drawing.Point(155, 175);
            this.TxtBalance.Margin = new System.Windows.Forms.Padding(5);
            this.TxtBalance.Name = "TxtBalance";
            this.TxtBalance.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtBalance.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBalance.Properties.Appearance.Options.UseBackColor = true;
            this.TxtBalance.Properties.Appearance.Options.UseFont = true;
            this.TxtBalance.Properties.MaxLength = 30;
            this.TxtBalance.Properties.ReadOnly = true;
            this.TxtBalance.Size = new System.Drawing.Size(314, 20);
            this.TxtBalance.TabIndex = 90;
            // 
            // FrmPettyCashDisbursement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1021, 536);
            this.Name = "FrmPettyCashDisbursement";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TcHeader)).EndInit();
            this.TcHeader.ResumeLayout(false);
            this.TpgGeneral.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePICCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDeptCode.Properties)).EndInit();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueCashType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCashTypeGrp.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCopyData.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLocalDocument.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCCCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBankAcCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCancelReason.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            this.TpgDroppingRequest.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemarkDRQ.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPCDTotalAmount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBudgetCategory.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPRJIDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMonth.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtYear.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDepartment.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDRQDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVoucherDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Tc1)).EndInit();
            this.Tc1.ResumeLayout(false);
            this.Tp1.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueCurCode2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAmt2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAmt1.Properties)).EndInit();
            this.Tp2.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkFile.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFile.Properties)).EndInit();
            this.Tp3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd5)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDRQAmount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBalance.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel3;
        private DevExpress.XtraTab.XtraTabControl Tc1;
        private DevExpress.XtraTab.XtraTabPage Tp1;
        private System.Windows.Forms.Panel panel4;
        private DevExpress.XtraTab.XtraTabPage Tp2;
        private System.Windows.Forms.Panel panel5;
        protected internal TenTec.Windows.iGridLib.iGrid Grd1;
        private System.Windows.Forms.Panel panel7;
        internal DevExpress.XtraEditors.TextEdit TxtAmt1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private DevExpress.XtraTab.XtraTabPage Tp3;
        protected TenTec.Windows.iGridLib.iGrid Grd5;
        private System.Windows.Forms.SplitContainer splitContainer2;
        internal DevExpress.XtraEditors.TextEdit TxtAmt2;
        private System.Windows.Forms.Label label8;
        internal DevExpress.XtraEditors.TextEdit TxtVoucherDocNo;
        private System.Windows.Forms.Label label9;
        public DevExpress.XtraEditors.SimpleButton BtnVoucher;
        private System.Windows.Forms.SaveFileDialog SFD;
        private System.Windows.Forms.OpenFileDialog OD;
        protected internal DevExpress.XtraEditors.CheckEdit ChkHideInfoInGrd;
        private System.Windows.Forms.Label LblFile;
        private System.Windows.Forms.ProgressBar PbUpload;
        public DevExpress.XtraEditors.SimpleButton BtnFile;
        private DevExpress.XtraEditors.CheckEdit ChkFile;
        internal DevExpress.XtraEditors.TextEdit TxtFile;
        private DevExpress.XtraEditors.LookUpEdit LueCurCode2;
        private DevExpress.XtraTab.XtraTabControl TcHeader;
        private DevExpress.XtraTab.XtraTabPage TpgGeneral;
        private System.Windows.Forms.Panel panel8;
        private DevExpress.XtraEditors.LookUpEdit LueCurCode1;
        public DevExpress.XtraEditors.LookUpEdit LuePICCode;
        public DevExpress.XtraEditors.LookUpEdit LueDeptCode;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Label label11;
        private DevExpress.XtraEditors.LookUpEdit LueCashType;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.LookUpEdit LueCashTypeGrp;
        public DevExpress.XtraEditors.SimpleButton BtnCopyData;
        internal DevExpress.XtraEditors.TextEdit TxtCopyData;
        private System.Windows.Forms.Label LblCopyData;
        internal DevExpress.XtraEditors.TextEdit TxtLocalDocument;
        private System.Windows.Forms.Label label10;
        private DevExpress.XtraEditors.LookUpEdit LueCCCode;
        private System.Windows.Forms.Label LblCCCode;
        private DevExpress.XtraEditors.LookUpEdit LueBankAcCode;
        private System.Windows.Forms.Label LblBankAcCode;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label27;
        internal DevExpress.XtraEditors.TextEdit TxtStatus;
        private DevExpress.XtraEditors.CheckEdit ChkCancelInd;
        private DevExpress.XtraEditors.MemoExEdit MeeCancelReason;
        private System.Windows.Forms.Label label17;
        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraTab.XtraTabPage TpgDroppingRequest;
        private System.Windows.Forms.Panel panel10;
        internal DevExpress.XtraEditors.TextEdit TxtDRQDocNo;
        private System.Windows.Forms.Label label12;
        public DevExpress.XtraEditors.SimpleButton BtnDRQDocNo;
        public DevExpress.XtraEditors.SimpleButton BtnDRQ;
        internal DevExpress.XtraEditors.TextEdit TxtDepartment;
        private System.Windows.Forms.Label label13;
        internal DevExpress.XtraEditors.TextEdit TxtYear;
        private System.Windows.Forms.Label label15;
        internal DevExpress.XtraEditors.TextEdit TxtMonth;
        private System.Windows.Forms.Label label16;
        internal DevExpress.XtraEditors.TextEdit TxtPRJIDocNo;
        private System.Windows.Forms.Label label18;
        internal DevExpress.XtraEditors.TextEdit TxtBudgetCategory;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        internal DevExpress.XtraEditors.TextEdit TxtPCDTotalAmount;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private DevExpress.XtraEditors.MemoExEdit MeeRemarkDRQ;
        private System.Windows.Forms.Label label23;
        internal DevExpress.XtraEditors.TextEdit TxtBalance;
        internal DevExpress.XtraEditors.TextEdit TxtDRQAmount;
    }
}