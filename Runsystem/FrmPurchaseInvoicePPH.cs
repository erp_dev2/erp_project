﻿#region Update
/*
    12/11/2018 [WED] tambah kolom remark, yang digunakan di poin d print out PPH23
    13/11/2018 [WED] BUG saat print, remark nya masih pakai list Vendor
    26/03/2019 [DITA] Print Out PPH 23: ambil Remark dari Purchase invoice bagian Note (sebelah Footer tab tax, PPH23)  
    28/03/2019 [MEY] menambahkan filter docno, date, cancelind, remark di header , dan dokumen dapat disave 
    02/04/2019 [WED] Note : jika ditambah PRI dan VRTax, jangan lupa di transaksi PRI dan VRTax ada validasi saat cancel transaksi nya, melihat ke PIPPH yang masih aktif
    18/06/2019 [TKG] warning apabila detail yg dipilih lbh dari 999.
    18/06/2019 [TKG] date di printout tidak menampilkan waktu
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmPurchaseInvoicePPH : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string mMenuCode = string.Empty, mAccessInd = string.Empty,mDocNo = string.Empty;
        private string mEmptaxwithholder = string.Empty;
        internal FrmPurchaseInvoicePPHFind FrmFind;
        #endregion

        #region Constructor

        public FrmPurchaseInvoicePPH(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Purchase Invoice PPH23";

            mAccessInd = Sm.SetFormAccessInd(mMenuCode);
            Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
            mEmptaxwithholder = Sm.GetParameter("EmpTaxWithholder");
            SetGrd();
            SetFormControl(mState.View);
            base.FrmLoad(sender, e);

        }

        #endregion

        #region Standard Method

      
        private void SetGrd()
        {
            Grd1.Cols.Count = 12;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "",

                        //1-5
                        "Document#",
                        "Date", 
                        "Vendor",
                        "Vendor's Address",
                        "Taxpayer"+Environment.NewLine+"Identification",
                       
                        //6-10
                        "Tax Invoice#",
                        "Tax Invoice"+Environment.NewLine+"Date",
                        "PPH Amount",
                        "Total Without Tax",
                        "Voucher#",

                        //11
                        "Remark"
                    },
                     new int[] 
                    {
                        20,
                        120, 100, 250, 300, 150, 
                        150, 100, 150, 150, 180,
                        200
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 8, 9 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 2 } );
            Sm.GrdColReadOnly(Grd1, new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 });
            Sm.GrdColButton(Grd1, new int[] { 0 });

        }

        private void ChkHideInfoInGrd_CheckedChanged_1(object sender, EventArgs e)
        {
            Sm.GrdColInvisible(Grd1, new int[] { }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 });
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, MeeCancelReason,MeeRemark,  ChkCancelInd, 
                    }, true);
                    break;
                case mState.Insert:
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 0, 11 });
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, MeeRemark, 
                    },false);
                    break;
                default:
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeCancelReason, ChkCancelInd }, false);
                    TxtDocNo.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, MeeCancelReason, MeeRemark, ChkCancelInd, 
            });
            ChkCancelInd.Checked = false;

            Sm.ClearGrd(Grd1, true);
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Button Method

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);

                Sm.SetDteCurrentDate(DteDocDt);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }
        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmPurchaseInvoicePPHFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }
        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }
        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            if ((IsGrdEmpty()) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;
            int jmlrow = Grd1.Rows.Count -1;
            for (int i = 0; i < jmlrow; i++)
            {
                ParPrint(i);
            }
        }
        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    CancelData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 0)
            {
                MessageBox.Show("");
                e.DoDefault = false;
                if (e.KeyChar == Char.Parse(" "))
                    Sm.FormShowDialog(new FrmPurchaseInvoicePPHDlg(this));
            }
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.GrdRemoveRow(Grd1, e, BtnSave);
        }

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            {
                Sm.FormShowDialog(new FrmPurchaseInvoicePPHDlg(this));
            }
        }

        #endregion

        #region Save

        #region Insert

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || 
                IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "PurchaseInvoicePPH", "TblPIPPHHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SavePIPPHHdr(DocNo));

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0) cml.Add(SavePIPPHDtl(DocNo, Row));
            }
            Sm.ExecCommands(cml);
            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                IsGrdEmpty2() ||
                IsGrdExceedMaxRecords();
        }

        private bool IsGrdEmpty2()
        {
            if (Grd1.Rows.Count <= 1)
            {
                Sm.StdMsg(mMsgType.Warning, "There is no Purchase Invoice PPH listed.");
                return true;
            }

            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {
            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Item data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private MySqlCommand SavePIPPHHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblPIPPHHdr(DocNo, DocDt, CancelReason, CancelInd, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @CancelReason, 'N', @Remark, @UserCode, CurrentDateTime()); ");
   
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);          
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }
        private MySqlCommand SavePIPPHDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblPIPPHDtl(DocNo, DNo, PIDocNo, TaxInvoiceNo, TaxInvoiceDt, PPHAmt, TotalWithoutTax, VoucherDocNo, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @PIDocNo, @TaxInvoiceNo, @TaxInvoiceDt, @PPHAmt, @TotalWithoutTax, @VoucherDocNo, @Remark, @UserCode, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@PIDocNo", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<String>(ref cm, "@TaxInvoiceNo", Sm.GetGrdStr(Grd1, Row, 6));
            Sm.CmParam<String>(ref cm, "@TaxInvoiceDt", Sm.GetGrdStr(Grd1, Row, 7));      
            Sm.CmParam<Decimal>(ref cm, "@PPHAmt", Sm.GetGrdDec(Grd1, Row, 8));
            Sm.CmParam<Decimal>(ref cm, "@TotalWithoutTax", Sm.GetGrdDec(Grd1, Row, 9));
            Sm.CmParam<String>(ref cm, "@VoucherDocNo", Sm.GetGrdStr(Grd1, Row, 10));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 11));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }
        #endregion

        #region Edit data

        private void CancelData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsCancelledDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(CancelPIPPHHdr());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsCancelledDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDataCancelledAlready();
        }

        private bool IsDataCancelledAlready()
        {
            return IsDataExists(
                "Select DocNo From TblPiPphHdr " +
                "Where CancelInd='Y' And DocNo=@Param; ",
                TxtDocNo.Text, "This document already cancelled."
                );
        }

   
        private bool IsDataExists(string SQL, string Param, string Warning)
        {
            var cm = new MySqlCommand() { CommandText = SQL };
            Sm.CmParam<String>(ref cm, "@Param", Param);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, Warning);
                return true;
            }
            return false;
        }
        private MySqlCommand CancelPIPPHHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblPIPPhHdr Set ");
            SQL.AppendLine("  CancelReason=@CancelReason,  CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N'; ");

    

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);

            return cm;
        }

        #endregion

        #region Show Data
        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowHdr(DocNo);
                ShowDtl(DocNo);

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }
        private void ShowHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo, DocDt, CancelReason, Remark, CancelInd From TblPIPPHHdr Where DocNo = @DocNo; ");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DocNo", 

                    //1-3
                    "DocDt", "CancelReason", "Remark", "CancelInd",
                    
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                    MeeCancelReason.EditValue = Sm.DrStr(dr, c[2]);
                    MeeRemark.EditValue = Sm.DrStr(dr, c[3]);
                    ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[4]), "Y");
                }, true
            );
        }
        private void ShowDtl(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
          
            var SQL = new StringBuilder();
            SQL.AppendLine("Select A.DocNo, A.DNo, B.DocDt , A.PIDocNo, C.VdName, C.Address, C.Tin, A.TaxInvoiceNo, A.TaxInvoiceDt, A.PPHAmt,  ");
             SQL.AppendLine("A.TotalWithoutTax, A.VoucherDocNo, A.Remark From TblPiPphDtl A ");
             SQL.AppendLine("Inner Join TblPurchaseInvoiceHdr B On A.PIDocNo=B.DocNo ");
             SQL.AppendLine("Inner Join TblVendor C On C.VdCode=B.VdCode ");
             SQL.AppendLine("Where A.DocNo = @DocNo; ");
                     

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                      //0
                    "PIDocNo", 
                    
                    //1-5
                    "DocDt", "VdName","Address", "Tin", "TaxInvoiceNo", 
                    
                    //6-10
                    "TaxInvoiceDt", "PPHAmt", "TotalWithoutTax","VoucherDocNo" , "Remark", 
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);       
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd1, 0, 1);
        }
        #endregion
        #endregion

        #region Additinal Method

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "Can not print, document is empty.");
                return true;
            }
            return false;
        }

        private void ParPrint(int row)
        {
            var l = new List<PPH23>();
            var l2 = new List<Employee>();
            string[] TableName = { "PPH23", "Employee" };
            List<IList> myLists = new List<IList>();

            #region PPH23

            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct TaxRate From (");
            SQL.AppendLine("Select Abs(C.TaxRate) As TaxRate ");
            SQL.AppendLine("From TblPurchaseInvoiceHdr A ");
            SQL.AppendLine("Inner Join TblParameter B On B.ParCode='PPH23TaxCode' And B.ParValue Is Not Null ");
            SQL.AppendLine("Inner Join tblTax C On A.TaxCode1=C.TaxCode ");
            SQL.AppendLine("Where A.TaxCode1 is Not Null And Find_In_Set(A.TaxCode1,B.ParValue) ");
            SQL.AppendLine("And A.DocNo=@Param ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select Abs(C.TaxRate) As TaxRate ");
            SQL.AppendLine("From TblPurchaseInvoiceHdr A ");
            SQL.AppendLine("Inner Join TblParameter B On B.ParCode='PPH23TaxCode' And B.ParValue Is Not Null ");
            SQL.AppendLine("Inner Join tblTax C On A.TaxCode2=C.TaxCode ");
            SQL.AppendLine("Where A.TaxCode2 is Not Null And Find_In_Set(A.TaxCode2,B.ParValue) ");
            SQL.AppendLine("And A.DocNo=@Param ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select Abs(C.TaxRate) As TaxRate ");
            SQL.AppendLine("From TblPurchaseInvoiceHdr A ");
            SQL.AppendLine("Inner Join TblParameter B On B.ParCode='PPH23TaxCode' And B.ParValue Is Not Null ");
            SQL.AppendLine("Inner Join tblTax C On A.TaxCode3=C.TaxCode ");
            SQL.AppendLine("Where A.TaxCode3 is Not Null And Find_In_Set(A.TaxCode3,B.ParValue) ");
            SQL.AppendLine("And A.DocNo=@Param ");
            SQL.AppendLine(") T Limit 1;");

            string Value = Sm.GetValue(SQL.ToString(), Sm.GetGrdStr(Grd1, row, 1));
            decimal TaxRateTemp = 0m;

            if (Value.Length > 0) TaxRateTemp = decimal.Parse(Value);

            l.Add(new PPH23()
            {
                DocNo = Sm.GetGrdStr(Grd1, row, 1),
                Date = String.Format("{0:dd/MMM/yyyy}", Sm.ConvertDateTime(Sm.GetGrdDate(Grd1, row, 2))),
                Vendor = Sm.GetGrdStr(Grd1, row, 3),
                VdAddress = Sm.GetGrdStr(Grd1, row, 4),
                TIN = Sm.GetGrdStr(Grd1, row, 5),
                TaxInvoice = Sm.GetGrdStr(Grd1, row, 6),
                TaxInvoiceDt = Sm.GetGrdStr(Grd1, row, 7),
                PPhAmt = Sm.GetGrdDec(Grd1, row, 8),
                TotalTax = Sm.GetGrdDec(Grd1, row, 9),
                Voucher = Sm.GetGrdStr(Grd1, row, 10),
                Terbilang = Sm.Terbilang(Sm.GetGrdDec(Grd1, row, 8)),
                Remark = Sm.GetGrdStr(Grd1, row, 11),
                PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime())),
                TaxRate = string.Concat(Sm.FormatNum(TaxRateTemp, 1), "%")
            });
                myLists.Add(l);
            #endregion

            #region Pemotong Pajak
            var cm2 = new MySqlCommand();
            var SQL2 = new StringBuilder();

            SQL2.AppendLine("Select A.EmpCode, A.EmpName, ");
            SQL2.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
            SQL2.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle6') As 'NPWP' ");
            SQL2.AppendLine("From TblEmployee A ");
            SQL2.AppendLine("Where A.EmpCode=@EmpCode ");

            using (var cn2 = new MySqlConnection(Gv.ConnectionString))
            {
                cn2.Open();
                cm2.Connection = cn2;
                cm2.CommandText = SQL2.ToString();
                Sm.CmParam<String>(ref cm2, "@EmpCode", mEmptaxwithholder);
                var dr2 = cm2.ExecuteReader();
                var c2 = Sm.GetOrdinal(dr2, new string[] 
                    {
                     //0-3
                     "EmpCode",
                     "EmpName",
                     "CompanyName",
                     "NPWP"
                    
                    });
                if (dr2.HasRows)
                {
                    while (dr2.Read())
                    {
                        l2.Add(new Employee()
                        {
                            EmpCode = Sm.DrStr(dr2, c2[0]),
                            EmpName = Sm.DrStr(dr2, c2[1]),
                            CompanyName = Sm.DrStr(dr2, c2[2]),
                            NPWP = Sm.DrStr(dr2, c2[3]),
                        });
                    }
                }
                dr2.Close();
            }
            myLists.Add(l2);

            #endregion

            Sm.PrintReport("PPH23", myLists, TableName, false);
        }

        #endregion
        #region Misc Control Method
        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }
        private void ChkCancelInd_CheckedChanged_1(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }
        #endregion


    

        #endregion
    }
       
    #region Report Class

    class PPH23
    {
        public string DocNo { set; get; }
        public string Date { get; set; }
        public string Vendor { get; set; }
        public string VdAddress { get; set; }
        public string TIN { get; set; }
        public string TaxInvoice { get; set; }
        public string TaxInvoiceDt { get; set; }
        public string TaxRate { get; set; }
        public decimal PPhAmt { get; set; }
        public decimal TotalTax { get; set; }
        public string Voucher { get; set; }
        public string Terbilang { get; set; }
        public string PrintBy { get; set; }
        public string Remark { get; set; }
    }

    class Employee
    {
        public string EmpCode { set; get; }
        public string EmpName { get; set; }
        public string CompanyName { get; set; }
        public string NPWP { get; set; }
    }

    #endregion
}

