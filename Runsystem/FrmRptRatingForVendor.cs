﻿# region Update
/*
 * 09/01/2023 [MYA/MNET] Reporting baru untuk mengakomodir reporting penilaian vendor
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptRatingForVendor : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty, mListofRatingforVendor = string.Empty;
        List<MasterRating> r = null;

        #endregion

        #region Constructor

        public FrmRptRatingForVendor(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        private void GetParameter()
        {
            mListofRatingforVendor = Sm.GetParameter("ListofRatingforVendor");
        }

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                r = new List<MasterRating>();
                GetRatingData(ref r);
                SetGrd();
                SetSQL();
                //SetRateValue();
                Sl.SetLueVdCode(ref LueVdCode);
                Sl.SetLueOption(ref LueRatingCode, "VendorRatingRpt");
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                //ChkExcludedCancelledItem.Checked = true;
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@Parameter", mListofRatingforVendor);

            SQL.AppendLine("SELECT T.DocNo, T.VdName, T.PODocNo, TRUNCATE(T.AverageRate, 2) as AverageRate, C.OptDesc, T.CreateBy, T.CreateDt, T.LastUpBy, T.LastUpDt, T.DocDt  ");
            SQL.AppendLine("FROM ");
            SQL.AppendLine("( ");
            SQL.AppendLine("Select A.DocNo, A.VdCode, B.VdName, A.PODocNo, (SUM(A.AverageRate)/COUNT(A.DocNo)) AS AverageRate, A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt, A.DocDt ");
            SQL.AppendLine("From TblRatingForVendorHdr A  ");
            SQL.AppendLine("Inner Join TBlVendor B On A.VdCode = B.VdCode  ");
            SQL.AppendLine("Where 1=1 And (A.DocDt Between @DocDt1 And @DocDt2)  ");
            SQL.AppendLine("GROUP BY A.VdCode ");
            SQL.AppendLine(")T ");
            SQL.AppendLine("Inner Join tbloption C ON C.OptCat = 'VendorRatingRpt' AND (T.AverageRate BETWEEN C.Property1 AND C.Property2)  ");
            if (ChkVdCode.Checked)
                SQL.AppendLine("AND T.VdCode = @VdCode ");
            if (ChkRatingCode.Checked)
                SQL.AppendLine("AND T.VdCode = @VdCode ");

            mSQL = SQL.ToString();
        }

        private string SetRateValue()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            //Sm.CmParam<String>(ref cm, "@Parameter", mFrmParent.mListofRatingforVendor);

            SQL.AppendLine("SELECT GROUP_CONCAT(A.Rate) RateValue ");
            SQL.AppendLine("FROM TblRatingForVendorDtl A ");
            SQL.AppendLine("INNER JOIN tblrating B ON A.CriteriaCode = B.RatingCode ");
            SQL.AppendLine("And Find_In_Set(B.RatingCode, '" + mListofRatingforVendor + "')");
            SQL.AppendLine("GROUP BY A.DocNo ");

            return SQL.ToString();
        }

        private void SetGrd()
        {
            //string Value = Sm.GetValue("SELECT Group_concat(Description) val FROM tblrating WHERE Find_In_Set(RatingCode,'" + mFrmParent.mListofRatingforVendor + "')");
            //string[] ColName = Value.Split(',');
            int ratingColsCount = 0;
            int defaultColsCount = 13;

            if (r.Count > 0)
            {
                r.ForEach(x =>
                {
                    x.Column = defaultColsCount + ratingColsCount;
                    ratingColsCount++;
                });
            }

            Grd1.Cols.Count = defaultColsCount + ratingColsCount;
            Grd1.FrozenArea.ColCount = 2;

            Grd1.Cols[0].Text = "No";
            Grd1.Cols[0].Width = 50;
            Grd1.Header.Cells[0, 0].TextAlign = iGContentAlignment.TopCenter;

            Grd1.Cols[1].Text = "Document#";
            Grd1.Cols[1].Width = 150;
            Grd1.Header.Cells[0, 1].TextAlign = iGContentAlignment.TopCenter;

            Grd1.Cols[2].Text = "Vendor";
            Grd1.Cols[2].Width = 150;
            Grd1.Header.Cells[0, 2].TextAlign = iGContentAlignment.TopCenter;

            Grd1.Cols[3].Text = "PO#";
            Grd1.Cols[3].Width = 150;
            Grd1.Header.Cells[0, 3].TextAlign = iGContentAlignment.TopCenter;

            Grd1.Cols[4].Text = "Average";
            Grd1.Cols[4].Width = 100;
            Grd1.Header.Cells[0, 4].TextAlign = iGContentAlignment.TopCenter;
            Grd1.Cols[4].CellStyle.TextAlign = iGContentAlignment.TopRight;

            Grd1.Cols[5].Text = "Rating";
            Grd1.Cols[5].Width = 100;
            Grd1.Header.Cells[0, 5].TextAlign = iGContentAlignment.TopCenter;

            Grd1.Cols[6].Text = "Created By";
            Grd1.Cols[6].Width = 100;
            Grd1.Header.Cells[0, 6].TextAlign = iGContentAlignment.TopCenter;

            Grd1.Cols[7].Text = "Created Date";
            Grd1.Cols[7].Width = 100;
            Grd1.Header.Cells[0, 7].TextAlign = iGContentAlignment.TopCenter;

            Grd1.Cols[8].Text = "Created Time";
            Grd1.Cols[8].Width = 100;
            Grd1.Header.Cells[0, 8].TextAlign = iGContentAlignment.TopCenter;

            Grd1.Cols[9].Text = "Last Updated By";
            Grd1.Cols[9].Width = 100;
            Grd1.Header.Cells[0, 9].TextAlign = iGContentAlignment.TopCenter;

            Grd1.Cols[10].Text = "Last Updated Date";
            Grd1.Cols[10].Width = 100;
            Grd1.Header.Cells[0, 10].TextAlign = iGContentAlignment.TopCenter;

            Grd1.Cols[11].Text = "Last Updated Time";
            Grd1.Cols[11].Width = 100;
            Grd1.Header.Cells[0, 11].TextAlign = iGContentAlignment.TopCenter;

            Grd1.Cols[12].Text = "Date";
            Grd1.Cols[12].Width = 100;
            Grd1.Header.Cells[0, 12].TextAlign = iGContentAlignment.TopCenter;

            r.ForEach(x =>
            {
                Grd1.Cols[x.Column].Text = x.CriteriaDesc;
                Grd1.Cols[x.Column].Width = 150;
                Grd1.Header.Cells[0, x.Column].TextAlign = iGContentAlignment.TopCenter;
                Grd1.Cols[x.Column].CellStyle.BackColor = Color.FromArgb(224, 224, 224);
                Grd1.Cols[x.Column].CellStyle.ReadOnly = iGBool.True;
                Grd1.Cols[x.Column].CellStyle.TextAlign = iGContentAlignment.TopRight;
            });

            //for (int Col = defaultColsCount; Col < Grd1.Cols.Count; Col++)
            //{
            //    for (int i = 0; i <= ColName.Length-1; i++)
            //    {

            //        Grd1.Cols[Col].Text = ColName[i];
            //        Grd1.Cols[Col].Width = 150;
            //        Grd1.Header.Cells[0, Col].TextAlign = iGContentAlignment.TopCenter;
            //        Grd1.Cols[Col].CellStyle.BackColor = Color.FromArgb(224, 224, 224);
            //        Grd1.Cols[Col].CellStyle.ReadOnly = iGBool.True;

            //        Col++;
            //    }
            //}

            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 });
            Sm.GrdFormatDate(Grd1, new int[] { 7, 10, 12 });
            Sm.GrdFormatTime(Grd1, new int[] { 8, 11 });
            Grd1.Cols[12].Move(2);
            Grd1.Cols[4].Move(Grd1.Cols.Count - 1);
            Grd1.Cols[5].Move(Grd1.Cols.Count - 1);
            Grd1.Cols[6].Move(Grd1.Cols.Count - 1);
            Grd1.Cols[7].Move(Grd1.Cols.Count - 1);
            Grd1.Cols[8].Move(Grd1.Cols.Count - 1);
            Grd1.Cols[9].Move(Grd1.Cols.Count - 1);
            Grd1.Cols[10].Move(Grd1.Cols.Count - 1);
            Grd1.Cols[11].Move(Grd1.Cols.Count - 1);

            Sm.GrdColInvisible(Grd1, new int[] { 1, 6, 7, 8, 9, 10, 11, 12 });

        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 8, 9 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            //string[] RateValue = GetValue.Split(',');
            //Grd1.Cols.Count = 6 + RateValue.Length;

            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                //if (ChkExcludedCancelledItem.Checked)
                //    Filter += "And A.CancelInd='N' ";

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                //Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, new string[] { "A.DocNo" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueVdCode), "T.VdCode", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueRatingCode), "C.OptCode", true);
                //Sm.FilterStr(ref Filter, ref cm, TxtPODocNo.Text, new string[] { "A.PODocNo" });

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    mSQL + Filter + " Order By T.CreateDt Desc;",
                    new string[]
                    {
                        //0
                        "DocNo", 
                            
                        //1-5
                        "VdName", "PODocNo", "AverageRate", "OptDesc", "CreateBy",

                        //6-9
                        "CreateDt", "LastUpBy", "LastUpDt", "DocDt"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 7, 6);
                        Sm.SetGrdValue("T", Grd, dr, c, Row, 8, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 10, 8);
                        Sm.SetGrdValue("T", Grd, dr, c, Row, 11, 8);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 12, 9);
                    }, true, false, false, false
                );

                var l = new List<RateValue>();
                GetRateValue(ref l, Sm.Left(Sm.GetDte(DteDocDt1), 8), Sm.Left(Sm.GetDte(DteDocDt2), 8));
                if (l.Count > 0)
                {
                    SetRateValue(ref l);
                }

                l.Clear();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method


        #endregion

        #region Misc Control Method



        #endregion

        #region Additional Methods

        private void GetRatingData(ref List<MasterRating> r)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select RatingCode, Description ");
            SQL.AppendLine("From TblRating ");
            SQL.AppendLine("Where Find_In_Set(RatingCode, @Param) ");
            SQL.AppendLine("; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@Param", mListofRatingforVendor);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "RatingCode", "Description" });

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        r.Add(new MasterRating()
                        {
                            CriteriaCode = Sm.DrStr(dr, c[0]),
                            CriteriaDesc = Sm.DrStr(dr, c[1]),
                            Column = 0
                        });
                    }
                }
                dr.Close();
            }
        }

        private void GetRateValue(ref List<RateValue> l, string DocDt1, string DocDt2)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.DocNo, A.CriteriaCode, TRUNCATE((SUM(A.Rate)/Count(B.DocNo)), 2) AS Rate ");
            SQL.AppendLine("From TblRatingForVendorDtl A ");
            SQL.AppendLine("Inner Join TblRatingForVendorHdr B On A.DocNo = B.DocNo ");
            SQL.AppendLine("    And B.DocDt Between @DocDt1 And @DocDt2 ");
            SQL.AppendLine("    And Find_In_Set(A.CriteriaCode, @ListofRatingforVendor) ");
            SQL.AppendLine("Group By B.VdCode, A.CriteriaCode; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParamDt(ref cm, "@DocDt1", DocDt1);
                Sm.CmParamDt(ref cm, "@DocDt2", DocDt2);
                Sm.CmParam<String>(ref cm, "@ListofRatingforVendor", mListofRatingforVendor);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DocNo", "CriteriaCode", "Rate" });

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new RateValue()
                        {
                            DocNo = Sm.DrStr(dr, c[0]),
                            CriteriaCode = Sm.DrStr(dr, c[1]),
                            Rate = Sm.DrDec(dr, c[2])
                        });
                    }
                }
                dr.Close();
            }
        }

        private void SetRateValue(ref List<RateValue> l)
        {
            for (int Row = 0; Row < Grd1.Rows.Count; ++Row)
            {
                foreach (var x in l.Where(w => w.DocNo == Sm.GetGrdStr(Grd1, Row, 1)))
                {
                    foreach (var y in r.Where(w => w.CriteriaCode == x.CriteriaCode))
                    {
                        Grd1.Cells[Row, y.Column].Value = x.Rate;
                    }
                }
            }
        }

        //private void ProcessRateValue(ref List<RateValue> l)
        //{
        //    foreach (var value in l)
        //    {
        //        for (int Col = 13; Col < Grd1.Cols.Count; Col++)
        //        {
        //            for (int i = 0; i <= value.RatingValue.Length; i++)
        //            {

        //            }
        //            //for (int i = 0; i <= RateValue.Length - 1; i++)
        //            //{
        //            //    Grd1.Cells[Row, Col].Value = RateValue[i];
        //            //    Sm.SetGrdValue("N", Grd, dr, c, Row, Col, int.Parse(RateValue[i]));
        //            //    Col++;
        //            //}
        //        }
        //    }

        //}

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue1(Sl.SetLueVdCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkVdCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Vendor");
        }

        private void LueRatingCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueRatingCode, new Sm.RefreshLue2(Sl.SetLueOption), "VendorRatingRpt");
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkRatingCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Rating");
        }

        #endregion

        #endregion

        #region Class

        private class MasterRating
        {
            public string CriteriaCode { get; set; }
            public string CriteriaDesc { get; set; }
            public int Column { get; set; }
        }

        private class RateValue
        {
            public string DocNo { get; set; }
            public string CriteriaCode { get; set; }
            public decimal Rate { get; set; }
        }
        #endregion
    }
}
