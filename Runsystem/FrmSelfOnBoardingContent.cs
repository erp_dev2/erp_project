﻿#region Update
    // 24/08/2020 : [IBL] New Application
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Imaging;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmSelfOnBoardingContent : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "", mContentId = string.Empty;
        //internal Frm FrmFind;
        internal bool mIsAccessUsed = false;

        public byte[] ImageByte = null;

        #endregion

        #region Constructor

        public FrmSelfOnBoardingContent(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Master Item";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                BtnPrint.Visible = false;
                SetFormControl(mState.View);

                //if this application is called from other application
                if (mContentId.Length != 0)
                {
                    ShowData(mContentId);
                    if (ImageByte != null && ImageByte.Length > 0)
                        PicBoxContent.Image = ByteToImage(ImageByte);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible =
                    BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = mIsAccessUsed;
                }
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            //if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtTitle
                    }, true);
                    RtbContent.ReadOnly = true;
                    TxtTitle.Focus();
                    break;
            }
        }

        internal void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtTitle
            });
            RtbContent.Text = string.Empty;
        }

        #endregion

        #region Show Data

        public void ShowData(string ContentId)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();

                var cm = new MySqlCommand();
                Sm.CmParam<String>(ref cm, "@ContentId", mContentId);

                Sm.ShowDataInCtrl(
                        ref cm,
                        "Select Title, Content " +
                        "From TblSelfOnBoarding2 Where Id=@ContentId;",
                        new string[] 
                    { 
                        "Title", 
                        "Content"
                    },
                        (MySqlDataReader dr, int[] c) =>
                        {
                            TxtTitle.EditValue = Sm.DrStr(dr, c[0]);
                            RtbContent.Text = Sm.DrStr(dr, c[1]);
                        }, true
                    );
                
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Additional Method

        public static Bitmap ByteToImage(byte[] blob)
        {
            MemoryStream mStream = new MemoryStream();
            byte[] pData = blob;
            mStream.Write(pData, 0, Convert.ToInt32(pData.Length));
            Bitmap bm = new Bitmap(mStream, false);
            mStream.Dispose();
            return bm;
        }

        #endregion

        #endregion
    }
}
