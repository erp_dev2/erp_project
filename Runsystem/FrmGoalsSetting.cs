﻿#region Update
/*
    19/04/2022 [TYO/HIN] New Apps Based on FrmKPIPerspective
    12/05/2022 [TYO/HIN] menambah docApproval 
    22/11/2022 [RDA/HIN] menambahkan field total pada header dan detail + validasi ketika jumlah total pada header > 100
    07/12/2022 [WED/HIN] tambah kolom dateline di tab Customer, Internal, dan Learning Perspective
    14/12/2022 [WED/HIN] tidak boleh buat Goals Setting yang sama di tahun dan employee yg sama
    15/12/2022 [WED/HIN] tambah validasi berdasarkan history posisi
    20/12/2022 [HAR/HIN] goal setting, urutan detail tidak sesuai, format tanggal masih format database
    08/02/2023 [VIN/HIN] feedback approval goalssetting, ngelihat site dan dept
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using TenTec.Windows.iGridLib;

#endregion

namespace RunSystem
{
    public partial class FrmGoalsSetting : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string mMenuCode = "", mAccessInd = "";
        internal string mDocNo = string.Empty,
            EvaluatorCode = string.Empty, TopEvaluatorCode = string.Empty;
        internal bool mIsFilterBySite = false;
        internal FrmGoalsSettingFind FrmFind;
        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmGoalsSetting(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            if (this.Text.Length == 0) this.Text = "Goals Setting";

            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                SetGrd();
                GetParameter();
                SetFormControl(mState.View);
                Sl.SetLueOption(ref LueDirectorate, "GoalsSettingDirectorate");
                LuePosition1.Visible = false;
                LuePosition2.Visible = false;
                LuePosition3.Visible = false;
                LuePosition4.Visible = false;

                Tpg.SelectedTab = TpgCustomerPerspective;
                Tpg.SelectedTab = TpgInternalPerspective;
                Tpg.SelectedTab = TpgLearningPerspective;
                Tpg.SelectedTab = TpgFinancialPerpective;

                base.FrmLoad(sender, e);

                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grid1

            Grd1.Cols.Count = 7;
            Grd1.FrozenArea.ColCount = 1;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                Grd1,
                new string[]
                {
                    //0
                    "",

                    //1-5
                    "Strategic Objective", //Work Goals
                    "Weight", //Quality
                    "Measurement and Target", //Measures and Target
                    "Type", //Evaluation
                    "Evaluation Code",

                    //6
                    "Deadline"
                },
                new int[]
                {
                    //0
                    20,
 
                    //1-5
                    150, 150, 150, 150, 60,

                    //6
                    100
                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 2 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 0, 5 });
            Sm.GrdFormatDate(Grd1, new int[] { 6 });

            #endregion

            #region Grid2

            Grd2.Cols.Count = 7;
            Grd2.FrozenArea.ColCount = 1;
            Grd2.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                Grd2,
                new string[]
                {
                    //0
                    "",

                    //1-5
                    "Strategic Objective", //Work Goals
                    "Weight", //Quality
                    "Measurement and Target", //Measures and Target
                    "Type", //Evaluation
                    "Evaluation Code",

                    //6
                    "Deadline"
                },
                new int[]
                {
                    //0
                    20,
 
                    //1-5
                    150, 150, 150, 150, 60,

                    //6
                    100
                }
            );
            Sm.GrdFormatDec(Grd2, new int[] { 2 }, 0);
            Sm.GrdColInvisible(Grd2, new int[] { 0, 5 });
            Sm.GrdFormatDate(Grd2, new int[] { 6 });

            #endregion

            #region Grid3

            Grd3.Cols.Count = 7;
            Grd3.FrozenArea.ColCount = 1;
            Grd3.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                Grd3,
                new string[]
                {
                    //0
                    "",

                    //1-5
                    "Strategic Objective", //Work Goals
                    "Weight", //Quality
                    "Measurement and Target", //Measures and Target
                    "Type", //Evaluation
                    "Evaluation Code",

                    //6
                    "Deadline"
                },
                new int[]
                {
                    //0
                    20,
 
                    //1-5
                    150, 150, 150, 150, 60,

                    //6
                    100
                }
            );
            Sm.GrdFormatDec(Grd3, new int[] { 2 }, 0);
            Sm.GrdColInvisible(Grd3, new int[] { 0, 5 });
            Sm.GrdFormatDate(Grd3, new int[] { 6 });

            #endregion

            #region Grid4

            Grd4.Cols.Count = 7;
            Grd4.FrozenArea.ColCount = 1;
            Grd4.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                Grd4,
                new string[]
                {
                    //0
                    "",

                    //1-5
                    "Strategic Objective", //Work Goals
                    "Weight", //Quality
                    "Measurement and Target", //Measures and Target
                    "Type", //Evaluation
                    "Evaluation Code",

                    //6
                    "Deadline"
                },
                new int[]
                {
                    //0
                    20,
 
                    //1-5
                    150, 150, 150, 150, 60,

                    //6
                    100
                }
            );
            Sm.GrdFormatDec(Grd4, new int[] { 2 }, 0);
            Sm.GrdColInvisible(Grd4, new int[] { 0, 5 });
            Sm.GrdFormatDate(Grd4, new int[] { 6 });

            #endregion
        }

        private void ChkHideInfoInGrd_CheckedChanged(object sender, EventArgs e)
        {


        }
        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                        TxtDocNo, DteDocDt, TxtGoalSetting, TxtPICCode, TxtPICName, MeeRemark,
                        LuePosition1, LuePosition2, LuePosition3, LuePosition4, TxtEvaluator, TxtTopEvaluator, LueDirectorate, TxtStatus
                    }, true);
                    ChkActiveInd.Properties.ReadOnly = true;
                    Grd1.ReadOnly = true;
                    Grd2.ReadOnly = true;
                    Grd3.ReadOnly = true;
                    Grd4.ReadOnly = true;

                    BtnPIC.Enabled = false;
                    BtnEvaluator.Enabled = false;
                    BtnTopEvaluator.Enabled = false;
                    DteDocDt.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                      DteDocDt, TxtGoalSetting, MeeRemark,
                      LuePosition1, LuePosition2, LuePosition3, LuePosition4, LueDirectorate
                    }, false);
                    ChkActiveInd.Checked = true;
                    Grd1.ReadOnly = false;
                    Grd2.ReadOnly = false;
                    Grd3.ReadOnly = false;
                    Grd4.ReadOnly = false;

                    Sm.FormatNumTxt(TxtTotalFinancial, 0);
                    Sm.FormatNumTxt(TxtTotalCustomer, 0);
                    Sm.FormatNumTxt(TxtTotalInternalProcess, 0);
                    Sm.FormatNumTxt(TxtTotalLearning, 0);

                    BtnPIC.Enabled = true;
                    BtnEvaluator.Enabled = true;
                    BtnTopEvaluator.Enabled = true;
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit>
                    {
                        TxtDocNo, DteDocDt, TxtGoalSetting, TxtPICCode, TxtPICName, MeeRemark,
                        LuePosition1, LuePosition2, LuePosition3, LuePosition4, TxtEvaluator, TxtTopEvaluator, LueDirectorate, TxtStatus
                    }, true);
                    ChkActiveInd.Properties.ReadOnly = false;
                    Grd1.ReadOnly = true;
                    Grd2.ReadOnly = true;
                    Grd3.ReadOnly = true;
                    Grd4.ReadOnly = true;

                    BtnPIC.Enabled = false;
                    BtnEvaluator.Enabled = false;
                    BtnTopEvaluator.Enabled = false;
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit>
            {
                TxtDocNo, DteDocDt, TxtGoalSetting, TxtPICCode, TxtPICName,  MeeRemark,
                LuePosition1, LuePosition2, LuePosition3, LuePosition4, TxtTopEvaluator, TxtEvaluator, LueDirectorate, TxtStatus,
                DteDeadLine, DteDeadLine0, DteDeadLine2, DteDeadLine3
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit>
            {
                TxtTotalGoalsSetting, TxtTotalFinancial, TxtTotalCustomer, TxtTotalInternalProcess, TxtTotalLearning
            }, 0);
            ClearGrd();
            ChkActiveInd.Checked = false;
        }

        private void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 2 });
            Sm.ClearGrd(Grd2, true);
            Sm.SetGrdNumValueZero(ref Grd2, 0, new int[] { 2 });
            Sm.ClearGrd(Grd3, true);
            Sm.SetGrdNumValueZero(ref Grd3, 0, new int[] { 2 });
            Sm.ClearGrd(Grd4, true);
            Sm.SetGrdNumValueZero(ref Grd4, 0, new int[] { 2 });
        }


        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmGoalsSettingFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);

                Sm.SetDteCurrentDate(DteDocDt);

            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    CancelData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            if (!Sm.IsDataExist("Select 1 From TblGoalsSettingHdr Where DocNo = @Param And ActInd = 'Y' And Status = 'A'; ", TxtDocNo.Text))
            {
                Sm.StdMsg(mMsgType.Info, "Only active and approved document that could be printed.");
                return;
            }
            if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;

            ParPrint();
        }

        #endregion

        #region Grid Method      

        private void Grd1_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 1 }, e.ColIndex))
            {
                Sm.GrdRequestEdit(Grd1, e.RowIndex);
                Sm.SetGrdNumValueZero(ref Grd1, e.RowIndex + 1, new int[] { 2 });
            }

            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 4, 6 }, e.ColIndex))
            {
                if (e.ColIndex == 4)
                {
                    LueRequestEdit(Grd1, LuePosition1, ref fCell, ref fAccept, e);
                    Sl.SetLueOption(ref LuePosition1, "GoalsSettingEvaluation");
                }

                if (e.ColIndex == 6)
                {
                    Sm.DteRequestEdit(Grd1, DteDeadLine0, ref fCell, ref fAccept, e);
                }

                Sm.GrdRequestEdit(Grd1, e.RowIndex);
            }
        }

        private void Grd2_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 1 }, e.ColIndex))
            {
                Sm.GrdRequestEdit(Grd2, e.RowIndex);
                Sm.SetGrdNumValueZero(ref Grd2, e.RowIndex + 1, new int[] { 2 });
            }

            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 4, 6 }, e.ColIndex))
            {
                if (e.ColIndex == 4)
                {
                    LueRequestEdit(Grd2, LuePosition2, ref fCell, ref fAccept, e);
                    Sl.SetLueOption(ref LuePosition2, "GoalsSettingEvaluation");
                }

                if (e.ColIndex == 6)
                {
                    Sm.DteRequestEdit(Grd2, DteDeadLine, ref fCell, ref fAccept, e);
                }

                Sm.GrdRequestEdit(Grd2, e.RowIndex);
            }
        }

        private void Grd3_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 1 }, e.ColIndex))
            {
                Sm.GrdRequestEdit(Grd3, e.RowIndex);
                Sm.SetGrdNumValueZero(ref Grd3, e.RowIndex + 1, new int[] { 2 });
            }

            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 4, 6 }, e.ColIndex))
            {
                if (e.ColIndex == 4)
                {
                    LueRequestEdit(Grd3, LuePosition3, ref fCell, ref fAccept, e);

                    Sl.SetLueOption(ref LuePosition3, "GoalsSettingEvaluation");
                }

                if (e.ColIndex == 6)
                {
                    Sm.DteRequestEdit(Grd3, DteDeadLine2, ref fCell, ref fAccept, e);
                }

                Sm.GrdRequestEdit(Grd3, e.RowIndex);
            }
        }

        private void Grd4_RequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 1 }, e.ColIndex))
            {
                Sm.GrdRequestEdit(Grd4, e.RowIndex);
                Sm.SetGrdNumValueZero(ref Grd4, e.RowIndex + 1, new int[] { 2 });
            }
            if (BtnSave.Enabled && Sm.IsGrdColSelected(new int[] { 4, 6 }, e.ColIndex))
            {
                if (e.ColIndex == 4)
                {
                    LueRequestEdit(Grd4, LuePosition4, ref fCell, ref fAccept, e);

                    Sl.SetLueOption(ref LuePosition4, "GoalsSettingEvaluation");
                }

                if (e.ColIndex == 6)
                {
                    Sm.DteRequestEdit(Grd4, DteDeadLine3, ref fCell, ref fAccept, e);
                }

                Sm.GrdRequestEdit(Grd4, e.RowIndex);
            }
        }

        private void Grd1_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled) Sm.GrdRemoveRow(Grd1, e, BtnSave);
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
            ComputeTotalDtl(Grd1, TxtTotalFinancial, 2);
        }

        private void Grd2_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled) Sm.GrdRemoveRow(Grd2, e, BtnSave);
            Sm.GrdKeyDown(Grd2, e, BtnFind, BtnSave);
            ComputeTotalDtl(Grd2, TxtTotalCustomer, 2);
        }

        private void Grd3_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled) Sm.GrdRemoveRow(Grd3, e, BtnSave);
            Sm.GrdKeyDown(Grd3, e, BtnFind, BtnSave);
            ComputeTotalDtl(Grd3, TxtTotalInternalProcess, 2);
        }

        private void Grd4_KeyDown(object sender, KeyEventArgs e)
        {
            if (BtnSave.Enabled) Sm.GrdRemoveRow(Grd4, e, BtnSave);
            Sm.GrdKeyDown(Grd4, e, BtnFind, BtnSave);
            ComputeTotalDtl(Grd4, TxtTotalLearning, 2);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", "") == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = string.Empty;

            if (TxtDocNo.Text.Length == 0)
            {
                DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "GoalsSetting", "TblGoalsSettingHdr");
            }
            else
            {
                DocNo = TxtDocNo.Text;
            }

            var cml = new List<MySqlCommand>();

            cml.Add(SaveGoalsSettingHdr(DocNo));

            Sm.ExecCommands(cml);
            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsTxtEmpty(TxtPICCode, "PIC Code", false) ||
                Sm.IsTxtEmpty(TxtGoalSetting, "Goal Setting", false) ||
                IsDocNoAlreadyProcess() ||
                Sm.IsTxtEmpty(TxtEvaluator, "Appraiser 1", false) ||
                Sm.IsLueEmpty(LueDirectorate, "Directorate") ||
                IsGoalsSettingExists() ||
                IsGrdInvalid() ||
                IsTotalNotValid();
        }

        private bool IsGoalsSettingExists()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 ");
            SQL.AppendLine("From ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select A.PICCode, B.PosCode ");
            SQL.AppendLine("    From TblGoalsSettingHdr A ");
            SQL.AppendLine("    Inner Join TblEmployeePPS B on A.PICCode = B.EmpCode ");
            SQL.AppendLine("        And((A.DocDt Between B.StartDt And B.EndDt And B.EndDt Is Not Null) Or(B.EndDt Is Null And B.StartDt <= A.DocDt)) ");
            SQL.AppendLine("    Where A.Actind = 'Y' And A.Status In('O', 'A') ");
            SQL.AppendLine("    And A.PICCode = @Param1 ");
            SQL.AppendLine("    And Left(A.DocDt, 4) = Left(@Param2, 4) ");
            SQL.AppendLine(") T1 ");
            SQL.AppendLine("Inner Join TblEmployee T2 On T1.PICCode = T2.EmpCode ");
            SQL.AppendLine("Where T1.PosCode = T2.PosCode ");
            SQL.AppendLine("Limit 1; ");

            if (Sm.IsDataExist(SQL.ToString(), TxtPICCode.Text, Sm.Left(Sm.GetDte(DteDocDt), 8), string.Empty))
            {
                Sm.StdMsg(mMsgType.Warning, "Only allowed one active goals setting document on the same employee, his/her position (on current period year), and period year.");
                return true;
            }

            return false;
        }

        private bool IsGrdInvalid()
        {
            if (Grd1.Rows.Count > 1)
            {
                for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
                {
                    if (Sm.GetGrdStr(Grd1, i, 6).Length == 0)
                    {
                        Tpg.SelectedTab = TpgFinancialPerpective;
                        Sm.StdMsg(mMsgType.Warning, "Deadline should not be empty.");
                        Sm.FocusGrd(Grd1, i, 6);
                        return true;
                    }
                }
            }

            if (Grd2.Rows.Count > 1)
            {
                for (int i = 0; i < Grd2.Rows.Count - 1; ++i)
                {
                    if (Sm.GetGrdStr(Grd2, i, 6).Length == 0)
                    {
                        Tpg.SelectedTab = TpgCustomerPerspective;
                        Sm.StdMsg(mMsgType.Warning, "Deadline should not be empty.");
                        Sm.FocusGrd(Grd2, i, 6);
                        return true;
                    }
                }
            }

            if (Grd3.Rows.Count > 1)
            {
                for (int i = 0; i < Grd3.Rows.Count - 1; ++i)
                {
                    if (Sm.GetGrdStr(Grd3, i, 6).Length == 0)
                    {
                        Tpg.SelectedTab = TpgInternalPerspective;
                        Sm.StdMsg(mMsgType.Warning, "Deadline should not be empty.");
                        Sm.FocusGrd(Grd3, i, 6);
                        return true;
                    }
                }
            }

            if (Grd4.Rows.Count > 1)
            {
                for (int i = 0; i < Grd4.Rows.Count - 1; ++i)
                {
                    if (Sm.GetGrdStr(Grd4, i, 6).Length == 0)
                    {
                        Tpg.SelectedTab = TpgLearningPerspective;
                        Sm.StdMsg(mMsgType.Warning, "Deadline should not be empty.");
                        Sm.FocusGrd(Grd4, i, 6);
                        return true;
                    }
                }
            }

            return false;
        }

        private bool IsTotalNotValid()
        {
            if (decimal.Parse(TxtTotalGoalsSetting.Text) != 100)
            {
                Sm.StdMsg(mMsgType.Warning, "Quality must be equal" + Environment.NewLine + "100 to all Perspective");
                return true;
            }
            return false;
        }

        private bool IsDocNoAlreadyProcess()
        {
            var ActInd = ChkActiveInd.Checked ? "Y" : "N";

            var cm = new MySqlCommand()
            {
                CommandText = "Select GoalsDocNo From TblGoalsProcessHdr Where GoalsDocNo=@DocNo And CancelInd='N'"
            };

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This document already Process.");
                return true;
            }
            return false;
        }


        private MySqlCommand SaveGoalsSettingHdr(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            bool IsFirst = true;
            int x = 0;

            SQL.AppendLine("Insert Into TblGoalsSettingHdr(DocNo, DocDt, ActInd, GoalsName, PICCode, DirectorateCode, EvaluatorCode, TopEvaluatorCode, Remark, Status, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @ActInd, @GoalsName, @PICCode, @DirectorateCode, @EvaluatorCode, @TopEvaluatorCode, @Remark, @Status, @UserCode, CurrentDateTime()) ");

            //region bisa edit hdr dan detail
            SQL.AppendLine("On Duplicate Key ");
            SQL.AppendLine("    Update DocDt=@DocDt, ActInd=@ActInd, GoalsName=@GoalsName, PICCode=@PICCode, DirectorateCode=@DirectorateCode, ");
            SQL.AppendLine("    EvaluatorCode=@EvaluatorCode, TopEvaluatorCode=@TopEvaluatorCode, Remark=@Remark, LastUpBy=@UserCode, LastUpDt=CurrentDateTime(); ");

            //Document Approval
            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblDocApprovalSetting T ");
            SQL.AppendLine("Inner Join TblGoalsSettingHdr T1 On T1.DocNo = @DocNo ");
            SQL.AppendLine("Inner Join TblEmployee T2 On T1.PICCode = T2.EmpCode And T.DeptCode = T2.DeptCode And T.SiteCode = T2.SiteCode ");
            SQL.AppendLine("Where T.DocType='GoalsSetting' ");
            SQL.AppendLine("And (T.StartAmt=0.00 Or T.StartAmt<=@Amt); ");

            SQL.AppendLine("Update TblGoalsSettingHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select 1 From TblDocApproval ");
            SQL.AppendLine("    Where DocType='GoalsSetting' ");
            SQL.AppendLine("    And DocNo=@DocNo ");
            SQL.AppendLine("    ); ");

            #region Detail

            #region Grid 1

            if (Grd1.Rows.Count > 1)
            {
                IsFirst = true;

                SQL.AppendLine("Insert Into TblGoalsSettingDtl(DocNo, DNo, WorkGoals, Quality, Measure, Evaluation, Deadline, CreateBy, CreateDt) ");
                SQL.AppendLine("Values ");

                for (int i = 0; i < Grd1.Rows.Count - 1; i++)
                {
                    if (Sm.GetGrdStr(Grd1, i, 6).Length > 0)
                    {
                        if (IsFirst) IsFirst = false;
                        else SQL.AppendLine(", ");

                        SQL.AppendLine("(@DocNo, @DNo__" + x.ToString() + ", @WorkGoals__" + x.ToString() + ", @Quality__" + x.ToString() + ", @Measure__" + x.ToString() + ", @Evaluation__" + x.ToString() + ", @Deadline__" + x.ToString() + ", @UserCode, CurrentDateTime()) ");

                        Sm.CmParam<String>(ref cm, "@DNo__" + x.ToString(), Sm.Right("000" + (i + 1).ToString(), 3));
                        Sm.CmParam<String>(ref cm, "@WorkGoals__" + x.ToString(), Sm.GetGrdStr(Grd1, i, 1));
                        Sm.CmParam<Decimal>(ref cm, "@Quality__" + x.ToString(), Sm.GetGrdDec(Grd1, i, 2));
                        Sm.CmParam<String>(ref cm, "@Measure__" + x.ToString(), Sm.GetGrdStr(Grd1, i, 3));
                        Sm.CmParam<String>(ref cm, "@Evaluation__" + x.ToString(), Sm.GetGrdStr(Grd1, i, 5));
                        Sm.CmParamDt(ref cm, "@Deadline__" + x.ToString(), Sm.GetGrdDate(Grd1, i, 6));

                        x++;
                    }
                }

                SQL.AppendLine("; ");
            }

            #endregion

            #region Grid 2

            if (Grd2.Rows.Count > 1)
            {
                IsFirst = true;

                SQL.AppendLine("Insert Into TblGoalsSettingDtl2(DocNo, DNo, WorkGoals, Quality, Measure, Evaluation, Deadline, CreateBy, CreateDt) ");
                SQL.AppendLine("Values ");

                for (int i = 0; i < Grd2.Rows.Count - 1; i++)
                {
                    if (Sm.GetGrdStr(Grd2, i, 6).Length > 0)
                    {
                        if (IsFirst) IsFirst = false;
                        else SQL.AppendLine(", ");

                        SQL.AppendLine("(@DocNo, @DNo__" + x.ToString() + ", @WorkGoals__" + x.ToString() + ", @Quality__" + x.ToString() + ", @Measure__" + x.ToString() + ", @Evaluation__" + x.ToString() + ", @Deadline__" + x.ToString() + ", @UserCode, CurrentDateTime()) ");

                        Sm.CmParam<String>(ref cm, "@DNo__" + x.ToString(), Sm.Right("000" + (i + 1).ToString(), 3));
                        Sm.CmParam<String>(ref cm, "@WorkGoals__" + x.ToString(), Sm.GetGrdStr(Grd2, i, 1));
                        Sm.CmParam<Decimal>(ref cm, "@Quality__" + x.ToString(), Sm.GetGrdDec(Grd2, i, 2));
                        Sm.CmParam<String>(ref cm, "@Measure__" + x.ToString(), Sm.GetGrdStr(Grd2, i, 3));
                        Sm.CmParam<String>(ref cm, "@Evaluation__" + x.ToString(), Sm.GetGrdStr(Grd2, i, 5));
                        Sm.CmParamDt(ref cm, "@Deadline__" + x.ToString(), Sm.GetGrdDate(Grd2, i, 6));

                        x++;
                    }
                }

                SQL.AppendLine("; ");
            }

            #endregion

            #region Grid 3

            if (Grd3.Rows.Count > 1)
            {
                IsFirst = true;

                SQL.AppendLine("Insert Into TblGoalsSettingDtl3(DocNo, DNo, WorkGoals, Quality, Measure, Evaluation, Deadline, CreateBy, CreateDt) ");
                SQL.AppendLine("Values ");

                for (int i = 0; i < Grd3.Rows.Count - 1; i++)
                {
                    if (Sm.GetGrdStr(Grd3, i, 6).Length > 0)
                    {
                        if (IsFirst) IsFirst = false;
                        else SQL.AppendLine(", ");

                        SQL.AppendLine("(@DocNo, @DNo__" + x.ToString() + ", @WorkGoals__" + x.ToString() + ", @Quality__" + x.ToString() + ", @Measure__" + x.ToString() + ", @Evaluation__" + x.ToString() + ", @Deadline__" + x.ToString() + ",  @UserCode, CurrentDateTime()) ");

                        Sm.CmParam<String>(ref cm, "@DNo__" + x.ToString(), Sm.Right("000" + (i + 1).ToString(), 3));
                        Sm.CmParam<String>(ref cm, "@WorkGoals__" + x.ToString(), Sm.GetGrdStr(Grd3, i, 1));
                        Sm.CmParam<Decimal>(ref cm, "@Quality__" + x.ToString(), Sm.GetGrdDec(Grd3, i, 2));
                        Sm.CmParam<String>(ref cm, "@Measure__" + x.ToString(), Sm.GetGrdStr(Grd3, i, 3));
                        Sm.CmParam<String>(ref cm, "@Evaluation__" + x.ToString(), Sm.GetGrdStr(Grd3, i, 5));
                        Sm.CmParamDt(ref cm, "@Deadline__" + x.ToString(), Sm.GetGrdDate(Grd3, i, 6));

                        x++;
                    }
                }

                SQL.AppendLine("; ");
            }

            #endregion

            #region Grid 4

            if (Grd4.Rows.Count > 1)
            {
                IsFirst = true;

                SQL.AppendLine("Insert Into TblGoalsSettingDtl4(DocNo, DNo, WorkGoals, Quality, Measure, Evaluation, Deadline, CreateBy, CreateDt) ");
                SQL.AppendLine("Values ");

                for (int i = 0; i < Grd4.Rows.Count - 1; i++)
                {
                    if (Sm.GetGrdStr(Grd4, i, 6).Length > 0)
                    {
                        if (IsFirst) IsFirst = false;
                        else SQL.AppendLine(", ");

                        SQL.AppendLine("(@DocNo, @DNo__" + x.ToString() + ", @WorkGoals__" + x.ToString() + ", @Quality__" + x.ToString() + ", @Measure__" + x.ToString() + ", @Evaluation__" + x.ToString() + ", @Deadline__" + x.ToString() + ",  @UserCode, CurrentDateTime()) ");

                        Sm.CmParam<String>(ref cm, "@DNo__" + x.ToString(), Sm.Right("000" + (i + 1).ToString(), 3));
                        Sm.CmParam<String>(ref cm, "@WorkGoals__" + x.ToString(), Sm.GetGrdStr(Grd4, i, 1));
                        Sm.CmParam<Decimal>(ref cm, "@Quality__" + x.ToString(), Sm.GetGrdDec(Grd4, i, 2));
                        Sm.CmParam<String>(ref cm, "@Measure__" + x.ToString(), Sm.GetGrdStr(Grd4, i, 3));
                        Sm.CmParam<String>(ref cm, "@Evaluation__" + x.ToString(), Sm.GetGrdStr(Grd4, i, 5));
                        Sm.CmParamDt(ref cm, "@Deadline__" + x.ToString(), Sm.GetGrdDate(Grd4, i, 6));

                        x++;
                    }
                }

                SQL.AppendLine("; ");
            }

            #endregion

            #endregion

            cm.CommandText = SQL.ToString();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@ActInd", ChkActiveInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@GoalsName", TxtGoalSetting.Text);
            Sm.CmParam<String>(ref cm, "@PICCode", TxtPICCode.Text);
            Sm.CmParam<String>(ref cm, "@DirectorateCode", Sm.GetLue(LueDirectorate));
            Sm.CmParam<String>(ref cm, "@EvaluatorCode", EvaluatorCode);
            Sm.CmParam<String>(ref cm, "@TopEvaluatorCode", TopEvaluatorCode);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@Status", "O");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveGoalSettingDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblGoalsSettingDtl(DocNo, DNo, WorkGoals, Quality, Measure, Evaluation, Deadline, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @WorkGoals, @Quality, @Measure, @Evaluation, @Deadline, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("000" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@WorkGoals", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<Decimal>(ref cm, "@Quality", Sm.GetGrdDec(Grd1, Row, 2));
            Sm.CmParam<String>(ref cm, "@Measure", Sm.GetGrdStr(Grd1, Row, 3));
            Sm.CmParam<String>(ref cm, "@Evaluation", Sm.GetGrdStr(Grd1, Row, 5));
            Sm.CmParamDt(ref cm, "@Deadline", Sm.GetGrdDate(Grd1, Row, 6));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            var test = Sm.GetGrdStr(Grd1, Row, 5);
            return cm;
        }

        private MySqlCommand SaveGoalSettingDtl2(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblGoalsSettingDtl2(DocNo, DNo, WorkGoals, Quality, Measure, Evaluation, Deadline, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @WorkGoals, @Quality, @Measure, @Evaluation1, @Deadline, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("000" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@WorkGoals", Sm.GetGrdStr(Grd2, Row, 1));
            Sm.CmParam<Decimal>(ref cm, "@Quality", Sm.GetGrdDec(Grd2, Row, 2));
            Sm.CmParam<String>(ref cm, "@Measure", Sm.GetGrdStr(Grd2, Row, 3));
            Sm.CmParam<String>(ref cm, "@Evaluation1", Sm.GetGrdStr(Grd2, Row, 5));
            Sm.CmParamDt(ref cm, "@Deadline", Sm.GetGrdDate(Grd2, Row, 6));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            var test = Sm.GetGrdStr(Grd2, Row, 5);
            return cm;
        }
        private MySqlCommand SaveGoalSettingDtl3(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblGoalsSettingDtl3(DocNo, DNo, WorkGoals, Quality, Measure, Evaluation, Deadline, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @WorkGoals, @Quality, @Measure, @Evaluation, @Deadline, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("000" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@WorkGoals", Sm.GetGrdStr(Grd3, Row, 1));
            Sm.CmParam<Decimal>(ref cm, "@Quality", Sm.GetGrdDec(Grd3, Row, 2));
            Sm.CmParam<String>(ref cm, "@Measure", Sm.GetGrdStr(Grd3, Row, 3));
            Sm.CmParam<String>(ref cm, "@Evaluation", Sm.GetGrdStr(Grd3, Row, 5));
            Sm.CmParamDt(ref cm, "@Deadline", Sm.GetGrdDate(Grd3, Row, 6));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }
        private MySqlCommand SaveGoalSettingDtl4(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblGoalsSettingDtl4(DocNo, DNo, WorkGoals, Quality, Measure, Evaluation, Deadline, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DNo, @WorkGoals, @Quality, @Measure, @Evaluation, @Deadline, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("000" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@WorkGoals", Sm.GetGrdStr(Grd4, Row, 1));
            Sm.CmParam<Decimal>(ref cm, "@Quality", Sm.GetGrdDec(Grd4, Row, 2));
            Sm.CmParam<String>(ref cm, "@Measure", Sm.GetGrdStr(Grd4, Row, 3));
            Sm.CmParam<String>(ref cm, "@Evaluation", Sm.GetGrdStr(Grd4, Row, 5));
            Sm.CmParamDt(ref cm, "@Deadline", Sm.GetGrdDate(Grd4, Row, 6));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }
        #endregion

        #region Cancel Data

        private void CancelData()
        {
            if (IsActIndDataNotValid() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;
            Cursor.Current = Cursors.WaitCursor;
            var cml = new List<MySqlCommand>();

            cml.Add(EditGoalsSettingActive(TxtDocNo.Text));
            if (ChkActiveInd.Checked)
            {
                cml.Add(DeleteGoalsSettingDtl(TxtDocNo.Text));

                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0)
                    {
                        cml.Add(SaveGoalSettingDtl(TxtDocNo.Text, Row));
                        cml.Add(SaveGoalSettingDtl2(TxtDocNo.Text, Row));
                        cml.Add(SaveGoalSettingDtl3(TxtDocNo.Text, Row));
                        cml.Add(SaveGoalSettingDtl4(TxtDocNo.Text, Row));
                    }
                }
            }

            Sm.ExecCommands(cml);
            ShowData(TxtDocNo.Text);
            //if (IsActIndDataNotValid() || Sm.StdMsgYN("Save", "") == DialogResult.No) return;
            //Cursor.Current = Cursors.WaitCursor;
            //EditKPIActive(TxtDocNo.Text);
            //ShowData(TxtDocNo.Text);
        }

        private bool IsActIndDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document Number", false) ||
                IsActIndEditedAlready() ||
                IsDocNoAlreadyProcess()
                ;
        }

        private bool IsActIndEditedAlready()
        {
            var ActInd = ChkActiveInd.Checked ? "Y" : "N";

            var cm = new MySqlCommand()
            {
                CommandText = "Select DocNo From TblGoalsSettingHdr Where DocNo=@DocNo And ActInd='N' "
            };

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@ActInd", ActInd);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This document already non-active.");
                return true;
            }
            return false;
        }

        private MySqlCommand EditGoalsSettingActive(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblGoalsSettingHdr Set ActInd=@ActInd, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@ActInd", ChkActiveInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand DeleteGoalsSettingDtl(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Delete From TblGoalsSettingDtl Where DocNo=@DocNo; ");
            SQL.AppendLine("Delete From TblGoalsSettingDtl2 Where DocNo=@DocNo; ");
            SQL.AppendLine("Delete From TblGoalsSettingDtl3 Where DocNo=@DocNo; ");
            SQL.AppendLine("Delete From TblGoalsSettingDtl4 Where DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            return cm;
        }


        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowGoalSettingHdr(DocNo);
                ShowGoalSettingDtl(DocNo);
                ShowGoalSettingDtl2(DocNo);
                ShowGoalSettingDtl3(DocNo);
                ShowGoalSettingDtl4(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowGoalSettingHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            SQL.AppendLine("SELECT A.DocNo, A.DocDt, A.ActInd, A.GoalsName, A.PICCode, B.EmpName AS PICName,   ");
            SQL.AppendLine("E.OptCode AS DirectorateName, A.EvaluatorCode, C.EmpName AS EvaluatorName, A.TopEvaluatorCode,  ");
            SQL.AppendLine("D.EmpName As TopEvaluatorName, A.Remark,   ");
            SQL.AppendLine("Case  ");
            SQL.AppendLine("	when A.`Status` = 'O' then 'Outstanding' ");
            SQL.AppendLine("	when A.`Status` = 'A' then 'Approved'  ");
            SQL.AppendLine("	when A.`Status` = 'C' then 'Canceled'  ");
            SQL.AppendLine("END AS `Status`  ");
            SQL.AppendLine("FROM TblGoalsSettingHdr A  ");
            SQL.AppendLine("LEFT JOIN tblemployee B ON B.EmpCode = A.PICCode  ");
            SQL.AppendLine("LEFT JOIN tblemployee C ON C.EmpCode = A.EvaluatorCode ");
            SQL.AppendLine("LEFT JOIN tblemployee D ON D.EmpCode = A.TopEvaluatorCode ");
            SQL.AppendLine("INNER JOIN tbloption E ON E.OptCode = A.DirectorateCode AND E.OptCat = 'GoalsSettingDirectorate'  ");
            SQL.AppendLine("WHERE A.DocNo = @DocNo  ");

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(),
                new string[]
                {
                    //0
                    "DocNo",

                    //1-5
                    "DocDt", "ActInd", "GoalsName", "PICCode", "PICName",

                    //6-10
                     "DirectorateName", "EvaluatorCode", "EvaluatorName", "TopEvaluatorCode", "TopEvaluatorName",

                    //11-12
                     "Remark", "Status"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                    ChkActiveInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[2]), "Y");
                    TxtGoalSetting.EditValue = Sm.DrStr(dr, c[3]);
                    TxtPICCode.EditValue = Sm.DrStr(dr, c[4]);
                    TxtPICName.EditValue = Sm.DrStr(dr, c[5]);
                    Sm.SetLue(LueDirectorate, Sm.DrStr(dr, c[6]));
                    TxtEvaluator.EditValue = Sm.DrStr(dr, c[8]);
                    TxtTopEvaluator.EditValue = Sm.DrStr(dr, c[10]);
                    MeeRemark.EditValue = Sm.DrStr(dr, c[11]);
                    TxtStatus.EditValue = Sm.DrStr(dr, c[12]);
                    EvaluatorCode = Sm.DrStr(dr, c[7]);
                    TopEvaluatorCode = Sm.DrStr(dr, c[9]);
                }, true
            );
        }

        private void ShowGoalSettingDtl(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            SQL.AppendLine("SELECT A.DNo, A.WorkGoals, A.Quality, A.Measure, B.OptDesc AS Evaluation, A.Evaluation EvaluationCode, A.Deadline ");
            SQL.AppendLine("FROM TblGoalsSettingDtl A  ");
            SQL.AppendLine("Left JOIN tbloption B ON B.OptCode = A.Evaluation AND B.OptCat ='GoalsSettingEvaluation' ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo ");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[]
                { 
                    //0
                    "DNo", 
                    //1-5
                    "WorkGoals", "Quality", "Measure", "Evaluation", "EvaluationCode" ,
                    //6
                    "Deadline"

                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("N", Grd1, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("S", Grd1, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("D", Grd1, dr, c, Row, 6, 6);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 2 });
            ComputeTotalDtl(Grd1, TxtTotalFinancial, 2);
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ShowGoalSettingDtl2(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            SQL.AppendLine("SELECT A.DNo, A.WorkGoals, A.Quality, A.Measure, B.OptDesc AS Evaluation, A.Evaluation EvaluationCode, A.Deadline ");
            SQL.AppendLine("FROM TblGoalsSettingDtl2 A  ");
            SQL.AppendLine("Left JOIN tbloption B ON B.OptCode = A.Evaluation AND B.OptCat ='GoalsSettingEvaluation' ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo ");

            Sm.ShowDataInGrid(
                ref Grd2, ref cm,
                SQL.ToString(),
                new string[]
                { 
                    //0
                    "DNo", 
                    //1-5
                    "WorkGoals", "Quality", "Measure", "Evaluation", "EvaluationCode",
                    //6
                    "Deadline"

                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("N", Grd2, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("S", Grd2, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("D", Grd2, dr, c, Row, 6, 6);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd2, Grd2.Rows.Count - 1, new int[] { 2 });
            Sm.FocusGrd(Grd2, 0, 1);
            ComputeTotalDtl(Grd2, TxtTotalCustomer, 2);
        }

        private void ShowGoalSettingDtl3(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            SQL.AppendLine("SELECT A.DNo, A.WorkGoals, A.Quality, A.Measure, B.OptDesc AS Evaluation, A.Evaluation EvaluationCode, A.Deadline ");
            SQL.AppendLine("FROM TblGoalsSettingDtl3 A  ");
            SQL.AppendLine("Left JOIN tbloption B ON B.OptCode = A.Evaluation AND B.OptCat ='GoalsSettingEvaluation' ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo ");

            Sm.ShowDataInGrid(
                ref Grd3, ref cm,
                SQL.ToString(),
                new string[]
                { 
                    //0
                    "DNo", 
                    //1-5
                    "WorkGoals", "Quality", "Measure", "Evaluation", "EvaluationCode",
                    //6
                    "Deadline"

                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd3, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd3, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("N", Grd3, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd3, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd3, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("S", Grd3, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("D", Grd3, dr, c, Row, 6, 6);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd3, Grd3.Rows.Count - 1, new int[] { 2 });
            ComputeTotalDtl(Grd3, TxtTotalInternalProcess, 2);
            Sm.FocusGrd(Grd3, 0, 1);
        }

        private void ShowGoalSettingDtl4(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            SQL.AppendLine("SELECT A.DNo, A.WorkGoals, A.Quality, A.Measure, B.OptDesc AS Evaluation, A.Evaluation EvaluationCode, A.Deadline ");
            SQL.AppendLine("FROM TblGoalsSettingDtl4 A  ");
            SQL.AppendLine("Left JOIN tbloption B ON B.OptCode = A.Evaluation AND B.OptCat ='GoalsSettingEvaluation' ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo ");

            Sm.ShowDataInGrid(
                ref Grd4, ref cm,
                SQL.ToString(),
                new string[]
                { 
                    //0
                    "DNo", 
                    //1-5
                    "WorkGoals", "Quality", "Measure", "Evaluation", "EvaluationCode",
                    //6
                    "Deadline"

                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd4, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd4, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("N", Grd4, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd4, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("S", Grd4, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("S", Grd4, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("D", Grd4, dr, c, Row, 6, 6);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd4, Grd4.Rows.Count - 1, new int[] { 2 });
            ComputeTotalDtl(Grd4, TxtTotalLearning, 2);
            Sm.FocusGrd(Grd4, 0, 1);
        }

        #endregion

        #region Additional Method

        private void ParPrint() // int dihapus
        {
            var l = new List<GoalsSettingHdr>();
            var lDtl = new List<GoalsSettingDtl>();

            string[] TableName = { "GoalsSettingHdr", "GoalsSettingDtl" };
            List<IList> myLists = new List<IList>();

            var cm = new MySqlCommand();

            #region Header

            var SQL = new StringBuilder();
            SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyAddressCity', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyPhone', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle5') As 'CompanyFax', ");
            SQL.AppendLine("A.DocNo,Date_Format(A.DocDt,'%d %M %Y')As DocDt, A.ActInd, A.GoalsName, A.PICCode, B.EmpName AS PICName,   ");
            SQL.AppendLine("E.OptCode AS DirectorateName, A.EvaluatorCode, C.EmpName AS EvaluatorName, A.TopEvaluatorCode,  ");
            SQL.AppendLine("D.EmpName As TopEvaluatorName, A.Remark, F.PosName AS PICPosName, G.PosName AS EvaluatorPosName, H.PosName AS TopEvaluatorPosName,   ");
            SQL.AppendLine("ifnull(I.Divisionname, '-') AS DivName, J.SiteName, A.EvaluatorCode, A.TopEvaluatorCode, ");
            SQL.AppendLine("Case  ");
            SQL.AppendLine("	when A.`Status` = 'O' then 'Outstanding'  ");
            SQL.AppendLine("	when A.`Status` = 'A' then 'Approved'  ");
            SQL.AppendLine("	when A.`Status` = 'C' then 'Canceled'  ");
            SQL.AppendLine("END AS `Status`  ");
            SQL.AppendLine("FROM TblGoalsSettingHdr A  ");
            SQL.AppendLine("LEFT JOIN tblemployee B ON B.EmpCode = A.PICCode  ");
            SQL.AppendLine("LEFT JOIN tblemployee C ON C.EmpCode = A.EvaluatorCode ");
            SQL.AppendLine("LEFT JOIN tblemployee D ON D.EmpCode = A.TopEvaluatorCode ");
            SQL.AppendLine("INNER JOIN tbloption E ON E.OptCode = A.DirectorateCode AND E.OptCat = 'GoalsSettingDirectorate'  ");
            SQL.AppendLine("LEFT JOIN tblposition F ON B.PosCode = F.PosCode ");
            SQL.AppendLine("LEFT JOIN tblposition G ON C.PosCode = G.PosCode ");
            SQL.AppendLine("LEFT JOIN tblposition H ON D.PosCode = H.PosCode ");
            SQL.AppendLine("LEFT JOIN tbldivision I ON B.DivisionCode = I.DivisionCode ");
            SQL.AppendLine("Inner Join TblSite J On B.SiteCode = J.SiteCode ");
            SQL.AppendLine("WHERE A.DocNo = @DocNo  ");


            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]
                        {
                        //0
                         "CompanyLogo",

                         //1-5
                         "CompanyName",
                         "CompanyAddress",
                         "CompanyAddressCity",
                         "CompanyPhone",
                         "CompanyFax",
                         //6-10
                         "DocNo",
                         "DocDt",
                         "GoalsName",
                         "PICCode",
                         "PICName",
                         //11-15
                         "DirectorateName",
                         "EvaluatorName",
                         "TopEvaluatorName",
                         "Remark",
                         "PICPosName",
                         //16-20
                         "EvaluatorPosName",
                         "TopEvaluatorPosName",
                         "DivName",
                         "SiteName",
                         "EvaluatorCode",
                         //21
                         "TopEvaluatorCode",
                    });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new GoalsSettingHdr()
                        {
                            CompanyLogo = Sm.DrStr(dr, c[0]),
                            CompanyName = Sm.DrStr(dr, c[1]),
                            CompanyAddress = Sm.DrStr(dr, c[2]),
                            CompanyAddressCity = Sm.DrStr(dr, c[3]),
                            CompanyPhone = Sm.DrStr(dr, c[4]),
                            CompanyFax = Sm.DrStr(dr, c[5]),

                            DocNo = Sm.DrStr(dr, c[6]),
                            DocDt = Sm.DrStr(dr, c[7]),
                            GoalsName = Sm.DrStr(dr, c[8]),
                            EmpCode = Sm.DrStr(dr, c[9]),
                            EmpName = Sm.DrStr(dr, c[10]),

                            Directorate = Sm.DrStr(dr, c[11]),
                            Appraiser1 = Sm.DrStr(dr, c[12]),
                            Appraiser2 = Sm.DrStr(dr, c[13]),
                            Remark = Sm.DrStr(dr, c[14]),
                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime())),
                            PICPosName = Sm.DrStr(dr, c[15]),
                            EvaluatorPosName = Sm.DrStr(dr, c[16]),
                            TopEvaluatorPosName = Sm.DrStr(dr, c[17]),
                            DivName = Sm.DrStr(dr, c[18]),
                            SiteName = Sm.DrStr(dr, c[19]),
                            EvaluatorCode = Sm.DrStr(dr, c[20]),
                            TopEvaluatorCode = Sm.DrStr(dr, c[21]),
                        });
                    }
                }
                dr.Close();
            }
            myLists.Add(l);
            #endregion

            #region Detail

            var cmDtl = new MySqlCommand();

            var SQLDtl = new StringBuilder();
            using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl.Open();
                cmDtl.Connection = cnDtl;

                SQLDtl.AppendLine("Select * from ( ");
                SQLDtl.AppendLine("SELECT '1' as seqno, 'Financial' AS Category, A.DNo, A.WorkGoals, A.Quality, A.Measure, B.OptDesc AS Evaluation, ");
                SQLDtl.AppendLine("A.Evaluation EvaluationCode, Date_Format(A.deadline,'%d %b %Y')As deadline ");
                SQLDtl.AppendLine("FROM TblGoalsSettingDtl A ");
                SQLDtl.AppendLine("Left JOIN tbloption B ON B.OptCode = A.Evaluation AND B.OptCat = 'GoalsSettingEvaluation' ");
                SQLDtl.AppendLine("Where A.DocNo = @DocNo ");
                SQLDtl.AppendLine("                UNION all ");
                SQLDtl.AppendLine("SELECT '2', 'Customer', A.DNo, A.WorkGoals, A.Quality, A.Measure, B.OptDesc AS Evaluation,  ");
                SQLDtl.AppendLine("A.Evaluation EvaluationCode, Date_Format(A.deadline,'%d %b %Y')As deadline ");
                SQLDtl.AppendLine("FROM tblgoalssettingdtl2 A ");
                SQLDtl.AppendLine("Left JOIN tbloption B ON B.OptCode = A.Evaluation AND B.OptCat = 'GoalsSettingEvaluation' ");
                SQLDtl.AppendLine("Where A.DocNo = @DocNo  ");
                SQLDtl.AppendLine("                UNION all ");
                SQLDtl.AppendLine("SELECT '3', 'Internal Business Process', A.DNo, A.WorkGoals, A.Quality, A.Measure, B.OptDesc AS Evaluation,  ");
                SQLDtl.AppendLine("A.Evaluation EvaluationCode,  Date_Format(A.deadline,'%d %b %Y')As deadline  ");
                SQLDtl.AppendLine("FROM tblgoalssettingdtl3 A ");
                SQLDtl.AppendLine("Left JOIN tbloption B ON B.OptCode = A.Evaluation AND B.OptCat = 'GoalsSettingEvaluation' ");
                SQLDtl.AppendLine("Where A.DocNo = @DocNo ");
                SQLDtl.AppendLine("                UNION all ");
                SQLDtl.AppendLine("SELECT '4', 'People Development', A.DNo, A.WorkGoals, A.Quality, A.Measure, B.OptDesc AS Evaluation,  ");
                SQLDtl.AppendLine("A.Evaluation EvaluationCode,  Date_Format(A.deadline,'%d %b %Y')As deadline  ");
                SQLDtl.AppendLine("FROM tblgoalssettingdtl4 A ");
                SQLDtl.AppendLine("Left JOIN tbloption B ON B.OptCode = A.Evaluation AND B.OptCat = 'GoalsSettingEvaluation' ");
                SQLDtl.AppendLine("Where A.DocNo = @DocNo  ");
                SQLDtl.AppendLine(")X Order By X.Category, X.DNo ");

                cmDtl.CommandText = SQLDtl.ToString();
                Sm.CmParam<String>(ref cmDtl, "@DocNo", TxtDocNo.Text);
                var drDtl = cmDtl.ExecuteReader();
                var cDtl = Sm.GetOrdinal(drDtl, new string[]
                        {
                         //0
                         "category" ,

                         //1-5
                         "DNo" ,
                         "WorkGoals",
                         "Quality",
                         "Measure",
                         "Evaluation",

                         //6
                         "EvaluationCode",
                         "deadline", 
                         "SeqNo"
                        });
                if (drDtl.HasRows)
                {
                    while (drDtl.Read())
                    {
                        lDtl.Add(new GoalsSettingDtl()
                        {
                            Category = Sm.DrStr(drDtl, cDtl[0]),
                            Dno = Sm.DrStr(drDtl, cDtl[1]),
                            WorksGoals = Sm.DrStr(drDtl, cDtl[2]),
                            Quality = Sm.DrDec(drDtl, cDtl[3]),
                            Measure = Sm.DrStr(drDtl, cDtl[4]),
                            Evaluation = Sm.DrStr(drDtl, cDtl[5]),
                            EvaluationCode = Sm.DrStr(drDtl, cDtl[6]),
                            Deadline = Sm.DrStr(drDtl, cDtl[7]),
                            SeqNo = Sm.DrDec(drDtl, cDtl[8])
                        });
                    }
                }
                drDtl.Close();
            }
            myLists.Add(lDtl);


            #endregion

            Sm.PrintReport("HINGoalsSetting", myLists, TableName, false);

        }

        private void GetParameter()
        {
            mIsFilterBySite = Sm.GetParameter("IsFilterBySite") == "Y";
        }

        private void LueRequestEdit(
           iGrid Grd,
           DevExpress.XtraEditors.LookUpEdit Lue,
           ref iGCell fCell,
           ref bool fAccept,
           TenTec.Windows.iGridLib.iGRequestEditEventArgs e)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;
            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        #endregion

        #region Event

        #region Button Click
        private void BtnPIC_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmGoalsSettingDlg(this, 1));
        }

        private void BtnEvaluator_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmGoalsSettingDlg(this, 2));
        }

        private void BtnTopEvaluator_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmGoalsSettingDlg(this, 3));
        }

        #endregion

        #region Date Edit

        private void DteDeadLine0_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd1, ref fAccept, e);
        }

        private void DteDeadLine0_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DteDeadLine0, ref fCell, ref fAccept);
        }

        private void DteDeadLine_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd2, ref fAccept, e);
        }

        private void DteDeadLine_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DteDeadLine, ref fCell, ref fAccept);
        }

        private void DteDeadLine2_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd3, ref fAccept, e);
        }

        private void DteDeadLine2_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DteDeadLine2, ref fCell, ref fAccept);
        }

        private void DteDeadLine3_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.DteKeyDown(Grd4, ref fAccept, e);
        }

        private void DteDeadLine3_Leave(object sender, EventArgs e)
        {
            Sm.DteLeave(DteDeadLine3, ref fCell, ref fAccept);
        }

        #endregion

        #region event Lue
        private void LuePosition1_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LuePosition1, new Sm.RefreshLue2(Sl.SetLueOption), "GoalsSettingEvaluation");
        }

        private void LuePosition2_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LuePosition2, new Sm.RefreshLue2(Sl.SetLueOption), "GoalsSettingEvaluation");
        }

        private void LuePosition3_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LuePosition3, new Sm.RefreshLue2(Sl.SetLueOption), "GoalsSettingEvaluation");
        }

        private void LuePosition4_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LuePosition4, new Sm.RefreshLue2(Sl.SetLueOption), "GoalsSettingEvaluation");
        }

        private void LuePosition1_Leave(object sender, EventArgs e)
        {
            if (LuePosition1.Visible && fAccept && fCell.ColIndex == 4)
            {
                if (Sm.GetLue(LuePosition1).Length == 0)
                {
                    Grd1.Cells[fCell.RowIndex, 5].Value =
                    Grd1.Cells[fCell.RowIndex, 4].Value = null;
                }
                else
                {
                    Grd1.Cells[fCell.RowIndex, 5].Value = Sm.GetLue(LuePosition1);
                    Grd1.Cells[fCell.RowIndex, 4].Value = LuePosition1.GetColumnValue("Col2");
                }
            }
            LuePosition1.Visible = false;
        }

        private void LuePosition2_Leave(object sender, EventArgs e)
        {
            if (LuePosition2.Visible && fAccept && fCell.ColIndex == 4)
            {
                if (Sm.GetLue(LuePosition2).Length == 0)
                {
                    Grd2.Cells[fCell.RowIndex, 5].Value =
                    Grd2.Cells[fCell.RowIndex, 4].Value = null;
                }
                else
                {
                    Grd2.Cells[fCell.RowIndex, 5].Value = Sm.GetLue(LuePosition2);
                    Grd2.Cells[fCell.RowIndex, 4].Value = LuePosition2.GetColumnValue("Col2");
                }
            }
            LuePosition2.Visible = false;
        }

        private void LuePosition3_Leave(object sender, EventArgs e)
        {
            if (LuePosition3.Visible && fAccept && fCell.ColIndex == 4)
            {
                if (Sm.GetLue(LuePosition3).Length == 0)
                {
                    Grd3.Cells[fCell.RowIndex, 5].Value =
                    Grd3.Cells[fCell.RowIndex, 4].Value = null;
                }
                else
                {
                    Grd3.Cells[fCell.RowIndex, 5].Value = Sm.GetLue(LuePosition3);
                    Grd3.Cells[fCell.RowIndex, 4].Value = LuePosition3.GetColumnValue("Col2");
                }
            }
            LuePosition3.Visible = false;
        }

        private void LuePosition4_Leave(object sender, EventArgs e)
        {
            if (LuePosition4.Visible && fAccept && fCell.ColIndex == 4)
            {
                if (Sm.GetLue(LuePosition4).Length == 0)
                {
                    Grd4.Cells[fCell.RowIndex, 5].Value =
                    Grd4.Cells[fCell.RowIndex, 4].Value = null;
                }
                else
                {
                    Grd4.Cells[fCell.RowIndex, 5].Value = Sm.GetLue(LuePosition4);
                    Grd4.Cells[fCell.RowIndex, 4].Value = LuePosition4.GetColumnValue("Col2");
                }
            }
            LuePosition4.Visible = false;
        }


        private void LuePosition1_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void LuePosition2_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd2, ref fAccept, e);
        }

        private void ComputeTotalDtl(iGrid Grd, DXE.TextEdit TxtTotal, int Col)
        {
            decimal tempTotal = 0m;

            if (Grd.Rows.Count > 0)
            {
                for (int i = 0; i < Grd.Rows.Count; i++)
                    tempTotal = tempTotal + Sm.GetGrdDec(Grd, i, Col);
            }

            TxtTotal.EditValue = Sm.FormatNum(tempTotal, 0);
            TxtTotalGoalsSetting.EditValue = Sm.FormatNum(decimal.Parse(TxtTotalFinancial.Text) + decimal.Parse(TxtTotalCustomer.Text) + decimal.Parse(TxtTotalInternalProcess.Text) + decimal.Parse(TxtTotalLearning.Text), 0);
        }

        private void Grd1_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            try
            {
                if (e.ColIndex == 2)
                {
                    ComputeTotalDtl(Grd1, TxtTotalFinancial, 2);
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void Grd2_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            try
            {
                ComputeTotalDtl(Grd2, TxtTotalCustomer, 2);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void Grd3_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            try
            {
                ComputeTotalDtl(Grd3, TxtTotalInternalProcess, 2);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void Grd4_AfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            try
            {
                ComputeTotalDtl(Grd4, TxtTotalLearning, 2);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void LuePosition3_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd3, ref fAccept, e);
        }

        private void LuePosition4_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd4, ref fAccept, e);
        }

        private void ChkActiveInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (ChkActiveInd.Checked == true)
                {
                    string ActiveInd = Sm.GetValue("SELECT A.ActInd FROM TblGoalsSettingHdr A WHERE A.DocNo = '" + TxtDocNo.Text + "'");
                    if (ActiveInd == "N")
                    {
                        Sm.StdMsg(mMsgType.Warning, "Goals Setting Can't Set To Active Because It's Already Not Active");
                        ChkActiveInd.Checked = false;
                    }
                }

            }
        }

        #endregion

        #endregion

        #region Class
        class GoalsSettingHdr
        {
            public string CompanyLogo { set; get; }
            public string CompanyName { get; set; }
            public string CompanyAddressCity { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyPhone { get; set; }
            public string CompanyFax { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string GoalsName { get; set; }
            public string EmpCode { get; set; }
            public string EmpName { get; set; }
            public string Directorate { get; set; }
            public string Remark { get; set; }
            public string Appraiser1 { get; set; }
            public string Appraiser2 { get; set; }
            public string PrintBy { get; set; }
            public string PICPosName { get; set; }
            public string EvaluatorPosName { get; set; }
            public string TopEvaluatorPosName { get; set; }
            public string DivName { get; set; }
            public string SiteName { get; set; }
            public string EvaluatorCode { get; set; }
            public string TopEvaluatorCode { get; set; }
        }

        class GoalsSettingDtl
        {
            public string Category { set; get; }
            public string Dno { get; set; }
            public string WorksGoals { get; set; }
            public decimal Quality { get; set; }
            public string Measure { get; set; }
            public string Evaluation { get; set; }
            public string EvaluationCode { get; set; }
            public string Deadline { get; set; }
            public decimal SeqNo { get; set; }
        }

        #endregion

        #endregion

    }
}