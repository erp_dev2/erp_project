﻿#region Update
/*
    11/09/2017 [WED] tambah title bar
    25/09/2017 [HAR] tambah informasi HM
    30/10/2017 [TKG] tambah filter dan informasi location dan fixed indicator
    08/12/2017 [HAR] bug fixing set usercode blm ada 
    11/Des/2017 [HAR] tambah Fixed By
    23/02/2022 [TRI] jumlah hours meter beda dengan detail form utama
    24/02/2022 [TRI] bug jumlah hours meter di detail sama angkanya per asset
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmWOFind : RunSystem.FrmBase2
    {
        private FrmWO mFrmParent;
        string mSQL = string.Empty;

        #region Constructor

        public FrmWOFind(FrmWO FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                SetGrd();
                SetSQL();
                SetLueLocCode(ref LueLocCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.CancelInd, A.SettleInd, A.WORDocNo, ");
            SQL.AppendLine("C.OptDesc As MtcStatusDesc, ");
            SQL.AppendLine("D.OptDesc As MtcTypeDesc, ");
            SQL.AppendLine("E.OptDesc As SymProblemDesc, ");
            SQL.AppendLine("B.Description, G.OptDesc As WOActivityDesc, L.LocName, A.FixedInd, A.FixedBy, F.Dt1, F.Tm1, F.Dt2, F.Tm2, ");
            SQL.AppendLine("H.AssetCode, I.Assetname, I.DisplayName, J.HoursMeter, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("From TblWOHdr A");
            SQL.AppendLine("Inner Join TblWOR B On A.WORDocNo=B.DocNo ");
            SQL.AppendLine("Left Join TblOption C On B.MtcStatus=C.OptCode And C.OptCat ='MaintenanceStatus' ");
            SQL.AppendLine("Left Join TblOption D On B.MtcType=D.OptCode And D.OptCat ='MaintenanceType' ");
            SQL.AppendLine("Left Join TblOption E On B.SymProblem=E.OptCode And E.OptCat ='SymptomProblem' ");
            SQL.AppendLine("Inner Join TblWODtl F On A.DocNo=F.DocNo ");
            SQL.AppendLine("Inner Join TblOption G On F.WOActivity=G.OptCode And G.OptCat ='WOActivity' ");
            SQL.AppendLine("Inner Join TblTohdr H On B.TOCode = H.AssetCode ");
            SQL.AppendLine("Inner Join TblAsset I On H.AssetCode = I.AssetCode ");
            SQL.AppendLine("Left Join TblHoursMeter J On A.HMDocNo = J.DocNo ");
            if (mFrmParent.mIsFilterBySite)
            {
                SQL.AppendLine("Inner Join TblTOHdr K On B.TOCode = K.AssetCode ");
                SQL.AppendLine("Inner Join TblLocation L On K.LocCode = L.LocCode ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From tblGroupSite  ");
                SQL.AppendLine("        Where SiteCode=L.SiteCode ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine("    ) ");
            }
            else
            {
                SQL.AppendLine("Left Join TblTOHdr K On B.TOCode = K.AssetCode ");
                SQL.AppendLine("Left Join TblLocation L On K.LocCode = L.LocCode ");
            }
            SQL.AppendLine("Where A.DocDt Between @DocDt1 And @DocDt2 ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 28;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#", 
                        "Date",
                        "Cancel",
                        "Settled",
                        "WO Request#",
                        
                        //6-10
                        "Status",
                        "Type",
                        "Symptom"+Environment.NewLine+"Problem",
                        "Description",
                        "Location",
                        
                        //11-15
                        "Fixed",
                        "Fixed By",
                        "Activity",
                        "Start Date",
                        "Start Time",

                        //16-20
                        "End Date",
                        "End Time",
                        "Asset Code",
                        "Asset Name",
                        "Display Name",
                        
                        //21-25
                        "Hours Meter",
                        "Created"+Environment.NewLine+"By",
                        "Created"+Environment.NewLine+"Date",
                        "Created"+Environment.NewLine+"Time",
                        "Last"+Environment.NewLine+"Updated By",

                        //26
                        "Last"+Environment.NewLine+"Updated Date", 
                        "Last"+Environment.NewLine+"Updated Time"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        150, 80, 80, 80, 150, 
                        
                        //6-10
                        100, 100, 100, 200, 150,  
                        
                        //11-15
                        100, 100, 200, 100, 100,  

                        //16-20
                        100, 100, 100, 150, 200,  

                        //21-25
                        100, 100, 100, 100, 100,  
                        
                        //26
                        100, 100
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] {21}, 0);
            Sm.GrdColCheck(Grd1, new int[] { 3, 4, 11 });
            Sm.GrdFormatDate(Grd1, new int[] { 2, 14, 16, 23, 26 });
            Sm.GrdFormatTime(Grd1, new int[] { 15, 17, 24, 27 });
            Sm.GrdColInvisible(Grd1, new int[] { 22, 23, 24, 25, 26, 27 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 22, 23, 24, 25, 26, 27 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtWORDocNo.Text, "A.WORDocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtAssetCode.Text, new string[] { "H.AssetCode", "I.Assetname", "I.DisplayName" });
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueLocCode), "L.LocCode", true);

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.CreateDt Desc;",
                        new string[]
                        {
                            //0
                            "DocNo",

                            //1-5
                            "DocDt", "CancelInd", "SettleInd", "WORDocNo", "MtcStatusDesc", 

                            //6-10
                            "MtcTypeDesc", "SymProblemDesc", "Description", "LocName", "FixedInd", 
                            
                            //11-15
                            "FixedBy", "WOActivityDesc", "Dt1", "Tm1", "Dt2",  
                            
                            //16-20
                            "Tm2", "AssetCode", "AssetName", "DisplayName", "HoursMeter",  
                            
                            //21-24
                            "CreateBy", "CreateDt", "LastUpBy", "LastUpDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("T2", Grd, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 15, 15);
                            Sm.SetGrdValue("T2", Grd, dr, c, Row, 16, 16);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 17);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 18);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 19);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 20);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 21);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 23, 22);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 24, 22);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 23);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 26, 24);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 27, 24);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Method

        private void SetLueLocCode(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select T.LocCode As Col1, T.LocName As Col2 ");
            SQL.AppendLine("From TblLocation T ");
            if (mFrmParent.mIsFilterBySite)
            {
                SQL.AppendLine("Where Exists( ");
                SQL.AppendLine("    Select 1 From tblGroupSite  ");
                SQL.AppendLine("    Where SiteCode=T.SiteCode ");
                SQL.AppendLine("    And GrpCode In ( ");
                SQL.AppendLine("        Select GrpCode From TblUser ");
                SQL.AppendLine("        Where UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }
            SQL.AppendLine("Order By T.LocName;");

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            Sm.SetLue2(
                ref Lue, ref cm,
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "WO#");
        }

        private void TxtWORDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkWORDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "WO request#");
        }

        private void ChkAssetCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Asset#");
        }

        private void TxtAssetCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void LueLocCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueLocCode, new Sm.RefreshLue1(SetLueLocCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkLocCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Location");
        }

        #endregion

        #endregion
    }
}
