﻿#region Update
/*
    03/11/2020 [TKG/IMS] tambah validasi payroll group 
    12/11/2020 [TKG/IMS] DNo jadi 5 karakter
    05/07/2021 [TKG/IMS] menghilangkan IsPeriodNotValid()
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using System.IO;

#endregion

namespace RunSystem
{
    public partial class FrmEmpIncentive2 : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string 
            mMenuCode = string.Empty, mAccessInd = string.Empty, 
            mDocNo = string.Empty, 
            mGenerateEmpCodeFormat = string.Empty;
        private string mPGCode = string.Empty;
        private bool mIsEmpCodeUseContractDt = false;
        internal FrmEmpIncentive2Find FrmFind;

        #endregion

        #region Constructor

        public FrmEmpIncentive2(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                SetGrd();
                SetFormControl(mState.View);
                Sl.SetLueYr(LueYr, string.Empty);
                Sl.SetLueMth(LueMth);
                base.FrmLoad(sender, e);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 26;
            Grd1.FrozenArea.ColCount = 3;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Employee's"+Environment.NewLine+"Code", 
                        "Employee's"+Environment.NewLine+"Name",
                        "Old Code", 
                        "Join Date",
                        "Grade",

                        //6-10
                        "Cost Group",
                        "Department",
                        "Payroll Group Code",
                        "Bank"+Environment.NewLine+"Account",
                        "Position",

                        //11-15
                        "On Duty"+Environment.NewLine+"Leave",
                        "Yearly"+Environment.NewLine+"Leave",
                        "Important"+Environment.NewLine+"Leave",
                        "Hospitality"+Environment.NewLine+"Leave",
                        "Pregnant"+Environment.NewLine+"Leave",

                        //16-20
                        "Off day",
                        "Absent",
                        "Effective"+Environment.NewLine+"Day",
                        "Salary",
                        "Allowance"+Environment.NewLine+"(jabatan)",

                        //21-25
                        "Allowance"+Environment.NewLine+"(functional)",
                        "Prosentase",
                        "Total",
                        "Work period",
                        "Payroll Group"
                    },
                    new int[] 
                    {
                        //0
                        50,
                        //1-5
                        80, 200, 80, 120, 150,
                        //6-10
                        100, 150, 0, 120, 120,
                        //11-15
                        100, 100, 100, 100, 100, 
                        //16-20
                        100, 100, 100, 150, 150, 
                        //21-25
                        150, 80, 150, 120, 200
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 4 });
            Sm.GrdFormatDec(Grd1, new int[] { 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3, 8 }, false);
            Grd1.Cols[20].Move(9);
            Sm.SetGrdProperty(Grd1, false);
        }


        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3 }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, ChkCancelInd, LueYr, LueMth,TxtTotalAmt, MeeRemark,
                    }, true); 
                    BtnImport.Enabled = false;
                    Grd1.ReadOnly = true;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueYr, LueMth, MeeRemark, 
                    }, false);
                    BtnImport.Enabled = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    ChkCancelInd.Properties.ReadOnly = false;
                    TxtDocNo.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            mPGCode = string.Empty;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, LueYr, LueMth, MeeRemark, 
            });
            ChkCancelInd.Checked = false;
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtTotalAmt }, 0);
            ClearGrd();
        }

        private void ClearGrd()
        {
            Grd1.Rows.Clear();
            Grd1.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24 });
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ClearData2()
        {
            if (BtnSave.Enabled) ClearGrd();
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmEmpIncentive2Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);

                var DocDt = Sm.GetDte(DteDocDt).Substring(0, 8);
                Sm.SetLue(LueYr, Sm.Left(DocDt, 4));
                Sm.SetLue(LueMth, DocDt.Substring(4, 2));
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, string.Empty, false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmEmployee("***");
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mEmpCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmEmployee("***");
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mEmpCode = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "EmpIncentive", "TblEmpIncentiveHdr");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveEmpIncentiveHdr(DocNo));
            if (Grd1.Rows.Count > 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0) cml.Add(SaveEmpIncentiveDtl(DocNo, Row));
            }

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            if (Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueYr, "Year") ||
                Sm.IsLueEmpty(LueMth, "Month") ||
                IsGrdEmpty() ||
                IsGrdValueNotValid() 
                //IsPeriodNotValid()
                )
                return true;
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            mPGCode = Sm.GetGrdStr(Grd1, 0, 8);
            for (int r = 0; r< Grd1.Rows.Count - 1; r++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, r, 1, false, "Employee's code is empty.")) return true;
                if (Sm.IsGrdValueEmpty(Grd1, r, 25, false, "Payroll group is empty.")) return true;
                if (!Sm.CompareStr(mPGCode, Sm.GetGrdStr(Grd1, r, 8)))
                {
                    Sm.StdMsg(mMsgType.Warning, 
                        "Employee's Code : " + Sm.GetGrdStr(Grd1, r, 1) + Environment.NewLine +
                        "Employee's Name : " + Sm.GetGrdStr(Grd1, r, 2) + Environment.NewLine + 
                        "Payroll's Group (Previous Data) : " + Sm.GetGrdStr(Grd1, 0, 25) + Environment.NewLine + 
                        "Payroll's Group (Current Data) : " + Sm.GetGrdStr(Grd1, r, 25) + Environment.NewLine + Environment.NewLine +
                        "One document should only process 1 payroll group.");
                    return true;
                }
            }
            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 employee.");
                return true;
            }
            return false;
        }

        private bool IsPeriodNotValid()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo From TblEmpIncentiveHdr ");
            SQL.AppendLine("Where Yr=@Yr ");
            SQL.AppendLine("And Mth=@Mth ");
            SQL.AppendLine("And CancelInd='N' ");
            SQL.AppendLine(";");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
            Sm.CmParam<String>(ref cm, "@Mth", Sm.GetLue(LueMth));
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Year : " + Sm.GetLue(LueYr) + Environment.NewLine +
                    "Month : " + Sm.GetLue(LueMth) + Environment.NewLine +
                    "Document already existed.");
                return true;
            }
            return false;
        }

        private MySqlCommand SaveEmpIncentiveHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblEmpIncentiveHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, Yr, Mth, PGCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values ");
            SQL.AppendLine("(@DocNo, @DocDt, 'N', @Yr, @Mth, @PGCode, @Remark, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
            Sm.CmParam<String>(ref cm, "@Mth", Sm.GetLue(LueMth));
            Sm.CmParam<String>(ref cm, "@PGCode", mPGCode);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveEmpIncentiveDtl(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblEmpIncentiveDtl(DocNo, DNo, EmpCode, OnDutyLeave, YearlyLeave, ImportantLeave, HospitalityLeave, PregnantLeave, OffDay, Absent,  "+
                    "EffectiveDay, Salary, PositionalAllowance, FunctionalAllowance, Prosentase, Amount, WorkPeriod, PayrunCode, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DNo, @EmpCode, @OnDutyLeave, @YearlyLeave, @ImportantLeave, @HospitalityLeave, @PregnantLeave, @OffDay, @Absent, "+
                    "@EffectiveDay, @Salary, @PositionalAllowance, @FunctionalAllowance, @Prosentase, @Amount, @WorkPeriod, null, @CreateBy, CurrentDateTime()); "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00000" + (Row + 1).ToString(), 5));
            Sm.CmParam<String>(ref cm, "@EmpCode", Sm.GetGrdStr(Grd1, Row, 1));
            Sm.CmParam<Decimal>(ref cm, "@OnDutyLeave", Sm.GetGrdDec(Grd1, Row, 11));
            Sm.CmParam<Decimal>(ref cm, "@YearlyLeave", Sm.GetGrdDec(Grd1, Row, 12));
            Sm.CmParam<Decimal>(ref cm, "@ImportantLeave", Sm.GetGrdDec(Grd1, Row, 13));
            Sm.CmParam<Decimal>(ref cm, "@HospitalityLeave", Sm.GetGrdDec(Grd1, Row, 14));
            Sm.CmParam<Decimal>(ref cm, "@PregnantLeave", Sm.GetGrdDec(Grd1, Row, 15));
            Sm.CmParam<Decimal>(ref cm, "@OffDay", Sm.GetGrdDec(Grd1, Row, 16));
            Sm.CmParam<Decimal>(ref cm, "@Absent", Sm.GetGrdDec(Grd1, Row, 17));
            Sm.CmParam<Decimal>(ref cm, "@EffectiveDay", Sm.GetGrdDec(Grd1, Row, 18));
            Sm.CmParam<Decimal>(ref cm, "@Salary", Sm.GetGrdDec(Grd1, Row, 19));
            Sm.CmParam<Decimal>(ref cm, "@PositionalAllowance", Sm.GetGrdDec(Grd1, Row, 20));
            Sm.CmParam<Decimal>(ref cm, "@FunctionalAllowance", Sm.GetGrdDec(Grd1, Row, 21));
            Sm.CmParam<Decimal>(ref cm, "@Prosentase", Sm.GetGrdDec(Grd1, Row, 22));
            Sm.CmParam<Decimal>(ref cm, "@Amount", Sm.GetGrdDec(Grd1, Row, 23));
            Sm.CmParam<Decimal>(ref cm, "@WorkPeriod", Sm.GetGrdDec(Grd1, Row, 24));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }
        #endregion

        #region Edit Data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(UpdateEmpIncentiveHdr(TxtDocNo.Text));

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                IsEmpIncentiveNotCancelled() ||
                IsEmpIncentiveCancelledAlready() ||
                IsDataProcessedAlready()
                ;
        }

        private bool IsEmpIncentiveNotCancelled()
        {
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this document.");
                return true;
            }
            return false;
        }

        private bool IsEmpIncentiveCancelledAlready()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DocNo From TblEmpIncentiveHdr ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='Y';");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This document already cancelled.");
                return true;
            }

            return false;
        }
        private bool IsDataProcessedAlready()
        {
            return Sm.IsDataExist(
                "Select 1 From TblVoucherRequestSpecialHdr A Inner Join TblVoucherRequestSpecialDtl B On A.DocNo=B.DocNo Where A.CancelInd='N' And B.DocNo2=@Param;",
                TxtDocNo.Text,
                "Data already processed into voucher Request Incentive.");
        }

        private MySqlCommand UpdateEmpIncentiveHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblEmpIncentiveHdr Set ");
            SQL.AppendLine("    CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowEmpIncentiveHdr(DocNo);
                ShowEmpIncentiveDtl(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowEmpIncentiveHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select DocNo, DocDt, CancelInd, Yr, Mth, PGCode, Remark " +
                    "From TblEmpIncentiveHdr Where DocNo=@DocNo; ",
                    new string[] { 
                        //0
                        "DocNo",

                        //1-5
                        "DocDt", "CancelInd", "Yr", "Mth", "PGCode", 
                        
                        //6
                        "Remark" 
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        ChkCancelInd.Checked = Sm.DrStr(dr, c[2]) == "Y";
                        Sm.SetLue(LueYr, Sm.DrStr(dr, c[3]));
                        Sm.SetLue(LueMth, Sm.DrStr(dr, c[4]));
                        mPGCode = Sm.DrStr(dr, c[5]);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[6]);
                    }, true
                );
        }

        private void ShowEmpIncentiveDtl(string DocNo)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();


            SQL.AppendLine("Select A.Dno, A.EmpCode, B.EmpName, B.EmpCodeOld, B.JoinDt, "); 
            SQL.AppendLine("C.GrdLvlName, B.CostGroup, D.DeptName,   ");
            SQL.AppendLine("B.PGCode, B.BankAcNo, E.PosName, A.OnDutyLeave, "); 
            SQL.AppendLine("A.YearlyLeave, A.ImportantLeave, A.HospitalityLeave, "); 
            SQL.AppendLine("A.PregnantLeave, A.OffDay, A.Absent, A.EffectiveDay, A.Salary, "); 
            SQL.AppendLine("A.PositionalAllowance, A.FunctionalAllowance, A.Prosentase,  ");
            SQL.AppendLine("A.Amount, A.WorkPeriod, G.PGName ");
            SQL.AppendLine("From TblEmpIncentiveDtl A ");
            SQL.AppendLine("Inner Join tblEmployee B On A.EmpCode = B.EmpCode "); 
            SQL.AppendLine("Left Join TblGradeLevelhdr C On B.GrdlvlCode = C.GrdlvlCode "); 
            SQL.AppendLine("Inner Join TblDepartment D On B.DeptCode = D.DeptCode  ");
            SQL.AppendLine("Left Join TblPosition E On B.PosCode = E.PosCode  ");
            SQL.AppendLine("Inner Join TblEmpIncentiveHdr F On A.DocNo=F.DocNo ");
            SQL.AppendLine("Left Join TblPayrollGrpHdr G On F.PGCode=G.PGCode  ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By A.DNo; ");

            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo",
                    //1-5
                    "EmpCode", "EmpName", "EmpCodeOld", "JoinDt", "GrdLvlName", 
                    //6-10
                    "CostGroup", "DeptName", "PGCode", "BankAcNo", "PosName",  
                    //11-15
                    "OnDutyLeave", "YearlyLeave", "ImportantLeave", "HospitalityLeave", "PregnantLeave",          
                    //16-20
                    "OffDay", "Absent", "EffectiveDay", "Salary", "PositionalAllowance", 
                    //21-25
                    "FunctionalAllowance", "Prosentase", "Amount", "WorkPeriod", "PGName"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 3);
                    Sm.SetGrdValue("D", Grd, dr, c, Row, 4, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 12);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 13);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 14);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 15);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 16);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 17);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 18);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 19);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 20);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 21);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 22, 22);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 23, 23);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 24, 24);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 25);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24 });
            Sm.FocusGrd(Grd1, 0, 1);
            ComputeToHeader();
        }

        #endregion

        #endregion 

        #region Additional Method

        private void GetParameter()
        {
            mGenerateEmpCodeFormat = Sm.GetParameter("GenerateEmpCodeFormat");
            mIsEmpCodeUseContractDt = Sm.GetParameterBoo("IsEmpCodeUseContractDt");
        }

        private void ProcessData()
        {
            if (Sm.IsDteEmpty(DteDocDt, "Date")) return;

            Cursor.Current = Cursors.WaitCursor;

            var lResult = new List<Result>();

            ClearGrd();
            try
            {
                Processa(ref lResult);
                if (lResult.Count > 0)
                {
                    Processb(ref lResult);
                }
                else
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                lResult.Clear();
                Cursor.Current = Cursors.Default;
            }
        }

        private void Processa(ref List<Result> l)
        {
            openFileDialog1.InitialDirectory = "c:";
            openFileDialog1.Filter = "CSV files (*.csv)|*.CSV";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.ShowDialog();

            var FileName = openFileDialog1.FileName;
            var EmpCodeTemp = string.Empty;
            bool IsFirst = true;

            using (var rd = new StreamReader(@FileName))
            {
                while (!rd.EndOfStream)
                {
                    var line = rd.ReadLine();
                    var arr = line.Split(',');

                    if (IsFirst)
                        IsFirst = false;
                    else
                    {
                        if (arr[1].Trim().Length > 0)
                        {
                            EmpCodeTemp = mGenerateEmpCodeFormat != "1" ?
                                               Sm.Right(string.Concat("00000000", arr[1].Trim()), 10) :
                                               mIsEmpCodeUseContractDt == false ?
                                                   Sm.Right(string.Concat("00000000", arr[1].Trim()), 8) :
                                                   Sm.Right(string.Concat("00000000", arr[1].Trim()), 9);
                            l.Add(new Result()
                            {
                                EmpCodeTemp = EmpCodeTemp,
                                EmpNameTemp = arr[2].Trim(),
                                OldCodeTemp = arr[3].Trim(),
                                JoinDateTemp = arr[4].Trim(),
                                GradeTemp = arr[5].Trim(),
                                CostGroupTemp = arr[6].Trim(),
                                DepartmentTemp = arr[7].Trim(),
                                PayrollGroupTemp = arr[8].Trim(),
                                BankAccountTemp = arr[9].Trim(),
                                PositionTemp = arr[10].Trim(),
                                onDutyLeaveTemp = Decimal.Parse(arr[11].Trim().Replace(",", "").Replace(@"\", "")),
                                YearlyLeaveTemp = Decimal.Parse(arr[12].Trim().Replace(",", "").Replace(@"\", "")),
                                ImportantLeaveTemp = Decimal.Parse(arr[13].Trim().Replace(",", "").Replace(@"\", "")),
                                HospitalityLeaveTemp = Decimal.Parse(arr[14].Trim().Replace(",", "").Replace(@"\", "")),
                                PregnantLeaveTemp = Decimal.Parse(arr[15].Trim().Replace(",", "").Replace(@"\", "")),
                                OffDayTemp = Decimal.Parse(arr[16].Trim().Replace(",", "").Replace(@"\", "")),
                                AbsentTemp = Decimal.Parse(arr[17].Trim().Replace(",", "").Replace(@"\", "")),
                                EffectiveDayTemp = Decimal.Parse(arr[18].Trim().Replace(",", "").Replace(@"\", "")),
                                SalaryTemp = Decimal.Parse(arr[19].Trim().Replace(",", "").Replace(@"\", "")),
                                AllowanceJabatanTemp = Decimal.Parse(arr[20].Trim().Replace(",", "").Replace(@"\", "")),
                                AllowanceFunctionalTemp = Decimal.Parse(arr[21].Trim().Replace(",", "").Replace(@"\", "")),
                                ProsentaseTemp = Decimal.Parse(arr[22].Trim().Replace(",", "").Replace(@"\", "")),
                                TotalTemp = Decimal.Parse(arr[23].Trim().Replace(",", "").Replace(@"\", "")),
                                WorkPeriodTemp = Decimal.Parse(arr[24].Trim().Replace(",", "").Replace(@"\", "")),
                            });
                        }
                    }
                }
            }
        }

        private void Processb(ref List<Result> l)
        {
            string PGCode = string.Empty, PGName = string.Empty;
            iGRow r;
            Grd1.BeginUpdate();
            Grd1.Rows.Count = 0;
            for (var i = 0; i < l.Count; i++)
            {
                r = Grd1.Rows.Add();
                r.Cells[0].Value = i+1;
                r.Cells[1].Value = l[i].EmpCodeTemp;
                r.Cells[2].Value = l[i].EmpNameTemp;
                r.Cells[3].Value = l[i].OldCodeTemp;
                r.Cells[4].Value = l[i].JoinDateTemp;
                r.Cells[5].Value = l[i].GradeTemp;
                r.Cells[6].Value = l[i].CostGroupTemp;
                r.Cells[7].Value = l[i].DepartmentTemp;
                if (!Sm.CompareStr(l[i].PayrollGroupTemp, PGCode))
                {
                    PGCode = l[i].PayrollGroupTemp;
                    PGName = Sm.GetValue("Select PGName From TblPayrollGrpHdr Where PGCode=@Param Limit 1;", PGCode);
                }
                r.Cells[8].Value = l[i].PayrollGroupTemp;
                r.Cells[25].Value = PGName;
                r.Cells[9].Value = l[i].BankAccountTemp;
                r.Cells[10].Value = l[i].PositionTemp;
                r.Cells[11].Value = l[i].onDutyLeaveTemp;
                r.Cells[12].Value = l[i].YearlyLeaveTemp;
                r.Cells[13].Value = l[i].ImportantLeaveTemp;
                r.Cells[14].Value = l[i].HospitalityLeaveTemp;
                r.Cells[15].Value = l[i].PregnantLeaveTemp;
                r.Cells[16].Value = l[i].OffDayTemp;
                r.Cells[17].Value = l[i].AbsentTemp;
                r.Cells[18].Value = l[i].EffectiveDayTemp;
                r.Cells[19].Value = l[i].SalaryTemp;
                r.Cells[20].Value = l[i].AllowanceJabatanTemp;
                r.Cells[21].Value = l[i].AllowanceFunctionalTemp;
                r.Cells[22].Value = l[i].ProsentaseTemp;
                r.Cells[23].Value = l[i].TotalTemp;
                r.Cells[24].Value = l[i].WorkPeriodTemp;
            }
            r = Grd1.Rows.Add();
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24 });
            Grd1.EndUpdate();
            ComputeToHeader();
        }

        private void ComputeToHeader()
        {
            decimal headAmt = 0m;
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.GetGrdStr(Grd1, Row, 1).Length > 0)
                {
                    headAmt += Sm.GetGrdDec(Grd1, Row, 23);
                }
            }
            TxtTotalAmt.EditValue = Sm.FormatNum(headAmt, 0);
        }

        #endregion

        #region event
        private void BtnImport_Click(object sender, EventArgs e)
        {
            ProcessData();
        }
        #endregion 

        #region Class

        private class Result
        {
            public string EmpCodeTemp { get; set; }
            public string EmpNameTemp { get; set; }
            public string OldCodeTemp { get; set; }
            public string JoinDateTemp { get; set; }
            public string GradeTemp { get; set; }
            public string CostGroupTemp { get; set; }
            public string DepartmentTemp { get; set; }
            public string PayrollGroupTemp { get; set; }
            public string BankAccountTemp { get; set; }
            public string PositionTemp { get; set; }
            public decimal onDutyLeaveTemp { get; set; }
            public decimal YearlyLeaveTemp { get; set; }
            public decimal ImportantLeaveTemp { get; set; }
            public decimal HospitalityLeaveTemp { get; set; }
            public decimal PregnantLeaveTemp { get; set; }
            public decimal OffDayTemp { get; set; }
            public decimal AbsentTemp { get; set; }
            public decimal EffectiveDayTemp { get; set; }
            public decimal SalaryTemp { get; set; }
            public decimal AllowanceJabatanTemp { get; set; }
            public decimal AllowanceFunctionalTemp { get; set; }
            public decimal ProsentaseTemp { get; set; }
            public decimal TotalTemp { get; set; }
            public decimal WorkPeriodTemp { get; set; }
        }

        #endregion
    }

 
}
