﻿namespace RunSystem
{
    partial class FrmAssesment2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel3 = new System.Windows.Forms.Panel();
            this.ChkActInd = new DevExpress.XtraEditors.CheckEdit();
            this.BtnImport = new DevExpress.XtraEditors.SimpleButton();
            this.label4 = new System.Windows.Forms.Label();
            this.LueCompetenceLevelType = new DevExpress.XtraEditors.LookUpEdit();
            this.label30 = new System.Windows.Forms.Label();
            this.LueCompLevelName = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtAsmName = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.label17 = new System.Windows.Forms.Label();
            this.TxtAsmCode = new DevExpress.XtraEditors.TextEdit();
            this.label18 = new System.Windows.Forms.Label();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.tabPage15 = new System.Windows.Forms.TabPage();
            this.MeeRemarkGrd15 = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeEviGrd15 = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeIndGrd15 = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeCompGrd15 = new DevExpress.XtraEditors.MemoExEdit();
            this.Grd15 = new TenTec.Windows.iGridLib.iGrid();
            this.panel21 = new System.Windows.Forms.Panel();
            this.LueLevel_15 = new DevExpress.XtraEditors.LookUpEdit();
            this.label28 = new System.Windows.Forms.Label();
            this.LueComp15 = new DevExpress.XtraEditors.LookUpEdit();
            this.label40 = new System.Windows.Forms.Label();
            this.tabPage14 = new System.Windows.Forms.TabPage();
            this.MeeRemarkGrd14 = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeEviGrd14 = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeIndGrd14 = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeCompGrd14 = new DevExpress.XtraEditors.MemoExEdit();
            this.Grd14 = new TenTec.Windows.iGridLib.iGrid();
            this.panel20 = new System.Windows.Forms.Panel();
            this.LueLevel_14 = new DevExpress.XtraEditors.LookUpEdit();
            this.label26 = new System.Windows.Forms.Label();
            this.LueComp14 = new DevExpress.XtraEditors.LookUpEdit();
            this.label38 = new System.Windows.Forms.Label();
            this.tabPage13 = new System.Windows.Forms.TabPage();
            this.MeeRemarkGrd13 = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeEviGrd13 = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeIndGrd13 = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeCompGrd13 = new DevExpress.XtraEditors.MemoExEdit();
            this.Grd13 = new TenTec.Windows.iGridLib.iGrid();
            this.panel19 = new System.Windows.Forms.Panel();
            this.LueLevel_13 = new DevExpress.XtraEditors.LookUpEdit();
            this.label24 = new System.Windows.Forms.Label();
            this.LueComp13 = new DevExpress.XtraEditors.LookUpEdit();
            this.label36 = new System.Windows.Forms.Label();
            this.tabPage12 = new System.Windows.Forms.TabPage();
            this.MeeRemarkGrd12 = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeEviGrd12 = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeIndGrd12 = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeCompGrd12 = new DevExpress.XtraEditors.MemoExEdit();
            this.Grd12 = new TenTec.Windows.iGridLib.iGrid();
            this.panel18 = new System.Windows.Forms.Panel();
            this.LueLevel_12 = new DevExpress.XtraEditors.LookUpEdit();
            this.label22 = new System.Windows.Forms.Label();
            this.LueComp12 = new DevExpress.XtraEditors.LookUpEdit();
            this.label34 = new System.Windows.Forms.Label();
            this.tabPage11 = new System.Windows.Forms.TabPage();
            this.MeeRemarkGrd11 = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeEviGrd11 = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeIndGrd11 = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeCompGrd11 = new DevExpress.XtraEditors.MemoExEdit();
            this.Grd11 = new TenTec.Windows.iGridLib.iGrid();
            this.panel5 = new System.Windows.Forms.Panel();
            this.LueLevel_11 = new DevExpress.XtraEditors.LookUpEdit();
            this.label20 = new System.Windows.Forms.Label();
            this.LueComp11 = new DevExpress.XtraEditors.LookUpEdit();
            this.label32 = new System.Windows.Forms.Label();
            this.tabPage10 = new System.Windows.Forms.TabPage();
            this.MeeRemarkGrd10 = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeEviGrd10 = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeIndGrd10 = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeCompGrd10 = new DevExpress.XtraEditors.MemoExEdit();
            this.Grd10 = new TenTec.Windows.iGridLib.iGrid();
            this.panel17 = new System.Windows.Forms.Panel();
            this.LueLevel_10 = new DevExpress.XtraEditors.LookUpEdit();
            this.label16 = new System.Windows.Forms.Label();
            this.LueComp10 = new DevExpress.XtraEditors.LookUpEdit();
            this.label31 = new System.Windows.Forms.Label();
            this.tabPage9 = new System.Windows.Forms.TabPage();
            this.MeeRemarkGrd9 = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeEviGrd9 = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeIndGrd9 = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeCompGrd9 = new DevExpress.XtraEditors.MemoExEdit();
            this.Grd9 = new TenTec.Windows.iGridLib.iGrid();
            this.panel16 = new System.Windows.Forms.Panel();
            this.LueLevel_9 = new DevExpress.XtraEditors.LookUpEdit();
            this.label14 = new System.Windows.Forms.Label();
            this.LueComp9 = new DevExpress.XtraEditors.LookUpEdit();
            this.label29 = new System.Windows.Forms.Label();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this.MeeRemarkGrd8 = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeEviGrd8 = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeIndGrd8 = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeCompGrd8 = new DevExpress.XtraEditors.MemoExEdit();
            this.Grd8 = new TenTec.Windows.iGridLib.iGrid();
            this.panel15 = new System.Windows.Forms.Panel();
            this.LueLevel_8 = new DevExpress.XtraEditors.LookUpEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.LueComp8 = new DevExpress.XtraEditors.LookUpEdit();
            this.label27 = new System.Windows.Forms.Label();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.MeeRemarkGrd7 = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeEviGrd7 = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeIndGrd7 = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeCompGrd7 = new DevExpress.XtraEditors.MemoExEdit();
            this.Grd7 = new TenTec.Windows.iGridLib.iGrid();
            this.panel14 = new System.Windows.Forms.Panel();
            this.LueLevel_7 = new DevExpress.XtraEditors.LookUpEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.LueComp7 = new DevExpress.XtraEditors.LookUpEdit();
            this.label25 = new System.Windows.Forms.Label();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.MeeRemarkGrd6 = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeEviGrd6 = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeIndGrd6 = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeCompGrd6 = new DevExpress.XtraEditors.MemoExEdit();
            this.Grd6 = new TenTec.Windows.iGridLib.iGrid();
            this.panel13 = new System.Windows.Forms.Panel();
            this.LueLevel_6 = new DevExpress.XtraEditors.LookUpEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.LueComp6 = new DevExpress.XtraEditors.LookUpEdit();
            this.label23 = new System.Windows.Forms.Label();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.MeeRemarkGrd5 = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeEviGrd5 = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeIndGrd5 = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeCompGrd5 = new DevExpress.XtraEditors.MemoExEdit();
            this.Grd5 = new TenTec.Windows.iGridLib.iGrid();
            this.panel12 = new System.Windows.Forms.Panel();
            this.LueLevel_5 = new DevExpress.XtraEditors.LookUpEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.LueComp5 = new DevExpress.XtraEditors.LookUpEdit();
            this.label21 = new System.Windows.Forms.Label();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.MeeRemarkGrd4 = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeEviGrd4 = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeIndGrd4 = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeCompGrd4 = new DevExpress.XtraEditors.MemoExEdit();
            this.Grd4 = new TenTec.Windows.iGridLib.iGrid();
            this.panel11 = new System.Windows.Forms.Panel();
            this.LueLevel_4 = new DevExpress.XtraEditors.LookUpEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.LueComp4 = new DevExpress.XtraEditors.LookUpEdit();
            this.label19 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.MeeRemarkGrd3 = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeEviGrd3 = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeIndGrd3 = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeCompGrd3 = new DevExpress.XtraEditors.MemoExEdit();
            this.Grd3 = new TenTec.Windows.iGridLib.iGrid();
            this.panel10 = new System.Windows.Forms.Panel();
            this.LueLevel_3 = new DevExpress.XtraEditors.LookUpEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.LueComp3 = new DevExpress.XtraEditors.LookUpEdit();
            this.label15 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.MeeRemarkGrd2 = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeEviGrd2 = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeIndGrd2 = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeCompGrd2 = new DevExpress.XtraEditors.MemoExEdit();
            this.Grd2 = new TenTec.Windows.iGridLib.iGrid();
            this.panel9 = new System.Windows.Forms.Panel();
            this.LueLevel_2 = new DevExpress.XtraEditors.LookUpEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.LueComp2 = new DevExpress.XtraEditors.LookUpEdit();
            this.label13 = new System.Windows.Forms.Label();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.MeeRemarkGrd1 = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeEviGrd1 = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeIndGrd1 = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeCompGrd1 = new DevExpress.XtraEditors.MemoExEdit();
            this.Grd1 = new TenTec.Windows.iGridLib.iGrid();
            this.panel8 = new System.Windows.Forms.Panel();
            this.LueLevel_1 = new DevExpress.XtraEditors.LookUpEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.LueComp1 = new DevExpress.XtraEditors.LookUpEdit();
            this.label11 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage16 = new System.Windows.Forms.TabPage();
            this.MeeRemarkGrd16 = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeEviGrd16 = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeIndGrd16 = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeCompGrd16 = new DevExpress.XtraEditors.MemoExEdit();
            this.Grd16 = new TenTec.Windows.iGridLib.iGrid();
            this.panel4 = new System.Windows.Forms.Panel();
            this.LueLevel_16 = new DevExpress.XtraEditors.LookUpEdit();
            this.label33 = new System.Windows.Forms.Label();
            this.LueComp16 = new DevExpress.XtraEditors.LookUpEdit();
            this.label35 = new System.Windows.Forms.Label();
            this.tabPage17 = new System.Windows.Forms.TabPage();
            this.MeeRemarkGrd17 = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeEviGrd17 = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeIndGrd17 = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeCompGrd17 = new DevExpress.XtraEditors.MemoExEdit();
            this.Grd17 = new TenTec.Windows.iGridLib.iGrid();
            this.panel6 = new System.Windows.Forms.Panel();
            this.LueLevel_17 = new DevExpress.XtraEditors.LookUpEdit();
            this.label37 = new System.Windows.Forms.Label();
            this.LueComp17 = new DevExpress.XtraEditors.LookUpEdit();
            this.label39 = new System.Windows.Forms.Label();
            this.tabPage18 = new System.Windows.Forms.TabPage();
            this.MeeRemarkGrd18 = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeEviGrd18 = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeIndGrd18 = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeCompGrd18 = new DevExpress.XtraEditors.MemoExEdit();
            this.Grd18 = new TenTec.Windows.iGridLib.iGrid();
            this.panel7 = new System.Windows.Forms.Panel();
            this.LueLevel_18 = new DevExpress.XtraEditors.LookUpEdit();
            this.label41 = new System.Windows.Forms.Label();
            this.LueComp18 = new DevExpress.XtraEditors.LookUpEdit();
            this.label42 = new System.Windows.Forms.Label();
            this.tabPage19 = new System.Windows.Forms.TabPage();
            this.MeeRemarkGrd19 = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeEviGrd19 = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeIndGrd19 = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeCompGrd19 = new DevExpress.XtraEditors.MemoExEdit();
            this.Grd19 = new TenTec.Windows.iGridLib.iGrid();
            this.panel22 = new System.Windows.Forms.Panel();
            this.LueLevel_19 = new DevExpress.XtraEditors.LookUpEdit();
            this.label43 = new System.Windows.Forms.Label();
            this.LueComp19 = new DevExpress.XtraEditors.LookUpEdit();
            this.label44 = new System.Windows.Forms.Label();
            this.tabPage20 = new System.Windows.Forms.TabPage();
            this.MeeRemarkGrd20 = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeEviGrd20 = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeIndGrd20 = new DevExpress.XtraEditors.MemoExEdit();
            this.MeeCompGrd20 = new DevExpress.XtraEditors.MemoExEdit();
            this.Grd20 = new TenTec.Windows.iGridLib.iGrid();
            this.panel23 = new System.Windows.Forms.Panel();
            this.LueLevel_20 = new DevExpress.XtraEditors.LookUpEdit();
            this.label45 = new System.Windows.Forms.Label();
            this.LueComp20 = new DevExpress.XtraEditors.LookUpEdit();
            this.label46 = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCompetenceLevelType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCompLevelName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAsmName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAsmCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            this.tabPage15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemarkGrd15.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeEviGrd15.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeIndGrd15.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCompGrd15.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd15)).BeginInit();
            this.panel21.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueLevel_15.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueComp15.Properties)).BeginInit();
            this.tabPage14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemarkGrd14.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeEviGrd14.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeIndGrd14.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCompGrd14.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd14)).BeginInit();
            this.panel20.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueLevel_14.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueComp14.Properties)).BeginInit();
            this.tabPage13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemarkGrd13.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeEviGrd13.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeIndGrd13.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCompGrd13.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd13)).BeginInit();
            this.panel19.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueLevel_13.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueComp13.Properties)).BeginInit();
            this.tabPage12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemarkGrd12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeEviGrd12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeIndGrd12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCompGrd12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd12)).BeginInit();
            this.panel18.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueLevel_12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueComp12.Properties)).BeginInit();
            this.tabPage11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemarkGrd11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeEviGrd11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeIndGrd11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCompGrd11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd11)).BeginInit();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueLevel_11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueComp11.Properties)).BeginInit();
            this.tabPage10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemarkGrd10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeEviGrd10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeIndGrd10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCompGrd10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd10)).BeginInit();
            this.panel17.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueLevel_10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueComp10.Properties)).BeginInit();
            this.tabPage9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemarkGrd9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeEviGrd9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeIndGrd9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCompGrd9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd9)).BeginInit();
            this.panel16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueLevel_9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueComp9.Properties)).BeginInit();
            this.tabPage8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemarkGrd8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeEviGrd8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeIndGrd8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCompGrd8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd8)).BeginInit();
            this.panel15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueLevel_8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueComp8.Properties)).BeginInit();
            this.tabPage7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemarkGrd7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeEviGrd7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeIndGrd7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCompGrd7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd7)).BeginInit();
            this.panel14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueLevel_7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueComp7.Properties)).BeginInit();
            this.tabPage6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemarkGrd6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeEviGrd6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeIndGrd6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCompGrd6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd6)).BeginInit();
            this.panel13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueLevel_6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueComp6.Properties)).BeginInit();
            this.tabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemarkGrd5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeEviGrd5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeIndGrd5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCompGrd5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd5)).BeginInit();
            this.panel12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueLevel_5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueComp5.Properties)).BeginInit();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemarkGrd4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeEviGrd4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeIndGrd4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCompGrd4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd4)).BeginInit();
            this.panel11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueLevel_4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueComp4.Properties)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemarkGrd3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeEviGrd3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeIndGrd3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCompGrd3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).BeginInit();
            this.panel10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueLevel_3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueComp3.Properties)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemarkGrd2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeEviGrd2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeIndGrd2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCompGrd2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).BeginInit();
            this.panel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueLevel_2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueComp2.Properties)).BeginInit();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemarkGrd1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeEviGrd1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeIndGrd1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCompGrd1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            this.panel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueLevel_1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueComp1.Properties)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemarkGrd16.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeEviGrd16.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeIndGrd16.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCompGrd16.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd16)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueLevel_16.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueComp16.Properties)).BeginInit();
            this.tabPage17.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemarkGrd17.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeEviGrd17.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeIndGrd17.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCompGrd17.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd17)).BeginInit();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueLevel_17.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueComp17.Properties)).BeginInit();
            this.tabPage18.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemarkGrd18.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeEviGrd18.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeIndGrd18.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCompGrd18.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd18)).BeginInit();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueLevel_18.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueComp18.Properties)).BeginInit();
            this.tabPage19.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemarkGrd19.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeEviGrd19.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeIndGrd19.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCompGrd19.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd19)).BeginInit();
            this.panel22.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueLevel_19.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueComp19.Properties)).BeginInit();
            this.tabPage20.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemarkGrd20.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeEviGrd20.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeIndGrd20.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCompGrd20.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd20)).BeginInit();
            this.panel23.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueLevel_20.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueComp20.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(847, 0);
            this.panel1.Size = new System.Drawing.Size(70, 461);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.tabControl1);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Size = new System.Drawing.Size(847, 461);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel3.Controls.Add(this.ChkActInd);
            this.panel3.Controls.Add(this.BtnImport);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Controls.Add(this.LueCompetenceLevelType);
            this.panel3.Controls.Add(this.label30);
            this.panel3.Controls.Add(this.LueCompLevelName);
            this.panel3.Controls.Add(this.TxtAsmName);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.DteDocDt);
            this.panel3.Controls.Add(this.label17);
            this.panel3.Controls.Add(this.TxtAsmCode);
            this.panel3.Controls.Add(this.label18);
            this.panel3.Controls.Add(this.MeeRemark);
            this.panel3.Controls.Add(this.label5);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(847, 169);
            this.panel3.TabIndex = 11;
            // 
            // ChkActInd
            // 
            this.ChkActInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkActInd.Location = new System.Drawing.Point(302, 10);
            this.ChkActInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkActInd.Name = "ChkActInd";
            this.ChkActInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkActInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkActInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkActInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkActInd.Properties.Appearance.Options.UseFont = true;
            this.ChkActInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkActInd.Properties.Caption = "Active";
            this.ChkActInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkActInd.Size = new System.Drawing.Size(67, 22);
            this.ChkActInd.TabIndex = 14;
            // 
            // BtnImport
            // 
            this.BtnImport.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnImport.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnImport.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnImport.Appearance.ForeColor = System.Drawing.Color.Green;
            this.BtnImport.Appearance.Options.UseBackColor = true;
            this.BtnImport.Appearance.Options.UseFont = true;
            this.BtnImport.Appearance.Options.UseForeColor = true;
            this.BtnImport.Appearance.Options.UseTextOptions = true;
            this.BtnImport.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnImport.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnImport.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnImport.Location = new System.Drawing.Point(3, 142);
            this.BtnImport.Name = "BtnImport";
            this.BtnImport.Size = new System.Drawing.Size(86, 21);
            this.BtnImport.TabIndex = 25;
            this.BtnImport.Text = "Import Data";
            this.BtnImport.ToolTip = "Import Data";
            this.BtnImport.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnImport.ToolTipTitle = "Run System";
            this.BtnImport.Click += new System.EventHandler(this.BtnImport_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(27, 101);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(135, 14);
            this.label4.TabIndex = 21;
            this.label4.Text = "Potency / Competence";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueCompetenceLevelType
            // 
            this.LueCompetenceLevelType.EnterMoveNextControl = true;
            this.LueCompetenceLevelType.Location = new System.Drawing.Point(168, 98);
            this.LueCompetenceLevelType.Margin = new System.Windows.Forms.Padding(5);
            this.LueCompetenceLevelType.Name = "LueCompetenceLevelType";
            this.LueCompetenceLevelType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCompetenceLevelType.Properties.Appearance.Options.UseFont = true;
            this.LueCompetenceLevelType.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCompetenceLevelType.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCompetenceLevelType.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCompetenceLevelType.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCompetenceLevelType.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCompetenceLevelType.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCompetenceLevelType.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCompetenceLevelType.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCompetenceLevelType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCompetenceLevelType.Properties.DropDownRows = 12;
            this.LueCompetenceLevelType.Properties.NullText = "[Empty]";
            this.LueCompetenceLevelType.Properties.PopupWidth = 500;
            this.LueCompetenceLevelType.Size = new System.Drawing.Size(249, 20);
            this.LueCompetenceLevelType.TabIndex = 22;
            this.LueCompetenceLevelType.ToolTip = "F4 : Show/hide list";
            this.LueCompetenceLevelType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCompetenceLevelType.EditValueChanged += new System.EventHandler(this.LueCompetenceLevelType_EditValueChanged);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.Red;
            this.label30.Location = new System.Drawing.Point(18, 78);
            this.label30.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(144, 14);
            this.label30.TabIndex = 19;
            this.label30.Text = "Competence Level Name";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueCompLevelName
            // 
            this.LueCompLevelName.EnterMoveNextControl = true;
            this.LueCompLevelName.Location = new System.Drawing.Point(168, 76);
            this.LueCompLevelName.Margin = new System.Windows.Forms.Padding(5);
            this.LueCompLevelName.Name = "LueCompLevelName";
            this.LueCompLevelName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCompLevelName.Properties.Appearance.Options.UseFont = true;
            this.LueCompLevelName.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCompLevelName.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCompLevelName.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCompLevelName.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCompLevelName.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCompLevelName.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCompLevelName.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCompLevelName.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCompLevelName.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCompLevelName.Properties.DropDownRows = 12;
            this.LueCompLevelName.Properties.NullText = "[Empty]";
            this.LueCompLevelName.Properties.PopupWidth = 500;
            this.LueCompLevelName.Size = new System.Drawing.Size(249, 20);
            this.LueCompLevelName.TabIndex = 20;
            this.LueCompLevelName.ToolTip = "F4 : Show/hide list";
            this.LueCompLevelName.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCompLevelName.EditValueChanged += new System.EventHandler(this.LueCompLevelName_EditValueChanged);
            // 
            // TxtAsmName
            // 
            this.TxtAsmName.EnterMoveNextControl = true;
            this.TxtAsmName.Location = new System.Drawing.Point(168, 32);
            this.TxtAsmName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAsmName.Name = "TxtAsmName";
            this.TxtAsmName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtAsmName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAsmName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAsmName.Properties.Appearance.Options.UseFont = true;
            this.TxtAsmName.Properties.MaxLength = 250;
            this.TxtAsmName.Size = new System.Drawing.Size(536, 20);
            this.TxtAsmName.TabIndex = 16;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(129, 55);
            this.label1.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 14);
            this.label1.TabIndex = 17;
            this.label1.Text = "Date";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(168, 54);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(126, 20);
            this.DteDocDt.TabIndex = 18;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Red;
            this.label17.Location = new System.Drawing.Point(61, 33);
            this.label17.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(101, 14);
            this.label17.TabIndex = 15;
            this.label17.Text = "Assesment Name";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtAsmCode
            // 
            this.TxtAsmCode.EnterMoveNextControl = true;
            this.TxtAsmCode.Location = new System.Drawing.Point(168, 10);
            this.TxtAsmCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAsmCode.Name = "TxtAsmCode";
            this.TxtAsmCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtAsmCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAsmCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAsmCode.Properties.Appearance.Options.UseFont = true;
            this.TxtAsmCode.Properties.MaxLength = 16;
            this.TxtAsmCode.Size = new System.Drawing.Size(125, 20);
            this.TxtAsmCode.TabIndex = 13;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Red;
            this.label18.Location = new System.Drawing.Point(73, 13);
            this.label18.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(89, 14);
            this.label18.TabIndex = 12;
            this.label18.Text = "Assesment No.";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(168, 120);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 400;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(536, 20);
            this.MeeRemark.TabIndex = 24;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(115, 122);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 14);
            this.label5.TabIndex = 23;
            this.label5.Text = "Remark";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage15
            // 
            this.tabPage15.Controls.Add(this.MeeRemarkGrd15);
            this.tabPage15.Controls.Add(this.MeeEviGrd15);
            this.tabPage15.Controls.Add(this.MeeIndGrd15);
            this.tabPage15.Controls.Add(this.MeeCompGrd15);
            this.tabPage15.Controls.Add(this.Grd15);
            this.tabPage15.Controls.Add(this.panel21);
            this.tabPage15.Location = new System.Drawing.Point(4, 23);
            this.tabPage15.Name = "tabPage15";
            this.tabPage15.Size = new System.Drawing.Size(764, 37);
            this.tabPage15.TabIndex = 14;
            this.tabPage15.Text = "No.15";
            this.tabPage15.UseVisualStyleBackColor = true;
            // 
            // MeeRemarkGrd15
            // 
            this.MeeRemarkGrd15.EnterMoveNextControl = true;
            this.MeeRemarkGrd15.Location = new System.Drawing.Point(594, 172);
            this.MeeRemarkGrd15.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemarkGrd15.Name = "MeeRemarkGrd15";
            this.MeeRemarkGrd15.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd15.Properties.Appearance.Options.UseFont = true;
            this.MeeRemarkGrd15.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd15.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemarkGrd15.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd15.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemarkGrd15.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd15.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemarkGrd15.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd15.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemarkGrd15.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemarkGrd15.Properties.MaxLength = 400;
            this.MeeRemarkGrd15.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeRemarkGrd15.Properties.ShowIcon = false;
            this.MeeRemarkGrd15.Size = new System.Drawing.Size(220, 20);
            this.MeeRemarkGrd15.TabIndex = 55;
            this.MeeRemarkGrd15.ToolTip = "F4 : Show/hide text";
            this.MeeRemarkGrd15.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemarkGrd15.ToolTipTitle = "Run System";
            this.MeeRemarkGrd15.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeRemarkGrd15_KeyDown);
            this.MeeRemarkGrd15.Leave += new System.EventHandler(this.MeeRemarkGrd15_Leave);
            // 
            // MeeEviGrd15
            // 
            this.MeeEviGrd15.EnterMoveNextControl = true;
            this.MeeEviGrd15.Location = new System.Drawing.Point(393, 170);
            this.MeeEviGrd15.Margin = new System.Windows.Forms.Padding(5);
            this.MeeEviGrd15.Name = "MeeEviGrd15";
            this.MeeEviGrd15.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd15.Properties.Appearance.Options.UseFont = true;
            this.MeeEviGrd15.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd15.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeEviGrd15.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd15.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeEviGrd15.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd15.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeEviGrd15.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd15.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeEviGrd15.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeEviGrd15.Properties.MaxLength = 400;
            this.MeeEviGrd15.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeEviGrd15.Properties.ShowIcon = false;
            this.MeeEviGrd15.Size = new System.Drawing.Size(180, 20);
            this.MeeEviGrd15.TabIndex = 54;
            this.MeeEviGrd15.ToolTip = "F4 : Show/hide text";
            this.MeeEviGrd15.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeEviGrd15.ToolTipTitle = "Run System";
            this.MeeEviGrd15.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeEviGrd15_KeyDown);
            this.MeeEviGrd15.Leave += new System.EventHandler(this.MeeEviGrd15_Leave);
            // 
            // MeeIndGrd15
            // 
            this.MeeIndGrd15.EnterMoveNextControl = true;
            this.MeeIndGrd15.Location = new System.Drawing.Point(225, 121);
            this.MeeIndGrd15.Margin = new System.Windows.Forms.Padding(5);
            this.MeeIndGrd15.Name = "MeeIndGrd15";
            this.MeeIndGrd15.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd15.Properties.Appearance.Options.UseFont = true;
            this.MeeIndGrd15.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd15.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeIndGrd15.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd15.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeIndGrd15.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd15.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeIndGrd15.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd15.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeIndGrd15.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeIndGrd15.Properties.MaxLength = 400;
            this.MeeIndGrd15.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeIndGrd15.Properties.ShowIcon = false;
            this.MeeIndGrd15.Size = new System.Drawing.Size(220, 20);
            this.MeeIndGrd15.TabIndex = 53;
            this.MeeIndGrd15.ToolTip = "F4 : Show/hide text";
            this.MeeIndGrd15.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeIndGrd15.ToolTipTitle = "Run System";
            this.MeeIndGrd15.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeIndGrd15_KeyDown);
            this.MeeIndGrd15.Leave += new System.EventHandler(this.MeeIndGrd15_Leave);
            // 
            // MeeCompGrd15
            // 
            this.MeeCompGrd15.EnterMoveNextControl = true;
            this.MeeCompGrd15.Location = new System.Drawing.Point(24, 119);
            this.MeeCompGrd15.Margin = new System.Windows.Forms.Padding(5);
            this.MeeCompGrd15.Name = "MeeCompGrd15";
            this.MeeCompGrd15.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd15.Properties.Appearance.Options.UseFont = true;
            this.MeeCompGrd15.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd15.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeCompGrd15.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd15.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeCompGrd15.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd15.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeCompGrd15.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd15.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeCompGrd15.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeCompGrd15.Properties.MaxLength = 400;
            this.MeeCompGrd15.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeCompGrd15.Properties.ShowIcon = false;
            this.MeeCompGrd15.Size = new System.Drawing.Size(180, 20);
            this.MeeCompGrd15.TabIndex = 52;
            this.MeeCompGrd15.ToolTip = "F4 : Show/hide text";
            this.MeeCompGrd15.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeCompGrd15.ToolTipTitle = "Run System";
            this.MeeCompGrd15.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeCompGrd15_KeyDown);
            this.MeeCompGrd15.Leave += new System.EventHandler(this.MeeCompGrd15_Leave);
            // 
            // Grd15
            // 
            this.Grd15.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd15.DefaultRow.Height = 20;
            this.Grd15.DefaultRow.Sortable = false;
            this.Grd15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd15.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd15.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd15.Header.Height = 21;
            this.Grd15.Location = new System.Drawing.Point(0, 33);
            this.Grd15.Name = "Grd15";
            this.Grd15.RowHeader.Visible = true;
            this.Grd15.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd15.SingleClickEdit = true;
            this.Grd15.Size = new System.Drawing.Size(764, 4);
            this.Grd15.TabIndex = 51;
            this.Grd15.TreeCol = null;
            this.Grd15.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd15.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd15_ColHdrDoubleClick);
            this.Grd15.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd15_RequestEdit);
            this.Grd15.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd15_KeyDown);
            // 
            // panel21
            // 
            this.panel21.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel21.Controls.Add(this.LueLevel_15);
            this.panel21.Controls.Add(this.label28);
            this.panel21.Controls.Add(this.LueComp15);
            this.panel21.Controls.Add(this.label40);
            this.panel21.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel21.Location = new System.Drawing.Point(0, 0);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(764, 33);
            this.panel21.TabIndex = 44;
            // 
            // LueLevel_15
            // 
            this.LueLevel_15.EnterMoveNextControl = true;
            this.LueLevel_15.Location = new System.Drawing.Point(468, 6);
            this.LueLevel_15.Margin = new System.Windows.Forms.Padding(5);
            this.LueLevel_15.Name = "LueLevel_15";
            this.LueLevel_15.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_15.Properties.Appearance.Options.UseFont = true;
            this.LueLevel_15.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_15.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueLevel_15.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_15.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueLevel_15.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_15.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueLevel_15.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_15.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueLevel_15.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueLevel_15.Properties.DropDownRows = 20;
            this.LueLevel_15.Properties.NullText = "[Empty]";
            this.LueLevel_15.Properties.PopupWidth = 500;
            this.LueLevel_15.Size = new System.Drawing.Size(84, 20);
            this.LueLevel_15.TabIndex = 55;
            this.LueLevel_15.ToolTip = "F4 : Show/hide list";
            this.LueLevel_15.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueLevel_15.EditValueChanged += new System.EventHandler(this.LueLevel15_EditValueChanged);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Red;
            this.label28.Location = new System.Drawing.Point(427, 9);
            this.label28.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(35, 14);
            this.label28.TabIndex = 54;
            this.label28.Text = "Level";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueComp15
            // 
            this.LueComp15.EnterMoveNextControl = true;
            this.LueComp15.Location = new System.Drawing.Point(90, 6);
            this.LueComp15.Margin = new System.Windows.Forms.Padding(5);
            this.LueComp15.Name = "LueComp15";
            this.LueComp15.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp15.Properties.Appearance.Options.UseFont = true;
            this.LueComp15.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp15.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueComp15.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp15.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueComp15.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp15.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueComp15.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp15.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueComp15.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueComp15.Properties.DropDownRows = 20;
            this.LueComp15.Properties.NullText = "[Empty]";
            this.LueComp15.Properties.PopupWidth = 500;
            this.LueComp15.Size = new System.Drawing.Size(330, 20);
            this.LueComp15.TabIndex = 46;
            this.LueComp15.ToolTip = "F4 : Show/hide list";
            this.LueComp15.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueComp15.EditValueChanged += new System.EventHandler(this.LueComp15_EditValueChanged);
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.ForeColor = System.Drawing.Color.Red;
            this.label40.Location = new System.Drawing.Point(8, 9);
            this.label40.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(77, 14);
            this.label40.TabIndex = 45;
            this.label40.Text = "Competence";
            this.label40.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage14
            // 
            this.tabPage14.Controls.Add(this.MeeRemarkGrd14);
            this.tabPage14.Controls.Add(this.MeeEviGrd14);
            this.tabPage14.Controls.Add(this.MeeIndGrd14);
            this.tabPage14.Controls.Add(this.MeeCompGrd14);
            this.tabPage14.Controls.Add(this.Grd14);
            this.tabPage14.Controls.Add(this.panel20);
            this.tabPage14.Location = new System.Drawing.Point(4, 23);
            this.tabPage14.Name = "tabPage14";
            this.tabPage14.Size = new System.Drawing.Size(764, 37);
            this.tabPage14.TabIndex = 13;
            this.tabPage14.Text = "No.14";
            this.tabPage14.UseVisualStyleBackColor = true;
            // 
            // MeeRemarkGrd14
            // 
            this.MeeRemarkGrd14.EnterMoveNextControl = true;
            this.MeeRemarkGrd14.Location = new System.Drawing.Point(594, 172);
            this.MeeRemarkGrd14.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemarkGrd14.Name = "MeeRemarkGrd14";
            this.MeeRemarkGrd14.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd14.Properties.Appearance.Options.UseFont = true;
            this.MeeRemarkGrd14.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd14.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemarkGrd14.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd14.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemarkGrd14.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd14.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemarkGrd14.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd14.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemarkGrd14.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemarkGrd14.Properties.MaxLength = 400;
            this.MeeRemarkGrd14.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeRemarkGrd14.Properties.ShowIcon = false;
            this.MeeRemarkGrd14.Size = new System.Drawing.Size(220, 20);
            this.MeeRemarkGrd14.TabIndex = 55;
            this.MeeRemarkGrd14.ToolTip = "F4 : Show/hide text";
            this.MeeRemarkGrd14.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemarkGrd14.ToolTipTitle = "Run System";
            this.MeeRemarkGrd14.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeRemarkGrd14_KeyDown);
            this.MeeRemarkGrd14.Leave += new System.EventHandler(this.MeeRemarkGrd14_Leave);
            // 
            // MeeEviGrd14
            // 
            this.MeeEviGrd14.EnterMoveNextControl = true;
            this.MeeEviGrd14.Location = new System.Drawing.Point(393, 170);
            this.MeeEviGrd14.Margin = new System.Windows.Forms.Padding(5);
            this.MeeEviGrd14.Name = "MeeEviGrd14";
            this.MeeEviGrd14.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd14.Properties.Appearance.Options.UseFont = true;
            this.MeeEviGrd14.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd14.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeEviGrd14.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd14.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeEviGrd14.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd14.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeEviGrd14.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd14.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeEviGrd14.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeEviGrd14.Properties.MaxLength = 400;
            this.MeeEviGrd14.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeEviGrd14.Properties.ShowIcon = false;
            this.MeeEviGrd14.Size = new System.Drawing.Size(180, 20);
            this.MeeEviGrd14.TabIndex = 54;
            this.MeeEviGrd14.ToolTip = "F4 : Show/hide text";
            this.MeeEviGrd14.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeEviGrd14.ToolTipTitle = "Run System";
            this.MeeEviGrd14.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeEviGrd14_KeyDown);
            this.MeeEviGrd14.Leave += new System.EventHandler(this.MeeEviGrd14_Leave);
            // 
            // MeeIndGrd14
            // 
            this.MeeIndGrd14.EnterMoveNextControl = true;
            this.MeeIndGrd14.Location = new System.Drawing.Point(225, 121);
            this.MeeIndGrd14.Margin = new System.Windows.Forms.Padding(5);
            this.MeeIndGrd14.Name = "MeeIndGrd14";
            this.MeeIndGrd14.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd14.Properties.Appearance.Options.UseFont = true;
            this.MeeIndGrd14.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd14.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeIndGrd14.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd14.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeIndGrd14.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd14.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeIndGrd14.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd14.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeIndGrd14.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeIndGrd14.Properties.MaxLength = 400;
            this.MeeIndGrd14.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeIndGrd14.Properties.ShowIcon = false;
            this.MeeIndGrd14.Size = new System.Drawing.Size(220, 20);
            this.MeeIndGrd14.TabIndex = 53;
            this.MeeIndGrd14.ToolTip = "F4 : Show/hide text";
            this.MeeIndGrd14.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeIndGrd14.ToolTipTitle = "Run System";
            this.MeeIndGrd14.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeIndGrd14_KeyDown);
            this.MeeIndGrd14.Leave += new System.EventHandler(this.MeeIndGrd14_Leave);
            // 
            // MeeCompGrd14
            // 
            this.MeeCompGrd14.EnterMoveNextControl = true;
            this.MeeCompGrd14.Location = new System.Drawing.Point(24, 119);
            this.MeeCompGrd14.Margin = new System.Windows.Forms.Padding(5);
            this.MeeCompGrd14.Name = "MeeCompGrd14";
            this.MeeCompGrd14.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd14.Properties.Appearance.Options.UseFont = true;
            this.MeeCompGrd14.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd14.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeCompGrd14.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd14.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeCompGrd14.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd14.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeCompGrd14.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd14.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeCompGrd14.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeCompGrd14.Properties.MaxLength = 400;
            this.MeeCompGrd14.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeCompGrd14.Properties.ShowIcon = false;
            this.MeeCompGrd14.Size = new System.Drawing.Size(180, 20);
            this.MeeCompGrd14.TabIndex = 52;
            this.MeeCompGrd14.ToolTip = "F4 : Show/hide text";
            this.MeeCompGrd14.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeCompGrd14.ToolTipTitle = "Run System";
            this.MeeCompGrd14.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeCompGrd14_KeyDown);
            this.MeeCompGrd14.Leave += new System.EventHandler(this.MeeCompGrd14_Leave);
            // 
            // Grd14
            // 
            this.Grd14.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd14.DefaultRow.Height = 20;
            this.Grd14.DefaultRow.Sortable = false;
            this.Grd14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd14.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd14.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd14.Header.Height = 21;
            this.Grd14.Location = new System.Drawing.Point(0, 33);
            this.Grd14.Name = "Grd14";
            this.Grd14.RowHeader.Visible = true;
            this.Grd14.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd14.SingleClickEdit = true;
            this.Grd14.Size = new System.Drawing.Size(764, 4);
            this.Grd14.TabIndex = 51;
            this.Grd14.TreeCol = null;
            this.Grd14.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd14.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd14_ColHdrDoubleClick);
            this.Grd14.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd14_RequestEdit);
            this.Grd14.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd14_KeyDown);
            // 
            // panel20
            // 
            this.panel20.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel20.Controls.Add(this.LueLevel_14);
            this.panel20.Controls.Add(this.label26);
            this.panel20.Controls.Add(this.LueComp14);
            this.panel20.Controls.Add(this.label38);
            this.panel20.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel20.Location = new System.Drawing.Point(0, 0);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(764, 33);
            this.panel20.TabIndex = 44;
            // 
            // LueLevel_14
            // 
            this.LueLevel_14.EnterMoveNextControl = true;
            this.LueLevel_14.Location = new System.Drawing.Point(472, 6);
            this.LueLevel_14.Margin = new System.Windows.Forms.Padding(5);
            this.LueLevel_14.Name = "LueLevel_14";
            this.LueLevel_14.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_14.Properties.Appearance.Options.UseFont = true;
            this.LueLevel_14.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_14.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueLevel_14.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_14.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueLevel_14.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_14.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueLevel_14.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_14.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueLevel_14.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueLevel_14.Properties.DropDownRows = 20;
            this.LueLevel_14.Properties.NullText = "[Empty]";
            this.LueLevel_14.Properties.PopupWidth = 500;
            this.LueLevel_14.Size = new System.Drawing.Size(84, 20);
            this.LueLevel_14.TabIndex = 55;
            this.LueLevel_14.ToolTip = "F4 : Show/hide list";
            this.LueLevel_14.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueLevel_14.EditValueChanged += new System.EventHandler(this.LueLevel14_EditValueChanged);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Red;
            this.label26.Location = new System.Drawing.Point(431, 9);
            this.label26.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(35, 14);
            this.label26.TabIndex = 54;
            this.label26.Text = "Level";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueComp14
            // 
            this.LueComp14.EnterMoveNextControl = true;
            this.LueComp14.Location = new System.Drawing.Point(96, 6);
            this.LueComp14.Margin = new System.Windows.Forms.Padding(5);
            this.LueComp14.Name = "LueComp14";
            this.LueComp14.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp14.Properties.Appearance.Options.UseFont = true;
            this.LueComp14.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp14.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueComp14.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp14.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueComp14.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp14.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueComp14.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp14.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueComp14.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueComp14.Properties.DropDownRows = 20;
            this.LueComp14.Properties.NullText = "[Empty]";
            this.LueComp14.Properties.PopupWidth = 500;
            this.LueComp14.Size = new System.Drawing.Size(330, 20);
            this.LueComp14.TabIndex = 46;
            this.LueComp14.ToolTip = "F4 : Show/hide list";
            this.LueComp14.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueComp14.EditValueChanged += new System.EventHandler(this.LueComp14_EditValueChanged);
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.ForeColor = System.Drawing.Color.Red;
            this.label38.Location = new System.Drawing.Point(8, 9);
            this.label38.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(77, 14);
            this.label38.TabIndex = 45;
            this.label38.Text = "Competence";
            this.label38.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage13
            // 
            this.tabPage13.Controls.Add(this.MeeRemarkGrd13);
            this.tabPage13.Controls.Add(this.MeeEviGrd13);
            this.tabPage13.Controls.Add(this.MeeIndGrd13);
            this.tabPage13.Controls.Add(this.MeeCompGrd13);
            this.tabPage13.Controls.Add(this.Grd13);
            this.tabPage13.Controls.Add(this.panel19);
            this.tabPage13.Location = new System.Drawing.Point(4, 23);
            this.tabPage13.Name = "tabPage13";
            this.tabPage13.Size = new System.Drawing.Size(764, 37);
            this.tabPage13.TabIndex = 12;
            this.tabPage13.Text = "No.13";
            this.tabPage13.UseVisualStyleBackColor = true;
            // 
            // MeeRemarkGrd13
            // 
            this.MeeRemarkGrd13.EnterMoveNextControl = true;
            this.MeeRemarkGrd13.Location = new System.Drawing.Point(594, 172);
            this.MeeRemarkGrd13.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemarkGrd13.Name = "MeeRemarkGrd13";
            this.MeeRemarkGrd13.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd13.Properties.Appearance.Options.UseFont = true;
            this.MeeRemarkGrd13.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd13.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemarkGrd13.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd13.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemarkGrd13.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd13.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemarkGrd13.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd13.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemarkGrd13.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemarkGrd13.Properties.MaxLength = 400;
            this.MeeRemarkGrd13.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeRemarkGrd13.Properties.ShowIcon = false;
            this.MeeRemarkGrd13.Size = new System.Drawing.Size(220, 20);
            this.MeeRemarkGrd13.TabIndex = 55;
            this.MeeRemarkGrd13.ToolTip = "F4 : Show/hide text";
            this.MeeRemarkGrd13.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemarkGrd13.ToolTipTitle = "Run System";
            this.MeeRemarkGrd13.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeRemarkGrd13_KeyDown);
            this.MeeRemarkGrd13.Leave += new System.EventHandler(this.MeeRemarkGrd13_Leave);
            // 
            // MeeEviGrd13
            // 
            this.MeeEviGrd13.EnterMoveNextControl = true;
            this.MeeEviGrd13.Location = new System.Drawing.Point(393, 170);
            this.MeeEviGrd13.Margin = new System.Windows.Forms.Padding(5);
            this.MeeEviGrd13.Name = "MeeEviGrd13";
            this.MeeEviGrd13.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd13.Properties.Appearance.Options.UseFont = true;
            this.MeeEviGrd13.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd13.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeEviGrd13.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd13.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeEviGrd13.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd13.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeEviGrd13.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd13.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeEviGrd13.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeEviGrd13.Properties.MaxLength = 400;
            this.MeeEviGrd13.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeEviGrd13.Properties.ShowIcon = false;
            this.MeeEviGrd13.Size = new System.Drawing.Size(180, 20);
            this.MeeEviGrd13.TabIndex = 54;
            this.MeeEviGrd13.ToolTip = "F4 : Show/hide text";
            this.MeeEviGrd13.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeEviGrd13.ToolTipTitle = "Run System";
            this.MeeEviGrd13.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeEviGrd13_KeyDown);
            this.MeeEviGrd13.Leave += new System.EventHandler(this.MeeEviGrd13_Leave);
            // 
            // MeeIndGrd13
            // 
            this.MeeIndGrd13.EnterMoveNextControl = true;
            this.MeeIndGrd13.Location = new System.Drawing.Point(225, 121);
            this.MeeIndGrd13.Margin = new System.Windows.Forms.Padding(5);
            this.MeeIndGrd13.Name = "MeeIndGrd13";
            this.MeeIndGrd13.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd13.Properties.Appearance.Options.UseFont = true;
            this.MeeIndGrd13.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd13.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeIndGrd13.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd13.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeIndGrd13.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd13.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeIndGrd13.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd13.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeIndGrd13.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeIndGrd13.Properties.MaxLength = 400;
            this.MeeIndGrd13.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeIndGrd13.Properties.ShowIcon = false;
            this.MeeIndGrd13.Size = new System.Drawing.Size(220, 20);
            this.MeeIndGrd13.TabIndex = 53;
            this.MeeIndGrd13.ToolTip = "F4 : Show/hide text";
            this.MeeIndGrd13.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeIndGrd13.ToolTipTitle = "Run System";
            this.MeeIndGrd13.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeIndGrd13_KeyDown);
            this.MeeIndGrd13.Leave += new System.EventHandler(this.MeeIndGrd13_Leave);
            // 
            // MeeCompGrd13
            // 
            this.MeeCompGrd13.EnterMoveNextControl = true;
            this.MeeCompGrd13.Location = new System.Drawing.Point(24, 119);
            this.MeeCompGrd13.Margin = new System.Windows.Forms.Padding(5);
            this.MeeCompGrd13.Name = "MeeCompGrd13";
            this.MeeCompGrd13.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd13.Properties.Appearance.Options.UseFont = true;
            this.MeeCompGrd13.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd13.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeCompGrd13.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd13.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeCompGrd13.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd13.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeCompGrd13.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd13.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeCompGrd13.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeCompGrd13.Properties.MaxLength = 400;
            this.MeeCompGrd13.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeCompGrd13.Properties.ShowIcon = false;
            this.MeeCompGrd13.Size = new System.Drawing.Size(180, 20);
            this.MeeCompGrd13.TabIndex = 52;
            this.MeeCompGrd13.ToolTip = "F4 : Show/hide text";
            this.MeeCompGrd13.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeCompGrd13.ToolTipTitle = "Run System";
            this.MeeCompGrd13.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeCompGrd13_KeyDown);
            this.MeeCompGrd13.Leave += new System.EventHandler(this.MeeCompGrd13_Leave);
            // 
            // Grd13
            // 
            this.Grd13.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd13.DefaultRow.Height = 20;
            this.Grd13.DefaultRow.Sortable = false;
            this.Grd13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd13.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd13.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd13.Header.Height = 21;
            this.Grd13.Location = new System.Drawing.Point(0, 33);
            this.Grd13.Name = "Grd13";
            this.Grd13.RowHeader.Visible = true;
            this.Grd13.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd13.SingleClickEdit = true;
            this.Grd13.Size = new System.Drawing.Size(764, 4);
            this.Grd13.TabIndex = 51;
            this.Grd13.TreeCol = null;
            this.Grd13.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd13.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd13_ColHdrDoubleClick);
            this.Grd13.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd13_RequestEdit);
            this.Grd13.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd13_KeyDown);
            // 
            // panel19
            // 
            this.panel19.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel19.Controls.Add(this.LueLevel_13);
            this.panel19.Controls.Add(this.label24);
            this.panel19.Controls.Add(this.LueComp13);
            this.panel19.Controls.Add(this.label36);
            this.panel19.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel19.Location = new System.Drawing.Point(0, 0);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(764, 33);
            this.panel19.TabIndex = 44;
            // 
            // LueLevel_13
            // 
            this.LueLevel_13.EnterMoveNextControl = true;
            this.LueLevel_13.Location = new System.Drawing.Point(470, 6);
            this.LueLevel_13.Margin = new System.Windows.Forms.Padding(5);
            this.LueLevel_13.Name = "LueLevel_13";
            this.LueLevel_13.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_13.Properties.Appearance.Options.UseFont = true;
            this.LueLevel_13.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_13.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueLevel_13.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_13.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueLevel_13.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_13.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueLevel_13.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_13.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueLevel_13.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueLevel_13.Properties.DropDownRows = 20;
            this.LueLevel_13.Properties.NullText = "[Empty]";
            this.LueLevel_13.Properties.PopupWidth = 500;
            this.LueLevel_13.Size = new System.Drawing.Size(84, 20);
            this.LueLevel_13.TabIndex = 55;
            this.LueLevel_13.ToolTip = "F4 : Show/hide list";
            this.LueLevel_13.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueLevel_13.EditValueChanged += new System.EventHandler(this.LueLevel13_EditValueChanged);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Red;
            this.label24.Location = new System.Drawing.Point(429, 9);
            this.label24.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(35, 14);
            this.label24.TabIndex = 54;
            this.label24.Text = "Level";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueComp13
            // 
            this.LueComp13.EnterMoveNextControl = true;
            this.LueComp13.Location = new System.Drawing.Point(91, 6);
            this.LueComp13.Margin = new System.Windows.Forms.Padding(5);
            this.LueComp13.Name = "LueComp13";
            this.LueComp13.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp13.Properties.Appearance.Options.UseFont = true;
            this.LueComp13.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp13.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueComp13.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp13.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueComp13.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp13.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueComp13.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp13.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueComp13.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueComp13.Properties.DropDownRows = 20;
            this.LueComp13.Properties.NullText = "[Empty]";
            this.LueComp13.Properties.PopupWidth = 500;
            this.LueComp13.Size = new System.Drawing.Size(330, 20);
            this.LueComp13.TabIndex = 46;
            this.LueComp13.ToolTip = "F4 : Show/hide list";
            this.LueComp13.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueComp13.EditValueChanged += new System.EventHandler(this.LueComp13_EditValueChanged);
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.ForeColor = System.Drawing.Color.Red;
            this.label36.Location = new System.Drawing.Point(8, 9);
            this.label36.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(77, 14);
            this.label36.TabIndex = 45;
            this.label36.Text = "Competence";
            this.label36.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage12
            // 
            this.tabPage12.Controls.Add(this.MeeRemarkGrd12);
            this.tabPage12.Controls.Add(this.MeeEviGrd12);
            this.tabPage12.Controls.Add(this.MeeIndGrd12);
            this.tabPage12.Controls.Add(this.MeeCompGrd12);
            this.tabPage12.Controls.Add(this.Grd12);
            this.tabPage12.Controls.Add(this.panel18);
            this.tabPage12.Location = new System.Drawing.Point(4, 23);
            this.tabPage12.Name = "tabPage12";
            this.tabPage12.Size = new System.Drawing.Size(764, 37);
            this.tabPage12.TabIndex = 11;
            this.tabPage12.Text = "No.12";
            this.tabPage12.UseVisualStyleBackColor = true;
            // 
            // MeeRemarkGrd12
            // 
            this.MeeRemarkGrd12.EnterMoveNextControl = true;
            this.MeeRemarkGrd12.Location = new System.Drawing.Point(594, 172);
            this.MeeRemarkGrd12.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemarkGrd12.Name = "MeeRemarkGrd12";
            this.MeeRemarkGrd12.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd12.Properties.Appearance.Options.UseFont = true;
            this.MeeRemarkGrd12.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd12.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemarkGrd12.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd12.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemarkGrd12.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd12.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemarkGrd12.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd12.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemarkGrd12.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemarkGrd12.Properties.MaxLength = 400;
            this.MeeRemarkGrd12.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeRemarkGrd12.Properties.ShowIcon = false;
            this.MeeRemarkGrd12.Size = new System.Drawing.Size(220, 20);
            this.MeeRemarkGrd12.TabIndex = 55;
            this.MeeRemarkGrd12.ToolTip = "F4 : Show/hide text";
            this.MeeRemarkGrd12.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemarkGrd12.ToolTipTitle = "Run System";
            this.MeeRemarkGrd12.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeRemarkGrd12_KeyDown);
            this.MeeRemarkGrd12.Leave += new System.EventHandler(this.MeeRemarkGrd12_Leave);
            // 
            // MeeEviGrd12
            // 
            this.MeeEviGrd12.EnterMoveNextControl = true;
            this.MeeEviGrd12.Location = new System.Drawing.Point(393, 170);
            this.MeeEviGrd12.Margin = new System.Windows.Forms.Padding(5);
            this.MeeEviGrd12.Name = "MeeEviGrd12";
            this.MeeEviGrd12.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd12.Properties.Appearance.Options.UseFont = true;
            this.MeeEviGrd12.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd12.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeEviGrd12.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd12.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeEviGrd12.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd12.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeEviGrd12.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd12.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeEviGrd12.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeEviGrd12.Properties.MaxLength = 400;
            this.MeeEviGrd12.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeEviGrd12.Properties.ShowIcon = false;
            this.MeeEviGrd12.Size = new System.Drawing.Size(180, 20);
            this.MeeEviGrd12.TabIndex = 54;
            this.MeeEviGrd12.ToolTip = "F4 : Show/hide text";
            this.MeeEviGrd12.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeEviGrd12.ToolTipTitle = "Run System";
            this.MeeEviGrd12.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeEviGrd12_KeyDown);
            this.MeeEviGrd12.Leave += new System.EventHandler(this.MeeEviGrd12_Leave);
            // 
            // MeeIndGrd12
            // 
            this.MeeIndGrd12.EnterMoveNextControl = true;
            this.MeeIndGrd12.Location = new System.Drawing.Point(225, 121);
            this.MeeIndGrd12.Margin = new System.Windows.Forms.Padding(5);
            this.MeeIndGrd12.Name = "MeeIndGrd12";
            this.MeeIndGrd12.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd12.Properties.Appearance.Options.UseFont = true;
            this.MeeIndGrd12.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd12.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeIndGrd12.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd12.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeIndGrd12.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd12.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeIndGrd12.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd12.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeIndGrd12.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeIndGrd12.Properties.MaxLength = 400;
            this.MeeIndGrd12.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeIndGrd12.Properties.ShowIcon = false;
            this.MeeIndGrd12.Size = new System.Drawing.Size(220, 20);
            this.MeeIndGrd12.TabIndex = 53;
            this.MeeIndGrd12.ToolTip = "F4 : Show/hide text";
            this.MeeIndGrd12.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeIndGrd12.ToolTipTitle = "Run System";
            this.MeeIndGrd12.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeIndGrd12_KeyDown);
            this.MeeIndGrd12.Leave += new System.EventHandler(this.MeeIndGrd12_Leave);
            // 
            // MeeCompGrd12
            // 
            this.MeeCompGrd12.EnterMoveNextControl = true;
            this.MeeCompGrd12.Location = new System.Drawing.Point(24, 119);
            this.MeeCompGrd12.Margin = new System.Windows.Forms.Padding(5);
            this.MeeCompGrd12.Name = "MeeCompGrd12";
            this.MeeCompGrd12.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd12.Properties.Appearance.Options.UseFont = true;
            this.MeeCompGrd12.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd12.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeCompGrd12.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd12.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeCompGrd12.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd12.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeCompGrd12.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd12.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeCompGrd12.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeCompGrd12.Properties.MaxLength = 400;
            this.MeeCompGrd12.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeCompGrd12.Properties.ShowIcon = false;
            this.MeeCompGrd12.Size = new System.Drawing.Size(180, 20);
            this.MeeCompGrd12.TabIndex = 52;
            this.MeeCompGrd12.ToolTip = "F4 : Show/hide text";
            this.MeeCompGrd12.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeCompGrd12.ToolTipTitle = "Run System";
            this.MeeCompGrd12.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeCompGrd12_KeyDown);
            this.MeeCompGrd12.Leave += new System.EventHandler(this.MeeCompGrd12_Leave);
            // 
            // Grd12
            // 
            this.Grd12.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd12.DefaultRow.Height = 20;
            this.Grd12.DefaultRow.Sortable = false;
            this.Grd12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd12.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd12.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd12.Header.Height = 21;
            this.Grd12.Location = new System.Drawing.Point(0, 33);
            this.Grd12.Name = "Grd12";
            this.Grd12.RowHeader.Visible = true;
            this.Grd12.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd12.SingleClickEdit = true;
            this.Grd12.Size = new System.Drawing.Size(764, 4);
            this.Grd12.TabIndex = 51;
            this.Grd12.TreeCol = null;
            this.Grd12.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd12.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd12_ColHdrDoubleClick);
            this.Grd12.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd12_RequestEdit);
            this.Grd12.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd12_KeyDown);
            // 
            // panel18
            // 
            this.panel18.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel18.Controls.Add(this.LueLevel_12);
            this.panel18.Controls.Add(this.label22);
            this.panel18.Controls.Add(this.LueComp12);
            this.panel18.Controls.Add(this.label34);
            this.panel18.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel18.Location = new System.Drawing.Point(0, 0);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(764, 33);
            this.panel18.TabIndex = 44;
            // 
            // LueLevel_12
            // 
            this.LueLevel_12.EnterMoveNextControl = true;
            this.LueLevel_12.Location = new System.Drawing.Point(466, 6);
            this.LueLevel_12.Margin = new System.Windows.Forms.Padding(5);
            this.LueLevel_12.Name = "LueLevel_12";
            this.LueLevel_12.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_12.Properties.Appearance.Options.UseFont = true;
            this.LueLevel_12.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_12.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueLevel_12.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_12.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueLevel_12.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_12.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueLevel_12.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_12.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueLevel_12.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueLevel_12.Properties.DropDownRows = 20;
            this.LueLevel_12.Properties.NullText = "[Empty]";
            this.LueLevel_12.Properties.PopupWidth = 500;
            this.LueLevel_12.Size = new System.Drawing.Size(84, 20);
            this.LueLevel_12.TabIndex = 55;
            this.LueLevel_12.ToolTip = "F4 : Show/hide list";
            this.LueLevel_12.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueLevel_12.EditValueChanged += new System.EventHandler(this.LueLevel12_EditValueChanged);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Red;
            this.label22.Location = new System.Drawing.Point(425, 9);
            this.label22.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(35, 14);
            this.label22.TabIndex = 54;
            this.label22.Text = "Level";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueComp12
            // 
            this.LueComp12.EnterMoveNextControl = true;
            this.LueComp12.Location = new System.Drawing.Point(90, 6);
            this.LueComp12.Margin = new System.Windows.Forms.Padding(5);
            this.LueComp12.Name = "LueComp12";
            this.LueComp12.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp12.Properties.Appearance.Options.UseFont = true;
            this.LueComp12.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp12.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueComp12.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp12.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueComp12.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp12.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueComp12.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp12.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueComp12.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueComp12.Properties.DropDownRows = 20;
            this.LueComp12.Properties.NullText = "[Empty]";
            this.LueComp12.Properties.PopupWidth = 500;
            this.LueComp12.Size = new System.Drawing.Size(330, 20);
            this.LueComp12.TabIndex = 46;
            this.LueComp12.ToolTip = "F4 : Show/hide list";
            this.LueComp12.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueComp12.EditValueChanged += new System.EventHandler(this.LueComp12_EditValueChanged);
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.Red;
            this.label34.Location = new System.Drawing.Point(8, 9);
            this.label34.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(77, 14);
            this.label34.TabIndex = 45;
            this.label34.Text = "Competence";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage11
            // 
            this.tabPage11.Controls.Add(this.MeeRemarkGrd11);
            this.tabPage11.Controls.Add(this.MeeEviGrd11);
            this.tabPage11.Controls.Add(this.MeeIndGrd11);
            this.tabPage11.Controls.Add(this.MeeCompGrd11);
            this.tabPage11.Controls.Add(this.Grd11);
            this.tabPage11.Controls.Add(this.panel5);
            this.tabPage11.Location = new System.Drawing.Point(4, 23);
            this.tabPage11.Name = "tabPage11";
            this.tabPage11.Size = new System.Drawing.Size(764, 37);
            this.tabPage11.TabIndex = 10;
            this.tabPage11.Text = "No.11";
            this.tabPage11.UseVisualStyleBackColor = true;
            // 
            // MeeRemarkGrd11
            // 
            this.MeeRemarkGrd11.EnterMoveNextControl = true;
            this.MeeRemarkGrd11.Location = new System.Drawing.Point(594, 172);
            this.MeeRemarkGrd11.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemarkGrd11.Name = "MeeRemarkGrd11";
            this.MeeRemarkGrd11.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd11.Properties.Appearance.Options.UseFont = true;
            this.MeeRemarkGrd11.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd11.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemarkGrd11.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd11.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemarkGrd11.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd11.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemarkGrd11.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd11.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemarkGrd11.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemarkGrd11.Properties.MaxLength = 400;
            this.MeeRemarkGrd11.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeRemarkGrd11.Properties.ShowIcon = false;
            this.MeeRemarkGrd11.Size = new System.Drawing.Size(220, 20);
            this.MeeRemarkGrd11.TabIndex = 55;
            this.MeeRemarkGrd11.ToolTip = "F4 : Show/hide text";
            this.MeeRemarkGrd11.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemarkGrd11.ToolTipTitle = "Run System";
            this.MeeRemarkGrd11.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeRemarkGrd11_KeyDown);
            this.MeeRemarkGrd11.Leave += new System.EventHandler(this.MeeRemarkGrd11_Leave);
            // 
            // MeeEviGrd11
            // 
            this.MeeEviGrd11.EnterMoveNextControl = true;
            this.MeeEviGrd11.Location = new System.Drawing.Point(393, 170);
            this.MeeEviGrd11.Margin = new System.Windows.Forms.Padding(5);
            this.MeeEviGrd11.Name = "MeeEviGrd11";
            this.MeeEviGrd11.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd11.Properties.Appearance.Options.UseFont = true;
            this.MeeEviGrd11.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd11.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeEviGrd11.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd11.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeEviGrd11.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd11.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeEviGrd11.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd11.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeEviGrd11.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeEviGrd11.Properties.MaxLength = 400;
            this.MeeEviGrd11.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeEviGrd11.Properties.ShowIcon = false;
            this.MeeEviGrd11.Size = new System.Drawing.Size(180, 20);
            this.MeeEviGrd11.TabIndex = 54;
            this.MeeEviGrd11.ToolTip = "F4 : Show/hide text";
            this.MeeEviGrd11.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeEviGrd11.ToolTipTitle = "Run System";
            this.MeeEviGrd11.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeEviGrd11_KeyDown);
            this.MeeEviGrd11.Leave += new System.EventHandler(this.MeeEviGrd11_Leave);
            // 
            // MeeIndGrd11
            // 
            this.MeeIndGrd11.EnterMoveNextControl = true;
            this.MeeIndGrd11.Location = new System.Drawing.Point(225, 121);
            this.MeeIndGrd11.Margin = new System.Windows.Forms.Padding(5);
            this.MeeIndGrd11.Name = "MeeIndGrd11";
            this.MeeIndGrd11.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd11.Properties.Appearance.Options.UseFont = true;
            this.MeeIndGrd11.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd11.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeIndGrd11.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd11.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeIndGrd11.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd11.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeIndGrd11.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd11.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeIndGrd11.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeIndGrd11.Properties.MaxLength = 400;
            this.MeeIndGrd11.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeIndGrd11.Properties.ShowIcon = false;
            this.MeeIndGrd11.Size = new System.Drawing.Size(220, 20);
            this.MeeIndGrd11.TabIndex = 53;
            this.MeeIndGrd11.ToolTip = "F4 : Show/hide text";
            this.MeeIndGrd11.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeIndGrd11.ToolTipTitle = "Run System";
            this.MeeIndGrd11.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeIndGrd11_KeyDown);
            this.MeeIndGrd11.Leave += new System.EventHandler(this.MeeIndGrd11_Leave);
            // 
            // MeeCompGrd11
            // 
            this.MeeCompGrd11.EnterMoveNextControl = true;
            this.MeeCompGrd11.Location = new System.Drawing.Point(24, 119);
            this.MeeCompGrd11.Margin = new System.Windows.Forms.Padding(5);
            this.MeeCompGrd11.Name = "MeeCompGrd11";
            this.MeeCompGrd11.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd11.Properties.Appearance.Options.UseFont = true;
            this.MeeCompGrd11.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd11.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeCompGrd11.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd11.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeCompGrd11.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd11.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeCompGrd11.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd11.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeCompGrd11.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeCompGrd11.Properties.MaxLength = 400;
            this.MeeCompGrd11.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeCompGrd11.Properties.ShowIcon = false;
            this.MeeCompGrd11.Size = new System.Drawing.Size(180, 20);
            this.MeeCompGrd11.TabIndex = 52;
            this.MeeCompGrd11.ToolTip = "F4 : Show/hide text";
            this.MeeCompGrd11.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeCompGrd11.ToolTipTitle = "Run System";
            this.MeeCompGrd11.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeCompGrd11_KeyDown);
            this.MeeCompGrd11.Leave += new System.EventHandler(this.MeeCompGrd11_Leave);
            // 
            // Grd11
            // 
            this.Grd11.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd11.DefaultRow.Height = 20;
            this.Grd11.DefaultRow.Sortable = false;
            this.Grd11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd11.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd11.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd11.Header.Height = 21;
            this.Grd11.Location = new System.Drawing.Point(0, 33);
            this.Grd11.Name = "Grd11";
            this.Grd11.RowHeader.Visible = true;
            this.Grd11.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd11.SingleClickEdit = true;
            this.Grd11.Size = new System.Drawing.Size(764, 4);
            this.Grd11.TabIndex = 51;
            this.Grd11.TreeCol = null;
            this.Grd11.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd11.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd11_ColHdrDoubleClick);
            this.Grd11.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd11_RequestEdit);
            this.Grd11.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd11_KeyDown);
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel5.Controls.Add(this.LueLevel_11);
            this.panel5.Controls.Add(this.label20);
            this.panel5.Controls.Add(this.LueComp11);
            this.panel5.Controls.Add(this.label32);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(764, 33);
            this.panel5.TabIndex = 44;
            // 
            // LueLevel_11
            // 
            this.LueLevel_11.EnterMoveNextControl = true;
            this.LueLevel_11.Location = new System.Drawing.Point(472, 7);
            this.LueLevel_11.Margin = new System.Windows.Forms.Padding(5);
            this.LueLevel_11.Name = "LueLevel_11";
            this.LueLevel_11.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_11.Properties.Appearance.Options.UseFont = true;
            this.LueLevel_11.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_11.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueLevel_11.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_11.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueLevel_11.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_11.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueLevel_11.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_11.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueLevel_11.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueLevel_11.Properties.DropDownRows = 20;
            this.LueLevel_11.Properties.NullText = "[Empty]";
            this.LueLevel_11.Properties.PopupWidth = 500;
            this.LueLevel_11.Size = new System.Drawing.Size(84, 20);
            this.LueLevel_11.TabIndex = 55;
            this.LueLevel_11.ToolTip = "F4 : Show/hide list";
            this.LueLevel_11.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueLevel_11.EditValueChanged += new System.EventHandler(this.LueLevel11_EditValueChanged);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Red;
            this.label20.Location = new System.Drawing.Point(431, 10);
            this.label20.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(35, 14);
            this.label20.TabIndex = 54;
            this.label20.Text = "Level";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueComp11
            // 
            this.LueComp11.EnterMoveNextControl = true;
            this.LueComp11.Location = new System.Drawing.Point(93, 6);
            this.LueComp11.Margin = new System.Windows.Forms.Padding(5);
            this.LueComp11.Name = "LueComp11";
            this.LueComp11.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp11.Properties.Appearance.Options.UseFont = true;
            this.LueComp11.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp11.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueComp11.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp11.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueComp11.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp11.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueComp11.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp11.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueComp11.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueComp11.Properties.DropDownRows = 20;
            this.LueComp11.Properties.NullText = "[Empty]";
            this.LueComp11.Properties.PopupWidth = 500;
            this.LueComp11.Size = new System.Drawing.Size(330, 20);
            this.LueComp11.TabIndex = 46;
            this.LueComp11.ToolTip = "F4 : Show/hide list";
            this.LueComp11.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueComp11.EditValueChanged += new System.EventHandler(this.LueComp11_EditValueChanged);
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.Red;
            this.label32.Location = new System.Drawing.Point(8, 9);
            this.label32.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(77, 14);
            this.label32.TabIndex = 45;
            this.label32.Text = "Competence";
            this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage10
            // 
            this.tabPage10.Controls.Add(this.MeeRemarkGrd10);
            this.tabPage10.Controls.Add(this.MeeEviGrd10);
            this.tabPage10.Controls.Add(this.MeeIndGrd10);
            this.tabPage10.Controls.Add(this.MeeCompGrd10);
            this.tabPage10.Controls.Add(this.Grd10);
            this.tabPage10.Controls.Add(this.panel17);
            this.tabPage10.Location = new System.Drawing.Point(4, 23);
            this.tabPage10.Name = "tabPage10";
            this.tabPage10.Size = new System.Drawing.Size(764, 37);
            this.tabPage10.TabIndex = 9;
            this.tabPage10.Text = "No.10";
            this.tabPage10.UseVisualStyleBackColor = true;
            // 
            // MeeRemarkGrd10
            // 
            this.MeeRemarkGrd10.EnterMoveNextControl = true;
            this.MeeRemarkGrd10.Location = new System.Drawing.Point(594, 172);
            this.MeeRemarkGrd10.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemarkGrd10.Name = "MeeRemarkGrd10";
            this.MeeRemarkGrd10.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd10.Properties.Appearance.Options.UseFont = true;
            this.MeeRemarkGrd10.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd10.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemarkGrd10.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd10.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemarkGrd10.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd10.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemarkGrd10.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd10.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemarkGrd10.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemarkGrd10.Properties.MaxLength = 400;
            this.MeeRemarkGrd10.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeRemarkGrd10.Properties.ShowIcon = false;
            this.MeeRemarkGrd10.Size = new System.Drawing.Size(220, 20);
            this.MeeRemarkGrd10.TabIndex = 55;
            this.MeeRemarkGrd10.ToolTip = "F4 : Show/hide text";
            this.MeeRemarkGrd10.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemarkGrd10.ToolTipTitle = "Run System";
            this.MeeRemarkGrd10.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeRemarkGrd10_KeyDown);
            this.MeeRemarkGrd10.Leave += new System.EventHandler(this.MeeRemarkGrd10_Leave);
            // 
            // MeeEviGrd10
            // 
            this.MeeEviGrd10.EnterMoveNextControl = true;
            this.MeeEviGrd10.Location = new System.Drawing.Point(393, 170);
            this.MeeEviGrd10.Margin = new System.Windows.Forms.Padding(5);
            this.MeeEviGrd10.Name = "MeeEviGrd10";
            this.MeeEviGrd10.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd10.Properties.Appearance.Options.UseFont = true;
            this.MeeEviGrd10.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd10.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeEviGrd10.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd10.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeEviGrd10.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd10.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeEviGrd10.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd10.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeEviGrd10.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeEviGrd10.Properties.MaxLength = 400;
            this.MeeEviGrd10.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeEviGrd10.Properties.ShowIcon = false;
            this.MeeEviGrd10.Size = new System.Drawing.Size(180, 20);
            this.MeeEviGrd10.TabIndex = 54;
            this.MeeEviGrd10.ToolTip = "F4 : Show/hide text";
            this.MeeEviGrd10.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeEviGrd10.ToolTipTitle = "Run System";
            this.MeeEviGrd10.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeEviGrd10_KeyDown);
            this.MeeEviGrd10.Leave += new System.EventHandler(this.MeeEviGrd10_Leave);
            // 
            // MeeIndGrd10
            // 
            this.MeeIndGrd10.EnterMoveNextControl = true;
            this.MeeIndGrd10.Location = new System.Drawing.Point(225, 121);
            this.MeeIndGrd10.Margin = new System.Windows.Forms.Padding(5);
            this.MeeIndGrd10.Name = "MeeIndGrd10";
            this.MeeIndGrd10.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd10.Properties.Appearance.Options.UseFont = true;
            this.MeeIndGrd10.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd10.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeIndGrd10.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd10.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeIndGrd10.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd10.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeIndGrd10.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd10.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeIndGrd10.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeIndGrd10.Properties.MaxLength = 400;
            this.MeeIndGrd10.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeIndGrd10.Properties.ShowIcon = false;
            this.MeeIndGrd10.Size = new System.Drawing.Size(220, 20);
            this.MeeIndGrd10.TabIndex = 53;
            this.MeeIndGrd10.ToolTip = "F4 : Show/hide text";
            this.MeeIndGrd10.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeIndGrd10.ToolTipTitle = "Run System";
            this.MeeIndGrd10.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeIndGrd10_KeyDown);
            this.MeeIndGrd10.Leave += new System.EventHandler(this.MeeIndGrd10_Leave);
            // 
            // MeeCompGrd10
            // 
            this.MeeCompGrd10.EnterMoveNextControl = true;
            this.MeeCompGrd10.Location = new System.Drawing.Point(24, 119);
            this.MeeCompGrd10.Margin = new System.Windows.Forms.Padding(5);
            this.MeeCompGrd10.Name = "MeeCompGrd10";
            this.MeeCompGrd10.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd10.Properties.Appearance.Options.UseFont = true;
            this.MeeCompGrd10.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd10.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeCompGrd10.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd10.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeCompGrd10.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd10.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeCompGrd10.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd10.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeCompGrd10.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeCompGrd10.Properties.MaxLength = 400;
            this.MeeCompGrd10.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeCompGrd10.Properties.ShowIcon = false;
            this.MeeCompGrd10.Size = new System.Drawing.Size(180, 20);
            this.MeeCompGrd10.TabIndex = 52;
            this.MeeCompGrd10.ToolTip = "F4 : Show/hide text";
            this.MeeCompGrd10.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeCompGrd10.ToolTipTitle = "Run System";
            this.MeeCompGrd10.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeCompGrd10_KeyDown);
            this.MeeCompGrd10.Leave += new System.EventHandler(this.MeeCompGrd10_Leave);
            // 
            // Grd10
            // 
            this.Grd10.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd10.DefaultRow.Height = 20;
            this.Grd10.DefaultRow.Sortable = false;
            this.Grd10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd10.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd10.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd10.Header.Height = 21;
            this.Grd10.Location = new System.Drawing.Point(0, 33);
            this.Grd10.Name = "Grd10";
            this.Grd10.RowHeader.Visible = true;
            this.Grd10.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd10.SingleClickEdit = true;
            this.Grd10.Size = new System.Drawing.Size(764, 4);
            this.Grd10.TabIndex = 51;
            this.Grd10.TreeCol = null;
            this.Grd10.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd10.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd10_ColHdrDoubleClick);
            this.Grd10.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd10_RequestEdit);
            this.Grd10.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd10_KeyDown);
            // 
            // panel17
            // 
            this.panel17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel17.Controls.Add(this.LueLevel_10);
            this.panel17.Controls.Add(this.label16);
            this.panel17.Controls.Add(this.LueComp10);
            this.panel17.Controls.Add(this.label31);
            this.panel17.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel17.Location = new System.Drawing.Point(0, 0);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(764, 33);
            this.panel17.TabIndex = 44;
            // 
            // LueLevel_10
            // 
            this.LueLevel_10.EnterMoveNextControl = true;
            this.LueLevel_10.Location = new System.Drawing.Point(474, 5);
            this.LueLevel_10.Margin = new System.Windows.Forms.Padding(5);
            this.LueLevel_10.Name = "LueLevel_10";
            this.LueLevel_10.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_10.Properties.Appearance.Options.UseFont = true;
            this.LueLevel_10.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_10.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueLevel_10.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_10.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueLevel_10.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_10.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueLevel_10.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_10.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueLevel_10.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueLevel_10.Properties.DropDownRows = 20;
            this.LueLevel_10.Properties.NullText = "[Empty]";
            this.LueLevel_10.Properties.PopupWidth = 500;
            this.LueLevel_10.Size = new System.Drawing.Size(84, 20);
            this.LueLevel_10.TabIndex = 55;
            this.LueLevel_10.ToolTip = "F4 : Show/hide list";
            this.LueLevel_10.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueLevel_10.EditValueChanged += new System.EventHandler(this.LueLevel10_EditValueChanged);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Red;
            this.label16.Location = new System.Drawing.Point(433, 8);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(35, 14);
            this.label16.TabIndex = 54;
            this.label16.Text = "Level";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueComp10
            // 
            this.LueComp10.EnterMoveNextControl = true;
            this.LueComp10.Location = new System.Drawing.Point(94, 6);
            this.LueComp10.Margin = new System.Windows.Forms.Padding(5);
            this.LueComp10.Name = "LueComp10";
            this.LueComp10.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp10.Properties.Appearance.Options.UseFont = true;
            this.LueComp10.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp10.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueComp10.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp10.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueComp10.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp10.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueComp10.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp10.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueComp10.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueComp10.Properties.DropDownRows = 20;
            this.LueComp10.Properties.NullText = "[Empty]";
            this.LueComp10.Properties.PopupWidth = 500;
            this.LueComp10.Size = new System.Drawing.Size(330, 20);
            this.LueComp10.TabIndex = 46;
            this.LueComp10.ToolTip = "F4 : Show/hide list";
            this.LueComp10.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueComp10.EditValueChanged += new System.EventHandler(this.LueComp10_EditValueChanged);
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.Red;
            this.label31.Location = new System.Drawing.Point(8, 9);
            this.label31.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(77, 14);
            this.label31.TabIndex = 45;
            this.label31.Text = "Competence";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage9
            // 
            this.tabPage9.Controls.Add(this.MeeRemarkGrd9);
            this.tabPage9.Controls.Add(this.MeeEviGrd9);
            this.tabPage9.Controls.Add(this.MeeIndGrd9);
            this.tabPage9.Controls.Add(this.MeeCompGrd9);
            this.tabPage9.Controls.Add(this.Grd9);
            this.tabPage9.Controls.Add(this.panel16);
            this.tabPage9.Location = new System.Drawing.Point(4, 23);
            this.tabPage9.Name = "tabPage9";
            this.tabPage9.Size = new System.Drawing.Size(764, 37);
            this.tabPage9.TabIndex = 8;
            this.tabPage9.Text = "No.9";
            this.tabPage9.UseVisualStyleBackColor = true;
            // 
            // MeeRemarkGrd9
            // 
            this.MeeRemarkGrd9.EnterMoveNextControl = true;
            this.MeeRemarkGrd9.Location = new System.Drawing.Point(594, 172);
            this.MeeRemarkGrd9.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemarkGrd9.Name = "MeeRemarkGrd9";
            this.MeeRemarkGrd9.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd9.Properties.Appearance.Options.UseFont = true;
            this.MeeRemarkGrd9.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd9.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemarkGrd9.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd9.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemarkGrd9.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd9.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemarkGrd9.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd9.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemarkGrd9.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemarkGrd9.Properties.MaxLength = 400;
            this.MeeRemarkGrd9.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeRemarkGrd9.Properties.ShowIcon = false;
            this.MeeRemarkGrd9.Size = new System.Drawing.Size(220, 20);
            this.MeeRemarkGrd9.TabIndex = 55;
            this.MeeRemarkGrd9.ToolTip = "F4 : Show/hide text";
            this.MeeRemarkGrd9.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemarkGrd9.ToolTipTitle = "Run System";
            this.MeeRemarkGrd9.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeRemarkGrd9_KeyDown);
            this.MeeRemarkGrd9.Leave += new System.EventHandler(this.MeeRemarkGrd9_Leave);
            // 
            // MeeEviGrd9
            // 
            this.MeeEviGrd9.EnterMoveNextControl = true;
            this.MeeEviGrd9.Location = new System.Drawing.Point(393, 170);
            this.MeeEviGrd9.Margin = new System.Windows.Forms.Padding(5);
            this.MeeEviGrd9.Name = "MeeEviGrd9";
            this.MeeEviGrd9.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd9.Properties.Appearance.Options.UseFont = true;
            this.MeeEviGrd9.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd9.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeEviGrd9.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd9.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeEviGrd9.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd9.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeEviGrd9.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd9.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeEviGrd9.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeEviGrd9.Properties.MaxLength = 400;
            this.MeeEviGrd9.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeEviGrd9.Properties.ShowIcon = false;
            this.MeeEviGrd9.Size = new System.Drawing.Size(180, 20);
            this.MeeEviGrd9.TabIndex = 54;
            this.MeeEviGrd9.ToolTip = "F4 : Show/hide text";
            this.MeeEviGrd9.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeEviGrd9.ToolTipTitle = "Run System";
            this.MeeEviGrd9.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeEviGrd9_KeyDown);
            this.MeeEviGrd9.Leave += new System.EventHandler(this.MeeEviGrd9_Leave);
            // 
            // MeeIndGrd9
            // 
            this.MeeIndGrd9.EnterMoveNextControl = true;
            this.MeeIndGrd9.Location = new System.Drawing.Point(225, 121);
            this.MeeIndGrd9.Margin = new System.Windows.Forms.Padding(5);
            this.MeeIndGrd9.Name = "MeeIndGrd9";
            this.MeeIndGrd9.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd9.Properties.Appearance.Options.UseFont = true;
            this.MeeIndGrd9.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd9.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeIndGrd9.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd9.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeIndGrd9.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd9.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeIndGrd9.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd9.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeIndGrd9.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeIndGrd9.Properties.MaxLength = 400;
            this.MeeIndGrd9.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeIndGrd9.Properties.ShowIcon = false;
            this.MeeIndGrd9.Size = new System.Drawing.Size(220, 20);
            this.MeeIndGrd9.TabIndex = 53;
            this.MeeIndGrd9.ToolTip = "F4 : Show/hide text";
            this.MeeIndGrd9.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeIndGrd9.ToolTipTitle = "Run System";
            this.MeeIndGrd9.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeIndGrd9_KeyDown);
            this.MeeIndGrd9.Leave += new System.EventHandler(this.MeeIndGrd9_Leave);
            // 
            // MeeCompGrd9
            // 
            this.MeeCompGrd9.EnterMoveNextControl = true;
            this.MeeCompGrd9.Location = new System.Drawing.Point(24, 119);
            this.MeeCompGrd9.Margin = new System.Windows.Forms.Padding(5);
            this.MeeCompGrd9.Name = "MeeCompGrd9";
            this.MeeCompGrd9.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd9.Properties.Appearance.Options.UseFont = true;
            this.MeeCompGrd9.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd9.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeCompGrd9.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd9.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeCompGrd9.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd9.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeCompGrd9.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd9.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeCompGrd9.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeCompGrd9.Properties.MaxLength = 400;
            this.MeeCompGrd9.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeCompGrd9.Properties.ShowIcon = false;
            this.MeeCompGrd9.Size = new System.Drawing.Size(180, 20);
            this.MeeCompGrd9.TabIndex = 52;
            this.MeeCompGrd9.ToolTip = "F4 : Show/hide text";
            this.MeeCompGrd9.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeCompGrd9.ToolTipTitle = "Run System";
            this.MeeCompGrd9.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeCompGrd9_KeyDown);
            this.MeeCompGrd9.Leave += new System.EventHandler(this.MeeCompGrd9_Leave);
            // 
            // Grd9
            // 
            this.Grd9.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd9.DefaultRow.Height = 20;
            this.Grd9.DefaultRow.Sortable = false;
            this.Grd9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd9.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd9.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd9.Header.Height = 21;
            this.Grd9.Location = new System.Drawing.Point(0, 32);
            this.Grd9.Name = "Grd9";
            this.Grd9.RowHeader.Visible = true;
            this.Grd9.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd9.SingleClickEdit = true;
            this.Grd9.Size = new System.Drawing.Size(764, 5);
            this.Grd9.TabIndex = 51;
            this.Grd9.TreeCol = null;
            this.Grd9.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd9.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd9_ColHdrDoubleClick);
            this.Grd9.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd9_RequestEdit);
            this.Grd9.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd9_KeyDown);
            // 
            // panel16
            // 
            this.panel16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel16.Controls.Add(this.LueLevel_9);
            this.panel16.Controls.Add(this.label14);
            this.panel16.Controls.Add(this.LueComp9);
            this.panel16.Controls.Add(this.label29);
            this.panel16.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel16.Location = new System.Drawing.Point(0, 0);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(764, 32);
            this.panel16.TabIndex = 44;
            // 
            // LueLevel_9
            // 
            this.LueLevel_9.EnterMoveNextControl = true;
            this.LueLevel_9.Location = new System.Drawing.Point(476, 6);
            this.LueLevel_9.Margin = new System.Windows.Forms.Padding(5);
            this.LueLevel_9.Name = "LueLevel_9";
            this.LueLevel_9.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_9.Properties.Appearance.Options.UseFont = true;
            this.LueLevel_9.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_9.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueLevel_9.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_9.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueLevel_9.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_9.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueLevel_9.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_9.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueLevel_9.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueLevel_9.Properties.DropDownRows = 20;
            this.LueLevel_9.Properties.NullText = "[Empty]";
            this.LueLevel_9.Properties.PopupWidth = 500;
            this.LueLevel_9.Size = new System.Drawing.Size(84, 20);
            this.LueLevel_9.TabIndex = 55;
            this.LueLevel_9.ToolTip = "F4 : Show/hide list";
            this.LueLevel_9.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueLevel_9.EditValueChanged += new System.EventHandler(this.LueLevel9_EditValueChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Red;
            this.label14.Location = new System.Drawing.Point(435, 9);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(35, 14);
            this.label14.TabIndex = 54;
            this.label14.Text = "Level";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueComp9
            // 
            this.LueComp9.EnterMoveNextControl = true;
            this.LueComp9.Location = new System.Drawing.Point(98, 6);
            this.LueComp9.Margin = new System.Windows.Forms.Padding(5);
            this.LueComp9.Name = "LueComp9";
            this.LueComp9.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp9.Properties.Appearance.Options.UseFont = true;
            this.LueComp9.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp9.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueComp9.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp9.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueComp9.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp9.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueComp9.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp9.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueComp9.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueComp9.Properties.DropDownRows = 20;
            this.LueComp9.Properties.NullText = "[Empty]";
            this.LueComp9.Properties.PopupWidth = 500;
            this.LueComp9.Size = new System.Drawing.Size(330, 20);
            this.LueComp9.TabIndex = 46;
            this.LueComp9.ToolTip = "F4 : Show/hide list";
            this.LueComp9.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueComp9.EditValueChanged += new System.EventHandler(this.LueComp9_EditValueChanged);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Red;
            this.label29.Location = new System.Drawing.Point(8, 9);
            this.label29.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(77, 14);
            this.label29.TabIndex = 45;
            this.label29.Text = "Competence";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage8
            // 
            this.tabPage8.Controls.Add(this.MeeRemarkGrd8);
            this.tabPage8.Controls.Add(this.MeeEviGrd8);
            this.tabPage8.Controls.Add(this.MeeIndGrd8);
            this.tabPage8.Controls.Add(this.MeeCompGrd8);
            this.tabPage8.Controls.Add(this.Grd8);
            this.tabPage8.Controls.Add(this.panel15);
            this.tabPage8.Location = new System.Drawing.Point(4, 23);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Size = new System.Drawing.Size(764, 37);
            this.tabPage8.TabIndex = 7;
            this.tabPage8.Text = "No.8";
            this.tabPage8.UseVisualStyleBackColor = true;
            // 
            // MeeRemarkGrd8
            // 
            this.MeeRemarkGrd8.EnterMoveNextControl = true;
            this.MeeRemarkGrd8.Location = new System.Drawing.Point(594, 172);
            this.MeeRemarkGrd8.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemarkGrd8.Name = "MeeRemarkGrd8";
            this.MeeRemarkGrd8.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd8.Properties.Appearance.Options.UseFont = true;
            this.MeeRemarkGrd8.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd8.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemarkGrd8.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd8.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemarkGrd8.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd8.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemarkGrd8.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd8.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemarkGrd8.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemarkGrd8.Properties.MaxLength = 400;
            this.MeeRemarkGrd8.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeRemarkGrd8.Properties.ShowIcon = false;
            this.MeeRemarkGrd8.Size = new System.Drawing.Size(220, 20);
            this.MeeRemarkGrd8.TabIndex = 55;
            this.MeeRemarkGrd8.ToolTip = "F4 : Show/hide text";
            this.MeeRemarkGrd8.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemarkGrd8.ToolTipTitle = "Run System";
            this.MeeRemarkGrd8.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeRemarkGrd8_KeyDown);
            this.MeeRemarkGrd8.Leave += new System.EventHandler(this.MeeRemarkGrd8_Leave);
            // 
            // MeeEviGrd8
            // 
            this.MeeEviGrd8.EnterMoveNextControl = true;
            this.MeeEviGrd8.Location = new System.Drawing.Point(393, 170);
            this.MeeEviGrd8.Margin = new System.Windows.Forms.Padding(5);
            this.MeeEviGrd8.Name = "MeeEviGrd8";
            this.MeeEviGrd8.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd8.Properties.Appearance.Options.UseFont = true;
            this.MeeEviGrd8.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd8.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeEviGrd8.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd8.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeEviGrd8.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd8.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeEviGrd8.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd8.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeEviGrd8.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeEviGrd8.Properties.MaxLength = 400;
            this.MeeEviGrd8.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeEviGrd8.Properties.ShowIcon = false;
            this.MeeEviGrd8.Size = new System.Drawing.Size(180, 20);
            this.MeeEviGrd8.TabIndex = 54;
            this.MeeEviGrd8.ToolTip = "F4 : Show/hide text";
            this.MeeEviGrd8.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeEviGrd8.ToolTipTitle = "Run System";
            this.MeeEviGrd8.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeEviGrd8_KeyDown);
            this.MeeEviGrd8.Leave += new System.EventHandler(this.MeeEviGrd8_Leave);
            // 
            // MeeIndGrd8
            // 
            this.MeeIndGrd8.EnterMoveNextControl = true;
            this.MeeIndGrd8.Location = new System.Drawing.Point(225, 121);
            this.MeeIndGrd8.Margin = new System.Windows.Forms.Padding(5);
            this.MeeIndGrd8.Name = "MeeIndGrd8";
            this.MeeIndGrd8.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd8.Properties.Appearance.Options.UseFont = true;
            this.MeeIndGrd8.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd8.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeIndGrd8.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd8.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeIndGrd8.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd8.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeIndGrd8.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd8.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeIndGrd8.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeIndGrd8.Properties.MaxLength = 400;
            this.MeeIndGrd8.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeIndGrd8.Properties.ShowIcon = false;
            this.MeeIndGrd8.Size = new System.Drawing.Size(220, 20);
            this.MeeIndGrd8.TabIndex = 53;
            this.MeeIndGrd8.ToolTip = "F4 : Show/hide text";
            this.MeeIndGrd8.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeIndGrd8.ToolTipTitle = "Run System";
            this.MeeIndGrd8.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeIndGrd8_KeyDown);
            this.MeeIndGrd8.Leave += new System.EventHandler(this.MeeIndGrd8_Leave);
            // 
            // MeeCompGrd8
            // 
            this.MeeCompGrd8.EnterMoveNextControl = true;
            this.MeeCompGrd8.Location = new System.Drawing.Point(24, 119);
            this.MeeCompGrd8.Margin = new System.Windows.Forms.Padding(5);
            this.MeeCompGrd8.Name = "MeeCompGrd8";
            this.MeeCompGrd8.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd8.Properties.Appearance.Options.UseFont = true;
            this.MeeCompGrd8.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd8.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeCompGrd8.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd8.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeCompGrd8.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd8.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeCompGrd8.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd8.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeCompGrd8.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeCompGrd8.Properties.MaxLength = 400;
            this.MeeCompGrd8.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeCompGrd8.Properties.ShowIcon = false;
            this.MeeCompGrd8.Size = new System.Drawing.Size(180, 20);
            this.MeeCompGrd8.TabIndex = 52;
            this.MeeCompGrd8.ToolTip = "F4 : Show/hide text";
            this.MeeCompGrd8.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeCompGrd8.ToolTipTitle = "Run System";
            this.MeeCompGrd8.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeCompGrd8_KeyDown);
            this.MeeCompGrd8.Leave += new System.EventHandler(this.MeeCompGrd8_Leave);
            // 
            // Grd8
            // 
            this.Grd8.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd8.DefaultRow.Height = 20;
            this.Grd8.DefaultRow.Sortable = false;
            this.Grd8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd8.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd8.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd8.Header.Height = 21;
            this.Grd8.Location = new System.Drawing.Point(0, 32);
            this.Grd8.Name = "Grd8";
            this.Grd8.RowHeader.Visible = true;
            this.Grd8.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd8.SingleClickEdit = true;
            this.Grd8.Size = new System.Drawing.Size(764, 5);
            this.Grd8.TabIndex = 51;
            this.Grd8.TreeCol = null;
            this.Grd8.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd8.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd8_ColHdrDoubleClick);
            this.Grd8.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd8_RequestEdit);
            this.Grd8.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd8_KeyDown);
            // 
            // panel15
            // 
            this.panel15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel15.Controls.Add(this.LueLevel_8);
            this.panel15.Controls.Add(this.label12);
            this.panel15.Controls.Add(this.LueComp8);
            this.panel15.Controls.Add(this.label27);
            this.panel15.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel15.Location = new System.Drawing.Point(0, 0);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(764, 32);
            this.panel15.TabIndex = 44;
            // 
            // LueLevel_8
            // 
            this.LueLevel_8.EnterMoveNextControl = true;
            this.LueLevel_8.Location = new System.Drawing.Point(467, 6);
            this.LueLevel_8.Margin = new System.Windows.Forms.Padding(5);
            this.LueLevel_8.Name = "LueLevel_8";
            this.LueLevel_8.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_8.Properties.Appearance.Options.UseFont = true;
            this.LueLevel_8.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_8.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueLevel_8.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_8.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueLevel_8.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_8.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueLevel_8.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_8.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueLevel_8.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueLevel_8.Properties.DropDownRows = 20;
            this.LueLevel_8.Properties.NullText = "[Empty]";
            this.LueLevel_8.Properties.PopupWidth = 500;
            this.LueLevel_8.Size = new System.Drawing.Size(84, 20);
            this.LueLevel_8.TabIndex = 55;
            this.LueLevel_8.ToolTip = "F4 : Show/hide list";
            this.LueLevel_8.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueLevel_8.EditValueChanged += new System.EventHandler(this.LueLevel8_EditValueChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Red;
            this.label12.Location = new System.Drawing.Point(426, 9);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(35, 14);
            this.label12.TabIndex = 54;
            this.label12.Text = "Level";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueComp8
            // 
            this.LueComp8.EnterMoveNextControl = true;
            this.LueComp8.Location = new System.Drawing.Point(89, 6);
            this.LueComp8.Margin = new System.Windows.Forms.Padding(5);
            this.LueComp8.Name = "LueComp8";
            this.LueComp8.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp8.Properties.Appearance.Options.UseFont = true;
            this.LueComp8.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp8.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueComp8.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp8.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueComp8.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp8.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueComp8.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp8.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueComp8.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueComp8.Properties.DropDownRows = 20;
            this.LueComp8.Properties.NullText = "[Empty]";
            this.LueComp8.Properties.PopupWidth = 500;
            this.LueComp8.Size = new System.Drawing.Size(330, 20);
            this.LueComp8.TabIndex = 46;
            this.LueComp8.ToolTip = "F4 : Show/hide list";
            this.LueComp8.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueComp8.EditValueChanged += new System.EventHandler(this.LueComp8_EditValueChanged);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Red;
            this.label27.Location = new System.Drawing.Point(8, 9);
            this.label27.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(77, 14);
            this.label27.TabIndex = 45;
            this.label27.Text = "Competence";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage7
            // 
            this.tabPage7.Controls.Add(this.MeeRemarkGrd7);
            this.tabPage7.Controls.Add(this.MeeEviGrd7);
            this.tabPage7.Controls.Add(this.MeeIndGrd7);
            this.tabPage7.Controls.Add(this.MeeCompGrd7);
            this.tabPage7.Controls.Add(this.Grd7);
            this.tabPage7.Controls.Add(this.panel14);
            this.tabPage7.Location = new System.Drawing.Point(4, 23);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Size = new System.Drawing.Size(764, 37);
            this.tabPage7.TabIndex = 6;
            this.tabPage7.Text = "No.7";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // MeeRemarkGrd7
            // 
            this.MeeRemarkGrd7.EnterMoveNextControl = true;
            this.MeeRemarkGrd7.Location = new System.Drawing.Point(594, 172);
            this.MeeRemarkGrd7.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemarkGrd7.Name = "MeeRemarkGrd7";
            this.MeeRemarkGrd7.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd7.Properties.Appearance.Options.UseFont = true;
            this.MeeRemarkGrd7.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd7.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemarkGrd7.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd7.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemarkGrd7.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd7.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemarkGrd7.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd7.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemarkGrd7.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemarkGrd7.Properties.MaxLength = 400;
            this.MeeRemarkGrd7.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeRemarkGrd7.Properties.ShowIcon = false;
            this.MeeRemarkGrd7.Size = new System.Drawing.Size(220, 20);
            this.MeeRemarkGrd7.TabIndex = 55;
            this.MeeRemarkGrd7.ToolTip = "F4 : Show/hide text";
            this.MeeRemarkGrd7.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemarkGrd7.ToolTipTitle = "Run System";
            this.MeeRemarkGrd7.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeRemarkGrd7_KeyDown);
            this.MeeRemarkGrd7.Leave += new System.EventHandler(this.MeeRemarkGrd7_Leave);
            // 
            // MeeEviGrd7
            // 
            this.MeeEviGrd7.EnterMoveNextControl = true;
            this.MeeEviGrd7.Location = new System.Drawing.Point(393, 170);
            this.MeeEviGrd7.Margin = new System.Windows.Forms.Padding(5);
            this.MeeEviGrd7.Name = "MeeEviGrd7";
            this.MeeEviGrd7.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd7.Properties.Appearance.Options.UseFont = true;
            this.MeeEviGrd7.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd7.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeEviGrd7.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd7.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeEviGrd7.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd7.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeEviGrd7.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd7.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeEviGrd7.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeEviGrd7.Properties.MaxLength = 400;
            this.MeeEviGrd7.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeEviGrd7.Properties.ShowIcon = false;
            this.MeeEviGrd7.Size = new System.Drawing.Size(180, 20);
            this.MeeEviGrd7.TabIndex = 54;
            this.MeeEviGrd7.ToolTip = "F4 : Show/hide text";
            this.MeeEviGrd7.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeEviGrd7.ToolTipTitle = "Run System";
            this.MeeEviGrd7.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeEviGrd7_KeyDown);
            this.MeeEviGrd7.Leave += new System.EventHandler(this.MeeEviGrd7_Leave);
            // 
            // MeeIndGrd7
            // 
            this.MeeIndGrd7.EnterMoveNextControl = true;
            this.MeeIndGrd7.Location = new System.Drawing.Point(225, 121);
            this.MeeIndGrd7.Margin = new System.Windows.Forms.Padding(5);
            this.MeeIndGrd7.Name = "MeeIndGrd7";
            this.MeeIndGrd7.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd7.Properties.Appearance.Options.UseFont = true;
            this.MeeIndGrd7.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd7.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeIndGrd7.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd7.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeIndGrd7.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd7.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeIndGrd7.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd7.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeIndGrd7.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeIndGrd7.Properties.MaxLength = 400;
            this.MeeIndGrd7.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeIndGrd7.Properties.ShowIcon = false;
            this.MeeIndGrd7.Size = new System.Drawing.Size(220, 20);
            this.MeeIndGrd7.TabIndex = 53;
            this.MeeIndGrd7.ToolTip = "F4 : Show/hide text";
            this.MeeIndGrd7.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeIndGrd7.ToolTipTitle = "Run System";
            this.MeeIndGrd7.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeIndGrd7_KeyDown);
            this.MeeIndGrd7.Leave += new System.EventHandler(this.MeeIndGrd7_Leave);
            // 
            // MeeCompGrd7
            // 
            this.MeeCompGrd7.EnterMoveNextControl = true;
            this.MeeCompGrd7.Location = new System.Drawing.Point(24, 119);
            this.MeeCompGrd7.Margin = new System.Windows.Forms.Padding(5);
            this.MeeCompGrd7.Name = "MeeCompGrd7";
            this.MeeCompGrd7.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd7.Properties.Appearance.Options.UseFont = true;
            this.MeeCompGrd7.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd7.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeCompGrd7.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd7.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeCompGrd7.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd7.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeCompGrd7.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd7.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeCompGrd7.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeCompGrd7.Properties.MaxLength = 400;
            this.MeeCompGrd7.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeCompGrd7.Properties.ShowIcon = false;
            this.MeeCompGrd7.Size = new System.Drawing.Size(180, 20);
            this.MeeCompGrd7.TabIndex = 52;
            this.MeeCompGrd7.ToolTip = "F4 : Show/hide text";
            this.MeeCompGrd7.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeCompGrd7.ToolTipTitle = "Run System";
            this.MeeCompGrd7.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeCompGrd7_KeyDown);
            this.MeeCompGrd7.Leave += new System.EventHandler(this.MeeCompGrd7_Leave);
            // 
            // Grd7
            // 
            this.Grd7.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd7.DefaultRow.Height = 20;
            this.Grd7.DefaultRow.Sortable = false;
            this.Grd7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd7.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd7.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd7.Header.Height = 21;
            this.Grd7.Location = new System.Drawing.Point(0, 32);
            this.Grd7.Name = "Grd7";
            this.Grd7.RowHeader.Visible = true;
            this.Grd7.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd7.SingleClickEdit = true;
            this.Grd7.Size = new System.Drawing.Size(764, 5);
            this.Grd7.TabIndex = 51;
            this.Grd7.TreeCol = null;
            this.Grd7.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd7.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd7_ColHdrDoubleClick);
            this.Grd7.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd7_RequestEdit);
            this.Grd7.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd7_KeyDown);
            // 
            // panel14
            // 
            this.panel14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel14.Controls.Add(this.LueLevel_7);
            this.panel14.Controls.Add(this.label10);
            this.panel14.Controls.Add(this.LueComp7);
            this.panel14.Controls.Add(this.label25);
            this.panel14.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel14.Location = new System.Drawing.Point(0, 0);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(764, 32);
            this.panel14.TabIndex = 44;
            // 
            // LueLevel_7
            // 
            this.LueLevel_7.EnterMoveNextControl = true;
            this.LueLevel_7.Location = new System.Drawing.Point(472, 6);
            this.LueLevel_7.Margin = new System.Windows.Forms.Padding(5);
            this.LueLevel_7.Name = "LueLevel_7";
            this.LueLevel_7.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_7.Properties.Appearance.Options.UseFont = true;
            this.LueLevel_7.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_7.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueLevel_7.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_7.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueLevel_7.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_7.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueLevel_7.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_7.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueLevel_7.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueLevel_7.Properties.DropDownRows = 20;
            this.LueLevel_7.Properties.NullText = "[Empty]";
            this.LueLevel_7.Properties.PopupWidth = 500;
            this.LueLevel_7.Size = new System.Drawing.Size(84, 20);
            this.LueLevel_7.TabIndex = 55;
            this.LueLevel_7.ToolTip = "F4 : Show/hide list";
            this.LueLevel_7.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueLevel_7.EditValueChanged += new System.EventHandler(this.LueLevel7_EditValueChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Red;
            this.label10.Location = new System.Drawing.Point(431, 9);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(35, 14);
            this.label10.TabIndex = 54;
            this.label10.Text = "Level";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueComp7
            // 
            this.LueComp7.EnterMoveNextControl = true;
            this.LueComp7.Location = new System.Drawing.Point(97, 6);
            this.LueComp7.Margin = new System.Windows.Forms.Padding(5);
            this.LueComp7.Name = "LueComp7";
            this.LueComp7.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp7.Properties.Appearance.Options.UseFont = true;
            this.LueComp7.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp7.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueComp7.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp7.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueComp7.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp7.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueComp7.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp7.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueComp7.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueComp7.Properties.DropDownRows = 20;
            this.LueComp7.Properties.NullText = "[Empty]";
            this.LueComp7.Properties.PopupWidth = 500;
            this.LueComp7.Size = new System.Drawing.Size(330, 20);
            this.LueComp7.TabIndex = 46;
            this.LueComp7.ToolTip = "F4 : Show/hide list";
            this.LueComp7.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueComp7.EditValueChanged += new System.EventHandler(this.LueComp7_EditValueChanged);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Red;
            this.label25.Location = new System.Drawing.Point(8, 9);
            this.label25.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(77, 14);
            this.label25.TabIndex = 45;
            this.label25.Text = "Competence";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.MeeRemarkGrd6);
            this.tabPage6.Controls.Add(this.MeeEviGrd6);
            this.tabPage6.Controls.Add(this.MeeIndGrd6);
            this.tabPage6.Controls.Add(this.MeeCompGrd6);
            this.tabPage6.Controls.Add(this.Grd6);
            this.tabPage6.Controls.Add(this.panel13);
            this.tabPage6.Location = new System.Drawing.Point(4, 23);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Size = new System.Drawing.Size(764, 37);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "No.6";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // MeeRemarkGrd6
            // 
            this.MeeRemarkGrd6.EnterMoveNextControl = true;
            this.MeeRemarkGrd6.Location = new System.Drawing.Point(594, 172);
            this.MeeRemarkGrd6.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemarkGrd6.Name = "MeeRemarkGrd6";
            this.MeeRemarkGrd6.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd6.Properties.Appearance.Options.UseFont = true;
            this.MeeRemarkGrd6.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd6.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemarkGrd6.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd6.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemarkGrd6.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd6.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemarkGrd6.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd6.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemarkGrd6.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemarkGrd6.Properties.MaxLength = 400;
            this.MeeRemarkGrd6.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeRemarkGrd6.Properties.ShowIcon = false;
            this.MeeRemarkGrd6.Size = new System.Drawing.Size(220, 20);
            this.MeeRemarkGrd6.TabIndex = 55;
            this.MeeRemarkGrd6.ToolTip = "F4 : Show/hide text";
            this.MeeRemarkGrd6.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemarkGrd6.ToolTipTitle = "Run System";
            this.MeeRemarkGrd6.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeRemarkGrd6_KeyDown);
            this.MeeRemarkGrd6.Leave += new System.EventHandler(this.MeeRemarkGrd6_Leave);
            // 
            // MeeEviGrd6
            // 
            this.MeeEviGrd6.EnterMoveNextControl = true;
            this.MeeEviGrd6.Location = new System.Drawing.Point(393, 170);
            this.MeeEviGrd6.Margin = new System.Windows.Forms.Padding(5);
            this.MeeEviGrd6.Name = "MeeEviGrd6";
            this.MeeEviGrd6.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd6.Properties.Appearance.Options.UseFont = true;
            this.MeeEviGrd6.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd6.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeEviGrd6.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd6.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeEviGrd6.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd6.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeEviGrd6.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd6.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeEviGrd6.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeEviGrd6.Properties.MaxLength = 400;
            this.MeeEviGrd6.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeEviGrd6.Properties.ShowIcon = false;
            this.MeeEviGrd6.Size = new System.Drawing.Size(180, 20);
            this.MeeEviGrd6.TabIndex = 54;
            this.MeeEviGrd6.ToolTip = "F4 : Show/hide text";
            this.MeeEviGrd6.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeEviGrd6.ToolTipTitle = "Run System";
            this.MeeEviGrd6.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeEviGrd6_KeyDown);
            this.MeeEviGrd6.Leave += new System.EventHandler(this.MeeEviGrd6_Leave);
            // 
            // MeeIndGrd6
            // 
            this.MeeIndGrd6.EnterMoveNextControl = true;
            this.MeeIndGrd6.Location = new System.Drawing.Point(225, 121);
            this.MeeIndGrd6.Margin = new System.Windows.Forms.Padding(5);
            this.MeeIndGrd6.Name = "MeeIndGrd6";
            this.MeeIndGrd6.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd6.Properties.Appearance.Options.UseFont = true;
            this.MeeIndGrd6.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd6.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeIndGrd6.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd6.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeIndGrd6.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd6.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeIndGrd6.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd6.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeIndGrd6.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeIndGrd6.Properties.MaxLength = 400;
            this.MeeIndGrd6.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeIndGrd6.Properties.ShowIcon = false;
            this.MeeIndGrd6.Size = new System.Drawing.Size(220, 20);
            this.MeeIndGrd6.TabIndex = 53;
            this.MeeIndGrd6.ToolTip = "F4 : Show/hide text";
            this.MeeIndGrd6.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeIndGrd6.ToolTipTitle = "Run System";
            this.MeeIndGrd6.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeIndGrd6_KeyDown);
            this.MeeIndGrd6.Leave += new System.EventHandler(this.MeeIndGrd6_Leave);
            // 
            // MeeCompGrd6
            // 
            this.MeeCompGrd6.EnterMoveNextControl = true;
            this.MeeCompGrd6.Location = new System.Drawing.Point(24, 119);
            this.MeeCompGrd6.Margin = new System.Windows.Forms.Padding(5);
            this.MeeCompGrd6.Name = "MeeCompGrd6";
            this.MeeCompGrd6.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd6.Properties.Appearance.Options.UseFont = true;
            this.MeeCompGrd6.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd6.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeCompGrd6.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd6.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeCompGrd6.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd6.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeCompGrd6.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd6.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeCompGrd6.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeCompGrd6.Properties.MaxLength = 400;
            this.MeeCompGrd6.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeCompGrd6.Properties.ShowIcon = false;
            this.MeeCompGrd6.Size = new System.Drawing.Size(180, 20);
            this.MeeCompGrd6.TabIndex = 52;
            this.MeeCompGrd6.ToolTip = "F4 : Show/hide text";
            this.MeeCompGrd6.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeCompGrd6.ToolTipTitle = "Run System";
            this.MeeCompGrd6.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeCompGrd6_KeyDown);
            this.MeeCompGrd6.Leave += new System.EventHandler(this.MeeCompGrd6_Leave);
            // 
            // Grd6
            // 
            this.Grd6.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd6.DefaultRow.Height = 20;
            this.Grd6.DefaultRow.Sortable = false;
            this.Grd6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd6.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd6.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd6.Header.Height = 21;
            this.Grd6.Location = new System.Drawing.Point(0, 33);
            this.Grd6.Name = "Grd6";
            this.Grd6.RowHeader.Visible = true;
            this.Grd6.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd6.SingleClickEdit = true;
            this.Grd6.Size = new System.Drawing.Size(764, 4);
            this.Grd6.TabIndex = 51;
            this.Grd6.TreeCol = null;
            this.Grd6.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd6.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd6_ColHdrDoubleClick);
            this.Grd6.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd6_RequestEdit);
            this.Grd6.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd6_KeyDown);
            // 
            // panel13
            // 
            this.panel13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel13.Controls.Add(this.LueLevel_6);
            this.panel13.Controls.Add(this.label9);
            this.panel13.Controls.Add(this.LueComp6);
            this.panel13.Controls.Add(this.label23);
            this.panel13.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel13.Location = new System.Drawing.Point(0, 0);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(764, 33);
            this.panel13.TabIndex = 44;
            // 
            // LueLevel_6
            // 
            this.LueLevel_6.EnterMoveNextControl = true;
            this.LueLevel_6.Location = new System.Drawing.Point(470, 7);
            this.LueLevel_6.Margin = new System.Windows.Forms.Padding(5);
            this.LueLevel_6.Name = "LueLevel_6";
            this.LueLevel_6.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_6.Properties.Appearance.Options.UseFont = true;
            this.LueLevel_6.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_6.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueLevel_6.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_6.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueLevel_6.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_6.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueLevel_6.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_6.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueLevel_6.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueLevel_6.Properties.DropDownRows = 20;
            this.LueLevel_6.Properties.NullText = "[Empty]";
            this.LueLevel_6.Properties.PopupWidth = 500;
            this.LueLevel_6.Size = new System.Drawing.Size(84, 20);
            this.LueLevel_6.TabIndex = 55;
            this.LueLevel_6.ToolTip = "F4 : Show/hide list";
            this.LueLevel_6.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueLevel_6.EditValueChanged += new System.EventHandler(this.LueLevel6_EditValueChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Location = new System.Drawing.Point(429, 10);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(35, 14);
            this.label9.TabIndex = 54;
            this.label9.Text = "Level";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueComp6
            // 
            this.LueComp6.EnterMoveNextControl = true;
            this.LueComp6.Location = new System.Drawing.Point(93, 7);
            this.LueComp6.Margin = new System.Windows.Forms.Padding(5);
            this.LueComp6.Name = "LueComp6";
            this.LueComp6.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp6.Properties.Appearance.Options.UseFont = true;
            this.LueComp6.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp6.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueComp6.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp6.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueComp6.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp6.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueComp6.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp6.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueComp6.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueComp6.Properties.DropDownRows = 20;
            this.LueComp6.Properties.NullText = "[Empty]";
            this.LueComp6.Properties.PopupWidth = 500;
            this.LueComp6.Size = new System.Drawing.Size(330, 20);
            this.LueComp6.TabIndex = 46;
            this.LueComp6.ToolTip = "F4 : Show/hide list";
            this.LueComp6.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueComp6.EditValueChanged += new System.EventHandler(this.LueComp6_EditValueChanged);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Red;
            this.label23.Location = new System.Drawing.Point(8, 9);
            this.label23.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(77, 14);
            this.label23.TabIndex = 45;
            this.label23.Text = "Competence";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.MeeRemarkGrd5);
            this.tabPage5.Controls.Add(this.MeeEviGrd5);
            this.tabPage5.Controls.Add(this.MeeIndGrd5);
            this.tabPage5.Controls.Add(this.MeeCompGrd5);
            this.tabPage5.Controls.Add(this.Grd5);
            this.tabPage5.Controls.Add(this.panel12);
            this.tabPage5.Location = new System.Drawing.Point(4, 23);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(764, 37);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "No.5";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // MeeRemarkGrd5
            // 
            this.MeeRemarkGrd5.EnterMoveNextControl = true;
            this.MeeRemarkGrd5.Location = new System.Drawing.Point(594, 172);
            this.MeeRemarkGrd5.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemarkGrd5.Name = "MeeRemarkGrd5";
            this.MeeRemarkGrd5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd5.Properties.Appearance.Options.UseFont = true;
            this.MeeRemarkGrd5.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd5.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemarkGrd5.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd5.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemarkGrd5.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd5.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemarkGrd5.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd5.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemarkGrd5.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemarkGrd5.Properties.MaxLength = 400;
            this.MeeRemarkGrd5.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeRemarkGrd5.Properties.ShowIcon = false;
            this.MeeRemarkGrd5.Size = new System.Drawing.Size(220, 20);
            this.MeeRemarkGrd5.TabIndex = 55;
            this.MeeRemarkGrd5.ToolTip = "F4 : Show/hide text";
            this.MeeRemarkGrd5.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemarkGrd5.ToolTipTitle = "Run System";
            this.MeeRemarkGrd5.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeRemarkGrd5_KeyDown);
            this.MeeRemarkGrd5.Leave += new System.EventHandler(this.MeeRemarkGrd5_Leave);
            // 
            // MeeEviGrd5
            // 
            this.MeeEviGrd5.EnterMoveNextControl = true;
            this.MeeEviGrd5.Location = new System.Drawing.Point(393, 170);
            this.MeeEviGrd5.Margin = new System.Windows.Forms.Padding(5);
            this.MeeEviGrd5.Name = "MeeEviGrd5";
            this.MeeEviGrd5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd5.Properties.Appearance.Options.UseFont = true;
            this.MeeEviGrd5.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd5.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeEviGrd5.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd5.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeEviGrd5.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd5.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeEviGrd5.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd5.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeEviGrd5.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeEviGrd5.Properties.MaxLength = 400;
            this.MeeEviGrd5.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeEviGrd5.Properties.ShowIcon = false;
            this.MeeEviGrd5.Size = new System.Drawing.Size(180, 20);
            this.MeeEviGrd5.TabIndex = 54;
            this.MeeEviGrd5.ToolTip = "F4 : Show/hide text";
            this.MeeEviGrd5.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeEviGrd5.ToolTipTitle = "Run System";
            this.MeeEviGrd5.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeEviGrd5_KeyDown);
            this.MeeEviGrd5.Leave += new System.EventHandler(this.MeeEviGrd5_Leave);
            // 
            // MeeIndGrd5
            // 
            this.MeeIndGrd5.EnterMoveNextControl = true;
            this.MeeIndGrd5.Location = new System.Drawing.Point(225, 121);
            this.MeeIndGrd5.Margin = new System.Windows.Forms.Padding(5);
            this.MeeIndGrd5.Name = "MeeIndGrd5";
            this.MeeIndGrd5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd5.Properties.Appearance.Options.UseFont = true;
            this.MeeIndGrd5.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd5.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeIndGrd5.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd5.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeIndGrd5.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd5.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeIndGrd5.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd5.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeIndGrd5.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeIndGrd5.Properties.MaxLength = 400;
            this.MeeIndGrd5.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeIndGrd5.Properties.ShowIcon = false;
            this.MeeIndGrd5.Size = new System.Drawing.Size(220, 20);
            this.MeeIndGrd5.TabIndex = 53;
            this.MeeIndGrd5.ToolTip = "F4 : Show/hide text";
            this.MeeIndGrd5.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeIndGrd5.ToolTipTitle = "Run System";
            this.MeeIndGrd5.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeIndGrd5_KeyDown);
            this.MeeIndGrd5.Leave += new System.EventHandler(this.MeeIndGrd5_Leave);
            // 
            // MeeCompGrd5
            // 
            this.MeeCompGrd5.EnterMoveNextControl = true;
            this.MeeCompGrd5.Location = new System.Drawing.Point(24, 119);
            this.MeeCompGrd5.Margin = new System.Windows.Forms.Padding(5);
            this.MeeCompGrd5.Name = "MeeCompGrd5";
            this.MeeCompGrd5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd5.Properties.Appearance.Options.UseFont = true;
            this.MeeCompGrd5.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd5.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeCompGrd5.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd5.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeCompGrd5.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd5.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeCompGrd5.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd5.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeCompGrd5.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeCompGrd5.Properties.MaxLength = 400;
            this.MeeCompGrd5.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeCompGrd5.Properties.ShowIcon = false;
            this.MeeCompGrd5.Size = new System.Drawing.Size(180, 20);
            this.MeeCompGrd5.TabIndex = 52;
            this.MeeCompGrd5.ToolTip = "F4 : Show/hide text";
            this.MeeCompGrd5.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeCompGrd5.ToolTipTitle = "Run System";
            this.MeeCompGrd5.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeCompGrd5_KeyDown);
            this.MeeCompGrd5.Leave += new System.EventHandler(this.MeeCompGrd5_Leave);
            // 
            // Grd5
            // 
            this.Grd5.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd5.DefaultRow.Height = 20;
            this.Grd5.DefaultRow.Sortable = false;
            this.Grd5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd5.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd5.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd5.Header.Height = 21;
            this.Grd5.Location = new System.Drawing.Point(0, 32);
            this.Grd5.Name = "Grd5";
            this.Grd5.RowHeader.Visible = true;
            this.Grd5.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd5.SingleClickEdit = true;
            this.Grd5.Size = new System.Drawing.Size(764, 5);
            this.Grd5.TabIndex = 51;
            this.Grd5.TreeCol = null;
            this.Grd5.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd5.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd5_ColHdrDoubleClick);
            this.Grd5.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd5_RequestEdit);
            this.Grd5.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd5_KeyDown);
            // 
            // panel12
            // 
            this.panel12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel12.Controls.Add(this.LueLevel_5);
            this.panel12.Controls.Add(this.label8);
            this.panel12.Controls.Add(this.LueComp5);
            this.panel12.Controls.Add(this.label21);
            this.panel12.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel12.Location = new System.Drawing.Point(0, 0);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(764, 32);
            this.panel12.TabIndex = 44;
            // 
            // LueLevel_5
            // 
            this.LueLevel_5.EnterMoveNextControl = true;
            this.LueLevel_5.Location = new System.Drawing.Point(472, 6);
            this.LueLevel_5.Margin = new System.Windows.Forms.Padding(5);
            this.LueLevel_5.Name = "LueLevel_5";
            this.LueLevel_5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_5.Properties.Appearance.Options.UseFont = true;
            this.LueLevel_5.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_5.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueLevel_5.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_5.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueLevel_5.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_5.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueLevel_5.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_5.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueLevel_5.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueLevel_5.Properties.DropDownRows = 20;
            this.LueLevel_5.Properties.NullText = "[Empty]";
            this.LueLevel_5.Properties.PopupWidth = 500;
            this.LueLevel_5.Size = new System.Drawing.Size(84, 20);
            this.LueLevel_5.TabIndex = 55;
            this.LueLevel_5.ToolTip = "F4 : Show/hide list";
            this.LueLevel_5.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueLevel_5.EditValueChanged += new System.EventHandler(this.LueLevel5_EditValueChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(431, 9);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(35, 14);
            this.label8.TabIndex = 54;
            this.label8.Text = "Level";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueComp5
            // 
            this.LueComp5.EnterMoveNextControl = true;
            this.LueComp5.Location = new System.Drawing.Point(94, 6);
            this.LueComp5.Margin = new System.Windows.Forms.Padding(5);
            this.LueComp5.Name = "LueComp5";
            this.LueComp5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp5.Properties.Appearance.Options.UseFont = true;
            this.LueComp5.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp5.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueComp5.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp5.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueComp5.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp5.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueComp5.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp5.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueComp5.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueComp5.Properties.DropDownRows = 20;
            this.LueComp5.Properties.NullText = "[Empty]";
            this.LueComp5.Properties.PopupWidth = 500;
            this.LueComp5.Size = new System.Drawing.Size(330, 20);
            this.LueComp5.TabIndex = 46;
            this.LueComp5.ToolTip = "F4 : Show/hide list";
            this.LueComp5.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueComp5.EditValueChanged += new System.EventHandler(this.LueComp5_EditValueChanged);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Red;
            this.label21.Location = new System.Drawing.Point(8, 9);
            this.label21.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(77, 14);
            this.label21.TabIndex = 45;
            this.label21.Text = "Competence";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.MeeRemarkGrd4);
            this.tabPage4.Controls.Add(this.MeeEviGrd4);
            this.tabPage4.Controls.Add(this.MeeIndGrd4);
            this.tabPage4.Controls.Add(this.MeeCompGrd4);
            this.tabPage4.Controls.Add(this.Grd4);
            this.tabPage4.Controls.Add(this.panel11);
            this.tabPage4.Location = new System.Drawing.Point(4, 23);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(839, 265);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "No.4";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // MeeRemarkGrd4
            // 
            this.MeeRemarkGrd4.EnterMoveNextControl = true;
            this.MeeRemarkGrd4.Location = new System.Drawing.Point(594, 172);
            this.MeeRemarkGrd4.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemarkGrd4.Name = "MeeRemarkGrd4";
            this.MeeRemarkGrd4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd4.Properties.Appearance.Options.UseFont = true;
            this.MeeRemarkGrd4.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd4.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemarkGrd4.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd4.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemarkGrd4.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd4.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemarkGrd4.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd4.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemarkGrd4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemarkGrd4.Properties.MaxLength = 400;
            this.MeeRemarkGrd4.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeRemarkGrd4.Properties.ShowIcon = false;
            this.MeeRemarkGrd4.Size = new System.Drawing.Size(220, 20);
            this.MeeRemarkGrd4.TabIndex = 55;
            this.MeeRemarkGrd4.ToolTip = "F4 : Show/hide text";
            this.MeeRemarkGrd4.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemarkGrd4.ToolTipTitle = "Run System";
            this.MeeRemarkGrd4.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeRemarkGrd4_KeyDown);
            this.MeeRemarkGrd4.Leave += new System.EventHandler(this.MeeRemarkGrd4_Leave);
            // 
            // MeeEviGrd4
            // 
            this.MeeEviGrd4.EnterMoveNextControl = true;
            this.MeeEviGrd4.Location = new System.Drawing.Point(393, 170);
            this.MeeEviGrd4.Margin = new System.Windows.Forms.Padding(5);
            this.MeeEviGrd4.Name = "MeeEviGrd4";
            this.MeeEviGrd4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd4.Properties.Appearance.Options.UseFont = true;
            this.MeeEviGrd4.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd4.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeEviGrd4.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd4.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeEviGrd4.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd4.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeEviGrd4.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd4.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeEviGrd4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeEviGrd4.Properties.MaxLength = 400;
            this.MeeEviGrd4.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeEviGrd4.Properties.ShowIcon = false;
            this.MeeEviGrd4.Size = new System.Drawing.Size(180, 20);
            this.MeeEviGrd4.TabIndex = 54;
            this.MeeEviGrd4.ToolTip = "F4 : Show/hide text";
            this.MeeEviGrd4.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeEviGrd4.ToolTipTitle = "Run System";
            this.MeeEviGrd4.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeEviGrd4_KeyDown);
            this.MeeEviGrd4.Leave += new System.EventHandler(this.MeeEviGrd4_Leave);
            // 
            // MeeIndGrd4
            // 
            this.MeeIndGrd4.EnterMoveNextControl = true;
            this.MeeIndGrd4.Location = new System.Drawing.Point(225, 121);
            this.MeeIndGrd4.Margin = new System.Windows.Forms.Padding(5);
            this.MeeIndGrd4.Name = "MeeIndGrd4";
            this.MeeIndGrd4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd4.Properties.Appearance.Options.UseFont = true;
            this.MeeIndGrd4.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd4.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeIndGrd4.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd4.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeIndGrd4.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd4.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeIndGrd4.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd4.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeIndGrd4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeIndGrd4.Properties.MaxLength = 400;
            this.MeeIndGrd4.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeIndGrd4.Properties.ShowIcon = false;
            this.MeeIndGrd4.Size = new System.Drawing.Size(220, 20);
            this.MeeIndGrd4.TabIndex = 53;
            this.MeeIndGrd4.ToolTip = "F4 : Show/hide text";
            this.MeeIndGrd4.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeIndGrd4.ToolTipTitle = "Run System";
            this.MeeIndGrd4.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeIndGrd4_KeyDown);
            this.MeeIndGrd4.Leave += new System.EventHandler(this.MeeIndGrd4_Leave);
            // 
            // MeeCompGrd4
            // 
            this.MeeCompGrd4.EnterMoveNextControl = true;
            this.MeeCompGrd4.Location = new System.Drawing.Point(24, 119);
            this.MeeCompGrd4.Margin = new System.Windows.Forms.Padding(5);
            this.MeeCompGrd4.Name = "MeeCompGrd4";
            this.MeeCompGrd4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd4.Properties.Appearance.Options.UseFont = true;
            this.MeeCompGrd4.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd4.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeCompGrd4.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd4.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeCompGrd4.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd4.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeCompGrd4.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd4.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeCompGrd4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeCompGrd4.Properties.MaxLength = 400;
            this.MeeCompGrd4.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeCompGrd4.Properties.ShowIcon = false;
            this.MeeCompGrd4.Size = new System.Drawing.Size(180, 20);
            this.MeeCompGrd4.TabIndex = 52;
            this.MeeCompGrd4.ToolTip = "F4 : Show/hide text";
            this.MeeCompGrd4.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeCompGrd4.ToolTipTitle = "Run System";
            this.MeeCompGrd4.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeCompGrd4_KeyDown);
            this.MeeCompGrd4.Leave += new System.EventHandler(this.MeeCompGrd4_Leave);
            // 
            // Grd4
            // 
            this.Grd4.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd4.DefaultRow.Height = 20;
            this.Grd4.DefaultRow.Sortable = false;
            this.Grd4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd4.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd4.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd4.Header.Height = 21;
            this.Grd4.Location = new System.Drawing.Point(0, 32);
            this.Grd4.Name = "Grd4";
            this.Grd4.RowHeader.Visible = true;
            this.Grd4.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd4.SingleClickEdit = true;
            this.Grd4.Size = new System.Drawing.Size(839, 233);
            this.Grd4.TabIndex = 51;
            this.Grd4.TreeCol = null;
            this.Grd4.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd4.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd4_ColHdrDoubleClick);
            this.Grd4.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd4_RequestEdit);
            this.Grd4.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd4_KeyDown);
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel11.Controls.Add(this.LueLevel_4);
            this.panel11.Controls.Add(this.label7);
            this.panel11.Controls.Add(this.LueComp4);
            this.panel11.Controls.Add(this.label19);
            this.panel11.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel11.Location = new System.Drawing.Point(0, 0);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(839, 32);
            this.panel11.TabIndex = 44;
            // 
            // LueLevel_4
            // 
            this.LueLevel_4.EnterMoveNextControl = true;
            this.LueLevel_4.Location = new System.Drawing.Point(468, 5);
            this.LueLevel_4.Margin = new System.Windows.Forms.Padding(5);
            this.LueLevel_4.Name = "LueLevel_4";
            this.LueLevel_4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_4.Properties.Appearance.Options.UseFont = true;
            this.LueLevel_4.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_4.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueLevel_4.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_4.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueLevel_4.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_4.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueLevel_4.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_4.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueLevel_4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueLevel_4.Properties.DropDownRows = 20;
            this.LueLevel_4.Properties.NullText = "[Empty]";
            this.LueLevel_4.Properties.PopupWidth = 500;
            this.LueLevel_4.Size = new System.Drawing.Size(84, 20);
            this.LueLevel_4.TabIndex = 55;
            this.LueLevel_4.ToolTip = "F4 : Show/hide list";
            this.LueLevel_4.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueLevel_4.EditValueChanged += new System.EventHandler(this.LueLevel4_EditValueChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Red;
            this.label7.Location = new System.Drawing.Point(427, 8);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(35, 14);
            this.label7.TabIndex = 54;
            this.label7.Text = "Level";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueComp4
            // 
            this.LueComp4.EnterMoveNextControl = true;
            this.LueComp4.Location = new System.Drawing.Point(89, 6);
            this.LueComp4.Margin = new System.Windows.Forms.Padding(5);
            this.LueComp4.Name = "LueComp4";
            this.LueComp4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp4.Properties.Appearance.Options.UseFont = true;
            this.LueComp4.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp4.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueComp4.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp4.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueComp4.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp4.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueComp4.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp4.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueComp4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueComp4.Properties.DropDownRows = 20;
            this.LueComp4.Properties.NullText = "[Empty]";
            this.LueComp4.Properties.PopupWidth = 500;
            this.LueComp4.Size = new System.Drawing.Size(330, 20);
            this.LueComp4.TabIndex = 46;
            this.LueComp4.ToolTip = "F4 : Show/hide list";
            this.LueComp4.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueComp4.EditValueChanged += new System.EventHandler(this.LueComp4_EditValueChanged);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Red;
            this.label19.Location = new System.Drawing.Point(8, 9);
            this.label19.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(77, 14);
            this.label19.TabIndex = 45;
            this.label19.Text = "Competence";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.MeeRemarkGrd3);
            this.tabPage3.Controls.Add(this.MeeEviGrd3);
            this.tabPage3.Controls.Add(this.MeeIndGrd3);
            this.tabPage3.Controls.Add(this.MeeCompGrd3);
            this.tabPage3.Controls.Add(this.Grd3);
            this.tabPage3.Controls.Add(this.panel10);
            this.tabPage3.Location = new System.Drawing.Point(4, 23);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(839, 265);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "No.3";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // MeeRemarkGrd3
            // 
            this.MeeRemarkGrd3.EnterMoveNextControl = true;
            this.MeeRemarkGrd3.Location = new System.Drawing.Point(594, 172);
            this.MeeRemarkGrd3.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemarkGrd3.Name = "MeeRemarkGrd3";
            this.MeeRemarkGrd3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd3.Properties.Appearance.Options.UseFont = true;
            this.MeeRemarkGrd3.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd3.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemarkGrd3.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd3.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemarkGrd3.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd3.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemarkGrd3.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd3.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemarkGrd3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemarkGrd3.Properties.MaxLength = 400;
            this.MeeRemarkGrd3.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeRemarkGrd3.Properties.ShowIcon = false;
            this.MeeRemarkGrd3.Size = new System.Drawing.Size(220, 20);
            this.MeeRemarkGrd3.TabIndex = 55;
            this.MeeRemarkGrd3.ToolTip = "F4 : Show/hide text";
            this.MeeRemarkGrd3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemarkGrd3.ToolTipTitle = "Run System";
            this.MeeRemarkGrd3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeRemarkGrd3_KeyDown);
            this.MeeRemarkGrd3.Leave += new System.EventHandler(this.MeeRemarkGrd3_Leave);
            // 
            // MeeEviGrd3
            // 
            this.MeeEviGrd3.EnterMoveNextControl = true;
            this.MeeEviGrd3.Location = new System.Drawing.Point(393, 170);
            this.MeeEviGrd3.Margin = new System.Windows.Forms.Padding(5);
            this.MeeEviGrd3.Name = "MeeEviGrd3";
            this.MeeEviGrd3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd3.Properties.Appearance.Options.UseFont = true;
            this.MeeEviGrd3.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd3.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeEviGrd3.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd3.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeEviGrd3.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd3.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeEviGrd3.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd3.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeEviGrd3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeEviGrd3.Properties.MaxLength = 400;
            this.MeeEviGrd3.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeEviGrd3.Properties.ShowIcon = false;
            this.MeeEviGrd3.Size = new System.Drawing.Size(180, 20);
            this.MeeEviGrd3.TabIndex = 54;
            this.MeeEviGrd3.ToolTip = "F4 : Show/hide text";
            this.MeeEviGrd3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeEviGrd3.ToolTipTitle = "Run System";
            this.MeeEviGrd3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeEviGrd3_KeyDown);
            this.MeeEviGrd3.Leave += new System.EventHandler(this.MeeEviGrd3_Leave);
            // 
            // MeeIndGrd3
            // 
            this.MeeIndGrd3.EnterMoveNextControl = true;
            this.MeeIndGrd3.Location = new System.Drawing.Point(225, 121);
            this.MeeIndGrd3.Margin = new System.Windows.Forms.Padding(5);
            this.MeeIndGrd3.Name = "MeeIndGrd3";
            this.MeeIndGrd3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd3.Properties.Appearance.Options.UseFont = true;
            this.MeeIndGrd3.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd3.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeIndGrd3.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd3.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeIndGrd3.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd3.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeIndGrd3.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd3.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeIndGrd3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeIndGrd3.Properties.MaxLength = 400;
            this.MeeIndGrd3.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeIndGrd3.Properties.ShowIcon = false;
            this.MeeIndGrd3.Size = new System.Drawing.Size(220, 20);
            this.MeeIndGrd3.TabIndex = 53;
            this.MeeIndGrd3.ToolTip = "F4 : Show/hide text";
            this.MeeIndGrd3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeIndGrd3.ToolTipTitle = "Run System";
            this.MeeIndGrd3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeIndGrd3_KeyDown);
            this.MeeIndGrd3.Leave += new System.EventHandler(this.MeeIndGrd3_Leave);
            // 
            // MeeCompGrd3
            // 
            this.MeeCompGrd3.EnterMoveNextControl = true;
            this.MeeCompGrd3.Location = new System.Drawing.Point(24, 119);
            this.MeeCompGrd3.Margin = new System.Windows.Forms.Padding(5);
            this.MeeCompGrd3.Name = "MeeCompGrd3";
            this.MeeCompGrd3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd3.Properties.Appearance.Options.UseFont = true;
            this.MeeCompGrd3.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd3.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeCompGrd3.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd3.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeCompGrd3.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd3.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeCompGrd3.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd3.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeCompGrd3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeCompGrd3.Properties.MaxLength = 400;
            this.MeeCompGrd3.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeCompGrd3.Properties.ShowIcon = false;
            this.MeeCompGrd3.Size = new System.Drawing.Size(180, 20);
            this.MeeCompGrd3.TabIndex = 52;
            this.MeeCompGrd3.ToolTip = "F4 : Show/hide text";
            this.MeeCompGrd3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeCompGrd3.ToolTipTitle = "Run System";
            this.MeeCompGrd3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeCompGrd3_KeyDown);
            this.MeeCompGrd3.Leave += new System.EventHandler(this.MeeCompGrd3_Leave);
            // 
            // Grd3
            // 
            this.Grd3.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd3.DefaultRow.Height = 20;
            this.Grd3.DefaultRow.Sortable = false;
            this.Grd3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd3.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd3.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd3.Header.Height = 21;
            this.Grd3.Location = new System.Drawing.Point(0, 34);
            this.Grd3.Name = "Grd3";
            this.Grd3.RowHeader.Visible = true;
            this.Grd3.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd3.SingleClickEdit = true;
            this.Grd3.Size = new System.Drawing.Size(839, 231);
            this.Grd3.TabIndex = 51;
            this.Grd3.TreeCol = null;
            this.Grd3.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd3.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd3_ColHdrDoubleClick);
            this.Grd3.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd3_RequestEdit);
            this.Grd3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd3_KeyDown);
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel10.Controls.Add(this.LueLevel_3);
            this.panel10.Controls.Add(this.label6);
            this.panel10.Controls.Add(this.LueComp3);
            this.panel10.Controls.Add(this.label15);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel10.Location = new System.Drawing.Point(0, 0);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(839, 34);
            this.panel10.TabIndex = 44;
            // 
            // LueLevel_3
            // 
            this.LueLevel_3.EnterMoveNextControl = true;
            this.LueLevel_3.Location = new System.Drawing.Point(470, 9);
            this.LueLevel_3.Margin = new System.Windows.Forms.Padding(5);
            this.LueLevel_3.Name = "LueLevel_3";
            this.LueLevel_3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_3.Properties.Appearance.Options.UseFont = true;
            this.LueLevel_3.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_3.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueLevel_3.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_3.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueLevel_3.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_3.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueLevel_3.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_3.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueLevel_3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueLevel_3.Properties.DropDownRows = 20;
            this.LueLevel_3.Properties.NullText = "[Empty]";
            this.LueLevel_3.Properties.PopupWidth = 500;
            this.LueLevel_3.Size = new System.Drawing.Size(84, 20);
            this.LueLevel_3.TabIndex = 55;
            this.LueLevel_3.ToolTip = "F4 : Show/hide list";
            this.LueLevel_3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueLevel_3.EditValueChanged += new System.EventHandler(this.LueLevel3_EditValueChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Red;
            this.label6.Location = new System.Drawing.Point(427, 11);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 14);
            this.label6.TabIndex = 54;
            this.label6.Text = "Level";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueComp3
            // 
            this.LueComp3.EnterMoveNextControl = true;
            this.LueComp3.Location = new System.Drawing.Point(89, 8);
            this.LueComp3.Margin = new System.Windows.Forms.Padding(5);
            this.LueComp3.Name = "LueComp3";
            this.LueComp3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp3.Properties.Appearance.Options.UseFont = true;
            this.LueComp3.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp3.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueComp3.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp3.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueComp3.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp3.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueComp3.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp3.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueComp3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueComp3.Properties.DropDownRows = 20;
            this.LueComp3.Properties.NullText = "[Empty]";
            this.LueComp3.Properties.PopupWidth = 500;
            this.LueComp3.Size = new System.Drawing.Size(330, 20);
            this.LueComp3.TabIndex = 46;
            this.LueComp3.ToolTip = "F4 : Show/hide list";
            this.LueComp3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueComp3.EditValueChanged += new System.EventHandler(this.LueComp3_EditValueChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Red;
            this.label15.Location = new System.Drawing.Point(8, 9);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(77, 14);
            this.label15.TabIndex = 45;
            this.label15.Text = "Competence";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.MeeRemarkGrd2);
            this.tabPage2.Controls.Add(this.MeeEviGrd2);
            this.tabPage2.Controls.Add(this.MeeIndGrd2);
            this.tabPage2.Controls.Add(this.MeeCompGrd2);
            this.tabPage2.Controls.Add(this.Grd2);
            this.tabPage2.Controls.Add(this.panel9);
            this.tabPage2.Location = new System.Drawing.Point(4, 23);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(839, 265);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "No.2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // MeeRemarkGrd2
            // 
            this.MeeRemarkGrd2.EnterMoveNextControl = true;
            this.MeeRemarkGrd2.Location = new System.Drawing.Point(594, 172);
            this.MeeRemarkGrd2.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemarkGrd2.Name = "MeeRemarkGrd2";
            this.MeeRemarkGrd2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd2.Properties.Appearance.Options.UseFont = true;
            this.MeeRemarkGrd2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemarkGrd2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemarkGrd2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd2.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemarkGrd2.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd2.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemarkGrd2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemarkGrd2.Properties.MaxLength = 400;
            this.MeeRemarkGrd2.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeRemarkGrd2.Properties.ShowIcon = false;
            this.MeeRemarkGrd2.Size = new System.Drawing.Size(220, 20);
            this.MeeRemarkGrd2.TabIndex = 55;
            this.MeeRemarkGrd2.ToolTip = "F4 : Show/hide text";
            this.MeeRemarkGrd2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemarkGrd2.ToolTipTitle = "Run System";
            this.MeeRemarkGrd2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeRemarkGrd2_KeyDown);
            this.MeeRemarkGrd2.Leave += new System.EventHandler(this.MeeRemarkGrd2_Leave);
            // 
            // MeeEviGrd2
            // 
            this.MeeEviGrd2.EnterMoveNextControl = true;
            this.MeeEviGrd2.Location = new System.Drawing.Point(393, 170);
            this.MeeEviGrd2.Margin = new System.Windows.Forms.Padding(5);
            this.MeeEviGrd2.Name = "MeeEviGrd2";
            this.MeeEviGrd2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd2.Properties.Appearance.Options.UseFont = true;
            this.MeeEviGrd2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeEviGrd2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeEviGrd2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd2.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeEviGrd2.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd2.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeEviGrd2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeEviGrd2.Properties.MaxLength = 400;
            this.MeeEviGrd2.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeEviGrd2.Properties.ShowIcon = false;
            this.MeeEviGrd2.Size = new System.Drawing.Size(180, 20);
            this.MeeEviGrd2.TabIndex = 54;
            this.MeeEviGrd2.ToolTip = "F4 : Show/hide text";
            this.MeeEviGrd2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeEviGrd2.ToolTipTitle = "Run System";
            this.MeeEviGrd2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeEviGrd2_KeyDown);
            this.MeeEviGrd2.Leave += new System.EventHandler(this.MeeEviGrd2_Leave);
            // 
            // MeeIndGrd2
            // 
            this.MeeIndGrd2.EnterMoveNextControl = true;
            this.MeeIndGrd2.Location = new System.Drawing.Point(225, 121);
            this.MeeIndGrd2.Margin = new System.Windows.Forms.Padding(5);
            this.MeeIndGrd2.Name = "MeeIndGrd2";
            this.MeeIndGrd2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd2.Properties.Appearance.Options.UseFont = true;
            this.MeeIndGrd2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeIndGrd2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeIndGrd2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd2.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeIndGrd2.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd2.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeIndGrd2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeIndGrd2.Properties.MaxLength = 400;
            this.MeeIndGrd2.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeIndGrd2.Properties.ShowIcon = false;
            this.MeeIndGrd2.Size = new System.Drawing.Size(220, 20);
            this.MeeIndGrd2.TabIndex = 53;
            this.MeeIndGrd2.ToolTip = "F4 : Show/hide text";
            this.MeeIndGrd2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeIndGrd2.ToolTipTitle = "Run System";
            this.MeeIndGrd2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeIndGrd2_KeyDown);
            this.MeeIndGrd2.Leave += new System.EventHandler(this.MeeIndGrd2_Leave);
            // 
            // MeeCompGrd2
            // 
            this.MeeCompGrd2.EnterMoveNextControl = true;
            this.MeeCompGrd2.Location = new System.Drawing.Point(24, 119);
            this.MeeCompGrd2.Margin = new System.Windows.Forms.Padding(5);
            this.MeeCompGrd2.Name = "MeeCompGrd2";
            this.MeeCompGrd2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd2.Properties.Appearance.Options.UseFont = true;
            this.MeeCompGrd2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeCompGrd2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeCompGrd2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd2.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeCompGrd2.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd2.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeCompGrd2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeCompGrd2.Properties.MaxLength = 400;
            this.MeeCompGrd2.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeCompGrd2.Properties.ShowIcon = false;
            this.MeeCompGrd2.Size = new System.Drawing.Size(180, 20);
            this.MeeCompGrd2.TabIndex = 52;
            this.MeeCompGrd2.ToolTip = "F4 : Show/hide text";
            this.MeeCompGrd2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeCompGrd2.ToolTipTitle = "Run System";
            this.MeeCompGrd2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeCompGrd2_KeyDown);
            this.MeeCompGrd2.Leave += new System.EventHandler(this.MeeCompGrd2_Leave);
            // 
            // Grd2
            // 
            this.Grd2.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd2.DefaultRow.Height = 20;
            this.Grd2.DefaultRow.Sortable = false;
            this.Grd2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd2.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd2.Header.Height = 21;
            this.Grd2.Location = new System.Drawing.Point(3, 35);
            this.Grd2.Name = "Grd2";
            this.Grd2.RowHeader.Visible = true;
            this.Grd2.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd2.SingleClickEdit = true;
            this.Grd2.Size = new System.Drawing.Size(833, 227);
            this.Grd2.TabIndex = 51;
            this.Grd2.TreeCol = null;
            this.Grd2.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd2.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd2_ColHdrDoubleClick);
            this.Grd2.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd2_RequestEdit);
            this.Grd2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd2_KeyDown);
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel9.Controls.Add(this.LueLevel_2);
            this.panel9.Controls.Add(this.label3);
            this.panel9.Controls.Add(this.LueComp2);
            this.panel9.Controls.Add(this.label13);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel9.Location = new System.Drawing.Point(3, 3);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(833, 32);
            this.panel9.TabIndex = 44;
            // 
            // LueLevel_2
            // 
            this.LueLevel_2.EnterMoveNextControl = true;
            this.LueLevel_2.Location = new System.Drawing.Point(469, 6);
            this.LueLevel_2.Margin = new System.Windows.Forms.Padding(5);
            this.LueLevel_2.Name = "LueLevel_2";
            this.LueLevel_2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_2.Properties.Appearance.Options.UseFont = true;
            this.LueLevel_2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueLevel_2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueLevel_2.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_2.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueLevel_2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_2.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueLevel_2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueLevel_2.Properties.DropDownRows = 20;
            this.LueLevel_2.Properties.NullText = "[Empty]";
            this.LueLevel_2.Properties.PopupWidth = 500;
            this.LueLevel_2.Size = new System.Drawing.Size(84, 20);
            this.LueLevel_2.TabIndex = 55;
            this.LueLevel_2.ToolTip = "F4 : Show/hide list";
            this.LueLevel_2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueLevel_2.EditValueChanged += new System.EventHandler(this.LueLevel2_EditValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(428, 9);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 14);
            this.label3.TabIndex = 54;
            this.label3.Text = "Level";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueComp2
            // 
            this.LueComp2.EnterMoveNextControl = true;
            this.LueComp2.Location = new System.Drawing.Point(91, 6);
            this.LueComp2.Margin = new System.Windows.Forms.Padding(5);
            this.LueComp2.Name = "LueComp2";
            this.LueComp2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp2.Properties.Appearance.Options.UseFont = true;
            this.LueComp2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueComp2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueComp2.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp2.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueComp2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp2.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueComp2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueComp2.Properties.DropDownRows = 20;
            this.LueComp2.Properties.NullText = "[Empty]";
            this.LueComp2.Properties.PopupWidth = 500;
            this.LueComp2.Size = new System.Drawing.Size(330, 20);
            this.LueComp2.TabIndex = 46;
            this.LueComp2.ToolTip = "F4 : Show/hide list";
            this.LueComp2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueComp2.EditValueChanged += new System.EventHandler(this.LueComp2_EditValueChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Red;
            this.label13.Location = new System.Drawing.Point(9, 9);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(77, 14);
            this.label13.TabIndex = 45;
            this.label13.Text = "Competence";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.MeeRemarkGrd1);
            this.tabPage1.Controls.Add(this.MeeEviGrd1);
            this.tabPage1.Controls.Add(this.MeeIndGrd1);
            this.tabPage1.Controls.Add(this.MeeCompGrd1);
            this.tabPage1.Controls.Add(this.Grd1);
            this.tabPage1.Controls.Add(this.panel8);
            this.tabPage1.Location = new System.Drawing.Point(4, 23);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(839, 265);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "No.1";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // MeeRemarkGrd1
            // 
            this.MeeRemarkGrd1.EnterMoveNextControl = true;
            this.MeeRemarkGrd1.Location = new System.Drawing.Point(593, 134);
            this.MeeRemarkGrd1.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemarkGrd1.Name = "MeeRemarkGrd1";
            this.MeeRemarkGrd1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd1.Properties.Appearance.Options.UseFont = true;
            this.MeeRemarkGrd1.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd1.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemarkGrd1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemarkGrd1.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd1.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemarkGrd1.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd1.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemarkGrd1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemarkGrd1.Properties.MaxLength = 400;
            this.MeeRemarkGrd1.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeRemarkGrd1.Properties.ShowIcon = false;
            this.MeeRemarkGrd1.Size = new System.Drawing.Size(220, 20);
            this.MeeRemarkGrd1.TabIndex = 35;
            this.MeeRemarkGrd1.ToolTip = "F4 : Show/hide text";
            this.MeeRemarkGrd1.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemarkGrd1.ToolTipTitle = "Run System";
            this.MeeRemarkGrd1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeRemarkGrd1_KeyDown);
            this.MeeRemarkGrd1.Leave += new System.EventHandler(this.MeeRemarkGrd1_Leave);
            // 
            // MeeEviGrd1
            // 
            this.MeeEviGrd1.EnterMoveNextControl = true;
            this.MeeEviGrd1.Location = new System.Drawing.Point(392, 132);
            this.MeeEviGrd1.Margin = new System.Windows.Forms.Padding(5);
            this.MeeEviGrd1.Name = "MeeEviGrd1";
            this.MeeEviGrd1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd1.Properties.Appearance.Options.UseFont = true;
            this.MeeEviGrd1.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd1.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeEviGrd1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeEviGrd1.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd1.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeEviGrd1.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd1.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeEviGrd1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeEviGrd1.Properties.MaxLength = 400;
            this.MeeEviGrd1.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeEviGrd1.Properties.ShowIcon = false;
            this.MeeEviGrd1.Size = new System.Drawing.Size(180, 20);
            this.MeeEviGrd1.TabIndex = 34;
            this.MeeEviGrd1.ToolTip = "F4 : Show/hide text";
            this.MeeEviGrd1.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeEviGrd1.ToolTipTitle = "Run System";
            this.MeeEviGrd1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeEviGrd1_KeyDown);
            this.MeeEviGrd1.Leave += new System.EventHandler(this.MeeEviGrd1_Leave);
            // 
            // MeeIndGrd1
            // 
            this.MeeIndGrd1.EnterMoveNextControl = true;
            this.MeeIndGrd1.Location = new System.Drawing.Point(224, 83);
            this.MeeIndGrd1.Margin = new System.Windows.Forms.Padding(5);
            this.MeeIndGrd1.Name = "MeeIndGrd1";
            this.MeeIndGrd1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd1.Properties.Appearance.Options.UseFont = true;
            this.MeeIndGrd1.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd1.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeIndGrd1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeIndGrd1.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd1.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeIndGrd1.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd1.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeIndGrd1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeIndGrd1.Properties.MaxLength = 400;
            this.MeeIndGrd1.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeIndGrd1.Properties.ShowIcon = false;
            this.MeeIndGrd1.Size = new System.Drawing.Size(220, 20);
            this.MeeIndGrd1.TabIndex = 33;
            this.MeeIndGrd1.ToolTip = "F4 : Show/hide text";
            this.MeeIndGrd1.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeIndGrd1.ToolTipTitle = "Run System";
            this.MeeIndGrd1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeIndGrd1_KeyDown);
            this.MeeIndGrd1.Leave += new System.EventHandler(this.MeeIndGrd1_Leave);
            // 
            // MeeCompGrd1
            // 
            this.MeeCompGrd1.EnterMoveNextControl = true;
            this.MeeCompGrd1.Location = new System.Drawing.Point(23, 81);
            this.MeeCompGrd1.Margin = new System.Windows.Forms.Padding(5);
            this.MeeCompGrd1.Name = "MeeCompGrd1";
            this.MeeCompGrd1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd1.Properties.Appearance.Options.UseFont = true;
            this.MeeCompGrd1.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd1.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeCompGrd1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeCompGrd1.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd1.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeCompGrd1.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd1.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeCompGrd1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeCompGrd1.Properties.MaxLength = 400;
            this.MeeCompGrd1.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeCompGrd1.Properties.ShowIcon = false;
            this.MeeCompGrd1.Size = new System.Drawing.Size(180, 20);
            this.MeeCompGrd1.TabIndex = 32;
            this.MeeCompGrd1.ToolTip = "F4 : Show/hide text";
            this.MeeCompGrd1.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeCompGrd1.ToolTipTitle = "Run System";
            this.MeeCompGrd1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeCompGrd1_KeyDown);
            this.MeeCompGrd1.Leave += new System.EventHandler(this.MeeCompGrd1_Leave);
            // 
            // Grd1
            // 
            this.Grd1.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd1.DefaultRow.Height = 20;
            this.Grd1.DefaultRow.Sortable = false;
            this.Grd1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 21;
            this.Grd1.Location = new System.Drawing.Point(3, 33);
            this.Grd1.Name = "Grd1";
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd1.SingleClickEdit = true;
            this.Grd1.Size = new System.Drawing.Size(833, 229);
            this.Grd1.TabIndex = 31;
            this.Grd1.TreeCol = null;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd1_ColHdrDoubleClick);
            this.Grd1.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd1_RequestEdit);
            this.Grd1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd1_KeyDown);
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel8.Controls.Add(this.LueLevel_1);
            this.panel8.Controls.Add(this.label2);
            this.panel8.Controls.Add(this.LueComp1);
            this.panel8.Controls.Add(this.label11);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel8.Location = new System.Drawing.Point(3, 3);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(833, 30);
            this.panel8.TabIndex = 44;
            // 
            // LueLevel_1
            // 
            this.LueLevel_1.EnterMoveNextControl = true;
            this.LueLevel_1.Location = new System.Drawing.Point(477, 4);
            this.LueLevel_1.Margin = new System.Windows.Forms.Padding(5);
            this.LueLevel_1.Name = "LueLevel_1";
            this.LueLevel_1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_1.Properties.Appearance.Options.UseFont = true;
            this.LueLevel_1.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_1.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueLevel_1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueLevel_1.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_1.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueLevel_1.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_1.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueLevel_1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueLevel_1.Properties.DropDownRows = 20;
            this.LueLevel_1.Properties.NullText = "[Empty]";
            this.LueLevel_1.Properties.PopupWidth = 500;
            this.LueLevel_1.Size = new System.Drawing.Size(84, 20);
            this.LueLevel_1.TabIndex = 30;
            this.LueLevel_1.ToolTip = "F4 : Show/hide list";
            this.LueLevel_1.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueLevel_1.EditValueChanged += new System.EventHandler(this.LueLevel1_EditValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(436, 7);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 14);
            this.label2.TabIndex = 29;
            this.label2.Text = "Level";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueComp1
            // 
            this.LueComp1.EnterMoveNextControl = true;
            this.LueComp1.Location = new System.Drawing.Point(97, 4);
            this.LueComp1.Margin = new System.Windows.Forms.Padding(5);
            this.LueComp1.Name = "LueComp1";
            this.LueComp1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp1.Properties.Appearance.Options.UseFont = true;
            this.LueComp1.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp1.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueComp1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueComp1.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp1.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueComp1.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp1.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueComp1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueComp1.Properties.DropDownRows = 20;
            this.LueComp1.Properties.NullText = "[Empty]";
            this.LueComp1.Properties.PopupWidth = 500;
            this.LueComp1.Size = new System.Drawing.Size(330, 20);
            this.LueComp1.TabIndex = 28;
            this.LueComp1.ToolTip = "F4 : Show/hide list";
            this.LueComp1.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueComp1.EditValueChanged += new System.EventHandler(this.LueComp1_EditValueChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Red;
            this.label11.Location = new System.Drawing.Point(13, 7);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(77, 14);
            this.label11.TabIndex = 27;
            this.label11.Text = "Competence";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Controls.Add(this.tabPage7);
            this.tabControl1.Controls.Add(this.tabPage8);
            this.tabControl1.Controls.Add(this.tabPage9);
            this.tabControl1.Controls.Add(this.tabPage10);
            this.tabControl1.Controls.Add(this.tabPage11);
            this.tabControl1.Controls.Add(this.tabPage12);
            this.tabControl1.Controls.Add(this.tabPage13);
            this.tabControl1.Controls.Add(this.tabPage14);
            this.tabControl1.Controls.Add(this.tabPage15);
            this.tabControl1.Controls.Add(this.tabPage16);
            this.tabControl1.Controls.Add(this.tabPage17);
            this.tabControl1.Controls.Add(this.tabPage18);
            this.tabControl1.Controls.Add(this.tabPage19);
            this.tabControl1.Controls.Add(this.tabPage20);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 169);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(847, 292);
            this.tabControl1.TabIndex = 26;
            // 
            // tabPage16
            // 
            this.tabPage16.Controls.Add(this.MeeRemarkGrd16);
            this.tabPage16.Controls.Add(this.MeeEviGrd16);
            this.tabPage16.Controls.Add(this.MeeIndGrd16);
            this.tabPage16.Controls.Add(this.MeeCompGrd16);
            this.tabPage16.Controls.Add(this.Grd16);
            this.tabPage16.Controls.Add(this.panel4);
            this.tabPage16.Location = new System.Drawing.Point(4, 23);
            this.tabPage16.Name = "tabPage16";
            this.tabPage16.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage16.Size = new System.Drawing.Size(764, 37);
            this.tabPage16.TabIndex = 15;
            this.tabPage16.Text = "No.16";
            this.tabPage16.UseVisualStyleBackColor = true;
            // 
            // MeeRemarkGrd16
            // 
            this.MeeRemarkGrd16.EnterMoveNextControl = true;
            this.MeeRemarkGrd16.Location = new System.Drawing.Point(594, 160);
            this.MeeRemarkGrd16.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemarkGrd16.Name = "MeeRemarkGrd16";
            this.MeeRemarkGrd16.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd16.Properties.Appearance.Options.UseFont = true;
            this.MeeRemarkGrd16.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd16.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemarkGrd16.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd16.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemarkGrd16.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd16.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemarkGrd16.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd16.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemarkGrd16.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemarkGrd16.Properties.MaxLength = 400;
            this.MeeRemarkGrd16.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeRemarkGrd16.Properties.ShowIcon = false;
            this.MeeRemarkGrd16.Size = new System.Drawing.Size(220, 20);
            this.MeeRemarkGrd16.TabIndex = 59;
            this.MeeRemarkGrd16.ToolTip = "F4 : Show/hide text";
            this.MeeRemarkGrd16.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemarkGrd16.ToolTipTitle = "Run System";
            this.MeeRemarkGrd16.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeRemarkGrd16_KeyDown);
            this.MeeRemarkGrd16.Leave += new System.EventHandler(this.MeeRemarkGrd16_Leave);
            // 
            // MeeEviGrd16
            // 
            this.MeeEviGrd16.EnterMoveNextControl = true;
            this.MeeEviGrd16.Location = new System.Drawing.Point(393, 158);
            this.MeeEviGrd16.Margin = new System.Windows.Forms.Padding(5);
            this.MeeEviGrd16.Name = "MeeEviGrd16";
            this.MeeEviGrd16.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd16.Properties.Appearance.Options.UseFont = true;
            this.MeeEviGrd16.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd16.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeEviGrd16.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd16.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeEviGrd16.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd16.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeEviGrd16.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd16.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeEviGrd16.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeEviGrd16.Properties.MaxLength = 400;
            this.MeeEviGrd16.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeEviGrd16.Properties.ShowIcon = false;
            this.MeeEviGrd16.Size = new System.Drawing.Size(180, 20);
            this.MeeEviGrd16.TabIndex = 58;
            this.MeeEviGrd16.ToolTip = "F4 : Show/hide text";
            this.MeeEviGrd16.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeEviGrd16.ToolTipTitle = "Run System";
            this.MeeEviGrd16.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeEviGrd16_KeyDown);
            this.MeeEviGrd16.Leave += new System.EventHandler(this.MeeEviGrd16_Leave);
            // 
            // MeeIndGrd16
            // 
            this.MeeIndGrd16.EnterMoveNextControl = true;
            this.MeeIndGrd16.Location = new System.Drawing.Point(225, 109);
            this.MeeIndGrd16.Margin = new System.Windows.Forms.Padding(5);
            this.MeeIndGrd16.Name = "MeeIndGrd16";
            this.MeeIndGrd16.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd16.Properties.Appearance.Options.UseFont = true;
            this.MeeIndGrd16.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd16.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeIndGrd16.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd16.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeIndGrd16.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd16.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeIndGrd16.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd16.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeIndGrd16.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeIndGrd16.Properties.MaxLength = 400;
            this.MeeIndGrd16.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeIndGrd16.Properties.ShowIcon = false;
            this.MeeIndGrd16.Size = new System.Drawing.Size(220, 20);
            this.MeeIndGrd16.TabIndex = 57;
            this.MeeIndGrd16.ToolTip = "F4 : Show/hide text";
            this.MeeIndGrd16.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeIndGrd16.ToolTipTitle = "Run System";
            this.MeeIndGrd16.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeIndGrd16_KeyDown);
            this.MeeIndGrd16.Leave += new System.EventHandler(this.MeeIndGrd16_Leave);
            // 
            // MeeCompGrd16
            // 
            this.MeeCompGrd16.EnterMoveNextControl = true;
            this.MeeCompGrd16.Location = new System.Drawing.Point(24, 107);
            this.MeeCompGrd16.Margin = new System.Windows.Forms.Padding(5);
            this.MeeCompGrd16.Name = "MeeCompGrd16";
            this.MeeCompGrd16.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd16.Properties.Appearance.Options.UseFont = true;
            this.MeeCompGrd16.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd16.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeCompGrd16.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd16.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeCompGrd16.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd16.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeCompGrd16.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd16.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeCompGrd16.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeCompGrd16.Properties.MaxLength = 400;
            this.MeeCompGrd16.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeCompGrd16.Properties.ShowIcon = false;
            this.MeeCompGrd16.Size = new System.Drawing.Size(180, 20);
            this.MeeCompGrd16.TabIndex = 56;
            this.MeeCompGrd16.ToolTip = "F4 : Show/hide text";
            this.MeeCompGrd16.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeCompGrd16.ToolTipTitle = "Run System";
            this.MeeCompGrd16.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeCompGrd16_KeyDown);
            this.MeeCompGrd16.Leave += new System.EventHandler(this.MeeCompGrd16_Leave);
            // 
            // Grd16
            // 
            this.Grd16.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd16.DefaultRow.Height = 20;
            this.Grd16.DefaultRow.Sortable = false;
            this.Grd16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd16.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd16.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd16.Header.Height = 21;
            this.Grd16.Location = new System.Drawing.Point(3, 36);
            this.Grd16.Name = "Grd16";
            this.Grd16.RowHeader.Visible = true;
            this.Grd16.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd16.SingleClickEdit = true;
            this.Grd16.Size = new System.Drawing.Size(758, 0);
            this.Grd16.TabIndex = 52;
            this.Grd16.TreeCol = null;
            this.Grd16.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd16.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd16_ColHdrDoubleClick);
            this.Grd16.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd16_RequestEdit);
            this.Grd16.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd16_KeyDown);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel4.Controls.Add(this.LueLevel_16);
            this.panel4.Controls.Add(this.label33);
            this.panel4.Controls.Add(this.LueComp16);
            this.panel4.Controls.Add(this.label35);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(3, 3);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(758, 33);
            this.panel4.TabIndex = 45;
            // 
            // LueLevel_16
            // 
            this.LueLevel_16.EnterMoveNextControl = true;
            this.LueLevel_16.Location = new System.Drawing.Point(468, 6);
            this.LueLevel_16.Margin = new System.Windows.Forms.Padding(5);
            this.LueLevel_16.Name = "LueLevel_16";
            this.LueLevel_16.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_16.Properties.Appearance.Options.UseFont = true;
            this.LueLevel_16.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_16.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueLevel_16.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_16.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueLevel_16.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_16.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueLevel_16.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_16.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueLevel_16.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueLevel_16.Properties.DropDownRows = 20;
            this.LueLevel_16.Properties.NullText = "[Empty]";
            this.LueLevel_16.Properties.PopupWidth = 500;
            this.LueLevel_16.Size = new System.Drawing.Size(84, 20);
            this.LueLevel_16.TabIndex = 55;
            this.LueLevel_16.ToolTip = "F4 : Show/hide list";
            this.LueLevel_16.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueLevel_16.EditValueChanged += new System.EventHandler(this.LueLevel16_EditValueChanged);
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.Red;
            this.label33.Location = new System.Drawing.Point(427, 9);
            this.label33.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(35, 14);
            this.label33.TabIndex = 54;
            this.label33.Text = "Level";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueComp16
            // 
            this.LueComp16.EnterMoveNextControl = true;
            this.LueComp16.Location = new System.Drawing.Point(90, 6);
            this.LueComp16.Margin = new System.Windows.Forms.Padding(5);
            this.LueComp16.Name = "LueComp16";
            this.LueComp16.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp16.Properties.Appearance.Options.UseFont = true;
            this.LueComp16.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp16.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueComp16.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp16.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueComp16.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp16.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueComp16.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp16.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueComp16.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueComp16.Properties.DropDownRows = 20;
            this.LueComp16.Properties.NullText = "[Empty]";
            this.LueComp16.Properties.PopupWidth = 500;
            this.LueComp16.Size = new System.Drawing.Size(330, 20);
            this.LueComp16.TabIndex = 46;
            this.LueComp16.ToolTip = "F4 : Show/hide list";
            this.LueComp16.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueComp16.EditValueChanged += new System.EventHandler(this.LueComp16_EditValueChanged);
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.Color.Red;
            this.label35.Location = new System.Drawing.Point(8, 9);
            this.label35.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(77, 14);
            this.label35.TabIndex = 45;
            this.label35.Text = "Competence";
            this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage17
            // 
            this.tabPage17.Controls.Add(this.MeeRemarkGrd17);
            this.tabPage17.Controls.Add(this.MeeEviGrd17);
            this.tabPage17.Controls.Add(this.MeeIndGrd17);
            this.tabPage17.Controls.Add(this.MeeCompGrd17);
            this.tabPage17.Controls.Add(this.Grd17);
            this.tabPage17.Controls.Add(this.panel6);
            this.tabPage17.Location = new System.Drawing.Point(4, 23);
            this.tabPage17.Name = "tabPage17";
            this.tabPage17.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage17.Size = new System.Drawing.Size(764, 37);
            this.tabPage17.TabIndex = 16;
            this.tabPage17.Text = "No.17";
            this.tabPage17.UseVisualStyleBackColor = true;
            // 
            // MeeRemarkGrd17
            // 
            this.MeeRemarkGrd17.EnterMoveNextControl = true;
            this.MeeRemarkGrd17.Location = new System.Drawing.Point(594, 160);
            this.MeeRemarkGrd17.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemarkGrd17.Name = "MeeRemarkGrd17";
            this.MeeRemarkGrd17.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd17.Properties.Appearance.Options.UseFont = true;
            this.MeeRemarkGrd17.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd17.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemarkGrd17.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd17.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemarkGrd17.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd17.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemarkGrd17.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd17.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemarkGrd17.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemarkGrd17.Properties.MaxLength = 400;
            this.MeeRemarkGrd17.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeRemarkGrd17.Properties.ShowIcon = false;
            this.MeeRemarkGrd17.Size = new System.Drawing.Size(220, 20);
            this.MeeRemarkGrd17.TabIndex = 59;
            this.MeeRemarkGrd17.ToolTip = "F4 : Show/hide text";
            this.MeeRemarkGrd17.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemarkGrd17.ToolTipTitle = "Run System";
            this.MeeRemarkGrd17.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeRemarkGrd17_KeyDown);
            this.MeeRemarkGrd17.Leave += new System.EventHandler(this.MeeRemarkGrd17_Leave);
            // 
            // MeeEviGrd17
            // 
            this.MeeEviGrd17.EnterMoveNextControl = true;
            this.MeeEviGrd17.Location = new System.Drawing.Point(393, 158);
            this.MeeEviGrd17.Margin = new System.Windows.Forms.Padding(5);
            this.MeeEviGrd17.Name = "MeeEviGrd17";
            this.MeeEviGrd17.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd17.Properties.Appearance.Options.UseFont = true;
            this.MeeEviGrd17.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd17.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeEviGrd17.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd17.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeEviGrd17.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd17.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeEviGrd17.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd17.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeEviGrd17.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeEviGrd17.Properties.MaxLength = 400;
            this.MeeEviGrd17.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeEviGrd17.Properties.ShowIcon = false;
            this.MeeEviGrd17.Size = new System.Drawing.Size(180, 20);
            this.MeeEviGrd17.TabIndex = 58;
            this.MeeEviGrd17.ToolTip = "F4 : Show/hide text";
            this.MeeEviGrd17.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeEviGrd17.ToolTipTitle = "Run System";
            this.MeeEviGrd17.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeEviGrd17_KeyDown);
            this.MeeEviGrd17.Leave += new System.EventHandler(this.MeeEviGrd17_Leave);
            // 
            // MeeIndGrd17
            // 
            this.MeeIndGrd17.EnterMoveNextControl = true;
            this.MeeIndGrd17.Location = new System.Drawing.Point(225, 109);
            this.MeeIndGrd17.Margin = new System.Windows.Forms.Padding(5);
            this.MeeIndGrd17.Name = "MeeIndGrd17";
            this.MeeIndGrd17.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd17.Properties.Appearance.Options.UseFont = true;
            this.MeeIndGrd17.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd17.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeIndGrd17.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd17.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeIndGrd17.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd17.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeIndGrd17.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd17.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeIndGrd17.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeIndGrd17.Properties.MaxLength = 400;
            this.MeeIndGrd17.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeIndGrd17.Properties.ShowIcon = false;
            this.MeeIndGrd17.Size = new System.Drawing.Size(220, 20);
            this.MeeIndGrd17.TabIndex = 57;
            this.MeeIndGrd17.ToolTip = "F4 : Show/hide text";
            this.MeeIndGrd17.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeIndGrd17.ToolTipTitle = "Run System";
            this.MeeIndGrd17.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeIndGrd17_KeyDown);
            this.MeeIndGrd17.Leave += new System.EventHandler(this.MeeIndGrd17_Leave);
            // 
            // MeeCompGrd17
            // 
            this.MeeCompGrd17.EnterMoveNextControl = true;
            this.MeeCompGrd17.Location = new System.Drawing.Point(24, 107);
            this.MeeCompGrd17.Margin = new System.Windows.Forms.Padding(5);
            this.MeeCompGrd17.Name = "MeeCompGrd17";
            this.MeeCompGrd17.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd17.Properties.Appearance.Options.UseFont = true;
            this.MeeCompGrd17.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd17.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeCompGrd17.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd17.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeCompGrd17.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd17.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeCompGrd17.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd17.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeCompGrd17.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeCompGrd17.Properties.MaxLength = 400;
            this.MeeCompGrd17.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeCompGrd17.Properties.ShowIcon = false;
            this.MeeCompGrd17.Size = new System.Drawing.Size(180, 20);
            this.MeeCompGrd17.TabIndex = 56;
            this.MeeCompGrd17.ToolTip = "F4 : Show/hide text";
            this.MeeCompGrd17.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeCompGrd17.ToolTipTitle = "Run System";
            this.MeeCompGrd17.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeCompGrd17_KeyDown);
            this.MeeCompGrd17.Leave += new System.EventHandler(this.MeeCompGrd17_Leave);
            // 
            // Grd17
            // 
            this.Grd17.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd17.DefaultRow.Height = 20;
            this.Grd17.DefaultRow.Sortable = false;
            this.Grd17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd17.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd17.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd17.Header.Height = 21;
            this.Grd17.Location = new System.Drawing.Point(3, 36);
            this.Grd17.Name = "Grd17";
            this.Grd17.RowHeader.Visible = true;
            this.Grd17.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd17.SingleClickEdit = true;
            this.Grd17.Size = new System.Drawing.Size(758, 0);
            this.Grd17.TabIndex = 53;
            this.Grd17.TreeCol = null;
            this.Grd17.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd17.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd17_ColHdrDoubleClick);
            this.Grd17.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd17_RequestEdit);
            this.Grd17.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd17_KeyDown);
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel6.Controls.Add(this.LueLevel_17);
            this.panel6.Controls.Add(this.label37);
            this.panel6.Controls.Add(this.LueComp17);
            this.panel6.Controls.Add(this.label39);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(3, 3);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(758, 33);
            this.panel6.TabIndex = 46;
            // 
            // LueLevel_17
            // 
            this.LueLevel_17.EnterMoveNextControl = true;
            this.LueLevel_17.Location = new System.Drawing.Point(468, 6);
            this.LueLevel_17.Margin = new System.Windows.Forms.Padding(5);
            this.LueLevel_17.Name = "LueLevel_17";
            this.LueLevel_17.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_17.Properties.Appearance.Options.UseFont = true;
            this.LueLevel_17.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_17.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueLevel_17.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_17.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueLevel_17.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_17.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueLevel_17.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_17.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueLevel_17.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueLevel_17.Properties.DropDownRows = 20;
            this.LueLevel_17.Properties.NullText = "[Empty]";
            this.LueLevel_17.Properties.PopupWidth = 500;
            this.LueLevel_17.Size = new System.Drawing.Size(84, 20);
            this.LueLevel_17.TabIndex = 55;
            this.LueLevel_17.ToolTip = "F4 : Show/hide list";
            this.LueLevel_17.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueLevel_17.EditValueChanged += new System.EventHandler(this.LueLevel17_EditValueChanged);
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.ForeColor = System.Drawing.Color.Red;
            this.label37.Location = new System.Drawing.Point(427, 9);
            this.label37.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(35, 14);
            this.label37.TabIndex = 54;
            this.label37.Text = "Level";
            this.label37.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueComp17
            // 
            this.LueComp17.EnterMoveNextControl = true;
            this.LueComp17.Location = new System.Drawing.Point(90, 6);
            this.LueComp17.Margin = new System.Windows.Forms.Padding(5);
            this.LueComp17.Name = "LueComp17";
            this.LueComp17.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp17.Properties.Appearance.Options.UseFont = true;
            this.LueComp17.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp17.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueComp17.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp17.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueComp17.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp17.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueComp17.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp17.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueComp17.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueComp17.Properties.DropDownRows = 20;
            this.LueComp17.Properties.NullText = "[Empty]";
            this.LueComp17.Properties.PopupWidth = 500;
            this.LueComp17.Size = new System.Drawing.Size(330, 20);
            this.LueComp17.TabIndex = 46;
            this.LueComp17.ToolTip = "F4 : Show/hide list";
            this.LueComp17.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueComp17.EditValueChanged += new System.EventHandler(this.LueComp17_EditValueChanged);
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.ForeColor = System.Drawing.Color.Red;
            this.label39.Location = new System.Drawing.Point(8, 9);
            this.label39.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(77, 14);
            this.label39.TabIndex = 45;
            this.label39.Text = "Competence";
            this.label39.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage18
            // 
            this.tabPage18.Controls.Add(this.MeeRemarkGrd18);
            this.tabPage18.Controls.Add(this.MeeEviGrd18);
            this.tabPage18.Controls.Add(this.MeeIndGrd18);
            this.tabPage18.Controls.Add(this.MeeCompGrd18);
            this.tabPage18.Controls.Add(this.Grd18);
            this.tabPage18.Controls.Add(this.panel7);
            this.tabPage18.Location = new System.Drawing.Point(4, 23);
            this.tabPage18.Name = "tabPage18";
            this.tabPage18.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage18.Size = new System.Drawing.Size(764, 37);
            this.tabPage18.TabIndex = 17;
            this.tabPage18.Text = "No.18";
            this.tabPage18.UseVisualStyleBackColor = true;
            // 
            // MeeRemarkGrd18
            // 
            this.MeeRemarkGrd18.EnterMoveNextControl = true;
            this.MeeRemarkGrd18.Location = new System.Drawing.Point(594, 160);
            this.MeeRemarkGrd18.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemarkGrd18.Name = "MeeRemarkGrd18";
            this.MeeRemarkGrd18.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd18.Properties.Appearance.Options.UseFont = true;
            this.MeeRemarkGrd18.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd18.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemarkGrd18.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd18.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemarkGrd18.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd18.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemarkGrd18.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd18.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemarkGrd18.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemarkGrd18.Properties.MaxLength = 400;
            this.MeeRemarkGrd18.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeRemarkGrd18.Properties.ShowIcon = false;
            this.MeeRemarkGrd18.Size = new System.Drawing.Size(220, 20);
            this.MeeRemarkGrd18.TabIndex = 59;
            this.MeeRemarkGrd18.ToolTip = "F4 : Show/hide text";
            this.MeeRemarkGrd18.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemarkGrd18.ToolTipTitle = "Run System";
            this.MeeRemarkGrd18.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeRemarkGrd18_KeyDown);
            this.MeeRemarkGrd18.Leave += new System.EventHandler(this.MeeRemarkGrd18_Leave);
            // 
            // MeeEviGrd18
            // 
            this.MeeEviGrd18.EnterMoveNextControl = true;
            this.MeeEviGrd18.Location = new System.Drawing.Point(393, 158);
            this.MeeEviGrd18.Margin = new System.Windows.Forms.Padding(5);
            this.MeeEviGrd18.Name = "MeeEviGrd18";
            this.MeeEviGrd18.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd18.Properties.Appearance.Options.UseFont = true;
            this.MeeEviGrd18.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd18.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeEviGrd18.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd18.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeEviGrd18.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd18.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeEviGrd18.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd18.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeEviGrd18.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeEviGrd18.Properties.MaxLength = 400;
            this.MeeEviGrd18.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeEviGrd18.Properties.ShowIcon = false;
            this.MeeEviGrd18.Size = new System.Drawing.Size(180, 20);
            this.MeeEviGrd18.TabIndex = 58;
            this.MeeEviGrd18.ToolTip = "F4 : Show/hide text";
            this.MeeEviGrd18.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeEviGrd18.ToolTipTitle = "Run System";
            this.MeeEviGrd18.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeEviGrd18_KeyDown);
            this.MeeEviGrd18.Leave += new System.EventHandler(this.MeeEviGrd18_Leave);
            // 
            // MeeIndGrd18
            // 
            this.MeeIndGrd18.EnterMoveNextControl = true;
            this.MeeIndGrd18.Location = new System.Drawing.Point(225, 109);
            this.MeeIndGrd18.Margin = new System.Windows.Forms.Padding(5);
            this.MeeIndGrd18.Name = "MeeIndGrd18";
            this.MeeIndGrd18.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd18.Properties.Appearance.Options.UseFont = true;
            this.MeeIndGrd18.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd18.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeIndGrd18.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd18.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeIndGrd18.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd18.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeIndGrd18.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd18.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeIndGrd18.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeIndGrd18.Properties.MaxLength = 400;
            this.MeeIndGrd18.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeIndGrd18.Properties.ShowIcon = false;
            this.MeeIndGrd18.Size = new System.Drawing.Size(220, 20);
            this.MeeIndGrd18.TabIndex = 57;
            this.MeeIndGrd18.ToolTip = "F4 : Show/hide text";
            this.MeeIndGrd18.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeIndGrd18.ToolTipTitle = "Run System";
            this.MeeIndGrd18.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeIndGrd18_KeyDown);
            this.MeeIndGrd18.Leave += new System.EventHandler(this.MeeIndGrd18_Leave);
            // 
            // MeeCompGrd18
            // 
            this.MeeCompGrd18.EnterMoveNextControl = true;
            this.MeeCompGrd18.Location = new System.Drawing.Point(24, 107);
            this.MeeCompGrd18.Margin = new System.Windows.Forms.Padding(5);
            this.MeeCompGrd18.Name = "MeeCompGrd18";
            this.MeeCompGrd18.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd18.Properties.Appearance.Options.UseFont = true;
            this.MeeCompGrd18.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd18.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeCompGrd18.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd18.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeCompGrd18.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd18.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeCompGrd18.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd18.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeCompGrd18.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeCompGrd18.Properties.MaxLength = 400;
            this.MeeCompGrd18.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeCompGrd18.Properties.ShowIcon = false;
            this.MeeCompGrd18.Size = new System.Drawing.Size(180, 20);
            this.MeeCompGrd18.TabIndex = 56;
            this.MeeCompGrd18.ToolTip = "F4 : Show/hide text";
            this.MeeCompGrd18.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeCompGrd18.ToolTipTitle = "Run System";
            this.MeeCompGrd18.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeCompGrd18_KeyDown);
            this.MeeCompGrd18.Leave += new System.EventHandler(this.MeeCompGrd18_Leave);
            // 
            // Grd18
            // 
            this.Grd18.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd18.DefaultRow.Height = 20;
            this.Grd18.DefaultRow.Sortable = false;
            this.Grd18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd18.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd18.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd18.Header.Height = 21;
            this.Grd18.Location = new System.Drawing.Point(3, 36);
            this.Grd18.Name = "Grd18";
            this.Grd18.RowHeader.Visible = true;
            this.Grd18.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd18.SingleClickEdit = true;
            this.Grd18.Size = new System.Drawing.Size(758, 0);
            this.Grd18.TabIndex = 53;
            this.Grd18.TreeCol = null;
            this.Grd18.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd18.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd18_ColHdrDoubleClick);
            this.Grd18.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd18_RequestEdit);
            this.Grd18.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd18_KeyDown);
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel7.Controls.Add(this.LueLevel_18);
            this.panel7.Controls.Add(this.label41);
            this.panel7.Controls.Add(this.LueComp18);
            this.panel7.Controls.Add(this.label42);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel7.Location = new System.Drawing.Point(3, 3);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(758, 33);
            this.panel7.TabIndex = 46;
            // 
            // LueLevel_18
            // 
            this.LueLevel_18.EnterMoveNextControl = true;
            this.LueLevel_18.Location = new System.Drawing.Point(468, 6);
            this.LueLevel_18.Margin = new System.Windows.Forms.Padding(5);
            this.LueLevel_18.Name = "LueLevel_18";
            this.LueLevel_18.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_18.Properties.Appearance.Options.UseFont = true;
            this.LueLevel_18.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_18.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueLevel_18.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_18.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueLevel_18.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_18.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueLevel_18.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_18.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueLevel_18.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueLevel_18.Properties.DropDownRows = 20;
            this.LueLevel_18.Properties.NullText = "[Empty]";
            this.LueLevel_18.Properties.PopupWidth = 500;
            this.LueLevel_18.Size = new System.Drawing.Size(84, 20);
            this.LueLevel_18.TabIndex = 55;
            this.LueLevel_18.ToolTip = "F4 : Show/hide list";
            this.LueLevel_18.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueLevel_18.EditValueChanged += new System.EventHandler(this.LueLevel18_EditValueChanged);
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.ForeColor = System.Drawing.Color.Red;
            this.label41.Location = new System.Drawing.Point(427, 9);
            this.label41.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(35, 14);
            this.label41.TabIndex = 54;
            this.label41.Text = "Level";
            this.label41.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueComp18
            // 
            this.LueComp18.EnterMoveNextControl = true;
            this.LueComp18.Location = new System.Drawing.Point(90, 6);
            this.LueComp18.Margin = new System.Windows.Forms.Padding(5);
            this.LueComp18.Name = "LueComp18";
            this.LueComp18.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp18.Properties.Appearance.Options.UseFont = true;
            this.LueComp18.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp18.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueComp18.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp18.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueComp18.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp18.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueComp18.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp18.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueComp18.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueComp18.Properties.DropDownRows = 20;
            this.LueComp18.Properties.NullText = "[Empty]";
            this.LueComp18.Properties.PopupWidth = 500;
            this.LueComp18.Size = new System.Drawing.Size(330, 20);
            this.LueComp18.TabIndex = 46;
            this.LueComp18.ToolTip = "F4 : Show/hide list";
            this.LueComp18.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueComp18.EditValueChanged += new System.EventHandler(this.LueComp18_EditValueChanged);
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.ForeColor = System.Drawing.Color.Red;
            this.label42.Location = new System.Drawing.Point(8, 9);
            this.label42.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(77, 14);
            this.label42.TabIndex = 45;
            this.label42.Text = "Competence";
            this.label42.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage19
            // 
            this.tabPage19.Controls.Add(this.MeeRemarkGrd19);
            this.tabPage19.Controls.Add(this.MeeEviGrd19);
            this.tabPage19.Controls.Add(this.MeeIndGrd19);
            this.tabPage19.Controls.Add(this.MeeCompGrd19);
            this.tabPage19.Controls.Add(this.Grd19);
            this.tabPage19.Controls.Add(this.panel22);
            this.tabPage19.Location = new System.Drawing.Point(4, 23);
            this.tabPage19.Name = "tabPage19";
            this.tabPage19.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage19.Size = new System.Drawing.Size(764, 37);
            this.tabPage19.TabIndex = 18;
            this.tabPage19.Text = "No.19";
            this.tabPage19.UseVisualStyleBackColor = true;
            // 
            // MeeRemarkGrd19
            // 
            this.MeeRemarkGrd19.EnterMoveNextControl = true;
            this.MeeRemarkGrd19.Location = new System.Drawing.Point(594, 160);
            this.MeeRemarkGrd19.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemarkGrd19.Name = "MeeRemarkGrd19";
            this.MeeRemarkGrd19.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd19.Properties.Appearance.Options.UseFont = true;
            this.MeeRemarkGrd19.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd19.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemarkGrd19.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd19.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemarkGrd19.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd19.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemarkGrd19.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd19.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemarkGrd19.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemarkGrd19.Properties.MaxLength = 400;
            this.MeeRemarkGrd19.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeRemarkGrd19.Properties.ShowIcon = false;
            this.MeeRemarkGrd19.Size = new System.Drawing.Size(220, 20);
            this.MeeRemarkGrd19.TabIndex = 59;
            this.MeeRemarkGrd19.ToolTip = "F4 : Show/hide text";
            this.MeeRemarkGrd19.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemarkGrd19.ToolTipTitle = "Run System";
            this.MeeRemarkGrd19.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeRemarkGrd19_KeyDown);
            this.MeeRemarkGrd19.Leave += new System.EventHandler(this.MeeRemarkGrd19_Leave);
            // 
            // MeeEviGrd19
            // 
            this.MeeEviGrd19.EnterMoveNextControl = true;
            this.MeeEviGrd19.Location = new System.Drawing.Point(393, 158);
            this.MeeEviGrd19.Margin = new System.Windows.Forms.Padding(5);
            this.MeeEviGrd19.Name = "MeeEviGrd19";
            this.MeeEviGrd19.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd19.Properties.Appearance.Options.UseFont = true;
            this.MeeEviGrd19.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd19.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeEviGrd19.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd19.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeEviGrd19.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd19.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeEviGrd19.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd19.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeEviGrd19.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeEviGrd19.Properties.MaxLength = 400;
            this.MeeEviGrd19.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeEviGrd19.Properties.ShowIcon = false;
            this.MeeEviGrd19.Size = new System.Drawing.Size(180, 20);
            this.MeeEviGrd19.TabIndex = 58;
            this.MeeEviGrd19.ToolTip = "F4 : Show/hide text";
            this.MeeEviGrd19.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeEviGrd19.ToolTipTitle = "Run System";
            this.MeeEviGrd19.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeEviGrd19_KeyDown);
            this.MeeEviGrd19.Leave += new System.EventHandler(this.MeeEviGrd19_Leave);
            // 
            // MeeIndGrd19
            // 
            this.MeeIndGrd19.EnterMoveNextControl = true;
            this.MeeIndGrd19.Location = new System.Drawing.Point(225, 109);
            this.MeeIndGrd19.Margin = new System.Windows.Forms.Padding(5);
            this.MeeIndGrd19.Name = "MeeIndGrd19";
            this.MeeIndGrd19.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd19.Properties.Appearance.Options.UseFont = true;
            this.MeeIndGrd19.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd19.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeIndGrd19.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd19.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeIndGrd19.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd19.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeIndGrd19.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd19.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeIndGrd19.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeIndGrd19.Properties.MaxLength = 400;
            this.MeeIndGrd19.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeIndGrd19.Properties.ShowIcon = false;
            this.MeeIndGrd19.Size = new System.Drawing.Size(220, 20);
            this.MeeIndGrd19.TabIndex = 57;
            this.MeeIndGrd19.ToolTip = "F4 : Show/hide text";
            this.MeeIndGrd19.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeIndGrd19.ToolTipTitle = "Run System";
            this.MeeIndGrd19.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeIndGrd19_KeyDown);
            this.MeeIndGrd19.Leave += new System.EventHandler(this.MeeIndGrd19_Leave);
            // 
            // MeeCompGrd19
            // 
            this.MeeCompGrd19.EnterMoveNextControl = true;
            this.MeeCompGrd19.Location = new System.Drawing.Point(24, 107);
            this.MeeCompGrd19.Margin = new System.Windows.Forms.Padding(5);
            this.MeeCompGrd19.Name = "MeeCompGrd19";
            this.MeeCompGrd19.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd19.Properties.Appearance.Options.UseFont = true;
            this.MeeCompGrd19.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd19.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeCompGrd19.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd19.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeCompGrd19.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd19.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeCompGrd19.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd19.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeCompGrd19.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeCompGrd19.Properties.MaxLength = 400;
            this.MeeCompGrd19.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeCompGrd19.Properties.ShowIcon = false;
            this.MeeCompGrd19.Size = new System.Drawing.Size(180, 20);
            this.MeeCompGrd19.TabIndex = 56;
            this.MeeCompGrd19.ToolTip = "F4 : Show/hide text";
            this.MeeCompGrd19.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeCompGrd19.ToolTipTitle = "Run System";
            this.MeeCompGrd19.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeCompGrd19_KeyDown);
            this.MeeCompGrd19.Leave += new System.EventHandler(this.MeeCompGrd19_Leave);
            // 
            // Grd19
            // 
            this.Grd19.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd19.DefaultRow.Height = 20;
            this.Grd19.DefaultRow.Sortable = false;
            this.Grd19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd19.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd19.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd19.Header.Height = 21;
            this.Grd19.Location = new System.Drawing.Point(3, 36);
            this.Grd19.Name = "Grd19";
            this.Grd19.RowHeader.Visible = true;
            this.Grd19.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd19.SingleClickEdit = true;
            this.Grd19.Size = new System.Drawing.Size(758, 0);
            this.Grd19.TabIndex = 53;
            this.Grd19.TreeCol = null;
            this.Grd19.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd19.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd19_ColHdrDoubleClick);
            this.Grd19.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd19_RequestEdit);
            this.Grd19.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd19_KeyDown);
            // 
            // panel22
            // 
            this.panel22.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel22.Controls.Add(this.LueLevel_19);
            this.panel22.Controls.Add(this.label43);
            this.panel22.Controls.Add(this.LueComp19);
            this.panel22.Controls.Add(this.label44);
            this.panel22.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel22.Location = new System.Drawing.Point(3, 3);
            this.panel22.Name = "panel22";
            this.panel22.Size = new System.Drawing.Size(758, 33);
            this.panel22.TabIndex = 46;
            // 
            // LueLevel_19
            // 
            this.LueLevel_19.EnterMoveNextControl = true;
            this.LueLevel_19.Location = new System.Drawing.Point(468, 6);
            this.LueLevel_19.Margin = new System.Windows.Forms.Padding(5);
            this.LueLevel_19.Name = "LueLevel_19";
            this.LueLevel_19.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_19.Properties.Appearance.Options.UseFont = true;
            this.LueLevel_19.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_19.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueLevel_19.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_19.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueLevel_19.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_19.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueLevel_19.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_19.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueLevel_19.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueLevel_19.Properties.DropDownRows = 20;
            this.LueLevel_19.Properties.NullText = "[Empty]";
            this.LueLevel_19.Properties.PopupWidth = 500;
            this.LueLevel_19.Size = new System.Drawing.Size(84, 20);
            this.LueLevel_19.TabIndex = 55;
            this.LueLevel_19.ToolTip = "F4 : Show/hide list";
            this.LueLevel_19.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueLevel_19.EditValueChanged += new System.EventHandler(this.LueLevel19_EditValueChanged);
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.ForeColor = System.Drawing.Color.Red;
            this.label43.Location = new System.Drawing.Point(427, 9);
            this.label43.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(35, 14);
            this.label43.TabIndex = 54;
            this.label43.Text = "Level";
            this.label43.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueComp19
            // 
            this.LueComp19.EnterMoveNextControl = true;
            this.LueComp19.Location = new System.Drawing.Point(90, 6);
            this.LueComp19.Margin = new System.Windows.Forms.Padding(5);
            this.LueComp19.Name = "LueComp19";
            this.LueComp19.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp19.Properties.Appearance.Options.UseFont = true;
            this.LueComp19.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp19.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueComp19.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp19.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueComp19.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp19.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueComp19.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp19.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueComp19.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueComp19.Properties.DropDownRows = 20;
            this.LueComp19.Properties.NullText = "[Empty]";
            this.LueComp19.Properties.PopupWidth = 500;
            this.LueComp19.Size = new System.Drawing.Size(330, 20);
            this.LueComp19.TabIndex = 46;
            this.LueComp19.ToolTip = "F4 : Show/hide list";
            this.LueComp19.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueComp19.EditValueChanged += new System.EventHandler(this.LueComp19_EditValueChanged);
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.ForeColor = System.Drawing.Color.Red;
            this.label44.Location = new System.Drawing.Point(8, 9);
            this.label44.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(77, 14);
            this.label44.TabIndex = 45;
            this.label44.Text = "Competence";
            this.label44.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage20
            // 
            this.tabPage20.Controls.Add(this.MeeRemarkGrd20);
            this.tabPage20.Controls.Add(this.MeeEviGrd20);
            this.tabPage20.Controls.Add(this.MeeIndGrd20);
            this.tabPage20.Controls.Add(this.MeeCompGrd20);
            this.tabPage20.Controls.Add(this.Grd20);
            this.tabPage20.Controls.Add(this.panel23);
            this.tabPage20.Location = new System.Drawing.Point(4, 23);
            this.tabPage20.Name = "tabPage20";
            this.tabPage20.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage20.Size = new System.Drawing.Size(764, 37);
            this.tabPage20.TabIndex = 19;
            this.tabPage20.Text = "No.20";
            this.tabPage20.UseVisualStyleBackColor = true;
            // 
            // MeeRemarkGrd20
            // 
            this.MeeRemarkGrd20.EnterMoveNextControl = true;
            this.MeeRemarkGrd20.Location = new System.Drawing.Point(594, 160);
            this.MeeRemarkGrd20.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemarkGrd20.Name = "MeeRemarkGrd20";
            this.MeeRemarkGrd20.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd20.Properties.Appearance.Options.UseFont = true;
            this.MeeRemarkGrd20.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd20.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemarkGrd20.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd20.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemarkGrd20.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd20.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemarkGrd20.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemarkGrd20.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemarkGrd20.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemarkGrd20.Properties.MaxLength = 400;
            this.MeeRemarkGrd20.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeRemarkGrd20.Properties.ShowIcon = false;
            this.MeeRemarkGrd20.Size = new System.Drawing.Size(220, 20);
            this.MeeRemarkGrd20.TabIndex = 63;
            this.MeeRemarkGrd20.ToolTip = "F4 : Show/hide text";
            this.MeeRemarkGrd20.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemarkGrd20.ToolTipTitle = "Run System";
            this.MeeRemarkGrd20.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeRemarkGrd20_KeyDown);
            this.MeeRemarkGrd20.Leave += new System.EventHandler(this.MeeRemarkGrd20_Leave);
            // 
            // MeeEviGrd20
            // 
            this.MeeEviGrd20.EnterMoveNextControl = true;
            this.MeeEviGrd20.Location = new System.Drawing.Point(393, 158);
            this.MeeEviGrd20.Margin = new System.Windows.Forms.Padding(5);
            this.MeeEviGrd20.Name = "MeeEviGrd20";
            this.MeeEviGrd20.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd20.Properties.Appearance.Options.UseFont = true;
            this.MeeEviGrd20.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd20.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeEviGrd20.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd20.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeEviGrd20.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd20.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeEviGrd20.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeEviGrd20.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeEviGrd20.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeEviGrd20.Properties.MaxLength = 400;
            this.MeeEviGrd20.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeEviGrd20.Properties.ShowIcon = false;
            this.MeeEviGrd20.Size = new System.Drawing.Size(180, 20);
            this.MeeEviGrd20.TabIndex = 62;
            this.MeeEviGrd20.ToolTip = "F4 : Show/hide text";
            this.MeeEviGrd20.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeEviGrd20.ToolTipTitle = "Run System";
            this.MeeEviGrd20.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeEviGrd20_KeyDown);
            this.MeeEviGrd20.Leave += new System.EventHandler(this.MeeEviGrd20_Leave);
            // 
            // MeeIndGrd20
            // 
            this.MeeIndGrd20.EnterMoveNextControl = true;
            this.MeeIndGrd20.Location = new System.Drawing.Point(225, 109);
            this.MeeIndGrd20.Margin = new System.Windows.Forms.Padding(5);
            this.MeeIndGrd20.Name = "MeeIndGrd20";
            this.MeeIndGrd20.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd20.Properties.Appearance.Options.UseFont = true;
            this.MeeIndGrd20.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd20.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeIndGrd20.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd20.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeIndGrd20.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd20.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeIndGrd20.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeIndGrd20.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeIndGrd20.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeIndGrd20.Properties.MaxLength = 400;
            this.MeeIndGrd20.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeIndGrd20.Properties.ShowIcon = false;
            this.MeeIndGrd20.Size = new System.Drawing.Size(220, 20);
            this.MeeIndGrd20.TabIndex = 61;
            this.MeeIndGrd20.ToolTip = "F4 : Show/hide text";
            this.MeeIndGrd20.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeIndGrd20.ToolTipTitle = "Run System";
            this.MeeIndGrd20.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeIndGrd20_KeyDown);
            this.MeeIndGrd20.Leave += new System.EventHandler(this.MeeIndGrd20_Leave);
            // 
            // MeeCompGrd20
            // 
            this.MeeCompGrd20.EnterMoveNextControl = true;
            this.MeeCompGrd20.Location = new System.Drawing.Point(24, 107);
            this.MeeCompGrd20.Margin = new System.Windows.Forms.Padding(5);
            this.MeeCompGrd20.Name = "MeeCompGrd20";
            this.MeeCompGrd20.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd20.Properties.Appearance.Options.UseFont = true;
            this.MeeCompGrd20.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd20.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeCompGrd20.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd20.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeCompGrd20.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd20.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeCompGrd20.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompGrd20.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeCompGrd20.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeCompGrd20.Properties.MaxLength = 400;
            this.MeeCompGrd20.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeCompGrd20.Properties.ShowIcon = false;
            this.MeeCompGrd20.Size = new System.Drawing.Size(180, 20);
            this.MeeCompGrd20.TabIndex = 60;
            this.MeeCompGrd20.ToolTip = "F4 : Show/hide text";
            this.MeeCompGrd20.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeCompGrd20.ToolTipTitle = "Run System";
            this.MeeCompGrd20.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MeeCompGrd20_KeyDown);
            this.MeeCompGrd20.Leave += new System.EventHandler(this.MeeCompGrd20_Leave);
            // 
            // Grd20
            // 
            this.Grd20.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd20.DefaultRow.Height = 20;
            this.Grd20.DefaultRow.Sortable = false;
            this.Grd20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd20.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd20.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd20.Header.Height = 21;
            this.Grd20.Location = new System.Drawing.Point(3, 36);
            this.Grd20.Name = "Grd20";
            this.Grd20.RowHeader.Visible = true;
            this.Grd20.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd20.SingleClickEdit = true;
            this.Grd20.Size = new System.Drawing.Size(758, 0);
            this.Grd20.TabIndex = 53;
            this.Grd20.TreeCol = null;
            this.Grd20.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd20.ColHdrDoubleClick += new TenTec.Windows.iGridLib.iGColHdrDoubleClickEventHandler(this.Grd20_ColHdrDoubleClick);
            this.Grd20.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd20_RequestEdit);
            this.Grd20.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd20_KeyDown);
            // 
            // panel23
            // 
            this.panel23.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel23.Controls.Add(this.LueLevel_20);
            this.panel23.Controls.Add(this.label45);
            this.panel23.Controls.Add(this.LueComp20);
            this.panel23.Controls.Add(this.label46);
            this.panel23.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel23.Location = new System.Drawing.Point(3, 3);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(758, 33);
            this.panel23.TabIndex = 46;
            // 
            // LueLevel_20
            // 
            this.LueLevel_20.EnterMoveNextControl = true;
            this.LueLevel_20.Location = new System.Drawing.Point(468, 6);
            this.LueLevel_20.Margin = new System.Windows.Forms.Padding(5);
            this.LueLevel_20.Name = "LueLevel_20";
            this.LueLevel_20.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_20.Properties.Appearance.Options.UseFont = true;
            this.LueLevel_20.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_20.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueLevel_20.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_20.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueLevel_20.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_20.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueLevel_20.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel_20.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueLevel_20.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueLevel_20.Properties.DropDownRows = 20;
            this.LueLevel_20.Properties.NullText = "[Empty]";
            this.LueLevel_20.Properties.PopupWidth = 500;
            this.LueLevel_20.Size = new System.Drawing.Size(84, 20);
            this.LueLevel_20.TabIndex = 55;
            this.LueLevel_20.ToolTip = "F4 : Show/hide list";
            this.LueLevel_20.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueLevel_20.EditValueChanged += new System.EventHandler(this.LueLevel20_EditValueChanged);
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.ForeColor = System.Drawing.Color.Red;
            this.label45.Location = new System.Drawing.Point(427, 9);
            this.label45.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(35, 14);
            this.label45.TabIndex = 54;
            this.label45.Text = "Level";
            this.label45.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueComp20
            // 
            this.LueComp20.EnterMoveNextControl = true;
            this.LueComp20.Location = new System.Drawing.Point(90, 6);
            this.LueComp20.Margin = new System.Windows.Forms.Padding(5);
            this.LueComp20.Name = "LueComp20";
            this.LueComp20.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp20.Properties.Appearance.Options.UseFont = true;
            this.LueComp20.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp20.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueComp20.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp20.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueComp20.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp20.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueComp20.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueComp20.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueComp20.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueComp20.Properties.DropDownRows = 20;
            this.LueComp20.Properties.NullText = "[Empty]";
            this.LueComp20.Properties.PopupWidth = 500;
            this.LueComp20.Size = new System.Drawing.Size(330, 20);
            this.LueComp20.TabIndex = 46;
            this.LueComp20.ToolTip = "F4 : Show/hide list";
            this.LueComp20.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueComp20.EditValueChanged += new System.EventHandler(this.LueComp20_EditValueChanged);
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.ForeColor = System.Drawing.Color.Red;
            this.label46.Location = new System.Drawing.Point(8, 9);
            this.label46.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(77, 14);
            this.label46.TabIndex = 45;
            this.label46.Text = "Competence";
            this.label46.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // FrmAssesment2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(917, 461);
            this.Name = "FrmAssesment2";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCompetenceLevelType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCompLevelName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAsmName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAsmCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            this.tabPage15.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemarkGrd15.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeEviGrd15.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeIndGrd15.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCompGrd15.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd15)).EndInit();
            this.panel21.ResumeLayout(false);
            this.panel21.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueLevel_15.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueComp15.Properties)).EndInit();
            this.tabPage14.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemarkGrd14.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeEviGrd14.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeIndGrd14.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCompGrd14.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd14)).EndInit();
            this.panel20.ResumeLayout(false);
            this.panel20.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueLevel_14.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueComp14.Properties)).EndInit();
            this.tabPage13.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemarkGrd13.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeEviGrd13.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeIndGrd13.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCompGrd13.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd13)).EndInit();
            this.panel19.ResumeLayout(false);
            this.panel19.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueLevel_13.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueComp13.Properties)).EndInit();
            this.tabPage12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemarkGrd12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeEviGrd12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeIndGrd12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCompGrd12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd12)).EndInit();
            this.panel18.ResumeLayout(false);
            this.panel18.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueLevel_12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueComp12.Properties)).EndInit();
            this.tabPage11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemarkGrd11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeEviGrd11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeIndGrd11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCompGrd11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd11)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueLevel_11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueComp11.Properties)).EndInit();
            this.tabPage10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemarkGrd10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeEviGrd10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeIndGrd10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCompGrd10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd10)).EndInit();
            this.panel17.ResumeLayout(false);
            this.panel17.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueLevel_10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueComp10.Properties)).EndInit();
            this.tabPage9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemarkGrd9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeEviGrd9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeIndGrd9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCompGrd9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd9)).EndInit();
            this.panel16.ResumeLayout(false);
            this.panel16.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueLevel_9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueComp9.Properties)).EndInit();
            this.tabPage8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemarkGrd8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeEviGrd8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeIndGrd8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCompGrd8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd8)).EndInit();
            this.panel15.ResumeLayout(false);
            this.panel15.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueLevel_8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueComp8.Properties)).EndInit();
            this.tabPage7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemarkGrd7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeEviGrd7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeIndGrd7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCompGrd7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd7)).EndInit();
            this.panel14.ResumeLayout(false);
            this.panel14.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueLevel_7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueComp7.Properties)).EndInit();
            this.tabPage6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemarkGrd6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeEviGrd6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeIndGrd6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCompGrd6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd6)).EndInit();
            this.panel13.ResumeLayout(false);
            this.panel13.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueLevel_6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueComp6.Properties)).EndInit();
            this.tabPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemarkGrd5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeEviGrd5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeIndGrd5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCompGrd5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd5)).EndInit();
            this.panel12.ResumeLayout(false);
            this.panel12.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueLevel_5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueComp5.Properties)).EndInit();
            this.tabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemarkGrd4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeEviGrd4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeIndGrd4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCompGrd4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd4)).EndInit();
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueLevel_4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueComp4.Properties)).EndInit();
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemarkGrd3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeEviGrd3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeIndGrd3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCompGrd3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).EndInit();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueLevel_3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueComp3.Properties)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemarkGrd2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeEviGrd2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeIndGrd2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCompGrd2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).EndInit();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueLevel_2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueComp2.Properties)).EndInit();
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemarkGrd1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeEviGrd1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeIndGrd1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCompGrd1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueLevel_1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueComp1.Properties)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage16.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemarkGrd16.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeEviGrd16.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeIndGrd16.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCompGrd16.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd16)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueLevel_16.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueComp16.Properties)).EndInit();
            this.tabPage17.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemarkGrd17.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeEviGrd17.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeIndGrd17.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCompGrd17.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd17)).EndInit();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueLevel_17.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueComp17.Properties)).EndInit();
            this.tabPage18.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemarkGrd18.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeEviGrd18.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeIndGrd18.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCompGrd18.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd18)).EndInit();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueLevel_18.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueComp18.Properties)).EndInit();
            this.tabPage19.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemarkGrd19.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeEviGrd19.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeIndGrd19.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCompGrd19.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd19)).EndInit();
            this.panel22.ResumeLayout(false);
            this.panel22.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueLevel_19.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueComp19.Properties)).EndInit();
            this.tabPage20.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemarkGrd20.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeEviGrd20.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeIndGrd20.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCompGrd20.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd20)).EndInit();
            this.panel23.ResumeLayout(false);
            this.panel23.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueLevel_20.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueComp20.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.Panel panel3;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private System.Windows.Forms.Label label5;
        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        private System.Windows.Forms.Label label17;
        internal DevExpress.XtraEditors.TextEdit TxtAsmCode;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        protected System.Windows.Forms.Panel panel8;
        private DevExpress.XtraEditors.LookUpEdit LueComp1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TabPage tabPage2;
        protected internal TenTec.Windows.iGridLib.iGrid Grd2;
        protected System.Windows.Forms.Panel panel9;
        private DevExpress.XtraEditors.LookUpEdit LueComp2;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TabPage tabPage3;
        protected internal TenTec.Windows.iGridLib.iGrid Grd3;
        protected System.Windows.Forms.Panel panel10;
        private DevExpress.XtraEditors.LookUpEdit LueComp3;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TabPage tabPage4;
        protected internal TenTec.Windows.iGridLib.iGrid Grd4;
        protected System.Windows.Forms.Panel panel11;
        private DevExpress.XtraEditors.LookUpEdit LueComp4;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TabPage tabPage5;
        protected internal TenTec.Windows.iGridLib.iGrid Grd5;
        protected System.Windows.Forms.Panel panel12;
        private DevExpress.XtraEditors.LookUpEdit LueComp5;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TabPage tabPage6;
        protected internal TenTec.Windows.iGridLib.iGrid Grd6;
        protected System.Windows.Forms.Panel panel13;
        private DevExpress.XtraEditors.LookUpEdit LueComp6;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TabPage tabPage7;
        protected internal TenTec.Windows.iGridLib.iGrid Grd7;
        protected System.Windows.Forms.Panel panel14;
        private DevExpress.XtraEditors.LookUpEdit LueComp7;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TabPage tabPage8;
        protected internal TenTec.Windows.iGridLib.iGrid Grd8;
        protected System.Windows.Forms.Panel panel15;
        private DevExpress.XtraEditors.LookUpEdit LueComp8;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TabPage tabPage9;
        protected internal TenTec.Windows.iGridLib.iGrid Grd9;
        protected System.Windows.Forms.Panel panel16;
        private DevExpress.XtraEditors.LookUpEdit LueComp9;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TabPage tabPage10;
        protected internal TenTec.Windows.iGridLib.iGrid Grd10;
        protected System.Windows.Forms.Panel panel17;
        private DevExpress.XtraEditors.LookUpEdit LueComp10;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TabPage tabPage11;
        protected internal TenTec.Windows.iGridLib.iGrid Grd11;
        protected System.Windows.Forms.Panel panel5;
        private DevExpress.XtraEditors.LookUpEdit LueComp11;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TabPage tabPage12;
        protected internal TenTec.Windows.iGridLib.iGrid Grd12;
        protected System.Windows.Forms.Panel panel18;
        private DevExpress.XtraEditors.LookUpEdit LueComp12;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TabPage tabPage13;
        protected internal TenTec.Windows.iGridLib.iGrid Grd13;
        protected System.Windows.Forms.Panel panel19;
        private DevExpress.XtraEditors.LookUpEdit LueComp13;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TabPage tabPage14;
        protected internal TenTec.Windows.iGridLib.iGrid Grd14;
        protected System.Windows.Forms.Panel panel20;
        private DevExpress.XtraEditors.LookUpEdit LueComp14;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TabPage tabPage15;
        protected internal TenTec.Windows.iGridLib.iGrid Grd15;
        protected System.Windows.Forms.Panel panel21;
        private DevExpress.XtraEditors.LookUpEdit LueComp15;
        private System.Windows.Forms.Label label40;
        internal DevExpress.XtraEditors.TextEdit TxtAsmName;
        private System.Windows.Forms.Label label1;
        protected internal TenTec.Windows.iGridLib.iGrid Grd1;
        private DevExpress.XtraEditors.LookUpEdit LueLevel_1;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.LookUpEdit LueLevel_2;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.LookUpEdit LueLevel_3;
        private System.Windows.Forms.Label label6;
        private DevExpress.XtraEditors.LookUpEdit LueLevel_4;
        private System.Windows.Forms.Label label7;
        private DevExpress.XtraEditors.LookUpEdit LueLevel_5;
        private System.Windows.Forms.Label label8;
        private DevExpress.XtraEditors.LookUpEdit LueLevel_6;
        private System.Windows.Forms.Label label9;
        private DevExpress.XtraEditors.LookUpEdit LueLevel_7;
        private System.Windows.Forms.Label label10;
        private DevExpress.XtraEditors.LookUpEdit LueLevel_8;
        private System.Windows.Forms.Label label12;
        private DevExpress.XtraEditors.LookUpEdit LueLevel_9;
        private System.Windows.Forms.Label label14;
        private DevExpress.XtraEditors.LookUpEdit LueLevel_10;
        private System.Windows.Forms.Label label16;
        private DevExpress.XtraEditors.LookUpEdit LueLevel_11;
        private System.Windows.Forms.Label label20;
        private DevExpress.XtraEditors.LookUpEdit LueLevel_12;
        private System.Windows.Forms.Label label22;
        private DevExpress.XtraEditors.LookUpEdit LueLevel_13;
        private System.Windows.Forms.Label label24;
        private DevExpress.XtraEditors.LookUpEdit LueLevel_14;
        private System.Windows.Forms.Label label26;
        private DevExpress.XtraEditors.LookUpEdit LueLevel_15;
        private System.Windows.Forms.Label label28;
        private DevExpress.XtraEditors.MemoExEdit MeeIndGrd1;
        private DevExpress.XtraEditors.MemoExEdit MeeCompGrd1;
        private DevExpress.XtraEditors.MemoExEdit MeeRemarkGrd1;
        private DevExpress.XtraEditors.MemoExEdit MeeEviGrd1;
        private DevExpress.XtraEditors.MemoExEdit MeeRemarkGrd2;
        private DevExpress.XtraEditors.MemoExEdit MeeEviGrd2;
        private DevExpress.XtraEditors.MemoExEdit MeeIndGrd2;
        private DevExpress.XtraEditors.MemoExEdit MeeCompGrd2;
        private DevExpress.XtraEditors.MemoExEdit MeeRemarkGrd3;
        private DevExpress.XtraEditors.MemoExEdit MeeEviGrd3;
        private DevExpress.XtraEditors.MemoExEdit MeeIndGrd3;
        private DevExpress.XtraEditors.MemoExEdit MeeCompGrd3;
        private DevExpress.XtraEditors.MemoExEdit MeeRemarkGrd4;
        private DevExpress.XtraEditors.MemoExEdit MeeEviGrd4;
        private DevExpress.XtraEditors.MemoExEdit MeeIndGrd4;
        private DevExpress.XtraEditors.MemoExEdit MeeCompGrd4;
        private DevExpress.XtraEditors.MemoExEdit MeeRemarkGrd5;
        private DevExpress.XtraEditors.MemoExEdit MeeEviGrd5;
        private DevExpress.XtraEditors.MemoExEdit MeeIndGrd5;
        private DevExpress.XtraEditors.MemoExEdit MeeCompGrd5;
        private DevExpress.XtraEditors.MemoExEdit MeeRemarkGrd6;
        private DevExpress.XtraEditors.MemoExEdit MeeEviGrd6;
        private DevExpress.XtraEditors.MemoExEdit MeeIndGrd6;
        private DevExpress.XtraEditors.MemoExEdit MeeCompGrd6;
        private DevExpress.XtraEditors.MemoExEdit MeeRemarkGrd7;
        private DevExpress.XtraEditors.MemoExEdit MeeEviGrd7;
        private DevExpress.XtraEditors.MemoExEdit MeeIndGrd7;
        private DevExpress.XtraEditors.MemoExEdit MeeCompGrd7;
        private DevExpress.XtraEditors.MemoExEdit MeeRemarkGrd8;
        private DevExpress.XtraEditors.MemoExEdit MeeEviGrd8;
        private DevExpress.XtraEditors.MemoExEdit MeeIndGrd8;
        private DevExpress.XtraEditors.MemoExEdit MeeCompGrd8;
        private DevExpress.XtraEditors.MemoExEdit MeeRemarkGrd9;
        private DevExpress.XtraEditors.MemoExEdit MeeEviGrd9;
        private DevExpress.XtraEditors.MemoExEdit MeeIndGrd9;
        private DevExpress.XtraEditors.MemoExEdit MeeCompGrd9;
        private DevExpress.XtraEditors.MemoExEdit MeeRemarkGrd10;
        private DevExpress.XtraEditors.MemoExEdit MeeEviGrd10;
        private DevExpress.XtraEditors.MemoExEdit MeeIndGrd10;
        private DevExpress.XtraEditors.MemoExEdit MeeCompGrd10;
        private DevExpress.XtraEditors.MemoExEdit MeeRemarkGrd11;
        private DevExpress.XtraEditors.MemoExEdit MeeEviGrd11;
        private DevExpress.XtraEditors.MemoExEdit MeeIndGrd11;
        private DevExpress.XtraEditors.MemoExEdit MeeCompGrd11;
        private DevExpress.XtraEditors.MemoExEdit MeeRemarkGrd12;
        private DevExpress.XtraEditors.MemoExEdit MeeEviGrd12;
        private DevExpress.XtraEditors.MemoExEdit MeeIndGrd12;
        private DevExpress.XtraEditors.MemoExEdit MeeCompGrd12;
        private DevExpress.XtraEditors.MemoExEdit MeeRemarkGrd13;
        private DevExpress.XtraEditors.MemoExEdit MeeEviGrd13;
        private DevExpress.XtraEditors.MemoExEdit MeeIndGrd13;
        private DevExpress.XtraEditors.MemoExEdit MeeCompGrd13;
        private DevExpress.XtraEditors.MemoExEdit MeeRemarkGrd14;
        private DevExpress.XtraEditors.MemoExEdit MeeEviGrd14;
        private DevExpress.XtraEditors.MemoExEdit MeeIndGrd14;
        private DevExpress.XtraEditors.MemoExEdit MeeCompGrd14;
        private DevExpress.XtraEditors.MemoExEdit MeeRemarkGrd15;
        private DevExpress.XtraEditors.MemoExEdit MeeEviGrd15;
        private DevExpress.XtraEditors.MemoExEdit MeeIndGrd15;
        private DevExpress.XtraEditors.MemoExEdit MeeCompGrd15;
        private System.Windows.Forms.Label label30;
        private DevExpress.XtraEditors.LookUpEdit LueCompLevelName;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.LookUpEdit LueCompetenceLevelType;
        private System.Windows.Forms.TabPage tabPage16;
        protected System.Windows.Forms.Panel panel4;
        private DevExpress.XtraEditors.LookUpEdit LueLevel_16;
        private System.Windows.Forms.Label label33;
        private DevExpress.XtraEditors.LookUpEdit LueComp16;
        private System.Windows.Forms.Label label35;
        protected internal TenTec.Windows.iGridLib.iGrid Grd16;
        private System.Windows.Forms.TabPage tabPage17;
        private System.Windows.Forms.TabPage tabPage18;
        private System.Windows.Forms.TabPage tabPage19;
        private System.Windows.Forms.TabPage tabPage20;
        protected internal TenTec.Windows.iGridLib.iGrid Grd17;
        protected System.Windows.Forms.Panel panel6;
        private DevExpress.XtraEditors.LookUpEdit LueLevel_17;
        private System.Windows.Forms.Label label37;
        private DevExpress.XtraEditors.LookUpEdit LueComp17;
        private System.Windows.Forms.Label label39;
        protected internal TenTec.Windows.iGridLib.iGrid Grd18;
        protected System.Windows.Forms.Panel panel7;
        private DevExpress.XtraEditors.LookUpEdit LueLevel_18;
        private System.Windows.Forms.Label label41;
        private DevExpress.XtraEditors.LookUpEdit LueComp18;
        private System.Windows.Forms.Label label42;
        protected internal TenTec.Windows.iGridLib.iGrid Grd19;
        protected System.Windows.Forms.Panel panel22;
        private DevExpress.XtraEditors.LookUpEdit LueLevel_19;
        private System.Windows.Forms.Label label43;
        private DevExpress.XtraEditors.LookUpEdit LueComp19;
        private System.Windows.Forms.Label label44;
        protected internal TenTec.Windows.iGridLib.iGrid Grd20;
        protected System.Windows.Forms.Panel panel23;
        private DevExpress.XtraEditors.LookUpEdit LueLevel_20;
        private System.Windows.Forms.Label label45;
        private DevExpress.XtraEditors.LookUpEdit LueComp20;
        private System.Windows.Forms.Label label46;
        private DevExpress.XtraEditors.MemoExEdit MeeRemarkGrd16;
        private DevExpress.XtraEditors.MemoExEdit MeeEviGrd16;
        private DevExpress.XtraEditors.MemoExEdit MeeIndGrd16;
        private DevExpress.XtraEditors.MemoExEdit MeeCompGrd16;
        private DevExpress.XtraEditors.MemoExEdit MeeRemarkGrd17;
        private DevExpress.XtraEditors.MemoExEdit MeeEviGrd17;
        private DevExpress.XtraEditors.MemoExEdit MeeIndGrd17;
        private DevExpress.XtraEditors.MemoExEdit MeeCompGrd17;
        private DevExpress.XtraEditors.MemoExEdit MeeRemarkGrd18;
        private DevExpress.XtraEditors.MemoExEdit MeeEviGrd18;
        private DevExpress.XtraEditors.MemoExEdit MeeIndGrd18;
        private DevExpress.XtraEditors.MemoExEdit MeeCompGrd18;
        private DevExpress.XtraEditors.MemoExEdit MeeRemarkGrd19;
        private DevExpress.XtraEditors.MemoExEdit MeeEviGrd19;
        private DevExpress.XtraEditors.MemoExEdit MeeIndGrd19;
        private DevExpress.XtraEditors.MemoExEdit MeeCompGrd19;
        private DevExpress.XtraEditors.MemoExEdit MeeRemarkGrd20;
        private DevExpress.XtraEditors.MemoExEdit MeeEviGrd20;
        private DevExpress.XtraEditors.MemoExEdit MeeIndGrd20;
        private DevExpress.XtraEditors.MemoExEdit MeeCompGrd20;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        public DevExpress.XtraEditors.SimpleButton BtnImport;
        private DevExpress.XtraEditors.CheckEdit ChkActInd;
    }
}