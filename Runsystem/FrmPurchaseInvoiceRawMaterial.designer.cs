﻿namespace RunSystem
{
    partial class FrmPurchaseInvoiceRawMaterial
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmPurchaseInvoiceRawMaterial));
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel9 = new System.Windows.Forms.Panel();
            this.LueDocType = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtGrandTotal = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.TxtCurCode = new DevExpress.XtraEditors.TextEdit();
            this.ChkCancelInd = new DevExpress.XtraEditors.CheckEdit();
            this.TxtDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.DteDocDt = new DevExpress.XtraEditors.DateEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.LueAcType = new DevExpress.XtraEditors.LookUpEdit();
            this.panel4 = new System.Windows.Forms.Panel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label43 = new System.Windows.Forms.Label();
            this.TxtTaxSlip = new DevExpress.XtraEditors.TextEdit();
            this.TxtRoundingValue = new DevExpress.XtraEditors.TextEdit();
            this.label42 = new System.Windows.Forms.Label();
            this.ChkTaxInd = new DevExpress.XtraEditors.CheckEdit();
            this.label37 = new System.Windows.Forms.Label();
            this.LueBankAcCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label13 = new System.Windows.Forms.Label();
            this.TxtGiroNo = new DevExpress.XtraEditors.TextEdit();
            this.LuePaymentType = new DevExpress.XtraEditors.LookUpEdit();
            this.label15 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.LueBankCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label14 = new System.Windows.Forms.Label();
            this.DteDueDt = new DevExpress.XtraEditors.DateEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.TxtPaidToBankAcName = new DevExpress.XtraEditors.TextEdit();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.TxtPaidToBankBranch = new DevExpress.XtraEditors.TextEdit();
            this.LuePaidToBankCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label22 = new System.Windows.Forms.Label();
            this.TxtPaymentUser = new DevExpress.XtraEditors.TextEdit();
            this.label24 = new System.Windows.Forms.Label();
            this.TxtPaidToBankAcNo = new DevExpress.XtraEditors.TextEdit();
            this.TxtTin = new DevExpress.XtraEditors.TextEdit();
            this.label33 = new System.Windows.Forms.Label();
            this.TxtTax = new DevExpress.XtraEditors.TextEdit();
            this.label31 = new System.Windows.Forms.Label();
            this.TxtTaxPercentage = new DevExpress.XtraEditors.TextEdit();
            this.label32 = new System.Windows.Forms.Label();
            this.TxtTTCode = new DevExpress.XtraEditors.TextEdit();
            this.TxtAmt = new DevExpress.XtraEditors.TextEdit();
            this.label30 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.TxtTransportCost = new DevExpress.XtraEditors.TextEdit();
            this.TxtOutstandingAmt = new DevExpress.XtraEditors.TextEdit();
            this.label27 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.TxtOutstandingTC = new DevExpress.XtraEditors.TextEdit();
            this.TxtTotalAmt = new DevExpress.XtraEditors.TextEdit();
            this.label11 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtTC = new DevExpress.XtraEditors.TextEdit();
            this.label29 = new System.Windows.Forms.Label();
            this.TxtNail = new DevExpress.XtraEditors.TextEdit();
            this.label25 = new System.Windows.Forms.Label();
            this.TxtVoucherRequestDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtVoucherDocNo = new DevExpress.XtraEditors.TextEdit();
            this.DteQueueDt = new DevExpress.XtraEditors.DateEdit();
            this.label19 = new System.Windows.Forms.Label();
            this.BtnRawMaterialVerifyDocNo2 = new DevExpress.XtraEditors.SimpleButton();
            this.TxtQueueNo = new DevExpress.XtraEditors.TextEdit();
            this.label17 = new System.Windows.Forms.Label();
            this.BtnRawMaterialVerifyDocNo = new DevExpress.XtraEditors.SimpleButton();
            this.TxtRawMaterialVerifyDocNo = new DevExpress.XtraEditors.TextEdit();
            this.label18 = new System.Windows.Forms.Label();
            this.TxtVdCode = new DevExpress.XtraEditors.TextEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.panel7 = new System.Windows.Forms.Panel();
            this.Grd1 = new TenTec.Windows.iGridLib.iGrid();
            this.panel5 = new System.Windows.Forms.Panel();
            this.TxtTotalLog2 = new DevExpress.XtraEditors.TextEdit();
            this.label39 = new System.Windows.Forms.Label();
            this.TxtTotalLog1 = new DevExpress.XtraEditors.TextEdit();
            this.label38 = new System.Windows.Forms.Label();
            this.TxtTotalLog = new DevExpress.XtraEditors.TextEdit();
            this.label35 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.panel8 = new System.Windows.Forms.Panel();
            this.Grd2 = new TenTec.Windows.iGridLib.iGrid();
            this.panel10 = new System.Windows.Forms.Panel();
            this.TxtTotalBalok2 = new DevExpress.XtraEditors.TextEdit();
            this.label40 = new System.Windows.Forms.Label();
            this.TxtTotalBalok1 = new DevExpress.XtraEditors.TextEdit();
            this.label41 = new System.Windows.Forms.Label();
            this.TxtTotalBalok = new DevExpress.XtraEditors.TextEdit();
            this.label36 = new System.Windows.Forms.Label();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.Grd3 = new TenTec.Windows.iGridLib.iGrid();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.Grd4 = new TenTec.Windows.iGridLib.iGrid();
            this.ChkHideInfoInGrd = new DevExpress.XtraEditors.CheckEdit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueDocType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGrandTotal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCurCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueAcType.Properties)).BeginInit();
            this.panel4.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxSlip.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRoundingValue.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkTaxInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBankAcCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGiroNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePaymentType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBankCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDueDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDueDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPaidToBankAcName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPaidToBankBranch.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePaidToBankCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPaymentUser.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPaidToBankAcNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTin.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTax.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxPercentage.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTTCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAmt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTransportCost.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtOutstandingAmt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtOutstandingTC.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalAmt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTC.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtNail.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVoucherRequestDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVoucherDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteQueueDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteQueueDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtQueueNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRawMaterialVerifyDocNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVdCode.Properties)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalLog2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalLog1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalLog.Properties)).BeginInit();
            this.tabPage3.SuspendLayout();
            this.panel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).BeginInit();
            this.panel10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalBalok2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalBalok1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalBalok.Properties)).BeginInit();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).BeginInit();
            this.tabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.ChkHideInfoInGrd);
            this.panel1.Size = new System.Drawing.Size(70, 473);
            this.panel1.Controls.SetChildIndex(this.BtnFind, 0);
            this.panel1.Controls.SetChildIndex(this.BtnInsert, 0);
            this.panel1.Controls.SetChildIndex(this.BtnEdit, 0);
            this.panel1.Controls.SetChildIndex(this.BtnDelete, 0);
            this.panel1.Controls.SetChildIndex(this.BtnSave, 0);
            this.panel1.Controls.SetChildIndex(this.BtnCancel, 0);
            this.panel1.Controls.SetChildIndex(this.BtnPrint, 0);
            this.panel1.Controls.SetChildIndex(this.ChkHideInfoInGrd, 0);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Size = new System.Drawing.Size(772, 473);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.panel9);
            this.panel3.Controls.Add(this.ChkCancelInd);
            this.panel3.Controls.Add(this.TxtDocNo);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.DteDocDt);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.label16);
            this.panel3.Controls.Add(this.LueAcType);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(772, 74);
            this.panel3.TabIndex = 9;
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.LueDocType);
            this.panel9.Controls.Add(this.TxtGrandTotal);
            this.panel9.Controls.Add(this.label3);
            this.panel9.Controls.Add(this.label34);
            this.panel9.Controls.Add(this.label26);
            this.panel9.Controls.Add(this.TxtCurCode);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel9.Location = new System.Drawing.Point(496, 0);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(276, 74);
            this.panel9.TabIndex = 17;
            // 
            // LueDocType
            // 
            this.LueDocType.EnterMoveNextControl = true;
            this.LueDocType.Location = new System.Drawing.Point(82, 5);
            this.LueDocType.Margin = new System.Windows.Forms.Padding(5);
            this.LueDocType.Name = "LueDocType";
            this.LueDocType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDocType.Properties.Appearance.Options.UseFont = true;
            this.LueDocType.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDocType.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueDocType.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDocType.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueDocType.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDocType.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueDocType.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDocType.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueDocType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueDocType.Properties.DropDownRows = 4;
            this.LueDocType.Properties.NullText = "[Empty]";
            this.LueDocType.Properties.PopupWidth = 143;
            this.LueDocType.Size = new System.Drawing.Size(186, 20);
            this.LueDocType.TabIndex = 19;
            this.LueDocType.ToolTip = "F4 : Show/hide list";
            this.LueDocType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueDocType.EditValueChanged += new System.EventHandler(this.LueDocType_EditValueChanged);
            // 
            // TxtGrandTotal
            // 
            this.TxtGrandTotal.EnterMoveNextControl = true;
            this.TxtGrandTotal.Location = new System.Drawing.Point(82, 47);
            this.TxtGrandTotal.Margin = new System.Windows.Forms.Padding(5);
            this.TxtGrandTotal.Name = "TxtGrandTotal";
            this.TxtGrandTotal.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtGrandTotal.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtGrandTotal.Properties.Appearance.Options.UseBackColor = true;
            this.TxtGrandTotal.Properties.Appearance.Options.UseFont = true;
            this.TxtGrandTotal.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtGrandTotal.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtGrandTotal.Properties.ReadOnly = true;
            this.TxtGrandTotal.Size = new System.Drawing.Size(186, 20);
            this.TxtGrandTotal.TabIndex = 23;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(54, 8);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(24, 14);
            this.label3.TabIndex = 18;
            this.label3.Text = "For";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.Black;
            this.label34.Location = new System.Drawing.Point(7, 50);
            this.label34.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(71, 14);
            this.label34.TabIndex = 22;
            this.label34.Text = "Grand Total";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Black;
            this.label26.Location = new System.Drawing.Point(23, 29);
            this.label26.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(55, 14);
            this.label26.TabIndex = 20;
            this.label26.Text = "Currency";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCurCode
            // 
            this.TxtCurCode.EnterMoveNextControl = true;
            this.TxtCurCode.Location = new System.Drawing.Point(82, 26);
            this.TxtCurCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCurCode.Name = "TxtCurCode";
            this.TxtCurCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtCurCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCurCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCurCode.Properties.Appearance.Options.UseFont = true;
            this.TxtCurCode.Properties.ReadOnly = true;
            this.TxtCurCode.Size = new System.Drawing.Size(75, 20);
            this.TxtCurCode.TabIndex = 21;
            // 
            // ChkCancelInd
            // 
            this.ChkCancelInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkCancelInd.Location = new System.Drawing.Point(285, 4);
            this.ChkCancelInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkCancelInd.Name = "ChkCancelInd";
            this.ChkCancelInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkCancelInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkCancelInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkCancelInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseFont = true;
            this.ChkCancelInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkCancelInd.Properties.Caption = "Cancel";
            this.ChkCancelInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkCancelInd.Size = new System.Drawing.Size(65, 22);
            this.ChkCancelInd.TabIndex = 12;
            // 
            // TxtDocNo
            // 
            this.TxtDocNo.EnterMoveNextControl = true;
            this.TxtDocNo.Location = new System.Drawing.Point(96, 5);
            this.TxtDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDocNo.Name = "TxtDocNo";
            this.TxtDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtDocNo.Properties.MaxLength = 30;
            this.TxtDocNo.Properties.ReadOnly = true;
            this.TxtDocNo.Size = new System.Drawing.Size(187, 20);
            this.TxtDocNo.TabIndex = 11;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(20, 8);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 14);
            this.label1.TabIndex = 10;
            this.label1.Text = "Document#";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteDocDt
            // 
            this.DteDocDt.EditValue = null;
            this.DteDocDt.EnterMoveNextControl = true;
            this.DteDocDt.Location = new System.Drawing.Point(96, 26);
            this.DteDocDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDocDt.Name = "DteDocDt";
            this.DteDocDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.Appearance.Options.UseFont = true;
            this.DteDocDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDocDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDocDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDocDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDocDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDocDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDocDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDocDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDocDt.Size = new System.Drawing.Size(116, 20);
            this.DteDocDt.TabIndex = 14;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(60, 29);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 14);
            this.label2.TabIndex = 13;
            this.label2.Text = "Date";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Red;
            this.label16.Location = new System.Drawing.Point(8, 50);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(85, 14);
            this.label16.TabIndex = 15;
            this.label16.Text = "Account Type";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueAcType
            // 
            this.LueAcType.EnterMoveNextControl = true;
            this.LueAcType.Location = new System.Drawing.Point(96, 47);
            this.LueAcType.Margin = new System.Windows.Forms.Padding(5);
            this.LueAcType.Name = "LueAcType";
            this.LueAcType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAcType.Properties.Appearance.Options.UseFont = true;
            this.LueAcType.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAcType.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueAcType.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAcType.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueAcType.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAcType.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueAcType.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAcType.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueAcType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueAcType.Properties.DropDownRows = 4;
            this.LueAcType.Properties.NullText = "[Empty]";
            this.LueAcType.Properties.PopupWidth = 143;
            this.LueAcType.Size = new System.Drawing.Size(116, 20);
            this.LueAcType.TabIndex = 16;
            this.LueAcType.ToolTip = "F4 : Show/hide list";
            this.LueAcType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueAcType.EditValueChanged += new System.EventHandler(this.LueAcType_EditValueChanged);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.tabControl1);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(0, 74);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(772, 399);
            this.panel4.TabIndex = 1;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(772, 399);
            this.tabControl1.TabIndex = 24;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.panel6);
            this.tabPage1.Location = new System.Drawing.Point(4, 23);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(764, 372);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "General";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel6.Controls.Add(this.label43);
            this.panel6.Controls.Add(this.TxtTaxSlip);
            this.panel6.Controls.Add(this.TxtRoundingValue);
            this.panel6.Controls.Add(this.label42);
            this.panel6.Controls.Add(this.ChkTaxInd);
            this.panel6.Controls.Add(this.label37);
            this.panel6.Controls.Add(this.LueBankAcCode);
            this.panel6.Controls.Add(this.label13);
            this.panel6.Controls.Add(this.TxtGiroNo);
            this.panel6.Controls.Add(this.LuePaymentType);
            this.panel6.Controls.Add(this.label15);
            this.panel6.Controls.Add(this.label10);
            this.panel6.Controls.Add(this.LueBankCode);
            this.panel6.Controls.Add(this.label14);
            this.panel6.Controls.Add(this.DteDueDt);
            this.panel6.Controls.Add(this.label12);
            this.panel6.Controls.Add(this.label23);
            this.panel6.Controls.Add(this.TxtPaidToBankAcName);
            this.panel6.Controls.Add(this.label20);
            this.panel6.Controls.Add(this.label21);
            this.panel6.Controls.Add(this.TxtPaidToBankBranch);
            this.panel6.Controls.Add(this.LuePaidToBankCode);
            this.panel6.Controls.Add(this.label22);
            this.panel6.Controls.Add(this.TxtPaymentUser);
            this.panel6.Controls.Add(this.label24);
            this.panel6.Controls.Add(this.TxtPaidToBankAcNo);
            this.panel6.Controls.Add(this.TxtTin);
            this.panel6.Controls.Add(this.label33);
            this.panel6.Controls.Add(this.TxtTax);
            this.panel6.Controls.Add(this.label31);
            this.panel6.Controls.Add(this.TxtTaxPercentage);
            this.panel6.Controls.Add(this.label32);
            this.panel6.Controls.Add(this.TxtTTCode);
            this.panel6.Controls.Add(this.TxtAmt);
            this.panel6.Controls.Add(this.label30);
            this.panel6.Controls.Add(this.label9);
            this.panel6.Controls.Add(this.TxtTransportCost);
            this.panel6.Controls.Add(this.TxtOutstandingAmt);
            this.panel6.Controls.Add(this.label27);
            this.panel6.Controls.Add(this.label7);
            this.panel6.Controls.Add(this.TxtOutstandingTC);
            this.panel6.Controls.Add(this.TxtTotalAmt);
            this.panel6.Controls.Add(this.label11);
            this.panel6.Controls.Add(this.label28);
            this.panel6.Controls.Add(this.MeeRemark);
            this.panel6.Controls.Add(this.label5);
            this.panel6.Controls.Add(this.TxtTC);
            this.panel6.Controls.Add(this.label29);
            this.panel6.Controls.Add(this.TxtNail);
            this.panel6.Controls.Add(this.label25);
            this.panel6.Controls.Add(this.TxtVoucherRequestDocNo);
            this.panel6.Controls.Add(this.label4);
            this.panel6.Controls.Add(this.label6);
            this.panel6.Controls.Add(this.TxtVoucherDocNo);
            this.panel6.Controls.Add(this.DteQueueDt);
            this.panel6.Controls.Add(this.label19);
            this.panel6.Controls.Add(this.BtnRawMaterialVerifyDocNo2);
            this.panel6.Controls.Add(this.TxtQueueNo);
            this.panel6.Controls.Add(this.label17);
            this.panel6.Controls.Add(this.BtnRawMaterialVerifyDocNo);
            this.panel6.Controls.Add(this.TxtRawMaterialVerifyDocNo);
            this.panel6.Controls.Add(this.label18);
            this.panel6.Controls.Add(this.TxtVdCode);
            this.panel6.Controls.Add(this.label8);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(3, 3);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(758, 366);
            this.panel6.TabIndex = 25;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.ForeColor = System.Drawing.Color.Black;
            this.label43.Location = new System.Drawing.Point(453, 288);
            this.label43.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(58, 14);
            this.label43.TabIndex = 82;
            this.label43.Text = "Tax Slip#";
            this.label43.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTaxSlip
            // 
            this.TxtTaxSlip.EnterMoveNextControl = true;
            this.TxtTaxSlip.Location = new System.Drawing.Point(517, 285);
            this.TxtTaxSlip.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTaxSlip.Name = "TxtTaxSlip";
            this.TxtTaxSlip.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtTaxSlip.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTaxSlip.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTaxSlip.Properties.Appearance.Options.UseFont = true;
            this.TxtTaxSlip.Properties.MaxLength = 30;
            this.TxtTaxSlip.Properties.ReadOnly = true;
            this.TxtTaxSlip.Size = new System.Drawing.Size(233, 20);
            this.TxtTaxSlip.TabIndex = 83;
            // 
            // TxtRoundingValue
            // 
            this.TxtRoundingValue.EnterMoveNextControl = true;
            this.TxtRoundingValue.Location = new System.Drawing.Point(151, 306);
            this.TxtRoundingValue.Margin = new System.Windows.Forms.Padding(5);
            this.TxtRoundingValue.Name = "TxtRoundingValue";
            this.TxtRoundingValue.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtRoundingValue.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtRoundingValue.Properties.Appearance.Options.UseBackColor = true;
            this.TxtRoundingValue.Properties.Appearance.Options.UseFont = true;
            this.TxtRoundingValue.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtRoundingValue.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtRoundingValue.Size = new System.Drawing.Size(152, 20);
            this.TxtRoundingValue.TabIndex = 56;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.ForeColor = System.Drawing.Color.Black;
            this.label42.Location = new System.Drawing.Point(54, 310);
            this.label42.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(92, 14);
            this.label42.TabIndex = 56;
            this.label42.Text = "Rounding Value";
            this.label42.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkTaxInd
            // 
            this.ChkTaxInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkTaxInd.Location = new System.Drawing.Point(342, 95);
            this.ChkTaxInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkTaxInd.Name = "ChkTaxInd";
            this.ChkTaxInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkTaxInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkTaxInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkTaxInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkTaxInd.Properties.Appearance.Options.UseFont = true;
            this.ChkTaxInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkTaxInd.Properties.Caption = "Tax";
            this.ChkTaxInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkTaxInd.Size = new System.Drawing.Size(51, 22);
            this.ChkTaxInd.TabIndex = 37;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.ForeColor = System.Drawing.Color.Black;
            this.label37.Location = new System.Drawing.Point(212, 183);
            this.label37.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(19, 14);
            this.label37.TabIndex = 45;
            this.label37.Text = "%";
            this.label37.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueBankAcCode
            // 
            this.LueBankAcCode.EnterMoveNextControl = true;
            this.LueBankAcCode.Location = new System.Drawing.Point(517, 138);
            this.LueBankAcCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueBankAcCode.Name = "LueBankAcCode";
            this.LueBankAcCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankAcCode.Properties.Appearance.Options.UseFont = true;
            this.LueBankAcCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankAcCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueBankAcCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankAcCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueBankAcCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankAcCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueBankAcCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankAcCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueBankAcCode.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankAcCode.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.LueBankAcCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueBankAcCode.Properties.DropDownRows = 25;
            this.LueBankAcCode.Properties.NullText = "[Empty]";
            this.LueBankAcCode.Properties.PopupWidth = 500;
            this.LueBankAcCode.Size = new System.Drawing.Size(233, 20);
            this.LueBankAcCode.TabIndex = 69;
            this.LueBankAcCode.ToolTip = "F4 : Show/hide list";
            this.LueBankAcCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueBankAcCode.EditValueChanged += new System.EventHandler(this.LueBankAcCode_EditValueChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Red;
            this.label13.Location = new System.Drawing.Point(458, 141);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(53, 14);
            this.label13.TabIndex = 68;
            this.label13.Text = "Account";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtGiroNo
            // 
            this.TxtGiroNo.EnterMoveNextControl = true;
            this.TxtGiroNo.Location = new System.Drawing.Point(517, 180);
            this.TxtGiroNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtGiroNo.Name = "TxtGiroNo";
            this.TxtGiroNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtGiroNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtGiroNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtGiroNo.Properties.Appearance.Options.UseFont = true;
            this.TxtGiroNo.Properties.MaxLength = 80;
            this.TxtGiroNo.Size = new System.Drawing.Size(233, 20);
            this.TxtGiroNo.TabIndex = 73;
            this.TxtGiroNo.Validated += new System.EventHandler(this.TxtGiroNo_Validated);
            // 
            // LuePaymentType
            // 
            this.LuePaymentType.EnterMoveNextControl = true;
            this.LuePaymentType.Location = new System.Drawing.Point(517, 117);
            this.LuePaymentType.Margin = new System.Windows.Forms.Padding(5);
            this.LuePaymentType.Name = "LuePaymentType";
            this.LuePaymentType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePaymentType.Properties.Appearance.Options.UseFont = true;
            this.LuePaymentType.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePaymentType.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePaymentType.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePaymentType.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePaymentType.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePaymentType.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePaymentType.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePaymentType.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePaymentType.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePaymentType.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.LuePaymentType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePaymentType.Properties.DropDownRows = 25;
            this.LuePaymentType.Properties.NullText = "[Empty]";
            this.LuePaymentType.Properties.PopupWidth = 250;
            this.LuePaymentType.Size = new System.Drawing.Size(233, 20);
            this.LuePaymentType.TabIndex = 67;
            this.LuePaymentType.ToolTip = "F4 : Show/hide list";
            this.LuePaymentType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LuePaymentType.EditValueChanged += new System.EventHandler(this.LuePaymentType_EditValueChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Red;
            this.label15.Location = new System.Drawing.Point(424, 120);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(87, 14);
            this.label15.TabIndex = 66;
            this.label15.Text = "Payment Type";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(395, 183);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(116, 14);
            this.label10.TabIndex = 72;
            this.label10.Text = "Giro Bilyet / Cheque";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueBankCode
            // 
            this.LueBankCode.EnterMoveNextControl = true;
            this.LueBankCode.Location = new System.Drawing.Point(517, 159);
            this.LueBankCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueBankCode.Name = "LueBankCode";
            this.LueBankCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.Appearance.Options.UseFont = true;
            this.LueBankCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueBankCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueBankCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueBankCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueBankCode.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.LueBankCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueBankCode.Properties.DropDownRows = 25;
            this.LueBankCode.Properties.NullText = "[Empty]";
            this.LueBankCode.Properties.PopupWidth = 300;
            this.LueBankCode.Size = new System.Drawing.Size(233, 20);
            this.LueBankCode.TabIndex = 71;
            this.LueBankCode.ToolTip = "F4 : Show/hide list";
            this.LueBankCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueBankCode.EditValueChanged += new System.EventHandler(this.LueBankCode_EditValueChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(443, 162);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(68, 14);
            this.label14.TabIndex = 70;
            this.label14.Text = "Bank Name";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteDueDt
            // 
            this.DteDueDt.EditValue = null;
            this.DteDueDt.EnterMoveNextControl = true;
            this.DteDueDt.Location = new System.Drawing.Point(517, 201);
            this.DteDueDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteDueDt.Name = "DteDueDt";
            this.DteDueDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDueDt.Properties.Appearance.Options.UseFont = true;
            this.DteDueDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteDueDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteDueDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteDueDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteDueDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDueDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteDueDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteDueDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteDueDt.Properties.MaxLength = 8;
            this.DteDueDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteDueDt.Size = new System.Drawing.Size(116, 20);
            this.DteDueDt.TabIndex = 75;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(452, 204);
            this.label12.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(59, 14);
            this.label12.TabIndex = 74;
            this.label12.Text = "Due Date";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Black;
            this.label23.Location = new System.Drawing.Point(423, 99);
            this.label23.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(88, 14);
            this.label23.TabIndex = 64;
            this.label23.Text = "Account Name";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPaidToBankAcName
            // 
            this.TxtPaidToBankAcName.EnterMoveNextControl = true;
            this.TxtPaidToBankAcName.Location = new System.Drawing.Point(517, 96);
            this.TxtPaidToBankAcName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPaidToBankAcName.Name = "TxtPaidToBankAcName";
            this.TxtPaidToBankAcName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPaidToBankAcName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPaidToBankAcName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPaidToBankAcName.Properties.Appearance.Options.UseFont = true;
            this.TxtPaidToBankAcName.Properties.MaxLength = 80;
            this.TxtPaidToBankAcName.Size = new System.Drawing.Size(234, 20);
            this.TxtPaidToBankAcName.TabIndex = 65;
            this.TxtPaidToBankAcName.Validated += new System.EventHandler(this.TxtPaidToBankAcName_Validated);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(443, 36);
            this.label20.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(68, 14);
            this.label20.TabIndex = 58;
            this.label20.Text = "Bank Name";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Black;
            this.label21.Location = new System.Drawing.Point(432, 57);
            this.label21.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(79, 14);
            this.label21.TabIndex = 60;
            this.label21.Text = "Branch Name";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPaidToBankBranch
            // 
            this.TxtPaidToBankBranch.EnterMoveNextControl = true;
            this.TxtPaidToBankBranch.Location = new System.Drawing.Point(517, 54);
            this.TxtPaidToBankBranch.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPaidToBankBranch.Name = "TxtPaidToBankBranch";
            this.TxtPaidToBankBranch.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPaidToBankBranch.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPaidToBankBranch.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPaidToBankBranch.Properties.Appearance.Options.UseFont = true;
            this.TxtPaidToBankBranch.Properties.MaxLength = 80;
            this.TxtPaidToBankBranch.Size = new System.Drawing.Size(234, 20);
            this.TxtPaidToBankBranch.TabIndex = 61;
            this.TxtPaidToBankBranch.Validated += new System.EventHandler(this.TxtPaidToBankBranch_Validated);
            // 
            // LuePaidToBankCode
            // 
            this.LuePaidToBankCode.EnterMoveNextControl = true;
            this.LuePaidToBankCode.Location = new System.Drawing.Point(517, 33);
            this.LuePaidToBankCode.Margin = new System.Windows.Forms.Padding(5);
            this.LuePaidToBankCode.Name = "LuePaidToBankCode";
            this.LuePaidToBankCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePaidToBankCode.Properties.Appearance.Options.UseFont = true;
            this.LuePaidToBankCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePaidToBankCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePaidToBankCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePaidToBankCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePaidToBankCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePaidToBankCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePaidToBankCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePaidToBankCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePaidToBankCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePaidToBankCode.Properties.DropDownRows = 25;
            this.LuePaidToBankCode.Properties.NullText = "[Empty]";
            this.LuePaidToBankCode.Properties.PopupWidth = 1000;
            this.LuePaidToBankCode.Size = new System.Drawing.Size(234, 20);
            this.LuePaidToBankCode.TabIndex = 59;
            this.LuePaidToBankCode.ToolTip = "F4 : Show/hide list";
            this.LuePaidToBankCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LuePaidToBankCode.EditValueChanged += new System.EventHandler(this.LuePaidToBankCode_EditValueChanged);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(449, 79);
            this.label22.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(62, 14);
            this.label22.TabIndex = 62;
            this.label22.Text = "Account#";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPaymentUser
            // 
            this.TxtPaymentUser.EnterMoveNextControl = true;
            this.TxtPaymentUser.Location = new System.Drawing.Point(517, 12);
            this.TxtPaymentUser.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPaymentUser.Name = "TxtPaymentUser";
            this.TxtPaymentUser.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPaymentUser.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPaymentUser.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPaymentUser.Properties.Appearance.Options.UseFont = true;
            this.TxtPaymentUser.Properties.MaxLength = 50;
            this.TxtPaymentUser.Size = new System.Drawing.Size(234, 20);
            this.TxtPaymentUser.TabIndex = 57;
            this.TxtPaymentUser.Validated += new System.EventHandler(this.TxtPaymentUser_Validated);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Black;
            this.label24.Location = new System.Drawing.Point(463, 14);
            this.label24.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(48, 14);
            this.label24.TabIndex = 56;
            this.label24.Text = "Paid To";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPaidToBankAcNo
            // 
            this.TxtPaidToBankAcNo.EnterMoveNextControl = true;
            this.TxtPaidToBankAcNo.Location = new System.Drawing.Point(517, 75);
            this.TxtPaidToBankAcNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPaidToBankAcNo.Name = "TxtPaidToBankAcNo";
            this.TxtPaidToBankAcNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPaidToBankAcNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPaidToBankAcNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPaidToBankAcNo.Properties.Appearance.Options.UseFont = true;
            this.TxtPaidToBankAcNo.Properties.MaxLength = 80;
            this.TxtPaidToBankAcNo.Size = new System.Drawing.Size(234, 20);
            this.TxtPaidToBankAcNo.TabIndex = 63;
            this.TxtPaidToBankAcNo.Validated += new System.EventHandler(this.TxtPaidToBankAcNo_Validated);
            // 
            // TxtTin
            // 
            this.TxtTin.EnterMoveNextControl = true;
            this.TxtTin.Location = new System.Drawing.Point(151, 96);
            this.TxtTin.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTin.Name = "TxtTin";
            this.TxtTin.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTin.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTin.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTin.Properties.Appearance.Options.UseFont = true;
            this.TxtTin.Properties.MaxLength = 50;
            this.TxtTin.Size = new System.Drawing.Size(186, 20);
            this.TxtTin.TabIndex = 36;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.Black;
            this.label33.Location = new System.Drawing.Point(5, 100);
            this.label33.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(141, 14);
            this.label33.TabIndex = 35;
            this.label33.Text = "Taxpayer Identification#";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTax
            // 
            this.TxtTax.EnterMoveNextControl = true;
            this.TxtTax.Location = new System.Drawing.Point(151, 201);
            this.TxtTax.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTax.Name = "TxtTax";
            this.TxtTax.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTax.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTax.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTax.Properties.Appearance.Options.UseFont = true;
            this.TxtTax.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTax.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTax.Size = new System.Drawing.Size(152, 20);
            this.TxtTax.TabIndex = 47;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.Black;
            this.label31.Location = new System.Drawing.Point(71, 205);
            this.label31.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(75, 14);
            this.label31.TabIndex = 46;
            this.label31.Text = "Tax Amount";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTaxPercentage
            // 
            this.TxtTaxPercentage.EnterMoveNextControl = true;
            this.TxtTaxPercentage.Location = new System.Drawing.Point(151, 180);
            this.TxtTaxPercentage.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTaxPercentage.Name = "TxtTaxPercentage";
            this.TxtTaxPercentage.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTaxPercentage.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTaxPercentage.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTaxPercentage.Properties.Appearance.Options.UseFont = true;
            this.TxtTaxPercentage.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTaxPercentage.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTaxPercentage.Size = new System.Drawing.Size(57, 20);
            this.TxtTaxPercentage.TabIndex = 44;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.Black;
            this.label32.Location = new System.Drawing.Point(103, 184);
            this.label32.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(43, 14);
            this.label32.TabIndex = 43;
            this.label32.Text = "Tax %";
            this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTTCode
            // 
            this.TxtTTCode.EnterMoveNextControl = true;
            this.TxtTTCode.Location = new System.Drawing.Point(151, 222);
            this.TxtTTCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTTCode.Name = "TxtTTCode";
            this.TxtTTCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTTCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTTCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTTCode.Properties.Appearance.Options.UseFont = true;
            this.TxtTTCode.Properties.MaxLength = 50;
            this.TxtTTCode.Size = new System.Drawing.Size(242, 20);
            this.TxtTTCode.TabIndex = 49;
            // 
            // TxtAmt
            // 
            this.TxtAmt.EnterMoveNextControl = true;
            this.TxtAmt.Location = new System.Drawing.Point(151, 159);
            this.TxtAmt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAmt.Name = "TxtAmt";
            this.TxtAmt.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtAmt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAmt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAmt.Properties.Appearance.Options.UseFont = true;
            this.TxtAmt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtAmt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtAmt.Size = new System.Drawing.Size(152, 20);
            this.TxtAmt.TabIndex = 42;
            this.TxtAmt.Validated += new System.EventHandler(this.TxtAmt_Validated);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.Black;
            this.label30.Location = new System.Drawing.Point(54, 226);
            this.label30.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(92, 14);
            this.label30.TabIndex = 48;
            this.label30.Text = "Transport Type";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Location = new System.Drawing.Point(57, 163);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(89, 14);
            this.label9.TabIndex = 41;
            this.label9.Text = "Paid Log+Balok";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTransportCost
            // 
            this.TxtTransportCost.EnterMoveNextControl = true;
            this.TxtTransportCost.Location = new System.Drawing.Point(151, 285);
            this.TxtTransportCost.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTransportCost.Name = "TxtTransportCost";
            this.TxtTransportCost.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTransportCost.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTransportCost.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTransportCost.Properties.Appearance.Options.UseFont = true;
            this.TxtTransportCost.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTransportCost.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTransportCost.Size = new System.Drawing.Size(152, 20);
            this.TxtTransportCost.TabIndex = 55;
            this.TxtTransportCost.Validated += new System.EventHandler(this.TxtTransportCost_Validated);
            // 
            // TxtOutstandingAmt
            // 
            this.TxtOutstandingAmt.EnterMoveNextControl = true;
            this.TxtOutstandingAmt.Location = new System.Drawing.Point(151, 138);
            this.TxtOutstandingAmt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtOutstandingAmt.Name = "TxtOutstandingAmt";
            this.TxtOutstandingAmt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtOutstandingAmt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtOutstandingAmt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtOutstandingAmt.Properties.Appearance.Options.UseFont = true;
            this.TxtOutstandingAmt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtOutstandingAmt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtOutstandingAmt.Properties.ReadOnly = true;
            this.TxtOutstandingAmt.Size = new System.Drawing.Size(152, 20);
            this.TxtOutstandingAmt.TabIndex = 40;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Black;
            this.label27.Location = new System.Drawing.Point(50, 289);
            this.label27.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(96, 14);
            this.label27.TabIndex = 54;
            this.label27.Text = "Paid TC Amount";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(12, 142);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(134, 14);
            this.label7.TabIndex = 39;
            this.label7.Text = "Outstanding Log+Balok";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtOutstandingTC
            // 
            this.TxtOutstandingTC.EnterMoveNextControl = true;
            this.TxtOutstandingTC.Location = new System.Drawing.Point(151, 264);
            this.TxtOutstandingTC.Margin = new System.Windows.Forms.Padding(5);
            this.TxtOutstandingTC.Name = "TxtOutstandingTC";
            this.TxtOutstandingTC.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtOutstandingTC.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtOutstandingTC.Properties.Appearance.Options.UseBackColor = true;
            this.TxtOutstandingTC.Properties.Appearance.Options.UseFont = true;
            this.TxtOutstandingTC.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtOutstandingTC.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtOutstandingTC.Size = new System.Drawing.Size(152, 20);
            this.TxtOutstandingTC.TabIndex = 53;
            // 
            // TxtTotalAmt
            // 
            this.TxtTotalAmt.EnterMoveNextControl = true;
            this.TxtTotalAmt.Location = new System.Drawing.Point(151, 117);
            this.TxtTotalAmt.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotalAmt.Name = "TxtTotalAmt";
            this.TxtTotalAmt.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtTotalAmt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotalAmt.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotalAmt.Properties.Appearance.Options.UseFont = true;
            this.TxtTotalAmt.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotalAmt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotalAmt.Properties.ReadOnly = true;
            this.TxtTotalAmt.Size = new System.Drawing.Size(152, 20);
            this.TxtTotalAmt.TabIndex = 38;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(51, 121);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(95, 14);
            this.label11.TabIndex = 37;
            this.label11.Text = "Total Log+Balok";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Black;
            this.label28.Location = new System.Drawing.Point(53, 268);
            this.label28.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(93, 14);
            this.label28.TabIndex = 52;
            this.label28.Text = "Outstanding TC";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeRemark
            // 
            this.MeeRemark.EditValue = "";
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(517, 306);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 250;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(400, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(233, 20);
            this.MeeRemark.TabIndex = 85;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(464, 309);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 14);
            this.label5.TabIndex = 84;
            this.label5.Text = "Remark";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTC
            // 
            this.TxtTC.EnterMoveNextControl = true;
            this.TxtTC.Location = new System.Drawing.Point(151, 243);
            this.TxtTC.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTC.Name = "TxtTC";
            this.TxtTC.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTC.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTC.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTC.Properties.Appearance.Options.UseFont = true;
            this.TxtTC.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTC.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTC.Size = new System.Drawing.Size(152, 20);
            this.TxtTC.TabIndex = 51;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Black;
            this.label29.Location = new System.Drawing.Point(29, 247);
            this.label29.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(117, 14);
            this.label29.TabIndex = 50;
            this.label29.Text = "Transport Cost (TC)";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtNail
            // 
            this.TxtNail.EnterMoveNextControl = true;
            this.TxtNail.Location = new System.Drawing.Point(517, 222);
            this.TxtNail.Margin = new System.Windows.Forms.Padding(5);
            this.TxtNail.Name = "TxtNail";
            this.TxtNail.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtNail.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtNail.Properties.Appearance.Options.UseBackColor = true;
            this.TxtNail.Properties.Appearance.Options.UseFont = true;
            this.TxtNail.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtNail.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtNail.Size = new System.Drawing.Size(116, 20);
            this.TxtNail.TabIndex = 77;
            this.TxtNail.Validated += new System.EventHandler(this.TxtNail_Validated);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Black;
            this.label25.Location = new System.Drawing.Point(405, 225);
            this.label25.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(106, 14);
            this.label25.TabIndex = 76;
            this.label25.Text = "Number of Nail (s)";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtVoucherRequestDocNo
            // 
            this.TxtVoucherRequestDocNo.EnterMoveNextControl = true;
            this.TxtVoucherRequestDocNo.Location = new System.Drawing.Point(517, 243);
            this.TxtVoucherRequestDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtVoucherRequestDocNo.Name = "TxtVoucherRequestDocNo";
            this.TxtVoucherRequestDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtVoucherRequestDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtVoucherRequestDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtVoucherRequestDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtVoucherRequestDocNo.Properties.MaxLength = 16;
            this.TxtVoucherRequestDocNo.Properties.ReadOnly = true;
            this.TxtVoucherRequestDocNo.Size = new System.Drawing.Size(233, 20);
            this.TxtVoucherRequestDocNo.TabIndex = 79;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(400, 246);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(111, 14);
            this.label4.TabIndex = 78;
            this.label4.Text = "Voucher Request#";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(449, 267);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(62, 14);
            this.label6.TabIndex = 80;
            this.label6.Text = "Voucher#";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtVoucherDocNo
            // 
            this.TxtVoucherDocNo.EnterMoveNextControl = true;
            this.TxtVoucherDocNo.Location = new System.Drawing.Point(517, 264);
            this.TxtVoucherDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtVoucherDocNo.Name = "TxtVoucherDocNo";
            this.TxtVoucherDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtVoucherDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtVoucherDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtVoucherDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtVoucherDocNo.Properties.MaxLength = 16;
            this.TxtVoucherDocNo.Properties.ReadOnly = true;
            this.TxtVoucherDocNo.Size = new System.Drawing.Size(233, 20);
            this.TxtVoucherDocNo.TabIndex = 81;
            // 
            // DteQueueDt
            // 
            this.DteQueueDt.EditValue = null;
            this.DteQueueDt.EnterMoveNextControl = true;
            this.DteQueueDt.Location = new System.Drawing.Point(151, 54);
            this.DteQueueDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteQueueDt.Name = "DteQueueDt";
            this.DteQueueDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteQueueDt.Properties.Appearance.Options.UseFont = true;
            this.DteQueueDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteQueueDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteQueueDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteQueueDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteQueueDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteQueueDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteQueueDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteQueueDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteQueueDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteQueueDt.Size = new System.Drawing.Size(116, 20);
            this.DteQueueDt.TabIndex = 32;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(72, 58);
            this.label19.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(74, 14);
            this.label19.TabIndex = 31;
            this.label19.Text = "Queue Date";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnRawMaterialVerifyDocNo2
            // 
            this.BtnRawMaterialVerifyDocNo2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnRawMaterialVerifyDocNo2.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnRawMaterialVerifyDocNo2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRawMaterialVerifyDocNo2.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnRawMaterialVerifyDocNo2.Appearance.Options.UseBackColor = true;
            this.BtnRawMaterialVerifyDocNo2.Appearance.Options.UseFont = true;
            this.BtnRawMaterialVerifyDocNo2.Appearance.Options.UseForeColor = true;
            this.BtnRawMaterialVerifyDocNo2.Appearance.Options.UseTextOptions = true;
            this.BtnRawMaterialVerifyDocNo2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnRawMaterialVerifyDocNo2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnRawMaterialVerifyDocNo2.Image = ((System.Drawing.Image)(resources.GetObject("BtnRawMaterialVerifyDocNo2.Image")));
            this.BtnRawMaterialVerifyDocNo2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnRawMaterialVerifyDocNo2.Location = new System.Drawing.Point(372, 11);
            this.BtnRawMaterialVerifyDocNo2.Name = "BtnRawMaterialVerifyDocNo2";
            this.BtnRawMaterialVerifyDocNo2.Size = new System.Drawing.Size(24, 21);
            this.BtnRawMaterialVerifyDocNo2.TabIndex = 28;
            this.BtnRawMaterialVerifyDocNo2.ToolTip = "Show Verification Document";
            this.BtnRawMaterialVerifyDocNo2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnRawMaterialVerifyDocNo2.ToolTipTitle = "Run System";
            this.BtnRawMaterialVerifyDocNo2.Click += new System.EventHandler(this.BtnRawMaterialVerifyDocNo2_Click);
            // 
            // TxtQueueNo
            // 
            this.TxtQueueNo.EnterMoveNextControl = true;
            this.TxtQueueNo.Location = new System.Drawing.Point(151, 33);
            this.TxtQueueNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtQueueNo.Name = "TxtQueueNo";
            this.TxtQueueNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtQueueNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtQueueNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtQueueNo.Properties.Appearance.Options.UseFont = true;
            this.TxtQueueNo.Properties.MaxLength = 16;
            this.TxtQueueNo.Properties.ReadOnly = true;
            this.TxtQueueNo.Size = new System.Drawing.Size(245, 20);
            this.TxtQueueNo.TabIndex = 30;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(93, 37);
            this.label17.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(53, 14);
            this.label17.TabIndex = 29;
            this.label17.Text = "Queue#";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BtnRawMaterialVerifyDocNo
            // 
            this.BtnRawMaterialVerifyDocNo.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnRawMaterialVerifyDocNo.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnRawMaterialVerifyDocNo.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRawMaterialVerifyDocNo.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnRawMaterialVerifyDocNo.Appearance.Options.UseBackColor = true;
            this.BtnRawMaterialVerifyDocNo.Appearance.Options.UseFont = true;
            this.BtnRawMaterialVerifyDocNo.Appearance.Options.UseForeColor = true;
            this.BtnRawMaterialVerifyDocNo.Appearance.Options.UseTextOptions = true;
            this.BtnRawMaterialVerifyDocNo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnRawMaterialVerifyDocNo.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnRawMaterialVerifyDocNo.Image = ((System.Drawing.Image)(resources.GetObject("BtnRawMaterialVerifyDocNo.Image")));
            this.BtnRawMaterialVerifyDocNo.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnRawMaterialVerifyDocNo.Location = new System.Drawing.Point(342, 11);
            this.BtnRawMaterialVerifyDocNo.Name = "BtnRawMaterialVerifyDocNo";
            this.BtnRawMaterialVerifyDocNo.Size = new System.Drawing.Size(24, 21);
            this.BtnRawMaterialVerifyDocNo.TabIndex = 27;
            this.BtnRawMaterialVerifyDocNo.ToolTip = "Find Verification Document";
            this.BtnRawMaterialVerifyDocNo.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnRawMaterialVerifyDocNo.ToolTipTitle = "Run System";
            this.BtnRawMaterialVerifyDocNo.Click += new System.EventHandler(this.BtnRawMaterialVerifyDocNo_Click);
            // 
            // TxtRawMaterialVerifyDocNo
            // 
            this.TxtRawMaterialVerifyDocNo.EnterMoveNextControl = true;
            this.TxtRawMaterialVerifyDocNo.Location = new System.Drawing.Point(151, 12);
            this.TxtRawMaterialVerifyDocNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtRawMaterialVerifyDocNo.Name = "TxtRawMaterialVerifyDocNo";
            this.TxtRawMaterialVerifyDocNo.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtRawMaterialVerifyDocNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtRawMaterialVerifyDocNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtRawMaterialVerifyDocNo.Properties.Appearance.Options.UseFont = true;
            this.TxtRawMaterialVerifyDocNo.Properties.MaxLength = 30;
            this.TxtRawMaterialVerifyDocNo.Properties.ReadOnly = true;
            this.TxtRawMaterialVerifyDocNo.Size = new System.Drawing.Size(187, 20);
            this.TxtRawMaterialVerifyDocNo.TabIndex = 26;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Red;
            this.label18.Location = new System.Drawing.Point(70, 16);
            this.label18.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(76, 14);
            this.label18.TabIndex = 25;
            this.label18.Text = "Verification#";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtVdCode
            // 
            this.TxtVdCode.EnterMoveNextControl = true;
            this.TxtVdCode.Location = new System.Drawing.Point(151, 75);
            this.TxtVdCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtVdCode.Name = "TxtVdCode";
            this.TxtVdCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtVdCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtVdCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtVdCode.Properties.Appearance.Options.UseFont = true;
            this.TxtVdCode.Properties.MaxLength = 80;
            this.TxtVdCode.Properties.ReadOnly = true;
            this.TxtVdCode.Size = new System.Drawing.Size(242, 20);
            this.TxtVdCode.TabIndex = 34;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(99, 79);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(47, 14);
            this.label8.TabIndex = 33;
            this.label8.Text = "Vendor";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.panel7);
            this.tabPage2.Location = new System.Drawing.Point(4, 23);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(764, 372);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Log";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel7.Controls.Add(this.Grd1);
            this.panel7.Controls.Add(this.panel5);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel7.Location = new System.Drawing.Point(3, 3);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(758, 366);
            this.panel7.TabIndex = 1;
            // 
            // Grd1
            // 
            this.Grd1.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd1.DefaultRow.Height = 20;
            this.Grd1.DefaultRow.Sortable = false;
            this.Grd1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 21;
            this.Grd1.Location = new System.Drawing.Point(0, 33);
            this.Grd1.Name = "Grd1";
            this.Grd1.ReadOnly = true;
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd1.SingleClickEdit = true;
            this.Grd1.Size = new System.Drawing.Size(758, 333);
            this.Grd1.TabIndex = 31;
            this.Grd1.TreeCol = null;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.panel5.Controls.Add(this.TxtTotalLog2);
            this.panel5.Controls.Add(this.label39);
            this.panel5.Controls.Add(this.TxtTotalLog1);
            this.panel5.Controls.Add(this.label38);
            this.panel5.Controls.Add(this.TxtTotalLog);
            this.panel5.Controls.Add(this.label35);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(758, 33);
            this.panel5.TabIndex = 24;
            // 
            // TxtTotalLog2
            // 
            this.TxtTotalLog2.EnterMoveNextControl = true;
            this.TxtTotalLog2.Location = new System.Drawing.Point(481, 5);
            this.TxtTotalLog2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotalLog2.Name = "TxtTotalLog2";
            this.TxtTotalLog2.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotalLog2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotalLog2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotalLog2.Properties.Appearance.Options.UseFont = true;
            this.TxtTotalLog2.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotalLog2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotalLog2.Size = new System.Drawing.Size(84, 20);
            this.TxtTotalLog2.TabIndex = 30;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.ForeColor = System.Drawing.Color.Black;
            this.label39.Location = new System.Drawing.Point(409, 7);
            this.label39.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(68, 14);
            this.label39.TabIndex = 29;
            this.label39.Text = "Total Kubik";
            this.label39.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotalLog1
            // 
            this.TxtTotalLog1.EnterMoveNextControl = true;
            this.TxtTotalLog1.Location = new System.Drawing.Point(317, 5);
            this.TxtTotalLog1.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotalLog1.Name = "TxtTotalLog1";
            this.TxtTotalLog1.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotalLog1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotalLog1.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotalLog1.Properties.Appearance.Options.UseFont = true;
            this.TxtTotalLog1.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotalLog1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotalLog1.Size = new System.Drawing.Size(84, 20);
            this.TxtTotalLog1.TabIndex = 28;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.ForeColor = System.Drawing.Color.Black;
            this.label38.Location = new System.Drawing.Point(236, 8);
            this.label38.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(77, 14);
            this.label38.TabIndex = 27;
            this.label38.Text = "Total Batang";
            this.label38.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotalLog
            // 
            this.TxtTotalLog.EnterMoveNextControl = true;
            this.TxtTotalLog.Location = new System.Drawing.Point(52, 6);
            this.TxtTotalLog.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotalLog.Name = "TxtTotalLog";
            this.TxtTotalLog.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotalLog.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotalLog.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotalLog.Properties.Appearance.Options.UseFont = true;
            this.TxtTotalLog.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotalLog.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotalLog.Size = new System.Drawing.Size(176, 20);
            this.TxtTotalLog.TabIndex = 26;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.Color.Black;
            this.label35.Location = new System.Drawing.Point(10, 9);
            this.label35.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(35, 14);
            this.label35.TabIndex = 25;
            this.label35.Text = "Total";
            this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.panel8);
            this.tabPage3.Location = new System.Drawing.Point(4, 23);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(764, 372);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Balok";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel8.Controls.Add(this.Grd2);
            this.panel8.Controls.Add(this.panel10);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel8.Location = new System.Drawing.Point(0, 0);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(764, 372);
            this.panel8.TabIndex = 1;
            // 
            // Grd2
            // 
            this.Grd2.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd2.DefaultRow.Height = 20;
            this.Grd2.DefaultRow.Sortable = false;
            this.Grd2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd2.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd2.Header.Height = 21;
            this.Grd2.Location = new System.Drawing.Point(0, 33);
            this.Grd2.Name = "Grd2";
            this.Grd2.ReadOnly = true;
            this.Grd2.RowHeader.Visible = true;
            this.Grd2.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd2.SingleClickEdit = true;
            this.Grd2.Size = new System.Drawing.Size(764, 339);
            this.Grd2.TabIndex = 32;
            this.Grd2.TreeCol = null;
            this.Grd2.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.panel10.Controls.Add(this.TxtTotalBalok2);
            this.panel10.Controls.Add(this.label40);
            this.panel10.Controls.Add(this.TxtTotalBalok1);
            this.panel10.Controls.Add(this.label41);
            this.panel10.Controls.Add(this.TxtTotalBalok);
            this.panel10.Controls.Add(this.label36);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel10.Location = new System.Drawing.Point(0, 0);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(764, 33);
            this.panel10.TabIndex = 25;
            // 
            // TxtTotalBalok2
            // 
            this.TxtTotalBalok2.EnterMoveNextControl = true;
            this.TxtTotalBalok2.Location = new System.Drawing.Point(478, 6);
            this.TxtTotalBalok2.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotalBalok2.Name = "TxtTotalBalok2";
            this.TxtTotalBalok2.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotalBalok2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotalBalok2.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotalBalok2.Properties.Appearance.Options.UseFont = true;
            this.TxtTotalBalok2.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotalBalok2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotalBalok2.Size = new System.Drawing.Size(84, 20);
            this.TxtTotalBalok2.TabIndex = 31;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.ForeColor = System.Drawing.Color.Black;
            this.label40.Location = new System.Drawing.Point(406, 9);
            this.label40.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(68, 14);
            this.label40.TabIndex = 30;
            this.label40.Text = "Total Kubik";
            this.label40.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotalBalok1
            // 
            this.TxtTotalBalok1.EnterMoveNextControl = true;
            this.TxtTotalBalok1.Location = new System.Drawing.Point(314, 6);
            this.TxtTotalBalok1.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotalBalok1.Name = "TxtTotalBalok1";
            this.TxtTotalBalok1.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotalBalok1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotalBalok1.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotalBalok1.Properties.Appearance.Options.UseFont = true;
            this.TxtTotalBalok1.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotalBalok1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotalBalok1.Size = new System.Drawing.Size(84, 20);
            this.TxtTotalBalok1.TabIndex = 29;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.ForeColor = System.Drawing.Color.Black;
            this.label41.Location = new System.Drawing.Point(233, 9);
            this.label41.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(77, 14);
            this.label41.TabIndex = 28;
            this.label41.Text = "Total Batang";
            this.label41.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTotalBalok
            // 
            this.TxtTotalBalok.EnterMoveNextControl = true;
            this.TxtTotalBalok.Location = new System.Drawing.Point(52, 6);
            this.TxtTotalBalok.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTotalBalok.Name = "TxtTotalBalok";
            this.TxtTotalBalok.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTotalBalok.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTotalBalok.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTotalBalok.Properties.Appearance.Options.UseFont = true;
            this.TxtTotalBalok.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTotalBalok.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTotalBalok.Size = new System.Drawing.Size(176, 20);
            this.TxtTotalBalok.TabIndex = 27;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.ForeColor = System.Drawing.Color.Black;
            this.label36.Location = new System.Drawing.Point(10, 9);
            this.label36.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(35, 14);
            this.label36.TabIndex = 26;
            this.label36.Text = "Total";
            this.label36.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.Grd3);
            this.tabPage4.Location = new System.Drawing.Point(4, 23);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(764, 372);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Inventory";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // Grd3
            // 
            this.Grd3.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd3.DefaultRow.Height = 20;
            this.Grd3.DefaultRow.Sortable = false;
            this.Grd3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd3.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd3.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd3.Header.Height = 21;
            this.Grd3.Location = new System.Drawing.Point(0, 0);
            this.Grd3.Name = "Grd3";
            this.Grd3.ReadOnly = true;
            this.Grd3.RowHeader.Visible = true;
            this.Grd3.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd3.SingleClickEdit = true;
            this.Grd3.Size = new System.Drawing.Size(764, 372);
            this.Grd3.TabIndex = 33;
            this.Grd3.TreeCol = null;
            this.Grd3.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.Grd4);
            this.tabPage5.Location = new System.Drawing.Point(4, 23);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(764, 372);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Retur";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // Grd4
            // 
            this.Grd4.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd4.DefaultRow.Height = 20;
            this.Grd4.DefaultRow.Sortable = false;
            this.Grd4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd4.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd4.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd4.Header.Height = 21;
            this.Grd4.Location = new System.Drawing.Point(0, 0);
            this.Grd4.Name = "Grd4";
            this.Grd4.ReadOnly = true;
            this.Grd4.RowHeader.Visible = true;
            this.Grd4.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd4.SingleClickEdit = true;
            this.Grd4.Size = new System.Drawing.Size(764, 372);
            this.Grd4.TabIndex = 34;
            this.Grd4.TreeCol = null;
            this.Grd4.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // ChkHideInfoInGrd
            // 
            this.ChkHideInfoInGrd.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ChkHideInfoInGrd.EditValue = true;
            this.ChkHideInfoInGrd.Location = new System.Drawing.Point(0, 451);
            this.ChkHideInfoInGrd.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ChkHideInfoInGrd.Name = "ChkHideInfoInGrd";
            this.ChkHideInfoInGrd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkHideInfoInGrd.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseFont = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkHideInfoInGrd.Properties.Appearance.Options.UseTextOptions = true;
            this.ChkHideInfoInGrd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.ChkHideInfoInGrd.Properties.Caption = "Hide";
            this.ChkHideInfoInGrd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkHideInfoInGrd.Size = new System.Drawing.Size(70, 22);
            this.ChkHideInfoInGrd.TabIndex = 8;
            this.ChkHideInfoInGrd.ToolTip = "Hide some informations in the list";
            this.ChkHideInfoInGrd.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.ChkHideInfoInGrd.ToolTipTitle = "Run System";
            this.ChkHideInfoInGrd.CheckedChanged += new System.EventHandler(this.ChkHideInfoInGrd_CheckedChanged);
            // 
            // FrmPurchaseInvoiceRawMaterial
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 473);
            this.Name = "FrmPurchaseInvoiceRawMaterial";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueDocType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGrandTotal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCurCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkCancelInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDocDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueAcType.Properties)).EndInit();
            this.panel4.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxSlip.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRoundingValue.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkTaxInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBankAcCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtGiroNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePaymentType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBankCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDueDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteDueDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPaidToBankAcName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPaidToBankBranch.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePaidToBankCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPaymentUser.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPaidToBankAcNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTin.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTax.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTaxPercentage.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTTCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAmt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTransportCost.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtOutstandingAmt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtOutstandingTC.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalAmt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTC.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtNail.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVoucherRequestDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVoucherDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteQueueDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteQueueDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtQueueNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRawMaterialVerifyDocNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVdCode.Properties)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalLog2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalLog1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalLog.Properties)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).EndInit();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalBalok2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalBalok1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTotalBalok.Properties)).EndInit();
            this.tabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).EndInit();
            this.tabPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkHideInfoInGrd.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private System.Windows.Forms.Label label5;
        private DevExpress.XtraEditors.CheckEdit ChkCancelInd;
        internal DevExpress.XtraEditors.TextEdit TxtDocNo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel8;
        private DevExpress.XtraEditors.LookUpEdit LueAcType;
        private System.Windows.Forms.Label label16;
        internal DevExpress.XtraEditors.DateEdit DteQueueDt;
        private System.Windows.Forms.Label label19;
        public DevExpress.XtraEditors.SimpleButton BtnRawMaterialVerifyDocNo2;
        internal DevExpress.XtraEditors.TextEdit TxtQueueNo;
        private System.Windows.Forms.Label label17;
        public DevExpress.XtraEditors.SimpleButton BtnRawMaterialVerifyDocNo;
        internal DevExpress.XtraEditors.TextEdit TxtRawMaterialVerifyDocNo;
        private System.Windows.Forms.Label label18;
        internal DevExpress.XtraEditors.TextEdit TxtVdCode;
        private System.Windows.Forms.Label label8;
        internal DevExpress.XtraEditors.DateEdit DteDocDt;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtVoucherRequestDocNo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        internal DevExpress.XtraEditors.TextEdit TxtVoucherDocNo;
        internal DevExpress.XtraEditors.TextEdit TxtNail;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel5;
        internal DevExpress.XtraEditors.TextEdit TxtTransportCost;
        private System.Windows.Forms.Label label27;
        internal DevExpress.XtraEditors.TextEdit TxtOutstandingTC;
        private System.Windows.Forms.Label label28;
        internal DevExpress.XtraEditors.TextEdit TxtTC;
        private System.Windows.Forms.Label label29;
        internal DevExpress.XtraEditors.TextEdit TxtCurCode;
        internal DevExpress.XtraEditors.TextEdit TxtAmt;
        private System.Windows.Forms.Label label9;
        internal DevExpress.XtraEditors.TextEdit TxtOutstandingAmt;
        private System.Windows.Forms.Label label7;
        internal DevExpress.XtraEditors.TextEdit TxtTotalAmt;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label26;
        internal DevExpress.XtraEditors.TextEdit TxtTTCode;
        private System.Windows.Forms.Label label30;
        internal DevExpress.XtraEditors.TextEdit TxtTax;
        private System.Windows.Forms.Label label31;
        internal DevExpress.XtraEditors.TextEdit TxtTaxPercentage;
        private System.Windows.Forms.Label label32;
        internal DevExpress.XtraEditors.TextEdit TxtGrandTotal;
        private System.Windows.Forms.Label label34;
        private DevExpress.XtraEditors.LookUpEdit LueBankAcCode;
        private System.Windows.Forms.Label label13;
        internal DevExpress.XtraEditors.TextEdit TxtGiroNo;
        private DevExpress.XtraEditors.LookUpEdit LuePaymentType;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label10;
        private DevExpress.XtraEditors.LookUpEdit LueBankCode;
        private System.Windows.Forms.Label label14;
        internal DevExpress.XtraEditors.DateEdit DteDueDt;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label23;
        internal DevExpress.XtraEditors.TextEdit TxtPaidToBankAcName;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        internal DevExpress.XtraEditors.TextEdit TxtPaidToBankBranch;
        private DevExpress.XtraEditors.LookUpEdit LuePaidToBankCode;
        private System.Windows.Forms.Label label22;
        internal DevExpress.XtraEditors.TextEdit TxtPaymentUser;
        private System.Windows.Forms.Label label24;
        internal DevExpress.XtraEditors.TextEdit TxtPaidToBankAcNo;
        internal DevExpress.XtraEditors.TextEdit TxtTin;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Panel panel9;
        protected internal TenTec.Windows.iGridLib.iGrid Grd1;
        internal DevExpress.XtraEditors.TextEdit TxtTotalLog;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Panel panel10;
        internal DevExpress.XtraEditors.TextEdit TxtTotalBalok;
        private System.Windows.Forms.Label label36;
        protected internal TenTec.Windows.iGridLib.iGrid Grd2;
        protected DevExpress.XtraEditors.CheckEdit ChkHideInfoInGrd;
        private System.Windows.Forms.Label label37;
        internal DevExpress.XtraEditors.TextEdit TxtTotalLog2;
        private System.Windows.Forms.Label label39;
        internal DevExpress.XtraEditors.TextEdit TxtTotalLog1;
        private System.Windows.Forms.Label label38;
        internal DevExpress.XtraEditors.TextEdit TxtTotalBalok2;
        private System.Windows.Forms.Label label40;
        internal DevExpress.XtraEditors.TextEdit TxtTotalBalok1;
        private System.Windows.Forms.Label label41;
        internal DevExpress.XtraEditors.CheckEdit ChkTaxInd;
        internal DevExpress.XtraEditors.LookUpEdit LueDocType;
        internal DevExpress.XtraEditors.TextEdit TxtRoundingValue;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.TabPage tabPage4;
        protected internal TenTec.Windows.iGridLib.iGrid Grd3;
        private System.Windows.Forms.TabPage tabPage5;
        protected internal TenTec.Windows.iGridLib.iGrid Grd4;
        private System.Windows.Forms.Label label43;
        internal DevExpress.XtraEditors.TextEdit TxtTaxSlip;

    }
}