﻿namespace RunSystem
{
    partial class FrmAllowanceDeduction
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.LueAmtType = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtADName = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtADCode = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.LueADType = new DevExpress.XtraEditors.LookUpEdit();
            this.ChkTaxInd = new DevExpress.XtraEditors.CheckEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.LueCostCenterGroup = new DevExpress.XtraEditors.LookUpEdit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueAmtType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtADName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtADCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueADType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkTaxInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCostCenterGroup.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.LueCostCenterGroup);
            this.panel2.Controls.Add(this.ChkTaxInd);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.LueADType);
            this.panel2.Controls.Add(this.MeeRemark);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.LueAmtType);
            this.panel2.Controls.Add(this.TxtADName);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.TxtADCode);
            this.panel2.Controls.Add(this.label1);
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(185, 145);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 350;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(400, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(319, 20);
            this.MeeRemark.TabIndex = 20;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(134, 147);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(47, 14);
            this.label6.TabIndex = 19;
            this.label6.Text = "Remark";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(98, 102);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(83, 14);
            this.label5.TabIndex = 15;
            this.label5.Text = "Amount Type";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueAmtType
            // 
            this.LueAmtType.EnterMoveNextControl = true;
            this.LueAmtType.Location = new System.Drawing.Point(185, 100);
            this.LueAmtType.Margin = new System.Windows.Forms.Padding(5);
            this.LueAmtType.Name = "LueAmtType";
            this.LueAmtType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAmtType.Properties.Appearance.Options.UseFont = true;
            this.LueAmtType.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAmtType.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueAmtType.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAmtType.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueAmtType.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAmtType.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueAmtType.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAmtType.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueAmtType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueAmtType.Properties.DropDownRows = 4;
            this.LueAmtType.Properties.NullText = "[Empty]";
            this.LueAmtType.Properties.PopupWidth = 500;
            this.LueAmtType.Size = new System.Drawing.Size(137, 20);
            this.LueAmtType.TabIndex = 16;
            this.LueAmtType.ToolTip = "F4 : Show/hide list";
            this.LueAmtType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueAmtType.EditValueChanged += new System.EventHandler(this.LueAmtType_EditValueChanged);
            // 
            // TxtADName
            // 
            this.TxtADName.EnterMoveNextControl = true;
            this.TxtADName.Location = new System.Drawing.Point(185, 56);
            this.TxtADName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtADName.Name = "TxtADName";
            this.TxtADName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtADName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtADName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtADName.Properties.Appearance.Options.UseFont = true;
            this.TxtADName.Properties.MaxLength = 40;
            this.TxtADName.Size = new System.Drawing.Size(319, 20);
            this.TxtADName.TabIndex = 12;
            this.TxtADName.Validated += new System.EventHandler(this.TxtADName_Validated);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(23, 59);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(158, 14);
            this.label2.TabIndex = 11;
            this.label2.Text = "Allowance/Deduction Name";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtADCode
            // 
            this.TxtADCode.EnterMoveNextControl = true;
            this.TxtADCode.Location = new System.Drawing.Point(185, 34);
            this.TxtADCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtADCode.Name = "TxtADCode";
            this.TxtADCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtADCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtADCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtADCode.Properties.Appearance.Options.UseFont = true;
            this.TxtADCode.Properties.MaxLength = 16;
            this.TxtADCode.Size = new System.Drawing.Size(137, 20);
            this.TxtADCode.TabIndex = 10;
            this.TxtADCode.Validated += new System.EventHandler(this.TxtADCode_Validated);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(26, 37);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(155, 14);
            this.label1.TabIndex = 9;
            this.label1.Text = "Allowance/Deduction Code";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(146, 81);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 14);
            this.label3.TabIndex = 13;
            this.label3.Text = "Type";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueADType
            // 
            this.LueADType.EnterMoveNextControl = true;
            this.LueADType.Location = new System.Drawing.Point(186, 78);
            this.LueADType.Margin = new System.Windows.Forms.Padding(5);
            this.LueADType.Name = "LueADType";
            this.LueADType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueADType.Properties.Appearance.Options.UseFont = true;
            this.LueADType.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueADType.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueADType.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueADType.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueADType.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueADType.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueADType.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueADType.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueADType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueADType.Properties.DropDownRows = 4;
            this.LueADType.Properties.NullText = "[Empty]";
            this.LueADType.Properties.PopupWidth = 500;
            this.LueADType.Size = new System.Drawing.Size(137, 20);
            this.LueADType.TabIndex = 14;
            this.LueADType.ToolTip = "F4 : Show/hide list";
            this.LueADType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueADType.EditValueChanged += new System.EventHandler(this.LueADType_EditValueChanged);
            // 
            // ChkTaxInd
            // 
            this.ChkTaxInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkTaxInd.Location = new System.Drawing.Point(185, 166);
            this.ChkTaxInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkTaxInd.Name = "ChkTaxInd";
            this.ChkTaxInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkTaxInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkTaxInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkTaxInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkTaxInd.Properties.Appearance.Options.UseFont = true;
            this.ChkTaxInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkTaxInd.Properties.Caption = "Taxable";
            this.ChkTaxInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkTaxInd.Size = new System.Drawing.Size(84, 22);
            this.ChkTaxInd.TabIndex = 21;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(72, 125);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(109, 14);
            this.label4.TabIndex = 17;
            this.label4.Text = "Cost Center Group";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueCostCenterGroup
            // 
            this.LueCostCenterGroup.EnterMoveNextControl = true;
            this.LueCostCenterGroup.Location = new System.Drawing.Point(185, 122);
            this.LueCostCenterGroup.Margin = new System.Windows.Forms.Padding(5);
            this.LueCostCenterGroup.Name = "LueCostCenterGroup";
            this.LueCostCenterGroup.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCostCenterGroup.Properties.Appearance.Options.UseFont = true;
            this.LueCostCenterGroup.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCostCenterGroup.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCostCenterGroup.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCostCenterGroup.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCostCenterGroup.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCostCenterGroup.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCostCenterGroup.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCostCenterGroup.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCostCenterGroup.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCostCenterGroup.Properties.DropDownRows = 4;
            this.LueCostCenterGroup.Properties.NullText = "[Empty]";
            this.LueCostCenterGroup.Properties.PopupWidth = 500;
            this.LueCostCenterGroup.Size = new System.Drawing.Size(179, 20);
            this.LueCostCenterGroup.TabIndex = 18;
            this.LueCostCenterGroup.ToolTip = "F4 : Show/hide list";
            this.LueCostCenterGroup.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCostCenterGroup.EditValueChanged += new System.EventHandler(this.LueCostCenterGroup_EditValueChanged);
            // 
            // FrmAllowanceDeduction
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 233);
            this.Name = "FrmAllowanceDeduction";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueAmtType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtADName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtADCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueADType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkTaxInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCostCenterGroup.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.LookUpEdit LueADType;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private DevExpress.XtraEditors.LookUpEdit LueAmtType;
        private DevExpress.XtraEditors.TextEdit TxtADName;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtADCode;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.CheckEdit ChkTaxInd;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.LookUpEdit LueCostCenterGroup;
    }
}