﻿#region Update
/*
    22/01/2021 [WED/IMS] new apps
    26/03/2021 [WED/IMS] Uom nya bisa isi manual, default nya sudah terisi sesuai item sebelumnya
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DevExpress.XtraEditors;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmMaterialRequest4Dlg6 : RunSystem.FrmBase12
    {
        #region Field, Property

        private string 
            mSQL = string.Empty,
            mItCtCode = string.Empty,
            mItCtName = string.Empty,
            mUom = string.Empty,
            mCopyItCode = string.Empty
            ;
        private FrmMaterialRequest4 mFrmParent;

        #endregion

        #region Constructor

        public FrmMaterialRequest4Dlg6(FrmMaterialRequest4 FrmParent, string CopyItCode, string ItCtCode, string ItCtName, string UoM)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mCopyItCode = CopyItCode;
            mItCtCode = ItCtCode;
            mItCtName = ItCtName;
            mUom = UoM;
        }

        #endregion

        #region Methods

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sl.SetLueUomCode(ref LueUomCode);
                SetHdr();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        override protected void BtnProcessClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", string.Empty, mFrmParent.mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            ProcessData();
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtItCtCode, "Category", false) ||
                Sm.IsLueEmpty(LueUomCode, "UoM") ||
                Sm.IsTxtEmpty(TxtItName, "Item", false)
                ;
        }

        private void ProcessData()
        {
            string ItCode = string.Empty;
            string ItCtCode = mItCtCode;
            int ItCtCodeLength = ItCtCode.Length + 1;
            var cml = new List<MySqlCommand>();

            ItCode = GetItCode(ItCtCode);

            cml.Add(SaveItem(ItCode));

            Sm.ExecCommands(cml);

            Sm.StdMsg(mMsgType.Info, "Item is saved with item code : " + ItCode);
            this.Close();
        }

        private string GetItCode(string ItCtCode)
        {
            string ItCode = string.Empty;

            if (mFrmParent.mIsItCodeUseItSeqNo)
            {
                ItCode = Sm.GetValue(
                     "Select Concat(@Param1,'-', " +
                     "   IfNull((Select Right(Concat(Repeat('0', " + mFrmParent.mItCodeSeqNo + "), Convert(V+1, Char)), " + mFrmParent.mItCodeSeqNo + ") From ( " +
                     "      Select ItSeqNo As V " +
                     "      From TblItem " +
                     "      Where Left(ItCode, @Param2)=Concat(@Param1, '-') " +
                     "      Order By ItSeqNo Desc Limit 1 " +
                     "      ) As Tbl), Right(Concat(Repeat('0', " + mFrmParent.mItCodeSeqNo + "), '1'), " + mFrmParent.mItCodeSeqNo + ") " +
                     "      )) As ItCode; ",
                     ItCtCode, (ItCtCode.Length + 1).ToString(), string.Empty);
            }
            else
            {
                ItCode = Sm.GetValue(
                     "Select Concat(@Param1,'-', " +
                     "   IfNull((Select Right(Concat('00000', Convert(ItCode+1, Char)), 5) From ( " +
                     "       Select Convert(Right(ItCode, 5), Decimal) As ItCode " +
                     "      From TblItem " +
                     "      Where Left(ItCode, @Param2)=Concat(@Param1, '-') " +
                     "      Order By Right(ItCode, 5) Desc Limit 1 " +
                     "       ) As TblItemTemp), '00001')) As ItCode; ",
                     ItCtCode, (ItCtCode.Length + 1).ToString(), string.Empty);
            }

            return ItCode;
        }

        private MySqlCommand SaveItem(string ItCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblItem(ItCode, ItName, ItSeqNo, ForeignName, ItCodeInternal, ItCodeOld, ItCtCode, ItScCode, ItemRequestDocNo, ");
            SQL.AppendLine("    ItBrCode, ItGrpCode, Specification, Remark, VdCode, PurchaseUomCode, PGCode, ");
            SQL.AppendLine("    TaxCode1, TaxCode2, TaxCode3, LengthUomCode, HeightUomCode, DiameterUomCode, ");
            SQL.AppendLine("    WidthUomCode, VolumeUomCode, WeightUomCode, SalesUomCode, SalesUomCode2, SalesUomCodeConvert12, ");
            SQL.AppendLine("    SalesUomCodeConvert21, InventoryUomCode, InventoryUomCode2, InventoryUomCode3, PlanningUomCode, PlanningUomCode2, ");
            SQL.AppendLine("    MinOrder, Length, Height, Width, Volume, Weight, Diameter, ");
            SQL.AppendLine("    MinStock, MaxStock, ReOrderStock, ControlByDeptCode, ActInd, InventoryItemInd, ");
            SQL.AppendLine("    SalesItemInd, PurchaseItemInd, FixedItemInd, PlanningItemInd, ServiceItemInd,TaxLiableInd, ");
            SQL.AppendLine("    InventoryUomCodeConvert12, InventoryUomCodeConvert13, InventoryUomCodeConvert21, InventoryUomCodeConvert23, ");
            SQL.AppendLine("    InventoryUomCodeConvert31, InventoryUomCodeConvert32, ");
            SQL.AppendLine("    PlanningUomCodeConvert12, PlanningUomCodeConvert21, ");
            SQL.AppendLine("    Information1, Information2, Information3, Information4, Information5, HSCode, ");
            SQL.AppendLine("    CreateBy, CreateDt) ");
            SQL.AppendLine("Select @ItCode, @ItName, @ItSeqNo, NULL, NULL, NULL, ItCtCode, NULL, NULL, ");
            SQL.AppendLine("    NULL, NULL, NULL, @Remark, NULL, @UomCode, NULL, ");
            SQL.AppendLine("    NULL, NULL, NULL, NULL, NULL, NULL, ");
            SQL.AppendLine("    NULL, NULL, NULL, @UomCode, @UomCode, 1, ");
            SQL.AppendLine("    1, @UomCode, @UomCode, @UomCode, @UomCode, @UomCode, ");
            SQL.AppendLine("    0, 0, 0, 0, 0, 0, 0, ");
            SQL.AppendLine("    0, 0, 0, NULL, 'Y', InventoryItemInd, ");
            SQL.AppendLine("    SalesItemInd, PurchaseItemInd, FixedItemInd, PlanningItemInd, ServiceItemInd, TaxLiableInd, ");
            SQL.AppendLine("    1, 1, 1, 1, ");
            SQL.AppendLine("    1, 1, ");
            SQL.AppendLine("    1, 1, ");
            SQL.AppendLine("    NULL, NULL, NULL, NULL, NULL, NULL, ");
            SQL.AppendLine("    @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblItem Where ItCode = @CopyItCode; ");
            
            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@CopyItCode", mCopyItCode);
            Sm.CmParam<String>(ref cm, "@ItCode", ItCode);
            Sm.CmParam<String>(ref cm, "@ItName", TxtItName.Text);
            Sm.CmParam<String>(ref cm, "@UomCode", Sm.GetLue(LueUomCode));
            if (mFrmParent.mIsItCodeUseItSeqNo)
                Sm.CmParam<Decimal>(ref cm, "@ItSeqNo", decimal.Parse(Sm.Right(ItCode, int.Parse(mFrmParent.mItCodeSeqNo))));
            else
                Sm.CmParam<Decimal>(ref cm, "@ItSeqNo", 0m);
            Sm.CmParam<String>(ref cm, "@ItCtCode", mItCtCode);
            Sm.CmParam<String>(ref cm, "@Remark", "Auto created on Purchase Order for Service");
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Additional Methods

        private void SetHdr()
        {
            TxtItCtCode.EditValue = mItCtCode;
            TxtItCtName.EditValue = mItCtName;
            Sm.SetLue(LueUomCode, mUom);
        }

        #endregion

        #endregion

        #region Events

        #region Misc Control Events

        private void LueUomCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueUomCode, new Sm.RefreshLue1(Sl.SetLueUomCode));
        }

        #endregion

        #endregion
    }
}
