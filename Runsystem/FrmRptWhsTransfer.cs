﻿#region Update
/*
    12/09/2017 [TKG] Reporting baru untuk transfer item antar gudang  
    29/09/2017 [TKG] tambah receiving from production 
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptWhsTransfer : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty;
        private int mNumberOfInventoryUomCode = 1;
        private bool mIsFilterByItCt = false; 
        
        #endregion

        #region Constructor

        public FrmRptWhsTransfer(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                Sl.SetLueWhsCode(ref LueWhsCode);
                Sl.SetLueWhsCode(ref LueWhsCode2);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T1.DocNo, T1.DocDt, T2.WhsName, T3.WhsName As WhsName2, ");
            SQL.AppendLine("T1.ItCode, T4.ItName, T4.ItCodeInternal, T1.BatchNo, T1.Source, T1.Lot, T1.Bin, ");
            SQL.AppendLine("T1.Qty, T4.InventoryUomCode, T1.Qty2, T4.InventoryUomCode2, T1.Qty3, T4.InventoryUomCode3, T1.Remark ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select A.DocNo, A.DocDt, A.WhsCode, A.WhsCode2, ");
            SQL.AppendLine("    C.ItCode, C.BatchNo, C.Source, C.Lot, C.Bin, ");
            SQL.AppendLine("    B.Qty, B.Qty2, B.Qty3, B.Remark ");
            SQL.AppendLine("    From TblRecvWhsHdr A ");
            SQL.AppendLine("    Inner Join TblRecvWhsDtl B On A.DocNo=B.DocNo And B.CancelInd='N' ");
            SQL.AppendLine("    Inner Join TblDOWhsDtl C On B.DOWhsDocNo=C.DocNo And B.DOWhsDNo=C.DNo ");
            SQL.AppendLine("    Where A.DocDt Between @DocDt1 And @DocDt2 ");

            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select A.DocNo, A.DocDt, A.WhsCode, A.WhsCode2, ");
            SQL.AppendLine("    B.ItCode, B.BatchNo, B.Source, B.Lot, B.Bin, ");
            SQL.AppendLine("    B.Qty, B.Qty2, B.Qty3, B.Remark ");
            SQL.AppendLine("    From TblRecvWhs2Hdr A ");
            SQL.AppendLine("    Inner Join TblRecvWhs2Dtl B On A.DocNo=B.DocNo And B.CancelInd='N' ");
            SQL.AppendLine("    Where A.DocDt Between @DocDt1 And @DocDt2 ");

            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select A.DocNo, A.DocDt, A.WhsCode, A.WhsCode2, ");
            SQL.AppendLine("    B.ItCode, B.BatchNo, B.Source, B.Lot, B.Bin, ");
            SQL.AppendLine("    B.Qty, B.Qty2, B.Qty3, B.Remark ");
            SQL.AppendLine("    From TblRecvWhs3Hdr A ");
            SQL.AppendLine("    Inner Join TblRecvWhs3Dtl B On A.DocNo=B.DocNo And B.CancelInd='N' ");
            SQL.AppendLine("    Where A.DocDt Between @DocDt1 And @DocDt2 ");

            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select A.DocNo, A.DocDt, A.WhsCode, A.WhsCode2, ");
            SQL.AppendLine("    C.ItCode, C.BatchNo, C.Source, C.Lot, C.Bin, ");
            SQL.AppendLine("    B.Qty, B.Qty2, B.Qty3, B.Remark ");
            SQL.AppendLine("    From TblRecvWhs4Hdr A ");
            SQL.AppendLine("    Inner Join TblRecvWhs4Dtl B On A.DocNo=B.DocNo And B.CancelInd='N' ");
            SQL.AppendLine("    Inner Join TblDOWhsDtl C On B.DOWhsDocNo=C.DocNo And B.DOWhsDNo=C.DNo ");
            SQL.AppendLine("    Where A.DocDt Between @DocDt1 And @DocDt2 ");

            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select A.DocNo, A.DocDt, B.WhsCode2 As WhsCode, B.WhsCode As WhsCode2, ");
            SQL.AppendLine("    D.ItCode, D.BatchNo, D.Source, D.Lot, D.Bin, ");
            SQL.AppendLine("    C.Qty, C.Qty2, C.Qty3, C.Remark ");
            SQL.AppendLine("    From TblBinTransferHdr A ");
            SQL.AppendLine("    Inner Join TblBinTransferRequestHdr B On A.BinTransferRequestDocNo=B.DocNo ");
            SQL.AppendLine("    Inner Join TblBinTransferDtl C On A.DocNo=C.DocNo ");
            SQL.AppendLine("    Inner Join TblBinTransferRequestDtl2 D On A.BinTransferRequestDocNo=D.DocNo And C.BinTransferRequestDNo=D.DNo ");
            SQL.AppendLine("    Where A.DocDt Between @DocDt1 And @DocDt2 ");

            SQL.AppendLine(") T1 ");
            SQL.AppendLine("Inner Join TblWarehouse T2 On T1.WhsCode=T2.WhsCode ");
            SQL.AppendLine("Inner Join TblWarehouse T3 On T1.WhsCode2=T3.WhsCode ");
            SQL.AppendLine("Inner Join TblItem T4 On T1.ItCode=T4.ItCode ");
            if (mIsFilterByItCt)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select 1 From TblGroupItemCategory ");
                SQL.AppendLine("    Where ItCtCode=T4.ItCtCode ");
                SQL.AppendLine("    And GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("    ) ");
            }

            mSQL = SQL.ToString();
        }

        private void GetParameter()
        {
            mIsFilterByItCt = Sm.GetParameterBoo("IsFilterByItCt");
            var NumberOfInventoryUomCode = Sm.GetParameter("NumberOfInventoryUomCode"); 
            if (NumberOfInventoryUomCode.Length != 0)
                mNumberOfInventoryUomCode = int.Parse(NumberOfInventoryUomCode);
        }
        
        private void SetGrd()
        {
            Grd1.Cols.Count = 19;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#",
                        "Date",
                        "From",
                        "To",
                        "Item's Code",

                        //6-10
                        "Item's Name",
                        "Local Code",
                        "Batch#",
                        "Source",
                        "Lot",

                        //11-15
                        "Bin",
                        "Quantity",
                        "UoM",                        
                        "Quantity", 
                        "UoM",

                        //16-18
                        "Quantity", 
                        "UoM",
                        "Remark"
                    },
                    new int[] 
                    {
                        //0
                        50,

                        //1-5
                        130, 80, 200, 200, 100, 
                        
                        //6-10
                        200, 100, 180, 180, 60,
 
                        //11-15
                        60, 80, 80, 80, 80, 

                        //16-18
                        60, 80, 400
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 2 });
            Sm.GrdFormatDec(Grd1, new int[] { 12, 14, 16 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 5, 7, 9, 10, 11, 14, 15, 16, 17 }, false);
            ShowInventoryUomCode();
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 5, 7, 9, 10, 11, }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueWhsCode2), "T1.WhsCode2", true);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueWhsCode), "T1.WhsCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "T4.ItCode", "T4.ItName", "T4.ItCodeInternal" });                

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By T1.DocDt, T1.DocNo;",
                        new string[]
                        {
                            //0
                            "DocNo",  

                            //1-5
                            "DocDt", "WhsName2", "WhsName", "ItCode", "ItName", 
                            
                            //6-10
                            "ItCodeInternal", "BatchNo", "Source", "Lot", "Bin", 
                            
                            //11-15
                            "Qty", "InventoryUomCode", "Qty2", "InventoryUomCode2", "Qty3", 
                            
                            //16-17
                            "InventoryUomCode3", "Remark" 
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 16, 15);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 16);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 17);
                        }, true, false, false, false
                    );
                    iGSubtotalManager.BackColor = Color.LightSalmon;
                    iGSubtotalManager.ShowSubtotalsInCells = true;
                    iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 12, 14, 16 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowInventoryUomCode()
        {
            if (mNumberOfInventoryUomCode == 2)
                Sm.GrdColInvisible(Grd1, new int[] { 14, 15 }, true);

            if (mNumberOfInventoryUomCode == 3)
                Sm.GrdColInvisible(Grd1, new int[] { 14, 15, 16, 17 }, true);
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue1(Sl.SetLueWhsCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkWhsCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Transfer to");
        }

        private void LueWhsCode2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode2, new Sm.RefreshLue1(Sl.SetLueWhsCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkWhsCode2_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Transferred from");
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        #endregion

        #endregion
    }
}
