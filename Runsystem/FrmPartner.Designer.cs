﻿namespace RunSystem
{
    partial class FrmPartner
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TxtShortName = new DevExpress.XtraEditors.TextEdit();
            this.label17 = new System.Windows.Forms.Label();
            this.LueProvCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label16 = new System.Windows.Forms.Label();
            this.TxtPostalCd = new DevExpress.XtraEditors.TextEdit();
            this.ChkActInd = new DevExpress.XtraEditors.CheckEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.LueCityCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.MeeAddress = new DevExpress.XtraEditors.MemoExEdit();
            this.label15 = new System.Windows.Forms.Label();
            this.TxtPnName = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtPnCode = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.TxtTIN = new DevExpress.XtraEditors.TextEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.TxtIdentityNo = new DevExpress.XtraEditors.TextEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtManPower = new DevExpress.XtraEditors.TextEdit();
            this.MeeRemark = new DevExpress.XtraEditors.MemoExEdit();
            this.label11 = new System.Windows.Forms.Label();
            this.TxtMobile = new DevExpress.XtraEditors.TextEdit();
            this.label7 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.TxtPhone = new DevExpress.XtraEditors.TextEdit();
            this.TxtEmail = new DevExpress.XtraEditors.TextEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.TxtFax = new DevExpress.XtraEditors.TextEdit();
            this.TxtAsset = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtTurnover = new DevExpress.XtraEditors.TextEdit();
            this.label14 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.LueGender = new DevExpress.XtraEditors.LookUpEdit();
            this.label19 = new System.Windows.Forms.Label();
            this.DteBirthDt = new DevExpress.XtraEditors.DateEdit();
            this.label20 = new System.Windows.Forms.Label();
            this.LueBCCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label21 = new System.Windows.Forms.Label();
            this.LueBSCode = new DevExpress.XtraEditors.LookUpEdit();
            this.panel4 = new System.Windows.Forms.Panel();
            this.TxtCompanyName = new DevExpress.XtraEditors.TextEdit();
            this.label22 = new System.Windows.Forms.Label();
            this.LueCompanyProvCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label23 = new System.Windows.Forms.Label();
            this.TxtCompanyPostalCd = new DevExpress.XtraEditors.TextEdit();
            this.label24 = new System.Windows.Forms.Label();
            this.LueCompanyCityCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label25 = new System.Windows.Forms.Label();
            this.MeeCompanyAddress = new DevExpress.XtraEditors.MemoExEdit();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtShortName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueProvCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPostalCd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActInd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCityCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeAddress.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPnName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPnCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTIN.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtIdentityNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtManPower.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMobile.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPhone.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmail.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFax.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAsset.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTurnover.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueGender.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteBirthDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteBirthDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBCCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBSCode.Properties)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCompanyName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCompanyProvCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCompanyPostalCd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCompanyCityCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCompanyAddress.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(891, 0);
            this.panel1.Size = new System.Drawing.Size(70, 383);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label27);
            this.panel2.Controls.Add(this.TxtCompanyName);
            this.panel2.Controls.Add(this.label22);
            this.panel2.Controls.Add(this.LueCompanyProvCode);
            this.panel2.Controls.Add(this.label23);
            this.panel2.Controls.Add(this.TxtCompanyPostalCd);
            this.panel2.Controls.Add(this.label24);
            this.panel2.Controls.Add(this.LueCompanyCityCode);
            this.panel2.Controls.Add(this.label25);
            this.panel2.Controls.Add(this.MeeCompanyAddress);
            this.panel2.Controls.Add(this.label26);
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.label19);
            this.panel2.Controls.Add(this.DteBirthDt);
            this.panel2.Controls.Add(this.label18);
            this.panel2.Controls.Add(this.LueGender);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.TxtIdentityNo);
            this.panel2.Controls.Add(this.TxtShortName);
            this.panel2.Controls.Add(this.label17);
            this.panel2.Controls.Add(this.LueProvCode);
            this.panel2.Controls.Add(this.label16);
            this.panel2.Controls.Add(this.TxtPostalCd);
            this.panel2.Controls.Add(this.ChkActInd);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.LueCityCode);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.MeeAddress);
            this.panel2.Controls.Add(this.label15);
            this.panel2.Controls.Add(this.TxtPnName);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.TxtPnCode);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Size = new System.Drawing.Size(891, 383);
            // 
            // TxtShortName
            // 
            this.TxtShortName.EnterMoveNextControl = true;
            this.TxtShortName.Location = new System.Drawing.Point(124, 52);
            this.TxtShortName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtShortName.Name = "TxtShortName";
            this.TxtShortName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtShortName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtShortName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtShortName.Properties.Appearance.Options.UseFont = true;
            this.TxtShortName.Properties.MaxLength = 40;
            this.TxtShortName.Size = new System.Drawing.Size(319, 20);
            this.TxtShortName.TabIndex = 15;
            this.TxtShortName.Validated += new System.EventHandler(this.TxtShortName_Validated);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(49, 55);
            this.label17.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(72, 14);
            this.label17.TabIndex = 14;
            this.label17.Text = "Short Name";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueProvCode
            // 
            this.LueProvCode.EnterMoveNextControl = true;
            this.LueProvCode.Location = new System.Drawing.Point(124, 115);
            this.LueProvCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueProvCode.Name = "LueProvCode";
            this.LueProvCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProvCode.Properties.Appearance.Options.UseFont = true;
            this.LueProvCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProvCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueProvCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProvCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueProvCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProvCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueProvCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProvCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueProvCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueProvCode.Properties.DropDownRows = 30;
            this.LueProvCode.Properties.NullText = "[Empty]";
            this.LueProvCode.Properties.PopupWidth = 350;
            this.LueProvCode.Size = new System.Drawing.Size(319, 20);
            this.LueProvCode.TabIndex = 21;
            this.LueProvCode.ToolTip = "F4 : Show/hide list";
            this.LueProvCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueProvCode.EditValueChanged += new System.EventHandler(this.LueProvCode_EditValueChanged);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(68, 119);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(53, 14);
            this.label16.TabIndex = 20;
            this.label16.Text = "Province";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPostalCd
            // 
            this.TxtPostalCd.EnterMoveNextControl = true;
            this.TxtPostalCd.Location = new System.Drawing.Point(124, 136);
            this.TxtPostalCd.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPostalCd.Name = "TxtPostalCd";
            this.TxtPostalCd.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPostalCd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPostalCd.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPostalCd.Properties.Appearance.Options.UseFont = true;
            this.TxtPostalCd.Properties.MaxLength = 20;
            this.TxtPostalCd.Size = new System.Drawing.Size(143, 20);
            this.TxtPostalCd.TabIndex = 23;
            this.TxtPostalCd.Validated += new System.EventHandler(this.TxtPostalCd_Validated);
            // 
            // ChkActInd
            // 
            this.ChkActInd.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkActInd.Location = new System.Drawing.Point(281, 9);
            this.ChkActInd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkActInd.Name = "ChkActInd";
            this.ChkActInd.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkActInd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkActInd.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkActInd.Properties.Appearance.Options.UseBackColor = true;
            this.ChkActInd.Properties.Appearance.Options.UseFont = true;
            this.ChkActInd.Properties.Appearance.Options.UseForeColor = true;
            this.ChkActInd.Properties.Caption = "Active";
            this.ChkActInd.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkActInd.Size = new System.Drawing.Size(76, 22);
            this.ChkActInd.TabIndex = 11;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(94, 98);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(27, 14);
            this.label4.TabIndex = 18;
            this.label4.Text = "City";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueCityCode
            // 
            this.LueCityCode.EnterMoveNextControl = true;
            this.LueCityCode.Location = new System.Drawing.Point(124, 94);
            this.LueCityCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueCityCode.Name = "LueCityCode";
            this.LueCityCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCityCode.Properties.Appearance.Options.UseFont = true;
            this.LueCityCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCityCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCityCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCityCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCityCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCityCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCityCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCityCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCityCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCityCode.Properties.DropDownRows = 30;
            this.LueCityCode.Properties.NullText = "[Empty]";
            this.LueCityCode.Properties.PopupWidth = 350;
            this.LueCityCode.Size = new System.Drawing.Size(319, 20);
            this.LueCityCode.TabIndex = 19;
            this.LueCityCode.ToolTip = "F4 : Show/hide list";
            this.LueCityCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCityCode.EditValueChanged += new System.EventHandler(this.LueCityCode_EditValueChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(50, 140);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(71, 14);
            this.label6.TabIndex = 22;
            this.label6.Text = "Postal Code";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeAddress
            // 
            this.MeeAddress.EnterMoveNextControl = true;
            this.MeeAddress.Location = new System.Drawing.Point(124, 73);
            this.MeeAddress.Margin = new System.Windows.Forms.Padding(5);
            this.MeeAddress.Name = "MeeAddress";
            this.MeeAddress.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAddress.Properties.Appearance.Options.UseFont = true;
            this.MeeAddress.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAddress.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeAddress.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAddress.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeAddress.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAddress.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeAddress.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAddress.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeAddress.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeAddress.Properties.MaxLength = 400;
            this.MeeAddress.Properties.PopupFormSize = new System.Drawing.Size(350, 20);
            this.MeeAddress.Properties.ShowIcon = false;
            this.MeeAddress.Size = new System.Drawing.Size(319, 20);
            this.MeeAddress.TabIndex = 17;
            this.MeeAddress.ToolTip = "F4 : Show/hide text";
            this.MeeAddress.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeAddress.ToolTipTitle = "Run System";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Red;
            this.label15.Location = new System.Drawing.Point(71, 76);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(50, 14);
            this.label15.TabIndex = 16;
            this.label15.Text = "Address";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPnName
            // 
            this.TxtPnName.EnterMoveNextControl = true;
            this.TxtPnName.Location = new System.Drawing.Point(124, 31);
            this.TxtPnName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPnName.Name = "TxtPnName";
            this.TxtPnName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPnName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPnName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPnName.Properties.Appearance.Options.UseFont = true;
            this.TxtPnName.Properties.MaxLength = 80;
            this.TxtPnName.Size = new System.Drawing.Size(319, 20);
            this.TxtPnName.TabIndex = 13;
            this.TxtPnName.Validated += new System.EventHandler(this.TxtPnName_Validated);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(39, 34);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 14);
            this.label2.TabIndex = 12;
            this.label2.Text = "Partner Name";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPnCode
            // 
            this.TxtPnCode.EnterMoveNextControl = true;
            this.TxtPnCode.Location = new System.Drawing.Point(124, 10);
            this.TxtPnCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPnCode.Name = "TxtPnCode";
            this.TxtPnCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPnCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPnCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPnCode.Properties.Appearance.Options.UseFont = true;
            this.TxtPnCode.Properties.MaxLength = 16;
            this.TxtPnCode.Size = new System.Drawing.Size(154, 20);
            this.TxtPnCode.TabIndex = 10;
            this.TxtPnCode.Validated += new System.EventHandler(this.TxtPnCode_Validated);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(42, 13);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 14);
            this.label1.TabIndex = 9;
            this.label1.Text = "Partner Code";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(4, 14);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(141, 14);
            this.label13.TabIndex = 42;
            this.label13.Text = "Taxpayer Identification#";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTIN
            // 
            this.TxtTIN.EnterMoveNextControl = true;
            this.TxtTIN.Location = new System.Drawing.Point(148, 9);
            this.TxtTIN.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTIN.Name = "TxtTIN";
            this.TxtTIN.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTIN.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTIN.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTIN.Properties.Appearance.Options.UseFont = true;
            this.TxtTIN.Properties.MaxLength = 40;
            this.TxtTIN.Size = new System.Drawing.Size(287, 20);
            this.TxtTIN.TabIndex = 43;
            this.TxtTIN.Validated += new System.EventHandler(this.TxtTIN_Validated);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Red;
            this.label12.Location = new System.Drawing.Point(24, 161);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(97, 14);
            this.label12.TabIndex = 24;
            this.label12.Text = "Identity Number";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtIdentityNo
            // 
            this.TxtIdentityNo.EnterMoveNextControl = true;
            this.TxtIdentityNo.Location = new System.Drawing.Point(124, 157);
            this.TxtIdentityNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtIdentityNo.Name = "TxtIdentityNo";
            this.TxtIdentityNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtIdentityNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIdentityNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtIdentityNo.Properties.Appearance.Options.UseFont = true;
            this.TxtIdentityNo.Properties.MaxLength = 40;
            this.TxtIdentityNo.Size = new System.Drawing.Size(319, 20);
            this.TxtIdentityNo.TabIndex = 25;
            this.TxtIdentityNo.Validated += new System.EventHandler(this.TxtIdentityNo_Validated);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(98, 223);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 14);
            this.label5.TabIndex = 62;
            this.label5.Text = "Remark";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtManPower
            // 
            this.TxtManPower.EnterMoveNextControl = true;
            this.TxtManPower.Location = new System.Drawing.Point(148, 114);
            this.TxtManPower.Margin = new System.Windows.Forms.Padding(5);
            this.TxtManPower.Name = "TxtManPower";
            this.TxtManPower.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtManPower.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtManPower.Properties.Appearance.Options.UseBackColor = true;
            this.TxtManPower.Properties.Appearance.Options.UseFont = true;
            this.TxtManPower.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtManPower.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtManPower.Size = new System.Drawing.Size(143, 20);
            this.TxtManPower.TabIndex = 53;
            this.TxtManPower.Validated += new System.EventHandler(this.TxtManPower_Validated);
            // 
            // MeeRemark
            // 
            this.MeeRemark.EnterMoveNextControl = true;
            this.MeeRemark.Location = new System.Drawing.Point(148, 219);
            this.MeeRemark.Margin = new System.Windows.Forms.Padding(5);
            this.MeeRemark.Name = "MeeRemark";
            this.MeeRemark.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.Appearance.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeRemark.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeRemark.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeRemark.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeRemark.Properties.MaxLength = 400;
            this.MeeRemark.Properties.PopupFormSize = new System.Drawing.Size(350, 20);
            this.MeeRemark.Properties.ShowIcon = false;
            this.MeeRemark.Size = new System.Drawing.Size(287, 20);
            this.MeeRemark.TabIndex = 63;
            this.MeeRemark.ToolTip = "F4 : Show/hide text";
            this.MeeRemark.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeRemark.ToolTipTitle = "Run System";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(81, 118);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(64, 14);
            this.label11.TabIndex = 52;
            this.label11.Text = "Manpower";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtMobile
            // 
            this.TxtMobile.EnterMoveNextControl = true;
            this.TxtMobile.Location = new System.Drawing.Point(148, 93);
            this.TxtMobile.Margin = new System.Windows.Forms.Padding(5);
            this.TxtMobile.Name = "TxtMobile";
            this.TxtMobile.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtMobile.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtMobile.Properties.Appearance.Options.UseBackColor = true;
            this.TxtMobile.Properties.Appearance.Options.UseFont = true;
            this.TxtMobile.Properties.MaxLength = 80;
            this.TxtMobile.Size = new System.Drawing.Size(287, 20);
            this.TxtMobile.TabIndex = 51;
            this.TxtMobile.Validated += new System.EventHandler(this.TxtMobile_Validated);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(103, 34);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(42, 14);
            this.label7.TabIndex = 44;
            this.label7.Text = "Phone";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(104, 97);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(41, 14);
            this.label10.TabIndex = 50;
            this.label10.Text = "Mobile";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPhone
            // 
            this.TxtPhone.EnterMoveNextControl = true;
            this.TxtPhone.Location = new System.Drawing.Point(148, 30);
            this.TxtPhone.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPhone.Name = "TxtPhone";
            this.TxtPhone.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPhone.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPhone.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPhone.Properties.Appearance.Options.UseFont = true;
            this.TxtPhone.Properties.MaxLength = 80;
            this.TxtPhone.Size = new System.Drawing.Size(287, 20);
            this.TxtPhone.TabIndex = 45;
            this.TxtPhone.Validated += new System.EventHandler(this.TxtPhone_Validated);
            // 
            // TxtEmail
            // 
            this.TxtEmail.EnterMoveNextControl = true;
            this.TxtEmail.Location = new System.Drawing.Point(148, 72);
            this.TxtEmail.Margin = new System.Windows.Forms.Padding(5);
            this.TxtEmail.Name = "TxtEmail";
            this.TxtEmail.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtEmail.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEmail.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEmail.Properties.Appearance.Options.UseFont = true;
            this.TxtEmail.Properties.MaxLength = 80;
            this.TxtEmail.Size = new System.Drawing.Size(287, 20);
            this.TxtEmail.TabIndex = 49;
            this.TxtEmail.Validated += new System.EventHandler(this.TxtEmail_Validated);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(64, 76);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(81, 14);
            this.label8.TabIndex = 48;
            this.label8.Text = "Email Address";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(120, 55);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(25, 14);
            this.label9.TabIndex = 46;
            this.label9.Text = "Fax";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtFax
            // 
            this.TxtFax.EnterMoveNextControl = true;
            this.TxtFax.Location = new System.Drawing.Point(148, 51);
            this.TxtFax.Margin = new System.Windows.Forms.Padding(5);
            this.TxtFax.Name = "TxtFax";
            this.TxtFax.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtFax.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtFax.Properties.Appearance.Options.UseBackColor = true;
            this.TxtFax.Properties.Appearance.Options.UseFont = true;
            this.TxtFax.Properties.MaxLength = 80;
            this.TxtFax.Size = new System.Drawing.Size(287, 20);
            this.TxtFax.TabIndex = 47;
            this.TxtFax.Validated += new System.EventHandler(this.TxtFax_Validated);
            // 
            // TxtAsset
            // 
            this.TxtAsset.EnterMoveNextControl = true;
            this.TxtAsset.Location = new System.Drawing.Point(148, 135);
            this.TxtAsset.Margin = new System.Windows.Forms.Padding(5);
            this.TxtAsset.Name = "TxtAsset";
            this.TxtAsset.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtAsset.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtAsset.Properties.Appearance.Options.UseBackColor = true;
            this.TxtAsset.Properties.Appearance.Options.UseFont = true;
            this.TxtAsset.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtAsset.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtAsset.Size = new System.Drawing.Size(143, 20);
            this.TxtAsset.TabIndex = 55;
            this.TxtAsset.Validated += new System.EventHandler(this.TxtAsset_Validated);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(108, 139);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 14);
            this.label3.TabIndex = 54;
            this.label3.Text = "Asset";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtTurnover
            // 
            this.TxtTurnover.EnterMoveNextControl = true;
            this.TxtTurnover.Location = new System.Drawing.Point(148, 156);
            this.TxtTurnover.Margin = new System.Windows.Forms.Padding(5);
            this.TxtTurnover.Name = "TxtTurnover";
            this.TxtTurnover.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtTurnover.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtTurnover.Properties.Appearance.Options.UseBackColor = true;
            this.TxtTurnover.Properties.Appearance.Options.UseFont = true;
            this.TxtTurnover.Properties.Appearance.Options.UseTextOptions = true;
            this.TxtTurnover.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TxtTurnover.Size = new System.Drawing.Size(143, 20);
            this.TxtTurnover.TabIndex = 57;
            this.TxtTurnover.Validated += new System.EventHandler(this.TxtTurnover_Validated);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Red;
            this.label14.Location = new System.Drawing.Point(88, 160);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(57, 14);
            this.label14.TabIndex = 56;
            this.label14.Text = "Turnover";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(74, 181);
            this.label18.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(47, 14);
            this.label18.TabIndex = 26;
            this.label18.Text = "Gender";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueGender
            // 
            this.LueGender.EnterMoveNextControl = true;
            this.LueGender.Location = new System.Drawing.Point(124, 178);
            this.LueGender.Margin = new System.Windows.Forms.Padding(5);
            this.LueGender.Name = "LueGender";
            this.LueGender.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGender.Properties.Appearance.Options.UseFont = true;
            this.LueGender.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGender.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueGender.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGender.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueGender.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGender.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueGender.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGender.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueGender.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueGender.Properties.DropDownRows = 30;
            this.LueGender.Properties.MaxLength = 1;
            this.LueGender.Properties.NullText = "[Empty]";
            this.LueGender.Properties.PopupWidth = 300;
            this.LueGender.Size = new System.Drawing.Size(143, 20);
            this.LueGender.TabIndex = 27;
            this.LueGender.ToolTip = "F4 : Show/hide list";
            this.LueGender.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueGender.EditValueChanged += new System.EventHandler(this.LueGender_EditValueChanged);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(59, 202);
            this.label19.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(62, 14);
            this.label19.TabIndex = 28;
            this.label19.Text = "Birth Date";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteBirthDt
            // 
            this.DteBirthDt.EditValue = null;
            this.DteBirthDt.EnterMoveNextControl = true;
            this.DteBirthDt.Location = new System.Drawing.Point(124, 199);
            this.DteBirthDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteBirthDt.Name = "DteBirthDt";
            this.DteBirthDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteBirthDt.Properties.Appearance.Options.UseFont = true;
            this.DteBirthDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteBirthDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteBirthDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteBirthDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteBirthDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteBirthDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteBirthDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteBirthDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteBirthDt.Properties.MaxLength = 8;
            this.DteBirthDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteBirthDt.Size = new System.Drawing.Size(143, 20);
            this.DteBirthDt.TabIndex = 29;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Red;
            this.label20.Location = new System.Drawing.Point(40, 181);
            this.label20.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(105, 14);
            this.label20.TabIndex = 58;
            this.label20.Text = "Business Category";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueBCCode
            // 
            this.LueBCCode.EnterMoveNextControl = true;
            this.LueBCCode.Location = new System.Drawing.Point(148, 177);
            this.LueBCCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueBCCode.Name = "LueBCCode";
            this.LueBCCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBCCode.Properties.Appearance.Options.UseFont = true;
            this.LueBCCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBCCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueBCCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBCCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueBCCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBCCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueBCCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBCCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueBCCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueBCCode.Properties.DropDownRows = 30;
            this.LueBCCode.Properties.NullText = "[Empty]";
            this.LueBCCode.Properties.PopupWidth = 350;
            this.LueBCCode.Size = new System.Drawing.Size(287, 20);
            this.LueBCCode.TabIndex = 59;
            this.LueBCCode.ToolTip = "F4 : Show/hide list";
            this.LueBCCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueBCCode.EditValueChanged += new System.EventHandler(this.LueBCCode_EditValueChanged);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Red;
            this.label21.Location = new System.Drawing.Point(53, 202);
            this.label21.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(92, 14);
            this.label21.TabIndex = 60;
            this.label21.Text = "Business Sector";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueBSCode
            // 
            this.LueBSCode.EnterMoveNextControl = true;
            this.LueBSCode.Location = new System.Drawing.Point(148, 198);
            this.LueBSCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueBSCode.Name = "LueBSCode";
            this.LueBSCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBSCode.Properties.Appearance.Options.UseFont = true;
            this.LueBSCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBSCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueBSCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBSCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueBSCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBSCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueBSCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBSCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueBSCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueBSCode.Properties.DropDownRows = 30;
            this.LueBSCode.Properties.NullText = "[Empty]";
            this.LueBSCode.Properties.PopupWidth = 350;
            this.LueBSCode.Size = new System.Drawing.Size(287, 20);
            this.LueBSCode.TabIndex = 61;
            this.LueBSCode.ToolTip = "F4 : Show/hide list";
            this.LueBSCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueBSCode.EditValueChanged += new System.EventHandler(this.LueBSCode_EditValueChanged);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.TxtTIN);
            this.panel4.Controls.Add(this.label21);
            this.panel4.Controls.Add(this.TxtFax);
            this.panel4.Controls.Add(this.LueBSCode);
            this.panel4.Controls.Add(this.label9);
            this.panel4.Controls.Add(this.label20);
            this.panel4.Controls.Add(this.label8);
            this.panel4.Controls.Add(this.LueBCCode);
            this.panel4.Controls.Add(this.TxtEmail);
            this.panel4.Controls.Add(this.TxtPhone);
            this.panel4.Controls.Add(this.label10);
            this.panel4.Controls.Add(this.label7);
            this.panel4.Controls.Add(this.TxtMobile);
            this.panel4.Controls.Add(this.TxtTurnover);
            this.panel4.Controls.Add(this.label11);
            this.panel4.Controls.Add(this.label14);
            this.panel4.Controls.Add(this.MeeRemark);
            this.panel4.Controls.Add(this.TxtAsset);
            this.panel4.Controls.Add(this.TxtManPower);
            this.panel4.Controls.Add(this.label3);
            this.panel4.Controls.Add(this.label5);
            this.panel4.Controls.Add(this.label13);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(448, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(443, 383);
            this.panel4.TabIndex = 52;
            // 
            // TxtCompanyName
            // 
            this.TxtCompanyName.EnterMoveNextControl = true;
            this.TxtCompanyName.Location = new System.Drawing.Point(124, 268);
            this.TxtCompanyName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCompanyName.Name = "TxtCompanyName";
            this.TxtCompanyName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCompanyName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCompanyName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCompanyName.Properties.Appearance.Options.UseFont = true;
            this.TxtCompanyName.Properties.MaxLength = 40;
            this.TxtCompanyName.Size = new System.Drawing.Size(319, 20);
            this.TxtCompanyName.TabIndex = 32;
            this.TxtCompanyName.Validated += new System.EventHandler(this.TxtCompanyName_Validated);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(27, 271);
            this.label22.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(92, 14);
            this.label22.TabIndex = 31;
            this.label22.Text = "Company Name";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueCompanyProvCode
            // 
            this.LueCompanyProvCode.EnterMoveNextControl = true;
            this.LueCompanyProvCode.Location = new System.Drawing.Point(124, 331);
            this.LueCompanyProvCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueCompanyProvCode.Name = "LueCompanyProvCode";
            this.LueCompanyProvCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCompanyProvCode.Properties.Appearance.Options.UseFont = true;
            this.LueCompanyProvCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCompanyProvCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCompanyProvCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCompanyProvCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCompanyProvCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCompanyProvCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCompanyProvCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCompanyProvCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCompanyProvCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCompanyProvCode.Properties.DropDownRows = 30;
            this.LueCompanyProvCode.Properties.NullText = "[Empty]";
            this.LueCompanyProvCode.Properties.PopupWidth = 350;
            this.LueCompanyProvCode.Size = new System.Drawing.Size(319, 20);
            this.LueCompanyProvCode.TabIndex = 38;
            this.LueCompanyProvCode.ToolTip = "F4 : Show/hide list";
            this.LueCompanyProvCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCompanyProvCode.EditValueChanged += new System.EventHandler(this.LueCompanyProvCode_EditValueChanged);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(66, 335);
            this.label23.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(53, 14);
            this.label23.TabIndex = 37;
            this.label23.Text = "Province";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtCompanyPostalCd
            // 
            this.TxtCompanyPostalCd.EnterMoveNextControl = true;
            this.TxtCompanyPostalCd.Location = new System.Drawing.Point(124, 352);
            this.TxtCompanyPostalCd.Margin = new System.Windows.Forms.Padding(5);
            this.TxtCompanyPostalCd.Name = "TxtCompanyPostalCd";
            this.TxtCompanyPostalCd.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtCompanyPostalCd.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtCompanyPostalCd.Properties.Appearance.Options.UseBackColor = true;
            this.TxtCompanyPostalCd.Properties.Appearance.Options.UseFont = true;
            this.TxtCompanyPostalCd.Properties.MaxLength = 20;
            this.TxtCompanyPostalCd.Size = new System.Drawing.Size(143, 20);
            this.TxtCompanyPostalCd.TabIndex = 40;
            this.TxtCompanyPostalCd.Validated += new System.EventHandler(this.TxtCompanyPostalCd_Validated);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Black;
            this.label24.Location = new System.Drawing.Point(92, 314);
            this.label24.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(27, 14);
            this.label24.TabIndex = 35;
            this.label24.Text = "City";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueCompanyCityCode
            // 
            this.LueCompanyCityCode.EnterMoveNextControl = true;
            this.LueCompanyCityCode.Location = new System.Drawing.Point(124, 310);
            this.LueCompanyCityCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueCompanyCityCode.Name = "LueCompanyCityCode";
            this.LueCompanyCityCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCompanyCityCode.Properties.Appearance.Options.UseFont = true;
            this.LueCompanyCityCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCompanyCityCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCompanyCityCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCompanyCityCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCompanyCityCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCompanyCityCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCompanyCityCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCompanyCityCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCompanyCityCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCompanyCityCode.Properties.DropDownRows = 30;
            this.LueCompanyCityCode.Properties.NullText = "[Empty]";
            this.LueCompanyCityCode.Properties.PopupWidth = 350;
            this.LueCompanyCityCode.Size = new System.Drawing.Size(319, 20);
            this.LueCompanyCityCode.TabIndex = 36;
            this.LueCompanyCityCode.ToolTip = "F4 : Show/hide list";
            this.LueCompanyCityCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCompanyCityCode.EditValueChanged += new System.EventHandler(this.LueCompanyCityCode_EditValueChanged);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Black;
            this.label25.Location = new System.Drawing.Point(48, 356);
            this.label25.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(71, 14);
            this.label25.TabIndex = 39;
            this.label25.Text = "Postal Code";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeCompanyAddress
            // 
            this.MeeCompanyAddress.EnterMoveNextControl = true;
            this.MeeCompanyAddress.Location = new System.Drawing.Point(124, 289);
            this.MeeCompanyAddress.Margin = new System.Windows.Forms.Padding(5);
            this.MeeCompanyAddress.Name = "MeeCompanyAddress";
            this.MeeCompanyAddress.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompanyAddress.Properties.Appearance.Options.UseFont = true;
            this.MeeCompanyAddress.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompanyAddress.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeCompanyAddress.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompanyAddress.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeCompanyAddress.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompanyAddress.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeCompanyAddress.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeCompanyAddress.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeCompanyAddress.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeCompanyAddress.Properties.MaxLength = 400;
            this.MeeCompanyAddress.Properties.PopupFormSize = new System.Drawing.Size(350, 20);
            this.MeeCompanyAddress.Properties.ShowIcon = false;
            this.MeeCompanyAddress.Size = new System.Drawing.Size(319, 20);
            this.MeeCompanyAddress.TabIndex = 34;
            this.MeeCompanyAddress.ToolTip = "F4 : Show/hide text";
            this.MeeCompanyAddress.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeCompanyAddress.ToolTipTitle = "Run System";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Black;
            this.label26.Location = new System.Drawing.Point(69, 292);
            this.label26.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(50, 14);
            this.label26.TabIndex = 33;
            this.label26.Text = "Address";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Black;
            this.label27.Location = new System.Drawing.Point(195, 244);
            this.label27.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(142, 14);
            this.label27.TabIndex = 30;
            this.label27.Text = "Company Information";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // FrmPartner
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(961, 383);
            this.Name = "FrmPartner";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtShortName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueProvCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPostalCd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActInd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCityCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeAddress.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPnName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPnCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTIN.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtIdentityNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtManPower.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMobile.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPhone.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmail.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtFax.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtAsset.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtTurnover.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueGender.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteBirthDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteBirthDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBCCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBSCode.Properties)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCompanyName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCompanyProvCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtCompanyPostalCd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCompanyCityCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeCompanyAddress.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected internal DevExpress.XtraEditors.TextEdit TxtShortName;
        private System.Windows.Forms.Label label17;
        private DevExpress.XtraEditors.LookUpEdit LueProvCode;
        private System.Windows.Forms.Label label16;
        protected internal DevExpress.XtraEditors.TextEdit TxtPostalCd;
        private DevExpress.XtraEditors.CheckEdit ChkActInd;
        private System.Windows.Forms.Label label4;
        protected internal DevExpress.XtraEditors.LookUpEdit LueCityCode;
        private System.Windows.Forms.Label label6;
        protected internal DevExpress.XtraEditors.MemoExEdit MeeAddress;
        private System.Windows.Forms.Label label15;
        protected internal DevExpress.XtraEditors.TextEdit TxtPnName;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtPnCode;
        private System.Windows.Forms.Label label1;
        internal DevExpress.XtraEditors.TextEdit TxtTurnover;
        private System.Windows.Forms.Label label14;
        internal DevExpress.XtraEditors.TextEdit TxtAsset;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label13;
        protected internal DevExpress.XtraEditors.TextEdit TxtTIN;
        private System.Windows.Forms.Label label12;
        private DevExpress.XtraEditors.TextEdit TxtIdentityNo;
        private System.Windows.Forms.Label label5;
        internal DevExpress.XtraEditors.TextEdit TxtManPower;
        private DevExpress.XtraEditors.MemoExEdit MeeRemark;
        private System.Windows.Forms.Label label11;
        protected internal DevExpress.XtraEditors.TextEdit TxtMobile;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label10;
        protected internal DevExpress.XtraEditors.TextEdit TxtPhone;
        protected internal DevExpress.XtraEditors.TextEdit TxtEmail;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        protected internal DevExpress.XtraEditors.TextEdit TxtFax;
        private System.Windows.Forms.Label label18;
        private DevExpress.XtraEditors.LookUpEdit LueGender;
        private System.Windows.Forms.Label label19;
        internal DevExpress.XtraEditors.DateEdit DteBirthDt;
        private System.Windows.Forms.Label label21;
        protected internal DevExpress.XtraEditors.LookUpEdit LueBSCode;
        private System.Windows.Forms.Label label20;
        protected internal DevExpress.XtraEditors.LookUpEdit LueBCCode;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label27;
        protected internal DevExpress.XtraEditors.TextEdit TxtCompanyName;
        private System.Windows.Forms.Label label22;
        private DevExpress.XtraEditors.LookUpEdit LueCompanyProvCode;
        private System.Windows.Forms.Label label23;
        protected internal DevExpress.XtraEditors.TextEdit TxtCompanyPostalCd;
        private System.Windows.Forms.Label label24;
        protected internal DevExpress.XtraEditors.LookUpEdit LueCompanyCityCode;
        private System.Windows.Forms.Label label25;
        protected internal DevExpress.XtraEditors.MemoExEdit MeeCompanyAddress;
        private System.Windows.Forms.Label label26;
    }
}