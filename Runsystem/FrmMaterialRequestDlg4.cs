﻿#region Update
/*
    26/06/2019 [TKG] New application
    04/07/2019 [TKG] Untuk MR berdasarkan project impelemtation, menggunakan validasi dropping request amount.
    24/07/2019 [TKG] Dropping Request BL (Project) dan BTL (department) semua nya Non Budget
    02/09/2019 [WED] ubah alur ke SO Contract Revision
    31/10/2019 [DITA/IMS] tambah informasi ItCodeInternal dan Specifications
    13/07/2020 [WED/YK] parameter ItGrpCodeNotShowOnMaterialRequest untuk membatasi Group yg muncul
    20/09/2022 [RDA/VIR] penarikan dropping request menjadi partial berdasarkan param IsDroppingRequestUseMRReceivingPartial (MR)
    06/01/2023 [RDA/MNET] penambahan kolom dan penyesuaian data yg muncul berdasarkan param IsDroppingRequestUseType
    06/01/2023 [RDA/MNET] get parameter IsDroppingRequestUseType
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmMaterialRequestDlg4 : RunSystem.FrmBase4
    {
        #region Field

        private FrmMaterialRequest mFrmParent;
        private string mDocDt = string.Empty, mDeptCode = string.Empty, mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmMaterialRequestDlg4(FrmMaterialRequest FrmParent, string DocDt, string DeptCode)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mDocDt = DocDt;
            mDeptCode = DeptCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -360);
                Sl.SetLueCtCode(ref LueCtCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 23;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#",
                        "",
                        "Date",
                        "Department",
                        "Year",

                        //6-10
                        "Month",
                        "Project"+Environment.NewLine+"Implementation#",
                        "",
                        "Budget's"+Environment.NewLine+"Category",
                        "Dropping's Request"+Environment.NewLine+"Amount",
                        
                        //11-15
                        "Other's"+Environment.NewLine+"MR Amount",
                        "Remark",
                        "BC Code",
                        "Project",
                        "Customer",

                        //16-20
                        "Type",
                        "Item's Code",
                        "Item's Name",
                        "Quantity",
                        "Item's Code"+Environment.NewLine+"Internal",

                        //21-22
                        "Specification",
                        "Dropping Request Type"
                    },
                     new int[] 
                    {
                        //0
                        50,
 
                        //1-5
                        150, 20, 80, 200, 80,
                        
                        //6-10
                        80, 150, 20, 200, 130,
                        
                        //11-15
                        0, 400, 0, 300, 250,

                        //16-20
                        130, 120, 250, 100, 100,

                        //21-22
                        300, 200
                    }
                );
            Sm.GrdColButton(Grd1, new int[] { 2, 8 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 3, 4, 5, 6, 7, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22 });
            Sm.GrdFormatDate(Grd1, new int[] { 3 });
            Sm.GrdFormatDec(Grd1, new int[] { 10, 11, 19 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 2, 4, 5, 6, 8, 13, 20 });
            if (!mFrmParent.mIsBOMShowSpecifications) Sm.GrdColInvisible(Grd1, new int[] { 21 });
            if (!mFrmParent.mIsDroppingRequestUseType) Sm.GrdColInvisible(Grd1, new int[] { 22 });
            Grd1.Cols[20].Move(18);;
            Grd1.Cols[12].Move(21);
            Grd1.Cols[22].Move(4);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 4, 5, 6, 8, 20 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct DocNo, DocDt, DeptName, Yr, Mth, PRJIDocNo, BCName, Amt, OtherAmt, Remark, BCCode, CtName, ProjectName, ");
            SQL.AppendLine("ProjectType, ItCode, ItName, Qty, ItCodeInternal, Specification ");
            SQL.AppendLine(", DocType, OptDesc as DroppingRequestType ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select '1' As DRType, X1.DocNo, X1.DocDt, X1.DeptName, X1.Yr, X1.Mth, X1.PRJIDocNo, X1.BCName, ");
            if (mFrmParent.mIsDroppingRequestUseMRReceivingPartial)
            {
                SQL.AppendLine("    X1.Amt - IFNULL(X7.TotalMRAmt,0) as Amt, ");
            }
            else
                SQL.AppendLine("    X1.Amt,   ");
            SQL.AppendLine("    X1.OtherAmt, X1.Remark, X1.BCCode, X1.CtCode, X2.CtName, X1.ProjectName, ");
            SQL.AppendLine("    X3.OptDesc As ProjectType, X6.ItCode, X6.ItName, X4.Qty, X6.ItCodeInternal, X6.Specification ");
            SQL.AppendLine("    , X1.DocType, X1.OptDesc "); //
            SQL.AppendLine("    From ");
            SQL.AppendLine("    ( ");
            SQL.AppendLine("        Select A.DocNo, A.DocDt, Null As DeptName, A.Yr, A.Mth, A.PRJIDocNo, Null As BCName, A.Amt, ");
            SQL.AppendLine("        0.00 As OtherAmt,  ");
            SQL.AppendLine("        A.Remark, Null As BCCode, E.CtCode, E.ProjectType, E.ProjectName  ");
            SQL.AppendLine("        , A.Doctype, F.OptDesc "); //
            SQL.AppendLine("        From TblDroppingRequestHdr A  ");
            SQL.AppendLine("        Inner Join TblProjectImplementationHdr B On A.PRJIDocNo = B.DocNo ");
            SQL.AppendLine("            And (A.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("            And A.CancelInd = 'N' ");
            SQL.AppendLine("            And A.Status = 'A' ");
            SQL.AppendLine("        Inner join TblSOCOntractRevisionHdr B1 On B.SOContractDocno = B1.Docno ");
            SQL.AppendLine("        Inner Join TblSOContractHdr C On B1.SOCDocNo = C.DocNo ");
            SQL.AppendLine("        Inner Join TblBOQHdr D on C.BOQDocNo = D.DocNo ");
            SQL.AppendLine("        Inner Join TblLOPHdr E On D.LOPDocNo = E.DocNo ");
            SQL.AppendLine("        LEFT JOIN tbloption F ON A.DocType=F.OptCode AND optcat='DroppingRequestDocType' "); //
            SQL.AppendLine("        Where A.DocNo Not In (  ");
            SQL.AppendLine("            Select Distinct T1.DocNo  ");
            SQL.AppendLine("            From TblDroppingRequestHdr T1, TblDroppingRequestDtl T2  ");
            SQL.AppendLine("            Where T1.CancelInd='N'  ");
            SQL.AppendLine("            And T1.Status='A'  ");
            SQL.AppendLine("            And T1.DocDt Between @DocDt1 And @DocDt2  ");
            SQL.AppendLine("            And T1.PRJIDocNo Is Not Null  ");
            SQL.AppendLine("            And T1.DocNo=T2.DocNo  ");
            SQL.AppendLine("            And T2.MRDocNo Is Not Null  ");
            SQL.AppendLine("            )  ");
            SQL.AppendLine("    ) X1 ");
            SQL.AppendLine("    Inner Join TblCustomer X2 On X1.CtCode = X2.CtCode ");
            SQL.AppendLine("    Inner Join TblOption X3 On X1.ProjectType = X3.OptCode And X3.OptCat = 'ProjectType' ");
            SQL.AppendLine("    Inner Join TblDroppingRequestDtl X4 On X1.DocNo = X4.DocNo ");
            SQL.AppendLine("    Inner Join TblProjectImplementationRBPHdr X5 On X4.PRBPDocNo = X5.DocNo ");
            SQL.AppendLine("    Inner Join TblItem X6 On X5.ResourceItCode = X6.ItCode ");

            if (mFrmParent.mIsDroppingRequestUseMRReceivingPartial)
            {
                SQL.AppendLine("LEFT JOIN( ");
                SQL.AppendLine("	 SELECT D.DocNo AS DroppingReqDocNo, E.ResourceItCode , G.ItName ");
                SQL.AppendLine("    , SUM(B.UPrice*B.Qty) TotalMRAmt   ");
                SQL.AppendLine("    FROM tblmaterialrequesthdr A   ");
                SQL.AppendLine("    INNER JOIN tblmaterialrequestdtl B ON A.DocNo = B.DocNo   ");
                SQL.AppendLine("    INNER JOIN tbldroppingrequesthdr C ON A.DroppingRequestDocNo = C.DocNo   ");
                SQL.AppendLine("    INNER JOIN tbldroppingrequestdtl D ON C.DocNo = D.DocNo  ");
                SQL.AppendLine("  	 inner Join TblProjectImplementationRBPHdr E On D.PRBPDocNo = E.DocNo  ");
                SQL.AppendLine("	 Inner Join TblProjectImplementationRBPDtl F On E.DocNo = F.DocNo And D.PRBPDNo = F.DNo   ");
                SQL.AppendLine("    LEFT JOIN tblitem G ON B.ItCode = E.ResourceItCode   ");
                SQL.AppendLine("    WHERE A.Status = 'A' AND A.CancelInd = 'N'   ");
                SQL.AppendLine("    AND B.CancelInd = 'N' AND B.Status = 'A'   ");
                SQL.AppendLine("    AND B.ItCode = E.ResourceItCode    ");
                SQL.AppendLine("    GROUP BY D.DocNo, E.ResourceItCode   ");
                SQL.AppendLine(")X7 ON X1.DocNo = X7.DroppingReqDocNo AND X5.ResourceItCode = X7.ResourceItCode ");
                SQL.AppendLine("WHERE X4.DRSourceInd IS NULL OR X4.DRSourceInd = '1'  ");
            }

            if (mFrmParent.mItGrpCodeNotShowOnMaterialRequest.Length > 0)
            {
                SQL.AppendLine("    And Not Find_In_Set(X6.ItGrpCode, @ItGrpCodeNotShowOnMaterialRequest) ");
            }

            SQL.AppendLine("    Union All ");

            SQL.AppendLine("    Select '2' As DRType, X1.DocNo, X2.DocDt, X3.DeptName, X2.Yr, X2.Mth,  ");
            SQL.AppendLine("    Null As PRJIDocNo, X4.BCName,  ");
            if (mFrmParent.mIsDroppingRequestUseMRReceivingPartial)
            {
                SQL.AppendLine("   X1.Amt - IFNULL(X7.TotalMRAmt,0) as Amt, ");
            }
            else
                SQL.AppendLine("    X1.Amt,   ");
            SQL.AppendLine("    0.00 As OtherAmt, X2.Remark, X1.BCCode, Null As CtCode, Null As CtName, Null As ProjectName, ");
            SQL.AppendLine("    Null As ProjectType, X5.ItCode, X6.ItName, X5.Qty, X6.ItCodeInternal, X6.Specification  ");
            SQL.AppendLine("    , X1.DocType, X1.OptDesc "); //
            SQL.AppendLine("    From (  ");
            SQL.AppendLine("        Select B.DocNo, B.BCCode, Sum(B.Amt*B.Qty) As Amt  ");
            SQL.AppendLine("        , A.Doctype, C.OptDesc "); //
            SQL.AppendLine("        From TblDroppingRequestHdr A  ");
            SQL.AppendLine("        Inner Join TblDroppingRequestDtl2 B On A.DocNo=B.DocNo And B.MRDocNo Is Null  ");
            SQL.AppendLine("        LEFT JOIN tbloption C ON A.DocType = C.OptCode AND optcat = 'DroppingRequestDocType' "); //
            SQL.AppendLine("        Where A.CancelInd='N' ");
            SQL.AppendLine("        And A.Status='A'  ");
            SQL.AppendLine("        And A.DocDt Between @DocDt1 And @DocDt2  ");
            SQL.AppendLine("        And A.DeptCode=@DeptCode  ");
            SQL.AppendLine("        And Concat(B.DocNo, B.BCCode) Not In (  ");
            SQL.AppendLine("            Select Concat(T2.DocNo, T2.BCCode) As TheKey  ");
            SQL.AppendLine("            From TblDroppingRequestHdr T1, TblDroppingRequestDtl2 T2  ");
            SQL.AppendLine("            Where T1.CancelInd='N'  ");
            SQL.AppendLine("            And T1.Status='A'  ");
            SQL.AppendLine("            And T1.DocDt Between @DocDt1 And @DocDt2  ");
            SQL.AppendLine("            And T1.DeptCode=@DeptCode  ");
            SQL.AppendLine("            And T1.PRJIDocNo Is Null  ");
            SQL.AppendLine("            And T1.DocNo=T2.DocNo  ");
            SQL.AppendLine("            And T2.MRDocNo Is Not Null  ");
            SQL.AppendLine("            )  ");
            SQL.AppendLine("        Group By B.DocNo, B.BCCode  ");
            SQL.AppendLine("    ) X1  ");
            SQL.AppendLine("    Inner Join TblDroppingRequestHdr X2 On X1.DocNo=X2.DocNo  ");
            SQL.AppendLine("    Inner Join TblDepartment X3 On X2.DeptCode=X3.DeptCode  ");
            SQL.AppendLine("    Inner Join TblBudgetCategory X4 On X1.BCCode=X4.BCCode ");
            SQL.AppendLine("    Inner Join TblDroppingRequestDtl2 X5 On X2.DocNo = X5.DocNo And X1.BCCode = X5.BCCode ");
            SQL.AppendLine("    Inner Join TblItem X6 On X5.ItCode = X6.ItCode ");

            SQL.AppendLine("         ");

            if (mFrmParent.mIsDroppingRequestUseMRReceivingPartial)
            {
                SQL.AppendLine("LEFT JOIN( ");
                SQL.AppendLine("    SELECT D.DocNo AS DroppingReqDocNo, D.BCCode ");
                SQL.AppendLine("    , SUM(B.UPrice*B.Qty) TotalMRAmt  ");
                SQL.AppendLine("    FROM tblmaterialrequesthdr A  ");
                SQL.AppendLine("    INNER JOIN tblmaterialrequestdtl B ON A.DocNo = B.DocNo  ");
                SQL.AppendLine("    INNER JOIN tbldroppingrequesthdr C ON A.DroppingRequestDocNo = C.DocNo  ");
                SQL.AppendLine("    INNER JOIN tbldroppingrequestdtl2 D ON C.DocNo = D.DocNo AND A.DroppingRequestBCCode = D.BCCode  ");
                SQL.AppendLine("    LEFT JOIN tblitem F ON D.ItCode = F.ItCode  ");
                SQL.AppendLine("    WHERE A.Status = 'A' AND A.CancelInd = 'N'  ");
                SQL.AppendLine("    AND B.CancelInd = 'N' AND B.Status = 'A'  ");
                SQL.AppendLine("    AND B.ItCode = D.ItCode  ");
                SQL.AppendLine("    GROUP BY D.DocNo, D.ItCode ");
                SQL.AppendLine(")X7 ON X1.DocNo = X7.DroppingReqDocNo AND X1.BCCode = X7.BCCode ");
                SQL.AppendLine("WHERE X5.DRSourceInd IS NULL OR X5.DRSourceInd = '1'  ");
            }

            if (mFrmParent.mItGrpCodeNotShowOnMaterialRequest.Length > 0)
            {
                SQL.AppendLine("    And Not Find_In_Set(X6.ItGrpCode, @ItGrpCodeNotShowOnMaterialRequest) ");
            }
            SQL.AppendLine(") T ");

            //SQL.AppendLine("Select DocNo, DocDt, DeptName, Yr, Mth, PRJIDocNo, BCName, Amt, OtherAmt, Remark, BCCode ");
            //SQL.AppendLine("From ( ");
            
            //    SQL.AppendLine("    Select '1' As DRType, A.DocNo, A.DocDt, Null As DeptName, A.Yr, A.Mth, A.PRJIDocNo, Null As BCName, A.Amt, ");
            //    SQL.AppendLine("    0.00 As OtherAmt, ");
            //    SQL.AppendLine("    A.Remark, Null As BCCode ");
            //    SQL.AppendLine("    From TblDroppingRequestHdr A ");
            //    SQL.AppendLine("    Where A.CancelInd='N' ");
            //    SQL.AppendLine("    And A.Status='A' ");
            //    SQL.AppendLine("    And A.DocDt Between @DocDt1 And @DocDt2 ");
            //    SQL.AppendLine("    And A.PRJIDocNo Is Not Null ");
            //    SQL.AppendLine("    And A.DocNo Not In ( ");
            //    SQL.AppendLine("        Select Distinct T1.DocNo ");
            //    SQL.AppendLine("        From TblDroppingRequestHdr T1, TblDroppingRequestDtl T2 ");
            //    SQL.AppendLine("        Where T1.CancelInd='N' ");
            //    SQL.AppendLine("        And T1.Status='A' ");
            //    SQL.AppendLine("        And T1.DocDt Between @DocDt1 And @DocDt2 ");
            //    SQL.AppendLine("        And T1.PRJIDocNo Is Not Null ");
            //    SQL.AppendLine("        And T1.DocNo=T2.DocNo ");
            //    SQL.AppendLine("        And T2.MRDocNo Is Not Null ");
            //    SQL.AppendLine("        ) ");

            //    SQL.AppendLine("    Union All ");

            //    SQL.AppendLine("    Select '2' As DRType, Tbl1.DocNo, Tbl2.DocDt, Tbl3.DeptName, Tbl2.Yr, Tbl2.Mth, ");
            //    SQL.AppendLine("    Null As PRJIDocNo, Tbl4.BCName, ");
            //    SQL.AppendLine("    Tbl1.Amt, 0.00 As OtherAmt, ");
            //    SQL.AppendLine("    Tbl2.Remark, Tbl1.BCCode ");
            //    SQL.AppendLine("    From ( ");
            //    SQL.AppendLine("        Select B.DocNo, B.BCCode, Sum(B.Amt) As Amt ");
            //    SQL.AppendLine("        From TblDroppingRequestHdr A ");
            //    SQL.AppendLine("        Inner Join TblDroppingRequestDtl2 B On A.DocNo=B.DocNo And B.MRDocNo Is Null ");
            //    SQL.AppendLine("        Where A.CancelInd='N' ");
            //    SQL.AppendLine("        And A.Status='A' ");
            //    SQL.AppendLine("        And A.DocDt Between @DocDt1 And @DocDt2 ");
            //    SQL.AppendLine("        And A.DeptCode=@DeptCode ");
            //    SQL.AppendLine("        And Concat(B.DocNo, B.BCCode) Not In ( ");
            //    SQL.AppendLine("            Select Concat(T2.DocNo, T2.BCCode) As TheKey ");
            //    SQL.AppendLine("            From TblDroppingRequestHdr T1, TblDroppingRequestDtl2 T2 ");
            //    SQL.AppendLine("            Where T1.CancelInd='N' ");
            //    SQL.AppendLine("            And T1.Status='A' ");
            //    SQL.AppendLine("            And T1.DocDt Between @DocDt1 And @DocDt2 ");
            //    SQL.AppendLine("            And T1.DeptCode=@DeptCode ");
            //    SQL.AppendLine("            And T1.PRJIDocNo Is Null ");
            //    SQL.AppendLine("            And T1.DocNo=T2.DocNo ");
            //    SQL.AppendLine("            And T2.MRDocNo Is Not Null ");
            //    SQL.AppendLine("            ) ");
            //    SQL.AppendLine("        Group By B.DocNo, B.BCCode ");
            //    SQL.AppendLine("    ) Tbl1 ");
            //    SQL.AppendLine("    Inner Join TblDroppingRequestHdr Tbl2 On Tbl1.DocNo=Tbl2.DocNo ");
            //    SQL.AppendLine("    Inner Join TblDepartment Tbl3 On Tbl2.DeptCode=Tbl3.DeptCode ");
            //    SQL.AppendLine("    Inner Join TblBudgetCategory Tbl4 On Tbl1.BCCode=Tbl4.BCCode ");
            
            //SQL.AppendLine(") T ");

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string 
                    Filter = string.Empty,
                    Yr = Sm.Left(mDocDt, 4),
                    Mth = mDocDt.Substring(4, 2);
                var cm = new MySqlCommand();
                
                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.CmParam<string>(ref cm, "@DeptCode", mDeptCode);
                Sm.CmParam<String>(ref cm, "@ItGrpCodeNotShowOnMaterialRequest", mFrmParent.mItGrpCodeNotShowOnMaterialRequest);
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtProjectName.Text, "ProjectName", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueCtCode), "CtCode", true);
                if (mFrmParent.mIsDroppingRequestUseMRReceivingPartial) Filter += Filter.Length > 0 ? " And T.Amt > 0 " : " Where T.Amt > 0 ";
                if (mFrmParent.mIsDroppingRequestUseType) Filter += Filter.Length > 0 ? " And T.DocType='1' " : " Where T.DocType='1' ";

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By T.DRType, T.DocDt, T.DocNo, T.PRJIDocNo, T.BCName;",
                        new string[] 
                        { 
                             //0
                             "DocNo", 
                             
                             //1-5
                             "DocDt", 
                             "DeptName", 
                             "Yr", 
                             "Mth", 
                             "PRJIDocNo",
 
                             //6-10
                             "BCName", 
                             "Amt", 
                             "OtherAmt", 
                             "Remark",
                             "BCCode",

                             //11-15
                             "ProjectName", 
                             "CtName", 
                             "ProjectType", 
                             "ItCode", 
                             "ItName", 

                             //16-19
                             "Qty",
                             "ItCodeInternal",
                             "Specification",
                             "DroppingRequestType"
                         },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 3, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 15);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 19, 16);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 17);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 21, 18);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 19);
                        }, true, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                int Row = Grd1.CurRow.Index;
                var DocDt = string.Concat(Sm.GetGrdStr(Grd1, Row, 5), Sm.GetGrdStr(Grd1, Row, 6), "01");
                mFrmParent.TxtDroppingRequestDocNo.EditValue = Sm.GetGrdStr(Grd1, Row, 1);
                mFrmParent.TxtDR_DeptCode.EditValue = Sm.GetGrdStr(Grd1, Row, 4);
                Sm.SetDte(mFrmParent.DteDocDt, DocDt);
                mFrmParent.TxtDR_Yr.EditValue = Sm.GetGrdStr(Grd1, Row, 5);
                mFrmParent.TxtDR_Mth.EditValue = Sm.GetGrdStr(Grd1, Row, 6);
                mFrmParent.TxtDR_PRJIDocNo.EditValue = Sm.GetGrdStr(Grd1, Row, 7);
                mFrmParent.TxtDR_BCCode.EditValue = Sm.GetGrdStr(Grd1, Row, 9);
                mFrmParent.TxtDR_DroppingRequestAmt.EditValue = Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 10), 0);
                mFrmParent.TxtDR_MRAmt.EditValue = Sm.FormatNum(0m, 0);
                mFrmParent.TxtDR_Balance.EditValue = Sm.FormatNum(Sm.GetGrdDec(Grd1, Row, 10), 0);
                mFrmParent.MeeDR_Remark.EditValue = Sm.GetGrdStr(Grd1, Row, 12);
                mFrmParent.mDroppingRequestBCCode = Sm.GetGrdStr(Grd1, Row, 13);
                mFrmParent.ClearGrd();
                this.Close();
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmDroppingRequest(mFrmParent.mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }

            if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmProjectImplementation(mFrmParent.mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 7);
                f.ShowDialog();
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 2 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                var f = new FrmDroppingRequest(mFrmParent.mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                f.ShowDialog();
            }

            if (e.ColIndex == 8 && Sm.GetGrdStr(Grd1, e.RowIndex, 7).Length != 0)
            {
                var f = new FrmProjectImplementation(mFrmParent.mMenuCode);
                f.Tag = "***";
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 7);
                f.ShowDialog();
            }
        }

        override protected void GrdCellDoubleClick(object sender, iGCellDoubleClickEventArgs e)
        {
            if (e.RowIndex >= 0) ChooseData();
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void TxtProjectName_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkProjectName_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Project");
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue1(Sl.SetLueCtCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkCtCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Customer");
        }

        #endregion

        #endregion        
    }
}
