﻿#region Update
/*
    08/08/2017 [TKG] tambah coa description
    29/01/2018 [WED] query TblAsset.AssetDt diubah menjadi TblDepreciationAssetHdr.DepreciationDt
    31/01/2018 [WED] tambah Display Name di filter pencarian Asset
    19/04/2018 [HAR] Asset Yng muncul yang ada depresiasi di tahun dan bulan yang dipilih 
    15/01/2019 [TKG] reporting asset value diambil dari depreciation asset
    21/08/2019 [TKG] tambah display name
    02/06/2020 [HAR/TWC] tambah infromasi journal untuk perbandingan nilai depresiasi denga GL
    08/11/2020 [TKG/PHT] ubah query (bug) dan tambah filter dan info depreciation date
    12/04/2021 [TKG/PHT] tambah validasi filter multi profit center
    06/10/2021 [BRI/ALL] penyesuaian nilai balance
    29/10/2021 [DITA/ALL] Residu Value ambil dari balance di asset depreciation h-1 bulan economic life nya
    01/11/2021 [DITA/ALL] Sumber pengurang residu value yaitu assetvalue seharusnya ambil dari transaksi depreciation asset bukan asset
    03/11/2021 [ICA/IMS] Total Depreciation Value ambil dari Total Depreciation tanpa dikalikan dengan tahun (sumber sebelumnya DepYear -> DepValue)
    09/11/2021 [YOG/PHT] ShortCode di Asset Depreciation reporting dimunculkan
    16/11/2021 [ICA/PHT] Residual Value sumbernya di ubah ke balance
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using DevExpress.XtraEditors;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRptDepreciationAsset : RunSystem.FrmBase6
    {
        #region Field

        private string
            mMenuCode = string.Empty,
            mAccessInd = string.Empty,
            mSQL = string.Empty, 
            mDocTitle = string.Empty;
        private List<String> mlProfitCenter = null;
        private bool 
            mIsAutoJournalActived = false,
            mIsRptDepreciationAssetUseProfitCenter = false,
            mIsAllProfitCenterSelected = false;
        private string
            mRptAssetCalculateBalanceAccumulationFormula = "1";
        #endregion

        #region Constructor

        public FrmRptDepreciationAsset(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetGrd();
                SetSQL();

                string CurrentDateTime = Sm.ServerCurrentDateTime();

                Sl.SetLueMth(LueMth);
                Sm.SetLue(LueMth, CurrentDateTime.Substring(4, 2));
                Sl.SetLueYr(LueYr, "1950");
                Sm.SetLue(LueYr, CurrentDateTime.Substring(0, 4));

                if (mIsRptDepreciationAssetUseProfitCenter)
                {
                    mlProfitCenter = new List<String>();
                    SetCcbProfitCenterCode(ref CcbProfitCenterCode);
                }
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        private void GetParameter()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string ParCode = string.Empty, ParValue = string.Empty;

            SQL.AppendLine("Select ParCode, ParValue From TblParameter Where ParValue Is Not Null And ParCode In (");
            SQL.AppendLine("'IsAutoJournalActived', 'IsRptDepreciationAssetUseProfitCenter', 'RptAssetCalculateBalanceAccumulationFormula', 'DocTitle' ");
            SQL.AppendLine(");");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ParCode", "ParValue" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        ParCode = Sm.DrStr(dr, c[0]);
                        ParValue = Sm.DrStr(dr, c[1]).Trim();
                        switch (ParCode)
                        {
                            //boolean
                            case "IsAutoJournalActived": mIsAutoJournalActived = ParValue == "Y"; break;
                            case "IsRptDepreciationAssetUseProfitCenter": mIsRptDepreciationAssetUseProfitCenter = ParValue == "Y"; break;

                            //string
                            case "RptAssetCalculateBalanceAccumulationFormula":
                                mRptAssetCalculateBalanceAccumulationFormula = ParValue;
                                if (mRptAssetCalculateBalanceAccumulationFormula.Length == 0)
                                    mRptAssetCalculateBalanceAccumulationFormula = "1";
                                break;
                            case "DocTitle": mDocTitle = ParValue;
                                break;
                        }
                    }
                }
                dr.Close();
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            #region Old

            //SQL.AppendLine("Select distinct * From ( ");
            //SQL.AppendLine("    Select A.AssetCode, A.AssetName, A.DisplayName, IfNull(Left(B.DepreciationDt, 4), '') As ThnPerolehan, E.CCName As location, H.AssetValue, ");
            //SQL.AppendLine("    if(A.DepreciationCode = '1', 'Garis Lurus', 'Saldo Menurun') As DepCode, ");
            //SQL.AppendLine("    A.EcoLife, F.ResiduValue, A.PercentageAnnualDepreciation,  B.DepValue, B.Age, B.DepYear, B.Balanced, G.AcDesc,  B.JournalDocNo ");
            //SQL.AppendLine("    From TblAsset A ");
            //SQL.AppendLine("    Inner Join ( ");
            //SQL.AppendLine("        Select B.DocNo, B.DepreciationDt, A.AssetCode, SUm(DepreciationValue) As DepValue, ((@Yr+1) - IfNull(Left(B.DepreciationDt, 4), 0000)) As Age, (SUm(DepreciationValue)*((@Yr+1)-IfNull(Left(B.DepreciationDt, 4), 0))) As DepYear, ");
            //SQL.AppendLine("        if(A.depreciationCode='1',(A.AssetValue -  (Sum(DepreciationValue)*((@Yr+1)-IfNull(Left(B.DepreciationDt, 4), 0)))), ");
            //SQL.AppendLine("        (A.AssetValue -  E.Depvalue))  As Balanced, C.JournalDocNo ");
            //SQL.AppendLine("        From TblAsset A ");
            //SQL.AppendLine("        Left Join TblDepreciationAssetHdr B On A.AssetCode = B.AssetCode ");
            //SQL.AppendLine("        Left Join TblDepreciationAssetDtl C On B.DocNo = C.DocNo ");
            //SQL.AppendLine("        Left Join TblOption D On A.DepreciationCode = D.OptCode And OptCat = 'DepreciationMethod' ");
            //SQL.AppendLine("        Left Join ( ");
            //SQL.AppendLine("            Select A.Docno, A.AssetCode, SUm(DepreciationValue) As DepValue  from tblDepreciationAssethdr A ");
            //SQL.AppendLine("            Inner Join TblDepreciationAssetDtl B On A.DocNo = B.DocNo ");
            //SQL.AppendLine("            Where concat(B.yr, B.mth) <= Concat(@yr,@mth) And A.CancelInd = 'N' ");
            //SQL.AppendLine("            Group By A.DocNo, A.AssetCode ");
            //SQL.AppendLine("            )E On A.AssetCode = E.AssetCode And B.DocNo = E.DocNo ");
            //SQL.AppendLine("        Where C.Yr = @Yr And C.Mth=@mth And B.CancelInd = 'N' And C.JournalDocNo Is Not NUll ");
            //SQL.AppendLine("        Group by B.DocNo, A.AssetCode ");
            //SQL.AppendLine("    )B On A.AssetCode = B.AssetCode ");
            //SQL.AppendLine("    Left Join TblOption D On A.DepreciationCode = D.OptCode And OptCat = 'DepreciationMethod' ");
            //SQL.AppendLine("    Left Join TblCostCenter E On A.CcCode = E.CCCode ");
            //SQL.AppendLine("    Left Join  ");
            //SQL.AppendLine("    ( ");
            //SQL.AppendLine("        Select A.AssetCode, B.DocNo, B.Dno, B.DepreciationValue As ResiduValue ");
            //SQL.AppendLine("        from TblDepreciationAssetHdr A ");
            //SQL.AppendLine("        Inner Join 	TbldepreciationAssetDtl B On A.DocNo = B.DocNo ");
            //SQL.AppendLine("        Inner Join ( ");
            //SQL.AppendLine("               SELECT MAX(A.Dno) AS dno, A.DocNo  ");
            //SQL.AppendLine("               FROM tbldepreciationAssetDtl  A ");
            //SQL.AppendLine("               Inner JOin TblDepreciationAssetHdr B On A.DocNo = B.DocNo ");
            //SQL.AppendLine("               Where B.cancelInd= 'N' And A.Yr = @Yr And A.Mth=@mth And B.CancelInd = 'N'  And A.JournalDocNo is not null  ");
            //SQL.AppendLine("               GROUP BY A.Docno  ");
            //SQL.AppendLine("               )C On B.Dno = C.Dno and B.DocNo = C.DocNo ");
            //SQL.AppendLine("        Where B.Yr = @Yr And B.Mth=@mth And A.CancelInd = 'N'  And B.JournalDocNo is not null ");
            //SQL.AppendLine("    )F On A.AssetCode=F.AssetCode And B.DocNo = F.DocNo ");
            //SQL.AppendLine("    Left Join TblCOA G On A.AcNo2=G.AcNo ");
            ////SQL.AppendLine("    Left Join TblDepreciationAssetHdr H On A.AssetCode=H.AssetCode And H.CancelInd='N' ");
            //SQL.AppendLine("    Inner Join  ");
            //SQL.AppendLine("    ( ");
            //SQL.AppendLine("        Select Distinct  H.AssetCode,  H.AssetValue  ");
            //SQL.AppendLine("        From TblDepreciationAssetHdr H  ");
            //SQL.AppendLine("        Inner Join TblDepreciationAssetDtl I On H.DocNo=I.DocNo  ");
            //SQL.AppendLine("        Where H.cancelInd= 'N' And I.Yr = @Yr And I.Mth=@mth And I.JournalDocNo is not null ");
            //SQL.AppendLine("    )H On A.AssetCode=H.AssetCode ");
            //SQL.AppendLine("    Where B.JournalDocNo Is Not NUll ");
            //SQL.AppendLine(") Tbl ");

             //SQL.AppendLine("SET @Yr = '"+Sm.GetLue(LueYr)+"' ;");
            //SQL.AppendLine("SET @Mth = '" + Sm.GetLue(LueYr) + "' ;");

            #endregion

            SQL.AppendLine("Select * From ( ");
            SQL.AppendLine("    Select  ");
            SQL.AppendLine("    A.AssetCode, A.AssetName, A.DisplayName, A.ShortCode, ");
            SQL.AppendLine("    IfNull(Left(B.DepreciationDt, 4), '') As ThnPerolehan, ");
            SQL.AppendLine("    E.CCName As Location, H.AssetValue, ");
            SQL.AppendLine("    if(A.DepreciationCode = '1', 'Garis Lurus', 'Saldo Menurun') As DepCode, ");
            SQL.AppendLine("    A.EcoLife, F.ResiduValue, A.PercentageAnnualDepreciation, ");
            SQL.AppendLine("    B.DepValue, B.Age, B.DepYear, B.Balanced, G.AcDesc, ");
            SQL.AppendLine("    B.JournalDocNo, B.DepreciationDt, B.ResiduValue2 ");
            SQL.AppendLine("    From TblAsset A ");
            SQL.AppendLine("    Inner Join (  ");
            SQL.AppendLine("        Select T2.DocNo, T2.AssetCode, T2.DepreciationDt, ");
            SQL.AppendLine("        T1.AssetValue, T1.DepreciationCode, ");
            SQL.AppendLine("        Group_Concat(Distinct IfNull(T3.JournalDocNo, '') Separator ', ') As JournalDocNo, ");
            SQL.AppendLine("        Sum(T3.DepreciationValue) As DepValue, ");
            SQL.AppendLine("        ((@Yr+1) - IfNull(Left(T2.DepreciationDt, 4), 0000)) As Age, ");
            SQL.AppendLine("        (Sum(T3.DepreciationValue)*((@Yr+1)-IfNull(Left(T2.DepreciationDt, 4), 0))) As DepYear,  ");
            if (mRptAssetCalculateBalanceAccumulationFormula == "1")
            {
                SQL.AppendLine("        If(T1.DepreciationCode='1', ");
                SQL.AppendLine("        (T1.AssetValue - (Sum(T3.DepreciationValue)*((@Yr+1)-IfNull(Left(T2.DepreciationDt, 4), 0)))),  ");
                SQL.AppendLine("        (T1.AssetValue -  T5.DepValue)) ");
            }
            if (mRptAssetCalculateBalanceAccumulationFormula == "2")
                SQL.AppendLine("        (T1.AssetValue -  T5.DepValue) ");
            SQL.AppendLine("        As Balanced, (T2.AssetValue -  T6.DepValue2) ResiduValue2 ");
            SQL.AppendLine("        From TblAsset T1 ");
            SQL.AppendLine("        Inner Join TblDepreciationAssetHdr T2 ");
            SQL.AppendLine("            On T1.AssetCode=T2.AssetCode ");
            SQL.AppendLine("            And T2.CancelInd='N' ");
            SQL.AppendLine("        Inner Join TblDepreciationAssetDtl T3 ");
            SQL.AppendLine("            On T2.DocNo=T3.DocNo ");
            if (mRptAssetCalculateBalanceAccumulationFormula == "1")
            {
                SQL.AppendLine("            And T3.Yr=@Yr ");
                SQL.AppendLine("            And T3.Mth=@Mth ");
            }
            if (mRptAssetCalculateBalanceAccumulationFormula == "2")
                SQL.AppendLine("            And Concat(T3.Yr, T3.Mth)<=Concat(@Yr,@Mth) ");
            if (ChkShowJournal.Checked) 
                SQL.AppendLine("            And T3.JournalDocNo Is Not Null ");
            SQL.AppendLine("        Left Join TblOption T4 On T1.DepreciationCode=T4.OptCode And T4.OptCat='DepreciationMethod'  ");
            SQL.AppendLine("        Left Join (  ");
            SQL.AppendLine("            Select X1.Docno, X1.AssetCode, ");
            SQL.AppendLine("            Sum(X2.DepreciationValue) As DepValue ");
            SQL.AppendLine("            From TblDepreciationAssethdr X1  ");
            SQL.AppendLine("            Inner Join TblDepreciationAssetDtl X2 On X1.DocNo=X2.DocNo  ");
            SQL.AppendLine("                And Concat(X2.Yr, X2.Mth)<=Concat(@Yr,@Mth) ");
            SQL.AppendLine("            Where X1.CancelInd = 'N'  ");
            SQL.AppendLine("            Group By X1.DocNo, X1.AssetCode ");
            SQL.AppendLine("        ) T5 On T2.AssetCode= T5.AssetCode And T2.DocNo=T5.DocNo  ");
            SQL.AppendLine("        Left Join (  ");
            SQL.AppendLine("            Select X1.Docno, X1.AssetCode, ");
            SQL.AppendLine("            Sum(X2.DepreciationValue) As DepValue2 ");
            SQL.AppendLine("            From TblDepreciationAssethdr X1  ");
            SQL.AppendLine("            Inner Join TblDepreciationAssetDtl X2 On X1.DocNo=X2.DocNo  ");
            SQL.AppendLine("                And Concat(X2.Yr, X2.Mth) < (Select Max(Concat(X3.Yr, X3.Mth)) from TblDepreciationAssetDtl X3 Where X3.DocNo=X2.DocNo) ");
            SQL.AppendLine("            Where X1.CancelInd = 'N'  ");
            SQL.AppendLine("            Group By X1.DocNo, X1.AssetCode ");
            SQL.AppendLine("        ) T6 On T2.AssetCode= T6.AssetCode And T2.DocNo=T6.DocNo  ");
            SQL.AppendLine("        Group By T2.DocNo, T2.AssetCode, T2.DepreciationDt, T1.AssetValue, T1.DepreciationCode ");
            SQL.AppendLine("    ) B On A.AssetCode = B.AssetCode  ");
            SQL.AppendLine("    Left Join TblOption D On A.DepreciationCode=D.OptCode And OptCat='DepreciationMethod'  ");
            SQL.AppendLine("    Left Join TblCostCenter E On A.CcCode = E.CCCode ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select F1.AssetCode, F2.DocNo, ");
            SQL.AppendLine("        F2.DepreciationValue As ResiduValue ");
            SQL.AppendLine("        From TblDepreciationAssetHdr F1 ");
            SQL.AppendLine("        Inner Join TblDepreciationAssetDtl F2 On F1.DocNo=F2.DocNo ");
            SQL.AppendLine("            And F2.Yr=@Yr ");
            SQL.AppendLine("            And F2.Mth=@Mth ");
            if (ChkShowJournal.Checked)
                SQL.AppendLine("            And F2.JournalDocNo Is Not Null ");
            SQL.AppendLine("        Inner Join ( ");
            SQL.AppendLine("            Select T1.DocNo, Max(T1.DNo) As DNo ");
            SQL.AppendLine("            From TblDepreciationAssetDtl T1 ");
            SQL.AppendLine("            Inner Join TblDepreciationAssetHdr T2 On T1.DocNo=T2.DocNo And T2.CancelInd='N' ");
            SQL.AppendLine("            Where T1.Yr=@Yr ");
            SQL.AppendLine("            And T1.Mth=@Mth ");
            if(ChkShowJournal.Checked) 
                SQL.AppendLine("            And T1.JournalDocNo is not null ");
            SQL.AppendLine("            Group By T1.Docno ");
            SQL.AppendLine("       ) F3 On F2.DocNo=F3.DocNo And F2.DNo=F3.DNo ");
            SQL.AppendLine("       Where F1.CancelInd = 'N'  ");
            SQL.AppendLine("    ) F On B.DocNo=F.DocNo And B.AssetCode=F.AssetCode ");
            SQL.AppendLine("    Left Join TblCOA G On A.AcNo2=G.AcNo  ");
            SQL.AppendLine("    Inner Join ( ");
            SQL.AppendLine("        Select Distinct T1.DocNo, T1.AssetCode, T1.AssetValue  ");
            SQL.AppendLine("        From TblDepreciationAssetHdr T1  ");
            SQL.AppendLine("        Inner Join TblDepreciationAssetDtl T2 ");
            SQL.AppendLine("            On T1.DocNo=T2.DocNo  ");
            SQL.AppendLine("            And T2.Yr = @Yr And T2.Mth=@Mth ");
            if (ChkShowJournal.Checked) 
                SQL.AppendLine("        And T2.JournalDocNo Is Not Null ");
            SQL.AppendLine("        Where T1.CancelInd= 'N' ");
            SQL.AppendLine("   ) H On B.DocNo=H.DocNo And B.AssetCode=H.AssetCode ");
            SQL.AppendLine("   Where A.ActiveInd ='Y' ");
            SQL.AppendLine(") Tbl ");

            mSQL = SQL.ToString();
        }
        
        private void SetGrd()
        {
            Grd1.Cols.Count = 19;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Asset Code", 
                    "Asset Name",
                    "Display Name",
                    "Year",
                    "Cost Center",

                    //6-10
                    "Asset"+Environment.NewLine+"Value",
                    "Depreciation"+Environment.NewLine+"Method",
                    "Economic Life"+Environment.NewLine+"(Month)",
                    "Residual"+Environment.NewLine+"value",
                    "Percentage"+Environment.NewLine+"Depreciation (%)",
                    
                    //11-15
                    "Depreciation"+Environment.NewLine+"Value",
                    "Age"+Environment.NewLine+"(Year)",
                    "Total Depreciation "+Environment.NewLine+"Value",
                    "Balance",
                    "Description",

                    //16-18
                    "Depreciation Date",
                    "Journal#",
                    "Short Code"
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    130, 200, 200, 80, 200, 

                    //6-10
                    130, 120, 120, 100, 120,

                    //11-15
                    150, 80, 150, 150, 200, 

                    //16-18
                    130, 200, 130
                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 6, 8, 9, 10, 11, 13, 14 }, 0);
            Sm.GrdFormatDate(Grd1, new int[] { 16 });
            Sm.GrdColInvisible(Grd1, new int[] { 1 }, false);
            Sm.GrdColInvisible(Grd1, new int[] { 14 }, !mIsAutoJournalActived);
            Grd1.Cols[15].Move(mIsAutoJournalActived?1:4);
            Grd1.Cols[18].Move(3);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 14 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            string Year = Sm.GetLue(LueYr);
            string Mth = Sm.GetLue(LueMth);

            if (Year.Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Year is empty.");
                return;
            }

            if (Mth.Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, "Month is empty.");
                return;
            }

            try
            {
                //SetSQL();
                Cursor.Current = Cursors.WaitCursor;

                string Filter = string.Empty, Filter2 = string.Empty, Query = string.Empty;
                var cm = new MySqlCommand();
                var SQL = new StringBuilder();
                var subSQL = new StringBuilder();

                SetProfitCenter();

                Sm.CmParam<String>(ref cm, "@Yr", Sm.GetLue(LueYr));
                Sm.CmParam<String>(ref cm, "@Mth", Sm.GetLue(LueMth));
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.FilterStr(ref Filter, ref cm, TxtAssetCode.Text, new string[] { "Tbl.AssetCode", "Tbl.AssetName", "Tbl.DisplayName" });
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDocDt1), Sm.GetDte(DteDocDt2), "Tbl.DepreciationDt");

                if (mIsRptDepreciationAssetUseProfitCenter)
                {
                    if (!mIsAllProfitCenterSelected)
                    {
                        int i = 0;

                        subSQL.AppendLine("    And X.CCCode Is Not Null ");
                        subSQL.AppendLine("    And X.CCCode In ( ");
                        subSQL.AppendLine("        Select Distinct CCCode ");
                        subSQL.AppendLine("        From TblCostCenter ");
                        subSQL.AppendLine("        Where ActInd='Y' ");
                        subSQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                        subSQL.AppendLine("        And ProfitCenterCode In ( ");
                        subSQL.AppendLine("            Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                        subSQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                        subSQL.AppendLine("        ) ");
                        foreach (var x in mlProfitCenter.Distinct())
                        {
                            if (Filter2.Length > 0) Filter2 += " Or ";
                            Filter2 += " (ProfitCenterCode=@ProfitCenterCode_" + i.ToString() + ") ";
                            Sm.CmParam<String>(ref cm, "@ProfitCenterCode_" + i.ToString(), x);
                            i++;
                        }
                        if (Filter2.Length == 0)
                            subSQL.AppendLine("    And 1=0 ");
                        else
                            subSQL.AppendLine("    And (" + Filter2 + ") ");
                        subSQL.AppendLine("    ) ");
                    }
                    else
                    {
                        if (ChkProfitCenterCode.Checked)
                        {
                            subSQL.AppendLine("    And X.CCCode Is Not Null ");
                            subSQL.AppendLine("    And X.CCCode In ( ");
                            subSQL.AppendLine("        Select Distinct CCCode ");
                            subSQL.AppendLine("        From TblCostCenter ");
                            subSQL.AppendLine("        Where ActInd='Y' ");
                            subSQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                            subSQL.AppendLine("        And Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ");
                            subSQL.AppendLine("    ) ");
                            if (ChkProfitCenterCode.Checked) Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                        }
                        else
                        {
                            subSQL.AppendLine("    And X.CCCode Is Not Null ");
                            subSQL.AppendLine("    And X.CCCode In ( ");
                            subSQL.AppendLine("        Select Distinct CCCode ");
                            subSQL.AppendLine("        From TblCostCenter ");
                            subSQL.AppendLine("        Where ActInd='Y' ");
                            subSQL.AppendLine("        And ProfitCenterCode Is Not Null ");
                            subSQL.AppendLine("        And ProfitCenterCode In (");
                            subSQL.AppendLine("            Select Distinct ProfitCenterCode From TblGroupProfitCenter ");
                            subSQL.AppendLine("            Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                            subSQL.AppendLine("    )) ");
                        }
                    }
                    Query = subSQL.ToString();
                }

                SQL.AppendLine("Select * From ( ");
                SQL.AppendLine("    Select  ");
                SQL.AppendLine("    A.AssetCode, A.AssetName, A.DisplayName,  A.ShortCode, ");
                SQL.AppendLine("    IfNull(Left(B.DepreciationDt, 4), '') As ThnPerolehan, ");
                SQL.AppendLine("    E.CCName As Location, H.AssetValue, ");
                SQL.AppendLine("    if(A.DepreciationCode = '1', 'Garis Lurus', 'Saldo Menurun') As DepCode, ");
                SQL.AppendLine("    A.EcoLife, F.ResiduValue, A.PercentageAnnualDepreciation, ");
                SQL.AppendLine("    B.DepValue, B.Age, B.DepYear, B.Balanced, G.AcDesc, ");
                SQL.AppendLine("    B.JournalDocNo, B.DepreciationDt, B.ResiduValue2 ");
                SQL.AppendLine("    From TblAsset A ");
                SQL.AppendLine("    Inner Join (  ");
                SQL.AppendLine("        Select T2.DocNo, T2.AssetCode, T2.DepreciationDt, ");
                SQL.AppendLine("        T1.AssetValue, T1.DepreciationCode, ");
                SQL.AppendLine("        Group_Concat(Distinct IfNull(T3.JournalDocNo, '') Separator ', ') As JournalDocNo, ");
                SQL.AppendLine("        Sum(T3.DepreciationValue) As DepValue, ");
                SQL.AppendLine("        ((@Yr+1) - IfNull(Left(T2.DepreciationDt, 4), 0000)) As Age, ");
                SQL.AppendLine("        (Sum(T3.DepreciationValue)*((@Yr+1)-IfNull(Left(T2.DepreciationDt, 4), 0))) As DepYear,  ");
                if (mRptAssetCalculateBalanceAccumulationFormula == "1")
                {
                    SQL.AppendLine("        If(T1.DepreciationCode='1', ");
                    SQL.AppendLine("        (T1.AssetValue - (Sum(T3.DepreciationValue)*((@Yr+1)-IfNull(Left(T2.DepreciationDt, 4), 0)))),  ");
                    SQL.AppendLine("        (T1.AssetValue -  T5.DepValue)) ");
                }
                if (mRptAssetCalculateBalanceAccumulationFormula == "2")
                    SQL.AppendLine("        (T1.AssetValue -  T5.DepValue) ");
                SQL.AppendLine("        As Balanced, (T2.AssetValue -  T6.DepValue2) ResiduValue2 ");
                SQL.AppendLine("        From TblAsset T1 ");
                SQL.AppendLine("        Inner Join TblDepreciationAssetHdr T2 ");
                SQL.AppendLine("            On T1.AssetCode=T2.AssetCode ");
                SQL.AppendLine("            And T2.CancelInd='N' ");
                SQL.AppendLine("        Inner Join TblDepreciationAssetDtl T3 ");
                SQL.AppendLine("            On T2.DocNo=T3.DocNo ");
                if (mRptAssetCalculateBalanceAccumulationFormula == "1")
                {
                    SQL.AppendLine("            And T3.Yr=@Yr ");
                    SQL.AppendLine("            And T3.Mth=@Mth ");
                }
                if (mRptAssetCalculateBalanceAccumulationFormula == "2")
                    SQL.AppendLine("            And Concat(T3.Yr, T3.Mth)<=Concat(@Yr,@Mth) ");
                if (ChkShowJournal.Checked)
                    SQL.AppendLine("            And T3.JournalDocNo Is Not Null ");
                SQL.AppendLine("        Left Join TblOption T4 On T1.DepreciationCode=T4.OptCode And T4.OptCat='DepreciationMethod'  ");
                SQL.AppendLine("        Left Join (  ");
                SQL.AppendLine("            Select X1.Docno, X1.AssetCode, ");
                SQL.AppendLine("            Sum(X2.DepreciationValue) As DepValue ");
                SQL.AppendLine("            From TblDepreciationAssetHdr X1  ");
                SQL.AppendLine("            Inner Join TblDepreciationAssetDtl X2 On X1.DocNo=X2.DocNo  ");
                SQL.AppendLine("                And Concat(X2.Yr, X2.Mth)<=Concat(@Yr,@Mth) ");
                SQL.AppendLine("            Inner Join TblAsset X3 On X1.AssetCode=X3.AssetCode ");
                SQL.AppendLine(Query.Replace("X.", "X3."));
                SQL.AppendLine("            Where X1.CancelInd = 'N'  ");
                SQL.AppendLine("            Group By X1.DocNo, X1.AssetCode ");
                SQL.AppendLine("        ) T5 On T2.AssetCode= T5.AssetCode And T2.DocNo=T5.DocNo ");
                SQL.AppendLine("        Left Join (  ");
                SQL.AppendLine("            Select X1.Docno, X1.AssetCode, ");
                SQL.AppendLine("            Sum(X2.DepreciationValue) As DepValue2 ");
                SQL.AppendLine("            From TblDepreciationAssetHdr X1  ");
                SQL.AppendLine("            Inner Join TblDepreciationAssetDtl X2 On X1.DocNo=X2.DocNo  ");
                SQL.AppendLine("                And Concat(X2.Yr, X2.Mth) < (Select Max(Concat(X3.Yr, X3.Mth)) from TblDepreciationAssetDtl X3 Where X3.DocNo=X2.DocNo) ");
                SQL.AppendLine("            Inner Join TblAsset X3 On X1.AssetCode=X3.AssetCode ");
                SQL.AppendLine(Query.Replace("X.", "X3."));
                SQL.AppendLine("            Where X1.CancelInd = 'N'  ");
                SQL.AppendLine("            Group By X1.DocNo, X1.AssetCode ");
                SQL.AppendLine("        ) T6 On T2.AssetCode= T6.AssetCode And T2.DocNo=T6.DocNo ");
                SQL.AppendLine(" Where 1=1 ");
                SQL.AppendLine(Query.Replace("X.", "T1."));
                SQL.AppendLine("        Group By T2.DocNo, T2.AssetCode, T2.DepreciationDt, T1.AssetValue, T1.DepreciationCode ");
                SQL.AppendLine("    ) B On A.AssetCode = B.AssetCode  ");
                SQL.AppendLine("    Left Join TblOption D On A.DepreciationCode=D.OptCode And OptCat='DepreciationMethod'  ");
                SQL.AppendLine("    Left Join TblCostCenter E On A.CCCode = E.CCCode ");
                SQL.AppendLine("    Left Join ( ");
                SQL.AppendLine("        Select F1.AssetCode, F2.DocNo, ");
                SQL.AppendLine("        F2.DepreciationValue As ResiduValue ");
                SQL.AppendLine("        From TblDepreciationAssetHdr F1 ");
                SQL.AppendLine("        Inner Join TblDepreciationAssetDtl F2 On F1.DocNo=F2.DocNo ");
                SQL.AppendLine("            And F2.Yr=@Yr ");
                SQL.AppendLine("            And F2.Mth=@Mth ");
                if (ChkShowJournal.Checked)
                    SQL.AppendLine("            And F2.JournalDocNo Is Not Null ");
                SQL.AppendLine("        Inner Join ( ");
                SQL.AppendLine("            Select T1.DocNo, Max(T1.DNo) As DNo ");
                SQL.AppendLine("            From TblDepreciationAssetDtl T1 ");
                SQL.AppendLine("            Inner Join TblDepreciationAssetHdr T2 On T1.DocNo=T2.DocNo And T2.CancelInd='N' ");
                SQL.AppendLine("            Inner Join TblAsset T3 On T2.AssetCode=T3.AssetCode ");
                SQL.AppendLine(Query.Replace("X.", "T3."));
                SQL.AppendLine("            Where T1.Yr=@Yr ");
                SQL.AppendLine("            And T1.Mth=@Mth ");
                if (ChkShowJournal.Checked)
                    SQL.AppendLine("            And T1.JournalDocNo is not null ");
                SQL.AppendLine("            Group By T1.Docno ");
                SQL.AppendLine("        ) F3 On F2.DocNo=F3.DocNo And F2.DNo=F3.DNo ");
                SQL.AppendLine("        Inner Join TblAsset F4 On F1.AssetCode=F4.AssetCode ");
                SQL.AppendLine(Query.Replace("X.", "F4."));
                SQL.AppendLine("       Where F1.CancelInd = 'N'  ");
                SQL.AppendLine("    ) F On B.DocNo=F.DocNo And B.AssetCode=F.AssetCode ");
                SQL.AppendLine("    Left Join TblCOA G On A.AcNo2=G.AcNo  ");
                SQL.AppendLine("    Inner Join ( ");
                SQL.AppendLine("        Select Distinct T1.DocNo, T1.AssetCode, T1.AssetValue  ");
                SQL.AppendLine("        From TblDepreciationAssetHdr T1  ");
                SQL.AppendLine("        Inner Join TblDepreciationAssetDtl T2 ");
                SQL.AppendLine("            On T1.DocNo=T2.DocNo  ");
                SQL.AppendLine("            And T2.Yr = @Yr And T2.Mth=@Mth ");
                if (ChkShowJournal.Checked)
                    SQL.AppendLine("        And T2.JournalDocNo Is Not Null ");
                SQL.AppendLine("        Inner Join TblAsset T3 On T1.AssetCode=T3.AssetCode ");
                SQL.AppendLine(Query.Replace("X.", "T3."));
                SQL.AppendLine("        Where T1.CancelInd= 'N' ");
                SQL.AppendLine("   ) H On B.DocNo=H.DocNo And B.AssetCode=H.AssetCode ");
                SQL.AppendLine("   Where A.ActiveInd ='Y' ");
                SQL.AppendLine(Query.Replace("X.", "A."));
                SQL.AppendLine(") Tbl ");

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        SQL.ToString() + Filter + " Order By Tbl.AssetName;",
                        new string[]
                        { 
                            //0
                            "AssetCode",

                            //1-5
                            "AssetName", "DisplayName", "ThnPerolehan", "Location", "AssetValue", 

                            //6-10
                            "DepCode", "EcoLife", "ResiduValue", "PercentageAnnualDepreciation", "DepValue",  

                            //11-15
                            "Age", "DepYear", "Balanced", "AcDesc", "DepreciationDt", 
                            
                            //16-18
                            "JournalDocNo", "ResiduValue2", "ShortCode"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                            if(mDocTitle == "PHT")
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 13);
                            else
                                Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 17);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 13, 10);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 14); 
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 16, 15);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 16);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 18);
                        }, true, false, false, false
                    );
                if (mIsAutoJournalActived)
                {
                    Grd1.GroupObject.Add(15);
                    Grd1.Group();
                    Sm.SetGrdAlwaysShowSubTotal(Grd1);
                }
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[]{ 11, 13, 14 });
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #region Additional Method

        private void SetProfitCenter()
        {
            mIsAllProfitCenterSelected = false;

            if (!mIsRptDepreciationAssetUseProfitCenter) return;
            if (!ChkProfitCenterCode.Checked)
                mIsAllProfitCenterSelected = true;

            if (mIsAllProfitCenterSelected) return;

            bool IsCompleted = false, IsFirst = true;

            mlProfitCenter.Clear();

            while (!IsCompleted)
                SetProfitCenter(ref IsFirst, ref IsCompleted);
        }

        private void SetProfitCenter(ref bool IsFirst, ref bool IsCompleted)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();
            string Filter = string.Empty, ProfitCenterCode = string.Empty;
            bool IsExisted = false;
            int i = 0;

            SQL.AppendLine("Select Distinct ProfitCenterCode From TblProfitCenter ");
            if (IsFirst)
            {
                if (ChkProfitCenterCode.Checked)
                {
                    SQL.AppendLine("    Where Find_In_Set(ProfitCenterCode, @ProfitCenterCode) ");
                    Sm.CmParam<String>(ref cm, "@ProfitCenterCode", GetCcbProfitCenterCode());
                    IsCompleted = false;
                }
                else
                    IsCompleted = true;
                IsFirst = false;
            }
            else
            {
                SQL.AppendLine("    Where Parent Is Not Null ");
                foreach (var x in mlProfitCenter.Distinct())
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += " (Parent=@ProfitCenterCode" + i.ToString() + ") ";

                    Sm.CmParam<String>(ref cm, "@ProfitCenterCode" + i.ToString(), x);
                    i++;
                }
                if (Filter.Length == 0)
                    SQL.AppendLine("    And 1=0 ");
                else
                {
                    SQL.AppendLine("    And (" + Filter + ") ");
                }
                IsCompleted = true;
            }
            SQL.AppendLine("Order By ProfitCenterCode;");

            cm.CommandText = SQL.ToString();

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "ProfitCenterCode" });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        if (IsFirst)
                            mlProfitCenter.Add(ProfitCenterCode);
                        else
                        {
                            ProfitCenterCode = Sm.DrStr(dr, c[0]);
                            IsExisted = false;
                            foreach (var x in mlProfitCenter.Where(w => Sm.CompareStr(w, ProfitCenterCode)))
                                IsExisted = true;
                            if (!IsExisted)
                            {
                                mlProfitCenter.Add(ProfitCenterCode);
                                IsCompleted = false;
                            }
                        }
                    }
                }
                else
                    IsCompleted = true;
                dr.Close();
            }
        }

        private void SetCcbProfitCenterCode(ref CheckedComboBoxEdit Ccb)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Col, Col2 From (");
            SQL.AppendLine("    Select ProfitCenterName AS Col, ProfitCenterCode As Col2 ");
            SQL.AppendLine("    From TblProfitCenter ");
            SQL.AppendLine("    Where ProfitCenterCode In ( ");
            SQL.AppendLine("        Select ProfitCenterCode From TblGroupProfitCenter ");
            SQL.AppendLine("        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
            SQL.AppendLine("        ) ");
            SQL.AppendLine(") Tbl Order By Col2; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            Sm.SetCcb(ref Ccb, cm);
        }

        private string GetCcbProfitCenterCode()
        {
            var Value = Sm.GetCcb(CcbProfitCenterCode);
            if (Value.Length == 0) return string.Empty;
            return GetProfitCenterCode(Value).Replace(", ", ",");
        }

        private string ProcessCcbProfitCenterCode(string Value)
        {
            if (Value.Length == 0) return string.Empty;
            return ("#" + GetProfitCenterCode(Value).Replace(", ", "# #") + "#").Replace("#", @"""");
        }

        private string GetProfitCenterCode(string Value)
        {
            if (Value.Length == 0) return string.Empty;

            return
                Sm.GetValue(
                    "Select Group_Concat(T.Code Separator ', ') As Code " +
                    "From (Select ProfitCenterCode As Code From TblProfitCenter Where Find_In_Set(ProfitCenterName, @Param) ) T; ",
                    Value.Replace(", ", ","));
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
            Grd1.Rows.AutoHeight();
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion       

        #region Event

        #region Misc Control Event

        private void TxtAssetCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkAssetCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Asset");
        }

        private void ChkDocDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Date");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void CcbProfitCenterCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterCcbSetCheckEdit(this, sender);
        }

        private void ChkProfitCenterCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetCheckedComboBoxEdit(this, sender, "Multi Profit Center");
        }

        #endregion

        #endregion
    }
}
