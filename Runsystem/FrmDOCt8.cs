﻿#region Update
/*
    20/11/2020 [WED/IMS] new apps
    08/12/2020 [WED/IMS] kalau progress gak di centang, docno jadi DOCT
    09/12/2020 [WED/IMS] tambah tampil informasi item dismantle kalau checkbox Progress di centang
    14/12/2020 [WED/IMS] kalau Progress di centang, DO Price di hide. tambah kolom Notes dan Notes Detail
    19/12/2020 [DITA/IMS] Printout baru DO Progress
    20/01/2021 [DITA/IMS] tambah kolom specification berdasarkan param : IsBOMShowSpecifications
    29/01/2021 [WED/IMS] kalau Progress di centang, Amount di hide.
    04/02/2021 [DITA/IMS] menampilkan item dismantle berdasrakan no di socontractdtl4 dan socontractdtl, karena no di boq pasti berbeda dengan di socontract
    02/03/2021 [IBL/IMS] Penyesuaian printout
    03/03/2021 [WED/IMS] Simpan dokumen Transfer Request Project
    03/03/2021 [IBL/IMS] Penyesuaian printout (Item Consumable)
    05/03/2021 [IBL/IMS] Feedback printout (Consumable) : specification blm muncul
    18/03/2021 [IBL/IMS] BUG printout laporan progress item tidak terurutkan
    13/04/2021 [VIN/IMS] BUG show dtl terdobel
    28/04/2021 [WED/IMS] kalau Progress di centang, tidak membentuk journal
    04/05/2021 [VIN/IMS] bug : printout
    11/05/2021 [VIN/IMS] Ganti source printout
    07/06/2021 [TRI/IMS] Ganti Order by di list Item's Name, berdasarkan Dno
    21/06/2021 [VIN/IMS] Item yang sudah cancel tidak perlu di print
    28/12/2021 [MYA/IMS] Menambahkan designer pada printout dan menghubungkan dengan approval
 */

#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

using FastReport;
using FastReport.Data;

#endregion

namespace RunSystem
{
    public partial class FrmDOCt8 : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string
            mMenuCode = string.Empty, mAccessInd = string.Empty,
            mDocNo = string.Empty, //if this application is called from other application;
            mCustomsDocCode = string.Empty,
            mCtSADNo = string.Empty,
            mDocNoFormat = string.Empty,
            mEntCode = string.Empty,
            mGrpCode = string.Empty,
            mMenuCodeForSOContract2 = string.Empty;
        internal FrmDOCt8Find FrmFind;
        internal int mNumberOfInventoryUomCode = 1;
        private bool
            mIsDOCtNeedShippingAddressInd = false,
            mIsDOCtPriceEditable = false,
            mIsAutoJournalActived = false,
            mIsAcNoForSaleUseItemCategory = false,
            mIsDOCtUseCtQtPrice = false,
            mIsDOCtUseProcessInd = false,
            mIsDOCtProsessToSalesInvoice = false,
            mIsDOCtRemarkEditable = false,
            mIsUseProductionWorkGroup = false
            ;
        internal bool mIsKawasanBerikatEnabled = false,
            mIsBOMShowSpecifications = false,
            mIsCustomerShpAddressDisplayWide = false,
            mIsDOCtShowPackagingUnit = false,
            mIsItGrpCodeShow = false,
            mIsShowLocalDocNo = false,
            mIsShowForeignName = false,
            mIsFilterBySite = false,
            //mIsAmtRoundingUp = false,
            mIsDOCtAmtRounded = false,
            mIsDOCtApprovalActived = false,
            mIsDOCtShowStockPrice = false,
            mIsDOCtHideBatchNo = false,
            mIsDOCtShowRemarkDetail = false,
            mIsDOCtUseSOContract = false,
            mIsAbleToAccessSOContractData = true
            ;
        private string 
            mDocType = "07",
            mMainCurCode = string.Empty;
        internal string
            mCityCode = string.Empty,
            mCntCode = string.Empty,
            mISDOCtShowCustomerItem = string.Empty,
            mPEBGrpCode = string.Empty,
            mPEBFilePath = string.Empty,
            mPEBPassword = string.Empty,
            mPEBDocType = string.Empty,
            mCustomerCategoryCodeForWideDOCt = string.Empty,
            mDocTitle = string.Empty;

        iGCell fCell;
        bool fAccept;

        #endregion

        #region Constructor

        public FrmDOCt8(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "DO To Customer";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                if (mIsKawasanBerikatEnabled)
                {
                    Tp2.PageVisible = true;
                    if (!IsAuthorizedToAccessPEB())
                    {
                        BtnPEB.Visible = false;
                    }
                }

                if (!mIsDOCtUseSOContract)
                {
                    Tp3.PageVisible = false;
                    mIsAbleToAccessSOContractData = false;
                }
                else
                {
                    Tp3.Select();
                    mMenuCodeForSOContract2 = Sm.GetValue("Select Menucode from TblMenu where Param = 'FrmSOContract2'");
                    mGrpCode = Sm.SetFormForGroupUser(mMenuCodeForSOContract2);
                    if (mGrpCode.Length == 0) 
                    {
                        BtnSOContractDocNo2.Visible = false;
                        mIsAbleToAccessSOContractData = false;
                    }
                }

                GetValue();
                SetGrd();
                SetFormControl(mState.View);
                Sl.SetLueCurCode(ref LueCurCode);
                SetLueProcessInd(ref LueProcessInd);

                LblSAName.ForeColor = mIsDOCtNeedShippingAddressInd?
                    System.Drawing.Color.Red:System.Drawing.Color.Black;

                LuePackagingUnitUomCode.Visible = false;

                if (!mIsDOCtUseProcessInd)
                {
                    LblProcessInd.Visible = LueProcessInd.Visible = false;
                }

                if (!mIsUseProductionWorkGroup)
                {
                    LblProductionWorkGroup.Visible = LueProductionWorkGroup.Visible = false;
                }
                else
                {
                    Sl.SetLueOption(ref LueProductionWorkGroup, "ProductionWorkGroup");
                }
              

                base.FrmLoad(sender, e);
                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs  e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void GetValue()
        {
            mIsDOCtApprovalActived = Sm.IsDataExist("Select 1 from TblDocApprovalSetting Where DocType='DOCt' Limit 1;");
        }

        private void GetParameter()
        {
            SetNumberOfInventoryUomCode();
            mIsDOCtNeedShippingAddressInd = Sm.GetParameterBoo("IsDOCtNeedShippingAddressInd");
            mIsItGrpCodeShow = Sm.GetParameterBoo("IsItGrpCodeShow");
            mIsDOCtShowPackagingUnit = Sm.GetParameterBoo("IsDOCtShowPackagingUnit");
            mISDOCtShowCustomerItem = Sm.GetParameter("ISDOCtShowCustomerItem");
            mIsDOCtPriceEditable = Sm.GetParameterBoo("IsDOCtPriceEditable");
            mIsShowForeignName = Sm.GetParameterBoo("IsShowForeignName");
            mIsShowLocalDocNo = Sm.GetParameterBoo("IsShowLocalDocNo");
            mIsFilterBySite = Sm.GetParameterBoo("IsFilterBySite");
            mIsDOCtProsessToSalesInvoice = Sm.GetParameterBoo("IsDOCtProsessToSalesInvoice");
            mIsAutoJournalActived = Sm.GetParameterBoo("IsAutoJournalActived");
            mMainCurCode = Sm.GetParameter("MainCurCode");
            mIsAcNoForSaleUseItemCategory = Sm.GetParameterBoo("IsAcNoForSaleUseItemCategory");
            mIsDOCtRemarkEditable = Sm.GetParameterBoo("IsDOCtRemarkEditable");
            mIsDOCtAmtRounded = Sm.GetParameterBoo("IsDOCtAmtRounded");
            mIsKawasanBerikatEnabled = Sm.GetParameter("KB_Server").Length > 0;
            mPEBGrpCode = Sm.GetParameter("PEBGrpCode");
            mPEBFilePath = Sm.GetParameter("PEBFilePath");
            mPEBPassword = Sm.GetParameter("PEBPassword");
            mPEBDocType = Sm.GetParameter("PEBDocType");
            mIsDOCtUseCtQtPrice = Sm.GetParameterBoo("IsDOCtUseCtQtPrice");
            mIsDOCtUseProcessInd = Sm.GetParameterBoo("IsDOCtUseProcessInd");
            mIsBOMShowSpecifications = Sm.GetParameterBoo("IsBOMShowSpecifications");
            mIsCustomerShpAddressDisplayWide = Sm.GetParameterBoo("IsCustomerShpAddressDisplayWide");
            mCustomerCategoryCodeForWideDOCt = Sm.GetParameter("CustomerCategoryCodeForWideDOCt");
            mDocNoFormat = Sm.GetParameter("DocNoFormat");
            mIsDOCtShowStockPrice = Sm.GetParameterBoo("IsDOCtShowStockPrice");
            mIsDOCtHideBatchNo = Sm.GetParameterBoo("IsDOCtHideBatchNo");
            mIsUseProductionWorkGroup = Sm.GetParameterBoo("IsUseProductionWorkGroup");
            mIsDOCtShowRemarkDetail = Sm.GetParameterBoo("IsDOCtShowRemarkDetail");
            mIsDOCtUseSOContract = Sm.GetParameterBoo("IsDOCtUseSOContract");
            

            if (mPEBDocType.Length <= 0) mPEBDocType = "30";
        }

        private void SetGrd()
        {
            #region Grid 1

            Grd1.Cols.Count = 44;
            Grd1.FrozenArea.ColCount = 4;

            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "DNo",
                        
                        //1-5
                        "Cancel",
                        "Old Cancel",
                        "",
                        "Item's Code",
                        "",
                        
                        //6-10
                        "Local Code",
                        "Item's Name",
                        "Property Code",
                        "Property",
                        "Batch#",

                        //11-15
                        "Source",
                        "Lot",
                        "Bin",
                        "Stock",
                        "Quantity",

                        //16-20
                        "UoM",
                        "Stock",
                        "Quantity",
                        "UoM",
                        "Stock",

                        //21-25
                        "Quantity",
                        "UoM",
                        "DO's Price",
                        "Amount",
                        "Remark",

                        //26-30
                        "Group",
                        "Quantity" +Environment.NewLine + "(Packaging)",
                        "UoM" +Environment.NewLine + "(Packaging)",
                        "Customer"+Environment.NewLine+"Refference",
                        "Foreign Name",

                        //31-35
                        "Specification",
                        "CtQtInd",
                        "Stock's Price",
                        "SOContractDNo",
                        "No",

                        //36-40
                        "Item Dismantle's"+Environment.NewLine+"Code",
                        "Item Dismantle's"+Environment.NewLine+"Name",
                        "Notes",
                        "Notes Detail",
                        "SOContractQty",

                        //41-43
                        "TransferRequestDocNo",
                        "TransferRequestDNo",
                        "SeqNo"
                    },
                     new int[] 
                    {
                        //0
                        20,
                        
                        //1-5
                        50, 50, 20, 100, 20, 
                        
                        //6-10
                        120, 250, 0, 100, 200, 
                        
                        //11-15
                        180, 60, 60, 80, 80, 
                        
                        //16-20
                        80, 80, 80, 80, 80, 
                        
                        //21-25
                        80, 80, 130, 100, 400,

                        //26-30
                        100, 100, 100, 100, 230,

                        //31-35
                        300, 0, 130, 0, 100,

                        //36-40
                        120, 200, 200, 200, 0,

                        //41-43
                        0, 0, 0
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1, 2 });
            Sm.GrdFormatDec(Grd1, new int[] { 14, 15, 17, 18, 20, 21, 23, 24, 27, 33, 40, 43 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 3, 5 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 16, 17, 19, 20, 22, 24, 26, 29, 30, 31, 32, 33, 34, 35, 36, 37, 40, 41, 42, 43 });
            Grd1.Cols[29].Move(11);
            if (!mIsBOMShowSpecifications) Sm.GrdColInvisible(Grd1, new int[] { 31 });
            Grd1.Cols[31].Move(8);
            if (mIsShowForeignName)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 4, 5, 6, 8, 9, 11, 12, 13, 17, 18, 19, 20, 21, 22, 26, 27, 28 }, false);
                Grd1.Cols[30].Move(8);
            } else
                Sm.GrdColInvisible(Grd1, new int[] { 0, 2, 4, 5, 6, 8, 9, 11, 12, 13, 17, 18, 19, 20, 21, 22, 26, 27, 28, 30 }, false);

            if (mISDOCtShowCustomerItem == "N")
            {
                Sm.GrdColInvisible(Grd1, new int[] { 29 }, false);
            }

            if (mIsItGrpCodeShow)
            {
                Grd1.Cols[26].Visible = true;
                if (mIsBOMShowSpecifications) Grd1.Cols[26].Move(9);
                else Grd1.Cols[26].Move(7);
            }

            if (mIsDOCtShowPackagingUnit)
            {
                Grd1.Cols[27].Visible = true;
                Grd1.Cols[28].Visible = true;
                Grd1.Cols[28].Move(14);
                Grd1.Cols[27].Move(15);
            }
            if (mIsDOCtHideBatchNo) Sm.GrdColInvisible(Grd1, new int[] { 10 });

            Grd1.Cols[33].Visible = mIsDOCtShowStockPrice;
            if (mIsDOCtShowStockPrice) Grd1.Cols[33].Move(23);

            Grd1.Cols[37].Move(9);
            Grd1.Cols[36].Move(9);
            Grd1.Cols[35].Move(4);

            if (!ChkProgressInd.Checked)
                Sm.GrdColInvisible(Grd1, new int[] { 36, 37 });
            
            #endregion

            #region Grid 2

            Grd2.Cols.Count = 3;
            Grd2.ReadOnly = true;

            Sm.GrdHdrWithColWidth(
                    Grd2,
                    new string[] 
                    {
                        "Total Quantity", "Total Quantity 2", "Total Quantity 3"
                    },
                    new int[] 
                    {
                        130, 130, 130
                    }
                );
            Sm.GrdFormatDec(Grd2, new int[] { 0, 1, 2 }, 0);
            Sm.GrdColInvisible(Grd2, new int[] { 1, 2 }, false);

            #endregion

            ShowInventoryUomCode();
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4, 5, 8, 9, 11, 12, 13 }, !ChkHideInfoInGrd.Checked);
            if (!mIsDOCtHideBatchNo) Sm.GrdColInvisible(Grd1, new int[] {10}, !ChkHideInfoInGrd.Checked);
            if (mIsBOMShowSpecifications) Sm.GrdColInvisible(Grd1, new int[] { 6 }, !ChkHideInfoInGrd.Checked);
            ShowInventoryUomCode();
        }

        private void ShowInventoryUomCode()
        {
            if (mNumberOfInventoryUomCode == 2)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 17, 18, 19 }, true);
                Sm.GrdColInvisible(Grd2, new int[] { 1 }, true);
            }

            if (mNumberOfInventoryUomCode == 3)
            {
                Sm.GrdColInvisible(Grd1, new int[] { 17, 18, 19, 20, 21, 22 }, true);
                Sm.GrdColInvisible(Grd2, new int[] { 1, 2 }, true);
            }
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueWhsCode, LueCtCode, TxtDocNoInternal, TxtSAName, 
                        TxtResiNo, TxtDocNoInternal, LueCurCode, TxtDriver, TxtExpedition, 
                        TxtVehicleRegNo, MeeRemark, LueProcessInd, LueProductionWorkGroup,
                        ChkProgressInd, ChkServiceInd
                    }, true);
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 1, 3, 15, 18, 21, 23, 25, 27, 28, 38, 39 });
                    BtnKBContractNo.Enabled = BtnPEB.Enabled = false;
                    BtnCustomerShipAddress.Enabled = false;
                    BtnSOContractDocNo.Enabled = false;
                    BtnSAName.Enabled = false;
                    BtnCopyDOCt.Enabled = false;
                    TxtDocNo.Focus();
                    
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueWhsCode, LueCtCode, TxtDocNoInternal, LueCurCode, 
                        TxtResiNo, TxtDriver, TxtExpedition, TxtVehicleRegNo, MeeRemark, 
                        LueProductionWorkGroup, ChkProgressInd, ChkServiceInd
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 3, 15, 18, 21, 23, 25, 27, 28, 38, 39 });
                    BtnCustomerShipAddress.Enabled = true;
                    BtnSAName.Enabled = true;
                    BtnSOContractDocNo.Enabled = true;
                    BtnKBContractNo.Enabled = BtnPEB.Enabled = true;
                    BtnCopyDOCt.Enabled = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1 });
                    if (mIsDOCtPriceEditable) Sm.GrdColReadOnly(false, true, Grd1, new int[] { 23 });
                    if (mIsDOCtRemarkEditable) Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ MeeRemark }, false);
                    if (mIsDOCtUseProcessInd)
                    {
                        if (Sm.IsDataExist("Select 1 From TblDOCtHdr Where DocNo = @Param And ProcessInd = 'D';", TxtDocNo.Text))
                        { 
                            Sm.SetControlReadOnly(new List<DXE.BaseEdit> { LueProcessInd }, false);
                            Sm.GrdColReadOnly(false, true, Grd1, new int[] { 15, 18, 21, 23 });

                            for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
                            {
                                if (Sm.GetGrdStr(Grd1, i, 32).Length != 0 || Sm.GetGrdBool(Grd1, i, 1))
                                {
                                    Grd1.Cells[i, 15].ReadOnly = iGBool.True;
                                    Grd1.Cells[i, 18].ReadOnly = iGBool.True;
                                    Grd1.Cells[i, 21].ReadOnly = iGBool.True;
                                    Grd1.Cells[i, 23].ReadOnly = iGBool.True;
                                }
                            }
                        }
                    }
                    TxtDocNo.Focus();
                    break;
            }
        }

        private void ClearData()
        {
            mCtSADNo = string.Empty;
            mCityCode = string.Empty;
            mCntCode = string.Empty;
            mCustomsDocCode = string.Empty;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, TxtStatus, LueWhsCode, LueCtCode, 
                TxtDocNoInternal, TxtDriver, TxtExpedition, TxtVehicleRegNo, MeeRemark, 
                LueCurCode, TxtSAName, MeeSAAddress, TxtCity, TxtCountry, 
                TxtPostalCd, TxtPhone, TxtFax, TxtEmail, TxtResiNo, 
                TxtMobile, TxtKBContractNo, DteKBContractDt, TxtKBPLNo, DteKBPLDt, 
                TxtKBRegistrationNo, DteKBRegistrationDt, TxtKBSubmissionNo, LueProcessInd, LueProductionWorkGroup,
                TxtSOContractDocNo, TxtProjectCode, TxtPONo, TxtProjectName
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit>{ TxtAmt }, 0);
            ClearGrd();
            ChkProgressInd.Checked = false;
            ChkHideInfoInGrd.Checked = false;
            ChkProgressIndChecked();
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ClearData2()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtSAName, MeeSAAddress, TxtCity, TxtCountry, TxtPostalCd, 
                TxtPhone, TxtFax, TxtEmail, TxtMobile
            });
        }

        internal void ClearGrd()
        {
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdBoolValueFalse(ref Grd1, 0, new int[] { 1, 2 });
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 14, 15, 17, 18, 20, 21, 23, 24, 27, 33 });

            Sm.ClearGrd(Grd2, true);
            Sm.SetGrdNumValueZero(ref Grd2, 0, new int[] { 0, 1, 2 });
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmDOCt8Find(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                Sl.SetLueWhsCode(ref LueWhsCode, string.Empty);
                Sm.SetLue(LueCurCode, Sm.GetParameter("MainCurCode"));
                Sl.SetLueCtCode(ref LueCtCode, string.Empty);
                if (mIsDOCtUseProcessInd) Sm.SetLue(LueProcessInd, "D");
                else Sm.SetLue(LueProcessInd, "F");
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select C.EntCode ");
            SQL.AppendLine("From TblWarehouse A ");
            SQL.AppendLine("Inner Join TblCostCenter B On A.CCCode = B.CCCode ");
            SQL.AppendLine("    And A.WhsCode = @Param ");
            SQL.AppendLine("Inner Join TblProfitCenter C On B.ProfitCenterCode = C.ProfitCenterCode; ");

            mEntCode = Sm.GetValue(SQL.ToString(), Sm.GetLue(LueWhsCode));

            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            try
            {
                if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                    Sm.StdMsgYN("Print", "") == DialogResult.No ||
                    IsDOCtStatusOutstanding() ||
                    IsDOCtStatusCancelled()
                    ) return;
                ParPrint();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private bool IsDOCtStatusOutstanding()
        {
            return Sm.IsDataExist(
                "Select 1 from TblDOCtHdr Where DocNo=@Param And Status='O';",
                TxtDocNo.Text,
                "PO's status still outstanding.");
        }

        private bool IsDOCtStatusCancelled()
        {
            return Sm.IsDataExist(
                "Select 1 from TblDOCtHdr Where DocNo=@Param And Status='C';",
                TxtDocNo.Text,
                "PO is cancelled.");
        }

        #endregion

        #region Grid Method

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (TxtDocNo.Text.Length == 0)
                {
                    if (e.ColIndex == 3 && !Sm.IsLueEmpty(LueWhsCode, "Warehouse"))
                    {
                        e.DoDefault = false;
                        if (e.KeyChar == Char.Parse(" ")) Sm.FormShowDialog(new FrmDOCt8Dlg(this, Sm.GetLue(LueWhsCode), Sm.GetLue(LueCtCode)));
                    }

                    if (Sm.IsGrdColSelected(new int[] { 3, 15, 18, 21, 23, 25, 27, 28 }, e.ColIndex))
                    {
                        if (e.ColIndex == 28 )
                        {
                            var ItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                            if (ItCode.Length == 0) ItCode = "XXX";
                            SetLuePackagingUnitUomCode(ref LuePackagingUnitUomCode, ItCode, string.Empty);
                            LueRequestEdit(Grd1, LuePackagingUnitUomCode, ref fCell, ref fAccept, e, 28);
                        }
                        Sm.GrdRequestEdit(Grd1, e.RowIndex);
                        Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 1, 2 });
                        Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 14, 15, 17, 18, 20, 21, 23, 24, 27, 33 });
                    }
                }
                else
                {
                    if (!(
                        (e.ColIndex==1 || (mIsDOCtPriceEditable && e.ColIndex==23)) &&
                        !Sm.GetGrdBool(Grd1, e.RowIndex, 2) && 
                        Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0
                        ))
                        e.DoDefault = false;

                    if (Sm.IsGrdColSelected(new int[] { 15, 18, 21, 23 }, e.ColIndex) && mIsDOCtUseCtQtPrice)
                    {
                        e.DoDefault = true;
                    }
                

                    //if (e.ColIndex == 1 && (Sm.GetGrdBool(Grd1, e.RowIndex, 2) || Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length == 0))
                    //    e.DoDefault = false;
                }
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                e.DoDefault = false;
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }
        }

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (TxtDocNo.Text.Length == 0)
            {
                Sm.GrdRemoveRow(Grd1, e, BtnSave);
                ComputeTotalQty();
                ComputeAmt();
            }
            Sm.GrdKeyDown(Grd1, e, BtnFind, BtnSave);
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && BtnSave.Enabled && !Sm.IsLueEmpty(LueWhsCode, "Warehouse") && TxtDocNo.Text.Length == 0)
            {
                if (mIsDOCtUseCtQtPrice)
                {
                    if (!Sm.IsLueEmpty(LueCtCode, "Customer"))
                    {
                        Sm.FormShowDialog(new FrmDOCt8Dlg(this, Sm.GetLue(LueWhsCode), Sm.GetLue(LueCtCode)));
                    }
                }
                else
                {
                    Sm.FormShowDialog(new FrmDOCt8Dlg(this, Sm.GetLue(LueWhsCode), Sm.GetLue(LueCtCode)));
                }
            }

            if (e.ColIndex == 5 && Sm.GetGrdStr(Grd1, e.RowIndex, 4).Length != 0)
            {
                var f = new FrmItem(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                f.ShowDialog();
            }
        }

        override protected void GrdAfterCommitEdit(object sender, iGAfterCommitEditEventArgs e)
        {
            try
            {
                Sm.GrdAfterCommitEditSetZeroIfEmpty(Grd1, new int[] { 15, 18, 21, 23, 27 }, e);
                Sm.GrdAfterCommitEditTrimString(Grd1, new int[] { 25 }, e);

                if (mIsDOCtShowPackagingUnit)
                {
                    if (e.ColIndex == 28) Sm.SetGrdNumValueZero(ref Grd1, e.RowIndex, new int[] { 15, 18, 21, 27 });
                    
                    if (e.ColIndex == 27)
                    {
                        string PackagingUnitUomCode = Sm.GetGrdStr(Grd1, e.RowIndex, 28);
                        if (PackagingUnitUomCode.Length>0)
                        {
                            decimal QtyPackagingUnit = Sm.GetGrdDec(Grd1, e.RowIndex, 27);
                            string ItCode = Sm.GetGrdStr(Grd1, e.RowIndex, 4);
                            string InventoryUomCode = Sm.GetGrdStr(Grd1, e.RowIndex, 16);
                            string SalesUomCode = string.Empty, SalesUomCode2 = string.Empty;
                            decimal Qty = 0m, Qty2 = 0m;

                            GetPackagingUnitInfo(ItCode, PackagingUnitUomCode, ref Qty, ref Qty2, ref SalesUomCode, ref SalesUomCode2);
                            if (Sm.CompareStr(InventoryUomCode, SalesUomCode))
                            {
                                Grd1.Cells[e.RowIndex, 15].Value = Qty * QtyPackagingUnit;
                                Sm.ComputeQtyBasedOnConvertionFormula("12", Grd1, e.RowIndex, 4, 15, 18, 21, 16, 19, 22);
                                Sm.ComputeQtyBasedOnConvertionFormula("13", Grd1, e.RowIndex, 4, 15, 21, 18, 16, 22, 19);
                                if (Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 16), Sm.GetGrdStr(Grd1, e.RowIndex, 19)))
                                    Sm.CopyGrdValue(Grd1, e.RowIndex, 18, Grd1, e.RowIndex, 15);
                                if (Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 16), Sm.GetGrdStr(Grd1, e.RowIndex, 22)))
                                    Sm.CopyGrdValue(Grd1, e.RowIndex, 21, Grd1, e.RowIndex, 15);
                            }
                            else
                            {
                                if (Sm.CompareStr(InventoryUomCode, SalesUomCode2))
                                {
                                    Grd1.Cells[e.RowIndex, 15].Value = Qty2 * QtyPackagingUnit;
                                    Sm.ComputeQtyBasedOnConvertionFormula("12", Grd1, e.RowIndex, 4, 15, 18, 21, 16, 19, 22);
                                    Sm.ComputeQtyBasedOnConvertionFormula("13", Grd1, e.RowIndex, 4, 15, 21, 18, 16, 22, 19);
                                    if (Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 16), Sm.GetGrdStr(Grd1, e.RowIndex, 19)))
                                        Sm.CopyGrdValue(Grd1, e.RowIndex, 18, Grd1, e.RowIndex, 15);
                                    if (Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 16), Sm.GetGrdStr(Grd1, e.RowIndex, 22)))
                                        Sm.CopyGrdValue(Grd1, e.RowIndex, 21, Grd1, e.RowIndex, 15);
                                }
                            }
                        }
                    }
                }

                if (e.ColIndex == 15)
                {
                    Sm.ComputeQtyBasedOnConvertionFormula("12", Grd1, e.RowIndex, 4, 15, 18, 21, 16, 19, 22);
                    Sm.ComputeQtyBasedOnConvertionFormula("13", Grd1, e.RowIndex, 4, 15, 21, 18, 16, 22, 19);
                }

                if (e.ColIndex == 18)
                {
                    Sm.ComputeQtyBasedOnConvertionFormula("21", Grd1, e.RowIndex, 4, 18, 15, 21, 19, 16, 22);
                    Sm.ComputeQtyBasedOnConvertionFormula("23", Grd1, e.RowIndex, 4, 18, 21, 15, 19, 22, 16);
                }

                if (e.ColIndex == 21)
                {
                    Sm.ComputeQtyBasedOnConvertionFormula("31", Grd1, e.RowIndex, 4, 21, 15, 18, 22, 16, 19);
                    Sm.ComputeQtyBasedOnConvertionFormula("32", Grd1, e.RowIndex, 4, 21, 18, 15, 22, 19, 16);
                }

                if (e.ColIndex == 15 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 16), Sm.GetGrdStr(Grd1, e.RowIndex, 19)))
                    Sm.CopyGrdValue(Grd1, e.RowIndex, 18, Grd1, e.RowIndex, 15);

                if (e.ColIndex == 15 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 16), Sm.GetGrdStr(Grd1, e.RowIndex, 22)))
                    Sm.CopyGrdValue(Grd1, e.RowIndex, 21, Grd1, e.RowIndex, 15);

                if (e.ColIndex == 18 && Sm.CompareStr(Sm.GetGrdStr(Grd1, e.RowIndex, 19), Sm.GetGrdStr(Grd1, e.RowIndex, 22)))
                    Sm.CopyGrdValue(Grd1, e.RowIndex, 21, Grd1, e.RowIndex, 18);

                if (Sm.IsGrdColSelected(new int[] { 1, 15, 18, 21, 27, 28 }, e.ColIndex)) ComputeTotalQty();
                if (Sm.IsGrdColSelected(new int[] { 1, 15, 23, 27, 28 }, e.ColIndex)) ComputeTotal(e.RowIndex);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void GrdColHdrDoubleClick(object sender, iGColHdrDoubleClickEventArgs e)
        {
            if (Sm.IsGrdColSelected(new int[] { 15, 18, 21, 24 }, e.ColIndex))
            {
                decimal Total = 0m;
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, e.ColIndex).Length != 0) Total += Sm.GetGrdDec(Grd1, Row, e.ColIndex);
                }
                Sm.StdMsg(mMsgType.Info, "Total : " + Sm.FormatNum(Total, 0));
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;
            string DocNo = string.Empty;
            //string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "DOCt", "TblDOCtHdr");
            if (mDocNoFormat == "1")
            {
                if (ChkProgressInd.Checked)
                    DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "DOCt8", "TblDOCtHdr");
                else
                    DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "DOCt", "TblDOCtHdr");
            }
            else
            {
                if (ChkProgressInd.Checked)
                    DocNo = Sm.GenerateDocNo3(Sm.GetDte(DteDocDt), "DoCt8", "TblDoctHdr", mEntCode, "1");
                else
                    DocNo = Sm.GenerateDocNo3(Sm.GetDte(DteDocDt), "DoCt", "TblDoctHdr", mEntCode, "1");
            }
            var cml = new List<MySqlCommand>();

            cml.Add(SaveDOCtHdr(DocNo));
            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdStr(Grd1, Row, 4).Length > 0) cml.Add(SaveDOCtDtl(DocNo, Row));

            if (!mIsDOCtUseProcessInd)
            {
                if (!ChkServiceInd.Checked && !ChkProgressInd.Checked) cml.Add(SaveStock(DocNo));

                if (mIsAutoJournalActived && !ChkProgressInd.Checked) cml.Add(SaveJournal(DocNo));
            }

            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            ReComputeStock();
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueWhsCode, "Warehouse") ||
                Sm.IsLueEmpty(LueCtCode, "Customer") ||
                (mIsDOCtNeedShippingAddressInd && Sm.IsTxtEmpty(TxtSAName, "Shipping address", false)) ||
                (mIsUseProductionWorkGroup && Sm.IsLueEmpty(LueProductionWorkGroup, "Group")) ||
                Sm.IsClosingJournalInvalid(Sm.GetDte(DteDocDt)) ||
                IsGrdEmpty() ||
                IsGrdExceedMaxRecords() ||
                IsGrdValueNotValid() ||
                IsItemInvalid() ||
                Sm.IsDocDtNotValid(
                    Sm.CompareStr(Sm.GetParameter("InventoryDocDtValidInd"), "Y"),
                    Sm.GetDte(DteDocDt));
                
        }

        private bool IsItemInvalid()
        {
            if (ChkProgressInd.Checked) return false;

            if (!ChkServiceInd.Checked)
            {
                string Msg = string.Empty;
                for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
                {
                    Msg =
                       "Item's Code : " + Sm.GetGrdStr(Grd1, i, 4) + Environment.NewLine +
                       "Local Code : " + Sm.GetGrdStr(Grd1, i, 6) + Environment.NewLine +
                       "Item's Name : " + Sm.GetGrdStr(Grd1, i, 7) + Environment.NewLine +
                       "Property : " + Sm.GetGrdStr(Grd1, i, 9) + Environment.NewLine +
                       "Batch# : " + Sm.GetGrdStr(Grd1, i, 10) + Environment.NewLine +
                       "Source : " + Sm.GetGrdStr(Grd1, i, 11) + Environment.NewLine +
                       "Lot : " + Sm.GetGrdStr(Grd1, i, 12) + Environment.NewLine +
                       "Bin : " + Sm.GetGrdStr(Grd1, i, 13) + Environment.NewLine + Environment.NewLine;

                    if (Sm.GetGrdStr(Grd1, i, 11).Length == 0) // kalau source nya kosong, harus disimpan sebagai service (chkService harus di checked) karena item ini bukan dari stock
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "You need to save this document as a service document, as for this item is not from stock.");
                        Sm.FocusGrd(Grd1, i, 11);
                        return true;
                    }
                }
            }

            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 item.");
                return true;
            }
            return false;
        }

        private bool IsGrdExceedMaxRecords()
        {

            if (Grd1.Rows.Count > 1000)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "DO data entered (" + (Grd1.Rows.Count - 1).ToString() + ") exceeds the maximum limit (999).");
                return true;
            }
            return false;
        }

        private bool IsGrdValueNotValid()
        {
            string Msg = string.Empty;
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                if (Sm.IsGrdValueEmpty(Grd1, Row, 4, false, "Item is empty.")) return true;

                Msg =
                   "Item's Code : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine +
                   "Local Code : " + Sm.GetGrdStr(Grd1, Row, 6) + Environment.NewLine +
                   "Item's Name : " + Sm.GetGrdStr(Grd1, Row, 7) + Environment.NewLine +
                   "Property : " + Sm.GetGrdStr(Grd1, Row, 9) + Environment.NewLine +
                   "Batch# : " + Sm.GetGrdStr(Grd1, Row, 10) + Environment.NewLine +
                   "Source : " + Sm.GetGrdStr(Grd1, Row, 11) + Environment.NewLine +
                   "Lot : " + Sm.GetGrdStr(Grd1, Row, 12) + Environment.NewLine +
                   "Bin : " + Sm.GetGrdStr(Grd1, Row, 13) + Environment.NewLine + Environment.NewLine;

                if (Sm.GetGrdDec(Grd1, Row, 15) <= 0m)
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Quantity should be greater than 0.");
                    return true;
                }

                if ((Sm.GetGrdDec(Grd1, Row, 14) < Sm.GetGrdDec(Grd1, Row, 15)) && ChkProgressInd.Checked)
                {
                    Sm.StdMsg(mMsgType.Warning, Msg + "Stock should not be less than DO's quantity.");
                    return true;
                }

                if (Grd1.Cols[18].Visible)
                {
                    if (Sm.GetGrdDec(Grd1, Row, 18) <= 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Quantity (2) should be greater than 0.");
                        return true;
                    }

                    if ((Sm.GetGrdDec(Grd1, Row, 17) < Sm.GetGrdDec(Grd1, Row, 18)) && ChkProgressInd.Checked)
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Stock (2) should not be less than DO's quantity (2).");
                        return true;
                    }
                }

                if (Grd1.Cols[21].Visible)
                {
                    if (Sm.GetGrdDec(Grd1, Row, 21) <= 0m)
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Quantity (3) should be greater than 0.");
                        return true;
                    }
                    if ((Sm.GetGrdDec(Grd1, Row, 20) < Sm.GetGrdDec(Grd1, Row, 21)) && ChkProgressInd.Checked)
                    {
                        Sm.StdMsg(mMsgType.Warning, Msg + "Stock (3) should not be less than DO's quantity (3).");
                        return true;
                    }
                }
            }
            return false;
        }

        private MySqlCommand SaveDOCtHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblDOCtHdr ");
            SQL.AppendLine("(DocNo, DocDt, Status, ProcessInd, WhsCode, CtCode, DocNoInternal, ResiNo, CurCode, SAName, SAAddress, SACityCode, SACntCode, SAPostalCd, SAPhone, SAFax, SAEmail, SAMobile, ");
            SQL.AppendLine("KBContractNo, KBContractDt, KBPLNo, KBPLDt, KBRegistrationNo, KBRegistrationDt, KBSubmissionNo, CustomsDocCode, ");
            SQL.AppendLine("Driver, Expedition, VehicleRegNo, ProductionWorkGroup, ");
            if (mIsDOCtUseSOContract)
                SQL.AppendLine("SOContractDocNo, ServiceInd, ProgressInd, ");
            SQL.AppendLine("Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @Status, @ProcessInd, @WhsCode, @CtCode, @DocNoInternal, @ResiNo, @CurCode, @SAName, @SAAddress, @SACityCode, @SACntCode, @SAPostalCd, @SAPhone, @SAFax, @SAEmail, @SAMobile, ");
            SQL.AppendLine("@KBContractNo, @KBContractDt, @KBPLNo, @KBPLDt, @KBRegistrationNo, @KBRegistrationDt, @KBSubmissionNo, @CustomsDocCode, ");
            SQL.AppendLine("@Driver, @Expedition, @VehicleRegNo, @ProductionWorkGroup, ");
            if (mIsDOCtUseSOContract)
                SQL.AppendLine("@SOContractDocNo, @ServiceInd, @ProgressInd, ");
            SQL.AppendLine("@Remark, @UserCode, CurrentDateTime()); ");

            if (mIsDOCtApprovalActived)
            {
                SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
                SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @UserCode, CurrentDateTime() ");
                SQL.AppendLine("From TblDocApprovalSetting T ");
                SQL.AppendLine("Where T.DocType='DOCt' ");
                SQL.AppendLine("And (T.StartAmt=0.00 Or T.StartAmt<=@Amt) ");
                SQL.AppendLine("And Exists (Select 1 From TblDOCtHdr Where DocNo = @DocNo And ProcessInd = 'F'); ");

                SQL.AppendLine("Update TblDOCtHdr Set Status='A' ");
                SQL.AppendLine("Where DocNo=@DocNo ");
                SQL.AppendLine("And Not Exists( ");
                SQL.AppendLine("    Select 1 From TblDocApproval ");
                SQL.AppendLine("    Where DocType='DOCt' ");
                SQL.AppendLine("    And DocNo=@DocNo ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine("And Exists (Select 1 From TblDOCtHdr Where DocNo = @DocNo And ProcessInd = 'F'); ");
            }

            var cm = new MySqlCommand()
            { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@Status", mIsDOCtApprovalActived?"O":"A");
            Sm.CmParam<String>(ref cm, "@ProcessInd", mIsDOCtUseProcessInd ? "D" : "F");
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            Sm.CmParam<String>(ref cm, "@CtCode", Sm.GetLue(LueCtCode));
            Sm.CmParam<String>(ref cm, "@DocNoInternal", TxtDocNoInternal.Text);
            Sm.CmParam<String>(ref cm, "@ResiNo", TxtResiNo.Text);
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<String>(ref cm, "@SAName", TxtSAName.Text);
            Sm.CmParam<String>(ref cm, "@SAAddress", MeeSAAddress.Text);
            Sm.CmParam<String>(ref cm, "@SACityCode", mCityCode);
            Sm.CmParam<String>(ref cm, "@SACntCode", mCntCode);
            Sm.CmParam<String>(ref cm, "@SAPostalCd", TxtPostalCd.Text);
            Sm.CmParam<String>(ref cm, "@SAPhone", TxtPhone.Text);
            Sm.CmParam<String>(ref cm, "@SAFax", TxtFax.Text);
            Sm.CmParam<String>(ref cm, "@SAEmail", TxtEmail.Text);
            Sm.CmParam<String>(ref cm, "@SAMobile", TxtMobile.Text);
            Sm.CmParam<String>(ref cm, "@KBContractNo", TxtKBContractNo.Text);
            Sm.CmParamDt(ref cm, "@KBContractDt", Sm.GetDte(DteKBContractDt));
            Sm.CmParam<String>(ref cm, "@KBPLNo", TxtKBPLNo.Text);
            Sm.CmParamDt(ref cm, "@KBPLDt", Sm.GetDte(DteKBPLDt));
            Sm.CmParam<String>(ref cm, "@KBRegistrationNo", TxtKBRegistrationNo.Text);
            Sm.CmParamDt(ref cm, "@KBRegistrationDt", Sm.GetDte(DteKBRegistrationDt));
            Sm.CmParam<String>(ref cm, "@KBSubmissionNo", TxtKBSubmissionNo.Text);
            Sm.CmParam<String>(ref cm, "@CustomsDocCode", mCustomsDocCode);
            Sm.CmParam<String>(ref cm, "@Driver", TxtDriver.Text);
            Sm.CmParam<String>(ref cm, "@Expedition", TxtExpedition.Text);
            Sm.CmParam<String>(ref cm, "@VehicleRegNo", TxtVehicleRegNo.Text);
            Sm.CmParam<String>(ref cm, "@ProductionWorkGroup", Sm.GetLue(LueProductionWorkGroup));
            if (mIsDOCtUseSOContract)
            {
                Sm.CmParam<String>(ref cm, "@SOContractDocNo", TxtSOContractDocNo.Text);
                Sm.CmParam<String>(ref cm, "@ServiceInd", ChkServiceInd.Checked ? "Y" : "N");
                Sm.CmParam<String>(ref cm, "@ProgressInd", ChkProgressInd.Checked ? "Y" : "N");
            }
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }

        private MySqlCommand SaveDOCtDtl(string DocNo, int Row)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblDOCtDtl(DocNo, DNo, CancelInd, ItCode, PropCode, BatchNo, ProcessInd, Source, Lot, Bin, QtyPackagingUnit, PackagingUnitUomCode, Qty, Qty2, Qty3, UPrice, SOContractDNo, Notes, NotesDetail, TransferRequestProjectDocNo, TransferRequestProjectDNo, Remark, CreateBy, CreateDt) " +
                    "Values(@DocNo, @DNo, 'N', @ItCode, @PropCode, @BatchNo, @ProcessInd, @Source, @Lot, @Bin, @QtyPackagingUnit, @PackagingUnitUomCode, @Qty, @Qty2, @Qty3, @UPrice, @SOContractDNo, @Notes, @NotesDetail, @TransferRequestProjectDocNo, @TransferRequestProjectDNo, @Remark, @CreateBy, CurrentDateTime()) "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@ItCode", Sm.GetGrdStr(Grd1, Row, 4));
            Sm.CmParam<String>(ref cm, "@PropCode", Sm.GetGrdStr(Grd1, Row, 8).Length > 0 ? Sm.GetGrdStr(Grd1, Row, 8) : "-");
            Sm.CmParam<String>(ref cm, "@BatchNo", Sm.GetGrdStr(Grd1, Row, 10).Length > 0 ? Sm.GetGrdStr(Grd1, Row, 10) : "-");
            Sm.CmParam<String>(ref cm, "@Source", Sm.GetGrdStr(Grd1, Row, 11).Length > 0 ? Sm.GetGrdStr(Grd1, Row, 11) : "-");
            Sm.CmParam<String>(ref cm, "@Lot", Sm.GetGrdStr(Grd1, Row, 12).Length > 0 ? Sm.GetGrdStr(Grd1, Row, 12) : "-");
            Sm.CmParam<String>(ref cm, "@Bin", Sm.GetGrdStr(Grd1, Row, 13).Length > 0 ? Sm.GetGrdStr(Grd1, Row, 13) : "-");
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 15));
            Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd1, Row, 18));
            Sm.CmParam<Decimal>(ref cm, "@Qty3", Sm.GetGrdDec(Grd1, Row, 21));
            Sm.CmParam<Decimal>(ref cm, "@UPrice", Sm.GetGrdDec(Grd1, Row, 23));
            Sm.CmParam<String>(ref cm, "@Remark", Sm.GetGrdStr(Grd1, Row, 25));
            Sm.CmParam<String>(ref cm, "@Notes", Sm.GetGrdStr(Grd1, Row, 38));
            Sm.CmParam<String>(ref cm, "@NotesDetail", Sm.GetGrdStr(Grd1, Row, 39));
            Sm.CmParam<String>(ref cm, "@SOContractDNo", Sm.GetGrdStr(Grd1, Row, 34));
            Sm.CmParam<Decimal>(ref cm, "@QtyPackagingUnit", Sm.GetGrdDec(Grd1, Row, 27));
            Sm.CmParam<String>(ref cm, "@PackagingUnitUomCode", Sm.GetGrdStr(Grd1, Row, 28));
            Sm.CmParam<String>(ref cm, "@TransferRequestProjectDocNo", Sm.GetGrdStr(Grd1, Row, 41));
            Sm.CmParam<String>(ref cm, "@TransferRequestProjectDNo", Sm.GetGrdStr(Grd1, Row, 42));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            if (mIsDOCtProsessToSalesInvoice)
            {
                Sm.CmParam<String>(ref cm, "@ProcessInd", "O");
            }
            else
            {
                Sm.CmParam<String>(ref cm, "@ProcessInd", "F");
            }
            return cm;
        }

        private MySqlCommand SaveStock(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblStockMovement(DocType, DocNo, DNo, CancelInd, DocDt, WhsCode, Lot, Bin, ItCode, PropCode, BatchNo, Source, Qty, Qty2, Qty3, Remark, ");
            if (mIsKawasanBerikatEnabled)
                SQL.AppendLine("CustomsDocCode, KBContractNo, KBSubmissionNo, KBRegistrationNo, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Select @DocType, A.DocNo, B.DNo, 'N', A.DocDt, A.WhsCode, B.Lot, B.Bin, B.ItCode, B.PropCode, B.BatchNo, B.Source, -1*B.Qty, -1*B.Qty2, -1*B.Qty3, ");
            SQL.AppendLine("Case When A.Remark Is Null Then ");
            SQL.AppendLine("    Case When B.Remark Is Null Then Null Else B.Remark End ");
            SQL.AppendLine("Else ");
            SQL.AppendLine("    Case When B.Remark Is Null Then A.Remark Else Concat(A.Remark, ' ( ', B.Remark, ' )') End ");
            SQL.AppendLine("End As Remark, ");
            if (mIsKawasanBerikatEnabled)
                SQL.AppendLine("A.CustomsDocCode, A.KBContractNo, A.KBSubmissionNo, A.KBRegistrationNo, ");
            SQL.AppendLine("@UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblDOCtHdr A ");
            SQL.AppendLine("Inner Join TblDOCtDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("And A.Status='A' ");
            SQL.AppendLine("And A.ProcessInd = 'F' ");
            SQL.AppendLine("And B.CancelInd = 'N'; ");

            SQL.AppendLine("Update TblStockSummary As T1 ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select A.WhsCode, A.Lot, A.Bin, A.Source, ");
            SQL.AppendLine("    Sum(A.Qty) As Qty, Sum(A.Qty2) As Qty2, Sum(A.Qty3) As Qty3 ");
            SQL.AppendLine("    From TblStockMovement A ");
            SQL.AppendLine("    Inner Join ( ");
            SQL.AppendLine("        Select Distinct WhsCode, Lot, Bin, Source ");
            SQL.AppendLine("        From TblStockMovement ");
            SQL.AppendLine("        Where DocType=@DocType ");
            SQL.AppendLine("        And DocNo=@DocNo ");
            SQL.AppendLine("        And CancelInd='N' ");
            SQL.AppendLine("    ) B ");
            SQL.AppendLine("        On A.WhsCode=B.WhsCode ");
            SQL.AppendLine("        And A.Lot=B.Lot ");
            SQL.AppendLine("        And A.Bin=B.Bin ");
            SQL.AppendLine("        And A.Source=B.Source ");
            SQL.AppendLine("    Where (A.Qty<>0) ");
            SQL.AppendLine("    Group By A.WhsCode, A.Lot, A.Bin, A.Source ");
            SQL.AppendLine(") T2 ");
            SQL.AppendLine("    On T1.WhsCode=T2.WhsCode ");
            SQL.AppendLine("    And T1.Lot=T2.Lot ");
            SQL.AppendLine("    And T1.Bin=T2.Bin ");
            SQL.AppendLine("    And T1.Source=T2.Source ");
            SQL.AppendLine("Set ");
            SQL.AppendLine("    T1.Qty=IfNull(T2.Qty, 0), ");
            SQL.AppendLine("    T1.Qty2=IfNull(T2.Qty2, 0), ");
            SQL.AppendLine("    T1.Qty3=IfNull(T2.Qty3, 0), ");
            SQL.AppendLine("    T1.LastUpBy=@UserCode, ");
            SQL.AppendLine("    T1.LastUpDt=CurrentDateTime(); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DocType", mDocType);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblDOCtHdr Set ");
            SQL.AppendLine("    JournalDocNo=");
            if (mDocNoFormat == "1")
                SQL.AppendLine(Sm.GetNewJournalDocNo(Sm.GetDte(DteDocDt), 1));
            else
                SQL.AppendLine(Sm.GetNewJournalDocNo3(Sm.GetDte(DteDocDt), mEntCode, "1" ));
            //else
            SQL.AppendLine(" Where DocNo=@DocNo And Status='A'; ");

            SQL.AppendLine("Insert Into TblJournalHdr ");
            SQL.AppendLine("(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.JournalDocNo, ");
            SQL.AppendLine("A.DocDt, ");
            SQL.AppendLine("Concat('DO To Customer : ', A.DocNo) As JnDesc, ");
            SQL.AppendLine("@MenuCode As MenuCode, ");
            SQL.AppendLine("(Select MenuDesc From TblMenu Where MenuCode=@MenuCode) As MenuDesc, ");
            SQL.AppendLine("B.CCCode, A.Remark, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblDOCtHdr A ");
            SQL.AppendLine("Left Join TblWarehouse B On A.WhsCode=B.WhsCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo ");
            SQL.AppendLine("And A.Status='A' ");
            SQL.AppendLine("And A.ProcessInd = 'F'; ");

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, SOContractDocNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, @EntCode, @SOContractDocNo, A.CreateBy, A.CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From (");

            if (mIsAcNoForSaleUseItemCategory)
            {
                SQL.AppendLine("        Select E.AcNo5 As AcNo, ");
                if (mIsDOCtAmtRounded)
                {
                    if (ChkProgressInd.Checked || ChkServiceInd.Checked)
                        SQL.AppendLine("        Sum(Floor(If(B.UPrice != C.UPriceAfTax, If(B.UPrice = 0.00, C.UPriceAfTax, B.UPrice), C.UPriceAfTax) * B.Qty)) As DAmt, ");
                    else
                        SQL.AppendLine("        Sum(Floor(IfNull(C.UPrice, 0)*IfNull(C.ExcRate, 0)*B.Qty)) As DAmt, ");
                }
                else
                {
                    if (ChkProgressInd.Checked || ChkServiceInd.Checked)
                        SQL.AppendLine("        Sum(If(B.UPrice != C.UPriceAfTax, If(B.UPrice = 0.00, C.UPriceAfTax, B.UPrice), C.UPriceAfTax) * B.Qty) As DAmt, ");
                    else
                        SQL.AppendLine("        Sum(IfNull(C.UPrice, 0)*IfNull(C.ExcRate, 0)*B.Qty) As DAmt, ");
                }
                SQL.AppendLine("        0.00 As CAmt ");
                SQL.AppendLine("        From TblDOCtHdr A ");
                SQL.AppendLine("        Inner Join TblDOCtDtl B On A.DocNo=B.DocNo And B.CancelInd = 'N' ");
                if (ChkProgressInd.Checked || ChkServiceInd.Checked)
                    SQL.AppendLine("        Inner join TblSOContractDtl C On A.SOContractDocNo = C.DocNo And B.SOContractDNo = C.DNo ");
                else
                    SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblItem D On B.ItCode=D.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo5 Is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("        Group By E.AcNo5 ");
            }
            else
            {
                SQL.AppendLine("        Select D.ParValue As AcNo, ");
                if (mIsDOCtAmtRounded)
                {
                    if (ChkProgressInd.Checked || ChkServiceInd.Checked)
                        SQL.AppendLine("        Floor(If(B.UPrice != C.UPriceAfTax, If(B.UPrice = 0.00, C.UPriceAfTax, B.UPrice), C.UPriceAfTax) * B.Qty) As DAmt, ");
                    else
                        SQL.AppendLine("        Floor(IfNull(C.UPrice, 0)*IfNull(C.ExcRate, 0)*B.Qty) As DAmt, ");
                }
                else
                {
                    if (ChkProgressInd.Checked || ChkServiceInd.Checked)
                        SQL.AppendLine("        If(B.UPrice != C.UPriceAfTax, If(B.UPrice = 0.00, C.UPriceAfTax, B.UPrice), C.UPriceAfTax) * B.Qty As DAmt, ");
                    else
                        SQL.AppendLine("        IfNull(C.UPrice, 0)*IfNull(C.ExcRate, 0)*B.Qty As DAmt, ");
                }
                SQL.AppendLine("        0.00 As CAmt ");
                SQL.AppendLine("        From TblDOCtHdr A ");
                SQL.AppendLine("        Inner Join TblDOCtDtl B On A.DocNo=B.DocNo And B.CancelInd = 'N' ");
                if (ChkProgressInd.Checked || ChkServiceInd.Checked)
                    SQL.AppendLine("        Inner join TblSOContractDtl C On A.SOContractDocNo = C.DocNo And B.SOContractDNo = C.DNo ");
                else
                    SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblParameter D On D.ParCode='AcNoForCOGS' And D.ParValue Is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
            }

            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select E.AcNo, ");
            SQL.AppendLine("        0.00 As DAmt, ");
            if (mIsDOCtAmtRounded)
            {
                if (ChkProgressInd.Checked || ChkServiceInd.Checked)
                    SQL.AppendLine("        Floor(If(B.UPrice != C.UPriceAfTax, If(B.UPrice = 0.00, C.UPriceAfTax, B.UPrice), C.UPriceAfTax) * B.Qty) As CAmt ");
                else
                    SQL.AppendLine("        Floor(IfNull(C.UPrice, 0)*IfNull(C.ExcRate, 0)*B.Qty) As CAmt ");
            }
            else
            {
                if (ChkProgressInd.Checked || ChkServiceInd.Checked)
                    SQL.AppendLine("        If(B.UPrice != C.UPriceAfTax, If(B.UPrice = 0.00, C.UPriceAfTax, B.UPrice), C.UPriceAfTax) * B.Qty As CAmt ");
                else
                    SQL.AppendLine("        IfNull(C.UPrice, 0)*IfNull(C.ExcRate, 0)*B.Qty As CAmt ");
            }
            SQL.AppendLine("        From TblDOCtHdr A ");
            SQL.AppendLine("        Inner Join TblDOCtDtl B On A.DocNo=B.DocNo And B.CancelInd = 'N' ");
            if (ChkProgressInd.Checked || ChkServiceInd.Checked)
                SQL.AppendLine("        Inner join TblSOContractDtl C On A.SOContractDocNo = C.DocNo And B.SOContractDNo = C.DNo ");
            else
                SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
            SQL.AppendLine("        Inner Join TblItem D On B.ItCode=D.ItCode ");
            SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo Is Not Null ");
            SQL.AppendLine("        Where A.DocNo=@DocNo ");
            
            SQL.AppendLine("        Union All ");
            SQL.AppendLine("        Select Concat(X2.ParValue, X3.CtCode) As AcNo, ");
            if (mIsDOCtAmtRounded)
            {
                SQL.AppendLine("        Sum(Floor(X1.Amt)) As DAmt, ");
            }
            else
            {
                SQL.AppendLine("        Sum(X1.Amt) As DAmt, ");
            }
            SQL.AppendLine("        0.00 As CAmt ");
            SQL.AppendLine("        From ( ");
            SQL.AppendLine("            Select Case When @MainCurCode=A.CurCode Then 1.00 Else IfNull(C.Amt, 0) End * ");
            SQL.AppendLine("            IfNull(B.UPrice, 0)*B.Qty As Amt ");
            SQL.AppendLine("            From TblDOCtHdr A ");
            SQL.AppendLine("            Inner Join TblDOCtDtl B On A.DocNo=B.DocNo And B.CancelInd = 'N' ");
            SQL.AppendLine("            Left Join ( ");
            SQL.AppendLine("                Select Amt From TblCurrencyRate ");
            SQL.AppendLine("                Where RateDt<=@DocDt And CurCode1=@CurCode And CurCode2=@MainCurCode ");
            SQL.AppendLine("                Order By RateDt Desc Limit 1 ");
            SQL.AppendLine("            ) C On 0=0 ");
            SQL.AppendLine("            Where A.DocNo=@DocNo ");
            SQL.AppendLine("        ) X1 ");
            SQL.AppendLine("        Inner Join TblParameter X2 On X2.ParCode='CustomerAcNoNonInvoice' And X2.ParValue Is Not Null ");
            SQL.AppendLine("        Inner Join TblDOCtHdr X3 On X3.DocNo=@DocNo ");
            SQL.AppendLine("        Group By X2.ParValue, X3.CtCode ");
            SQL.AppendLine("        Union All ");

            if (mIsAcNoForSaleUseItemCategory)
            {
                SQL.AppendLine("        Select X.AcNo, ");
                SQL.AppendLine("        0.00 As DAmt, ");
                if (mIsDOCtAmtRounded)
                    SQL.AppendLine("        Sum(Floor(X.Amt)) As CAmt ");
                else
                    SQL.AppendLine("        Sum(X.Amt) As CAmt ");
                SQL.AppendLine("        From ( ");
                SQL.AppendLine("            Select E.AcNo4 As AcNo, ");
                SQL.AppendLine("            Case When @MainCurCode=A.CurCode Then 1.00 ");
                SQL.AppendLine("            Else IfNull(C.Amt, 0) End * IfNull(B.UPrice, 0)*B.Qty ");
                SQL.AppendLine("            As Amt ");
                SQL.AppendLine("            From TblDOCtHdr A ");
                SQL.AppendLine("            Inner Join TblDOCtDtl B On A.DocNo=B.DocNo And B.CancelInd = 'N' ");
                SQL.AppendLine("            Left Join ( ");
                SQL.AppendLine("                Select Amt From TblCurrencyRate ");
                SQL.AppendLine("                Where RateDt<=@DocDt And CurCode1=@CurCode And CurCode2=@MainCurCode ");
                SQL.AppendLine("                Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("            ) C On 0=0 ");
                SQL.AppendLine("            Inner Join TblItem D On B.ItCode=D.ItCode ");
                SQL.AppendLine("            Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo4 Is Not Null ");
                SQL.AppendLine("            Where A.DocNo=@DocNo ");
                SQL.AppendLine("        ) X ");
                SQL.AppendLine("        Group By X.AcNo ");
            }
            else
            {
                SQL.AppendLine("        Select X2.ParValue As AcNo, ");
                SQL.AppendLine("        0.00 As DAmt, ");
                if (mIsDOCtAmtRounded)
                    SQL.AppendLine("        Sum(Floor(X1.Amt)) As CAmt ");
                else
                    SQL.AppendLine("        Sum(X1.Amt) As CAmt ");
                SQL.AppendLine("        From ( ");
                SQL.AppendLine("            Select Case When @MainCurCode=A.CurCode Then 1.00 Else IfNull(C.Amt, 0) End * ");
                SQL.AppendLine("            IfNull(B.UPrice, 0)*B.Qty As Amt ");
                SQL.AppendLine("            From TblDOCtHdr A ");
                SQL.AppendLine("            Inner Join TblDOCtDtl B On A.DocNo=B.DocNo And B.CancelInd = 'N' ");
                SQL.AppendLine("            Left Join ( ");
                SQL.AppendLine("                Select Amt From TblCurrencyRate ");
                SQL.AppendLine("                Where RateDt<=@DocDt And CurCode1=@CurCode And CurCode2=@MainCurCode ");
                SQL.AppendLine("                Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("            ) C On 1=1 ");
                SQL.AppendLine("            Where A.DocNo=@DocNo ");
                SQL.AppendLine("        ) X1 ");
                SQL.AppendLine("        Inner Join TblParameter X2 On X2.ParCode='AcNoForSaleOfFinishedGoods' And X2.ParValue Is Not Null ");
                SQL.AppendLine("        Group By X2.ParValue ");
            }

            SQL.AppendLine("    ) Tbl Group By AcNo ");
            SQL.AppendLine(") B On 1=1 ");
            SQL.AppendLine("Where A.DocNo In ( ");
            SQL.AppendLine("    Select JournalDocNo ");
            SQL.AppendLine("    From TblDOCtHdr ");
            SQL.AppendLine("    Where DocNo=@DocNo ");
            SQL.AppendLine("    And JournalDocNo Is Not Null ");
            SQL.AppendLine("    ); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@MainCurCode", mMainCurCode);
            Sm.CmParam<String>(ref cm, "@SOContractDocNo", TxtSOContractDocNo.Text);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<String>(ref cm, "@EntCode", Sm.GetEntityWarehouse(Sm.GetLue(LueWhsCode)));

            return cm;
        }

        #endregion

        #region Edit Data

        private void EditData()
        {
            UpdateCancelledItem();

            string mInitProcessInd = Sm.GetValue("Select ProcessInd From TblDOCtHdr Where DocNo = @Param;", TxtDocNo.Text);
            string DNo = "##XXX##";

            for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                if (Sm.GetGrdBool(Grd1, Row, 1) && !Sm.GetGrdBool(Grd1, Row, 2) && Sm.GetGrdStr(Grd1, Row, 4).Length > 0)
                    DNo += "##" + Sm.GetGrdStr(Grd1, Row, 0) + "##";

            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || 
                IsEditedDataNotValid(DNo)) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            if (mIsDOCtUseProcessInd)
            {
                for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
                {
                    cml.Add(EditDOCtDtlQty(i));

                    if (Sm.GetGrdStr(Grd1, i, 4).Length > 0 && !Sm.GetGrdBool(Grd1, i, 2))
                    {
                        if (Sm.GetGrdBool(Grd1, i, 1))
                            cml.Add(EditDOCtDtl1(i));
                        else
                            cml.Add(EditDOCtDtl2(i));
                    }
                }                

                if (mInitProcessInd != Sm.GetLue(LueProcessInd) && mInitProcessInd == "D")
                {
                    cml.Add(EditDOCtHdr());

                    if (!ChkServiceInd.Checked && !ChkProgressInd.Checked) cml.Add(SaveStock(TxtDocNo.Text));

                    if (mIsAutoJournalActived && !ChkProgressInd.Checked) cml.Add(SaveJournal(TxtDocNo.Text));
                }

                if (mIsDOCtApprovalActived) cml.Add(UpdateDOCtStatus(TxtDocNo.Text));

                if (mIsAutoJournalActived && !ChkProgressInd.Checked) cml.Add(SaveJournal());
            }
            else
            {
                if (mIsDOCtRemarkEditable) cml.Add(EditDOCtHdr());

                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 4).Length > 0 && !Sm.GetGrdBool(Grd1, Row, 2))
                    {
                        if (Sm.GetGrdBool(Grd1, Row, 1))
                            cml.Add(EditDOCtDtl1(Row));
                        else
                            cml.Add(EditDOCtDtl2(Row));
                    }
                }

                if (mIsDOCtApprovalActived) cml.Add(UpdateDOCtStatus(TxtDocNo.Text));

                if (mIsAutoJournalActived && !ChkProgressInd.Checked) cml.Add(SaveJournal());
            }

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private void UpdateCancelledItem()
        {
            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand()
                {
                    Connection = cn,
                    CommandText =
                        "Select DNo, CancelInd From TblDOCtDtl " +
                        "Where DocNo=@DocNo Order By DNo;"
                };
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { "DNo", "CancelInd" });

                if (dr.HasRows)
                {
                    Grd1.ProcessTab = true;
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                        {
                            if (Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 0), Sm.DrStr(dr, 0)))
                            {
                                if (Sm.CompareStr(Sm.DrStr(dr, 1), "Y"))
                                {
                                    Sm.SetGrdValue("B", Grd1, dr, c, Row, 1, 1);
                                    Sm.SetGrdValue("B", Grd1, dr, c, Row, 2, 1);
                                }
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        private bool IsEditedDataNotValid(string DNo)
        {
            if (mIsDOCtPriceEditable) return false;
            return
                (mIsDOCtUseProcessInd && Sm.IsLueEmpty(LueProcessInd, "Process")) ||
                Sm.IsClosingJournalInvalid(mIsAutoJournalActived, true, Sm.GetDte(DteDocDt)) ||
                IsDOCtStatusCancelled() ||
                IsCancelledItemNotExisted(DNo) || 
                IsCancelledDOCtInvalid() ||
                IsDataAlreadyUsedInRecvCt(DNo) || IsDataAlreadyUsedInSalesInvoice(DNo);
        }

        private bool IsCancelledDOCtInvalid()
        {
            if (!mIsDOCtApprovalActived) return false;

            bool IsCancelled = Sm.GetGrdBool(Grd1, 0, 1);
            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdStr(Grd1, r, 4).Length > 0)
                {
                    if (!Sm.GetGrdBool(Grd1, r, 1) != IsCancelled)
                    {
                        Sm.StdMsg(mMsgType.Warning, "You need to cancel ALL items.");
                        return true;
                    }
                }
            }
            return false;
        }

        private bool IsDataAlreadyUsedInRecvCt(string DNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select DNo From TblRecvCtDtl ");
            SQL.AppendLine("Where DOCtDocNo=@DocNo ");
            SQL.AppendLine("And CancelInd='N' ");
            SQL.AppendLine("And Position(Concat('##', DOCtDNo, '##') In @DNo)>0 Limit 1; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DNo", DNo);

            var key = Sm.GetValue(cm);
            if (key.Length != 0)
            {
                string Msg = string.Empty;
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                {
                    if (Sm.GetGrdStr(Grd1, Row, 0) == key)
                    {
                        Msg =
                           "Item's Code : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine +
                           "Local Code : " + Sm.GetGrdStr(Grd1, Row, 6) + Environment.NewLine +
                           "Item's Name : " + Sm.GetGrdStr(Grd1, Row, 7) + Environment.NewLine +
                           "Property : " + Sm.GetGrdStr(Grd1, Row, 9) + Environment.NewLine +
                           "Batch# : " + Sm.GetGrdStr(Grd1, Row, 10) + Environment.NewLine +
                           "Source : " + Sm.GetGrdStr(Grd1, Row, 11) + Environment.NewLine +
                           "Lot : " + Sm.GetGrdStr(Grd1, Row, 12) + Environment.NewLine +
                           "Bin : " + Sm.GetGrdStr(Grd1, Row, 13) + Environment.NewLine + Environment.NewLine;
                        break;
                    }
                }
                Sm.StdMsg(mMsgType.Warning, Msg + Environment.NewLine + "Data already used in receiving from customer transactions.");
                return true;
            }
            return false;
        }

        private bool IsDataAlreadyUsedInSalesInvoice(string DNo)
        {
            if (mIsDOCtProsessToSalesInvoice)
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select B.DOCtDno As KeyWord ");
                SQL.AppendLine("from tblSalesinvoiceHdr A ");
                SQL.AppendLine("Inner Join TblsalesinvoiceDtl B On A.DocNo = B.Docno ");
                SQL.AppendLine("Where A.cancelInd = 'N' ");
                SQL.AppendLine("And B.DOCtDocNo=@DocNo ");
                SQL.AppendLine("And CancelInd='N' ");
                SQL.AppendLine("And Position(Concat('##', DOCtDNo, '##') In @DNo)>0 Limit 1; ");

                var cm = new MySqlCommand() { CommandText = SQL.ToString() };
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cm, "@DNo", DNo);

                var key = Sm.GetValue(cm);
                if (key.Length != 0)
                {
                    string Msg = string.Empty;
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    {
                        if (Sm.GetGrdStr(Grd1, Row, 0) == key)
                        {
                            Msg =
                               "Item Code : " + Sm.GetGrdStr(Grd1, Row, 4) + Environment.NewLine +
                               "Item's Local Code : " + Sm.GetGrdStr(Grd1, Row, 6) + Environment.NewLine +
                               "Item Name : " + Sm.GetGrdStr(Grd1, Row, 7) + Environment.NewLine +
                               "Property : " + Sm.GetGrdStr(Grd1, Row, 9) + Environment.NewLine +
                               "Batch# : " + Sm.GetGrdStr(Grd1, Row, 10) + Environment.NewLine +
                               "Source : " + Sm.GetGrdStr(Grd1, Row, 11) + Environment.NewLine +
                               "Lot : " + Sm.GetGrdStr(Grd1, Row, 12) + Environment.NewLine +
                               "Bin : " + Sm.GetGrdStr(Grd1, Row, 13) + Environment.NewLine + Environment.NewLine;
                            break;
                        }
                    }
                    Sm.StdMsg(mMsgType.Warning, Msg + Environment.NewLine + "Data already used in sales invoice transactions.");
                    return true;
                }
            }
            return false;

        }

        private bool IsCancelledItemNotExisted(string DNo)
        {
            if (mIsDOCtUseProcessInd) return false;

            if (Sm.CompareStr(DNo, "##XXX##")&&!mIsDOCtRemarkEditable)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel minimum 1 item.");
                return true;
            }
            return false;
        }

        private MySqlCommand EditDOCtDtlQty(int Row)
        {
            string sSQL = string.Empty;

            sSQL += "Update TblDOCtDtl ";
            sSQL += "Set Qty = @Qty, Qty2 = @Qty2, Qty3 = @Qty3, UPrice = @UPrice, ";
            sSQL += "LastUpBy = @UserCode, LastUpDt = CurrentDateTime() ";
            sSQL += "Where DocNo = @DocNo "; 
            sSQL += "And DNo = @DNo ";
            sSQL += "And CancelInd = 'N'; ";

            var cm = new MySqlCommand() { CommandText = sSQL };

            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.GetGrdStr(Grd1, Row, 0));
            Sm.CmParam<Decimal>(ref cm, "@Qty", Sm.GetGrdDec(Grd1, Row, 15));
            Sm.CmParam<Decimal>(ref cm, "@Qty2", Sm.GetGrdDec(Grd1, Row, 18));
            Sm.CmParam<Decimal>(ref cm, "@Qty3", Sm.GetGrdDec(Grd1, Row, 21));
            Sm.CmParam<Decimal>(ref cm, "@UPrice", Sm.GetGrdDec(Grd1, Row, 23));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand EditDOCtDtl1(int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblDOCtDtl Set CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And DNo=@DNo ");
            SQL.AppendLine("And CancelInd='N'; ");

            SQL.AppendLine("Insert Into TblStockMovement ");
            SQL.AppendLine("(DocType, DocNo, DNo, DocDt, Source, CancelInd, Source2, ");
            SQL.AppendLine("WhsCode, Lot, Bin, ItCode, PropCode, BatchNo, ");
            SQL.AppendLine("Qty, Qty2, Qty3, ");
            SQL.AppendLine("Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select DocType, DocNo, DNo, DocDt, Source, 'Y', Source2, ");
            SQL.AppendLine("WhsCode, Lot, Bin, ItCode, PropCode, BatchNo, ");
            SQL.AppendLine("Qty*-1, Qty2*-1, Qty3*-1, ");
            SQL.AppendLine("Null, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblStockMovement ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And DNo=@DNo ");
            SQL.AppendLine("And DocType=@DocType ");
            SQL.AppendLine("And CancelInd='N' ");
            SQL.AppendLine("And Exists (Select 1 From TblDOCtHdr Where DocNo = @DocNo And ProcessInd = 'F'); ");
            
            SQL.AppendLine("Update TblStockSummary As T1 ");
            SQL.AppendLine("Inner Join ( ");
            SQL.AppendLine("    Select A.WhsCode, A.Lot, A.Bin, A.Source, ");
            SQL.AppendLine("    Sum(A.Qty) As Qty, Sum(A.Qty2) As Qty2, Sum(A.Qty3) As Qty3 ");
            SQL.AppendLine("    From TblStockMovement A ");
            SQL.AppendLine("    Inner Join ( ");
            SQL.AppendLine("        Select WhsCode, Lot, Bin, Source ");
            SQL.AppendLine("        From TblStockMovement ");
            SQL.AppendLine("        Where DocType=@DocType ");
            SQL.AppendLine("        And DocNo=@DocNo ");
            SQL.AppendLine("        And DNo=@DNo ");
            SQL.AppendLine("        And CancelInd='Y' ");
            SQL.AppendLine("    ) B ");
            SQL.AppendLine("        On A.WhsCode=B.WhsCode ");
            SQL.AppendLine("        And A.Lot=B.Lot ");
            SQL.AppendLine("        And A.Bin=B.Bin ");
            SQL.AppendLine("        And A.Source=B.Source ");
            SQL.AppendLine("    Where A.Qty<>0 ");
            SQL.AppendLine("    Group By A.WhsCode, A.Lot, A.Bin, A.Source ");
            SQL.AppendLine(") T2 ");
            SQL.AppendLine("    On T1.WhsCode=T2.WhsCode ");
            SQL.AppendLine("    And T1.Lot=T2.Lot ");
            SQL.AppendLine("    And T1.Bin=T2.Bin ");
            SQL.AppendLine("    And T1.Source=T2.Source ");
            SQL.AppendLine("Set ");
            SQL.AppendLine("    T1.Qty=IfNull(T2.Qty, 0), ");
            SQL.AppendLine("    T1.Qty2=IfNull(T2.Qty2, 0), ");
            SQL.AppendLine("    T1.Qty3=IfNull(T2.Qty3, 0), ");
            SQL.AppendLine("    T1.LastUpBy=@UserCode, ");
            SQL.AppendLine("    T1.LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where Exists (Select 1 From TblDOCtHdr Where DocNo = @DocNo And ProcessInd = 'F'); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.GetGrdStr(Grd1, Row, 0));
            Sm.CmParam<String>(ref cm, "@DocType", mDocType);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand EditDOCtDtl2(int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblDOCtDtl Set UPrice=@UPrice, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And DNo=@DNo ");
            SQL.AppendLine("And CancelInd='N'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.GetGrdStr(Grd1, Row, 0));
            Sm.CmParam<Decimal>(ref cm, "@UPrice", Sm.GetGrdDec(Grd1, Row, 23));
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand EditDOCtHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblDOCtHdr Set Remark=@Remark, ProcessInd = @ProcessInd, LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo; ");

            if (mIsDOCtApprovalActived && mIsDOCtUseProcessInd)
            {
                SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
                SQL.AppendLine("Select T.DocType, @DocNo, '001', T.DNo, @UserCode, CurrentDateTime() ");
                SQL.AppendLine("From TblDocApprovalSetting T ");
                SQL.AppendLine("Where T.DocType='DOCt' ");
                SQL.AppendLine("And (T.StartAmt=0.00 Or T.StartAmt<=@Amt) ");
                SQL.AppendLine("And Exists (Select 1 From TblDOCtHdr Where DocNo = @DocNo And ProcessInd = 'F'); ");

                SQL.AppendLine("Update TblDOCtHdr Set Status='A' ");
                SQL.AppendLine("Where DocNo=@DocNo ");
                SQL.AppendLine("And Not Exists( ");
                SQL.AppendLine("    Select 1 From TblDocApproval ");
                SQL.AppendLine("    Where DocType='DOCt' ");
                SQL.AppendLine("    And DocNo=@DocNo ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine("And Exists (Select 1 From TblDOCtHdr Where DocNo = @DocNo And ProcessInd = 'F'); ");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@ProcessInd", mIsDOCtUseProcessInd ? Sm.GetLue(LueProcessInd) : "F");
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand UpdateDOCtStatus(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblDOCtHdr Set ");
            SQL.AppendLine("    Status='C', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select 1 From TblDOCtDtl Where CancelInd='N' And DocNo=@DocNo ");
            SQL.AppendLine("    ); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveJournal()
        {
            var cm = new MySqlCommand();
            var Filter = string.Empty;
            var SQL = new StringBuilder();
            var EntCode = Sm.GetEntityWarehouse(Sm.GetLue(LueWhsCode));
            var CurrentDt = Sm.ServerCurrentDate();
            var DocDt = Sm.GetDte(DteDocDt);
            var IsClosingJournalUseCurrentDt = Sm.IsClosingJournalUseCurrentDt(DocDt);

            for (int r = 0; r < Grd1.Rows.Count; r++)
            {
                if (Sm.GetGrdBool(Grd1, r, 1) && !Sm.GetGrdBool(Grd1, r, 2) && Sm.GetGrdStr(Grd1, r, 4).Length > 0)
                {
                    if (Filter.Length > 0) Filter += " Or ";
                    Filter += " (B.DNo=@DNo00" + r.ToString() + ") ";
                    Sm.CmParam<String>(ref cm, "@DNo00" + r.ToString(), Sm.GetGrdStr(Grd1, r, 0));
                }
            }

            if (Filter.Length > 0) Filter = " And (" + Filter + ") ";

            if (Filter.Length > 0)
            {
                SQL.AppendLine("Update TblDOCtDtl Set JournalDocNo=@JournalDocNo ");
                SQL.AppendLine("Where DocNo=@DocNo " + Filter.Replace("B.", string.Empty));
                SQL.AppendLine("And Exists(Select 1 From TblDOCtHdr Where DocNo=@DocNo And JournalDocNo Is Not Null) ");
                SQL.AppendLine("And JournalDocNo Is Null; ");

                SQL.AppendLine("Insert Into TblJournalHdr ");
                SQL.AppendLine("(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, CCCode, Remark, CreateBy, CreateDt) ");
                SQL.AppendLine("Select @JournalDocNo, ");
                if (IsClosingJournalUseCurrentDt)
                    SQL.AppendLine("Replace(CurDate(), '-', ''), ");
                else
                    SQL.AppendLine("DocDt, ");
                SQL.AppendLine("Concat('Cancelling ', JnDesc) As JnDesc, ");
                SQL.AppendLine("MenuCode, MenuDesc, CCCode, Remark, @UserCode, CurrentDateTime() ");
                SQL.AppendLine("From TblJournalHdr ");
                SQL.AppendLine("Where DocNo In (Select JournalDocNo From TblDOCtHdr Where DocNo=@DocNo And JournalDocNo Is Not Null); ");

                SQL.AppendLine("Set @Row:=0; ");

                SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, EntCode, SOContractDocNo, CreateBy, CreateDt) ");
                SQL.AppendLine("Select A.DocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
                SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, @EntCode, @SOContractDocNo, A.CreateBy, A.CreateDt ");
                SQL.AppendLine("From TblJournalHdr A ");
                SQL.AppendLine("Inner Join ( ");
                SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From (");

                if (mIsAcNoForSaleUseItemCategory)
                {
                    SQL.AppendLine("        Select E.AcNo5 As AcNo, ");
                    SQL.AppendLine("        0.00 As DAmt, ");
                    if (mIsDOCtAmtRounded)
                    {
                        if (ChkProgressInd.Checked || ChkServiceInd.Checked)
                            SQL.AppendLine("        Floor(If(B.UPrice != C.UPriceAfTax, If(B.UPrice = 0.00, C.UPriceAfTax, B.UPrice), C.UPriceAfTax) * B.Qty) As CAmt ");
                        else
                            SQL.AppendLine("        Floor(IfNull(C.UPrice, 0)*IfNull(C.ExcRate, 0)*B.Qty) As CAmt ");
                    }
                    else
                    {
                        if (ChkProgressInd.Checked || ChkServiceInd.Checked)
                            SQL.AppendLine("        If(B.UPrice != C.UPriceAfTax, If(B.UPrice = 0.00, C.UPriceAfTax, B.UPrice), C.UPriceAfTax) * B.Qty As CAmt ");
                        else
                            SQL.AppendLine("        IfNull(C.UPrice, 0)*IfNull(C.ExcRate, 0)*B.Qty As CAmt ");
                    }
                    SQL.AppendLine("        From TblDOCtHdr A ");
                    SQL.AppendLine("        Inner Join TblDOCtDtl B On A.DocNo=B.DocNo " + Filter);
                    if (ChkProgressInd.Checked || ChkServiceInd.Checked)
                        SQL.AppendLine("        Inner join TblSOContractDtl C On A.SOContractDocNo = C.DocNo And B.SOContractDNo = C.DNo ");
                    else
                        SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                    SQL.AppendLine("        Inner Join TblItem D On B.ItCode=D.ItCode ");
                    SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo5 Is Not Null ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo ");
                }
                else
                {
                    SQL.AppendLine("        Select D.ParValue As AcNo, ");
                    SQL.AppendLine("        0.00 As DAmt, ");
                    if (mIsDOCtAmtRounded)
                    {
                        if (ChkProgressInd.Checked || ChkServiceInd.Checked)
                            SQL.AppendLine("        Floor(If(B.UPrice != C.UPriceAfTax, If(B.UPrice = 0.00, C.UPriceAfTax, B.UPrice), C.UPriceAfTax) * B.Qty) As CAmt ");
                        else
                            SQL.AppendLine("        Floor(IfNull(C.UPrice, 0)*IfNull(C.ExcRate, 0)*B.Qty) As CAmt ");
                    }
                    else
                    {
                        if (ChkProgressInd.Checked || ChkServiceInd.Checked)
                            SQL.AppendLine("        If(B.UPrice != C.UPriceAfTax, If(B.UPrice = 0.00, C.UPriceAfTax, B.UPrice), C.UPriceAfTax) * B.Qty As CAmt ");
                        else
                            SQL.AppendLine("        IfNull(C.UPrice, 0)*IfNull(C.ExcRate, 0)*B.Qty As CAmt ");
                    }
                    SQL.AppendLine("        From TblDOCtHdr A ");
                    SQL.AppendLine("        Inner Join TblDOCtDtl B On A.DocNo=B.DocNo " + Filter);
                    if (ChkProgressInd.Checked || ChkServiceInd.Checked)
                        SQL.AppendLine("        Inner join TblSOContractDtl C On A.SOContractDocNo = C.DocNo And B.SOContractDNo = C.DNo ");
                    else
                        SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                    SQL.AppendLine("        Inner Join TblParameter D On D.ParCode='AcNoForCOGS' And D.ParValue Is Not Null ");
                    SQL.AppendLine("        Where A.DocNo=@DocNo ");
                }

                SQL.AppendLine("        Union All ");
                SQL.AppendLine("        Select E.AcNo, ");
                if (mIsDOCtAmtRounded)
                {
                    if (ChkProgressInd.Checked || ChkServiceInd.Checked)
                        SQL.AppendLine("        Floor(If(B.UPrice != C.UPriceAfTax, If(B.UPrice = 0.00, C.UPriceAfTax, B.UPrice), C.UPriceAfTax) * B.Qty) As DAmt, ");
                    else
                        SQL.AppendLine("        Floor(IfNull(C.UPrice, 0)*IfNull(C.ExcRate, 0)*B.Qty) As DAmt, ");
                }
                else
                {
                    if (ChkProgressInd.Checked || ChkServiceInd.Checked)
                        SQL.AppendLine("        If(B.UPrice != C.UPriceAfTax, If(B.UPrice = 0.00, C.UPriceAfTax, B.UPrice), C.UPriceAfTax) * B.Qty As DAmt, ");
                    else
                        SQL.AppendLine("        IfNull(C.UPrice, 0)*IfNull(C.ExcRate, 0)*B.Qty As DAmt, ");
                }
                SQL.AppendLine("        0.00 As CAmt ");
                SQL.AppendLine("        From TblDOCtHdr A ");
                SQL.AppendLine("        Inner Join TblDOCtDtl B On A.DocNo=B.DocNo " + Filter);
                if (ChkProgressInd.Checked || ChkServiceInd.Checked)
                    SQL.AppendLine("        Inner join TblSOContractDtl C On A.SOContractDocNo = C.DocNo And B.SOContractDNo = C.DNo ");
                else
                    SQL.AppendLine("        Inner Join TblStockPrice C On B.Source=C.Source ");
                SQL.AppendLine("        Inner Join TblItem D On B.ItCode=D.ItCode ");
                SQL.AppendLine("        Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo Is Not Null ");
                SQL.AppendLine("        Where A.DocNo=@DocNo ");
                SQL.AppendLine("        Union All ");
                SQL.AppendLine("        Select Concat(X2.ParValue, X3.CtCode) As AcNo, ");
                SQL.AppendLine("        0.00 As DAmt, ");
                SQL.AppendLine("        Sum(X1.Amt) As CAmt ");
                SQL.AppendLine("        From ( ");
                if (mIsDOCtAmtRounded)
                    SQL.AppendLine("            Select Floor(Case When @MainCurCode=A.CurCode Then 1.00 Else IfNull(C.Amt, 0) End * IfNull(B.UPrice, 0)*B.Qty) As Amt ");
                else
                    SQL.AppendLine("            Select Case When @MainCurCode=A.CurCode Then 1.00 Else IfNull(C.Amt, 0) End * IfNull(B.UPrice, 0)*B.Qty As Amt ");
                SQL.AppendLine("            From TblDOCtHdr A ");
                SQL.AppendLine("            Inner Join TblDOCtDtl B On A.DocNo=B.DocNo " + Filter);
                SQL.AppendLine("            Left Join ( ");
                SQL.AppendLine("                Select Amt From TblCurrencyRate ");
                SQL.AppendLine("                Where RateDt<=@DocDt And CurCode1=@CurCode And CurCode2=@MainCurCode ");
                SQL.AppendLine("                Order By RateDt Desc Limit 1 ");
                SQL.AppendLine("            ) C On 0=0 ");
                SQL.AppendLine("            Where A.DocNo=@DocNo ");
                SQL.AppendLine("        ) X1 ");
                SQL.AppendLine("        Inner Join TblParameter X2 On X2.ParCode='CustomerAcNoNonInvoice' And X2.ParValue Is Not Null ");
                SQL.AppendLine("        Inner Join TblDOCtHdr X3 On X3.DocNo=@DocNo ");
                SQL.AppendLine("        Group By X2.ParValue, X3.CtCode ");
                SQL.AppendLine("        Union All ");

                if (mIsAcNoForSaleUseItemCategory)
                {
                    SQL.AppendLine("        Select X.AcNo, ");
                    SQL.AppendLine("        Sum(X.Amt) As DAmt, ");
                    SQL.AppendLine("        0.00 As CAmt ");
                    SQL.AppendLine("        From ( ");
                    SQL.AppendLine("            Select E.AcNo4 As AcNo, ");
                    if (mIsDOCtAmtRounded)
                        SQL.AppendLine("            Floor(Case When @MainCurCode=A.CurCode Then 1.00 Else IfNull(C.Amt, 0) End * IfNull(B.UPrice, 0)*B.Qty) As Amt ");
                    else
                        SQL.AppendLine("            Case When @MainCurCode=A.CurCode Then 1.00 Else IfNull(C.Amt, 0) End * IfNull(B.UPrice, 0)*B.Qty As Amt ");
                    SQL.AppendLine("            From TblDOCtHdr A ");
                    SQL.AppendLine("            Inner Join TblDOCtDtl B On A.DocNo=B.DocNo " + Filter);
                    SQL.AppendLine("            Left Join ( ");
                    SQL.AppendLine("                Select Amt From TblCurrencyRate ");
                    SQL.AppendLine("                Where RateDt<=@DocDt And CurCode1=@CurCode And CurCode2=@MainCurCode ");
                    SQL.AppendLine("                Order By RateDt Desc Limit 1 ");
                    SQL.AppendLine("            ) C On 0=0 ");
                    SQL.AppendLine("            Inner Join TblItem D On B.ItCode=D.ItCode ");
                    SQL.AppendLine("            Inner Join TblItemCategory E On D.ItCtCode=E.ItCtCode And E.AcNo4 Is Not Null ");
                    SQL.AppendLine("            Where A.DocNo=@DocNo ");
                    SQL.AppendLine("        ) X ");
                    SQL.AppendLine("        Group By X.AcNo ");
                }
                else
                {
                    SQL.AppendLine("        Select X2.ParValue As AcNo, ");
                    SQL.AppendLine("        Sum(X1.Amt) As DAmt, ");
                    SQL.AppendLine("        0.00 As CAmt ");
                    SQL.AppendLine("        From ( ");
                    if (mIsDOCtAmtRounded)
                        SQL.AppendLine("            Select Floor(Case When @MainCurCode=A.CurCode Then 1.00 Else IfNull(C.Amt, 0) End * IfNull(B.UPrice, 0)*B.Qty) As Amt ");
                    else
                        SQL.AppendLine("            Select Case When @MainCurCode=A.CurCode Then 1.00 Else IfNull(C.Amt, 0) End * IfNull(B.UPrice, 0)*B.Qty As Amt ");
                    SQL.AppendLine("            From TblDOCtHdr A ");
                    SQL.AppendLine("            Inner Join TblDOCtDtl B On A.DocNo=B.DocNo " + Filter);
                    SQL.AppendLine("            Left Join ( ");
                    SQL.AppendLine("                Select Amt From TblCurrencyRate ");
                    SQL.AppendLine("                Where RateDt<=@DocDt And CurCode1=@CurCode And CurCode2=@MainCurCode ");
                    SQL.AppendLine("                Order By RateDt Desc Limit 1 ");
                    SQL.AppendLine("            ) C On 0=0 ");
                    SQL.AppendLine("            Where A.DocNo=@DocNo ");
                    SQL.AppendLine("        ) X1 ");
                    SQL.AppendLine("        Inner Join TblParameter X2 On X2.ParCode='AcNoForSaleOfFinishedGoods' And X2.ParValue Is Not Null ");
                    SQL.AppendLine("        Group By X2.ParValue ");
                }
                SQL.AppendLine("    ) Tbl Group By AcNo ");
                SQL.AppendLine(") B On 1=1 ");
                SQL.AppendLine("Where A.DocNo=@JournalDocNo;");
            }
            else
            {
                SQL.AppendLine("Select 1; ");
            }

            cm.CommandText = SQL.ToString();
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (IsClosingJournalUseCurrentDt)
            {
                if (mDocNoFormat == "1")
                    Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(CurrentDt, 1));
                else
                    Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNo3(CurrentDt, "Journal", "TblJournalHdr", mEntCode, "1"));
            }
            else
            {
                if (mDocNoFormat == "1")
                    Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(DocDt, 1));
                else
                    Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNo3(DocDt, "Journal", "TblJournalHdr", mEntCode, "1"));
            }
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParam<String>(ref cm, "@SOContractDocNo", TxtSOContractDocNo.Text);
            Sm.CmParam<String>(ref cm, "@MainCurCode", mMainCurCode);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@CurCode", Sm.GetLue(LueCurCode));
            Sm.CmParam<String>(ref cm, "@EntCode", EntCode);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowDOCtHdr(DocNo);
                ShowDOCtDtl(DocNo);
                ComputeTotalQty();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        public void ShowDOCt(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                Sm.ClearGrd(Grd1, true);
                Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 2 });
                Sm.FocusGrd(Grd1, 0, 0);
                ShowDOCtHdr2(DocNo);
                ShowDOCtDtl2(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.Insert);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowDOCtHdr(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.WhsCode, A.CtCode, A.DocNoInternal, A.CurCode, ");
            SQL.AppendLine("A.SAName, A.SAAddress, A.SACityCode, B.CityName, A.SACntCode, C.CntName, A.SAPostalCd, ");
            SQL.AppendLine("A.SAPhone, A.SAFax, A.SAEmail, A.SAMobile,  A.ResiNo, A.Remark, ");
            SQL.AppendLine("Case A.Status When 'O' Then 'Outstanding' When 'A' Then 'Approved' When 'C' Then 'Cancel' End As StatusDesc, ");
            SQL.AppendLine("KBContractNo, KBContractDt, KBPLNo, KBPLDt, KBRegistrationNo, KBRegistrationDt, ");
            SQL.AppendLine("A.KBSubmissionNo, A.CustomsDocCode, A.ProductionWorkGroup, ");
            SQL.AppendLine("A.Driver, A.Expedition, A.VehicleRegNo, A.ProcessInd, ");
            if (mIsDOCtUseSOContract)
                SQL.AppendLine("A.SOContractDocNo, D.ProjectCode, D.PONo, D.ProjectName, A.ServiceInd, A.ProgressInd ");
            else
                SQL.AppendLine("Null As SOContractDocNo, Null As ProjectCode, Null As PONo, Null As ProjectName, 'N' As ServiceInd, 'N' As ProgressInd ");
            SQL.AppendLine("From TblDOCtHdr A  ");
            SQL.AppendLine("Left Join TblCity B On A.SACityCode = B.CityCode ");
            SQL.AppendLine("Left Join TblCountry C On A.SACntCode = C.CntCode ");
            if (mIsDOCtUseSOContract)
            {
                SQL.AppendLine("Left Join ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select T1.DocNo, T1.SOContractDocNo, T5.ProjectCode, T2.PONo, IfNull(T5.ProjectName, T4.ProjectName) ProjectName ");
                SQL.AppendLine("    From TblDOCtHdr T1 ");
                SQL.AppendLine("    Inner Join TblSOContractHdr T2 On T1.SOContractDocNo = T2.DocNo ");
                SQL.AppendLine("        And T1.DocNo = @DocNo ");
                SQL.AppendLine("    Inner Join TblBOQHdr T3 On T2.BOQDocNo = T3.DocNo ");
                SQL.AppendLine("    Inner Join TblLOPHdr T4 On T3.LOPDocNo = T4.DocNo ");
                SQL.AppendLine("    Left Join TblProjectGroup T5 On T4.PGCode = T5.PGCode ");
                SQL.AppendLine(") D On A.DocNo = D.DocNo ");
            }
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                ref cm, SQL.ToString(), 
                new string[] 
                { 
                    //0
                    "DocNo", 

                    //1-5
                    "DocDt", "WhsCode", "CtCode", "DocNoInternal", "SAName",  

                    //6-10
                    "SAAddress", "SACityCode", "CityName", "SACntCode", "CntName",  

                    //11-15
                    "SAPostalCd", "SAPhone", "SAFax", "SAEmail", "SAMobile",

                    //16-20
                    "CurCode", "ResiNo", "KBContractNo", "KBContractDt", "KBPLNo", 
                    
                    //21-25
                    "KBPLDt", "KBRegistrationNo", "KBRegistrationDt", "Remark", "StatusDesc",

                    //26-30
                    "KBSubmissionNo", "CustomsDocCode", "Driver", "Expedition", "VehicleRegNo",

                    //31-35
                    "ProcessInd", "ProductionWorkGroup", "SOContractDocNo", "ProjectCode", "PONo",

                    //36-38
                    "ProjectName", "ServiceInd", "ProgressInd"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                    Sl.SetLueWhsCode(ref LueWhsCode, Sm.DrStr(dr, c[2]));
                    Sl.SetLueCtCode(ref LueCtCode, Sm.DrStr(dr, c[3]));
                    TxtDocNoInternal.EditValue = Sm.DrStr(dr, c[4]);
                    TxtSAName.EditValue = Sm.DrStr(dr, c[5]);
                    MeeSAAddress.EditValue = Sm.DrStr(dr, c[6]);
                    mCityCode = Sm.DrStr(dr, c[7]);
                    TxtCity.EditValue = Sm.DrStr(dr, c[8]);
                    mCntCode = Sm.DrStr(dr, c[9]);
                    TxtCountry.EditValue = Sm.DrStr(dr, c[10]);
                    TxtPostalCd.EditValue = Sm.DrStr(dr, c[11]);
                    TxtPhone.EditValue = Sm.DrStr(dr, c[12]);
                    TxtFax.EditValue = Sm.DrStr(dr, c[13]);
                    TxtEmail.EditValue = Sm.DrStr(dr, c[14]);
                    TxtMobile.EditValue = Sm.DrStr(dr, c[15]);
                    Sm.SetLue(LueCurCode, Sm.DrStr(dr, c[16]));
                    TxtResiNo.EditValue = Sm.DrStr(dr, c[17]);
                    TxtKBContractNo.EditValue = Sm.DrStr(dr, c[18]);
                    Sm.SetDte(DteKBContractDt, Sm.DrStr(dr, c[19]));
                    TxtKBPLNo.EditValue = Sm.DrStr(dr, c[20]);
                    Sm.SetDte(DteKBPLDt, Sm.DrStr(dr, c[21]));
                    TxtKBRegistrationNo.EditValue = Sm.DrStr(dr, c[22]);
                    Sm.SetDte(DteKBRegistrationDt, Sm.DrStr(dr, c[23])); 
                    MeeRemark.EditValue = Sm.DrStr(dr, c[24]);
                    TxtStatus.EditValue = Sm.DrStr(dr, c[25]);
                    TxtKBSubmissionNo.EditValue = Sm.DrStr(dr, c[26]);
                    mCustomsDocCode = Sm.DrStr(dr, c[27]);
                    TxtDriver.EditValue = Sm.DrStr(dr, c[28]);
                    TxtExpedition.EditValue = Sm.DrStr(dr, c[29]);
                    TxtVehicleRegNo.EditValue = Sm.DrStr(dr, c[30]);
                    Sm.SetLue(LueProcessInd, Sm.DrStr(dr, c[31]));
                    Sm.SetLue(LueProductionWorkGroup, Sm.DrStr(dr, c[32]));
                    if (mIsDOCtUseSOContract)
                    {
                        TxtSOContractDocNo.EditValue = Sm.DrStr(dr, c[33]);
                        TxtProjectCode.EditValue = Sm.DrStr(dr, c[34]);
                        TxtPONo.EditValue = Sm.DrStr(dr, c[35]);
                        TxtProjectName.EditValue = Sm.DrStr(dr, c[36]);
                        ChkServiceInd.Checked = Sm.DrStr(dr, c[37]) == "Y" ? true : false;
                        ChkProgressInd.Checked = Sm.DrStr(dr, c[38]) == "Y" ? true : false;
                    }
                }, true
            );
            if (ChkProgressInd.Checked) TxtAmt.Visible = false;
        }

        private void ShowDOCtDtl(string DocNo)
        {
            var SQL = new StringBuilder();
            
            SQL.AppendLine("Select B.DNo, B.CancelInd, B.ItCode, C.ItCodeInternal, C.ItName, C.ForeignName, B.PropCode, D.PropName, B.BatchNo, B.Source, B.Lot, B.Bin, ");
            //SQL.AppendLine("IfNull(E.Qty, 0) + Case When B.CancelInd='N' Then B.Qty Else 0 End As Stock, ");
            //SQL.AppendLine("IfNull(E.Qty2, 0) + Case When B.CancelInd='N' Then B.Qty2 Else 0 End As Stock2, ");
            //SQL.AppendLine("IfNull(E.Qty3, 0) + Case When B.CancelInd='N' Then B.Qty3 Else 0 End As Stock3, ");
            SQL.AppendLine("IfNull(E.Qty, 0) Stock, ");
            SQL.AppendLine("IfNull(E.Qty2, 0) Stock2, ");
            SQL.AppendLine("IfNull(E.Qty3, 0) Stock3, ");
            SQL.AppendLine("B.Qty, B.Qty2, B.Qty3, ");
            SQL.AppendLine("C.InventoryUomCode, C.InventoryUomCode2, C.InventoryUomCode3, ");
            if (mIsDOCtShowStockPrice)
                SQL.AppendLine("H.UPrice*H.ExcRate As StockPrice, ");
            else
                SQL.AppendLine("0.00 As StockPrice, ");
            SQL.AppendLine("B.UPrice, B.Remark, C.ItGrpCode, B.QtyPackagingUnit, B.PackagingUnitUomCode, ");
            SQL.AppendLine("F.CtItCode, I.Remark Specification, G.DNo As CtQtInd, ");
            SQL.AppendLine("B.SOContractDNo, I.No, J.ItCodeDismantle, J.ItNameDismantle, B.Notes, B.NotesDetail, I.Qty SOContractQty, ");
            SQL.AppendLine("B.TransferRequestProjectDocNo, B.TransferRequestProjectDNo, Cast(J.SeqNo As Decimal) SeqNo ");
            SQL.AppendLine("From TblDOCtHdr A ");
            SQL.AppendLine("Inner Join TblDOCtDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblItem C On B.ItCode=C.ItCode ");
            SQL.AppendLine("Left Join TblProperty D On B.PropCode=D.PropCode ");
            SQL.AppendLine("Left Join TblStockSummary E ");
            SQL.AppendLine("    On A.WhsCode=E.WhsCode ");
            SQL.AppendLine("    And B.Lot=E.Lot ");
            SQL.AppendLine("    And B.Bin=E.Bin ");
            SQL.AppendLine("    And B.Source=E.Source ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select CtCode, itCode, CtitCode From tblCustomeritem ");
            SQL.AppendLine(")F On A.CtCode = F.CtCode And B.ItCode = F.itCode ");
            SQL.AppendLine("Left Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select Distinct T5.DNo ");
            SQL.AppendLine("    From TblCtQtHdr T1 ");
            SQL.AppendLine("    Inner join TblCtQtDtl T2 On T1.DocNo = T2.Docno ");
            SQL.AppendLine("        And T1.ActInd = 'Y' ");
            SQL.AppendLine("        And T1.CtCode = (Select CtCode From TblDOCtHdr Where DocNo = @DocNo) ");
            SQL.AppendLine("        And T1.Status = 'A' ");
            SQL.AppendLine("    Inner Join TblItemPriceHdr T3 On T2.ItemPriceDocNo = T3.DocNo ");
            SQL.AppendLine("    Inner Join TblItemPriceDtl T4 On T3.DocNo = T4.DocNo And T2.ItemPriceDNo = T4.DNo ");
            SQL.AppendLine("    Inner Join TblDOCtDtl T5 On T5.DocNo = @DocNo And T5.ItCode = T4.ItCode ");
            SQL.AppendLine(") G On B.DNo = G.DNo ");
            if (mIsDOCtShowStockPrice)
                SQL.AppendLine("Left Join TblStockPrice H On B.Source=H.Source ");
            SQL.AppendLine("Left JOin TblSOContractDtl I On A.SOContractDocNo = I.DocNo And B.SOContractDNo = I.DNo ");
            SQL.AppendLine("Left Join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select Distinct X1.DocNo, X1.DNo, X2.ItCodeDismantle, X4.ItName As ItNameDismantle, X4.ItCodeInternal As LocalCodeDismantle, ");
            SQL.AppendLine("    X4.Specification As SpecificationDismantle, X3.SeqNo ");
            SQL.AppendLine("    From TblSOContractDtl X1 ");
            SQL.AppendLine("    Inner Join TblBOQDtl2 X2 On X1.ItCode = X2.ItCode  ");
            SQL.AppendLine("    Inner Join TblSOContractDtl4 X3 On X1.DocNo = X3.DocNo And X1.ItCode = X3.ItCode And X1.No = X3.No And X2.SeqNo = X3.SeqNo ");
            SQL.AppendLine("        And X1.DocNo In (Select SOContractDocNo From TblDOCtHdr Where DocNo = @DocNo) ");
            SQL.AppendLine("        And X2.DocNo In (Select BOQDocNo From TblSOContractHdr Where DocNo In (Select SOContractDocNo From TblDOCtHdr Where DocNo = @DocNo)) ");
            SQL.AppendLine("    Inner Join TblItem X4 On X2.ItCodeDismantle = X4.ItCode ");
            SQL.AppendLine(") J ON I.Docno = J.DocNo And I.DNo = J.DNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo  ");
            SQL.AppendLine("Group By B.DNo  ");
            SQL.AppendLine("ORDER BY B.DNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 

                    //1-5
                    "CancelInd", "ItCode", "ItCodeInternal", "ItName", "PropCode", 
                    
                    //6-10
                    "PropName", "BatchNo", "Source", "Lot", "Bin", 
                    
                    //11-15
                    "Stock", "Qty", "InventoryUomCode", "Stock2", "Qty2", 
                    
                    //16-20
                    "InventoryUomCode2", "Stock3", "Qty3", "InventoryUomCode3", "UPrice", 
                    
                    //21-25
                    "Remark", "ItGrpCode", "QtyPackagingUnit", "PackagingUnitUomCode", "CtItCode",
 
                    //26-30
                    "ForeignName", "Specification", "CtQtInd", "StockPrice", "SOContractDNo",

                    //31-35
                    "No", "ItCodeDismantle", "ItNameDismantle", "Notes", "NotesDetail",

                    //36-39
                    "SOContractQty", "TransferRequestProjectDocNo", "TransferRequestProjectDNo", "SeqNo"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 12);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 13);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 14);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 15);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 16);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 17);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 18);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 19);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 23, 20);
                    ComputeTotal(Row);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 21);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 22);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 27, 23);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 28, 24);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 29, 25);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 30, 26);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 31, 27);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 32, 28);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 33, 29);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 34, 30);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 35, 31);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 36, 32);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 37, 33);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 38, 34);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 39, 35);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 40, 36);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 41, 37);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 42, 38);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 43, 39);
                }, false, false, true, false
            );
            Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 1, 2 });
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 14, 15, 17, 18, 20, 21, 23, 24, 27, 33 });
            Sm.FocusGrd(Grd1, 0, 1);

            Grd1.Cols[36].Visible = false;
            Grd1.Cols[37].Visible = false;
            Grd1.Cols[23].Visible = true;
            Grd1.Cols[24].Visible = true;
            if (ChkProgressInd.Checked)
            {
                Grd1.Cols[36].Visible = true;
                Grd1.Cols[37].Visible = true;
                Grd1.Cols[23].Visible = false;
                Grd1.Cols[24].Visible = false;
            }

            ChkServiceIndChecked();
        }

        private void ShowDOCtHdr2(string DocNo)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.WhsCode, A.CtCode, A.DocNoInternal, A.CurCode, ");
            SQL.AppendLine("A.SAName, A.SAAddress, A.SACityCode, B.CityName, A.SACntCode, C.CntName, A.SAPostalCd, ");
            SQL.AppendLine("A.SAPhone, A.SAFax, A.SAEmail, A.SAMobile,  A.ResiNo, A.Remark, ");
            SQL.AppendLine("Case A.Status When 'O' Then 'Outstanding' When 'A' Then 'Approved' When 'C' Then 'Cancel' End As StatusDesc, ");
            SQL.AppendLine("KBContractNo, KBContractDt, KBPLNo, KBPLDt, KBRegistrationNo, KBRegistrationDt, ");
            SQL.AppendLine("A.KBSubmissionNo, A.CustomsDocCode ");
            SQL.AppendLine("From TblDOCtHdr A  ");
            SQL.AppendLine("Left Join TblCity B On A.SACityCode = B.CityCode ");
            SQL.AppendLine("Left Join TblCountry C On A.SACntCode = C.CntCode ");
            SQL.AppendLine("Where DocNo=@DocNo;");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "WhsCode", "CtCode", "DocNoInternal", "SAName",  

                        //6-10
                        "SAAddress", "SACityCode", "CityName", "SACntCode", "CntName",  

                        //11-15
                        "SAPostalCd", "SAPhone", "SAFax", "SAEmail", "SAMobile",
   
                        //16-20
                        "CurCode", "ResiNo", "KBContractNo", "KBContractDt", "KBPLNo", 
                        
                        //21-25
                        "KBPLDt", "KBRegistrationNo", "KBRegistrationDt", "Remark", "StatusDesc",

                        //26-27
                        "KBSubmissionNo", "CustomsDocCode"
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        
                        Sl.SetLueWhsCode(ref LueWhsCode, Sm.DrStr(dr, c[2]));
                        Sl.SetLueCtCode(ref LueCtCode, Sm.DrStr(dr, c[3]));
                        TxtDocNoInternal.EditValue = Sm.DrStr(dr, c[4]);
                        TxtSAName.EditValue = Sm.DrStr(dr, c[5]);
                        MeeSAAddress.EditValue = Sm.DrStr(dr, c[6]);
                        mCityCode = Sm.DrStr(dr, c[7]);
                        TxtCity.EditValue = Sm.DrStr(dr, c[8]);
                        mCntCode = Sm.DrStr(dr, c[9]);
                        TxtCountry.EditValue = Sm.DrStr(dr, c[10]);
                        TxtPostalCd.EditValue = Sm.DrStr(dr, c[11]);
                        TxtPhone.EditValue = Sm.DrStr(dr, c[12]);
                        TxtFax.EditValue = Sm.DrStr(dr, c[13]);
                        TxtEmail.EditValue = Sm.DrStr(dr, c[14]);
                        TxtMobile.EditValue = Sm.DrStr(dr, c[15]);
                        Sm.SetLue(LueCurCode, Sm.DrStr(dr, c[16]));
                        TxtResiNo.EditValue = Sm.DrStr(dr, c[17]);
                        TxtKBContractNo.EditValue = Sm.DrStr(dr, c[18]);
                        Sm.SetDte(DteKBContractDt, Sm.DrStr(dr, c[19]));
                        TxtKBPLNo.EditValue = Sm.DrStr(dr, c[20]);
                        Sm.SetDte(DteKBPLDt, Sm.DrStr(dr, c[21]));
                        TxtKBRegistrationNo.EditValue = Sm.DrStr(dr, c[22]);
                        Sm.SetDte(DteKBRegistrationDt, Sm.DrStr(dr, c[23]));
                        MeeRemark.EditValue = Sm.DrStr(dr, c[24]);
                        TxtKBSubmissionNo.EditValue = Sm.DrStr(dr, c[26]);
                        mCustomsDocCode = Sm.DrStr(dr, c[27]);
                    }, true
                );
        }

        private void ShowDOCtDtl2(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select B.DNo, B.CancelInd, B.ItCode, C.ItCodeInternal, C.ItName, C.ForeignName, B.PropCode, D.PropName, B.BatchNo, B.Source, B.Lot, B.Bin, ");
            SQL.AppendLine("IfNull(E.Qty, 0) + Case When B.CancelInd='N' Then B.Qty Else 0 End As Stock, ");
            SQL.AppendLine("IfNull(E.Qty2, 0) + Case When B.CancelInd='N' Then B.Qty2 Else 0 End As Stock2, ");
            SQL.AppendLine("IfNull(E.Qty3, 0) + Case When B.CancelInd='N' Then B.Qty3 Else 0 End As Stock3, ");
            SQL.AppendLine("B.Qty, B.Qty2, B.Qty3, ");
            SQL.AppendLine("C.InventoryUomCode, C.InventoryUomCode2, C.InventoryUomCode3, ");
            SQL.AppendLine("B.UPrice, B.Remark, C.ItGrpCode, B.QtyPackagingUnit, B.PackagingUnitUomCode, F.CtItCode, C.Specification  ");
            SQL.AppendLine("From TblDOCtHdr A ");
            SQL.AppendLine("Inner Join TblDOCtDtl B On A.DocNo=B.DocNo ");
            SQL.AppendLine("Inner Join TblItem C On B.ItCode=C.ItCode ");
            SQL.AppendLine("Left Join TblProperty D On B.PropCode=D.PropCode ");
            SQL.AppendLine("Left Join TblStockSummary E ");
            SQL.AppendLine("    On A.WhsCode=E.WhsCode ");
            SQL.AppendLine("    And B.Lot=E.Lot ");
            SQL.AppendLine("    And B.Bin=E.Bin ");
            SQL.AppendLine("    And B.ItCode=E.ItCode ");
            SQL.AppendLine("    And B.PropCode=E.PropCode ");
            SQL.AppendLine("    And B.BatchNo=E.BatchNo ");
            SQL.AppendLine("    And B.Source=E.Source ");
            SQL.AppendLine("Left Join ( ");
            SQL.AppendLine("    Select CtCode, itCode, CtitCode From tblCustomeritem ");
            SQL.AppendLine(")F On A.CtCode = F.CtCode And B.ItCode = F.itCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo Order By B.DNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "DNo", 

                    //1-5
                    "CancelInd", "ItCode", "ItCodeInternal", "ItName", "PropCode", 
                    
                    //6-10
                    "PropName", "BatchNo", "Source", "Lot", "Bin", 
                    
                    //11-15
                    "Stock", "Qty", "InventoryUomCode", "Stock2", "Qty2", 
                    
                    //16-20
                    "InventoryUomCode2", "Stock3", "Qty3", "InventoryUomCode3", "UPrice", 
                    
                    //21-25
                    "Remark", "ItGrpCode", "QtyPackagingUnit", "PackagingUnitUomCode", "CtItCode",
 
                    //26-27
                    "ForeignName", "Specification"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("B", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 4);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 6);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 7);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 8);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 9);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 10);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 14, 11);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 15, 12);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 13);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 14);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 18, 15);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 16);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 20, 17);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 21, 18);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 22, 19);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 23, 20);
                    ComputeTotal(Row);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 25, 21);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 26, 22);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 27, 23);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 28, 24);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 29, 25);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 30, 26);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 31, 27);
                }, false, false, true, false
            );
            Sm.SetGrdBoolValueFalse(ref Grd1, Grd1.Rows.Count - 1, new int[] { 1, 2 });
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 14, 15, 17, 18, 20, 21, 23, 24, 27 });
            Sm.FocusGrd(Grd1, 0, 1);
        }


        private void ShowShippingAddressData(string CtCode, string ShipName)
        {
            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@CtCode", CtCode);
            Sm.CmParam<String>(ref cm, "@ShipName", ShipName);

            Sm.ShowDataInCtrl(
                    ref cm,
                    "Select A.Address, A.CityCode, B.CityName, A.CntCode, C.CntName, A.PostalCd, A.Phone, A.Fax, A.Email, A.Mobile " +
                    "From TblCustomerShipAddress A " +
                    "Left Join TblCity B On A.CityCode = B.CityCode " +
                    "Left Join TblCountry C On A.CntCode = C.CntCode Where A.CtCode=@CtCode And A.Name=@ShipName ", 
                    new string[] 
                    { 
                        "Address", 
                        "CityCode", "CityName", "CntCode", "CntName", "PostalCd", 
                        "Phone", "Fax", "Email", "Mobile" 
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        MeeSAAddress.EditValue = Sm.DrStr(dr, c[0]);
                        mCityCode = Sm.DrStr(dr, c[1]);
                        TxtCity.EditValue = Sm.DrStr(dr, c[2]);
                        mCntCode = Sm.DrStr(dr, c[3]);
                        TxtCountry.EditValue = Sm.DrStr(dr, c[4]);
                        TxtPostalCd.EditValue = Sm.DrStr(dr, c[5]);
                        TxtPhone.EditValue = Sm.DrStr(dr, c[6]);
                        TxtFax.EditValue = Sm.DrStr(dr, c[7]);
                        TxtEmail.EditValue = Sm.DrStr(dr, c[8]);
                        TxtMobile.EditValue = Sm.DrStr(dr, c[9]);
                    }, false
                );
        }

        #endregion

        #region Additional Method

        internal void ChkServiceIndChecked()
        {
            Grd1.Cols[41].Visible = false;
            Grd1.Cols[42].Visible = false;

            if (TxtSOContractDocNo.Text.Length > 0 && ChkServiceInd.Checked)
            {
                Grd1.Cols[41].Visible = true;
                Grd1.Cols[42].Visible = true;
            }
        }

        private void ChkProgressIndChecked()
        {
            if (mIsDOCtUseSOContract)
            {
                Grd1.Cols[36].Visible = false;
                Grd1.Cols[37].Visible = false;
                Grd1.Cols[23].Visible = true;
                Grd1.Cols[24].Visible = true;
                TxtAmt.Visible = true;

                if (ChkProgressInd.Checked)
                {
                    if (TxtSOContractDocNo.Text.Trim().Length == 0)
                    {
                        Sm.StdMsg(mMsgType.Warning, "SO Contract# is empty.");
                        ChkProgressInd.Checked = false;
                        TxtSOContractDocNo.Focus();
                    }
                    else
                    {
                        ClearGrd();
                        ComputeAmt();
                        Grd1.Cols[36].Visible = true;
                        Grd1.Cols[37].Visible = true;
                        Grd1.Cols[23].Visible = false;
                        Grd1.Cols[24].Visible = false;
                        TxtAmt.Visible = false;
                    }
                }
            }
        }

        internal string GetSelectedDNo()
        {
            string s = string.Empty;

            if (Grd1.Rows.Count > 1)
            {
                for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
                {
                    if (s.Length > 0) s += ",";
                    s += Sm.GetGrdStr(Grd1, i, 34);
                }
            }

            return (s.Length > 0) ? s : "XXX";
        }

        private void SetLueProcessInd(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 'D' As Col1, 'Draft' As Col2 ");
            SQL.AppendLine("Union All Select 'F' As Col1, 'Final' As Col2; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.SetLue2(
              ref Lue, ref cm,
              0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        internal void GetCtQtUPrice(int Row)
        {
            if (mIsDOCtUseCtQtPrice)
            {
                var SQL = new StringBuilder();

                SQL.AppendLine("Select D.UPrice ");
                SQL.AppendLine("From TblCtQtHdr A ");
                SQL.AppendLine("Inner Join TblCtQtDtl B On A.DocNo = B.Docno And A.ActInd = 'Y' ");
                SQL.AppendLine("    And A.CtCode = @Param1 ");
                SQL.AppendLine("    And A.Status = 'A' ");
                SQL.AppendLine("Inner Join TblItemPriceHdr C On B.ItemPriceDocNo = C.DocNo ");
                SQL.AppendLine("Inner Join TblItemPriceDtl D On B.ItemPriceDocNo = D.DocNo And B.ItemPriceDNo = D.DNo ");
                SQL.AppendLine("    And D.ItCode = @Param2 ");
                SQL.AppendLine("Order By A.DocDt Desc ");
                SQL.AppendLine("Limit 1; ");

                if(Sm.IsDataExist(SQL.ToString(), Sm.GetLue(LueCtCode), Sm.GetGrdStr(Grd1, Row, 4), string.Empty))
                {
                    Grd1.Cells[Row, 23].Value = Decimal.Parse(Sm.GetValue(SQL.ToString(), Sm.GetLue(LueCtCode), Sm.GetGrdStr(Grd1, Row, 4), string.Empty));
                    Grd1.Cells[Row, 23].ReadOnly = iGBool.True;
                    Grd1.Cells[Row, 23].BackColor = Color.FromArgb(224, 224, 224);
                }
            }
        }

        private bool IsAuthorizedToAccessPEB()
        {
            bool mFlag = false;
            string mCurrentGrpCode = Sm.GetValue("Select GrpCode From TblUser Where UserCode = @Param; ", Gv.CurrentUserCode);

            string[] mPEBGrpCodeList = mPEBGrpCode.Split(',');
            foreach (string m in mPEBGrpCodeList)
            {
                if (m == mCurrentGrpCode)
                {
                    mFlag = true;
                    break;
                }
            }

            return mFlag;
        }

        private void GetPackagingUnitInfo(
            string ItCode, string PackagingUnitUomCode,
            ref decimal Qty, ref decimal Qty2, ref string SalesUomCode, ref string SalesUomCode2)
        {
            string Filter = string.Empty, Source = string.Empty, Lot = string.Empty, Bin = string.Empty;
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Qty, Qty2, B.SalesUomCode, B.SalesUomCode2 ");
            SQL.AppendLine("From TblItemPackagingUnit A ");
            SQL.AppendLine("Inner Join TblItem B on A.ItCode=B.ItCode ");
            SQL.AppendLine("Where A.ItCode=@ItCode And A.UomCode=@PackagingUnitUomCode Limit 1; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand();
                cm.Connection = cn;
                Sm.CmParam<String>(ref cm, "@ItCode", ItCode);
                Sm.CmParam<String>(ref cm, "@PackagingUnitUomCode", PackagingUnitUomCode);
                
                cm.CommandText = SQL.ToString();
                cm.CommandTimeout = 600;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                    { 
                        //0
                        "Qty", 
                        
                        //1-3
                        "Qty2", "SalesUomCode", "SalesUomCode2" 
                    });

                if (dr.HasRows)
                {
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        Qty = Sm.DrDec(dr, 0);
                        Qty2 = Sm.DrDec(dr, 1);
                        SalesUomCode = Sm.DrStr(dr, 2);
                        SalesUomCode2 = Sm.DrStr(dr, 3);
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        private void SetNumberOfInventoryUomCode()
        {
            string NumberOfInventoryUomCode = Sm.GetParameter("NumberOfInventoryUomCode");
            if (NumberOfInventoryUomCode.Length == 0)
                mNumberOfInventoryUomCode = 1;
            else
                mNumberOfInventoryUomCode = int.Parse(NumberOfInventoryUomCode);
        }

        internal string GetSelectedItem()
        {
            var SQL = string.Empty;
            if (Grd1.Rows.Count != 1)
            {
                for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    if (Sm.GetGrdStr(Grd1, Row, 4).Length != 0)
                        SQL += 
                            "##" + 
                            Sm.GetGrdStr(Grd1, Row, 4) +
                            Sm.GetGrdStr(Grd1, Row, 8) +
                            Sm.GetGrdStr(Grd1, Row, 10) +
                            Sm.GetGrdStr(Grd1, Row, 11) +
                            Sm.GetGrdStr(Grd1, Row, 12) +
                            Sm.GetGrdStr(Grd1, Row, 13) +
                            "##";
            }
            return (SQL.Length == 0 ? "##XXX##" : SQL);
        }

        private void ReComputeStock()
        {
            if (ChkServiceInd.Checked) return;

            if (ChkProgressInd.Checked)
            {
                RecomputeSOContractDO(); // re compute Qty SO Contract - sum(DO Qty), ceplokin ke kolom stock
            }

            string Filter = string.Empty, Source = string.Empty, Lot = string.Empty, Bin = string.Empty;
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Source, Lot, Bin, Qty, Qty2, Qty3 ");
            SQL.AppendLine("From TblStockSummary ");
            SQL.AppendLine("Where WhsCode=@WhsCode ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                var cm = new MySqlCommand();
                cm.Connection = cn;
                Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
                if (Grd1.Rows.Count != 1)
                {
                    int No = 1;
                    for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
                    {
                        if (Sm.GetGrdStr(Grd1, Row, 11).Length != 0)
                        {
                            Sm.GenerateSQLConditionForInventory(ref cm, ref Filter, No, ref Grd1, Row, 11);
                            No += 1;
                        }
                    }
                }
                if (Filter.Length == 0)
                    Filter = " And 0=1 ";
                else
                    Filter = " And (" + Filter + ")";

                cm.CommandText = SQL.ToString() + Filter;
                cm.CommandTimeout = 600;
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                    { 
                        //0
                        "Source", 
                        
                        //1-5
                        "Lot", "Bin", "Qty", "Qty2", "Qty3" 
                    });

                if (dr.HasRows)
                {
                    Grd1.BeginUpdate();
                    while (dr.Read())
                    {
                        Source = Sm.DrStr(dr, 0);
                        Lot = Sm.DrStr(dr, 1);
                        Bin = Sm.DrStr(dr, 2);
                        for (int row = 0; row < Grd1.Rows.Count - 1; row++)
                        {
                            if (
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, row, 11), Source) &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, row, 12), Lot) &&
                                Sm.CompareStr(Sm.GetGrdStr(Grd1, row, 13), Bin)
                                )
                            {
                                Sm.SetGrdValue("N", Grd1, dr, c, row, 14, 3);
                                Sm.SetGrdValue("N", Grd1, dr, c, row, 17, 4);
                                Sm.SetGrdValue("N", Grd1, dr, c, row, 20, 5);
                                break;
                            }
                        }
                    }
                    Grd1.EndUpdate();
                }
                dr.Close();
            }
        }

        private void RecomputeSOContractDO()
        {
            string DNos = string.Empty;

            for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
            {
                if (DNos.Length > 0) DNos += ",";
                DNos += Sm.GetGrdStr(Grd1, i, 34);
            }

            if (DNos.Length > 0)
            {
                var SQL = new StringBuilder();
                var cm = new MySqlCommand();

                SQL.AppendLine("Select A.DNo, A.Qty - IfNull(B.DOQty, 0.00) OutstandingQty ");
                SQL.AppendLine("From TblSOContractDtl A ");
                SQL.AppendLine("Left Join ");
                SQL.AppendLine("( ");
                SQL.AppendLine("    Select T1.SOContractDocNo, T2.SOContractDNo, Sum(T2.Qty) DOQty ");
                SQL.AppendLine("    From TblDOCtHdr T1 ");
                SQL.AppendLine("    Inner Join TblDOCtDtl T2 On T1.DocNo = T2.DocNo ");
                SQL.AppendLine("        And T2.CancelInd = 'N' ");
                SQL.AppendLine("        And T1.SOContractDocNo Is Not Null ");
                SQL.AppendLine("        And T1.SOContractDocNo = @SOContractDocNo ");
                SQL.AppendLine("        And Find_In_Set(T2.SOContractDNo, @DNos) ");
                SQL.AppendLine("    Group By T1.SOContractDocNo, T2.SOContractDNo ");
                SQL.AppendLine(") B On A.DocNo = B.SOContractDocNo And A.DNo = B.SOContractDNo ");
                SQL.AppendLine("Where A.DocNo = @SOContractDocNo ");
                SQL.AppendLine("And Find_In_Set(A.DNo, @DNos); ");

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandText = SQL.ToString();
                    Sm.CmParam<String>(ref cm, "@SOContractDocNo", TxtSOContractDocNo.Text);
                    Sm.CmParam<String>(ref cm, "@DNos", DNos);
                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] { "DNo", "OutstandingQty" });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            for (int i = 0; i < Grd1.Rows.Count; ++i)
                            {
                                if (Sm.GetGrdStr(Grd1, i, 34) == Sm.DrStr(dr, c[0]))
                                {
                                    Sm.SetGrdValue("N", Grd1, dr, c, i, 14, 1);
                                    Sm.SetGrdValue("N", Grd1, dr, c, i, 17, 1);
                                    Sm.SetGrdValue("N", Grd1, dr, c, i, 20, 1);
                                    break;
                                }
                            }
                        }
                    }
                    dr.Close();
                }
            }
        }

        #region Convert To Words

        private static string[] _ones =
            {
                "Zero",
                "One",
                "Two",
                "Three",
                "Four",
                "Five",
                "Six",
                "Seven",
                "Eight",
                "Nine"
            };

        private static string[] _teens =
            {
                "Ten",
                "Eleven",
                "Twelve",
                "Thirteen",
                "Fourteen",
                "Fifteen",
                "Sixteen",
                "Seventeen",
                "Eighteen",
                "Nineteen"
            };

        private static string[] _tens =
            {
                "",
                "Ten",
                "Twenty",
                "Thirty",
                "Forty",
                "Fifty",
                "Sixty",
                "Seventy",
                "Eighty",
                "Ninety"
            };

        // US Nnumbering:
        private static string[] _thousands =
            {
                "",
                "Thousand",
                "Million",
                "Billion",
                "Trillion",
                "Quadrillion"
            };



        private static string Convert(decimal value)
        {
            string digits, temp;
            bool showThousands = false;
            bool allZeros = true;

            StringBuilder builder = new StringBuilder();
            digits = ((long)value).ToString();
            for (int i = digits.Length - 1; i >= 0; i--)
            {
                int ndigit = (int)(digits[i] - '0');
                int column = (digits.Length - (i + 1));

                switch (column % 3)
                {
                    case 0:        // Ones position
                        showThousands = true;
                        if (i == 0)
                        {
                            temp = String.Format("{0} ", _ones[ndigit]);
                        }
                        else if (digits[i - 1] == '1')
                        {
                            temp = String.Format("{0} ", _teens[ndigit]);
                            i--;
                        }
                        else if (ndigit != 0)
                        {
                            temp = String.Format("{0} ", _ones[ndigit]);
                        }
                        else
                        {
                            temp = String.Empty;
                            if (digits[i - 1] != '0' || (i > 1 && digits[i - 2] != '0'))
                                showThousands = true;
                            else
                                showThousands = false;
                        }

                        if (showThousands)
                        {
                            if (column > 0)
                            {
                                temp = String.Format("{0}{1}{2}",
                                    temp,
                                    _thousands[column / 3],
                                    allZeros ? " " : " ");
                            }
                            allZeros = false;
                        }
                        builder.Insert(0, temp);
                        break;

                    case 1:        // Tens column
                        if (ndigit > 0)
                        {
                            temp = String.Format("{0}{1}",
                                _tens[ndigit],
                                (digits[i + 1] != '0') ? " " : " ");
                            builder.Insert(0, temp);
                        }
                        break;

                    case 2:        // Hundreds column
                        if (ndigit > 0)
                        {
                            temp = String.Format("{0} Hundred ", _ones[ndigit]);
                            builder.Insert(0, temp);
                        }
                        break;
                }
            }

            string cents = value.ToString();
            decimal cettt = Decimal.Parse(cents.Substring(cents.Length - 2, 2));
            string cent = Sm.Terbilang2(cettt);
            builder.AppendFormat("Dollars And " + cent + " Cents # ", (value - (long)value) * 100);

            return String.Format("{0}{1} ",
                Char.ToUpper(builder[0]),
                builder.ToString(1, builder.Length - 1));
        }
        #endregion

        private void ParPrint()
        {
            string Doctitle = Sm.GetParameter("Doctitle");

            var l = new List<DOCt>();
            var ldtl = new List<DOCtDtl>();
            var l2 = new List<DoctSignature>();
            var lProgress = new List<DOCtProgress>();
            var lProgressDtl = new List<DOCtProgressDtl>();
            var lProgressDtl2 = new List<DOCtProgressDtl2>();
            var lTotalProgress = new List<TotalProgress>();

            var EmpCode = Sm.GetValue("Select parvalue From TblParameter where Parcode = 'DOCtPrintOutConsumableSignName' ");
            var EmpCode2 = Sm.GetValue("Select parvalue From TblParameter where Parcode = 'DOCtPrintOutSignName' ");
            string[] TableName = { "DOToCt", "DOToCtDtl", "DoctSignature", "DOCtProgress", "DOCtProgressDtl", "DOCtProgressDtl2" };
            List<IList> myLists = new List<IList>();


            #region Header

            var cm = new MySqlCommand();
            var SQL = new StringBuilder();
            decimal mTotal = decimal.Parse(TxtAmt.Text);

            #region Old Query

            //SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
            //SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
            //SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyPhone', ");
            //SQL.AppendLine("Replace(A.DocNo, Concat('/',(Select ParValue From Tblparameter Where ParCode = 'DocTitle')),'') As DocNo,");
            //SQL.AppendLine("DATE_FORMAT(A.DocDt,'%d %M %Y') As DocDt, A.DocNoInternal, C.CtName, ");
            //SQL.AppendLine("A.SAName, A.SAAddress, D.CityName As SACityCode, E.CntName As SACntCode, A.SAPostalCd, A.SAPhone, A.SAFax, A.SAEmail, A.SAMobile, A.Remark, '0' As Indicator ");
            //SQL.AppendLine("From TblDOCtHdr A ");
            //SQL.AppendLine("Inner Join TblCustomer C On A.CtCode = C.CtCode ");
            //SQL.AppendLine("Left Join TblCity D On A.SACityCode = D.CityCode ");
            //SQL.AppendLine("Left Join TblCountry E On A.SACntCode = E.CntCode ");
            //SQL.AppendLine("Where A.DocNo=@DocNo ");

            #endregion

            #region Old Query 02/03/2021
            //if (mIsFilterBySite)
            //{
            //    SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, IfNull(G.CompanyName,(Select ParValue From TblParameter Where Parcode='ReportTitle1'))As CompanyName, G.CompanyPhone, G.CompanyFax, ");
            //    SQL.AppendLine("IfNull(G.CompanyAddress,(Select ParValue From TblParameter Where Parcode='ReportTitle2'))As CompanyAddress,  ");
            //    SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3')As CompanyAddressFull,  ");
            //    SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4')As CompanyPhoneFull, '' As CompanyAddressCity, ");
            //}
            //else
            //{
            //    SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
            //    SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
            //    SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyAddressFull', ");
            //    SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle4') As 'CompanyPhone', ");
            //    SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle5') As 'CompanyFax', ");
            //    SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle6') As 'CompanyNPWP', null as CompanyPhoneFull, ");
            //}
            //if (Sm.GetParameter("DocTitle") == "SIER")
            //    SQL.AppendLine("A.DocNo,");

            //else
            //    SQL.AppendLine("Replace(A.DocNo, Concat('/',(Select ParValue From Tblparameter Where ParCode = 'DocTitle')),'') As DocNo,");
            //SQL.AppendLine("DATE_FORMAT(A.DocDt,'%d %M %Y') As DocDt, A.DocNoInternal, C.CtName, A.SAName, A.SAAddress, D.CityName As SACityCode, ");
            //SQL.AppendLine("E.CntName As SACntCode, A.SAPostalCd, A.SAPhone, A.SAFax, A.SAEmail, A.SAMobile, A.Remark, '0' As Indicator, ");
            //SQL.AppendLine("(Select Length(substring(parvalue,11)) As FormatNum from tblparameter Where Parcode='FormatNum0') As 'FormatNum',");
            //SQL.AppendLine("(Select parvalue from tblparameter where parcode='IsShowForeignName') As IsForeignName, ");
            //SQL.AppendLine("(Select parvalue from tblparameter where parcode='IsShowBatchNo') As IsShowBatchNo, A.Driver, A.VehicleRegNo, A.ResiNo, C.Address As CustomerAddress ");
            //SQL.AppendLine("From TblDOCtHdr A ");
            //SQL.AppendLine("Inner Join TblCustomer C On A.CtCode = C.CtCode ");
            //SQL.AppendLine("Left Join TblCity D On A.SACityCode = D.CityCode ");
            //SQL.AppendLine("Left Join TblCountry E On A.SACntCode = E.CntCode ");
            //SQL.AppendLine("Inner Join TblWarehouse F On A.WhsCode=F.WhsCode ");

            //if (mIsFilterBySite)
            //{
            //    SQL.AppendLine("Left Join (");
            //    SQL.AppendLine("    Select distinct A.DocNo, E.EntName As CompanyName, E.EntPhone As CompanyPhone, E.EntFax As CompanyFax, E.EntAddress As CompanyAddress  ");

            //    SQL.AppendLine("    From TblDoCtHdr A  ");
            //    SQL.AppendLine("    Inner Join TblWarehouse B On A.WhsCode = B.WhsCode ");
            //    SQL.AppendLine("    Left Join Tblcostcenter C On B.CCCode = C.CCCode ");
            //    SQL.AppendLine("    Left Join TblProfitCenter D On C.ProfitCenterCode  = D.ProfitCenterCode  ");
            //    SQL.AppendLine("    Left Join TblEntity E On D.EntCode = E.EntCode ");
            //    SQL.AppendLine("    Where A.DocNo=@DocNo ");
            //    SQL.AppendLine(") G On A.DocNo = G.DocNo ");
            //}
            //SQL.AppendLine("Where A.DocNo=@DocNo ");
            #endregion

            SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
            SQL.AppendLine("A.DocNoInternal As LocalDocNo, C.CtName, DATE_FORMAT(A.DocDt,'%d %M %Y') As DocDt, ");
            if (mIsDOCtUseSOContract && TxtSOContractDocNo.Text.Length > 0 && ChkServiceInd.Checked)
                SQL.AppendLine("Group_Concat(Distinct(B.TransferRequestProjectDocNo) Separator '\n' ) As TransferRequestProjectDocNo ");
            else
                SQL.AppendLine("Null As TransferRequestProjectDocNo ");
            SQL.AppendLine("From TblDOCtHdr A ");
            SQL.AppendLine("Inner Join TblDOCtDtl B On A.DocNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblCustomer C On A.CtCode = C.CtCode ");
            SQL.AppendLine("Where A.DocNo = @DocNo ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                if (mIsFilterBySite)
                {
                    string CompanyLogo = Sm.GetValue(
                       "Select E.EntLogoName " +
                       "From TblDoCtHdr A  " +
                       "Inner Join TblWarehouse B On A.WhsCode = B.WhsCode " +
                       "Left Join Tblcostcenter C On B.CCCode = C.CCCode " +
                       "Left Join TblProfitCenter D On C.ProfitCenterCode  = D.ProfitCenterCode " +
                       "Left Join TblEntity E On D.EntCode = E.EntCode " +
                       "Where A.Docno='" + TxtDocNo.Text + "' "
                   );
                    if (CompanyLogo.Length > 0)
                    {
                        Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo2(CompanyLogo));
                    }
                    else
                    {
                        Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                    }
                }
                else
                {
                    Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                }

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]

                {
                    //0
                    "CompanyLogo",

                    //1-5
                    "CompanyName",
                    "LocalDocNo",
                    "CtName",
                    "DocDt",
                    "TransferRequestProjectDocNo",
                });

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new DOCt()
                        {
                            CompanyLogo = Sm.DrStr(dr, c[0]),
                            CompanyName = Sm.DrStr(dr, c[1]),
                            LocalDocNo = Sm.DrStr(dr, c[2]),
                            CtName = Sm.DrStr(dr, c[3]),
                            DocDt = Sm.DrStr(dr, c[4]),
                            TransferRequestProjectDocNo = Sm.DrStr(dr, c[5]),
                            ProjectCode = TxtProjectCode.Text,
                            ProjectName = TxtProjectName.Text,
                            CustomerPO  = TxtPONo.Text,
                            EmpName = Sm.GetValue("Select EmpName From TblEmployee Where EmpCode = @Param; ", EmpCode),
                        });
                    }
                }
                dr.Close();
            }

            myLists.Add(l);

            #endregion

            #region Detail
            var cmDtl = new MySqlCommand();
            var SQLDtl = new StringBuilder();
            decimal Numb = 0;

            SQLDtl.AppendLine("Select B.ItCodeInternal ItCode, B.ItName, B.Specification, A.Qty, B.InventoryUOMCode, A.Remark ");
            SQLDtl.AppendLine("From TblDOCtDtl A ");
            SQLDtl.AppendLine("Inner Join TblItem B On A.ItCode = B.ItCode ");
            SQLDtl.AppendLine("Where A.CancelInd = 'N' ");
            SQLDtl.AppendLine("And A.DocNo = @DocNo ");

            using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl.Open();
                cmDtl.Connection = cnDtl;
                cmDtl.CommandText = SQLDtl.ToString();
                Sm.CmParam<String>(ref cmDtl, "@DocNo", TxtDocNo.Text);

                var drDtl = cmDtl.ExecuteReader();
                var cDtl = Sm.GetOrdinal(drDtl, new string[]

                {
                    //0
                    "ItCode",

                    //1-5
                    "ItName",
                    "Specification",
                    "Qty",
                    "InventoryUomCode",
                    "Remark",
                });

                if (drDtl.HasRows)
                {
                    while (drDtl.Read())
                    {
                        Numb = Numb + 1;
                        ldtl.Add(new DOCtDtl()
                        {
                            Number = Numb,
                            ItCode = Sm.DrStr(drDtl, cDtl[0]),
                            ItName = Sm.DrStr(drDtl, cDtl[1]),
                            Specification = Sm.DrStr(drDtl, cDtl[2]),
                            Qty = Sm.DrDec(drDtl, cDtl[3]),
                            InventoryUomCode = Sm.DrStr(drDtl, cDtl[4]),
                            Remark = Sm.DrStr(drDtl, cDtl[5]),
                        });
                    }
                }
                drDtl.Close();
            }

            myLists.Add(ldtl);

            #endregion

            #region Signature
            var cm2 = new MySqlCommand();
            var SQL2 = new StringBuilder();

            SQL2.AppendLine("Select B.UserName, Concat(IfNull(C.ParValue, ''), B.UserCode, '.JPG') As EmpPict ");
            SQL2.AppendLine("From TblDOcApproval A ");
            SQL2.AppendLine("Inner Join TblUser B On A.UserCode=B.UserCode ");
            SQL2.AppendLine("Left Join TblParameter C On C.ParCode = 'ImgFileSignature' ");
            SQL2.AppendLine("Where A.DocNo=@DocNo ");

            using (var cn2 = new MySqlConnection(Gv.ConnectionString))
            {
                cn2.Open();
                cm2.Connection = cn2;
                cm2.CommandText = SQL2.ToString();
                Sm.CmParam<String>(ref cm2, "@DocNo", TxtDocNo.Text);
                var dr2 = cm2.ExecuteReader();
                var c2 = Sm.GetOrdinal(dr2, new string[] 
                        {
                         "UserName", "EmpPict"
                        });
                if (dr2.HasRows)
                {
                    while (dr2.Read())
                    {
                        l2.Add(new DoctSignature()
                        {
                            UserName = Sm.DrStr(dr2, c2[0]),
                            EmpPict = Sm.DrStr(dr2, c2[1]),
                        });
                    }
                }
                dr2.Close();
            }
            myLists.Add(l2);

            #endregion

            #region DOCt Progress

            lProgress.Add(new DOCtProgress()
            {
                CompanyLogo = Sm.GetValue("Select Distinct @Param As CompanyLogo; ", @Sm.CompanyLogo()),
                CompanyName = Sm.GetValue("Select ParValue From tblparameter Where ParCode='ReportTitle1'; "),
                DocDt = DteDocDt.Text,
                PONo = TxtPONo.Text,
                ProjectName = TxtProjectName.Text,
                Remark = MeeRemark.Text,
                EmpName = Sm.GetValue("Select EmpName From TblEmployee Where EmpCode = @Param; ", EmpCode2),
                PosName = Sm.GetValue("Select B.PosName From TblEmployee A Inner Join TblPosition B On A.PosCode = B.PosCode where A.EmpCode = @Param; ", EmpCode2),
            });

            myLists.Add(lProgress);

            #endregion

            #region DOCt Progress Detail

            for (int i = 0; i < Grd1.Rows.Count-1; i++)
            {
                if (!Sm.GetGrdBool(Grd1, i, 1)) // tidak di cancel
                {
                    lProgressDtl.Add(new DOCtProgressDtl()
                    {
                        SOContractDocNo = TxtSOContractDocNo.Text,
                        SOContractDNo = Sm.GetGrdStr(Grd1, i, 34),
                        No = (TxtDocNo.Text.Contains("DOC")) ? 0 : Int32.Parse(Sm.GetGrdStr(Grd1, i, 35)),
                        SeqNo = Sm.GetGrdDec(Grd1, i, 43),
                        ItName = Sm.GetGrdStr(Grd1, i, 7),
                        Specification = Sm.GetGrdStr(Grd1, i, 31),
                        ItNameDismantle = Sm.GetGrdStr(Grd1, i, 37),
                        Unit = Sm.GetGrdDec(Grd1, i, 40),
                        UoM = Sm.GetGrdStr(Grd1, i, 16),
                        Progress = Sm.GetGrdDec(Grd1, i, 15),
                        TotalProgress = 0m,
                        Balance = 0m,
                    });
                }
            }

            #region Total Progress

            var SQLP = new StringBuilder();

            SQLP.AppendLine("Select T.SOContractDNo, SUM(T.Qty) TotalProgress ");
            SQLP.AppendLine("From ");
            SQLP.AppendLine("( ");
            SQLP.AppendLine("	Select B.SOContractDNo, B.Qty ");
            SQLP.AppendLine("	From TblDOCtHdr A  ");
            SQLP.AppendLine("	Inner Join TblDOCtDtl B oN A.DocNo = B.DocNo And A.`Status` In ('O', 'A') And B.CancelInd = 'N' ");
            SQLP.AppendLine("	Where Concat(A.DocDt, Left(A.DocNo,4)) <= Concat(@DocDt, Left(@DocNo,4)) ");
            SQLP.AppendLine("	And A.SOContractDocNo = @SOContractDocNo ");
            SQLP.AppendLine(") T ");
            SQLP.AppendLine("Group By T.SOContractDNo; ");


            var cmP = new MySqlCommand();
            Sm.CmParam<String>(ref cmP, "@SOContractDocNo", TxtSOContractDocNo.Text);
            Sm.CmParam<String>(ref cmP, "@DocNo", TxtDocNo.Text);
            Sm.CmParamDt(ref cmP, "@DocDt", Sm.GetDte(DteDocDt));
            using (var cnP = new MySqlConnection(Gv.ConnectionString))
            {
                cnP.Open();
                cmP.Connection = cnP;
                cmP.CommandTimeout = 600;
                cmP.CommandText = SQLP.ToString();
                var drP = cmP.ExecuteReader();
                var cP = Sm.GetOrdinal(drP, new string[] { "SOContractDNo", "TotalProgress" });
                if (drP.HasRows)
                {
                    while (drP.Read())
                    {
                        lTotalProgress.Add(new TotalProgress()
                        {
                            SOContractDNo = Sm.DrStr(drP, cP[0]),
                            TotalProgressDO = Sm.DrDec(drP, cP[1]),
                            
                        });
                    }
                }
                drP.Close();
            }

            foreach (var x in lProgressDtl)
            {
                foreach (var y in lTotalProgress.Where(w => w.SOContractDNo == x.SOContractDNo))
                {
                    x.TotalProgress = y.TotalProgressDO;
                    x.Balance = x.Unit - y.TotalProgressDO;
                }
            }


            myLists.Add(lProgressDtl);

            #endregion

            #endregion

            #region DOCt Progress Detail2

            for (int i = 0; i < Grd1.Rows.Count-1; i++)
            {
                if (Sm.GetGrdStr(Grd1, i, 38).Length != 0 || Sm.GetGrdStr(Grd1, i, 39).Length != 0)
                {
                    lProgressDtl2.Add(new DOCtProgressDtl2()
                    {
                        SOContractDNo = Sm.GetGrdStr(Grd1, i, 34),
                        Notes = Sm.GetGrdStr(Grd1, i, 38),
                        NotesDetail = Sm.GetGrdStr(Grd1, i, 39),
                       
                    });
                }
            }

            foreach (var x in lProgressDtl2)
            {
                foreach (var y in lProgressDtl.Where(w => w.SOContractDNo == x.SOContractDNo))
                {
                    x.No = y.No;
                    x.SeqNo = y.SeqNo;
                    x.Unit = y.Unit;
                    x.UoM = y.UoM;
                    x.Progress = y.Progress;
                    x.TotalProgress= y.TotalProgress;
                    x.Balance = y.Balance;
                }
            }


            myLists.Add(lProgressDtl2);

            #endregion

            string RptName = Sm.GetParameter("FormPrintOutDOCT");

            if(TxtDocNo.Text.Contains("DOC"))
                Sm.PrintReport("DOToCt" + RptName, myLists, TableName, false);
            else
                Sm.PrintReport("DOToCtProgressIMS", myLists, TableName, false);

        }
        
              
        internal void ComputeTotalQty()
        {
            decimal Total = 0m;
            int col = 15;
            int col2 = 0;

            while (col <= 21)
            {
                Total = 0m;
                for (int row = 0; row <= Grd1.Rows.Count - 1; row++)
                {
                    if (Sm.GetGrdStr(Grd1, row, col).Length != 0 &&
                        Sm.GetGrdStr(Grd1, row, 4).Length != 0 && 
                        !Sm.GetGrdBool(Grd1, row, 1))
                        Total += Sm.GetGrdDec(Grd1, row, col);
                }
                if (col == 15) col2 = 0;
                if (col == 18) col2 = 1;
                if (col == 21) col2 = 2;
                Grd2.Cells[0, col2].Value = Total;
                col += 3;
            }
        }

        private void ComputeTotal(int row)
        {
            decimal Qty = 0m, UPrice = 0m;

            if (Sm.GetGrdStr(Grd1, row, 15).Length != 0) Qty = Sm.GetGrdDec(Grd1, row, 15);
            if (Sm.GetGrdStr(Grd1, row, 23).Length != 0) UPrice = Sm.GetGrdDec(Grd1, row, 23);
            if (mIsDOCtAmtRounded)
                Grd1.Cells[row, 24].Value = Sm.FormatNum(decimal.Truncate(Qty*UPrice), 0);
            else
                Grd1.Cells[row, 24].Value = Sm.FormatNum(Qty * UPrice, 0);
            ComputeAmt();
        }

        private void ComputeAmt()
        {
            decimal Amt = 0m;
            for (int row = 0; row <= Grd1.Rows.Count - 1; row++)
                if (Sm.GetGrdStr(Grd1, row, 24).Length != 0 && !Sm.GetGrdBool(Grd1, row, 1)) 
                    Amt += Sm.GetGrdDec(Grd1, row, 24);
            if(mIsDOCtAmtRounded)
                //TxtAmt.EditValue = Sm.FormatNum(Math.Ceiling(Amt),0); 
                TxtAmt.EditValue = Sm.FormatNum(decimal.Truncate(Amt), 0); 
            else
                TxtAmt.EditValue = Sm.FormatNum(Amt, 0);
        }

        private void LueRequestEdit(iGrid Grd, DevExpress.XtraEditors.LookUpEdit Lue, ref iGCell fCell, ref bool fAccept, TenTec.Windows.iGridLib.iGRequestEditEventArgs e, int Col)
        {
            e.DoDefault = false;

            fCell = Grd.Cells[e.RowIndex, e.ColIndex];
            fCell.EnsureVisible();
            Rectangle myBounds = fCell.Bounds;
            myBounds.Width -= Grd.GridLines.Vertical.Width;
            myBounds.Height -= Grd.GridLines.Horizontal.Width;
            if (myBounds.Width <= 0 || myBounds.Height <= 0) return;

            Rectangle myCellsArea = Grd.CellsAreaBounds;
            if (myBounds.Right > myCellsArea.Right)
                myBounds.Width -= myBounds.Right - myCellsArea.Right;
            if (myBounds.Bottom > myCellsArea.Bottom)
                myBounds.Height -= myBounds.Bottom - myCellsArea.Bottom;

            myBounds.Offset(Grd.Location);

            Lue.Bounds = myBounds;

            if (Sm.GetGrdStr(Grd, fCell.RowIndex, fCell.ColIndex).Length == 0)
                Lue.EditValue = null;
            else
                Sm.SetLue(Lue, Sm.GetGrdStr(Grd, fCell.RowIndex, Col));

            Lue.Visible = true;
            Lue.Focus();

            fAccept = true;
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueProcessInd_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueProcessInd, new Sm.RefreshLue1(SetLueProcessInd));
            }
        }

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue2(Sl.SetLueWhsCode), string.Empty);
            ClearGrd();
        }

        private void LueCtCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                Sm.RefreshLookUpEdit(LueCtCode, new Sm.RefreshLue2(Sl.SetLueCtCode), string.Empty);
                ClearData2();
                if (mIsDOCtUseCtQtPrice) ClearGrd();
            }
        }

        private void TxtDocNoInternal_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtDocNoInternal);
        }

        private void TxtDriver_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtDriver);
        }

        private void TxtExpedition_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtExpedition);
        }

        private void TxtVehicleRegNo_Validated(object sender, EventArgs e)
        {
            Sm.TxtTrim(TxtVehicleRegNo);
        }

        private void LueShipAdd_ProcessNewValue(object sender, DevExpress.XtraEditors.Controls.ProcessNewValueEventArgs e)
        {
            //(LueSAName.Properties.DataSource as List<Lue1>).Add
            //   (
            //       new Lue1 { 
            //           Col1 = e.DisplayValue.ToString() 
            //       }
            //   );
            //e.Handled = true;
        }

        //private void LueShipAdd_EditValueChanged(object sender, EventArgs e)
        //{
        //    Sm.RefreshLookUpEdit(LueSAName, new Sm.RefreshLue3(SetLueShippingAddress), Sm.GetLue(LueCtCode), TxtDocNo.Text);
        //    try
        //    {
        //        ClearData2();
        //        ShowShippingAddressData(Sm.GetLue(LueCtCode), Sm.GetLue(LueSAName));
        //    }
        //    catch (Exception Exc)
        //    {
        //        Sm.ShowErrorMsg(Exc);
        //    }
        //}

        private void BtnCtShippingAddress_Click(object sender, EventArgs e)
        {
            if (!Sm.IsLueEmpty(LueCtCode, "Customer"))
            {
                try
                {
                    var f = new FrmCustomer(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mCtCode = Sm.GetLue(LueCtCode);
                    f.ShowDialog();
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
            }
        }

        private void LuePackagingUnitUomCode_EditValueChanged(object sender, EventArgs e)
        {
            var ItCode = Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 4);
            if (ItCode.Length == 0) ItCode = "XXX";
            Sm.RefreshLookUpEdit(LuePackagingUnitUomCode, 
                new Sm.RefreshLue3(SetLuePackagingUnitUomCode), ItCode, string.Empty);
        }

        private void SetLuePackagingUnitUomCode(ref LookUpEdit Lue, string ItCode, string PackagingUnitUomCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select UomCode As Col1, UomCode As Col2 ");
            SQL.AppendLine("From TblItemPackagingUnit Where ItCode=@ItCode ");
            if (PackagingUnitUomCode.Length != 0)
            {
                SQL.AppendLine("Union All ");
                SQL.AppendLine("Select UomCode As Col1, UomCode As Col2 ");
                SQL.AppendLine("From TblUom Where UomCode=@UomCode ");
            }
            SQL.AppendLine("Order By UomCode;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@ItCode", ItCode);
            Sm.CmParam<String>(ref cm, "@UomCode", PackagingUnitUomCode);

            Sm.SetLue2(
              ref Lue, ref cm,
              0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        private void LuePackagingUnitUomCode_Leave(object sender, EventArgs e)
        {
            if (LuePackagingUnitUomCode.Visible && fAccept)
            {
                if (Sm.GetLue(LuePackagingUnitUomCode).Length == 0)
                    Grd1.Cells[fCell.RowIndex, fCell.ColIndex].Value = null;
                else
                    Grd1.Cells[fCell.RowIndex, fCell.ColIndex].Value = Sm.GetLue(LuePackagingUnitUomCode);
                LuePackagingUnitUomCode.Visible = false;
            }
        }

        private void LuePackagingUnitUomCode_KeyDown(object sender, KeyEventArgs e)
        {
            Sm.LueKeyDown(Grd1, ref fAccept, e);
        }

        private void BtnCopyDOCt_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled && !Sm.IsLueEmpty(LueCtCode, "Customer"))
                Sm.FormShowDialog(new FrmDOCt8Dlg5(this, Sm.GetLue(LueCtCode)));
        }

        private void LueProductionWorkGroup_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled)
            {
                if (mIsUseProductionWorkGroup)
                    Sm.RefreshLookUpEdit(LueProductionWorkGroup, new Sm.RefreshLue2(Sl.SetLueOption), "ProductionWorkGroup");
            }
        }

        private void ChkProgressInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled && mIsDOCtUseSOContract)
            {
                ChkProgressIndChecked();
            }
        }

        private void ChkServiceInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled && TxtDocNo.Text.Length == 0)
            {
                ChkServiceIndChecked();
            }
        }

        #endregion

        #region Button Event

        private void BtnCustomerShipAddress_Click(object sender, EventArgs e)
        {
            if (!Sm.IsLueEmpty(LueCtCode, "Customer"))
                Sm.FormShowDialog(new FrmDOCt8Dlg2(this, Sm.GetLue(LueCtCode)));
        }

        private void BtnKBContractNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmDOCt8Dlg3(this));
        }

        private void BtnPEB_Click(object sender, EventArgs e)
        {
            if (mIsKawasanBerikatEnabled && IsAuthorizedToAccessPEB())
            {
                if (BtnSave.Enabled)
                {
                    Sm.FormShowDialog(new FrmDOCt8Dlg4(this));
                }
            }
        }

        private void BtnSOContractDocNo_Click(object sender, EventArgs e)
        {
            if (BtnSave.Enabled && TxtDocNo.Text.Length == 0 && mIsDOCtUseSOContract && !Sm.IsLueEmpty(LueCtCode, "Customer"))
            {
                Sm.FormShowDialog(new FrmDOCt8Dlg6(this));
            }
        }

        private void BtnSOContractDocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtSOContractDocNo, "SO Contract#", false))
            {
                var f1 = new FrmSOContract2(mMenuCode);
                f1.Tag = mMenuCode;
                f1.WindowState = FormWindowState.Normal;
                f1.StartPosition = FormStartPosition.CenterScreen;
                f1.mDocNo = TxtSOContractDocNo.Text;
                f1.ShowDialog();
            }
        }


        #endregion

        #endregion

        #region Report Class

        class DOCt
        {
            public string CompanyLogo { set; get; }
            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyAddressFull { get; set; }
            public string CompanyPhoneFull { get; set; }
            public string CompanyPhone { get; set; }
            public string DocNo { get; set; }
            public string DocDt { get; set; }
            public string CtName { get; set; }
            public string LocalDocNo { get; set; }
            public string TransferRequestProjectDocNo { get; set; }
            public string ProjectCode { get; set; }
            public string ProjectName { get; set; }
            public string CustomerPO { get; set; }
            public string EmpName { get; set; }
            public string PosName { get; set; }
        }

        class DOCtDtl
        {
            public decimal Number { get; set; }
            public string ItCode { get; set; }
            public string ItName { get; set; }
            public string Specification { get; set; }
            public decimal Qty { get; set; }
            public string InventoryUomCode { get; set; }
            public string Remark { get; set; }

        }

        class DOCtDtl3
        {
            public string PalletNo { get; set; }
            public string ItName { get; set; }
            public decimal Length { get; set; }
            public decimal Width { get; set; }
            public decimal Height { get; set; }
            public decimal QtyInventory { get; set; }
            public decimal QtySales { get; set; }
            public decimal QtyPackagingUnit { get; set; }
        }

        class DoctSignature
        {
            public string UserName { set; get; }
            public string EmpPict { get; set; }
        }

        class DOCtProgress
        {
            public string CompanyLogo { get; set; }
            public string CompanyName { get; set; }
            public string DocDt { get; set; }
            public string PONo { get; set; }
            public string ProjectName { get; set; }
            public string Remark { get; set; }
            public string EmpName { get; set; }
            public string PosName { get; set; }
        }

        class DOCtProgressDtl
        {
            public string SOContractDocNo { get; set; }
            public string SOContractDNo { get; set; }
            public int No { get; set; }
            public decimal SeqNo { get; set; }
            public string ItName { get; set; }
            public string ItNameDismantle { get; set; }
            public string Specification { get; set; }
            public decimal Unit { get; set; }
            public string UoM { get; set; }
            public decimal Progress { get; set; }
            public decimal TotalProgress { get; set; }
            public decimal Balance { get; set; }
        }

        class DOCtProgressDtl2
        {
            public string SOContractDNo { get; set; }
            public int No { get; set; }
            public decimal SeqNo { get; set; }
            public string Notes { get; set; }
            public string NotesDetail { get; set; }
            public decimal Unit { get; set; }
            public string UoM { get; set; }
            public decimal Progress { get; set; }
            public decimal TotalProgress { get; set; }
            public decimal Balance { get; set; }
        }

        class TotalProgress
        {
            public string SOContractDNo { get; set; }
            public decimal TotalProgressDO { get; set; }
        }



        #endregion
    }
}