﻿#region Update
/*
    18/06/2020 [TKG/YK] tambah site
 *  11/05/2021 [MYA/SIER] tambah kolom Active Indicator pada grid saat find
 *  12/07/2021 [SET/PHT] membuat filter ketika find di master cash and bank account itu bisa search nama bank (bank account name), no rekening (Account#), bank account code
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;

#endregion

namespace RunSystem
{
    public partial class FrmBankAccountFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmBankAccount mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmBankAccountFind(FrmBankAccount FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Methods

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.BankAcCode, B.BankName, A.BankAcNo, A.ActInd, A.BankAcTp, A.BankAcNm, ");
            SQL.AppendLine("C.AcNo, C.AcDesc, A.CurCode, A.BranchAcNm, A.BranchAcSwiftCode, D.EntName, E.SiteName, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("From TblBankAccount A ");
            SQL.AppendLine("Left Join TblBank B On A.BankCode=B.BankCode ");
            SQL.AppendLine("Left Join TblCOA C On A.COAAcNo=C.AcNo ");
            SQL.AppendLine("Left Join TblEntity D On A.EntCode=D.EntCode ");
            SQL.AppendLine("Left Join TblSite E On A.SiteCode=E.SiteCode ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 20;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Account Code", 
                        "Bank",
                        "Account#",
                        "Active",
                        "Type",
                        
                        
                        //6-10
                        "Name",
                        "COA Account#",
                        "COA Account's"+Environment.NewLine+"Description",
                        "Currency",
                        "Branch Account's"+Environment.NewLine+"Name",
                        
                        
                        //11-15
                        "Branch Account's"+Environment.NewLine+"Swift Code",
                        "Entity",
                        "Site",
                        "Created By",   
                        "Created Date", 
                        
                        
                        //16-19
                        "Created Time", 
                        "Last Updated By",
                        "Last Updated Date",
                        "Last Updated Time"
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 4 });
            Sm.GrdFormatDate(Grd1, new int[] { 15, 18 });
            Sm.GrdFormatTime(Grd1, new int[] { 16, 19 });
            Sm.GrdColInvisible(Grd1, new int[] { 14, 15, 16, 17, 18, 19 }, false);
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 14, 15, 16, 17, 18, 19 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdAutoSize(Grd1);
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = string.Empty;

                var cm = new MySqlCommand();

                Sm.FilterStr(ref Filter, ref cm, TxtBankAcCode.Text, new string[] { "A.BankAcCode", "B.BankName", "A.BankAcNo", "A.BankAcNm" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, mSQL + Filter + " Order By A.BankAcCode;",
                        new string[]
                        {
                              //0
                             "BankAcCode", 

                              //1-5
                             "BankName",
                             "BankAcNo",
                             "ActInd",
                             "BankAcTp",
                             "BankAcNm",
                             

                             //6-10
                             "AcNo",
                             "AcDesc",
                             "CurCode",
                             "BranchAcNm", 
                             "BranchAcSwiftCode",
                             
                             
                             //11-15
                             "EntName",
                             "SiteName",
                             "CreateBy",
                             "CreateDt", 
                             "LastUpBy", 

                             //16
                             "LastUpDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("B", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 13, 12);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 13);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 15, 14);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 16, 14);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 17, 15);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 18, 16);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 19, 16);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtBankAcCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkBankAcCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Bank account");
        }

        #endregion

        #endregion
    }
}
