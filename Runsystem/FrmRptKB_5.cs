﻿#region Update
/*
    10/09/2020 [DITA/MAI] New Reporting 
    24/09/2020 [DITA/MAI] Tambah Max Value dan Balance dari tpb
    10/12/2020 [WED/MAI] tambah fasilitas print per baris nya
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using FastReport;
using FastReport.Data;
using System.IO;
using System.Net;
using System.Threading;
using DevExpress.XtraEditors;
using System.Collections;

#endregion

namespace RunSystem
{
    public partial class FrmRptKB_5 : RunSystem.FrmBase6
    {
        #region Field

        private string
            mMenuCode = string.Empty, mAccessInd = string.Empty, mSQL = string.Empty,
            mStartDt = string.Empty, 
            mEndDt = string.Empty,
            mKB_Server = string.Empty,
            mKB_Database = string.Empty,
            mKB_DBUser = string.Empty,
            mKB_DBPwd = string.Empty,
            mKB_Port = string.Empty,
            mKBRegistrationNo = string.Empty,
            mKBRegistrationDt = string.Empty,
            mKBSubmissionNo = string.Empty,
            mItGrpCode = string.Empty;

        #endregion

        #region Constructor

        public FrmRptKB_5(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                GetParameter();
                SetLueType(ref LueType);
                SetGrd();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -180);
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private string GetSQL(string Type)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select T.* ");
            SQL.AppendLine("From ");
            SQL.AppendLine("( ");

            SQL.AppendLine("    SELECT Distinct A.DocNo, A.DocDt, A.KBSubmissionNo, A.CustomsDocCode, A.KBDecreeNo, A.KBDecreeDt, A.KBDecreeDt KBDecreeDtPlain, ");
            SQL.AppendLine("    A.KBRegistrationNo, A.KBRegistrationDt, D.ItGrpCode, B.Qty, A.KBContractNo, A.KBContractDt, A.KBInsuranceAmt, E.ItCtName, D.ItName ");
            SQL.AppendLine("    FROM TblRecvWhsHdr A ");
            SQL.AppendLine("    INNER JOIN TblRecvWhsDtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("    INNER JOIN TblDOWhsDtl C ON B.DOWhsDocNo = C.DocNo AND B.DOWhsDNo = C.DNo AND C.CancelInd = 'N' ");
            SQL.AppendLine("    INNER JOIN TblItem D ON C.ItCode = D.ItCode ");
            SQL.AppendLine("    Inner Join TblItemCategory E On D.ItCtCode = E.ItCtCode ");
            SQL.AppendLine("    WHERE A.KBRegistrationNo IS NOT NULL ");
            SQL.AppendLine("    AND B.CancelInd = 'N' ");
            SQL.AppendLine("    And A.DocDt Between @DocDt1 And @DocDt2 ");

            SQL.AppendLine("    UNION ALL ");

            SQL.AppendLine("    SELECT Distinct A.DocNo, A.DocDt, A.KBSubmissionNo, A.CustomsDocCode, A.KBDecreeNo, A.KBDecreeDt, A.KBDecreeDt KBDecreeDtPlain, ");
            SQL.AppendLine("    A.KBRegistrationNo, A.KBRegistrationDt, C.ItGrpCode, B.Qty, A.KBContractNo, A.KBContractDt, A.KBInsuranceAmt, D.ItCtName, C.ItName ");
            SQL.AppendLine("    FROM TblRecvWhs2Hdr A ");
            SQL.AppendLine("    INNER JOIN TblRecvWhs2Dtl B ON A.DocNo = B.DocNo ");
            SQL.AppendLine("    INNER JOIN TblItem C ON B.ItCode = C.ItCode ");
            SQL.AppendLine("    Inner Join TblItemCategory D On C.ItCtCode = D.ItCtCode ");
            SQL.AppendLine("    WHERE A.KBRegistrationNo IS NOT NULL ");
            SQL.AppendLine("    AND B.CancelInd = 'N' ");
            SQL.AppendLine("    And A.DocDt Between @DocDt1 And @DocDt2 ");


            SQL.AppendLine(") T ");

            mSQL = SQL.ToString();

            return mSQL;
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 21;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Document#",
                    "Date",
                    "",
                    "Submission#",
                    "Type",
                    
                    //6-10
                    "Decree#",
                    "Decree Date",
                    "Registration#",
                    "Registration Date",
                    "Item Group Code",

                    //11-15
                    "Quantity",
                    "Max Value",
                    "Balance",
                    "",
                    "Contract#",

                    //16-20
                    "Contract Date",
                    "Insurance Amount",
                    "Item Category",
                    "Item Name",
                    "Decree Date (Plain)"
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    150, 120, 20, 200, 100,
                    
                    //6-10
                    220, 120, 0, 0, 0,

                    //11-15
                    0, 100, 100, 20, 0,

                    //16-20
                    0, 0, 0, 0, 0
                }
            );
            Sm.GrdColButton(Grd1, new int[] { 3, 14 });
            Sm.GrdFormatDate(Grd1, new int[] { 2, 7 });
            Sm.GrdFormatDec(Grd1, new int[] { 11, 12, 13, 17 }, 0);
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 15, 16, 17, 18, 19, 20 });
            Sm.GrdColInvisible(Grd1, new int[] { 15, 16, 17, 18, 19, 20 });
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void ShowData()
        {
            mStartDt =  mEndDt = mKBRegistrationNo = mKBRegistrationDt = mKBSubmissionNo = mItGrpCode = string.Empty;
            Sm.ClearGrd(Grd1, true);
            if (
               Sm.IsDteEmpty(DteDocDt1, "Start date") ||
               Sm.IsDteEmpty(DteDocDt2, "End date") ||
               Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
               ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " Where 0 = 0 ";
                var l = new List<KB_Data>();

                var cm = new MySqlCommand();

                mStartDt = Sm.GetDte(DteDocDt1).Substring(0, 8);
                mEndDt = Sm.GetDte(DteDocDt2).Substring(0, 8);

                Sm.CmParamDt(ref cm, "@DocDt1", mStartDt);
                Sm.CmParamDt(ref cm, "@DocDt2", mEndDt);
                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "T.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, TxtSubmissionNo.Text, "T.KBSubmissionNo", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueType), "T.CustomsDocCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtDecreeNo.Text, "T.KBDecreeNo", false);
                Sm.FilterDt(ref Filter, ref cm, Sm.GetDte(DteDecreeDt1), Sm.GetDte(DteDecreeDt2), "T.KBDecreeDt");

                Sm.ShowDataInGrid(
                    ref Grd1, ref cm,
                    GetSQL(Sm.GetLue(LueType)) + Filter + " Order By T.DocDt, T.DocNo; ",
                    new string[]
                    {
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "KBSubmissionNo", "CustomsDocCode", "KBDecreeNo", "KBDecreeDt",
 
                        //6-10
                        "KBRegistrationNo", "KBRegistrationDt", "ItGrpCode", "Qty", "KBContractNo",

                        //11-15
                        "KBContractDt", "KBInsuranceAmt", "ItCtName", "ItName", "KBDecreeDtPlain"
                    },
                    (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                    {
                        Grd.Cells[Row, 0].Value = Row + 1;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 2);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 3);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 4);
                        Sm.SetGrdValue("D", Grd, dr, c, Row, 7, 5);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 8, 6);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 9, 7);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 8);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 11, 9);
                        Grd1.Cells[Row, 12].Value = 0m;
                        Grd1.Cells[Row, 13].Value = 0m;
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 15, 10);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 16, 11);
                        Sm.SetGrdValue("N", Grd, dr, c, Row, 17, 12);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 18, 13);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 19, 14);
                        Sm.SetGrdValue("S", Grd, dr, c, Row, 20, 15);
                    }, true, false, false, false
                );
                Process1(ref l);
                iGSubtotalManager.BackColor = Color.LightSalmon;
                iGSubtotalManager.ShowSubtotalsInCells = true;
                iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 12, 13 });
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        private void Process1(ref List<KB_Data> l)
        {
            var cm = new MySqlCommand();
            var SQL = new StringBuilder();

            for (int i = 0; i < Grd1.Rows.Count; i++)
            {
                if(mKBRegistrationNo.Length > 0) mKBRegistrationNo += ",";
                mKBRegistrationNo += Sm.GetGrdStr(Grd1, i, 8);

                if(mKBSubmissionNo.Length > 0) mKBSubmissionNo += ",";
                mKBSubmissionNo += Sm.GetGrdStr(Grd1, i, 4);

                if(mKBRegistrationDt.Length > 0) mKBRegistrationDt += ",";
                mKBRegistrationDt += Sm.GetGrdStr(Grd1, i, 9);

                if(mItGrpCode.Length > 0) mItGrpCode += ",";
                mItGrpCode += Sm.GetGrdStr(Grd1, i, 10);
            }


            Sm.CmParam<String>(ref cm, "@KBRegistrationNo", mKBRegistrationNo);
            Sm.CmParam<String>(ref cm, "@KBSubmissionNo", mKBSubmissionNo);
            Sm.CmParam<String>(ref cm, "@KBRegistrationDt", mKBRegistrationDt);
            Sm.CmParam<String>(ref cm, "@ItGrpCode", mItGrpCode);

            SQL.AppendLine("Select T1.Nomor_Daftar As KBRegistrationNo, Date_Format(T1.Tanggal_Daftar, '%Y%m%d') As KBRegistrationDt, ");
            SQL.AppendLine("T1.Nomor_Aju As KBSubmissionNo, ");
            SQL.AppendLine("T3.Kode_Barang ItGrpCode, Sum(T3.Jumlah_Satuan) MaxQty ");
            SQL.AppendLine("From tpb_header T1 ");
            SQL.AppendLine("Inner Join tpb_dokumen T2 On T1.ID=T2.ID_HEADER And T2.Kode_Jenis_Dokumen='315' ");
            SQL.AppendLine("Inner Join tpb_barang T3 On T1.ID=T3.ID_HEADER ");
            SQL.AppendLine("Where T1.Nomor_Daftar IS Not Null ");
            SQL.AppendLine("And Find_In_set(T1.Nomor_Daftar, @KBRegistrationNo) ");
            SQL.AppendLine("And Find_In_Set(T1.Nomor_Aju, @KBSubmissionNo) ");
            SQL.AppendLine("And Find_In_Set(Date_Format(T1.Tanggal_Daftar, '%Y%m%d'), @KBRegistrationDt) ");
            SQL.AppendLine("And Find_In_Set(T3.Kode_Barang, @ItGrpCode) ");
            SQL.AppendLine("Group By T1.Nomor_Daftar, T1.Tanggal_Daftar, T1.Nomor_Aju, T3.Kode_Barang ");

            using (var cn = new MySqlConnection(ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandTimeout = 600;
                cm.CommandText = SQL.ToString();
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] { 
                    //0
                    "KBRegistrationNo", 
                    
                    //1-5
                    "KBRegistrationDt", 
                    "KBSubmissionNo", 
                    "ItGrpCode", 
                    "MaxQty", 
                   
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new KB_Data()
                        {
                            KBRegistrationNo = Sm.DrStr(dr, c[0]),
                            KBRegistrationDt = Sm.DrStr(dr, c[1]),
                            KBSubmissionNo = Sm.DrStr(dr, c[2]),
                            ItGrpCode = Sm.DrStr(dr, c[3]),
                            MaxQty = Sm.DrDec(dr, c[4]),
                        });
                    }
                }
                dr.Close();
            }

           if (l.Count > 0)
           {
                for (int i = 0; i < Grd1.Rows.Count; ++i)
                {
                    Grd1.Cells[i, 12].Value = 0m;

                    foreach (var x in l.Where(w => Sm.CompareStr(w.KBRegistrationNo, Sm.GetGrdStr(Grd1, i, 8))&&
                        Sm.CompareStr(w.KBSubmissionNo, Sm.GetGrdStr(Grd1, i, 4))&&
                        Sm.CompareStr(w.KBRegistrationDt, Sm.GetGrdStr(Grd1, i, 9))&&
                        Sm.CompareStr(w.ItGrpCode, Sm.GetGrdStr(Grd1, i, 10))
                        ))
                    {
                        Grd1.Cells[i, 12].Value = Sm.FormatNum(x.MaxQty, 0);
                        break;
                    }

                    Grd1.Cells[i, 13].Value = Sm.FormatNum(Sm.GetGrdDec(Grd1, i, 12) - Sm.GetGrdDec(Grd1, i, 11) , 0);
                }
                l.Clear();
           }
        }


        private string ConnectionString
        {
            get
            {
                return
                    string.Concat(
                    @"Server=", mKB_Server, ";Database=", mKB_Database, ";Uid=", mKB_DBUser, ";Password=", mKB_DBPwd, ";Allow User Variables=True;Connection Timeout=1200;",
                    mKB_Port.Length == 0 ? string.Empty : string.Concat("Port=", mKB_Port, ";"));
            }
        }

        private void GetParameter()
        {
            mKB_Server = Sm.GetParameter("KB_Server");
            mKB_Database = Sm.GetParameter("KB_Database");
            mKB_DBUser = Sm.GetParameter("KB_DBUser");
            mKB_DBPwd = Sm.GetParameter("KB_DBPwd");
            mKB_Port = Sm.GetParameter("KB_Port");
        }

        private void PrintPerRow(int Row)
        {
            string DocNo = Sm.GetGrdStr(Grd1, Row, 1);
            string SelectedRow = string.Empty;
            string[] SelectedRows = {};
            string DecreeDt = Sm.GetGrdStr(Grd1, Row, 20);
            string RegistrationDt = Sm.GetGrdStr(Grd1, Row, 9);
            string ContractDt = Sm.GetGrdStr(Grd1, Row, 16);
            int No = 1;

            for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
            {
                if (Sm.GetGrdStr(Grd1, i, 1) == DocNo)
                {
                    if (SelectedRow.Length > 0) SelectedRow += ",";
                    SelectedRow += i.ToString();
                }
            }

            if (SelectedRow.Length > 0)
            {
                SelectedRows = SelectedRow.Split(',');
            }

            var l = new List<KB_5Hdr>();
            var l2 = new List<KB_5Dtl>();

            string[] TableName = { "KB_5Hdr", "KB_5Dtl" };
            List<IList> myLists = new List<IList>();
            if (Grd1.Rows.Count == 0 || mStartDt.Length == 0)
            {
                Sm.StdMsg(mMsgType.NoData, string.Empty);
                return;
            }

            #region Header

            l.Add(new KB_5Hdr() 
            {
                KBDecreeNo = Sm.GetGrdStr(Grd1, Row, 6),
                KBDecreeDt = DecreeDt.Length > 0 ? string.Concat(Sm.Right(DecreeDt, 2), "/", DecreeDt.Substring(4, 2), "/", Sm.Left(DecreeDt, 4)) : string.Empty,
                KBContractNo = Sm.GetGrdStr(Grd1, Row, 15),
                KBContractDt = ContractDt.Length > 0 ? string.Concat(Sm.Right(ContractDt, 2), "/", ContractDt.Substring(4, 2), "/", Sm.Left(ContractDt, 4)) : string.Empty,
                KBInsuranceAmt = Sm.GetGrdDec(Grd1, Row, 17),
                PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime()))
            });

            myLists.Add(l);

            #endregion

            #region Detail

            for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
            {
                foreach (var x in SelectedRows.Where(w => Int32.Parse(w) == i))
                {
                    l2.Add(new KB_5Dtl() 
                    {
                        No = No.ToString(),
                        CustomsDocCode = Sm.GetGrdStr(Grd1, i, 5),
                        KBRegistrationNo = Sm.GetGrdStr(Grd1, i, 8),
                        KBRegistrationDt = RegistrationDt.Length > 0 ? string.Concat(Sm.Right(RegistrationDt, 2), "/", RegistrationDt.Substring(4, 2), "/", Sm.Left(RegistrationDt, 4)) : string.Empty,
                        KBInsuranceAmt = Sm.GetGrdDec(Grd1, Row, 17),
                        ItCtName = Sm.GetGrdStr(Grd1, Row, 18),
                        MaxQty = Sm.GetGrdDec(Grd1, i, 12),
                        ItName = Sm.GetGrdStr(Grd1, i, 19),
                        Qty = Sm.GetGrdDec(Grd1, i, 11),
                        Balance = Sm.GetGrdDec(Grd1, i, 13) // - Sm.GetGrdDec(Grd1, i, 11)
                    });

                    No += 1;
                    break;
                }
            }

            myLists.Add(l2);

            #endregion

            Sm.PrintReport("KB_5", myLists, TableName, false);

            l.Clear(); l2.Clear();
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        override protected void GrdRequestEdit(object sender, iGRequestEditEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 1).Contains("RWDO"))
                {
                    e.DoDefault = false;
                    var f = new FrmRecvWhs(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                    f.ShowDialog();
                }
                else
                {
                    e.DoDefault = false;
                    var f = new FrmRecvWhs2(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                    f.ShowDialog();
                }
            }

            if (e.ColIndex == 14 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                PrintPerRow(e.RowIndex);
            }
        }

        override protected void GrdEllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (e.ColIndex == 3 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                if (Sm.GetGrdStr(Grd1, e.RowIndex, 1).Contains("RWDO"))
                {
                    var f = new FrmRecvWhs(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                    f.ShowDialog();
                }
                else
                {
                    var f = new FrmRecvWhs2(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = Sm.GetGrdStr(Grd1, e.RowIndex, 1);
                    f.ShowDialog();
                }
            }

            if (e.ColIndex == 14 && Sm.GetGrdStr(Grd1, e.RowIndex, 1).Length != 0)
            {
                PrintPerRow(e.RowIndex);
            }
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #region Additional Methods

        public static string ToRoman(int number)
        {
            //if ((number < 0) || (number > 3999)) throw new ArgumentOutOfRangeException("insert value betwheen 1 and 3999");
            if (number < 1) return string.Empty;
            else
            {
                if (number >= 1000) return "M" + ToRoman(number - 1000);
                if (number >= 900) return "CM" + ToRoman(number - 900); 
                if (number >= 500) return "D" + ToRoman(number - 500);
                if (number >= 400) return "CD" + ToRoman(number - 400);
                if (number >= 100) return "C" + ToRoman(number - 100);            
                if (number >= 90) return "XC" + ToRoman(number - 90);
                if (number >= 50) return "L" + ToRoman(number - 50);
                if (number >= 40) return "XL" + ToRoman(number - 40);
                if (number >= 10) return "X" + ToRoman(number - 10);
                if (number >= 9) return "IX" + ToRoman(number - 9);
                if (number >= 5) return "V" + ToRoman(number - 5);
                if (number >= 4) return "IV" + ToRoman(number - 4);
                if (number >= 1) return "I" + ToRoman(number - 1);
            }
            throw new ArgumentOutOfRangeException("something bad happened");
        }

        private void SetLueType(ref LookUpEdit Lue)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("SELECT Distinct T.* ");
            SQL.AppendLine("FROM( ");
            SQL.AppendLine("SELECT OptCode AS Col1, CONCAT('BC ', OptDesc) AS Col2 ");
            SQL.AppendLine("FROM TblOption WHERE OptCat = 'CustomsDocCode261' ");

            SQL.AppendLine("UNION ALL ");

            SQL.AppendLine("SELECT OptCode AS Col1, CONCAT('BC ', OptDesc) AS Col2 ");
            SQL.AppendLine("FROM TblOption WHERE OptCat = 'CustomsDocCode262' ");
            SQL.AppendLine(")T ");

            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }


        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LueType_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueType, new Sm.RefreshLue1(SetLueType));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void ChkType_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Type");
        }

        private void TxtSubmissionNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkSubmissionNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Submission#");
        }

        private void TxtDecreeNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void ChkDecreeNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Decree#");
        }

        private void DteDecreeDt1_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void DteDecreeDt2_EditValueChanged(object sender, EventArgs e)
        {
            Sm.FilterDteSetCheckEdit(this, sender);
        }

        private void ChkDecreeDt_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetDateEdit(this, sender, "Decree date");
        }


        #endregion

        #endregion

        #region Class

        class KB_Data
        {
            public string KBRegistrationNo { get; set; }
            public string KBRegistrationDt { get; set; }
            public string KBSubmissionNo { get; set; }
            public string ItGrpCode { get; set; }
            public decimal MaxQty { get; set; }
        }

        private class KB_5Hdr
        {
            public string KBDecreeNo { get; set; }
            public string KBDecreeDt { get; set; }
            public string KBContractNo { get; set; }
            public string KBContractDt { get; set; }
            public decimal KBInsuranceAmt { get; set; }
            public string PrintBy { get; set; }
        }

        private class KB_5Dtl
        {
            public string No { get; set; }
            public string CustomsDocCode { get; set; }
            public string KBRegistrationNo { get; set; }
            public string KBRegistrationDt { get; set; }
            public decimal KBInsuranceAmt { get; set; }
            public string ItCtName { get; set; }
            public decimal MaxQty { get; set; }
            public string ItName { get; set; }
            public decimal Qty { get; set; }
            public decimal Balance { get; set; }
        }

        #endregion
    }
}
