﻿#region Update
/*
    12/09/2017 [TKG] tambah employment status
    13/09/2017 [ARI] tambah Local docno
    30/10/2017 [TKG] utk kmi, apabila belum ada pps, maka employment status lsg diupdate di master employee.
    13/03/2018 [TKG] tambah contract extension, validasi employment status, hilangkan gaji
    26/03/2018 [ARI] tambah employee management reperesentatif, printout
    27/03/2018 [ARI] tambah mIsEmployeeManagementRepresentative utk employee management reperesentatif
    10/09/2018 [HAR] print jika HO ind disite = Y maka dataservice di hide, jika HO ind disite = N maka dataservice di munculin
    12/09/2019 [DITA] Remark ChkRenewal
    28/10/2019 [RF/IMS] Print out Document Contract
    29/10/2019 [HAR/IMS] BUG Print out Document Contract (tambah parameter FormPrintOutEmpContractDoc buat set nama printout)
    31/10/2019 [RF/IMS] tambah parameter mGradeListOfContractDocumant untuk employee yang terdaftar bisa kerja lembur
    11/03/2020 [VIN/SIER] printout sier sesuaikan 
    11/03/2020 [VIN/VIR] printout vir baru
    21/03/2022 [ISD/GSS] field contract extension dibuat autofill
 */
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;
using DevExpress.XtraEditors;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmEmpContractDoc : RunSystem.FrmBase1
    {
        #region Field, Property

        internal string 
            mMenuCode = string.Empty, mAccessInd = string.Empty;
        internal bool
            mIsFilterBySiteHR = false,
            mIsEmpContractDocInclEmpSts = false,
            mIsNotFilterByAuthorization = false,
            mIsFilteredByEmploymentStatus = false,
            mIsEmpContractDocExtValidationEnabled = false,
            mIsEmployeeManagementRepresentative = false,
            mIsContractExtensionAutoFill = false;

        internal string 
            mFormPrintOutEmpContractDoc = string.Empty,
            mGradeListOfContractDocument = string.Empty;
            
        internal FrmEmpContractDocFind FrmFind;

        private static string[] satuan = new string[10] { "Nol", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh", "Delapan", "Sembilan" };
        private static string[] belasan = new string[10] { "Sepuluh", "Sebelas", "Dua Belas", "Tiga Belas", "Empat Belas", "Lima Belas", "Enam Belas", "Tujuh Belas", "Delapan Belas", "Sembilan Belas" };
        private static string[] puluhan = new string[10] { "", "", "Dua Puluh", "Tiga Puluh", "Empat Puluh", "Lima Puluh", "Enam Puluh", "Tujuh Puluh", "Delapan Puluh", "Sembilan Puluh" };
        private static string[] ribuan = new string[5] { "", "Ribu", "Juta", "Milyar", "Triliyun" };

        #endregion

        #region Constructor

        public FrmEmpContractDoc(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Employee's Contract Document";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                GetParameter();
                if (mIsEmployeeManagementRepresentative) LblEmpCodeManagement.ForeColor = Color.Red;
                SetGrd();
                SetFormControl(mState.View);
                if (mIsEmpContractDocInclEmpSts) LblEmploymentStatus.ForeColor = Color.Red;
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 7;
            Grd1.FrozenArea.ColCount = 2;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "Document#", 
                    "Date",
                    "Local#",
                    "Contract"+Environment.NewLine+"Extension",
                    "Start Date",

                    //6
                    "End Date"
                },
                new int[]
                {
                    //0
                    50,

                    //1-5
                    150, 80, 150, 100, 100,

                    //6
                    100
                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 4 }, 11);
            Sm.GrdFormatDate(Grd1, new int[] { 2, 5, 6 });
            Sm.SetGrdProperty(Grd1, false);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        LueEmploymentStatus, TxtLocalDocNo, TxtContractExt, DteDocDt, DteStartDt, 
                        DteEndDt, MeeRemark 
                    }, true);
                    ChkActInd.Properties.ReadOnly = true;
                    ChkRenewal.Properties.ReadOnly = true;
                    BtnEmpCode.Enabled = false;
                    BtnEmpCode2.Enabled = true;
                    BtnEmpCode3.Enabled = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtLocalDocNo, TxtContractExt, DteDocDt, DteStartDt, DteEndDt, 
                        MeeRemark 
                    }, false);
                    if (mIsEmpContractDocInclEmpSts)
                        Sm.SetControlReadOnly(new List<DXE.BaseEdit>{ LueEmploymentStatus }, false);
                    ChkRenewal.Properties.ReadOnly = false;
                    BtnEmpCode.Enabled = true;
                    BtnEmpCode2.Enabled = true;
                    BtnEmpCode3.Enabled = true;
                    ChkActInd.Checked = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    ChkActInd.Properties.ReadOnly = false;
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, TxtEmpCode, TxtEmpName, TxtDeptName, 
                TxtPosition, TxtGrade, TxtLocalDocNo, LueEmploymentStatus, DteStartDt, 
                DteEndDt, MeeRemark, TxtSite, TxtEmpCode2, TxtEmpName2, TxtPosition2
            });
            Sm.SetControlNumValueZero(new List<DevExpress.XtraEditors.TextEdit> { TxtContractExt }, 11);
            Sm.SetControlNumValueZero(new List<DevExpress.XtraEditors.TextEdit>{ TxtSalary }, 0);
            ChkActInd.Checked = false;
            ChkRenewal.Checked = false;
            Sm.ClearGrd(Grd1, false);
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmEmpContractDocFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
                SetLueEmploymentStatus(ref LueEmploymentStatus, string.Empty);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, string.Empty, false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData();
                else
                    UpdateData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            string Doctitle = Sm.GetParameter("DocTitle");
            string GradeLevelCode = Sm.GetValue("Select GrdLvlCode From TblEmployee " +
                "Where EmpCode=@Param Order By EmpCode Desc Limit 1;",
                TxtEmpCode.Text);
            string OTIndicator = Sm.GetValue("SELECT FIND_IN_SET('" + GradeLevelCode + "','" + mGradeListOfContractDocument + "')");

            if (Sm.IsTxtEmpty(TxtDocNo, "Document#", false) || Sm.StdMsgYN("Print", "") == DialogResult.No) return;
            

            var l = new List<ContractDoc>();
            var l2 = new List<VIRFixAllowance>();
            var l3 = new List<VIRVariableAllowance>();
            var l4 = new List<VIRSocialSecurity>();


            string[] TableName = { "ContractDoc", "VIRFixAllowance", "VIRVariableAllowance", "VIRSocialSecurity" };

            List<IList> myLists = new List<IList>();


           
            #region Header
            var cm = new MySqlCommand();

            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo,(Select ParValue From tblparameter Where ParCode='ReportTitle1')As CompanyName, ");
            SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle2')As CompanyAddress, ");
            SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle3')As CompanyPhone, ");
            SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='ReportTitle4')As CompanyFax, ");
            SQL.AppendLine("(Select ParValue From tblparameter Where ParCode='FirstPartyName')As FirstPartyName, ");
            SQL.AppendLine(" A.DocNo, A.LocalDocNo, A.Remark, BB.TotalTunjangan, date_format(A.docdt, '%d %M %Y')As DocDt, date_format(A.docdt, '%d/%m/%Y')As DocDtVIR, date_format(A.StartDt,'%d %M %Y')As StartDt, date_format(A.EndDt,'%d %M %Y')As EndDt, ");
            SQL.AppendLine(" Case date_format(A.docdt,'%W') When 'Sunday' Then 'Minggu' When 'Monday' Then 'Senin' When 'Tuesday' Then 'Selasa' ");
            SQL.AppendLine(" When 'Wednesday' Then 'Rabu' When 'Thursday' Then 'Kamis' When 'Friday' Then 'Jumat' When 'Saturday' Then 'Sabtu' End As Day, ");
            SQL.AppendLine(" Case monthname(A.DocDt) When 'January' Then 'Januari' When 'February' Then 'Februari' When 'March' Then 'Maret' ");
            SQL.AppendLine(" When 'April' Then 'April' When 'May' Then 'Mei' When 'June' Then 'Juni' When 'July' Then 'Juli' When 'August' Then 'Agustus' ");
            SQL.AppendLine(" When 'September' Then 'September' When 'October' Then 'Oktober' When 'November' Then 'Nopember' When 'December' Then 'Desember' End As Month, ");
            SQL.AppendLine(" AAA.OptDesc As MaritalStatus, B.NPWP, Right(A.DocDt,2)As Dt, Left(A.DocDt,4)As Yr, B.EmpCode, B.EmpCodeOld, B.EmpName, B.IDNumber, B.BirthPlace, Date_Format(B.BirthDt, '%d %M %Y')As BirthDt, ");
            SQL.AppendLine(" B.Address, B.RTRW, K.Address As AddressEmp2, IfNull(H.SDName,'-')As SDName, IfNull(I.VilName,'-')As VilName, J.CityName, ");
            SQL.AppendLine(" C.SiteName, C.Address As SiteAddress, D.PosName, E.DeptName, F.GrdLvlName, G.LevelName, Z.Salary, K.EmpName As EmpName2, L.PosName As PosName2, C.HOInd, AA.OptDesc As Gender, ");
            SQL.AppendLine(" CC.SalaryAmt, CC.Brutto, CC.SSAmt, CC.THP ");
            SQL.AppendLine(" From TblEmpContractDoc A ");
            SQL.AppendLine(" Inner Join TblEmployee B On A.EmpCode=B.EmpCode ");
            SQL.AppendLine(" Left Join tblsite C On B.SiteCode=C.SiteCode ");
            SQL.AppendLine(" Left Join tblposition D On B.PosCode=D.PosCode ");
            SQL.AppendLine(" Left Join tbldepartment E On B.DeptCode=E.DeptCode ");
            SQL.AppendLine(" Left Join TblGradeLevelHdr F On B.GrdLvlCode=F.GrdLvlCode ");
            SQL.AppendLine(" Left Join TblLevelHdr G On F.LevelCode=G.LevelCode ");
            SQL.AppendLine(" left join tblsubdistrict H On B.SubDistrict=H.SDCode ");
            SQL.AppendLine(" left join tblvillage I On B.Village=I.VilCode ");
            SQL.AppendLine(" left join tblcity J On B.CityCode=J.CityCode ");
            SQL.AppendLine(" Left Join TblEmployee K On A.EmpCode2=K.EmpCode ");
            SQL.AppendLine(" Left Join tblposition L On K.PosCode=L.PosCode ");
            SQL.AppendLine(" left join ");
            SQL.AppendLine("( ");
            SQL.AppendLine("	Select EmpCode, max(Amt)As Salary From Tblemployeesalary Where ActInd='Y' ");
            SQL.AppendLine("	group By EmpCode ");
            SQL.AppendLine(")Z On A.EmpCode=Z.EmpCode ");
            SQL.AppendLine(" Inner Join tbloption AA on B.Gender = AA.OptCode And AA.OptCat = 'Gender' ");
            SQL.AppendLine(" INNER JOIN tbloption AAA ON B.MaritalStatus = AAA.OptCode AND AAA.OptCat='MaritalStatus' ");
            SQL.AppendLine(" left join ( ");
            SQL.AppendLine(" select A.empcode, sum(A.Amt) As TotalTunjangan ");
            SQL.AppendLine(" from tblemployeeallowancededuction A inner join tblallowancededuction B on A.ADCode = B.ADCode ");
            SQL.AppendLine(" where B.ADType='A' and B.AmtType = '1'");
            SQL.AppendLine(" group by A.empcode ");
            SQL.AppendLine(")BB On A.EmpCode = BB.EmpCode  ");
            //query tambahan untuk perhitungan gaji vir
            SQL.AppendLine(" LEFT JOIN   ");
            SQL.AppendLine(" (  ");
            SQL.AppendLine(" SELECT A.DocNo, A.EmpCode, IFNULL(C2.Amt, 0.00) SalaryAmt,  ");
            SQL.AppendLine(" (IFNULL(C2.Amt, 0.00) + IFNULL(D2.Amt, 0.00)) Brutto, IFNULL(E2.Amt, 0.00) SSAmt,  ");
            SQL.AppendLine(" ((IFNULL(C2.Amt, 0.00) + IFNULL(D2.Amt, 0.00)) - IFNULL(E2.Amt, 0.00)) THP  ");
            SQL.AppendLine(" FROM tblempcontractdoc A  ");
            SQL.AppendLine(" INNER JOIN TblParameter B ON B.ParCode = 'SalaryInd' AND B.ParValue = '2'  ");
            SQL.AppendLine("     AND A.DocNo = @DocNo  ");
            SQL.AppendLine(" LEFT JOIN  ");
            SQL.AppendLine(" (  ");
            SQL.AppendLine("     SELECT T1.EmpCode, T2.Amt  ");
            SQL.AppendLine("     FROM TblEmpContractDoc T1  ");
            SQL.AppendLine("     INNER JOIN TblEmployeeSalary T2 ON T1.EmpCode = T2.EmpCode  ");
            SQL.AppendLine("         AND T1.DocNo = @DocNo  ");
            SQL.AppendLine("         AND T2.ActInd = 'Y'  ");
            SQL.AppendLine("         AND T2.StartDt <= T1.EndDt  ");
            SQL.AppendLine("     ORDER BY T2.StartDt Desc  ");
            SQL.AppendLine("     LIMIT 1  ");
            SQL.AppendLine(" ) C2 ON A.EmpCode = C2.EmpCode  ");
            SQL.AppendLine(" LEFT JOIN  ");
            SQL.AppendLine(" (  ");
            SQL.AppendLine("     SELECT T1.EmpCode, SUM(T2.Amt) Amt ");
            SQL.AppendLine("     FROM TblEmpContractDoc T1 ");
            SQL.AppendLine("     INNER JOIN TblEmployeeAllowanceDeduction T2 ON T1.EmpCode = T2.EmpCode ");
            SQL.AppendLine("         AND T1.DocNo = @DocNo ");
            SQL.AppendLine("         AND T1.StartDt <= T2.EndDt ");
            SQL.AppendLine("         AND T1.EndDt <= T2.EndDt ");
            SQL.AppendLine("     GROUP BY T1.EmpCode ");
            SQL.AppendLine(" ) D2 ON A.EmpCode = D2.EmpCode ");
            SQL.AppendLine(" LEFT JOIN ");
            SQL.AppendLine(" ( ");
            SQL.AppendLine("     SELECT T1.EmpCode, SUM(T2.Amt) Amt ");
            SQL.AppendLine("     FROM TblEmpContractDoc T1 ");
            SQL.AppendLine("     INNER JOIN TblEmployeeSalarySS T2 ON T1.EmpCode = T2.EmpCode ");
            SQL.AppendLine("         AND T1.DocNo = @DocNo ");
            SQL.AppendLine("         AND T2.SSPCode IN ('001', '002')  ");
            SQL.AppendLine("         AND T1.StartDt <= T2.EndDt  ");
            SQL.AppendLine("         AND T1.EndDt <= T2.EndDt  ");
            SQL.AppendLine("     GROUP BY T1.EmpCode  ");
            SQL.AppendLine(" ) E2 ON A.EmpCode = E2.EmpCode ");
            SQL.AppendLine(" UNION ALL  ");
            SQL.AppendLine(" SELECT A.DocNo, A.EmpCode, IFNULL(D.BasicSalary, 0.00) SalaryAmt,  ");
            SQL.AppendLine(" (IFNULL(D.BasicSalary, 0.00) + IFNULL(E.Amt, 0.00)) Brutto,  ");
            SQL.AppendLine(" (IFNULL(D.HealthSSSalary, 0.00) + IFNULL(D.EmploymentSSSalary, 0.00)) SSAmt,  ");
            SQL.AppendLine(" ((IFNULL(D.BasicSalary, 0.00) + IFNULL(E.Amt, 0.00)) - (IFNULL(D.HealthSSSalary, 0.00) + IFNULL(D.EmploymentSSSalary, 0.00))) THP ");
            SQL.AppendLine(" FROM TblEmpContractDoc A  ");
            SQL.AppendLine(" INNER JOIN TblParameter B ON B.ParCode = 'SalaryInd' AND B.ParValue = '1'  ");
            SQL.AppendLine("     AND A.DocNo = @DocNo  "); 
            SQL.AppendLine(" INNER JOIN TblEmployee C ON A.EmpCode = C.EmpCode ");
            SQL.AppendLine(" LEFT JOIN  TblGradeLevelHdr D ON C.GrdLvlCode = D.GrdLvlCode ");
            SQL.AppendLine(" LEFT JOIN ");
            SQL.AppendLine(" (  ");
            SQL.AppendLine("     SELECT T1.EmpCode, SUM(T3.Amt) Amt  ");
            SQL.AppendLine("     FROM TblEmpContractDoc T1  ");
            SQL.AppendLine("     INNER JOIN TblEmployee T2 ON T1.EmpCode = T2.EmpCode  ");
            SQL.AppendLine("         AND T1.DocNo = @DocNo  ");
            SQL.AppendLine("     INNER JOIN TblGradeLevelDtl T3 ON T2.GrdLvlCode = T3.GrdLvlCode  ");
            SQL.AppendLine("     GROUP BY T1.EmpCode  ");
            SQL.AppendLine(" ) E ON A.EmpCode = E.EmpCode  ");
            SQL.AppendLine(" ) CC ON CC.DocNo=A.DocNo   ");
            SQL.AppendLine("where A.DocNo = @DocNo ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                        {
                         //0
                         "CompanyLogo",
                         
                         //1-5
                         "CompanyName",
                         "CompanyAddress",
                         "CompanyPhone",
                         "CompanyFax",
                         "DocNo", 

                         //6-10
                         "DocDt", 
                         "StartDt",
                         "EndDt",
                         "Day",
                         "Month",

                         //11-15
                         "Dt",
                         "Yr",
                         "EmpCode",
                         "EmpCodeOld",
                         "EmpName",

                         //16-20
                         "IDNumber",
                         "BirthPlace",
                         "BirthDt",
                         "Address",
                         "RTRW",

                         //21-25
                         "SDName",
                         "VilName",
                         "CityName",
                         "SiteName",
                         "SiteAddress",
                        
                         //26-30
                         "PosName",
                         "DeptName",
                         "GrdLvlName",
                         "LevelName",
                         "Salary",

                         //31-35
                         "EmpName2",
                         "PosName2",
                         "HOInd",
                         "Gender",
                         "Remark",

                         //36-40
                         "TotalTunjangan",
                         "LocalDocNo",
                         "FirstPartyName",
                         "NPWP",
                         "MaritalStatus",
                         
                         //41-45
                         "AddressEmp2",
                         "DocDtVIR",
                         "SalaryAmt",
                         "Brutto",
                         "SSAmt",

                         //46
                         "THP"
                        });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new ContractDoc()
                        {
                            CompanyLogo = Sm.DrStr(dr, c[0]),

                            CompanyName = Sm.DrStr(dr, c[1]),
                            CompanyAddress = Sm.DrStr(dr, c[2]),
                            CompanyPhone = Sm.DrStr(dr, c[3]),
                            CompanyFax = Sm.DrStr(dr, c[4]),
                            DocNo = Sm.DrStr(dr, c[5]),

                            DocDt = Sm.DrStr(dr, c[6]),
                            StartDt = Sm.DrStr(dr, c[7]),
                            EndDt = Sm.DrStr(dr, c[8]),
                            Day = Sm.DrStr(dr, c[9]),
                            Month = Sm.DrStr(dr, c[10]),

                            Dt = Sm.DrStr(dr, c[11]),
                            Yr = Sm.DrStr(dr, c[12]),
                            EmpCode = Sm.DrStr(dr, c[13]),
                            EmpCodeOld = Sm.DrStr(dr, c[14]),
                            EmpName = Sm.DrStr(dr, c[15]),

                            IDNumber = Sm.DrStr(dr, c[16]),
                            BirthPlace = Sm.DrStr(dr, c[17]),
                            BirthDt = Sm.DrStr(dr, c[18]),
                            Address = Sm.DrStr(dr, c[19]),
                            RTRW = Sm.DrStr(dr, c[20]),

                            SDName = Sm.DrStr(dr, c[21]),
                            VilName = Sm.DrStr(dr, c[22]),
                            CityName = Sm.DrStr(dr, c[23]),
                            SiteName = Sm.DrStr(dr, c[24]),
                            SiteAddress = Sm.DrStr(dr, c[25]),

                            Posname = Sm.DrStr(dr, c[26]),
                            DeptName = Sm.DrStr(dr, c[27]),
                            GrdLvlName = Sm.DrStr(dr, c[28]),
                            LevelName = Sm.DrStr(dr, c[29]),
                            TerbilangDt = Terbilang(Sm.DrDec(dr, c[11])),

                            TerbilangYr = Terbilang2(Sm.DrDec(dr, c[12])),
                            Salary = Sm.DrDec(dr, c[30]),
                            Terbilangsalary = Sm.Terbilang(Sm.DrDec(dr, c[30])),
                            EmpName2 = Sm.DrStr(dr, c[31]),
                            PosName2 = Sm.DrStr(dr, c[32]),

                            HOInd = Sm.DrStr(dr, c[33]),
                            Gender = Sm.DrStr(dr, c[34]),
                            Remark = Sm.DrStr(dr, c[35]),
                            TotalTunjangan = Sm.DrDec(dr, c[36]),
                            OTInd = decimal.Parse(OTIndicator) > 0 ? "Y" : "N",

                            LocalDocNo = Sm.DrStr(dr, c[37]),
                            FirstPartyName = Sm.DrStr(dr, c[38]),
                            NPWP = Sm.DrStr(dr, c[39]),
                            MaritalStatus = Sm.DrStr(dr, c[40]),
                            AddressEmp2 = Sm.DrStr(dr, c[41]),

                            DocDtVIR = Sm.DrStr(dr, c[42]),
                            SalaryAmt = Sm.DrDec(dr, c[43]),
                            Bruto = Sm.DrDec(dr, c[44]),
                            SSAmt = Sm.DrDec(dr, c[45]),
                            THP = Sm.DrDec(dr, c[46]),
    
                        });
                    }
                }
                dr.Close();
            }

            myLists.Add(l);
            #endregion

            #region VIR Fix Allowance

            var cm2 = new MySqlCommand();
            var SQL2 = new StringBuilder();

            
            SQL2.AppendLine(" SELECT A.DocNo, A.EmpCode, D.ADName, C.Amt ");
            SQL2.AppendLine(" FROM TblEmpContractDoc A ");
            SQL2.AppendLine(" INNER JOIN TblParameter B ON B.Parcode = 'SalaryInd' AND B.ParValue = '2' ");
            SQL2.AppendLine("     AND A.DocNo = @DocNo ");
            SQL2.AppendLine(" INNER JOIN TblEmployeeAllowanceDeduction C ON A.EmpCode = C.EmpCode ");
            SQL2.AppendLine("     AND A.StartDt <= C.EndDt ");
            SQL2.AppendLine("     AND A.EndDt <= C.EndDt ");
            SQL2.AppendLine(" INNER JOIN TblAllowanceDeduction D ON C.ADCode = D.ADCode ");
            SQL2.AppendLine("     AND D.ADType = 'A' ");
            SQL2.AppendLine("     AND D.AmtType = '1' ");
            SQL2.AppendLine(" UNION ALL ");
            SQL2.AppendLine(" SELECT A.DocNo, A.EmpCode, E.ADName, D.Amt ");
            SQL2.AppendLine(" FROM TblEmpContractDoc A ");
            SQL2.AppendLine(" INNER JOIN TblParameter B ON B.Parcode = 'SalaryInd' AND B.ParValue = '1' ");
            SQL2.AppendLine("     AND A.DocNo = @DocNo ");
            SQL2.AppendLine(" INNER JOIN TblEmployee C ON A.EmpCode = C.EmpCode ");
            SQL2.AppendLine(" INNER JOIN TblGradeLevelDtl D ON C.GrdLvlCode = D.GrdLvlCode ");
            SQL2.AppendLine(" INNER JOIN TblAllowanceDeduction E ON D.ADCode = E.ADCode ");
            SQL2.AppendLine("     AND E.ADType = 'A' ");
            SQL2.AppendLine("     AND E.AmtType = '1' ");
            SQL2.AppendLine(" ; ");

            using (var cn2 = new MySqlConnection(Gv.ConnectionString))
            {
                cn2.Open();
                cm2.Connection = cn2;
                cm2.CommandText = SQL2.ToString();
                Sm.CmParam<String>(ref cm2, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cm2, "@CompanyLogo", @Sm.CompanyLogo());
                var dr2 = cm2.ExecuteReader();
                var c2 = Sm.GetOrdinal(dr2, new string[] 
                        {
                         //0
                         "DocNo",
                         
                         //1-5
                         "EmpCode",
                         "ADName",
                         "Amt"
                         
                        });
                if (dr2.HasRows)
                {
                    while (dr2.Read())
                    {
                        l2.Add(new VIRFixAllowance()
                        {
                            DocNo = Sm.DrStr(dr2, c2[0]),

                            EmpCode = Sm.DrStr(dr2, c2[1]),
                            AdName = Sm.DrStr(dr2, c2[2]),
                            Amt = Sm.DrDec(dr2, c2[3])

                        });
                    }
                }
                dr2.Close();
            }

            myLists.Add(l2);


            #endregion

            #region VIR Variable Allowance

            var cm3 = new MySqlCommand();
            var SQL3 = new StringBuilder();

            SQL3.AppendLine("SELECT A.DocNo, A.EmpCode, D.ADName, C.Amt ");
            SQL3.AppendLine("FROM TblEmpContractDoc A ");
            SQL3.AppendLine("INNER JOIN TblParameter B ON B.Parcode = 'SalaryInd' AND B.ParValue = '2' ");
            SQL3.AppendLine("    AND A.DocNo = @DocNo ");
            SQL3.AppendLine("INNER JOIN TblEmployeeAllowanceDeduction C ON A.EmpCode = C.EmpCode ");
            SQL3.AppendLine("    AND A.StartDt <= C.EndDt ");
            SQL3.AppendLine("    AND A.EndDt <= C.EndDt ");
            SQL3.AppendLine("INNER JOIN TblAllowanceDeduction D ON C.ADCode = D.ADCode ");
            SQL3.AppendLine("    AND D.ADType = 'A' ");
            SQL3.AppendLine("    AND D.AmtType = '2' ");
            SQL3.AppendLine("UNION ALL ");
            SQL3.AppendLine("SELECT A.DocNo, A.EmpCode, E.ADName, D.Amt ");
            SQL3.AppendLine("FROM TblEmpContractDoc A ");
            SQL3.AppendLine("INNER JOIN TblParameter B ON B.Parcode = 'SalaryInd' AND B.ParValue = '1' ");
            SQL3.AppendLine("    AND A.DocNo = @DocNo ");
            SQL3.AppendLine("INNER JOIN TblEmployee C ON A.EmpCode = C.EmpCode ");
            SQL3.AppendLine("INNER JOIN TblGradeLevelDtl D ON C.GrdLvlCode = D.GrdLvlCode ");
            SQL3.AppendLine("INNER JOIN TblAllowanceDeduction E ON D.ADCode = E.ADCode ");
            SQL3.AppendLine("    AND E.ADType = 'A' ");
            SQL3.AppendLine("    AND E.AmtType = '2' ");
            SQL3.AppendLine("; ");

            using (var cn3 = new MySqlConnection(Gv.ConnectionString))
            {
                cn3.Open();
                cm3.Connection = cn3;
                cm3.CommandText = SQL3.ToString();
                Sm.CmParam<String>(ref cm3, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cm3, "@CompanyLogo", @Sm.CompanyLogo());
                var dr3 = cm3.ExecuteReader();
                var c3 = Sm.GetOrdinal(dr3, new string[] 
                        {
                         //0
                         "DocNo",
                         
                         //1-5
                         "EmpCode",
                         "ADName",
                         "Amt"
                         
                        });
                if (dr3.HasRows)
                {
                    while (dr3.Read())
                    {
                        l3.Add(new VIRVariableAllowance()
                        {
                            DocNo = Sm.DrStr(dr3, c3[0]),

                            EmpCode = Sm.DrStr(dr3, c3[1]),
                            AdName = Sm.DrStr(dr3, c3[2]),
                            Amt = Sm.DrDec(dr3, c3[3])

                        });
                    }
                }
                dr3.Close();
            }

            myLists.Add(l3);
            #endregion

            #region VIR Social Security

            var cm4 = new MySqlCommand();
            var SQL4 = new StringBuilder();

            SQL4.AppendLine("SELECT A.DocNo, A.EmpCode, D.SSPName, C.Amt ");
            SQL4.AppendLine("FROM TblEmpContractDoc A ");
            SQL4.AppendLine("INNER JOIN TblParameter B ON B.ParCode = 'SalaryInd' AND B.ParValue = '2' ");
            SQL4.AppendLine("    AND A.DocNo = @DocNo ");
            SQL4.AppendLine("INNER JOIN TblEmployeeSalarySS C ON A.EmpCode = C.EmpCode ");
            SQL4.AppendLine("    AND C.SSPCode IN ('001', '002') ");
            SQL4.AppendLine("    AND A.StartDt <= C.EndDt ");
            SQL4.AppendLine("    AND A.EndDt <= C.EndDt ");
            SQL4.AppendLine("INNER JOIN TblSSProgram D ON C.SSPCode = D.SSPCode ");
            SQL4.AppendLine("UNION ALL ");
            SQL4.AppendLine("SELECT A.DocNo, A.EmpCode, E.SSPName, D.HealthSSSalary Amt ");
            SQL4.AppendLine("FROM TblEmpContractDoc A ");
            SQL4.AppendLine("INNER JOIN TblParameter B ON B.ParCode = 'SalaryInd' AND B.Parvalue = '1' ");
            SQL4.AppendLine("    AND A.DocNo = @DocNo ");
            SQL4.AppendLine("INNER JOIN TblEmployee C ON A.EmpCode = C.EmpCode ");
            SQL4.AppendLine("INNER JOIN TblGradeLevelHdr D ON C.GrdLvlCode = D.GrdLvlCode ");
            SQL4.AppendLine("INNER JOIN TblSSProgram E ON E.SSPCode = '001' ");
            SQL4.AppendLine("UNION ALL ");
            SQL4.AppendLine("SELECT A.DocNo, A.EmpCode, E.SSPName, D.EmploymentSSSalary Amt ");
            SQL4.AppendLine("FROM TblEmpContractDoc A ");
            SQL4.AppendLine("INNER JOIN TblParameter B ON B.ParCode = 'SalaryInd' AND B.Parvalue = '1' ");
            SQL4.AppendLine("    AND A.DocNo = @DocNo ");
            SQL4.AppendLine("INNER JOIN TblEmployee C ON A.EmpCode = C.EmpCode ");
            SQL4.AppendLine("INNER JOIN TblGradeLevelHdr D ON C.GrdLvlCode = D.GrdLvlCode ");
            SQL4.AppendLine("INNER JOIN TblSSProgram E ON E.SSPCode = '002'; ");



            using (var cn4 = new MySqlConnection(Gv.ConnectionString))
            {
                cn4.Open();
                cm4.Connection = cn4;
                cm4.CommandText = SQL4.ToString();
                Sm.CmParam<String>(ref cm4, "@DocNo", TxtDocNo.Text);
                Sm.CmParam<String>(ref cm4, "@CompanyLogo", @Sm.CompanyLogo());
                var dr4 = cm4.ExecuteReader();
                var c4 = Sm.GetOrdinal(dr4, new string[] 
                        {
                         //0
                         "DocNo",
                         
                         //1-5
                         "EmpCode",
                         "SSPName",
                         "Amt"
                         
                        });
                if (dr4.HasRows)
                {
                    while (dr4.Read())
                    {
                        l4.Add(new VIRSocialSecurity()
                        {
                            DocNo = Sm.DrStr(dr4, c4[0]),

                            EmpCode = Sm.DrStr(dr4, c4[1]),
                            SSPName = Sm.DrStr(dr4, c4[2]),
                            Amt = Sm.DrDec(dr4, c4[3])

                        });
                    }
                }
                dr4.Close();
            }

            myLists.Add(l4);
            #endregion


            if (mFormPrintOutEmpContractDoc.Length > 0)
                Sm.PrintReport(mFormPrintOutEmpContractDoc, myLists, TableName, false);
            else
                Sm.PrintReport("ContractDoc", myLists, TableName, false);
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "EmpContractDoc", "TblEmpContractDoc");
            var PPSDocNo = string.Empty; 
            bool IsEmploymentStatusChanged = false;

            if (mIsEmpContractDocInclEmpSts)
            {
                IsEmploymentStatusChanged = IsEmployeeEmploymentStatusChanged();
                if (IsEmploymentStatusChanged) 
                    PPSDocNo=Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "PPS", "TblPPS");
            }
            

            var cml = new List<MySqlCommand>();

            cml.Add(SaveEmpContractDoc(DocNo, PPSDocNo, IsEmploymentStatusChanged));

            Sm.ExecCommands(cml);
            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Document's date") ||
                Sm.IsTxtEmpty(TxtEmpCode, "Employee", false)||
                (mIsEmpContractDocInclEmpSts && Sm.IsLueEmpty(LueEmploymentStatus, "Employment status")) ||
                Sm.IsDteEmpty(DteStartDt, "Start date") ||
                Sm.IsDteEmpty(DteEndDt, "End date")||
                (mIsEmployeeManagementRepresentative && Sm.IsTxtEmpty(TxtEmpCode2, "Employee management representative", false)) ||
                IsDataInActiveAlready()||
                IsDateNotValid() ||
                (mIsEmpContractDocInclEmpSts && IsStartDtNotValid()) ||
                IsContractExtInvalid();
        }

        private bool IsContractExtInvalid()
        {
            if (!mIsEmpContractDocExtValidationEnabled) return false;

              var SQL = new StringBuilder();

            SQL.AppendLine("Select 1 From TblEmpContractDoc ");
            SQL.AppendLine("Where ActInd='Y' ");
            SQL.AppendLine("And EmpCode=@EmpCode ");
            SQL.AppendLine("And ContractExt>=@ContractExt ");
            SQL.AppendLine("Limit 1;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            
            Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
            Sm.CmParam<Decimal>(ref cm, "@ContractExt", decimal.Parse(TxtContractExt.Text));

            return Sm.IsDataExist(cm, "Invalid contract extension.");
        }

        private bool IsStartDtNotValid()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select StartDt From TblEmployeePPS ");
            SQL.AppendLine("Where StartDt>=@StartDt ");
            SQL.AppendLine("And EmpCode=@EmpCode ");
            SQL.AppendLine("Limit 1;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);

            var StartDt = Sm.GetValue(cm);
            if (StartDt.Length > 0)
            {
                Sm.StdMsg(mMsgType.Warning,
                    "Invalid start date. " + Environment.NewLine +
                    "Start date should not be earlier than/equal as " + Sm.DateValue(StartDt) + ".");
                return true;
            }
            return false;
        }

        private bool IsDataInActiveAlready()
        {
            return 
                Sm.IsDataExist(
                    "Select 1 From TblEmpContractDoc Where ActInd='N' And DocNo=@Param;",
                    TxtDocNo.Text, 
                    "This document already inactive.");
        }

        private bool IsDateNotValid()
        {
            if (Convert.ToInt32(Sm.GetDte(DteEndDt).Substring(0, 8))< Convert.ToInt32(Sm.GetDte(DteStartDt).Substring(0, 8)))
            {
                Sm.StdMsg(mMsgType.Warning, "End date not valid");
                return true;
            }
            return false;
        }

        private bool IsEmployeeEmploymentStatusChanged()
        {
            var EmploymentStatusNew = Sm.GetLue(LueEmploymentStatus);
            var EmploymentStatusOld = Sm.GetValue(
                "Select EmploymentStatus From TblEmployeePPS " +
                "Where EmpCode=@Param Order By StartDt Desc Limit 1;",
                TxtEmpCode.Text);

            return !Sm.CompareStr(EmploymentStatusOld, EmploymentStatusNew);
        }

        private MySqlCommand SaveEmpContractDoc(string DocNo, string PPSDocNo, bool IsEmploymentStatusChanged)
        {
            var SQL = new StringBuilder();
            SQL.AppendLine("Insert Into TblEmpContractDoc ");
            SQL.AppendLine("(DocNo, DocDt, ActInd, LocalDocNo, EmpCode, EmpCode2, ");
            if (mIsEmpContractDocInclEmpSts)
                SQL.AppendLine("EmploymentStatus, ");
            SQL.AppendLine("ContractExt, StartDt, EndDt, RenewalInd, Remark, ");
            SQL.AppendLine("CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @ActInd, @LocalDocNo, @EmpCode, @EmpCode2, ");
            if (mIsEmpContractDocInclEmpSts)
                SQL.AppendLine("@EmploymentStatus, ");
            SQL.AppendLine("@ContractExt, @StartDt, @EndDt, @RenewalInd, @Remark, ");
            SQL.AppendLine("@CreateBy, CurrentDateTime()); ");
            
           /* if (!ChkRenewal.Checked)
            {
                SQL.AppendLine("Update TblEmployee ");
                SQL.AppendLine("Set ResignDt=@EndDt ");
                SQL.AppendLine("Where EmpCode=@EmpCode;");
            }
            */
            if (mIsEmpContractDocInclEmpSts)
            {
                if (IsEmploymentStatusChanged)
                {
                    SQL.AppendLine("Insert Into TblPPS(DocNo, DocDt, CancelInd, Status, Jobtransfer, StartDt, EmpCode, ProposeCandidateDocNo, ProposeCandidateDNo, ");
                    SQL.AppendLine("DeptCodeOld, PosCodeOld, GrdLvlCodeOld, EmploymentStatusOld, SystemTypeOld, PayrunPeriodOld, PGCodeOld, SiteCodeOld, ResignDtOld, ");
                    SQL.AppendLine("DeptCodeNew, PosCodeNew, GrdLvlCodeNew, EmploymentStatusNew, SystemTypeNew, PayrunPeriodNew, PGCodeNew, SiteCodeNew, ResignDtNew, UpdLeaveStartDtInd, Remark, ");
                    SQL.AppendLine("CreateBy, CreateDt) ");
                    SQL.AppendLine("Select @PPSDocNo, @DocDt, 'N', 'A', 'M', @StartDt, @EmpCode, Null, Null, ");
                    SQL.AppendLine("DeptCode, PosCode, GrdLvlCode, EmploymentStatus, SystemType, PayrunPeriod, PGCode, SiteCode, Null, ");
                    SQL.AppendLine("DeptCode, PosCode, GrdLvlCode, @EmploymentStatus, SystemType, PayrunPeriod, PGCode, SiteCode, Null, 'N', 'Automatic Generated From Employee Contract Document.', ");
                    SQL.AppendLine("@CreateBy, CurrentDateTime() ");
                    SQL.AppendLine("From TblEmployeePPS ");
                    SQL.AppendLine("Where EmpCode=@EmpCode Order By StartDt Desc Limit 1;");

                    SQL.AppendLine("Update TblEmployeePPS Set ");
                    SQL.AppendLine("    EndDt=@PPSEndDt ");
                    SQL.AppendLine("Where EmpCode=@EmpCode ");
                    SQL.AppendLine("And StartDt=( ");
                    SQL.AppendLine("    Select StartDt From (");
                    SQL.AppendLine("        Select Max(StartDt) StartDt ");
                    SQL.AppendLine("        From TblEmployeePPS ");
                    SQL.AppendLine("        Where EmpCode=@EmpCode ");
                    SQL.AppendLine("        And StartDt<@StartDt ");
                    SQL.AppendLine("    ) T );");

                    SQL.AppendLine("Insert Into TblEmployeePPS(EmpCode, StartDt, EndDt, ");
                    SQL.AppendLine("DeptCode, PosCode, GrdLvlCode, EmploymentStatus, SystemType, PayrunPeriod, PGCode, SiteCode, ");
                    SQL.AppendLine("CreateBy, CreateDt) ");
                    SQL.AppendLine("Select EmpCode, StartDt, Null, ");
                    SQL.AppendLine("DeptCodeNew, PosCodeNew, GrdLvlCodeNew, EmploymentStatusNew, SystemTypeNew, PayrunPeriodNew, PGCodeNew, SiteCodeNew, ");
                    SQL.AppendLine("CreateBy, CreateDt ");
                    SQL.AppendLine("From TblPPS Where DocNo=@PPSDocNo;");

                    SQL.AppendLine("Update TblPPS Set ");
                    SQL.AppendLine("    CancelInd='Y', LastUpBy=@CreateBy, LastUpDt=CurrentDateTime() ");
                    SQL.AppendLine("Where EmpCode=@EmpCode ");
                    SQL.AppendLine("And Status='O' ");
                    SQL.AppendLine("And CancelInd='N'; ");

                    if (Decimal.Parse(Sm.GetDte(DteStartDt).Substring(0, 8)) <= 
                        Decimal.Parse(Sm.GetValue("Select Replace(CurDate(), '-', '');")))
                    {
                        if (Sm.IsDataExist(
                            "Select 1 From TblEmployeePPS Where EmpCode=@Param Limit 1;", TxtEmpCode.Text))
                        {

                            SQL.AppendLine("Update TblEmployee A ");
                            SQL.AppendLine("Inner Join TblPPS B On B.DocNo=@PPSDocNo ");
                            SQL.AppendLine("Left Join TblSite C On B.SiteCodeNew=C.SiteCode ");
                            SQL.AppendLine("Left Join TblProfitCenter D On C.ProfitCenterCode=D.ProfitCenterCode ");
                            SQL.AppendLine("Set ");
                            SQL.AppendLine("    A.DeptCode=B.DeptCodeNew, ");
                            SQL.AppendLine("    A.PosCode=B.PosCodeNew, ");
                            SQL.AppendLine("    A.GrdLvlCode=B.GrdLvlCodeNew, ");
                            SQL.AppendLine("    A.EmploymentStatus=B.EmploymentStatusNew, ");
                            SQL.AppendLine("    A.SystemType=B.SystemTypeNew, ");
                            SQL.AppendLine("    A.PayrunPeriod=B.PayrunPeriodNew, ");
                            SQL.AppendLine("    A.PGCode=B.PGCodeNew, ");
                            SQL.AppendLine("    A.SiteCode=B.SiteCodeNew, ");
                            SQL.AppendLine("    A.EntCode=D.EntCode, ");
                            SQL.AppendLine("    A.LastUpBy=@CreateBy, ");
                            SQL.AppendLine("    A.LastUpDt=CurrentDateTime() ");
                            SQL.AppendLine("Where A.EmpCode=@EmpCode; ");

                            SQL.AppendLine("Update TblEmployeePPS Set ");
                            SQL.AppendLine("    ProcessInd='Y' ");
                            SQL.AppendLine("Where EmpCode=@EmpCode ");
                            SQL.AppendLine("And ProcessInd='N' ");
                            SQL.AppendLine("And EndDt Is Null; ");
                        }
                        else
                        {
                            SQL.AppendLine("Update TblEmployee Set ");
                            SQL.AppendLine("    EmploymentStatus=@EmploymentStatus, ");
                            SQL.AppendLine("    LastUpBy=@CreateBy, ");
                            SQL.AppendLine("    LastUpDt=CurrentDateTime() ");
                            SQL.AppendLine("Where EmpCode=@EmpCode; ");
                        }
                    }
                }
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@ActInd", ChkActInd.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@LocalDocNo", TxtLocalDocNo.Text);
            Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
            if (mIsEmpContractDocInclEmpSts)
                Sm.CmParam<String>(ref cm, "@EmploymentStatus", Sm.GetLue(LueEmploymentStatus));
            Sm.CmParam<Decimal>(ref cm, "@ContractExt", decimal.Parse(TxtContractExt.Text));
            Sm.CmParamDt(ref cm, "@StartDt", Sm.GetDte(DteStartDt));
            Sm.CmParamDt(ref cm, "@EndDt", Sm.GetDte(DteEndDt));
            Sm.CmParam<String>(ref cm, "@RenewalInd", ChkRenewal.Checked ? "Y" : "N");
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@PPSDocNo", PPSDocNo);
            Sm.CmParam<String>(ref cm, "@PPSEndDt", (Sm.ConvertDate(Sm.GetDte(DteStartDt)).AddDays(-1)).ToString("yyyyMMdd"));
            Sm.CmParam<String>(ref cm, "@EmpCode2", TxtEmpCode2.Text);

            return cm;
        }

        #endregion

        #region Edit Data

        private void UpdateData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || 
                IsUpdateDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(UpdateEmpContractDoc());
            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsUpdateDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDataInActiveAlready();
        }

        private MySqlCommand UpdateEmpContractDoc()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblEmpContractDoc Set ");
            SQL.AppendLine("    ActInd='N', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo; ");

           /* if (!ChkRenewal.Checked)
            {
                SQL.AppendLine("Update TblEmployee Set ResignDt= null ");
                SQL.AppendLine("Where EmpCode =@EmpCode ");
            }*/

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowEmpContractDoc(DocNo);
                ShowContractExtHistory();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowEmpContractDoc(string DocNo)
        {

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.ActInd, A.LocalDocNo, A.EmpCode, B.EmpName, C.DeptName,  ");
            SQL.AppendLine("D.PosName, E.GrdLvlName, E.BasicSalary, A.ContractExt, A.StartDt, A.EndDt, ");
            SQL.AppendLine("A.RenewalInd, A.Remark, A.EmploymentStatus,  A.EmpCode2, F.EmpName as EmpName2, G.PosName As PosName2, H.SiteName ");
            SQL.AppendLine("From TblEmpContractDoc A  ");
            SQL.AppendLine("Left Join TblEmployee B On A.EmpCode = B.EmpCode "); 
            SQL.AppendLine("Left Join TblDepartment C On B.DeptCode = C.DeptCode "); 
            SQL.AppendLine("Left Join TblPosition D On B.PosCode = D.PosCode ");
            SQL.AppendLine("Left Join TblGradeLevelHdr E On B.GrdLvlCode = E.GrdLvlCode  ");
            SQL.AppendLine("Left Join TblEmployee F On A.EmpCode2=F.EmpCode ");
            SQL.AppendLine("Left Join TblPosition G On F.PosCode = G.PosCode ");
            SQL.AppendLine("Left Join TblSite H On B.SiteCode = H.SiteCode ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            Sm.ShowDataInCtrl(
               ref cm, SQL.ToString(),
               new string[] { 
                    //0
                    "DocNo", 
                
                    //1-5
                    "DocDt", "LocalDocNo", "ActInd", "EmpCode", "EmpName",   
                    
                    //6-10
                    "DeptName", "PosName", "GrdLvlName", "BasicSalary", "ContractExt", 
                    
                    //11-15
                    "StartDt", "EndDt", "RenewalInd", "Remark", "EmploymentStatus",

                    //16-18
                    "EmpCode2", "EmpName2", "PosName2", "SiteName"
                },
                (MySqlDataReader dr, int[] c) =>
                {
                    TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                    Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                    TxtLocalDocNo.EditValue = Sm.DrStr(dr, c[2]);
                    ChkActInd.Checked = Sm.DrStr(dr, c[3]) == "Y";
                    TxtEmpCode.EditValue = Sm.DrStr(dr, c[4]);
                    TxtEmpName.EditValue = Sm.DrStr(dr, c[5]);
                    TxtDeptName.EditValue = Sm.DrStr(dr, c[6]);
                    TxtPosition.EditValue = Sm.DrStr(dr, c[7]);
                    TxtGrade.EditValue = Sm.DrStr(dr, c[8]);
                    TxtSalary.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[9]), 0);
                    TxtContractExt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[10]), 11);
                    Sm.SetDte(DteStartDt, Sm.DrStr(dr, c[11]));
                    Sm.SetDte(DteEndDt, Sm.DrStr(dr, c[12])); 
                    ChkRenewal.Checked = Sm.DrStr(dr, c[13]) == "Y";
                    MeeRemark.EditValue = Sm.DrStr(dr, c[14]);
                    SetLueEmploymentStatus(ref LueEmploymentStatus, Sm.DrStr(dr, c[15]));
                    TxtEmpCode2.EditValue = Sm.DrStr(dr, c[16]);
                    TxtEmpName2.EditValue = Sm.DrStr(dr, c[17]);
                    TxtPosition2.EditValue = Sm.DrStr(dr, c[18]);
                    TxtSite.EditValue = Sm.DrStr(dr, c[19]);
                }, true
            );
        }

        internal void ShowContractExtHistory()
        {
            Sm.ClearGrd(Grd1, false);
            Cursor.Current = Cursors.WaitCursor;
            try
            {
                var SQL = new StringBuilder();
                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@EmpCode", TxtEmpCode.Text);

                SQL.AppendLine("Select DocNo, DocDt, LocalDocNo, ContractExt, StartDt, EndDt ");
                SQL.AppendLine("From TblEmpContractDoc ");
                SQL.AppendLine("Where EmpCode=@EmpCode ");
                SQL.AppendLine("And ActInd='Y' ");
                SQL.AppendLine("Order By ContractExt, StartDt;");

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm, SQL.ToString(),
                        new string[] 
                        { 
                            //0
                            "DocNo", 
                            
                            //1-5
                            "DocDt", "LocalDocNo", "ContractExt", "StartDt", "EndDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 6, 5);
                        }, false, false, false, false
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mIsNotFilterByAuthorization = Sm.GetParameter("IsPayrollDataFilterByAuthorization") == "N";
            mIsEmpContractDocInclEmpSts = Sm.GetParameterBoo("IsEmpContractDocInclEmpSts");
            mIsFilteredByEmploymentStatus = Sm.GetParameter("EmpContractDocEmploymentStatus").Length > 0;
            mIsEmpContractDocExtValidationEnabled = Sm.GetParameterBoo("IsEmpContractDocExtValidationEnabled");
            mIsEmployeeManagementRepresentative = Sm.GetParameterBoo("IsEmployeeManagementRepresentative");
            mIsFilterBySiteHR = Sm.GetParameterBoo("IsFilterBySiteHR");
            mFormPrintOutEmpContractDoc = Sm.GetParameter("FormPrintOutEmpContractDoc");
            mGradeListOfContractDocument = Sm.GetParameter("GradeListOfContractDocument");
            mIsContractExtensionAutoFill = Sm.GetParameterBoo("IsContractExtensionAutoFill");
        }

        private void SetLueEmploymentStatus(ref LookUpEdit Lue, string Code)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select OptCode As Col1, OptDesc As Col2 ");
            SQL.AppendLine("From TblOption Where OptCat='EmploymentStatus' ");
            if (Code.Length > 0)
            {
                SQL.AppendLine("And OptCode=@Code; ");
            }
            else
            {
                if (mIsFilteredByEmploymentStatus)
                {
                    SQL.AppendLine("And Find_In_Set(OptCode, ");
                    SQL.AppendLine("(Select ParValue From TblParameter ");
                    SQL.AppendLine("Where ParCode='EmpContractDocEmploymentStatus' And ParValue Is Not Null)) ");
                }
                SQL.AppendLine("Order By OptDesc;");
            }

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@Code", Code);

            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (Code.Length > 0) Sm.SetLue(Lue, Code);
        }

        private static string Terbilang(Decimal d)
        {
            d = Decimal.Round(Math.Abs(d));

            string strHasil = string.Empty;
            Decimal frac = d - Decimal.Truncate(d);

            if (Decimal.Compare(frac, 0.0m) != 0)
                strHasil = Terbilang(Decimal.Round(frac * 100)) + " sen";
            else
                strHasil = "";

            int nDigit = 0;
            int nPosisi = 0;

            string strTemp = Decimal.Truncate(d).ToString();
            for (int i = strTemp.Length; i > 0; i--)
            {
                string tmpBuff = string.Empty;
                nDigit = Convert.ToInt32(strTemp.Substring(i - 1, 1));
                nPosisi = (strTemp.Length - i) + 1;
                switch (nPosisi % 3)
                {
                    case 1:
                        bool bAllZeros = false;
                        if (i == 1)
                            tmpBuff = satuan[nDigit] + " ";
                        else if (strTemp.Substring(i - 2, 1) == "1")
                            tmpBuff = belasan[nDigit] + " ";
                        else if (nDigit > 0)
                            tmpBuff = satuan[nDigit] + " ";
                        else
                        {
                            bAllZeros = true;
                            if (i > 1)
                                if (strTemp.Substring(i - 2, 1) != "0") bAllZeros = false;
                            if (i > 2)
                                if (strTemp.Substring(i - 3, 1) != "0") bAllZeros = false;
                            tmpBuff = string.Empty;
                        }

                        if ((!bAllZeros) && (nPosisi > 1))
                        {
                            if ((strTemp.Length == 4) && (strTemp.Substring(0, 1) == "1"))
                                tmpBuff = "se" + ribuan[(int)Decimal.Round(nPosisi / 3m)] + " ";
                            else
                                tmpBuff = tmpBuff + ribuan[(int)Decimal.Round(nPosisi / 3)] + " ";
                        }
                        strHasil = tmpBuff + strHasil;
                        break;
                    case 2:
                        if (nDigit > 0) strHasil = puluhan[nDigit] + " " + strHasil;
                        break;
                    case 0:
                        if (nDigit > 0)
                        {
                            if (nDigit == 1)
                                strHasil = "Seratus " + strHasil;
                            else
                                strHasil = satuan[nDigit] + " Ratus " + strHasil;
                        }
                        break;
                }
            }
            strHasil = strHasil.Trim().ToLower();
            if (strHasil.Length > 0)
            {
                strHasil = strHasil.Substring(0, 1).ToUpper() +
                  strHasil.Substring(1, strHasil.Length - 1);
            }

            return strHasil;
        }

        private static string Terbilang2(Decimal d)
        {
            d = Decimal.Round(Math.Abs(d));

            string strHasil = string.Empty;
            Decimal frac = d - Decimal.Truncate(d);

            if (Decimal.Compare(frac, 0.0m) != 0)
                strHasil = Terbilang(Decimal.Round(frac * 100)) + " sen";
            else
                strHasil = "";

            int nDigit = 0;
            int nPosisi = 0;

            string strTemp = Decimal.Truncate(d).ToString();
            for (int i = strTemp.Length; i > 0; i--)
            {
                string tmpBuff = string.Empty;
                nDigit = Convert.ToInt32(strTemp.Substring(i - 1, 1));
                nPosisi = (strTemp.Length - i) + 1;
                switch (nPosisi % 3)
                {
                    case 1:
                        bool bAllZeros = false;
                        if (i == 1)
                            tmpBuff = satuan[nDigit] + " ";
                        else if (strTemp.Substring(i - 2, 1) == "1")
                            tmpBuff = belasan[nDigit] + " ";
                        else if (nDigit > 0)
                            tmpBuff = satuan[nDigit] + " ";
                        else
                        {
                            bAllZeros = true;
                            if (i > 1)
                                if (strTemp.Substring(i - 2, 1) != "0") bAllZeros = false;
                            if (i > 2)
                                if (strTemp.Substring(i - 3, 1) != "0") bAllZeros = false;
                            tmpBuff = string.Empty;
                        }

                        if ((!bAllZeros) && (nPosisi > 1))
                        {
                            if ((strTemp.Length == 4) && (strTemp.Substring(0, 1) == "1"))
                                tmpBuff = "se" + ribuan[(int)Decimal.Round(nPosisi / 3m)] + " ";
                            else
                                tmpBuff = tmpBuff + ribuan[(int)Decimal.Round(nPosisi / 3)] + " ";
                        }
                        strHasil = tmpBuff + strHasil;
                        break;
                    case 2:
                        if (nDigit > 0) strHasil = puluhan[nDigit] + " " + strHasil;
                        break;
                    case 0:
                        if (nDigit > 0)
                        {
                            if (nDigit == 1)
                                strHasil = "Seratus " + strHasil;
                            else
                                strHasil = satuan[nDigit] + " Ratus " + strHasil;
                        }
                        break;
                }
            }
            strHasil = strHasil.Trim().ToLower();
            if (strHasil.Length > 0)
            {
                strHasil = strHasil.Substring(0, 1) + //.ToUpper()
                  strHasil.Substring(1, strHasil.Length - 1);
            }

            return strHasil;
        }


        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void LueEmploymentStatus_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueEmploymentStatus, new Sm.RefreshLue2(SetLueEmploymentStatus), string.Empty);
        }

        private void TxtContractExt_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.FormatNumTxt(TxtContractExt, 11);
        }

        #endregion

        #region Button Event

        private void BtnEmpCode_Click(object sender, EventArgs e)
        {
            Decimal ContractExt = 0m;
            Sm.FormShowDialog(new FrmEmpContractDocDlg(this, Sm.GetDte(DteDocDt).Substring(0, 8)));
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtEmpCode2, TxtEmpName2, TxtPosition2
            });

            if (mIsContractExtensionAutoFill)
            {
                ContractExt = Sm.GetValueDec("Select ContractExt+1 From tblempcontractdoc WHERE ActInd = 'Y' And EmpCode = @Param And CreateDt = (select Max(CreateDt) from tblempcontractdoc WHERE ActInd = 'Y' And EmpCode = @Param);", TxtEmpCode.Text);
                TxtContractExt.Text = Sm.FormatNum((ContractExt > 0 ? ContractExt:0), 1);
            }
        }

        private void BtnEmpCode2_Click_1(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtEmpCode, "Employee", false))
            {
                var f = new FrmEmployee(this.mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mEmpCode = TxtEmpCode.Text;
                f.ShowDialog();
            }
        }

        private void BtnEmpCode3_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmEmpContractDocDlg2(this));
        }

        #endregion

        #endregion

        #region Report Class

        private class ContractDoc
        {
            public string CompanyLogo { get; set; }

            public string CompanyName { get; set; }
            public string CompanyAddress { get; set; }
            public string CompanyPhone { get; set; }
            public string CompanyFax { get; set; }
            public string DocNo { get; set; }

            public string DocDt { get; set; }
            public string StartDt { get; set; }
            public string EndDt { get; set; }
            public string Day { get; set; }
            public string Month { get; set; }

            public string Dt { get; set; }
            public string Yr { get; set; }
            public string EmpCode { get; set; }
            public string EmpCodeOld { get; set; }
            public string EmpName { get; set; }

            public string IDNumber { get; set; }
            public string BirthPlace { get; set; }
            public string BirthDt { get; set; }
            public string Address { get; set; }
            public string RTRW { get; set; }

            public string SDName { get; set; }
            public string VilName { get; set; }
            public string CityName { get; set; }
            public string SiteName { get; set; }
            public string SiteAddress { get; set; }

            public string Posname { get; set; }
            public string DeptName { get; set; }
            public string GrdLvlName { get; set; }
            public string LevelName { get; set; }
            public string TerbilangDt { get; set; }

            public string TerbilangYr { get; set; }
            public decimal Salary { get; set; }
            public string Terbilangsalary { get; set; }
            public string EmpName2 { get; set; }
            public string PosName2 { get; set; }

            public string HOInd { get; set; }
            public string Gender { get; set; }
            public string Remark { get; set; }
            public decimal TotalTunjangan { get; set; }
            public string OTInd { get; set; }

            public string LocalDocNo { get; set; }
            public string FirstPartyName { get; set;}
            public string MaritalStatus { get; set; }
            public string NPWP { get; set; }
            public string AddressEmp2 { get; set; }

            public string DocDtVIR { get; set; }
            public decimal SalaryAmt { get; set; }
            public decimal SSAmt { get; set; }
            public decimal Bruto { get; set; }
            public decimal THP { get; set; }

        }

        private class VIRFixAllowance
        {
            public string DocNo { get; set; }

            public string EmpCode { get; set; }
            public string AdName { get; set; }
            public decimal Amt { get; set; }
        }

        private class VIRVariableAllowance
        {
            public string DocNo { get; set; }

            public string EmpCode { get; set; }
            public string AdName { get; set; }
            public decimal Amt { get; set; }
        }

        private class VIRSocialSecurity
        {
            public string DocNo { get; set; }

            public string EmpCode { get; set; }
            public string SSPName { get; set; }
            public decimal Amt { get; set; }
        }
        #endregion
    }
}
