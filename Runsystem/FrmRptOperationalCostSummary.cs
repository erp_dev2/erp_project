﻿#region update 
/*
    22/01/2021 [ICA/IMS] New App : Menampilkan FOHAmt, ProduksiAmt dan BebanUsahaAmt based on parameter CCGrpCodeForFOH, CCGrpCodeForBebanUsaha, CCGrpCodeForProduksi
                         CostCategory digroup by CCtName. 
    04/03/2021 [WED/IMS] link berubah
    09/03/2021 [WED/IMS] hanya yang ada cost center nya aja
    10/03/2021 [WED/IMS] hanya cost center yang sama dengan journal
    07/04/2021 [WED/IMS] tambah COA dan diurutkan berdasarkan nomor COA nya, berdasarkan parameter IsRptOperationalCostSummaryUseCOA
*/
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmRptOperationalCostSummary : RunSystem.FrmBase6
    {
        #region Field

        private string mMenuCode = "", mAccessInd = "", mSQL = "";
        private bool mIsRptOperationalCostSummaryUseCOA = false;

        #endregion

        #region Constructor

        public FrmRptOperationalCostSummary(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Methods

        #region Standard Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnPrint, ref BtnExcel);
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -30);
                GetParameter();
                SetGrd();
                SetSQL();
                base.FrmLoad(sender, e);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        private void GetParameter()
        {
            mIsRptOperationalCostSummaryUseCOA = Sm.GetParameterBoo("IsRptOperationalCostSummaryUseCOA");
        }

        override protected void SetSQL() //Data yg ditampilkan digroupby berdasarkan T.CostOfGroup dan T.CCtName nya. CCtName dengan CCCode yg berbeda akan dianggap satu data.
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Group_Concat(Distinct IfNull(T.AcNo, '')) AcNo, T.CCtCode, T.CCtName, T.CostOfGroupCode, T.CostOfGroupName, ");
            SQL.AppendLine("Sum(T.FOHAmt) FOHAmt, Sum(T.ProduksiAmt) ProduksiAmt, Sum(T.BebanUsahaAmt) BebanUsahaAmt, ");
            SQL.AppendLine("(Sum(T.FOHAmt) + Sum(T.ProduksiAmt)+ Sum(T.BebanUsahaAmt)) As TotalAmt ");
            SQL.AppendLine("From ( ");
            SQL.AppendLine("    Select A.AcNo, A.CCtCode, A.CCtName, A.CostOfGroupCode, B.OptDesc As CostOfGroupName, ");
            SQL.AppendLine("    Case ");
            SQL.AppendLine("        When Find_In_Set(A.CCGrpCode, D.Parvalue) Then IfNull(C.Amt, 0.00) ");
            SQL.AppendLine("        Else 0.00 ");
            SQL.AppendLine("    End As FOHAmt, ");
            SQL.AppendLine("    Case ");
            SQL.AppendLine("        When Find_In_Set(A.CCGrpCode, E.Parvalue) Then IfNull(C.Amt, 0.00) ");
            SQL.AppendLine("        Else 0.00 ");
            SQL.AppendLine("    End As ProduksiAmt, ");
            SQL.AppendLine("    Case  ");
            SQL.AppendLine("        When Find_In_Set(A.CCGrpCode, F.Parvalue) Then IfNull(C.Amt, 0.00) ");
            SQL.AppendLine("        Else 0.00 ");
            SQL.AppendLine("    End As BebanUsahaAmt ");
            SQL.AppendLine("    From TblCostCategory A ");
            SQL.AppendLine("    Left Join TblOption B On A.CostOfGroupCode = B.OptCode And B.OptCat = 'CostOfGroup' ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select X.CCtCode, Sum(X.Amt) Amt From ( ");

            // Journal ada Cost Center ada budget
            SQL.AppendLine("            Select T3.CCtCode, Case T6.AcType When 'C' Then (T2.CAmt - T2.DAmt) Else (T2.DAmt - T2.CAmt) End Amt ");
            SQL.AppendLine("            From TblJournalHdr T1 ");
            SQL.AppendLine("            Inner Join TblJournalDtl T2 ON T1.DocNo = T2.DocNo ");
            SQL.AppendLine("                And (T1.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("                And T1.CancelInd = 'N' ");
            SQL.AppendLine("                And T1.CCCode Is Not Null ");
            SQL.AppendLine("            Inner Join TblCostCategory T3 On T2.AcNo = T3.AcNo ");
            SQL.AppendLine("                And T3.CCCode = T1.CCCode ");
            SQL.AppendLine("            Inner Join TblBudgetCategory T4 On T3.CCtCode = T4.CCtCode ");
            SQL.AppendLine("            Inner Join TblCostCenter T5 On T1.CCCode = T5.CCCode And T4.DeptCode = T5.DeptCode ");
            SQL.AppendLine("            Inner Join TblCOA T6 On T2.AcNo = T6.AcNo And T6.ActInd = 'Y' ");
            SQL.AppendLine("                And T6.AcNo Not In (Select Distinct Parent From TblCOA Where Parent Is Not Null) ");

            SQL.AppendLine("            Union All ");

            // Journal ada Cost Center tidak ada budget
            SQL.AppendLine("            Select T3.CCtCode, Case T6.AcType When 'C' Then (T2.CAmt - T2.DAmt) Else (T2.DAmt - T2.CAmt) End Amt ");
            SQL.AppendLine("            From TblJournalHdr T1 ");
            SQL.AppendLine("            Inner Join TblJournalDtl T2 ON T1.DocNo = T2.DocNo ");
            SQL.AppendLine("                And (T1.DocDt Between @DocDt1 And @DocDt2) ");
            SQL.AppendLine("                And T1.CancelInd = 'N' ");
            SQL.AppendLine("                And T1.CCCode Is Not Null ");
            SQL.AppendLine("            Inner Join TblCostCategory T3 On T2.AcNo = T3.AcNo ");
            SQL.AppendLine("                And T3.CCCode = T1.CCCode ");
            SQL.AppendLine("                And T3.CCtCode Not In (Select Distinct CCtCode From TblBudgetCategory Where CCtCode Is Not Null) ");
            SQL.AppendLine("            Inner Join TblCostCenter T5 On T1.CCCode = T5.CCCode ");
            SQL.AppendLine("            Inner Join TblCOA T6 On T2.AcNo = T6.AcNo And T6.ActInd = 'Y' ");
            SQL.AppendLine("                And T6.AcNo Not In (Select Distinct Parent From TblCOA Where Parent Is Not Null) ");

            //SQL.AppendLine("            Union All ");

            // Journal tanpa cost center tapi connect budget
            //SQL.AppendLine("            Select T3.CCtCode, Case T6.AcType When 'C' Then (T2.CAmt - T2.DAmt) Else (T2.DAmt - T2.CAmt) End Amt ");
            //SQL.AppendLine("            From TblJournalHdr T1 ");
            //SQL.AppendLine("            Inner Join TblJournalDtl T2 ON T1.DocNo = T2.DocNo ");
            //SQL.AppendLine("                And (T1.DocDt Between @DocDt1 And @DocDt2) ");
            //SQL.AppendLine("                And T1.CancelInd = 'N' ");
            //SQL.AppendLine("                And T1.CCCode Is Null ");
            //SQL.AppendLine("            Inner Join TblCostCategory T3 On T2.AcNo = T3.AcNo ");
            //SQL.AppendLine("            Inner Join TblBudgetCategory T4 On T3.CCtCode = T4.CCtCode ");
            //SQL.AppendLine("            Inner Join TblCOA T6 On T2.AcNo = T6.AcNo And T6.ActInd = 'Y' ");
            //SQL.AppendLine("                And T6.AcNo Not In (Select Distinct Parent From TblCOA Where Parent Is Not Null) ");

            //SQL.AppendLine("            Union All ");

            // Journal tanpa cost center tapi tidak connect budget
            //SQL.AppendLine("            Select T3.CCtCode, Case T6.AcType When 'C' Then (T2.CAmt - T2.DAmt) Else (T2.DAmt - T2.CAmt) End Amt ");
            //SQL.AppendLine("            From TblJournalHdr T1 ");
            //SQL.AppendLine("            Inner Join TblJournalDtl T2 ON T1.DocNo = T2.DocNo ");
            //SQL.AppendLine("                And (T1.DocDt Between @DocDt1 And @DocDt2) ");
            //SQL.AppendLine("                And T1.CancelInd = 'N' ");
            //SQL.AppendLine("                And T1.CCCode Is Null ");
            //SQL.AppendLine("            Inner Join TblCostCategory T3 On T2.AcNo = T3.AcNo ");
            //SQL.AppendLine("                And T3.CCtCode Not In (Select Distinct CCtCode From TblBudgetCategory Where CCtCode Is Not Null) ");
            //SQL.AppendLine("            Inner Join TblCOA T6 On T2.AcNo = T6.AcNo And T6.ActInd = 'Y' ");
            //SQL.AppendLine("                And T6.AcNo Not In (Select Distinct Parent From TblCOA Where Parent Is Not Null) ");
            SQL.AppendLine("        ) X ");
            SQL.AppendLine("        Group By X.CCtCode ");

            SQL.AppendLine("    ) C On A.CCtCode = C.CCtCode ");
            SQL.AppendLine("    Left Join TblParameter D On D.Parcode = 'CCGrpCodeForFOH' ");
            SQL.AppendLine("    Left Join TblParameter E On E.Parcode = 'CCGrpCodeForProduksi' ");
            SQL.AppendLine("    Left Join TblParameter F On F.Parcode = 'CCGrpCodeForBebanUsaha' ");
            SQL.AppendLine(")T ");
            SQL.AppendLine("Group By T.CostOfGroupCode, T.CCtName ");
            if (mIsRptOperationalCostSummaryUseCOA)
                SQL.AppendLine("Order By T.AcNo ");
            SQL.AppendLine("; ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 8;
            Grd1.FrozenArea.ColCount = 4;
            Grd1.ReadOnly = false;
            Sm.GrdHdrWithColWidth(
                Grd1, new string[] 
                {
                    //0
                    "No",

                    //1-5
                    "Cost of Group",
                    "COA",
                    "Cost Category", 
                    "FOH",
                    "Production",

                    //6-7
                    "Operating"+Environment.NewLine+"Expenses",
                    "Amt",
                },
                new int[] 
                {
                    //0
                    50,

                    //1-5
                    150, 150, 400, 150, 150, 

                    //6-7
                    150, 150
                }
            );
            Sm.GrdFormatDec(Grd1, new int[] { 4, 5, 6, 7 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 0, 2 });
            Grd1.ReadOnly = true;
        }

        override protected void HideInfoInGrd()
        {
            if (mIsRptOperationalCostSummaryUseCOA)
                Sm.GrdColInvisible(Grd1, new int[] { 2 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter,
                        new string[]
                        {
                            //0
                            "CostOfGroupName", 

                            //1-5
                            "AcNo", "CCtName", "FOHAmt", "ProduksiAmt", "BebanUsahaAmt", 
                            
                            //6
                            "TotalAmt" 
                            
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                        }, true, false, false, false
                    );

                if (!mIsRptOperationalCostSummaryUseCOA)
                {
                    Grd1.GroupObject.Add(1);
                    Grd1.Group();
                    AdjustSubtotals();
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 0);
                Cursor.Current = Cursors.Default;
            }
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        private void AdjustSubtotals()
        {
            iGSubtotalManager.BackColor = Color.LightSalmon;
            iGSubtotalManager.ForeColor = Color.Black;
            iGSubtotalManager.ShowSubtotalsInCells = true;
            iGSubtotalManager.HideSubtotals(Grd1);
            iGSubtotalManager.ShowSubtotals(Grd1, new int[] { 4, 5, 6, 7 });
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void DteDocDt2_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt1).Length == 0) DteDocDt1.EditValue = DteDocDt2.EditValue;
        }

        #endregion
    }
}
