﻿namespace RunSystem
{
    partial class FrmEmployee
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmEmployee));
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.BtnCopyGeneral = new DevExpress.XtraEditors.SimpleButton();
            this.LblCopyGeneral = new System.Windows.Forms.Label();
            this.TxtDisplayName = new DevExpress.XtraEditors.TextEdit();
            this.label66 = new System.Windows.Forms.Label();
            this.TxtEmpName = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtEmpCode = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtEmpCodeOld = new DevExpress.XtraEditors.TextEdit();
            this.label48 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.TpgGeneral = new System.Windows.Forms.TabPage();
            this.BtnSpouseEmpCode = new DevExpress.XtraEditors.SimpleButton();
            this.TxtSpouseEmpCode = new DevExpress.XtraEditors.TextEdit();
            this.LblSpouseEmpCode = new System.Windows.Forms.Label();
            this.ChkActingOfficial = new DevExpress.XtraEditors.CheckEdit();
            this.DteContractDt = new DevExpress.XtraEditors.DateEdit();
            this.LblContractDt = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.LuePositionStatusCode = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtRegEntCode = new DevExpress.XtraEditors.TextEdit();
            this.label53 = new System.Windows.Forms.Label();
            this.DteTGDt = new DevExpress.XtraEditors.DateEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.LueEntityCode = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtPOH = new DevExpress.XtraEditors.TextEdit();
            this.label69 = new System.Windows.Forms.Label();
            this.LblDivisionCode = new System.Windows.Forms.Label();
            this.LueDivisionCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label65 = new System.Windows.Forms.Label();
            this.LueWorkGroup = new DevExpress.XtraEditors.LookUpEdit();
            this.label64 = new System.Windows.Forms.Label();
            this.LueSection = new DevExpress.XtraEditors.LookUpEdit();
            this.label63 = new System.Windows.Forms.Label();
            this.lblMarital = new System.Windows.Forms.Label();
            this.MeeDomicile = new DevExpress.XtraEditors.MemoExEdit();
            this.LblSiteCode = new System.Windows.Forms.Label();
            this.LueSiteCode = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtRTRW = new DevExpress.XtraEditors.TextEdit();
            this.LueMaritalStatus = new DevExpress.XtraEditors.LookUpEdit();
            this.label56 = new System.Windows.Forms.Label();
            this.TxtVillage = new DevExpress.XtraEditors.TextEdit();
            this.label55 = new System.Windows.Forms.Label();
            this.TxtSubDistrict = new DevExpress.XtraEditors.TextEdit();
            this.label54 = new System.Windows.Forms.Label();
            this.LblEmploymentStatus = new System.Windows.Forms.Label();
            this.TxtBarcodeCode = new DevExpress.XtraEditors.TextEdit();
            this.label52 = new System.Windows.Forms.Label();
            this.TxtShortCode = new DevExpress.XtraEditors.TextEdit();
            this.label50 = new System.Windows.Forms.Label();
            this.LueEmploymentStatus = new DevExpress.XtraEditors.LookUpEdit();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label73 = new System.Windows.Forms.Label();
            this.LueBloodRhesus = new DevExpress.XtraEditors.LookUpEdit();
            this.label71 = new System.Windows.Forms.Label();
            this.LueCostGroup = new DevExpress.XtraEditors.LookUpEdit();
            this.label67 = new System.Windows.Forms.Label();
            this.LueEducationType = new DevExpress.XtraEditors.LookUpEdit();
            this.label68 = new System.Windows.Forms.Label();
            this.LueShoeSize = new DevExpress.XtraEditors.LookUpEdit();
            this.label61 = new System.Windows.Forms.Label();
            this.LueClothesSize = new DevExpress.XtraEditors.LookUpEdit();
            this.label51 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.TxtLevelCode = new DevExpress.XtraEditors.TextEdit();
            this.label47 = new System.Windows.Forms.Label();
            this.DteLeaveStartDt = new DevExpress.XtraEditors.DateEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.LblPGCode = new System.Windows.Forms.Label();
            this.LuePGCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label62 = new System.Windows.Forms.Label();
            this.LueBloodType = new DevExpress.XtraEditors.LookUpEdit();
            this.LblSystemType = new System.Windows.Forms.Label();
            this.LueSystemType = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtMother = new DevExpress.XtraEditors.TextEdit();
            this.LblMother = new System.Windows.Forms.Label();
            this.LblPayrunPeriod = new System.Windows.Forms.Label();
            this.LuePayrunPeriod = new DevExpress.XtraEditors.LookUpEdit();
            this.DteWeddingDt = new DevExpress.XtraEditors.DateEdit();
            this.label59 = new System.Windows.Forms.Label();
            this.TxtBankAcName = new DevExpress.XtraEditors.TextEdit();
            this.label49 = new System.Windows.Forms.Label();
            this.LblGrdLvlCode = new System.Windows.Forms.Label();
            this.TxtEmail = new DevExpress.XtraEditors.TextEdit();
            this.LueGrdLvlCode = new DevExpress.XtraEditors.LookUpEdit();
            this.DteBirthDt = new DevExpress.XtraEditors.DateEdit();
            this.TxtBankBranch = new DevExpress.XtraEditors.TextEdit();
            this.label18 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.TxtBirthPlace = new DevExpress.XtraEditors.TextEdit();
            this.label13 = new System.Windows.Forms.Label();
            this.TxtBankAcNo = new DevExpress.XtraEditors.TextEdit();
            this.label12 = new System.Windows.Forms.Label();
            this.LueReligion = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtMobile = new DevExpress.XtraEditors.TextEdit();
            this.label24 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.LueBankCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label21 = new System.Windows.Forms.Label();
            this.LuePTKP = new DevExpress.XtraEditors.LookUpEdit();
            this.label17 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.LuePayrollType = new DevExpress.XtraEditors.LookUpEdit();
            this.label20 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.TxtNPWP = new DevExpress.XtraEditors.TextEdit();
            this.LueGender = new DevExpress.XtraEditors.LookUpEdit();
            this.label16 = new System.Windows.Forms.Label();
            this.TxtPhone = new DevExpress.XtraEditors.TextEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.LueCity = new DevExpress.XtraEditors.LookUpEdit();
            this.TxtIdNumber = new DevExpress.XtraEditors.TextEdit();
            this.DteResignDt = new DevExpress.XtraEditors.DateEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.DteJoinDt = new DevExpress.XtraEditors.DateEdit();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtUserCode = new DevExpress.XtraEditors.TextEdit();
            this.TxtPostalCode = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.MeeAddress = new DevExpress.XtraEditors.MemoExEdit();
            this.label8 = new System.Windows.Forms.Label();
            this.LblPosCode = new System.Windows.Forms.Label();
            this.LuePosCode = new DevExpress.XtraEditors.LookUpEdit();
            this.LblDeptCode = new System.Windows.Forms.Label();
            this.LueDeptCode = new DevExpress.XtraEditors.LookUpEdit();
            this.TpgEmployeeFamily = new System.Windows.Forms.TabPage();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.LueEducationLevelCode = new DevExpress.XtraEditors.LookUpEdit();
            this.LueProfessionCode = new DevExpress.XtraEditors.LookUpEdit();
            this.DteFamBirthDt = new DevExpress.XtraEditors.DateEdit();
            this.LueFamGender = new DevExpress.XtraEditors.LookUpEdit();
            this.LueStatus = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd1 = new TenTec.Windows.iGridLib.iGrid();
            this.BtnFamily = new System.Windows.Forms.Button();
            this.Grd7 = new TenTec.Windows.iGridLib.iGrid();
            this.button1 = new System.Windows.Forms.Button();
            this.TpgEmployeeWorkExp = new System.Windows.Forms.TabPage();
            this.Grd2 = new TenTec.Windows.iGridLib.iGrid();
            this.TpgEmployeeEducation = new System.Windows.Forms.TabPage();
            this.LueFacCode = new DevExpress.XtraEditors.LookUpEdit();
            this.LueField = new DevExpress.XtraEditors.LookUpEdit();
            this.LueMajor = new DevExpress.XtraEditors.LookUpEdit();
            this.LueLevel = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd3 = new TenTec.Windows.iGridLib.iGrid();
            this.TpgEmployeeTraining = new System.Windows.Forms.TabPage();
            this.LueTrainingCode = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd8 = new TenTec.Windows.iGridLib.iGrid();
            this.TpgEmployeeTraining2 = new System.Windows.Forms.TabPage();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.Grd9 = new TenTec.Windows.iGridLib.iGrid();
            this.LueTrainingCode2 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd10 = new TenTec.Windows.iGridLib.iGrid();
            this.TpgEmployeeTraining3 = new System.Windows.Forms.TabPage();
            this.LueTrainingCategory = new DevExpress.XtraEditors.LookUpEdit();
            this.LueTrainingEducationCenter = new DevExpress.XtraEditors.LookUpEdit();
            this.LueTrainingSpecialization = new DevExpress.XtraEditors.LookUpEdit();
            this.LueTrainingType = new DevExpress.XtraEditors.LookUpEdit();
            this.LueTrainingCode3 = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd16 = new TenTec.Windows.iGridLib.iGrid();
            this.TpgEmployeeCompetence = new System.Windows.Forms.TabPage();
            this.LueCompetenceCode = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd4 = new TenTec.Windows.iGridLib.iGrid();
            this.TpgPositionHistory = new System.Windows.Forms.TabPage();
            this.Grd15 = new TenTec.Windows.iGridLib.iGrid();
            this.panel9 = new System.Windows.Forms.Panel();
            this.LblPositionHistoryRefreshData = new System.Windows.Forms.Label();
            this.TpgGradeHistory = new System.Windows.Forms.TabPage();
            this.Grd17 = new TenTec.Windows.iGridLib.iGrid();
            this.panel10 = new System.Windows.Forms.Panel();
            this.LblGradeHistoryRefreshData = new System.Windows.Forms.Label();
            this.TpgWarningLetter = new System.Windows.Forms.TabPage();
            this.Grd13 = new TenTec.Windows.iGridLib.iGrid();
            this.panel8 = new System.Windows.Forms.Panel();
            this.LblWarningLetterRefreshData = new System.Windows.Forms.Label();
            this.TpgEmployeeSS = new System.Windows.Forms.TabPage();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.Grd5 = new TenTec.Windows.iGridLib.iGrid();
            this.Grd6 = new TenTec.Windows.iGridLib.iGrid();
            this.TpgAward = new System.Windows.Forms.TabPage();
            this.LueAwardCt = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd14 = new TenTec.Windows.iGridLib.iGrid();
            this.TpgVaccine = new System.Windows.Forms.TabPage();
            this.DteVaccineDt = new DevExpress.XtraEditors.DateEdit();
            this.Grd12 = new TenTec.Windows.iGridLib.iGrid();
            this.TpgPicture = new System.Windows.Forms.TabPage();
            this.BtnDownload = new DevExpress.XtraEditors.SimpleButton();
            this.PicEmployee = new System.Windows.Forms.PictureBox();
            this.BtnPicture = new DevExpress.XtraEditors.SimpleButton();
            this.splitContainer4 = new System.Windows.Forms.SplitContainer();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.LueUploadFileCategory = new DevExpress.XtraEditors.LookUpEdit();
            this.Grd11 = new TenTec.Windows.iGridLib.iGrid();
            this.button2 = new System.Windows.Forms.Button();
            this.TpgAdditional = new System.Windows.Forms.TabPage();
            this.label74 = new System.Windows.Forms.Label();
            this.LueFirstGrdLvlCode = new DevExpress.XtraEditors.LookUpEdit();
            this.label72 = new System.Windows.Forms.Label();
            this.LueLevelCode = new DevExpress.XtraEditors.LookUpEdit();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.panel6 = new System.Windows.Forms.Panel();
            this.textEdit3 = new DevExpress.XtraEditors.TextEdit();
            this.label25 = new System.Windows.Forms.Label();
            this.textEdit4 = new DevExpress.XtraEditors.TextEdit();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.lookUpEdit3 = new DevExpress.XtraEditors.LookUpEdit();
            this.label28 = new System.Windows.Forms.Label();
            this.lookUpEdit4 = new DevExpress.XtraEditors.LookUpEdit();
            this.label29 = new System.Windows.Forms.Label();
            this.lookUpEdit5 = new DevExpress.XtraEditors.LookUpEdit();
            this.label30 = new System.Windows.Forms.Label();
            this.textEdit5 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit6 = new DevExpress.XtraEditors.TextEdit();
            this.label31 = new System.Windows.Forms.Label();
            this.textEdit7 = new DevExpress.XtraEditors.TextEdit();
            this.label32 = new System.Windows.Forms.Label();
            this.textEdit8 = new DevExpress.XtraEditors.TextEdit();
            this.label33 = new System.Windows.Forms.Label();
            this.textEdit9 = new DevExpress.XtraEditors.TextEdit();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.lookUpEdit6 = new DevExpress.XtraEditors.LookUpEdit();
            this.dateEdit1 = new DevExpress.XtraEditors.DateEdit();
            this.label36 = new System.Windows.Forms.Label();
            this.textEdit10 = new DevExpress.XtraEditors.TextEdit();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.lookUpEdit7 = new DevExpress.XtraEditors.LookUpEdit();
            this.textEdit11 = new DevExpress.XtraEditors.TextEdit();
            this.label39 = new System.Windows.Forms.Label();
            this.dateEdit2 = new DevExpress.XtraEditors.DateEdit();
            this.label40 = new System.Windows.Forms.Label();
            this.dateEdit3 = new DevExpress.XtraEditors.DateEdit();
            this.label41 = new System.Windows.Forms.Label();
            this.textEdit12 = new DevExpress.XtraEditors.TextEdit();
            this.label42 = new System.Windows.Forms.Label();
            this.memoExEdit1 = new DevExpress.XtraEditors.MemoExEdit();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.lookUpEdit8 = new DevExpress.XtraEditors.LookUpEdit();
            this.label45 = new System.Windows.Forms.Label();
            this.lookUpEdit9 = new DevExpress.XtraEditors.LookUpEdit();
            this.label46 = new System.Windows.Forms.Label();
            this.lookUpEdit10 = new DevExpress.XtraEditors.LookUpEdit();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.dateEdit4 = new DevExpress.XtraEditors.DateEdit();
            this.lookUpEdit11 = new DevExpress.XtraEditors.LookUpEdit();
            this.lookUpEdit12 = new DevExpress.XtraEditors.LookUpEdit();
            this.iGrid1 = new TenTec.Windows.iGridLib.iGrid();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.iGrid2 = new TenTec.Windows.iGridLib.iGrid();
            this.SFD = new System.Windows.Forms.SaveFileDialog();
            this.OD = new System.Windows.Forms.OpenFileDialog();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDisplayName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpCodeOld.Properties)).BeginInit();
            this.panel4.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.TpgGeneral.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSpouseEmpCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActingOfficial.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteContractDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteContractDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePositionStatusCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRegEntCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTGDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTGDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueEntityCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPOH.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDivisionCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWorkGroup.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSection.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeDomicile.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSiteCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRTRW.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueMaritalStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVillage.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSubDistrict.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBarcodeCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtShortCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueEmploymentStatus.Properties)).BeginInit();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueBloodRhesus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCostGroup.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueEducationType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShoeSize.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueClothesSize.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLevelCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteLeaveStartDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteLeaveStartDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePGCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBloodType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSystemType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMother.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePayrunPeriod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteWeddingDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteWeddingDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBankAcName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmail.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueGrdLvlCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteBirthDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteBirthDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBankBranch.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBirthPlace.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBankAcNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueReligion.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMobile.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBankCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePTKP.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePayrollType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtNPWP.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueGender.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPhone.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCity.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtIdNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteResignDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteResignDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteJoinDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteJoinDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtUserCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPostalCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeAddress.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePosCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDeptCode.Properties)).BeginInit();
            this.TpgEmployeeFamily.SuspendLayout();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueEducationLevelCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueProfessionCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteFamBirthDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteFamBirthDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueFamGender.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd7)).BeginInit();
            this.TpgEmployeeWorkExp.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).BeginInit();
            this.TpgEmployeeEducation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueFacCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueMajor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueLevel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).BeginInit();
            this.TpgEmployeeTraining.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueTrainingCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd8)).BeginInit();
            this.TpgEmployeeTraining2.SuspendLayout();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueTrainingCode2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd10)).BeginInit();
            this.TpgEmployeeTraining3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueTrainingCategory.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueTrainingEducationCenter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueTrainingSpecialization.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueTrainingType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueTrainingCode3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd16)).BeginInit();
            this.TpgEmployeeCompetence.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueCompetenceCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd4)).BeginInit();
            this.TpgPositionHistory.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd15)).BeginInit();
            this.panel9.SuspendLayout();
            this.TpgGradeHistory.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd17)).BeginInit();
            this.panel10.SuspendLayout();
            this.TpgWarningLetter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd13)).BeginInit();
            this.panel8.SuspendLayout();
            this.TpgEmployeeSS.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grd5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd6)).BeginInit();
            this.TpgAward.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueAwardCt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd14)).BeginInit();
            this.TpgVaccine.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DteVaccineDt.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteVaccineDt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd12)).BeginInit();
            this.TpgPicture.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PicEmployee)).BeginInit();
            this.splitContainer4.Panel1.SuspendLayout();
            this.splitContainer4.Panel2.SuspendLayout();
            this.splitContainer4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueUploadFileCategory.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd11)).BeginInit();
            this.TpgAdditional.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueFirstGrdLvlCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueLevelCode.Properties)).BeginInit();
            this.tabPage1.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit2.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit3.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoExEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit10.Properties)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit4.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.iGrid1)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.iGrid2)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(926, 0);
            this.panel1.Size = new System.Drawing.Size(70, 669);
            // 
            // BtnCancel
            // 
            this.BtnCancel.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCancel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancel.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCancel.Appearance.Options.UseBackColor = true;
            this.BtnCancel.Appearance.Options.UseFont = true;
            this.BtnCancel.Appearance.Options.UseForeColor = true;
            this.BtnCancel.Appearance.Options.UseTextOptions = true;
            this.BtnCancel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnCancel.Margin = new System.Windows.Forms.Padding(2);
            // 
            // BtnSave
            // 
            this.BtnSave.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSave.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSave.Appearance.Options.UseBackColor = true;
            this.BtnSave.Appearance.Options.UseFont = true;
            this.BtnSave.Appearance.Options.UseForeColor = true;
            this.BtnSave.Appearance.Options.UseTextOptions = true;
            this.BtnSave.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnSave.Margin = new System.Windows.Forms.Padding(2);
            // 
            // BtnDelete
            // 
            this.BtnDelete.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDelete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDelete.Appearance.Options.UseBackColor = true;
            this.BtnDelete.Appearance.Options.UseFont = true;
            this.BtnDelete.Appearance.Options.UseForeColor = true;
            this.BtnDelete.Appearance.Options.UseTextOptions = true;
            this.BtnDelete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDelete.Margin = new System.Windows.Forms.Padding(2);
            // 
            // BtnEdit
            // 
            this.BtnEdit.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnEdit.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnEdit.Appearance.Options.UseBackColor = true;
            this.BtnEdit.Appearance.Options.UseFont = true;
            this.BtnEdit.Appearance.Options.UseForeColor = true;
            this.BtnEdit.Appearance.Options.UseTextOptions = true;
            this.BtnEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnEdit.Margin = new System.Windows.Forms.Padding(2);
            // 
            // BtnInsert
            // 
            this.BtnInsert.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnInsert.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnInsert.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnInsert.Appearance.Options.UseBackColor = true;
            this.BtnInsert.Appearance.Options.UseFont = true;
            this.BtnInsert.Appearance.Options.UseForeColor = true;
            this.BtnInsert.Appearance.Options.UseTextOptions = true;
            this.BtnInsert.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnInsert.Margin = new System.Windows.Forms.Padding(2);
            // 
            // BtnFind
            // 
            this.BtnFind.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnFind.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFind.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnFind.Appearance.Options.UseBackColor = true;
            this.BtnFind.Appearance.Options.UseFont = true;
            this.BtnFind.Appearance.Options.UseForeColor = true;
            this.BtnFind.Appearance.Options.UseTextOptions = true;
            this.BtnFind.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnFind.Margin = new System.Windows.Forms.Padding(2);
            // 
            // BtnPrint
            // 
            this.BtnPrint.Appearance.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPrint.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPrint.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPrint.Appearance.Options.UseBackColor = true;
            this.BtnPrint.Appearance.Options.UseFont = true;
            this.BtnPrint.Appearance.Options.UseForeColor = true;
            this.BtnPrint.Appearance.Options.UseTextOptions = true;
            this.BtnPrint.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnPrint.Margin = new System.Windows.Forms.Padding(2);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Size = new System.Drawing.Size(926, 669);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel3.Controls.Add(this.panel7);
            this.panel3.Controls.Add(this.TxtDisplayName);
            this.panel3.Controls.Add(this.label66);
            this.panel3.Controls.Add(this.TxtEmpName);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.TxtEmpCode);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.TxtEmpCodeOld);
            this.panel3.Controls.Add(this.label48);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(926, 50);
            this.panel3.TabIndex = 9;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.BtnCopyGeneral);
            this.panel7.Controls.Add(this.LblCopyGeneral);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel7.Location = new System.Drawing.Point(806, 0);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(116, 46);
            this.panel7.TabIndex = 18;
            // 
            // BtnCopyGeneral
            // 
            this.BtnCopyGeneral.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnCopyGeneral.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnCopyGeneral.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCopyGeneral.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnCopyGeneral.Appearance.Options.UseBackColor = true;
            this.BtnCopyGeneral.Appearance.Options.UseFont = true;
            this.BtnCopyGeneral.Appearance.Options.UseForeColor = true;
            this.BtnCopyGeneral.Appearance.Options.UseTextOptions = true;
            this.BtnCopyGeneral.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnCopyGeneral.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnCopyGeneral.Image = ((System.Drawing.Image)(resources.GetObject("BtnCopyGeneral.Image")));
            this.BtnCopyGeneral.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnCopyGeneral.Location = new System.Drawing.Point(90, 22);
            this.BtnCopyGeneral.Name = "BtnCopyGeneral";
            this.BtnCopyGeneral.Size = new System.Drawing.Size(24, 21);
            this.BtnCopyGeneral.TabIndex = 20;
            this.BtnCopyGeneral.ToolTip = "Show Employee List";
            this.BtnCopyGeneral.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnCopyGeneral.ToolTipTitle = "Run System";
            this.BtnCopyGeneral.Click += new System.EventHandler(this.BtnCopyGeneral_Click);
            // 
            // LblCopyGeneral
            // 
            this.LblCopyGeneral.AutoSize = true;
            this.LblCopyGeneral.Font = new System.Drawing.Font("Tahoma", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblCopyGeneral.ForeColor = System.Drawing.Color.Green;
            this.LblCopyGeneral.Location = new System.Drawing.Point(15, 25);
            this.LblCopyGeneral.Name = "LblCopyGeneral";
            this.LblCopyGeneral.Size = new System.Drawing.Size(71, 14);
            this.LblCopyGeneral.TabIndex = 19;
            this.LblCopyGeneral.Tag = "Copy Data From Existing Employee";
            this.LblCopyGeneral.Text = "Copy Data";
            // 
            // TxtDisplayName
            // 
            this.TxtDisplayName.EnterMoveNextControl = true;
            this.TxtDisplayName.Location = new System.Drawing.Point(543, 24);
            this.TxtDisplayName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtDisplayName.Name = "TxtDisplayName";
            this.TxtDisplayName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtDisplayName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtDisplayName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtDisplayName.Properties.Appearance.Options.UseFont = true;
            this.TxtDisplayName.Properties.MaxLength = 40;
            this.TxtDisplayName.Size = new System.Drawing.Size(239, 20);
            this.TxtDisplayName.TabIndex = 17;
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label66.ForeColor = System.Drawing.Color.Black;
            this.label66.Location = new System.Drawing.Point(461, 27);
            this.label66.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(78, 14);
            this.label66.TabIndex = 16;
            this.label66.Text = "Display Name";
            this.label66.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtEmpName
            // 
            this.TxtEmpName.EnterMoveNextControl = true;
            this.TxtEmpName.Location = new System.Drawing.Point(103, 24);
            this.TxtEmpName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtEmpName.Name = "TxtEmpName";
            this.TxtEmpName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtEmpName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEmpName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEmpName.Properties.Appearance.Options.UseFont = true;
            this.TxtEmpName.Properties.MaxLength = 40;
            this.TxtEmpName.Size = new System.Drawing.Size(354, 20);
            this.TxtEmpName.TabIndex = 15;
            this.TxtEmpName.Validated += new System.EventHandler(this.TxtEmpName_Validated);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(4, 27);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(95, 14);
            this.label2.TabIndex = 14;
            this.label2.Text = "Employee Name";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtEmpCode
            // 
            this.TxtEmpCode.EnterMoveNextControl = true;
            this.TxtEmpCode.Location = new System.Drawing.Point(103, 3);
            this.TxtEmpCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtEmpCode.Name = "TxtEmpCode";
            this.TxtEmpCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtEmpCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEmpCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEmpCode.Properties.Appearance.Options.UseFont = true;
            this.TxtEmpCode.Properties.MaxLength = 50;
            this.TxtEmpCode.Size = new System.Drawing.Size(354, 20);
            this.TxtEmpCode.TabIndex = 11;
            this.TxtEmpCode.Validated += new System.EventHandler(this.TxtEmpCode_Validated);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(7, 6);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(92, 14);
            this.label1.TabIndex = 10;
            this.label1.Text = "Employee Code";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtEmpCodeOld
            // 
            this.TxtEmpCodeOld.EnterMoveNextControl = true;
            this.TxtEmpCodeOld.Location = new System.Drawing.Point(543, 3);
            this.TxtEmpCodeOld.Margin = new System.Windows.Forms.Padding(5);
            this.TxtEmpCodeOld.Name = "TxtEmpCodeOld";
            this.TxtEmpCodeOld.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtEmpCodeOld.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEmpCodeOld.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEmpCodeOld.Properties.Appearance.Options.UseFont = true;
            this.TxtEmpCodeOld.Properties.MaxLength = 50;
            this.TxtEmpCodeOld.Size = new System.Drawing.Size(239, 20);
            this.TxtEmpCodeOld.TabIndex = 13;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.ForeColor = System.Drawing.Color.Black;
            this.label48.Location = new System.Drawing.Point(482, 6);
            this.label48.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(57, 14);
            this.label48.TabIndex = 12;
            this.label48.Text = "Old Code";
            this.label48.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel4.Controls.Add(this.tabControl1);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(0, 50);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(926, 619);
            this.panel4.TabIndex = 10;
            // 
            // tabControl1
            // 
            this.tabControl1.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.tabControl1.Controls.Add(this.TpgGeneral);
            this.tabControl1.Controls.Add(this.TpgEmployeeFamily);
            this.tabControl1.Controls.Add(this.TpgEmployeeWorkExp);
            this.tabControl1.Controls.Add(this.TpgEmployeeEducation);
            this.tabControl1.Controls.Add(this.TpgEmployeeTraining);
            this.tabControl1.Controls.Add(this.TpgEmployeeTraining2);
            this.tabControl1.Controls.Add(this.TpgEmployeeTraining3);
            this.tabControl1.Controls.Add(this.TpgEmployeeCompetence);
            this.tabControl1.Controls.Add(this.TpgPositionHistory);
            this.tabControl1.Controls.Add(this.TpgGradeHistory);
            this.tabControl1.Controls.Add(this.TpgWarningLetter);
            this.tabControl1.Controls.Add(this.TpgEmployeeSS);
            this.tabControl1.Controls.Add(this.TpgAward);
            this.tabControl1.Controls.Add(this.TpgVaccine);
            this.tabControl1.Controls.Add(this.TpgPicture);
            this.tabControl1.Controls.Add(this.TpgAdditional);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Multiline = true;
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.ShowToolTips = true;
            this.tabControl1.Size = new System.Drawing.Size(926, 619);
            this.tabControl1.TabIndex = 22;
            // 
            // TpgGeneral
            // 
            this.TpgGeneral.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.TpgGeneral.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.TpgGeneral.Controls.Add(this.BtnSpouseEmpCode);
            this.TpgGeneral.Controls.Add(this.TxtSpouseEmpCode);
            this.TpgGeneral.Controls.Add(this.LblSpouseEmpCode);
            this.TpgGeneral.Controls.Add(this.ChkActingOfficial);
            this.TpgGeneral.Controls.Add(this.DteContractDt);
            this.TpgGeneral.Controls.Add(this.LblContractDt);
            this.TpgGeneral.Controls.Add(this.label60);
            this.TpgGeneral.Controls.Add(this.LuePositionStatusCode);
            this.TpgGeneral.Controls.Add(this.TxtRegEntCode);
            this.TpgGeneral.Controls.Add(this.label53);
            this.TpgGeneral.Controls.Add(this.DteTGDt);
            this.TpgGeneral.Controls.Add(this.label4);
            this.TpgGeneral.Controls.Add(this.label70);
            this.TpgGeneral.Controls.Add(this.LueEntityCode);
            this.TpgGeneral.Controls.Add(this.TxtPOH);
            this.TpgGeneral.Controls.Add(this.label69);
            this.TpgGeneral.Controls.Add(this.LblDivisionCode);
            this.TpgGeneral.Controls.Add(this.LueDivisionCode);
            this.TpgGeneral.Controls.Add(this.label65);
            this.TpgGeneral.Controls.Add(this.LueWorkGroup);
            this.TpgGeneral.Controls.Add(this.label64);
            this.TpgGeneral.Controls.Add(this.LueSection);
            this.TpgGeneral.Controls.Add(this.label63);
            this.TpgGeneral.Controls.Add(this.lblMarital);
            this.TpgGeneral.Controls.Add(this.MeeDomicile);
            this.TpgGeneral.Controls.Add(this.LblSiteCode);
            this.TpgGeneral.Controls.Add(this.LueSiteCode);
            this.TpgGeneral.Controls.Add(this.TxtRTRW);
            this.TpgGeneral.Controls.Add(this.LueMaritalStatus);
            this.TpgGeneral.Controls.Add(this.label56);
            this.TpgGeneral.Controls.Add(this.TxtVillage);
            this.TpgGeneral.Controls.Add(this.label55);
            this.TpgGeneral.Controls.Add(this.TxtSubDistrict);
            this.TpgGeneral.Controls.Add(this.label54);
            this.TpgGeneral.Controls.Add(this.LblEmploymentStatus);
            this.TpgGeneral.Controls.Add(this.TxtBarcodeCode);
            this.TpgGeneral.Controls.Add(this.label52);
            this.TpgGeneral.Controls.Add(this.TxtShortCode);
            this.TpgGeneral.Controls.Add(this.label50);
            this.TpgGeneral.Controls.Add(this.LueEmploymentStatus);
            this.TpgGeneral.Controls.Add(this.panel5);
            this.TpgGeneral.Controls.Add(this.label9);
            this.TpgGeneral.Controls.Add(this.LueCity);
            this.TpgGeneral.Controls.Add(this.TxtIdNumber);
            this.TpgGeneral.Controls.Add(this.DteResignDt);
            this.TpgGeneral.Controls.Add(this.label10);
            this.TpgGeneral.Controls.Add(this.label11);
            this.TpgGeneral.Controls.Add(this.DteJoinDt);
            this.TpgGeneral.Controls.Add(this.label6);
            this.TpgGeneral.Controls.Add(this.TxtUserCode);
            this.TpgGeneral.Controls.Add(this.TxtPostalCode);
            this.TpgGeneral.Controls.Add(this.label3);
            this.TpgGeneral.Controls.Add(this.label15);
            this.TpgGeneral.Controls.Add(this.MeeAddress);
            this.TpgGeneral.Controls.Add(this.label8);
            this.TpgGeneral.Controls.Add(this.LblPosCode);
            this.TpgGeneral.Controls.Add(this.LuePosCode);
            this.TpgGeneral.Controls.Add(this.LblDeptCode);
            this.TpgGeneral.Controls.Add(this.LueDeptCode);
            this.TpgGeneral.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TpgGeneral.Location = new System.Drawing.Point(4, 51);
            this.TpgGeneral.Name = "TpgGeneral";
            this.TpgGeneral.Size = new System.Drawing.Size(918, 564);
            this.TpgGeneral.TabIndex = 0;
            this.TpgGeneral.Text = "General";
            this.TpgGeneral.UseVisualStyleBackColor = true;
            // 
            // BtnSpouseEmpCode
            // 
            this.BtnSpouseEmpCode.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnSpouseEmpCode.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnSpouseEmpCode.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSpouseEmpCode.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnSpouseEmpCode.Appearance.Options.UseBackColor = true;
            this.BtnSpouseEmpCode.Appearance.Options.UseFont = true;
            this.BtnSpouseEmpCode.Appearance.Options.UseForeColor = true;
            this.BtnSpouseEmpCode.Appearance.Options.UseTextOptions = true;
            this.BtnSpouseEmpCode.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnSpouseEmpCode.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnSpouseEmpCode.Image = ((System.Drawing.Image)(resources.GetObject("BtnSpouseEmpCode.Image")));
            this.BtnSpouseEmpCode.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnSpouseEmpCode.Location = new System.Drawing.Point(382, 527);
            this.BtnSpouseEmpCode.Name = "BtnSpouseEmpCode";
            this.BtnSpouseEmpCode.Size = new System.Drawing.Size(24, 21);
            this.BtnSpouseEmpCode.TabIndex = 21;
            this.BtnSpouseEmpCode.ToolTip = "Show Employee List";
            this.BtnSpouseEmpCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnSpouseEmpCode.ToolTipTitle = "Run System";
            this.BtnSpouseEmpCode.Click += new System.EventHandler(this.BtnSpouseEmpCode_Click);
            // 
            // TxtSpouseEmpCode
            // 
            this.TxtSpouseEmpCode.EnterMoveNextControl = true;
            this.TxtSpouseEmpCode.Location = new System.Drawing.Point(125, 528);
            this.TxtSpouseEmpCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSpouseEmpCode.Name = "TxtSpouseEmpCode";
            this.TxtSpouseEmpCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtSpouseEmpCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSpouseEmpCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSpouseEmpCode.Properties.Appearance.Options.UseFont = true;
            this.TxtSpouseEmpCode.Properties.MaxLength = 80;
            this.TxtSpouseEmpCode.Properties.ReadOnly = true;
            this.TxtSpouseEmpCode.Size = new System.Drawing.Size(257, 20);
            this.TxtSpouseEmpCode.TabIndex = 79;
            // 
            // LblSpouseEmpCode
            // 
            this.LblSpouseEmpCode.AutoSize = true;
            this.LblSpouseEmpCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblSpouseEmpCode.ForeColor = System.Drawing.Color.Black;
            this.LblSpouseEmpCode.Location = new System.Drawing.Point(10, 530);
            this.LblSpouseEmpCode.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblSpouseEmpCode.Name = "LblSpouseEmpCode";
            this.LblSpouseEmpCode.Size = new System.Drawing.Size(111, 14);
            this.LblSpouseEmpCode.TabIndex = 78;
            this.LblSpouseEmpCode.Text = "Spouse Emp. Code";
            this.LblSpouseEmpCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ChkActingOfficial
            // 
            this.ChkActingOfficial.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ChkActingOfficial.Location = new System.Drawing.Point(387, 189);
            this.ChkActingOfficial.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.ChkActingOfficial.Name = "ChkActingOfficial";
            this.ChkActingOfficial.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ChkActingOfficial.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkActingOfficial.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ChkActingOfficial.Properties.Appearance.Options.UseBackColor = true;
            this.ChkActingOfficial.Properties.Appearance.Options.UseFont = true;
            this.ChkActingOfficial.Properties.Appearance.Options.UseForeColor = true;
            this.ChkActingOfficial.Properties.Caption = "Acting Official (PLT)";
            this.ChkActingOfficial.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.ChkActingOfficial.ShowToolTips = false;
            this.ChkActingOfficial.Size = new System.Drawing.Size(140, 22);
            this.ChkActingOfficial.TabIndex = 77;
            // 
            // DteContractDt
            // 
            this.DteContractDt.EditValue = null;
            this.DteContractDt.EnterMoveNextControl = true;
            this.DteContractDt.Location = new System.Drawing.Point(287, 317);
            this.DteContractDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteContractDt.Name = "DteContractDt";
            this.DteContractDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteContractDt.Properties.Appearance.Options.UseFont = true;
            this.DteContractDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteContractDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteContractDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteContractDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteContractDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteContractDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteContractDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteContractDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteContractDt.Properties.MaxLength = 16;
            this.DteContractDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteContractDt.Size = new System.Drawing.Size(95, 20);
            this.DteContractDt.TabIndex = 70;
            // 
            // LblContractDt
            // 
            this.LblContractDt.AutoSize = true;
            this.LblContractDt.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblContractDt.ForeColor = System.Drawing.Color.Black;
            this.LblContractDt.Location = new System.Drawing.Point(224, 320);
            this.LblContractDt.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.LblContractDt.Name = "LblContractDt";
            this.LblContractDt.Size = new System.Drawing.Size(54, 14);
            this.LblContractDt.TabIndex = 69;
            this.LblContractDt.Text = "Contract";
            this.LblContractDt.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label60.ForeColor = System.Drawing.Color.Black;
            this.label60.Location = new System.Drawing.Point(37, 215);
            this.label60.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(84, 14);
            this.label60.TabIndex = 40;
            this.label60.Text = "PositionStatus";
            this.label60.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LuePositionStatusCode
            // 
            this.LuePositionStatusCode.EnterMoveNextControl = true;
            this.LuePositionStatusCode.Location = new System.Drawing.Point(125, 212);
            this.LuePositionStatusCode.Margin = new System.Windows.Forms.Padding(5);
            this.LuePositionStatusCode.Name = "LuePositionStatusCode";
            this.LuePositionStatusCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePositionStatusCode.Properties.Appearance.Options.UseFont = true;
            this.LuePositionStatusCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePositionStatusCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePositionStatusCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePositionStatusCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePositionStatusCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePositionStatusCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePositionStatusCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePositionStatusCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePositionStatusCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePositionStatusCode.Properties.DropDownRows = 30;
            this.LuePositionStatusCode.Properties.MaxLength = 16;
            this.LuePositionStatusCode.Properties.NullText = "[Empty]";
            this.LuePositionStatusCode.Properties.PopupWidth = 300;
            this.LuePositionStatusCode.Size = new System.Drawing.Size(257, 20);
            this.LuePositionStatusCode.TabIndex = 41;
            this.LuePositionStatusCode.ToolTip = "F4 : Show/hide list";
            this.LuePositionStatusCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LuePositionStatusCode.EditValueChanged += new System.EventHandler(this.LuePositionStatusCode_EditValueChanged);
            // 
            // TxtRegEntCode
            // 
            this.TxtRegEntCode.EnterMoveNextControl = true;
            this.TxtRegEntCode.Location = new System.Drawing.Point(125, 107);
            this.TxtRegEntCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtRegEntCode.Name = "TxtRegEntCode";
            this.TxtRegEntCode.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(195)))), ((int)(((byte)(195)))));
            this.TxtRegEntCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtRegEntCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtRegEntCode.Properties.Appearance.Options.UseFont = true;
            this.TxtRegEntCode.Properties.MaxLength = 80;
            this.TxtRegEntCode.Properties.ReadOnly = true;
            this.TxtRegEntCode.Size = new System.Drawing.Size(257, 20);
            this.TxtRegEntCode.TabIndex = 33;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.ForeColor = System.Drawing.Color.Black;
            this.label53.Location = new System.Drawing.Point(54, 110);
            this.label53.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(67, 14);
            this.label53.TabIndex = 32;
            this.label53.Text = "Entity (SS)";
            this.label53.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteTGDt
            // 
            this.DteTGDt.EditValue = null;
            this.DteTGDt.EnterMoveNextControl = true;
            this.DteTGDt.Location = new System.Drawing.Point(125, 317);
            this.DteTGDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteTGDt.Name = "DteTGDt";
            this.DteTGDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTGDt.Properties.Appearance.Options.UseFont = true;
            this.DteTGDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteTGDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteTGDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteTGDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteTGDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteTGDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteTGDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteTGDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteTGDt.Properties.MaxLength = 16;
            this.DteTGDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteTGDt.Size = new System.Drawing.Size(98, 20);
            this.DteTGDt.TabIndex = 51;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(8, 320);
            this.label4.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(113, 14);
            this.label4.TabIndex = 50;
            this.label4.Text = "Training Graduation";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label70.ForeColor = System.Drawing.Color.Black;
            this.label70.Location = new System.Drawing.Point(82, 89);
            this.label70.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(39, 14);
            this.label70.TabIndex = 30;
            this.label70.Text = "Entity";
            this.label70.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueEntityCode
            // 
            this.LueEntityCode.EnterMoveNextControl = true;
            this.LueEntityCode.Location = new System.Drawing.Point(125, 86);
            this.LueEntityCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueEntityCode.Name = "LueEntityCode";
            this.LueEntityCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEntityCode.Properties.Appearance.Options.UseFont = true;
            this.LueEntityCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEntityCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueEntityCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEntityCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueEntityCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEntityCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueEntityCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEntityCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueEntityCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueEntityCode.Properties.DropDownRows = 30;
            this.LueEntityCode.Properties.MaxLength = 16;
            this.LueEntityCode.Properties.NullText = "[Empty]";
            this.LueEntityCode.Properties.PopupWidth = 300;
            this.LueEntityCode.Size = new System.Drawing.Size(257, 20);
            this.LueEntityCode.TabIndex = 31;
            this.LueEntityCode.ToolTip = "F4 : Show/hide list";
            this.LueEntityCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueEntityCode.EditValueChanged += new System.EventHandler(this.LueEntityCode_EditValueChanged);
            // 
            // TxtPOH
            // 
            this.TxtPOH.EnterMoveNextControl = true;
            this.TxtPOH.Location = new System.Drawing.Point(125, 275);
            this.TxtPOH.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPOH.Name = "TxtPOH";
            this.TxtPOH.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPOH.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPOH.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPOH.Properties.Appearance.Options.UseFont = true;
            this.TxtPOH.Properties.MaxLength = 40;
            this.TxtPOH.Size = new System.Drawing.Size(257, 20);
            this.TxtPOH.TabIndex = 45;
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label69.ForeColor = System.Drawing.Color.Black;
            this.label69.Location = new System.Drawing.Point(46, 278);
            this.label69.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(75, 14);
            this.label69.TabIndex = 44;
            this.label69.Text = "Point of Hire";
            this.label69.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblDivisionCode
            // 
            this.LblDivisionCode.AutoSize = true;
            this.LblDivisionCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblDivisionCode.ForeColor = System.Drawing.Color.Black;
            this.LblDivisionCode.Location = new System.Drawing.Point(75, 152);
            this.LblDivisionCode.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblDivisionCode.Name = "LblDivisionCode";
            this.LblDivisionCode.Size = new System.Drawing.Size(46, 14);
            this.LblDivisionCode.TabIndex = 36;
            this.LblDivisionCode.Text = "Division";
            this.LblDivisionCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueDivisionCode
            // 
            this.LueDivisionCode.EnterMoveNextControl = true;
            this.LueDivisionCode.Location = new System.Drawing.Point(125, 149);
            this.LueDivisionCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueDivisionCode.Name = "LueDivisionCode";
            this.LueDivisionCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDivisionCode.Properties.Appearance.Options.UseFont = true;
            this.LueDivisionCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDivisionCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueDivisionCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDivisionCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueDivisionCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDivisionCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueDivisionCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDivisionCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueDivisionCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueDivisionCode.Properties.DropDownRows = 30;
            this.LueDivisionCode.Properties.MaxLength = 16;
            this.LueDivisionCode.Properties.NullText = "[Empty]";
            this.LueDivisionCode.Properties.PopupWidth = 300;
            this.LueDivisionCode.Size = new System.Drawing.Size(257, 20);
            this.LueDivisionCode.TabIndex = 36;
            this.LueDivisionCode.ToolTip = "F4 : Show/hide list";
            this.LueDivisionCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueDivisionCode.EditValueChanged += new System.EventHandler(this.LueDivisionCode_EditValueChanged);
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label65.ForeColor = System.Drawing.Color.Black;
            this.label65.Location = new System.Drawing.Point(32, 257);
            this.label65.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(89, 14);
            this.label65.TabIndex = 42;
            this.label65.Text = "Working Group";
            this.label65.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueWorkGroup
            // 
            this.LueWorkGroup.EnterMoveNextControl = true;
            this.LueWorkGroup.Location = new System.Drawing.Point(125, 254);
            this.LueWorkGroup.Margin = new System.Windows.Forms.Padding(5);
            this.LueWorkGroup.Name = "LueWorkGroup";
            this.LueWorkGroup.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWorkGroup.Properties.Appearance.Options.UseFont = true;
            this.LueWorkGroup.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWorkGroup.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueWorkGroup.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWorkGroup.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueWorkGroup.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWorkGroup.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueWorkGroup.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueWorkGroup.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueWorkGroup.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueWorkGroup.Properties.DropDownRows = 30;
            this.LueWorkGroup.Properties.MaxLength = 16;
            this.LueWorkGroup.Properties.NullText = "[Empty]";
            this.LueWorkGroup.Properties.PopupWidth = 300;
            this.LueWorkGroup.Size = new System.Drawing.Size(257, 20);
            this.LueWorkGroup.TabIndex = 43;
            this.LueWorkGroup.ToolTip = "F4 : Show/hide list";
            this.LueWorkGroup.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueWorkGroup.EditValueChanged += new System.EventHandler(this.LueWorkGroup_EditValueChanged);
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label64.ForeColor = System.Drawing.Color.Black;
            this.label64.Location = new System.Drawing.Point(73, 236);
            this.label64.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(48, 14);
            this.label64.TabIndex = 41;
            this.label64.Text = "Section";
            this.label64.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueSection
            // 
            this.LueSection.EnterMoveNextControl = true;
            this.LueSection.Location = new System.Drawing.Point(125, 233);
            this.LueSection.Margin = new System.Windows.Forms.Padding(5);
            this.LueSection.Name = "LueSection";
            this.LueSection.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSection.Properties.Appearance.Options.UseFont = true;
            this.LueSection.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSection.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSection.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSection.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSection.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSection.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSection.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSection.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSection.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSection.Properties.DropDownRows = 30;
            this.LueSection.Properties.MaxLength = 16;
            this.LueSection.Properties.NullText = "[Empty]";
            this.LueSection.Properties.PopupWidth = 300;
            this.LueSection.Size = new System.Drawing.Size(257, 20);
            this.LueSection.TabIndex = 42;
            this.LueSection.ToolTip = "F4 : Show/hide list";
            this.LueSection.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSection.EditValueChanged += new System.EventHandler(this.LueSection_EditValueChanged);
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label63.ForeColor = System.Drawing.Color.Black;
            this.label63.Location = new System.Drawing.Point(70, 467);
            this.label63.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(51, 14);
            this.label63.TabIndex = 64;
            this.label63.Text = "Domicile";
            this.label63.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblMarital
            // 
            this.lblMarital.AutoSize = true;
            this.lblMarital.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMarital.ForeColor = System.Drawing.Color.Black;
            this.lblMarital.Location = new System.Drawing.Point(41, 509);
            this.lblMarital.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblMarital.Name = "lblMarital";
            this.lblMarital.Size = new System.Drawing.Size(80, 14);
            this.lblMarital.TabIndex = 69;
            this.lblMarital.Text = "Marital Status";
            this.lblMarital.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeDomicile
            // 
            this.MeeDomicile.EnterMoveNextControl = true;
            this.MeeDomicile.Location = new System.Drawing.Point(125, 464);
            this.MeeDomicile.Margin = new System.Windows.Forms.Padding(5);
            this.MeeDomicile.Name = "MeeDomicile";
            this.MeeDomicile.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeDomicile.Properties.Appearance.Options.UseFont = true;
            this.MeeDomicile.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeDomicile.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeDomicile.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeDomicile.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeDomicile.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeDomicile.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeDomicile.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeDomicile.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeDomicile.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeDomicile.Properties.MaxLength = 400;
            this.MeeDomicile.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeDomicile.Properties.ShowIcon = false;
            this.MeeDomicile.Size = new System.Drawing.Size(257, 20);
            this.MeeDomicile.TabIndex = 65;
            this.MeeDomicile.ToolTip = "F4 : Show/hide text";
            this.MeeDomicile.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeDomicile.ToolTipTitle = "Run System";
            // 
            // LblSiteCode
            // 
            this.LblSiteCode.AutoSize = true;
            this.LblSiteCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblSiteCode.ForeColor = System.Drawing.Color.Black;
            this.LblSiteCode.Location = new System.Drawing.Point(93, 131);
            this.LblSiteCode.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblSiteCode.Name = "LblSiteCode";
            this.LblSiteCode.Size = new System.Drawing.Size(28, 14);
            this.LblSiteCode.TabIndex = 34;
            this.LblSiteCode.Text = "Site";
            this.LblSiteCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueSiteCode
            // 
            this.LueSiteCode.EnterMoveNextControl = true;
            this.LueSiteCode.Location = new System.Drawing.Point(125, 128);
            this.LueSiteCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueSiteCode.Name = "LueSiteCode";
            this.LueSiteCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.Appearance.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSiteCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSiteCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSiteCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSiteCode.Properties.DropDownRows = 30;
            this.LueSiteCode.Properties.MaxLength = 16;
            this.LueSiteCode.Properties.NullText = "[Empty]";
            this.LueSiteCode.Properties.PopupWidth = 300;
            this.LueSiteCode.Size = new System.Drawing.Size(257, 20);
            this.LueSiteCode.TabIndex = 35;
            this.LueSiteCode.ToolTip = "F4 : Show/hide list";
            this.LueSiteCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSiteCode.EditValueChanged += new System.EventHandler(this.LueSiteCode_EditValueChanged);
            this.LueSiteCode.Validated += new System.EventHandler(this.LueSiteCode_Validated);
            // 
            // TxtRTRW
            // 
            this.TxtRTRW.EnterMoveNextControl = true;
            this.TxtRTRW.Location = new System.Drawing.Point(125, 422);
            this.TxtRTRW.Margin = new System.Windows.Forms.Padding(5);
            this.TxtRTRW.Name = "TxtRTRW";
            this.TxtRTRW.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtRTRW.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtRTRW.Properties.Appearance.Options.UseBackColor = true;
            this.TxtRTRW.Properties.Appearance.Options.UseFont = true;
            this.TxtRTRW.Properties.MaxLength = 10;
            this.TxtRTRW.Size = new System.Drawing.Size(257, 20);
            this.TxtRTRW.TabIndex = 61;
            this.TxtRTRW.Validated += new System.EventHandler(this.TxtRTRW_Validated);
            // 
            // LueMaritalStatus
            // 
            this.LueMaritalStatus.EnterMoveNextControl = true;
            this.LueMaritalStatus.Location = new System.Drawing.Point(125, 506);
            this.LueMaritalStatus.Margin = new System.Windows.Forms.Padding(5);
            this.LueMaritalStatus.Name = "LueMaritalStatus";
            this.LueMaritalStatus.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMaritalStatus.Properties.Appearance.Options.UseFont = true;
            this.LueMaritalStatus.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMaritalStatus.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueMaritalStatus.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMaritalStatus.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueMaritalStatus.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMaritalStatus.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueMaritalStatus.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMaritalStatus.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueMaritalStatus.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueMaritalStatus.Properties.DropDownRows = 30;
            this.LueMaritalStatus.Properties.MaxLength = 1;
            this.LueMaritalStatus.Properties.NullText = "[Empty]";
            this.LueMaritalStatus.Properties.PopupWidth = 300;
            this.LueMaritalStatus.Size = new System.Drawing.Size(257, 20);
            this.LueMaritalStatus.TabIndex = 70;
            this.LueMaritalStatus.ToolTip = "F4 : Show/hide list";
            this.LueMaritalStatus.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueMaritalStatus.EditValueChanged += new System.EventHandler(this.LueMaritalStatus_EditValueChanged);
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label56.ForeColor = System.Drawing.Color.Black;
            this.label56.Location = new System.Drawing.Point(75, 425);
            this.label56.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(46, 14);
            this.label56.TabIndex = 60;
            this.label56.Text = "RT/RW";
            this.label56.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtVillage
            // 
            this.TxtVillage.EnterMoveNextControl = true;
            this.TxtVillage.Location = new System.Drawing.Point(125, 401);
            this.TxtVillage.Margin = new System.Windows.Forms.Padding(5);
            this.TxtVillage.Name = "TxtVillage";
            this.TxtVillage.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtVillage.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtVillage.Properties.Appearance.Options.UseBackColor = true;
            this.TxtVillage.Properties.Appearance.Options.UseFont = true;
            this.TxtVillage.Properties.MaxLength = 40;
            this.TxtVillage.Size = new System.Drawing.Size(257, 20);
            this.TxtVillage.TabIndex = 59;
            this.TxtVillage.Validated += new System.EventHandler(this.TxtVillage_Validated);
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.ForeColor = System.Drawing.Color.Black;
            this.label55.Location = new System.Drawing.Point(80, 404);
            this.label55.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(41, 14);
            this.label55.TabIndex = 58;
            this.label55.Text = "Village";
            this.label55.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtSubDistrict
            // 
            this.TxtSubDistrict.EnterMoveNextControl = true;
            this.TxtSubDistrict.Location = new System.Drawing.Point(125, 380);
            this.TxtSubDistrict.Margin = new System.Windows.Forms.Padding(5);
            this.TxtSubDistrict.Name = "TxtSubDistrict";
            this.TxtSubDistrict.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtSubDistrict.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtSubDistrict.Properties.Appearance.Options.UseBackColor = true;
            this.TxtSubDistrict.Properties.Appearance.Options.UseFont = true;
            this.TxtSubDistrict.Properties.MaxLength = 40;
            this.TxtSubDistrict.Size = new System.Drawing.Size(257, 20);
            this.TxtSubDistrict.TabIndex = 57;
            this.TxtSubDistrict.Validated += new System.EventHandler(this.TxtSubDistrict_Validated);
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.ForeColor = System.Drawing.Color.Black;
            this.label54.Location = new System.Drawing.Point(52, 383);
            this.label54.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(69, 14);
            this.label54.TabIndex = 56;
            this.label54.Text = "Sub District";
            this.label54.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblEmploymentStatus
            // 
            this.LblEmploymentStatus.AutoSize = true;
            this.LblEmploymentStatus.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblEmploymentStatus.ForeColor = System.Drawing.Color.Black;
            this.LblEmploymentStatus.Location = new System.Drawing.Point(7, 68);
            this.LblEmploymentStatus.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblEmploymentStatus.Name = "LblEmploymentStatus";
            this.LblEmploymentStatus.Size = new System.Drawing.Size(114, 14);
            this.LblEmploymentStatus.TabIndex = 28;
            this.LblEmploymentStatus.Text = "Employment Status";
            this.LblEmploymentStatus.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtBarcodeCode
            // 
            this.TxtBarcodeCode.EnterMoveNextControl = true;
            this.TxtBarcodeCode.Location = new System.Drawing.Point(125, 44);
            this.TxtBarcodeCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtBarcodeCode.Name = "TxtBarcodeCode";
            this.TxtBarcodeCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtBarcodeCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBarcodeCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtBarcodeCode.Properties.Appearance.Options.UseFont = true;
            this.TxtBarcodeCode.Properties.MaxLength = 16;
            this.TxtBarcodeCode.Size = new System.Drawing.Size(257, 20);
            this.TxtBarcodeCode.TabIndex = 27;
            this.TxtBarcodeCode.Visible = false;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.ForeColor = System.Drawing.Color.Black;
            this.label52.Location = new System.Drawing.Point(38, 47);
            this.label52.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(83, 14);
            this.label52.TabIndex = 26;
            this.label52.Text = "Barcode Code";
            this.label52.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtShortCode
            // 
            this.TxtShortCode.EnterMoveNextControl = true;
            this.TxtShortCode.Location = new System.Drawing.Point(125, 23);
            this.TxtShortCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtShortCode.Name = "TxtShortCode";
            this.TxtShortCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtShortCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtShortCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtShortCode.Properties.Appearance.Options.UseFont = true;
            this.TxtShortCode.Properties.MaxLength = 8;
            this.TxtShortCode.Size = new System.Drawing.Size(257, 20);
            this.TxtShortCode.TabIndex = 25;
            this.TxtShortCode.Validated += new System.EventHandler(this.TxtShortCode_Validated);
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.ForeColor = System.Drawing.Color.Black;
            this.label50.Location = new System.Drawing.Point(52, 26);
            this.label50.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(69, 14);
            this.label50.TabIndex = 24;
            this.label50.Text = "Short Code";
            this.label50.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueEmploymentStatus
            // 
            this.LueEmploymentStatus.EnterMoveNextControl = true;
            this.LueEmploymentStatus.Location = new System.Drawing.Point(125, 65);
            this.LueEmploymentStatus.Margin = new System.Windows.Forms.Padding(5);
            this.LueEmploymentStatus.Name = "LueEmploymentStatus";
            this.LueEmploymentStatus.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEmploymentStatus.Properties.Appearance.Options.UseFont = true;
            this.LueEmploymentStatus.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEmploymentStatus.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueEmploymentStatus.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEmploymentStatus.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueEmploymentStatus.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEmploymentStatus.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueEmploymentStatus.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEmploymentStatus.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueEmploymentStatus.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueEmploymentStatus.Properties.DropDownRows = 30;
            this.LueEmploymentStatus.Properties.MaxLength = 1;
            this.LueEmploymentStatus.Properties.NullText = "[Empty]";
            this.LueEmploymentStatus.Properties.PopupWidth = 300;
            this.LueEmploymentStatus.Size = new System.Drawing.Size(257, 20);
            this.LueEmploymentStatus.TabIndex = 29;
            this.LueEmploymentStatus.ToolTip = "F4 : Show/hide list";
            this.LueEmploymentStatus.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueEmploymentStatus.EditValueChanged += new System.EventHandler(this.LueEmploymentStatus_EditValueChanged);
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel5.Controls.Add(this.label73);
            this.panel5.Controls.Add(this.LueBloodRhesus);
            this.panel5.Controls.Add(this.label71);
            this.panel5.Controls.Add(this.LueCostGroup);
            this.panel5.Controls.Add(this.label67);
            this.panel5.Controls.Add(this.LueEducationType);
            this.panel5.Controls.Add(this.label68);
            this.panel5.Controls.Add(this.LueShoeSize);
            this.panel5.Controls.Add(this.label61);
            this.panel5.Controls.Add(this.LueClothesSize);
            this.panel5.Controls.Add(this.label51);
            this.panel5.Controls.Add(this.label14);
            this.panel5.Controls.Add(this.TxtLevelCode);
            this.panel5.Controls.Add(this.label47);
            this.panel5.Controls.Add(this.DteLeaveStartDt);
            this.panel5.Controls.Add(this.label5);
            this.panel5.Controls.Add(this.LblPGCode);
            this.panel5.Controls.Add(this.LuePGCode);
            this.panel5.Controls.Add(this.label62);
            this.panel5.Controls.Add(this.LueBloodType);
            this.panel5.Controls.Add(this.LblSystemType);
            this.panel5.Controls.Add(this.LueSystemType);
            this.panel5.Controls.Add(this.TxtMother);
            this.panel5.Controls.Add(this.LblMother);
            this.panel5.Controls.Add(this.LblPayrunPeriod);
            this.panel5.Controls.Add(this.LuePayrunPeriod);
            this.panel5.Controls.Add(this.DteWeddingDt);
            this.panel5.Controls.Add(this.label59);
            this.panel5.Controls.Add(this.TxtBankAcName);
            this.panel5.Controls.Add(this.label49);
            this.panel5.Controls.Add(this.LblGrdLvlCode);
            this.panel5.Controls.Add(this.TxtEmail);
            this.panel5.Controls.Add(this.LueGrdLvlCode);
            this.panel5.Controls.Add(this.DteBirthDt);
            this.panel5.Controls.Add(this.TxtBankBranch);
            this.panel5.Controls.Add(this.label18);
            this.panel5.Controls.Add(this.label23);
            this.panel5.Controls.Add(this.TxtBirthPlace);
            this.panel5.Controls.Add(this.label13);
            this.panel5.Controls.Add(this.TxtBankAcNo);
            this.panel5.Controls.Add(this.label12);
            this.panel5.Controls.Add(this.LueReligion);
            this.panel5.Controls.Add(this.TxtMobile);
            this.panel5.Controls.Add(this.label24);
            this.panel5.Controls.Add(this.label22);
            this.panel5.Controls.Add(this.LueBankCode);
            this.panel5.Controls.Add(this.label21);
            this.panel5.Controls.Add(this.LuePTKP);
            this.panel5.Controls.Add(this.label17);
            this.panel5.Controls.Add(this.label19);
            this.panel5.Controls.Add(this.LuePayrollType);
            this.panel5.Controls.Add(this.label20);
            this.panel5.Controls.Add(this.label7);
            this.panel5.Controls.Add(this.TxtNPWP);
            this.panel5.Controls.Add(this.LueGender);
            this.panel5.Controls.Add(this.label16);
            this.panel5.Controls.Add(this.TxtPhone);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel5.Location = new System.Drawing.Point(544, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(370, 560);
            this.panel5.TabIndex = 68;
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label73.ForeColor = System.Drawing.Color.Black;
            this.label73.Location = new System.Drawing.Point(34, 132);
            this.label73.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(79, 14);
            this.label73.TabIndex = 85;
            this.label73.Text = "Blood Rhesus";
            this.label73.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueBloodRhesus
            // 
            this.LueBloodRhesus.EnterMoveNextControl = true;
            this.LueBloodRhesus.Location = new System.Drawing.Point(115, 128);
            this.LueBloodRhesus.Margin = new System.Windows.Forms.Padding(5);
            this.LueBloodRhesus.Name = "LueBloodRhesus";
            this.LueBloodRhesus.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBloodRhesus.Properties.Appearance.Options.UseFont = true;
            this.LueBloodRhesus.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBloodRhesus.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueBloodRhesus.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBloodRhesus.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueBloodRhesus.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBloodRhesus.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueBloodRhesus.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBloodRhesus.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueBloodRhesus.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueBloodRhesus.Properties.DropDownRows = 25;
            this.LueBloodRhesus.Properties.MaxLength = 2;
            this.LueBloodRhesus.Properties.NullText = "[Empty]";
            this.LueBloodRhesus.Properties.PopupWidth = 500;
            this.LueBloodRhesus.Size = new System.Drawing.Size(245, 20);
            this.LueBloodRhesus.TabIndex = 86;
            this.LueBloodRhesus.ToolTip = "F4 : Show/hide list";
            this.LueBloodRhesus.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueBloodRhesus.EditValueChanged += new System.EventHandler(this.LueBloodRhesus_EditValueChanged);
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label71.ForeColor = System.Drawing.Color.Black;
            this.label71.Location = new System.Drawing.Point(44, 5);
            this.label71.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(68, 14);
            this.label71.TabIndex = 71;
            this.label71.Text = "Cost Group";
            this.label71.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueCostGroup
            // 
            this.LueCostGroup.EnterMoveNextControl = true;
            this.LueCostGroup.Location = new System.Drawing.Point(115, 2);
            this.LueCostGroup.Margin = new System.Windows.Forms.Padding(5);
            this.LueCostGroup.Name = "LueCostGroup";
            this.LueCostGroup.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCostGroup.Properties.Appearance.Options.UseFont = true;
            this.LueCostGroup.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCostGroup.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCostGroup.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCostGroup.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCostGroup.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCostGroup.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCostGroup.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCostGroup.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCostGroup.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCostGroup.Properties.DropDownRows = 25;
            this.LueCostGroup.Properties.MaxLength = 2;
            this.LueCostGroup.Properties.NullText = "[Empty]";
            this.LueCostGroup.Properties.PopupWidth = 500;
            this.LueCostGroup.Size = new System.Drawing.Size(245, 20);
            this.LueCostGroup.TabIndex = 72;
            this.LueCostGroup.ToolTip = "F4 : Show/hide list";
            this.LueCostGroup.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCostGroup.EditValueChanged += new System.EventHandler(this.LueCostGroup_EditValueChanged);
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label67.ForeColor = System.Drawing.Color.Black;
            this.label67.Location = new System.Drawing.Point(20, 531);
            this.label67.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(93, 14);
            this.label67.TabIndex = 126;
            this.label67.Text = "Education Type";
            this.label67.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueEducationType
            // 
            this.LueEducationType.EnterMoveNextControl = true;
            this.LueEducationType.Location = new System.Drawing.Point(117, 528);
            this.LueEducationType.Margin = new System.Windows.Forms.Padding(5);
            this.LueEducationType.Name = "LueEducationType";
            this.LueEducationType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEducationType.Properties.Appearance.Options.UseFont = true;
            this.LueEducationType.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEducationType.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueEducationType.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEducationType.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueEducationType.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEducationType.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueEducationType.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEducationType.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueEducationType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueEducationType.Properties.DropDownRows = 30;
            this.LueEducationType.Properties.MaxLength = 1;
            this.LueEducationType.Properties.NullText = "[Empty]";
            this.LueEducationType.Properties.PopupWidth = 300;
            this.LueEducationType.Size = new System.Drawing.Size(244, 20);
            this.LueEducationType.TabIndex = 127;
            this.LueEducationType.ToolTip = "F4 : Show/hide list";
            this.LueEducationType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueEducationType.EditValueChanged += new System.EventHandler(this.LueEducationType_EditValueChanged);
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label68.ForeColor = System.Drawing.Color.Black;
            this.label68.Location = new System.Drawing.Point(220, 510);
            this.label68.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(60, 14);
            this.label68.TabIndex = 124;
            this.label68.Text = "Shoe Size";
            this.label68.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueShoeSize
            // 
            this.LueShoeSize.EnterMoveNextControl = true;
            this.LueShoeSize.Location = new System.Drawing.Point(280, 507);
            this.LueShoeSize.Margin = new System.Windows.Forms.Padding(5);
            this.LueShoeSize.Name = "LueShoeSize";
            this.LueShoeSize.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShoeSize.Properties.Appearance.Options.UseFont = true;
            this.LueShoeSize.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShoeSize.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueShoeSize.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShoeSize.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueShoeSize.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShoeSize.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueShoeSize.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueShoeSize.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueShoeSize.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueShoeSize.Properties.DropDownRows = 30;
            this.LueShoeSize.Properties.MaxLength = 1;
            this.LueShoeSize.Properties.NullText = "[Empty]";
            this.LueShoeSize.Properties.PopupWidth = 300;
            this.LueShoeSize.Size = new System.Drawing.Size(80, 20);
            this.LueShoeSize.TabIndex = 125;
            this.LueShoeSize.ToolTip = "F4 : Show/hide list";
            this.LueShoeSize.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueShoeSize.EditValueChanged += new System.EventHandler(this.LueShoeSize_EditValueChanged);
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label61.ForeColor = System.Drawing.Color.Black;
            this.label61.Location = new System.Drawing.Point(41, 510);
            this.label61.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(72, 14);
            this.label61.TabIndex = 122;
            this.label61.Text = "Clothes Size";
            this.label61.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueClothesSize
            // 
            this.LueClothesSize.EnterMoveNextControl = true;
            this.LueClothesSize.Location = new System.Drawing.Point(116, 507);
            this.LueClothesSize.Margin = new System.Windows.Forms.Padding(5);
            this.LueClothesSize.Name = "LueClothesSize";
            this.LueClothesSize.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueClothesSize.Properties.Appearance.Options.UseFont = true;
            this.LueClothesSize.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueClothesSize.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueClothesSize.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueClothesSize.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueClothesSize.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueClothesSize.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueClothesSize.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueClothesSize.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueClothesSize.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueClothesSize.Properties.DropDownRows = 30;
            this.LueClothesSize.Properties.MaxLength = 1;
            this.LueClothesSize.Properties.NullText = "[Empty]";
            this.LueClothesSize.Properties.PopupWidth = 300;
            this.LueClothesSize.Size = new System.Drawing.Size(104, 20);
            this.LueClothesSize.TabIndex = 123;
            this.LueClothesSize.ToolTip = "F4 : Show/hide list";
            this.LueClothesSize.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueClothesSize.EditValueChanged += new System.EventHandler(this.LueClothesSize_EditValueChanged);
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.ForeColor = System.Drawing.Color.Black;
            this.label51.Location = new System.Drawing.Point(78, 216);
            this.label51.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(35, 14);
            this.label51.TabIndex = 93;
            this.label51.Text = "Level";
            this.label51.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(51, 89);
            this.label14.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(62, 14);
            this.label14.TabIndex = 81;
            this.label14.Text = "Birth Date";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtLevelCode
            // 
            this.TxtLevelCode.EnterMoveNextControl = true;
            this.TxtLevelCode.Location = new System.Drawing.Point(116, 213);
            this.TxtLevelCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtLevelCode.Name = "TxtLevelCode";
            this.TxtLevelCode.Properties.Appearance.BackColor = System.Drawing.Color.Silver;
            this.TxtLevelCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtLevelCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtLevelCode.Properties.Appearance.Options.UseFont = true;
            this.TxtLevelCode.Properties.MaxLength = 80;
            this.TxtLevelCode.Properties.ReadOnly = true;
            this.TxtLevelCode.Size = new System.Drawing.Size(245, 20);
            this.TxtLevelCode.TabIndex = 94;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.label47.Location = new System.Drawing.Point(223, 488);
            this.label47.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(137, 14);
            this.label47.TabIndex = 121;
            this.label47.Text = "(Annual / Long Service)";
            this.label47.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteLeaveStartDt
            // 
            this.DteLeaveStartDt.EditValue = null;
            this.DteLeaveStartDt.EnterMoveNextControl = true;
            this.DteLeaveStartDt.Location = new System.Drawing.Point(116, 486);
            this.DteLeaveStartDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteLeaveStartDt.Name = "DteLeaveStartDt";
            this.DteLeaveStartDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteLeaveStartDt.Properties.Appearance.Options.UseFont = true;
            this.DteLeaveStartDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteLeaveStartDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteLeaveStartDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteLeaveStartDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteLeaveStartDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteLeaveStartDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteLeaveStartDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteLeaveStartDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteLeaveStartDt.Properties.MaxLength = 16;
            this.DteLeaveStartDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteLeaveStartDt.Size = new System.Drawing.Size(105, 20);
            this.DteLeaveStartDt.TabIndex = 120;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(16, 489);
            this.label5.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(97, 14);
            this.label5.TabIndex = 119;
            this.label5.Text = "Permanent Date";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblPGCode
            // 
            this.LblPGCode.AutoSize = true;
            this.LblPGCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblPGCode.ForeColor = System.Drawing.Color.Black;
            this.LblPGCode.Location = new System.Drawing.Point(35, 238);
            this.LblPGCode.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblPGCode.Name = "LblPGCode";
            this.LblPGCode.Size = new System.Drawing.Size(78, 14);
            this.LblPGCode.TabIndex = 95;
            this.LblPGCode.Text = "Payroll Group";
            this.LblPGCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LuePGCode
            // 
            this.LuePGCode.EnterMoveNextControl = true;
            this.LuePGCode.Location = new System.Drawing.Point(116, 234);
            this.LuePGCode.Margin = new System.Windows.Forms.Padding(5);
            this.LuePGCode.Name = "LuePGCode";
            this.LuePGCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePGCode.Properties.Appearance.Options.UseFont = true;
            this.LuePGCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePGCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePGCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePGCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePGCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePGCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePGCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePGCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePGCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePGCode.Properties.DropDownRows = 30;
            this.LuePGCode.Properties.NullText = "[Empty]";
            this.LuePGCode.Properties.PopupWidth = 300;
            this.LuePGCode.Size = new System.Drawing.Size(245, 20);
            this.LuePGCode.TabIndex = 96;
            this.LuePGCode.ToolTip = "F4 : Show/hide list";
            this.LuePGCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LuePGCode.EditValueChanged += new System.EventHandler(this.LuePGCode_EditValueChanged);
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label62.ForeColor = System.Drawing.Color.Black;
            this.label62.Location = new System.Drawing.Point(44, 111);
            this.label62.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(69, 14);
            this.label62.TabIndex = 83;
            this.label62.Text = "Blood Type";
            this.label62.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueBloodType
            // 
            this.LueBloodType.EnterMoveNextControl = true;
            this.LueBloodType.Location = new System.Drawing.Point(115, 107);
            this.LueBloodType.Margin = new System.Windows.Forms.Padding(5);
            this.LueBloodType.Name = "LueBloodType";
            this.LueBloodType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBloodType.Properties.Appearance.Options.UseFont = true;
            this.LueBloodType.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBloodType.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueBloodType.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBloodType.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueBloodType.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBloodType.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueBloodType.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBloodType.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueBloodType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueBloodType.Properties.DropDownRows = 25;
            this.LueBloodType.Properties.MaxLength = 2;
            this.LueBloodType.Properties.NullText = "[Empty]";
            this.LueBloodType.Properties.PopupWidth = 500;
            this.LueBloodType.Size = new System.Drawing.Size(245, 20);
            this.LueBloodType.TabIndex = 84;
            this.LueBloodType.ToolTip = "F4 : Show/hide list";
            this.LueBloodType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueBloodType.EditValueChanged += new System.EventHandler(this.LueBloodType_EditValueChanged);
            // 
            // LblSystemType
            // 
            this.LblSystemType.AutoSize = true;
            this.LblSystemType.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblSystemType.ForeColor = System.Drawing.Color.Black;
            this.LblSystemType.Location = new System.Drawing.Point(34, 175);
            this.LblSystemType.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblSystemType.Name = "LblSystemType";
            this.LblSystemType.Size = new System.Drawing.Size(79, 14);
            this.LblSystemType.TabIndex = 89;
            this.LblSystemType.Text = "System Type";
            this.LblSystemType.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueSystemType
            // 
            this.LueSystemType.EnterMoveNextControl = true;
            this.LueSystemType.Location = new System.Drawing.Point(115, 171);
            this.LueSystemType.Margin = new System.Windows.Forms.Padding(5);
            this.LueSystemType.Name = "LueSystemType";
            this.LueSystemType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSystemType.Properties.Appearance.Options.UseFont = true;
            this.LueSystemType.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSystemType.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueSystemType.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSystemType.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueSystemType.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSystemType.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueSystemType.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueSystemType.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueSystemType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueSystemType.Properties.DropDownRows = 30;
            this.LueSystemType.Properties.NullText = "[Empty]";
            this.LueSystemType.Properties.PopupWidth = 300;
            this.LueSystemType.Size = new System.Drawing.Size(245, 20);
            this.LueSystemType.TabIndex = 90;
            this.LueSystemType.ToolTip = "F4 : Show/hide list";
            this.LueSystemType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueSystemType.EditValueChanged += new System.EventHandler(this.LueSystemType_EditValueChanged);
            // 
            // TxtMother
            // 
            this.TxtMother.EnterMoveNextControl = true;
            this.TxtMother.Location = new System.Drawing.Point(115, 150);
            this.TxtMother.Margin = new System.Windows.Forms.Padding(5);
            this.TxtMother.Name = "TxtMother";
            this.TxtMother.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtMother.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtMother.Properties.Appearance.Options.UseBackColor = true;
            this.TxtMother.Properties.Appearance.Options.UseFont = true;
            this.TxtMother.Properties.MaxLength = 40;
            this.TxtMother.Size = new System.Drawing.Size(245, 20);
            this.TxtMother.TabIndex = 88;
            this.TxtMother.Validated += new System.EventHandler(this.TxtMother_Validated);
            // 
            // LblMother
            // 
            this.LblMother.AutoSize = true;
            this.LblMother.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblMother.ForeColor = System.Drawing.Color.Black;
            this.LblMother.Location = new System.Drawing.Point(15, 154);
            this.LblMother.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblMother.Name = "LblMother";
            this.LblMother.Size = new System.Drawing.Size(98, 14);
            this.LblMother.TabIndex = 87;
            this.LblMother.Text = "Biological Mother";
            this.LblMother.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblPayrunPeriod
            // 
            this.LblPayrunPeriod.AutoSize = true;
            this.LblPayrunPeriod.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblPayrunPeriod.ForeColor = System.Drawing.Color.Black;
            this.LblPayrunPeriod.Location = new System.Drawing.Point(31, 280);
            this.LblPayrunPeriod.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblPayrunPeriod.Name = "LblPayrunPeriod";
            this.LblPayrunPeriod.Size = new System.Drawing.Size(82, 14);
            this.LblPayrunPeriod.TabIndex = 99;
            this.LblPayrunPeriod.Text = "Payrun Period";
            this.LblPayrunPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LuePayrunPeriod
            // 
            this.LuePayrunPeriod.EnterMoveNextControl = true;
            this.LuePayrunPeriod.Location = new System.Drawing.Point(116, 276);
            this.LuePayrunPeriod.Margin = new System.Windows.Forms.Padding(5);
            this.LuePayrunPeriod.Name = "LuePayrunPeriod";
            this.LuePayrunPeriod.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePayrunPeriod.Properties.Appearance.Options.UseFont = true;
            this.LuePayrunPeriod.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePayrunPeriod.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePayrunPeriod.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePayrunPeriod.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePayrunPeriod.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePayrunPeriod.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePayrunPeriod.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePayrunPeriod.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePayrunPeriod.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePayrunPeriod.Properties.DropDownRows = 30;
            this.LuePayrunPeriod.Properties.MaxLength = 1;
            this.LuePayrunPeriod.Properties.NullText = "[Empty]";
            this.LuePayrunPeriod.Properties.PopupWidth = 300;
            this.LuePayrunPeriod.Size = new System.Drawing.Size(245, 20);
            this.LuePayrunPeriod.TabIndex = 100;
            this.LuePayrunPeriod.ToolTip = "F4 : Show/hide list";
            this.LuePayrunPeriod.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LuePayrunPeriod.EditValueChanged += new System.EventHandler(this.LuePayrunPeriod_EditValueChanged);
            // 
            // DteWeddingDt
            // 
            this.DteWeddingDt.EditValue = null;
            this.DteWeddingDt.EnterMoveNextControl = true;
            this.DteWeddingDt.Location = new System.Drawing.Point(115, 23);
            this.DteWeddingDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteWeddingDt.Name = "DteWeddingDt";
            this.DteWeddingDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteWeddingDt.Properties.Appearance.Options.UseFont = true;
            this.DteWeddingDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteWeddingDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteWeddingDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteWeddingDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteWeddingDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteWeddingDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteWeddingDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteWeddingDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteWeddingDt.Properties.MaxLength = 16;
            this.DteWeddingDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteWeddingDt.Size = new System.Drawing.Size(245, 20);
            this.DteWeddingDt.TabIndex = 74;
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label59.ForeColor = System.Drawing.Color.Black;
            this.label59.Location = new System.Drawing.Point(27, 26);
            this.label59.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(86, 14);
            this.label59.TabIndex = 73;
            this.label59.Text = "Wedding Date";
            this.label59.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtBankAcName
            // 
            this.TxtBankAcName.EnterMoveNextControl = true;
            this.TxtBankAcName.Location = new System.Drawing.Point(116, 381);
            this.TxtBankAcName.Margin = new System.Windows.Forms.Padding(5);
            this.TxtBankAcName.Name = "TxtBankAcName";
            this.TxtBankAcName.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtBankAcName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBankAcName.Properties.Appearance.Options.UseBackColor = true;
            this.TxtBankAcName.Properties.Appearance.Options.UseFont = true;
            this.TxtBankAcName.Properties.MaxLength = 80;
            this.TxtBankAcName.Size = new System.Drawing.Size(245, 20);
            this.TxtBankAcName.TabIndex = 110;
            this.TxtBankAcName.Validated += new System.EventHandler(this.TxtBankAcName_Validated);
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.ForeColor = System.Drawing.Color.Black;
            this.label49.Location = new System.Drawing.Point(25, 385);
            this.label49.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(88, 14);
            this.label49.TabIndex = 109;
            this.label49.Text = "Account Name";
            this.label49.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblGrdLvlCode
            // 
            this.LblGrdLvlCode.AutoSize = true;
            this.LblGrdLvlCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblGrdLvlCode.ForeColor = System.Drawing.Color.Black;
            this.LblGrdLvlCode.Location = new System.Drawing.Point(74, 196);
            this.LblGrdLvlCode.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblGrdLvlCode.Name = "LblGrdLvlCode";
            this.LblGrdLvlCode.Size = new System.Drawing.Size(39, 14);
            this.LblGrdLvlCode.TabIndex = 91;
            this.LblGrdLvlCode.Text = "Grade";
            this.LblGrdLvlCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtEmail
            // 
            this.TxtEmail.EnterMoveNextControl = true;
            this.TxtEmail.Location = new System.Drawing.Point(116, 465);
            this.TxtEmail.Margin = new System.Windows.Forms.Padding(5);
            this.TxtEmail.Name = "TxtEmail";
            this.TxtEmail.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtEmail.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtEmail.Properties.Appearance.Options.UseBackColor = true;
            this.TxtEmail.Properties.Appearance.Options.UseFont = true;
            this.TxtEmail.Properties.MaxLength = 80;
            this.TxtEmail.Size = new System.Drawing.Size(245, 20);
            this.TxtEmail.TabIndex = 118;
            this.TxtEmail.Validated += new System.EventHandler(this.TxtEmail_Validated);
            // 
            // LueGrdLvlCode
            // 
            this.LueGrdLvlCode.EnterMoveNextControl = true;
            this.LueGrdLvlCode.Location = new System.Drawing.Point(116, 192);
            this.LueGrdLvlCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueGrdLvlCode.Name = "LueGrdLvlCode";
            this.LueGrdLvlCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGrdLvlCode.Properties.Appearance.Options.UseFont = true;
            this.LueGrdLvlCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGrdLvlCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueGrdLvlCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGrdLvlCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueGrdLvlCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGrdLvlCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueGrdLvlCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGrdLvlCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueGrdLvlCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueGrdLvlCode.Properties.DropDownRows = 30;
            this.LueGrdLvlCode.Properties.NullText = "[Empty]";
            this.LueGrdLvlCode.Properties.PopupWidth = 300;
            this.LueGrdLvlCode.Size = new System.Drawing.Size(245, 20);
            this.LueGrdLvlCode.TabIndex = 92;
            this.LueGrdLvlCode.ToolTip = "F4 : Show/hide list";
            this.LueGrdLvlCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueGrdLvlCode.EditValueChanged += new System.EventHandler(this.LueGrdLvlCode_EditValueChanged);
            // 
            // DteBirthDt
            // 
            this.DteBirthDt.EditValue = null;
            this.DteBirthDt.EnterMoveNextControl = true;
            this.DteBirthDt.Location = new System.Drawing.Point(115, 86);
            this.DteBirthDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteBirthDt.Name = "DteBirthDt";
            this.DteBirthDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteBirthDt.Properties.Appearance.Options.UseFont = true;
            this.DteBirthDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteBirthDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteBirthDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteBirthDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteBirthDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteBirthDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteBirthDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteBirthDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteBirthDt.Properties.MaxLength = 8;
            this.DteBirthDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteBirthDt.Size = new System.Drawing.Size(245, 20);
            this.DteBirthDt.TabIndex = 82;
            // 
            // TxtBankBranch
            // 
            this.TxtBankBranch.EnterMoveNextControl = true;
            this.TxtBankBranch.Location = new System.Drawing.Point(116, 360);
            this.TxtBankBranch.Margin = new System.Windows.Forms.Padding(5);
            this.TxtBankBranch.Name = "TxtBankBranch";
            this.TxtBankBranch.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtBankBranch.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBankBranch.Properties.Appearance.Options.UseBackColor = true;
            this.TxtBankBranch.Properties.Appearance.Options.UseFont = true;
            this.TxtBankBranch.Properties.MaxLength = 80;
            this.TxtBankBranch.Size = new System.Drawing.Size(245, 20);
            this.TxtBankBranch.TabIndex = 108;
            this.TxtBankBranch.Validated += new System.EventHandler(this.TxtBankBranch_Validated);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(79, 468);
            this.label18.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(34, 14);
            this.label18.TabIndex = 117;
            this.label18.Text = "Email";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Black;
            this.label23.Location = new System.Drawing.Point(34, 364);
            this.label23.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(79, 14);
            this.label23.TabIndex = 107;
            this.label23.Text = "Branch Name";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtBirthPlace
            // 
            this.TxtBirthPlace.EnterMoveNextControl = true;
            this.TxtBirthPlace.Location = new System.Drawing.Point(115, 65);
            this.TxtBirthPlace.Margin = new System.Windows.Forms.Padding(5);
            this.TxtBirthPlace.Name = "TxtBirthPlace";
            this.TxtBirthPlace.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtBirthPlace.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBirthPlace.Properties.Appearance.Options.UseBackColor = true;
            this.TxtBirthPlace.Properties.Appearance.Options.UseFont = true;
            this.TxtBirthPlace.Properties.MaxLength = 80;
            this.TxtBirthPlace.Size = new System.Drawing.Size(245, 20);
            this.TxtBirthPlace.TabIndex = 80;
            this.TxtBirthPlace.Validated += new System.EventHandler(this.TxtBirthPlace_Validated);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(49, 68);
            this.label13.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(64, 14);
            this.label13.TabIndex = 79;
            this.label13.Text = "Birth Place";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtBankAcNo
            // 
            this.TxtBankAcNo.EnterMoveNextControl = true;
            this.TxtBankAcNo.Location = new System.Drawing.Point(116, 402);
            this.TxtBankAcNo.Margin = new System.Windows.Forms.Padding(5);
            this.TxtBankAcNo.Name = "TxtBankAcNo";
            this.TxtBankAcNo.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtBankAcNo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtBankAcNo.Properties.Appearance.Options.UseBackColor = true;
            this.TxtBankAcNo.Properties.Appearance.Options.UseFont = true;
            this.TxtBankAcNo.Properties.MaxLength = 80;
            this.TxtBankAcNo.Size = new System.Drawing.Size(245, 20);
            this.TxtBankAcNo.TabIndex = 112;
            this.TxtBankAcNo.Validated += new System.EventHandler(this.TxtBankAcNo_Validated);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(213, 47);
            this.label12.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(48, 14);
            this.label12.TabIndex = 77;
            this.label12.Text = "Religion";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueReligion
            // 
            this.LueReligion.EnterMoveNextControl = true;
            this.LueReligion.Location = new System.Drawing.Point(262, 44);
            this.LueReligion.Margin = new System.Windows.Forms.Padding(5);
            this.LueReligion.Name = "LueReligion";
            this.LueReligion.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueReligion.Properties.Appearance.Options.UseFont = true;
            this.LueReligion.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueReligion.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueReligion.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueReligion.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueReligion.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueReligion.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueReligion.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueReligion.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueReligion.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueReligion.Properties.DropDownRows = 30;
            this.LueReligion.Properties.MaxLength = 2;
            this.LueReligion.Properties.NullText = "[Empty]";
            this.LueReligion.Properties.PopupWidth = 300;
            this.LueReligion.Size = new System.Drawing.Size(98, 20);
            this.LueReligion.TabIndex = 78;
            this.LueReligion.ToolTip = "F4 : Show/hide list";
            this.LueReligion.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueReligion.EditValueChanged += new System.EventHandler(this.LueReligion_EditValueChanged);
            // 
            // TxtMobile
            // 
            this.TxtMobile.EnterMoveNextControl = true;
            this.TxtMobile.Location = new System.Drawing.Point(116, 444);
            this.TxtMobile.Margin = new System.Windows.Forms.Padding(5);
            this.TxtMobile.Name = "TxtMobile";
            this.TxtMobile.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtMobile.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtMobile.Properties.Appearance.Options.UseBackColor = true;
            this.TxtMobile.Properties.Appearance.Options.UseFont = true;
            this.TxtMobile.Properties.MaxLength = 80;
            this.TxtMobile.Size = new System.Drawing.Size(245, 20);
            this.TxtMobile.TabIndex = 116;
            this.TxtMobile.Validated += new System.EventHandler(this.TxtMobile_Validated);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Black;
            this.label24.Location = new System.Drawing.Point(51, 405);
            this.label24.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(62, 14);
            this.label24.TabIndex = 111;
            this.label24.Text = "Account#";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(45, 342);
            this.label22.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(68, 14);
            this.label22.TabIndex = 105;
            this.label22.Text = "Bank Name";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueBankCode
            // 
            this.LueBankCode.EnterMoveNextControl = true;
            this.LueBankCode.Location = new System.Drawing.Point(116, 339);
            this.LueBankCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueBankCode.Name = "LueBankCode";
            this.LueBankCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.Appearance.Options.UseFont = true;
            this.LueBankCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueBankCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueBankCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueBankCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueBankCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueBankCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueBankCode.Properties.DropDownRows = 30;
            this.LueBankCode.Properties.NullText = "[Empty]";
            this.LueBankCode.Properties.PopupWidth = 300;
            this.LueBankCode.Size = new System.Drawing.Size(245, 20);
            this.LueBankCode.TabIndex = 106;
            this.LueBankCode.ToolTip = "F4 : Show/hide list";
            this.LueBankCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueBankCode.EditValueChanged += new System.EventHandler(this.LueBankCode_EditValueChanged);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Black;
            this.label21.Location = new System.Drawing.Point(6, 322);
            this.label21.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(107, 14);
            this.label21.TabIndex = 103;
            this.label21.Text = "Non-Incoming Tax";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LuePTKP
            // 
            this.LuePTKP.EnterMoveNextControl = true;
            this.LuePTKP.Location = new System.Drawing.Point(116, 318);
            this.LuePTKP.Margin = new System.Windows.Forms.Padding(5);
            this.LuePTKP.Name = "LuePTKP";
            this.LuePTKP.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePTKP.Properties.Appearance.Options.UseFont = true;
            this.LuePTKP.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePTKP.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePTKP.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePTKP.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePTKP.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePTKP.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePTKP.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePTKP.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePTKP.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePTKP.Properties.DropDownRows = 30;
            this.LuePTKP.Properties.NullText = "[Empty]";
            this.LuePTKP.Properties.PopupWidth = 300;
            this.LuePTKP.Size = new System.Drawing.Size(245, 20);
            this.LuePTKP.TabIndex = 104;
            this.LuePTKP.ToolTip = "F4 : Show/hide list";
            this.LuePTKP.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LuePTKP.EditValueChanged += new System.EventHandler(this.LuePTKP_EditValueChanged);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(72, 447);
            this.label17.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(41, 14);
            this.label17.TabIndex = 115;
            this.label17.Text = "Mobile";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(40, 259);
            this.label19.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(73, 14);
            this.label19.TabIndex = 97;
            this.label19.Text = "Payroll Type";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LuePayrollType
            // 
            this.LuePayrollType.EnterMoveNextControl = true;
            this.LuePayrollType.Location = new System.Drawing.Point(116, 255);
            this.LuePayrollType.Margin = new System.Windows.Forms.Padding(5);
            this.LuePayrollType.Name = "LuePayrollType";
            this.LuePayrollType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePayrollType.Properties.Appearance.Options.UseFont = true;
            this.LuePayrollType.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePayrollType.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePayrollType.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePayrollType.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePayrollType.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePayrollType.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePayrollType.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePayrollType.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePayrollType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePayrollType.Properties.DropDownRows = 30;
            this.LuePayrollType.Properties.NullText = "[Empty]";
            this.LuePayrollType.Properties.PopupWidth = 300;
            this.LuePayrollType.Size = new System.Drawing.Size(245, 20);
            this.LuePayrollType.TabIndex = 98;
            this.LuePayrollType.ToolTip = "F4 : Show/hide list";
            this.LuePayrollType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LuePayrollType.EditValueChanged += new System.EventHandler(this.LuePayrollType_EditValueChanged);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(72, 301);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(41, 14);
            this.label20.TabIndex = 101;
            this.label20.Text = "NPWP";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(66, 47);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(47, 14);
            this.label7.TabIndex = 75;
            this.label7.Text = "Gender";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtNPWP
            // 
            this.TxtNPWP.EnterMoveNextControl = true;
            this.TxtNPWP.Location = new System.Drawing.Point(116, 297);
            this.TxtNPWP.Name = "TxtNPWP";
            this.TxtNPWP.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtNPWP.Properties.Appearance.Options.UseFont = true;
            this.TxtNPWP.Properties.MaxLength = 40;
            this.TxtNPWP.Size = new System.Drawing.Size(245, 20);
            this.TxtNPWP.TabIndex = 102;
            // 
            // LueGender
            // 
            this.LueGender.EnterMoveNextControl = true;
            this.LueGender.Location = new System.Drawing.Point(115, 44);
            this.LueGender.Margin = new System.Windows.Forms.Padding(5);
            this.LueGender.Name = "LueGender";
            this.LueGender.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGender.Properties.Appearance.Options.UseFont = true;
            this.LueGender.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGender.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueGender.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGender.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueGender.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGender.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueGender.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueGender.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueGender.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueGender.Properties.DropDownRows = 30;
            this.LueGender.Properties.MaxLength = 1;
            this.LueGender.Properties.NullText = "[Empty]";
            this.LueGender.Properties.PopupWidth = 300;
            this.LueGender.Size = new System.Drawing.Size(98, 20);
            this.LueGender.TabIndex = 76;
            this.LueGender.ToolTip = "F4 : Show/hide list";
            this.LueGender.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueGender.EditValueChanged += new System.EventHandler(this.LueGender_EditValueChanged);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(71, 426);
            this.label16.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(42, 14);
            this.label16.TabIndex = 113;
            this.label16.Text = "Phone";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtPhone
            // 
            this.TxtPhone.EnterMoveNextControl = true;
            this.TxtPhone.Location = new System.Drawing.Point(116, 423);
            this.TxtPhone.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPhone.Name = "TxtPhone";
            this.TxtPhone.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPhone.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPhone.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPhone.Properties.Appearance.Options.UseFont = true;
            this.TxtPhone.Properties.MaxLength = 80;
            this.TxtPhone.Size = new System.Drawing.Size(245, 20);
            this.TxtPhone.TabIndex = 114;
            this.TxtPhone.Validated += new System.EventHandler(this.TxtPhone_Validated);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(94, 362);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(27, 14);
            this.label9.TabIndex = 54;
            this.label9.Text = "City";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueCity
            // 
            this.LueCity.EnterMoveNextControl = true;
            this.LueCity.Location = new System.Drawing.Point(125, 359);
            this.LueCity.Margin = new System.Windows.Forms.Padding(5);
            this.LueCity.Name = "LueCity";
            this.LueCity.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCity.Properties.Appearance.Options.UseFont = true;
            this.LueCity.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCity.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCity.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCity.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCity.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCity.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCity.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCity.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCity.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCity.Properties.DropDownRows = 30;
            this.LueCity.Properties.MaxLength = 16;
            this.LueCity.Properties.NullText = "[Empty]";
            this.LueCity.Properties.PopupWidth = 300;
            this.LueCity.Size = new System.Drawing.Size(257, 20);
            this.LueCity.TabIndex = 55;
            this.LueCity.ToolTip = "F4 : Show/hide list";
            this.LueCity.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCity.EditValueChanged += new System.EventHandler(this.LueCity_EditValueChanged);
            // 
            // TxtIdNumber
            // 
            this.TxtIdNumber.EnterMoveNextControl = true;
            this.TxtIdNumber.Location = new System.Drawing.Point(125, 485);
            this.TxtIdNumber.Margin = new System.Windows.Forms.Padding(5);
            this.TxtIdNumber.Name = "TxtIdNumber";
            this.TxtIdNumber.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtIdNumber.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtIdNumber.Properties.Appearance.Options.UseBackColor = true;
            this.TxtIdNumber.Properties.Appearance.Options.UseFont = true;
            this.TxtIdNumber.Properties.MaxLength = 40;
            this.TxtIdNumber.Size = new System.Drawing.Size(257, 20);
            this.TxtIdNumber.TabIndex = 67;
            // 
            // DteResignDt
            // 
            this.DteResignDt.EditValue = null;
            this.DteResignDt.EnterMoveNextControl = true;
            this.DteResignDt.Location = new System.Drawing.Point(287, 296);
            this.DteResignDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteResignDt.Name = "DteResignDt";
            this.DteResignDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteResignDt.Properties.Appearance.Options.UseFont = true;
            this.DteResignDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteResignDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteResignDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteResignDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteResignDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteResignDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteResignDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteResignDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteResignDt.Properties.MaxLength = 16;
            this.DteResignDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteResignDt.Size = new System.Drawing.Size(95, 20);
            this.DteResignDt.TabIndex = 49;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(236, 299);
            this.label10.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(42, 14);
            this.label10.TabIndex = 48;
            this.label10.Text = "Resign";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(62, 488);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(59, 14);
            this.label11.TabIndex = 66;
            this.label11.Text = "Identity#";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DteJoinDt
            // 
            this.DteJoinDt.EditValue = null;
            this.DteJoinDt.EnterMoveNextControl = true;
            this.DteJoinDt.Location = new System.Drawing.Point(125, 296);
            this.DteJoinDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteJoinDt.Name = "DteJoinDt";
            this.DteJoinDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteJoinDt.Properties.Appearance.Options.UseFont = true;
            this.DteJoinDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteJoinDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteJoinDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteJoinDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteJoinDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteJoinDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteJoinDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteJoinDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteJoinDt.Properties.MaxLength = 16;
            this.DteJoinDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteJoinDt.Size = new System.Drawing.Size(98, 20);
            this.DteJoinDt.TabIndex = 47;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Red;
            this.label6.Location = new System.Drawing.Point(63, 299);
            this.label6.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(58, 14);
            this.label6.TabIndex = 46;
            this.label6.Text = "Join Date";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TxtUserCode
            // 
            this.TxtUserCode.EnterMoveNextControl = true;
            this.TxtUserCode.Location = new System.Drawing.Point(125, 2);
            this.TxtUserCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtUserCode.Name = "TxtUserCode";
            this.TxtUserCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtUserCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtUserCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtUserCode.Properties.Appearance.Options.UseFont = true;
            this.TxtUserCode.Properties.MaxLength = 50;
            this.TxtUserCode.Size = new System.Drawing.Size(257, 20);
            this.TxtUserCode.TabIndex = 23;
            this.TxtUserCode.Validated += new System.EventHandler(this.TxtUserCode_Validated);
            // 
            // TxtPostalCode
            // 
            this.TxtPostalCode.EnterMoveNextControl = true;
            this.TxtPostalCode.Location = new System.Drawing.Point(125, 443);
            this.TxtPostalCode.Margin = new System.Windows.Forms.Padding(5);
            this.TxtPostalCode.Name = "TxtPostalCode";
            this.TxtPostalCode.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TxtPostalCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPostalCode.Properties.Appearance.Options.UseBackColor = true;
            this.TxtPostalCode.Properties.Appearance.Options.UseFont = true;
            this.TxtPostalCode.Properties.MaxLength = 16;
            this.TxtPostalCode.Size = new System.Drawing.Size(257, 20);
            this.TxtPostalCode.TabIndex = 63;
            this.TxtPostalCode.Validated += new System.EventHandler(this.TxtPostalCode_Validated);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(58, 5);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 14);
            this.label3.TabIndex = 22;
            this.label3.Text = "User Code";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(50, 446);
            this.label15.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(71, 14);
            this.label15.TabIndex = 62;
            this.label15.Text = "Postal Code";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeeAddress
            // 
            this.MeeAddress.EnterMoveNextControl = true;
            this.MeeAddress.Location = new System.Drawing.Point(125, 338);
            this.MeeAddress.Margin = new System.Windows.Forms.Padding(5);
            this.MeeAddress.Name = "MeeAddress";
            this.MeeAddress.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAddress.Properties.Appearance.Options.UseFont = true;
            this.MeeAddress.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAddress.Properties.AppearanceDisabled.Options.UseFont = true;
            this.MeeAddress.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAddress.Properties.AppearanceDropDown.Options.UseFont = true;
            this.MeeAddress.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAddress.Properties.AppearanceFocused.Options.UseFont = true;
            this.MeeAddress.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MeeAddress.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.MeeAddress.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MeeAddress.Properties.MaxLength = 80;
            this.MeeAddress.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.MeeAddress.Properties.ShowIcon = false;
            this.MeeAddress.Size = new System.Drawing.Size(257, 20);
            this.MeeAddress.TabIndex = 53;
            this.MeeAddress.ToolTip = "F4 : Show/hide text";
            this.MeeAddress.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.MeeAddress.ToolTipTitle = "Run System";
            this.MeeAddress.EditValueChanged += new System.EventHandler(this.MeeAddress_EditValueChanged);
            this.MeeAddress.Validated += new System.EventHandler(this.MeeAddress_Validated);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(71, 341);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(50, 14);
            this.label8.TabIndex = 52;
            this.label8.Text = "Address";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LblPosCode
            // 
            this.LblPosCode.AutoSize = true;
            this.LblPosCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblPosCode.ForeColor = System.Drawing.Color.Black;
            this.LblPosCode.Location = new System.Drawing.Point(72, 194);
            this.LblPosCode.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblPosCode.Name = "LblPosCode";
            this.LblPosCode.Size = new System.Drawing.Size(49, 14);
            this.LblPosCode.TabIndex = 38;
            this.LblPosCode.Text = "Position";
            this.LblPosCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LuePosCode
            // 
            this.LuePosCode.EnterMoveNextControl = true;
            this.LuePosCode.Location = new System.Drawing.Point(125, 191);
            this.LuePosCode.Margin = new System.Windows.Forms.Padding(5);
            this.LuePosCode.Name = "LuePosCode";
            this.LuePosCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePosCode.Properties.Appearance.Options.UseFont = true;
            this.LuePosCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePosCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LuePosCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePosCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LuePosCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePosCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LuePosCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LuePosCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LuePosCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LuePosCode.Properties.DropDownRows = 30;
            this.LuePosCode.Properties.MaxLength = 16;
            this.LuePosCode.Properties.NullText = "[Empty]";
            this.LuePosCode.Properties.PopupWidth = 300;
            this.LuePosCode.Size = new System.Drawing.Size(257, 20);
            this.LuePosCode.TabIndex = 39;
            this.LuePosCode.ToolTip = "F4 : Show/hide list";
            this.LuePosCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LuePosCode.EditValueChanged += new System.EventHandler(this.LuePosCode_EditValueChanged);
            this.LuePosCode.Validated += new System.EventHandler(this.LuePosCode_Validated);
            // 
            // LblDeptCode
            // 
            this.LblDeptCode.AutoSize = true;
            this.LblDeptCode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblDeptCode.ForeColor = System.Drawing.Color.Black;
            this.LblDeptCode.Location = new System.Drawing.Point(48, 173);
            this.LblDeptCode.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.LblDeptCode.Name = "LblDeptCode";
            this.LblDeptCode.Size = new System.Drawing.Size(73, 14);
            this.LblDeptCode.TabIndex = 36;
            this.LblDeptCode.Text = "Department";
            this.LblDeptCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueDeptCode
            // 
            this.LueDeptCode.EnterMoveNextControl = true;
            this.LueDeptCode.Location = new System.Drawing.Point(125, 170);
            this.LueDeptCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueDeptCode.Name = "LueDeptCode";
            this.LueDeptCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.Appearance.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueDeptCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueDeptCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueDeptCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueDeptCode.Properties.DropDownRows = 30;
            this.LueDeptCode.Properties.MaxLength = 16;
            this.LueDeptCode.Properties.NullText = "[Empty]";
            this.LueDeptCode.Properties.PopupWidth = 300;
            this.LueDeptCode.Size = new System.Drawing.Size(257, 20);
            this.LueDeptCode.TabIndex = 37;
            this.LueDeptCode.ToolTip = "F4 : Show/hide list";
            this.LueDeptCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueDeptCode.EditValueChanged += new System.EventHandler(this.LueDeptCode_EditValueChanged);
            this.LueDeptCode.Validated += new System.EventHandler(this.LueDeptCode_Validated);
            // 
            // TpgEmployeeFamily
            // 
            this.TpgEmployeeFamily.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.TpgEmployeeFamily.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.TpgEmployeeFamily.Controls.Add(this.splitContainer2);
            this.TpgEmployeeFamily.Location = new System.Drawing.Point(4, 51);
            this.TpgEmployeeFamily.Name = "TpgEmployeeFamily";
            this.TpgEmployeeFamily.Size = new System.Drawing.Size(918, 564);
            this.TpgEmployeeFamily.TabIndex = 1;
            this.TpgEmployeeFamily.Text = "Personal Ref+Vaksin";
            this.TpgEmployeeFamily.UseVisualStyleBackColor = true;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.LueEducationLevelCode);
            this.splitContainer2.Panel1.Controls.Add(this.LueProfessionCode);
            this.splitContainer2.Panel1.Controls.Add(this.DteFamBirthDt);
            this.splitContainer2.Panel1.Controls.Add(this.LueFamGender);
            this.splitContainer2.Panel1.Controls.Add(this.LueStatus);
            this.splitContainer2.Panel1.Controls.Add(this.Grd1);
            this.splitContainer2.Panel1.Controls.Add(this.BtnFamily);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.Grd7);
            this.splitContainer2.Panel2.Controls.Add(this.button1);
            this.splitContainer2.Size = new System.Drawing.Size(914, 560);
            this.splitContainer2.SplitterDistance = 252;
            this.splitContainer2.TabIndex = 41;
            // 
            // LueEducationLevelCode
            // 
            this.LueEducationLevelCode.EnterMoveNextControl = true;
            this.LueEducationLevelCode.Location = new System.Drawing.Point(639, 44);
            this.LueEducationLevelCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueEducationLevelCode.Name = "LueEducationLevelCode";
            this.LueEducationLevelCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEducationLevelCode.Properties.Appearance.Options.UseFont = true;
            this.LueEducationLevelCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEducationLevelCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueEducationLevelCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEducationLevelCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueEducationLevelCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEducationLevelCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueEducationLevelCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueEducationLevelCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueEducationLevelCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueEducationLevelCode.Properties.DropDownRows = 25;
            this.LueEducationLevelCode.Properties.NullText = "[Empty]";
            this.LueEducationLevelCode.Properties.PopupWidth = 500;
            this.LueEducationLevelCode.Size = new System.Drawing.Size(131, 20);
            this.LueEducationLevelCode.TabIndex = 27;
            this.LueEducationLevelCode.ToolTip = "F4 : Show/hide list";
            this.LueEducationLevelCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueEducationLevelCode.EditValueChanged += new System.EventHandler(this.LueEducationLevelCode_EditValueChanged);
            this.LueEducationLevelCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueEducationLevelCode_KeyDown);
            this.LueEducationLevelCode.Leave += new System.EventHandler(this.LueEducationLevelCode_Leave);
            // 
            // LueProfessionCode
            // 
            this.LueProfessionCode.EnterMoveNextControl = true;
            this.LueProfessionCode.Location = new System.Drawing.Point(498, 44);
            this.LueProfessionCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueProfessionCode.Name = "LueProfessionCode";
            this.LueProfessionCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProfessionCode.Properties.Appearance.Options.UseFont = true;
            this.LueProfessionCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProfessionCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueProfessionCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProfessionCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueProfessionCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProfessionCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueProfessionCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueProfessionCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueProfessionCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueProfessionCode.Properties.DropDownRows = 25;
            this.LueProfessionCode.Properties.NullText = "[Empty]";
            this.LueProfessionCode.Properties.PopupWidth = 500;
            this.LueProfessionCode.Size = new System.Drawing.Size(131, 20);
            this.LueProfessionCode.TabIndex = 26;
            this.LueProfessionCode.ToolTip = "F4 : Show/hide list";
            this.LueProfessionCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueProfessionCode.EditValueChanged += new System.EventHandler(this.LueProfessionCode_EditValueChanged);
            this.LueProfessionCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueProfessionCode_KeyDown);
            this.LueProfessionCode.Leave += new System.EventHandler(this.LueProfessionCode_Leave);
            // 
            // DteFamBirthDt
            // 
            this.DteFamBirthDt.EditValue = null;
            this.DteFamBirthDt.EnterMoveNextControl = true;
            this.DteFamBirthDt.Location = new System.Drawing.Point(374, 44);
            this.DteFamBirthDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteFamBirthDt.Name = "DteFamBirthDt";
            this.DteFamBirthDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteFamBirthDt.Properties.Appearance.Options.UseFont = true;
            this.DteFamBirthDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteFamBirthDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteFamBirthDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteFamBirthDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteFamBirthDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteFamBirthDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteFamBirthDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteFamBirthDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteFamBirthDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteFamBirthDt.Size = new System.Drawing.Size(104, 20);
            this.DteFamBirthDt.TabIndex = 25;
            this.DteFamBirthDt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DteFamBirthDt_KeyDown);
            this.DteFamBirthDt.Leave += new System.EventHandler(this.DteFamBirthDt_Leave);
            this.DteFamBirthDt.Validated += new System.EventHandler(this.DteFamBirthDt_Validated);
            // 
            // LueFamGender
            // 
            this.LueFamGender.EnterMoveNextControl = true;
            this.LueFamGender.Location = new System.Drawing.Point(244, 44);
            this.LueFamGender.Margin = new System.Windows.Forms.Padding(5);
            this.LueFamGender.Name = "LueFamGender";
            this.LueFamGender.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFamGender.Properties.Appearance.Options.UseFont = true;
            this.LueFamGender.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFamGender.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueFamGender.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFamGender.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueFamGender.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFamGender.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueFamGender.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFamGender.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueFamGender.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueFamGender.Properties.DropDownRows = 25;
            this.LueFamGender.Properties.NullText = "[Empty]";
            this.LueFamGender.Properties.PopupWidth = 500;
            this.LueFamGender.Size = new System.Drawing.Size(122, 20);
            this.LueFamGender.TabIndex = 24;
            this.LueFamGender.ToolTip = "F4 : Show/hide list";
            this.LueFamGender.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueFamGender.EditValueChanged += new System.EventHandler(this.LueFamGender_EditValueChanged);
            this.LueFamGender.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueFamGender_KeyDown);
            this.LueFamGender.Leave += new System.EventHandler(this.LueFamGender_Leave);
            // 
            // LueStatus
            // 
            this.LueStatus.EnterMoveNextControl = true;
            this.LueStatus.Location = new System.Drawing.Point(100, 44);
            this.LueStatus.Margin = new System.Windows.Forms.Padding(5);
            this.LueStatus.Name = "LueStatus";
            this.LueStatus.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStatus.Properties.Appearance.Options.UseFont = true;
            this.LueStatus.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStatus.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueStatus.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStatus.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueStatus.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStatus.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueStatus.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueStatus.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueStatus.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueStatus.Properties.DropDownRows = 25;
            this.LueStatus.Properties.NullText = "[Empty]";
            this.LueStatus.Properties.PopupWidth = 500;
            this.LueStatus.Size = new System.Drawing.Size(131, 20);
            this.LueStatus.TabIndex = 23;
            this.LueStatus.ToolTip = "F4 : Show/hide list";
            this.LueStatus.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueStatus.EditValueChanged += new System.EventHandler(this.LueStatus_EditValueChanged);
            this.LueStatus.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueStatus_KeyDown);
            this.LueStatus.Leave += new System.EventHandler(this.LueStatus_Leave);
            // 
            // Grd1
            // 
            this.Grd1.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd1.DefaultRow.Height = 20;
            this.Grd1.DefaultRow.Sortable = false;
            this.Grd1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd1.Header.Height = 21;
            this.Grd1.Location = new System.Drawing.Point(0, 23);
            this.Grd1.Name = "Grd1";
            this.Grd1.ReadOnly = true;
            this.Grd1.RowHeader.Visible = true;
            this.Grd1.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd1.SingleClickEdit = true;
            this.Grd1.Size = new System.Drawing.Size(914, 229);
            this.Grd1.TabIndex = 22;
            this.Grd1.TreeCol = null;
            this.Grd1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd1.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd1_RequestEdit);
            this.Grd1.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd1_AfterCommitEdit);
            this.Grd1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd1_KeyDown);
            // 
            // BtnFamily
            // 
            this.BtnFamily.BackColor = System.Drawing.Color.SteelBlue;
            this.BtnFamily.Dock = System.Windows.Forms.DockStyle.Top;
            this.BtnFamily.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnFamily.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnFamily.ForeColor = System.Drawing.Color.AliceBlue;
            this.BtnFamily.Location = new System.Drawing.Point(0, 0);
            this.BtnFamily.Name = "BtnFamily";
            this.BtnFamily.Size = new System.Drawing.Size(914, 23);
            this.BtnFamily.TabIndex = 21;
            this.BtnFamily.Text = "Family";
            this.BtnFamily.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.BtnFamily.UseVisualStyleBackColor = false;
            // 
            // Grd7
            // 
            this.Grd7.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd7.DefaultRow.Height = 20;
            this.Grd7.DefaultRow.Sortable = false;
            this.Grd7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd7.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd7.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd7.Header.Height = 21;
            this.Grd7.Location = new System.Drawing.Point(0, 23);
            this.Grd7.Name = "Grd7";
            this.Grd7.RowHeader.Visible = true;
            this.Grd7.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd7.SingleClickEdit = true;
            this.Grd7.Size = new System.Drawing.Size(914, 281);
            this.Grd7.TabIndex = 29;
            this.Grd7.TreeCol = null;
            this.Grd7.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd7.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd7_RequestEdit);
            this.Grd7.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd7_KeyDown);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.SteelBlue;
            this.button1.Dock = System.Windows.Forms.DockStyle.Top;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.AliceBlue;
            this.button1.Location = new System.Drawing.Point(0, 0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(914, 23);
            this.button1.TabIndex = 28;
            this.button1.Text = "Personal Reference";
            this.button1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.button1.UseVisualStyleBackColor = false;
            // 
            // TpgEmployeeWorkExp
            // 
            this.TpgEmployeeWorkExp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.TpgEmployeeWorkExp.Controls.Add(this.Grd2);
            this.TpgEmployeeWorkExp.Location = new System.Drawing.Point(4, 51);
            this.TpgEmployeeWorkExp.Name = "TpgEmployeeWorkExp";
            this.TpgEmployeeWorkExp.Size = new System.Drawing.Size(764, 128);
            this.TpgEmployeeWorkExp.TabIndex = 3;
            this.TpgEmployeeWorkExp.Text = "Work Exp.";
            this.TpgEmployeeWorkExp.UseVisualStyleBackColor = true;
            // 
            // Grd2
            // 
            this.Grd2.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd2.DefaultRow.Height = 20;
            this.Grd2.DefaultRow.Sortable = false;
            this.Grd2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd2.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd2.Header.Height = 21;
            this.Grd2.Location = new System.Drawing.Point(0, 0);
            this.Grd2.Name = "Grd2";
            this.Grd2.RowHeader.Visible = true;
            this.Grd2.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd2.SingleClickEdit = true;
            this.Grd2.Size = new System.Drawing.Size(764, 128);
            this.Grd2.TabIndex = 22;
            this.Grd2.TreeCol = null;
            this.Grd2.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd2.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd2_EllipsisButtonClick);
            this.Grd2.RequestCellToolTipText += new TenTec.Windows.iGridLib.iGRequestCellToolTipTextEventHandler(this.Grd2_RequestCellToolTipText);
            this.Grd2.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd2_RequestEdit);
            this.Grd2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd2_KeyDown);
            // 
            // TpgEmployeeEducation
            // 
            this.TpgEmployeeEducation.Controls.Add(this.LueFacCode);
            this.TpgEmployeeEducation.Controls.Add(this.LueField);
            this.TpgEmployeeEducation.Controls.Add(this.LueMajor);
            this.TpgEmployeeEducation.Controls.Add(this.LueLevel);
            this.TpgEmployeeEducation.Controls.Add(this.Grd3);
            this.TpgEmployeeEducation.Location = new System.Drawing.Point(4, 51);
            this.TpgEmployeeEducation.Name = "TpgEmployeeEducation";
            this.TpgEmployeeEducation.Size = new System.Drawing.Size(764, 128);
            this.TpgEmployeeEducation.TabIndex = 4;
            this.TpgEmployeeEducation.Text = "Education";
            this.TpgEmployeeEducation.UseVisualStyleBackColor = true;
            // 
            // LueFacCode
            // 
            this.LueFacCode.EnterMoveNextControl = true;
            this.LueFacCode.Location = new System.Drawing.Point(639, 21);
            this.LueFacCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueFacCode.Name = "LueFacCode";
            this.LueFacCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFacCode.Properties.Appearance.Options.UseFont = true;
            this.LueFacCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFacCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueFacCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFacCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueFacCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFacCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueFacCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFacCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueFacCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueFacCode.Properties.DropDownRows = 25;
            this.LueFacCode.Properties.NullText = "[Empty]";
            this.LueFacCode.Properties.PopupWidth = 500;
            this.LueFacCode.Size = new System.Drawing.Size(118, 20);
            this.LueFacCode.TabIndex = 26;
            this.LueFacCode.ToolTip = "F4 : Show/hide list";
            this.LueFacCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueFacCode.EditValueChanged += new System.EventHandler(this.LueFacCode_EditValueChanged);
            this.LueFacCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueFacCode_KeyDown);
            this.LueFacCode.Leave += new System.EventHandler(this.LueFacCode_Leave);
            this.LueFacCode.Validated += new System.EventHandler(this.LueFacCode_Validated);
            // 
            // LueField
            // 
            this.LueField.EnterMoveNextControl = true;
            this.LueField.Location = new System.Drawing.Point(449, 23);
            this.LueField.Margin = new System.Windows.Forms.Padding(5);
            this.LueField.Name = "LueField";
            this.LueField.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueField.Properties.Appearance.Options.UseFont = true;
            this.LueField.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueField.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueField.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueField.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueField.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueField.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueField.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueField.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueField.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueField.Properties.DropDownRows = 25;
            this.LueField.Properties.NullText = "[Empty]";
            this.LueField.Properties.PopupWidth = 500;
            this.LueField.Size = new System.Drawing.Size(118, 20);
            this.LueField.TabIndex = 25;
            this.LueField.ToolTip = "F4 : Show/hide list";
            this.LueField.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueField.EditValueChanged += new System.EventHandler(this.LueField_EditValueChanged);
            this.LueField.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueField_KeyDown);
            this.LueField.Leave += new System.EventHandler(this.LueField_Leave);
            this.LueField.Validated += new System.EventHandler(this.LueField_Validated);
            // 
            // LueMajor
            // 
            this.LueMajor.EnterMoveNextControl = true;
            this.LueMajor.Location = new System.Drawing.Point(231, 23);
            this.LueMajor.Margin = new System.Windows.Forms.Padding(5);
            this.LueMajor.Name = "LueMajor";
            this.LueMajor.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMajor.Properties.Appearance.Options.UseFont = true;
            this.LueMajor.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMajor.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueMajor.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMajor.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueMajor.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMajor.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueMajor.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueMajor.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueMajor.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueMajor.Properties.DropDownRows = 25;
            this.LueMajor.Properties.NullText = "[Empty]";
            this.LueMajor.Properties.PopupWidth = 500;
            this.LueMajor.Size = new System.Drawing.Size(118, 20);
            this.LueMajor.TabIndex = 24;
            this.LueMajor.ToolTip = "F4 : Show/hide list";
            this.LueMajor.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueMajor.EditValueChanged += new System.EventHandler(this.LueMajor_EditValueChanged);
            this.LueMajor.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueMajor_KeyDown);
            this.LueMajor.Leave += new System.EventHandler(this.LueMajor_Leave);
            this.LueMajor.Validated += new System.EventHandler(this.LueMajor_Validated);
            // 
            // LueLevel
            // 
            this.LueLevel.EnterMoveNextControl = true;
            this.LueLevel.Location = new System.Drawing.Point(72, 22);
            this.LueLevel.Margin = new System.Windows.Forms.Padding(5);
            this.LueLevel.Name = "LueLevel";
            this.LueLevel.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel.Properties.Appearance.Options.UseFont = true;
            this.LueLevel.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueLevel.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueLevel.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueLevel.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevel.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueLevel.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueLevel.Properties.DropDownRows = 25;
            this.LueLevel.Properties.NullText = "[Empty]";
            this.LueLevel.Properties.PopupWidth = 500;
            this.LueLevel.Size = new System.Drawing.Size(118, 20);
            this.LueLevel.TabIndex = 23;
            this.LueLevel.ToolTip = "F4 : Show/hide list";
            this.LueLevel.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueLevel.EditValueChanged += new System.EventHandler(this.LueLevel_EditValueChanged);
            this.LueLevel.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueLevel_KeyDown);
            this.LueLevel.Leave += new System.EventHandler(this.LueLevel_Leave);
            this.LueLevel.Validated += new System.EventHandler(this.LueLevel_Validated);
            // 
            // Grd3
            // 
            this.Grd3.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd3.DefaultRow.Height = 20;
            this.Grd3.DefaultRow.Sortable = false;
            this.Grd3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd3.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd3.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd3.Header.Height = 21;
            this.Grd3.Location = new System.Drawing.Point(0, 0);
            this.Grd3.Name = "Grd3";
            this.Grd3.RowHeader.Visible = true;
            this.Grd3.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd3.SingleClickEdit = true;
            this.Grd3.Size = new System.Drawing.Size(764, 128);
            this.Grd3.TabIndex = 22;
            this.Grd3.TreeCol = null;
            this.Grd3.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd3.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd3_EllipsisButtonClick);
            this.Grd3.RequestCellToolTipText += new TenTec.Windows.iGridLib.iGRequestCellToolTipTextEventHandler(this.Grd3_RequestCellToolTipText);
            this.Grd3.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd3_RequestEdit);
            this.Grd3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd3_KeyDown);
            // 
            // TpgEmployeeTraining
            // 
            this.TpgEmployeeTraining.Controls.Add(this.LueTrainingCode);
            this.TpgEmployeeTraining.Controls.Add(this.Grd8);
            this.TpgEmployeeTraining.Location = new System.Drawing.Point(4, 51);
            this.TpgEmployeeTraining.Name = "TpgEmployeeTraining";
            this.TpgEmployeeTraining.Size = new System.Drawing.Size(764, 128);
            this.TpgEmployeeTraining.TabIndex = 10;
            this.TpgEmployeeTraining.Text = "Training";
            this.TpgEmployeeTraining.UseVisualStyleBackColor = true;
            // 
            // LueTrainingCode
            // 
            this.LueTrainingCode.EnterMoveNextControl = true;
            this.LueTrainingCode.Location = new System.Drawing.Point(141, 22);
            this.LueTrainingCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueTrainingCode.Name = "LueTrainingCode";
            this.LueTrainingCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTrainingCode.Properties.Appearance.Options.UseFont = true;
            this.LueTrainingCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTrainingCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueTrainingCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTrainingCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueTrainingCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTrainingCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueTrainingCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTrainingCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueTrainingCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueTrainingCode.Properties.DropDownRows = 25;
            this.LueTrainingCode.Properties.NullText = "[Empty]";
            this.LueTrainingCode.Properties.PopupWidth = 500;
            this.LueTrainingCode.Size = new System.Drawing.Size(131, 20);
            this.LueTrainingCode.TabIndex = 23;
            this.LueTrainingCode.ToolTip = "F4 : Show/hide list";
            this.LueTrainingCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueTrainingCode.EditValueChanged += new System.EventHandler(this.LueTrainingCode_EditValueChanged);
            this.LueTrainingCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueTrainingCode_KeyDown);
            this.LueTrainingCode.Leave += new System.EventHandler(this.LueTrainingCode_Leave);
            // 
            // Grd8
            // 
            this.Grd8.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd8.DefaultRow.Height = 20;
            this.Grd8.DefaultRow.Sortable = false;
            this.Grd8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd8.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd8.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd8.Header.Height = 21;
            this.Grd8.Location = new System.Drawing.Point(0, 0);
            this.Grd8.Name = "Grd8";
            this.Grd8.RowHeader.Visible = true;
            this.Grd8.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd8.SingleClickEdit = true;
            this.Grd8.Size = new System.Drawing.Size(764, 128);
            this.Grd8.TabIndex = 22;
            this.Grd8.TreeCol = null;
            this.Grd8.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd8.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd8_RequestEdit);
            this.Grd8.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd8_KeyDown);
            // 
            // TpgEmployeeTraining2
            // 
            this.TpgEmployeeTraining2.Controls.Add(this.splitContainer3);
            this.TpgEmployeeTraining2.Location = new System.Drawing.Point(4, 51);
            this.TpgEmployeeTraining2.Name = "TpgEmployeeTraining2";
            this.TpgEmployeeTraining2.Padding = new System.Windows.Forms.Padding(3);
            this.TpgEmployeeTraining2.Size = new System.Drawing.Size(764, 128);
            this.TpgEmployeeTraining2.TabIndex = 11;
            this.TpgEmployeeTraining2.Text = "Training";
            this.TpgEmployeeTraining2.UseVisualStyleBackColor = true;
            // 
            // splitContainer3
            // 
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer3.Location = new System.Drawing.Point(3, 3);
            this.splitContainer3.Name = "splitContainer3";
            this.splitContainer3.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.Controls.Add(this.Grd9);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.Controls.Add(this.LueTrainingCode2);
            this.splitContainer3.Panel2.Controls.Add(this.Grd10);
            this.splitContainer3.Size = new System.Drawing.Size(758, 122);
            this.splitContainer3.SplitterDistance = 55;
            this.splitContainer3.TabIndex = 0;
            // 
            // Grd9
            // 
            this.Grd9.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd9.DefaultRow.Height = 20;
            this.Grd9.DefaultRow.Sortable = false;
            this.Grd9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd9.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd9.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd9.Header.Height = 21;
            this.Grd9.Location = new System.Drawing.Point(0, 0);
            this.Grd9.Name = "Grd9";
            this.Grd9.RowHeader.Visible = true;
            this.Grd9.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd9.SingleClickEdit = true;
            this.Grd9.Size = new System.Drawing.Size(758, 55);
            this.Grd9.TabIndex = 23;
            this.Grd9.TreeCol = null;
            this.Grd9.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // LueTrainingCode2
            // 
            this.LueTrainingCode2.EnterMoveNextControl = true;
            this.LueTrainingCode2.Location = new System.Drawing.Point(91, 24);
            this.LueTrainingCode2.Margin = new System.Windows.Forms.Padding(5);
            this.LueTrainingCode2.Name = "LueTrainingCode2";
            this.LueTrainingCode2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTrainingCode2.Properties.Appearance.Options.UseFont = true;
            this.LueTrainingCode2.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTrainingCode2.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueTrainingCode2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTrainingCode2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueTrainingCode2.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTrainingCode2.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueTrainingCode2.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTrainingCode2.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueTrainingCode2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueTrainingCode2.Properties.DropDownRows = 12;
            this.LueTrainingCode2.Properties.NullText = "[Empty]";
            this.LueTrainingCode2.Properties.PopupWidth = 500;
            this.LueTrainingCode2.Size = new System.Drawing.Size(131, 20);
            this.LueTrainingCode2.TabIndex = 24;
            this.LueTrainingCode2.ToolTip = "F4 : Show/hide list";
            this.LueTrainingCode2.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueTrainingCode2.EditValueChanged += new System.EventHandler(this.LueTrainingCode2_EditValueChanged);
            this.LueTrainingCode2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueTrainingCode2_KeyDown);
            this.LueTrainingCode2.Leave += new System.EventHandler(this.LueTrainingCode2_Leave);
            // 
            // Grd10
            // 
            this.Grd10.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd10.DefaultRow.Height = 20;
            this.Grd10.DefaultRow.Sortable = false;
            this.Grd10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd10.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd10.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd10.Header.Height = 21;
            this.Grd10.Location = new System.Drawing.Point(0, 0);
            this.Grd10.Name = "Grd10";
            this.Grd10.RowHeader.Visible = true;
            this.Grd10.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd10.SingleClickEdit = true;
            this.Grd10.Size = new System.Drawing.Size(758, 63);
            this.Grd10.TabIndex = 23;
            this.Grd10.TreeCol = null;
            this.Grd10.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd10.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd10_RequestEdit);
            this.Grd10.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd10_KeyDown);
            // 
            // TpgEmployeeTraining3
            // 
            this.TpgEmployeeTraining3.Controls.Add(this.LueTrainingCategory);
            this.TpgEmployeeTraining3.Controls.Add(this.LueTrainingEducationCenter);
            this.TpgEmployeeTraining3.Controls.Add(this.LueTrainingSpecialization);
            this.TpgEmployeeTraining3.Controls.Add(this.LueTrainingType);
            this.TpgEmployeeTraining3.Controls.Add(this.LueTrainingCode3);
            this.TpgEmployeeTraining3.Controls.Add(this.Grd16);
            this.TpgEmployeeTraining3.Location = new System.Drawing.Point(4, 51);
            this.TpgEmployeeTraining3.Name = "TpgEmployeeTraining3";
            this.TpgEmployeeTraining3.Size = new System.Drawing.Size(764, 128);
            this.TpgEmployeeTraining3.TabIndex = 17;
            this.TpgEmployeeTraining3.Text = "Training";
            this.TpgEmployeeTraining3.UseVisualStyleBackColor = true;
            // 
            // LueTrainingCategory
            // 
            this.LueTrainingCategory.EnterMoveNextControl = true;
            this.LueTrainingCategory.Location = new System.Drawing.Point(579, 22);
            this.LueTrainingCategory.Margin = new System.Windows.Forms.Padding(5);
            this.LueTrainingCategory.Name = "LueTrainingCategory";
            this.LueTrainingCategory.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTrainingCategory.Properties.Appearance.Options.UseFont = true;
            this.LueTrainingCategory.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTrainingCategory.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueTrainingCategory.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTrainingCategory.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueTrainingCategory.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTrainingCategory.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueTrainingCategory.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTrainingCategory.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueTrainingCategory.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueTrainingCategory.Properties.DropDownRows = 25;
            this.LueTrainingCategory.Properties.NullText = "[Empty]";
            this.LueTrainingCategory.Properties.PopupWidth = 500;
            this.LueTrainingCategory.Size = new System.Drawing.Size(148, 20);
            this.LueTrainingCategory.TabIndex = 33;
            this.LueTrainingCategory.ToolTip = "F4 : Show/hide list";
            this.LueTrainingCategory.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueTrainingCategory.EditValueChanged += new System.EventHandler(this.LueTrainingCategory_EditValueChanged);
            this.LueTrainingCategory.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueTrainingCategory_KeyDown);
            this.LueTrainingCategory.Leave += new System.EventHandler(this.LueTrainingCategory_Leave);
            // 
            // LueTrainingEducationCenter
            // 
            this.LueTrainingEducationCenter.EnterMoveNextControl = true;
            this.LueTrainingEducationCenter.Location = new System.Drawing.Point(739, 22);
            this.LueTrainingEducationCenter.Margin = new System.Windows.Forms.Padding(5);
            this.LueTrainingEducationCenter.Name = "LueTrainingEducationCenter";
            this.LueTrainingEducationCenter.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTrainingEducationCenter.Properties.Appearance.Options.UseFont = true;
            this.LueTrainingEducationCenter.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTrainingEducationCenter.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueTrainingEducationCenter.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTrainingEducationCenter.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueTrainingEducationCenter.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTrainingEducationCenter.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueTrainingEducationCenter.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTrainingEducationCenter.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueTrainingEducationCenter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueTrainingEducationCenter.Properties.DropDownRows = 25;
            this.LueTrainingEducationCenter.Properties.NullText = "[Empty]";
            this.LueTrainingEducationCenter.Properties.PopupWidth = 500;
            this.LueTrainingEducationCenter.Size = new System.Drawing.Size(148, 20);
            this.LueTrainingEducationCenter.TabIndex = 34;
            this.LueTrainingEducationCenter.ToolTip = "F4 : Show/hide list";
            this.LueTrainingEducationCenter.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueTrainingEducationCenter.EditValueChanged += new System.EventHandler(this.LueTrainingEducationCenter_EditValueChanged);
            this.LueTrainingEducationCenter.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueTrainingEducationCenter_KeyDown);
            this.LueTrainingEducationCenter.Leave += new System.EventHandler(this.LueTrainingEducationCenter_Leave);
            // 
            // LueTrainingSpecialization
            // 
            this.LueTrainingSpecialization.EnterMoveNextControl = true;
            this.LueTrainingSpecialization.Location = new System.Drawing.Point(417, 24);
            this.LueTrainingSpecialization.Margin = new System.Windows.Forms.Padding(5);
            this.LueTrainingSpecialization.Name = "LueTrainingSpecialization";
            this.LueTrainingSpecialization.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTrainingSpecialization.Properties.Appearance.Options.UseFont = true;
            this.LueTrainingSpecialization.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTrainingSpecialization.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueTrainingSpecialization.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTrainingSpecialization.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueTrainingSpecialization.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTrainingSpecialization.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueTrainingSpecialization.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTrainingSpecialization.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueTrainingSpecialization.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueTrainingSpecialization.Properties.DropDownRows = 25;
            this.LueTrainingSpecialization.Properties.NullText = "[Empty]";
            this.LueTrainingSpecialization.Properties.PopupWidth = 500;
            this.LueTrainingSpecialization.Size = new System.Drawing.Size(148, 20);
            this.LueTrainingSpecialization.TabIndex = 32;
            this.LueTrainingSpecialization.ToolTip = "F4 : Show/hide list";
            this.LueTrainingSpecialization.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueTrainingSpecialization.EditValueChanged += new System.EventHandler(this.LueTrainingSpecialization_EditValueChanged);
            this.LueTrainingSpecialization.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueTrainingSpecialization_KeyDown);
            this.LueTrainingSpecialization.Leave += new System.EventHandler(this.LueTrainingSpecialization_Leave);
            // 
            // LueTrainingType
            // 
            this.LueTrainingType.EnterMoveNextControl = true;
            this.LueTrainingType.Location = new System.Drawing.Point(254, 24);
            this.LueTrainingType.Margin = new System.Windows.Forms.Padding(5);
            this.LueTrainingType.Name = "LueTrainingType";
            this.LueTrainingType.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTrainingType.Properties.Appearance.Options.UseFont = true;
            this.LueTrainingType.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTrainingType.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueTrainingType.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTrainingType.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueTrainingType.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTrainingType.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueTrainingType.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTrainingType.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueTrainingType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueTrainingType.Properties.DropDownRows = 25;
            this.LueTrainingType.Properties.NullText = "[Empty]";
            this.LueTrainingType.Properties.PopupWidth = 500;
            this.LueTrainingType.Size = new System.Drawing.Size(148, 20);
            this.LueTrainingType.TabIndex = 31;
            this.LueTrainingType.ToolTip = "F4 : Show/hide list";
            this.LueTrainingType.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueTrainingType.EditValueChanged += new System.EventHandler(this.LueTrainingType_EditValueChanged);
            this.LueTrainingType.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueTrainingType_KeyDown);
            this.LueTrainingType.Leave += new System.EventHandler(this.LueTrainingType_Leave);
            // 
            // LueTrainingCode3
            // 
            this.LueTrainingCode3.EnterMoveNextControl = true;
            this.LueTrainingCode3.Location = new System.Drawing.Point(76, 25);
            this.LueTrainingCode3.Margin = new System.Windows.Forms.Padding(5);
            this.LueTrainingCode3.Name = "LueTrainingCode3";
            this.LueTrainingCode3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTrainingCode3.Properties.Appearance.Options.UseFont = true;
            this.LueTrainingCode3.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTrainingCode3.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueTrainingCode3.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTrainingCode3.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueTrainingCode3.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTrainingCode3.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueTrainingCode3.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueTrainingCode3.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueTrainingCode3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueTrainingCode3.Properties.DropDownRows = 25;
            this.LueTrainingCode3.Properties.NullText = "[Empty]";
            this.LueTrainingCode3.Properties.PopupWidth = 500;
            this.LueTrainingCode3.Size = new System.Drawing.Size(148, 20);
            this.LueTrainingCode3.TabIndex = 30;
            this.LueTrainingCode3.ToolTip = "F4 : Show/hide list";
            this.LueTrainingCode3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueTrainingCode3.EditValueChanged += new System.EventHandler(this.LueTrainingCode3_EditValueChanged);
            this.LueTrainingCode3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueTrainingCode3_KeyDown);
            this.LueTrainingCode3.Leave += new System.EventHandler(this.LueTrainingCode3_Leave);
            // 
            // Grd16
            // 
            this.Grd16.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd16.DefaultRow.Height = 20;
            this.Grd16.DefaultRow.Sortable = false;
            this.Grd16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd16.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd16.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd16.Header.Height = 21;
            this.Grd16.Location = new System.Drawing.Point(0, 0);
            this.Grd16.Name = "Grd16";
            this.Grd16.RowHeader.Visible = true;
            this.Grd16.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd16.SingleClickEdit = true;
            this.Grd16.Size = new System.Drawing.Size(764, 128);
            this.Grd16.TabIndex = 29;
            this.Grd16.TreeCol = null;
            this.Grd16.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd16.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd16_EllipsisButtonClick);
            this.Grd16.RequestCellToolTipText += new TenTec.Windows.iGridLib.iGRequestCellToolTipTextEventHandler(this.Grd16_RequestCellToolTipText);
            this.Grd16.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd16_RequestEdit);
            this.Grd16.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd16_AfterCommitEdit);
            this.Grd16.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd16_KeyDown);
            // 
            // TpgEmployeeCompetence
            // 
            this.TpgEmployeeCompetence.Controls.Add(this.LueCompetenceCode);
            this.TpgEmployeeCompetence.Controls.Add(this.Grd4);
            this.TpgEmployeeCompetence.Location = new System.Drawing.Point(4, 51);
            this.TpgEmployeeCompetence.Name = "TpgEmployeeCompetence";
            this.TpgEmployeeCompetence.Size = new System.Drawing.Size(764, 128);
            this.TpgEmployeeCompetence.TabIndex = 5;
            this.TpgEmployeeCompetence.Text = "Competence";
            this.TpgEmployeeCompetence.UseVisualStyleBackColor = true;
            // 
            // LueCompetenceCode
            // 
            this.LueCompetenceCode.EnterMoveNextControl = true;
            this.LueCompetenceCode.Location = new System.Drawing.Point(99, 21);
            this.LueCompetenceCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueCompetenceCode.Name = "LueCompetenceCode";
            this.LueCompetenceCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCompetenceCode.Properties.Appearance.Options.UseFont = true;
            this.LueCompetenceCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCompetenceCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueCompetenceCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCompetenceCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueCompetenceCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCompetenceCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueCompetenceCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueCompetenceCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueCompetenceCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueCompetenceCode.Properties.DropDownRows = 30;
            this.LueCompetenceCode.Properties.NullText = "[Empty]";
            this.LueCompetenceCode.Properties.PopupWidth = 300;
            this.LueCompetenceCode.Size = new System.Drawing.Size(300, 20);
            this.LueCompetenceCode.TabIndex = 23;
            this.LueCompetenceCode.ToolTip = "F4 : Show/hide list";
            this.LueCompetenceCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueCompetenceCode.EditValueChanged += new System.EventHandler(this.LueCompetenceCode_EditValueChanged);
            this.LueCompetenceCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueCompetenceCode_KeyDown);
            this.LueCompetenceCode.Leave += new System.EventHandler(this.LueCompetenceCode_Leave);
            // 
            // Grd4
            // 
            this.Grd4.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd4.DefaultRow.Height = 20;
            this.Grd4.DefaultRow.Sortable = false;
            this.Grd4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd4.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd4.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd4.Header.Height = 21;
            this.Grd4.Location = new System.Drawing.Point(0, 0);
            this.Grd4.Name = "Grd4";
            this.Grd4.RowHeader.Visible = true;
            this.Grd4.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd4.SingleClickEdit = true;
            this.Grd4.Size = new System.Drawing.Size(764, 128);
            this.Grd4.TabIndex = 22;
            this.Grd4.TreeCol = null;
            this.Grd4.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd4.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd4_EllipsisButtonClick);
            this.Grd4.RequestCellToolTipText += new TenTec.Windows.iGridLib.iGRequestCellToolTipTextEventHandler(this.Grd4_RequestCellToolTipText);
            this.Grd4.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd4_RequestEdit);
            this.Grd4.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd4_AfterCommitEdit);
            this.Grd4.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd4_KeyDown);
            // 
            // TpgPositionHistory
            // 
            this.TpgPositionHistory.Controls.Add(this.Grd15);
            this.TpgPositionHistory.Controls.Add(this.panel9);
            this.TpgPositionHistory.Location = new System.Drawing.Point(4, 51);
            this.TpgPositionHistory.Name = "TpgPositionHistory";
            this.TpgPositionHistory.Size = new System.Drawing.Size(764, 128);
            this.TpgPositionHistory.TabIndex = 16;
            this.TpgPositionHistory.Text = "Position History";
            this.TpgPositionHistory.UseVisualStyleBackColor = true;
            // 
            // Grd15
            // 
            this.Grd15.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd15.DefaultRow.Height = 20;
            this.Grd15.DefaultRow.Sortable = false;
            this.Grd15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd15.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd15.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd15.Header.Height = 21;
            this.Grd15.Location = new System.Drawing.Point(0, 29);
            this.Grd15.Name = "Grd15";
            this.Grd15.ReadOnly = true;
            this.Grd15.RowHeader.Visible = true;
            this.Grd15.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd15.SingleClickEdit = true;
            this.Grd15.Size = new System.Drawing.Size(764, 99);
            this.Grd15.TabIndex = 25;
            this.Grd15.TreeCol = null;
            this.Grd15.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd15.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd15_EllipsisButtonClick);
            this.Grd15.RequestCellToolTipText += new TenTec.Windows.iGridLib.iGRequestCellToolTipTextEventHandler(this.Grd15_RequestCellToolTipText);
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel9.Controls.Add(this.LblPositionHistoryRefreshData);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel9.Location = new System.Drawing.Point(0, 0);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(764, 29);
            this.panel9.TabIndex = 23;
            // 
            // LblPositionHistoryRefreshData
            // 
            this.LblPositionHistoryRefreshData.AutoSize = true;
            this.LblPositionHistoryRefreshData.Font = new System.Drawing.Font("Tahoma", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblPositionHistoryRefreshData.ForeColor = System.Drawing.Color.Green;
            this.LblPositionHistoryRefreshData.Location = new System.Drawing.Point(11, 8);
            this.LblPositionHistoryRefreshData.Name = "LblPositionHistoryRefreshData";
            this.LblPositionHistoryRefreshData.Size = new System.Drawing.Size(87, 14);
            this.LblPositionHistoryRefreshData.TabIndex = 24;
            this.LblPositionHistoryRefreshData.Tag = "Refresh Data";
            this.LblPositionHistoryRefreshData.Text = "Refresh Data";
            this.LblPositionHistoryRefreshData.Click += new System.EventHandler(this.LblPositionHistoryRefreshData_Click);
            // 
            // TpgGradeHistory
            // 
            this.TpgGradeHistory.Controls.Add(this.Grd17);
            this.TpgGradeHistory.Controls.Add(this.panel10);
            this.TpgGradeHistory.Location = new System.Drawing.Point(4, 51);
            this.TpgGradeHistory.Name = "TpgGradeHistory";
            this.TpgGradeHistory.Size = new System.Drawing.Size(764, 128);
            this.TpgGradeHistory.TabIndex = 18;
            this.TpgGradeHistory.Text = "Grade History";
            this.TpgGradeHistory.UseVisualStyleBackColor = true;
            // 
            // Grd17
            // 
            this.Grd17.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd17.DefaultRow.Height = 20;
            this.Grd17.DefaultRow.Sortable = false;
            this.Grd17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd17.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd17.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd17.Header.Height = 21;
            this.Grd17.Location = new System.Drawing.Point(0, 29);
            this.Grd17.Name = "Grd17";
            this.Grd17.RowHeader.Visible = true;
            this.Grd17.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd17.SingleClickEdit = true;
            this.Grd17.Size = new System.Drawing.Size(764, 99);
            this.Grd17.TabIndex = 24;
            this.Grd17.TreeCol = null;
            this.Grd17.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd17.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd17_EllipsisButtonClick);
            this.Grd17.RequestCellToolTipText += new TenTec.Windows.iGridLib.iGRequestCellToolTipTextEventHandler(this.Grd17_RequestCellToolTipText);
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel10.Controls.Add(this.LblGradeHistoryRefreshData);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel10.Location = new System.Drawing.Point(0, 0);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(764, 29);
            this.panel10.TabIndex = 25;
            // 
            // LblGradeHistoryRefreshData
            // 
            this.LblGradeHistoryRefreshData.AutoSize = true;
            this.LblGradeHistoryRefreshData.Font = new System.Drawing.Font("Tahoma", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblGradeHistoryRefreshData.ForeColor = System.Drawing.Color.Green;
            this.LblGradeHistoryRefreshData.Location = new System.Drawing.Point(11, 8);
            this.LblGradeHistoryRefreshData.Name = "LblGradeHistoryRefreshData";
            this.LblGradeHistoryRefreshData.Size = new System.Drawing.Size(87, 14);
            this.LblGradeHistoryRefreshData.TabIndex = 23;
            this.LblGradeHistoryRefreshData.Tag = "Refresh Data";
            this.LblGradeHistoryRefreshData.Text = "Refresh Data";
            this.LblGradeHistoryRefreshData.Click += new System.EventHandler(this.LblGradeHistoryRefreshData_Click);
            // 
            // TpgWarningLetter
            // 
            this.TpgWarningLetter.Controls.Add(this.Grd13);
            this.TpgWarningLetter.Controls.Add(this.panel8);
            this.TpgWarningLetter.Location = new System.Drawing.Point(4, 51);
            this.TpgWarningLetter.Name = "TpgWarningLetter";
            this.TpgWarningLetter.Size = new System.Drawing.Size(764, 128);
            this.TpgWarningLetter.TabIndex = 14;
            this.TpgWarningLetter.Text = "Warning Letter";
            this.TpgWarningLetter.UseVisualStyleBackColor = true;
            // 
            // Grd13
            // 
            this.Grd13.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd13.DefaultRow.Height = 20;
            this.Grd13.DefaultRow.Sortable = false;
            this.Grd13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd13.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd13.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd13.Header.Height = 21;
            this.Grd13.Location = new System.Drawing.Point(0, 29);
            this.Grd13.Name = "Grd13";
            this.Grd13.RowHeader.Visible = true;
            this.Grd13.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd13.SingleClickEdit = true;
            this.Grd13.Size = new System.Drawing.Size(764, 99);
            this.Grd13.TabIndex = 23;
            this.Grd13.TreeCol = null;
            this.Grd13.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd13.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd13_EllipsisButtonClick);
            this.Grd13.RequestCellToolTipText += new TenTec.Windows.iGridLib.iGRequestCellToolTipTextEventHandler(this.Grd13_RequestCellToolTipText);
            this.Grd13.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd13_RequestEdit);
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel8.Controls.Add(this.LblWarningLetterRefreshData);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel8.Location = new System.Drawing.Point(0, 0);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(764, 29);
            this.panel8.TabIndex = 1;
            // 
            // LblWarningLetterRefreshData
            // 
            this.LblWarningLetterRefreshData.AutoSize = true;
            this.LblWarningLetterRefreshData.Font = new System.Drawing.Font("Tahoma", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblWarningLetterRefreshData.ForeColor = System.Drawing.Color.Green;
            this.LblWarningLetterRefreshData.Location = new System.Drawing.Point(11, 8);
            this.LblWarningLetterRefreshData.Name = "LblWarningLetterRefreshData";
            this.LblWarningLetterRefreshData.Size = new System.Drawing.Size(87, 14);
            this.LblWarningLetterRefreshData.TabIndex = 20;
            this.LblWarningLetterRefreshData.Tag = "Refresh Data";
            this.LblWarningLetterRefreshData.Text = "Refresh Data";
            this.LblWarningLetterRefreshData.Click += new System.EventHandler(this.LblWarningLetterRefreshData_Click);
            // 
            // TpgEmployeeSS
            // 
            this.TpgEmployeeSS.Controls.Add(this.splitContainer1);
            this.TpgEmployeeSS.Location = new System.Drawing.Point(4, 51);
            this.TpgEmployeeSS.Name = "TpgEmployeeSS";
            this.TpgEmployeeSS.Size = new System.Drawing.Size(764, 128);
            this.TpgEmployeeSS.TabIndex = 8;
            this.TpgEmployeeSS.Text = "Social Security";
            this.TpgEmployeeSS.UseVisualStyleBackColor = true;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.Grd5);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.Grd6);
            this.splitContainer1.Size = new System.Drawing.Size(764, 128);
            this.splitContainer1.SplitterDistance = 58;
            this.splitContainer1.TabIndex = 0;
            // 
            // Grd5
            // 
            this.Grd5.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd5.DefaultRow.Height = 20;
            this.Grd5.DefaultRow.Sortable = false;
            this.Grd5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd5.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd5.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd5.Header.Height = 21;
            this.Grd5.Location = new System.Drawing.Point(0, 0);
            this.Grd5.Name = "Grd5";
            this.Grd5.RowHeader.Visible = true;
            this.Grd5.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd5.SingleClickEdit = true;
            this.Grd5.Size = new System.Drawing.Size(764, 58);
            this.Grd5.TabIndex = 22;
            this.Grd5.TreeCol = null;
            this.Grd5.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // Grd6
            // 
            this.Grd6.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd6.DefaultRow.Height = 20;
            this.Grd6.DefaultRow.Sortable = false;
            this.Grd6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd6.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd6.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd6.Header.Height = 21;
            this.Grd6.Location = new System.Drawing.Point(0, 0);
            this.Grd6.Name = "Grd6";
            this.Grd6.RowHeader.Visible = true;
            this.Grd6.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd6.SingleClickEdit = true;
            this.Grd6.Size = new System.Drawing.Size(764, 66);
            this.Grd6.TabIndex = 23;
            this.Grd6.TreeCol = null;
            this.Grd6.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // TpgAward
            // 
            this.TpgAward.Controls.Add(this.LueAwardCt);
            this.TpgAward.Controls.Add(this.Grd14);
            this.TpgAward.Location = new System.Drawing.Point(4, 51);
            this.TpgAward.Name = "TpgAward";
            this.TpgAward.Size = new System.Drawing.Size(764, 128);
            this.TpgAward.TabIndex = 15;
            this.TpgAward.Text = "Award";
            this.TpgAward.UseVisualStyleBackColor = true;
            // 
            // LueAwardCt
            // 
            this.LueAwardCt.EnterMoveNextControl = true;
            this.LueAwardCt.Location = new System.Drawing.Point(141, 23);
            this.LueAwardCt.Margin = new System.Windows.Forms.Padding(5);
            this.LueAwardCt.Name = "LueAwardCt";
            this.LueAwardCt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAwardCt.Properties.Appearance.Options.UseFont = true;
            this.LueAwardCt.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAwardCt.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueAwardCt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAwardCt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueAwardCt.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAwardCt.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueAwardCt.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueAwardCt.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueAwardCt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueAwardCt.Properties.DropDownRows = 25;
            this.LueAwardCt.Properties.NullText = "[Empty]";
            this.LueAwardCt.Properties.PopupWidth = 500;
            this.LueAwardCt.Size = new System.Drawing.Size(215, 20);
            this.LueAwardCt.TabIndex = 26;
            this.LueAwardCt.ToolTip = "F4 : Show/hide list";
            this.LueAwardCt.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueAwardCt.EditValueChanged += new System.EventHandler(this.LueAwardCt_EditValueChanged);
            this.LueAwardCt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueAwardCt_KeyDown);
            this.LueAwardCt.Leave += new System.EventHandler(this.LueAwardCt_Leave);
            // 
            // Grd14
            // 
            this.Grd14.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd14.DefaultRow.Height = 20;
            this.Grd14.DefaultRow.Sortable = false;
            this.Grd14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd14.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd14.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd14.Header.Height = 21;
            this.Grd14.Location = new System.Drawing.Point(0, 0);
            this.Grd14.Name = "Grd14";
            this.Grd14.RowHeader.Visible = true;
            this.Grd14.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd14.SingleClickEdit = true;
            this.Grd14.Size = new System.Drawing.Size(764, 128);
            this.Grd14.TabIndex = 25;
            this.Grd14.TreeCol = null;
            this.Grd14.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd14.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd14_EllipsisButtonClick);
            this.Grd14.RequestCellToolTipText += new TenTec.Windows.iGridLib.iGRequestCellToolTipTextEventHandler(this.Grd14_RequestCellToolTipText);
            this.Grd14.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd14_RequestEdit);
            this.Grd14.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd14_AfterCommitEdit);
            this.Grd14.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd14_KeyDown);
            // 
            // TpgVaccine
            // 
            this.TpgVaccine.Controls.Add(this.DteVaccineDt);
            this.TpgVaccine.Controls.Add(this.Grd12);
            this.TpgVaccine.Location = new System.Drawing.Point(4, 51);
            this.TpgVaccine.Name = "TpgVaccine";
            this.TpgVaccine.Padding = new System.Windows.Forms.Padding(3);
            this.TpgVaccine.Size = new System.Drawing.Size(764, 128);
            this.TpgVaccine.TabIndex = 13;
            this.TpgVaccine.Text = "Vaccine";
            this.TpgVaccine.UseVisualStyleBackColor = true;
            // 
            // DteVaccineDt
            // 
            this.DteVaccineDt.EditValue = null;
            this.DteVaccineDt.EnterMoveNextControl = true;
            this.DteVaccineDt.Location = new System.Drawing.Point(380, 25);
            this.DteVaccineDt.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DteVaccineDt.Name = "DteVaccineDt";
            this.DteVaccineDt.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteVaccineDt.Properties.Appearance.Options.UseFont = true;
            this.DteVaccineDt.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DteVaccineDt.Properties.AppearanceDropDown.Options.UseFont = true;
            this.DteVaccineDt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DteVaccineDt.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.DteVaccineDt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteVaccineDt.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.DteVaccineDt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DteVaccineDt.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.DteVaccineDt.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DteVaccineDt.Size = new System.Drawing.Size(150, 20);
            this.DteVaccineDt.TabIndex = 26;
            this.DteVaccineDt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DteVaccineDt_KeyDown);
            this.DteVaccineDt.Leave += new System.EventHandler(this.DteVaccineDt_Leave);
            this.DteVaccineDt.Validated += new System.EventHandler(this.DteVaccineDt_Validated);
            // 
            // Grd12
            // 
            this.Grd12.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd12.DefaultRow.Height = 20;
            this.Grd12.DefaultRow.Sortable = false;
            this.Grd12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd12.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd12.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd12.Header.Height = 21;
            this.Grd12.Location = new System.Drawing.Point(3, 3);
            this.Grd12.Name = "Grd12";
            this.Grd12.ReadOnly = true;
            this.Grd12.RowHeader.Visible = true;
            this.Grd12.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd12.SingleClickEdit = true;
            this.Grd12.Size = new System.Drawing.Size(758, 122);
            this.Grd12.TabIndex = 23;
            this.Grd12.TreeCol = null;
            this.Grd12.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd12.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd12_EllipsisButtonClick);
            this.Grd12.RequestCellToolTipText += new TenTec.Windows.iGridLib.iGRequestCellToolTipTextEventHandler(this.Grd12_RequestCellToolTipText);
            this.Grd12.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd12_RequestEdit);
            this.Grd12.AfterCommitEdit += new TenTec.Windows.iGridLib.iGAfterCommitEditEventHandler(this.Grd12_AfterCommitEdit);
            this.Grd12.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd12_KeyDown);
            // 
            // TpgPicture
            // 
            this.TpgPicture.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.TpgPicture.Controls.Add(this.BtnDownload);
            this.TpgPicture.Controls.Add(this.PicEmployee);
            this.TpgPicture.Controls.Add(this.BtnPicture);
            this.TpgPicture.Controls.Add(this.splitContainer4);
            this.TpgPicture.Location = new System.Drawing.Point(4, 51);
            this.TpgPicture.Name = "TpgPicture";
            this.TpgPicture.Size = new System.Drawing.Size(764, 128);
            this.TpgPicture.TabIndex = 9;
            this.TpgPicture.Text = "Picture & File";
            this.TpgPicture.UseVisualStyleBackColor = true;
            // 
            // BtnDownload
            // 
            this.BtnDownload.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnDownload.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnDownload.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDownload.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnDownload.Appearance.Options.UseBackColor = true;
            this.BtnDownload.Appearance.Options.UseFont = true;
            this.BtnDownload.Appearance.Options.UseForeColor = true;
            this.BtnDownload.Appearance.Options.UseTextOptions = true;
            this.BtnDownload.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnDownload.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnDownload.Image = ((System.Drawing.Image)(resources.GetObject("BtnDownload.Image")));
            this.BtnDownload.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnDownload.Location = new System.Drawing.Point(52, 26);
            this.BtnDownload.Name = "BtnDownload";
            this.BtnDownload.Size = new System.Drawing.Size(24, 21);
            this.BtnDownload.TabIndex = 33;
            this.BtnDownload.ToolTip = "Download Employee\'s Picture";
            this.BtnDownload.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnDownload.ToolTipTitle = "Run System";
            this.BtnDownload.Click += new System.EventHandler(this.BtnDownload_Click);
            // 
            // PicEmployee
            // 
            this.PicEmployee.Location = new System.Drawing.Point(11, 88);
            this.PicEmployee.Name = "PicEmployee";
            this.PicEmployee.Size = new System.Drawing.Size(318, 333);
            this.PicEmployee.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PicEmployee.TabIndex = 32;
            this.PicEmployee.TabStop = false;
            // 
            // BtnPicture
            // 
            this.BtnPicture.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.BtnPicture.Appearance.BackColor2 = System.Drawing.Color.SteelBlue;
            this.BtnPicture.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPicture.Appearance.ForeColor = System.Drawing.Color.White;
            this.BtnPicture.Appearance.Options.UseBackColor = true;
            this.BtnPicture.Appearance.Options.UseFont = true;
            this.BtnPicture.Appearance.Options.UseForeColor = true;
            this.BtnPicture.Appearance.Options.UseTextOptions = true;
            this.BtnPicture.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.BtnPicture.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.BtnPicture.Image = ((System.Drawing.Image)(resources.GetObject("BtnPicture.Image")));
            this.BtnPicture.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.BtnPicture.Location = new System.Drawing.Point(11, 26);
            this.BtnPicture.Name = "BtnPicture";
            this.BtnPicture.Size = new System.Drawing.Size(24, 21);
            this.BtnPicture.TabIndex = 22;
            this.BtnPicture.ToolTip = "Show Employee\'s Picture";
            this.BtnPicture.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.BtnPicture.ToolTipTitle = "Run System";
            this.BtnPicture.Click += new System.EventHandler(this.BtnPicture_Click);
            // 
            // splitContainer4
            // 
            this.splitContainer4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer4.Location = new System.Drawing.Point(0, 0);
            this.splitContainer4.Name = "splitContainer4";
            // 
            // splitContainer4.Panel1
            // 
            this.splitContainer4.Panel1.Controls.Add(this.button4);
            this.splitContainer4.Panel1.Controls.Add(this.button3);
            // 
            // splitContainer4.Panel2
            // 
            this.splitContainer4.Panel2.Controls.Add(this.LueUploadFileCategory);
            this.splitContainer4.Panel2.Controls.Add(this.Grd11);
            this.splitContainer4.Panel2.Controls.Add(this.button2);
            this.splitContainer4.Size = new System.Drawing.Size(764, 128);
            this.splitContainer4.SplitterDistance = 335;
            this.splitContainer4.TabIndex = 34;
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.Transparent;
            this.button4.Dock = System.Windows.Forms.DockStyle.Top;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.ForeColor = System.Drawing.Color.AliceBlue;
            this.button4.Location = new System.Drawing.Point(0, 23);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(335, 40);
            this.button4.TabIndex = 23;
            this.button4.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.button4.UseVisualStyleBackColor = false;
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.SteelBlue;
            this.button3.Dock = System.Windows.Forms.DockStyle.Top;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.ForeColor = System.Drawing.Color.AliceBlue;
            this.button3.Location = new System.Drawing.Point(0, 0);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(335, 23);
            this.button3.TabIndex = 22;
            this.button3.Text = "Picture";
            this.button3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.button3.UseVisualStyleBackColor = false;
            // 
            // LueUploadFileCategory
            // 
            this.LueUploadFileCategory.EnterMoveNextControl = true;
            this.LueUploadFileCategory.Location = new System.Drawing.Point(273, 39);
            this.LueUploadFileCategory.Margin = new System.Windows.Forms.Padding(5);
            this.LueUploadFileCategory.Name = "LueUploadFileCategory";
            this.LueUploadFileCategory.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUploadFileCategory.Properties.Appearance.Options.UseFont = true;
            this.LueUploadFileCategory.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUploadFileCategory.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueUploadFileCategory.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUploadFileCategory.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueUploadFileCategory.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUploadFileCategory.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueUploadFileCategory.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueUploadFileCategory.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueUploadFileCategory.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueUploadFileCategory.Properties.DropDownRows = 25;
            this.LueUploadFileCategory.Properties.NullText = "[Empty]";
            this.LueUploadFileCategory.Properties.PopupWidth = 500;
            this.LueUploadFileCategory.Size = new System.Drawing.Size(131, 20);
            this.LueUploadFileCategory.TabIndex = 29;
            this.LueUploadFileCategory.ToolTip = "F4 : Show/hide list";
            this.LueUploadFileCategory.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueUploadFileCategory.EditValueChanged += new System.EventHandler(this.LueUploadFileCategory_EditValueChanged);
            this.LueUploadFileCategory.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LueUploadFileCategory_KeyDown);
            this.LueUploadFileCategory.Leave += new System.EventHandler(this.LueUploadFileCategory_Leave);
            // 
            // Grd11
            // 
            this.Grd11.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.Grd11.DefaultRow.Height = 20;
            this.Grd11.DefaultRow.Sortable = false;
            this.Grd11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grd11.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Grd11.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Grd11.Header.Height = 21;
            this.Grd11.Location = new System.Drawing.Point(0, 23);
            this.Grd11.Name = "Grd11";
            this.Grd11.RowHeader.Visible = true;
            this.Grd11.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.Grd11.SingleClickEdit = true;
            this.Grd11.Size = new System.Drawing.Size(425, 105);
            this.Grd11.TabIndex = 28;
            this.Grd11.TreeCol = null;
            this.Grd11.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            this.Grd11.EllipsisButtonClick += new TenTec.Windows.iGridLib.iGEllipsisButtonClickEventHandler(this.Grd11_EllipsisButtonClick);
            this.Grd11.RequestCellToolTipText += new TenTec.Windows.iGridLib.iGRequestCellToolTipTextEventHandler(this.Grd11_RequestCellToolTipText);
            this.Grd11.RequestEdit += new TenTec.Windows.iGridLib.iGRequestEditEventHandler(this.Grd11_RequestEdit);
            this.Grd11.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Grd11_KeyDown);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.SteelBlue;
            this.button2.Dock = System.Windows.Forms.DockStyle.Top;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.Color.AliceBlue;
            this.button2.Location = new System.Drawing.Point(0, 0);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(425, 23);
            this.button2.TabIndex = 22;
            this.button2.Text = "Attachment File";
            this.button2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.button2.UseVisualStyleBackColor = false;
            // 
            // TpgAdditional
            // 
            this.TpgAdditional.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.TpgAdditional.Controls.Add(this.label74);
            this.TpgAdditional.Controls.Add(this.LueFirstGrdLvlCode);
            this.TpgAdditional.Controls.Add(this.label72);
            this.TpgAdditional.Controls.Add(this.LueLevelCode);
            this.TpgAdditional.Location = new System.Drawing.Point(4, 51);
            this.TpgAdditional.Name = "TpgAdditional";
            this.TpgAdditional.Size = new System.Drawing.Size(764, 128);
            this.TpgAdditional.TabIndex = 12;
            this.TpgAdditional.Text = "Additional";
            this.TpgAdditional.UseVisualStyleBackColor = true;
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label74.ForeColor = System.Drawing.Color.Red;
            this.label74.Location = new System.Drawing.Point(5, 39);
            this.label74.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(65, 14);
            this.label74.TabIndex = 24;
            this.label74.Text = "First Grade";
            this.label74.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueFirstGrdLvlCode
            // 
            this.LueFirstGrdLvlCode.EnterMoveNextControl = true;
            this.LueFirstGrdLvlCode.Location = new System.Drawing.Point(75, 36);
            this.LueFirstGrdLvlCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueFirstGrdLvlCode.Name = "LueFirstGrdLvlCode";
            this.LueFirstGrdLvlCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFirstGrdLvlCode.Properties.Appearance.Options.UseFont = true;
            this.LueFirstGrdLvlCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFirstGrdLvlCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueFirstGrdLvlCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFirstGrdLvlCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueFirstGrdLvlCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFirstGrdLvlCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueFirstGrdLvlCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueFirstGrdLvlCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueFirstGrdLvlCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueFirstGrdLvlCode.Properties.DropDownRows = 30;
            this.LueFirstGrdLvlCode.Properties.NullText = "[Empty]";
            this.LueFirstGrdLvlCode.Properties.PopupWidth = 300;
            this.LueFirstGrdLvlCode.Size = new System.Drawing.Size(245, 20);
            this.LueFirstGrdLvlCode.TabIndex = 25;
            this.LueFirstGrdLvlCode.ToolTip = "F4 : Show/hide list";
            this.LueFirstGrdLvlCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label72.ForeColor = System.Drawing.Color.Black;
            this.label72.Location = new System.Drawing.Point(35, 16);
            this.label72.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(35, 14);
            this.label72.TabIndex = 22;
            this.label72.Text = "Level";
            this.label72.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // LueLevelCode
            // 
            this.LueLevelCode.EnterMoveNextControl = true;
            this.LueLevelCode.Location = new System.Drawing.Point(75, 13);
            this.LueLevelCode.Margin = new System.Windows.Forms.Padding(5);
            this.LueLevelCode.Name = "LueLevelCode";
            this.LueLevelCode.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevelCode.Properties.Appearance.Options.UseFont = true;
            this.LueLevelCode.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevelCode.Properties.AppearanceDisabled.Options.UseFont = true;
            this.LueLevelCode.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevelCode.Properties.AppearanceDropDown.Options.UseFont = true;
            this.LueLevelCode.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevelCode.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.LueLevelCode.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LueLevelCode.Properties.AppearanceFocused.Options.UseFont = true;
            this.LueLevelCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LueLevelCode.Properties.DropDownRows = 30;
            this.LueLevelCode.Properties.NullText = "[Empty]";
            this.LueLevelCode.Properties.PopupWidth = 400;
            this.LueLevelCode.Size = new System.Drawing.Size(400, 20);
            this.LueLevelCode.TabIndex = 23;
            this.LueLevelCode.ToolTip = "F4 : Show/hide list";
            this.LueLevelCode.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.LueLevelCode.EditValueChanged += new System.EventHandler(this.LueLevelCode_EditValueChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.tabPage1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tabPage1.Controls.Add(this.panel6);
            this.tabPage1.Controls.Add(this.textEdit6);
            this.tabPage1.Controls.Add(this.label31);
            this.tabPage1.Controls.Add(this.textEdit7);
            this.tabPage1.Controls.Add(this.label32);
            this.tabPage1.Controls.Add(this.textEdit8);
            this.tabPage1.Controls.Add(this.label33);
            this.tabPage1.Controls.Add(this.textEdit9);
            this.tabPage1.Controls.Add(this.label34);
            this.tabPage1.Controls.Add(this.label35);
            this.tabPage1.Controls.Add(this.lookUpEdit6);
            this.tabPage1.Controls.Add(this.dateEdit1);
            this.tabPage1.Controls.Add(this.label36);
            this.tabPage1.Controls.Add(this.textEdit10);
            this.tabPage1.Controls.Add(this.label37);
            this.tabPage1.Controls.Add(this.label38);
            this.tabPage1.Controls.Add(this.lookUpEdit7);
            this.tabPage1.Controls.Add(this.textEdit11);
            this.tabPage1.Controls.Add(this.label39);
            this.tabPage1.Controls.Add(this.dateEdit2);
            this.tabPage1.Controls.Add(this.label40);
            this.tabPage1.Controls.Add(this.dateEdit3);
            this.tabPage1.Controls.Add(this.label41);
            this.tabPage1.Controls.Add(this.textEdit12);
            this.tabPage1.Controls.Add(this.label42);
            this.tabPage1.Controls.Add(this.memoExEdit1);
            this.tabPage1.Controls.Add(this.label43);
            this.tabPage1.Controls.Add(this.label44);
            this.tabPage1.Controls.Add(this.lookUpEdit8);
            this.tabPage1.Controls.Add(this.label45);
            this.tabPage1.Controls.Add(this.lookUpEdit9);
            this.tabPage1.Controls.Add(this.label46);
            this.tabPage1.Controls.Add(this.lookUpEdit10);
            this.tabPage1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage1.Location = new System.Drawing.Point(4, 26);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(764, 373);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "General";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.panel6.Controls.Add(this.textEdit3);
            this.panel6.Controls.Add(this.label25);
            this.panel6.Controls.Add(this.textEdit4);
            this.panel6.Controls.Add(this.label26);
            this.panel6.Controls.Add(this.label27);
            this.panel6.Controls.Add(this.lookUpEdit3);
            this.panel6.Controls.Add(this.label28);
            this.panel6.Controls.Add(this.lookUpEdit4);
            this.panel6.Controls.Add(this.label29);
            this.panel6.Controls.Add(this.lookUpEdit5);
            this.panel6.Controls.Add(this.label30);
            this.panel6.Controls.Add(this.textEdit5);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel6.Location = new System.Drawing.Point(408, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(352, 369);
            this.panel6.TabIndex = 46;
            // 
            // textEdit3
            // 
            this.textEdit3.EnterMoveNextControl = true;
            this.textEdit3.Location = new System.Drawing.Point(93, 115);
            this.textEdit3.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit3.Name = "textEdit3";
            this.textEdit3.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit3.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit3.Properties.Appearance.Options.UseFont = true;
            this.textEdit3.Properties.MaxLength = 80;
            this.textEdit3.Size = new System.Drawing.Size(243, 20);
            this.textEdit3.TabIndex = 57;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Black;
            this.label25.Location = new System.Drawing.Point(17, 119);
            this.label25.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(74, 14);
            this.label25.TabIndex = 56;
            this.label25.Text = "Bank Branch";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit4
            // 
            this.textEdit4.EnterMoveNextControl = true;
            this.textEdit4.Location = new System.Drawing.Point(93, 93);
            this.textEdit4.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit4.Name = "textEdit4";
            this.textEdit4.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit4.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit4.Properties.Appearance.Options.UseFont = true;
            this.textEdit4.Properties.MaxLength = 80;
            this.textEdit4.Size = new System.Drawing.Size(242, 20);
            this.textEdit4.TabIndex = 55;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Black;
            this.label26.Location = new System.Drawing.Point(8, 97);
            this.label26.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(83, 14);
            this.label26.TabIndex = 54;
            this.label26.Text = "Bank Account";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Black;
            this.label27.Location = new System.Drawing.Point(23, 75);
            this.label27.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(68, 14);
            this.label27.TabIndex = 52;
            this.label27.Text = "Bank Name";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit3
            // 
            this.lookUpEdit3.EnterMoveNextControl = true;
            this.lookUpEdit3.Location = new System.Drawing.Point(93, 71);
            this.lookUpEdit3.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit3.Name = "lookUpEdit3";
            this.lookUpEdit3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit3.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit3.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit3.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit3.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit3.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit3.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit3.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit3.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit3.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit3.Properties.DropDownRows = 12;
            this.lookUpEdit3.Properties.NullText = "[Empty]";
            this.lookUpEdit3.Properties.PopupWidth = 500;
            this.lookUpEdit3.Size = new System.Drawing.Size(243, 20);
            this.lookUpEdit3.TabIndex = 53;
            this.lookUpEdit3.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit3.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Black;
            this.label28.Location = new System.Drawing.Point(55, 53);
            this.label28.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(36, 14);
            this.label28.TabIndex = 50;
            this.label28.Text = "PTKP";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit4
            // 
            this.lookUpEdit4.EnterMoveNextControl = true;
            this.lookUpEdit4.Location = new System.Drawing.Point(93, 49);
            this.lookUpEdit4.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit4.Name = "lookUpEdit4";
            this.lookUpEdit4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit4.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit4.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit4.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit4.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit4.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit4.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit4.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit4.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit4.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit4.Properties.DropDownRows = 12;
            this.lookUpEdit4.Properties.NullText = "[Empty]";
            this.lookUpEdit4.Properties.PopupWidth = 500;
            this.lookUpEdit4.Size = new System.Drawing.Size(243, 20);
            this.lookUpEdit4.TabIndex = 51;
            this.lookUpEdit4.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit4.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Black;
            this.label29.Location = new System.Drawing.Point(18, 31);
            this.label29.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(73, 14);
            this.label29.TabIndex = 48;
            this.label29.Text = "Payroll Type";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit5
            // 
            this.lookUpEdit5.EnterMoveNextControl = true;
            this.lookUpEdit5.Location = new System.Drawing.Point(93, 27);
            this.lookUpEdit5.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit5.Name = "lookUpEdit5";
            this.lookUpEdit5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit5.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit5.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit5.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit5.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit5.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit5.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit5.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit5.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit5.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit5.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit5.Properties.DropDownRows = 12;
            this.lookUpEdit5.Properties.NullText = "[Empty]";
            this.lookUpEdit5.Properties.PopupWidth = 500;
            this.lookUpEdit5.Size = new System.Drawing.Size(243, 20);
            this.lookUpEdit5.TabIndex = 49;
            this.lookUpEdit5.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit5.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(50, 9);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(41, 14);
            this.label30.TabIndex = 46;
            this.label30.Text = "NPWP";
            // 
            // textEdit5
            // 
            this.textEdit5.EnterMoveNextControl = true;
            this.textEdit5.Location = new System.Drawing.Point(93, 5);
            this.textEdit5.Name = "textEdit5";
            this.textEdit5.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit5.Properties.Appearance.Options.UseFont = true;
            this.textEdit5.Properties.MaxLength = 40;
            this.textEdit5.Size = new System.Drawing.Size(243, 20);
            this.textEdit5.TabIndex = 47;
            // 
            // textEdit6
            // 
            this.textEdit6.EnterMoveNextControl = true;
            this.textEdit6.Location = new System.Drawing.Point(94, 334);
            this.textEdit6.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit6.Name = "textEdit6";
            this.textEdit6.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit6.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit6.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit6.Properties.Appearance.Options.UseFont = true;
            this.textEdit6.Properties.MaxLength = 40;
            this.textEdit6.Size = new System.Drawing.Size(219, 20);
            this.textEdit6.TabIndex = 45;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.Black;
            this.label31.Location = new System.Drawing.Point(58, 337);
            this.label31.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(34, 14);
            this.label31.TabIndex = 44;
            this.label31.Text = "Email";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit7
            // 
            this.textEdit7.EnterMoveNextControl = true;
            this.textEdit7.Location = new System.Drawing.Point(94, 312);
            this.textEdit7.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit7.Name = "textEdit7";
            this.textEdit7.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit7.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit7.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit7.Properties.Appearance.Options.UseFont = true;
            this.textEdit7.Properties.MaxLength = 40;
            this.textEdit7.Size = new System.Drawing.Size(185, 20);
            this.textEdit7.TabIndex = 43;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.Black;
            this.label32.Location = new System.Drawing.Point(51, 315);
            this.label32.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(41, 14);
            this.label32.TabIndex = 42;
            this.label32.Text = "Mobile";
            this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit8
            // 
            this.textEdit8.EnterMoveNextControl = true;
            this.textEdit8.Location = new System.Drawing.Point(94, 290);
            this.textEdit8.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit8.Name = "textEdit8";
            this.textEdit8.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit8.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit8.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit8.Properties.Appearance.Options.UseFont = true;
            this.textEdit8.Properties.MaxLength = 40;
            this.textEdit8.Size = new System.Drawing.Size(185, 20);
            this.textEdit8.TabIndex = 41;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.Black;
            this.label33.Location = new System.Drawing.Point(50, 293);
            this.label33.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(42, 14);
            this.label33.TabIndex = 40;
            this.label33.Text = "Phone";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit9
            // 
            this.textEdit9.EnterMoveNextControl = true;
            this.textEdit9.Location = new System.Drawing.Point(94, 268);
            this.textEdit9.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit9.Name = "textEdit9";
            this.textEdit9.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit9.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit9.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit9.Properties.Appearance.Options.UseFont = true;
            this.textEdit9.Properties.MaxLength = 20;
            this.textEdit9.Size = new System.Drawing.Size(185, 20);
            this.textEdit9.TabIndex = 39;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.Black;
            this.label34.Location = new System.Drawing.Point(21, 271);
            this.label34.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(71, 14);
            this.label34.TabIndex = 38;
            this.label34.Text = "Postal Code";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.Color.Black;
            this.label35.Location = new System.Drawing.Point(65, 249);
            this.label35.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(27, 14);
            this.label35.TabIndex = 36;
            this.label35.Text = "City";
            this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit6
            // 
            this.lookUpEdit6.EnterMoveNextControl = true;
            this.lookUpEdit6.Location = new System.Drawing.Point(94, 246);
            this.lookUpEdit6.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit6.Name = "lookUpEdit6";
            this.lookUpEdit6.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit6.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit6.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit6.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit6.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit6.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit6.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit6.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit6.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit6.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit6.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit6.Properties.DropDownRows = 12;
            this.lookUpEdit6.Properties.MaxLength = 16;
            this.lookUpEdit6.Properties.NullText = "[Empty]";
            this.lookUpEdit6.Properties.PopupWidth = 500;
            this.lookUpEdit6.Size = new System.Drawing.Size(295, 20);
            this.lookUpEdit6.TabIndex = 37;
            this.lookUpEdit6.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit6.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // dateEdit1
            // 
            this.dateEdit1.EditValue = null;
            this.dateEdit1.EnterMoveNextControl = true;
            this.dateEdit1.Location = new System.Drawing.Point(94, 202);
            this.dateEdit1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dateEdit1.Name = "dateEdit1";
            this.dateEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit1.Properties.Appearance.Options.UseFont = true;
            this.dateEdit1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dateEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit1.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.dateEdit1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit1.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dateEdit1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit1.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dateEdit1.Properties.MaxLength = 8;
            this.dateEdit1.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEdit1.Size = new System.Drawing.Size(102, 20);
            this.dateEdit1.TabIndex = 33;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.ForeColor = System.Drawing.Color.Black;
            this.label36.Location = new System.Drawing.Point(30, 205);
            this.label36.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(62, 14);
            this.label36.TabIndex = 32;
            this.label36.Text = "Birth Date";
            this.label36.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit10
            // 
            this.textEdit10.EnterMoveNextControl = true;
            this.textEdit10.Location = new System.Drawing.Point(94, 180);
            this.textEdit10.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit10.Name = "textEdit10";
            this.textEdit10.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit10.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit10.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit10.Properties.Appearance.Options.UseFont = true;
            this.textEdit10.Properties.MaxLength = 80;
            this.textEdit10.Size = new System.Drawing.Size(298, 20);
            this.textEdit10.TabIndex = 31;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.ForeColor = System.Drawing.Color.Black;
            this.label37.Location = new System.Drawing.Point(28, 183);
            this.label37.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(64, 14);
            this.label37.TabIndex = 30;
            this.label37.Text = "Birth Place";
            this.label37.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.ForeColor = System.Drawing.Color.Black;
            this.label38.Location = new System.Drawing.Point(44, 161);
            this.label38.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(48, 14);
            this.label38.TabIndex = 28;
            this.label38.Text = "Religion";
            this.label38.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit7
            // 
            this.lookUpEdit7.EnterMoveNextControl = true;
            this.lookUpEdit7.Location = new System.Drawing.Point(94, 158);
            this.lookUpEdit7.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit7.Name = "lookUpEdit7";
            this.lookUpEdit7.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit7.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit7.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit7.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit7.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit7.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit7.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit7.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit7.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit7.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit7.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit7.Properties.DropDownRows = 12;
            this.lookUpEdit7.Properties.MaxLength = 2;
            this.lookUpEdit7.Properties.NullText = "[Empty]";
            this.lookUpEdit7.Properties.PopupWidth = 500;
            this.lookUpEdit7.Size = new System.Drawing.Size(100, 20);
            this.lookUpEdit7.TabIndex = 29;
            this.lookUpEdit7.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit7.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // textEdit11
            // 
            this.textEdit11.EnterMoveNextControl = true;
            this.textEdit11.Location = new System.Drawing.Point(94, 114);
            this.textEdit11.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit11.Name = "textEdit11";
            this.textEdit11.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit11.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit11.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit11.Properties.Appearance.Options.UseFont = true;
            this.textEdit11.Properties.MaxLength = 40;
            this.textEdit11.Size = new System.Drawing.Size(299, 20);
            this.textEdit11.TabIndex = 25;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.ForeColor = System.Drawing.Color.Black;
            this.label39.Location = new System.Drawing.Point(27, 117);
            this.label39.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(65, 14);
            this.label39.TabIndex = 24;
            this.label39.Text = "Id Number";
            this.label39.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dateEdit2
            // 
            this.dateEdit2.EditValue = null;
            this.dateEdit2.EnterMoveNextControl = true;
            this.dateEdit2.Location = new System.Drawing.Point(94, 92);
            this.dateEdit2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dateEdit2.Name = "dateEdit2";
            this.dateEdit2.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit2.Properties.Appearance.Options.UseFont = true;
            this.dateEdit2.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit2.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dateEdit2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit2.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.dateEdit2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit2.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dateEdit2.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit2.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dateEdit2.Properties.MaxLength = 16;
            this.dateEdit2.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEdit2.Size = new System.Drawing.Size(99, 20);
            this.dateEdit2.TabIndex = 23;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.ForeColor = System.Drawing.Color.Black;
            this.label40.Location = new System.Drawing.Point(20, 95);
            this.label40.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(72, 14);
            this.label40.TabIndex = 22;
            this.label40.Text = "Resign Date";
            this.label40.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dateEdit3
            // 
            this.dateEdit3.EditValue = null;
            this.dateEdit3.EnterMoveNextControl = true;
            this.dateEdit3.Location = new System.Drawing.Point(94, 70);
            this.dateEdit3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dateEdit3.Name = "dateEdit3";
            this.dateEdit3.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit3.Properties.Appearance.Options.UseFont = true;
            this.dateEdit3.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit3.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dateEdit3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit3.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.dateEdit3.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit3.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dateEdit3.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit3.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dateEdit3.Properties.MaxLength = 16;
            this.dateEdit3.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEdit3.Size = new System.Drawing.Size(99, 20);
            this.dateEdit3.TabIndex = 21;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.ForeColor = System.Drawing.Color.Red;
            this.label41.Location = new System.Drawing.Point(34, 73);
            this.label41.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(58, 14);
            this.label41.TabIndex = 20;
            this.label41.Text = "Join Date";
            this.label41.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textEdit12
            // 
            this.textEdit12.EnterMoveNextControl = true;
            this.textEdit12.Location = new System.Drawing.Point(94, 4);
            this.textEdit12.Margin = new System.Windows.Forms.Padding(5);
            this.textEdit12.Name = "textEdit12";
            this.textEdit12.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.textEdit12.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEdit12.Properties.Appearance.Options.UseBackColor = true;
            this.textEdit12.Properties.Appearance.Options.UseFont = true;
            this.textEdit12.Properties.MaxLength = 16;
            this.textEdit12.Size = new System.Drawing.Size(136, 20);
            this.textEdit12.TabIndex = 15;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.ForeColor = System.Drawing.Color.Black;
            this.label42.Location = new System.Drawing.Point(29, 7);
            this.label42.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(63, 14);
            this.label42.TabIndex = 14;
            this.label42.Text = "User Code";
            this.label42.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // memoExEdit1
            // 
            this.memoExEdit1.EnterMoveNextControl = true;
            this.memoExEdit1.Location = new System.Drawing.Point(94, 224);
            this.memoExEdit1.Margin = new System.Windows.Forms.Padding(5);
            this.memoExEdit1.Name = "memoExEdit1";
            this.memoExEdit1.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit1.Properties.Appearance.Options.UseFont = true;
            this.memoExEdit1.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit1.Properties.AppearanceDisabled.Options.UseFont = true;
            this.memoExEdit1.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit1.Properties.AppearanceDropDown.Options.UseFont = true;
            this.memoExEdit1.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit1.Properties.AppearanceFocused.Options.UseFont = true;
            this.memoExEdit1.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memoExEdit1.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.memoExEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.memoExEdit1.Properties.MaxLength = 80;
            this.memoExEdit1.Properties.PopupFormSize = new System.Drawing.Size(600, 20);
            this.memoExEdit1.Properties.ShowIcon = false;
            this.memoExEdit1.Size = new System.Drawing.Size(295, 20);
            this.memoExEdit1.TabIndex = 35;
            this.memoExEdit1.ToolTip = "F4 : Show/hide text";
            this.memoExEdit1.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            this.memoExEdit1.ToolTipTitle = "Run System";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.Location = new System.Drawing.Point(42, 227);
            this.label43.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(50, 14);
            this.label43.TabIndex = 34;
            this.label43.Text = "Address";
            this.label43.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.ForeColor = System.Drawing.Color.Black;
            this.label44.Location = new System.Drawing.Point(45, 139);
            this.label44.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(47, 14);
            this.label44.TabIndex = 26;
            this.label44.Text = "Gender";
            this.label44.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit8
            // 
            this.lookUpEdit8.EnterMoveNextControl = true;
            this.lookUpEdit8.Location = new System.Drawing.Point(94, 136);
            this.lookUpEdit8.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit8.Name = "lookUpEdit8";
            this.lookUpEdit8.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit8.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit8.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit8.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit8.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit8.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit8.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit8.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit8.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit8.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit8.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit8.Properties.DropDownRows = 12;
            this.lookUpEdit8.Properties.MaxLength = 1;
            this.lookUpEdit8.Properties.NullText = "[Empty]";
            this.lookUpEdit8.Properties.PopupWidth = 500;
            this.lookUpEdit8.Size = new System.Drawing.Size(99, 20);
            this.lookUpEdit8.TabIndex = 27;
            this.lookUpEdit8.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit8.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.ForeColor = System.Drawing.Color.Black;
            this.label45.Location = new System.Drawing.Point(43, 51);
            this.label45.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(49, 14);
            this.label45.TabIndex = 18;
            this.label45.Text = "Position";
            this.label45.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit9
            // 
            this.lookUpEdit9.EnterMoveNextControl = true;
            this.lookUpEdit9.Location = new System.Drawing.Point(94, 48);
            this.lookUpEdit9.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit9.Name = "lookUpEdit9";
            this.lookUpEdit9.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit9.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit9.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit9.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit9.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit9.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit9.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit9.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit9.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit9.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit9.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit9.Properties.DropDownRows = 12;
            this.lookUpEdit9.Properties.MaxLength = 16;
            this.lookUpEdit9.Properties.NullText = "[Empty]";
            this.lookUpEdit9.Properties.PopupWidth = 500;
            this.lookUpEdit9.Size = new System.Drawing.Size(177, 20);
            this.lookUpEdit9.TabIndex = 19;
            this.lookUpEdit9.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit9.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.ForeColor = System.Drawing.Color.Black;
            this.label46.Location = new System.Drawing.Point(12, 29);
            this.label46.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(80, 14);
            this.label46.TabIndex = 16;
            this.label46.Text = "Departement";
            this.label46.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lookUpEdit10
            // 
            this.lookUpEdit10.EnterMoveNextControl = true;
            this.lookUpEdit10.Location = new System.Drawing.Point(94, 26);
            this.lookUpEdit10.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit10.Name = "lookUpEdit10";
            this.lookUpEdit10.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit10.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit10.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit10.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit10.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit10.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit10.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit10.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit10.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit10.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit10.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit10.Properties.DropDownRows = 12;
            this.lookUpEdit10.Properties.MaxLength = 16;
            this.lookUpEdit10.Properties.NullText = "[Empty]";
            this.lookUpEdit10.Properties.PopupWidth = 500;
            this.lookUpEdit10.Size = new System.Drawing.Size(223, 20);
            this.lookUpEdit10.TabIndex = 17;
            this.lookUpEdit10.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit10.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.tabPage2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tabPage2.Controls.Add(this.dateEdit4);
            this.tabPage2.Controls.Add(this.lookUpEdit11);
            this.tabPage2.Controls.Add(this.lookUpEdit12);
            this.tabPage2.Controls.Add(this.iGrid1);
            this.tabPage2.Location = new System.Drawing.Point(4, 26);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Size = new System.Drawing.Size(764, 373);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Employee Familly";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // dateEdit4
            // 
            this.dateEdit4.EditValue = null;
            this.dateEdit4.EnterMoveNextControl = true;
            this.dateEdit4.Location = new System.Drawing.Point(359, 26);
            this.dateEdit4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dateEdit4.Name = "dateEdit4";
            this.dateEdit4.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit4.Properties.Appearance.Options.UseFont = true;
            this.dateEdit4.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateEdit4.Properties.AppearanceDropDown.Options.UseFont = true;
            this.dateEdit4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit4.Properties.DisplayFormat.FormatString = "dd/MMM/yyyy";
            this.dateEdit4.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit4.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.dateEdit4.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dateEdit4.Properties.Mask.EditMask = "dd/MM/yyyy";
            this.dateEdit4.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEdit4.Size = new System.Drawing.Size(104, 20);
            this.dateEdit4.TabIndex = 40;
            // 
            // lookUpEdit11
            // 
            this.lookUpEdit11.EnterMoveNextControl = true;
            this.lookUpEdit11.Location = new System.Drawing.Point(236, 25);
            this.lookUpEdit11.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit11.Name = "lookUpEdit11";
            this.lookUpEdit11.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit11.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit11.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit11.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit11.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit11.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit11.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit11.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit11.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit11.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit11.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit11.Properties.DropDownRows = 12;
            this.lookUpEdit11.Properties.NullText = "[Empty]";
            this.lookUpEdit11.Properties.PopupWidth = 500;
            this.lookUpEdit11.Size = new System.Drawing.Size(122, 20);
            this.lookUpEdit11.TabIndex = 37;
            this.lookUpEdit11.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit11.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // lookUpEdit12
            // 
            this.lookUpEdit12.EnterMoveNextControl = true;
            this.lookUpEdit12.Location = new System.Drawing.Point(104, 24);
            this.lookUpEdit12.Margin = new System.Windows.Forms.Padding(5);
            this.lookUpEdit12.Name = "lookUpEdit12";
            this.lookUpEdit12.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit12.Properties.Appearance.Options.UseFont = true;
            this.lookUpEdit12.Properties.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit12.Properties.AppearanceDisabled.Options.UseFont = true;
            this.lookUpEdit12.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit12.Properties.AppearanceDropDown.Options.UseFont = true;
            this.lookUpEdit12.Properties.AppearanceDropDownHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit12.Properties.AppearanceDropDownHeader.Options.UseFont = true;
            this.lookUpEdit12.Properties.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lookUpEdit12.Properties.AppearanceFocused.Options.UseFont = true;
            this.lookUpEdit12.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit12.Properties.DropDownRows = 12;
            this.lookUpEdit12.Properties.NullText = "[Empty]";
            this.lookUpEdit12.Properties.PopupWidth = 500;
            this.lookUpEdit12.Size = new System.Drawing.Size(131, 20);
            this.lookUpEdit12.TabIndex = 36;
            this.lookUpEdit12.ToolTip = "F4 : Show/hide list";
            this.lookUpEdit12.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information;
            // 
            // iGrid1
            // 
            this.iGrid1.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.iGrid1.DefaultRow.Height = 20;
            this.iGrid1.DefaultRow.Sortable = false;
            this.iGrid1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.iGrid1.ForeColor = System.Drawing.SystemColors.WindowText;
            this.iGrid1.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.iGrid1.Header.Height = 19;
            this.iGrid1.Location = new System.Drawing.Point(0, 0);
            this.iGrid1.Name = "iGrid1";
            this.iGrid1.ReadOnly = true;
            this.iGrid1.RowHeader.Visible = true;
            this.iGrid1.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.iGrid1.SingleClickEdit = true;
            this.iGrid1.Size = new System.Drawing.Size(760, 369);
            this.iGrid1.TabIndex = 35;
            this.iGrid1.TreeCol = null;
            this.iGrid1.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(255)))));
            this.tabPage3.Controls.Add(this.iGrid2);
            this.tabPage3.Location = new System.Drawing.Point(4, 26);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(764, 373);
            this.tabPage3.TabIndex = 3;
            this.tabPage3.Text = "Employee Work Exp";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // iGrid2
            // 
            this.iGrid2.DefaultCol.SortOrder = TenTec.Windows.iGridLib.iGSortOrder.None;
            this.iGrid2.DefaultRow.Height = 20;
            this.iGrid2.DefaultRow.Sortable = false;
            this.iGrid2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.iGrid2.ForeColor = System.Drawing.SystemColors.WindowText;
            this.iGrid2.Header.BackColor = System.Drawing.Color.LightSteelBlue;
            this.iGrid2.Header.Height = 19;
            this.iGrid2.Location = new System.Drawing.Point(0, 0);
            this.iGrid2.Name = "iGrid2";
            this.iGrid2.RowHeader.Visible = true;
            this.iGrid2.RowSelectionInCellMode = TenTec.Windows.iGridLib.iGRowSelectionInCellModeTypes.SingleRow;
            this.iGrid2.SingleClickEdit = true;
            this.iGrid2.Size = new System.Drawing.Size(764, 373);
            this.iGrid2.TabIndex = 47;
            this.iGrid2.TreeCol = null;
            this.iGrid2.TreeLines.Color = System.Drawing.SystemColors.WindowText;
            // 
            // OD
            // 
            this.OD.FileName = "openFileDialog1";
            // 
            // FrmEmployee
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(996, 669);
            this.Name = "FrmEmployee";
            this.Activated += new System.EventHandler(this.FrmEmployee_Activated);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtDisplayName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmpCodeOld.Properties)).EndInit();
            this.panel4.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.TpgGeneral.ResumeLayout(false);
            this.TpgGeneral.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSpouseEmpCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChkActingOfficial.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteContractDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteContractDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePositionStatusCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRegEntCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTGDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteTGDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueEntityCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPOH.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDivisionCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueWorkGroup.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSection.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeDomicile.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSiteCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtRTRW.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueMaritalStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtVillage.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtSubDistrict.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBarcodeCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtShortCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueEmploymentStatus.Properties)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueBloodRhesus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCostGroup.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueEducationType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueShoeSize.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueClothesSize.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtLevelCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteLeaveStartDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteLeaveStartDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePGCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBloodType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueSystemType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMother.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePayrunPeriod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteWeddingDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteWeddingDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBankAcName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtEmail.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueGrdLvlCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteBirthDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteBirthDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBankBranch.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBirthPlace.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtBankAcNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueReligion.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtMobile.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueBankCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePTKP.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePayrollType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtNPWP.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueGender.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPhone.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueCity.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtIdNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteResignDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteResignDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteJoinDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteJoinDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtUserCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TxtPostalCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MeeAddress.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LuePosCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueDeptCode.Properties)).EndInit();
            this.TpgEmployeeFamily.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueEducationLevelCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueProfessionCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteFamBirthDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteFamBirthDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueFamGender.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd7)).EndInit();
            this.TpgEmployeeWorkExp.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd2)).EndInit();
            this.TpgEmployeeEducation.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueFacCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueMajor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueLevel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd3)).EndInit();
            this.TpgEmployeeTraining.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueTrainingCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd8)).EndInit();
            this.TpgEmployeeTraining2.ResumeLayout(false);
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel2.ResumeLayout(false);
            this.splitContainer3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueTrainingCode2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd10)).EndInit();
            this.TpgEmployeeTraining3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueTrainingCategory.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueTrainingEducationCenter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueTrainingSpecialization.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueTrainingType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueTrainingCode3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd16)).EndInit();
            this.TpgEmployeeCompetence.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueCompetenceCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd4)).EndInit();
            this.TpgPositionHistory.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd15)).EndInit();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.TpgGradeHistory.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd17)).EndInit();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.TpgWarningLetter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd13)).EndInit();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.TpgEmployeeSS.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grd5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd6)).EndInit();
            this.TpgAward.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueAwardCt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd14)).EndInit();
            this.TpgVaccine.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DteVaccineDt.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DteVaccineDt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd12)).EndInit();
            this.TpgPicture.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PicEmployee)).EndInit();
            this.splitContainer4.Panel1.ResumeLayout(false);
            this.splitContainer4.Panel2.ResumeLayout(false);
            this.splitContainer4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LueUploadFileCategory.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grd11)).EndInit();
            this.TpgAdditional.ResumeLayout(false);
            this.TpgAdditional.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LueFirstGrdLvlCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LueLevelCode.Properties)).EndInit();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit2.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit3.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoExEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit10.Properties)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit4.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.iGrid1)).EndInit();
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.iGrid2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.Panel panel3;
        private DevExpress.XtraEditors.TextEdit TxtEmpName;
        private System.Windows.Forms.Label label2;
        internal DevExpress.XtraEditors.TextEdit TxtEmpCode;
        private System.Windows.Forms.Label label1;
        protected System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.TabPage tabPage1;
        protected System.Windows.Forms.Panel panel6;
        internal DevExpress.XtraEditors.TextEdit textEdit3;
        private System.Windows.Forms.Label label25;
        internal DevExpress.XtraEditors.TextEdit textEdit4;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit3;
        private System.Windows.Forms.Label label28;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit4;
        private System.Windows.Forms.Label label29;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit5;
        private System.Windows.Forms.Label label30;
        private DevExpress.XtraEditors.TextEdit textEdit5;
        internal DevExpress.XtraEditors.TextEdit textEdit6;
        private System.Windows.Forms.Label label31;
        internal DevExpress.XtraEditors.TextEdit textEdit7;
        private System.Windows.Forms.Label label32;
        internal DevExpress.XtraEditors.TextEdit textEdit8;
        private System.Windows.Forms.Label label33;
        internal DevExpress.XtraEditors.TextEdit textEdit9;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit6;
        internal DevExpress.XtraEditors.DateEdit dateEdit1;
        private System.Windows.Forms.Label label36;
        private DevExpress.XtraEditors.TextEdit textEdit10;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit7;
        internal DevExpress.XtraEditors.TextEdit textEdit11;
        private System.Windows.Forms.Label label39;
        internal DevExpress.XtraEditors.DateEdit dateEdit2;
        private System.Windows.Forms.Label label40;
        internal DevExpress.XtraEditors.DateEdit dateEdit3;
        private System.Windows.Forms.Label label41;
        internal DevExpress.XtraEditors.TextEdit textEdit12;
        private System.Windows.Forms.Label label42;
        private DevExpress.XtraEditors.MemoExEdit memoExEdit1;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit8;
        private System.Windows.Forms.Label label45;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit9;
        private System.Windows.Forms.Label label46;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit10;
        private System.Windows.Forms.TabPage tabPage2;
        internal DevExpress.XtraEditors.DateEdit dateEdit4;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit11;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit12;
        protected internal TenTec.Windows.iGridLib.iGrid iGrid1;
        private System.Windows.Forms.TabPage tabPage3;
        protected internal TenTec.Windows.iGridLib.iGrid iGrid2;
        internal DevExpress.XtraEditors.TextEdit TxtEmpCodeOld;
        private System.Windows.Forms.Label label48;
        private DevExpress.XtraEditors.TextEdit TxtDisplayName;
        private System.Windows.Forms.Label label66;
        public DevExpress.XtraEditors.SimpleButton BtnCopyGeneral;
        private System.Windows.Forms.Label LblCopyGeneral;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage TpgGeneral;
        private System.Windows.Forms.Label label60;
        public DevExpress.XtraEditors.LookUpEdit LuePositionStatusCode;
        internal DevExpress.XtraEditors.TextEdit TxtRegEntCode;
        private System.Windows.Forms.Label label53;
        internal DevExpress.XtraEditors.DateEdit DteTGDt;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label70;
        private DevExpress.XtraEditors.LookUpEdit LueEntityCode;
        internal DevExpress.XtraEditors.TextEdit TxtPOH;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label LblDivisionCode;
        private DevExpress.XtraEditors.LookUpEdit LueDivisionCode;
        private System.Windows.Forms.Label label65;
        private DevExpress.XtraEditors.LookUpEdit LueWorkGroup;
        private System.Windows.Forms.Label label64;
        private DevExpress.XtraEditors.LookUpEdit LueSection;
        private System.Windows.Forms.Label label63;
        private DevExpress.XtraEditors.MemoExEdit MeeDomicile;
        private System.Windows.Forms.Label LblSiteCode;
        private DevExpress.XtraEditors.LookUpEdit LueSiteCode;
        internal DevExpress.XtraEditors.TextEdit TxtRTRW;
        private System.Windows.Forms.Label label56;
        internal DevExpress.XtraEditors.TextEdit TxtVillage;
        private System.Windows.Forms.Label label55;
        internal DevExpress.XtraEditors.TextEdit TxtSubDistrict;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label LblEmploymentStatus;
        internal DevExpress.XtraEditors.TextEdit TxtBarcodeCode;
        private System.Windows.Forms.Label label52;
        internal DevExpress.XtraEditors.TextEdit TxtShortCode;
        private System.Windows.Forms.Label label50;
        private DevExpress.XtraEditors.LookUpEdit LueEmploymentStatus;
        protected System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label14;
        internal DevExpress.XtraEditors.TextEdit TxtLevelCode;
        private System.Windows.Forms.Label label47;
        internal DevExpress.XtraEditors.DateEdit DteLeaveStartDt;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label LblPGCode;
        private DevExpress.XtraEditors.LookUpEdit LuePGCode;
        private System.Windows.Forms.Label label62;
        private DevExpress.XtraEditors.LookUpEdit LueBloodType;
        private System.Windows.Forms.Label LblSystemType;
        private DevExpress.XtraEditors.LookUpEdit LueSystemType;
        private DevExpress.XtraEditors.TextEdit TxtMother;
        private System.Windows.Forms.Label LblMother;
        private System.Windows.Forms.Label LblPayrunPeriod;
        private DevExpress.XtraEditors.LookUpEdit LuePayrunPeriod;
        internal DevExpress.XtraEditors.DateEdit DteWeddingDt;
        private System.Windows.Forms.Label label59;
        internal DevExpress.XtraEditors.TextEdit TxtBankAcName;
        private System.Windows.Forms.Label lblMarital;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label LblGrdLvlCode;
        internal DevExpress.XtraEditors.TextEdit TxtEmail;
        private DevExpress.XtraEditors.LookUpEdit LueMaritalStatus;
        private DevExpress.XtraEditors.LookUpEdit LueGrdLvlCode;
        internal DevExpress.XtraEditors.DateEdit DteBirthDt;
        internal DevExpress.XtraEditors.TextEdit TxtBankBranch;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label23;
        private DevExpress.XtraEditors.TextEdit TxtBirthPlace;
        private System.Windows.Forms.Label label13;
        internal DevExpress.XtraEditors.TextEdit TxtBankAcNo;
        private System.Windows.Forms.Label label12;
        private DevExpress.XtraEditors.LookUpEdit LueReligion;
        internal DevExpress.XtraEditors.TextEdit TxtMobile;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label22;
        private DevExpress.XtraEditors.LookUpEdit LueBankCode;
        private System.Windows.Forms.Label label21;
        private DevExpress.XtraEditors.LookUpEdit LuePTKP;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label19;
        private DevExpress.XtraEditors.LookUpEdit LuePayrollType;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label7;
        private DevExpress.XtraEditors.TextEdit TxtNPWP;
        private DevExpress.XtraEditors.LookUpEdit LueGender;
        private System.Windows.Forms.Label label16;
        internal DevExpress.XtraEditors.TextEdit TxtPhone;
        private System.Windows.Forms.Label label9;
        private DevExpress.XtraEditors.LookUpEdit LueCity;
        internal DevExpress.XtraEditors.TextEdit TxtIdNumber;
        internal DevExpress.XtraEditors.DateEdit DteResignDt;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        internal DevExpress.XtraEditors.DateEdit DteJoinDt;
        private System.Windows.Forms.Label label6;
        internal DevExpress.XtraEditors.TextEdit TxtUserCode;
        internal DevExpress.XtraEditors.TextEdit TxtPostalCode;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label15;
        private DevExpress.XtraEditors.MemoExEdit MeeAddress;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label LblPosCode;
        public DevExpress.XtraEditors.LookUpEdit LuePosCode;
        private System.Windows.Forms.Label LblDeptCode;
        private DevExpress.XtraEditors.LookUpEdit LueDeptCode;
        private System.Windows.Forms.TabPage TpgEmployeeFamily;
        private System.Windows.Forms.SplitContainer splitContainer2;
        internal DevExpress.XtraEditors.DateEdit DteFamBirthDt;
        private DevExpress.XtraEditors.LookUpEdit LueFamGender;
        private DevExpress.XtraEditors.LookUpEdit LueStatus;
        protected internal TenTec.Windows.iGridLib.iGrid Grd1;
        private System.Windows.Forms.Button BtnFamily;
        protected internal TenTec.Windows.iGridLib.iGrid Grd7;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TabPage TpgEmployeeWorkExp;
        protected internal TenTec.Windows.iGridLib.iGrid Grd2;
        private System.Windows.Forms.TabPage TpgEmployeeEducation;
        private DevExpress.XtraEditors.LookUpEdit LueFacCode;
        private DevExpress.XtraEditors.LookUpEdit LueField;
        private DevExpress.XtraEditors.LookUpEdit LueMajor;
        private DevExpress.XtraEditors.LookUpEdit LueLevel;
        protected internal TenTec.Windows.iGridLib.iGrid Grd3;
        private System.Windows.Forms.TabPage TpgEmployeeTraining;
        private DevExpress.XtraEditors.LookUpEdit LueTrainingCode;
        protected internal TenTec.Windows.iGridLib.iGrid Grd8;
        private System.Windows.Forms.TabPage TpgEmployeeTraining2;
        private System.Windows.Forms.SplitContainer splitContainer3;
        protected internal TenTec.Windows.iGridLib.iGrid Grd9;
        protected internal TenTec.Windows.iGridLib.iGrid Grd10;
        private System.Windows.Forms.TabPage TpgEmployeeCompetence;
        private DevExpress.XtraEditors.LookUpEdit LueCompetenceCode;
        protected internal TenTec.Windows.iGridLib.iGrid Grd4;
        private System.Windows.Forms.TabPage TpgEmployeeSS;
        private System.Windows.Forms.SplitContainer splitContainer1;
        protected internal TenTec.Windows.iGridLib.iGrid Grd5;
        protected internal TenTec.Windows.iGridLib.iGrid Grd6;
        private System.Windows.Forms.TabPage TpgPicture;
        public DevExpress.XtraEditors.SimpleButton BtnDownload;
        private System.Windows.Forms.PictureBox PicEmployee;
        public DevExpress.XtraEditors.SimpleButton BtnPicture;
        private DevExpress.XtraEditors.LookUpEdit LueTrainingCode2;
        internal DevExpress.XtraEditors.DateEdit DteContractDt;
        private System.Windows.Forms.Label LblContractDt;
        private System.Windows.Forms.Label label68;
        private DevExpress.XtraEditors.LookUpEdit LueShoeSize;
        private System.Windows.Forms.Label label61;
        private DevExpress.XtraEditors.LookUpEdit LueClothesSize;
        private System.Windows.Forms.Label label67;
        private DevExpress.XtraEditors.LookUpEdit LueEducationType;
        private System.Windows.Forms.SplitContainer splitContainer4;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        protected internal TenTec.Windows.iGridLib.iGrid Grd11;
        private System.Windows.Forms.SaveFileDialog SFD;
        private System.Windows.Forms.OpenFileDialog OD;
        private System.Windows.Forms.Label label71;
        private DevExpress.XtraEditors.LookUpEdit LueCostGroup;
        internal DevExpress.XtraEditors.CheckEdit ChkActingOfficial;
        private DevExpress.XtraEditors.LookUpEdit LueEducationLevelCode;
        private DevExpress.XtraEditors.LookUpEdit LueProfessionCode;
        private System.Windows.Forms.TabPage TpgAdditional;
        private System.Windows.Forms.Label label72;
        private DevExpress.XtraEditors.LookUpEdit LueLevelCode;
        private System.Windows.Forms.TabPage TpgVaccine;
        protected internal TenTec.Windows.iGridLib.iGrid Grd12;
        internal DevExpress.XtraEditors.DateEdit DteVaccineDt;
        private System.Windows.Forms.Label label73;
        private DevExpress.XtraEditors.LookUpEdit LueBloodRhesus;
        private System.Windows.Forms.TabPage TpgWarningLetter;
        protected internal TenTec.Windows.iGridLib.iGrid Grd13;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label LblWarningLetterRefreshData;
        private System.Windows.Forms.TabPage TpgEmployeeTraining3;
        protected internal TenTec.Windows.iGridLib.iGrid Grd16;
        private System.Windows.Forms.TabPage TpgPositionHistory;
        protected internal TenTec.Windows.iGridLib.iGrid Grd15;
        private System.Windows.Forms.TabPage TpgAward;
        protected internal TenTec.Windows.iGridLib.iGrid Grd14;
        private DevExpress.XtraEditors.LookUpEdit LueAwardCt;
        private DevExpress.XtraEditors.LookUpEdit LueTrainingEducationCenter;
        private DevExpress.XtraEditors.LookUpEdit LueTrainingSpecialization;
        private DevExpress.XtraEditors.LookUpEdit LueTrainingType;
        private DevExpress.XtraEditors.LookUpEdit LueTrainingCode3;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Label LblPositionHistoryRefreshData;
        private System.Windows.Forms.TabPage TpgGradeHistory;
        protected internal TenTec.Windows.iGridLib.iGrid Grd17;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Label LblGradeHistoryRefreshData;
        private DevExpress.XtraEditors.LookUpEdit LueTrainingCategory;
        private System.Windows.Forms.Label label74;
        private DevExpress.XtraEditors.LookUpEdit LueFirstGrdLvlCode;
        private DevExpress.XtraEditors.LookUpEdit LueUploadFileCategory;
        public DevExpress.XtraEditors.SimpleButton BtnSpouseEmpCode;
        internal DevExpress.XtraEditors.TextEdit TxtSpouseEmpCode;
        private System.Windows.Forms.Label LblSpouseEmpCode;
    }
}