﻿#region Update
// 22/12/2017 [HAR/SRN] BUG saat refresh karena query
#endregion 

#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using MySql.Data.MySqlClient;
using TenTec.Windows.iGridLib;
using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;

#endregion

namespace RunSystem
{
    public partial class FrmPORequestRevisionFind : RunSystem.FrmBase2
    {
        #region Field

        private FrmPORequestRevision mFrmParent;
        private string mSQL = string.Empty;

        #endregion

        #region Constructor

        public FrmPORequestRevisionFind(FrmPORequestRevision FrmParent)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
        }

        #endregion

        #region Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                this.Text = mFrmParent.Text;
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
                Sm.SetDefaultPeriod(ref DteDocDt1, ref DteDocDt2, -1);
                Sl.SetLueVdCode(ref LueVdCode);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt,  ");
            SQL.AppendLine("A.PORequestDocNo, E.ItCode, I.ItName, H.VdName, F.UPrice, A.QtyOld, A.Qty, A.Remark, ");
            SQL.AppendLine("A.CreateBy, A.CreateDt, A.LastUpBy, A.LastUpDt ");
            SQL.AppendLine("From TblPORequestRevision A ");
            SQL.AppendLine("Inner Join TblPORequestHdr B On A.PORequestDocNo = B.DocNo ");
            SQL.AppendLine("Inner Join TblPORequestDtl D On A.PORequestDocNo=D.DocNo And A.PORequestDNo=D.DNo ");
            SQL.AppendLine("Inner Join TblMaterialRequestDtl E On D.MaterialRequestDocNo=E.DocNo And D.MaterialRequestDNo=E.DNo ");
            SQL.AppendLine("Inner Join TblItem I On E.ItCode = I.ItCode ");
            SQL.AppendLine("Inner Join TblQtDtl F On F.DocNo = D.QtDocNo And D.QtDNo = F.DNo ");
            SQL.AppendLine("Inner Join TblQtHdr G On G.DocNo = F.DocNo ");
            if (mFrmParent.mIsGroupPaymentTermActived)
            {
                SQL.AppendLine("    And (G.PtCode Is Null Or ( ");
                SQL.AppendLine("    And G.PtCode Is Not Null ");
                SQL.AppendLine("    And G.PtCode In (");
                SQL.AppendLine("        Select PtCode From TblGroupPaymentTerm ");
                SQL.AppendLine("        Where GrpCode In (Select GrpCode From TblUser Where UserCode=@UserCode) ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine(")) ");
            }
            SQL.AppendLine("Inner Join TblVendor H On G.VdCode= H.VdCode ");
            SQL.AppendLine("Inner Join TblMaterialRequestHdr L On E.DocNo=L.DocNo ");
            if (mFrmParent.mIsFilterBySite)
            {
                SQL.AppendLine("    And (L.SiteCode Is Null Or ( ");
                SQL.AppendLine("    L.SiteCode Is Not Null ");
                SQL.AppendLine("    And Exists( ");
                SQL.AppendLine("        Select 1 From TblGroupSite ");
                SQL.AppendLine("        Where SiteCode=IfNull(L.SiteCode, '') ");
                SQL.AppendLine("        And GrpCode In ( ");
                SQL.AppendLine("            Select GrpCode From TblUser ");
                SQL.AppendLine("            Where UserCode=@UserCode ");
                SQL.AppendLine("            ) ");
                SQL.AppendLine("        ) ");
                SQL.AppendLine(")) ");
            }
            SQL.AppendLine("Where A.DocDt Between @DocDt1 And @DocDt2 ");

            mSQL = SQL.ToString();
        }

        private void SetGrd()
        {
            Grd1.Cols.Count = 17;
            Grd1.FrozenArea.ColCount = 2;
            Grd1.ReadOnly = false;
            Sm.GrdHdr(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "Document#", 
                        "Date",
                        "PO Request#",
                        "Item's"+Environment.NewLine+"Code",
                        "Item's Name",

                        //6-10
                        "Vendor",
                        "Unit Price",
                        "Outstanding"+Environment.NewLine+"Quantity",
                        "New Quantity",
                        "Remark",

                        //11-15
                        "Created"+Environment.NewLine+"By",
                        "Created"+Environment.NewLine+"Date", 
                        "Created"+Environment.NewLine+"Time", 
                        "Last"+Environment.NewLine+"Updated By", 
                        "Last"+Environment.NewLine+"Updated Date",
                        
                        //16
                        "Last"+Environment.NewLine+"Updated Time"
                    }
                );
            Sm.GrdFormatDate(Grd1, new int[] { 2, 12, 15 });
            Sm.GrdFormatTime(Grd1, new int[] { 13, 16 });
            Sm.GrdFormatDec(Grd1, new int[] { 7, 8, 9 }, 0);
            Sm.GrdColReadOnly(true, false, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 });
            Sm.GrdColInvisible(Grd1, new int[] { 4, 11, 12, 13, 14, 15, 16 }, false);
            Sm.SetGrdProperty(Grd1, true);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 4, 11, 12, 13, 14, 15, 16 }, !ChkHideInfoInGrd.Checked);
            Sm.SetGrdAutoSize(Grd1);
        }

        override protected void ShowData()
        {
            Sm.ClearGrd(Grd1, false);
            if (
                Sm.IsDteEmpty(DteDocDt1, "Start date") ||
                Sm.IsDteEmpty(DteDocDt2, "End date") ||
                Sm.IsFilterByDateInvalid(ref DteDocDt1, ref DteDocDt2)
                ) return;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                string Filter = " ";

                var cm = new MySqlCommand();

                Sm.CmParamDt(ref cm, "@DocDt1", Sm.GetDte(DteDocDt1));
                Sm.CmParamDt(ref cm, "@DocDt2", Sm.GetDte(DteDocDt2));
                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

                Sm.FilterStr(ref Filter, ref cm, TxtDocNo.Text, "A.DocNo", false);
                Sm.FilterStr(ref Filter, ref cm, Sm.GetLue(LueVdCode), "G.VdCode", true);
                Sm.FilterStr(ref Filter, ref cm, TxtItCode.Text, new string[] { "E.ItCode", "I.ItName" });

                Sm.ShowDataInGrid(
                        ref Grd1, ref cm,
                        mSQL + Filter + " Order By A.CreateDt Desc;",
                        new string[]
                        {
                            //0
                            "DocNo", 
                            
                            //1-5
                            "DocDt", "PORequestDocNo", "ItCode", "ItName", "VdName", 
                            
                            //6-10
                            "UPrice", "QtyOld", "Qty",  "Remark", "CreateBy",   
                            
                            //11-13
                            "CreateDt", "LastUpBy", "LastUpDt"
                        },
                        (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                        {
                            Grd.Cells[Row, 0].Value = Row + 1;
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 2, 1);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 6, 5);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 7, 6);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 8, 7);
                            Sm.SetGrdValue("N", Grd, dr, c, Row, 9, 8);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 10, 9);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 11, 10);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 12, 11);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 13, 11);
                            Sm.SetGrdValue("S", Grd, dr, c, Row, 14, 12);
                            Sm.SetGrdValue("D", Grd, dr, c, Row, 15, 13);
                            Sm.SetGrdValue("T", Grd, dr, c, Row, 16, 13);
                        }, true, false, false, true
                    );
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void ChooseData()
        {
            if (Sm.IsFindGridValid(Grd1, 1))
            {
                mFrmParent.ShowData(Sm.GetGrdStr(Grd1, Grd1.CurRow.Index, 1));
                this.Hide();
            }
        }

        #endregion

        #region Event

        #region Misc Control Event

        private void ChkDocNo_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Document#");
        }

        private void ChkVdCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetLookUpEdit(this, sender, "Vendor");
        }

        private void ChkItCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Item");
        }

        private void TxtDocNo_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        private void DteDocDt1_EditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetDte(DteDocDt2).Length == 0) DteDocDt2.EditValue = DteDocDt1.EditValue;
        }

        private void LueVdCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LueVdCode, new Sm.RefreshLue1(Sl.SetLueVdCode));
            Sm.FilterLueSetCheckEdit(this, sender);
        }

        private void TxtItCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        #endregion

        #endregion
    }
}
