﻿#region Update
/*
    21/12/2020 [IBL/SRN] New Apps
    17/01/2021 [TKG/PHT] ubah GenerateVoucherRequestDocNo
    02/02/2021 [IBL/SRN] Tambah validasi: Tidak boleh ada lebih dari 1 transaksi outstanding VR For Consignment untuk 1 vendor yang samas
    09/02/2021 [VIN/SRN] tambah journal
*/
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmVoucherRequestConsignment : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string
            mMenuCode = string.Empty,
            mAccessInd = string.Empty,
            mDocNo = string.Empty, //if this application is called from other application
            mVdCode = string.Empty,
            mVdCodeInternal = string.Empty,
            mDBNamePOS = string.Empty
            ;
        internal FrmVoucherRequestConsignmentFind FrmFind;

        private string
            mVoucherCodeFormatType = string.Empty,
            mBankAcCodeForVRConsigne = string.Empty,
            mVoucherRequestConsignmentDeptCode = string.Empty,
            mVoucherDocType = "71";  // VoucherRequestConsignment

        private bool mIsInsert = false,
            mIsAutoJournalActived = false
            ;

        #endregion

        #region Constructor

        public FrmVoucherRequestConsignment(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Voucher Request";

                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);
                Sm.SetControlReadOnly(new List<DXE.BaseEdit> { TxtDocNo, TxtVdCode, TxtAmt, TxtVoucherRequestDocNo, TxtVoucherDocNo }, true);
                GetParameter();
                SetGrd();
                SetLueWhsCode(ref LueWhsCode, string.Empty);
                SetFormControl(mState.View);

                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible = BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible = BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmVoucherRequestConsignmentFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("CancelProcess", string.Empty) == DialogResult.No) return;
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            mIsInsert = false;
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    EditData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        internal void SetLueWhsCode(ref LookUpEdit Lue, string Code)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select 'all' As Col1, 'ALL' As Col2 ");
            SQL.AppendLine("Union All ");
            SQL.AppendLine("Select Distinct B.WhsCode As Col1, B.WhsName As Col2 ");
            SQL.AppendLine("From " + mDBNamePOS + ".Tbl_Consignment_Billing A ");
            SQL.AppendLine("Inner Join TblWarehouse B On A.Warehouse = B.WhsCode ");
            if (Code.Length > 0)
                SQL.AppendLine("Where B.WhsCode=@Code;");
            else
                SQL.AppendLine(" Order By Col2;");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@Code", Code);
            Sm.SetLue2(ref Lue, ref cm, 0, 35, false, true, "Code", "Name", "Col2", "Col1");
            if (Code.Length > 0) Sm.SetLue(Lue, Code);
        }

        #endregion

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (
                Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No ||
                IsInsertedDataNotValid()
                ) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "VoucherRequestConsignment", "TblVoucherRequestConsignmentHdr");
            string VoucherRequestDocNo = string.Empty;

            if (mVoucherCodeFormatType == "2")
                VoucherRequestDocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "VoucherRequest", "TblVoucherRequestHdr");
            else
                VoucherRequestDocNo = GenerateVoucherRequestDocNo();

            var cml = new List<MySqlCommand>();
            var cml2 = new List<MySqlCommand>();

            cml.Add(SaveVoucherRequestConsignmentHdr(DocNo, VoucherRequestDocNo));

            for (int i = 0; i < Grd1.Rows.Count - 1; ++i)
                cml.Add(SaveVoucherRequestConsignmentDtl(DocNo, i));

            Sm.ExecCommands(cml);

            var l = new List<DeductionDtl2>();
            PrepDeductionDetail(ref l, DocNo);
            foreach (var x in l.OrderBy(o => o.DocNo).ThenBy(o => o.DNo).ThenBy(o => o.DNo2))
                cml2.Add(SaveVoucherRequestConsignmentDtl2(x));

            cml2.Add(SaveVoucherRequestHdr(VoucherRequestDocNo, DocNo));
            cml2.Add(SaveVoucherRequestDtl(VoucherRequestDocNo));
            if (mIsAutoJournalActived) cml2.Add(SaveJournal(DocNo));


            Sm.ExecCommands(cml2);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                Sm.IsLueEmpty(LueWhsCode, "Warehouse") ||
                Sm.IsTxtEmpty(TxtVdCode, "Vendor", false) ||
                IsVendorNotValid() ||
                IsGrdEmpty() ||
                IsGrdValueNotValid()
                ;
        }

        private bool IsVendorNotValid()
        {
            string DocNo = string.Empty, VRDocNo = string.Empty;
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select Concat( ");
            SQL.AppendLine("    'There is an outstanding transaction for this vendor.', '\n', ");
            SQL.AppendLine("    'Document#          : ',A.DocNo, '\n', ");
            SQL.AppendLine("    'Voucer Request# : ',A.VoucherRequestDocNo) As Remark ");
            SQL.AppendLine("From TblVoucherRequestConsignmentHdr A ");
            SQL.AppendLine("Inner Join TblVoucherRequestHdr B On A.VoucherRequestDocNo = B.DocNo ");
            SQL.AppendLine("Where VdCode = @Param ");
            SQL.AppendLine("And A.CancelInd = 'N' ");
            SQL.AppendLine("And A.Status In ('O','A') ");
            SQL.AppendLine("And B.CancelInd = 'N' ");
            SQL.AppendLine("And B.VoucherDocNo Is Null ");
            SQL.AppendLine("Limit 1  ");

            if (Sm.IsDataExist(SQL.ToString(), mVdCode))
            {
                Sm.StdMsg(mMsgType.Warning, Sm.GetValue(SQL.ToString(), mVdCode));
                return true;
            }

            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count <= 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least one data.");
                return true;
            }

            return false;
        }

        private bool IsGrdValueNotValid()
        {
            for (int Row = 0; Row < Grd1.Rows.Count - 1; Row++)
            {
                for (int Row2 = 0; Row2 < Grd1.Rows.Count - 1; Row2++)
                {
                    if (Row != Row2 &&
                        Sm.CompareStr(Sm.GetGrdStr(Grd1, Row, 2), Sm.GetGrdStr(Grd1, Row2, 2)))
                    {
                        Sm.StdMsg(mMsgType.Warning,
                            "PPBK#   : " + Sm.GetGrdStr(Grd1, Row, 2) + Environment.NewLine +
                            "Payable : " + Sm.GetGrdStr(Grd1, Row, 6) + Environment.NewLine + Environment.NewLine +
                            "Duplicate item in the list."
                            );
                        Sm.FocusGrd(Grd1, Row, 1);
                        return true;
                    }
                }
            }
            return false;
        }

        private MySqlCommand SaveVoucherRequestConsignmentHdr(string DocNo, string VoucherRequestDocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblVoucherRequestConsignmentHdr "); 
            SQL.AppendLine("(DocNo, DocDt, CancelInd, Status, WhsCode, VdCode, ");
            SQL.AppendLine("VoucherRequestDocNo, Amt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, 'N', 'O', @WhsCode, @VdCode, ");
            SQL.AppendLine("@VoucherRequestDocNo, @Amt, @Remark, @CreateBy, CurrentDateTime());");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetLue(LueWhsCode));
            Sm.CmParam<String>(ref cm, "@VdCode", mVdCode);
            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo", VoucherRequestDocNo);
            Sm.CmParam<decimal>(ref cm, "@Amt", Convert.ToDecimal(TxtAmt.Text));
            //Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveVoucherRequestConsignmentDtl(string DocNo, int Row)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblVoucherRequestConsignmentDtl(DocNo, DNo, BillNo, WhsCode, Mth, Yr, Amt, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@DocNo, @DNo, @BillNo, @WhsCode, @Mth, @Yr, @Amt, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", Sm.Right("00" + (Row + 1).ToString(), 3));
            Sm.CmParam<String>(ref cm, "@BillNo", Sm.GetGrdStr(Grd1, Row, 2));
            Sm.CmParam<String>(ref cm, "@Mth", Sm.GetGrdStr(Grd1, Row, 4));
            Sm.CmParam<String>(ref cm, "@Yr", Sm.GetGrdStr(Grd1, Row, 5));
            Sm.CmParam<Decimal>(ref cm, "@Amt", Sm.GetGrdDec(Grd1, Row, 6));
            Sm.CmParam<String>(ref cm, "@WhsCode", Sm.GetGrdStr(Grd1, Row, 7));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private void PrepDeductionDetail(ref List<DeductionDtl2> l, string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Set @DocNo = '"+DocNo+"'; ");
            SQL.AppendLine("Select * From ");
            SQL.AppendLine("( ");
            SQL.AppendLine("    Select '01' `No`, DocNo, DNo, 'Sarana Penjualan' DeductionName, tDeduction1 Amt ");
            SQL.AppendLine("    From " + mDBNamePOS + ".Tbl_Consignment_Billing A ");
            SQL.AppendLine("    Inner Join TblVoucherRequestConsignmentDtl B On A.Bill_No = B.BillNo ");

            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select '02' `No`, DocNo, DNo, 'Biaya Listrik' DeductionName, tDeduction2 Amt ");
            SQL.AppendLine("    From " + mDBNamePOS + ".Tbl_Consignment_Billing A ");
            SQL.AppendLine("    Inner Join TblVoucherRequestConsignmentDtl B On A.Bill_No = B.BillNo ");

            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select '03' `No`, DocNo, DNo, 'Biaya Telepon' DeductionName, tDeduction3 Amt ");
            SQL.AppendLine("    From " + mDBNamePOS + ".Tbl_Consignment_Billing A ");
            SQL.AppendLine("    Inner Join TblVoucherRequestConsignmentDtl B On A.Bill_No = B.BillNo ");

            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select '04' `No`, DocNo, DNo, 'Credit Card' DeductionName, tDeduction5 Amt ");
            SQL.AppendLine("    From " + mDBNamePOS + ".Tbl_Consignment_Billing A ");
            SQL.AppendLine("    Inner Join TblVoucherRequestConsignmentDtl B On A.Bill_No = B.BillNo ");

            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select '05' `No`, DocNo, DNo, 'PUKK / PKBL' DeductionName, tDeduction6 Amt ");
            SQL.AppendLine("    From " + mDBNamePOS + ".Tbl_Consignment_Billing A ");
            SQL.AppendLine("    Inner Join TblVoucherRequestConsignmentDtl B On A.Bill_No = B.BillNo ");

            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select '06' `No`, DocNo, DNo, 'Dana Promosi' DeductionName, tDeduction7 Amt ");
            SQL.AppendLine("    From " + mDBNamePOS + ".Tbl_Consignment_Billing A ");
            SQL.AppendLine("    Inner Join TblVoucherRequestConsignmentDtl B On A.Bill_No = B.BillNo ");

            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select '07' `No`, DocNo, DNo, 'Pengurangan' DeductionName, tDeduction8 Amt ");
            SQL.AppendLine("    From " + mDBNamePOS + ".Tbl_Consignment_Billing A ");
            SQL.AppendLine("    Inner Join TblVoucherRequestConsignmentDtl B On A.Bill_No = B.BillNo ");

            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select '08' `No`, DocNo, DNo, 'Lain-lain' DeductionName, tDeduction9 Amt ");
            SQL.AppendLine("    From " + mDBNamePOS + ".Tbl_Consignment_Billing A ");
            SQL.AppendLine("    Inner Join TblVoucherRequestConsignmentDtl B On A.Bill_No = B.BillNo ");

            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select '09' `No`, DocNo, DNo, 'PPN Wapu' DeductionName, tPPN_Wapu Amt ");
            SQL.AppendLine("    From " + mDBNamePOS + ".Tbl_Consignment_Billing A ");
            SQL.AppendLine("    Inner Join TblVoucherRequestConsignmentDtl B On A.Bill_No = B.BillNo ");

            SQL.AppendLine("    Union All ");
            SQL.AppendLine("    Select '10' `No`, DocNo, DNo, 'PPH Pasal 22' DeductionName, tPPH22 Amt ");
            SQL.AppendLine("    From " + mDBNamePOS + ".Tbl_Consignment_Billing A ");
            SQL.AppendLine("    Inner Join TblVoucherRequestConsignmentDtl B On A.Bill_No = B.BillNo ");

            SQL.AppendLine(") Tbl ");
            SQL.AppendLine("Where DocNo = @DocNo ");
            SQL.AppendLine("And Amt != 0 ");
            SQL.AppendLine("Order By DocNo, DNo, No ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@BillNo", GetBillNo());
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                {
                    "DocNo", "DNo", "DeductionName", "Amt",
                });
                if (dr.HasRows)
                {
                    int i = 1;
                    string mDNo = "001";

                    while (dr.Read())
                    {
                        if (Sm.DrStr(dr, c[1]) == mDNo)
                            {
                                l.Add(new DeductionDtl2()
                                {
                                    DocNo = Sm.DrStr(dr, c[0]),
                                    DNo = Sm.DrStr(dr, c[1]),
                                    DNo2 = Sm.Right("00" + i.ToString(), 3),
                                    DeductionName = Sm.DrStr(dr, c[2]),
                                    Amt = Sm.DrDec(dr, c[3]),
                                });
                                i++;
                            }
                            else
                            {
                                mDNo = Sm.DrStr(dr, c[1]);
                                i = 1;
                                l.Add(new DeductionDtl2()
                                {
                                    DocNo = Sm.DrStr(dr, c[0]),
                                    DNo = Sm.DrStr(dr, c[1]),
                                    DNo2 = Sm.Right("00" + i.ToString(), 3),
                                    DeductionName = Sm.DrStr(dr, c[2]),
                                    Amt = Sm.DrDec(dr, c[3]),
                                });
                                i++;
                            }
                    }
                }
                dr.Close();
            }

        }

        private MySqlCommand SaveVoucherRequestConsignmentDtl2(DeductionDtl2 x)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblVoucherRequestConsignmentDtl2(DocNo, DNo, DNo2, Deduction, Amt, CreateBy, CreateDt) ");
            SQL.AppendLine("Values (@DocNo, @DNo, @DNo2, @DeductionName, @Amt, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", x.DocNo);
            Sm.CmParam<String>(ref cm, "@DNo", x.DNo);
            Sm.CmParam<String>(ref cm, "@DNo2", x.DNo2);
            Sm.CmParam<String>(ref cm, "@DeductionName", x.DeductionName);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            Sm.CmParam<decimal>(ref cm, "@Amt", x.Amt);

            return cm;
        }

        private string GenerateVoucherRequestDocNo()
        {
            var SQL = new StringBuilder();
            bool IsDocSeqNoEnabled = Sm.GetParameterBoo("IsDocSeqNoEnabled");
            string
                Yr = Sm.GetDte(DteDocDt).Substring(2, 2),
                Mth = Sm.GetDte(DteDocDt).Substring(4, 2),
                DocTitle = Sm.GetParameter("DocTitle"),
                DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='VoucherRequest'"),
                type = string.Empty,
                DocSeqNo = "4";

            if (IsDocSeqNoEnabled) DocSeqNo = Sm.GetParameter("DocSeqNo");

            type = Sm.GetValue("Select AutoNoCredit From TblBankAccount Where BankAcCode = '" + mBankAcCodeForVRConsigne + "' ");

            if (type == string.Empty)
            {
                SQL.Append("Select Concat('" + Yr + "','/', '" + Mth + "', '/', (select ifnull ( ");
                SQL.Append("(Select Right(Concat(Repeat('0', " + DocSeqNo + "), Convert(DocNoTemp+1, Char)), " + DocSeqNo + ") As Numb From ( ");
                SQL.Append("    Select Convert(SUBSTRING(DocNo,7," + DocSeqNo + "), Decimal) As DocNoTemp ");
                SQL.Append("From TblVoucherRequestHdr ");
                //SQL.Append("(Select Right(Concat('0000', Convert(DocNoTemp+1, Char)), 4) As Numb From ( ");
                //SQL.Append("Select Convert(SUBSTRING(DocNo,7,4), Decimal) As DocNoTemp From TblVoucherRequestHdr ");
                SQL.Append("Where Left(DocNo, 5)= Concat('" + Yr + "','/', '" + Mth + "') ");
                SQL.Append("Order By SUBSTRING(DocNo,7," + DocSeqNo + ") Desc Limit 1) As temp ");
                SQL.Append("), Right(Concat(Repeat('0', " + DocSeqNo + "), '1'), " + DocSeqNo + ") ");
                //SQL.Append("), '0001' ");
                SQL.Append(") As Number), '/', '" + DocAbbr + "' ) As DocNo ");
            }
            else
            {
                SQL.Append("Select Concat('" + Yr + "','/', '" + Mth + "', '/', (select ifnull ( ");
                SQL.Append("(Select Right(Concat(Repeat('0', " + DocSeqNo + "), Convert(DocNoTemp+1, Char)), " + DocSeqNo + ") As Numb From ( ");
                SQL.Append("    Select Convert(SUBSTRING(DocNo,7," + DocSeqNo + "), Decimal) As DocNoTemp ");
                SQL.Append("From TblVoucherRequestHdr ");
                //SQL.Append("(Select Right(Concat('0000', Convert(DocNoTemp+1, Char)), 4) As Numb From ( ");
                //SQL.Append("Select Convert(SUBSTRING(DocNo,7,4), Decimal) As DocNoTemp From TblVoucherRequestHdr ");
                SQL.Append("Where Left(DocNo, 5)= Concat('" + Yr + "','/', '" + Mth + "') ");
                SQL.Append("And Right(DocNo, '" + type.Length + "') = '" + type + "' ");
                SQL.Append("Order By SUBSTRING(DocNo,7," + DocSeqNo + ") Desc Limit 1) As temp ");
                SQL.Append("), Right(Concat(Repeat('0', " + DocSeqNo + "), '1'), " + DocSeqNo + ") ");
                //SQL.Append("), '0001' ");
                SQL.Append(") As Number), '/', '" + DocAbbr + "', '/', '" + type + "' ) As DocNo ");
            }
            return Sm.GetValue(SQL.ToString());
        }

        //private string GenerateVoucherRequestDocNo()
        //{
        //    string
        //        Yr = Sm.GetDte(DteDocDt).Substring(2, 2),
        //        Mth = Sm.GetDte(DteDocDt).Substring(4, 2),
        //        DocTitle = Sm.GetParameter("DocTitle"),
        //        DocAbbr = Sm.GetValue("Select DocAbbr From TblDocAbbreviation Where DocType='VoucherRequest'"),
        //        type = string.Empty;

        //    type = Sm.GetValue("Select AutoNoCredit From TblBankAccount Where BankAcCode = '" + mBankAcCodeForVRConsigne + "' ");

        //    var SQL = new StringBuilder();

        //    if (type == string.Empty)
        //    {
        //        SQL.Append("Select Concat('" + Yr + "','/', '" + Mth + "', '/', (select ifnull ( ");
        //        SQL.Append("(Select Right(Concat('0000', Convert(DocNoTemp+1, Char)), 4) As Numb From ( ");
        //        SQL.Append("Select Convert(SUBSTRING(DocNo,7,4), Decimal) As DocNoTemp From TblVoucherRequestHdr ");
        //        SQL.Append("Where Left(DocNo, 5)= Concat('" + Yr + "','/', '" + Mth + "') ");
        //        SQL.Append("Order By SUBSTRING(DocNo,7,5) Desc Limit 1) As temp ");
        //        SQL.Append("), '0001') As Number), '/', '" + DocAbbr + "' ) As DocNo ");
        //    }
        //    else
        //    {
        //        SQL.Append("Select Concat('" + Yr + "','/', '" + Mth + "', '/', (select ifnull ( ");
        //        SQL.Append("(Select Right(Concat('0000', Convert(DocNo+1, Char)), 4) As Numb From ( ");
        //        SQL.Append("Select Convert(SUBSTRING(DocNo,7,4), Decimal) As DocNo From TblVoucherRequestHdr ");
        //        SQL.Append("Where Left(DocNo, 5)= Concat('" + Yr + "','/', '" + Mth + "') ");
        //        SQL.Append("And Right(DocNo, '" + type.Length + "') = '" + type + "' ");
        //        SQL.Append("Order By SUBSTRING(DocNo,7,4) Desc Limit 1) As temp ");
        //        SQL.Append("), '0001') As Number), '/', '" + DocAbbr + "', '/', '" + type + "' ) As DocNo ");
        //    }
        //    return Sm.GetValue(SQL.ToString());
        //}
        
        private MySqlCommand SaveVoucherRequestHdr(string VoucherRequestDocNo, string VoucherRequestConsignmentDocNo)
        {
            string mBankCode = string.Empty;
            mBankCode = Sm.GetValue("Select BankCode From TblBankAccount Where BankAcCode = (Select ParValue From TblParameter Where ParCode = 'BankAcCodeForVRConsigne')");

            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblVoucherRequestHdr ");
            SQL.AppendLine("(DocNo, DocDt, CancelInd, Status, DeptCode, ");
            SQL.AppendLine("DocType, VoucherDocNo, AcType, PaymentType, BankAcCode, BankCode, ");
            SQL.AppendLine("PIC, CurCode, Amt, ");
            SQL.AppendLine("CreateBy, CreateDt) ");

            SQL.AppendLine("Values (@DocNo, @DocDt, 'N', 'O', ");
            SQL.AppendLine("@DeptCode, @DocType, Null, 'C', 'B', @BankAcCode, @BankCode, ");
            SQL.AppendLine("(Select UserCode From TblUser Where UserCode=@CreateBy), ");
            SQL.AppendLine("'IDR', @Amt, ");
            SQL.AppendLine("@CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblDocApproval(DocType, DocNo, DNo, ApprovalDNo, CreateBy, CreateDt) ");
            SQL.AppendLine("Select T.DocType, @VoucherRequestConsignmentDocNo, '001', T.DNo, @CreateBy, CurrentDateTime() ");
            SQL.AppendLine("From TblDocApprovalSetting T ");
            SQL.AppendLine("Where T.DocType='VoucherRequestConsignment' ");
            SQL.AppendLine("And (T.StartAmt=0 ");
            SQL.AppendLine("Or T.StartAmt<=IfNull(( ");
            SQL.AppendLine("    Select A.Amt*IfNull(B.Amt, 1) ");
            SQL.AppendLine("    From TblVoucherRequestHdr A ");
            SQL.AppendLine("    Left Join ( ");
            SQL.AppendLine("        Select B1.CurCode1, B1.Amt ");
            SQL.AppendLine("        From TblCurrencyRate B1 ");
            SQL.AppendLine("        Inner Join ( ");
            SQL.AppendLine("            Select CurCode1, Max(RateDt) RateDt ");
            SQL.AppendLine("            From TblCurrencyRate ");
            SQL.AppendLine("            Where CurCode2=(Select ParValue From TblParameter Where ParCode='MainCurCode') ");
            SQL.AppendLine("            Group By CurCode1 ");
            SQL.AppendLine("        ) B2 On B1.CurCode1=B2.CurCode1 And B1.RateDt=B2.RateDt ");
            SQL.AppendLine("    ) B On A.CurCode=B.CurCode1 ");
            SQL.AppendLine("    Where A.DocNo=@DocNo ");
            SQL.AppendLine("), 0)); ");

            SQL.AppendLine("Update TblVoucherRequestHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@DocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select DocNo From TblDocApproval ");
            SQL.AppendLine("    Where DocType='VoucherRequestConsignment' ");
            SQL.AppendLine("    And DocNo=@VoucherRequestDeductionDocNo ");
            SQL.AppendLine("    ); ");

            SQL.AppendLine("Update TblVoucherRequestConsignmentHdr Set Status='A' ");
            SQL.AppendLine("Where DocNo=@VoucherRequestConsignmentDocNo ");
            SQL.AppendLine("And Not Exists( ");
            SQL.AppendLine("    Select DocNo From TblDocApproval ");
            SQL.AppendLine("    Where DocType='VoucherRequestConsignment' ");
            SQL.AppendLine("    And DocNo=@VoucherRequestConsignmentDocNo ");
            SQL.AppendLine("    ); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };

            Sm.CmParam<String>(ref cm, "@DocNo", VoucherRequestDocNo);
            Sm.CmParam<String>(ref cm, "@DocType", mVoucherDocType);
            Sm.CmParam<String>(ref cm, "@DeptCode", mVoucherRequestConsignmentDeptCode);
            Sm.CmParam<String>(ref cm, "@VoucherRequestConsignmentDocNo", VoucherRequestConsignmentDocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@BankAcCode", mBankAcCodeForVRConsigne);
            Sm.CmParam<String>(ref cm, "@BankCode", mBankCode);
            Sm.CmParam<Decimal>(ref cm, "@Amt", decimal.Parse(TxtAmt.Text));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private MySqlCommand SaveVoucherRequestDtl(string DocNo)
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Insert Into TblVoucherRequestDtl(DocNo, DNo, Description, Amt, Remark, CreateBy, CreateDt) " +
                    "Values (@DocNo, '001', @Description, @Amt, @Remark, @CreateBy, CurrentDateTime()); "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@Description", VRDescription());
            Sm.CmParam<Decimal>(ref cm, "@Amt", Convert.ToDecimal(TxtAmt.Text));
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        private string VRDescription()
        {
            string BillNo = string.Empty;

            if (Grd1.Rows.Count > 1)
            {
                for (int i = 0; i < Grd1.Rows.Count - 1; i++)
                {
                    if (BillNo.Length > 0) BillNo += ", ";
                    BillNo += Sm.GetGrdStr(Grd1, i, 2);
                }
            }

            return "Vendor : " + TxtVdCode.Text + ". PPBK# : " + BillNo;
        }

        #endregion

        #region Edit data

        private void EditData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsEditedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(UpdateVoucherRequestConsignmentHdr());

            if (ChkCancelInd.Checked && TxtJournalDocNo.Text.Length != 0)
                if (mIsAutoJournalActived) cml.Add(SaveJournal2());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsEditedDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDataNotCancelled() ||
                IsDataCancelledAlready() ||
                IsDataProcessedAlready();
        }

        private bool IsDataNotCancelled()
        {
            if (!ChkCancelInd.Checked)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to cancel this data.");
                return true;
            }
            return false;
        }

        private bool IsDataCancelledAlready()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select DocNo From TblVoucherRequestConsignmentHdr " +
                    "Where (CancelInd='Y' Or Status='C') And DocNo=@DocNo "
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data already cancelled.");
                return true;
            }
            return false;
        }

        private bool IsDataProcessedAlready()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo ");
            SQL.AppendLine("From TblVoucherRequestHdr A ");
            SQL.AppendLine("Inner join TblVoucherHdr B On A.VoucherDocNo = B.DocNo ");
            SQL.AppendLine("    And B.CancelInd = 'N' ");
            SQL.AppendLine("    And A.DocNo = @DocNo; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtVoucherRequestDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "Data already processed into voucher.");
                return true;
            }

            return false;
        }

        private MySqlCommand UpdateVoucherRequestConsignmentHdr()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblVoucherRequestConsignmentHdr Set ");
            SQL.AppendLine("    CancelReason=@CancelReason, CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N' And Status<>'C'; ");

            SQL.AppendLine("Update TblVoucherRequestHdr Set ");
            SQL.AppendLine("    CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@VoucherRequestDocNo And CancelInd='N' And Status<>'C'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@VoucherRequestDocNo", TxtVoucherRequestDocNo.Text);
            Sm.CmParam<String>(ref cm, "@CancelReason", MeeCancelReason.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                ClearData();
                ShowVoucherRequestConsignmentHdr(DocNo);
                ShowVoucherRequestConsignmentDtl(DocNo);
                ShowVoucherRequestConsignmentDtl2(DocNo);
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowVoucherRequestConsignmentHdr(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.WhsCode, C.VdName, ");
            SQL.AppendLine("A.Amt, A.CancelReason, A.CancelInd, ");
            SQL.AppendLine("D.DocNo VoucherRequestDocNo, D.VoucherDocNo, A.JournalDocNo, A.JournalDocNo2 ");
            SQL.AppendLine("From TblVoucherRequestConsignmentHdr A ");
            SQL.AppendLine("Inner Join ");
            SQL.AppendLine("( ");
	        SQL.AppendLine("    Select 'all' WhsCode, 'ALL' WhsName ");
	        SQL.AppendLine("    Union All ");
	        SQL.AppendLine("    Select WhsCode, WhsName From TblWarehouse ");
            SQL.AppendLine(") B On A.WhsCode = B.WhsCode ");
            SQL.AppendLine("Inner Join TblVendor C On A.VdCode = C.VdCode ");
            SQL.AppendLine("Inner Join TblVoucherRequestHdr D On A.VoucherRequestDocNo = D.DocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInCtrl(
                 ref cm, SQL.ToString(),
                 new string[] 
                {
                    //0
                    "DocNo",
                    
                    //1-5
                    "DocDt", "CancelReason", "WhsCode", "VdName", "Amt", 
                    
                    //6-10
                    "CancelInd", "VoucherRequestDocNo", "VoucherDocNo", "JournalDocNo", "JournalDocNo2"
                },
                 (MySqlDataReader dr, int[] c) =>
                 {
                     TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                     Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                     MeeCancelReason.EditValue = Sm.DrStr(dr, c[2]);
                     Sm.SetLue(LueWhsCode, Sm.DrStr(dr, c[3]));
                     TxtVdCode.EditValue = Sm.DrStr(dr, c[4]);
                     TxtAmt.EditValue = Sm.FormatNum(Sm.DrDec(dr, c[5]), 0);
                     ChkCancelInd.Checked = Sm.DrStr(dr, c[6]) == "Y";
                     TxtVoucherRequestDocNo.EditValue = Sm.DrStr(dr, c[7]);
                     TxtVoucherDocNo.EditValue = Sm.DrStr(dr, c[8]);
                     TxtJournalDocNo.EditValue = Sm.DrStr(dr, c[9]);
                     TxtJournalDocNo2.EditValue = Sm.DrStr(dr, c[10]);
                 }, true
             );
        }

        private void ShowVoucherRequestConsignmentDtl(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select A.DocNo, A.BillNo, B.WhsName, A.Mth, A.Yr, A.Amt ");
            SQL.AppendLine("From TblVoucherRequestConsignmentDtl A ");
            SQL.AppendLine("Inner Join TblWarehouse B On A.WhsCode = B.WhsCode ");
            SQL.AppendLine("Where A.DocNo = @DocNo ");
            SQL.AppendLine("Order By A.DNo; ");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd1, ref cm, SQL.ToString(),
                new string[] { "DocNo", "BillNo", "WhsName", "Mth", "Yr", "Amt" },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Grd1.Cells[Row, 0].Value = Row + 1;
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 3, 2);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                }, false, false, true, false
            );

            ComputeDeductionAmt();
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 6 });
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private void ShowVoucherRequestConsignmentDtl2(string DocNo)
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            SQL.AppendLine("Select Deduction, Sum(Amt) Amt ");
            SQL.AppendLine("From TblVoucherRequestConsignmentDtl2 ");
            SQL.AppendLine("Where DocNo = @DocNo ");
            SQL.AppendLine("Group By DNo2 ");
            SQL.AppendLine("Order By DNo2 ");

            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.ShowDataInGrid(
                ref Grd2, ref cm, SQL.ToString(),
                new string[] { "Deduction", "Amt" },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Grd2.Cells[Row, 0].Value = Row + 1;
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 0);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 2, 1);
                }, false, false, true, false
            );
            Sm.FocusGrd(Grd2, 0, 0);
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            #region Grid 1

            Grd1.Cols.Count = 8;
            Grd1.FrozenArea.ColCount = 3;

            Sm.GrdHdrWithColWidth(
                Grd1,
                new string[] 
                {
                    //0
                    "No.",

                    //1-5
                    "",
                    "PPBK#",
                    "Warehouse",
                    "Month Outs",
                    "Year Outs",

                    //6-7
                    "Payable",
                    "Warehouse Code",
                }, new int[]
                {
                    //0
                    50,
                    
                    //1-5
                    20, 150, 180, 80, 80,

                    //6-10
                    160, 80,
                }
            );
            Sm.GrdColButton(Grd1, new int[] { 1 });
            Sm.GrdFormatDec(Grd1, new int[] { 6 }, 0);
            Sm.GrdColReadOnly(Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6, 7 }, true);
            Sm.GrdColInvisible(Grd1, new int[] { 7 }, false);
            Grd1.Cols[7].Move(2);
            Sm.SetGrdProperty(Grd1, false);

            #endregion

            #region Grid 2

            Grd2.Cols.Count = 3;
            Grd2.FrozenArea.ColCount = 3;

            Sm.GrdHdrWithColWidth(
                Grd2,
                new string[] 
                {
                    //0
                    "No.",

                    //1-2
                    "Deduction",
                    "Amount",
                },
                new int[] { 50, 120, 120 }
            );
            Sm.GrdFormatDec(Grd2, new int[] { 2 }, 0);
            Sm.GrdColReadOnly(Grd2, new int[] { 0, 1, 2 }, true);
            Sm.SetGrdProperty(Grd2, false);

            #endregion
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { }, !ChkHideInfoInGrd.Checked);
        }

        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, TxtVdCode, LueWhsCode, MeeCancelReason, 
                        ChkCancelInd, TxtJournalDocNo, TxtJournalDocNo2
                    }, true);
                    BtnVdCode.Enabled = false;
                    Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 3, 4, 5, 6 });
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, LueWhsCode
                    }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1 });
                    mIsInsert = true;
                    BtnVdCode.Enabled = true;
                    ChkCancelInd.Checked = false;
                    ChkCancelInd.Properties.ReadOnly = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> { MeeCancelReason, ChkCancelInd }, false);
                    Sm.GrdColReadOnly(false, true, Grd1, new int[] { 1 });
                    ChkCancelInd.Focus();
                    break;
                default:
                    break;
            }
        }

        private void ClearData()
        {
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, TxtVdCode, LueWhsCode, MeeCancelReason, TxtVoucherRequestDocNo,
                TxtVoucherDocNo, TxtJournalDocNo, TxtJournalDocNo2
            });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtAmt }, 0);
            mVdCode = mVdCodeInternal = string.Empty;
            ChkCancelInd.Checked = false;
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 0);
        }

        #endregion

        #region Grid Method

        override protected void GrdKeyDown(object sender, KeyEventArgs e)
        {
            if (mIsInsert)
            {
                if (BtnSave.Enabled)
                {
                    Sm.GrdRemoveRow(Grd1, e, BtnSave);
                    ComputeDeductionAmt();
                }
            }

        }

        #endregion

        #region Additional Method

        private void GetParameter()
        {
            mVoucherRequestConsignmentDeptCode = Sm.GetParameter("VoucherRequestConsignmentDeptCode");
            mBankAcCodeForVRConsigne = Sm.GetParameter("BankAcCodeForVRConsigne");
            mVoucherCodeFormatType = Sm.GetParameter("VoucherCodeFormatType");
            mDBNamePOS = Sm.GetParameter("DBNamePOS");
            mIsAutoJournalActived = Sm.GetParameterBoo("IsAutoJournalActived");
        }

        internal void ClearGrd()
        {
            Grd1.Rows.Clear();
            Grd2.Rows.Clear();
            Grd1.Rows.Count = 1;
            Grd2.Rows.Count = 1;
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 6 });
            Sm.SetGrdNumValueZero(ref Grd2, 0, new int[] { 2 });
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtAmt }, 0);
            Sm.FocusGrd(Grd1, 0, 0);
        }

        private bool IsVendorEmpty()
        {
            if (TxtVdCode.Text.Length == 0)
            {
                Sm.IsTxtEmpty(TxtVdCode, "Vendor", false);
                return true;
            }

            return false;
        }

        internal string GetBillNo()
        {
            string BillNo = string.Empty;

            if(Grd1.Rows.Count > 1)
            {
                for (int i = 0; i < Grd1.Rows.Count - 1; i++ )
                {
                    if (BillNo.Length > 0) BillNo += ",";
                    BillNo += Sm.GetGrdStr(Grd1, i, 2);
                }
            }

            return BillNo.Length > 0 ? BillNo : "XXX";
        }

        internal void ComputeVRAmt()
        {
            decimal Amt = 0m;
            if (Grd1.Rows.Count > 1)
            {
                for (int i = 0; i < Grd1.Rows.Count - 1; i++)
                {
                    Amt += Sm.GetGrdDec(Grd1, i, 6);
                }
            }

            TxtAmt.EditValue = Sm.FormatNum(Amt, 0);
        }

        internal void ComputeDeductionAmt()
        {
            var l = new List<DeductionDtl>();
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            int Nomor = 1;

            SQL.AppendLine("Select 'Sarana Penjualan' DeductionName, SUM(tDeduction1) DeductionAmt ");
            SQL.AppendLine("From " + mDBNamePOS + ".Tbl_Consignment_Billing Where Find_In_Set(Bill_No, @BillNo)");
            SQL.AppendLine("Union All");
            SQL.AppendLine("Select 'Listrik' DeductionName, SUM(tDeduction2) DeductionAmt ");
            SQL.AppendLine("From " + mDBNamePOS + ".Tbl_Consignment_Billing Where Find_In_Set(Bill_No, @BillNo)");
            SQL.AppendLine("Union All");
            SQL.AppendLine("Select 'Telepon' DeductionName, SUM(tDeduction3) DeductionAmt ");
            SQL.AppendLine("From " + mDBNamePOS + ".Tbl_Consignment_Billing Where Find_In_Set(Bill_No, @BillNo)");
            SQL.AppendLine("Union All");
            SQL.AppendLine("Select 'Credit Card' DeductionName, SUM(tDeduction5) DeductionAmt ");
            SQL.AppendLine("From " + mDBNamePOS + ".Tbl_Consignment_Billing Where Find_In_Set(Bill_No, @BillNo)");
            SQL.AppendLine("Union All");
            SQL.AppendLine("Select 'PUKK / PKBL' DeductionName, SUM(tDeduction6) DeductionAmt ");
            SQL.AppendLine("From " + mDBNamePOS + ".Tbl_Consignment_Billing Where Find_In_Set(Bill_No, @BillNo)");
            SQL.AppendLine("Union All");
            SQL.AppendLine("Select 'Dana Promosi' DeductionName, SUM(tDeduction7) DeductionAmt ");
            SQL.AppendLine("From " + mDBNamePOS + ".Tbl_Consignment_Billing Where Find_In_Set(Bill_No, @BillNo)");
            SQL.AppendLine("Union All");
            SQL.AppendLine("Select 'Pengurangan' DeductionName, SUM(tDeduction8) DeductionAmt ");
            SQL.AppendLine("From " + mDBNamePOS + ".Tbl_Consignment_Billing Where Find_In_Set(Bill_No, @BillNo)");
            SQL.AppendLine("Union All");
            SQL.AppendLine("Select 'Lain-lain' DeductionName, SUM(tDeduction9) DeductionAmt ");
            SQL.AppendLine("From " + mDBNamePOS + ".Tbl_Consignment_Billing Where Find_In_Set(Bill_No, @BillNo)");
            SQL.AppendLine("Union All");
            SQL.AppendLine("Select 'PPN Wapu' DeductionName, SUM(tPPN_Wapu) DeductionAmt ");
            SQL.AppendLine("From " + mDBNamePOS + ".Tbl_Consignment_Billing Where Find_In_Set(Bill_No, @BillNo)");
            SQL.AppendLine("Union All");
            SQL.AppendLine("Select 'PPH Pasal 22' DeductionName, SUM(tPPH22) DeductionAmt ");
            SQL.AppendLine("From " + mDBNamePOS + ".Tbl_Consignment_Billing Where Find_In_Set(Bill_No, @BillNo)");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@BillNo", GetBillNo());
                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[] 
                {
                    "DeductionName", "DeductionAmt",
                });
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        if (Sm.DrDec(dr, c[1]) != 0)
                        {
                            l.Add(new DeductionDtl()
                            {
                                Number = Nomor,
                                DeductionName = Sm.DrStr(dr, c[0]),
                                DeductionAmt = Sm.DrDec(dr, c[1]),
                            });
                            Nomor += 1;
                        }
                    }
                }
                dr.Close();
            }

            Grd2.Rows.Clear();

            foreach(var x in l.OrderBy(o => o.Number))
            {
                Grd2.Rows.Add();
                Grd2.Cells[x.Number - 1, 0].Value = x.Number;
                Grd2.Cells[x.Number - 1, 1].Value = x.DeductionName;
                Grd2.Cells[x.Number - 1, 2].Value = x.DeductionAmt;
            }
        }

        private MySqlCommand SaveJournal(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Set @Row:=0; ");

            SQL.AppendLine("Update TblVoucherRequestConsignmentHdr Set JournalDocNo=@JournalDocNo Where DocNo=@DocNo;");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@JournalDocNo, ");
            SQL.AppendLine("@DocDt, ");
            SQL.AppendLine("Concat('Voucher Request Consignment : ', @DocNo), ");
            SQL.AppendLine("@MenuCode, (Select MenuDesc From TblMenu Where MenuCode=@MenuCode), ");
            SQL.AppendLine("Null, @CreateBy, CurrentDateTime()); ");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, Right(Concat('00', Cast((@row:=@row+1) As Char(3))), 3) As DNo, ");
            SQL.AppendLine("B.AcNo, B.DAmt, B.CAMt, Null As Remark, @CreateBy As CreateBy, CurrentDateTime() As CreateDt ");
            SQL.AppendLine("From TblJournalHdr A ");
            SQL.AppendLine("Inner Join ( ");


            SQL.AppendLine("    Select AcNo, Sum(DAmt) DAmt, Sum(CAmt) CAmt From ( ");

            SQL.AppendLine("        Select B.ParValue As AcNo, ");
            SQL.AppendLine("        A.Amt As DAmt, ");
            SQL.AppendLine("        0.00 As CAmt  ");
            SQL.AppendLine("        From TblVoucherRequestConsignmentHdr A  ");
            SQL.AppendLine("        Inner Join tblparameter B On B.ParCode='AcNoForPOSConsignment' And B.ParValue Is Not Null ");
            SQL.AppendLine("        Where A.DocNo=@DocNo  ");

            SQL.AppendLine("        Union All ");

            SQL.AppendLine("        Select CONCAT(B.ParValue, A.VdCode) As AcNo, ");
            SQL.AppendLine("        0.00 As DAmt, ");
            SQL.AppendLine("        A.Amt As CAmt ");
            SQL.AppendLine("        From TblVoucherRequestConsignmentHdr A ");
            SQL.AppendLine("        Inner Join tblparameter B On B.ParCode='VendorAcNoAP' And B.ParValue Is Not Null ");
            SQL.AppendLine("        Where A.DocNo=@DocNo  ");

            SQL.AppendLine("    ) Tbl ");
            SQL.AppendLine("    Where AcNo Is Not Null ");
            SQL.AppendLine("    Group By AcNo ");
            SQL.AppendLine(") B On 1=1 ");
            SQL.AppendLine("Where A.DocNo In ( ");
            SQL.AppendLine("    Select JournalDocNo ");
            SQL.AppendLine("    From TblVoucherRequestConsignmentHdr ");
            SQL.AppendLine("    Where DocNo=@DocNo ");
            SQL.AppendLine("    And JournalDocNo Is Not Null ");
            SQL.AppendLine("    ); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            string Journal = Sm.GenerateDocNoJournal(Sm.GetDte(DteDocDt), 1);
            Sm.CmParam<String>(ref cm, "@JournalDocNo", Journal);
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);
            Sm.CmParam<String>(ref cm, "@MenuCode", mMenuCode);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));

            return cm;
        }

        private MySqlCommand SaveJournal2()
        {
            var SQL = new StringBuilder();
            var CurrentDt = Sm.ServerCurrentDate();
            var DocDt = Sm.GetDte(DteDocDt);
            var IsClosingJournalUseCurrentDt = Sm.IsClosingJournalUseCurrentDt(DocDt);

            SQL.AppendLine("Update TblVoucherRequestConsignmentHdr Set JournalDocNo2=@JournalDocNo Where DocNo=@DocNo And CancelInd='Y';");

            SQL.AppendLine("Insert Into TblJournalHdr(DocNo, DocDt, JnDesc, MenuCode, MenuDesc, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, ");
            if (IsClosingJournalUseCurrentDt)
                SQL.AppendLine("Replace(CurDate(), '-', ''), ");
            else
                SQL.AppendLine("DocDt, ");
            SQL.AppendLine("Concat('Cancelling ', JnDesc) As JnDesc, ");
            SQL.AppendLine("MenuCode, MenuDesc, Remark, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblJournalHdr Where DocNo In (Select JournalDocNo From TblVoucherRequestConsignmentHdr Where DocNo=@DocNo And CancelInd='Y');");

            SQL.AppendLine("Insert Into TblJournalDtl(DocNo, DNo, AcNo, DAmt, CAmt, CreateBy, CreateDt) ");
            SQL.AppendLine("Select @JournalDocNo, DNo, AcNo, CAMt As DAmt, DAmt As CAmt, @UserCode, CurrentDateTime() ");
            SQL.AppendLine("From TblJournalDtl Where DocNo In (Select JournalDocNo From TblVoucherRequestConsignmentHdr Where DocNo=@DocNo And CancelInd='Y');");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            if (IsClosingJournalUseCurrentDt)
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(CurrentDt, 1));
            else
                Sm.CmParam<String>(ref cm, "@JournalDocNo", Sm.GenerateDocNoJournal(DocDt, 1));
               
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
            return cm;
        }


        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void BtnVdCode_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmVoucherRequestConsignmentDlg(this));
        }

        private void LueWhsCode_EditValueChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.RefreshLookUpEdit(LueWhsCode, new Sm.RefreshLue2(SetLueWhsCode), string.Empty);
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtAmt }, 0);
            ClearGrd();
        }

        private void TxtVdCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.SetControlNumValueZero(new List<DXE.TextEdit> { TxtAmt }, 0);
            ClearGrd();
        }

        private void BtnVoucherRequestDocNo_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtVoucherRequestDocNo, "Voucher Request#", false))
            {
                var f = new FrmVoucherRequest(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtVoucherRequestDocNo.Text;
                f.ShowDialog();
            }
        }

        private void BtnVoucherDocNo_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtVoucherDocNo, "Voucher#", false))
            {
                var f = new FrmVoucher(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtVoucherDocNo.Text;
                f.ShowDialog();
            }
        }
        private void BtnJournalDocNo_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtJournalDocNo, "Journal#", false))
            {
                var f = new FrmJournal(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtJournalDocNo.Text;
                f.ShowDialog();
            }
        }

        private void BtnJournalDocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtJournalDocNo2, "Journal#", false))
            {
                var f = new FrmJournal(mMenuCode);
                f.Tag = mMenuCode;
                f.WindowState = FormWindowState.Normal;
                f.StartPosition = FormStartPosition.CenterScreen;
                f.mDocNo = TxtJournalDocNo2.Text;
                f.ShowDialog();
            }
        }
        private void MeeCancelReason_Validated(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.MeeCancelReasonValidated(MeeCancelReason, ChkCancelInd);
        }

        private void ChkCancelInd_CheckedChanged(object sender, EventArgs e)
        {
            if (BtnSave.Enabled) Sm.ChkCancelIndCheckedChanged(BtnSave, MeeCancelReason, ChkCancelInd);
        }

        #endregion

        #region Grid Control Event

        private void Grd1_EllipsisButtonClick(object sender, iGEllipsisButtonClickEventArgs e)
        {
            if (IsVendorEmpty()) return;
            if (e.ColIndex == 1)
                Sm.FormShowDialog(new FrmVoucherRequestConsignmentDlg2(this));
        }

        #endregion

        #endregion

        #region Class

        class DeductionDtl
        {
            public int Number { set; get; }
            public string DeductionName { set; get; }
            public decimal DeductionAmt { set; get; }
        }

        class DeductionDtl2
        {
            public string DocNo { set; get; }
            public string DNo { set; get; }
            public string DNo2 { set; get; }
            public string DeductionName { set; get; }
            public decimal Amt { set; get; }
        }

        #endregion      
        
    }
}
