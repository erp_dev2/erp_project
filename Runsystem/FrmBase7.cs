﻿#region Update
/*
    22/11/2019 [WED/ALL] ganti logo icon ke RS.ico
 */
#endregion

#region Namespace

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Drawing;

using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;

#endregion

namespace RunSystem
{
    public partial class FrmBase7 : Form
    {
        #region Constructor

        public FrmBase7()
        {
            InitializeComponent();
        }

        #endregion

        #region Method

        #region Standard Method

        virtual protected void SetSQL()
        {
            
        }

        virtual protected void ShowData()
        {

        }

        virtual protected void SaveData()
        {

        }

        virtual protected void HideInfoInGrd()
        {

        }

        #endregion

        #region Form Method

        virtual protected void FrmLoad(object sender, EventArgs e)
        {
            Sm.SetLookUpEdit(LueFontSize, new string[] { "6", "7", "8", "9", "10", "11", "12" });
            Sm.SetLue(LueFontSize, "9");
        }

        virtual protected void FrmClosing(object sender, EventArgs e)
        {

        }

        #endregion

        #region Button Method

        virtual protected void BtnRefreshClick(object sender, EventArgs e)
        {
            ShowData();
        }

        virtual protected void BtnSaveClick(object sender, EventArgs e)
        {
            SaveData();
        }

        #endregion

        #endregion

        #region Event

        #region Form Event

        private void FrmBase7_Load(object sender, EventArgs e)
        {
            FrmLoad(sender, e);
        }

        private void FrmBase7_FormClosing(object sender, FormClosingEventArgs e)
        {
            FrmClosing(sender, e);
        }

        #endregion

        #region Button Event

        private void BtnRefresh_Click(object sender, EventArgs e)
        {
            BtnRefreshClick(sender, e);
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            BtnSaveClick(sender, e);
        }


        #endregion

        #region Misc Event

        private void LueFontSize_EditValueChanged(object sender, EventArgs e)
        {
            
        }

        private void ChkHideInfoInGrd_CheckedChanged(object sender, EventArgs e)
        {
            HideInfoInGrd();
        }

        #endregion

        

        #endregion
       
    }
}
