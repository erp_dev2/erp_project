﻿#region Update
/*
    30/08/2017 [TKG] tambah panjang karakter menjadi 30 untuk local document.
*/ 
#endregion

#region Namespace

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;

using FastReport;
using FastReport.Data;

#endregion

namespace RunSystem
{
    public partial class FrmDKO : RunSystem.FrmBase3
    {
        #region Field, Property

        internal string
            mMenuCode = string.Empty, mAccessInd = string.Empty,
            mDocNo = string.Empty, //if this application is called from other application
            mCtCode = string.Empty, mSectionNo = string.Empty;
        internal FrmDKOFind FrmFind;
        //private string mDocType = "22";
       
        #endregion

        #region Constructor

        public FrmDKO(string MenuCode)
        {
            InitializeComponent();
            mMenuCode = MenuCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                if (this.Text.Length == 0) this.Text = "Daftar Kayu Olahan";
                mAccessInd = Sm.SetFormAccessInd(mMenuCode);
                Sm.ButtonVisible(mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

                SetGrd();
                SetLuePbsCode(ref LuePbsCode);
                base.FrmLoad(sender, e);

                SetFormControl(mState.View);
                //if this application is called from other application
                if (mDocNo.Length != 0)
                {
                    ShowData(mDocNo);
                    BtnFind.Visible = BtnInsert.Visible = BtnEdit.Visible =
                    BtnSave.Visible = BtnDelete.Visible = BtnCancel.Visible =
                    BtnPrint.Visible = false;
                }
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void FrmClosing(object sender, FormClosingEventArgs e)
        {
            //if (FrmFind != null) FrmFind.Close();
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
          
            #region Grid 1

            Grd1.Cols.Count = 8;
            Sm.GrdHdrWithColWidth(
                    Grd1,
                    new string[] 
                    {
                        //0
                        "Item Code",
                        
                        //1-5
                        "Item's" + Environment.NewLine + "Local Code",
                        "Item's" + Environment.NewLine + "Name",
                        "",
                        "Quantity",
                        "Uom (Sales)",
                        
                        //6-10
                        "Quantity",
                        "Uom " + Environment.NewLine + "(Inventory)",
                        
                    },
                     new int[] 
                    {
                        //0
                        80,

                        //1-5
                        150, 250, 20, 100, 100, 
                        
                        //6-10
                        100, 100
                    }
                );
            Sm.GrdFormatDec(Grd1, new int[] { 4, 6 }, 0);
            Sm.GrdColButton(Grd1, new int[] { 3 });
            Sm.GrdColInvisible(Grd1, new int[] { 1,3 }, false);
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 1, 2, 4, 5, 6, 7 });

            #endregion

        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 1, 3 }, !ChkHideInfoInGrd.Checked);
        }


        private void SetFormControl(RunSystem.mState state)
        {
            Sm.ButtonReadOnly(state, mAccessInd, ref BtnFind, ref BtnInsert, ref BtnEdit, ref BtnDelete, ref BtnSave, ref BtnCancel, ref BtnPrint);

            switch (state)
            {
                case mState.View:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        TxtDocNo, DteDocDt, TxtCtCode, TxtCnt, TxtDRDocNo, TxtFakoDocNo, TxtLocalDocNo,
                        TxtPLDocNo,TxtSeal, TxtTT, TxtPlatNo, TxtDestination, TxtPort, ChkCancelInd, LuePbsCode,
                        MeeRemark
                    }, true);
                    BtnDRDocNo.Enabled = false;
                    BtnPLDocNo.Enabled = false;
                    TxtDocNo.Focus();
                    break;
                case mState.Insert:
                    Sm.SetControlReadOnly(new List<DXE.BaseEdit> 
                    { 
                        DteDocDt, TxtLocalDocNo, TxtDestination, LuePbsCode, 
                        MeeRemark
                    }, false);
                    BtnDRDocNo.Enabled = true;
                    BtnPLDocNo.Enabled = true;
                    DteDocDt.Focus();
                    break;
                case mState.Edit:
                    TxtDocNo.Focus();
                    ChkCancelInd.Properties.ReadOnly = false;
                    break;
            }
        }

        private void ClearData()
        {
            mCtCode = string.Empty;
            mSectionNo = string.Empty;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtDocNo, DteDocDt, TxtDRDocNo, TxtPLDocNo, TxtCnt, TxtLocalDocNo,
                TxtPlatNo, TxtFakoDocNo, TxtTT, TxtPort, LuePbsCode,
                TxtSeal, TxtCtCode, TxtDestination, MeeRemark
            });
            ChkCancelInd.Checked = false;
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 1);
        }

        internal void ClearData2()
        {
            mCtCode = string.Empty;
            mSectionNo = string.Empty;
            Sm.SetControlEditValueNull(new List<DXE.BaseEdit> 
            {
                TxtPlatNo, TxtFakoDocNo, TxtTT, TxtPort,
                TxtSeal, TxtCtCode, TxtDestination, MeeRemark
            });
            ClearGrd();
            Sm.FocusGrd(Grd1, 0, 1);
        }

        internal void ClearGrd()
        {
          
            Sm.ClearGrd(Grd1, true);
            Sm.SetGrdNumValueZero(ref Grd1, 0, new int[] { 4, 6 });

        }

       
        #endregion

        #region Button Method

        override protected void BtnFindClick(object sender, EventArgs e)
        {
            if (FrmFind == null) FrmFind = new FrmDKOFind(this);
            Sm.SetFormFindSetting(this, FrmFind);
        }

        override protected void BtnInsertClick(object sender, EventArgs e)
        {
            try
            {
                ClearData();
                SetFormControl(mState.Insert);
                Sm.SetDteCurrentDate(DteDocDt);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        override protected void BtnEditClick(object sender, EventArgs e)
        {
            if (Sm.IsTxtEmpty(TxtDocNo, "", false)) return;
            SetFormControl(mState.Edit);
        }

        override protected void BtnSaveClick(object sender, EventArgs e)
        {
            try
            {
                if (TxtDocNo.Text.Length == 0)
                    InsertData(sender, e);
                else
                    CancelData();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        override protected void BtnCancelClick(object sender, EventArgs e)
        {
            ClearData();
            SetFormControl(mState.View);
        }

        override protected void BtnPrintClick(object sender, EventArgs e)
        {
            try
            {
                ParPrint(TxtDocNo.Text);
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Save Data

        #region Insert Data

        private void InsertData(object sender, EventArgs e)
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsInsertedDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            string DocNo = Sm.GenerateDocNo(Sm.GetDte(DteDocDt), "DKO", "TblDKO");

            var cml = new List<MySqlCommand>();

            cml.Add(SaveDKO(DocNo));


            Sm.ExecCommands(cml);

            ShowData(DocNo);
        }

        private bool IsInsertedDataNotValid()
        {
            return
                Sm.IsDteEmpty(DteDocDt, "Date") ||
                IsDocumentEmpty() ||
                Sm.IsLueEmpty(LuePbsCode, "Publisher")||
                IsGrdEmpty() ||
                IsDRNotValid() ||
                IsPLNotValid();
        }

       

        private bool IsDocumentEmpty()
        {
            if (TxtDRDocNo.Text.Length == 0 && TxtPLDocNo.Text.Length == 0)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 document (Delivery request or packing list).");
                return true;
            }
            return false;
        }

        private bool IsGrdEmpty()
        {
            if (Grd1.Rows.Count == 1)
            {
                Sm.StdMsg(mMsgType.Warning, "You need to input at least 1 item.");
                return true;
            }
            return false;
        }

       

 

        private bool IsDRNotValid()
        {
            return (TxtDRDocNo.Text.Length == 0) ?
                false :
                IsDRAlreadyCancelled() ||
                IsDRAlreadyFulfilled();
        }

        private bool IsDRAlreadyCancelled()
        {
            return IsDataExists(
                "Select 1 From TblDRHdr " +
                "Where CancelInd='Y' And DocNo=@Param;",
                TxtDRDocNo.Text,
                "This request delivery already cancelled."
                );
        }

        private bool IsDRAlreadyFulfilled()
        {
            return IsDataExists(
                "Select 1 From TblDRHdr " +
                "Where IfNull(ProcessInd, 'O') In ('F', 'M') And DocNo=@Param; ",
                TxtDRDocNo.Text,
                "This request delivery already fulfilled."
                );
        }

        private bool IsPLNotValid()
        {
            return (TxtPLDocNo.Text.Length == 0) ?
                false :
                IsSPStatusNotValid() 
                ;
        }

       private bool IsSPStatusNotValid()
        {
            return IsDataExists(
                "Select 1 From TblPLHdr A " +
                "Inner Join TblSIHdr B On A.SIDocNo=B.DocNo " +
                "Inner Join TblSP C On B.SPDocNo=C.DocNo And IfNull(C.Status, '')<>'R' " +
                "Where A.Docno=@DocNo;",
                TxtPLDocNo.Text,
                "This packing list's planning status is not valid."
                );
        }

   
        private MySqlCommand SaveDKO(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Insert Into TblDKO(DocNo, DocDt, LocalDocNo, PbsCode, DRDocNo, PLDocNo, SectionNo, Cnt, Seal, CtCode, Destination, Remark, CreateBy, CreateDt) ");
            SQL.AppendLine("Values(@DocNo, @DocDt, @LocalDocno, @PbsCode, @DRDocNo, @PLDocNo, @SectionNo, @Cnt, @Seal, @CtCode, @Destination, @Remark, @CreateBy, CurrentDateTime()); ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
            Sm.CmParamDt(ref cm, "@DocDt", Sm.GetDte(DteDocDt));
            Sm.CmParam<String>(ref cm, "@LocalDocNo", TxtLocalDocNo.Text);
            Sm.CmParam<String>(ref cm, "@PbsCode", Sm.GetLue(LuePbsCode));
            Sm.CmParam<String>(ref cm, "@DRDocNo", TxtDRDocNo.Text);
            Sm.CmParam<String>(ref cm, "@PLDocNo", TxtPLDocNo.Text);
            Sm.CmParam<String>(ref cm, "@SectionNo", mSectionNo);
            Sm.CmParam<String>(ref cm, "@Cnt", TxtCnt.Text);
            Sm.CmParam<String>(ref cm, "@Seal", TxtSeal.Text);
            Sm.CmParam<String>(ref cm, "@CtCode", mCtCode);
            Sm.CmParam<String>(ref cm, "@Destination", TxtDestination.Text);
            Sm.CmParam<String>(ref cm, "@Remark", MeeRemark.Text);
            Sm.CmParam<String>(ref cm, "@CreateBy", Gv.CurrentUserCode);

            return cm;
        }

        #endregion

        #region Cancel Data

        private void CancelData()
        {
            if (Sm.StdMsgYN("Save", string.Empty, mMenuCode) == DialogResult.No || IsCancelledDataNotValid()) return;

            Cursor.Current = Cursors.WaitCursor;

            var cml = new List<MySqlCommand>();

            cml.Add(CancelDKO());

            Sm.ExecCommands(cml);

            ShowData(TxtDocNo.Text);
        }

        private bool IsCancelledDataNotValid()
        {
            return
                Sm.IsTxtEmpty(TxtDocNo, "Document#", false) ||
                IsDataCancelledAlready();
        }

     
        private bool IsDataCancelledAlready()
        {
            var cm = new MySqlCommand()
            {
                CommandText =
                    "Select 1 From TblDKO " +
                    "Where CancelInd='Y' And DocNo=@DocNo;"
            };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);

            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, "This data already cancelled.");
                return true;
            }
            return false;
        }

        private MySqlCommand CancelDKO()
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Update TblDKO Set ");
            SQL.AppendLine("    CancelInd='Y', LastUpBy=@UserCode, LastUpDt=CurrentDateTime() ");
            SQL.AppendLine("Where DocNo=@DocNo And CancelInd='N'; ");

            var cm = new MySqlCommand() { CommandText = SQL.ToString() };
            Sm.CmParam<String>(ref cm, "@DocNo", TxtDocNo.Text);
            Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);

            return cm;
        }


        #endregion

        #endregion

        #region  Show Data

        public void ShowData(string DocNo)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ClearData();
                ShowDKO(DocNo);
                ShowDataPLDR();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
            finally
            {
                SetFormControl(mState.View);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ShowDKO(string DocNo)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.DocNo, A.DocDt, A.LocalDocNo, A.DRDocNo, A.PLDocNo, A.PbsCode, ");
            SQL.AppendLine("A.CtCode, B.CtName, A.SectionNo, A.Cnt, A.Seal, A.Destination, D.TTName, C.ExpPlatNo, A.CancelInd, A.FakoDocNo, A.Remark ");
            SQL.AppendLine("From TblDKO A ");
            SQL.AppendLine("Inner Join TblCustomer B On A.CtCode=B.CtCode  ");
            SQL.AppendLine("Left Join TblDRHdr C On A.DRDocNo=C.DocNo ");
            SQL.AppendLine("Left join TbltransportType D On D.TTCode = C.ExpTTCode ");
            SQL.AppendLine("Left join tblFako E On A.DocNo = E.DkoDocNo ");
            SQL.AppendLine("Where A.DocNo=@DocNo;");

            var cm = new MySqlCommand();
            Sm.CmParam<String>(ref cm, "@DocNo", DocNo);

            Sm.ShowDataInCtrl(
                    ref cm, SQL.ToString(),
                    new string[] 
                    { 
                        //0
                        "DocNo", 

                        //1-5
                        "DocDt", "DRDocNo", "PLDocNo", "CtCode", "CtName", 
                        
                        //6-10
                        "SectionNo", "Cnt", "Seal", "Destination", "TTName", 
                        
                        //11-15
                        "ExpPlatNo", "PbsCode", "LocalDocNo", "cancelInd", "FakoDocNo", 
                        
                        //16
                        "Remark" 
                    },
                    (MySqlDataReader dr, int[] c) =>
                    {
                        TxtDocNo.EditValue = Sm.DrStr(dr, c[0]);
                        Sm.SetDte(DteDocDt, Sm.DrStr(dr, c[1]));
                        TxtDRDocNo.EditValue = Sm.DrStr(dr, c[2]);
                        TxtPLDocNo.EditValue = Sm.DrStr(dr, c[3]);
                        mCtCode = Sm.DrStr(dr, c[4]);
                        TxtCtCode.EditValue = Sm.DrStr(dr, c[5]);
                        mSectionNo = Sm.DrStr(dr, c[6]);
                        TxtCnt.EditValue = Sm.DrStr(dr, c[7]);
                        TxtSeal.EditValue = Sm.DrStr(dr, c[8]);
                        TxtDestination.EditValue = Sm.DrStr(dr, c[9]);
                        TxtTT.EditValue = Sm.DrStr(dr, c[10]);
                        TxtPlatNo.EditValue = Sm.DrStr(dr, c[11]);
                        Sm.SetLue(LuePbsCode, Sm.DrStr(dr, c[12]));
                        TxtLocalDocNo.EditValue = Sm.DrStr(dr, c[13]);
                        ChkCancelInd.Checked = Sm.CompareStr(Sm.DrStr(dr, c[14]), "Y");
                        TxtFakoDocNo.EditValue = Sm.DrStr(dr, c[15]);
                        MeeRemark.EditValue = Sm.DrStr(dr, c[16]);
                        if (TxtPLDocNo.Text.Length > 0)
                        {
                            TxtPort.EditValue = Sm.GetValue("Select D.Portname  from "+
                                            "TblPlhdr A "+
                                            "Inner Join tblSIHdr B On A.SIDocNo = B.DocNo "+
                                            "Inner Join TblSP C On B.SPDocNo = C.DocNo "+
                                            "Left Join TblPort D On C.PortCode1 = D.PortCode "+
                                            "Where A.Docno = '"+TxtPLDocNo.Text+"' ");
                        }
                    }, true
                );
        }

        #endregion

        #region Additional Method

      
        public static void SetLuePbsCode(ref LookUpEdit Lue)
        {
            Sm.SetLue2(
                ref Lue,
                "Select A.PbsCode As Col1, B.Empname As Col2  fROM TBLpublisher A "+
                "Inner Join TblEMployee B on A.pbsEmpCode = B.EmpCOde "+
                "Where A.Actind = 'Y' ",
                0, 35, false, true, "Code", "Publisher Name", "Col2", "Col1");
        }


        private bool IsDataExists(string SQL, string Param, string Warning)
        {
            var cm = new MySqlCommand() { CommandText = SQL };
            Sm.CmParam<String>(ref cm, "@Param", Param);
            if (Sm.IsDataExist(cm))
            {
                Sm.StdMsg(mMsgType.Warning, Warning);
                return true;
            }
            return false;
        }

        private void SetLuePropertyCode(ref DXE.LookUpEdit Lue, string ItCode)
        {
            var SQL = new StringBuilder();

            SQL.AppendLine("Select A.PropCode As Col1, B.PropName As Col2 ");
            SQL.AppendLine("From TblItemProperty A ");
            SQL.AppendLine("Inner Join TblProperty B On A.PropCode = B.PropCode ");
            SQL.AppendLine("Where ItCode = '" + ItCode + "'  ");

            Sm.SetLue2(
                ref Lue, SQL.ToString(),
                0, 35, false, true, "Code", "Name", "Col2", "Col1");
        }

        internal void ShowDataPLDR()
        {
            var SQL = new StringBuilder();
            var cm = new MySqlCommand();

            if (TxtDRDocNo.Text.Length > 0)
            {
                SQL.AppendLine("Select A.DocNo, A2.Dno, E.ItCode, F.ItCodeInternal, F.ItName, ");
                SQL.AppendLine("Round(A2.Qty, 4) As Qty, G.PriceUomCode, Round(A2.QtyInventory, 4) As QtyInventory, F.InventoryUomCode ");
                SQL.AppendLine("From TblDRhdr A  ");
                SQL.AppendLine("Inner Join TblDRDtl A2 On A.DocNo = A2.DocNo  ");
                SQL.AppendLine("Inner Join TblSOHdr B On A2.SODocNo=B.DocNo  "); 
                SQL.AppendLine("Inner Join TblSODtl C On A2.SODocNo=C.DocNo And A2.SODNo=C.DNo ");
                SQL.AppendLine("Inner Join TblCtQtDtl D On B.CtQtDocNo=D.DocNo And C.CtQtDNo=D.DNo ");
                SQL.AppendLine("Inner Join TblItemPriceDtl E On D.ItemPriceDocNo=E.DocNo And D.ItemPriceDNo=E.DNo  ");
                SQL.AppendLine("Inner Join TblItem F On E.ItCode=F.ItCode ");
                SQL.AppendLine("Inner Join TblItemPriceHdr G On D.ItemPriceDocNo = G.DocNo ");
                SQL.AppendLine("Left Join TblItemPackagingUnit I On C.PackagingUnitUomCode = I.UomCOde And E.ItCode = I.ItCode");
                SQL.AppendLine("Where A.DocNo=@DocNo ");


                Sm.CmParam<String>(ref cm, "@DocNo", TxtDRDocNo.Text);
            }

            if (TxtPLDocNo.Text.Length > 0)
            {
                SQL.AppendLine("Select A.DocNo, A2.Dno, A2.ItCode, B.ItCodeInternal, B.ItName, ");
                SQL.AppendLine("Round(A2.Qty, 4)As Qty, G.PriceUomCode, Round(A2.QtyInventory, 4) As QtyInventory, B.InventoryUomCode ");
                SQL.AppendLine("From TblPlhdr A  ");
                SQL.AppendLine("Inner Join TblPlDtl A2 On A.DocNo = A2.DocNo  "); 
                SQL.AppendLine("Inner Join TblItem B On A2.ItCode = B.ItCode "); 
                SQL.AppendLine("Inner Join TblSODtl C On A2.SoDocNo = C.DocNo And A2.SoDno = C.Dno  ");
                SQL.AppendLine("Inner Join TblSOHdr D On C.DocNo = D.DocNo And A2.SODocNo = D.DocNo   ");
                SQL.AppendLine("Inner Join TblCtQtDtl F On D.CtQtDocNo = F.DocNo And C.CtQtDNo = F.Dno  ");
                SQL.AppendLine("Inner Join TblCtQtHdr F2 On D.CtQtDocNo = F2.DocNo  ");
                SQL.AppendLine("Inner Join TblUser F3 On F2.SpCode = F3.UserCode  ");
                SQL.AppendLine("Inner Join TblItemPriceHdr G On F.ItemPriceDocNo = G.DocNo  ");
                SQL.AppendLine("Left Join TblItemPackagingUnit I On C.PackagingUnitUomCode = I.UomCOde And A2.ItCode = I.ItCode  ");
                SQL.AppendLine("Where A.DocNo=@DocNo And A2.SectionNo=@SectionNo ");
                SQL.AppendLine("Order By A2.ItCode, A2.DNo;");

                Sm.CmParam<String>(ref cm, "@SectionNo", mSectionNo);
                Sm.CmParam<String>(ref cm, "@DocNo", TxtPLDocNo.Text);
            }


            Sm.ShowDataInGrid(
                ref Grd1, ref cm,
                SQL.ToString(),
                new string[] 
                { 
                    //0
                    "ItCode", 
                    
                    "ItCodeInternal", "ItName", "Qty", "PriceUomCode", "QtyInventory", 
                    
                    "InventoryUomCode"
                },
                (MySqlDataReader dr, iGrid Grd, int[] c, int Row) =>
                {
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 0, 0);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 1, 1);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 2, 2);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 4, 3);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 5, 4);
                    Sm.SetGrdValue("N", Grd, dr, c, Row, 6, 5);
                    Sm.SetGrdValue("S", Grd, dr, c, Row, 7, 6);
                }, false, false, true, false
            );
            Sm.SetGrdNumValueZero(ref Grd1, Grd1.Rows.Count - 1, new int[] { 4,6 });
            Sm.FocusGrd(Grd1, 0, 1);
        }

        private void ParPrint(string DocNo)
        {
            var l = new List<DKOHdr>();
            var ldtl = new List<DKODtl>();
            string[] TableName = { "DKOHdr", "DKODtl" };
            List<IList> myLists = new List<IList>();
            var cm = new MySqlCommand();

            #region Header
            var SQL = new StringBuilder();

            SQL.AppendLine("Select Distinct @CompanyLogo As CompanyLogo, (Select ParValue From TblParameter Where Parcode='ReportTitle1') As 'CompanyName', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle2') As 'CompanyAddress', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='ReportTitle3') As 'CompanyPhone', ");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='Shipper1') As 'Shipper1',");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='Shipper2') As 'Shipper2',");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='Shipper3') As 'Shipper3',");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='Shipper4') As 'Shipper4',");
            SQL.AppendLine("(Select ParValue From TblParameter Where Parcode='Shipper5') As 'Shipper5',");
            SQL.AppendLine("A.DocNo, DATE_FORMAT(A.DocDt,'%d %M %Y') As DocDt, A.LocalDocno, A.DrDocNo, A.PLDocNo, A.FakoDocNo,");
            SQL.AppendLine("B.CtName, C.Pbsregisterno, D.EmpName, A.Destination, A.cancelInd, A.remark");
            SQL.AppendLine("From TblDKO A");
            SQL.AppendLine("Inner Join TblCustomer B On A.CtCode= B.CtCode ");
            SQL.AppendLine("Inner Join TblPublisher C On A.PbsCode = C.PbsCode");
            SQL.AppendLine("Inner Join TblEmployee D On C.PbsEmpCode = D.EmpCode");
            SQL.AppendLine("Where A.DocNo=@DocNo; ");

            using (var cn = new MySqlConnection(Gv.ConnectionString))
            {
                cn.Open();
                cm.Connection = cn;
                cm.CommandText = SQL.ToString();
                Sm.CmParam<String>(ref cm, "@DocNo", DocNo);
                Sm.CmParam<String>(ref cm, "@CompanyLogo", @Sm.CompanyLogo());

                var dr = cm.ExecuteReader();
                var c = Sm.GetOrdinal(dr, new string[]
                {
                //0
                "CompanyLogo",
                //1-5
                "CompanyName",
                "CompanyAddress",
                "CompanyPhone",
                "Shipper1",
                "Shipper2",
                //6-10
                "Shipper3",
                "Shipper4",
                "DocNo",
                "DocDt",
                "LocalDocNo",
                //11-15
                "DRDocNo",
                "PLDocNo",
                "FakoDocNo",
                "CtName",
                "Pbsregisterno",
                //16-18
                "EmpName",
                "Destination",
                "remark",
                "CancelInd",
                "Shipper5"
                });

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        l.Add(new DKOHdr()
                        {
                            CompanyLogo = Sm.DrStr(dr, c[0]),
                            CompanyName = Sm.DrStr(dr, c[1]),
                            CompanyAddress = Sm.DrStr(dr, c[2]),
                            CompanyPhone = Sm.DrStr(dr, c[3]),
                            Shipper1 = Sm.DrStr(dr, c[4]),
                            Shipper2 = Sm.DrStr(dr, c[5]),

                            Shipper3 = Sm.DrStr(dr, c[6]),
                            Shipper4 = Sm.DrStr(dr, c[7]),
                            DocNo = Sm.DrStr(dr, c[8]),
                            DocDt = Sm.DrStr(dr, c[9]),
                            LocalDocNo = Sm.DrStr(dr, c[10]),

                            DRDocNo = Sm.DrStr(dr, c[11]),
                            PLDocNo = Sm.DrStr(dr, c[12]),
                            FakoDocNo = Sm.GetValue("Select LocalDocNo from TblFako Where DocNo = '"+Sm.DrStr(dr, c[13])+"' "),
                            CtName = Sm.DrStr(dr, c[14]),
                            Pbsregisterno = Sm.DrStr(dr, c[15]),

                            EmpName = Sm.DrStr(dr, c[16]),
                            Destination = Sm.DrStr(dr, c[17]),
                            remark = Sm.DrStr(dr, c[18]),
                            PrintBy = "Printed By: " + Gv.CurrentUserCode + " on " + String.Format("{0:dd/MMM/yyyy HH:mm}", Sm.ConvertDateTime(Sm.ServerCurrentDateTime())),
                            Port = Sm.GetValue("Select D.Portname  from "+
                            "TblPlhdr A "+
                            "Inner Join tblSIHdr B On A.SIDocNo = B.DocNo "+
                            "Inner Join TblSP C On B.SPDocNo = C.DocNo "+
                            "Left Join TblPort D On C.PortCode1 = D.PortCode "+
                            "Where A.Docno = '"+TxtPLDocNo.Text+"' "),
                            CancelInd = Sm.DrStr(dr, c[19]),
                            Shipper5 = Sm.DrStr(dr, c[20]),
                        });
                    }
                }
                dr.Close();
            }
            myLists.Add(l);

            #endregion

            #region Detail

            var cmDtl = new MySqlCommand();
            var SQLDtl = new StringBuilder();
            using (var cnDtl = new MySqlConnection(Gv.ConnectionString))
            {
                cnDtl.Open();
                cmDtl.Connection = cnDtl;

                if (TxtDRDocNo.Text.Length > 0)
                {
                    SQLDtl.AppendLine("Select X.DocNo, X.Dno, X.ItCode, X.ItCodeInternal, X.ItName, X.height, X.length, X.Width, X.ItGrpName, ");
                    SQLDtl.AppendLine("if(right(X.Qty, 4) = '0000', 0, 1) As QtyInd, X.PriceuomCode, ");
                    SQLDtl.AppendLine("if(right(X.QtyInventory, 4) = '0000', 0, 1) As QtyInventoryInd,  ");
                    SQLDtl.AppendLine("X.InventoryUomCode, if(Right(X.Qty2, 4) = '0000', 0, 1) As Qty2Ind, ");
                    SQLDtl.AppendLine("X.Qty, X.Qty2, X.QtyInventory, X.QtyPackagingUnit, X.PackagingUnitUomCode ");
                    SQLDtl.AppendLine("From ( ");
                    SQLDtl.AppendLine("Select A.DocNo, A2.Dno, E.ItCode, F.ItCodeInternal, F.ItName, F.height, F.length, F.Width, J.ItGrpName, ");
                    SQLDtl.AppendLine("Round(A2.Qty, 4) As Qty, G.PriceUomCode, Round(A2.QtyInventory, 4) As QtyInventory, F.InventoryUomCode, ");
                    SQLDtl.AppendLine("Round(if(I.Qty =0, 0, if(G.PriceUomCode = F.SalesUomCode2, A2.Qty, ((I.Qty2*A2.Qty)/I.Qty))), 4) As Qty2, C.PackagingUnitUomCode As QtyPackagingUnit, C.PackagingUnitUomCode  ");
                    SQLDtl.AppendLine("From TblDRhdr A  ");
                    SQLDtl.AppendLine("Inner Join TblDRDtl A2 On A.DocNo = A2.DocNo  ");
                    SQLDtl.AppendLine("Inner Join TblSOHdr B On A2.SODocNo=B.DocNo  "); 
                    SQLDtl.AppendLine("Inner Join TblSODtl C On A2.SODocNo=C.DocNo And A2.SODNo=C.DNo ");
                    SQLDtl.AppendLine("Inner Join TblCtQtDtl D On B.CtQtDocNo=D.DocNo And C.CtQtDNo=D.DNo ");
                    SQLDtl.AppendLine("Inner Join TblItemPriceDtl E On D.ItemPriceDocNo=E.DocNo And D.ItemPriceDNo=E.DNo  ");
                    SQLDtl.AppendLine("Inner Join TblItem F On E.ItCode=F.ItCode ");
                    SQLDtl.AppendLine("Inner Join TblItemPriceHdr G On D.ItemPriceDocNo = G.DocNo ");
                    SQLDtl.AppendLine("Left Join TblItemPackagingUnit I On C.PackagingUnitUomCode = I.UomCOde And E.ItCode = I.ItCode");
                    SQLDtl.AppendLine("Left Join TblItemGroup J On F.ItGrpCode = J.ItGrpCode ");
                    SQLDtl.AppendLine("Where A.DocNo=@DocNoDR ");
                    SQLDtl.AppendLine(")X ");
                 }

                if (TxtPLDocNo.Text.Length > 0)
                {
                    SQLDtl.AppendLine("Select X.DocNo, X.Dno, X.ItCode, X.ItCodeInternal, X.ItName, X.height, X.length, X.Width, X.ItGrpName, ");
                    SQLDtl.AppendLine("if(right(X.Qty, 4) = '0000', 0, 1) As QtyInd, X.PriceuomCode,  ");
                    SQLDtl.AppendLine("if(right(X.QtyInventory, 4) = '0000', 0, 1) As QtyInventoryInd, "); 
                    SQLDtl.AppendLine("X.InventoryUomCode, if(Right(X.Qty2, 4) = '0000', 0, 1) As Qty2Ind, ");
                    SQLDtl.AppendLine("X.Qty, X.Qty2, X.QtyInventory, X.QtyPackagingUnit, X.PackagingUnitUomCode  "); 
                    SQLDtl.AppendLine("From ( ");
                    SQLDtl.AppendLine("Select A.DocNo, A2.Dno, A2.ItCode, B.ItCodeInternal, B.ItName, B.height, B.length, B.Width, J.ItGrpName, ");
                    SQLDtl.AppendLine("Round(A2.Qty, 4)As Qty, G.PriceUomCode, Round(A2.QtyInventory, 4) As QtyInventory, B.InventoryUomCode, ");
                    SQLDtl.AppendLine("Round(if(I.Qty =0, 0, if(G.PriceUomCode = B.SalesUomCode2, A2.Qty, ((I.Qty2*A2.Qty)/I.Qty))), 4) As Qty2, A2.PlNo As QtyPackagingUnit, C.PackagingUnitUomCode ");
                    SQLDtl.AppendLine("From TblPlhdr A  ");
                    SQLDtl.AppendLine("Inner Join TblPlDtl A2 On A.DocNo = A2.DocNo  ");
                    SQLDtl.AppendLine("Inner Join TblItem B On A2.ItCode = B.ItCode ");
                    SQLDtl.AppendLine("Inner Join TblSODtl C On A2.SoDocNo = C.DocNo And A2.SoDno = C.Dno  ");
                    SQLDtl.AppendLine("Inner Join TblSOHdr D On C.DocNo = D.DocNo And A2.SODocNo = D.DocNo   ");
                    SQLDtl.AppendLine("Inner Join TblCtQtDtl F On D.CtQtDocNo = F.DocNo And C.CtQtDNo = F.Dno  ");
                    SQLDtl.AppendLine("Inner Join TblCtQtHdr F2 On D.CtQtDocNo = F2.DocNo  ");
                    SQLDtl.AppendLine("Inner Join TblUser F3 On F2.SpCode = F3.UserCode  ");
                    SQLDtl.AppendLine("Inner Join TblItemPriceHdr G On F.ItemPriceDocNo = G.DocNo  ");
                    SQLDtl.AppendLine("Left Join TblItemPackagingUnit I On C.PackagingUnitUomCode = I.UomCOde And A2.ItCode = I.ItCode  ");
                    SQLDtl.AppendLine("Left Join TblItemGroup J On B.ItGrpCode = J.ItGrpCode ");
                    SQLDtl.AppendLine("Where A.DocNo=@DocNoPL And A2.SectionNo=@SectionNo ");
                    SQLDtl.AppendLine("Order By A2.ItCode, A2.DNo ");
                    SQLDtl.AppendLine(")X ");
                }
               
                cmDtl.CommandText = SQLDtl.ToString();

                Sm.CmParam<String>(ref cmDtl, "@SectionNo", mSectionNo);
                Sm.CmParam<String>(ref cmDtl, "@DocNoDR", TxtDRDocNo.Text);
                Sm.CmParam<String>(ref cmDtl, "@DocNoPL", TxtPLDocNo.Text);

                var drDtl = cmDtl.ExecuteReader();
                var cDtl = Sm.GetOrdinal(drDtl, new string[]
                {
                    "ItCode" ,

                    "ItCodeInternal" ,
                    "ItName",
                    "ItGrpName",
                    "Height",
                    "Length",
                    
                    "Width",
                    "Qty" ,
                    "Qty2",
                    "QtyInventory",
                    "PriceUomCode",

                    "InventoryUomCode",
                    "QtyInd" ,
                    "Qty2Ind",
                    "QtyInventoryInd",
                    "QtyPackagingUnit",
                    "PackagingUnitUomCode"
                });

                if (drDtl.HasRows)
                {
                    while (drDtl.Read())
                    {
                        ldtl.Add(new DKODtl()
                        {
                            ItCode = Sm.DrStr(drDtl, cDtl[0]),
                            ItCodeInternal = Sm.DrStr(drDtl, cDtl[1]),
                            ItName = Sm.DrStr(drDtl, cDtl[2]),
                            GrpName = Sm.DrStr(drDtl, cDtl[3]),
                            Height = Sm.DrDec(drDtl, cDtl[4]),
                            Length = Sm.DrDec(drDtl, cDtl[5]),
                            Width = Sm.DrDec(drDtl, cDtl[6]),
                            Qty = Sm.DrDec(drDtl, cDtl[7]),
                            Qty2 = Sm.DrDec(drDtl, cDtl[8]),
                            QtyInventory = Sm.DrDec(drDtl, cDtl[9]),
                            PriceUomCode = Sm.DrStr(drDtl, cDtl[10]),
                            InventoryUomCode = Sm.DrStr(drDtl, cDtl[11]),
                            QtyInd = Sm.DrDec(drDtl, cDtl[12]),
                            Qty2Ind = Sm.DrDec(drDtl, cDtl[13]),
                            QtyInventoryInd = Sm.DrDec(drDtl, cDtl[14]),
                            QtyPackagingUnit = Sm.DrStr(drDtl, cDtl[15])
                        });
                    }
                }
                drDtl.Close();
            }
            myLists.Add(ldtl);

            #endregion                                                    
            Sm.PrintReport("DKO", myLists, TableName, false);

        }

        #endregion

        #endregion

        #region Event
      
        private void BtnDRDocNo_Click(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmDKODlg(this, 1));
        }

        private void BtnPLDocNo_Click_1(object sender, EventArgs e)
        {
            Sm.FormShowDialog(new FrmDKODlg(this, 2));
        }

        private void BtnDRDocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtDRDocNo, "Delivery Request#", false))
            {
                try
                {
                    var f = new FrmDR(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = TxtDRDocNo.Text;
                    f.ShowDialog();
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
            }
        }

        private void BtnPLDocNo2_Click(object sender, EventArgs e)
        {
            if (!Sm.IsTxtEmpty(TxtPLDocNo, "Packing list#", false))
            {
                try
                {
                    var f = new FrmPL(mMenuCode);
                    f.Tag = mMenuCode;
                    f.WindowState = FormWindowState.Normal;
                    f.StartPosition = FormStartPosition.CenterScreen;
                    f.mDocNo = TxtPLDocNo.Text;
                    f.ShowDialog();
                }
                catch (Exception Exc)
                {
                    Sm.ShowErrorMsg(Exc);
                }
            }
        }

        private void LuePbsCode_EditValueChanged(object sender, EventArgs e)
        {
            Sm.RefreshLookUpEdit(LuePbsCode, new Sm.RefreshLue1(SetLuePbsCode));
        }


        #endregion
      
    }

    #region Report Class

    class DKOHdr
    {
        public string CompanyLogo { set; get; }
        public string CompanyName { get; set; }
        public string CompanyAddress { get; set; }
        public string CompanyPhone { get; set; }
        public string Shipper1 { get; set; }
        public string Shipper2 { get; set; }
        public string Shipper3 { get; set; }
        public string Shipper4 { get; set; }
        public string Shipper5 { get; set; }
        public string DocNo { get; set; }
        public string DocDt { get; set; }
        public string LocalDocNo { get; set; } 
        public string DRDocNo { get; set; }
        public string PLDocNo{ get; set; }
        public string FakoDocNo{ get; set; }
        public string CtName{ get; set; }
        public string Pbsregisterno{ get; set; }
        public string EmpName{ get; set; }
        public string Destination { get; set; }
        public string remark{ get; set; }
        public string PrintBy { get; set; }
        public string Port { get; set; }
        public string CancelInd { get; set; }
    }

    class DKODtl
    {

        public string ItCode { get; set; }
        public string QtyPackagingUnit { get; set; }
        public string ItCodeInternal{ get; set; }
        public string ItName{ get; set; }
        public string GrpName { get; set; }
        public decimal Height{ get; set; }
        public decimal Length{ get; set; }
        public decimal Width{ get; set; }
        
        public decimal Qty{ get; set; }
        public decimal Qty2{ get; set; }
        public decimal QtyInventory { get; set; }
        public decimal QtyInd { get; set; }
        public decimal Qty2Ind { get; set; }
        public decimal QtyInventoryInd { get; set; }

        public string PriceUomCode { get; set; }
        public string InventoryUomCode { get; set; }

    }

    #endregion
}
