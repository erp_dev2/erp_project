﻿#region Namespace

using System;
using System.Collections.Generic;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TenTec.Windows.iGridLib;

using Gv = RunSystem.GlobalVar;
using Sm = RunSystem.StdMtd;
using Sl = RunSystem.SetLue;
using DXE = DevExpress.XtraEditors;

#endregion

namespace RunSystem
{
    public partial class FrmRLPDlg : RunSystem.FrmBase4
    {
        #region Field

        private FrmRLP mFrmParent;
        private string 
            mSQL = string.Empty,
            mSalaryInd = string.Empty,
            mYr = string.Empty,
            mRLPCode = string.Empty,
            mDeptCode = string.Empty,
            mSiteCode = string.Empty;

        #endregion

        #region Constructor

        public FrmRLPDlg(FrmRLP FrmParent, string SalaryInd, string Yr, string RLPCode, string DeptCode, string SiteCode)
        {
            InitializeComponent();
            mFrmParent = FrmParent;
            mSalaryInd = SalaryInd;
            mYr = Yr;
            mRLPCode = RLPCode;
            mDeptCode = DeptCode;
            mSiteCode = SiteCode;
        }

        #endregion

        #region Method

        #region Form Method

        override protected void FrmLoad(object sender, EventArgs e)
        {
            try
            {
                base.FrmLoad(sender, e);
                Sm.ButtonVisible(mFrmParent.mAccessInd, ref BtnPrint, ref BtnExcel);
                SetGrd();
                SetSQL();
            }
            catch (Exception Exc)
            {
                Sm.ShowErrorMsg(Exc);
            }
        }

        #endregion

        #region Standard Method

        private void SetGrd()
        {
            Grd1.Cols.Count = 13;
            Grd1.FrozenArea.ColCount = 2;
            
            string Value2 = "None";
            if (mFrmParent.mIsUseADCodeSalary) Value2 = "Allowances";

            Sm.GrdHdrWithColWidth(
                    Grd1, new string[] 
                    {
                        //0
                        "No.",

                        //1-5
                        "",
                        "Employee's"+Environment.NewLine+"Code",
                        "Employee's Name",
                        "Old Code",
                        "Position",

                        //6-10
                        "Leave's"+Environment.NewLine+"Initial Date",
                        "Start Date",
                        "End Date",
                        "Number of Day",
                        "Salary",

                        //11-12
                        Value2,
                        "Amount"
                      },
                     new int[] 
                    {
                        //0
                        50,
 
                        //1-5
                        20, 100, 250, 80, 150, 
                        
                        //6-10
                        100, 100, 100, 100, 120,

                        //11-12
                        120, 130
                    }
                );
            Sm.GrdColCheck(Grd1, new int[] { 1 });
            Sm.GrdColReadOnly(true, true, Grd1, new int[] { 0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 });
            Sm.GrdFormatDate(Grd1, new int[] { 6, 7, 8 });
            Sm.GrdFormatDec(Grd1, new int[] { 9, 10, 11, 12 }, 0);
            Sm.GrdColInvisible(Grd1, new int[] { 2, 4, 5, 10, 11 }, false);
            Sm.SetGrdProperty(Grd1, false);
        }

        override protected void HideInfoInGrd()
        {
            Sm.GrdColInvisible(Grd1, new int[] { 2, 4, 5 }, !ChkHideInfoInGrd.Checked);
        }

        override protected void SetSQL()
        {
            var SQL = new StringBuilder();
            
            SQL.AppendLine("Select A.EmpCode, A.EmpName, A.EmpCodeOld, B.PosName, ");
            if (mSalaryInd == "1")
                SQL.AppendLine("E.BasicSalary2 As Salary, 0.00 As Value2 ");
            if (mSalaryInd == "2")
                SQL.AppendLine("E.Amt As Salary, F.Amt As Value2 ");
            SQL.AppendLine("From TblEmployee A ");
            SQL.AppendLine("Left Join TblPosition B On A.PosCode=B.PosCode ");
            SQL.AppendLine("Inner Join TblDepartment C On A.DeptCode=C.DeptCode ");
            SQL.AppendLine("Left Join TblSite D On A.SiteCode=D.SiteCode ");
            if (mSalaryInd=="1")
                SQL.AppendLine("Left Join TblGradeLevelHdr E On A.GrdLvlCode=E.GrdLvlCode ");

            if (mSalaryInd == "2")
            {
                SQL.AppendLine("Left Join TblEmployeeSalary E On A.EmpCode=E.EmpCode And E.ActInd='Y' ");
                SQL.AppendLine("Left Join ( ");
                SQL.AppendLine("    Select EmpCode, Sum(Amt) As Amt ");
                SQL.AppendLine("    From TblEmployeeAllowanceDeduction ");
                SQL.AppendLine("    Where Find_In_Set(IfNull(ADCode, ''), @ADCodeSalary) ");
                SQL.AppendLine("    And EmpCode In ( ");
                SQL.AppendLine("        Select EmpCode From TblEmployee ");
                SQL.AppendLine("        Where ((ResignDt Is Not Null And ResignDt>=@CurrentDate) Or ResignDt Is Null) ");
                SQL.AppendLine("        And DeptCode=@DeptCode ");
                if (mSiteCode.Length > 0)
                    SQL.AppendLine("        And SiteCode=@SiteCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine("    Group By EmpCode ");
                SQL.AppendLine(") F On A.EmpCode=F.EmpCode ");
            }
            SQL.AppendLine("Where ((A.ResignDt Is Not Null And A.ResignDt>=@CurrentDate) Or A.ResignDt Is Null) ");
            SQL.AppendLine("And A.DeptCode=@DeptCode ");
            SQL.AppendLine("And A.EmpCode Not In (");
            SQL.AppendLine("    Select T2.EmpCode ");
            SQL.AppendLine("    From TblRLPHdr T1 ");
            SQL.AppendLine("    Inner Join TblRLPDtl T2 On T1.DocNo=T2.DocNo ");
            SQL.AppendLine("    Where T1.CancelInd='N' ");
            SQL.AppendLine("    And T1.Status In ('O', 'A') ");
            SQL.AppendLine("    And T1.RLPCode=@RLPCode ");
            SQL.AppendLine("    And T1.Yr=@Yr ");
            SQL.AppendLine(") ");
            if (mSiteCode.Length>0) SQL.AppendLine("And A.SiteCode=@SiteCode ");
            if (!mFrmParent.mIsNotFilterByAuthorization)
            {
                SQL.AppendLine("And Exists( ");
                SQL.AppendLine("    Select EmpCode From TblEmployee ");
                SQL.AppendLine("    Where EmpCode=A.EmpCode ");
                SQL.AppendLine("    And GrdLvlCode In ( ");
                SQL.AppendLine("        Select T2.GrdLvlCode ");
                SQL.AppendLine("        From TblPPAHdr T1 ");
                SQL.AppendLine("        Inner Join TblPPADtl T2 On T1.DocNo=T2.DocNo ");
                SQL.AppendLine("        Where T1.ActInd='Y' And T1.UserCode=@UserCode ");
                SQL.AppendLine("    ) ");
                SQL.AppendLine(") ");
            }

            mSQL = SQL.ToString();
        }

        override protected void ShowData()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                Grd1.Rows.Count = 0;

                var Filter = " ";
                var EmpCode = string.Empty;
                var l1 = new List<RLP>();
                var l2 = new List<EmployeeResidualLeave>();
                var cm = new MySqlCommand();

                Sm.CmParam<String>(ref cm, "@UserCode", Gv.CurrentUserCode);
                Sm.CmParam<String>(ref cm, "@RLPCode", mRLPCode);
                Sm.CmParam<String>(ref cm, "@Yr", mYr);
                Sm.CmParam<String>(ref cm, "@DeptCode", mDeptCode);
                Sm.CmParam<String>(ref cm, "@SiteCode", mSiteCode);
                Sm.CmParam<String>(ref cm, "@ADCodeSalary", mFrmParent.mADCodeSalary);
                Sm.CmParamDt(ref cm, "@CurrentDate", Sm.ServerCurrentDateTime());
                Sm.FilterStr(ref Filter, ref cm, TxtEmpCode.Text, new string[] { "A.EmpCode", "A.EmpName", "A.EmpCodeOld" });

                
                for (int i = 0; i < mFrmParent.Grd1.Rows.Count; i++)
                {
                    EmpCode=Sm.GetGrdStr(mFrmParent.Grd1, i, 1);

                    if (EmpCode.Length > 0)
                    {
                        if (Filter.Length > 0) Filter += " And ";
                        Filter += "(A.EmpCode<>@EmpCode" + i.ToString() + ") ";
                        Sm.CmParam<String>(ref cm, "@EmpCode" + i.ToString(), EmpCode);
                    }
                }

                using (var cn = new MySqlConnection(Gv.ConnectionString))
                {
                    cn.Open();
                    cm.Connection = cn;
                    cm.CommandTimeout = 600;
                    cm.CommandText = mSQL + Filter + " Order By A.EmpCode;";
                    var dr = cm.ExecuteReader();
                    var c = Sm.GetOrdinal(dr, new string[] 
                    { 
                         //0
                         "EmpCode", 
                         
                         //1-5
                         "EmpName", 
                         "EmpCodeOld", 
                         "PosName",
                         "Salary",
                         "Value2"
                    });
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            EmpCode = dr.GetString(c[0]);
                            l1.Add(new RLP()
                            {
                                EmpCode = EmpCode,
                                EmpName = Sm.DrStr(dr, 1),
                                EmpCodeOld = Sm.DrStr(dr, 2),
                                PosName = Sm.DrStr(dr, 3),
                                Salary = Sm.DrDec(dr, 4),
                                Value2 = Sm.DrDec(dr, 5),
                                JoinDt = string.Empty,
                                StartDt = string.Empty,
                                EndDt = string.Empty,
                                NoOfDay = 0m,
                                Amt = 0m
                            });
                            l2.Add(new EmployeeResidualLeave() { EmpCode = EmpCode });
                        }
                    }
                    dr.Close();
                }

                if (l1.Count > 0)
                {
                    ProcessRLP1(ref l2);
                    ProcessRLP2(ref l1, ref l2);
                    ProcessRLP3(ref l1);

                    l1.Clear();
                    l2.Clear();

                }
                else
                {
                    Sm.StdMsg(mMsgType.NoData, string.Empty);
                }
            }
            catch (Exception Exc)
            {
                Sm.StdMsg(mMsgType.Warning, Exc.Message);
            }
            finally
            {
                Sm.FocusGrd(Grd1, 0, 1);
                Cursor.Current = Cursors.Default;
            }
        }

        private void ProcessRLP1(ref List<EmployeeResidualLeave> l)
        {
            if (mRLPCode == "01") Sm.GetEmpAnnualLeave(ref l, mYr);
            if (mRLPCode == "02") Sm.GetEmpLongServiceLeave(ref l, mYr);
        }

        private void ProcessRLP2(ref List<RLP> l1, ref List<EmployeeResidualLeave> l2)
        {
            int Index = 0;
            for (int i = 0; i < l1.Count; i++)
            {
                for (int j = Index; j < l2.Count; j++)
                {
                    if (Sm.CompareStr(l1[i].EmpCode, l2[j].EmpCode))
                    {
                        l1[i].JoinDt = l2[j].JoinDt;
                        l1[i].StartDt = l2[j].StartDt;
                        l1[i].EndDt = l2[j].EndDt;
                        l1[i].NoOfDay = l2[j].RemainingDay;
                        if (mSalaryInd == "2" && mRLPCode == "02")
                        { 
                            l1[i].Amt = l2[i].RemainingDay * ((l1[i].Salary + l1[i].Value2)/23);
                        }
                        else
                            l1[i].Amt = l2[i].RemainingDay * (l1[i].Salary + l1[i].Value2);
                        Index = j;
                        break;
                    }
                }
            }
        }

        private void ProcessRLP3(ref List<RLP> l)
        {
            l.Sort(delegate(RLP x1, RLP x2){ return x1.EmpName.CompareTo(x2.EmpName); });
            l.RemoveAll(x => x.Amt == 0);
            if (l.Count==0)
            {
                Sm.StdMsg(mMsgType.NoData, string.Empty);
                return;
            }
            int r = 0;
            Grd1.BeginUpdate();
            for (int i = 0; i < l.Count; i++)
            {
                Grd1.Rows.Add();
                Grd1.Cells[r, 0].Value = r + 1;
                Grd1.Cells[r, 1].Value = false; 
                Grd1.Cells[r, 2].Value = l[i].EmpCode;
                Grd1.Cells[r, 3].Value = l[i].EmpName;
                Grd1.Cells[r, 4].Value = l[i].EmpCodeOld;
                Grd1.Cells[r, 5].Value = l[i].PosName;
                Grd1.Cells[r, 6].Value = Sm.ConvertDate(l[i].JoinDt);
                Grd1.Cells[r, 7].Value = Sm.ConvertDate(l[i].StartDt);
                Grd1.Cells[r, 8].Value = Sm.ConvertDate(l[i].EndDt);
                Grd1.Cells[r, 9].Value = l[i].NoOfDay;
                Grd1.Cells[r, 10].Value = l[i].Salary;
                Grd1.Cells[r, 11].Value = l[i].Value2;
                Grd1.Cells[r, 12].Value = l[i].Amt;
                r++;
            }
            Grd1.EndUpdate();
        }

        override protected void ChooseData()
        {
            int Row1 = 0, Row2 = 0;
            bool IsChoose = false;

            if (Grd1.Rows.Count != 0)
            {
                mFrmParent.Grd1.BeginUpdate();
                for (int Row = 0; Row < Grd1.Rows.Count; Row++)
                {
                    if (Sm.GetGrdBool(Grd1, Row, 1) && !IsDataAlreadyChosen(Row))
                    {
                        if (IsChoose == false) IsChoose = true;

                        Row1 = mFrmParent.Grd1.Rows.Count - 1;
                        Row2 = Row;
                       
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 1, Grd1, Row2, 2);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 2, Grd1, Row2, 3);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 3, Grd1, Row2, 4);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 4, Grd1, Row2, 5);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 5, Grd1, Row2, 6);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 6, Grd1, Row2, 7);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 7, Grd1, Row2, 8);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 8, Grd1, Row2, 9);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 9, Grd1, Row2, 10);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 10, Grd1, Row2, 11);
                        Sm.CopyGrdValue(mFrmParent.Grd1, Row1, 11, Grd1, Row2, 12);
                        mFrmParent.Grd1.Rows.Add();
                        mFrmParent.Grd1.Cells[Row1, 0].Value = mFrmParent.Grd1.Rows.Count - 1;
                        Sm.SetGrdNumValueZero(mFrmParent.Grd1, mFrmParent.Grd1.Rows.Count - 1, new int[] { 8, 9, 10, 11 });
                    }
                }
                mFrmParent.Grd1.EndUpdate();
            }
            mFrmParent.ComputeAmt();
            if (!IsChoose) Sm.StdMsg(mMsgType.Warning, "You need to choose at least 1 record.");
        }

        private bool IsDataAlreadyChosen(int Row)
        {
            var EmpCode = Sm.GetGrdStr(Grd1, Row, 2);
            for (int Index = 0; Index < mFrmParent.Grd1.Rows.Count - 1; Index++)
                if (Sm.CompareStr(Sm.GetGrdStr(mFrmParent.Grd1, Index, 1), EmpCode)) return true;
            return false;
        }

        #endregion

        #region Grid Method

        override protected void GrdAfterContentsGrouped(object sender, EventArgs e)
        {
            Sm.SetGridNo(Grd1, 0, 1, true);
        }

        override protected void GrdAfterRowStateChanged(object sender, iGAfterRowStateChangedEventArgs e)
        {
        }

        #endregion

        #region Misc Control Method

        override protected void LueFontSizeEditValueChanged(object sender, EventArgs e)
        {
            if (Sm.GetLue(LueFontSize).Length != 0)
            {
                Grd1.Font = new Font(
                    Grd1.Font.FontFamily.Name.ToString(),
                    int.Parse(Sm.GetLue(LueFontSize))
                    );
            }
        }

        #endregion

        #endregion

        #region Event

        #region Misc Control Event

        private void ChkEmpCode_CheckedChanged(object sender, EventArgs e)
        {
            Sm.FilterSetTextEdit(this, sender, "Employee");
        }

        private void TxtEmpCode_Validated(object sender, EventArgs e)
        {
            Sm.FilterTxtSetCheckEdit(this, sender);
        }

        #endregion
       
        #endregion

        #region Class

        private class RLP
        {
            public string EmpCode { get; set; }
            public string EmpName { get; set; }
            public string EmpCodeOld { get; set; }
            public string PosName { get; set; }
            public decimal Salary { get; set; }
            public decimal Value2 { get; set; }
            public string JoinDt { get; set; }
            public string StartDt { get; set; }
            public string EndDt { get; set; }
            public decimal NoOfDay { get; set; }
            public decimal Amt { get; set; }
        }

        #endregion
    }
}
